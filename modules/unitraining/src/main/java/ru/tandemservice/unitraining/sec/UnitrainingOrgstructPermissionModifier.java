/* $Id$ */
package ru.tandemservice.unitraining.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author oleyba
 * @since 8/18/11
 */
public class UnitrainingOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uni");
        config.setName("unitraining-orgstruct-sec-config");
        config.setTitle("");

        final ModuleGlobalGroupMeta mggOrgstruct = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        final ModuleLocalGroupMeta mlgOrgstruct = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        final ModuleLocalGroupMeta mlgTraining = PermissionMetaUtil.createModuleLocalGroup(config, "uniTrLocal", "Модуль «БРС и журналы»");
        final ClassGroupMeta lcgJournal = PermissionMetaUtil.createClassGroup(mlgTraining, "trJournalLocalClass", "Объект «Реализация дисциплины»", ru.tandemservice.unitraining.base.entity.journal.TrJournal.class.getName());
        PermissionMetaUtil.createGroupRelation(config, "trJournalPG", lcgJournal.getName());

        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            final String code = description.getCode();
            final ClassGroupMeta gcg = PermissionMetaUtil.createClassGroup(mggOrgstruct, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            final ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(mlgOrgstruct, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            final PermissionGroupMeta pgTrainingTab = PermissionMetaUtil.createPermissionGroup(config, code + "TrainingTabPermissionGroup", "Вкладка «Журналы»");
            PermissionMetaUtil.createGroupRelation(config, pgTrainingTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, pgTrainingTab.getName(), lcg.getName());
            PermissionMetaUtil.createPermission(pgTrainingTab, "uniTrTabView_" + code, "Просмотр");

            final PermissionGroupMeta pgJournalTab = PermissionMetaUtil.createPermissionGroup(pgTrainingTab, code + "TrJournalTabPG", "Вкладка «Реализации дисциплин»");
            PermissionMetaUtil.createPermission(pgJournalTab, "trJournalTabView_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgJournalTab, "addTrJournal_" + code, "Добавление реализации");
            PermissionMetaUtil.createPermission(pgJournalTab, "genTrJournals_" + code, "Автоформирование реализаций");
            PermissionMetaUtil.createPermission(pgJournalTab, "editTrJournal_" + code, "Редактирование реализаций");
            PermissionMetaUtil.createPermission(pgJournalTab, "deleteTrJournal_" + code, "Удаление реализаций");

            final PermissionGroupMeta pgJournalGroupTab = PermissionMetaUtil.createPermissionGroup(pgTrainingTab, code + "TrainingEduGroupTabPG", "Вкладка «Учебные группы»");
            PermissionMetaUtil.createPermission(pgJournalGroupTab, "trEduGroupTabView_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgJournalGroupTab, "autoLinkEduGroups_" + code, "Автоматическое прикрепление групп");
            PermissionMetaUtil.createPermission(pgJournalGroupTab, "groupsJournalGen_" + code, "Формирование реализации на учебные группы");
            PermissionMetaUtil.createPermission(pgJournalGroupTab, "linkEduGroups_" + code, "Прикрепление групп");

            final PermissionGroupMeta pgTraningBrsTab = PermissionMetaUtil.createPermissionGroup(pgTrainingTab, code + "TrainingBrsTabPG", "Вкладка «БРС (читающее подр.)»");
            PermissionMetaUtil.createPermission(pgTraningBrsTab, "trBrsTabView_" + code, "Просмотр");

            final PermissionGroupMeta realGroupTab = PermissionMetaUtil.createPermissionGroup(config, code + "EppRealGroupPG", "Вкладка «Учебные группы»");

            final PermissionGroupMeta scheduleTab = PermissionMetaUtil.createPermissionGroup(realGroupTab, code + "EppRealGroupScheduleTabPG", "Вкладка «Расписание»");
            PermissionMetaUtil.createPermission(scheduleTab, "viewTrScheduleTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(scheduleTab, "editTrScheduleTab_" + code, "Редактирование расписания");

            final PermissionGroupMeta realGroupOwnerTab = PermissionMetaUtil.createPermissionGroup(realGroupTab, code + "EppRealGroupOwnerTabPG", "Вкладка «Диспетчерская»");
            final PermissionGroupMeta realGroupOwnerPub = PermissionMetaUtil.createPermissionGroup(realGroupOwnerTab, code + "EppRealGroupOwnerPubPG", "Карточка учебной группы для диспетчерской");
            PermissionMetaUtil.createPermission(realGroupOwnerPub, "pubJournalView_eppEduGroup_owner_" + code, "Просмотр журнала группы");
            PermissionMetaUtil.createPermission(realGroupOwnerPub, "pubJournalMark_eppEduGroup_owner_" + code, "Работа с журналом группы");
            PermissionMetaUtil.createPermission(realGroupOwnerPub, "pubJournalLink_eppEduGroup_owner_" + code, "Прикрепление группы к реализации");
            PermissionMetaUtil.createPermission(realGroupOwnerPub, "pubJournalAbsenceTabView_eppEduGroup_owner_" + code, "Просмотр причин пропусков занятий");
            PermissionMetaUtil.createPermission(realGroupOwnerPub, "pubJournalEditAbsenceReason_eppEduGroup_owner_" + code, "Редактирование причин пропусков занятий");

            final PermissionGroupMeta realGroupGroupTab = PermissionMetaUtil.createPermissionGroup(realGroupTab, code + "EppRealGroupGroupTabPG", "Вкладка «Деканат»");
            final PermissionGroupMeta realGroupGroupPub = PermissionMetaUtil.createPermissionGroup(realGroupGroupTab, code + "EppRealGroupGroupPubPG", "Карточка учебной группы для деканата");
            PermissionMetaUtil.createPermission(realGroupGroupPub, "pubJournalView_eppEduGroup_group_" + code, "Просмотр журнала группы");
            PermissionMetaUtil.createPermission(realGroupGroupPub, "pubJournalMark_eppEduGroup_group_" + code, "Работа с журналом группы");
            PermissionMetaUtil.createPermission(realGroupGroupPub, "pubJournalLink_eppEduGroup_group_" + code, "Прикрепление группы к реализации");
            PermissionMetaUtil.createPermission(realGroupGroupPub, "pubJournalAbsenceTabView_eppEduGroup_group_" + code, "Просмотр причин пропусков занятий");
            PermissionMetaUtil.createPermission(realGroupGroupPub, "pubJournalEditAbsenceReason_eppEduGroup_group_" + code, "Редактирование причин пропусков занятий");

            final PermissionGroupMeta realGroupTutorTab = PermissionMetaUtil.createPermissionGroup(realGroupTab, code + "EppRealGroupTutorTabPG", "Вкладка «Читающее подр.»");
            final PermissionGroupMeta realGroupTutorPub = PermissionMetaUtil.createPermissionGroup(realGroupTutorTab, code + "EppRealGroupTutorPubPG", "Карточка учебной группы для читащего подр.");
            PermissionMetaUtil.createPermission(realGroupTutorPub, "pubJournalView_eppEduGroup_tutor_" + code, "Просмотр журнала группы");
            PermissionMetaUtil.createPermission(realGroupTutorPub, "pubJournalMark_eppEduGroup_tutor_" + code, "Работа с журналом группы");
            PermissionMetaUtil.createPermission(realGroupTutorPub, "pubJournalLink_eppEduGroup_tutor_" + code, "Прикрепление группы к реализации");
            PermissionMetaUtil.createPermission(realGroupTutorPub, "pubJournalAbsenceTabView_eppEduGroup_tutor_" + code, "Просмотр причин пропусков занятий");
            PermissionMetaUtil.createPermission(realGroupTutorPub, "pubJournalEditAbsenceReason_eppEduGroup_tutor_" + code, "Редактирование причин пропусков занятий");

            final PermissionGroupMeta pgSessionTab = PermissionMetaUtil.createPermissionGroup(config, code + "SessionTabPermissionGroup", "Вкладка «Сессия»");
            final PermissionGroupMeta pgSessionBrsTab = PermissionMetaUtil.createPermissionGroup(pgSessionTab, code + "SessionBrsTabPG", "Вкладка «БРС (деканат)»");
            PermissionMetaUtil.createPermission(pgSessionBrsTab, "sessionBrsTabView_" + code, "Просмотр");

        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}

