package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.EduGroupList;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.logic.TrOrgUnitEduGroupDSHandler;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.EduGroupLink.TrOrgUnitEduGroupLink;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.EduGroupLink.TrOrgUnitEduGroupLinkUI;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.GroupsJournalGen.TrOrgUnitGroupsJournalGen;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
public class TrOrgUnitEduGroupListUI extends UIPresenter {

    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    public OrgUnitHolder getOrgUnitHolder() { return this.orgUnitHolder; }
    public Long getOrgUnitId() { return this.getOrgUnitHolder().getId(); }
    public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }
    public CommonPostfixPermissionModelBase getSecModel() { return this.getOrgUnitHolder().getSecModel(); }

    @Override
    public String getSettingsKey()
    {
        return getConfig().getName() + "." + String.valueOf(FastBeanUtils.getValue(this, "orgUnitHolder.id"));
    }

    @Override
    public void onComponentRefresh() {
        this.getOrgUnitHolder().refresh(OrgUnit.class);

        getConfig().getDataSource(TrOrgUnitEduGroupList.TR_ORGUNIT_EDU_GROUP_DS).doCleanupDataSource();
    }

    @Override
    public void onBeforeDataSourceFetch(final IUIDataSource dataSource)
    {
        // добавляем текущее подразделение и часть учебного года
        dataSource.put(TrOrgUnitEduGroupDSHandler.PARAM_ORG_UNIT, this.getOrgUnit());
        dataSource.put(TrOrgUnitEduGroupDSHandler.PARAM_YEAR_PART, this.getYearPart());

        if (TrOrgUnitEduGroupList.TR_ORGUNIT_EDU_GROUP_DS.equals(dataSource.getName()))
        {
            // добавляем значения фильтров с текущей страницы
            final Map<String, Object> settingMap = this._uiSettings.getAsMap(
                    TrOrgUnitEduGroupDSHandler.PARAM_ACTIVITY_ELEMENT_PART,
                    TrOrgUnitEduGroupDSHandler.PARAM_LINKED,
                    TrOrgUnitEduGroupDSHandler.PARAM_STATE,
                    TrOrgUnitEduGroupDSHandler.PARAM_TUTOR,
                    TrOrgUnitEduGroupDSHandler.PARAM_GROUP_LIST
            );
            dataSource.putAll(settingMap);
        }

        if (TrOrgUnitManager.TR_ORGUNIT_ACTIVITY_ELEMENT_PART_DS.equals(dataSource.getName()))
            dataSource.put(TrOrgUnitManager.TR_ORGUNIT_ORGUNIT_KEY, getOrgUnit());

        final boolean filterPps = Boolean.TRUE.equals(_uiSettings.get("filterPps"));

        if (filterPps && TrOrgUnitManager.TR_ORGUNIT_PPS_DS.equals(dataSource.getName()))
            dataSource.put(TrOrgUnitManager.TR_ORGUNIT_ORGUNIT_KEY, getOrgUnit());
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((CheckboxColumn) ((PageableSearchListDataSource) _uiConfig.getDataSource(TrOrgUnitEduGroupList.TR_ORGUNIT_EDU_GROUP_DS)).getLegacyDataSource().getColumn(TrOrgUnitEduGroupDSHandler.COLUMN_SELECT)).getSelectedObjects();
            selected.clear();
        }
    }

    public void onClickAutoLink()
    {
        TrOrgUnitManager.instance().dao().doAutoLinkEduGroups(getOrgUnit(), getYearPart());
    }

    public void onClickLink()
    {
        Collection<IEntity> selected = ((CheckboxColumn) ((PageableSearchListDataSource) _uiConfig.getDataSource(TrOrgUnitEduGroupList.TR_ORGUNIT_EDU_GROUP_DS)).getLegacyDataSource().getColumn(TrOrgUnitEduGroupDSHandler.COLUMN_SELECT)).getSelectedObjects();
        _uiActivation.asRegionDialog(TrOrgUnitEduGroupLink.class)
        .parameter(IUIPresenter.PUBLISHER_ID, getOrgUnitId())
        .parameter(TrOrgUnitEduGroupLinkUI.GROUP_ID_LIST, UniBaseDao.ids(selected))
        .activate();
    }

    public void onClickGroupsJournalGen()
    {
        final Collection<IEntity> selected = ((CheckboxColumn) ((PageableSearchListDataSource) _uiConfig.getDataSource(TrOrgUnitEduGroupList.TR_ORGUNIT_EDU_GROUP_DS)).getLegacyDataSource().getColumn(TrOrgUnitEduGroupDSHandler.COLUMN_SELECT)).getSelectedObjects();

        checkGroupsYearPart(selected);

        getActivationBuilder().asRegionDialog(TrOrgUnitGroupsJournalGen.class)
                .parameter(IUIPresenter.PUBLISHER_ID, getOrgUnitId())
                .parameter(TrOrgUnitEduGroupLinkUI.GROUP_ID_LIST, UniBaseDao.ids(selected))
                .activate();
    }

    protected Long getYearPartId() {
        final EppYearPart part = this.getYearPart();
        return (null == part ? null : part.getId());
    }

    public EppYearPart getYearPart() {
        return (EppYearPart)this._uiSettings.get(TrOrgUnitEduGroupDSHandler.PARAM_YEAR_PART);
    }

    /**
     * Проверяет что все УГС на одну Часть года.
     * @param groupList список УГС
     */
    protected void checkGroupsYearPart(Collection<IEntity> groupList)
    {
        if (groupList.isEmpty())
            return;

        final List<EppRealEduGroup> list = DataAccessServices.dao().<EppRealEduGroup>getList(
                new DQLSelectBuilder().fromEntity(EppRealEduGroup.class, "grp").column(property("grp"))
                        .where(in(property(EppRealEduGroup.id().fromAlias("grp")), UniBaseDao.ids(groupList))));

        final EppYearPart yearPart = list.get(0).getSummary().getYearPart();
        for (EppRealEduGroup group : list)
        {
            if (!yearPart.equals(group.getSummary().getYearPart()))
                throw new ApplicationException(TrOrgUnitManager.instance().getProperty("groupsJournalGen.error.different-eppYearPart"));
        }
    }
}
