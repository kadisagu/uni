/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.event;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.dset.IAffectedDSet;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteAction;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAddon;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/25/11
 */
public class TrJournalContentEditCheckDSetListener implements IDSetEventListener
{
    public void init()
    {
        // меняется состав модулей на дисциплину
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeInsert, TrJournalModule.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, TrJournalModule.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, TrJournalModule.class, this);

        // меняется состав событий
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeInsert, TrJournalEvent.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, TrJournalEvent.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, TrJournalEvent.class, this);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onEvent(DSetEvent event)
    {
        IAffectedDSet multitude = event.getMultitude();
        Class entityClass = multitude.getEntityMeta().getEntityClass();
        List<Long> entityIds = getEntityIds(event);

        if (TrJournalEvent.class.isAssignableFrom(entityClass)) {
            if (event.getEventType().isUpdate()) {
                // можно редактировать тему и комментарий
                Set<String> properties = new HashSet<String>(multitude.getAffectedProperties());
                properties.remove(TrJournalEvent.P_THEME);
                properties.remove(TrJournalEvent.P_COMMENT);
                properties.remove(TrJournalEvent.L_TYPE);
                properties.remove(TrJournalEvent.P_HAS_MARKS);
                if (properties.isEmpty()) {
                    return;
                }
            }
            
            TrJournalContentEditCheckDSetListener.action.register(event, getJournalIdsFromEvents(entityIds));
        } else if (TrJournalModule.class.isAssignableFrom(entityClass)) {
            if (event.getEventType().isUpdate())
            {
                // редкий случай, когда можно редактировать поля из натурального идентификатора  - объединение дублей элементов реестра
                final Set<String> properties = new HashSet<>(multitude.getAffectedProperties());
                properties.remove(TrJournalModule.L_MODULE);
                if (properties.isEmpty()) {
                    return;
                }
            }
            TrJournalContentEditCheckDSetListener.action.register(event, getJournalIdsFromModules(entityIds));
        }
    }

    protected List<Long> getEntityIds(DSetEvent event) {
        IAffectedDSet multitude = event.getMultitude();
        if (multitude.isSingular()) { return Collections.singletonList(multitude.getSingularEntity().getId()); }
        return new DQLSelectBuilder().fromDataSource(multitude.getSource(), "e").column("e.id").createStatement(event.getContext()).<Long>list();
    }

    private static final ParamTransactionCompleteAction<Long, Void> action = new ParamTransactionCompleteAction<Long, Void>() {
        @Override public Void beforeCompletion(Session session, Collection<Long> params) {

            if (isFoundNonFormativeJournal(params)) {
                throw new ApplicationException("Нельзя редактировать структуру чтения дисциплины в реализации, так как состояние реализации запрещает редактирование.");
            }

            return null;
        }
    };

    private Collection<Long> getJournalIdsFromModules(List<Long> entityIds)
    {
        final Set<Long> result = new HashSet<Long>();
        BatchUtils.execute(entityIds, 60, new BatchUtils.Action<Long>() {
            @Override public void execute(Collection<Long> elements) {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(TrJournalModule.class, "m")
                .where(in("m.id", elements))
                .where(in(property(TrJournalModule.journal().state().code().fromAlias("m")), EppState.READ_ONLY_CODES)) // состояние проверяем на момент запроса
                .column(property(TrJournalModule.journal().id().fromAlias("m")))
                .predicate(DQLPredicateType.distinct);
                result.addAll(IUniBaseDao.instance.get().<Long>getList(dql));
            }
        });
        return result;
    }

    private Collection<Long> getJournalIdsFromEvents(List<Long> entityIds)
    {
        final Set<Long> result = new HashSet<Long>();
        BatchUtils.execute(entityIds, 60, new BatchUtils.Action<Long>() {
            @Override public void execute(Collection<Long> elements) {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(TrJournalEvent.class, "e")
                .where(in("e.id", elements))
                .where(notInstanceOf("e", TrEventAddon.class))
                .where(in(property(TrJournalEvent.journalModule().journal().state().code().fromAlias("e")), EppState.READ_ONLY_CODES)) // состояние проверяем на момент запроса
                .column(property(TrJournalEvent.journalModule().journal().id().fromAlias("e")))
                .predicate(DQLPredicateType.distinct);
                result.addAll(IUniBaseDao.instance.get().<Long>getList(dql));
            }
        });
        return result;
    }

    private static boolean isFoundNonFormativeJournal(Collection<Long> journalIds)
    {
        final Set<Long> result = new HashSet<Long>();
        BatchUtils.execute(journalIds, 60, new BatchUtils.Action<Long>() {
            @Override public void execute(Collection<Long> elements) {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(TrJournal.class, "j")
                .where(in("j.id", elements))
                .where(in(property(TrJournal.state().code().fromAlias("j")), EppState.READ_ONLY_CODES))
                .column(property(TrJournal.id().fromAlias("j")))
                .predicate(DQLPredicateType.distinct);
                result.addAll(IUniBaseDao.instance.get().<Long>getList(dql));
            }
        });
        return !result.isEmpty();
    }
}
