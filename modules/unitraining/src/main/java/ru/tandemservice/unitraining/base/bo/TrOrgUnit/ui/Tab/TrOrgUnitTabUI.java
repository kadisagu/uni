/* $Id: DppOrgUnitTabUI.java 18117 2011-05-25 09:49:04Z vcheremuhin $ */
package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.sec.OrgUnitHolder;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
@State({
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class TrOrgUnitTabUI extends UIPresenter
{
    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    public OrgUnitHolder getOrgUnitHolder() { return this.orgUnitHolder; }
    public Long getOrgUnitId() { return this.getOrgUnitHolder().getId(); }
    public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }
    public CommonPostfixPermissionModelBase getSecModel() { return this.getOrgUnitHolder().getSecModel(); }

    @Override
    public void onComponentRefresh() {
        this.getOrgUnitHolder().refresh(OrgUnit.class);
    }


    private String selectedTab;
    public String getSelectedTab() { return this.selectedTab; }
    public void setSelectedTab(final String selectedTab) { this.selectedTab = selectedTab; }


}
