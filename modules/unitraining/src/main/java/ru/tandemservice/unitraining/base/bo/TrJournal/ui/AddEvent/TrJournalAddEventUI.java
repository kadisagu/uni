/* $Id: TrJournalEditUI.java 20463 2011-10-25 05:43:26Z vdanilov $ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.AddEvent;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.journal.*;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.EditBase.TrBrsCoefficientEditBaseUI;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;

/**
 * @author oleyba
 * @since 8/5/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journal.id", required = true),
    @Bind(key = TrJournalAddEventUI.BIND_MODULE, binding = "module.id"),
    @Bind(key = TrJournalAddEventUI.BIND_EDU_GROUP, binding = "group.id"),
    @Bind(key = TrJournalAddEventUI.BIND_IS_ADDITIONAL, binding = "additional", required = true)
})
@Output({
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_HOLDER, binding = "ownerHolder", required = true),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType", required = true)
})
public class TrJournalAddEventUI extends UIPresenter
{
    public static final String BIND_MODULE = "module";
    public static final String BIND_EDU_GROUP = "group";
    public static final String BIND_IS_ADDITIONAL = "additional";

    private TrJournal journal = new TrJournal();
    private TrJournalModule module = new TrJournalModule();
    private TrJournalGroup group = new TrJournalGroup();
    boolean additional;
    boolean isModuleSelected;

    private EntityHolder<IBrsSettingsOwner> ownerHolder = new EntityHolder<>();
    private TrBrsCoefficientOwnerType ownerType = new TrBrsCoefficientOwnerType();

    @Override
    public void onComponentActivate()
    {
        setModuleSelected(getModule().getId() != null);
    }

    @Override
    public void onComponentRender()
    {
        ContextLocal.beginPageTitlePart(isAdditional() ? "Добавление дополнительного события" : "Добавление контрольного мероприятия");
    }

    @Override
    public void onComponentRefresh() {
        setJournal(DataAccessServices.dao().get(TrJournal.class, getJournal().getId()));
        if (getModule() != null)
            setModule(getModule().getId() == null ? DataAccessServices.dao().getList(TrJournalModule.class, TrJournalModule.journal().s(), getJournal(), TrJournalModule.id().s()).iterator().next() : DataAccessServices.dao().get(TrJournalModule.class, getModule().getId()));
        if (getGroup() != null)
            setGroup(getGroup().getId() == null ? null : DataAccessServices.dao().get(TrJournalGroup.class, getGroup().getId()));
        setOwnerType(DataAccessServices.dao().get(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerType.code(), TrBrsCoefficientOwnerTypeCodes.JOURNAL_EVENT));
        setOwnerHolder(new EntityHolder<>());
        TrJournalEvent event;
        if (isAdditional())
            event = new TrEventAddon();
        else
            event = new TrEventAction();
        getOwnerHolder().setValue(event);
        event.setJournalModule(getModule());
        if (getGroup() != null)
            event.setType(getGroup().getGroup().getType());

        RenderComponent.deactivateAll();
    }

    public void onClickApply() {
        DataAccessServices.dao().doInTransaction(session -> {
            TrJournalEvent newEvent = (TrJournalEvent) getOwnerHolder().getValue();
            TrJournalManager.instance().eventDao().saveNewEvent(newEvent, getEvent().getJournalModule(), getGroup());

            TrBrsCoefficientEditBaseUI brsCoefffEditComponent = (TrBrsCoefficientEditBaseUI) _uiSupport.getChildrenUI().entrySet().iterator().next().getValue();
            brsCoefffEditComponent.save();

            return newEvent;
        });
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(TrJournalAddEvent.BIND_JOURNAL, getJournal());
        dataSource.put(TrJournalAddEvent.BIND_MODULE, getModule());
    }

    // presenter


    public boolean isModuleSelected()
    {
        return isModuleSelected;
    }

    public void setModuleSelected(boolean moduleSelected)
    {
        isModuleSelected = moduleSelected;
    }

    public boolean getLoadTypeSelected()
    {
        return getGroup() != null;
    }

    public TrJournalEvent getEvent()
    {
        return (TrJournalEvent) getOwnerHolder().getValue();
    }

    // accessors


    public TrJournalModule getModule()
    {
        return module;
    }

    public void setModule(TrJournalModule module)
    {
        this.module = module;
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(TrBrsCoefficientOwnerType ownerType)
    {
        this.ownerType = ownerType;
    }

    public boolean isAdditional()
    {
        return additional;
    }

    public void setAdditional(boolean additional)
    {
        this.additional = additional;
    }

    public EntityHolder<IBrsSettingsOwner> getOwnerHolder()
    {
        return ownerHolder;
    }

    public void setOwnerHolder(EntityHolder<IBrsSettingsOwner> ownerHolder)
    {
        this.ownerHolder = ownerHolder;
    }

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }

    public TrJournalGroup getGroup()
    {
        return group;
    }

    public void setGroup(TrJournalGroup group)
    {
        this.group = group;
    }
}
