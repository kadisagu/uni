/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkView;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.mutable.MutableDouble;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsIRatingValueFormatter;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkInfo.TrJournalMarkInfo;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkCell;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkColumn;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkColumnGroup;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkRow;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/7/11
 */
@Input
({
    @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "journal.id", required = true)
})
public class TrJournalMarkViewUI extends UIPresenter
{
    private TrJournal journal = new TrJournal();

    private List<TrJournalMarkColumnGroup> columnGroupList = new ArrayList<>();
    private List<TrJournalMarkRow> rowList = new ArrayList<>();
    private Collection<EppGroupType> typeList = new LinkedHashSet<>();
    private List<ISessionBrsDao.IRatingAdditParamDef> additParamList = new ArrayList<>();

    private TrJournalMarkColumnGroup currentColumnGroup;
    private TrJournalMarkColumn currentColumn;
    private TrJournalMarkRow currentRow;
    private ISessionBrsDao.IRatingAdditParamDef currentAdditParam;
    private ISessionBrsDao.IRatingAdditParamDef currentStatParam;

    private Map<MultiKey, TrJournalMarkCell> cellMap = new HashMap<>();
    private Map<Long, MutableDouble> totals = SafeMap.get(MutableDouble.class);
    private Map<MultiKey, MutableDouble> loadTypeTotals = SafeMap.get(MutableDouble.class);

    private Set<Long> wrappedBlockIds = new HashSet<>();

    private IBrsDao.ICurrentRatingCalc calculatedRating;
    private IBrsDao.IJournalEventStatCalc calculatedStat;

    // actions

    @Override
    public void onComponentRefresh()
    {
        setJournal(IUniBaseDao.instance.get().getNotNull(TrJournal.class, getJournal().getId()));

        getColumnGroupList().clear();
        final List<TrJournalEvent> events = IUniBaseDao.instance.get().getList(TrJournalEvent.class, TrJournalEvent.journalModule().journal().s(), getJournal(), TrJournalEvent.journalModule().module().number().s(), TrJournalEvent.number().s());

        for (TrJournalModule module : UniBaseUtils.<TrJournalModule>getPropertiesSet(events, TrJournalEvent.journalModule().s()))
        {
            TrJournalMarkColumnGroup wrapper = new TrJournalMarkColumnGroup(module);
            for (TrJournalEvent event : events)
            {
                if (event.getJournalModule().equals(module))
                    wrapper.getColumnList().add(new TrJournalMarkColumn(event));
                getTypeList().add(event.getType());
            }
            getColumnGroupList().add(wrapper);
        }
        getRowList().clear();
        for (Map.Entry<EppStudentWorkPlanElement, Boolean> entry : getStudentMap().entrySet())
            getRowList().add(new TrJournalMarkRow(entry.getKey(), entry.getValue()));

        setCalculatedRating(TrBrsCoefficientManager.instance().brsDao().getCalculatedRating(getJournal()));
        setCalculatedStat(TrBrsCoefficientManager.instance().brsDao().getCalculatedStat(getJournal()));

        fillCellMap();

        setAdditParamList(new ArrayList<ISessionBrsDao.IRatingAdditParamDef>());
        Map<String, ISessionBrsDao.IRatingAdditParamDef> additParamDefMap = TrBrsCoefficientManager.instance().brsDao().getRatingAdditionalParamDefinitions(getJournal());
        for (String key : TrBrsCoefficientManager.instance().brsDao().getDisplayableAdditParams(getJournal())) {
            ISessionBrsDao.IRatingAdditParamDef def = additParamDefMap.get(key);
            if (null != def)
                getAdditParamList().add(def);    
        }
    }

    public void onClickShowInfo()
    {
        _uiActivation.asRegion(TrJournalMarkInfo.class, "dialog")
        .parameter(IUIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
        .activate();
    }

    public void onClickWrap()
    {
        final Long columnBlockId = getListenerParameterAsLong();
        if (!wrappedBlockIds.remove(columnBlockId))
            wrappedBlockIds.add(columnBlockId);
    }

    // presenters

    public int getTableRowCount()
    {
        return 3 + getRowList().size() + getCalculatedStat().getStatParamDefinitions().size() + 1;
    }

    public int getTableColumnCount() {
        int count = 2 + getAdditParamList().size();
        for (TrJournalMarkColumnGroup group : getColumnGroupList())
            if (!wrappedBlockIds.contains(group.getId()))
                count = count + group.getColumnListSize() + 1;
        return count;
    }

    public boolean isShowModuleRating()
    {
        return getCalculatedRating().isModuleRatingCalculated();
    }

    public ISessionBrsDao.IRatingValue getRowModuleRating()
    {
        if (!isShowModuleRating())
            return null;
        IBrsDao.IStudentCurrentRatingData studentRating = getCalculatedRating().getCurrentRating(getCurrentRow().getStudent());
        return studentRating == null ? null : studentRating.getModuleRating(getCurrentColumnGroup().getModule().getModule().getModule());
    }

    public ISessionBrsDao.IRatingValue getRowRating()
    {
        IBrsDao.IStudentCurrentRatingData studentRating = getCalculatedRating().getCurrentRating(getCurrentRow().getStudent());
        return studentRating == null ? null : studentRating.getRatingValue();
    }

    public String getRatingValue(ISessionBrsDao.IRatingValue rating)
    {
        if (rating == null)
            return "-";
        if (rating.getValue() == null && rating.getMessage() != null)
            return "!";
        if (rating.getValue() == null)
            return "-";
        return DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(rating.getValue());
    }

    public String getRatingErrorText(ISessionBrsDao.IRatingValue rating)
    {
        return null == rating ? null : rating.getMessage();
    }

    public Boolean isRatingHasError(ISessionBrsDao.IRatingValue rating)
    {
        return null != rating && (rating.getValue() == null || rating.getMessage() != null);
    }

    public List<TrJournalMarkColumn> getColumnList()
    {
        return getCurrentColumnGroup().getColumnList();
    }

    public String getCurrentRowStyle()
    {
        return getCurrentRow().isActive() ? "" : "color: gray";
    }

    public String getCurrentRowClassName()
    {
        return getRowList().indexOf(getCurrentRow()) % 2 == 0 ? "list-row list-row-odd" : "list-row list-row-even";
    }

    public String getCurrentStatRowClassName()
    {
        return (getCalculatedStat().getStatParamDefinitions().indexOf(getCurrentStatParam()) % 2 == 0 ? "list-row list-row-odd" : "list-row list-row-even");
    }

    public String getCurrentRowNumber() {
        return String.valueOf(getRowList().indexOf(getCurrentRow()) + 1);
    }

    public TrJournalMarkCell getCurrentCell()
    {
        return getCellMap().get(createCellKey(getCurrentColumn().getJEvent(), getCurrentRow().getStudent()));
    }

    public boolean isColumnGroupShow()
    {
        return !wrappedBlockIds.contains(getCurrentColumnGroup().getId());
    }

    public String getColumnGroupWrapButtonId()
    {
        return "columnBlockWrap" + getCurrentColumnGroup().getId();
    }
    
    public String getAdditParamValue() {
        IBrsDao.IStudentCurrentRatingData studentRating = getCalculatedRating().getCurrentRating(getCurrentRow().getStudent());
        if (studentRating == null || getCurrentAdditParam() == null)
            return "";
        ISessionBrsDao.IRatingValue paramValue = studentRating.getRatingAdditParam(getCurrentAdditParam().getKey());
        return BrsIRatingValueFormatter.instance.format(paramValue);
    }

    public String getStatParamValue() {
        if (getCurrentColumn() == null || getCurrentStatParam() == null)
            return "";
        ISessionBrsDao.IRatingValue paramValue = getCalculatedStat().getStatValue(getCurrentColumn().getJEvent(), getCurrentStatParam().getKey());
        return BrsIRatingValueFormatter.instance.format(paramValue);
    }

    public String getAggregatedRatingValue()
    {
        if (getCurrentStatParam() == null)
            return "";
        IBrsDao.IAggregatedRatingValue paramValue = getCalculatedStat().getStatValueAggregated(getCurrentStatParam().getKey(), IBrsDao.RATING_COLUMN_KEY);
        return paramValue.isApplicable() ? BrsIRatingValueFormatter.instance.format(paramValue) : "";
    }

    public String getAggregatedAdditParamValue()
    {
        if (getCurrentStatParam() == null || getCurrentAdditParam() == null)
            return "";
        IBrsDao.IAggregatedRatingValue paramValue = getCalculatedStat().getStatValueAggregated(getCurrentStatParam().getKey(), getCurrentAdditParam().getKey());
        return paramValue.isApplicable() ? BrsIRatingValueFormatter.instance.format(paramValue) : "";
    }

    // util

    private MultiKey createCellKey(TrJournalEvent event, EppStudentWorkPlanElement studentSlot)
    {
        return new MultiKey(event.getId(), studentSlot.getId());
    }

    // data access

    private void fillCellMap()
    {
        cellMap.clear();
        totals.clear();
        loadTypeTotals.clear();

        Map<Long, TrJournalMarkColumnGroup> groupMap = new HashMap<>();
        for (TrJournalMarkColumnGroup group : getColumnGroupList())
        {
            groupMap.put(group.getId(), group);
            for (TrJournalMarkColumn column : group.getColumnList())
                for (TrJournalMarkRow row : getRowList())
                    cellMap.put(createCellKey(column.getJEvent(), row.getStudent()), new TrJournalMarkCell(column.getJEvent()));
        }

        DQLSelectBuilder eventDQL = new DQLSelectBuilder()
            .fromEntity(TrEduGroupEventStudent.class, "event", true).column("event")
            .where(eq(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("event")), value(getJournal())))
            .where(isNull(property(TrEduGroupEventStudent.transferDate().fromAlias("event"))))
            ;
        final List<TrEduGroupEventStudent> events = IUniBaseDao.instance.get().getList(eventDQL);

        for (TrEduGroupEventStudent event : events)
        {
            final TrJournalEvent journalEvent = event.getEvent().getJournalEvent();
            final EppGroupType type = journalEvent.getType();

            cellMap.put(createCellKey(journalEvent, event.getStudentWpe()), new TrJournalMarkCell(event));

            groupMap.get(journalEvent.getJournalModule().getId()).incTotal(event);
            if (null != event.getGrade())
            {
                totals.get(event.getStudentWpe().getId()).add(event.getGrade());
                loadTypeTotals.get(new MultiKey(type.getId(), event.getStudentWpe().getId())).add(event.getGrade());
            }
        }
    }

    private Map<EppStudentWorkPlanElement, Boolean> getStudentMap()
    {
        // Значение в мапе (Boolean) - в УГС студент или нет
        Map<EppStudentWorkPlanElement, Boolean> map = new TreeMap<>(EppStudentWorkPlanElement.FULL_FIO_COMPARATOR);

        DQLSelectBuilder studentMarkedDQL = new DQLSelectBuilder()
                .fromEntity(TrEduGroupEventStudent.class, "event").column(property(TrEduGroupEventStudent.studentWpe().fromAlias("event")))
                .where(eq(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("event")), value(getJournal())))
                .where(isNull(property(TrEduGroupEventStudent.transferDate().fromAlias("event"))))
                ;
        for (EppStudentWorkPlanElement student : DataAccessServices.dao().<EppStudentWorkPlanElement>getList(studentMarkedDQL))
        {
            map.put(student, false);
        }

        DQLSelectBuilder studentFromGroupsDQL = new DQLSelectBuilder()
            .fromEntity(EppRealEduGroupRow.class, "eppRow").column(property(EppRealEduGroupRow.studentWpePart().studentWpe().fromAlias("eppRow")))
            .joinEntity("eppRow", DQLJoinType.inner, TrJournalGroup.class, "jg", eq(property(TrJournalGroup.group().fromAlias("jg")), property(EppRealEduGroupRow.group().fromAlias("eppRow"))))
            .where(eq(property(TrJournalGroup.journal().fromAlias("jg")), value(getJournal())))
            ;
        for (EppStudentWorkPlanElement student : DataAccessServices.dao().<EppStudentWorkPlanElement>getList(studentFromGroupsDQL))
        {
            map.put(student, true);
        }

        return map;
    }

    // getters and setters

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }

    public List<TrJournalMarkColumnGroup> getColumnGroupList()
    {
        return columnGroupList;
    }

    public void setColumnGroupList(List<TrJournalMarkColumnGroup> columnGroupList)
    {
        this.columnGroupList = columnGroupList;
    }

    public List<TrJournalMarkRow> getRowList()
    {
        return rowList;
    }

    public void setRowList(List<TrJournalMarkRow> rowList)
    {
        this.rowList = rowList;
    }

    public TrJournalMarkColumnGroup getCurrentColumnGroup()
    {
        return currentColumnGroup;
    }

    public void setCurrentColumnGroup(TrJournalMarkColumnGroup currentColumnGroup)
    {
        this.currentColumnGroup = currentColumnGroup;
    }

    public TrJournalMarkColumn getCurrentColumn()
    {
        return currentColumn;
    }

    public void setCurrentColumn(TrJournalMarkColumn currentColumn)
    {
        this.currentColumn = currentColumn;
    }

    public TrJournalMarkRow getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(TrJournalMarkRow currentRow)
    {
        this.currentRow = currentRow;
    }

    public Map<MultiKey, TrJournalMarkCell> getCellMap()
    {
        return cellMap;
    }

    public void setCellMap(Map<MultiKey, TrJournalMarkCell> cellMap)
    {
        this.cellMap = cellMap;
    }

    public Collection<EppGroupType> getTypeList()
    {
        return typeList;
    }

    public void setTypeList(Collection<EppGroupType> typeList)
    {
        this.typeList = typeList;
    }

    public IBrsDao.ICurrentRatingCalc getCalculatedRating()
    {
        return calculatedRating;
    }

    public void setCalculatedRating(IBrsDao.ICurrentRatingCalc calculatedRating)
    {
        this.calculatedRating = calculatedRating;
    }

    public List<ISessionBrsDao.IRatingAdditParamDef> getAdditParamList() {
        return additParamList;
    }

    public void setAdditParamList(List<ISessionBrsDao.IRatingAdditParamDef> additParamList) {
        this.additParamList = additParamList;
    }

    public ISessionBrsDao.IRatingAdditParamDef getCurrentAdditParam() {
        return currentAdditParam;
    }

    public void setCurrentAdditParam(ISessionBrsDao.IRatingAdditParamDef currentAdditParam) {
        this.currentAdditParam = currentAdditParam;
    }

    public IBrsDao.IJournalEventStatCalc getCalculatedStat()
    {
        return calculatedStat;
    }

    public void setCalculatedStat(IBrsDao.IJournalEventStatCalc calculatedStat)
    {
        this.calculatedStat = calculatedStat;
    }

    public ISessionBrsDao.IRatingAdditParamDef getCurrentStatParam()
    {
        return currentStatParam;
    }

    public void setCurrentStatParam(ISessionBrsDao.IRatingAdditParamDef currentStatParam)
    {
        this.currentStatParam = currentStatParam;
    }
}