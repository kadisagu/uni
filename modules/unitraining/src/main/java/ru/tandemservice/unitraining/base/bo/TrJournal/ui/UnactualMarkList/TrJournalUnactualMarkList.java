/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.UnactualMarkList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.TrJournalUnactualMarksDSHandler;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;

/**
 * @author oleyba
 * @since 9/8/11
 */
@Configuration
public class TrJournalUnactualMarkList extends BusinessComponentManager
{
    public static final String TR_JOURNAL_UNACTUAL_MARK_DS = "trJournalUnactualMarkDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(this.searchListDS(TrJournalUnactualMarkList.TR_JOURNAL_UNACTUAL_MARK_DS, this.trJournalUnactualMarkDS(), this.trJournalUnactualMarkDSHandler()))
        .addDataSource(TrJournalManager.instance().trJournalStudentDSConfig())
        .create();
    }

    @Bean
    public ColumnListExtPoint trJournalUnactualMarkDS() {
        return this.columnListExtPointBuilder(TrJournalUnactualMarkList.TR_JOURNAL_UNACTUAL_MARK_DS)
        .addColumn(textColumn("studentFio", TrEduGroupEventStudent.studentWpe().student().person().identityCard().fullFio()).create())
        .addColumn(textColumn("eduGroup", TrEduGroupEventStudent.event().group().title()).create())
        .addColumn(textColumn("eventTitle", TrJournalUnactualMarksDSHandler.COLUMN_EVENT_TITLE).create())
        .addColumn(textColumn("eventTime", TrEduGroupEventStudent.event().scheduleEvent().durationBegin()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).create())
        .addColumn(booleanColumn("absenceNote").path(TrJournalUnactualMarksDSHandler.COLUMN_ABSENCE_NOTE).create())
        .addColumn(textColumn("grade", "grade").create())
        .addColumn(textColumn("comment", TrEduGroupEventStudent.comment()).create())
        .addColumn(textColumn("markedBy", TrEduGroupEventStudent.markedBy().s() + ".fullTitle").create())
        .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("deleteUnactualMarkTrJournal").create())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trJournalUnactualMarkDSHandler() {
        return new TrJournalUnactualMarksDSHandler(this.getName());
    }
}
