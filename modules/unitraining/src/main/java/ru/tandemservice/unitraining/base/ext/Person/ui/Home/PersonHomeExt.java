/* $Id$ */
package ru.tandemservice.unitraining.base.ext.Person.ui.Home;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.person.base.bo.Person.ui.Home.PersonHome;

/**
 * @author Vasily Zhukov
 * @since 17.11.2011
 */
@Configuration
public class PersonHomeExt extends BusinessComponentExtensionManager
{
    public static final String BUTTON_NAME = "ppsJournalPageButton";
    public static final String ADDON_NAME = "unitraining" + PersonHomeExtUI.class.getSimpleName();
    public static final String LISTENER_NAME = ADDON_NAME + ":onClickGoToPpsJournalPage";
    public static final String VISIBLE_PROPERTY = "addon:" + ADDON_NAME + ".ppsFound";

    @Autowired
    private PersonHome _personHome;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_personHome.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, PersonHomeExtUI.class))
                .create();
    }

    @Bean
    public ButtonListExtension homeBLExtension()
    {
        return buttonListExtensionBuilder(_personHome.homeBLExtPoint())
                .addButton(submitButton(BUTTON_NAME, LISTENER_NAME).visible(VISIBLE_PROPERTY).create())
                .create();
    }
}
