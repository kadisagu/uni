package ru.tandemservice.unitraining.base.entity.journal.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAction;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Событие в реализации (текущий контроль)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrEventActionGen extends TrJournalEvent
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.base.entity.journal.TrEventAction";
    public static final String ENTITY_NAME = "trEventAction";
    public static final int VERSION_HASH = -2024942348;
    private static IEntityMeta ENTITY_META;

    public static final String L_ACTION_TYPE = "actionType";

    private EppIControlActionType _actionType;     // Форма текущего контроля

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Форма текущего контроля. Свойство не может быть null.
     */
    @NotNull
    public EppIControlActionType getActionType()
    {
        return _actionType;
    }

    /**
     * @param actionType Форма текущего контроля. Свойство не может быть null.
     */
    public void setActionType(EppIControlActionType actionType)
    {
        dirty(_actionType, actionType);
        _actionType = actionType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TrEventActionGen)
        {
            setActionType(((TrEventAction)another).getActionType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrEventActionGen> extends TrJournalEvent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrEventAction.class;
        }

        public T newInstance()
        {
            return (T) new TrEventAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "actionType":
                    return obj.getActionType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "actionType":
                    obj.setActionType((EppIControlActionType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "actionType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "actionType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "actionType":
                    return EppIControlActionType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrEventAction> _dslPath = new Path<TrEventAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrEventAction");
    }
            

    /**
     * @return Форма текущего контроля. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrEventAction#getActionType()
     */
    public static EppIControlActionType.Path<EppIControlActionType> actionType()
    {
        return _dslPath.actionType();
    }

    public static class Path<E extends TrEventAction> extends TrJournalEvent.Path<E>
    {
        private EppIControlActionType.Path<EppIControlActionType> _actionType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Форма текущего контроля. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrEventAction#getActionType()
     */
        public EppIControlActionType.Path<EppIControlActionType> actionType()
        {
            if(_actionType == null )
                _actionType = new EppIControlActionType.Path<EppIControlActionType>(L_ACTION_TYPE, this);
            return _actionType;
        }

        public Class getEntityClass()
        {
            return TrEventAction.class;
        }

        public String getEntityName()
        {
            return "trEventAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
