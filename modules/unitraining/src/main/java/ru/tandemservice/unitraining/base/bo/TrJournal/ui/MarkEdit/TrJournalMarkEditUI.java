/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

/**
 * @author oleyba
 * @since 9/8/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journal.id", required = true)
})
@Output({
    @Bind(key = TrJournalGroupMarkEditUI.GROUP_BINDING, binding = "eduGroup.id")
})
public class TrJournalMarkEditUI extends UIPresenter
{
    private TrJournal journal = new TrJournal();
    private TrJournalGroup eduGroup;
    private boolean selected;

    // actions

    @Override
    public void onComponentRefresh()
    {
        setJournal(IUniBaseDao.instance.get().getNotNull(TrJournal.class, getJournal().getId()));
    }

    public void onClickSelect()
    {
        setSelected(null != eduGroup);
        RenderComponent.deactivate("TrJournalGroupMarkEdit");
        //
        //        final IUIPresenter trJournalGroupMark = _uiSupport.getChildUI(RenderComponent.getRegionName("TrJournalGroupMarkEdit"));
        //        if (null != trJournalGroupMark)
        //            trJournalGroupMark.deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(TrJournalManager.TR_JOURNAL_JOURNAL_KEY, getJournal());
    }

    // presenters

    public String getDisciplineTitle()
    {
        return getJournal().getRegistryElementPart().getTitle();
    }

    // getters and setters

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }

    public TrJournalGroup getEduGroup()
    {
        return eduGroup;
    }

    public void setEduGroup(TrJournalGroup eduGroup)
    {
        this.eduGroup = eduGroup;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }
}
