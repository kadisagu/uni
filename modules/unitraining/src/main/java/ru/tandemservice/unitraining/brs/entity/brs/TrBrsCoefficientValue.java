package ru.tandemservice.unitraining.brs.entity.brs;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unitraining.brs.entity.brs.gen.TrBrsCoefficientValueGen;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;

/**
 * Значение коэффициента для БРС
 */
public class TrBrsCoefficientValue extends TrBrsCoefficientValueGen implements ISessionBrsDao.IBrsCoefficientValue
{
    public TrBrsCoefficientValue()
    {
    }
    
    public TrBrsCoefficientValue(IBrsSettingsOwner owner, TrBrsCoefficientDef def)
    {
        setOwner(owner);
        setDefinition(def);
    }

    @Override
    public boolean isBoolValue()
    {
        return getDefinition().isBoolValueType();
    }

    @Override
    @EntityDSLSupport(parts = TrBrsCoefficientValue.P_VALUE_AS_LONG)
    public double getValueAsDouble()
    {
        if (isBoolValue())
            throw new IllegalStateException();
        return ((double) getValueAsLong()) / 100;
    }

    public void setValueAsDouble(double value)
    {
        if (isBoolValue())
            throw new IllegalStateException();
        setValueAsLong(Math.round(value * 100));
    }

    @Override
    @EntityDSLSupport(parts = TrBrsCoefficientValue.P_VALUE_AS_LONG)
    public boolean isValueAsBoolean()
    {
        if (!isBoolValue())
            throw new IllegalStateException();
        return  getValueAsLong() > 0;
    }

    public void setValueAsBoolean(boolean value)
    {
        if (!isBoolValue())
            throw new IllegalStateException();
        setValueAsLong(value ? 1L : 0L);
    }
    
    public void setValue(Object value)
    {
        if (isBoolValue() && value instanceof Boolean) {
            setValueAsBoolean((Boolean) value);
            return;
        }
        if (!isBoolValue() && value instanceof Number) {
            setValueAsDouble(((Number) value).doubleValue());
            return;
        }
        throw new IllegalStateException();
    }

    public Object getValue()
    {
        if (isBoolValue())
            return isValueAsBoolean();
        else
            return getValueAsDouble();
    }
    
    @Override
    public String getValueStr()
    {
        if (isBoolValue())
            return YesNoFormatter.IGNORE_NUMBERS.format(isValueAsBoolean());
        else 
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getValueAsDouble());
    }
}