package ru.tandemservice.unitraining.base.entity.event;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.unitraining.base.entity.event.gen.TrEduGroupEventGen;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;

/**
 * Событие УГС
 *
 * Реализация планируемого события для УГС - показывает, что в УГС есть данное событие
 * (объекты создаются автоматически для всех пар УГС и событий журнала)
 */
public class TrEduGroupEvent extends TrEduGroupEventGen implements ITitled
{
    public TrEduGroupEvent() {}
    public TrEduGroupEvent(EppRealEduGroup group, TrJournalEvent journalEvent) {
        setGroup(group);
        setJournalEvent(journalEvent);
    }

    @Override
    public String getTitle()
    {
        if (getJournalEvent() == null) {
            return this.getClass().getSimpleName();
        }
        return "Событие " + getJournalEvent().getTitle() + " в реализации " + getJournalEvent().getJournalModule().getJournal().getCalculatedTitle() + " для УГС " + getGroup().getTitle();
    }
}