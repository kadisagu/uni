package ru.tandemservice.unitraining.base.entity.journal.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Журнал (УГС в реализации дисциплины)
 *
 * Определяет из каких УГС состоит журнал (по разным видам нагрузки)
 * Все УГС считаются равноправными и не делятся на потоки (на уровне журналов)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrJournalGroupGen extends EntityBase
 implements INaturalIdentifiable<TrJournalGroupGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup";
    public static final String ENTITY_NAME = "trJournalGroup";
    public static final int VERSION_HASH = 2077777155;
    private static IEntityMeta ENTITY_META;

    public static final String L_JOURNAL = "journal";
    public static final String L_GROUP = "group";

    private TrJournal _journal;     // Журнал
    private EppRealEduGroup _group;     // Учебная группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Журнал. Свойство не может быть null.
     */
    @NotNull
    public TrJournal getJournal()
    {
        return _journal;
    }

    /**
     * @param journal Журнал. Свойство не может быть null.
     */
    public void setJournal(TrJournal journal)
    {
        dirty(_journal, journal);
        _journal = journal;
    }

    /**
     * @return Учебная группа. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppRealEduGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Учебная группа. Свойство не может быть null и должно быть уникальным.
     */
    public void setGroup(EppRealEduGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TrJournalGroupGen)
        {
            if (withNaturalIdProperties)
            {
                setJournal(((TrJournalGroup)another).getJournal());
                setGroup(((TrJournalGroup)another).getGroup());
            }
        }
    }

    public INaturalId<TrJournalGroupGen> getNaturalId()
    {
        return new NaturalId(getJournal(), getGroup());
    }

    public static class NaturalId extends NaturalIdBase<TrJournalGroupGen>
    {
        private static final String PROXY_NAME = "TrJournalGroupNaturalProxy";

        private Long _journal;
        private Long _group;

        public NaturalId()
        {}

        public NaturalId(TrJournal journal, EppRealEduGroup group)
        {
            _journal = ((IEntity) journal).getId();
            _group = ((IEntity) group).getId();
        }

        public Long getJournal()
        {
            return _journal;
        }

        public void setJournal(Long journal)
        {
            _journal = journal;
        }

        public Long getGroup()
        {
            return _group;
        }

        public void setGroup(Long group)
        {
            _group = group;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TrJournalGroupGen.NaturalId) ) return false;

            TrJournalGroupGen.NaturalId that = (NaturalId) o;

            if( !equals(getJournal(), that.getJournal()) ) return false;
            if( !equals(getGroup(), that.getGroup()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getJournal());
            result = hashCode(result, getGroup());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getJournal());
            sb.append("/");
            sb.append(getGroup());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrJournalGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrJournalGroup.class;
        }

        public T newInstance()
        {
            return (T) new TrJournalGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "journal":
                    return obj.getJournal();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "journal":
                    obj.setJournal((TrJournal) value);
                    return;
                case "group":
                    obj.setGroup((EppRealEduGroup) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "journal":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "journal":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "journal":
                    return TrJournal.class;
                case "group":
                    return EppRealEduGroup.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrJournalGroup> _dslPath = new Path<TrJournalGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrJournalGroup");
    }
            

    /**
     * @return Журнал. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup#getJournal()
     */
    public static TrJournal.Path<TrJournal> journal()
    {
        return _dslPath.journal();
    }

    /**
     * @return Учебная группа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup#getGroup()
     */
    public static EppRealEduGroup.Path<EppRealEduGroup> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends TrJournalGroup> extends EntityPath<E>
    {
        private TrJournal.Path<TrJournal> _journal;
        private EppRealEduGroup.Path<EppRealEduGroup> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Журнал. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup#getJournal()
     */
        public TrJournal.Path<TrJournal> journal()
        {
            if(_journal == null )
                _journal = new TrJournal.Path<TrJournal>(L_JOURNAL, this);
            return _journal;
        }

    /**
     * @return Учебная группа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup#getGroup()
     */
        public EppRealEduGroup.Path<EppRealEduGroup> group()
        {
            if(_group == null )
                _group = new EppRealEduGroup.Path<EppRealEduGroup>(L_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return TrJournalGroup.class;
        }

        public String getEntityName()
        {
            return "trJournalGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
