/* $Id:$ */
package ru.tandemservice.unitraining.brs.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;

import java.util.*;

/**
 * @author oleyba
 * @since 8/24/12
 */
public interface IBrsDao extends INeedPersistenceSupport
{
    String RATING_COLUMN_KEY = "rating";

    EppYearPart getEppYearPart(EducationYear educationYear, YearDistributionPart yearDistributionPart);

    /**
     * Получаение первой части текущего учебного года
     */
    EppYearPart getFirstEppYearPartForCurrentEduYear();

    /**
     * Сохраняет значение у пользовательского скрипта (обнуляет class-loader)
     * @param settings - подразделение БРС
     */
    void doSaveUserScript(TrOrgUnitSettings settings);

    /**
     * Получение настроек БРС оргюнита с самой свежей частью года, на которую есть настройки для этого оргюнита
     */
    TrOrgUnitSettings getOuSettingsForLastYearPart(Long orgUnitId);

    /**
     * Позволяет получить актуальные в контексте настройки БРС.
     * @param eppYearPart часть уч. года
     * @param tutorOu читающее подр. можно передать null, если в контексте его нет (например, работаем со студентом)
     * @param groupOu деканат. можно передать null, если в контексте его нет (например, работаем со всеми ведомостями преподавателя)
     * @return актуальные на год настройки БРС. null, если не найдены ни на каком уровне.
     */
    TrOrgUnitSettings getOrgUnitBrsSettings(EppYearPart eppYearPart, OrgUnit tutorOu, OrgUnit groupOu);
    
    /**
     * Валидация реализации при отправке на согласование - например, заполнены ли все нужные коэфф. БРС
     * Вызывает метод скрипта для осуществления валидации:
     * @see ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalScriptFunctions#validateOnJournalAcceptance(org.tandemframework.core.info.ErrorCollector, java.util.Map, IBrsDao.IBrsPreparedSettings)
     * Скрипт берется по правилам для журнала - сначала с читающего, потом глобальный, потом дефолтный
     * @param journal реализация
     */
    void validateOnAcceptance(TrJournal journal);

    /**
     * Валидация отметки при выставлении в журнал
     * Вызывает метод скрипта для осуществления валидации:
     * @see ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalGroupScriptFunctions#validateMark(org.tandemframework.core.info.ErrorCollector, ru.tandemservice.uni.entity.employee.Student, ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent, ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao.IJournalMarkData, IBrsDao.IBrsPreparedSettings)
     * Скрипт берется по правилам для журнала - сначала с читающего, потом глобальный, потом дефолтный
     * @param slot - МСРП
     * @param event - событие в журнале
     * @param mark - выставляемая отметка
     */
    void validateJournalMark(EppStudentWorkPlanElement slot, TrEduGroupEvent event, ITrJournalEventDao.IJournalMarkData mark);

    /**
     * Возвращает список коэфф. для настройки для данного владельца на данном уровне настройки
     * Для событий вызывает метод скрипта для фильтрации набора коэфф. из справочника:
     * @see ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalScriptFunctions#filterCoefficientDefListForEvent(ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent, java.util.List)
     * Для журнала и модуля фильтрует по @see ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalScriptFunctions#getCoefficientDefList()
     * @param owner владелец
     * @param ownerType уровень настройки коэфф.
     * @return список коэфф. для настройки
     */
    List<TrBrsCoefficientDef> getFilteredCoefficientDefList(IBrsSettingsOwner owner, TrBrsCoefficientOwnerType ownerType);

    /**
     * Вычисляет тек. рейтинг и тек. рейтинг по модулю для активных студентов в реализации
     * @param journal реализация дисциплины
     * @return тек. рейтинг
     */
    ICurrentRatingCalc getCalculatedRating(TrJournal journal);

    /**
     * Заполняет отметку о прохождении межсессионной аттестации в ведомости в соответствии с БРС.
     * @param bulletin ведомость межсессионной аттестации
     */
    void doFillAttestationMarkBasedOnCurrentRating(SessionAttestationBulletin bulletin);

    /**
     *  Рассчитывает суммарную оценку по БРС за мероприятие в сессии всем студентам УГС в реализации,
     *  принимая балл за непосредственную сдачу мероприятия за 0.
     *  Метод позволяет получить расчет мин. балла на оценки по шкале мероприятия в сессии,
     *  соответствующего данной реализации дисциплины,
     *  и определить допуск по БРС до мероприятия.
     *
     *  Расчеты сгруппированы по скриптам, в рамках которых проводились.
     *
     * @param group УГС в реализации дисциплины
     * @return расчет суммарной оценки, см. интерфейс
     */
    Collection<ISessionMarkCalc> getCalculatedSessionMark(TrJournalGroup group);

    /**
     * todo
     * @param document
     */
    void saveCurrentRating(SessionDocument document, boolean silent);

    /**
     * todo
     */
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    ISessionMarkDAO.MarkData calculateMarkData(Date perfomDate, SessionDocumentSlot documentSlot, Double points);

    ISessionBrsDao.ISessionRatingSettings getRatingSettings(ISessionBrsDao.ISessionRatingSettingsKey key);

    Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions(ISessionBrsDao.ISessionRatingSettingsKey key);

    Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions(TrJournal journal);

    List<String> getDisplayableAdditParams(TrJournal journal);

    IBrsPreparedSettings getPreparedBrsSettings(TrJournal journal);

    /**
     * Позволяет рассчитать для отображения доп. статистику по событиям реализации.
     * @param journal реализация
     * @return расчет статистики
     */
    IBrsDao.IJournalEventStatCalc getCalculatedStat(TrJournal journal);

    /**
     * Позволяет рассчитать для отображения доп. статистику по журналу УГС.
     * @param journalGroup журнал УГС
     * @return расчет статистики
     */
    IBrsDao.IJournalEventStatCalc getCalculatedStat(TrJournalGroup journalGroup);

    // --- Интерфейсы -------------------------------------------------------------------------------------------

    /** Интерфейс для передачи коэфф. для расчета рейтинга в скрипты расчета рейтинга */
    interface IBrsPreparedSettings
    {
        TrBrsCoefficientValue getSettings(String defCode);
        TrBrsCoefficientValue getModuleSettings(TrJournalModule module, String defCode);
        TrBrsCoefficientValue getEventSettings(TrJournalEvent event, String defCode);
    }

    /** Интерфейс для передачи вычисленного тек. рейтинга для набора студентов */
    interface ICurrentRatingCalc
    {
        /** @return Реализация, по которой вычислялся рейтинг. */
        TrJournal getJournal();

        /** @return Вычисляется ли рейтинг по модулю в используемой формуле. false - формула не предусматривает рейтинга по модулю, и отображать его не нужно. */
        boolean isModuleRatingCalculated();

        /**
         * @param student МСРП
         * @return тек. рейтинг студента. null, если для переданного МСРП рейтинг не вычислялся при подготовке данных
         */
        IStudentCurrentRatingData getCurrentRating(EppStudentWorkPlanElement student);

        /** @return html с текстом о формуле расчета рейтинга */
        String getDisplayableRatingFormula();
    }

    /** Расчет суммарной оценки и других параметров по БРС для мероприятия в сессии. */
    interface ISessionMarkCalc
    {
        /** @return Согласно настроек какого подразделения был рассчитан рейтинг. */
        TrOrgUnitSettings getScriptOwner();

        /** @return Шкала оценок. */
        EppGradeScale getGradeScale();

        /** @return Тип ФИК. */
        EppFControlActionGroup getCaGroup();

        /** @param student студент
         * @return Рассчитанная суммарная оценка за мероприятие. */
        IStudentSessionMarkData getSessionMark(EppStudentWorkPlanElement student);

        /** @return Формула расчета рейтинга с вычисленными и подставленными значениями коэфф. для печати. */
        RtfString getPrintableRatingFormula();
    }


    /** Интерфейс для передачи результатов расчета текущего рейтинга студента. */
    interface IStudentCurrentRatingData extends ISessionBrsDao.IStudentCurrentRatingData {
        ISessionBrsDao.IRatingValue getModuleRating(EppRegistryModule module);
    }

    /** Интерфейс для передачи результатов прохождения межсессионной аттестации, вычисленных по БРС. */
    interface IStudentAttestationData {

        /** @return Прошел ли студент аттестацию. */
        Boolean passed();

        /** @return Доп. данные для фиксации (могут отсутствовать или не быть предусмотрены, тогда null). */
        SessionAttestationSlotAdditionalData getAdditionalData();
    }

    /** Интерфейс для передачи результатов прохождения межсессионной аттестации с указание оценки из шкалы текущего контроля. */
    interface IStudentAttestationMarkData extends IStudentAttestationData {

        /** @return Оценки из шкалы текущего контроля. */
        SessionAttestationMarkCatalogItem mark();
    }

        /** Данные суммарной оценки для мероприятия в сессии, вычисленные по БРС. */
    interface IStudentSessionMarkData
    {
        /** @return Суммарная оценка по шкале. */
        SessionMarkGradeValueCatalogItem getSessionMark();

        /** @return Рейтинговое значение для суммарной оценки. */
        ISessionBrsDao.IRatingValue getRatingValue();

        /** @return Тек. рейтинг, вошедший в суммарную оценку. */
        ISessionBrsDao.IRatingValue getCurrentRatingValue();

        /** @return Доп. данные, вычисленные при вычислении суммарной оценки. Ключи определяются в скрипте, данные используются в проектном слое - печатных формах, етц. */
        ISessionBrsDao.IRatingValue getRatingAdditParam(String key);

        /** @return Допуск до сдачи по БРС. */
        Boolean isAllowed();

        /**
         * Вычисляет минимальный необходимый балл за сдачу мероприятия для получения студентом переданной суммарной оценки:
         * Каким должно быть набранное за непосредственную сдачу мероприятия значение,
         * чтобы студент получил суммарную оценку, равную mark, при имеющихся текущих данных об успеваемости.
         * @param mark Значение оценки из шкалы расчета.
         * @return минимальный балл за сдачу на получение оценки.
         */
        Double getRatingMissingForMark(SessionMarkGradeValueCatalogItem mark);
    }

    interface IAggregatedRatingValue extends ISessionBrsDao.IRatingValue
    {
        /**
         * @return false, если параметр не может (не должен) быть аггрегирован в данную колонку
         */
        boolean isApplicable();
    }

    /** Интерфейс для передачи вычисленной статистики по событиям журнала. */
    interface IJournalEventStatCalc
    {
        /** @return реализация, по которой вычислялась статистика */
        TrJournal getJournal();

        /** @return Группа, по которой вычислялась статистика. Может быть null, если статистика вычислялась по всей реализации. */
        TrJournalGroup getJournalGroup();

        /** @return перечень студентов, которые включены в расчет. */
        Set<EppStudentWorkPlanElement> getIncludedStudents();

        /**
         * @return перечень рассчитанных стат. параметров
         */
        List<ISessionBrsDao.IRatingAdditParamDef> getStatParamDefinitions();

        /**
         * @param event событие
         * @param statKey ключ доп. параметра
         * @return рассчитанное значение стат. параметра
         */
        ISessionBrsDao.IRatingValue getStatValue(TrJournalEvent event, String statKey);

        /**
         * @param statKey ключ доп. параметра
         * @param columnKey ключ колонки. для колонок доп. параметров - ключ доп. параметра, для колонки рейтинга - константа {@link IBrsDao#RATING_COLUMN_KEY}
         * @return рассчитанное значение стат. параметра по всему журналу для отображения в соотв. колонке
         */
        IAggregatedRatingValue getStatValueAggregated(String statKey, String columnKey);
    }
}
