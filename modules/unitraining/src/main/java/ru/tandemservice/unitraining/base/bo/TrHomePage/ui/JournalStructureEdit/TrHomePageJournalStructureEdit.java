/* $Id:$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalStructureEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import ru.tandemservice.unitraining.base.bo.TrHomePage.TrHomePageManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

/**
 * @author oleyba
 * @since 8/29/12
 */
@Configuration
public class TrHomePageJournalStructureEdit extends BusinessComponentManager
{
    public static final String GROUP_LIST_COL_GROUP = "group";
    public static final String GROUP_LIST_COL_AGREEMENT = "agreement";
    public static final String GROUP_LIST_COL_GROUP_TYPE = "groupType";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(TrHomePageManager.instance().trHomePageJournalDSConfig())
                .addDataSource(searchListDS(TrJournalManager.TR_GROUP_WITH_PPS_DS, groupListDSColumnExtPoint(),
                                            TrJournalManager.instance().trJournalGroupWithPpsDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint groupListDSColumnExtPoint()
    {
        return columnListExtPointBuilder(TrJournalManager.TR_GROUP_WITH_PPS_DS)
                .addColumn(textColumn(GROUP_LIST_COL_GROUP, TrJournalGroup.group().title().s()))
                .addColumn(textColumn(GROUP_LIST_COL_AGREEMENT, TrJournalGroup.group().level().displayableTitle().s()))
                .addColumn(textColumn(GROUP_LIST_COL_GROUP_TYPE, TrJournalGroup.group().type().title().s()))
                .addColumn(textColumn(TrJournalManager.TR_GROUP_DS_PPS_LIST_PROPERTY, TrJournalManager.TR_GROUP_DS_PPS_LIST_PROPERTY))
                .create();
    }
}
