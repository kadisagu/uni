package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalGen;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;

/**
 * @author vdanilov
 */
@Configuration
public class TrOrgUnitJournalGen extends BusinessComponentManager {

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(TrOrgUnitManager.instance().trOrgUnitYearPartDSConfig())
        .create();
    }

}
