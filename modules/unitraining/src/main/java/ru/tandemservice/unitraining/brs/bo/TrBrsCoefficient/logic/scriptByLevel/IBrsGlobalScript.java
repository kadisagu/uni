/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByLevel;

import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsAttestationScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalGroupScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsSessionScriptFunctions;

/**
 * Интерфейс, которому должен соответствовать глобальный скрипт расчета рейтинга
 * т.е. заданный на уровне настройки «Использование и коэффициенты БРС»
 * @author oleyba
 * @since 8/24/12
 */
public interface IBrsGlobalScript extends
    IBrsJournalGroupScriptFunctions,
    IBrsJournalScriptFunctions,
    IBrsAttestationScriptFunctions,
    IBrsSessionScriptFunctions
{
}
