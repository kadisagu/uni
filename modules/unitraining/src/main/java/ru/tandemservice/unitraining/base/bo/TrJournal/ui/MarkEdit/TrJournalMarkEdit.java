/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;

/**
 * @author oleyba
 * @since 9/8/11
 */
@Configuration
public class TrJournalMarkEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(TrJournalManager.instance().trJournalEduGroupDSConfig())
        .create();
    }

}
