/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.EditBase;

import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.IBrsCoefficientRowWrapper;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;

import java.util.*;

/**
 * @author oleyba
 * @since 8/20/12
 */
@Input({
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_HOLDER, binding = "ownerHolder", required = true),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType", required = true)
})
public class TrBrsCoefficientEditBaseUI extends UIPresenter
{
    private EntityHolder<IBrsSettingsOwner> ownerHolder;
    private TrBrsCoefficientOwnerType ownerType;

    private List<IBrsCoefficientRowWrapper> rowList;
    private RowWrapper currentRow;

    @Override
    public void onComponentRefresh()
    {
        List<IBrsCoefficientRowWrapper> wrappers = new ArrayList<>();
        Map<TrBrsCoefficientDef, TrBrsCoefficientValue> values = new HashMap<TrBrsCoefficientDef, TrBrsCoefficientValue>();
        for (TrBrsCoefficientValue value : DataAccessServices.dao().getList(TrBrsCoefficientValue.class, TrBrsCoefficientValue.owner().s(), getOwnerHolder().getValue()))
            values.put(value.getDefinition(), value);
        for (TrBrsCoefficientDef def : TrBrsCoefficientManager.instance().brsDao().getFilteredCoefficientDefList(getOwner(), getOwnerType()))
            wrappers.add(new RowWrapper(def, values.get(def)));
        setRowList(wrappers);
    }

    public void save()
    {
        HashMap<TrBrsCoefficientDef, Object> values = new HashMap<TrBrsCoefficientDef, Object>();
        for (IBrsCoefficientRowWrapper wrapper : getRowList())
            values.put(wrapper.getDef(), wrapper.getValue());
        TrBrsCoefficientManager.instance().coefDao().doSaveCoefficients(getOwner(), values);
    }

    public EntityHolder<IBrsSettingsOwner> getOwnerHolder()
    {
        return ownerHolder;
    }

    public IBrsSettingsOwner getOwner()
    {
        return getOwnerHolder().getValue();
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(TrBrsCoefficientOwnerType ownerType)
    {
        this.ownerType = ownerType;
    }

    public List<IBrsCoefficientRowWrapper> getRowList()
    {
        return rowList;
    }

    public void setRowList(List<IBrsCoefficientRowWrapper> rowList)
    {
        this.rowList = rowList;
    }

    public RowWrapper getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(RowWrapper currentRow)
    {
        this.currentRow = currentRow;
    }

    public void setOwnerHolder(EntityHolder<IBrsSettingsOwner> ownerHolder)
    {
        this.ownerHolder = ownerHolder;
    }
    
    public List<Validator> getValidators()
    {
        if (getCurrentRow().getDef().isRequired())
            return Collections.singletonList((Validator) new Required());
        return Collections.emptyList();
    }

    public static class RowWrapper extends IdentifiableWrapper<TrBrsCoefficientDef> implements IBrsCoefficientRowWrapper
    {
        private TrBrsCoefficientDef def;
        private Object value;

        public RowWrapper(TrBrsCoefficientDef def, TrBrsCoefficientValue value)
        {
            super(def.getId(), def.getShortTitle());
            this.def = def;
            this.value = value == null ? null : value.getValue();
        }

        public Object getValue()
        {
            return value;
        }

        public void setValue(Object value)
        {
            this.value = value;
        }

        public TrBrsCoefficientDef getDef()
        {
            return def;
        }
    }
}
