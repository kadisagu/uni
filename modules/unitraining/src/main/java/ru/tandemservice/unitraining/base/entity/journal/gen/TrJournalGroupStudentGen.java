package ru.tandemservice.unitraining.base.entity.journal.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись студента в журнале
 *
 * Определяет, в какой группе находился или находится студент в журнале.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrJournalGroupStudentGen extends EntityBase
 implements INaturalIdentifiable<TrJournalGroupStudentGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent";
    public static final String ENTITY_NAME = "trJournalGroupStudent";
    public static final int VERSION_HASH = 1490248621;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP = "group";
    public static final String L_STUDENT_WPE = "studentWpe";
    public static final String P_TRANSFER_DATE = "transferDate";

    private TrJournalGroup _group;     // Журнал (УГС в реализации)
    private EppStudentWorkPlanElement _studentWpe;     // Студент (МСРП)
    private Date _transferDate;     // Дата утраты актуальности записи в журнале (в связи с переводом)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Журнал (УГС в реализации). Свойство не может быть null.
     */
    @NotNull
    public TrJournalGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Журнал (УГС в реализации). Свойство не может быть null.
     */
    public void setGroup(TrJournalGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Студент (МСРП). Свойство не может быть null.
     */
    @NotNull
    public EppStudentWorkPlanElement getStudentWpe()
    {
        return _studentWpe;
    }

    /**
     * @param studentWpe Студент (МСРП). Свойство не может быть null.
     */
    public void setStudentWpe(EppStudentWorkPlanElement studentWpe)
    {
        dirty(_studentWpe, studentWpe);
        _studentWpe = studentWpe;
    }

    /**
     * См. ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean
     *
     * @return Дата утраты актуальности записи в журнале (в связи с переводом).
     */
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата утраты актуальности записи в журнале (в связи с переводом).
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TrJournalGroupStudentGen)
        {
            if (withNaturalIdProperties)
            {
                setGroup(((TrJournalGroupStudent)another).getGroup());
                setStudentWpe(((TrJournalGroupStudent)another).getStudentWpe());
            }
            setTransferDate(((TrJournalGroupStudent)another).getTransferDate());
        }
    }

    public INaturalId<TrJournalGroupStudentGen> getNaturalId()
    {
        return new NaturalId(getGroup(), getStudentWpe());
    }

    public static class NaturalId extends NaturalIdBase<TrJournalGroupStudentGen>
    {
        private static final String PROXY_NAME = "TrJournalGroupStudentNaturalProxy";

        private Long _group;
        private Long _studentWpe;

        public NaturalId()
        {}

        public NaturalId(TrJournalGroup group, EppStudentWorkPlanElement studentWpe)
        {
            _group = ((IEntity) group).getId();
            _studentWpe = ((IEntity) studentWpe).getId();
        }

        public Long getGroup()
        {
            return _group;
        }

        public void setGroup(Long group)
        {
            _group = group;
        }

        public Long getStudentWpe()
        {
            return _studentWpe;
        }

        public void setStudentWpe(Long studentWpe)
        {
            _studentWpe = studentWpe;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TrJournalGroupStudentGen.NaturalId) ) return false;

            TrJournalGroupStudentGen.NaturalId that = (NaturalId) o;

            if( !equals(getGroup(), that.getGroup()) ) return false;
            if( !equals(getStudentWpe(), that.getStudentWpe()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getGroup());
            result = hashCode(result, getStudentWpe());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getGroup());
            sb.append("/");
            sb.append(getStudentWpe());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrJournalGroupStudentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrJournalGroupStudent.class;
        }

        public T newInstance()
        {
            return (T) new TrJournalGroupStudent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "group":
                    return obj.getGroup();
                case "studentWpe":
                    return obj.getStudentWpe();
                case "transferDate":
                    return obj.getTransferDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "group":
                    obj.setGroup((TrJournalGroup) value);
                    return;
                case "studentWpe":
                    obj.setStudentWpe((EppStudentWorkPlanElement) value);
                    return;
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "group":
                        return true;
                case "studentWpe":
                        return true;
                case "transferDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "group":
                    return true;
                case "studentWpe":
                    return true;
                case "transferDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "group":
                    return TrJournalGroup.class;
                case "studentWpe":
                    return EppStudentWorkPlanElement.class;
                case "transferDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrJournalGroupStudent> _dslPath = new Path<TrJournalGroupStudent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrJournalGroupStudent");
    }
            

    /**
     * @return Журнал (УГС в реализации). Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent#getGroup()
     */
    public static TrJournalGroup.Path<TrJournalGroup> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Студент (МСРП). Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent#getStudentWpe()
     */
    public static EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> studentWpe()
    {
        return _dslPath.studentWpe();
    }

    /**
     * См. ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean
     *
     * @return Дата утраты актуальности записи в журнале (в связи с переводом).
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    public static class Path<E extends TrJournalGroupStudent> extends EntityPath<E>
    {
        private TrJournalGroup.Path<TrJournalGroup> _group;
        private EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> _studentWpe;
        private PropertyPath<Date> _transferDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Журнал (УГС в реализации). Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent#getGroup()
     */
        public TrJournalGroup.Path<TrJournalGroup> group()
        {
            if(_group == null )
                _group = new TrJournalGroup.Path<TrJournalGroup>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Студент (МСРП). Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent#getStudentWpe()
     */
        public EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> studentWpe()
        {
            if(_studentWpe == null )
                _studentWpe = new EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement>(L_STUDENT_WPE, this);
            return _studentWpe;
        }

    /**
     * См. ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean
     *
     * @return Дата утраты актуальности записи в журнале (в связи с переводом).
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(TrJournalGroupStudentGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

        public Class getEntityClass()
        {
            return TrJournalGroupStudent.class;
        }

        public String getEntityName()
        {
            return "trJournalGroupStudent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
