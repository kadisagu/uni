// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unitraining.base.bo.TrJournal.ui.EduGroupLink;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

import java.util.List;

/**
 * @author oleyba
 * @since 8/14/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journal.id", required = true)
})
public class TrJournalEduGroupLinkUI extends UIPresenter
{
    private TrJournal journal = new TrJournal();
    private List<EppRealEduGroup> groupList;

    @Override
    public void onComponentRefresh() {
        setJournal(IUniBaseDao.instance.get().get(TrJournal.class, getJournal().getId()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (TrJournalManager.TR_JOURNAL_EDU_GROUP_UNLINKED_DS.equals(dataSource.getName())) {
            dataSource.put(TrJournalManager.TR_JOURNAL_EDU_GROUP_DS_DISC_KEY, getJournal().getRegistryElementPart());
            dataSource.put(TrJournalManager.TR_JOURNAL_EDU_GROUP_DS_YP_KEY, getJournal().getYearPart());
        }
    }

    public void onClickApply() {
        TrJournalManager.instance().dao().doLinkEduGroups(getJournal(), getGroupList());
        deactivate();
    }

    public void onNeedResize()
    {
        // do nothing, empty listener just to trigger dialog box resize
    }

    // accessors

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }

    public List<EppRealEduGroup> getGroupList()
    {
        return groupList;
    }

    public void setGroupList(List<EppRealEduGroup> groupList)
    {
        this.groupList = groupList;
    }
}
