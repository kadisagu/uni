/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrEduGroup.ui.JournalLink;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

import java.util.Arrays;

/**
 * @author oleyba
 * @since 9/15/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "groupId", required = true)
})
public class TrEduGroupJournalLinkUI extends UIPresenter
{
    private Long _groupId;
    private EppRealEduGroup group;
    private TrJournal journal;

    @Override
    public void onComponentRefresh()
    {
        setGroup(IUniBaseDao.instance.get().get(EppRealEduGroup.class, getGroupId()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(TrEduGroupJournalLink.TR_EDU_GROUP_JOURNAL_LINK_DISC_KEY, getGroup().getActivityPart());
        dataSource.put(TrEduGroupJournalLink.TR_EDU_GROUP_JOURNAL_LINK_YEAR_PART_KEY, getGroup().getSummary().getYearPart());
    }

    public void onClickApply() {
        TrJournalManager.instance().dao().doLinkEduGroups(getJournal(), Arrays.asList(getGroup()));
        deactivate();
    }

    public Long getGroupId()
    {
        return _groupId;
    }

    public void setGroupId(Long groupId)
    {
        _groupId = groupId;
    }

    public EppRealEduGroup getGroup()
    {
        return group;
    }

    public void setGroup(EppRealEduGroup group)
    {
        this.group = group;
    }

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }
}
