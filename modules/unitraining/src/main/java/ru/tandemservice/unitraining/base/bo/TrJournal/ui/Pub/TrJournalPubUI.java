package ru.tandemservice.unitraining.base.bo.TrJournal.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.support.crud.AbstractCRUDEntityUI;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.Copy.TrJournalCopy;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.Edit.TrJournalEdit;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.Edit.TrJournalEditUI;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkEdit.TrJournalMarkEdit;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.ScheduleData.TrJournalScheduleDataUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;


/**
 * @author Codegen
 */
@State({
    @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "entity.id"),
    @Bind(key = "selectedTabId", binding = "selectedTabId")
})
@Output({
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_ID, binding = "entity.id"),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType.id")
})
public class TrJournalPubUI extends AbstractCRUDEntityUI<TrJournal>
{
    private String selectedTabId;
    private Set<PpsEntry> tutors;

    private TrBrsCoefficientOwnerType ownerType = new TrBrsCoefficientOwnerType();

    private EppFControlActionGroup fcaGroup;
    
    private boolean hasMarks;

    public TrJournalPubUI()
    {
        prepareEntity(TrJournal.class);
    }

    // actions

    @Override
    public void onComponentRefresh()
    {
        _entity = DataAccessServices.dao().getNotNull(getEntityId());

        tutors = new LinkedHashSet<>(TrJournalManager.instance().dao().getTrJournalEppPps(getEntityId()));

        setOwnerType(DataAccessServices.dao().get(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerType.code(), TrBrsCoefficientOwnerTypeCodes.JOURNAL));

        try {
            setFcaGroup(TrBrsCoefficientManager.instance().coefDao().getFcaGroup(getTrJournal()));
        }
        catch (ApplicationException e) { 
            // ничего не делаем, считаем, что мероприятие просто не заполнено/отсутствует в реестре
        }
        
        setHasMarks(ISharedBaseDao.instance.get().existsEntity(
                            TrJournalEvent.class,
                            TrJournalEvent.journalModule().journal().s(), getTrJournal(),
                            TrJournalEvent.P_HAS_MARKS, Boolean.TRUE)
        );

        RenderComponent.deactivateAll();
    }

    public void onChangeTab()
    {
        _uiSupport.setRefreshScheduled(true);
        RenderComponent.deactivateAll();
    }

    public void onClickMark()
    {
        this._uiActivation.asDesktopRoot(TrJournalMarkEdit.class)
        .parameter(IUIPresenter.PUBLISHER_ID, getEntity().getId())
        .activate();
    }

    @Override
    public void deleteEntity()
    {
        IUniBaseDao.instance.get().delete(getEntity());
        deactivate();
    }

    @Override
    public void createOrUpdateEntity()
    {
        throw new UnsupportedOperationException();
    }

    public void onClickModuleGen()
    {
        TrJournalDaemonBean.DAEMON.wakeUpAndWaitDaemon(Integer.MAX_VALUE);
        _uiSupport.setRefreshScheduled(true);
    }

    @Override
    public void onEditEntity()
    {
        _uiActivation.asRegionDialog(TrJournalEdit.class)
        .parameter(IUIPresenter.PUBLISHER_ID, getTrJournal().getId())
        .parameter(TrJournalEditUI.BIND_ALLOW_EDIT_RESPONSIBLE_PPS, Boolean.TRUE)
        .   activate();
    }

    public void onClickCopy()
    {
        getActivationBuilder().asRegionDialog(TrJournalCopy.class)
                .parameter(UIPresenter.PUBLISHER_ID, _entity.getId())
                .activate();
    }


    // presenter

    public boolean isReadonly()
    {
        return getEntity().getState().isReadOnlyState();
    }

    public boolean isScheduleVisible()
    {
        return getEntity().getState().getCode().equals(EppState.STATE_ACCEPTED);
    }

    public boolean isMarksVisible()
    {
        return getEntity().getState().getCode().equals(EppState.STATE_ACCEPTED) || getEntity().getState().getCode().equals(EppState.STATE_ARCHIVED);
    }

    @Override
    protected boolean isOneComponentForNewAndEdit()
    {
        return false;
    }

    public boolean isMarkState()
    {
        return !getEntity().getRegistryElementPart().getRegistryElement().getState().getCode().equals(EppState.STATE_ACCEPTED);
    }

    public Map<String, Object> getScheduleDataParameters()
    {
        return new ParametersMap()
        .add(TrJournalScheduleDataUI.CAN_CHANGE_LOAD_TYPE_BIND, CoreServices.securityService().check(getEntity(), ContextLocal.getUserContext().getPrincipalContext(), "editLoadTypeTrJournal"))
        .add(TrJournalScheduleDataUI.VIEW_MODE_BIND, !CoreServices.securityService().check(getEntity(), ContextLocal.getUserContext().getPrincipalContext(), "editScheduleTrJournal"));
    }
    
    public String getDeleteAlert()
    {
        return isHasMarks() ? "Удалить реализацию? В реализации уже выставлены оценки. При ее удалении они будут удалены без возможности восстановления." : "Удалить реализацию?";
    }

    // getters and setters

    public String getSelectedTabId()
    {
        return selectedTabId;
    }

    public void setSelectedTabId(String selectedTabId)
    {
        this.selectedTabId = selectedTabId;
    }

    public Set<PpsEntry> getTutors()
    {
        return tutors;
    }

    public TrJournal getTrJournal()
    {
        return getEntity();
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(TrBrsCoefficientOwnerType ownerType)
    {
        this.ownerType = ownerType;
    }

    public EppFControlActionGroup getFcaGroup()
    {
        return fcaGroup;
    }

    public void setFcaGroup(EppFControlActionGroup fcaGroup)
    {
        this.fcaGroup = fcaGroup;
    }

    public boolean isHasMarks() 
    {
        return hasMarks;
    }

    public void setHasMarks(boolean hasMarks) 
    {
        this.hasMarks = hasMarks;
    }
}
