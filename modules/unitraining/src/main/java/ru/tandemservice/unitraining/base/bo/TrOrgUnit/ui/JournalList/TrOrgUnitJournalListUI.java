package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalList;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.Edit.TrJournalEdit;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.logic.TrOrgUnitJournalDSHandler;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalAdd.TrOrgUnitJournalAdd;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalAdd.TrOrgUnitJournalAddUI;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalGen.TrOrgUnitJournalGen;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalGen.TrOrgUnitJournalGenUI;

import java.util.Map;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
public class TrOrgUnitJournalListUI extends UIPresenter {

    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    public OrgUnitHolder getOrgUnitHolder() { return this.orgUnitHolder; }
    public Long getOrgUnitId() { return this.getOrgUnitHolder().getId(); }
    public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }
    public CommonPostfixPermissionModelBase getSecModel() { return this.getOrgUnitHolder().getSecModel(); }

    @Override
    public void onComponentRefresh() {
        this.getOrgUnitHolder().refresh(OrgUnit.class);
    }

    @Override
    public void onBeforeDataSourceFetch(final IUIDataSource dataSource)
    {
        if (TrOrgUnitJournalList.TR_ORGUNIT_JOURNAL_DS.equals(dataSource.getName()))
        {
            // добавляем текущее подразделение и часть учебного года
            dataSource.put(TrOrgUnitJournalDSHandler.PARAM_ORG_UNIT, this.getOrgUnit());
            dataSource.put(TrOrgUnitJournalDSHandler.PARAM_YEAR_PART, this.getYearPart());

            // добавляем значения фильтров с текущей страницы
            final Map<String, Object> settingMap = this._uiSettings.getAsMap(
                    TrOrgUnitJournalDSHandler.PARAM_NUMBER,
                    TrOrgUnitJournalDSHandler.PARAM_PERIOD_DATE_FROM,
                    TrOrgUnitJournalDSHandler.PARAM_PERIOD_DATE_TO,
                    TrOrgUnitJournalDSHandler.PARAM_ACTIVITY_ELEMENT,
                    TrOrgUnitJournalDSHandler.PARAM_RESPONSIBLE,
                    TrOrgUnitJournalDSHandler.PARAM_TUTOR,
                    TrOrgUnitJournalDSHandler.PARAM_STATE
            );
            dataSource.putAll(settingMap);
        }

        final boolean filterPps = Boolean.TRUE.equals(_uiSettings.get("filterPps"));

        if (filterPps && TrOrgUnitManager.TR_ORGUNIT_PPS_DS.equals(dataSource.getName()))
            dataSource.put(TrOrgUnitManager.TR_ORGUNIT_ORGUNIT_KEY, getOrgUnit());

        if (TrOrgUnitManager.TR_ORGUNIT_ACTIVITY_ELEMENT_PART_DS.equals(dataSource.getName()))
            dataSource.put(TrOrgUnitManager.TR_ORGUNIT_ORGUNIT_KEY, getOrgUnit());
    }

    public void onClickGenTrJournals()
    {
        this._uiActivation.asRegionDialog(TrOrgUnitJournalGen.class)
        .parameter(IUIPresenter.PUBLISHER_ID, this.getOrgUnitId())
        .parameter(TrOrgUnitJournalGenUI.YAERPART_ID, this.getYearPartId())
        .activate();
    }

    public void onClickAddTrJournal()
    {
        this._uiActivation.asRegionDialog(TrOrgUnitJournalAdd.class)
        .parameter(IUIPresenter.PUBLISHER_ID, this.getOrgUnitId())
        .parameter(TrOrgUnitJournalAddUI.YEARPART_ID, this.getYearPartId())
        .activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(TrJournalEdit.class)
        .parameter(IUIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
        .activate();
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
        _uiSupport.setRefreshScheduled(true);
    }

    protected Long getYearPartId() {
        final EppYearPart part = this.getYearPart();
        return (null == part ? null : part.getId());
    }

    public EppYearPart getYearPart() {
        return (EppYearPart)this._uiSettings.get(TrOrgUnitJournalDSHandler.PARAM_YEAR_PART);
    }
}
