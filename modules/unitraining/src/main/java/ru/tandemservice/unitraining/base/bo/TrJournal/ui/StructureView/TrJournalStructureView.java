/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.StructureView;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 8/10/11
 */
@Configuration
public class TrJournalStructureView extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
                .create();
    }

}
