/* $Id: TrJournalEdit.java 20463 2011-10-25 05:43:26Z vdanilov $ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.AddEvent;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartModuleWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.unitraining.base.entity.catalog.TrAddonType;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/5/11
 */
@Configuration
public class TrJournalAddEvent extends BusinessComponentManager
{
    public static final String BIND_JOURNAL = "trJournal";
    public static final String BIND_MODULE = "trJournalModule";
    // moduleDS actionTypeDS addonTypeDS loadTypeDS

    public static final Map<String, String> GROUP_TYPE_LOAD_ACTION_CODES = Maps.newHashMap();

    static
    {
        GROUP_TYPE_LOAD_ACTION_CODES.put(EppGroupTypeALTCodes.TYPE_LECTURES, EppALoadType.FULL_CODE_TOTAL_LECTURES);
        GROUP_TYPE_LOAD_ACTION_CODES.put(EppGroupTypeALTCodes.TYPE_PRACTICE, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
        GROUP_TYPE_LOAD_ACTION_CODES.put(EppGroupTypeALTCodes.TYPE_LABS, EppALoadType.FULL_CODE_TOTAL_LABS);
        GROUP_TYPE_LOAD_ACTION_CODES.put(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM);
        GROUP_TYPE_LOAD_ACTION_CODES.put(EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF);
        GROUP_TYPE_LOAD_ACTION_CODES.put(EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF_DIFF, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF);
        GROUP_TYPE_LOAD_ACTION_CODES.put(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK, EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK);
        GROUP_TYPE_LOAD_ACTION_CODES.put(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT, EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT);
        GROUP_TYPE_LOAD_ACTION_CODES.put(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM_ACCUM);
        GROUP_TYPE_LOAD_ACTION_CODES.put(EppGroupTypeFCACodes.CONTROL_ACTION_CONTROL_WORK, EppControlActionType.FULL_CODE_CONTROL_ACTION_CONTROL_WORK);
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(selectDS("moduleDS", moduleDSHandler()))
        .addDataSource(selectDS("actionTypeDS", actionTypeDSHandler()))
        .addDataSource(selectDS("addonTypeDS", addonTypeDSHandler()))
        .addDataSource(selectDS("groupTypeDS", groupTypeDSHandler()))
        .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> moduleDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), TrJournalModule.class)
        .where(TrJournalModule.journal(), BIND_JOURNAL)
        .order(TrJournalModule.moduleBeginDate())
        .filter(TrJournalModule.module().module().title())
        ;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> actionTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppIControlActionType.class)
        .where(EppIControlActionType.disabled(), Boolean.FALSE)
        .order(EppIControlActionType.code())
        .filter(EppIControlActionType.title())
        ;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> addonTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), TrAddonType.class)
        .order(TrAddonType.title())
        .filter(TrAddonType.title())
        ;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> groupTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppGroupType.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                TrJournal journal = context.get(BIND_JOURNAL);
                TrJournalModule journalModule = context.get(BIND_MODULE);
                if (journal == null || journalModule == null) {
                    dql.where(isNull(alias + ".id"));
                    return;
                }

                Long dicsiplineId = journal.getRegistryElementPart().getId();
                IEppRegElPartWrapper disciplineWrapper = IEppRegistryDAO.instance.get().getRegistryElementPartDataMap(Collections.singleton(dicsiplineId)).get(dicsiplineId);
                Collection<IEppRegElPartModuleWrapper> disciplineModules = disciplineWrapper.getModuleMap().values();
                List<EppGroupType> groupTypes = IUniBaseDao.instance.get().getList(EppGroupType.class);

                Set<EppFControlActionType> actionTypeList = IEppRegistryDAO.instance.get().getRegistryElementPartControlActions(journal.getRegistryElementPart()).keySet();
                List<String> actionCodes = Lists.newArrayList();
                for (EppFControlActionType type : actionTypeList)
                {
                    actionCodes.add(type.getFullCode());
                }

                Set<Long> includedGroupTypes = new HashSet<>();
                for (EppGroupType groupType : groupTypes)
                {
                    String fullCode = GROUP_TYPE_LOAD_ACTION_CODES.get(groupType.getCode());
                    for (IEppRegElPartModuleWrapper module : disciplineModules)
                        if (module.getItem().equals(journalModule.getModule()))
                        {
                            if (groupType instanceof EppGroupTypeALT && module.getLoadAsDouble(fullCode) > 0)
                                includedGroupTypes.add(groupType.getId());
                        }
                    if (groupType instanceof EppGroupTypeFCA && actionCodes.contains(fullCode))
                        includedGroupTypes.add(groupType.getId());
                }
                dql.where(in(property(alias + ".id"), includedGroupTypes));
            }
        }
                .order(EppGroupType.title())
                .filter(EppGroupType.title());
    }
}