/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.StructureView;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAddon;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 8/10/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journal.id", required = true),
    @Bind(key = TrJournalStructureViewUI.GROUP_TYPE_BIND, binding = "groupTypeId")
})
public class TrJournalStructureViewUI extends UIPresenter
{
    public static final String GROUP_TYPE_BIND = "groupTypeId";

    private ITrJournalStructureOwner owner;

    private TrJournal journal = new TrJournal();
    private Long _groupTypeId;

    private StaticListDataSource<Row> dataSource;

    private ITrJournalStructureOwner.IButtonInfo currentButton;

    @Override
    public void onComponentRefresh()
    {
        if (!(_uiSupport.getParentUI() instanceof ITrJournalStructureOwner))
            throw new IllegalStateException();
        owner = (ITrJournalStructureOwner) _uiSupport.getParentUI();

        IUniBaseDao iCoreDao = IUniBaseDao.instance.get();

        setJournal(iCoreDao.get(TrJournal.class, getJournal().getId()));

        dataSource = new StaticListDataSource<Row>();

        // кастомизация списка
        dataSource.setRowCustomizer(new SimpleRowCustomizer<Row>() {
            @Override  public String getRowStyle(final Row entity) {
                final IEntity w = entity.getEntity();
                if (w instanceof TrJournal) { return "font-weight:bold;background-color: "+ UniDefines.COLOR_BLUE+";"; }
                if (w instanceof TrJournalModule) { return "background-color: "+UniDefines.COLOR_BLUE+";"; }
                return "";
            }
        });

        // базовые колонки
        dataSource.addColumn(UniEppUtils.getStateColumn("item.module"));
        dataSource.addColumn(new SimpleColumn("Название", "title").setClickable(false).setOrderable(false).setTreeColumn(true));

        // дейсвтия со строками
        if (owner.isEditable())
            dataSource.addColumn(new BlockColumn("actions", "", "rowActionsBlock").setWidth(1));

        Map<TrJournalModule, List<TrJournalEvent>> journalMap = new LinkedHashMap<TrJournalModule, List<TrJournalEvent>>();

        for (TrJournalModule module : iCoreDao.getList(TrJournalModule.class, TrJournalModule.journal(), getJournal(), TrJournalModule.moduleBeginDate().s(), TrJournalModule.module().number().s())) {
            journalMap.put(module, new ArrayList<TrJournalEvent>());
        }

        for (TrJournalEvent event : iCoreDao.getList(TrJournalEvent.class, TrJournalEvent.journalModule().journal(), getJournal(), TrJournalEvent.number().s())) {
            if (event instanceof TrEventAddon) { continue; /* только основные события */ }
            if (null == getGroupTypeId() || event.getType().getId().equals(getGroupTypeId())) {
                journalMap.get(event.getJournalModule()).add(event);
            }
        }

        final List<Row> rowList = new ArrayList<Row>();
        for (Map.Entry<TrJournalModule, List<TrJournalEvent>> entry : journalMap.entrySet())
        {
            final Row moduleRow = new Row(entry.getKey(), null);
            rowList.add(moduleRow);

            for (final TrJournalEvent event: entry.getValue())
            {
                rowList.add(new Row(event, moduleRow));
            }
        }
        dataSource.setupRows(rowList);
    }

    public void onClickButton()
    {
        Map<String, Object> params = getListenerParameter();
        ((ITrJournalStructureOwner.IButtonInfo) params.get("button")).onClick((Long) params.get("entityId"));
    }
    
    public String getCurrentButtonAlert()
    {
        return getCurrentButton().isHasAlert() ? getCurrentButton().getAlert(getCurrentEntityId()) : null;
    }

    public String getCurrentBlockName() {
        if (!owner.isEditable())
            return "emptyBlock";
        final IEntity currentEntity = getCurrentEntity();
        if (currentEntity instanceof TrJournalModule)
            return "rowActionsBlock4module";
        if (currentEntity instanceof TrJournalEvent)
            return "rowActionsBlock4event";
        return "emptyBlock";
    }

    public static class Row extends ViewWrapper<IEntity>
    {
        private final Row parent;
        @Override public Row getHierarhyParent() { return this.parent; }

        public Row(final IEntity i, final Row parent) {
            super(i);
            this.parent = parent;
        }
    }

    public String getPermissionKey()
    {
        return "editTrJournal";
    }

    public String getDisabledIcon()
    {
        return getCurrentButton().getIcon().replace(".png", "_disabled.png");
    }

    public boolean isCurrentButtonDisabled()
    {
        return getCurrentButton().isDisabled(getCurrentEntityId());
    }

    // accessors

    public ITrJournalStructureOwner getOwner()
    {
        return owner;
    }

    public ITrJournalStructureOwner.IButtonInfo getCurrentButton()
    {
        return currentButton;
    }

    public void setCurrentButton(ITrJournalStructureOwner.IButtonInfo currentButton)
    {
        this.currentButton = currentButton;
    }

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }

    public StaticListDataSource<Row> getDataSource()
    {
        return dataSource;
    }

    public IEntity getCurrentEntity()
    {
        return getDataSource().getCurrentEntity().getEntity();
    }

    public Long getCurrentEntityId()
    {
        return getDataSource().getCurrentEntity().getId();
    }

    public Long getGroupTypeId()
    {
        return _groupTypeId;
    }

    public void setGroupTypeId(Long groupTypeId)
    {
        _groupTypeId = groupTypeId;
    }
}
