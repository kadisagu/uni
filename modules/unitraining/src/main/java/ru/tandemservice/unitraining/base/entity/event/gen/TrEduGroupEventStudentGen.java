package ru.tandemservice.unitraining.base.entity.event.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.sec.entity.gen.IPrincipalContextGen;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unitraining.base.entity.catalog.TrAbsenceReason;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись студента по событию в журнале
 *
 * Создаются автоматически (на основе данных УГС) при определенных условиях
 * (за k дней до дедлайна по событию, например)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrEduGroupEventStudentGen extends EntityBase
 implements INaturalIdentifiable<TrEduGroupEventStudentGen>, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent";
    public static final String ENTITY_NAME = "trEduGroupEventStudent";
    public static final int VERSION_HASH = 236840693;
    private static IEntityMeta ENTITY_META;

    public static final String L_EVENT = "event";
    public static final String L_STUDENT_WPE = "studentWpe";
    public static final String P_ABSENT = "absent";
    public static final String L_ABSENCE_REASON = "absenceReason";
    public static final String P_GRADE_AS_LONG = "gradeAsLong";
    public static final String P_COMMENT = "comment";
    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String L_MARKED_BY = "markedBy";
    public static final String P_GRADE = "grade";
    public static final String P_PRESENT = "present";

    private TrEduGroupEvent _event;     // Событие УГС
    private EppStudentWorkPlanElement _studentWpe;     // Студент (МСРП)
    private Boolean _absent;     // Отсутствовал
    private TrAbsenceReason _absenceReason;     // Причина пропуска занятия
    private Long _gradeAsLong;     // Балл
    private String _comment;     // Комментарий
    private Date _transferDate;     // Дата утраты актуальности записи в журнале (в связи с переводом)
    private IPrincipalContext _markedBy;     // Автор отметки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Событие УГС. Свойство не может быть null.
     */
    @NotNull
    public TrEduGroupEvent getEvent()
    {
        return _event;
    }

    /**
     * @param event Событие УГС. Свойство не может быть null.
     */
    public void setEvent(TrEduGroupEvent event)
    {
        dirty(_event, event);
        _event = event;
    }

    /**
     * @return Студент (МСРП). Свойство не может быть null.
     */
    @NotNull
    public EppStudentWorkPlanElement getStudentWpe()
    {
        return _studentWpe;
    }

    /**
     * @param studentWpe Студент (МСРП). Свойство не может быть null.
     */
    public void setStudentWpe(EppStudentWorkPlanElement studentWpe)
    {
        dirty(_studentWpe, studentWpe);
        _studentWpe = studentWpe;
    }

    /**
     * null - не обработан, true - не посетил, false - посетил
     *
     * @return Отсутствовал.
     */
    public Boolean getAbsent()
    {
        return _absent;
    }

    /**
     * @param absent Отсутствовал.
     */
    public void setAbsent(Boolean absent)
    {
        dirty(_absent, absent);
        _absent = absent;
    }

    /**
     * @return Причина пропуска занятия.
     */
    public TrAbsenceReason getAbsenceReason()
    {
        return _absenceReason;
    }

    /**
     * @param absenceReason Причина пропуска занятия.
     */
    public void setAbsenceReason(TrAbsenceReason absenceReason)
    {
        dirty(_absenceReason, absenceReason);
        _absenceReason = absenceReason;
    }

    /**
     * @return Балл.
     */
    public Long getGradeAsLong()
    {
        return _gradeAsLong;
    }

    /**
     * @param gradeAsLong Балл.
     */
    public void setGradeAsLong(Long gradeAsLong)
    {
        dirty(_gradeAsLong, gradeAsLong);
        _gradeAsLong = gradeAsLong;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        initLazyForGet("comment");
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        initLazyForSet("comment");
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * См. ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean
     *
     * @return Дата утраты актуальности записи в журнале (в связи с переводом).
     */
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата утраты актуальности записи в журнале (в связи с переводом).
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * Системное поле, показывает, кем в последний раз были обновлены данные об отметке - отметка отсутствия, балл, комментарий.
     *
     * @return Автор отметки.
     */
    public IPrincipalContext getMarkedBy()
    {
        initLazyForGet("markedBy");
        return _markedBy;
    }

    /**
     * @param markedBy Автор отметки.
     */
    public void setMarkedBy(IPrincipalContext markedBy)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && markedBy!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPrincipalContext.class);
            IEntityMeta actual =  markedBy instanceof IEntity ? EntityRuntime.getMeta((IEntity) markedBy) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        initLazyForSet("markedBy");
        dirty(_markedBy, markedBy);
        _markedBy = markedBy;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TrEduGroupEventStudentGen)
        {
            if (withNaturalIdProperties)
            {
                setEvent(((TrEduGroupEventStudent)another).getEvent());
                setStudentWpe(((TrEduGroupEventStudent)another).getStudentWpe());
            }
            setAbsent(((TrEduGroupEventStudent)another).getAbsent());
            setAbsenceReason(((TrEduGroupEventStudent)another).getAbsenceReason());
            setGradeAsLong(((TrEduGroupEventStudent)another).getGradeAsLong());
            setComment(((TrEduGroupEventStudent)another).getComment());
            setTransferDate(((TrEduGroupEventStudent)another).getTransferDate());
            setMarkedBy(((TrEduGroupEventStudent)another).getMarkedBy());
        }
    }

    public INaturalId<TrEduGroupEventStudentGen> getNaturalId()
    {
        return new NaturalId(getEvent(), getStudentWpe());
    }

    public static class NaturalId extends NaturalIdBase<TrEduGroupEventStudentGen>
    {
        private static final String PROXY_NAME = "TrEduGroupEventStudentNaturalProxy";

        private Long _event;
        private Long _studentWpe;

        public NaturalId()
        {}

        public NaturalId(TrEduGroupEvent event, EppStudentWorkPlanElement studentWpe)
        {
            _event = ((IEntity) event).getId();
            _studentWpe = ((IEntity) studentWpe).getId();
        }

        public Long getEvent()
        {
            return _event;
        }

        public void setEvent(Long event)
        {
            _event = event;
        }

        public Long getStudentWpe()
        {
            return _studentWpe;
        }

        public void setStudentWpe(Long studentWpe)
        {
            _studentWpe = studentWpe;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TrEduGroupEventStudentGen.NaturalId) ) return false;

            TrEduGroupEventStudentGen.NaturalId that = (NaturalId) o;

            if( !equals(getEvent(), that.getEvent()) ) return false;
            if( !equals(getStudentWpe(), that.getStudentWpe()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEvent());
            result = hashCode(result, getStudentWpe());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEvent());
            sb.append("/");
            sb.append(getStudentWpe());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrEduGroupEventStudentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrEduGroupEventStudent.class;
        }

        public T newInstance()
        {
            return (T) new TrEduGroupEventStudent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "event":
                    return obj.getEvent();
                case "studentWpe":
                    return obj.getStudentWpe();
                case "absent":
                    return obj.getAbsent();
                case "absenceReason":
                    return obj.getAbsenceReason();
                case "gradeAsLong":
                    return obj.getGradeAsLong();
                case "comment":
                    return obj.getComment();
                case "transferDate":
                    return obj.getTransferDate();
                case "markedBy":
                    return obj.getMarkedBy();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "event":
                    obj.setEvent((TrEduGroupEvent) value);
                    return;
                case "studentWpe":
                    obj.setStudentWpe((EppStudentWorkPlanElement) value);
                    return;
                case "absent":
                    obj.setAbsent((Boolean) value);
                    return;
                case "absenceReason":
                    obj.setAbsenceReason((TrAbsenceReason) value);
                    return;
                case "gradeAsLong":
                    obj.setGradeAsLong((Long) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "markedBy":
                    obj.setMarkedBy((IPrincipalContext) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "event":
                        return true;
                case "studentWpe":
                        return true;
                case "absent":
                        return true;
                case "absenceReason":
                        return true;
                case "gradeAsLong":
                        return true;
                case "comment":
                        return true;
                case "transferDate":
                        return true;
                case "markedBy":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "event":
                    return true;
                case "studentWpe":
                    return true;
                case "absent":
                    return true;
                case "absenceReason":
                    return true;
                case "gradeAsLong":
                    return true;
                case "comment":
                    return true;
                case "transferDate":
                    return true;
                case "markedBy":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "event":
                    return TrEduGroupEvent.class;
                case "studentWpe":
                    return EppStudentWorkPlanElement.class;
                case "absent":
                    return Boolean.class;
                case "absenceReason":
                    return TrAbsenceReason.class;
                case "gradeAsLong":
                    return Long.class;
                case "comment":
                    return String.class;
                case "transferDate":
                    return Date.class;
                case "markedBy":
                    return IPrincipalContext.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrEduGroupEventStudent> _dslPath = new Path<TrEduGroupEventStudent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrEduGroupEventStudent");
    }
            

    /**
     * @return Событие УГС. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getEvent()
     */
    public static TrEduGroupEvent.Path<TrEduGroupEvent> event()
    {
        return _dslPath.event();
    }

    /**
     * @return Студент (МСРП). Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getStudentWpe()
     */
    public static EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> studentWpe()
    {
        return _dslPath.studentWpe();
    }

    /**
     * null - не обработан, true - не посетил, false - посетил
     *
     * @return Отсутствовал.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getAbsent()
     */
    public static PropertyPath<Boolean> absent()
    {
        return _dslPath.absent();
    }

    /**
     * @return Причина пропуска занятия.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getAbsenceReason()
     */
    public static TrAbsenceReason.Path<TrAbsenceReason> absenceReason()
    {
        return _dslPath.absenceReason();
    }

    /**
     * @return Балл.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getGradeAsLong()
     */
    public static PropertyPath<Long> gradeAsLong()
    {
        return _dslPath.gradeAsLong();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * См. ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean
     *
     * @return Дата утраты актуальности записи в журнале (в связи с переводом).
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * Системное поле, показывает, кем в последний раз были обновлены данные об отметке - отметка отсутствия, балл, комментарий.
     *
     * @return Автор отметки.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getMarkedBy()
     */
    public static IPrincipalContextGen.Path<IPrincipalContext> markedBy()
    {
        return _dslPath.markedBy();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getGrade()
     */
    public static SupportedPropertyPath<Double> grade()
    {
        return _dslPath.grade();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getPresent()
     */
    public static SupportedPropertyPath<Boolean> present()
    {
        return _dslPath.present();
    }

    public static class Path<E extends TrEduGroupEventStudent> extends EntityPath<E>
    {
        private TrEduGroupEvent.Path<TrEduGroupEvent> _event;
        private EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> _studentWpe;
        private PropertyPath<Boolean> _absent;
        private TrAbsenceReason.Path<TrAbsenceReason> _absenceReason;
        private PropertyPath<Long> _gradeAsLong;
        private PropertyPath<String> _comment;
        private PropertyPath<Date> _transferDate;
        private IPrincipalContextGen.Path<IPrincipalContext> _markedBy;
        private SupportedPropertyPath<Double> _grade;
        private SupportedPropertyPath<Boolean> _present;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Событие УГС. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getEvent()
     */
        public TrEduGroupEvent.Path<TrEduGroupEvent> event()
        {
            if(_event == null )
                _event = new TrEduGroupEvent.Path<TrEduGroupEvent>(L_EVENT, this);
            return _event;
        }

    /**
     * @return Студент (МСРП). Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getStudentWpe()
     */
        public EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> studentWpe()
        {
            if(_studentWpe == null )
                _studentWpe = new EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement>(L_STUDENT_WPE, this);
            return _studentWpe;
        }

    /**
     * null - не обработан, true - не посетил, false - посетил
     *
     * @return Отсутствовал.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getAbsent()
     */
        public PropertyPath<Boolean> absent()
        {
            if(_absent == null )
                _absent = new PropertyPath<Boolean>(TrEduGroupEventStudentGen.P_ABSENT, this);
            return _absent;
        }

    /**
     * @return Причина пропуска занятия.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getAbsenceReason()
     */
        public TrAbsenceReason.Path<TrAbsenceReason> absenceReason()
        {
            if(_absenceReason == null )
                _absenceReason = new TrAbsenceReason.Path<TrAbsenceReason>(L_ABSENCE_REASON, this);
            return _absenceReason;
        }

    /**
     * @return Балл.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getGradeAsLong()
     */
        public PropertyPath<Long> gradeAsLong()
        {
            if(_gradeAsLong == null )
                _gradeAsLong = new PropertyPath<Long>(TrEduGroupEventStudentGen.P_GRADE_AS_LONG, this);
            return _gradeAsLong;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(TrEduGroupEventStudentGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * См. ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean
     *
     * @return Дата утраты актуальности записи в журнале (в связи с переводом).
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(TrEduGroupEventStudentGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * Системное поле, показывает, кем в последний раз были обновлены данные об отметке - отметка отсутствия, балл, комментарий.
     *
     * @return Автор отметки.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getMarkedBy()
     */
        public IPrincipalContextGen.Path<IPrincipalContext> markedBy()
        {
            if(_markedBy == null )
                _markedBy = new IPrincipalContextGen.Path<IPrincipalContext>(L_MARKED_BY, this);
            return _markedBy;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getGrade()
     */
        public SupportedPropertyPath<Double> grade()
        {
            if(_grade == null )
                _grade = new SupportedPropertyPath<Double>(TrEduGroupEventStudentGen.P_GRADE, this);
            return _grade;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent#getPresent()
     */
        public SupportedPropertyPath<Boolean> present()
        {
            if(_present == null )
                _present = new SupportedPropertyPath<Boolean>(TrEduGroupEventStudentGen.P_PRESENT, this);
            return _present;
        }

        public Class getEntityClass()
        {
            return TrEduGroupEventStudent.class;
        }

        public String getEntityName()
        {
            return "trEduGroupEventStudent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getGrade();

    public abstract Boolean getPresent();
}
