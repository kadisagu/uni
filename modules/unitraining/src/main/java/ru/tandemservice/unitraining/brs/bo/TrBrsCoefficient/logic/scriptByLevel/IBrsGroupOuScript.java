/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByLevel;

import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsAttestationScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalGroupScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsSessionScriptFunctions;

/**
 * Интерфейс, которому должен соответствовать скрипт расчета рейтинга
 * заданный на деканате (Вкладка «Сессия»/«БРС (деканат)»)
 * @since 8/24/12
 */
public interface IBrsGroupOuScript extends
    IBrsJournalGroupScriptFunctions,
    IBrsAttestationScriptFunctions,
    IBrsSessionScriptFunctions
{

}
