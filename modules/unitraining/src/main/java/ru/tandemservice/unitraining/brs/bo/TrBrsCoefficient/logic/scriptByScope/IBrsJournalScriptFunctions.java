/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope;

import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.util.groovy.IScriptInstance;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.unitraining.brs.entity.catalogs.IBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;

import java.util.List;
import java.util.Map;

/**
 * Группа функций скрипта, отвечающих за работу с реализациями дисциплин
 * @author oleyba
 * @since 8/24/12
 */
public interface IBrsJournalScriptFunctions extends IScriptInstance
{
    // todo когда будет время, убить validateOnJournalAcceptance, и заменить на проверку по результатам методов getCoefficientDefList и filterCoefficientDefListForEvent

    /**
     * Метод вызывается при согласовании реализации, и должен валидировать заполнение коэфф. БРС, если это необходимо.
     * @param errors контейнер для сообщений об ошибках
     * @param journalContent структура чтения дисциплины в реализации - модули и их события
     * @param brsSettings заполненные для реализации и выше коэффициенты БРС
     * @return верно ли заполнены коэффициенты (и заполнены ли все необходимые)
     */
    boolean validateOnJournalAcceptance(ErrorCollector errors, Map<TrJournalModule, List<TrJournalEvent>> journalContent, IBrsDao.IBrsPreparedSettings brsSettings);

    /**
     * Метод вызывается при задании коэффициентов для события реализации
     * Метод должен фильтровать список предусмотренных коэфф. по параметрам события, если это предусмотрено схемой коэфф.
     * @param event событие в реализации
     * @param defList список всех предусмотренных коэфф. для событий
     * @return отфильтрованный список коэфф.
     */
    List<TrBrsCoefficientDef> filterCoefficientDefListForEvent(TrJournalEvent event, List<TrBrsCoefficientDef> defList);

    /**
     * Метод вызывается при задании коэфф. для реализации и модуля,
     * для того, чтобы отображать не все заданные в справочнике коэфф. этих уровней,
     * а только предусмотренные скриптом (имеет смысл, если на разных читающих разные схемы коэфф.).
     * Кроме того, при сохранении скрипта происходит вызов метода и создание определений коэфф. в справочнике,
     * если они там еще не присутствуют.
     * Для удобства реализации предусмотрены методы в BrsScriptUtils:
     * @see ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils#coeffDefinition(String, ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils.OwnerType, String)
     * @see ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils#coeffDefinition(String, ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils.OwnerType, String, boolean, boolean)
     * @return список определений коэфф., которые использует данный скрипт
     */
    List<IBrsCoefficientDef> getCoefficientDefList();

    /**
     * Метод рассчитывает текущий рейтинг в рамках реализации
     * @param journal реализация
     * @param journalContent структура чтения дисциплины в реализации - модули и их события
     * @param markMap отметки студентов. в скрипт передаются только те отметки, где задано либо посещение\пропуск, либо балл
     * @param preparedBrsSettings заполненные для реализации и выше коэффициенты БРС
     * @return рассчитанный текущий рейтинг и рейтинг по модулям, если это предусмотрено. См. подробнее комментарии в интерфейсе.
     */
    IBrsDao.ICurrentRatingCalc calculateCurrentRating(TrJournal journal, Map<TrJournalModule, List<TrJournalEvent>> journalContent, Map<EppStudentWorkPlanElement, Map<TrJournalEvent,ITrJournalEventDao.IJournalMarkData>> markMap, IBrsDao.IBrsPreparedSettings preparedBrsSettings);

    /**
     * @return Описания дополнительных параметров, которые рассчитываются для студента вместе с текущим рейтингом
     */
    Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions();

    /**
     * @return Список ключей дополнительных параметров, которые нужно отображать в колонках в журнале, в порядке отображения колонок
     */
    List<String> getDisplayableAdditParams();
}
