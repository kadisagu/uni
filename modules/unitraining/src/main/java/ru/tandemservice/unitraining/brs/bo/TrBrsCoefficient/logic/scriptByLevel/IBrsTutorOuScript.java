/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByLevel;

import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsAttestationScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalGroupScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsSessionScriptFunctions;

/**
 * Интерфейс, которому должен соответствовать скрипт расчета рейтинга
 * заданный на читающем подразделении (Вкладка «Журналы»/«БРС (читающее подр.)»)
 * @author oleyba
 * @since 8/24/12
 */
public interface IBrsTutorOuScript extends
    IBrsJournalGroupScriptFunctions,
    IBrsJournalScriptFunctions,
    IBrsAttestationScriptFunctions,
    IBrsSessionScriptFunctions
{
}
