package ru.tandemservice.unitraining.base.bo.TrJournal.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author vdanilov
 */
public interface ITrJournalDao extends INeedPersistenceSupport {

    /** @return правило генерации номеров */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    INumberGenerationRule<TrJournal> getNumberGenerationRule();

    /** @return создает журнал на подразделении (всегда новый) */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    TrJournal doCreateTrJournal(EppRegistryElementPart registryElementPart, EppYearPart yearPart, Boolean controlOnlyMode);

    /** @return сохраняет журнал, заполняет автозаполняемые поля - состояние, номер, название */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public TrJournal doCreateTrJournal(TrJournal journal, Boolean controlOnlyMode);


    /**
     * Линкует УГС к журналу
     * @param journal журнал
     * @param groupList список групп
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doLinkEduGroups(TrJournal journal, List<EppRealEduGroup> groupList);

    /**
     * Список студентов, которые на данный момент могут получать оценки в журнале данной группы.
     * @param group группа
     * @return Список студентов
     */
    public Collection<EppStudentWorkPlanElement> getStudentList(TrJournalGroup group);

    /**
     * Деканаты студентов, которые на данный момент могут получать оценки в журнале данной группы.
     * @param group группа
     * @return id деканатов
     */
    public Set<Long> getStudentGroupOUs(TrJournalGroup group);

    /**
     * Вычисляет для студента журналы, где он находится, по группам ФИК, к которым относится журнал.
     * @param studentSlots - набор студентов. вызывать для слотов разных частей уч. года бессмысленно, потому что журналы ищутся в части уч. года первого слота в списке.
     * @param discipline - часть дисциплины
     * @return (студент -> (группа ФИК - > журнал))
     * @throws IllegalStateException если журналов по группе больше одного - в исключении будет сообщение о том, для какого студента найден не единственный журнал.
     */
    Map<EppStudentWorkPlanElement, Map<EppFControlActionGroup, TrJournal>> fillJournalMap(Collection<EppStudentWorkPlanElement> studentSlots, EppRegistryElementPart discipline) throws IllegalStateException;

    /**
     * Вычисляет список актуальных студентов реализации
     * (актуальные в УГС в данный момент
     * + включенные на каком-то этапе в данную реализацию и до сих поо не включенных в другую).
     * @param journal журнал
     * @return список студентов
     */
    public Collection<EppStudentWorkPlanElement> getActualStudentList(TrJournal journal);

    /**
     * Вычисляет есть ли в реализации у событий оценки.
     * @param journal реализация
     * @return true, если в журнале уже есть оценки, иначе false
     */
    boolean hasMarks(TrJournal journal);

    /**
     * Копирование структуры чтения из уже имеющейся реализации в текущую.
     * @param sourceJournal копировать из
     * @param targetJournal копировать в
     */
    void doCopy(TrJournal sourceJournal, TrJournal targetJournal);

    /**
     * @return значение глобальной настройки «Не включать в структуру чтения аудиторные занятия»
     */
    boolean isGlobalControlOnlyMode();

    /**
     * Выставляет значение глобальной настройки «Не включать в структуру чтения аудиторные занятия»
     * @param globalControlOnlyMode
     */
    void setGlobalControlOnlyMode(boolean globalControlOnlyMode);

    /**
     * @return значение глобальной настройки «Не отмечать посещение для контрольных мероприятий»
     */
    boolean isHideAttendanceForControlEvents();

    /**
     * Выставляет значение глобальной настройки «Не отмечать посещение для контрольных мероприятий»
     * @param value
     */
    void setHideAttendanceForControlEvents(boolean value);

    /**
     * Получение списка преподавателей для реализации дисциплины
     *
     * @param  trJournalId id реализации
     * @return преподаватели, назначенные на реализацию дисциплины */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    List<PpsEntry> getTrJournalEppPps(Long trJournalId);


    /**
     *  Вычисляет возможно ли редактировать пары звонкового расписания
     *
     * @param  sppBellScheduleId id звонкового расписания
     * @return true, если в журнале есть события пары звонкового расписания, иначе false */
    boolean canEditBellsEntries(Long sppBellScheduleId);

    /**
     * @return Список шкал оценок форм контроля используемых в части дисциплине журнала.
     * Список содержит только уникальные шкалы и отсортирован по приоритету
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    List<EppGradeScale> getGradeScales(TrJournal journal);
}
