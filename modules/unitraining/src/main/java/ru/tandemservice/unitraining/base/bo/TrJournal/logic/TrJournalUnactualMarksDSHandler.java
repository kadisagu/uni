/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * @author oleyba
 * @since 9/8/11
 */
public class TrJournalUnactualMarksDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_STUDENT = "trJournalUnactualMarksDS_studentKey";
    public static final String COLUMN_EVENT_TITLE = "eventTitle";
    public static final String COLUMN_ABSENCE_NOTE = "absenceNote";

    public TrJournalUnactualMarksDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        final TrJournal journal = context.get(TrJournalManager.TR_JOURNAL_JOURNAL_KEY);
        final Student student = context.get(PARAM_STUDENT);

        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(TrEduGroupEventStudent.class, "e", true)
        .where(eq(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("e")), DQLExpressions.value(journal)))
        .where(isNotNull(property(TrEduGroupEventStudent.transferDate().fromAlias("e"))))
        .column("e")
        .order(property(TrEduGroupEventStudent.studentWpe().student().person().identityCard().fullFio().fromAlias("e")))
        .order(property(TrEduGroupEventStudent.event().scheduleEvent().durationBegin().fromAlias("e")))
        ;

        if (null != student)
            dql.where(eq(property(TrEduGroupEventStudent.studentWpe().student().fromAlias("e")), value(student)));

        final DSOutput output = DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();

        final List<DataWrapper> wrappers = DataWrapper.wrap(output, "id", "title");

        for (final DataWrapper wrapper: wrappers)
        {
            TrEduGroupEventStudent mark = wrapper.getWrapped();
            wrapper.put(COLUMN_EVENT_TITLE, mark.getEvent().getJournalEvent().getTitle());
            wrapper.put(COLUMN_ABSENCE_NOTE, mark.getPresent() == null ? StringUtils.EMPTY : (mark.getPresent() ? "да" : "нет"));
        }

        return output;
    }
}
