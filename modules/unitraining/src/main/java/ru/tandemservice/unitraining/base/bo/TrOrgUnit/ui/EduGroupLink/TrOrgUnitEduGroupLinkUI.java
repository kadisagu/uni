// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.EduGroupLink;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 8/14/11
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitHolder.id", required = true),
        @Bind(key = TrOrgUnitEduGroupLinkUI.GROUP_ID_LIST, binding = "groupIds")
})
public class TrOrgUnitEduGroupLinkUI extends UIPresenter
{
    public static final String GROUP_ID_LIST = "groupIds";

    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    private Collection<Long> groupIds;
    private EppYearPart yearPart;
    private EppRegistryElementPart discipline;
    private StaticListDataSource<EppRealEduGroup> groupDataSource;
    private TrJournal journal;

    @Override
    public void onComponentRefresh() {
        this.getOrgUnitHolder().refresh(OrgUnit.class);
        prepareGroupDataSource();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (TrOrgUnitManager.TR_ORGUNIT_JOURNAL_DS.equals(dataSource.getName()))
        {
            dataSource.put(TrOrgUnitManager.TR_ORGUNIT_DISCIPLINE_KEY, getDiscipline());
            dataSource.put(TrOrgUnitManager.TR_ORGUNIT_YEARPART_DS, getYearPart());
        }
    }

    public void onClickApply() {
        TrJournalManager.instance().dao().doLinkEduGroups(getJournal(), getGroupDataSource().getEntityList());
        deactivate();
    }

    private void prepareGroupDataSource()
    {
        if (CollectionUtils.isEmpty(groupIds))
            throw new ApplicationException("Не выбраны группы для прикрепления.");

        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EppRealEduGroup4LoadType.class, "grp")
        .column("grp")
        .where(in(property("grp.id"), getGroupIds()))
        .order(property(EppRealEduGroup4LoadType.type().priority().fromAlias("grp")))
        .order(property(EppRealEduGroup4LoadType.title().fromAlias("grp")))
        ;
        List<EppRealEduGroup4LoadType> groups = dql.createStatement(_uiSupport.getSession()).list();

        if (groups.isEmpty())
            throw new ApplicationException("Не выбраны группы для прикрепления.");

        DQLSelectBuilder relDQL = new DQLSelectBuilder()
        .fromEntity(TrJournalGroup.class, "rel")
        .column("rel")
        .joinPath(DQLJoinType.inner, TrJournalGroup.group().fromAlias("rel"), "grp")
        .where(in(property("grp.id"), getGroupIds()))
        ;
        List<TrJournalGroup> relList = relDQL.createStatement(_uiSupport.getSession()).list();
        if (!relList.isEmpty())
            throw new ApplicationException("Невозможно прикрепить группы - часть из них уже прикреплена к реализации.");

        setDiscipline(groups.get(0).getActivityPart());
        setYearPart(groups.get(0).getSummary().getYearPart());

        for (EppRealEduGroup4LoadType group : groups)
        {
            if (!getYearPart().equals(group.getSummary().getYearPart()))
                throw new ApplicationException("Невозможно прикрепить группы - выбранные группы относятся к разным частям года.");
            if (!getDiscipline().equals(group.getActivityPart()))
                throw new ApplicationException("Невозможно прикрепить группы - выбранные группы относятся к разным частям дисциплины реестра.");
        }

        // сформируем датасурс групп
        groupDataSource = new StaticListDataSource<EppRealEduGroup>();
        groupDataSource.addColumn(new SimpleColumn("Учебная группа", EppRealEduGroup4LoadType.title().s()));
        groupDataSource.addColumn(new SimpleColumn("Вид УГС", EppRealEduGroup4LoadType.type().title().s()));
        groupDataSource.setupRows(groups);
    }

    /*
Manager manager =  _uiConfig.getBusinessComponentMeta().getManager(); DSOutput output = manager.myListDSHandler().command().dispatch(new DSInput(), myParameter); List resultList = output.getRecordList();
     */

    // getters and setters

    public OrgUnitHolder getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public OrgUnit getOrgUnit() {
        return this.getOrgUnitHolder().getValue();
    }

    public Collection<Long> getGroupIds()
    {
        return groupIds;
    }

    public void setGroupIds(Collection<Long> groupIds)
    {
        this.groupIds = groupIds;
    }

    public EppYearPart getYearPart()
    {
        return yearPart;
    }

    public void setYearPart(EppYearPart yearPart)
    {
        this.yearPart = yearPart;
    }

    public EppRegistryElementPart getDiscipline()
    {
        return discipline;
    }

    public void setDiscipline(EppRegistryElementPart discipline)
    {
        this.discipline = discipline;
    }

    public StaticListDataSource<EppRealEduGroup> getGroupDataSource()
    {
        return groupDataSource;
    }

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }
}
