package ru.tandemservice.unitraining.base.entity.event.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Событие в журнале
 *
 * Реализация события для конкретного журнала (определяется через УГС) - показывает, что для этой УГС должно проводиться данное событие.
 * (объекты создаются автоматически для всех пар УГС и событий журнала)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrEduGroupEventGen extends EntityBase
 implements INaturalIdentifiable<TrEduGroupEventGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent";
    public static final String ENTITY_NAME = "trEduGroupEvent";
    public static final int VERSION_HASH = 1389983273;
    private static IEntityMeta ENTITY_META;

    public static final String L_JOURNAL_EVENT = "journalEvent";
    public static final String L_GROUP = "group";
    public static final String P_DEADLINE_DATE = "deadlineDate";
    public static final String L_SCHEDULE_EVENT = "scheduleEvent";
    public static final String L_BELL_SCHEDULE_ENTRY = "bellScheduleEntry";

    private TrJournalEvent _journalEvent;     // Событие в реализации
    private EppRealEduGroup _group;     // УГС
    private Date _deadlineDate;     // Дата, до которой должно произойти данное событие
    private ScheduleEvent _scheduleEvent;     // Событие в расписании
    private ScheduleBellEntry _bellScheduleEntry;     // Пара звонкового расписания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Событие в реализации. Свойство не может быть null.
     */
    @NotNull
    public TrJournalEvent getJournalEvent()
    {
        return _journalEvent;
    }

    /**
     * @param journalEvent Событие в реализации. Свойство не может быть null.
     */
    public void setJournalEvent(TrJournalEvent journalEvent)
    {
        dirty(_journalEvent, journalEvent);
        _journalEvent = journalEvent;
    }

    /**
     * @return УГС. Свойство не может быть null.
     */
    @NotNull
    public EppRealEduGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group УГС. Свойство не может быть null.
     */
    public void setGroup(EppRealEduGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Дата, до которой должно произойти данное событие.
     */
    public Date getDeadlineDate()
    {
        return _deadlineDate;
    }

    /**
     * @param deadlineDate Дата, до которой должно произойти данное событие.
     */
    public void setDeadlineDate(Date deadlineDate)
    {
        dirty(_deadlineDate, deadlineDate);
        _deadlineDate = deadlineDate;
    }

    /**
     * @return Событие в расписании.
     */
    public ScheduleEvent getScheduleEvent()
    {
        return _scheduleEvent;
    }

    /**
     * @param scheduleEvent Событие в расписании.
     */
    public void setScheduleEvent(ScheduleEvent scheduleEvent)
    {
        dirty(_scheduleEvent, scheduleEvent);
        _scheduleEvent = scheduleEvent;
    }

    /**
     * @return Пара звонкового расписания.
     */
    public ScheduleBellEntry getBellScheduleEntry()
    {
        return _bellScheduleEntry;
    }

    /**
     * @param bellScheduleEntry Пара звонкового расписания.
     */
    public void setBellScheduleEntry(ScheduleBellEntry bellScheduleEntry)
    {
        dirty(_bellScheduleEntry, bellScheduleEntry);
        _bellScheduleEntry = bellScheduleEntry;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TrEduGroupEventGen)
        {
            if (withNaturalIdProperties)
            {
                setJournalEvent(((TrEduGroupEvent)another).getJournalEvent());
                setGroup(((TrEduGroupEvent)another).getGroup());
            }
            setDeadlineDate(((TrEduGroupEvent)another).getDeadlineDate());
            setScheduleEvent(((TrEduGroupEvent)another).getScheduleEvent());
            setBellScheduleEntry(((TrEduGroupEvent)another).getBellScheduleEntry());
        }
    }

    public INaturalId<TrEduGroupEventGen> getNaturalId()
    {
        return new NaturalId(getJournalEvent(), getGroup());
    }

    public static class NaturalId extends NaturalIdBase<TrEduGroupEventGen>
    {
        private static final String PROXY_NAME = "TrEduGroupEventNaturalProxy";

        private Long _journalEvent;
        private Long _group;

        public NaturalId()
        {}

        public NaturalId(TrJournalEvent journalEvent, EppRealEduGroup group)
        {
            _journalEvent = ((IEntity) journalEvent).getId();
            _group = ((IEntity) group).getId();
        }

        public Long getJournalEvent()
        {
            return _journalEvent;
        }

        public void setJournalEvent(Long journalEvent)
        {
            _journalEvent = journalEvent;
        }

        public Long getGroup()
        {
            return _group;
        }

        public void setGroup(Long group)
        {
            _group = group;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TrEduGroupEventGen.NaturalId) ) return false;

            TrEduGroupEventGen.NaturalId that = (NaturalId) o;

            if( !equals(getJournalEvent(), that.getJournalEvent()) ) return false;
            if( !equals(getGroup(), that.getGroup()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getJournalEvent());
            result = hashCode(result, getGroup());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getJournalEvent());
            sb.append("/");
            sb.append(getGroup());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrEduGroupEventGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrEduGroupEvent.class;
        }

        public T newInstance()
        {
            return (T) new TrEduGroupEvent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "journalEvent":
                    return obj.getJournalEvent();
                case "group":
                    return obj.getGroup();
                case "deadlineDate":
                    return obj.getDeadlineDate();
                case "scheduleEvent":
                    return obj.getScheduleEvent();
                case "bellScheduleEntry":
                    return obj.getBellScheduleEntry();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "journalEvent":
                    obj.setJournalEvent((TrJournalEvent) value);
                    return;
                case "group":
                    obj.setGroup((EppRealEduGroup) value);
                    return;
                case "deadlineDate":
                    obj.setDeadlineDate((Date) value);
                    return;
                case "scheduleEvent":
                    obj.setScheduleEvent((ScheduleEvent) value);
                    return;
                case "bellScheduleEntry":
                    obj.setBellScheduleEntry((ScheduleBellEntry) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "journalEvent":
                        return true;
                case "group":
                        return true;
                case "deadlineDate":
                        return true;
                case "scheduleEvent":
                        return true;
                case "bellScheduleEntry":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "journalEvent":
                    return true;
                case "group":
                    return true;
                case "deadlineDate":
                    return true;
                case "scheduleEvent":
                    return true;
                case "bellScheduleEntry":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "journalEvent":
                    return TrJournalEvent.class;
                case "group":
                    return EppRealEduGroup.class;
                case "deadlineDate":
                    return Date.class;
                case "scheduleEvent":
                    return ScheduleEvent.class;
                case "bellScheduleEntry":
                    return ScheduleBellEntry.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrEduGroupEvent> _dslPath = new Path<TrEduGroupEvent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrEduGroupEvent");
    }
            

    /**
     * @return Событие в реализации. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent#getJournalEvent()
     */
    public static TrJournalEvent.Path<TrJournalEvent> journalEvent()
    {
        return _dslPath.journalEvent();
    }

    /**
     * @return УГС. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent#getGroup()
     */
    public static EppRealEduGroup.Path<EppRealEduGroup> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Дата, до которой должно произойти данное событие.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent#getDeadlineDate()
     */
    public static PropertyPath<Date> deadlineDate()
    {
        return _dslPath.deadlineDate();
    }

    /**
     * @return Событие в расписании.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent#getScheduleEvent()
     */
    public static ScheduleEvent.Path<ScheduleEvent> scheduleEvent()
    {
        return _dslPath.scheduleEvent();
    }

    /**
     * @return Пара звонкового расписания.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent#getBellScheduleEntry()
     */
    public static ScheduleBellEntry.Path<ScheduleBellEntry> bellScheduleEntry()
    {
        return _dslPath.bellScheduleEntry();
    }

    public static class Path<E extends TrEduGroupEvent> extends EntityPath<E>
    {
        private TrJournalEvent.Path<TrJournalEvent> _journalEvent;
        private EppRealEduGroup.Path<EppRealEduGroup> _group;
        private PropertyPath<Date> _deadlineDate;
        private ScheduleEvent.Path<ScheduleEvent> _scheduleEvent;
        private ScheduleBellEntry.Path<ScheduleBellEntry> _bellScheduleEntry;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Событие в реализации. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent#getJournalEvent()
     */
        public TrJournalEvent.Path<TrJournalEvent> journalEvent()
        {
            if(_journalEvent == null )
                _journalEvent = new TrJournalEvent.Path<TrJournalEvent>(L_JOURNAL_EVENT, this);
            return _journalEvent;
        }

    /**
     * @return УГС. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent#getGroup()
     */
        public EppRealEduGroup.Path<EppRealEduGroup> group()
        {
            if(_group == null )
                _group = new EppRealEduGroup.Path<EppRealEduGroup>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Дата, до которой должно произойти данное событие.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent#getDeadlineDate()
     */
        public PropertyPath<Date> deadlineDate()
        {
            if(_deadlineDate == null )
                _deadlineDate = new PropertyPath<Date>(TrEduGroupEventGen.P_DEADLINE_DATE, this);
            return _deadlineDate;
        }

    /**
     * @return Событие в расписании.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent#getScheduleEvent()
     */
        public ScheduleEvent.Path<ScheduleEvent> scheduleEvent()
        {
            if(_scheduleEvent == null )
                _scheduleEvent = new ScheduleEvent.Path<ScheduleEvent>(L_SCHEDULE_EVENT, this);
            return _scheduleEvent;
        }

    /**
     * @return Пара звонкового расписания.
     * @see ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent#getBellScheduleEntry()
     */
        public ScheduleBellEntry.Path<ScheduleBellEntry> bellScheduleEntry()
        {
            if(_bellScheduleEntry == null )
                _bellScheduleEntry = new ScheduleBellEntry.Path<ScheduleBellEntry>(L_BELL_SCHEDULE_ENTRY, this);
            return _bellScheduleEntry;
        }

        public Class getEntityClass()
        {
            return TrEduGroupEvent.class;
        }

        public String getEntityName()
        {
            return "trEduGroupEvent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
