package ru.tandemservice.unitraining.base.bo.TrStudent;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author avedernikov
 * @since 30.06.2016
 */
@Configuration
public class TrStudentManager extends BusinessObjectManager
{
	public static TrStudentManager instance()
	{
		return instance(TrStudentManager.class);
	}
}
