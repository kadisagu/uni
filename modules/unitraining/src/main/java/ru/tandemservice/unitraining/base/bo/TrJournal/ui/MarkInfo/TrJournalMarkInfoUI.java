/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkInfo;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;

/**
 * @author oleyba
 * @since 9/2/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "mark.id", required = true)
})
public class TrJournalMarkInfoUI extends UIPresenter
{
    private TrEduGroupEventStudent mark = new TrEduGroupEventStudent();

    @Override
    public void onComponentRefresh()
    {
        setMark(IUniBaseDao.instance.get().<TrEduGroupEventStudent>getNotNull(TrEduGroupEventStudent.class, getMark().getId()));
    }

    public TrEduGroupEventStudent getMark()
    {
        return mark;
    }

    public void setMark(TrEduGroupEventStudent mark)
    {
        this.mark = mark;
    }

    public String getStudent()
    {
        final Student student = mark.getStudentWpe().getStudent();
        return student.getPerson().getFullFio() + (student.getGroup() == null ? "" : ", " + student.getGroup().getTitle());
    }

    public String getDate()
    {
        return getMark().getEvent().getScheduleEvent() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(getMark().getEvent().getScheduleEvent().getDurationBegin());
    }

    public String getPresent()
    {
        if (null == mark.getAbsent()) { return ""; }
        return mark.getAbsent() ? "нет" : "да";
    }

    public String getGrade()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark.getGrade());
    }
}
