/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope;

import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;

import java.util.List;
import java.util.Map;

/**
 * Группа функций скрипта, отвечающих за расчет доп. статистики по графам в журналах.
 * Можно не реализовывать, тогда доп. статистики не будет.
 * Реализации можно добавлять на глобальный уровень и уровень кафедры.
 * @author oleyba
 * @since 9/25/13
 */
public interface IBrsJournalEventStatScriptFunctions
{
    /**
     * Вычисляет доп. статистику
     * @param journal реализация
     * @param group конкретный журнал, в пределах которого нужно считать статистику. может быть null, тогда статистика по всей реализации.
     * @param journalContent структура реализации для расчета
     * @param markMap данные о студентах для расчета - в расчете для каждого события считаем участвующими всех студентов, кто есть в перечне ключей мапы
     * @param brsSettings коэфф. БРС
     * @return расчет статистики
     */
    IBrsDao.IJournalEventStatCalc calculateEventStat(TrJournal journal, TrJournalGroup group, Map<TrJournalModule, List<TrJournalEvent>> journalContent, Map<EppStudentWorkPlanElement, Map<TrJournalEvent,ITrJournalEventDao.IJournalMarkData>> markMap, IBrsDao.IBrsPreparedSettings brsSettings);
}
