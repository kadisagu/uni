package ru.tandemservice.unitraining.brs.entity.catalogs.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Коэффициент БРС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrBrsCoefficientDefGen extends EntityBase
 implements INaturalIdentifiable<TrBrsCoefficientDefGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef";
    public static final String ENTITY_NAME = "trBrsCoefficientDef";
    public static final int VERSION_HASH = -1618585143;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_USER_CODE = "userCode";
    public static final String L_OWNER_TYPE = "ownerType";
    public static final String P_BOOL_VALUE_TYPE = "boolValueType";
    public static final String P_REQUIRED = "required";
    public static final String P_COMMENT = "comment";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Краткое название
    private String _userCode;     // Пользовательский код
    private TrBrsCoefficientOwnerType _ownerType;     // Тип владельца
    private boolean _boolValueType;     // Является признаком, а не числовым значением
    private boolean _required;     // Обязателен для заполнения
    private String _comment;     // Комментарий
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Краткое название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Краткое название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Пользовательский код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getUserCode()
    {
        return _userCode;
    }

    /**
     * @param userCode Пользовательский код. Свойство не может быть null и должно быть уникальным.
     */
    public void setUserCode(String userCode)
    {
        dirty(_userCode, userCode);
        _userCode = userCode;
    }

    /**
     * @return Тип владельца. Свойство не может быть null.
     */
    @NotNull
    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return _ownerType;
    }

    /**
     * @param ownerType Тип владельца. Свойство не может быть null.
     */
    public void setOwnerType(TrBrsCoefficientOwnerType ownerType)
    {
        dirty(_ownerType, ownerType);
        _ownerType = ownerType;
    }

    /**
     * @return Является признаком, а не числовым значением. Свойство не может быть null.
     */
    @NotNull
    public boolean isBoolValueType()
    {
        return _boolValueType;
    }

    /**
     * @param boolValueType Является признаком, а не числовым значением. Свойство не может быть null.
     */
    public void setBoolValueType(boolean boolValueType)
    {
        dirty(_boolValueType, boolValueType);
        _boolValueType = boolValueType;
    }

    /**
     * @return Обязателен для заполнения. Свойство не может быть null.
     */
    @NotNull
    public boolean isRequired()
    {
        return _required;
    }

    /**
     * @param required Обязателен для заполнения. Свойство не может быть null.
     */
    public void setRequired(boolean required)
    {
        dirty(_required, required);
        _required = required;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=1024)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TrBrsCoefficientDefGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((TrBrsCoefficientDef)another).getCode());
            }
            setShortTitle(((TrBrsCoefficientDef)another).getShortTitle());
            setUserCode(((TrBrsCoefficientDef)another).getUserCode());
            setOwnerType(((TrBrsCoefficientDef)another).getOwnerType());
            setBoolValueType(((TrBrsCoefficientDef)another).isBoolValueType());
            setRequired(((TrBrsCoefficientDef)another).isRequired());
            setComment(((TrBrsCoefficientDef)another).getComment());
            setTitle(((TrBrsCoefficientDef)another).getTitle());
        }
    }

    public INaturalId<TrBrsCoefficientDefGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<TrBrsCoefficientDefGen>
    {
        private static final String PROXY_NAME = "TrBrsCoefficientDefNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TrBrsCoefficientDefGen.NaturalId) ) return false;

            TrBrsCoefficientDefGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrBrsCoefficientDefGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrBrsCoefficientDef.class;
        }

        public T newInstance()
        {
            return (T) new TrBrsCoefficientDef();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "userCode":
                    return obj.getUserCode();
                case "ownerType":
                    return obj.getOwnerType();
                case "boolValueType":
                    return obj.isBoolValueType();
                case "required":
                    return obj.isRequired();
                case "comment":
                    return obj.getComment();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "userCode":
                    obj.setUserCode((String) value);
                    return;
                case "ownerType":
                    obj.setOwnerType((TrBrsCoefficientOwnerType) value);
                    return;
                case "boolValueType":
                    obj.setBoolValueType((Boolean) value);
                    return;
                case "required":
                    obj.setRequired((Boolean) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "userCode":
                        return true;
                case "ownerType":
                        return true;
                case "boolValueType":
                        return true;
                case "required":
                        return true;
                case "comment":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "userCode":
                    return true;
                case "ownerType":
                    return true;
                case "boolValueType":
                    return true;
                case "required":
                    return true;
                case "comment":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "userCode":
                    return String.class;
                case "ownerType":
                    return TrBrsCoefficientOwnerType.class;
                case "boolValueType":
                    return Boolean.class;
                case "required":
                    return Boolean.class;
                case "comment":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrBrsCoefficientDef> _dslPath = new Path<TrBrsCoefficientDef>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrBrsCoefficientDef");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Краткое название. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Пользовательский код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#getUserCode()
     */
    public static PropertyPath<String> userCode()
    {
        return _dslPath.userCode();
    }

    /**
     * @return Тип владельца. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#getOwnerType()
     */
    public static TrBrsCoefficientOwnerType.Path<TrBrsCoefficientOwnerType> ownerType()
    {
        return _dslPath.ownerType();
    }

    /**
     * @return Является признаком, а не числовым значением. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#isBoolValueType()
     */
    public static PropertyPath<Boolean> boolValueType()
    {
        return _dslPath.boolValueType();
    }

    /**
     * @return Обязателен для заполнения. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#isRequired()
     */
    public static PropertyPath<Boolean> required()
    {
        return _dslPath.required();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends TrBrsCoefficientDef> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _userCode;
        private TrBrsCoefficientOwnerType.Path<TrBrsCoefficientOwnerType> _ownerType;
        private PropertyPath<Boolean> _boolValueType;
        private PropertyPath<Boolean> _required;
        private PropertyPath<String> _comment;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(TrBrsCoefficientDefGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Краткое название. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(TrBrsCoefficientDefGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Пользовательский код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#getUserCode()
     */
        public PropertyPath<String> userCode()
        {
            if(_userCode == null )
                _userCode = new PropertyPath<String>(TrBrsCoefficientDefGen.P_USER_CODE, this);
            return _userCode;
        }

    /**
     * @return Тип владельца. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#getOwnerType()
     */
        public TrBrsCoefficientOwnerType.Path<TrBrsCoefficientOwnerType> ownerType()
        {
            if(_ownerType == null )
                _ownerType = new TrBrsCoefficientOwnerType.Path<TrBrsCoefficientOwnerType>(L_OWNER_TYPE, this);
            return _ownerType;
        }

    /**
     * @return Является признаком, а не числовым значением. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#isBoolValueType()
     */
        public PropertyPath<Boolean> boolValueType()
        {
            if(_boolValueType == null )
                _boolValueType = new PropertyPath<Boolean>(TrBrsCoefficientDefGen.P_BOOL_VALUE_TYPE, this);
            return _boolValueType;
        }

    /**
     * @return Обязателен для заполнения. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#isRequired()
     */
        public PropertyPath<Boolean> required()
        {
            if(_required == null )
                _required = new PropertyPath<Boolean>(TrBrsCoefficientDefGen.P_REQUIRED, this);
            return _required;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(TrBrsCoefficientDefGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(TrBrsCoefficientDefGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return TrBrsCoefficientDef.class;
        }

        public String getEntityName()
        {
            return "trBrsCoefficientDef";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
