/* $Id: controllerPub.vm 19678 2011-08-30 02:54:12Z vdanilov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unitraining.component.catalog.trBrsCoefficientDef.TrBrsCoefficientDefPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;

/**
 * @author AutoGenerator
 * Created on 30.08.2012
 */
public class Controller extends DefaultCatalogPubController<TrBrsCoefficientDef, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<TrBrsCoefficientDef> createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<TrBrsCoefficientDef> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(new SimpleColumn("Код", TrBrsCoefficientDef.P_USER_CODE));
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", TrBrsCoefficientDef.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", TrBrsCoefficientDef.P_SHORT_TITLE));
        dataSource.addColumn(new SimpleColumn("Уровень", TrBrsCoefficientDef.ownerType().title()));
        dataSource.addColumn(new SimpleColumn("Флаг", TrBrsCoefficientDef.P_BOOL_VALUE_TYPE).setFormatter(YesNoFormatter.IGNORE_NUMBERS));
        dataSource.addColumn(new SimpleColumn("Обязательный", TrBrsCoefficientDef.P_REQUIRED).setFormatter(YesNoFormatter.IGNORE_NUMBERS));
        dataSource.addColumn(new SimpleColumn("Комментарий", TrBrsCoefficientDef.P_COMMENT));


        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", TrBrsCoefficientDef.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        return dataSource;
    }
}
