package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.EduGroupList;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.Pub.TrJournalPub;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.logic.TrOrgUnitEduGroupDSHandler;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * @author vdanilov
 */
@Configuration
public class TrOrgUnitEduGroupList extends BusinessComponentManager
{
    public static final String TR_ORGUNIT_EDU_GROUP_DS = "trOrgUnitEduGroupDS";
    public static final String GROUP_DS = "groupDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(this.searchListDS(TrOrgUnitEduGroupList.TR_ORGUNIT_EDU_GROUP_DS, this.trOrgUnitEduGroupDSColumns(), this.trOrgUnitEduGroupDSHandler()))
        .addDataSource(TrOrgUnitManager.instance().trOrgUnitYearPartDSConfig())
        .addDataSource(TrOrgUnitManager.instance().trOrgUnitregistryElementPartDSConfig())
        .addDataSource(TrOrgUnitManager.instance().trOrgUnitPpsDSConfig())
        .addDataSource(EppStateManager.instance().eppStateDSConfig())
        .addDataSource(selectDS(GROUP_DS, groupDSHandler()))
        .create();
    }

    @Bean
    public ColumnListExtPoint trOrgUnitEduGroupDSColumns() {

        final IPublisherLinkResolver resolver = new IPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) {
                return new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId())
                .add("orgUnitId", ((TrOrgUnitEduGroupListUI)ContextLocal.getComponent().getPresenter()).getOrgUnit().getId());
            }
            @Override public String getComponentName(final IEntity entity) {
                return "ru.tandemservice.uniepp.component.edugroup.pub.TutorOrgUnitGroupPub";
            }
        };

        return this.columnListExtPointBuilder(TrOrgUnitEduGroupList.TR_ORGUNIT_EDU_GROUP_DS)
        .addColumn(checkboxColumn("select").create())
        .addColumn(publisherColumn(EppRealEduGroup4LoadType.P_TITLE, EppRealEduGroup4LoadType.P_TITLE).publisherLinkResolver(resolver).order().create())
        .addColumn(textColumn(EppRealEduGroup4LoadType.L_ACTIVITY_PART, EppRealEduGroup4LoadType.activityPart().titleWithNumber()).order().create())
        .addColumn(textColumn(EppRealEduGroup4LoadType.L_TYPE, EppRealEduGroup4LoadType.type().title()).order().create())
        .addColumn(textColumn("course", TrOrgUnitEduGroupDSHandler.COLUMN_COURSE).formatter(UniEppUtils.NEW_LINE_FORMATTER).create())
        .addColumn(textColumn("group", TrOrgUnitEduGroupDSHandler.COLUMN_GROUP).formatter(UniEppUtils.NEW_LINE_FORMATTER).create())
        .addColumn(textColumn("eduOu", TrOrgUnitEduGroupDSHandler.COLUMN_EDU_OU).formatter(UniEppUtils.NEW_LINE_FORMATTER).create())
        .addColumn(textColumn("pps", TrOrgUnitEduGroupDSHandler.COLUMN_PPS).formatter(UniEppUtils.NEW_LINE_FORMATTER).create())
        .addColumn(textColumn(EppRealEduGroup4LoadType.L_LEVEL, EppRealEduGroup4LoadType.level().displayableTitle()).create())
        .addColumn(publisherColumn("journal", "calculatedTitle").publisherLinkResolver(new DefaultPublisherLinkResolver(TrJournalPub.class.getSimpleName())).entityListProperty(TrOrgUnitEduGroupDSHandler.COLUMN_JOURNAL).create())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trOrgUnitEduGroupDSHandler() {
        return new TrOrgUnitEduGroupDSHandler(this.getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> groupDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final String filter = input.getComboFilterByValue();

                final OrgUnit orgUnit = context.get(TrOrgUnitEduGroupDSHandler.PARAM_ORG_UNIT);
                final EppYearPart yearPart = context.get(TrOrgUnitEduGroupDSHandler.PARAM_YEAR_PART);

                DQLSelectBuilder entityDQL = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadType.class, "e")
                    .where(DQLExpressions.eq(DQLExpressions.property(EppRealEduGroup4LoadType.activityPart().registryElement().owner().fromAlias("e")), DQLExpressions.value(orgUnit)))
                    .where(DQLExpressions.eq(DQLExpressions.property(EppRealEduGroup4LoadType.summary().yearPart().fromAlias("e")), DQLExpressions.value(yearPart)));

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "b").column(property(EppRealEduGroup4LoadTypeRow.studentGroupTitle().fromAlias("b")))
                    .where(in(property(EppRealEduGroup4LoadTypeRow.group().fromAlias("b")), entityDQL.buildQuery()))
                    .where(isNull(property(EppRealEduGroup4LoadTypeRow.removalDate().fromAlias("b"))))
                    .order(property(EppRealEduGroup4LoadTypeRow.studentGroupTitle().fromAlias("b")))
                    .predicate(DQLPredicateType.distinct);

                if (!StringUtils.isEmpty(filter))
                    builder.where(like(upper(property(EppRealEduGroup4LoadTypeRow.studentGroupTitle().fromAlias("b"))), value(CoreStringUtils.escapeLike(filter, true))));

                final List<DataWrapper> wrapperList = new ArrayList<>();
                for (String s : builder.createStatement(context.getSession()).<String>list()) {
                    String title = StringUtils.trimToEmpty(s);
                    wrapperList.add(new DataWrapper(System.identityHashCode(title.intern()), title));
                }

                context.put(UIDefines.COMBO_OBJECT_LIST, wrapperList);
                return super.execute(input, context);
            }

        };
    }
}
