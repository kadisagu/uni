/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.unitraining.brs.entity.catalogs.IBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;

/**
 * @author oleyba
 * @since 11/15/12
 */
public class BrsScriptUtils
{
    public static ISessionBrsDao.IRatingValue ratingValue(final Double value, final String message)
    {
        return new ISessionBrsDao.IRatingValue() {
            @Override public String getMessage() { return message; }
            @Override public Double getValue() { return value; }
        };
    }

    public static SessionMarkGradeValueCatalogItem getSessionMark(String code)
    {
        return IUniBaseDao.instance.get().getByNaturalId(new SessionMarkGradeValueCatalogItem.NaturalId(code, SessionMarkGradeValueCatalogItem.CATALOG_CODE));
    }
    
    public static enum OwnerType
    {
        ACADEMY, GROUP_OU, TUTOR_OU, JOURNAL, MODULE, EVENT
    }

    /**
     * Утильный метод для создания определения коэффициента в скрипте
     * @see ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalScriptFunctions#getCoefficientDefList()
     * Создает определение числового обязательного коэфф.
     * Метод для нечисловых/обязательных:
     * @see ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils#coeffDefinition(String, ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils.OwnerType, String, boolean, boolean)
     * @param code пользовательский код элемента справочника
     * @param ownerType уровень коэффициента - глобальный, деканат, читающее, журнал, модуль, событие
     * @param title название коэффициента
     * @return определение коэффициента
     */
    public static IBrsCoefficientDef coeffDefinition(final String code, final OwnerType ownerType, final String title) {
        return coeffDefinition(code, ownerType, title, true, false);
    }

    /**
     * Утильный метод для создания определения коэффициента в скрипте
     * @see ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalScriptFunctions#getCoefficientDefList()
     * @param code пользовательский код элемента справочника
     * @param ownerType уровень коэффициента - глобальный, деканат, читающее, журнал, модуль, событие
     * @param title название коэффициента
     * @param required является ли коэфф. обязательным
     * @param boolValue является ли коэфф. флагом
     * @return определение коэффициента
     */
    public static IBrsCoefficientDef coeffDefinition(final String code, final OwnerType ownerType, final String title, final boolean required, final boolean boolValue) {
        return new IBrsCoefficientDef() {
            @Override public String getUserCode() { return code; }
            @Override public String getTitle() {return title; }
            @Override public String getShortTitle() {return title; }
            @Override public boolean isRequired() { return required; }
            @Override public boolean isBoolValueType() { return boolValue; }
            @Override public TrBrsCoefficientOwnerType getOwnerType() {
                switch (ownerType) {
                    case ACADEMY: return IUniBaseDao.instance.get().getCatalogItem(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerTypeCodes.ACADEMY);
                    case GROUP_OU: return IUniBaseDao.instance.get().getCatalogItem(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerTypeCodes.GROUP_OU);
                    case TUTOR_OU: return IUniBaseDao.instance.get().getCatalogItem(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerTypeCodes.TUTOR_OU);
                    case JOURNAL: return IUniBaseDao.instance.get().getCatalogItem(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerTypeCodes.JOURNAL);
                    case MODULE: return IUniBaseDao.instance.get().getCatalogItem(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerTypeCodes.JOURNAL_MODULE);
                    case EVENT: return IUniBaseDao.instance.get().getCatalogItem(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerTypeCodes.JOURNAL_EVENT);
                }
                throw new IllegalArgumentException();
            }
        };
    }
    
    public static ISessionBrsDao.IRatingAdditParamDef additParamDef(final String key, final String title, final String abbreviation, final String comment) {
        return new ISessionBrsDao.IRatingAdditParamDef() {
            @Override public String getKey() { return key; }
            @Override public String getTitle() { return title; }
            @Override public String getAbbreviation() { return abbreviation; }
            @Override public String getComment() {
                return comment;
            }
        };
    }

    public static IBrsDao.IAggregatedRatingValue getNotApplicableValue() {
        return AGGREGATED_VALUE_NA;
    }

    private static final IBrsDao.IAggregatedRatingValue AGGREGATED_VALUE_NA = new IBrsDao.IAggregatedRatingValue() {
        @Override public boolean isApplicable() { return false; }
        @Override public String getMessage() { return null; }
        @Override public Double getValue() { return null; }
    };
}
