/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.EventThemeEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unitraining.base.bo.TrHomePage.TrHomePageManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAction;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.EditBase.TrBrsCoefficientEditBase;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.EditBase.TrBrsCoefficientEditBaseUI;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;
import ru.tandemservice.unitraining.brs.entity.catalogs.gen.TrBrsCoefficientOwnerTypeGen;

import java.util.List;

/**
 * @author oleyba
 * @since 8/10/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true)
})
@Output({
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_HOLDER, binding = "holder", required = true),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType", required = true)
})
public class TrJournalEventThemeEditUI extends UIPresenter
{
    private static final List<String> ALLOW_STATES = TrHomePageManager.ALLOW_EDIT_EVENT_CONFIG_STATES;
    private static final String COMPONENT_NAME = TrBrsCoefficientEditBase.class.getSimpleName();

    private EntityHolder<TrJournalEvent> holder = new EntityHolder<>();

    private boolean disabled = false;

    @Override
    public void onComponentRefresh()
    {
        RenderComponent.deactivate(COMPONENT_NAME);
        this.disabled = !isEnabled(getHolder().refresh());
    }

    /* @return true, если редактировать можно */
    protected boolean isEnabled(final TrJournalEvent event)
    {
        // редактировать можно в определенных состояниях
        boolean allowEditByState = ALLOW_STATES.contains(event.getJournalModule().getJournal().getState().getCode());
        return allowEditByState && !event.isHasMarks();

    }

    public void onClickApply()
    {
        IUniBaseDao.instance.get().saveOrUpdate(getEvent());
        if (!isDisabled()) {
            DataAccessServices.dao().doInTransaction(session -> {
                    ((TrBrsCoefficientEditBaseUI) RenderComponent.get(COMPONENT_NAME).getActiveComponent().getPresenter()).save();
                    return getEvent();
                });
        }
        TrJournalDaemonBean.DAEMON.wakeUpAndWaitDaemon(Integer.MAX_VALUE);
        _uiSupport.setRefreshScheduled(true);
        deactivate();
    }

    public boolean isDisabled()
    {
        return this.disabled;
    }

    public void setDisabled(boolean disabled)
    {
        this.disabled = disabled;
    }

    public boolean isControlActionEvent() {
        return getEvent() instanceof TrEventAction;
    }

    public TrJournalEvent getEvent() {
        return holder.getValue();
    }

    public EntityHolder<TrJournalEvent> getHolder() {
        return holder;
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return DataAccessServices.dao().getByNaturalId(new TrBrsCoefficientOwnerTypeGen.NaturalId(TrBrsCoefficientOwnerTypeCodes.JOURNAL_EVENT));
    }
}
