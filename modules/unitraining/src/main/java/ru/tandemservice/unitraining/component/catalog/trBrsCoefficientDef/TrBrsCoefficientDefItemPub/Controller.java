/* $Id: controllerItemPub.vm 6177 2009-01-13 14:09:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unitraining.component.catalog.trBrsCoefficientDef.TrBrsCoefficientDefItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubController;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;

/**
 * @author AutoGenerator
 * Created on 30.08.2012
 */
public class Controller extends DefaultCatalogItemPubController<TrBrsCoefficientDef, Model, IDAO>
{
}
