/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrEduGroup.ui.AbsenceReasonsTab;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.component.edugroup.pub.GroupContent.IGroupContentParameters;
import ru.tandemservice.unitraining.base.bo.TrEduGroup.TrEduGroupManager;
import ru.tandemservice.unitraining.base.bo.TrEduGroup.logic.TrEduGroupAbsenceDSHandler;
import ru.tandemservice.unitraining.base.bo.TrEduGroup.ui.AbsenceReasonEdit.TrEduGroupAbsenceReasonEdit;
import ru.tandemservice.unitraining.base.bo.TrEduGroup.ui.AbsenceReasonEdit.TrEduGroupAbsenceReasonEditUI;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.Collection;
import java.util.Map;

/**
 * @author oleyba
 * @since 9/23/11
 */
@Input({
    @Bind(key = TrJournalGroupMarkEditUI.GROUP_BINDING, binding = "group.id", required = true),
    @Bind(key="parameters", binding="parameters", required=true)
})
public class TrEduGroupAbsenceReasonsTabUI extends UIPresenter
{
    private static final Long NOT_FILLED_ID = 0L;
    private static final Long VALID_ID = 1L;
    private static final Long NOT_VALID_ID = 2L;

    private IGroupContentParameters parameters;
    private EntityHolder<TrJournalGroup> group = new EntityHolder<>();

    private ISelectModel filledModel = new LazySimpleSelectModel<>(ImmutableList.of(
            new IdentifiableWrapper(NOT_FILLED_ID, "Не заполнена"),
            new IdentifiableWrapper(VALID_ID, "Уважительная"),
            new IdentifiableWrapper(NOT_VALID_ID, "Неуважительная")));

    // actions

    @Override
    public void onComponentRefresh()
    {
        getGroup().refresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (TrEduGroupAbsenceReasonsTab.TR_EDU_GROUP_ABSENCE_DS.equals(dataSource.getName()))
        {

            dataSource.put(TrEduGroupAbsenceDSHandler.PARAM_GROUP, getGroup().getValue());
            dataSource.put(TrEduGroupAbsenceDSHandler.PARAM_STUDENT, getSettings().get("student"));

            final IdentifiableWrapper filled = getSettings().get("filled");

            if (null != filled)
            {
                if (NOT_FILLED_ID.equals(filled.getId()))
                    dataSource.put(TrEduGroupAbsenceDSHandler.PARAM_NOT_FILLED, Boolean.TRUE);
                else
                    dataSource.put(TrEduGroupAbsenceDSHandler.PARAM_VALID, VALID_ID.equals(filled.getId()));
            }
        }

        if (TrEduGroupManager.DS_TR_EDU_GROUP_STUDENT.equals(dataSource.getName()))
            dataSource.put(TrEduGroupManager.KEY_EDU_GROUP, getGroup().getValue().getGroup());
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((CheckboxColumn) ((PageableSearchListDataSource) _uiConfig.getDataSource(TrEduGroupAbsenceReasonsTab.TR_EDU_GROUP_ABSENCE_DS)).getLegacyDataSource().getColumn(TrEduGroupAbsenceDSHandler.COLUMN_SELECT)).getSelectedObjects();
            selected.clear();
        }
    }

    public void onEditReason()
    {
        Collection<IEntity> selected = ((CheckboxColumn) ((PageableSearchListDataSource) _uiConfig.getDataSource(TrEduGroupAbsenceReasonsTab.TR_EDU_GROUP_ABSENCE_DS)).getLegacyDataSource().getColumn(TrEduGroupAbsenceDSHandler.COLUMN_SELECT)).getSelectedObjects();
        _uiActivation.asRegionDialog(TrEduGroupAbsenceReasonEdit.class)
        .parameter(TrEduGroupAbsenceReasonEditUI.BINDING_IDS, UniBaseDao.ids(selected))
        .activate();
    }

    // presenter

    public CommonPostfixPermissionModelBase getSec()
    {
        return getParameters().getOrgUnitHolder().getSecModel();
    }

    // getters and setters

    public EntityHolder<TrJournalGroup> getGroup()
    {
        return group;
    }

    public void setGroup(EntityHolder<TrJournalGroup> group)
    {
        this.group = group;
    }

    public ISelectModel getFilledModel()
    {
        return filledModel;
    }

    public IGroupContentParameters getParameters()
    {
        return parameters;
    }

    public void setParameters(IGroupContentParameters parameters)
    {
        this.parameters = parameters;
    }
}
