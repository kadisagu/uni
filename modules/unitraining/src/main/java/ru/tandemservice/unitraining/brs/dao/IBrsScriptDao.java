/* $Id:$ */
package ru.tandemservice.unitraining.brs.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniScriptDao;
import ru.tandemservice.uni.util.groovy.IScriptInstance;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsAttestationScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalGroupScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsSessionScriptFunctions;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;

/**
 * @author oleyba
 * @since 1/15/13
 */
public interface IBrsScriptDao extends IUniScriptDao
{
    SpringBeanCache<IBrsScriptDao> instance = new SpringBeanCache<>(IBrsScriptDao.class.getName());

    void doSaveUserScript(TrOrgUnitSettings settings);

    TrOrgUnitSettings getOrgUnitBrsSettings(EppYearPart yearPart, OrgUnit tutorOu, OrgUnit groupOu);

    IBrsScriptWrapper<IBrsJournalScriptFunctions> getJournalScript(final TrJournal journal);

    IBrsScriptWrapper<IBrsJournalGroupScriptFunctions> getJournalGroupScript(final TrJournal journal, final EppStudentWorkPlanElement slot);

    IBrsScriptWrapper<IBrsAttestationScriptFunctions> getAttestationScript(final EppStudentWorkPlanElement slot);

    IBrsScriptWrapper<IBrsSessionScriptFunctions> getSessionScript(final EppStudentWorkPlanElement slot);

    IBrsScriptWrapper<IBrsSessionScriptFunctions> getSessionScript(EppYearPart yearPart, OrgUnit groupOu, OrgUnit tutorOu);

    <S extends IScriptInstance> IBrsScriptWrapper<S> buildDefaultGlobalScriptWrapper(final EppYearPart yearPart, final Class<S> klass);

    interface IBrsScriptWrapper<S extends IScriptInstance> extends IScriptWrapper<S, TrOrgUnitSettings> {

    }
}
