/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalMark;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unitraining.base.bo.TrHomePage.TrHomePageManager;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalMarkView.TrHomePageJournalMarkView;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.IJournalGroupMarkEditAdditionalButton;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.Arrays;
import java.util.List;

/**
 * @author oleyba
 * @since 9/8/11
 */
@State({
    @Bind(key = TrJournalGroupMarkEditUI.GROUP_BINDING, binding = "group.id", required = true)
})
@Input({
    @Bind(key = TrJournalGroupMarkEditUI.EVENT_BINDING, binding = "journalEventForEditId")
})
@Output({
    @Bind(key = TrJournalGroupMarkEditUI.GROUP_BINDING, binding = "group.id"),
    @Bind(key = TrJournalGroupMarkEditUI.EVENT_BINDING, binding = "journalEventForEditId"),
    @Bind(key = TrJournalGroupMarkEditUI.ADDITIONAL_BUTTONS_BINDING, binding = "additionalButtons")        
})
public class TrHomePageJournalMarkUI extends UIPresenter
{
    private TrJournalGroup group = new TrJournalGroup();
    private Long journalEventForEditId;

    private boolean hasAccess;
    private String accessErrorMessage;

    // actions

    @Override
    public void onComponentRefresh()
    {
        setGroup(IUniBaseDao.instance.get().get(TrJournalGroup.class, getGroup().getId()));
        checkAccess();
    }

    public void onClickViewMarks() {
        _uiActivation.asDesktopRoot(TrHomePageJournalMarkView.class)
        .parameter(IUIPresenter.PUBLISHER_ID, getGroup().getJournal().getId())
        .activate();
    }

    // presenter
    
    public List<IJournalGroupMarkEditAdditionalButton> getAdditionalButtons() {
        return Arrays.asList((IJournalGroupMarkEditAdditionalButton) new IJournalGroupMarkEditAdditionalButton() {
            @Override
            public String getId() {
                return "viewAllMarks";
            }

            @Override
            public String getTitle() {
                return "Просмотреть полную таблицу отметок";
            }

            @Override
            public void onClickAction(TrJournalGroup group) {
                _uiActivation.asDesktopRoot(TrHomePageJournalMarkView.class)
                .parameter(IUIPresenter.PUBLISHER_ID, group.getJournal().getId())
                .activate();
            }
        });
    }

    public String getDisciplineTitle()
    {
        return getGroup().getJournal().getRegistryElementPart().getTitle();
    }

    public String getGroupTitle()
    {
        return getGroup().getGroup().getTitle();
    }

    // data access

    private void checkAccess()
    {
        setAccessErrorMessage(TrHomePageManager.instance().dao().checkPpsAccessForMarking(_uiConfig.getUserContext().getPrincipalContext(), getGroup()));
        setHasAccess(getAccessErrorMessage() == null);
    }

    // getters and setters

    public TrJournalGroup getGroup()
    {
        return group;
    }

    public void setGroup(TrJournalGroup group)
    {
        this.group = group;
    }

    public Long getJournalEventForEditId()
    {
        return journalEventForEditId;
    }

    public void setJournalEventForEditId(Long journalEventForEditId)
    {
        this.journalEventForEditId = journalEventForEditId;
    }

    public boolean isHasAccess()
    {
        return hasAccess;
    }

    public void setHasAccess(boolean hasAccess)
    {
        this.hasAccess = hasAccess;
    }

    public String getAccessErrorMessage()
    {
        return accessErrorMessage;
    }

    public void setAccessErrorMessage(String accessErrorMessage)
    {
        this.accessErrorMessage = accessErrorMessage;
    }
}
