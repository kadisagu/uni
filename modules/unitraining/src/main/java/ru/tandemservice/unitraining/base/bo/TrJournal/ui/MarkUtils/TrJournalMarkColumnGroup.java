/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAddon;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;

/**
 * @author oleyba
 * @since 9/7/11
 */
public class TrJournalMarkColumnGroup extends IdentifiableWrapper<TrJournalModule>
{
    private TrJournalModule module;
    private List<TrJournalMarkColumn> columnList = new ArrayList<>();
    private Map<Long, Double> totals = SafeMap.get(key -> .0);

    public TrJournalMarkColumnGroup(TrJournalModule module)
    {
        super(module.getId(), module.getModule().getModule().getTitle());
        this.module = module;
    }

    public TrJournalModule getModule() { return module; }
    public List<TrJournalMarkColumn> getColumnList() { return columnList; }
    public void setColumnList(List<TrJournalMarkColumn> columnList) { this.columnList = columnList; }

    public Double getTotalDoubleValue(Long epvStudentId)
    {
        return totals.get(epvStudentId);
    }

    public String getTotal(Long epvStudentId)
    {
        final Double total = totals.get(epvStudentId);
        return UniBaseUtils.eq(total, 0) ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(total);
    }

    public void incTotal(TrEduGroupEventStudent event)
    {
        if (null != event.getGrade())
            totals.put(event.getStudentWpe().getId(), totals.get(event.getStudentWpe().getId()) + event.getGrade());
    }
    
    public int getColumnListSize()
    {
        for (TrJournalMarkColumn c : getColumnList())
            if (c.getJEvent() instanceof TrEventAddon)
                return getColumnList().size() + 1;
        return getColumnList().size();
    }
    
    public boolean isAdditionalStarts(TrJournalMarkColumn column)
    {
        return column.getJEvent() instanceof TrEventAddon && ! (getColumnList().get(Math.max(0, getColumnList().indexOf(column) - 1)).getJEvent() instanceof TrEventAddon);
    }
}
