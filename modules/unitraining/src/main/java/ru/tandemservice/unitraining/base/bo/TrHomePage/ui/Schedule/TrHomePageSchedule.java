/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage.ui.Schedule;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.util.Icon;

import ru.tandemservice.unitraining.base.bo.TrHomePage.logic.TrHomePageScheduleDSHandler;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;

/**
 * @author oleyba
 * @since 9/22/11
 */
@Configuration
public class TrHomePageSchedule extends BusinessComponentManager
{
    public static final String TR_HOME_PAGE_SCHEDULE_DS = "trHomePageScheduleDS";
    
        @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(this.searchListDS(TrHomePageSchedule.TR_HOME_PAGE_SCHEDULE_DS, this.trHomePageScheduleDSColumns(), this.trHomePageScheduleDSHandler()))
        .create();
    }

    @Bean
    public ColumnListExtPoint trHomePageScheduleDSColumns() {
        return this.columnListExtPointBuilder(TrHomePageSchedule.TR_HOME_PAGE_SCHEDULE_DS)
            .addColumn(textColumn("eventTime", TrEduGroupEvent.scheduleEvent().fullTimeStr().s()).create())
            .addColumn(textColumn("eventTitle", TrEduGroupEvent.journalEvent().s() + ".title").create())
            .addColumn(textColumn("eduGroup", TrEduGroupEvent.group().title()).create())
            .addColumn(actionColumn("mark", new Icon("edit_mark", "Выставить отметки"), "onEditMark").visible("ui:editColumnVisible").create())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trHomePageScheduleDSHandler() {
        return new TrHomePageScheduleDSHandler(this.getName());
    }
}
