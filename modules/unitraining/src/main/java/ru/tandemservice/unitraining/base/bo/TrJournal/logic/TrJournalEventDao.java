/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.logic;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.EntityRenumerator;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace;
import ru.tandemservice.unitraining.base.entity.catalog.TrAbsenceReason;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.*;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;

import java.util.*;

/**
 * @author oleyba
 * @since 8/11/11
 */
public class TrJournalEventDao extends UniBaseDao implements ITrJournalEventDao
{
    @Override
    public void deleteEvent(Long evendId)
    {
        final IEntity currentEntity = get(evendId);
        if (!(currentEntity instanceof TrJournalEvent))
            return;

        delete(currentEntity);
        getSession().flush();

        List<TrJournalEvent> events = IUniBaseDao.instance.get().getList(TrJournalEvent.class, TrJournalEvent.journalModule(), ((TrJournalEvent) currentEntity).getJournalModule(), TrJournalEvent.P_NUMBER);

        new EntityRenumerator<TrJournalEvent>() {
            @Override protected int getNumber(final TrJournalEvent e) { return e.getNumber(); }
            @Override protected void setNumber(final TrJournalEvent e, final int number) { e.setNumber(number); getSession().update(e); getSession().flush(); }
        }.execute(events);
    }

    @Override
    public void moveEvent(Long evendId, boolean down)
    {
        final IEntity currentEntity = get(evendId);
        if (!(currentEntity instanceof TrJournalEvent))
            return;

        TrJournalEvent baseEvent = (TrJournalEvent) currentEntity;
        List<TrJournalEvent> events = getList(TrJournalEvent.class, TrJournalEvent.journalModule(), baseEvent.getJournalModule(), TrJournalEvent.P_NUMBER);
        final int num = events.indexOf(baseEvent);
        if ((!down  && num <= 0) || (down && num >= events.size() - 1))
            return;
        final TrJournalEvent neighbourEvent = events.get(num + (down ? 1 : -1));

        int newNum = neighbourEvent.getNumber();
        int oldNum = baseEvent.getNumber();
        neighbourEvent.setNumber(Integer.MIN_VALUE);
        getSession().flush();
        baseEvent.setNumber(newNum);
        getSession().flush();
        neighbourEvent.setNumber(oldNum);
        getSession().flush();

        new EntityRenumerator<TrJournalEvent>() {
            @Override protected int getNumber(final TrJournalEvent e) { return e.getNumber(); }
            @Override protected void setNumber(final TrJournalEvent e, final int number) { e.setNumber(number); getSession().update(e); getSession().flush(); }
        }.execute(events);
    }

    @Override
    public void saveNewEvent(TrJournalEvent newEvent, TrJournalModule journalModule, TrJournalGroup journalGroup) {

        // todo привести к общему знаменателю с методом ниже
        List<TrJournalEvent> events = getList(TrJournalEvent.class, TrJournalEvent.journalModule(), journalModule, TrJournalEvent.P_NUMBER);
        Collections.reverse(events);
        for (TrJournalEvent event : events)
            if (event.getNumber() >= newEvent.getNumber())
                newEvent.setNumber(event.getNumber() + 1);
        newEvent.setJournalModule(journalModule);
        saveOrUpdate(newEvent);
        getSession().flush();

        if (journalGroup != null) {
            save(new TrEduGroupEvent(journalGroup.getGroup(), newEvent));
        }
    }

    @Override
    public Long copyEvent(Long sourceEventId)
    {
        final IEntity currentEntity = get(sourceEventId);
        if (!(currentEntity instanceof TrJournalEvent))
            return null;

        TrJournalEvent baseEvent = (TrJournalEvent) currentEntity;
        TrJournalEvent newEvent = baseEvent.getClone();

        List<TrJournalEvent> events = getList(TrJournalEvent.class, TrJournalEvent.journalModule(), ((TrJournalEvent) currentEntity).getJournalModule(), TrJournalEvent.P_NUMBER);
        Collections.reverse(events);
        for (TrJournalEvent event : events)
            if (event.getNumber() >= newEvent.getNumber())
            {
                event.setNumber(event.getNumber() + 1);
                getSession().flush();
            }

        events.add(newEvent);
        save(newEvent);
        getSession().flush();

        copyCoefBrs(baseEvent, newEvent);

        renumerate(events);

        return newEvent.getId();
    }

    @Override
    public void renumerate(List<TrJournalEvent> events)
    {
        new EntityRenumerator<TrJournalEvent>() {
            @Override protected int getNumber(final TrJournalEvent e) { return e.getNumber(); }
            @Override protected void setNumber(final TrJournalEvent e, final int number) { e.setNumber(number); update(e); getSession().flush(); }
        }.execute(events);
    }


    @Override
    public void saveOrUpdateScheduleEvent(TrEduGroupEvent groupEvent, ScheduleEvent schEvent, Collection<UniplacesPlace> places)
    {
        saveOrUpdate(schEvent);
        groupEvent.setScheduleEvent(schEvent);

        final List<ScheduleEventPlace> existingList = this.getList(ScheduleEventPlace.class, ScheduleEventPlace.scheduleEvent().s(), schEvent);
        final List<ScheduleEventPlace> localList = new ArrayList<>();
        for (final UniplacesPlace place : places)
            localList.add(new ScheduleEventPlace(place, schEvent));
        new MergeAction.SessionMergeAction<Long, ScheduleEventPlace>() {
            @Override protected Long key(final ScheduleEventPlace source) { return source.getPlace().getId(); }
            @Override protected ScheduleEventPlace buildRow(final ScheduleEventPlace source) { return source; }
            @Override protected void fill(final ScheduleEventPlace target, final ScheduleEventPlace source) {  }
            @Override protected boolean isDeleteable(final ScheduleEventPlace target) { return true; }
        }.merge(existingList, localList);

        update(groupEvent);
    }

    @Override
    public void deleteScheduleEvent(TrEduGroupEvent groupEvent)
    {
        final ScheduleEvent sEvent = groupEvent.getScheduleEvent();
        groupEvent.setScheduleEvent(null);
        getSession().flush();
        if (null != sEvent && null != sEvent.getId())
            delete(sEvent);
    }

    @Override
    public void saveJournalMark(EppStudentWorkPlanElement slot, TrEduGroupEvent event, IJournalMarkData mark)
    {
        TrBrsCoefficientManager.instance().brsDao().validateJournalMark(slot, event, mark);
        Session session = lock("tj."+event.getJournalEvent().getJournalModule().getJournal().getId());
        // todo проверка, что такой event можно создать
        TrEduGroupEventStudent rel = getByNaturalId(new TrEduGroupEventStudent.NaturalId(event, slot));
        if (rel == null)
        {
            rel = new TrEduGroupEventStudent();
            rel.setEvent(event);
            rel.setStudentWpe(slot);
        }
        if (event.getJournalEvent() instanceof TrEventLoad || !event.getJournalEvent().getJournalModule().getJournal().isHideAttendanceForControlEvents()) {
            rel.setAbsent(mark.isAbsent());
        }
        rel.setGrade(mark.getGrade());
        rel.setComment(mark.getComment());
        session.saveOrUpdate(rel);
    }

    @Override
    public void updateReason(Collection<Long> ids, TrAbsenceReason reason)
    {
        if (CollectionUtils.isEmpty(ids))
            return;
        for(TrEduGroupEventStudent student : getList(TrEduGroupEventStudent.class, ids))
        {
            if (student.getAbsent() != null && student.getAbsent())
            {
                student.setAbsenceReason(reason);
                save(student);
            }
        }
    }

    public void copyCoefBrs(TrJournalEvent baseEvent, TrJournalEvent event) {
        Map<TrBrsCoefficientDef, Object> values = new HashMap<>();
        for (TrBrsCoefficientValue value : DataAccessServices.dao().getList(TrBrsCoefficientValue.class, TrBrsCoefficientValue.owner().s(), baseEvent))
            values.put(value.getDefinition(), value.getValue());
        EntityHolder<IBrsSettingsOwner> owner = new EntityHolder<>();
        owner.setValue(event);
        TrBrsCoefficientManager.instance().coefDao().doSaveCoefficients(owner.getValue(), values);
    }

    @Override
    public void deleteAdditionalEvent(Long eventId) throws IllegalArgumentException
    {
        TrEduGroupEvent groupEvent = getNotNull(TrEduGroupEvent.class, eventId);

        if (!(groupEvent.getJournalEvent() instanceof TrEventAddon))
            throw new IllegalArgumentException();

        try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler())) {
            delete(groupEvent);
            delete(groupEvent.getJournalEvent());
        }

    }
}
