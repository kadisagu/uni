/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.ScheduleTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.ScheduleData.TrJournalScheduleDataUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * @author oleyba
 * @since 9/15/11
 */
@Output({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journal.id"),
    @Bind(key = TrJournalScheduleDataUI.VIEW_MODE_BIND, binding = "viewMode")
})

public class TrOrgUnitScheduleTabUI extends OrgUnitUIPresenter
{
    private TrJournal journal;
    private boolean viewMode;

    @Override
    public void onComponentRefresh()
    {
        setViewMode(!CoreServices.securityService().check(getOrgUnit(), ContextLocal.getUserContext().getPrincipalContext(), getSec().getPermission("editTrScheduleTab")));
        setJournal(_uiSettings.<TrJournal>get("journal"));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(TrOrgUnitScheduleTab.TR_ORGUNIT_SCHEDULE_TAB_ORG_UNIT_KEY, this.getOrgUnit());
        dataSource.put(TrOrgUnitScheduleTab.TR_ORGUNIT_SCHEDULE_TAB_DISC_KEY, _uiSettings.get("discipline"));
    }

    @Override
    public void saveSettings()
    {
        super.saveSettings();
        setJournal(_uiSettings.<TrJournal>get("journal"));
        final IUIPresenter journalScheduleView = _uiSupport.getChildUI(RenderComponent.getRegionName("TrJournalScheduleData"));
        if (null != journalScheduleView)
            journalScheduleView.deactivate();
    }

    public boolean isSelected()
    {
        return getJournal() != null;
    }

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }

    public boolean isViewMode()
    {
        return viewMode;
    }

    public void setViewMode(boolean viewMode)
    {
        this.viewMode = viewMode;
    }
}
