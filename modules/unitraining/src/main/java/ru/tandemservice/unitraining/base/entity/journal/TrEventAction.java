package ru.tandemservice.unitraining.base.entity.journal;

import org.tandemframework.core.view.formatter.StringLimitFormatter;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateMutable;
import ru.tandemservice.uniepp.base.bo.EppState.EppStatePath;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.unitraining.base.entity.journal.gen.TrEventActionGen;

/**
 * Планируемое событие журнала (текущий контроль)
 */
@EppStatePath(TrJournalEvent.L_JOURNAL_MODULE+"."+TrJournalModule.L_JOURNAL)
@EppStateMutable({ TrJournalEvent.P_THEME, TrJournalEvent.L_TYPE, TrJournalEvent.P_COMMENT, TrJournalEvent.P_HAS_MARKS })
public class TrEventAction extends TrEventActionGen
{
    public TrEventAction() {}

    public TrEventAction(EppGroupType type, EppIControlActionType actionType) {
        setType(type);
        setActionType(actionType);
    }

    public TrEventAction(TrJournalModule journalModule, int number, EppGroupType type, EppIControlActionType actionType) {
        setJournalModule(journalModule);
        setNumber(number);
        setType(type);
        setActionType(actionType);
    }

    @Override
    public TrJournalEvent getClone()
    {
        TrEventAction newEvent = new TrEventAction();
        newEvent.setJournalModule(getJournalModule());
        newEvent.setType(getType());
        newEvent.setActionType(getActionType());
        newEvent.setNumber(this.getNumber() + 1);
        return newEvent;
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2719821")
    public String getTitle()
    {
        if (getType() == null) {
            return this.getClass().getSimpleName();
        }
        String title = getNumber() + ". (" + getType().getAbbreviation() + ") " + getActionType().getTitle();
        if (null != getTheme())
            title = title + " (" + StringLimitFormatter.NON_BROKEN_WORDS.format(getTheme()) + ")";
        return title;
    }

    @Override
    public String getDisplayableTitle()
    {
        return getTitle();
    }

    @Override
    public String getTypeTitle()
    {
        return "Контрольное мероприятие";
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2719821")
    public String getShortTitle()
    {
        return getNumber() + ". " + getActionType().getTitle();
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2719821")
    public String getAbbreviationTitle()
    {
        return getActionType().getAbbreviation() + getNumber();
    }
}