package ru.tandemservice.unitraining.base.bo.TrStudent.ui.AttendanceTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unitraining.base.bo.TrStudent.logic.TrStudentAttendanceHandler;

/**
 * @author avedernikov
 * @since 30.06.2016
 */
@Configuration
public class TrStudentAttendanceTab extends BusinessComponentManager
{
	public static final String ATTENDANCE_DS = "attendanceDS";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
				.addDataSource(simpleSearchListDS(ATTENDANCE_DS, attendanceDSColumn(), attendanceDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint attendanceDSColumn()
	{
		return columnListExtPointBuilder(ATTENDANCE_DS)
				.addColumn(textColumn(EppStudentWorkPlanElement.L_TERM, Term.intValue()))
				.addColumn(textColumn(EppStudentWorkPlanElement.L_PART, TrStudentAttendanceHandler.PROP_PART_TITLE))
				.addColumn(textColumn(TrStudentAttendanceHandler.PROP_ABSENCE_MESSAGE, TrStudentAttendanceHandler.PROP_ABSENCE_MESSAGE))
				.create();
	}


	@Bean
	public IBusinessHandler<DSInput, DSOutput> attendanceDSHandler()
	{
		return new TrStudentAttendanceHandler(getName());
	}
}