package ru.tandemservice.unitraining.base.bo.TrJournal.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;

/**
 * @author Codegen
 */
@Configuration        
public class TrJournalPub extends BusinessComponentManager
{
    public static final String TR_JOURNAL_ACTIONS_BL = "trJournalActionsBL";
    public static final String TR_JOURNAL_MAIN_IL = "trJournalMainIL";

    @Bean
    public ButtonListExtPoint actionsButtonList()
    {
        return buttonListExtPointBuilder(TR_JOURNAL_ACTIONS_BL)
                .addButton(submitButton("edit", "onEditEntity").disabled("ui:readonly").permissionKey("editTrJournal").create())
                .addButton(submitButton("copy", "onClickCopy").disabled("mvel:!presenter.trJournal.state.formative").permissionKey("copyTrJournal").create())
                .addButton(submitButton("genModules", "onClickModuleGen").disabled("ui:readonly").permissionKey("editTrJournal").alert("message:" + TR_JOURNAL_ACTIONS_BL + ".genModulesAlert").create())
                .addButton(submitButton("delete", "deleteEntity")
                        .disabled("ui:readonly").permissionKey("deleteTrJournal")
                        .alert("ui:deleteAlert")
                        .create())
                .create();
    }
}
