/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.EditBase.TrBrsCoefficientEditBaseUI;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;

/**
 * @author oleyba
 * @since 8/20/12
 */
@Input({
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_ID, binding = "ownerHolder.id", required = true),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType.id", required = true)
})
@Output({
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_HOLDER, binding = "ownerHolder", required = true),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType", required = true)
})
public class TrBrsCoefficientEditUI extends UIPresenter
{
    private EntityHolder<IBrsSettingsOwner> ownerHolder = new EntityHolder<IBrsSettingsOwner>();
    private TrBrsCoefficientOwnerType ownerType = new TrBrsCoefficientOwnerType();

    @Override
    public void onComponentRefresh()
    {
        getOwnerHolder().refresh();
        setOwnerType(DataAccessServices.dao().get(TrBrsCoefficientOwnerType.class, getOwnerType().getId()));
    }

    public void onClickApply()
    {
        ((TrBrsCoefficientEditBaseUI) _uiSupport.getChildrenUI().values().iterator().next()).save();
        deactivate();
    }

    public EntityHolder<IBrsSettingsOwner> getOwnerHolder()
    {
        return ownerHolder;
    }

    public IBrsSettingsOwner getOwner()
    {
        return getOwnerHolder().getValue();
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(TrBrsCoefficientOwnerType ownerType)
    {
        this.ownerType = ownerType;
    }
}
