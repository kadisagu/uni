/* $Id$ */
package ru.tandemservice.unitraining.sec;

import com.google.common.collect.ImmutableMap;
import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.uniepp.sec.UnieppStateChangePermissionModifier;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

import java.util.Map;

/**
 * @author oleyba
 * @since 8/22/11
 */
public class UnitrainingStateChangePermissionModifier extends UnieppStateChangePermissionModifier
{
    private static final Map<Class, CoreCollectionUtils.Pair<String, String>> classPermissionMap = ImmutableMap.<Class, CoreCollectionUtils.Pair<String, String>>of(
    TrJournal.class, new CoreCollectionUtils.Pair<>("trJournal", "Объект «Реализация дисциплины»"))
    ;

    @Override
    protected String getModuleName()
    {
        return "unitraining";
    }

    @Override
    protected String getConfigName()
    {
        return "unitraining-stateChange-config";
    }

    @Override
    protected Map<Class, CoreCollectionUtils.Pair<String, String>> getClassPermissionMap()
    {
        return classPermissionMap;
    }
}

