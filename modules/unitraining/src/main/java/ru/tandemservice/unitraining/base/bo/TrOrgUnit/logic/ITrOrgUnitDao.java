package ru.tandemservice.unitraining.base.bo.TrOrgUnit.logic;

import java.util.Collection;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.GroupsJournalGen.GroupsJournalGenOption;

/**
 * @author vdanilov
 */
public interface ITrOrgUnitDao extends INeedPersistenceSupport {

    /** проверяет видимость складки на подразделении */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    boolean isTrOrgUnitTabVisible(Long orgUnitId);

    /** формирует перечень читаемых дисциплин на подразделении (читающее подразделение) на указанный год на основе данных РУП */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    int doGenerateJournals(OrgUnit orgUnit, EppYearPart yearPart, Boolean controlOnlyMode);

    /** автоматически линкует УГС к журналам, если найден единственный журнал по дисциплине */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doAutoLinkEduGroups(OrgUnit orgUnit, EppYearPart yearPart);

    /**
     * Формирование реализаций для УГС.
     * @param option способ формирования
     * @param groupIdList список id выбранных групп
     */
    void doGroupsJournalGen(GroupsJournalGenOption option, Collection<Long> groupIdList, Boolean controlOnlyMode);
}
