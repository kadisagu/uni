/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Katkov
 * @since 12.02.2016
 */
public class EducationLevelHighSchoolDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public EducationLevelHighSchoolDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "eou")
                .predicate(DQLPredicateType.distinct)
                .column(property("eou", EducationOrgUnit.educationLevelHighSchool().id()));

        OrgUnit formativeOrgUnit = context.get(TrJournalListDSHandler.PARAM_FORMATIVE_ORG_UNIT);
        if (formativeOrgUnit != null)
            subBuilder.where(eq(property("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT), value(formativeOrgUnit)));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, "e")
                .column("e")
                .where(in("e.id", subBuilder.buildQuery()));

        final Set primaryKeys = input.getPrimaryKeys();
        if (primaryKeys != null) {
            if (primaryKeys.size() == 1)
                builder.where(eq(property("e.id"), commonValue(primaryKeys.iterator().next(), PropertyType.LONG)));
            else
                builder.where(in("e.id", primaryKeys));
        }

        if (StringUtils.isNotEmpty(input.getComboFilterByValue()))
            builder.where(likeUpper(property("e", EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY), value(CoreStringUtils.escapeLike(input.getComboFilterByValue(), true))));

        builder.order(property("e", EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }
}