package ru.tandemservice.unitraining.brs.entity.brs.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройки оценивания на подразделении на часть года
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrOrgUnitSettingsGen extends EntityBase
 implements IBrsSettingsOwner, INaturalIdentifiable<TrOrgUnitSettingsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings";
    public static final String ENTITY_NAME = "trOrgUnitSettings";
    public static final int VERSION_HASH = -686861058;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_EDU_YEAR_PART = "eduYearPart";
    public static final String P_USE_CURRENT_RATING_IN_SESSION = "useCurrentRatingInSession";
    public static final String P_USE_CURRENT_RATING_IN_ATTESTATION = "useCurrentRatingInAttestation";
    public static final String P_USER_SCRIPT = "userScript";

    private OrgUnit _orgUnit;     // Подразделение
    private EppYearPart _eduYearPart;     // Часть учебного года
    private boolean _useCurrentRatingInSession;     // Использовать рейтинг при выставлении оценки в сессии
    private boolean _useCurrentRatingInAttestation;     // Использовать рейтинг при выставлении аттестации
    private String _userScript;     // Скрипт расчета рейтинга

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     */
    @NotNull
    public EppYearPart getEduYearPart()
    {
        return _eduYearPart;
    }

    /**
     * @param eduYearPart Часть учебного года. Свойство не может быть null.
     */
    public void setEduYearPart(EppYearPart eduYearPart)
    {
        dirty(_eduYearPart, eduYearPart);
        _eduYearPart = eduYearPart;
    }

    /**
     * @return Использовать рейтинг при выставлении оценки в сессии. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseCurrentRatingInSession()
    {
        return _useCurrentRatingInSession;
    }

    /**
     * @param useCurrentRatingInSession Использовать рейтинг при выставлении оценки в сессии. Свойство не может быть null.
     */
    public void setUseCurrentRatingInSession(boolean useCurrentRatingInSession)
    {
        dirty(_useCurrentRatingInSession, useCurrentRatingInSession);
        _useCurrentRatingInSession = useCurrentRatingInSession;
    }

    /**
     * @return Использовать рейтинг при выставлении аттестации. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseCurrentRatingInAttestation()
    {
        return _useCurrentRatingInAttestation;
    }

    /**
     * @param useCurrentRatingInAttestation Использовать рейтинг при выставлении аттестации. Свойство не может быть null.
     */
    public void setUseCurrentRatingInAttestation(boolean useCurrentRatingInAttestation)
    {
        dirty(_useCurrentRatingInAttestation, useCurrentRatingInAttestation);
        _useCurrentRatingInAttestation = useCurrentRatingInAttestation;
    }

    /**
     * @return Скрипт расчета рейтинга.
     */
    public String getUserScript()
    {
        return _userScript;
    }

    /**
     * @param userScript Скрипт расчета рейтинга.
     */
    public void setUserScript(String userScript)
    {
        dirty(_userScript, userScript);
        _userScript = userScript;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TrOrgUnitSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setOrgUnit(((TrOrgUnitSettings)another).getOrgUnit());
                setEduYearPart(((TrOrgUnitSettings)another).getEduYearPart());
            }
            setUseCurrentRatingInSession(((TrOrgUnitSettings)another).isUseCurrentRatingInSession());
            setUseCurrentRatingInAttestation(((TrOrgUnitSettings)another).isUseCurrentRatingInAttestation());
            setUserScript(((TrOrgUnitSettings)another).getUserScript());
        }
    }

    public INaturalId<TrOrgUnitSettingsGen> getNaturalId()
    {
        return new NaturalId(getOrgUnit(), getEduYearPart());
    }

    public static class NaturalId extends NaturalIdBase<TrOrgUnitSettingsGen>
    {
        private static final String PROXY_NAME = "TrOrgUnitSettingsNaturalProxy";

        private Long _orgUnit;
        private Long _eduYearPart;

        public NaturalId()
        {}

        public NaturalId(OrgUnit orgUnit, EppYearPart eduYearPart)
        {
            _orgUnit = ((IEntity) orgUnit).getId();
            _eduYearPart = ((IEntity) eduYearPart).getId();
        }

        public Long getOrgUnit()
        {
            return _orgUnit;
        }

        public void setOrgUnit(Long orgUnit)
        {
            _orgUnit = orgUnit;
        }

        public Long getEduYearPart()
        {
            return _eduYearPart;
        }

        public void setEduYearPart(Long eduYearPart)
        {
            _eduYearPart = eduYearPart;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TrOrgUnitSettingsGen.NaturalId) ) return false;

            TrOrgUnitSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getOrgUnit(), that.getOrgUnit()) ) return false;
            if( !equals(getEduYearPart(), that.getEduYearPart()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOrgUnit());
            result = hashCode(result, getEduYearPart());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOrgUnit());
            sb.append("/");
            sb.append(getEduYearPart());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrOrgUnitSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrOrgUnitSettings.class;
        }

        public T newInstance()
        {
            return (T) new TrOrgUnitSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "eduYearPart":
                    return obj.getEduYearPart();
                case "useCurrentRatingInSession":
                    return obj.isUseCurrentRatingInSession();
                case "useCurrentRatingInAttestation":
                    return obj.isUseCurrentRatingInAttestation();
                case "userScript":
                    return obj.getUserScript();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "eduYearPart":
                    obj.setEduYearPart((EppYearPart) value);
                    return;
                case "useCurrentRatingInSession":
                    obj.setUseCurrentRatingInSession((Boolean) value);
                    return;
                case "useCurrentRatingInAttestation":
                    obj.setUseCurrentRatingInAttestation((Boolean) value);
                    return;
                case "userScript":
                    obj.setUserScript((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "eduYearPart":
                        return true;
                case "useCurrentRatingInSession":
                        return true;
                case "useCurrentRatingInAttestation":
                        return true;
                case "userScript":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "eduYearPart":
                    return true;
                case "useCurrentRatingInSession":
                    return true;
                case "useCurrentRatingInAttestation":
                    return true;
                case "userScript":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "eduYearPart":
                    return EppYearPart.class;
                case "useCurrentRatingInSession":
                    return Boolean.class;
                case "useCurrentRatingInAttestation":
                    return Boolean.class;
                case "userScript":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrOrgUnitSettings> _dslPath = new Path<TrOrgUnitSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrOrgUnitSettings");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings#getEduYearPart()
     */
    public static EppYearPart.Path<EppYearPart> eduYearPart()
    {
        return _dslPath.eduYearPart();
    }

    /**
     * @return Использовать рейтинг при выставлении оценки в сессии. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings#isUseCurrentRatingInSession()
     */
    public static PropertyPath<Boolean> useCurrentRatingInSession()
    {
        return _dslPath.useCurrentRatingInSession();
    }

    /**
     * @return Использовать рейтинг при выставлении аттестации. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings#isUseCurrentRatingInAttestation()
     */
    public static PropertyPath<Boolean> useCurrentRatingInAttestation()
    {
        return _dslPath.useCurrentRatingInAttestation();
    }

    /**
     * @return Скрипт расчета рейтинга.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings#getUserScript()
     */
    public static PropertyPath<String> userScript()
    {
        return _dslPath.userScript();
    }

    public static class Path<E extends TrOrgUnitSettings> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private EppYearPart.Path<EppYearPart> _eduYearPart;
        private PropertyPath<Boolean> _useCurrentRatingInSession;
        private PropertyPath<Boolean> _useCurrentRatingInAttestation;
        private PropertyPath<String> _userScript;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings#getEduYearPart()
     */
        public EppYearPart.Path<EppYearPart> eduYearPart()
        {
            if(_eduYearPart == null )
                _eduYearPart = new EppYearPart.Path<EppYearPart>(L_EDU_YEAR_PART, this);
            return _eduYearPart;
        }

    /**
     * @return Использовать рейтинг при выставлении оценки в сессии. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings#isUseCurrentRatingInSession()
     */
        public PropertyPath<Boolean> useCurrentRatingInSession()
        {
            if(_useCurrentRatingInSession == null )
                _useCurrentRatingInSession = new PropertyPath<Boolean>(TrOrgUnitSettingsGen.P_USE_CURRENT_RATING_IN_SESSION, this);
            return _useCurrentRatingInSession;
        }

    /**
     * @return Использовать рейтинг при выставлении аттестации. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings#isUseCurrentRatingInAttestation()
     */
        public PropertyPath<Boolean> useCurrentRatingInAttestation()
        {
            if(_useCurrentRatingInAttestation == null )
                _useCurrentRatingInAttestation = new PropertyPath<Boolean>(TrOrgUnitSettingsGen.P_USE_CURRENT_RATING_IN_ATTESTATION, this);
            return _useCurrentRatingInAttestation;
        }

    /**
     * @return Скрипт расчета рейтинга.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings#getUserScript()
     */
        public PropertyPath<String> userScript()
        {
            if(_userScript == null )
                _userScript = new PropertyPath<String>(TrOrgUnitSettingsGen.P_USER_SCRIPT, this);
            return _userScript;
        }

        public Class getEntityClass()
        {
            return TrOrgUnitSettings.class;
        }

        public String getEntityName()
        {
            return "trOrgUnitSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
