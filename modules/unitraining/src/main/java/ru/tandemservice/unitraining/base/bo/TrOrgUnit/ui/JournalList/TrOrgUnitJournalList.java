package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.logic.TrOrgUnitJournalDSHandler;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * @author vdanilov
 */
@Configuration
public class TrOrgUnitJournalList extends BusinessComponentManager {

    public static final String TR_ORGUNIT_JOURNAL_DS = "trOrgUnitJournalDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(this.searchListDS(TrOrgUnitJournalList.TR_ORGUNIT_JOURNAL_DS, this.trOrgUnitJournalDSColumns(), this.trOrgUnitJournalDSHandler()))
        .addDataSource(TrOrgUnitManager.instance().trOrgUnitYearPartDSConfig())
        .addDataSource(TrOrgUnitManager.instance().trOrgUnitregistryElementPartDSConfig())
        .addDataSource(EppStateManager.instance().eppStateDSConfig())
        .addDataSource(TrOrgUnitManager.instance().trOrgUnitPpsDSConfig())
        .create();
    }

    @Bean
    public ColumnListExtPoint trOrgUnitJournalDSColumns() {
        return this.columnListExtPointBuilder(TrOrgUnitJournalList.TR_ORGUNIT_JOURNAL_DS)
        .addColumn(textColumn(TrJournal.P_NUMBER, TrJournal.number()).order().width("1").create())
        .addColumn(publisherColumn(TrJournal.L_REGISTRY_ELEMENT_PART, TrJournal.registryElementPart().titleWithNumber()).order().create())
        .addColumn(textColumn("durationPeriod", TrJournal.durationPeriodString()).order().width("1").create())
        .addColumn(textColumn("pps", TrOrgUnitJournalDSHandler.COLUMN_PPS).formatter(UniEppUtils.NEW_LINE_FORMATTER).create())
        .addColumn(textColumn(TrJournal.L_RESPONSIBLE, TrJournal.responsible().shortTitle()).create())
        .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("ui:secModel.editTrJournal").create())
        .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                .permissionKey("ui:secModel.deleteTrJournal")
                .disabled(TrJournal.state().s() + ".readOnlyState")
                .alert(FormattedMessage.with().template("trOrgUnitJournalDS.delete.alert").parameter(TrOrgUnitJournalDSHandler.COLUMN_HAS_MARKS_ALERT).create()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trOrgUnitJournalDSHandler() {
        return new TrOrgUnitJournalDSHandler(this.getName());
    }

}
