/* $Id:$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage.logic;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 3/12/13
 */
public class TrHomePageDao extends UniBaseDao implements ITrHomePageDao {
    
    @Override
    public boolean checkPpsAccessForViewing(IPrincipalContext principalContext, TrJournal journal) {
        Person person = PersonManager.instance().dao().getPerson(principalContext);
        
        DQLSelectBuilder checkPps = new DQLSelectBuilder()
            .fromEntity(TrJournalGroup.class, "jg")
            .where(eq(property(TrJournalGroup.journal().fromAlias("jg")), value(journal.getId())))
            .joinPath(DQLJoinType.inner, TrJournalGroup.group().fromAlias("jg"), "g")
            .joinEntity("g", DQLJoinType.inner, EppPpsCollectionItem.class, "pps", eq(property(EppPpsCollectionItem.list().fromAlias("pps")), property("g")))
            .where(eq(property(EppPpsCollectionItem.pps().person().fromAlias("pps")), value(person)))
            .column(property(TrJournalGroup.id().fromAlias("jg")));
        
        Number journalCount = checkPps.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        
        return !(journalCount == null || journalCount.intValue() == 0);
    }

    @Override
    public boolean checkResponsibleAccessForViewing(IPrincipalContext principalContext, TrJournal journal) {
        Person person = PersonManager.instance().dao().getPerson(principalContext);
        return journal.getResponsible() != null && journal.getResponsible().getPerson().equals(person);
    }

    @Override
    public boolean hasJournalsForMarking(IPrincipalContext principalContext) {
        Person person = PersonManager.instance().dao().getPerson(principalContext);

        DQLSelectBuilder journals = new DQLSelectBuilder()
            .fromEntity(TrJournal.class, "j")
            .where(eq(property(TrJournal.state().code().fromAlias("j")), value(EppState.STATE_ACCEPTED)))
            .joinEntity("j", DQLJoinType.inner, TrJournalGroup.class, "jg", eq(property(TrJournalGroup.journal().fromAlias("jg")), property("j")))
            .joinPath(DQLJoinType.inner, TrJournalGroup.group().fromAlias("jg"), "g")
            .joinEntity("g", DQLJoinType.inner, EppPpsCollectionItem.class, "pps", eq(property(EppPpsCollectionItem.list().fromAlias("pps")), property("g")))
            .where(eq(property(EppPpsCollectionItem.pps().person().fromAlias("pps")), value(person)))
            .column(property(TrJournal.id().fromAlias("j")));

        Number journalCount = journals.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();

        return journalCount != null && journalCount.intValue() > 0;
    }

    @Override
    public String checkPpsAccessForMarking(IPrincipalContext principalContext, TrJournalGroup group) {

        Person person = PersonManager.instance().dao().getPerson(principalContext);

        DQLSelectBuilder checkPps = new DQLSelectBuilder()
            .fromEntity(TrJournalGroup.class, "jg").where(eq(property("jg.id"), value(group.getId())))
            .joinPath(DQLJoinType.inner, TrJournalGroup.group().fromAlias("jg"), "g")
            .joinEntity("g", DQLJoinType.inner, EppPpsCollectionItem.class, "pps", eq(property(EppPpsCollectionItem.list().fromAlias("pps")), property("g")))
            .where(eq(property(EppPpsCollectionItem.pps().person().fromAlias("pps")), value(person)))
            .column(property(TrJournalGroup.id().fromAlias("jg")));
        Number journalCount = checkPps.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        if (journalCount == null || journalCount.intValue() == 0) {
            return "Вы не можете работать с журналом, так как не отмечены в нем в качестве преподавателя.";
        }
        DQLSelectBuilder checkState = new DQLSelectBuilder()
            .fromEntity(TrJournalGroup.class, "jg").where(eq(property("jg.id"), value(group.getId())))
            .where(eq(property(TrJournalGroup.journal().state().code().fromAlias("jg")), value(EppState.STATE_ACCEPTED)))
            ;
        journalCount = checkState.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        if (journalCount == null || journalCount.intValue() == 0) {
            return "Вы не можете работать с журналом, так как реализация дисциплины не в состоянии «Согласована».";
        }

        return null;
    }

    @Override
    public long getStructureCount(Person person, List<String> allowStates)
    {
        Number structureCount = new DQLSelectBuilder()
                .fromEntity(TrJournal.class, "j")
                .column(DQLFunctions.countStar())
                .where(eq(property(TrJournal.responsible().person().fromAlias("j")), value(person)))
                .where(in(property(TrJournal.state().code().fromAlias("j")), allowStates))
                .createStatement(getSession()).uniqueResult();

        return structureCount != null ? structureCount.longValue() : 0L;
    }
}
