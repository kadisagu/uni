package ru.tandemservice.unitraining.base.bo.TrJournal.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface ITrJournalDaemonBean {

    final SpringBeanCache<ITrJournalDaemonBean> instance = new SpringBeanCache<ITrJournalDaemonBean>(ITrJournalDaemonBean.class.getName());
    final String LOCK_NAME = ITrJournalDaemonBean.class.getName()+".lock";

    /**
     * Обновляет перечень модулей журнала (на основе набора модулей части дисциплины)
     * @return true, если что-то изменилось
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    boolean doUpdateJournalModuleSet();

    /**
     * Обновляет перечень событий модулей журнала (на основе нагрузки по модулям из реестра)
     * @return true, если что-то изменилось
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    boolean doUpdateJournalModuleEvents();

    /**
     * Обновляет перечень событий групп журнала (на основе событий журнала)
     * @return true, если что-то изменилось
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    boolean doUpdateJournalGroupEvents();

    /**
     * Обновляет список студентов в событиях на текущую дату
     * @return true, если что-то изменилось
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    boolean doUpdateJournalGroupEventStudents();

    /**
     * Проверяет соответствие списка студентов в событиях их текущим группам
     * @return true, если что-то изменилось
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    boolean doCheckGroupEventStudents();

    /**
     * Исправляет отметки о неявке - ставит "н" везде, где указана причина
     * @return true, если что-то изменилось
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    boolean doCorrectAbsence();

    /**
     * Заполняет поле "есть оценки" в событиях журналов
     * @return true, если что-то изменилось
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    boolean doUpdateHasMarksFieldForJournalEvents();

    /**
     * Количество дней до начала события в расписании, за которое создаются события студентов в журнале.
     */
    int daysBeforeEventWhenCreateStudentEvents();
}
