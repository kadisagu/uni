package ru.tandemservice.unitraining.base.bo.TrJournal;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.transformer.BaseOutputTransformer;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.BaseRegistryElementPartDSHandler;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.*;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
@Configuration
public class TrJournalManager extends BusinessObjectManager
{
    public static final String TR_JOURNAL_JOURNAL_KEY = "trJournal";
    public static final String TR_JOURNAL_EDU_GROUP_DS_DISC_KEY = "discipline";
    public static final String TR_JOURNAL_EDU_GROUP_DS_YP_KEY = "yearPart";
    public static final String TR_JOURNAL_EDU_GROUP_UNLINKED_DS = "trJournalEduGroupUnlinkedDS";
    public static final String TR_JOURNAL_EDU_GROUP_DS = "trJournalEduGroupDS";
    public static final String TR_JOURNAL_STUDENT_DS = "trJournalStudentDS";
    public static final String TR_JOURNAL_GROUP_TYPE_DS = "groupTypeDS";
    public static final String TR_PPS_DS = "trPpsDS";
    public static final String TR_OWNER_DS = "trOwnerDS";
    public static final String TR_OWNER_KEY = "trOwnerDS_orgUnit";
    public static final String TR_REGISTRY_ELEMENT_PART_DS = "trRegistryElementPartDS";
    public static final String TR_RESPONSIBLE_DS = "trResponsibleDS";
    public static final String TR_RESPONSIBLE_KEY = "trResponsibleElementPartDS_orgUnit";
    public static final String TR_PPS_ORGUNIT_KEY = "trPpsDS_orgUnit";
    public static final String TR_GROUP_WITH_PPS_DS = "groupListDS";
    public static final String TR_GROUP_DS_PPS_LIST_PROPERTY = "ppsList";

    public static TrJournalManager instance()
    {
        return instance(TrJournalManager.class);
    }

    @Bean
    public ITrJournalDao dao()
    {
        return new TrJournalDao();
    }

    @Bean
    public ITrJournalEventDao eventDao()
    {
        return new TrJournalEventDao();
    }

    @Bean
    public ITrJournalPrintDao printDao()
    {
        return new TrJournalPrintDao();
    }

    @Bean
    public UIDataSourceConfig trJournalEduGroupUnlinkedDSConfig()
    {
        return SelectDSConfig.with(TrJournalManager.TR_JOURNAL_EDU_GROUP_UNLINKED_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn("title", EppRealEduGroup4LoadType.title().s())
                .addColumn("loadType", EppRealEduGroup4LoadType.type().title().s())
                .handler(this.trJournalEduGroupUnlinkedDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trJournalEduGroupUnlinkedDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppRealEduGroup.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.joinEntity(alias, DQLJoinType.left, TrJournalGroup.class, "rel", eq(property(TrJournalGroup.group().fromAlias("rel")), property(alias)));
                dql.where(isNull("rel.id"));
            }
        }
                .where(EppRealEduGroup.activityPart(), TrJournalManager.TR_JOURNAL_EDU_GROUP_DS_DISC_KEY)
                .where(EppRealEduGroup.summary().yearPart(), TrJournalManager.TR_JOURNAL_EDU_GROUP_DS_YP_KEY)
                .order(EppRealEduGroup.title())
                .filter(EppRealEduGroup.title());
    }

    @Bean
    public UIDataSourceConfig trJournalEduGroupDSConfig()
    {
        return SelectDSConfig.with(TrJournalManager.TR_JOURNAL_EDU_GROUP_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn("title", TrJournalGroup.group().title().s())
                .addColumn("loadType", TrJournalGroup.group().type().title().s())
                .handler(this.trJournalEduGroupDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trJournalEduGroupDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), TrJournalGroup.class)
                .where(TrJournalGroup.journal(), TR_JOURNAL_JOURNAL_KEY)
                .order(TrJournalGroup.group().title())
                .filter(TrJournalGroup.group().title());
    }

    @Bean
    public UIDataSourceConfig trJournalStudentDSConfig()
    {
        return SelectDSConfig.with(TrJournalManager.TR_JOURNAL_STUDENT_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn("title", Student.person().identityCard().fullFio().s())
                .handler(this.trJournalStudentDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trJournalStudentDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), Student.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                final DQLSelectBuilder studentDQL = new DQLSelectBuilder()
                        .fromEntity(TrEduGroupEventStudent.class, "rel").column("rel.id")
                        .where(eq(property(TrEduGroupEventStudent.studentWpe().student().fromAlias("rel")), property(alias)))
                        .where(eq(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("rel")), commonValue(context.<Object>get(TR_JOURNAL_JOURNAL_KEY))));
                dql.where(exists(studentDQL.buildQuery()));
            }
        }
                .order(Student.person().identityCard().fullFio())
                .filter(Student.person().identityCard().fullFio());
    }

    @Bean
    public UIDataSourceConfig eppGroupTypeDSConfig()
    {
        return SelectDSConfig.with(TR_JOURNAL_GROUP_TYPE_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(eppGroupTypeDSHandler())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eppGroupTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppGroupType.class)
                .order(EppGroupType.title())
                .filter(EppGroupType.title());
    }

    @Bean
    public UIDataSourceConfig trPpsDSConfig()
    {
        return SelectDSConfig.with(TrJournalManager.TR_PPS_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn(PpsEntry.person().fio().s())
                .addColumn(PpsEntry.orgUnit().shortTitle().s())
                .addColumn(PpsEntry.titleInfo().s())
                .handler(this.trPpsDSHandler())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> trPpsDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), PpsEntry.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                DQLSelectBuilder dqlInnerBuilder = new DQLSelectBuilder()
                        .fromEntity(TrJournal.class, "j")
                        .joinEntity("j", DQLJoinType.inner, TrJournalGroup.class, "j2g", eq(property(TrJournalGroup.journal().id().fromAlias("j2g")), property(TrJournal.id().fromAlias("j"))))
                        .fromEntity(EppPpsCollectionItem.class, "pps2g").where(eq(property(EppPpsCollectionItem.list().fromAlias("pps2g")), property(TrJournalGroup.group().fromAlias("j2g"))))
                        .column(property(EppPpsCollectionItem.pps().fromAlias("pps2g")));
                dql.where(in(property(PpsEntry.id().fromAlias(alias)), dqlInnerBuilder.buildQuery()));
            }
        }
                .where(PpsEntry.orgUnit(), TR_PPS_ORGUNIT_KEY)
                .where(PpsEntry.removalDate(), (Object) null)
                .order(PpsEntry.orgUnit().title())
                .order(PpsEntry.person().identityCard().fullFio())
                .filter(PpsEntry.person().identityCard().fullFio())
                .filter(PpsEntry.orgUnit().title());
    }

    @Bean
    public UIDataSourceConfig trOwnerDSConfig()
    {
        return SelectDSConfig.with(TrJournalManager.TR_OWNER_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(this.trOwnerDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler trOwnerDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                        new DQLSelectBuilder()
                                .fromEntity(EppTutorOrgUnit.class, "rel")
                                .where(eq(
                                        property(alias, OrgUnit.id()),
                                        property("rel", EppTutorOrgUnit.orgUnit().id())
                                ))
                                .fromEntity(TrJournal.class, "j2u")
                                .where(eq(
                                        property(alias, OrgUnit.id()),
                                        property("j2u", TrJournal.registryElementPart().registryElement().owner().id()))
                                )
                                .buildQuery()));
            }
        }
                .order(OrgUnit.title())
                .filter(OrgUnit.title());
    }

    @Bean
    public UIDataSourceConfig trRegistryElementPartDSConfig()
    {
        return SelectDSConfig.with(TrJournalManager.TR_REGISTRY_ELEMENT_PART_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(this.trRegistryElementPartDSHandler())
                .addColumn(EppRegistryElementPart.P_TITLE_WITH_NUMBER)
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trRegistryElementPartDSHandler()
    {
        return new BaseRegistryElementPartDSHandler(getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                final OrgUnit ownerKey = context.get(TR_OWNER_KEY);
                if (ownerKey != null) {
                    FilterUtils.applySelectFilter(dql, alias, EppRegistryElementPart.registryElement().owner(), ownerKey);
                }
                dql.where(exists(
                        new DQLSelectBuilder()
                                .fromEntity(TrJournal.class, "j2d")
                                .where(eq(property("j2d", TrJournal.registryElementPart()), property(alias)))
                                .buildQuery()));
            }
        };
    }

    @Bean
    public UIDataSourceConfig trResponsibleDSConfig()
    {
        return SelectDSConfig.with(TrJournalManager.TR_RESPONSIBLE_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn(PpsEntry.person().fio().s())
                .addColumn(PpsEntry.orgUnit().shortTitle().s())
                .addColumn(PpsEntry.titleInfo().s())
                .handler(this.trResponsibleDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trResponsibleDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), PpsEntry.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                        new DQLSelectBuilder()
                                .fromEntity(TrJournal.class, "j2u")
                                .where(eq(
                                        property(alias, PpsEntry.id()),
                                        property("j2u", TrJournal.responsible()))
                                )
                                .buildQuery()));
            }
        }
                .where(PpsEntry.orgUnit(), TR_RESPONSIBLE_KEY)
                .where(PpsEntry.removalDate(), (Object) null)
                .order(PpsEntry.orgUnit().title())
                .order(PpsEntry.person().identityCard().fullFio())
                .filter(PpsEntry.person().identityCard().fullFio())
                .filter(PpsEntry.orgUnit().title());
    }

    /**
     * Группы для журнала обёрнутые со списком преподавателей.
     * Показывается весь список.
     * Сортировка по приоритету групп и нахзванию
     */
    @Bean
    public EntityComboDataSourceHandler trJournalGroupWithPpsDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), TrJournalGroup.class)
                .where(TrJournalGroup.journal(), TR_JOURNAL_JOURNAL_KEY)
                .outputTransformer(new BaseOutputTransformer<DSInput, DSOutput>(TR_GROUP_WITH_PPS_DS, getName())
                {
                    @Override
                    public void transformOutput(DSInput input, DSOutput output, ExecutionContext context)
                    {
                        List<Long> groupIds = CommonBaseUtil.getPropertiesList(output.getRecordList(), TrJournalGroup.group().id().s());

                        String ppsAlias = "pps";
                        List<Object[]> list = new DQLSelectBuilder()
                                .fromEntity(EppPpsCollectionItem.class, ppsAlias)
                                .column(property(ppsAlias, EppPpsCollectionItem.list().id()))
                                .column(property(ppsAlias, EppPpsCollectionItem.pps().person().identityCard()))
                                .where(in(property(ppsAlias, EppPpsCollectionItem.list().id()), groupIds))
                                .createStatement(context.getSession()).list();

                        Map<Long, String> ppsMap = list.stream()
                                .sorted((row1, row2) -> ((IdentityCard) row1[1]).getFio().compareTo(((IdentityCard) row2[1]).getFio()))
                                .collect(Collectors.groupingBy(
                                        row -> (Long) row[0],
                                        Collectors.mapping(row -> ((IdentityCard) row[1]).getFio(), Collectors.joining(", "))
                                ));

                        CollectionUtils.transform(output.getRecordList(), in -> {
                            if (in instanceof DataWrapper)
                                return in;

                            DataWrapper wrapper = new DataWrapper(in);
                            wrapper.put(TR_GROUP_DS_PPS_LIST_PROPERTY, ppsMap.get(((TrJournalGroup) in).getGroup().getId()));
                            return wrapper;
                        });
                    }
                })
                .order(TrJournalGroup.group().type().priority())
                .order(TrJournalGroup.group().title())
                .pageable(false);
    }
}
