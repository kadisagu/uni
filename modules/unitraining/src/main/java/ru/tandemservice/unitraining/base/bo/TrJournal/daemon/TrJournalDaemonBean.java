package ru.tandemservice.unitraining.base.bo.TrJournal.daemon;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.EntityReorderer;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeALoad;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;
import static org.tandemframework.hibsupport.dql.DQLFunctions.max;

/**
 * @author vdanilov
 */
public class TrJournalDaemonBean extends UniBaseDao implements ITrJournalDaemonBean
{

    public static final SyncDaemon DAEMON = new SyncDaemon(TrJournalDaemonBean.class.getName(), 120, LOCK_NAME)
    {
        @Override protected void main()
        {
            // сначала обновляем список модулей (каждое действие - в своей транзакции)
            try { ITrJournalDaemonBean.instance.get().doUpdateJournalModuleSet(); }
            catch (final Exception t) { Debug.exception(t.getMessage(), t); }

            // по всем созданным модулям актуализируем список событий
            try { ITrJournalDaemonBean.instance.get().doUpdateJournalModuleEvents(); }
            catch (final Exception t) { Debug.exception(t.getMessage(), t); }

            // по всем созданным событиям актуализируем список событий угс
            try { ITrJournalDaemonBean.instance.get().doUpdateJournalGroupEvents(); }
            catch (final Exception t) { Debug.exception(t.getMessage(), t); }

            // по всем событиям угс актуализируем список студентов из угс (на дату события)
            try { ITrJournalDaemonBean.instance.get().doUpdateJournalGroupEventStudents(); }
            catch (final Exception t) { Debug.exception(t.getMessage(), t); }

            // обновляем поле "есть оценки" в событиях журнала
            try { ITrJournalDaemonBean.instance.get().doUpdateHasMarksFieldForJournalEvents(); }
            catch (final Exception t) { Debug.exception(t.getMessage(), t); }

            // проверяем связи студентов с событиями групп
            try { ITrJournalDaemonBean.instance.get().doCheckGroupEventStudents(); }
            catch (final Exception t) { Debug.exception(t.getMessage(), t); }

            // исправляем отметки о неявке
            try { ITrJournalDaemonBean.instance.get().doCorrectAbsence(); }
            catch (final Exception t) { Debug.exception(t.getMessage(), t); }
        }
    };

    /**
     * Отдельный демон - для обновления связанных с оценками данных, чтобы при выставлении оценок не делать вообще все
     */
    public static final SyncDaemon MARK_DAEMON = new SyncDaemon(TrJournalDaemonBean.class.getName() + ".mark", 120, LOCK_NAME)
    {
        @Override protected void main()
        {
            // исправляем отметки о неявке
            try { ITrJournalDaemonBean.instance.get().doCorrectAbsence(); }
            catch (final Throwable t) { Debug.exception(t.getMessage(), t); }

            // обновляем поле "есть оценки" в событиях журнала
            try { ITrJournalDaemonBean.instance.get().doUpdateHasMarksFieldForJournalEvents(); }
            catch (final Throwable t) { Debug.exception(t.getMessage(), t); }
        }
    };

    protected DQLSelectBuilder getActiveJournalDQLSelectBuilder(final String alias, final String stateCode) {
        return new DQLSelectBuilder()
        .fromEntity(TrJournal.class, alias)
        .where(eq(property(TrJournal.state().code().fromAlias(alias)), value(stateCode)))
        ;
    }

    @Override
    public boolean doUpdateJournalModuleSet()
    {
        boolean result = false;
        final Session session = this.lock(TrJournalModule.class.getName());

        // удаление неактуальных модулей в журнале
        {
            // TODO
        }

        // добавление новых модулей в журнал
        {
            final List<Object[]> rows = this.getActiveJournalDQLSelectBuilder("j", EppState.STATE_FORMATIVE)
            .column(property("j.id"))
            .fromEntity(EppRegistryElementPartModule.class, "m").column(property("m.id"))
            .where(eq(property(TrJournal.registryElementPart().fromAlias("j")), property(EppRegistryElementPartModule.part().fromAlias("m"))))
            .where(notExists(
                new DQLSelectBuilder()
                .fromEntity(TrJournalModule.class, "jm").column(property("jm.id"))
                .where(eq(property(TrJournalModule.journal().fromAlias("jm")), property("j")))
                .where(eq(property(TrJournalModule.module().fromAlias("jm")), property("m")))
                .buildQuery()
            ))
            .order(property(TrJournal.registryElementPart().id().fromAlias("j")))
            .order(property(TrJournal.id().fromAlias("j")))
            .createStatement(session).list();

            if (rows.size() > 0) {
                for (List<Object[]> rowsPart : Lists.partition(rows, 256)) {
                    final Map<Long, TrJournal> journalLoadCache = TrJournalDaemonBean.this.getLoadCacheMap(TrJournal.class);
                    final Map<Long, EppRegistryElementPartModule> moduleLoadCache = TrJournalDaemonBean.this.getLoadCacheMap(EppRegistryElementPartModule.class);

                    for (final Object[] row: rowsPart)
                    {
                        final TrJournal journal = journalLoadCache.get((Long) row[0]);
                        final EppRegistryElementPartModule module = moduleLoadCache.get((Long) row[1]);
                        session.save(new TrJournalModule(journal, module));
                    }

                    session.flush();
                    session.clear();
                    TrJournalDaemonBean.this.infoActivity('.');
                }
                result = true;
            }
        }

        return result;
    }


    private final Comparator<TrJournalEvent> REORDER_COMPARATOR = new Comparator<TrJournalEvent>() {

        // проверка на то, что событие является дополнительным
        private int ad(final TrJournalEvent o) {
            // TODO: здесь можно вставить проверку для доп.событий, запланированных на этапе формирования журнала, если потребуется
            if (o instanceof TrEventAddon) { return 1; }
            return 0;
        }

        // проверка на то, что событие еще не в базе
        private int tr(final TrJournalEvent o) {
            if (null == o.getId()) { return 1; }
            return 0;
        }

        @Override
        public int compare(final TrJournalEvent o1, final TrJournalEvent o2) {
            int r;

            // сначала сортируем по типу события: сначала идут основные, затем дополнительные
            if (0 != (r = (this.ad(o1)-this.ad(o2)))) { return r; }

            // затем сортируем по персистентности: сначала должны быть те, которые уже в базе
            if (0 != (r = (this.tr(o1)-this.tr(o2)))) { return r; }

            // затем сортируем по номерам
            return (o1.getNumber() - o2.getNumber());
        }
    };


    @Override
    public boolean doUpdateJournalModuleEvents()
    {
        boolean result = false;
        final Session session = this.lock(TrJournalEvent.class.getName());
        final double aloadFactor = 0.5;

        // удаление событий, если нет видов нагрузки и форм контроля
        {
            // TODO
        }

        final Map<Long, List<TrJournalEvent>> jm2EventMap = SafeMap.get(ArrayList.class);

        // добавление событий в активный журнал на основе данных из дисциплины
        Debug.begin("doUpdateJournalModuleEvents.create-new-rows");
        try {
            // по видам аудиторной нагрузки
            {
                final List<Object[]> rows = this.getActiveJournalDQLSelectBuilder("j", EppState.STATE_FORMATIVE)
                .where(eq(property(TrJournal.controlOnlyMode().fromAlias("j")), value(Boolean.FALSE))) // для тех, у которых «не только контроль»

                .joinEntity("j", DQLJoinType.inner, TrJournalModule.class, "jm", eq(property("jm", TrJournalModule.L_JOURNAL), property("j")))
                .joinPath(DQLJoinType.inner, TrJournalModule.module().fromAlias("jm"), "m")

                .fromEntity(EppRegistryModuleALoad.class, "x")
                .where(eq(property(EppRegistryModuleALoad.module().fromAlias("x")), property(EppRegistryElementPartModule.module().fromAlias("m"))))
                .where(notExists(
                    new DQLSelectBuilder().fromEntity(TrEventLoad.class, "l").column(property("l.id"))
                    .where(eq(property(TrEventLoad.type().fromAlias("l")), property(EppRegistryModuleALoad.loadType().eppGroupType().fromAlias("x"))))
                    .where(eq(property(TrEventLoad.journalModule().fromAlias("l")), property("jm")))
                    .buildQuery()
                ))

                .column(property("jm.id")).order(property("jm.id"))
                .column(property("x")).order(property(EppRegistryModuleALoad.loadType().code().fromAlias("x")))
                .createStatement(session)
                .list();

                for (final Object[] row: rows)
                {
                    final List<TrJournalEvent> events = jm2EventMap.get((Long) row[0]);
                    final EppRegistryModuleALoad aLoad = (EppRegistryModuleALoad) row[1];
                    final int count = (int)Math.round(aloadFactor*aLoad.getLoadAsDouble());
                    for (int i=0;i<count;i++) {
                        events.add(new TrEventLoad(aLoad.getLoadType().getEppGroupType()));
                    }
                }
            }

            // по формам текущего контроля
            {
                final List<Object[]> rows = this.getActiveJournalDQLSelectBuilder("j", EppState.STATE_FORMATIVE)
                .joinEntity("j", DQLJoinType.inner, TrJournalModule.class, "jm", eq(property("jm", TrJournalModule.L_JOURNAL), property("j")))
                .joinPath(DQLJoinType.inner, TrJournalModule.module().fromAlias("jm"), "m")

                .fromEntity(EppRegistryModuleIControlAction.class, "x")
                .where(eq(property(EppRegistryModuleIControlAction.module().fromAlias("x")), property(EppRegistryElementPartModule.module().fromAlias("m"))))
                .where(notExists(
                    new DQLSelectBuilder().fromEntity(TrEventAction.class, "a").column(property("a.id"))
                    .where(eq(property(TrEventAction.actionType().fromAlias("a")), property(EppRegistryModuleIControlAction.controlAction().fromAlias("x"))))
                    .where(eq(property(TrEventAction.journalModule().fromAlias("a")), property("jm")))
                    .buildQuery()
                ))

                .column(property("jm.id")).order(property("jm.id"))
                .column(property("x")).order(property(EppRegistryModuleIControlAction.controlAction().code().fromAlias("x")))
                .createStatement(session)
                .list();

                for (final Object[] row: rows)
                {
                    final List<TrJournalEvent> events = jm2EventMap.get((Long) row[0]);
                    final EppRegistryModuleIControlAction iControl = (EppRegistryModuleIControlAction)row[1];
                    for (int i=0;i<iControl.getAmount();i++) {
                        events.add(new TrEventAction(iControl.getControlAction().getDefaultGroupType(), iControl.getControlAction()));
                    }
                }
            }


        } finally {
            Debug.end();
        }


        // если на приватное событие (доп.событие) ссылается более одной группы - для каждой группы надо создать копию этого события
        Debug.begin("doUpdateJournalModuleEvents.split-private-events");
        try {

            // список событий, которые надо разбить - обрабатывать будем по одному
            final List<Long> jeIds = new DQLSelectBuilder()
            .fromEntity(TrJournalEvent.class, "je").where(instanceOf("je", TrEventAddon.class))
            .fromEntity(TrEduGroupEvent.class, "ge").where(eq(property("ge", TrEduGroupEvent.L_JOURNAL_EVENT), property("je")))
            .column(property("je.id"))
            .group(property("je.id"))
            .having(gt(count(property("ge.id")), value(1)))
            .createStatement(session).list();

            for (final Long jeId: jeIds)
            {
                // берем наше событие
                final TrEventAddon je = (TrEventAddon) session.get(TrEventAddon.class, jeId);

                // XXX:  добавляем в перенумератор
                // XXX: jm2EventMap.put(je.getJournalModule().getId(), new ArrayList<TrJournalEvent>()); // добавлять не надо, т.к. далее сработает проверка

                // находим максимальный занятый номер
                final Number number = new DQLSelectBuilder()
                .fromEntity(TrJournalEvent.class, "je")
                .column(max(property("je", TrJournalEvent.P_NUMBER)))
                .where(eq(property("je", TrJournalEvent.L_JOURNAL_MODULE), value(je.getJournalModule())))
                .createStatement(session).uniqueResult();
                int i = (null == number ? 0 : number.intValue());

                // берем все события групп (для данного события журнала), кроме первого
                final List<Long> geIds = new DQLSelectBuilder()
                .fromEntity(TrEduGroupEvent.class, "ge")
                .where(eq(property("ge", TrEduGroupEvent.L_JOURNAL_EVENT), value(je)))
                .column(property("ge.id"))
                .order(property("ge.id"))
                .createStatement(this.getSession()).list();

                geIds.remove(0); // первую группу нам не надо менять

                for (final Long geId: geIds)
                {
                    // для них - создаем копию события журнала (с новым номером)
                    final TrEventAddon ne = new TrEventAddon();
                    ne.update(je);
                    ne.setNumber(++i);
                    session.save(ne);

                    // обновляем события групп
                    final TrEduGroupEvent ge = (TrEduGroupEvent)session.get(TrEduGroupEvent.class, geId);
                    ge.setJournalEvent(ne); // natural-id mutable=true
                    session.saveOrUpdate(ge);
                }

                // все, что было изменено - сохраняем
                session.flush();
                session.clear();
            }

        } finally {
            Debug.end();
        }


        // перенумерация событий (для каждого модуля: дополнительные события должны идти после основных)
        Debug.begin("doUpdateJournalModuleEvents.select-renumerate");
        try {
            // изменения должны проходить во !всех! состояниях журнала
            // если журнал в редактируемом состоянии - то это допустимо, если же нет, то могут возникнуть проблемы с перенумерацией обычных событий (их редактирование запрещено листенером, который на eppState)
            // по этой причине перенумерация ведется только для формирующихся журналов; вопрос о том, стоит ли менять правила - отложим до первого инцедента
            // пути решения - переписать логику либо разрешить у события менять номер во всех состояниях

            // список модулей для которых надо запустить перенумерацию
            final List<Long> jmIds = new DQLSelectBuilder()
            .fromEntity(TrJournalEvent.class, "je_a").where(instanceOf("je_a", TrEventAddon.class))
            .fromEntity(TrJournalEvent.class, "je_o").where(notInstanceOf("je_o", TrEventAddon.class))
            .where(eq(property("je_a", TrJournalEvent.L_JOURNAL_MODULE), property("je_o", TrJournalEvent.L_JOURNAL_MODULE)))
            .where(lt(property("je_a", TrJournalEvent.P_NUMBER), property("je_o", TrJournalEvent.P_NUMBER)))
            .column(property(TrJournalEvent.journalModule().id().fromAlias("je_a")))
            .where(in(
                property(TrJournalEvent.journalModule().journal().id().fromAlias("je_a")),
                this.getActiveJournalDQLSelectBuilder("j", EppState.STATE_FORMATIVE).column(property("j.id")).buildQuery()
            ))
            .createStatement(session).list();

            if (jmIds.size() > 0) {
                Debug.message("journal-module-to-be-renumerate=" + jmIds.size());
                for (final Long id: jmIds) {
                    jm2EventMap.put(id, new ArrayList<>());
                }
            }

        } finally {
            Debug.end();
        }


        // если что-то надо делать - делаем
        if (jm2EventMap.size() > 0) {
            Debug.begin("doUpdateJournalModuleEvents.apply-and-renumerate");
            try {
                BatchUtils.execute(jm2EventMap.keySet(), 256, new BatchUtils.Action<Long>() {
                    EntityReorderer<TrJournalEvent> renumerator = new EntityReorderer<TrJournalEvent>() {
                        @Override protected int getNumber(final TrJournalEvent e) {
                            return e.getNumber();
                        }
                        @Override protected void setNumber(final TrJournalEvent e, final int number) {
                            e.setNumber(number);
                            session.saveOrUpdate(e);
                            session.flush();
                        }
                    };

                    @Override public void execute(final Collection<Long> ids) {

                        // грузим уже добавленные события
                        {
                            final List<TrJournalEvent> events = new DQLSelectBuilder()
                            .fromEntity(TrJournalEvent.class, "je").column(property("je"))
                            .where(in(property(TrJournalEvent.journalModule().id().fromAlias("je")), ids))
                            .createStatement(session).list();

                            for (final TrJournalEvent e: events) {
                                jm2EventMap.get(e.getJournalModule().getId()).add(e);
                            }
                        }

                        // перенумеруем
                        for (final Long id: ids)
                        {
                            final TrJournalModule jm = (TrJournalModule)session.load(TrJournalModule.class, id);
                            List<TrJournalEvent> list = jm2EventMap.get(id);

                            // берем максимальный номер (среди всех добавленных)
                            int i = 0;
                            for (final TrJournalEvent e: list) { i = Math.max(i, e.getNumber()); }

                            // выставляем новым объектам номера (номера должны быть уникальны)
                            for (final TrJournalEvent e: list) {
                                if (null == e.getId()) {
                                    e.setJournalModule(jm);
                                    e.setNumber(++i);
                                }
                            }

                            // сортируем объекты (выстраиваем им верный порядок)
                            Collections.sort(list, TrJournalDaemonBean.this.REORDER_COMPARATOR);

                            // перенумеруем объекты
                            list = this.renumerator.execute(list);

                            // сохраняем объекты (если что-то осталось сохранить)
                            for (final TrJournalEvent e: list) {
                                if (null == e.getId()) {
                                    session.saveOrUpdate(e);
                                }
                            }

                            // сохраняем в базу и сбрасываем кэш
                            session.flush();
                            session.clear();
                        }

                        TrJournalDaemonBean.this.infoActivity('.');
                    }
                });
                result = true;
            } finally {
                Debug.end();
            }
        }

        return result;
    }


    @Override
    public boolean doUpdateJournalGroupEvents()
    {
        boolean result = false;
        final Session session = this.lock(TrEduGroupEvent.class.getName());

        Debug.begin("doUpdateJournalGroupEvents.deactivate-error-rows");
        try {
            // сначала удалим связи со студентами для ошибочных событий - те, в которые никаких отметок не ставили
            final IDQLExpression canDeleteRelExpression = new DQLCanDeleteExpressionBuilder(TrEduGroupEventStudent.class, "id").getExpression();
            this.executeAndClear(
                new DQLDeleteBuilder(TrEduGroupEventStudent.class)

                // можно удалять только те объекты, на которые никто не ссылается
                .where(canDeleteRelExpression)

                // можно удалять только те связи, в которых человек ничего не заполнял
                .where(isNull(property(TrEduGroupEventStudent.absent())))
                .where(isNull(property(TrEduGroupEventStudent.gradeAsLong())))
                .where(isNull(property(TrEduGroupEventStudent.comment())))

                // события, в которых ВН в УГС не соотвествует ВН в событии журнала
                .where(in(
                    property(TrEduGroupEventStudent.event()),
                    new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ge").column(property("ge.id"))
                    .where(ne(property(TrEduGroupEvent.journalEvent().type().fromAlias("ge")), property(TrEduGroupEvent.group().type().fromAlias("ge"))))
                    .buildQuery()
                ))
            );
            // а теперь удалим сами ошибочные события
            final IDQLExpression canDeleteExpression = new DQLCanDeleteExpressionBuilder(TrEduGroupEvent.class, "id").getExpression();
            this.executeAndClear(
                new DQLDeleteBuilder(TrEduGroupEvent.class)

                // можно удалять только те объекты, на которые никто не ссылается
                .where(canDeleteExpression)

                // можно удалять только те события, для которых не указаны элементы расписания
                .where(isNull(property(TrEduGroupEvent.scheduleEvent())))

                // нужно удалять только то, что не соотвествует типам
                .where(in(
                    property("id"),
                    new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ge").column(property("ge.id"))
                    .where(ne(property(TrEduGroupEvent.journalEvent().type().fromAlias("ge")), property(TrEduGroupEvent.group().type().fromAlias("ge"))))
                    .buildQuery()
                ))
            );
        } finally {
            Debug.end();
        }

        Debug.begin("doUpdateJournalGroupEvents.create-or-activate-rows");
        try {

            // берем только правильные журналы
            final List<Object[]> rows = this.getActiveJournalDQLSelectBuilder("j", EppState.STATE_ACCEPTED)

            // их группы
            .fromEntity(TrJournalGroup.class, "jg").where(eq(property(TrJournalGroup.journal().fromAlias("jg")), property("j")))
            .column(property(TrJournalGroup.group().id().fromAlias("jg")))

            // и события журнала
            .fromEntity(TrJournalEvent.class, "je").where(eq(property(TrJournalEvent.journalModule().journal().fromAlias("je")), property("j")))
            .where(notInstanceOf("je", TrEventAddon.class)) // не добавляем события группы на временные события
            .column(property(TrJournalEvent.id().fromAlias("je")))

            // и так, чтобы события соответсовали типам групп
            .where(eq(property(TrJournalGroup.group().type().fromAlias("jg")), property(TrJournalEvent.type().fromAlias("je"))))

            // и не было событий групп
            .where(notExists(
                new DQLSelectBuilder()
                .fromEntity(TrEduGroupEvent.class, "ge")
                .where(eq(property(TrEduGroupEvent.group().fromAlias("ge")), property(TrJournalGroup.group().fromAlias("jg"))))
                .where(eq(property(TrEduGroupEvent.journalEvent().fromAlias("ge")), property("je")))
                .buildQuery()
            ))

            .createStatement(session).list();

            if (rows.size() > 0) {
                for (List<Object[]> rowsPart : Lists.partition(rows, 256)) {

                    for (final Object[] row: rowsPart) {
                        final EppRealEduGroup g = (EppRealEduGroup)session.load(EppRealEduGroup.class, (Long)row[0]);
                        final TrJournalEvent e = (TrJournalEvent)session.load(TrJournalEvent.class, (Long)row[1]);
                        session.save(new TrEduGroupEvent(g, e));
                    }

                    session.flush();
                    session.clear();
                    TrJournalDaemonBean.this.infoActivity('.');
                }
                result = true;
            }


        } finally {
            Debug.end();
        }

        return result;
    }

    @Override
    public int daysBeforeEventWhenCreateStudentEvents()
    {
        // TODO: за 2-3 дня (настройка) до текущей даты
        return 1;
    }

    @Override
    public boolean doUpdateJournalGroupEventStudents() {

        boolean result = false;
        final Session session = this.lock(TrEduGroupEventStudent.class.getName());

        final Date deadline = CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(new Date(), Calendar.DAY_OF_YEAR, daysBeforeEventWhenCreateStudentEvents()));

        Debug.begin("doUpdateJournalGroupEventStudents.create-missing-students");
        try {
            final List<Long> ids = new DQLSelectBuilder()
            .fromEntity(TrEduGroupEvent.class, "ge").column(property("ge.id"))
            .where(le(property(TrEduGroupEvent.scheduleEvent().durationBegin().fromAlias("ge")), valueTimestamp(deadline)))
            .where(or(
                /* дата обработки не была выставлена (TODO: выделить поле - дата обработки) *
                 * (это первая обработка события)                                           */
                isNull(property(TrEduGroupEvent.deadlineDate().fromAlias("ge"))),

                /* дата обработки раньше события (TODO: выделить поле - дата обработки) *
                 * (событие перенесли вперед)                                           */
                lt(property(TrEduGroupEvent.deadlineDate().fromAlias("ge")), property(TrEduGroupEvent.scheduleEvent().durationBegin().fromAlias("ge")))
            ))
            .createStatement(session).list();

            if (ids.size() > 0) {
                for (List<Long> idsPart : Lists.partition(ids, DQL.MAX_VALUES_ROW_NUMBER)) {

                    final Map<Long, TrEduGroupEvent> groupEventMap = TrJournalDaemonBean.this.getLoadCacheMap(TrEduGroupEvent.class);

                    final List<Object[]> rows = new DQLSelectBuilder()

                            // для всех событий УГС
                            .fromEntity(TrEduGroupEvent.class, "ge")
                            .where(in(property("ge.id"), idsPart))
                            .column(property("ge.id"), "ge_id")

                                    // для всех активных связей студентов этих УГС
                            .joinEntity("ge", DQLJoinType.inner, EppRealEduGroupRow.class, "gr", and(
                                    eq(property(EppRealEduGroupRow.group().fromAlias("gr")), property(TrEduGroupEvent.group().fromAlias("ge"))),
                                    isNull(property(EppRealEduGroupRow.removalDate().fromAlias("gr")))
                            ))

                                    // для всех их студентов
                            .joinPath(DQLJoinType.inner, EppRealEduGroupRow.studentWpePart().studentWpe().fromAlias("gr"), "slot")
                            .column(property("slot.id"), "slot_id")

                                    // для которых нет событий в журнале
                            .joinEntity("slot", DQLJoinType.left, TrEduGroupEventStudent.class, "xx", and(
                                    eq(property(TrEduGroupEventStudent.event().fromAlias("xx")), property("ge")),
                                    eq(property(TrEduGroupEventStudent.studentWpe().fromAlias("xx")), property("slot"))
                            ))
                            .where(isNull(property("xx.id")))

                            .predicate(DQLPredicateType.distinct)
                            .createStatement(session).list();

                    // создаем события
                    for (final Object[] row: rows) {
                        final TrEduGroupEvent ge = groupEventMap.get((Long) row[0]);
                        final EppStudentWorkPlanElement s = (EppStudentWorkPlanElement)session.load(EppStudentWorkPlanElement.class, (Long)row[1]);
                        session.save(new TrEduGroupEventStudent(ge, s));
                    }

                    // выставляем даты изменения
                    new DQLUpdateBuilder(TrEduGroupEvent.class)
                            .set(TrEduGroupEvent.P_DEADLINE_DATE, valueDate(deadline))
                            .where(or(
                                    isNull(property(TrEduGroupEvent.P_DEADLINE_DATE)),
                                    lt(property(TrEduGroupEvent.P_DEADLINE_DATE), valueDate(deadline))
                            ))
                            .where(in(TrEduGroupEvent.id(), idsPart))
                            .createStatement(session).execute();

                    session.flush();
                    session.clear();
                    TrJournalDaemonBean.this.infoActivity('.');
                }
                result = true;
            }
        } finally {
            Debug.end();
        }

        return result;
    }


    @Override
    public boolean doCheckGroupEventStudents()
    {
        final MutableBoolean result = new MutableBoolean(false);
        final Session session = this.lock(TrEduGroupEventStudent.class.getName());

        // если студента переводят из одной УГС в другую, то отметки и доп. баллы в журналах,
        // полученные в старой УГС, должны стать неактуальными - нужно проставить им дату перевода, transferDate
        // чтобы не писать два раза одни и те же запросы, вводим связь студента с группой в журнале - TrJournalGroupStudent
        // и проставляем transferDate в нее, а потом на ее основании апдейтим transferDate в отметках в журнале и доп. баллах

        // 1. сначала разберемся с этими вспомогательными связями студента с группой в журнале - TrJournalGroupStudent

        // 1.1 создаем недостающие связи студента с группой в журнале - TrJournalGroupStudent
        Debug.begin("doCheckGroupEventStudents.create-relations-by-journal-event");
        try
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder()

            // события студента
            .fromEntity(TrEduGroupEventStudent.class, "ges")
            .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.studentWpe().fromAlias("ges"), "ss")
            .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.event().group().fromAlias("ges"), "eg")
            .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("ges"), "jj")

            // группа журнала
            .fromEntity(TrJournalGroup.class, "jg")
            .where(eq(property(TrJournalGroup.group().fromAlias("jg")), property("eg")))
            .where(eq(property(TrJournalGroup.journal().fromAlias("jg")), property("jj")))

            // нет связи студента с группой
            .where(notExists(
                new DQLSelectBuilder()
                .fromEntity(TrJournalGroupStudent.class, "js").column("js.id")
                .where(eq(property(TrJournalGroupStudent.studentWpe().fromAlias("js")), property("ss")))
                .where(eq(property(TrJournalGroupStudent.group().fromAlias("js")), property("jg")))
                .buildQuery()
            ))

            // выбираем группу и студента, чтобы создать связь
            .predicate(DQLPredicateType.distinct)
            .column(property("jg.id"))
            .column(property("ss.id"));

            final List<Object[]> rows = dql.createStatement(session).list();
            Debug.message("count: " + rows.size());
            for (final Object[] row : rows)
            {
                final TrJournalGroupStudent rel = new TrJournalGroupStudent();
                rel.setGroup((TrJournalGroup) session.load(TrJournalGroup.class, (Long) row[0]));
                rel.setStudentWpe((EppStudentWorkPlanElement) session.load(EppStudentWorkPlanElement.class, (Long) row[1]));
                session.save(rel);
            }

            session.flush();
        }
        finally
        {
            Debug.end();
        }

        // 1.3 теперь снимем отметку о переводе с тех связей,
        // которые снова стали соответствовать текущей группе студента
        Debug.begin("doCheckGroupEventStudents.unmark-correct-relations");
        try
        {
            final List<Long> ids = new DQLSelectBuilder()
            .fromEntity(EppRealEduGroupRow.class, "ls")
            // связи, в которых текущая группа соответствует группе в реализации
            .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("ls"))))
            .fromEntity(TrJournalGroupStudent.class, "gs")
            // и в которых стоит дата перевода из УГС
            .where(isNotNull(property(TrJournalGroupStudent.transferDate().fromAlias("gs"))))
            .where(eq(property(EppRealEduGroupRow.studentWpePart().studentWpe().fromAlias("ls")), property(TrJournalGroupStudent.studentWpe().fromAlias("gs"))))
            .where(eq(property(EppRealEduGroupRow.group().fromAlias("ls")), property(TrJournalGroupStudent.group().group().fromAlias("gs"))))
            // только активные журналы
            .where(eq(property(TrJournalGroupStudent.group().journal().state().code().fromAlias("gs")), value(EppState.STATE_ACCEPTED)))
            .column(property("gs.id"))
            .createStatement(session).list();

            if (ids.size() > 0)
            {
                for (List<Long> idsPart : Lists.partition(ids, DQL.MAX_VALUES_ROW_NUMBER)) {
                    // снимаем отметку - transferDate
                    new DQLUpdateBuilder(TrJournalGroupStudent.class)
                            .set(TrJournalGroupStudent.P_TRANSFER_DATE, DQLFunctions.cast(valueTimestamp(null), PropertyType.TIMESTAMP))
                            .where(in("id", idsPart))
                            .createStatement(session).execute();
                    session.flush();
                    session.clear();
                    TrJournalDaemonBean.this.infoActivity('.');
                }
                result.setValue(true);
            }
            Debug.message("count: " + ids.size());
        }
        finally
        {
            Debug.end();
        }

        // 1.4 теперь самое интересное
        // разрешаем конфликты, проставляя transferDate,
        // когда у нас есть более одной связи студента с группой без transferDate,
        // для одного и того же МСРП, ВН, и дисциплиночасти
        Debug.begin("doCheckGroupEventStudents.set-transfer-date-for-relations");
        try
        {
            // ищем конфликты
            final DQLSelectBuilder inner = new DQLSelectBuilder()
            // события в журналах для студента
            .fromEntity(TrJournalGroupStudent.class, "ges_i")
            // выбирать будем дисциплину, вид нагрузки, студента
            .column(property(TrJournalGroupStudent.group().journal().registryElementPart().id().fromAlias("ges_i")), "disc")
            .column(property(TrJournalGroupStudent.group().group().type().id().fromAlias("ges_i")), "lt")
            .column(property(TrJournalGroupStudent.studentWpe().id().fromAlias("ges_i")), "s")
            // и группировать тоже по дисциплине, виду нагрузки, студенту
            .group(property(TrJournalGroupStudent.group().journal().registryElementPart().id().fromAlias("ges_i")))
            .group(property(TrJournalGroupStudent.group().group().type().id().fromAlias("ges_i")))
            .group(property(TrJournalGroupStudent.studentWpe().id().fromAlias("ges_i")))
            // нет даты перевода - transferDate, и только текущий учебный год
            .where(isNull(property(TrJournalGroupStudent.transferDate().fromAlias("ges_i"))))
            //.where(eq(property(TrJournalGroupStudent.group().journal().yearPart().year().educationYear().current().fromAlias("ges_i")), value(Boolean.TRUE)))
            // событий больше одного
            .having(gt(count(property(TrJournalGroupStudent.id().fromAlias("ges_i"))), value(1)));

            final List<Object[]> rows = inner.createStatement(session).list();
            Debug.message("count: " + rows.size());
            for (final Object[] lsRow : rows)
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                // собственно, связи студента с группой журнала, нет даты перевода - transferDate
                .fromEntity(TrJournalGroupStudent.class, "ges")
                .where(isNull(property(TrJournalGroupStudent.transferDate().fromAlias("ges"))))
                // интересующая нас дисциплина, студент, вид нагрузки
                .where(eq(property(TrJournalGroupStudent.group().journal().registryElementPart().id().fromAlias("ges")), commonValue(lsRow[0])))
                .where(eq(property(TrJournalGroupStudent.group().group().type().id().fromAlias("ges")), commonValue(lsRow[1])))
                .where(eq(property(TrJournalGroupStudent.studentWpe().id().fromAlias("ges")), commonValue(lsRow[2])))

                // нам нужны данные из связей студентов с УГС, чтобы разрешить конфликт - по текущей или последней УГС студента
                .joinEntity("ges", DQLJoinType.inner, EppStudentWpeALoad.class, "ls",
                    eq(property(TrJournalGroupStudent.studentWpe().fromAlias("ges")), property(EppStudentWpeALoad.studentWpe().fromAlias("ls")))
                )
                .where(eq(property(EppStudentWpeALoad.type().id().fromAlias("ls")), commonValue(lsRow[1])))

                .joinPath(DQLJoinType.left, TrJournalGroupStudent.group().group().fromAlias("ges"), "g")
                .joinEntity("ls", DQLJoinType.left, EppRealEduGroupRow.class, "gs", and(
                    eq(property("ls"), property(EppRealEduGroupRow.studentWpePart().fromAlias("gs"))),
                    eq(property("g"), property(EppRealEduGroupRow.group().fromAlias("gs")))
                ))

                // выбираем id связи, ключ - дисциплину, вн, мсрп - и данные из связей студентов с УГС для разрешения конфликта
                .column(property("ges.id"))
                .column(property(TrJournalGroupStudent.group().journal().registryElementPart().id().fromAlias("ges")))
                .column(property(TrJournalGroupStudent.group().group().type().id().fromAlias("ges")))
                .column(property(TrJournalGroupStudent.studentWpe().id().fromAlias("ges")))
                .column(property("gs.id"))
                .column(property(EppRealEduGroupRow.removalDate().fromAlias("gs")));

                // сортируем найденное
                final Map<MultiKey, Map<Long, PairKey<Long, Date>>> map = SafeMap.get(HashMap.class);
                final List<Object[]> conflictRows = dql.createStatement(session).list();
                for (final Object[] row : conflictRows)
                {
                    final Long ges = (Long) row[0];
                    final MultiKey key = new MultiKey(row[1], row[2], row[3]);
                    final Long groupStudent = (Long) row[4];
                    final Date rDate = (Date) row[5];
                    // в данном случае суть конфликта - две связи для одного ключа: мсрп, вн, дисциплиночасть, и нам надо выбрать актуальную связь
                    // так что по ключу фактически лежит тройка:
                    // данные связи с угс для разрешения конфликта, т.е. дата утраты актуальности + факт существования связи
                    // и id связи студента с группой журнала
                    if (null != map.get(key).put(ges, new PairKey<>(groupStudent, rDate)))
                        throw new IllegalStateException();
                }

                final Set<Long> gesIdsSet = new HashSet<>();
                for (final Map<Long, PairKey<Long, Date>> conflictMap : map.values())
                {
                    // проходим по мапе конфликтов последовательно,
                    // и для каждого ключа убираем ту запись, которую считаем наиболее актуальной
                    conflictMap.remove(Collections.max(conflictMap.entrySet(), (o1, o2) -> {
                        // собственно, определяем, кто более актуален
                        // если у связи с УГС есть id (т.е. связь вообще есть) и в ней нет даты -
                        // - то студент на данный момент в этой УГС, и это самая актуальная УГС
                        if ((o1.getValue().getFirst() != null) && (o1.getValue().getSecond() == null)) {
                            return 1;
                        }
                        if ((o2.getValue().getFirst() != null) && (o2.getValue().getSecond() == null)) {
                            return -1;
                        }
                        // если обе связи с УГС неактуальны - смотрим по дате утраты актуальности
                        if ((o1.getValue().getSecond() != null) && (o2.getValue().getSecond() != null)) {
                            return o1.getValue().getSecond().compareTo(o2.getValue().getSecond());
                        }
                        // все совсем плохо, связи уже утрачены - смотрим просто по id записи - связи студента с группой журнала,
                        // т.к. id возрастают - выбираем фактически последнюю созданную запись
                        return o1.getKey().compareTo(o2.getKey());
                    }).getKey());
                    gesIdsSet.addAll(conflictMap.keySet());
                }

                // ну и ставим дату перевода всем неактуальным записям - текущим временем
                if (gesIdsSet.size() > 0)
                {
                    for (List<Long> gesIdsPart : Iterables.partition(gesIdsSet, DQL.MAX_VALUES_ROW_NUMBER)) {
                        new DQLUpdateBuilder(TrJournalGroupStudent.class)
                                .set(TrJournalGroupStudent.P_TRANSFER_DATE, DQLFunctions.cast(valueTimestamp(new Date()), PropertyType.TIMESTAMP))
                                .where(in("id", gesIdsPart))
                                .createStatement(session).execute();

                        session.flush();
                        session.clear();
                        TrJournalDaemonBean.this.infoActivity('.');
                    }
                    result.setValue(true);
                }

                Debug.message("count: " + gesIdsSet.size());

                session.flush();
                session.clear();
                TrJournalDaemonBean.this.infoActivity('.');
            }
        }
        finally
        {
            Debug.end();
        }

        // 2. теперь обновим transferDate уже в событиях студента в журнале и доп. баллах

        // 2.1 удалим связи со студентом в журнале, когда нет связи студента с группой и нет отметок\баллов
        Debug.begin("doCheckGroupEventStudents.delete-unrelated-empty-event-students");
        try
        {
            final IDQLExpression canDeleteRelExpression = new DQLCanDeleteExpressionBuilder(TrEduGroupEventStudent.class, "id").getExpression();
            final int i = this.executeAndClear(
                new DQLDeleteBuilder(TrEduGroupEventStudent.class)
                // можно удалять только те объекты, на которые никто не ссылается
                .where(canDeleteRelExpression)
                .where(in(
                    property("id"),
                    new DQLSelectBuilder()
                    .fromEntity(TrEduGroupEventStudent.class, "ges").column("ges.id")
                    .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.event().fromAlias("ges"), "ge")
                    .joinPath(DQLJoinType.inner, TrEduGroupEvent.journalEvent().journalModule().journal().fromAlias("ge"), "j")
                    // и в которых человек ничего не заполнял
                    .where(isNull(property(TrEduGroupEventStudent.absent().fromAlias("ges"))))
                    .where(isNull(property(TrEduGroupEventStudent.gradeAsLong().fromAlias("ges"))))
                    // нет связи студента с группой журнала
                    .where(notExists(
                        new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "js")
                        .where(eq(property(TrEduGroupEventStudent.studentWpe().fromAlias("ges")), property(TrJournalGroupStudent.studentWpe().fromAlias("js"))))
                        .where(eq(property(TrEduGroupEvent.group().fromAlias("ge")), property(TrJournalGroupStudent.group().group().fromAlias("js"))))
                        .where(eq(property("j"), property(TrJournalGroupStudent.group().journal().fromAlias("js"))))
                        .column(property("js.id"))
                        .buildQuery()
                    ))
                    .buildQuery()
                ))
            );
            Debug.message("count: " + i);
        }
        finally
        {
            Debug.end();
        }

        // 2.2 обновим transferDate в отметках на основании данных связи студента с группой журнала
        Debug.begin("doCheckGroupEventStudents.set-transfer-date-for-event-students");
        try
        {
            final int i = this.executeAndClear(
                new DQLUpdateBuilder(TrEduGroupEventStudent.class)
                .fromEntity(TrEduGroupEventStudent.class, "ges")
                .where(eq(property("id"), property(TrEduGroupEventStudent.id().fromAlias("ges"))))
                .fromEntity(TrJournalGroupStudent.class, "gs")
                .where(eq(property(TrEduGroupEventStudent.studentWpe().fromAlias("ges")), property(TrJournalGroupStudent.studentWpe().fromAlias("gs"))))
                .where(eq(property(TrEduGroupEventStudent.event().group().fromAlias("ges")), property(TrJournalGroupStudent.group().group().fromAlias("gs"))))
                .where(eq(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("ges")), property(TrJournalGroupStudent.group().journal().fromAlias("gs"))))
                .where(or(
                    and(
                        isNull(TrEduGroupEventStudent.transferDate().fromAlias("ges")),
                        isNotNull(property(TrJournalGroupStudent.transferDate().fromAlias("gs")))
                    ),
                    and(
                        isNotNull(TrEduGroupEventStudent.transferDate().fromAlias("ges")),
                        isNull(property(TrJournalGroupStudent.transferDate().fromAlias("gs")))
                    )
                ))
                .set(TrEduGroupEventStudent.P_TRANSFER_DATE, property(TrJournalGroupStudent.transferDate().fromAlias("gs"))));
            Debug.message("count: " + i);
        }
        finally
        {
            Debug.end();
        }

        // 2.3 поставим transferDate в студентах в журнале там, где нет связи студента с группой журнала, и есть отметки\баллы
        Debug.begin("doCheckGroupEventStudents.set-transfer-date-for-unrelated-event-students");
        try
        {
            final int i = this.executeAndClear(
                new DQLUpdateBuilder(TrEduGroupEventStudent.class)
                .where(in(
                    property("id"),
                    new DQLSelectBuilder()
                    .fromEntity(TrEduGroupEventStudent.class, "ges")
                    .where(isNull(property(TrEduGroupEventStudent.transferDate().fromAlias("ges"))))
                    .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.event().fromAlias("ges"), "ge")
                    .joinPath(DQLJoinType.inner, TrEduGroupEvent.journalEvent().journalModule().journal().fromAlias("ge"), "j")
                    .where(notExists(
                        new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "js")
                        .where(eq(property(TrEduGroupEventStudent.studentWpe().fromAlias("ges")), property(TrJournalGroupStudent.studentWpe().fromAlias("js"))))
                        .where(eq(property(TrEduGroupEvent.group().fromAlias("ge")), property(TrJournalGroupStudent.group().group().fromAlias("js"))))
                        .where(eq(property("j"), property(TrJournalGroupStudent.group().journal().fromAlias("js"))))
                        .column(property("js.id"))
                        .buildQuery()
                    ))
                    .column(property("ges.id"))
                    .buildQuery()
                ))
                .set(TrEduGroupEventStudent.P_TRANSFER_DATE, DQLFunctions.cast(valueTimestamp(new Date()), PropertyType.TIMESTAMP))
            );
            Debug.message("count: " + i);
        }
        finally
        {
            Debug.end();
        }

        // фффсе (:

        return result.booleanValue();
    }

    @Override
    public boolean doCorrectAbsence()
    {
        return this.executeAndClear(
            new DQLUpdateBuilder(TrEduGroupEventStudent.class)
            .set(TrEduGroupEventStudent.P_ABSENT, value(Boolean.TRUE))
            .where(isNotNull(property(TrEduGroupEventStudent.L_ABSENCE_REASON)))
            .where(or(
                eq(property(TrEduGroupEventStudent.P_ABSENT), value(Boolean.FALSE)),
                isNull(property(TrEduGroupEventStudent.P_ABSENT))
            ))
        ) > 0;
    }

    @Override
    public boolean doUpdateHasMarksFieldForJournalEvents()
    {
        Debug.begin("TrJournalDaemonBean.doUpdateHasMarksFieldForJournalEvents");
        try {

            int count = 0;

            count += executeAndClear(
                new DQLUpdateBuilder(TrJournalEvent.class)
                .set(TrJournalEvent.P_HAS_MARKS, value(Boolean.FALSE))
                .where(eq(property(TrJournalEvent.P_HAS_MARKS), value(Boolean.TRUE)))
                .where(eq(property(TrJournalEvent.journalModule().journal().yearPart().year().educationYear().current()), value(Boolean.TRUE)))
                .where(notExists(
                    new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "ges").column(property("ges.id"))
                    .where(eq(property(TrEduGroupEventStudent.event().journalEvent().id().fromAlias("ges")), property("id")))
                    .where(isNull(property(TrEduGroupEventStudent.transferDate().fromAlias("ges"))))
                    .where(or(
                        isNotNull(property(TrEduGroupEventStudent.absent().fromAlias("ges"))),
                        isNotNull(property(TrEduGroupEventStudent.gradeAsLong().fromAlias("ges")))
                    ))
                    .buildQuery()
                ))
            );

            count += executeAndClear(
                new DQLUpdateBuilder(TrJournalEvent.class)
                .set(TrJournalEvent.P_HAS_MARKS, value(Boolean.TRUE))
                .where(eq(property(TrJournalEvent.P_HAS_MARKS), value(Boolean.FALSE)))
                .where(eq(property(TrJournalEvent.journalModule().journal().yearPart().year().educationYear().current()), value(Boolean.TRUE)))
                .where(exists(
                    new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "ges").column(property("ges.id"))
                    .where(eq(property(TrEduGroupEventStudent.event().journalEvent().id().fromAlias("ges")), property("id")))
                    .where(isNull(property(TrEduGroupEventStudent.transferDate().fromAlias("ges"))))
                    .where(or(
                        isNotNull(property(TrEduGroupEventStudent.absent().fromAlias("ges"))),
                        isNotNull(property(TrEduGroupEventStudent.gradeAsLong().fromAlias("ges")))
                    ))
                    .buildQuery()
                ))
            );

            if (count > 0) {
                Debug.message("count: " + count);
                return true;
            }

            return false;

        } finally {
            Debug.end();
        }
    }
}
