/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.UnactualMarkList;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.TrJournalUnactualMarksDSHandler;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * @author oleyba
 * @since 9/8/11
 */
@State
({
    @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "journal.id")
})
public class TrJournalUnactualMarkListUI extends UIPresenter
{
    private TrJournal journal = new TrJournal();

    @Override
    public void onComponentRefresh()
    {
        setJournal(IUniBaseDao.instance.get().get(TrJournal.class, getJournal().getId()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(TrJournalManager.TR_JOURNAL_JOURNAL_KEY, getJournal());
        dataSource.put(TrJournalUnactualMarksDSHandler.PARAM_STUDENT, getSettings().<Object>get("student"));
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    // getters and setters

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }
}
