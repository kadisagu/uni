package ru.tandemservice.unitraining.brs.entity.brs;

import org.tandemframework.shared.commonbase.base.util.IScriptOwner;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniScriptDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.ISelectableByEducationYear;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unitraining.brs.dao.BrsScriptDao;
import ru.tandemservice.unitraining.brs.entity.brs.gen.*;

/**
 * Настройки оценивания на подразделении на год
 */
public class TrOrgUnitSettings extends TrOrgUnitSettingsGen implements ISelectableByEducationYear, IScriptOwner
{
    public TrOrgUnitSettings()
    {
    }

    public TrOrgUnitSettings(OrgUnit orgUnit, EppYearPart yearPart)
    {
        setOrgUnit(orgUnit);
        setEduYearPart(yearPart);
    }

    @Override
    public String getDisplayableTitle()
    {
        return getOrgUnit().getTitleWithType();
    }

    @Override
    public EducationYear getEducationYear()
    {
        return getEduYearPart().getYear().getEducationYear();
    }

    @Override
    public String getTypeTitle()
    {
        return "Подразделение";
    }

    @Override
    public boolean isAllowEditSettings()
    {
        return getOrgUnit() != null && !getOrgUnit().isArchival();
    }

    @Override
    public String getScriptPath()
    {
        return UniScriptDao.findScriptPath(BrsScriptDao.DEFAULT_GLOBAL_RATING_SCRIPT);
    }
}