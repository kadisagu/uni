/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.ScheduleEventEdit;

import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unischedule.util.SppBellScheduleModel;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/22/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "groupEvent.id", required = true)
})
public class TrJournalScheduleEventEditUI extends UIPresenter
{
    private TrEduGroupEvent groupEvent = new TrEduGroupEvent();
    private ScheduleEvent schEvent = new ScheduleEvent();

    private Date date;
    private ISelectModel entryModel;
    private Map<Integer, ScheduleBellEntry> entryMap = new HashMap<>();

    private boolean copyEvent;
    private ISelectModel sourceEventModel;
    private TrEduGroupEvent sourceEvent;

    private boolean incrementWeeks;
    private Integer incrementWeeksAmount;

    private List<UniplacesPlace> placeList;

    @Override
    public void onComponentActivate()
    {
        setEntryModel(new SppBellScheduleModel());
        setGroupEvent(IUniBaseDao.instance.get().<TrEduGroupEvent>getNotNull(TrEduGroupEvent.class, getGroupEvent().getId()));
        if (null != getGroupEvent().getScheduleEvent())
        {
            setPlaceList(CommonBaseUtil.<UniplacesPlace>getPropertiesList(IUniBaseDao.instance.get().getList(ScheduleEventPlace.class, ScheduleEventPlace.scheduleEvent().s(), getGroupEvent().getScheduleEvent()), ScheduleEventPlace.place().s()));
            setDate(getGroupEvent().getScheduleEvent().getDurationBegin());
        }
        else
        {
            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(TrEduGroupEvent.class, "e")
                    .where(eq(property(TrEduGroupEvent.group().fromAlias("e")), value(getGroupEvent().getGroup())))
                    .where(eq(property(TrEduGroupEvent.journalEvent().journalModule().journal().fromAlias("e")), value(getGroupEvent().getJournalEvent().getJournalModule().getJournal())))
                    .where(eq(property(TrEduGroupEvent.journalEvent().type().fromAlias("e")), value(getGroupEvent().getJournalEvent().getType())))
                    .where(isNotNull(property(TrEduGroupEvent.bellScheduleEntry().fromAlias("e"))))
                    .joinPath(DQLJoinType.inner, TrEduGroupEvent.scheduleEvent().fromAlias("e"), "se")
                    .order(property(ScheduleEvent.durationBegin().fromAlias("se")), OrderDirection.asc)
                    .column("e")
                    ;
            final List<TrEduGroupEvent> list = dql.createStatement(_uiSupport.getSession()).list();
            for (TrEduGroupEvent entry : list)
                entryMap.put(CoreDateUtils.get(entry.getScheduleEvent().getDurationBegin(), Calendar.DAY_OF_WEEK), entry.getBellScheduleEntry());
        }
    }

    @Override
    public void onComponentRefresh()
    {
        setGroupEvent(IUniBaseDao.instance.get().<TrEduGroupEvent>getNotNull(TrEduGroupEvent.class, getGroupEvent().getId()));
        if (getGroupEvent().getScheduleEvent() != null)
            setSchEvent(getGroupEvent().getScheduleEvent());
        setSourceEventModel(new DQLFullCheckSelectModel(TrEduGroupEvent.journalEvent().s() + ".title")
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                return new DQLSelectBuilder()
                .fromEntity(TrEduGroupEvent.class, alias)
                .order(property(TrEduGroupEvent.journalEvent().number().fromAlias(alias)))
                .where(isNotNull(property(TrEduGroupEvent.scheduleEvent().fromAlias(alias))))
                .where(eq(property(TrEduGroupEvent.group().fromAlias(alias)), DQLExpressions.value(getGroupEvent().getGroup())));
            }
        });
    }

    public void onClickApply()
    {
        if (getSchEvent().getDurationBegin().after(getSchEvent().getDurationEnd()))
        {
            _uiConfig.getUserContext().getErrorCollector().add("Время окончания события должно быть позже времени начала события.", "durationBegin", "durationEnd");
            return;
        }
        String confirmMessage = checkDate();
        if (null == getClientParameter() && null != confirmMessage)
        {
            ConfirmInfo confirm = new ConfirmInfo(confirmMessage, new ClickButtonAction("submit_dialog", "ok", true));
            TapSupportUtils.displayConfirm(confirm);
            return;
        }
        TrJournalManager.instance().eventDao().saveOrUpdateScheduleEvent(getGroupEvent(), getSchEvent(), getPlaceList());
        deactivate();
    }

    public void onClickDelete()
    {
        TrJournalManager.instance().eventDao().deleteScheduleEvent(getGroupEvent());
        deactivate();
    }

    public void onChangeSourceEvent()
    {
        if (null != getSourceEvent() && null != getSourceEvent().getScheduleEvent())
        {
            if (!isIncrementWeeks() || null == getIncrementWeeksAmount())
            {
                getSchEvent().setDurationBegin(getSourceEvent().getScheduleEvent().getDurationBegin());
                getSchEvent().setDurationEnd(getSourceEvent().getScheduleEvent().getDurationEnd());
                return;
            }
            getSchEvent().setDurationBegin(DateUtils.addWeeks(getSourceEvent().getScheduleEvent().getDurationBegin(), getIncrementWeeksAmount()));
            getSchEvent().setDurationEnd(DateUtils.addWeeks(getSourceEvent().getScheduleEvent().getDurationEnd(), getIncrementWeeksAmount()));
        }
    }

    public void onChangeDate()
    {
        if (getGroupEvent().getBellScheduleEntry() == null && getDate() != null)
            getGroupEvent().setBellScheduleEntry(entryMap.get((CoreDateUtils.get(getDate(), Calendar.DAY_OF_WEEK))));
        onChangeEntry();
    }

    public void onChangeEntry()
    {
        if (getDate() != null)
        {
            Calendar newTime = Calendar.getInstance();
            newTime.setTime(getDate());
            if (null != getSchEvent().getDurationBegin()) {
                Calendar oldTime = Calendar.getInstance();
                oldTime.setTime(getSchEvent().getDurationBegin());
                newTime.set(Calendar.HOUR_OF_DAY, oldTime.get(Calendar.HOUR_OF_DAY));
                newTime.set(Calendar.MINUTE, oldTime.get(Calendar.MINUTE));
            }
            getSchEvent().setDurationBegin(newTime.getTime());
            newTime.setTime(getDate());
            if (null != getSchEvent().getDurationEnd()) {
                Calendar oldTime = Calendar.getInstance();
                oldTime.setTime(getSchEvent().getDurationBegin());
                newTime.set(Calendar.HOUR_OF_DAY, oldTime.get(Calendar.HOUR_OF_DAY));
                newTime.set(Calendar.MINUTE, oldTime.get(Calendar.MINUTE));
            }
            getSchEvent().setDurationEnd(newTime.getTime());
        }
        if (getGroupEvent().getBellScheduleEntry() != null)
        {
            Date date = getSchEvent().getDurationBegin();
            if (null == date)
                date = new Date(0);
            date = DateUtils.setHours(date, getGroupEvent().getBellScheduleEntry().getStartHour());
            date = DateUtils.setMinutes(date, getGroupEvent().getBellScheduleEntry().getStartMin());
            getSchEvent().setDurationBegin(date);
            date = getSchEvent().getDurationEnd();
            if (null == date)
                date = new Date(0);
            date = DateUtils.setHours(date, getGroupEvent().getBellScheduleEntry().getEndHour());
            date = DateUtils.setMinutes(date, getGroupEvent().getBellScheduleEntry().getEndMin());
            getSchEvent().setDurationEnd(date);
        }
    }

    public void doNothing()
    {

    }

    public String getPps()
    {
        return  UniStringUtils.join(IUniBaseDao.instance.get().getList(EppPpsCollectionItem.class, EppPpsCollectionItem.list().s(), getGroupEvent().getGroup(), EppPpsCollectionItem.pps().person().identityCard().fullFio().s()), EppPpsCollectionItem.pps().person().identityCard().fio().s(), ", ");
    }

    private String checkDate()
    {
        final Date moduleBeginDate = getGroupEvent().getJournalEvent().getJournalModule().getModuleBeginDate();
        boolean startError = null != moduleBeginDate && CoreDateUtils.getDayFirstTimeMoment(moduleBeginDate).after(getSchEvent().getDurationBegin());
        final Date moduleEndDate = getGroupEvent().getJournalEvent().getJournalModule().getModuleEndDate();
        boolean endError = null != moduleEndDate && CoreDateUtils.getDayLastTimeMoment(moduleEndDate).before(getSchEvent().getDurationEnd());
        if (startError && endError)
            return "Вы ввели дату начала события до даты начала обучения по модулю, и дату окончания события после окончания обучения по модулю. Сохранить событие с этими датами?";
        else if (startError)
            return "Вы ввели дату начала события до даты начала обучения по модулю. Сохранить событие с такой датой начала?";
        else if (endError)
            return "Вы ввели дату окончания события после окончания обучения по модулю. Сохранить событие с такой датой окончания?";
        return null;
    }

    // getters and setters

    public TrEduGroupEvent getGroupEvent()
    {
        return groupEvent;
    }

    public void setGroupEvent(TrEduGroupEvent groupEvent)
    {
        this.groupEvent = groupEvent;
    }

    public ScheduleEvent getSchEvent()
    {
        return schEvent;
    }

    public void setSchEvent(ScheduleEvent schEvent)
    {
        this.schEvent = schEvent;
    }

    public boolean isCopyEvent()
    {
        return copyEvent;
    }

    public void setCopyEvent(boolean copyEvent)
    {
        this.copyEvent = copyEvent;
    }

    public ISelectModel getSourceEventModel()
    {
        return sourceEventModel;
    }

    public void setSourceEventModel(ISelectModel sourceEventModel)
    {
        this.sourceEventModel = sourceEventModel;
    }

    public TrEduGroupEvent getSourceEvent()
    {
        return sourceEvent;
    }

    public void setSourceEvent(TrEduGroupEvent sourceEvent)
    {
        this.sourceEvent = sourceEvent;
    }

    public boolean isIncrementWeeks()
    {
        return incrementWeeks;
    }

    public void setIncrementWeeks(boolean incrementWeeks)
    {
        this.incrementWeeks = incrementWeeks;
    }

    public Integer getIncrementWeeksAmount()
    {
        return incrementWeeksAmount;
    }

    public void setIncrementWeeksAmount(Integer incrementWeeksAmount)
    {
        this.incrementWeeksAmount = incrementWeeksAmount;
    }

    public List<UniplacesPlace> getPlaceList()
    {
        return placeList;
    }

    public void setPlaceList(List<UniplacesPlace> placeList)
    {
        this.placeList = placeList;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public ISelectModel getEntryModel()
    {
        return entryModel;
    }

    public void setEntryModel(ISelectModel entryModel)
    {
        this.entryModel = entryModel;
    }
}
