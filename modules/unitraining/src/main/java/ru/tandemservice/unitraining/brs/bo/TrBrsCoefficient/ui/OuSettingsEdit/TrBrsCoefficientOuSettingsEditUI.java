/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuSettingsEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;

/**
 * @author oleyba
 * @since 8/17/12
 */
@Input({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="holder.id", required=true)
})
public class TrBrsCoefficientOuSettingsEditUI extends UIPresenter
{
    private EntityHolder<TrOrgUnitSettings> holder = new EntityHolder<TrOrgUnitSettings>();

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
    }

    public void onClickApply()
    {
        DataAccessServices.dao().save(getOu());
        deactivate();
    }

    public EntityHolder<TrOrgUnitSettings> getHolder()
    {
        return holder;
    }

    public TrOrgUnitSettings getOu()
    {
        return getHolder().getValue();
    }

    public void onEditSettings()
    {

    }
}
