package ru.tandemservice.unitraining.brs.entity.brs.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;

/**
 * Владелец настроек для БРС
 *
 * Интерфейс для объектов, которые могут владеть настройками БРС
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IBrsSettingsOwnerGen extends InterfaceStubBase
 implements IBrsSettingsOwner{
    public static final int VERSION_HASH = 2005116685;



    private static final Path<IBrsSettingsOwner> _dslPath = new Path<IBrsSettingsOwner>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner");
    }
            

    public static class Path<E extends IBrsSettingsOwner> extends EntityPath<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return IBrsSettingsOwner.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
