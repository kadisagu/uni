/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;

/**
 * @author oleyba
 * @since 8/5/11
 */
@Configuration
public class TrJournalEdit extends BusinessComponentManager
{
    public static final String GRADE_SCALE_DS = "gradeScaleDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(TrOrgUnitManager.instance().trOrgUnitPpsDSConfig())
        .addDataSource(selectDS("fcaGroupDS").handler(TrBrsCoefficientManager.instance().fcaGroupDSHandler()))
                .addDataSource(selectDS(GRADE_SCALE_DS, gradeScaleDSHandler()))
        .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> gradeScaleDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppGradeScale.class).order(EppGradeScale.title());
    }}
