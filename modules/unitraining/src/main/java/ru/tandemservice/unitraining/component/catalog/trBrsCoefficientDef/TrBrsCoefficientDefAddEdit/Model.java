/* $Id: modelAddEdit.vm 6177 2009-01-13 14:09:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unitraining.component.catalog.trBrsCoefficientDef.TrBrsCoefficientDefAddEdit;

import java.util.Collections;
import java.util.List;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;

import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;

/**
 * @author AutoGenerator
 * Created on 30.08.2012
 */
public class Model extends DefaultCatalogAddEditModel<TrBrsCoefficientDef>
{

    private List<TrBrsCoefficientOwnerType> ownerTypeList = Collections.emptyList();
    public List<TrBrsCoefficientOwnerType> getOwnerTypeList() { return this.ownerTypeList; }
    public void setOwnerTypeList(List<TrBrsCoefficientOwnerType> ownerTypeList) { this.ownerTypeList = ownerTypeList; }


}
