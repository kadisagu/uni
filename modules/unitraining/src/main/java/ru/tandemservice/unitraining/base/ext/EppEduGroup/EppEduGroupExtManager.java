/**
 *$Id$
 */
package ru.tandemservice.unitraining.base.ext.EppEduGroup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import ru.tandemservice.unitraining.base.ext.EppEduGroup.logic.EppEduGroupList4StudentIdsExtTrainingDSHandler;

/**
 * @author Alexander Shaburov
 * @since 17.07.13
 */
@Configuration
public class EppEduGroupExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IReadAggregateHandler<DSInput, DSOutput> groupList4StudentIdsDSHandler()
    {
        return new EppEduGroupList4StudentIdsExtTrainingDSHandler(getName());
    }
}
