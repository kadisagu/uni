package ru.tandemservice.unitraining.base.bo.TrOrgUnit.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class TrOrgUnitJournalDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(TrJournal.class, "e");
    }

    public TrOrgUnitJournalDSHandler(final String ownerId) {
        super(ownerId);
    }

    public static final String PARAM_ORG_UNIT = "orgUnit";
    public static final String PARAM_YEAR_PART = "yearPart";
    public static final String PARAM_NUMBER = "number";
    public static final String PARAM_PERIOD_DATE_FROM = "periodDateFrom";
    public static final String PARAM_PERIOD_DATE_TO = "periodDateTo";
    public static final String PARAM_ACTIVITY_ELEMENT = "registryElementPart";
    public static final String PARAM_STATE = "state";
    public static final String PARAM_RESPONSIBLE = "responsible";
    public static final String PARAM_TUTOR = "tutor";
    
    public static final String COLUMN_PPS = "pps";
    public static final String COLUMN_HAS_MARKS_ALERT = "hasMarksAlert";

    @Override
    protected DSOutput execute(final DSInput input, final ExecutionContext context) {
        final Session session = context.getSession();

        // обязательные параметры
        final OrgUnit orgUnit = context.get(TrOrgUnitJournalDSHandler.PARAM_ORG_UNIT);
        final EppYearPart yearPart = context.get(TrOrgUnitJournalDSHandler.PARAM_YEAR_PART);

        DQLSelectBuilder jDQL = registry.buildDQLSelectBuilder()
        .where(eq(property(TrJournal.registryElementPart().registryElement().owner().fromAlias("e")), value(orgUnit)))
        .where(eq(property(TrJournal.yearPart().fromAlias("e")), value(yearPart)));

        jDQL = this.applyFilters(jDQL, input, context);

        DSOutput output = new DSOutput(input);
        if (input.getEntityOrder().getKeyString().equals("number"))
        {
            jDQL.column(property("e.id"));
            jDQL.column(property(TrJournal.number().fromAlias("e")));
            Comparator<Object[]> comparator = output.getEntityOrder().getDirection() == OrderDirection.asc ? UniBaseDao.ID_VALUE_PAIR__NUMBER_AS_STRING__COMPARATOR_DIRECT : UniBaseDao.ID_VALUE_PAIR__NUMBER_AS_STRING__COMPARATOR_INDIRECT;

            List<Object[]> rows = jDQL.createStatement(session).list();
            Collections.sort(rows, comparator);

            final List<Long> ids = UniBaseUtils.getColumn(rows, 0);

            final int count = ids.size();
            output.setTotalSize(count);

            final List<Long> subIds = ids.subList(output.getStartRecord(), Math.min(output.getStartRecord() + output.getCountRecord(), count));
            output.getRecordList().addAll(CommonDAO.sort(IUniBaseDao.instance.get().getList(TrJournal.class, "id", subIds), subIds));
        }
        else
        {
            jDQL.column("e");
            registry.applyOrder(jDQL, input.getEntityOrder());
            output = DQLSelectOutputBuilder.get(input, jDQL, session).build();
        }

        final List<DataWrapper> wrappers = DataWrapper.wrap(output, "id", "title");
        for (final DataWrapper wrapper: wrappers) {
            wrapper.put(TrOrgUnitJournalDSHandler.COLUMN_PPS, StringUtils.EMPTY);
            wrapper.put(TrOrgUnitJournalDSHandler.COLUMN_HAS_MARKS_ALERT, StringUtils.EMPTY);
        }


        BatchUtils.execute(wrappers, 256, new BatchUtils.Action<DataWrapper>() {
            @Override public void execute(final Collection<DataWrapper> wrappers) {

                final Map<Long, DataWrapper> id2wrapper = CommonDAO.map(wrappers);

                // pps
                {
                    final List<Object[]> rows = new DQLSelectBuilder()
                    .fromEntity(TrJournalGroup.class, "j2g").where(in(property(TrJournalGroup.journal().id().fromAlias("j2g")), id2wrapper.keySet()))
                    .fromEntity(EppPpsCollectionItem.class, "pps2g").where(eq(property(EppPpsCollectionItem.list().fromAlias("pps2g")), property(TrJournalGroup.group().fromAlias("j2g"))))
                    .column(property(TrJournalGroup.journal().id().fromAlias("j2g")))
                    .column(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("pps2g")))
                    .predicate(DQLPredicateType.distinct)
                    .createStatement(session).list();

                    final Map<Long, Collection<String>> id2fio = SafeMap.get(SafeMap.<Long, Collection<String>>createCallback(ArrayList.class), id2wrapper.size());
                    for (final Object[] row: rows) {
                        final Long journalId = (Long)row[0];
                        final String fullFio = (String)row[1];
                        id2fio.get(journalId).add(fullFio);
                    }

                    for (final DataWrapper wrapper: wrappers) {
                        final Collection<String> fio = id2fio.get(wrapper.getId());
                        wrapper.put(TrOrgUnitJournalDSHandler.COLUMN_PPS, null == fio ? StringUtils.EMPTY : StringUtils.join(fio, '\n'));
                    }
                }
                
                // marks
                {
                    final List<Long> rows = new DQLSelectBuilder()
                        .fromEntity(TrJournalEvent.class, "e")
                        .where(in(property(TrJournalEvent.journalModule().journal().id().fromAlias("e")), id2wrapper.keySet()))
                        .where(eq(property(TrJournalEvent.hasMarks().fromAlias("e")), value(Boolean.TRUE)))
                        .column(property(TrJournalEvent.journalModule().journal().id().fromAlias("e")))
                        .createStatement(session).list();

                    for (final Long id: rows) {
                        id2wrapper.get(id).put(COLUMN_HAS_MARKS_ALERT, " В реализации уже выставлены оценки. При ее удалении они будут удалены без возможности восстановления.");
                    }
                }
            }
        });

        return output;
    }

    @SuppressWarnings("UnusedParameters")
    protected DQLSelectBuilder applyFilters(final DQLSelectBuilder dql, final DSInput input, final ExecutionContext context) {

        // номер
        {
            final String number = context.get(TrOrgUnitJournalDSHandler.PARAM_NUMBER);
            if (number != null) {
                dql.where(DQLExpressions.likeUpper(DQLExpressions.property(TrJournal.number().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(number))));
            }
        }

        // по пересечению периодов
        {
            final Date periodDateFrom = context.get(TrOrgUnitJournalDSHandler.PARAM_PERIOD_DATE_FROM);
            if (periodDateFrom != null) {
                dql.where(ge(property(TrJournal.durationEndDate().fromAlias("e")), valueDate(CoreDateUtils.getDayFirstTimeMoment(periodDateFrom))));
            }

            final Date periodDateTo = context.get(TrOrgUnitJournalDSHandler.PARAM_PERIOD_DATE_TO);
            if (periodDateTo != null) {
                dql.where(lt(property(TrJournal.durationBeginDate().fromAlias("e")), valueDate(CoreDateUtils.getNextDayFirstTimeMoment(periodDateTo, 1))));
            }
        }

        final EppRegistryElementPart activityElement = context.get(PARAM_ACTIVITY_ELEMENT);
        if (null != activityElement)
            dql.where(eq(property(TrJournal.registryElementPart().fromAlias("e")), value(activityElement)));

        final PpsEntry responsible = context.get(PARAM_RESPONSIBLE);
        if (null != responsible)
            dql.where(eq(property(TrJournal.responsible().fromAlias("e")), value(responsible)));

        final PpsEntry tutor = context.get(PARAM_TUTOR);
        if (null != tutor)
        {
            dql.joinEntity("e", DQLJoinType.inner, TrJournalGroup.class, "grpRel", eq(property(TrJournalGroup.journal().fromAlias("grpRel")), property("e")));
            dql.joinPath(DQLJoinType.inner, TrJournalGroup.group().fromAlias("grpRel"), "grp");
            dql.joinEntity("grp", DQLJoinType.inner, EppPpsCollectionItem.class, "ppsC", eq(property(EppPpsCollectionItem.list().fromAlias("ppsC")), property("grp")));
            dql.where(eq(property(EppPpsCollectionItem.pps().fromAlias("ppsC")), value(tutor)));
        }

        final EppState state = context.get(PARAM_STATE);
        if (null != state)
            dql.where(eq(property(TrJournal.state().fromAlias("e")), value(state)));

        return dql;
    }

}
