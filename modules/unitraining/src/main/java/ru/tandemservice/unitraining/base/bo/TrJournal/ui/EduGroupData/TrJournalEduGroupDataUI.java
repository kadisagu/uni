/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.EduGroupData;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.EduGroupLink.TrJournalEduGroupLink;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/12/11
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journal.id", required = true)
})
public class TrJournalEduGroupDataUI extends UIPresenter
{
    private TrJournal journal = new TrJournal();
    private StaticListDataSource<ViewWrapper<Student>> studentDataSource;

    @Override
    public void onComponentRefresh()
    {
        setJournal(IUniBaseDao.instance.get().get(TrJournal.class, getJournal().getId()));

        // получим список групп
        List<EppRealEduGroup> groups = new DQLSelectBuilder()
                .fromEntity(EppRealEduGroup.class, "grp")
                .column("grp")
                .where(exists(TrJournalGroup.class, TrJournalGroup.journal().s(), getJournal(), TrJournalGroup.group().s(), property("grp")))
                .createStatement(_uiSupport.getSession()).list();

        // получим список студентов
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppRealEduGroupRow.class, "grpRow")
                .joinPath(DQLJoinType.inner, EppRealEduGroupRow.group().fromAlias("grpRow"), "grp")
                .joinEntity("grp", DQLJoinType.inner, TrJournalGroup.class, "rel", eq(property(TrJournalGroup.group().fromAlias("rel")), property("grp")))
                .where(eq(property(TrJournalGroup.journal().fromAlias("rel")), value(journal)))
                .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("grpRow"))))
                .order(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().person().identityCard().fullFio().fromAlias("grpRow")))
                .column(property(EppRealEduGroupRow.group().id().fromAlias("grpRow")))
                .column(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().fromAlias("grpRow")));

        // заполним галочки для групп, в которых состоит студент
        Set<ViewWrapper<Student>> students = new LinkedHashSet<>();
        final Map<Long, Integer> studentCountMap = new HashMap<>();
        Map<Long, ViewWrapper<Student>> wrapperMap = new HashMap<>();
        for (Object[] row : dql.createStatement(_uiSupport.getSession()).<Object[]>list())
        {
            Long groupId = (Long) row[0];
            Student student = (Student) row[1];

            ViewWrapper<Student> wrapper = wrapperMap.get(student.getId());
            if (null == wrapper)
            {
                wrapper = new ViewWrapper<>(student);
                students.add(wrapper);
                wrapperMap.put(student.getId(), wrapper);
            }

            wrapper.setViewProperty(groupId.toString(), Boolean.TRUE);
            studentCountMap.put(groupId, UniBaseUtils.nullToZero(studentCountMap.get(groupId)) + 1);
        }

        // колонки списка со студентами
        studentDataSource = new StaticListDataSource<>();
        studentDataSource.addColumn(new PublisherColumnBuilder("Студент", Student.person().identityCard().fullFio(), "studentTab").tabBind(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY).build());

        // отсортируем группы по числу студентов
        Collections.sort(groups, (o1, o2) -> {
            int order = o1.getType().getPriority() - o2.getType().getPriority();
            if (0 == order)
                order = UniBaseUtils.nullToZero(studentCountMap.get(o2.getId())) - UniBaseUtils.nullToZero(studentCountMap.get(o1.getId()));
            return order;
        });

        // колонки списка со студентами - колонка с галочками
        final HeadColumn head = new HeadColumn("eduGroups", "Учебные группы");
        for (EppGroupType type : CommonBaseEntityUtil.<EppGroupType>getPropertiesSet(groups, EppRealEduGroup.type().s()))
        {
            final HeadColumn typeColumn = new HeadColumn(type.getCode(), type.getShortTitle());
            head.addColumn(typeColumn);
            for (final EppRealEduGroup group : groups)
            {
                if (!group.getType().equals(type))
                    continue;
                typeColumn.addColumn(new IndicatorColumn(group.getTitle(), group.getId().toString())
                {
                    final IndicatorColumn.Item itemYes = new IndicatorColumn.Item(IndicatorColumn.YES);

                    @Override
                    public IndicatorColumn.Item getContent(final IEntity entity)
                    {
                        try
                        {
                            if (Boolean.TRUE.equals(((ViewWrapper) entity).getViewProperty(group.getId().toString())))
                            {
                                return itemYes;
                            }
                        }
                        catch (final Throwable t)
                        {
                            // do nothing
                        }
                        return null;
                    }
                }.setImageHeader(false).setVerticalHeader(true).setWidth(1).setClickable(false).setOrderable(false));
            }
        }
        studentDataSource.addColumn(head.setWidth(1));

        // отсортируем студентов так, чтобы одинаковые группы галочек были ближе
        // для каждого студента составим число, бинарное представление которого образуют галочки присутствия в группе
        for (int i = 0; i < groups.size(); i++)
            for (ViewWrapper<Student> student : students)
            {
                Integer num = (Integer) student.getViewProperty("order");
                if (null == num) num = 0;
                if (Boolean.TRUE.equals(student.getViewProperty(groups.get(i).getId().toString())))
                    num = num + 2 ^ i;
                student.setViewProperty("order", num);
            }
        // и отсортируем по этому числу
        List<ViewWrapper<Student>> studentList = new ArrayList<>(students);
        Collections.sort(studentList, new CheckedFastPropertyComparator<>("order"));

        studentDataSource.setupRows(studentList);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(TrJournalManager.TR_GROUP_WITH_PPS_DS))
            dataSource.put(TrJournalManager.TR_JOURNAL_JOURNAL_KEY, journal);
    }

    public void onDeleteGroup()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
        _uiSupport.setRefreshScheduled(true);
    }

    public void onLinkEduGroup()
    {
        this._uiActivation.asRegionDialog(TrJournalEduGroupLink.class)
                .parameter(IUIPresenter.PUBLISHER_ID, this.getJournal().getId())
                .activate();
    }

    // presenter

    public boolean isLinkDisabled()
    {
        return getJournal().getState().getCode().equals(EppState.STATE_ARCHIVED);
    }

    public boolean isLinkNotArchived()
    {
        return !isLinkDisabled();
    }

    // accessors

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }

    public StaticListDataSource<ViewWrapper<Student>> getStudentDataSource()
    {
        return studentDataSource;
    }
}
