/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.EventThemeEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;

/**
 * @author oleyba
 * @since 8/10/11
 */
@Configuration
public class TrJournalEventThemeEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
                .addDataSource(TrJournalManager.instance().eppGroupTypeDSConfig())
                .create();
    }
}
