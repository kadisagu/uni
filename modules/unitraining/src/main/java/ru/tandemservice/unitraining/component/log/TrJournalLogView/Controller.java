/* $Id$ */
package ru.tandemservice.unitraining.component.log.TrJournalLogView;

import org.tandemframework.core.component.IBusinessComponent;

import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author oleyba
 * @since 9/19/11
 */
public class Controller extends ru.tandemservice.uni.component.log.EntityLogViewBase.Controller
{

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = component.getModel();
        model.setSettings(UniBaseUtils.getDataSettings(component, "logView.trJournal.filter."));
        this.getDao().prepare(model);
        this.prepareDataSource(component);
    }
}
