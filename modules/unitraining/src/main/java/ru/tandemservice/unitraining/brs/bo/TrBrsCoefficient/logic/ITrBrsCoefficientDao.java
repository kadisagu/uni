/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;

import java.util.Map;

/**
 * @author oleyba
 * @since 8/17/12
 */
public interface ITrBrsCoefficientDao extends INeedPersistenceSupport
{
    TrOrgUnitSettings doAddOuSettings(OrgUnit orgUnit, EppYearPart yearPart);

    public void doSaveCoefficients(IBrsSettingsOwner owner, Map<TrBrsCoefficientDef, Object> values);

    /**
     * Для реализации можно указывать тип ФИК для уточнения мероприятия в сессии,
     * в котором должен использоваться рейтинг, набранный студентами в рамках данной реализации.
     * Это можно делать, если по части дисциплины предусмотрены и мероприятие типа «Зачет», и мероприятие типа «Экзамен»,
     * и в настройках БРС, определенных для данной реализации, включено использование БРС при выставлении оценки в сессии.
     * @param journal реализация дисциплины
     * @return нужно ли указывать тип ФИК
     */
    boolean isCanEditFcaGroup(TrJournal journal);

    /**
     * По мероприятию с данным типом ФИК должен быть учтен рейтинг, набранный студентами в рамках данной реализации.
     * Возвращает тип ФИК по реестру, если он единственный для части дисциплины реестра
     * (т.е. есть только мероприятие типа «Зачет» или мероприятие типа «Экзамен»).
     * Иначе возвращает заполенный в реализации тип.
     * Никогда не возвращает null: кидает ApplicationException с указанием причины, почему он получился.
     * @param journal реализация дисциплины
     * @return тип ФИК
     */
    EppFControlActionGroup getFcaGroup(TrJournal journal);
}
