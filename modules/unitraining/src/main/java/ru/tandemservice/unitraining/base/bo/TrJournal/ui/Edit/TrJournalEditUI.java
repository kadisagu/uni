/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.Edit;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.EditBase.TrBrsCoefficientEditBaseUI;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;

import java.util.List;

/**
 * @author oleyba
 * @since 8/5/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true),
    @Bind(key = TrJournalEditUI.BIND_ALLOW_EDIT_RESPONSIBLE_PPS, binding = "allowEditResponsiblePps")
})
@Output({
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_HOLDER, binding = "holder", required = true),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType", required = true)
})
public class TrJournalEditUI extends UIPresenter
{
    public static final String BIND_ALLOW_EDIT_RESPONSIBLE_PPS = "allowEditResponsiblePps";

    private EntityHolder<TrJournal> holder = new EntityHolder<>();
    private TrBrsCoefficientOwnerType ownerType = new TrBrsCoefficientOwnerType();
    private PpsEntrySelectBlockData ppsData;
    boolean allowEditResponsiblePps = true;

    @Override
    public void onComponentRefresh() {
        getHolder().refresh();
        ICommonDAO dao = DataAccessServices.dao();
        setOwnerType(dao.get(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerType.code(), TrBrsCoefficientOwnerTypeCodes.JOURNAL));

        TrJournal journal = getJournal();
        PpsEntrySelectBlockData ppsData = new PpsEntrySelectBlockData(journal.getOrgUnit().getId()).setInitiallySelectedPps(journal.getResponsible());
        ppsData.setPpsFieldTitle("Ответственный преподаватель");
        ppsData.setPpsSourceFilterFieldTitle("Выбор ответственного преподавателя");
        setPpsData(ppsData);

        if(journal.getGradeScale() == null)
        {
            List<EppGradeScale> scales = TrJournalManager.instance().dao().getGradeScales(journal);
            if (CollectionUtils.isNotEmpty(scales))
            {
                journal.setGradeScale(scales.get(0));
                dao.update(journal);
            }
        }

        RenderComponent.deactivateAll();
    }

    public void onClickApply() {
        DataAccessServices.dao().doInTransaction(session -> {
            getJournal().setResponsible(getPpsList());
            IUniBaseDao.instance.get().saveOrUpdate(getJournal());
            ((TrBrsCoefficientEditBaseUI) _uiSupport.getChildrenUI().entrySet().iterator().next().getValue()).save();
            return getJournal();
        });
        deactivate();
    }

    public boolean isCanEditBrsCoefficient()
    {
        return getJournal().getState().isFormative();
    }

    public boolean isCanEditFcaGroup()
    {
        return TrBrsCoefficientManager.instance().coefDao().isCanEditFcaGroup(getJournal());
    }

    // accessors

    public TrJournal getJournal()
    {
        return getHolder().getValue();
    }

    public EntityHolder<TrJournal> getHolder()
    {
        return holder;
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(TrBrsCoefficientOwnerType ownerType)
    {
        this.ownerType = ownerType;
    }

    public boolean isAllowEditResponsiblePps()
    {
        return allowEditResponsiblePps;
    }

    public void setAllowEditResponsiblePps(boolean allowEditResponsiblePps)
    {
        this.allowEditResponsiblePps = allowEditResponsiblePps;
    }

    public PpsEntrySelectBlockData getPpsData()
    {
        return ppsData;
    }

    public void setPpsData(PpsEntrySelectBlockData ppsData)
    {
        this.ppsData = ppsData;
    }

    public PpsEntry getPpsList()
    {
        return getPpsData().getSingleSelectedPps();
    }

    public void setPpsList(final List<PpsEntry> ppsList)
    {
        getPpsData().setSelectedPps(ppsList);
    }


}
