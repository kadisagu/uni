package ru.tandemservice.unitraining.brs.entity.catalogs;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.unitraining.brs.entity.catalogs.gen.TrBrsCoefficientDefGen;

/**
 * Коэффициент для БРС
 */
public class TrBrsCoefficientDef extends TrBrsCoefficientDefGen implements IBrsCoefficientDef
{
    public TrBrsCoefficientDef() {
    }

    public TrBrsCoefficientDef(IBrsCoefficientDef def) {
        super();
        setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(TrBrsCoefficientDef.class));
        setUserCode(def.getUserCode());
        setTitle(def.getTitle());
        setShortTitle(def.getShortTitle());
        setOwnerType(def.getOwnerType());
        setBoolValueType(def.isBoolValueType());
        setRequired(def.isRequired());
    }
}