/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalGroup;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unitraining.base.bo.TrHomePage.TrHomePageManager;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalMark.TrHomePageJournalMark;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditUI;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.ScheduleEventEdit.TrJournalScheduleEventEdit;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.StructureView.ITrJournalStructureOwner;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.StructureView.TrJournalStructureViewUI;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.*;

/**
 * @author oleyba
 * @since 8/30/11
 */
@Output({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journalId"),
    @Bind(key = TrJournalStructureViewUI.GROUP_TYPE_BIND, binding = "groupTypeId"),
    @Bind(key = TrJournalGroupMarkEditUI.GROUP_BINDING, binding = "groupId")
})
public class TrHomePageJournalGroupUI extends UIPresenter implements ITrJournalStructureOwner
{
    private TrJournalGroup group;
    private boolean empty = true;
    private Map<Long, TrEduGroupEvent> groupEventMap = new HashMap<>();

    @Override
    public void onComponentRefresh()
    {
        empty = ! TrHomePageManager.instance().dao().hasJournalsForMarking(_uiConfig.getUserContext().getPrincipalContext());
        if (!empty)
            onClickSelect();
        for (IUIPresenter child : _uiSupport.getChildrenUI().values())
            child.onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(TrHomePageManager.TR_HOME_PAGE_PERSON_KEY, PersonManager.instance().dao().getPerson(_uiConfig.getUserContext().getPrincipalContext()));
        dataSource.put(TrHomePageManager.TR_HOME_PAGE_YEAR_PART_KEY, _uiSettings.get("yearPart"));
        dataSource.put(TrHomePageManager.TR_HOME_PAGE_ACTIVITY_ELEMENT_PART_KEY, _uiSettings.get("registryElementPart"));
        dataSource.put(TrHomePageManager.TR_HOME_PAGE_GROUP_TYPE_KEY, _uiSettings.get("loadType"));
    }

    public void onClickSelect()
    {
        saveSettings();

        Object group = _uiSettings.get("eduGroup");
        setGroup(group instanceof TrJournalGroup ? (TrJournalGroup) group : null);
        if (getGroup() == null)
            return;
        
        fillGroupEventMap();

        final IUIPresenter trJournalStructureView = _uiSupport.getChildUI(RenderComponent.getRegionName("TrJournalGroupMarkEdit"));
        if (null != trJournalStructureView)
            trJournalStructureView.deactivate();
    }

    public boolean isEmpty()
    {
        return empty;
    }

    public boolean isSelected()
    {
        return getJournalId() != null && getGroupTypeId() != null;
    }

    @Override
    public String getPermissionKey()
    {
        return "";
    }

    @Override
    public boolean isEditable()
    {
        return true;
    }

    @Override
    public List<IButtonInfo> getEventButtonList()
    {
        return Arrays.<IButtonInfo>asList(
            new ButtonInfo("dateEdit", "Редактировать даты", "img/general/edit_date.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    final TrEduGroupEvent groupEvent = getGroupEvent(entityId);
                    if (groupEvent == null)
                        throw new ApplicationException("Состав событий в журнале изменился, обновите страницу.");
                    _uiActivation.asRegionDialog(TrJournalScheduleEventEdit.class)
                    .parameter(IUIPresenter.PUBLISHER_ID, groupEvent.getId())
                    .activate();
                }
            },
            new ButtonInfo("mark", "Выставить отметки", "img/general/edit_mark.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    _uiActivation.asDesktopRoot(TrHomePageJournalMark.class)
                    .parameter(TrJournalGroupMarkEditUI.GROUP_BINDING, getGroupId())
                    .parameter(TrJournalGroupMarkEditUI.EVENT_BINDING, entityId)
                    .activate();
                }

                @Override
                public boolean isDisabled(Long rowEntityId)
                {
                    final TrEduGroupEvent groupEvent = getGroupEvent(rowEntityId);
                    return groupEvent == null || groupEvent.getScheduleEvent() == null;
                }
            }
        );
    }

    private TrEduGroupEvent getGroupEvent(Long journalEventId)
    {
        return groupEventMap.get(journalEventId);
    }

    private void fillGroupEventMap()
    {
        groupEventMap.clear();
        final List<TrEduGroupEvent> list = IUniBaseDao.instance.get().getList(TrEduGroupEvent.class, TrEduGroupEvent.group().s(), getGroup().getGroup());
        for (TrEduGroupEvent event : list)
            groupEventMap.put(event.getJournalEvent().getId(), event);
    }


    @Override
    public List<IButtonInfo> getModuleButtonList()
    {
        return Collections.emptyList();
    }

    public void setGroup(TrJournalGroup group)
    {
        this.group = group;
    }

    public TrJournalGroup getGroup()
    {
        return group;
    }

    public Long getJournalId()
    {
        return getGroup() == null ? null : getGroup().getJournal().getId();
    }

    public Long getGroupTypeId()
    {
        return getGroup() == null ? null : getGroup().getGroup().getType().getId();
    }

    public Long getGroupId()
    {
        return getGroup() == null ? null : getGroup().getId();
    }
}
