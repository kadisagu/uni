/* $Id:$ */
package ru.tandemservice.unitraining.brs.dao;

import org.apache.commons.collections15.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniScriptDao;
import ru.tandemservice.uni.util.groovy.IScriptInstance;
import ru.tandemservice.uni.util.groovy.UniScriptLoader;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsAttestationScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalGroupScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalScriptFunctions;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsSessionScriptFunctions;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;
import ru.tandemservice.unitraining.brs.entity.catalogs.IBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;

import java.util.*;

/**
 * @author oleyba
 * @since 1/15/13
 */
public class BrsScriptDao extends UniScriptDao implements IBrsScriptDao
{
    public static final String DEFAULT_GLOBAL_RATING_SCRIPT = "scripts/DefaultGlobalRatingScript.groovy";

    @Override
    public void doSaveUserScript(final TrOrgUnitSettings settings) {
        super.doSaveUserScript(settings);
        try {
            this.save(settings);
            createMissingCoefficients(settings);
        } finally {
            UniScriptLoader.reset(); // сбрасываем loader (ВСЕГДА)
        }
    }

    @Override
    public TrOrgUnitSettings getOrgUnitBrsSettings(final EppYearPart yearPart, final OrgUnit tutorOu, final OrgUnit groupOu)
    {
        final List<TrOrgUnitSettings> settings = this.getOrgUnitBrsSettingsCascade(yearPart, tutorOu, groupOu);
        if (settings.isEmpty()) { return null; }
        return settings.get(0);
    }

    @Override
    public IBrsScriptWrapper<IBrsJournalScriptFunctions> getJournalScript(final TrJournal journal)
    {
        final List<TrOrgUnitSettings> settings = this.getOrgUnitBrsSettingsCascade(journal.getYearPart(), journal.getRegistryElementPart().getTutorOu(), null);

        for (final TrOrgUnitSettings ouSettings : settings) {
            if (this.hasUserScript(ouSettings)) {
                return this.buildLocalScriptWrapper(ouSettings, IBrsJournalScriptFunctions.class);
            }
        }

        return this.buildDefaultGlobalScriptWrapper(journal.getYearPart(), IBrsJournalScriptFunctions.class);
    }

    @Override
    public IBrsScriptWrapper<IBrsJournalGroupScriptFunctions> getJournalGroupScript(final TrJournal journal, final EppStudentWorkPlanElement slot)
    {
        final List<TrOrgUnitSettings> settings = this.getOrgUnitBrsSettingsCascade(journal.getYearPart(), journal.getRegistryElementPart().getTutorOu(), slot.getStudent().getEducationOrgUnit().getGroupOrgUnit());

        for (final TrOrgUnitSettings ouSettings : settings) {
            if (this.hasUserScript(ouSettings)) {
                return this.buildLocalScriptWrapper(ouSettings, IBrsJournalGroupScriptFunctions.class);
            }
        }

        return this.buildDefaultGlobalScriptWrapper(journal.getYearPart(), IBrsJournalGroupScriptFunctions.class);
    }

    @Override
    public IBrsScriptWrapper<IBrsAttestationScriptFunctions> getAttestationScript(final EppStudentWorkPlanElement slot)
    {
        final EppYearPart yearPart = slot.getEppYearPart();
        final List<TrOrgUnitSettings> settings = this.getOrgUnitBrsSettingsCascade(yearPart, slot.getTutorOu(), slot.getStudent().getEducationOrgUnit().getGroupOrgUnit());

        for (final TrOrgUnitSettings ouSettings : settings) {
            if (this.hasUserScript(ouSettings)) {
                return this.buildLocalScriptWrapper(ouSettings, IBrsAttestationScriptFunctions.class);
            }
        }

        return this.buildDefaultGlobalScriptWrapper(yearPart, IBrsAttestationScriptFunctions.class);
    }

    @Override
    public IBrsScriptWrapper<IBrsSessionScriptFunctions> getSessionScript(final EppStudentWorkPlanElement slot)
    {
        return this.getSessionScript(slot.getEppYearPart(), slot.getStudent().getEducationOrgUnit().getGroupOrgUnit(), slot.getTutorOu());
    }

    @Override
    public IBrsScriptWrapper<IBrsSessionScriptFunctions> getSessionScript(final EppYearPart yearPart, final OrgUnit groupOu, final OrgUnit tutorOu)
    {
        final List<TrOrgUnitSettings> settings = this.getOrgUnitBrsSettingsCascade(yearPart, tutorOu, groupOu);

        for (final TrOrgUnitSettings ouSettings : settings) {
            if (this.hasUserScript(ouSettings)) {
                return this.buildLocalScriptWrapper(ouSettings, IBrsSessionScriptFunctions.class);
            }
        }

        return this.buildDefaultGlobalScriptWrapper(yearPart, IBrsSessionScriptFunctions.class);
    }

    private List<TrOrgUnitSettings> getOrgUnitBrsSettingsCascade(final EppYearPart yearPart, final OrgUnit tutorOu, final OrgUnit groupOu)
    {
        final MultiKey<Object> cacheKey = new MultiKey<Object>(
        "trOuSettings",
        (yearPart == null ? null : yearPart.getId()),
        (tutorOu == null ? null : tutorOu.getId()),
        (groupOu == null ? null : groupOu.getId())
        );

        List<TrOrgUnitSettings> settings = DaoCache.get(cacheKey);
        if (null != settings) {
            return settings;
        }

        if (null == yearPart) {
            DaoCache.put(cacheKey, settings = Collections.emptyList());
            return settings;
        }

        settings = new ArrayList<>();
        TopOrgUnit topOrgUnitProxy = (TopOrgUnit) getSession().load(TopOrgUnit.class, OrgUnitManager.instance().dao().getTopOrgUnitId());
        for (final OrgUnit ou : new OrgUnit[] { tutorOu, groupOu, topOrgUnitProxy}) {
            if (null == ou) { continue; }
            final TrOrgUnitSettings ouSettings = this.getByNaturalId(new TrOrgUnitSettings.NaturalId(ou, yearPart));
            if (null != ouSettings) {
                settings.add(ouSettings);
            }
        }
        DaoCache.put(cacheKey, settings);
        return settings;
    }

    protected boolean hasUserScript(final TrOrgUnitSettings ouSettings) {
        return (null != ouSettings) && StringUtils.isNotBlank(ouSettings.getUserScript());
    }

    public <S extends IScriptInstance> IBrsScriptWrapper<S> buildDefaultGlobalScriptWrapper(final EppYearPart yearPart, final Class<S> klass)
    {
        final TrOrgUnitSettings ouSettings = this.getByNaturalId(new TrOrgUnitSettings.NaturalId(TopOrgUnit.getInstance(), yearPart));

        // грузим (скрипт возвращает экземпляр класса-обработчика)
        return this.buildLocalScriptWrapper(ouSettings, klass);
    }

    protected <S extends IScriptInstance> IBrsScriptWrapper<S> buildLocalScriptWrapper(TrOrgUnitSettings ouSettings, Class<S> klass)
    {
        final String configKey = "rating_script_" + (null == ouSettings ? "null" : (ouSettings.getOrgUnit().getId() + "_" + ouSettings.getEduYearPart().getYear().getEducationYear().getIntValue() + "_" + ouSettings.getEduYearPart().getPart().getNumber()));
        final String scriptTitle = (null == ouSettings) ? "Скрипт по умолчанию (без настроек)" : "Скрипт " + ouSettings.getDisplayableTitle() + " " + ouSettings.getEduYearPart().getTitle();
        final String defScriptPath = BrsScriptDao.findScriptPath(DEFAULT_GLOBAL_RATING_SCRIPT);

        // wrap to wrap
        final IScriptWrapper<S, TrOrgUnitSettings> wrapper = this.buildScriptWrapper(ouSettings, klass, configKey, defScriptPath, scriptTitle);
        return new IBrsScriptWrapper<S>() {
            @Override public S getScript() { return wrapper.getScript();}
            @Override public TrOrgUnitSettings getScriptOwner() { return wrapper.getScriptOwner(); }
        };
    }

    /**
     * Сохраняем недостающие коэффициенты из скрипта.
     * @param settings настройки со скриптом, которые сохраняются
     */
    private void createMissingCoefficients(TrOrgUnitSettings settings) {
        try {
            IBrsScriptWrapper<IBrsJournalScriptFunctions> journalScript = buildLocalScriptWrapper(settings, IBrsJournalScriptFunctions.class);
            Map<String, TrBrsCoefficientDef> defMap = new HashMap<>();
            for (TrBrsCoefficientDef def : getList(TrBrsCoefficientDef.class)) {
                defMap.put(def.getUserCode(), def);
            }
            for (IBrsCoefficientDef def : journalScript.getScript().getCoefficientDefList()) {
                TrBrsCoefficientDef existing = defMap.get(def.getUserCode());
                if (null != existing) {
                    if (!existing.getOwnerType().equals(def.getOwnerType()))
                        throw new ApplicationException("В скрипте определен коэффициент, который уже есть в справочнике (совпадает пользовательский код) и при этом тип владельца не совпадает с указанным в справочнике. Измените определение или используйте другой код коэффициента.");
                    if (!existing.isBoolValueType() == (def.isBoolValueType()))
                        throw new ApplicationException("В скрипте определен коэффициент, который уже есть в справочнике (совпадает пользовательский код) и при этом тип коэффициента (булевский или численный) не совпадает с указанным в справочнике. Измените определение или используйте другой код коэффициента.");
                }
                else {
                    // нет коэффициента - создаем
                    TrBrsCoefficientDef newDef = new TrBrsCoefficientDef(def);
                    save(newDef);
                    try {
                        ContextLocal.getInfoCollector().add("В справочник добавлен коэффициент " + def.getTitle());
                    }
                    catch (Exception e) {
                        // ну и фиг с ним
                    }
                }
            }
        }
        catch (ClassCastException e) {
            // не скрипт журнала - ничего не делаем
        }
    }

}
