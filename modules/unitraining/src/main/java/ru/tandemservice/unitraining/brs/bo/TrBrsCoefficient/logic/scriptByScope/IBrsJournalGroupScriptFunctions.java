/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope;

import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.groovy.IScriptInstance;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;

/**
 * Группа методов скрипта для работы журналов.
 * @author oleyba
 * @since 8/24/12
 */
public interface IBrsJournalGroupScriptFunctions extends IScriptInstance
{
    /**
     * Позволяет провалидировать балл, который преподаватель выставляет студенту по событию, в соответствии с логикой скрипта и коэффициентами БРС.
     * @param errors коллектор ошибок валидации
     * @param student студент
     * @param event событие
     * @param mark балл и другие параметры отметки
     * @param brsSettings коэффициенты БРС
     * @return true, если балл корректен, false в противном случае
     */
    boolean validateMark(ErrorCollector errors, Student student, TrEduGroupEvent event, ITrJournalEventDao.IJournalMarkData mark, IBrsDao.IBrsPreparedSettings brsSettings);
}
