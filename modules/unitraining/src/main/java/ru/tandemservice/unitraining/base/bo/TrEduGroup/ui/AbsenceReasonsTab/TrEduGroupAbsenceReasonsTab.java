/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrEduGroup.ui.AbsenceReasonsTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unitraining.base.bo.TrEduGroup.TrEduGroupManager;
import ru.tandemservice.unitraining.base.bo.TrEduGroup.logic.TrEduGroupAbsenceDSHandler;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;

/**
 * @author oleyba
 * @since 9/23/11
 */
@Configuration
public class TrEduGroupAbsenceReasonsTab extends BusinessComponentManager
{
    public static final String TR_EDU_GROUP_ABSENCE_DS = "trEduGroupAbsenceDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(this.searchListDS(TrEduGroupAbsenceReasonsTab.TR_EDU_GROUP_ABSENCE_DS, this.trEduGroupAbsenceDS(), this.trEduGroupAbsenceDSHandler()))
        .addDataSource(TrEduGroupManager.instance().trEduGroupAbsentStudentDSConfig())
        .create();
    }

    @Bean
    public ColumnListExtPoint trEduGroupAbsenceDS() {
        return this.columnListExtPointBuilder(TrEduGroupAbsenceReasonsTab.TR_EDU_GROUP_ABSENCE_DS)
        .addColumn(checkboxColumn("select").create())
        .addColumn(textColumn("eventTitle", TrEduGroupEventStudent.event().journalEvent().s() + ".title").create())
        .addColumn(textColumn("eventTime", TrEduGroupEventStudent.event().scheduleEvent().durationBegin()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).create())
        .addColumn(textColumn("absenceReason", TrEduGroupEventStudent.absenceReason().title().s()).create())
        .addColumn(booleanColumn("valid").path(TrEduGroupEventStudent.absenceReason().valid().s()).create())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trEduGroupAbsenceDSHandler() {
        return new TrEduGroupAbsenceDSHandler(this.getName());
    }
}
