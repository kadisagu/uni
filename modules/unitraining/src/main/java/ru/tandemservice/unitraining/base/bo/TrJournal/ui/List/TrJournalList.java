/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.EducationLevelHighSchoolDSHandler;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.TrJournalListDSHandler;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * @author Denis Katkov
 * @since 10.02.2016
 */
@Configuration
public class TrJournalList extends BusinessComponentManager
{
    public static final String TR_JOURNAL_LIST_DS = "trJournalListDS";
    public static final String TR_YEAR_PART_DS = "trYearPartDS";
    public static final String TR_FORMATIVE_DS = "formativeOrgUnitDS";
    public static final String EDUCATION_LEVEL_HIGH_SCHOOL_DS = "educationLevelHighSchoolDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(TrJournalList.TR_JOURNAL_LIST_DS, trJournalListDSColumns(), trJournalListDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(TR_YEAR_PART_DS, getName(), EppYearPart.defaultSelectDSHandler(getName())))
                .addDataSource(TrJournalManager.instance().trOwnerDSConfig())
                .addDataSource(TrJournalManager.instance().trRegistryElementPartDSConfig())
                .addDataSource(TrJournalManager.instance().trResponsibleDSConfig())
                .addDataSource(TrJournalManager.instance().trPpsDSConfig())
                .addDataSource(EppStateManager.instance().eppStateDSConfig())
                .addDataSource(selectDS(TR_FORMATIVE_DS, UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
                .addDataSource(selectDS(EDUCATION_LEVEL_HIGH_SCHOOL_DS, educationLevelHighSchoolDSHandler()).addColumn(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY))
                .create();
    }

    @Bean
    public ColumnListExtPoint trJournalListDSColumns()
    {
        return columnListExtPointBuilder(TR_JOURNAL_LIST_DS)
                .addColumn(textColumn(TrJournal.P_NUMBER, TrJournal.number()).width("1").order())
                .addColumn(publisherColumn(TrJournal.L_REGISTRY_ELEMENT_PART, TrJournal.registryElementPart().titleWithNumber()).order())
                .addColumn(textColumn(TrJournal.L_YEAR_PART, TrJournal.yearPart().title()))
                .addColumn(dateColumn(TrJournal.P_DURATION_BEGIN_DATE, TrJournal.durationBeginDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(dateColumn(TrJournal.P_DURATION_END_DATE, TrJournal.durationEndDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn(EppRegistryElement.L_OWNER, TrJournal.registryElementPart().registryElement().owner().shortTitle()).order())
                .addColumn(textColumn(TrJournal.L_RESPONSIBLE, TrJournal.responsible().fio()))
                .addColumn(textColumn("pps", TrJournalListDSHandler.COLUMN_PPS).formatter(UniEppUtils.NEW_LINE_FORMATTER))
                .addColumn(textColumn(TrJournal.L_STATE, TrJournal.state().title()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("trJournalListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                        alert(TR_JOURNAL_LIST_DS + ".delete.alert", TrJournal.registryElementPart().title())).permissionKey("trJournalListDelete"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trJournalListDSHandler()
    {
        return new TrJournalListDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationLevelHighSchoolDSHandler()
    {
        return new EducationLevelHighSchoolDSHandler(getName());
    }
}