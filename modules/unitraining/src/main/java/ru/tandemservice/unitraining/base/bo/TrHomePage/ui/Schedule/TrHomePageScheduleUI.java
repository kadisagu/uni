/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage.ui.Schedule;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unitraining.base.bo.TrHomePage.logic.TrHomePageScheduleDSHandler;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalMark.TrHomePageJournalMark;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditUI;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

/**
 * @author oleyba
 * @since 9/22/11
 */
public class TrHomePageScheduleUI extends UIPresenter
{
    private static final String FILTER_DATE_END = TrHomePageScheduleDSHandler.PARAM_END_DATE;
    private static final String FILTER_DATE_START = TrHomePageScheduleDSHandler.PARAM_START_DATE;

    // actions

    @Override
    public void onComponentRefresh()
    {
        if (_uiSettings.get(FILTER_DATE_START) == null)
            _uiSettings.set(FILTER_DATE_START, new Date());
        if (_uiSettings.get(FILTER_DATE_END) == null)
            _uiSettings.set(FILTER_DATE_END, new Date());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (TrHomePageSchedule.TR_HOME_PAGE_SCHEDULE_DS.equals(dataSource.getName()))
        {
            final Person person = PersonManager.instance().dao().getPerson(_uiConfig.getUserContext().getPrincipalContext());

            dataSource.put(TrHomePageScheduleDSHandler.PARAM_PERSON, person);

            final Map<String, Object> settingMap = this._uiSettings.getAsMap(
                    TrHomePageScheduleDSHandler.PARAM_START_DATE,
                    TrHomePageScheduleDSHandler.PARAM_END_DATE
            );
            dataSource.putAll(settingMap);
        }
    }

    @Override
    public void clearSettings()
    {
        super.clearSettings();
        _uiSettings.set(FILTER_DATE_START, new Date());
        _uiSettings.set(FILTER_DATE_END, new Date());
        _uiSettings.save();
    }

    public void onEditMark()
    {
        TrEduGroupEvent event = IUniBaseDao.instance.get().get(TrEduGroupEvent.class, getListenerParameterAsLong());
        final TrJournalGroup group = getGroup(event);
        if (event == null || group == null)
            return;
        _uiActivation.asDesktopRoot(TrHomePageJournalMark.class)
            .parameter(TrJournalGroupMarkEditUI.GROUP_BINDING, group.getId())
            .parameter(TrJournalGroupMarkEditUI.EVENT_BINDING, event.getJournalEvent().getId())
            .activate();
    }

    public void onPrint()
    {
        this._uiActivation.asDesktopRoot(TrHomePageSchedule.class).printDisplayMode(true)
        .activate();
    }

    // presenter

    public String getDateStart()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) _uiSettings.get(FILTER_DATE_START));
    }

    public String getDateEnd()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) _uiSettings.get(FILTER_DATE_END));
    }

    public boolean isEditColumnVisible()
    {
        return !_uiConfig.getBusinessComponent().getDesktop().isPrintDisplayMode();
    }

    // data access

    private TrJournalGroup getGroup(TrEduGroupEvent event)
    {
        if (event == null)
            return null;
        List<TrJournalGroup> groups = new DQLSelectBuilder()
            .fromEntity(TrJournalGroup.class, "jg")
            .where(eq(property(TrJournalGroup.group().fromAlias("jg")), value(event.getGroup())))
            .where(eq(property(TrJournalGroup.journal().fromAlias("jg")), value(event.getJournalEvent().getJournalModule().getJournal())))
            .createStatement(_uiSupport.getSession()).list();
        return groups.isEmpty() ? null : groups.get(0);
    }

}
