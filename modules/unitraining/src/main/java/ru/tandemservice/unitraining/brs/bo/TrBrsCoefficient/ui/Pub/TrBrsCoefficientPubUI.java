/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.Pub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.IBrsCoefficientRowWrapper;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.Edit.TrBrsCoefficientEdit;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;

/**
 * @author oleyba
 * @since 8/20/12
 */
@Input({
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_ID, binding = "ownerHolder.id", required = true),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType.id", required = true)
})
public class TrBrsCoefficientPubUI extends UIPresenter
{
    private EntityHolder<IBrsSettingsOwner> ownerHolder = new EntityHolder<IBrsSettingsOwner>();
    private TrBrsCoefficientOwnerType ownerType = new TrBrsCoefficientOwnerType();

    private List<IBrsCoefficientRowWrapper> rowList;
    private RowWrapper currentRow;

    @Override
    public void onComponentRefresh()
    {
        getOwnerHolder().refresh();
        setOwnerType(DataAccessServices.dao().get(TrBrsCoefficientOwnerType.class, getOwnerType().getId()));

        List<IBrsCoefficientRowWrapper> wrappers = new ArrayList<>();
        Map<TrBrsCoefficientDef, TrBrsCoefficientValue> values = new HashMap<TrBrsCoefficientDef, TrBrsCoefficientValue>();
        for (TrBrsCoefficientValue value : DataAccessServices.dao().getList(TrBrsCoefficientValue.class, TrBrsCoefficientValue.owner().s(), getOwnerHolder().getValue()))
            values.put(value.getDefinition(), value);
        for (TrBrsCoefficientDef def : TrBrsCoefficientManager.instance().brsDao().getFilteredCoefficientDefList(getOwner(), getOwnerType()))
            wrappers.add(new RowWrapper(def, values.get(def)));
        setRowList(wrappers);
    }

    public void onEditCoefficient()
    {
        _uiActivation.asRegionDialog(TrBrsCoefficientEdit.class)
        .parameter(TrBrsCoefficientManager.BIND_OWNER_ID, getOwnerHolder().getId())
        .parameter(TrBrsCoefficientManager.BIND_OWNER_TYPE, getOwnerType().getId())
        .activate();
    }

    public IBrsSettingsOwner getOwner()
    {
        return getOwnerHolder().getValue();
    }

    public EntityHolder<IBrsSettingsOwner> getOwnerHolder()
    {
        return ownerHolder;
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(TrBrsCoefficientOwnerType ownerType)
    {
        this.ownerType = ownerType;
    }

    public List<IBrsCoefficientRowWrapper> getRowList()
    {
        return rowList;
    }

    public void setRowList(List<IBrsCoefficientRowWrapper> rowList)
    {
        this.rowList = rowList;
    }

    public RowWrapper getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(RowWrapper currentRow)
    {
        this.currentRow = currentRow;
    }

    private static class RowWrapper extends IdentifiableWrapper<TrBrsCoefficientDef> implements IBrsCoefficientRowWrapper
    {
        private TrBrsCoefficientDef def;
        private TrBrsCoefficientValue value;

        private RowWrapper(TrBrsCoefficientDef def, TrBrsCoefficientValue value)
        {
            super(def.getId(), def.getShortTitle());
            this.def = def;
            this.value = value;
        }

        public TrBrsCoefficientValue getValue()
        {
            return value;
        }

        public TrBrsCoefficientDef getDef()
        {
            return def;
        }
    }
}
