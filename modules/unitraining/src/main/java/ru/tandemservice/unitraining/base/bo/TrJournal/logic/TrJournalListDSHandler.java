/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.bean.IFastBeanOwner;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DelegatePropertyComparator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Denis Katkov
 * @since 10.02.2016
 */
public class TrJournalListDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String COLUMN_PPS = "pps";
    public static final String PARAM_ORG_UNIT = "orgUnit";
    public static final String PARAM_YEAR_PART = "yearPart";
    public static final String PARAM_NUMBER = "number";
    public static final String PARAM_PERIOD_DATE_FROM = "periodDateFrom";
    public static final String PARAM_PERIOD_DATE_TO = "periodDateTo";
    public static final String PARAM_OWNER = "owner";
    public static final String PARAM_RESPONSIBLE = "responsible";
    public static final String PARAM_TUTOR = "tutor";
    public static final String PARAM_STATE = "state";
    public static final String PARAM_REGISTRY_ELEMENT_PART = "registryElementPart";
    public static final String PARAM_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String PARAM_EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";

    public TrJournalListDSHandler(final String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String alias = "journal";
        Session session = context.getSession();

        DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(TrJournal.class, alias);
        DQLSelectBuilder dql = registry.buildDQLSelectBuilder();
        applyFilters(dql, alias, context);
        dql.column(alias);
	    DSOutput output = applyOrderAndBuildOutput(input, registry, dql, session);

        List<DataWrapper> wrappers = DataWrapper.wrap(output, "id", "title");
        Map<Long, DataWrapper> id2wrapper = CommonDAO.map(wrappers);

        DQLSelectBuilder dqlEppPps = new DQLSelectBuilder()
                .fromEntity(TrJournalGroup.class, "j2g").where(in(property(TrJournalGroup.journal().id().fromAlias("j2g")), id2wrapper.keySet()))
                .fromEntity(EppPpsCollectionItem.class, "pps2g").where(eq(property(EppPpsCollectionItem.list().fromAlias("pps2g")), property(TrJournalGroup.group().fromAlias("j2g"))))
                .column(property(TrJournalGroup.journal().id().fromAlias("j2g")))
                .column(property(EppPpsCollectionItem.pps().person().identityCard().fromAlias("pps2g")))
                .predicate(DQLPredicateType.distinct);
        List<Object[]> rows = dqlEppPps.createStatement(session).list();

        Map<Long, List<String>> id2fio = SafeMap.get(ArrayList.class);

        for (Object[] row : rows) {
            Long journalId = (Long) row[0];
            IdentityCard elementCard = (IdentityCard) row[1];
            String shortFio = elementCard.getFio();
            id2fio.get(journalId).add(shortFio);
        }

        for (DataWrapper wrapper : wrappers) {
            Collection<String> fio = id2fio.get(wrapper.getId());
            wrapper.put(TrJournalListDSHandler.COLUMN_PPS, StringUtils.join(fio, '\n'));
        }
        return output;
    }

    protected void applyFilters(DQLSelectBuilder dql, String alias, ExecutionContext context)
    {
        EppYearPart yearPart = context.get(PARAM_YEAR_PART);
        if (yearPart != null) {
            dql.where(eq(property(alias, TrJournal.L_YEAR_PART), value(yearPart)));
        }

        String number = context.get(PARAM_NUMBER);
        if (number != null) {
            dql.where(eq(property(TrJournal.number().fromAlias(alias)), value(number)));
        }

        Date periodDateFrom = context.get(PARAM_PERIOD_DATE_FROM);
        if (periodDateFrom != null) {
            dql.where(ge(property(TrJournal.durationBeginDate().fromAlias(alias)), valueDate(periodDateFrom)));
        }

        Date periodDateTo = context.get(PARAM_PERIOD_DATE_TO);
        if (periodDateTo != null) {
            dql.where(le(property(TrJournal.durationEndDate().fromAlias(alias)), valueDate(periodDateTo)));
        }

        OrgUnit owner = context.get(PARAM_OWNER);
        if (owner != null) {
            dql.where(eq(property(TrJournal.registryElementPart().registryElement().owner().fromAlias(alias)), value(owner)));
        }

        EppRegistryElementPart registryElementPart = context.get(PARAM_REGISTRY_ELEMENT_PART);
        if (registryElementPart != null) {
            dql.where(eq(property(TrJournal.registryElementPart().fromAlias(alias)), value(registryElementPart)));
        }

        PpsEntry responsible = context.get(PARAM_RESPONSIBLE);
        if (null != responsible) {
            dql.where(eq(property(TrJournal.responsible().fromAlias(alias)), value(responsible)));
        }

        PpsEntry tutor = context.get(PARAM_TUTOR);
        if (tutor != null) {
            dql.where(exists(
                    new DQLSelectBuilder().fromEntity(TrJournalGroup.class, "grpRel")
                            .joinEntity("grpRel", DQLJoinType.inner, EppPpsCollectionItem.class, "ppsC", eq(property("ppsC", EppPpsCollectionItem.list()), property("grpRel", TrJournalGroup.group())))
                            .where(eq(property("grpRel", TrJournalGroup.journal()), property(alias)))
                            .where(eq(property(EppPpsCollectionItem.pps().fromAlias("ppsC")), value(tutor)))
                            .buildQuery()
            ));
        }

        EppState state = context.get(PARAM_STATE);
        if (state != null) {
            dql.where(eq(property(TrJournal.state().fromAlias(alias)), value(state)));
        }

        OrgUnit formativeOrgUnit = context.get(PARAM_FORMATIVE_ORG_UNIT);
        if (formativeOrgUnit != null) {
            DQLSelectBuilder studentsBuilder = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "stu")
                    .column(property("stu", TrJournalGroupStudent.group().journal().id()))
                    .joinPath(DQLJoinType.inner, TrJournalGroupStudent.studentWpe().student().educationOrgUnit().fromAlias("stu"), "eou")
                    .where(eq(property("eou", EducationOrgUnit.formativeOrgUnit()), value(formativeOrgUnit)));
            dql.where(in(alias + ".id", studentsBuilder.buildQuery()));
        }

        EducationLevelsHighSchool educationLevelsHighSchool = context.get(PARAM_EDUCATION_LEVEL_HIGH_SCHOOL);
        if (educationLevelsHighSchool != null) {
            DQLSelectBuilder studentsBuilder = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "stu")
                    .column(property("stu", TrJournalGroupStudent.group().journal().id()))
                    .joinPath(DQLJoinType.inner, TrJournalGroupStudent.studentWpe().student().educationOrgUnit().fromAlias("stu"), "eou")
                    .where(eq(property("eou", EducationOrgUnit.educationLevelHighSchool()), value(educationLevelsHighSchool)));
            dql.where(in(alias + ".id", studentsBuilder.buildQuery()));
        }
    }

	private static final Comparator<IFastBeanOwner> journalByNumberComparator = new DelegatePropertyComparator(NumberAsStringComparator.INSTANCE, TrJournal.number().s());

	/**
	 * Применить сортировку по выбранному пользователем столбцу и построить output. Сортировку по номеру производим как по числу, а не как по строке
	 * (т.е. правильный порядок при сортировке по возрастанию "1", "2", "10", "20", а не "1", "10", "2", "20").
	 */
	protected DSOutput applyOrderAndBuildOutput(DSInput input, DQLOrderDescriptionRegistry registry, DQLSelectBuilder dql, Session session)
	{
		EntityOrder order = input.getEntityOrder();
		if (order.getKeyString().equals(TrJournal.P_NUMBER))
		{
			List<TrJournal> journals = dql.createStatement(session).list();
			Collections.sort(journals, (order.getDirection() == OrderDirection.desc) ? journalByNumberComparator.reversed() : journalByNumberComparator);
			return ListOutputBuilder.get(input, journals).pageable(true).build();
		}
		registry.applyOrder(dql, order);
		return DQLSelectOutputBuilder.get(input, dql, session).build();
	}
}