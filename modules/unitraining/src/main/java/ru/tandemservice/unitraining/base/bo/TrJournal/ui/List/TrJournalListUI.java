/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.TrJournalListDSHandler;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.Edit.TrJournalEdit;

import java.util.Map;

/**
 * @author Denis Katkov
 * @since 10.02.2016
 */
public class TrJournalListUI extends UIPresenter
{
    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(TrJournalEdit.class)
                .parameter(IUIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    @Override
    public void onBeforeDataSourceFetch(final IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(TrJournalList.TR_JOURNAL_LIST_DS)) {
            Map<String, Object> settingMap = _uiSettings.getAsMap(
                    TrJournalListDSHandler.PARAM_ORG_UNIT,
                    TrJournalListDSHandler.PARAM_YEAR_PART,
                    TrJournalListDSHandler.PARAM_NUMBER,
                    TrJournalListDSHandler.PARAM_PERIOD_DATE_FROM,
                    TrJournalListDSHandler.PARAM_PERIOD_DATE_TO,
                    TrJournalListDSHandler.PARAM_OWNER,
                    TrJournalListDSHandler.PARAM_REGISTRY_ELEMENT_PART,
                    TrJournalListDSHandler.PARAM_TUTOR,
                    TrJournalListDSHandler.PARAM_STATE,
                    TrJournalListDSHandler.PARAM_RESPONSIBLE,
                    TrJournalListDSHandler.PARAM_FORMATIVE_ORG_UNIT,
                    TrJournalListDSHandler.PARAM_EDUCATION_LEVEL_HIGH_SCHOOL
            );
            dataSource.putAll(settingMap);
        } else if (dataSource.getName().equals(TrJournalManager.TR_REGISTRY_ELEMENT_PART_DS)) {
            dataSource.put(TrJournalManager.TR_OWNER_KEY, getSettings().get(TrJournalListDSHandler.PARAM_OWNER));
        } else if (dataSource.getName().equals(TrJournalList.EDUCATION_LEVEL_HIGH_SCHOOL_DS)) {
            dataSource.put(TrJournalListDSHandler.PARAM_FORMATIVE_ORG_UNIT, getSettings().get(TrJournalListDSHandler.PARAM_FORMATIVE_ORG_UNIT));//?
        }
    }
}