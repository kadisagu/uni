/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrEduGroup.ui.JournalLink;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * @author oleyba
 * @since 9/15/11
 */
@Configuration
public class TrEduGroupJournalLink extends BusinessComponentManager
{
    public static final String TR_EDU_GROUP_JOURNAL_DS = "trEduGroupJournalDS";
    public static final String TR_EDU_GROUP_JOURNAL_LINK_DISC_KEY = "trEduGroupJournalLink_disc";
    public static final String TR_EDU_GROUP_JOURNAL_LINK_YEAR_PART_KEY = "trEduGroupJournalLink_yearPart";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(journalDSConfig())
            .create();
    }

    @Bean
    public UIDataSourceConfig journalDSConfig()
    {
        return SelectDSConfig.with(TR_EDU_GROUP_JOURNAL_DS, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .addColumn(TrJournal.P_CALCULATED_TITLE)
            .handler(this.journalDSHandler())
            .create();

    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> journalDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), TrJournal.class)
            .where(TrJournal.registryElementPart(), TR_EDU_GROUP_JOURNAL_LINK_DISC_KEY)
            .where(TrJournal.yearPart(), TR_EDU_GROUP_JOURNAL_LINK_YEAR_PART_KEY)
            .where(TrJournal.state().code(), (Object) EppState.STATE_ACCEPTED)
            .order(TrJournal.title())
            .filter(TrJournal.title())
            ;
    }

}
