/* $Id:$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.List;

/**
 * @author oleyba
 * @since 3/12/13
 */
public interface ITrHomePageDao extends INeedPersistenceSupport
{
    boolean checkPpsAccessForViewing(IPrincipalContext principalContext, TrJournal journal);

    boolean checkResponsibleAccessForViewing(IPrincipalContext principalContext, TrJournal journal);

    boolean hasJournalsForMarking(IPrincipalContext principalContext);

    String checkPpsAccessForMarking(IPrincipalContext principalContext, TrJournalGroup group);

    long getStructureCount(Person person, List<String> allowStates);
}
