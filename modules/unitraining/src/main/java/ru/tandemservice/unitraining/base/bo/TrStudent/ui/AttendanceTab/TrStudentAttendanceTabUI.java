package ru.tandemservice.unitraining.base.bo.TrStudent.ui.AttendanceTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitraining.base.bo.TrStudent.logic.TrStudentAttendanceHandler;

/**
 * @author avedernikov
 * @since 30.06.2016
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "student.id", required = true)})
public class TrStudentAttendanceTabUI extends UIPresenter
{
	private Student student = new Student();

	@Override
	public void onComponentRefresh()
	{
		student = DataAccessServices.dao().getNotNull(getStudent().getId());
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case TrStudentAttendanceTab.ATTENDANCE_DS:
			{
				dataSource.put(TrStudentAttendanceHandler.PARAM_STUDENT_ID, getStudent().getId());
				break;
			}
		}
	}

	public Student getStudent()
	{
		return student;
	}
	public void setStudent(Student student)
	{
		this.student = student;
	}
}