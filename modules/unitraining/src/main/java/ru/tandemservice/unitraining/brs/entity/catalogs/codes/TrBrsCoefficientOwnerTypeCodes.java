package ru.tandemservice.unitraining.brs.entity.catalogs.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Уровень настройки коэффициентов БРС"
 * Имя сущности : trBrsCoefficientOwnerType
 * Файл data.xml : unitraining.data.xml
 */
public interface TrBrsCoefficientOwnerTypeCodes
{
    /** Константа кода (code) элемента : Глобальная настройка (по всему вузу) (title) */
    String ACADEMY = "academy";
    /** Константа кода (code) элемента : На читающем подразделении (title) */
    String TUTOR_OU = "tutorOu";
    /** Константа кода (code) элемента : На деканате (title) */
    String GROUP_OU = "groupOu";
    /** Константа кода (code) элемента : Для журнала (реализации) (title) */
    String JOURNAL = "journal";
    /** Константа кода (code) элемента : Для модуля в журнале (реализации) (title) */
    String JOURNAL_MODULE = "journalModule";
    /** Константа кода (code) элемента : Для события в журнале (реализации) (title) */
    String JOURNAL_EVENT = "journalEvent";

    Set<String> CODES = ImmutableSet.of(ACADEMY, TUTOR_OU, GROUP_OU, JOURNAL, JOURNAL_MODULE, JOURNAL_EVENT);
}
