/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuEduYearPartAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;

/**
 * @author oleyba
 * @since 8/17/12
 */
@Input({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="holder.id", required=true)
})
public class TrBrsCoefficientOuEduYearPartAddUI extends UIPresenter
{
    private EntityHolder<OrgUnit> holder = new EntityHolder<>();
    private TrOrgUnitSettings _owner;
    private EppYearPart _yearPart;

    public static final String ORG_UNIT_PARAM = "orgUnit";

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        setYearPart(TrBrsCoefficientManager.instance().brsDao().getFirstEppYearPartForCurrentEduYear());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(ORG_UNIT_PARAM, getHolder().getValue());
    }

    public void onClickApply()
    {
        _owner = TrBrsCoefficientManager.instance().coefDao().doAddOuSettings(getHolder().getValue(), getYearPart());
        deactivate(ParametersMap.createWith(TrBrsCoefficientManager.BIND_OWNER_ID, _owner.getId()));
    }

    public TrOrgUnitSettings getOuSettings()
    {
        return _owner;
    }

    public EntityHolder<OrgUnit> getHolder()
    {
        return holder;
    }

    public EppYearPart getYearPart()
    {
        return _yearPart;
    }

    public void setYearPart(EppYearPart yearPart)
    {
        _yearPart = yearPart;
    }
}
