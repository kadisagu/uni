/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.ITrBrsCoefficientDao;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.TrBrsCoefficientDao;
import ru.tandemservice.unitraining.brs.dao.BrsDao;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/17/12
 */
@Configuration
public class TrBrsCoefficientManager extends BusinessObjectManager
{
    // public static final String BIND_OWNER = "owner";

    public static final String BIND_OWNER_ID = "ownerId";
    public static final String BIND_OWNER_HOLDER = "ownerHolder";
    public static final String BIND_OWNER_TYPE = "ownerType";

    public static TrBrsCoefficientManager instance() {
        return instance(TrBrsCoefficientManager.class);
    }

    @Bean
    public ITrBrsCoefficientDao coefDao() {
        return new TrBrsCoefficientDao();
    }

    @Bean
    public IBrsDao brsDao() {
        return new BrsDao();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> fcaGroupDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppFControlActionGroup.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(ne(property(EppFControlActionGroup.code().fromAlias(alias)), value(EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_PROJECT)));
            }
        }
        .order(EppFControlActionGroup.title())
        .filter(EppFControlActionGroup.title())
        ;
    }
}
