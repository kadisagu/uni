/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage.ui.TutorJournal;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;

import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.unitraining.base.bo.TrHomePage.TrHomePageManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

/**
 * @author oleyba
 * @since 9/22/11
 */
@State
({
    @Bind(key = "selectedTabId", binding = "selectedTabId")
})
public class TrHomePageTutorJournalUI extends UIPresenter
{
    private static final List<String> ALLOW_STATES = TrHomePageManager.ALLOW_EDIT_EVENT_CONFIG_STATES;

    private boolean empty = true;
    private String selectedTabId;

    @Override
    public void onComponentRefresh()
    {
        Person person = PersonManager.instance().dao().getPerson(_uiConfig.getUserContext().getPrincipalContext());

        DQLSelectBuilder journals = new DQLSelectBuilder()
            .fromEntity(TrJournal.class, "j")
            .where(eq(property(TrJournal.state().code().fromAlias("j")), value(EppState.STATE_ACCEPTED)))
            .joinEntity("j", DQLJoinType.inner, TrJournalGroup.class, "jg", eq(property(TrJournalGroup.journal().fromAlias("jg")), property("j")))
            .joinPath(DQLJoinType.inner, TrJournalGroup.group().fromAlias("jg"), "g")
            .joinEntity("g", DQLJoinType.inner, EppPpsCollectionItem.class, "pps", eq(property(EppPpsCollectionItem.list().fromAlias("pps")), property("g")))
            .where(eq(property(EppPpsCollectionItem.pps().person().fromAlias("pps")), value(person)))
            .column(property(TrJournal.id().fromAlias("j")));
        Number journalCount = journals.createCountStatement(new DQLExecutionContext(_uiSupport.getSession())).uniqueResult();

        long structureCount = TrHomePageManager.instance().dao().getStructureCount(person, ALLOW_STATES);

        setEmpty((journalCount == null || journalCount.intValue() == 0) && (structureCount == 0));
    }

    // getters and setters

    public boolean isEmpty()
    {
        return empty;
    }

    public void setEmpty(boolean empty)
    {
        this.empty = empty;
    }

    public String getSelectedTabId()
    {
        return selectedTabId;
    }

    public void setSelectedTabId(String selectedTabId)
    {
        this.selectedTabId = selectedTabId;
    }
}
