/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuSettingsPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.ScriptEdit.CommonScriptEdit;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.ScriptEdit.CommonScriptEditUI;
import org.tandemframework.shared.commonbase.base.bo.Common.ui.ScriptEdit.ScriptHolder;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuEduYearPartAdd.TrBrsCoefficientOuEduYearPartAdd;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuSettingsEdit.TrBrsCoefficientOuSettingsEdit;
import ru.tandemservice.unitraining.brs.dao.BrsDao;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;

/**
 * @author oleyba
 * @since 8/17/12
 */
@Input({
    @Bind(key= UIPresenter.PUBLISHER_ID, binding="orgUnitHolder.id", required=true),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType.id"),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_ID, binding = "ouSettingsId")
})
@Output({
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_ID, binding = "ouSettings.id"),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType.id")
})
public class TrBrsCoefficientOuSettingsPubUI extends UIPresenter
{
    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    private TrBrsCoefficientOwnerType ownerType = new TrBrsCoefficientOwnerType();
    private TrOrgUnitSettings ouSettings;
    private Long _ouSettingsId;
    private boolean _hasYearParts;

    @Override
    public void onComponentRefresh()
    {
        OrgUnit ou = getOrgUnitHolder().refresh();

        if (getOuSettingsId() == null) {
            EppYearPart yearPart = TrBrsCoefficientManager.instance().brsDao().getFirstEppYearPartForCurrentEduYear();
            if (yearPart == null) {
                setHasYearParts(false);
                setOuSettings(null);
                return;
            }
            setHasYearParts(true);
            setOuSettings(DataAccessServices.dao().<TrOrgUnitSettings>getByNaturalId(new TrOrgUnitSettings.NaturalId(ou, yearPart)));
        } else {
            setOuSettings(DataAccessServices.dao().<TrOrgUnitSettings>getNotNull(getOuSettingsId()));
        }

        // надо убить все компоненты
        RenderComponent.deactivateAll();
    }

    public void onChangeYearPart()
    {
        setOuSettingsId(getOuSettings() != null ? getOuSettings().getId() : null);
        onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(TrBrsCoefficientOuSettingsPub.BIND_OU, getOrgUnitHolder().getValue());
    }

    public void onClickAddEduYear()
    {
        _uiActivation.asRegionDialog(TrBrsCoefficientOuEduYearPartAdd.class)
        .parameter(UIPresenter.PUBLISHER_ID, getOrgUnitHolder().getId())
        .activate();
    }

    public void onEditSettings()
    {
        _uiActivation.asRegionDialog(TrBrsCoefficientOuSettingsEdit.class)
        .parameter(UIPresenter.PUBLISHER_ID, getOuSettings().getId())
        .activate();
    }

    public void onEditScript()
    {
        _uiActivation.asDesktopRoot(CommonScriptEdit.class)
        .parameter(CommonScriptEditUI.PARAM_HOLDER, new ScriptHolder<TrOrgUnitSettings>(getOuSettings()) {

            @Override public String getScriptText() {
                final String script = StringUtils.trimToNull(getValue().getUserScript());
                if (null != script) { return script; }
                return getDefaultScriptText();
            }

            @Override public void setScriptText(String userScript) {
                userScript = StringUtils.trimToNull(userScript);
                if (null != userScript && scriptEquals(userScript, getDefaultScriptText())) { userScript = null; }
                getValue().setUserScript(userScript);
            }

            @Override public String getDefaultScriptText() {
                return StringUtils.trimToEmpty(BrsDao.getDefaultScriptText());
            }

            @Override protected void internalSave() {
                TrBrsCoefficientManager.instance().brsDao().doSaveUserScript(getValue());
            }

        })
        .activate();
    }

    public void onRevertScript()
    {
        TrOrgUnitSettings ouSettings = getOuSettings();
        ouSettings.setUserScript(null);
        TrBrsCoefficientManager.instance().brsDao().doSaveUserScript(ouSettings);
    }

    public void onClickRemove()
    {
        IUniBaseDao.instance.get().delete(getOuSettings());
        setOuSettings(TrBrsCoefficientManager.instance().brsDao().getOuSettingsForLastYearPart(getOrgUnitHolder().getId()));
        onChangeYearPart();
    }

    public String getBrsCaption()
    {
        return "Настройки БРС на " + getOuSettings().getEduYearPart().getTitle();
    }

    public String getDeleteAlert()
    {
        return "Удалить настройки на " + getOuSettings().getEduYearPart().getTitle() + "?";
    }

    public boolean isSelected()
    {
        return getOuSettings() != null;
    }

    public IBrsSettingsOwner getOwner()
    {
        return getOuSettings();
    }

    public OrgUnitHolder getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public TrOrgUnitSettings getOuSettings()
    {
        return ouSettings;
    }

    public void setOuSettings(TrOrgUnitSettings ouSettings)
    {
        this.ouSettings = ouSettings;
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(TrBrsCoefficientOwnerType ownerType)
    {
        this.ownerType = ownerType;
    }

    public Long getOuSettingsId()
    {
        return _ouSettingsId;
    }

    public void setOuSettingsId(Long ouSettingsId)
    {
        _ouSettingsId = ouSettingsId;
    }

    public boolean isHasYearParts()
    {
        return _hasYearParts;
    }

    public void setHasYearParts(boolean hasYearParts)
    {
        _hasYearParts = hasYearParts;
    }
}
