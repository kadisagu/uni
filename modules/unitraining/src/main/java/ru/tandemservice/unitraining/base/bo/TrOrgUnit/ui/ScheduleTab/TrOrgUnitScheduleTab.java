/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.ScheduleTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.BaseRegistryElementPartDSHandler;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/15/11
 */
@Configuration
public class TrOrgUnitScheduleTab extends BusinessComponentManager
{
    public static final String TR_ORGUNIT_SCHEDULE_TAB_DISCIPLINE_DS = "trOrgUnitScheduleTabDisciplineDS";
    public static final String TR_ORGUNIT_SCHEDULE_TAB_JOURNAL_DS = "trOrgUnitScheduleTabJournalDS";

    public static final String TR_ORGUNIT_SCHEDULE_TAB_ORG_UNIT_KEY = "trOrgUnitScheduleTab_orgUnit";
    public static final String TR_ORGUNIT_SCHEDULE_TAB_DISC_KEY = "trOrgUnitScheduleTab_disc";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(disciplineDSConfig())
        .addDataSource(journalDSConfig())
        .create();
    }

    @Bean
    public UIDataSourceConfig disciplineDSConfig()
    {
        return SelectDSConfig.with(TrOrgUnitScheduleTab.TR_ORGUNIT_SCHEDULE_TAB_DISCIPLINE_DS, this.getName())
            .addColumn(EppRegistryElementPart.titleWithNumber().s())
            .dataSourceClass(SelectDataSource.class)
            .handler(this.disciplineDSHandler())
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> disciplineDSHandler()
    {
        return new BaseRegistryElementPartDSHandler(this.getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                final Object ou = context.get(TR_ORGUNIT_SCHEDULE_TAB_ORG_UNIT_KEY);

                DQLSelectBuilder disc = new DQLSelectBuilder()
                    .fromEntity(TrJournalGroup.class, "jg")
                    .where(eq(property(TrJournalGroup.journal().state().code().fromAlias("jg")), value(EppState.STATE_ACCEPTED)))
                    .where(groupDQL(ou))
                    .column(property(TrJournalGroup.journal().registryElementPart().id().fromAlias("jg")))
                    ;
                dql.where(in(property(alias + ".id"), disc.buildQuery()));
            }
        }
        ;
    }

    @Bean
    public UIDataSourceConfig journalDSConfig()
    {
        return SelectDSConfig.with(TrOrgUnitScheduleTab.TR_ORGUNIT_SCHEDULE_TAB_JOURNAL_DS, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .addColumn(TrJournal.P_CALCULATED_TITLE)
        .handler(this.journalDSHandler())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> journalDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), TrJournal.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                final Object ou = context.get(TR_ORGUNIT_SCHEDULE_TAB_ORG_UNIT_KEY);

                DQLSelectBuilder journal = new DQLSelectBuilder()
                    .fromEntity(TrJournalGroup.class, "jg")
                    .where(eq(property(TrJournalGroup.journal().state().code().fromAlias("jg")), value(EppState.STATE_ACCEPTED)))
                    .where(groupDQL(ou))
                    .column(property(TrJournalGroup.journal().id().fromAlias("jg")))
                    ;
                dql.where(in(property(alias + ".id"), journal.buildQuery()));
            }
        }
        .where(TrJournal.registryElementPart(), TR_ORGUNIT_SCHEDULE_TAB_DISC_KEY)
        .order(TrJournal.title())
        .filter(TrJournal.title())
        ;
    }

    private IDQLExpression groupDQL(Object ou)
    {
        return or(
            // подразделение - читающее для дисциплины журнала
            eq(property(TrJournalGroup.journal().registryElementPart().registryElement().owner().fromAlias("jg")), commonValue(ou)),
            // подразделение - диспетчерская для УГС
            eq(property(TrJournalGroup.group().summary().owner().fromAlias("jg")), commonValue(ou)),
            // подразделение - деканат для УГС
            exists(new DQLSelectBuilder()
                    .fromEntity(EppRealEduGroup4LoadTypeRow.class, "rel")
                    .column(property("rel.id"))
                    .where(eq(property(EppRealEduGroup4LoadTypeRow.studentEducationOrgUnit().groupOrgUnit().fromAlias("rel")), commonValue(ou)))
                    .where(eq(property(EppRealEduGroup4LoadTypeRow.group().fromAlias("rel")), property(TrJournalGroup.group().fromAlias("jg"))))
                .buildQuery()))
            ;
    }
}
