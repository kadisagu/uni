/* $Id$ */
package ru.tandemservice.unitraining.rules.condition.schedulebell;

import org.apache.commons.collections15.Predicate;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;

/**
 * @author nvankov
 * @since 2/19/14
 */
public class UTCanEditBellScheduleEntriesPredicate implements Predicate<ScheduleBell>
{
    @Override
    public boolean evaluate(ScheduleBell bellSchedule)
    {
        return TrJournalManager.instance().dao().canEditBellsEntries(bellSchedule.getId());
    }
}
