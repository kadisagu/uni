/* $Id$ */
package ru.tandemservice.unitraining.component.eduGroup.TrJournalTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import ru.tandemservice.uniepp.component.edugroup.pub.GroupContent.IGroupContentParameters;

/**
 * @author oleyba
 * @since 9/14/11
 */
@Input({
    @Bind(key="parameters", binding="parameters", required=true)
})
@Output({
    @Bind(key="parameters", binding="parameters", required=true)
})
public class Model
{
    private IGroupContentParameters parameters;

    public String getPermissionKey()
    {
        return getParameters().getOrgUnitHolder().getSecModel().getPermission("pubJournalView_eppEduGroup");
    }

    public IGroupContentParameters getParameters()
    {
        return parameters;
    }

    public void setParameters(IGroupContentParameters parameters)
    {
        this.parameters = parameters;
    }
}
