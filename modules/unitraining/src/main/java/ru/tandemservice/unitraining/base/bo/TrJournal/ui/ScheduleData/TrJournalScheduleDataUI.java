/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.ScheduleData;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEventPlace;
import ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.EventActionEditLoadType.TrJournalEventActionEditLoadType;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.ScheduleEventEdit.TrJournalScheduleEventEdit;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/22/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journal.id", required = true),
    @Bind(key = TrJournalScheduleDataUI.VIEW_MODE_BIND, binding = "viewMode"),
    @Bind(key = TrJournalScheduleDataUI.CAN_CHANGE_LOAD_TYPE_BIND, binding = "canChangeLoadType")
})
public class TrJournalScheduleDataUI extends UIPresenter
{
    public static final String VIEW_MODE_BIND = "viewMode";
    public static final String CAN_CHANGE_LOAD_TYPE_BIND = "canChangeLoadType";

    private TrJournal journal = new TrJournal();
    private boolean viewMode;
    private boolean canChangeLoadType;

    private StaticListDataSource<Row> dataSource;
    private final Map<MultiKey, TrEduGroupEvent> eventMap = new HashMap<>();
    private final Map<ScheduleEvent, String> placeMap = new HashMap<>();

    // actions

    @Override
    public void onComponentRefresh()
    {
        setJournal(IUniBaseDao.instance.get().get(TrJournal.class, getJournal().getId()));

        // получим список групп
        List<EppRealEduGroup> groups = getGroups();

        // добавим колонки в датасурс
        dataSource = new StaticListDataSource<>();
        dataSource.addColumn(new SimpleColumn("Название", "title").setClickable(false).setOrderable(false).setTreeColumn(true));
//        if (!isViewMode() && isCanChangeLoadType())
//            dataSource.addColumn(getLoadTypeChangeColumn());
        final HeadColumn eduGroupsHeadColumn = new HeadColumn("eduGroups", "Учебные группы");
        addlEduGroupColumns(groups, CommonBaseEntityUtil.<EppGroupType>getPropertiesSet(groups, EppRealEduGroup.type().s()), eduGroupsHeadColumn);
        dataSource.addColumn(eduGroupsHeadColumn.setWidth(1));

        // получим список событий и событий групп и добавим данные в датасурс
        final List<Row> rowList = getEventRowList();
        dataSource.setupRows(rowList);
        fillEventMap();
    }

    public void onClickEditEvent()
    {
        final  Map<String, Object> params = getListenerParameter();
        if (null == params) return;

        TrEduGroupEvent groupEvent = eventMap.get(new MultiKey(params.get("trEventId"), params.get("eduGroupId")));
        if (null == groupEvent) return;

        _uiActivation.asRegionDialog(TrJournalScheduleEventEdit.class)
        .parameter(IUIPresenter.PUBLISHER_ID, groupEvent.getId())
        .activate();
    }

    public void onClickEditLoadType()
    {
        _uiActivation.asRegionDialog(TrJournalEventActionEditLoadType.class)
        .parameter(IUIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
        .activate();
    }

    public void onClickRefresh()
    {
        TrJournalDaemonBean.DAEMON.wakeUpAndWaitDaemon(60*3);
        deactivate();
    }

    // presenter methods

    public String getCurrentBlockName()
    {
        if (null != getCurrentSchEvent())
            return isViewMode() ? "showEventBlock" : "editEventBlock";
        if (null != getCurrentGroupEvent())
            return isViewMode() ? "emptyBlock" : "addEventBlock";
        return "emptyBlock";
    }

    public boolean isDatesDiffer()
    {
        final ScheduleEvent event = getCurrentSchEvent();
        return null != event && event.isDatesDiffer();
    }

    public String getEventDate()
    {
        final ScheduleEvent schEvent = getCurrentSchEvent();
        if (null != schEvent)
            return schEvent.getBeginDateStr();
        return "";
    }

    public String getEventEndDate()
    {
        final ScheduleEvent schEvent = getCurrentSchEvent();
        if (null != schEvent)
            return schEvent.getEndDateStr();
        return "";
    }

    public String getEventTime()
    {
        final ScheduleEvent schEvent = getCurrentSchEvent();
        if (null != schEvent)
            return schEvent.getTimePeriodStr();
        return "";
    }

    public boolean isShowPlace()
    {
        return null != placeMap.get(getCurrentSchEvent());
    }

    public String getEventPlace()
    {
        return placeMap.get(getCurrentSchEvent());
    }

    public Map<String, Object> getEditEventParameters()
    {
        return new ParametersMap().add("trEventId", dataSource.getCurrentEntity().getId()).add("eduGroupId",dataSource.getCurrentColumn().getName());
    }

    public String getDaemonStatus()
    {
        final Long date = TrJournalDaemonBean.DAEMON.getCompleteStatus();
        if (null != date) {
            return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));
        }
        return "обновляется";
    }


    public boolean isCanEditLoadType()
    {
        return dataSource.getCurrentEntity().getEntity() instanceof TrEventAction;
    }

    // private util and data access methods

    private TrEduGroupEvent getCurrentGroupEvent()
    {
        if (dataSource.getCurrentEntity().getEntity() instanceof TrJournalEvent)
        {
            TrJournalEvent jEvent = (TrJournalEvent) dataSource.getCurrentEntity().getEntity();
            final String columnName = dataSource.getCurrentColumn().getName();
            return eventMap.get(new MultiKey(jEvent.getId(), columnName));
        }
        return null;
    }

    private ScheduleEvent getCurrentSchEvent()
    {
        final TrEduGroupEvent gEvent = getCurrentGroupEvent();
        return null == gEvent ? null : gEvent.getScheduleEvent();
    }

    private BlockColumn getLoadTypeChangeColumn()
    {
        final BlockColumn loadTypeEditColumn = new BlockColumn("loadTypeEdit", "Изменить\nвид УГС", "loadTypeEditBlock");
        loadTypeEditColumn.setHeaderBlockName("emptyBlock");
        loadTypeEditColumn.setWidth(1);
        return loadTypeEditColumn;
    }

    private void addlEduGroupColumns(List<EppRealEduGroup> groups, Set<EppGroupType> types, HeadColumn eduGroupsHeadColumn)
    {
        final String filledCellColor = "fffbdd";
        final String errorColor = "FAB5B0";

        for (EppGroupType type : types)
        {
            final HeadColumn typeColumn = new HeadColumn(type.getCode(), type.getShortTitle());
            eduGroupsHeadColumn.addColumn(typeColumn);
            for (final EppRealEduGroup group : groups)
            {
                if (!group.getType().equals(type))
                    continue;
                BlockColumn groupColumn = new BlockColumn(group.getId().toString(), group.getTitle(), "schEventBlock");
                typeColumn.addColumn(groupColumn);
                groupColumn.setVerticalHeader(false);
                groupColumn.setWidth(1);
                groupColumn.setClickable(false);
                groupColumn.setOrderable(false);
                groupColumn.setStyleResolver(rowEntity -> {
                    if (rowEntity instanceof Row && ((Row) rowEntity).getEntity() instanceof TrJournalEvent)
                    {
                        final TrJournalEvent event = (TrJournalEvent) ((Row) rowEntity).getEntity();
                        final boolean canBeFilled = eventMap.containsKey(new MultiKey(event.getId(), group.getId().toString()));
                        if (!event.getType().equals(group.getType()) && canBeFilled)
                            return "background-color: #" + errorColor + ";";
                        else if (canBeFilled)
                            return "background-color: #" + filledCellColor + ";";
                    }
                    return "";
                });
            }
        }
    }

    private void fillEventMap()
    {
        eventMap.clear();
        DQLSelectBuilder groupEventDQL = new DQLSelectBuilder()
                .fromEntity(TrEduGroupEvent.class, "e")
                .where(eq(property(TrEduGroupEvent.journalEvent().journalModule().journal().fromAlias("e")), value(getJournal())))
                .column("e")
                ;
        for (TrEduGroupEvent event : groupEventDQL.createStatement(_uiSupport.getSession()).<TrEduGroupEvent>list())
            eventMap.put(new MultiKey(event.getJournalEvent().getId(), event.getGroup().getId().toString()), event);

        placeMap.clear();
        Map<ScheduleEvent, Set<String>> map = SafeMap.get(LinkedHashSet.class);
        DQLSelectBuilder placeDQL = new DQLSelectBuilder()
                .fromEntity(TrEduGroupEvent.class, "e")
                .joinPath(DQLJoinType.inner, TrEduGroupEvent.scheduleEvent().fromAlias("e"), "se")
                .joinEntity("se", DQLJoinType.inner, ScheduleEventPlace.class, "rel", eq(property(ScheduleEventPlace.scheduleEvent().fromAlias("rel")), property("se")))
                .where(eq(property(TrEduGroupEvent.journalEvent().journalModule().journal().fromAlias("e")), value(getJournal())))
                .column(property(ScheduleEventPlace.scheduleEvent().fromAlias("rel")))
                .column(property(ScheduleEventPlace.place().fromAlias("rel")))
                .order(property(ScheduleEventPlace.place().title().fromAlias("rel")))
                ;
        for (Object[] row : placeDQL.createStatement(_uiSupport.getSession()).<Object[]>list())
            map.get((ScheduleEvent) row[0]).add(((UniplacesPlace)row[1]).getDisplayableTitle());

        for (Map.Entry<ScheduleEvent, Set<String>> entry : map.entrySet())
            placeMap.put(entry.getKey(), StringUtils.join(entry.getValue(), ", "));
    }

    private List<EppRealEduGroup> getGroups()
    {
        // получим группы
        DQLSelectBuilder groupDQL = new DQLSelectBuilder()
        .fromEntity(TrJournalGroup.class, "rel")
        .column(property(TrJournalGroup.group().fromAlias("rel")))
        .joinPath(DQLJoinType.inner, TrJournalGroup.group().fromAlias("rel"), "grp")
        .where(eq(property(TrJournalGroup.journal().fromAlias("rel")), value(getJournal())))
        .order(property(EppRealEduGroup.type().priority().fromAlias("grp")))
        .order(property(EppRealEduGroup.title().fromAlias("grp")))
        ;
        List<EppRealEduGroup> groups = groupDQL.createStatement(_uiSupport.getSession()).<EppRealEduGroup>list();

        // получим число студентов в группе
        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EppRealEduGroup4LoadTypeRow.class, "grpRow")
        .joinPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.group().fromAlias("grpRow"), "grp")
        .joinEntity("grp", DQLJoinType.inner, TrJournalGroup.class, "rel", eq(property(TrJournalGroup.group().fromAlias("rel")), property("grp")))
        .where(eq(property(TrJournalGroup.journal().fromAlias("rel")), value(journal)))
        .group(property(EppRealEduGroup4LoadTypeRow.group().id().fromAlias("grpRow")))
        .column(property(EppRealEduGroup4LoadTypeRow.group().id().fromAlias("grpRow")))
        .column(DQLFunctions.count(property(EppRealEduGroup4LoadTypeRow.id().fromAlias("grpRow"))))
        ;

        final Map<Long, Integer> studentCountMap = new HashMap<>();
        for (Object[] row : dql.createStatement(_uiSupport.getSession()).<Object[]>list())
                studentCountMap.put((Long) row[0], ((Number) row[1]).intValue());

        // отсортируем группы по числу студентов
        Collections.sort(groups, (o1, o2) -> {
            int order = o1.getType().getPriority() - o2.getType().getPriority();
            if (0 == order)
                order = - Long.compare(UniBaseUtils.nullToZero(studentCountMap.get(o1.getId())), UniBaseUtils.nullToZero(studentCountMap.get(o2.getId())));
            return order;
        });

        return groups;
    }

    private List<Row> getEventRowList()
    {
        Map<TrJournalModule, List<TrJournalEvent>> journalMap = new LinkedHashMap<>();

        for (TrJournalModule module : IUniBaseDao.instance.get().getList(TrJournalModule.class, TrJournalModule.journal(), getJournal(), TrJournalModule.moduleBeginDate().s()))
            journalMap.put(module, new ArrayList<>());

        for (TrJournalEvent event : IUniBaseDao.instance.get().getList(TrJournalEvent.class, TrJournalEvent.journalModule().journal(), getJournal(), TrJournalEvent.number().s()))
            journalMap.get(event.getJournalModule()).add(event);

        final List<Row> rowList = new ArrayList<>();
        for (Map.Entry<TrJournalModule, List<TrJournalEvent>> entry : journalMap.entrySet())
        {
            final Row moduleRow = new Row(entry.getKey(), null);
            rowList.add(moduleRow);

            for (final TrJournalEvent event: entry.getValue())
            {
                rowList.add(new Row(event, moduleRow));
            }
        }
        return rowList;
    }

    // wrapper classes

    public static class Row extends ViewWrapper<IEntity>
    {
        private final Row parent;
        @Override public Row getHierarhyParent() { return this.parent; }

        public Row(final IEntity i, final Row parent) {
            super(i);
            this.parent = parent;
        }
    }

    // getters and setters

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }

    public StaticListDataSource<Row> getDataSource()
    {
        return dataSource;
    }

    public boolean isViewMode()
    {
        return viewMode;
    }

    public void setViewMode(boolean viewMode)
    {
        this.viewMode = viewMode;
    }

    public boolean isCanChangeLoadType()
    {
        return canChangeLoadType;
    }

    public void setCanChangeLoadType(boolean canChangeLoadType)
    {
        this.canChangeLoadType = canChangeLoadType;
    }
}
