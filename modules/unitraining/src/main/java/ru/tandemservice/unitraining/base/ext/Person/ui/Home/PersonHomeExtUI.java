/* $Id$ */
package ru.tandemservice.unitraining.base.ext.Person.ui.Home;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.sec.IPrincipalContext;
import ru.tandemservice.uni.dao.pps.IPpsEntryDao;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.TutorJournal.TrHomePageTutorJournal;

/**
 * @author Vasily Zhukov
 * @since 18.11.2011
 */
public class PersonHomeExtUI extends UIAddon
{
    public PersonHomeExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickGoToPpsJournalPage()
    {
        getPresenter().getActivationBuilder().asDesktopRoot(TrHomePageTutorJournal.class).activate();
    }

    public boolean isPpsFound()
    {
        IPrincipalContext principalContext = getPresenter().getConfig().getUserContext().getPrincipalContext();
        return !IPpsEntryDao.instance.get().getPpsList(principalContext).isEmpty();
    }
}
