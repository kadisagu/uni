/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.ScheduleEventEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;

/**
 * @author oleyba
 * @since 8/22/11
 */
@Configuration
public class TrJournalScheduleEventEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(TrOrgUnitManager.instance().trOrgUnitPlaceDSConfig())
        .create();
    }
}
