/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrEventLoad;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;

import java.util.Date;

/**
 * @author oleyba
 * @since 9/7/11
 */
public class TrJournalMarkCell implements ITrJournalEventDao.IJournalMarkData
{
    public static final IdentifiableWrapper IDW_PRESENT = new IdentifiableWrapper(0L, "был");
    public static final IdentifiableWrapper IDW_ABSENT = new IdentifiableWrapper(1L, "не был");

    private static final ISelectModel absenceModel = new LazySimpleSelectModel<>(ImmutableList.of(IDW_PRESENT, IDW_ABSENT));

    private IdentifiableWrapper absenceNote;
    private Double grade;
    private String comment;
    private Long eventId;
    private String tooltipMessage;

    private TrJournalEvent journalEvent;
    private TrEduGroupEventStudent mark;

    public TrJournalMarkCell(TrJournalEvent journalEvent) {
        this.journalEvent = journalEvent;
    }

    public TrJournalMarkCell(TrEduGroupEventStudent mark)
    {
        this.mark = mark;
        if (mark.getAbsenceReason() != null)
        {
            absenceNote = IDW_ABSENT;
            tooltipMessage = "Для студента была указана причина пропуска занятий «"+mark.getAbsenceReason().getTitle()+"». Чтобы снять отметку об отсутствии, уберите указание о причине на странице работы с пропусками (вкладка «Пропущенные занятия» на карточке учебной группы).";
        }
        else
        {
            if (null == mark.getAbsent())
                absenceNote = null;
            else if (Boolean.TRUE.equals(mark.getAbsent()))
                absenceNote = IDW_ABSENT;
            else if (Boolean.FALSE.equals(mark.getAbsent()))
                absenceNote = IDW_PRESENT;
        }

        grade = mark.getGrade();
        comment = mark.getComment();
        eventId = mark.getId();
        journalEvent = mark.getEvent().getJournalEvent();
    }

    public String getText()
    {
        return CellTextFormatter.format(this);
    }

    @Override public Boolean isAbsent()
    {
        if (null == absenceNote)
            return null;
        else
            return IDW_ABSENT.getId().equals(absenceNote.getId());
    }

    public boolean isShowAttendance() {
        return getJournalEvent() instanceof TrEventLoad || !getJournal().isHideAttendanceForControlEvents();
    }

    public boolean isClickable()
    {
        return eventId != null;
    }

    public String getButtonId()
    {
        return "showInfo" + eventId;
    }

    public boolean isAbsenceEditDisabled()
    {
        return tooltipMessage != null;
    }

    @Override
    public Date getScheduledDate()
    {
        return null;  // todo
    }

    @Override
    public Date getActualDate()
    {
        return null;  // todo
    }

    public TrEduGroupEventStudent getMark()
    {
        return mark;
    }

    public interface TrJournalCellTooltipInfo {
        String getStudent();
        String getDate();
        String getPresent();
        String getGrade();
    }

    public TrJournalCellTooltipInfo getInfo() {
        return new TrJournalCellTooltipInfo() {
            public String getStudent() {
                final Student student = mark.getStudentWpe().getStudent();
                return student.getPerson().getFullFio() + (student.getGroup() == null ? "" : ", " + student.getGroup().getTitle());
            }

            public String getDate() {
                return getMark().getEvent().getScheduleEvent() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(getMark().getEvent().getScheduleEvent().getDurationBegin());
            }

            public String getPresent() {
                if (null == mark.getAbsent()) { return ""; }
                return mark.getAbsent() ? "нет" : "да";
            }

            public String getGrade() {
                return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark.getGrade());
            }
        };
    }

    // getters and setters

    public IdentifiableWrapper getAbsenceNote()
    {
        return absenceNote;
    }

    public void setAbsenceNote(IdentifiableWrapper absenceNote)
    {
        this.absenceNote = absenceNote;
    }

    @Override
    public Double getGrade()
    {
        return grade;
    }

    public void setGrade(Double grade)
    {
        this.grade = grade;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public Long getEventId()
    {
        return eventId;
    }

    public String getTooltipMessage()
    {
        return tooltipMessage;
    }

    public static ISelectModel getAbsenceModel()
    {
        return absenceModel;
    }

    public TrJournal getJournal()
    {
        return getJournalEvent().getJournalModule().getJournal();
    }

    public TrJournalEvent getJournalEvent()
    {
        return journalEvent;
    }

}
