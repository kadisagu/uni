/* $Id:$ */
package ru.tandemservice.unitraining.brs.dao;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.brs.dao.ISessionBrsSettings;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.SessionBrsSettingsWrapper;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;

import java.util.Date;
import java.util.Map;

/**
 * @author oleyba
 * @since 10/16/12
 */
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=9865668")
public class SessionBrsDao extends ru.tandemservice.unisession.brs.dao.SessionBrsDao
{
    // todo прикрутить кэширование на все эти дурные проверки, используется ли рейтинг
    private static final String CACHE_SESSION_BRS_GLOBAL_SETTINGS = SessionBrsDao.class.getSimpleName() + "globalSessionBrtSettings";

    @Override
    public ISessionBrsSettings getSettings()
    {
        ISessionBrsSettings settings = DaoCache.get(CACHE_SESSION_BRS_GLOBAL_SETTINGS);
        if (settings == null)
        {
            settings = new SessionBrsSettingsWrapper(true);
            DaoCache.put(CACHE_SESSION_BRS_GLOBAL_SETTINGS, settings);
        }
        return settings;
    }

    @Override
    public void saveSettings(ISessionBrsSettings settingsWrapper)
    {
        DataSettingsFacade.saveSettings(((SessionBrsSettingsWrapper)settingsWrapper).getDataSettings());
        DaoCache.put(CACHE_SESSION_BRS_GLOBAL_SETTINGS, null);
    }

    @Override
    public ISessionRatingSettings getRatingSettings(ISessionRatingSettingsKey key)
    {
        return TrBrsCoefficientManager.instance().brsDao().getRatingSettings(key);
    }

    @Override
    public boolean isUseCurrentRating(SessionAttestationBulletin bulletin)
    {
        if (null == bulletin)
            return false;
        bulletin = get(SessionAttestationBulletin.class, bulletin.getId());
        SessionObject sessionObject = bulletin.getAttestation().getSessionObject();
        EppYearPart yearPart = TrBrsCoefficientManager.instance().brsDao().getEppYearPart(sessionObject.getEducationYear(), sessionObject.getYearDistributionPart());
        if (yearPart == null) return false;
        TrOrgUnitSettings settings = TrBrsCoefficientManager.instance().brsDao().getOrgUnitBrsSettings(
                yearPart,
                bulletin.getRegistryElementPart().getTutorOu(),
                bulletin.getOrgUnit());
        return null != settings && settings.isUseCurrentRatingInAttestation();
    }

    @Override
    public boolean isUseCurrentRating(SessionAttestation attestation)
    {
        if (null == attestation)
            return false;
        attestation = get(SessionAttestation.class, attestation.getId());
        SessionObject sessionObject = attestation.getSessionObject();
        EppYearPart yearPart = TrBrsCoefficientManager.instance().brsDao().getEppYearPart(sessionObject.getEducationYear(), sessionObject.getYearDistributionPart());
        if (yearPart == null) return false;
        TrOrgUnitSettings settings = TrBrsCoefficientManager.instance().brsDao().getOrgUnitBrsSettings(
                yearPart,
                null,
                attestation.getOrgUnit());
        return null != settings && settings.isUseCurrentRatingInAttestation();
    }

    @Override
    public void doFillAttestationMarkBasedOnCurrentRating(SessionAttestationBulletin bulletin)
    {
        if (!isUseCurrentRating(bulletin))
            throw new ApplicationException("Выставление аттестации на основании тек. рейтинга отключено для дисциплин подразделения «" + bulletin.getOrgUnit().getTitleWithType() + "» в " + bulletin.getAttestation().getSessionObject().getEducationYear().getTitle() + " учебном году. Проверьте настройки БРС подразделения.");
        TrBrsCoefficientManager.instance().brsDao().doFillAttestationMarkBasedOnCurrentRating(bulletin);
    }

    @Override
    public void saveCurrentRating(SessionDocument document)
    {
        TrBrsCoefficientManager.instance().brsDao().saveCurrentRating(document, false);
    }

    @Override
    public void saveCurrentRating(SessionDocument document, boolean silent)
    {
        TrBrsCoefficientManager.instance().brsDao().saveCurrentRating(document, silent);
    }

    @Override
    public ISessionMarkDAO.MarkData calculateMarkData(Date performDate, SessionDocumentSlot slot, Double points)
    {
        return TrBrsCoefficientManager.instance().brsDao().calculateMarkData(performDate, slot, points);
    }

    @Override
    public Map<String, IRatingAdditParamDef> getRatingAdditionalParamDefinitions(ISessionRatingSettingsKey key) {
        return TrBrsCoefficientManager.instance().brsDao().getRatingAdditionalParamDefinitions(key);
    }
}
