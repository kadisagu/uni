package ru.tandemservice.unitraining.brs.entity.brs.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Значение коэффициента для БРС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrBrsCoefficientValueGen extends EntityBase
 implements INaturalIdentifiable<TrBrsCoefficientValueGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue";
    public static final String ENTITY_NAME = "trBrsCoefficientValue";
    public static final int VERSION_HASH = -740334039;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String L_DEFINITION = "definition";
    public static final String P_VALUE_AS_LONG = "valueAsLong";
    public static final String P_VALUE_AS_BOOLEAN = "valueAsBoolean";
    public static final String P_VALUE_AS_DOUBLE = "valueAsDouble";

    private IBrsSettingsOwner _owner;     // Владелец
    private TrBrsCoefficientDef _definition;     // Коэффициент
    private long _valueAsLong;     // Значение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Владелец. Свойство не может быть null.
     */
    @NotNull
    public IBrsSettingsOwner getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Владелец. Свойство не может быть null.
     */
    public void setOwner(IBrsSettingsOwner owner)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && owner!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IBrsSettingsOwner.class);
            IEntityMeta actual =  owner instanceof IEntity ? EntityRuntime.getMeta((IEntity) owner) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Коэффициент. Свойство не может быть null.
     */
    @NotNull
    public TrBrsCoefficientDef getDefinition()
    {
        return _definition;
    }

    /**
     * @param definition Коэффициент. Свойство не может быть null.
     */
    public void setDefinition(TrBrsCoefficientDef definition)
    {
        dirty(_definition, definition);
        _definition = definition;
    }

    /**
     * @return Значение. Свойство не может быть null.
     */
    @NotNull
    public long getValueAsLong()
    {
        return _valueAsLong;
    }

    /**
     * @param valueAsLong Значение. Свойство не может быть null.
     */
    public void setValueAsLong(long valueAsLong)
    {
        dirty(_valueAsLong, valueAsLong);
        _valueAsLong = valueAsLong;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TrBrsCoefficientValueGen)
        {
            if (withNaturalIdProperties)
            {
                setOwner(((TrBrsCoefficientValue)another).getOwner());
                setDefinition(((TrBrsCoefficientValue)another).getDefinition());
            }
            setValueAsLong(((TrBrsCoefficientValue)another).getValueAsLong());
        }
    }

    public INaturalId<TrBrsCoefficientValueGen> getNaturalId()
    {
        return new NaturalId(getOwner(), getDefinition());
    }

    public static class NaturalId extends NaturalIdBase<TrBrsCoefficientValueGen>
    {
        private static final String PROXY_NAME = "TrBrsCoefficientValueNaturalProxy";

        private Long _owner;
        private Long _definition;

        public NaturalId()
        {}

        public NaturalId(IBrsSettingsOwner owner, TrBrsCoefficientDef definition)
        {
            _owner = ((IEntity) owner).getId();
            _definition = ((IEntity) definition).getId();
        }

        public Long getOwner()
        {
            return _owner;
        }

        public void setOwner(Long owner)
        {
            _owner = owner;
        }

        public Long getDefinition()
        {
            return _definition;
        }

        public void setDefinition(Long definition)
        {
            _definition = definition;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TrBrsCoefficientValueGen.NaturalId) ) return false;

            TrBrsCoefficientValueGen.NaturalId that = (NaturalId) o;

            if( !equals(getOwner(), that.getOwner()) ) return false;
            if( !equals(getDefinition(), that.getDefinition()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOwner());
            result = hashCode(result, getDefinition());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOwner());
            sb.append("/");
            sb.append(getDefinition());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrBrsCoefficientValueGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrBrsCoefficientValue.class;
        }

        public T newInstance()
        {
            return (T) new TrBrsCoefficientValue();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "definition":
                    return obj.getDefinition();
                case "valueAsLong":
                    return obj.getValueAsLong();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((IBrsSettingsOwner) value);
                    return;
                case "definition":
                    obj.setDefinition((TrBrsCoefficientDef) value);
                    return;
                case "valueAsLong":
                    obj.setValueAsLong((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "definition":
                        return true;
                case "valueAsLong":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "definition":
                    return true;
                case "valueAsLong":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return IBrsSettingsOwner.class;
                case "definition":
                    return TrBrsCoefficientDef.class;
                case "valueAsLong":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrBrsCoefficientValue> _dslPath = new Path<TrBrsCoefficientValue>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrBrsCoefficientValue");
    }
            

    /**
     * @return Владелец. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue#getOwner()
     */
    public static IBrsSettingsOwnerGen.Path<IBrsSettingsOwner> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Коэффициент. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue#getDefinition()
     */
    public static TrBrsCoefficientDef.Path<TrBrsCoefficientDef> definition()
    {
        return _dslPath.definition();
    }

    /**
     * @return Значение. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue#getValueAsLong()
     */
    public static PropertyPath<Long> valueAsLong()
    {
        return _dslPath.valueAsLong();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue#isValueAsBoolean()
     */
    public static SupportedPropertyPath<Boolean> valueAsBoolean()
    {
        return _dslPath.valueAsBoolean();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue#getValueAsDouble()
     */
    public static SupportedPropertyPath<Double> valueAsDouble()
    {
        return _dslPath.valueAsDouble();
    }

    public static class Path<E extends TrBrsCoefficientValue> extends EntityPath<E>
    {
        private IBrsSettingsOwnerGen.Path<IBrsSettingsOwner> _owner;
        private TrBrsCoefficientDef.Path<TrBrsCoefficientDef> _definition;
        private PropertyPath<Long> _valueAsLong;
        private SupportedPropertyPath<Boolean> _valueAsBoolean;
        private SupportedPropertyPath<Double> _valueAsDouble;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Владелец. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue#getOwner()
     */
        public IBrsSettingsOwnerGen.Path<IBrsSettingsOwner> owner()
        {
            if(_owner == null )
                _owner = new IBrsSettingsOwnerGen.Path<IBrsSettingsOwner>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Коэффициент. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue#getDefinition()
     */
        public TrBrsCoefficientDef.Path<TrBrsCoefficientDef> definition()
        {
            if(_definition == null )
                _definition = new TrBrsCoefficientDef.Path<TrBrsCoefficientDef>(L_DEFINITION, this);
            return _definition;
        }

    /**
     * @return Значение. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue#getValueAsLong()
     */
        public PropertyPath<Long> valueAsLong()
        {
            if(_valueAsLong == null )
                _valueAsLong = new PropertyPath<Long>(TrBrsCoefficientValueGen.P_VALUE_AS_LONG, this);
            return _valueAsLong;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue#isValueAsBoolean()
     */
        public SupportedPropertyPath<Boolean> valueAsBoolean()
        {
            if(_valueAsBoolean == null )
                _valueAsBoolean = new SupportedPropertyPath<Boolean>(TrBrsCoefficientValueGen.P_VALUE_AS_BOOLEAN, this);
            return _valueAsBoolean;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue#getValueAsDouble()
     */
        public SupportedPropertyPath<Double> valueAsDouble()
        {
            if(_valueAsDouble == null )
                _valueAsDouble = new SupportedPropertyPath<Double>(TrBrsCoefficientValueGen.P_VALUE_AS_DOUBLE, this);
            return _valueAsDouble;
        }

        public Class getEntityClass()
        {
            return TrBrsCoefficientValue.class;
        }

        public String getEntityName()
        {
            return "trBrsCoefficientValue";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract boolean isValueAsBoolean();

    public abstract double getValueAsDouble();
}
