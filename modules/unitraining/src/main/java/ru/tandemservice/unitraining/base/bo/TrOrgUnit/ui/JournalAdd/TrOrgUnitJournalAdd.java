/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;

/**
 * @author oleyba
 * @since 8/4/11
 */
@Configuration
public class TrOrgUnitJournalAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(TrOrgUnitManager.instance().trOrgUnitYearPartDSConfig())
        .addDataSource(TrOrgUnitManager.instance().trOrgUnitregistryElementPartDSConfig())
        .addDataSource(TrOrgUnitManager.instance().trOrgUnitPpsDSConfig())
        .create();
    }

}
