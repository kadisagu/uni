package ru.tandemservice.unitraining.base.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unitraining.base.entity.catalog.TrAddonType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип дополнительных событий
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrAddonTypeGen extends EntityBase
 implements INaturalIdentifiable<TrAddonTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.base.entity.catalog.TrAddonType";
    public static final String ENTITY_NAME = "trAddonType";
    public static final int VERSION_HASH = 513760146;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_ABBREVIATION = "abbreviation";
    public static final String P_USER_CODE = "userCode";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _abbreviation;     // Аббревиатура
    private String _userCode;     // Пользовательский код
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Аббревиатура. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getAbbreviation()
    {
        return _abbreviation;
    }

    /**
     * @param abbreviation Аббревиатура. Свойство не может быть null.
     */
    public void setAbbreviation(String abbreviation)
    {
        dirty(_abbreviation, abbreviation);
        _abbreviation = abbreviation;
    }

    /**
     * @return Пользовательский код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getUserCode()
    {
        return _userCode;
    }

    /**
     * @param userCode Пользовательский код. Свойство не может быть null и должно быть уникальным.
     */
    public void setUserCode(String userCode)
    {
        dirty(_userCode, userCode);
        _userCode = userCode;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TrAddonTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((TrAddonType)another).getCode());
            }
            setAbbreviation(((TrAddonType)another).getAbbreviation());
            setUserCode(((TrAddonType)another).getUserCode());
            setTitle(((TrAddonType)another).getTitle());
        }
    }

    public INaturalId<TrAddonTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<TrAddonTypeGen>
    {
        private static final String PROXY_NAME = "TrAddonTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TrAddonTypeGen.NaturalId) ) return false;

            TrAddonTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrAddonTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrAddonType.class;
        }

        public T newInstance()
        {
            return (T) new TrAddonType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "abbreviation":
                    return obj.getAbbreviation();
                case "userCode":
                    return obj.getUserCode();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "abbreviation":
                    obj.setAbbreviation((String) value);
                    return;
                case "userCode":
                    obj.setUserCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "abbreviation":
                        return true;
                case "userCode":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "abbreviation":
                    return true;
                case "userCode":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "abbreviation":
                    return String.class;
                case "userCode":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrAddonType> _dslPath = new Path<TrAddonType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrAddonType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitraining.base.entity.catalog.TrAddonType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Аббревиатура. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.catalog.TrAddonType#getAbbreviation()
     */
    public static PropertyPath<String> abbreviation()
    {
        return _dslPath.abbreviation();
    }

    /**
     * @return Пользовательский код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitraining.base.entity.catalog.TrAddonType#getUserCode()
     */
    public static PropertyPath<String> userCode()
    {
        return _dslPath.userCode();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unitraining.base.entity.catalog.TrAddonType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends TrAddonType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _abbreviation;
        private PropertyPath<String> _userCode;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitraining.base.entity.catalog.TrAddonType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(TrAddonTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Аббревиатура. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.catalog.TrAddonType#getAbbreviation()
     */
        public PropertyPath<String> abbreviation()
        {
            if(_abbreviation == null )
                _abbreviation = new PropertyPath<String>(TrAddonTypeGen.P_ABBREVIATION, this);
            return _abbreviation;
        }

    /**
     * @return Пользовательский код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitraining.base.entity.catalog.TrAddonType#getUserCode()
     */
        public PropertyPath<String> userCode()
        {
            if(_userCode == null )
                _userCode = new PropertyPath<String>(TrAddonTypeGen.P_USER_CODE, this);
            return _userCode;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unitraining.base.entity.catalog.TrAddonType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(TrAddonTypeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return TrAddonType.class;
        }

        public String getEntityName()
        {
            return "trAddonType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
