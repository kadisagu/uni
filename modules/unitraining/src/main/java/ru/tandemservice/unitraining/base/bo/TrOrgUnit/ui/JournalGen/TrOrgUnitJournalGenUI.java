package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalGen;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitHolder.id", required = true),
    @Bind(key = TrOrgUnitJournalGenUI.YAERPART_ID, binding = "yearPartHolder.id")
})
public class TrOrgUnitJournalGenUI extends UIPresenter {

    public static final String YAERPART_ID = "yearPartId";

    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    public OrgUnitHolder getOrgUnitHolder() { return this.orgUnitHolder; }
    public Long getOrgUnitId() { return this.getOrgUnitHolder().getId(); }
    public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }

    private final EntityHolder<EppYearPart> yearPartHolder = new EntityHolder<EppYearPart>();
    public EntityHolder<EppYearPart> getYearPartHolder() { return this.yearPartHolder; }
    public EppYearPart getYearPart() { return getYearPartHolder().getValue(); }
    public void setYearPart(final EppYearPart yearPart) { getYearPartHolder().setValue(yearPart); }

    private boolean controlOnlyMode = TrJournalManager.instance().dao().isGlobalControlOnlyMode();
    public boolean isControlOnlyMode() { return this.controlOnlyMode; }
    public void setControlOnlyMode(boolean controlOnlyMode) { this.controlOnlyMode = controlOnlyMode; }


    @Override
    public void onComponentRefresh() {
        this.getOrgUnitHolder().refresh(OrgUnit.class);
        this.getYearPartHolder().refresh();
    }

    @Override
    public void onBeforeDataSourceFetch(final IUIDataSource dataSource) {

    }

    public void onClickApply() {
        int size = TrOrgUnitManager.instance().dao().doGenerateJournals(getOrgUnit(), getYearPart(), isControlOnlyMode());

        deactivate();

        if (size > 0) {
            ContextLocal.getInfoCollector().add(
                _uiConfig.getProperty("info.created-n-journals", new Object[] { String.valueOf(size) })
            );
        } else {
            ContextLocal.getInfoCollector().add(
                _uiConfig.getProperty("info.created-no-journals")
            );

        }
    }

}
