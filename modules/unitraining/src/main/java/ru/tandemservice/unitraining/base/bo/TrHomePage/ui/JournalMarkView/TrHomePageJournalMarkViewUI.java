/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalMarkView;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.IPrincipalContext;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unitraining.base.bo.TrHomePage.TrHomePageManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * @author oleyba
 * @since 9/8/11
 */
@State({
    @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "journal.id")
})
public class TrHomePageJournalMarkViewUI extends UIPresenter
{
    private TrJournal journal = new TrJournal();

    private boolean hasAccess;
    private String accessErrorMessage;

    // actions

    @Override
    public void onComponentRefresh()
    {
        setJournal(IUniBaseDao.instance.get().get(TrJournal.class, getJournal().getId()));
        checkAccess();
    }

    // data access

    private void checkAccess()
    {
        IPrincipalContext principalContext = _uiConfig.getUserContext().getPrincipalContext();
        setHasAccess(false);
        setAccessErrorMessage(null);

        if (TrHomePageManager.instance().dao().checkPpsAccessForViewing(principalContext, getJournal())) {
            setHasAccess(true);
        }

        if (TrHomePageManager.instance().dao().checkResponsibleAccessForViewing(principalContext, getJournal())) {
            setHasAccess(true);
        }

        if (!isHasAccess()) {
            setAccessErrorMessage("Вы не можете просматривать журнал, так как не отмечены в нем в качестве преподавателя.");
        }
    }

    // getters and setters

    public TrJournal getJournal() {
        return journal;
    }

    public void setJournal(TrJournal journal) {
        this.journal = journal;
    }

    public boolean isHasAccess()
    {
        return hasAccess;
    }

    public void setHasAccess(boolean hasAccess)
    {
        this.hasAccess = hasAccess;
    }

    public String getAccessErrorMessage()
    {
        return accessErrorMessage;
    }

    public void setAccessErrorMessage(String accessErrorMessage)
    {
        this.accessErrorMessage = accessErrorMessage;
    }
}
