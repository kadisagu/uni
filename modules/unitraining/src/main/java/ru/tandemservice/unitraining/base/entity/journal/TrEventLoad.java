package ru.tandemservice.unitraining.base.entity.journal;

import org.tandemframework.core.view.formatter.StringLimitFormatter;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateMutable;
import ru.tandemservice.uniepp.base.bo.EppState.EppStatePath;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.unitraining.base.entity.journal.gen.TrEventLoadGen;

/**
 * Планируемое событие журнала (аудиторная нагрузка)
 */
@EppStatePath(TrJournalEvent.L_JOURNAL_MODULE+"."+TrJournalModule.L_JOURNAL)
@EppStateMutable({ TrJournalEvent.P_THEME, TrJournalEvent.L_TYPE, TrJournalEvent.P_COMMENT, TrJournalEvent.P_HAS_MARKS })
public class TrEventLoad extends TrEventLoadGen
{
    public TrEventLoad() {}

    public TrEventLoad(EppGroupType type) {
        setType(type);
    }

    public TrEventLoad(TrJournalModule journalModule, int number, EppGroupType type) {
        setJournalModule(journalModule);
        setNumber(number);
        setType(type);
    }


    @Override
    public TrJournalEvent getClone()
    {
        TrEventLoad newEvent = new TrEventLoad();
        newEvent.setJournalModule(getJournalModule());
        newEvent.setType(getType());
        newEvent.setNumber(getNumber() + 1);
        return newEvent;
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2719821")
    public String getTitle()
    {
        if (getType() == null) {
            return this.getClass().getSimpleName();
        }
        String title = getNumber() + ". " + getType().getTitle();
        if (null != getTheme())
            title = title + " (" + StringLimitFormatter.NON_BROKEN_WORDS.format(getTheme()) + ")";
        return title;
    }

    @Override
    public String getDisplayableTitle()
    {
        return getTitle();
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2719821")
    public String getShortTitle()
    {
        return getNumber() + ". " + getType().getTitle();
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=2719821")
    public String getAbbreviationTitle()
    {
        return getType().getAbbreviation() + getNumber();
    }

    @Override
    public String getTypeTitle()
    {
        return "Занятие";
    }
}