/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unitraining.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.Tab.TrOrgUnitTab;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        /**
         <component-tab name="abstractOrgUnit_TrOrgUnit" label="Журналы" before="abstractOrgUnit_OrgUnitSecSettings"	component-name="TrOrgUnitTab"
         permission-key="mvel:model.getPermissionKey('uniTrTabView_')" secured-object="fast:model.orgUnit"
         visible="ognl:@ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager@instance().dao().isTrOrgUnitTabVisible(model.getOrgUnitId())"/>
         */
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab("abstractOrgUnit_TrOrgUnit", TrOrgUnitTab.class).permissionKey("ui:secModel.uniTrTabView").visible("ognl:@ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager@instance().dao().isTrOrgUnitTabVisible(presenter.orgUnit.id)").parameters("mvel:['orgUnitId':presenter.orgUnit.id]"))
                .create();
    }
}
