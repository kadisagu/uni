package ru.tandemservice.unitraining.base.entity.journal;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.journal.gen.TrJournalGen;

import java.util.Arrays;
import java.util.Collection;

/**
 * Журнал
 */
public class TrJournal extends TrJournalGen implements IEppStateObject, INumberObject, ISecLocalEntityOwner, ITitled
{
    public TrJournal() {}
    public TrJournal(EppRegistryElementPart registryElementPart, EppYearPart yearPart) {
        setRegistryElementPart(registryElementPart);
        setYearPart(yearPart);
    }

    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1671209")
    @EntityDSLSupport(parts={P_NUMBER})
    @Override
    public String getCalculatedTitle() {
        StringBuilder s = new StringBuilder("№");
        s.append(this.getNumber()).append(" ").append(this.getRegistryElementPart().getTitle()).append(", ").append(this.getYearPart().getTitle());
        if (!StringUtils.isEmpty(getTitle()) && !getNumber().equalsIgnoreCase(getTitle()))
            s.append(" (").append(getTitle()).append(")");
        return s.toString();
    }

    @Override
    @EntityDSLSupport
    public String getDisplayableTitle()
    {
        return getCalculatedTitle();
    }

    @Override
    @EntityDSLSupport(parts={P_DURATION_BEGIN_DATE, P_DURATION_END_DATE})
    public String getDurationPeriodString() {
        return new StringBuilder()
        .append(null == getDurationBeginDate() ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(getDurationBeginDate()))
        .append(" - ")
        .append(null == getDurationEndDate() ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(getDurationEndDate()))
        .toString();
    }

    @Override
    public INumberGenerationRule getNumberGenerationRule() {
        return TrJournalManager.instance().dao().getNumberGenerationRule();
    }

    public OrgUnit getOrgUnit()
    {
        return getRegistryElementPart().getTutorOu();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return Arrays.asList((IEntity) getRegistryElementPart().getTutorOu());
    }

    @Override
    public String getTypeTitle()
    {
        return "Реализация дисциплины";
    }

    @Override
    public boolean isAllowEditSettings()
    {
        return getState().isFormative();
    }
}