/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.GlobalSettings;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.SessionBrsSettingsWrapper;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;

/**
 * @author oleyba
 * @since 8/17/12
 */
@Output({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "academy.id"),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType.id")
})
public class TrBrsCoefficientGlobalSettingsUI extends UIPresenter
{
    private OrgUnit academy;
    private TrBrsCoefficientOwnerType ownerType;
    private SessionBrsSettingsWrapper _globalSettings;

    @Override
    public void onComponentRefresh()
    {
        setAcademy(TopOrgUnit.getInstance());
        setOwnerType(DataAccessServices.dao().get(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerType.code(), TrBrsCoefficientOwnerTypeCodes.ACADEMY));
        _globalSettings = new SessionBrsSettingsWrapper(false);
    }

    public void onClickSaveGlobalSettings()
    {
        ISessionBrsDao.instance.get().saveSettings(getGlobalSettings());
    }

    public SessionBrsSettingsWrapper getGlobalSettings()
    {
        return _globalSettings;
    }

    public OrgUnit getAcademy()
    {
        return academy;
    }

    public void setAcademy(OrgUnit academy)
    {
        this.academy = academy;
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(TrBrsCoefficientOwnerType ownerType)
    {
        this.ownerType = ownerType;
    }
}
