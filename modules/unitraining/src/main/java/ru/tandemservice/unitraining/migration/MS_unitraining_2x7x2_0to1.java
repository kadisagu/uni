package ru.tandemservice.unitraining.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unitraining_2x7x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность trJournalEvent

		// раньше свойство type ссылалось на сущность eppALoadType
		// теперь оно ссылается на сущность eppGroupType
		{
            tool.dropConstraint("tr_journal_event", "fk_type_trjournalevent");

            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("select aload.eppgrouptype_id, jevent.id from tr_journal_event as jevent inner join epp_c_loadtype_a_t as aload on jevent.type_id = aload.id");
            PreparedStatement update = tool.prepareStatement("update tr_journal_event set type_id = ? where id = ?");

            ResultSet src = stmt.getResultSet();

            while (src.next())
            {
                Long groupTypeId = src.getLong(1);
                Long jEventId = src.getLong(2);

                update.setLong(1, groupTypeId);
                update.setLong(2, jEventId);
                update.executeUpdate();
            }
		}
    }
}