/* $Id:$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalStructureEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.unitraining.base.bo.TrHomePage.TrHomePageManager;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalMarkView.TrHomePageJournalMarkView;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.AddEvent.TrJournalAddEvent;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.AddEvent.TrJournalAddEventUI;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.Copy.TrJournalCopy;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.Edit.TrJournalEdit;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.Edit.TrJournalEditUI;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.EventThemeEdit.TrJournalEventThemeEdit;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.ModuleEdit.TrJournalModuleEdit;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.StructureView.ITrJournalStructureOwner;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author oleyba
 * @since 8/29/12
 */
@Output({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journal.id"),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_ID, binding = "journal.id"),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType.id")
})
public class TrHomePageJournalStructureEditUI extends UIPresenter implements ITrJournalStructureOwner
{
    public static final List<String> ALLOW_STATES = TrHomePageManager.ALLOW_EDIT_EVENT_CONFIG_STATES;

    private TrJournal journal = new TrJournal();
    private TrBrsCoefficientOwnerType ownerType = new TrBrsCoefficientOwnerType();

    private boolean empty;

    @Override
    public void onComponentRefresh()
    {
        Person person = PersonManager.instance().dao().getPerson(_uiConfig.getUserContext().getPrincipalContext());

        setEmpty(TrHomePageManager.instance().dao().getStructureCount(person, ALLOW_STATES) == 0);

        TrJournal journal = getSettings().get("journal");
        if (journal != null && journal.getResponsible().getPerson().equals(person)) {
            setJournal(journal);
        }
        else {
            setJournal(null);
        }

        setOwnerType(DataAccessServices.dao().get(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerType.code(), TrBrsCoefficientOwnerTypeCodes.JOURNAL));

        RenderComponent.deactivateAll();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(TrHomePageManager.TR_HOME_PAGE_PERSON_KEY, PersonManager.instance().dao().getPerson(_uiConfig.getUserContext().getPrincipalContext()));

        if(dataSource.getName().equals(TrJournalManager.TR_GROUP_WITH_PPS_DS))
            dataSource.put(TrJournalManager.TR_JOURNAL_JOURNAL_KEY, journal);
    }

    public void onClickShow()
    {
        saveSettings();
        setJournal((TrJournal) getSettings().get("journal"));
        for (IUIPresenter child : new ArrayList<IUIPresenter>(_uiSupport.getChildrenUI().values()))
            child.deactivate();
    }

    public void onClickEdit()
    {
        _uiActivation.asRegionDialog(TrJournalEdit.class)
        .parameter(IUIPresenter.PUBLISHER_ID, getJournal().getId())
        .parameter(TrJournalEditUI.BIND_ALLOW_EDIT_RESPONSIBLE_PPS, Boolean.FALSE)
        .   activate();
    }

    public void onClickCopy()
    {
        getActivationBuilder().asRegionDialog(TrJournalCopy.class)
                .parameter(UIPresenter.PUBLISHER_ID, getJournal().getId())
                .activate();
    }

    public void onClickViewMarks() {
        _uiActivation.asDesktopRoot(TrHomePageJournalMarkView.class)
        .parameter(IUIPresenter.PUBLISHER_ID, getJournal().getId())
        .activate();
    }

    public boolean isSelected()
    {
        return getJournal() != null;
    }

    @Override
    public String getPermissionKey()
    {
        return "";
    }

    @Override
    public boolean isEditable()
    {
        return getJournal() != null && ALLOW_STATES.contains(getJournal().getState().getCode());
    }

    @Override
    public List<IButtonInfo> getModuleButtonList()
    {
        boolean formativeState = EppState.STATE_FORMATIVE.equals(getJournal().getState().getCode());
        if (!formativeState) { return Collections.emptyList(); }

        return Arrays.<IButtonInfo>asList(
            new ButtonInfo("moduleEdit", "Редактировать", "img/general/edit.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    _uiActivation.asRegionDialog(TrJournalModuleEdit.class)
                    .parameter(IUIPresenter.PUBLISHER_ID, entityId)
                    .activate();
                }
            },
            new ButtonInfo("caEventAdd", "Добавить контрольное мероприятие", "img/general/add.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    _uiActivation.asRegionDialog(TrJournalAddEvent.class)
                    .parameter(IUIPresenter.PUBLISHER_ID, getJournal().getId())
                    .parameter(TrJournalAddEventUI.BIND_MODULE, entityId)
                    .parameter(TrJournalAddEventUI.BIND_IS_ADDITIONAL, Boolean.FALSE)
                    .activate();
                }
            }
        );
    }

    @Override
    public List<IButtonInfo> getEventButtonList()
    {
        boolean formativeState = EppState.STATE_FORMATIVE.equals(getJournal().getState().getCode());
        if (!formativeState) {
            return Arrays.<IButtonInfo>asList(
                new ButtonInfo("eventEdit", "Редактировать событие", "img/general/edit.png")
                {
                    @Override
                    public void onClick(Long entityId)
                    {
                        _uiActivation.asRegionDialog(TrJournalEventThemeEdit.class)
                        .parameter(IUIPresenter.PUBLISHER_ID, entityId)
                        .activate();
                    }
                }
            );
        }

        return Arrays.<IButtonInfo>asList(
            new ButtonInfo("eventEdit", "Редактировать событие", "img/general/edit.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    _uiActivation.asRegionDialog(TrJournalEventThemeEdit.class)
                    .parameter(IUIPresenter.PUBLISHER_ID, entityId)
                    .activate();
                }
            },
            new ButtonInfo("eventAdd", "Добавить событие", "img/general/plus_plus.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    TrJournalManager.instance().eventDao().copyEvent(entityId);
                    _uiSupport.setRefreshScheduled(true);
                }
            },
            new ButtonInfo("eventUp", "Переместить вверх", "img/general/up.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    TrJournalManager.instance().eventDao().moveEvent(entityId, false);
                    _uiSupport.setRefreshScheduled(true);
                }
            },
            new ButtonInfo("eventDown", "Переместить вниз", "img/general/down.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    TrJournalManager.instance().eventDao().moveEvent(entityId, true);
                    _uiSupport.setRefreshScheduled(true);
                }
            },
            new ButtonInfo("eventDelete", "Удалить событие", "img/general/delete.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    TrJournalManager.instance().eventDao().deleteEvent(entityId);
                    _uiSupport.setRefreshScheduled(true);
                }

                @Override
                public boolean isHasAlert()
                {
                    return true;
                }

                @Override
                public String getAlert(Long entityId)
                {
                    TrJournalEvent event = IUniBaseDao.instance.get().getNotNull(TrJournalEvent.class, entityId);
                    if (event.isHasMarks())
                        return "Удалить событие «" + event.getTitle() +"»? По событию были выставлены отметки, которые также будут удалены.";
                    return "Удалить событие «" + event.getTitle() +"»?";
                }
            }
        );
    }

    // getters and setters

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }

    public boolean isEmpty()
    {
        return empty;
    }

    public void setEmpty(boolean empty)
    {
        this.empty = empty;
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(TrBrsCoefficientOwnerType ownerType)
    {
        this.ownerType = ownerType;
    }
}
