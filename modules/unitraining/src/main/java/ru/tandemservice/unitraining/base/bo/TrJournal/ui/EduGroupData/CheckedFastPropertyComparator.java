/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.EduGroupData;

import java.util.Comparator;

import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.IEntity;

/**
 * @author oleyba
 * @since 8/12/11
 */
public class CheckedFastPropertyComparator<T extends IEntity> implements Comparator<T>
{
    private final String _property;

    public CheckedFastPropertyComparator(String property)
    {
        _property = property;
    }

    @Override
    public int compare(T o1, T o2)
    {
        Object v1 = FastBeanUtils.getValue(o1, _property);
        String s1 = v1==null ? "" : v1.toString();
        Object v2 = FastBeanUtils.getValue(o2, _property);
        String s2 = v2==null ? "" : v2.toString();
        return s1.compareTo(s2);
    }
}