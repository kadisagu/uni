package ru.tandemservice.unitraining.base.bo.TrStudent.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 30.06.2016
 */
public class TrStudentAttendanceHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
	public static final String PARAM_STUDENT_ID = "student";

	public static final String PROP_PART_TITLE = "partTitle";
	public static final String PROP_ABSENCE_MESSAGE = "absenceMessage";

	public TrStudentAttendanceHandler(String ownerId)
	{
		super(ownerId);
	}

	@Override
	protected DSOutput execute(DSInput input, ExecutionContext context)
	{
		Long studentId = context.getNotNull(PARAM_STUDENT_ID);

		final String eventAlias = "event";
		List<TrEduGroupEventStudent> events = new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, eventAlias)
				.where(eq(property(eventAlias, TrEduGroupEventStudent.studentWpe().student().id()), value(studentId)))
				.createStatement(getSession()).list();
		List<Term> terms = events.stream()
				.map(event -> event.getStudentWpe().getTerm())
				.distinct()
				.sorted((term1, term2) -> Integer.compare(term1.getIntValue(), term2.getIntValue()))
				.collect(Collectors.toList());

		DSOutput output = ListOutputBuilder.get(input, terms).build();
		for (DataWrapper wrapper : DataWrapper.wrap(output))
		{
			Term term = wrapper.getWrapped();
			String partTitle = events.stream().filter(e -> e.getStudentWpe().getTerm().equals(term)).findAny().get().getStudentWpe().getEppYearPart().getTitle();
			long eventsTotal = events.stream().filter(e -> e.getStudentWpe().getTerm().equals(term)).count();
			long absenceCount = events.stream().filter(e -> e.getStudentWpe().getTerm().equals(term) && Boolean.TRUE.equals(e.getAbsent())).count();
			String absenceStr = String.format("%d%% (%s из %s)", 100 * absenceCount / eventsTotal, absenceCountStr(absenceCount), eventsTotalStr(eventsTotal));
			wrapper.put(PROP_PART_TITLE, partTitle);
			wrapper.put(PROP_ABSENCE_MESSAGE, absenceStr);
		}
		if (output.getRecordList().size() == 0)
		{
			output.setCountRecord(5);
			output.setTotalSize(5);
		}
		return output;
	}

	private String absenceCountStr(long absenceCount)
	{
		return CommonBaseStringUtil.numberWithPostfixCase(absenceCount, "пропуск", "пропуска", "пропусков");
	}

	private String eventsTotalStr(long eventsTotal)
	{
		return CommonBaseStringUtil.numberWithPostfixCase(eventsTotal, "занятия", "занятий", "занятий");
	}
}
