/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuEduYearPartAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;

/**
 * @author oleyba
 * @since 8/17/12
 */
@Configuration
public class TrBrsCoefficientOuEduYearPartAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(trEduYearPartDSConfig())
        .create();
    }

    @Bean
    public UIDataSourceConfig trEduYearPartDSConfig()
    {
        return SelectDSConfig.with("trEduYearPartDS", this.getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn(EppYearPart.title().s())
                .handler(this.trEduYearPartDSHandler())
                .valueStyleSource(EducationCatalogsManager.getEduYearValueStyleSource())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trEduYearPartDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppYearPart.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                OrgUnit orgUnit = context.get(TrBrsCoefficientOuEduYearPartAddUI.ORG_UNIT_PARAM);
                if (orgUnit != null)
                {
                    dql.where(DQLExpressions.not(DQLExpressions.exists(
                            TrOrgUnitSettings.class,
                            TrOrgUnitSettings.L_ORG_UNIT, orgUnit,
                            TrOrgUnitSettings.L_EDU_YEAR_PART, DQLExpressions.property(alias)
                    )));
                }
            }
        }
                .order(EppYearPart.year().educationYear().intValue())
                .order(EppYearPart.part().number())
                .filter(EppYearPart.year().educationYear().title())
                .filter(EppYearPart.part().title())
                ;
    }
}