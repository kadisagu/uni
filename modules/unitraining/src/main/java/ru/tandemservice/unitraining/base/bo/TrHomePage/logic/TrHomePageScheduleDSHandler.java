/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/22/11
 */
public class TrHomePageScheduleDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_PERSON = "trHomePageScheduleDS_personKey";
    public static final String PARAM_START_DATE = "dateStart";
    public static final String PARAM_END_DATE = "dateEnd";

    public TrHomePageScheduleDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        Person person = context.get(PARAM_PERSON);
        Date start = context.get(PARAM_START_DATE);
        Date end = context.get(PARAM_END_DATE);

        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(TrEduGroupEvent.class, "ge").column("ge")

        .joinEntity("ge", DQLJoinType.inner, EppPpsCollectionItem.class, "pps", eq(property(EppPpsCollectionItem.list().fromAlias("pps")), property(TrEduGroupEvent.group().fromAlias("ge"))))
        .where(eq(property(EppPpsCollectionItem.pps().person().fromAlias("pps")), value(person)))

        .joinPath(DQLJoinType.inner, TrEduGroupEvent.scheduleEvent().fromAlias("ge"), "se")
        .where(or(
            betweenDays(ScheduleEvent.durationBegin().fromAlias("se"), start, end),
            betweenDays(ScheduleEvent.durationEnd().fromAlias("se"), start, end)))

        .fromEntity(TrJournalGroup.class, "jg")
        .where(eq(property(TrJournalGroup.group().fromAlias("jg")), property(TrEduGroupEvent.group().fromAlias("ge"))))
        .where(eq(property(TrJournalGroup.journal().fromAlias("jg")), property(TrEduGroupEvent.journalEvent().journalModule().journal().fromAlias("ge"))))

        .order(property(ScheduleEvent.durationBegin().fromAlias("se")))
        ;

        return DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
    }
}
