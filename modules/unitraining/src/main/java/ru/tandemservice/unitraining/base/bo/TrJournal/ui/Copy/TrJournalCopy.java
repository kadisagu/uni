/**
 *$Id$
 */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.Copy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 12.03.13
 */
@Configuration
public class TrJournalCopy extends BusinessComponentManager
{
    public static final String JOURNAL_DS = "journalDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(JOURNAL_DS, journalDSHandler())
                        .addColumn(TrJournal.calculatedTitle().s())
                        .addColumn(TrJournal.responsible().person().fio().s())
                        .addColumn(TrJournal.registryElementPart().registryElement().owner().shortTitle().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> journalDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), TrJournal.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final TrJournal journal = context.get(TrJournalCopyUI.PARAM_JOURNAL);
                final Boolean sameOrgUnit = context.get(TrJournalCopyUI.PARAM_SAME_ORG_UNIT);
                final Boolean sameRegElement = context.get(TrJournalCopyUI.PARAM_SAME_REGISTRY_ELEMENT);
                final Boolean sameRegElementPart = context.get(TrJournalCopyUI.PARAM_SAME_REGISTRY_ELEMENT_PART);
                final Boolean samePps = context.get(TrJournalCopyUI.PARAM_SAME_PPS);

                if (sameOrgUnit)
                    dql.where(eq(property(TrJournal.registryElementPart().registryElement().owner().fromAlias(alias)), value(journal.getRegistryElementPart().getRegistryElement().getOwner())));

                if (sameRegElement)
                    dql.where(eq(property(TrJournal.registryElementPart().registryElement().fromAlias(alias)), value(journal.getRegistryElementPart().getRegistryElement())));

                if (sameRegElementPart)
                    dql.where(eq(property(TrJournal.registryElementPart().fromAlias(alias)), value(journal.getRegistryElementPart())));

                if (samePps)
                    dql.where(eq(property(TrJournal.responsible().fromAlias(alias)), value(journal.getResponsible())));

                dql.where(ne(property(TrJournal.id().fromAlias(alias)), value(journal.getId())));
            }
        }
                .filter(TrJournal.number())
                .filter(TrJournal.title())
                .filter(TrJournal.registryElementPart().registryElement().title())
//                .filter(TrJournal.responsible().person().identityCard().fullFio())
                .order(TrJournal.title())
                .pageable(true);
    }
}
