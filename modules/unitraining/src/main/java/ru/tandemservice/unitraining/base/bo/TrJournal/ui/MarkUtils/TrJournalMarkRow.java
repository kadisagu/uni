/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;

/**
 * @author oleyba
 * @since 9/7/11
 */
public class TrJournalMarkRow extends IdentifiableWrapper<EppStudentWorkPlanElement>
{
    private final EppStudentWorkPlanElement student;
    private final boolean inGroup;
    private final boolean activeStatus;

    public TrJournalMarkRow(EppStudentWorkPlanElement student, boolean inGroup)
    {
        super(student.getId(), student.getStudent().getPerson().getIdentityCard().getFio());
        this.student = student;
        this.inGroup = inGroup;
        this.activeStatus = student.getStudent().getStatus().isActive();
    }

    public EppStudentWorkPlanElement getStudent() { return student; }
    public boolean isActive() { return this.activeStatus && this.inGroup; }

    public String getInactiveHelpMessage()
    {
        if (!this.activeStatus)
            return "Состояние студента: " + CommonBaseStringUtil.appendDot(this.student.getStudent().getStatus().getTitle());
        if (!this.inGroup)
            return "Студент переведен в другую учебную группу.";
        return "";
    }

}
