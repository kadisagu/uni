package ru.tandemservice.unitraining.base.entity.journal.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Модуль в реализации дисциплины
 *
 * Определяет, что в рамках журнала будет читаться определенный модуль
 * (объекты создаются автоматом по данным из части элемента реестра)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrJournalModuleGen extends EntityBase
 implements IBrsSettingsOwner, INaturalIdentifiable<TrJournalModuleGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.base.entity.journal.TrJournalModule";
    public static final String ENTITY_NAME = "trJournalModule";
    public static final int VERSION_HASH = 693719046;
    private static IEntityMeta ENTITY_META;

    public static final String L_JOURNAL = "journal";
    public static final String L_MODULE = "module";
    public static final String P_MODULE_BEGIN_DATE = "moduleBeginDate";
    public static final String P_MODULE_END_DATE = "moduleEndDate";

    private TrJournal _journal;     // Журнал
    private EppRegistryElementPartModule _module;     // Учебный модуль (для части элемента реестра)
    private Date _moduleBeginDate;     // Дата начала обучения по модулю
    private Date _moduleEndDate;     // Дата окончания обучения по модулю

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Журнал. Свойство не может быть null.
     */
    @NotNull
    public TrJournal getJournal()
    {
        return _journal;
    }

    /**
     * @param journal Журнал. Свойство не может быть null.
     */
    public void setJournal(TrJournal journal)
    {
        dirty(_journal, journal);
        _journal = journal;
    }

    /**
     * @return Учебный модуль (для части элемента реестра). Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPartModule getModule()
    {
        return _module;
    }

    /**
     * @param module Учебный модуль (для части элемента реестра). Свойство не может быть null.
     */
    public void setModule(EppRegistryElementPartModule module)
    {
        dirty(_module, module);
        _module = module;
    }

    /**
     * @return Дата начала обучения по модулю.
     */
    public Date getModuleBeginDate()
    {
        return _moduleBeginDate;
    }

    /**
     * @param moduleBeginDate Дата начала обучения по модулю.
     */
    public void setModuleBeginDate(Date moduleBeginDate)
    {
        dirty(_moduleBeginDate, moduleBeginDate);
        _moduleBeginDate = moduleBeginDate;
    }

    /**
     * @return Дата окончания обучения по модулю.
     */
    public Date getModuleEndDate()
    {
        return _moduleEndDate;
    }

    /**
     * @param moduleEndDate Дата окончания обучения по модулю.
     */
    public void setModuleEndDate(Date moduleEndDate)
    {
        dirty(_moduleEndDate, moduleEndDate);
        _moduleEndDate = moduleEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TrJournalModuleGen)
        {
            if (withNaturalIdProperties)
            {
                setJournal(((TrJournalModule)another).getJournal());
                setModule(((TrJournalModule)another).getModule());
            }
            setModuleBeginDate(((TrJournalModule)another).getModuleBeginDate());
            setModuleEndDate(((TrJournalModule)another).getModuleEndDate());
        }
    }

    public INaturalId<TrJournalModuleGen> getNaturalId()
    {
        return new NaturalId(getJournal(), getModule());
    }

    public static class NaturalId extends NaturalIdBase<TrJournalModuleGen>
    {
        private static final String PROXY_NAME = "TrJournalModuleNaturalProxy";

        private Long _journal;
        private Long _module;

        public NaturalId()
        {}

        public NaturalId(TrJournal journal, EppRegistryElementPartModule module)
        {
            _journal = ((IEntity) journal).getId();
            _module = ((IEntity) module).getId();
        }

        public Long getJournal()
        {
            return _journal;
        }

        public void setJournal(Long journal)
        {
            _journal = journal;
        }

        public Long getModule()
        {
            return _module;
        }

        public void setModule(Long module)
        {
            _module = module;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof TrJournalModuleGen.NaturalId) ) return false;

            TrJournalModuleGen.NaturalId that = (NaturalId) o;

            if( !equals(getJournal(), that.getJournal()) ) return false;
            if( !equals(getModule(), that.getModule()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getJournal());
            result = hashCode(result, getModule());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getJournal());
            sb.append("/");
            sb.append(getModule());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrJournalModuleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrJournalModule.class;
        }

        public T newInstance()
        {
            return (T) new TrJournalModule();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "journal":
                    return obj.getJournal();
                case "module":
                    return obj.getModule();
                case "moduleBeginDate":
                    return obj.getModuleBeginDate();
                case "moduleEndDate":
                    return obj.getModuleEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "journal":
                    obj.setJournal((TrJournal) value);
                    return;
                case "module":
                    obj.setModule((EppRegistryElementPartModule) value);
                    return;
                case "moduleBeginDate":
                    obj.setModuleBeginDate((Date) value);
                    return;
                case "moduleEndDate":
                    obj.setModuleEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "journal":
                        return true;
                case "module":
                        return true;
                case "moduleBeginDate":
                        return true;
                case "moduleEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "journal":
                    return true;
                case "module":
                    return true;
                case "moduleBeginDate":
                    return true;
                case "moduleEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "journal":
                    return TrJournal.class;
                case "module":
                    return EppRegistryElementPartModule.class;
                case "moduleBeginDate":
                    return Date.class;
                case "moduleEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrJournalModule> _dslPath = new Path<TrJournalModule>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrJournalModule");
    }
            

    /**
     * @return Журнал. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalModule#getJournal()
     */
    public static TrJournal.Path<TrJournal> journal()
    {
        return _dslPath.journal();
    }

    /**
     * @return Учебный модуль (для части элемента реестра). Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalModule#getModule()
     */
    public static EppRegistryElementPartModule.Path<EppRegistryElementPartModule> module()
    {
        return _dslPath.module();
    }

    /**
     * @return Дата начала обучения по модулю.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalModule#getModuleBeginDate()
     */
    public static PropertyPath<Date> moduleBeginDate()
    {
        return _dslPath.moduleBeginDate();
    }

    /**
     * @return Дата окончания обучения по модулю.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalModule#getModuleEndDate()
     */
    public static PropertyPath<Date> moduleEndDate()
    {
        return _dslPath.moduleEndDate();
    }

    public static class Path<E extends TrJournalModule> extends EntityPath<E>
    {
        private TrJournal.Path<TrJournal> _journal;
        private EppRegistryElementPartModule.Path<EppRegistryElementPartModule> _module;
        private PropertyPath<Date> _moduleBeginDate;
        private PropertyPath<Date> _moduleEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Журнал. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalModule#getJournal()
     */
        public TrJournal.Path<TrJournal> journal()
        {
            if(_journal == null )
                _journal = new TrJournal.Path<TrJournal>(L_JOURNAL, this);
            return _journal;
        }

    /**
     * @return Учебный модуль (для части элемента реестра). Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalModule#getModule()
     */
        public EppRegistryElementPartModule.Path<EppRegistryElementPartModule> module()
        {
            if(_module == null )
                _module = new EppRegistryElementPartModule.Path<EppRegistryElementPartModule>(L_MODULE, this);
            return _module;
        }

    /**
     * @return Дата начала обучения по модулю.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalModule#getModuleBeginDate()
     */
        public PropertyPath<Date> moduleBeginDate()
        {
            if(_moduleBeginDate == null )
                _moduleBeginDate = new PropertyPath<Date>(TrJournalModuleGen.P_MODULE_BEGIN_DATE, this);
            return _moduleBeginDate;
        }

    /**
     * @return Дата окончания обучения по модулю.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalModule#getModuleEndDate()
     */
        public PropertyPath<Date> moduleEndDate()
        {
            if(_moduleEndDate == null )
                _moduleEndDate = new PropertyPath<Date>(TrJournalModuleGen.P_MODULE_END_DATE, this);
            return _moduleEndDate;
        }

        public Class getEntityClass()
        {
            return TrJournalModule.class;
        }

        public String getEntityName()
        {
            return "trJournalModule";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
