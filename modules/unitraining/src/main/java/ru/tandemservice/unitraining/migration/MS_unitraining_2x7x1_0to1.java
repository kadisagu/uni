package ru.tandemservice.unitraining.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLInsertQuery;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.List;

/**
 * Автоматически сгенерированная миграция (но потом исправленная)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unitraining_2x7x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.16"),
                        new ScriptDependency("org.tandemframework.shared", "1.7.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность trOrgUnitSettings
        if (tool.columnExists("tr_ou_settings", "eduyearpart_id"))
            return;

        tool.table("tr_ou_settings").constraints().clear();

        // создать колонку на часть года
        tool.createColumn("tr_ou_settings", new DBColumn("eduyearpart_id", DBType.LONG));

        // Необходимо создать части года для годов, для которых они не созданы
        {
            List<Object[]> values = tool.executeQuery(MigrationUtils.processor(Long.class), "select y.id from epp_year_epp_t y where not exists(select * from epp_year_part_t p where p.year_id=y.id)");
            for (Object[] item : values)
            {
                createEppYearParts(tool, (Long) item[0]);
            }
        }

        final String funcPrefix = tool.getDataSource().getSqlFunctionPrefix();
        final ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        // Копируем строки года в виде строк для частей года
        {
            SQLInsertQuery ins = new SQLInsertQuery("tr_ou_settings");
            // Для createIdFromNumber нужно уникальное число, на основе которого сгенерится идентификатор
            // Сделаем хитро - получим это число из row_number + текущее время в миллисекундах (ну или любое другое псевдонеповторимое число)
            ins.set("id", funcPrefix + "createIdFromNumber(src.rowNum, src.discriminator)");
            ins.set("discriminator", "src.discriminator");
            ins.set("eduyear_id", "src.eduyear_id");
            ins.set("orgunit_id", "src.orgunit_id");
            ins.set("eduyearpart_id", "src.yp_id");
            ins.set("usecurrentratinginsession_p", "src.usecurrentratinginsession_p");
            ins.set("scrrntrtnginattsttn_p", "src.scrrntrtnginattsttn_p");
            ins.set("userscript_p", "src.userscript_p");
            ins.from(SQLFrom.select(
                    "select s.discriminator, s.orgunit_id, s.usecurrentratinginsession_p, s.scrrntrtnginattsttn_p, s.userscript_p, s.eduyear_id, yp.id yp_id, " +
                            "? + row_number() over (order by s.id) rowNum " +
                            "from tr_ou_settings s " +
                            "join epp_year_part_t yp on yp.year_id=s.eduyear_id ",
                    "src"
            ));

            tool.executeUpdate(translator.toSql(ins), System.currentTimeMillis());
        }

        // Мигрируем расширение ДВФУ (trOrgUnitSettingsFefuExt) путем размножения на каждую часть года
        if (tool.tableExists("trorgunitsettingsfefuext_t"))
        {
            SQLInsertQuery ins = new SQLInsertQuery("trorgunitsettingsfefuext_t");
            ins.set("id", funcPrefix + "createIdFromNumber(src.rowNum, src.discriminator)");
            ins.set("discriminator", "src.discriminator");
            ins.set("trorgunitsettings_id", "(select id from tr_ou_settings s where s.eduyearpart_id=src.yp_id and s.orgunit_id=src.orgunit_id)");
            ins.set("blocktutoredit_p", "src.blocktutoredit_p");
            ins.from(SQLFrom.select(
                    "select ext.discriminator, yp.id yp_id, s.orgunit_id, ext.blocktutoredit_p, " +
                            "? + row_number() over (order by ext.id) rowNum " +
                            "from trorgunitsettingsfefuext_t ext " +
                            "join tr_ou_settings s on ext.trorgunitsettings_id=s.id " +
                            "join epp_year_part_t yp on s.eduyear_id=yp.year_id ",
                    "src"
            ));

            tool.executeUpdate(translator.toSql(ins), System.currentTimeMillis());

            // Удаляем записи, которые ссылаются на год, а не на часть
            tool.executeUpdate("delete from trorgunitsettingsfefuext_t where trorgunitsettings_id in (select id from tr_ou_settings where eduyearpart_id is null)");
        }

        // Мигрируем ссылки на настройки (trBrsCoefficientValue) путем размножения на каждую часть года
        {
            SQLInsertQuery ins = new SQLInsertQuery("tr_brs_coef_val");
            ins.set("id", funcPrefix + "createIdFromNumber(src.rowNum, src.discriminator)");
            ins.set("discriminator", "src.discriminator");
            ins.set("owner_id", "(select id from tr_ou_settings s where s.eduyearpart_id=src.yp_id and s.orgunit_id=src.orgunit_id)");
            ins.set("definition_id", "src.definition_id");
            ins.set("valueaslong_p", "src.valueaslong_p");
            ins.from(SQLFrom.select(
                    "select v.discriminator, yp.id yp_id, s.orgunit_id, v.definition_id, v.valueaslong_p, " +
                            "? + row_number() over (order by v.id) rowNum " +
                            "from tr_brs_coef_val v " +
                            "join tr_ou_settings s on v.owner_id=s.id " +
                            "join epp_year_part_t yp on s.eduyear_id=yp.year_id ",
                    "src"
            ));

            tool.executeUpdate(translator.toSql(ins), System.currentTimeMillis());
        }


        // Удаляем записи, которые ссылаются на год, а не на часть
        tool.executeUpdate("delete from tr_brs_coef_val where owner_id in (select id from tr_ou_settings where eduyearpart_id is null)");
        tool.executeUpdate("delete from tr_ou_settings where eduyearpart_id is null");

        // удалить колонку года
        tool.dropColumn("tr_ou_settings", "eduyear_id");

        // сделать колонку части года NOT NULL
        tool.setColumnNullable("tr_ou_settings", "eduyearpart_id", false);
    }

    private void createEppYearParts(DBTool tool, Long eppYearId) throws Exception
    {
        // Для года eppYearId нет созданных частей

        // Сначала пытаемся найти РУПы для данного года и на основе них создаем части
        List<Object[]> values = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                "select distinct g.part_id from epp_workplan_t wp " +
                        "inner join developgridterm_t g on g.id = wp.cachedGridTerm_id " +
                        "where wp.year_id=? ",
                eppYearId
        );

        if (values.isEmpty())
        {
            // Если рабочих планов нет, создаем части года на основе разбиений учебного года (yearDistribution) с параметром "Используется"
            values = tool.executeQuery(
                    MigrationUtils.processor(Long.class),
                    "select p.id from yeardistributionpart_t p " +
                            "inner join yeardistribution_t d on d.id=p.yeardistribution_id " +
                            "where d.inuse_p=?",
                    true
            );
        }

        short discriminator = tool.entityCodes().ensure("eppYearPart");
        for (Object[] item : values)
        {
            tool.executeUpdate(
                    "insert into epp_year_part_t(id, discriminator, year_id, part_id) values(?,?,?,?)",
                    EntityIDGenerator.generateNewId(discriminator), discriminator, eppYearId, item[0]
            );
        }
    }

}