/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author oleyba
 * @since 9/20/11
 */
public interface ITrJournalPrintDao extends INeedPersistenceSupport
{
    /**
     * Печатает вспомогательную таблицу для выставления сессионной оценки в БРС.
     * @param journalGroupId Группа в журнале
     * @return rtf
     */
    @Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
    public byte[] printSessionScaleSheet(Long journalGroupId);
}
