/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.StructureEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.AddEvent.TrJournalAddEvent;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.AddEvent.TrJournalAddEventUI;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.EventThemeEdit.TrJournalEventThemeEdit;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.ModuleEdit.TrJournalModuleEdit;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.StructureView.ITrJournalStructureOwner;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;

import java.util.Arrays;
import java.util.List;

/**
 * @author oleyba
 * @since 8/30/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journal.id", required = true)
})
@Output({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journal.id")
})
public class TrJournalStructureEditUI extends UIPresenter implements ITrJournalStructureOwner
{
    private TrJournal journal = new TrJournal();

    @Override
    public void onComponentRefresh()
    {
        setJournal(IUniBaseDao.instance.get().get(TrJournal.class, getJournal().getId()));
        RenderComponent.deactivateAll();
    }

    @Override
    public String getPermissionKey()
    {
        return "editTrJournal";
    }

    @Override
    public boolean isEditable()
    {
        return !getJournal().getState().isReadOnlyState();
    }

    @Override
    public List<IButtonInfo> getModuleButtonList()
    {
        return Arrays.asList(
            (IButtonInfo) new ButtonInfo("moduleEdit", "Редактировать", "img/general/edit.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    _uiActivation.asRegionDialog(TrJournalModuleEdit.class)
                    .parameter(IUIPresenter.PUBLISHER_ID, entityId)
                    .activate();
                }
            },
            (IButtonInfo) new ButtonInfo("caEventAdd", "Добавить контрольное мероприятие", "img/general/add.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    _uiActivation.asRegionDialog(TrJournalAddEvent.class)
                    .parameter(IUIPresenter.PUBLISHER_ID, getJournal().getId())
                    .parameter(TrJournalAddEventUI.BIND_MODULE, entityId)
                    .parameter(TrJournalAddEventUI.BIND_IS_ADDITIONAL, Boolean.FALSE)
                    .activate();
                }
            }
        );
    }

    @Override
    public List<IButtonInfo> getEventButtonList()
    {
        return Arrays.<IButtonInfo>asList(
            new ButtonInfo("eventEdit", "Редактировать событие", "img/general/edit.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    _uiActivation.asRegionDialog(TrJournalEventThemeEdit.class)
                    .parameter(IUIPresenter.PUBLISHER_ID, entityId)
                    .activate();
                }
            },
            new ButtonInfo("eventAdd", "Добавить событие", "img/general/plus_plus.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    TrJournalManager.instance().eventDao().copyEvent(entityId);
                    _uiSupport.setRefreshScheduled(true);
                }
            },
            new ButtonInfo("eventUp", "Переместить вверх", "img/general/up.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    TrJournalManager.instance().eventDao().moveEvent(entityId, false);
                    _uiSupport.setRefreshScheduled(true);
                }
            },
            new ButtonInfo("eventDown", "Переместить вниз", "img/general/down.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    TrJournalManager.instance().eventDao().moveEvent(entityId, true);
                    _uiSupport.setRefreshScheduled(true);
                }
            },
            new ButtonInfo("eventDelete", "Удалить событие", "img/general/delete.png")
            {
                @Override
                public void onClick(Long entityId)
                {
                    TrJournalManager.instance().eventDao().deleteEvent(entityId);
                    _uiSupport.setRefreshScheduled(true);
                }

                @Override
                public boolean isHasAlert()
                {
                    return true;
                }

                @Override
                public String getAlert(Long entityId)
                {
                    TrJournalEvent event = IUniBaseDao.instance.get().getNotNull(TrJournalEvent.class, entityId);
                    if (event.isHasMarks())
                        return "Удалить событие «" + event.getTitle() +"»? По событию были выставлены отметки, которые также будут удалены.";
                    return "Удалить событие «" + event.getTitle() +"»?";
                }
            }
        );
    }

    // getters and setters

    public TrJournal getJournal()
    {
        return journal;
    }

    public void setJournal(TrJournal journal)
    {
        this.journal = journal;
    }
}
