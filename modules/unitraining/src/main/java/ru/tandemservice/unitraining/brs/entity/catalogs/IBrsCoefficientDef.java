/* $Id:$ */
package ru.tandemservice.unitraining.brs.entity.catalogs;

/**
 * @author oleyba
 * @since 2/4/13
 */
public interface IBrsCoefficientDef {
    
    String getUserCode();
    
    String getTitle();
    String getShortTitle();

    boolean isRequired();
    
    boolean isBoolValueType();

    TrBrsCoefficientOwnerType getOwnerType();
}
