package ru.tandemservice.unitraining.base.bo.TrOrgUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.BaseRegistryElementPartDSHandler;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.logic.ITrOrgUnitDao;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.logic.TrOrgUnitDao;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * @author vdanilov
 */
@Configuration
public class TrOrgUnitManager extends BusinessObjectManager {

    public static final String TR_ORGUNIT_YEARPART_DS = "trOrgUnitYearPartDS";

    public static final String TR_ORGUNIT_ACTIVITY_ELEMENT_PART_DS = "trOrgUnitregistryElementPartDS";
    public static final String TR_ORGUNIT_ORGUNIT_KEY = "trOrgUnitregistryElementPartDS_orgUnit";

    public static final String TR_ORGUNIT_PPS_DS = "trOrgUnitPpsDS";

    public static final String TR_ORGUNIT_JOURNAL_DS = "trOrgUnitJournalDS";
    public static final String TR_ORGUNIT_DISCIPLINE_KEY = "trOrgUnitJournalDS_dicipline";
    private static final String TR_ORGUNIT_YEAR_PART_KEY = "trOrgUnitJournalDS_yearPart";

    public static final String TR_ORGUNIT_PLACE_DS = "trOrgUnitPlaceDS";

    public static TrOrgUnitManager instance() {
        return instance(TrOrgUnitManager.class);
    }

    @Bean
    public ITrOrgUnitDao dao() {
        return new TrOrgUnitDao();
    }

    @Bean
    public UIDataSourceConfig trOrgUnitYearPartDSConfig() {
        return SelectDSConfig.with(TrOrgUnitManager.TR_ORGUNIT_YEARPART_DS, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.trOrgUnitYearPartDSHandler())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trOrgUnitYearPartDSHandler() {
        return new EntityComboDataSourceHandler(this.getName(), EppYearPart.class)
            .where(EppYearPart.part().yearDistribution().inUse(), Boolean.TRUE)
            .order(EppYearPart.year().educationYear().intValue(), OrderDirection.desc)
            // .order(EppYearPart.year().title())
            .order(EppYearPart.part().yearDistribution().amount())
            .order(EppYearPart.part().number())
            .filter(EppYearPart.year().educationYear().title())
            .filter(EppYearPart.year().title())
            .filter(EppYearPart.part().yearDistribution().title())
            .filter(EppYearPart.part().title());
    }

    @Bean
    public UIDataSourceConfig trOrgUnitregistryElementPartDSConfig()
    {
        return SelectDSConfig.with(TrOrgUnitManager.TR_ORGUNIT_ACTIVITY_ELEMENT_PART_DS, this.getName())
            .addColumn(EppRegistryElementPart.titleWithNumber().s())
            .dataSourceClass(SelectDataSource.class)
            .handler(this.trOrgUnitregistryElementPartDSHandler())
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trOrgUnitregistryElementPartDSHandler()
    {
        return new BaseRegistryElementPartDSHandler(this.getName())
            .where(EppRegistryElementPart.registryElement().owner(), TR_ORGUNIT_ORGUNIT_KEY)
            ;
    }

    @Bean
    public UIDataSourceConfig trOrgUnitPpsDSConfig()
    {
        return SelectDSConfig.with(TrOrgUnitManager.TR_ORGUNIT_PPS_DS, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .addColumn("ФИО", PpsEntry.person().fio().s())
        .addColumn("Подразделение", PpsEntry.orgUnit().shortTitle().s())
        .addColumn("Данные преподавателя", PpsEntry.titleInfo().s())
        .handler(this.trOrgUnitPpsDSHandler())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trOrgUnitPpsDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), PpsEntry.class)
            .where(PpsEntry.orgUnit(), TR_ORGUNIT_ORGUNIT_KEY)
            .where(PpsEntry.removalDate(), (Object) null)
            .order(PpsEntry.orgUnit().title())
            .order(PpsEntry.person().identityCard().fullFio())
            .filter(PpsEntry.person().identityCard().fullFio())
            .filter(PpsEntry.orgUnit().title())
            ;
    }

    @Bean
    public UIDataSourceConfig trOrgUnitJournalDSConfig()
    {
        return SelectDSConfig.with(TrOrgUnitManager.TR_ORGUNIT_JOURNAL_DS, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .handler(this.trOrgUnitJournalDSHandler())
            .addColumn(TrJournal.P_CALCULATED_TITLE)
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trOrgUnitJournalDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), TrJournal.class)
            .where(TrJournal.registryElementPart(), TR_ORGUNIT_DISCIPLINE_KEY)
            .where(TrJournal.yearPart(), TR_ORGUNIT_YEAR_PART_KEY)
            .where(TrJournal.state().code(), (Object) EppState.STATE_ACCEPTED)
            .order(TrJournal.title())
            .filter(TrJournal.title())
            ;
    }

    @Bean
    public UIDataSourceConfig trOrgUnitPlaceDSConfig()
    {
        return SelectDSConfig.with(TrOrgUnitManager.TR_ORGUNIT_PLACE_DS, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .handler(this.trOrgUnitPlaceDSHandler())
            .addColumn(UniplacesPlace.P_DISPLAYABLE_TITLE)
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trOrgUnitPlaceDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), UniplacesPlace.class)
            .order(UniplacesPlace.title())
            .filter(UniplacesPlace.title())
            ;
    }
}
