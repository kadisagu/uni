/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalGroup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import ru.tandemservice.unitraining.base.bo.TrHomePage.TrHomePageManager;

/**
 * @author oleyba
 * @since 8/30/11
 */
@Configuration
public class TrHomePageJournalGroup extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(TrHomePageManager.instance().trHomePageJournalGroupregistryElementPartDSConfig())
        .addDataSource(TrHomePageManager.instance().trHomePageJournalGroupEduGroupDSConfig())
        .addDataSource(TrHomePageManager.instance().trHomePageJournalGroupTypeDSConfig())
        .addDataSource(TrHomePageManager.instance().trHomePageJournalGroupYearPartDSConfig())
        .create();
    }
}
