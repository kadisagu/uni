/* $Id:$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.GlobalSettings;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;

/**
 * @author oleyba
 * @since 8/17/12
 */
public class TrJournalGlobalSettingsUI extends UIPresenter
{
    private boolean controlOnlyMode;
    private boolean hideAttendanceForControlEvents;

    @Override
    public void onComponentRefresh()
    {
        this.setControlOnlyMode(TrJournalManager.instance().dao().isGlobalControlOnlyMode());
        this.setHideAttendanceForControlEvents(TrJournalManager.instance().dao().isHideAttendanceForControlEvents());
    }

    public void onClickApply()
    {
        TrJournalManager.instance().dao().setGlobalControlOnlyMode(isControlOnlyMode());
        TrJournalManager.instance().dao().setHideAttendanceForControlEvents(isHideAttendanceForControlEvents());
        deactivate();
    }

    // getters and setters

    public boolean isControlOnlyMode() {
        return this.controlOnlyMode;
    }
    public void setControlOnlyMode(boolean controlOnlyMode) {
        this.controlOnlyMode = controlOnlyMode;
    }

    public boolean isHideAttendanceForControlEvents() {
        return hideAttendanceForControlEvents;
    }

    public void setHideAttendanceForControlEvents(boolean hideAttendanceForControlEvents) {
        this.hideAttendanceForControlEvents = hideAttendanceForControlEvents;
    }
}
