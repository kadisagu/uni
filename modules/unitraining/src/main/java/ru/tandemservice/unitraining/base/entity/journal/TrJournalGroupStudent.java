package ru.tandemservice.unitraining.base.entity.journal;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unitraining.base.entity.journal.gen.TrJournalGroupStudentGen;

/**
 * Студент в УГС реализации
 *
 * Определяет, в какой группе находился или находится студент в журнале.
 */
public class TrJournalGroupStudent extends TrJournalGroupStudentGen implements ITitled
{
    @Override
    public String getTitle()
    {
        if (getStudentWpe() == null) {
            return this.getClass().getSimpleName();
        }
        return "Студент " + getStudentWpe().getStudent().getFio() + " в УГС реализации (" + getGroup().getTitle() + ")";
    }

}