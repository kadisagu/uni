/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;

import java.util.Collections;
import java.util.Map;

/**
 * @author oleyba
 * @since 8/17/12
 */
public class TrBrsCoefficientDao extends UniBaseDao implements ITrBrsCoefficientDao
{
    @Override
    public TrOrgUnitSettings doAddOuSettings(OrgUnit orgUnit, EppYearPart yearPart)
    {
        TrOrgUnitSettings settings = getByNaturalId(new TrOrgUnitSettings.NaturalId(orgUnit, yearPart));
        if (null == settings) {
            settings = new TrOrgUnitSettings(orgUnit, yearPart);
            save(settings);
        }
        return settings;
    }

    @Override
    public void doSaveCoefficients(IBrsSettingsOwner owner, Map<TrBrsCoefficientDef, Object> values)
    {
        for (Map.Entry<TrBrsCoefficientDef, Object> valueEntry : values.entrySet()) {
            TrBrsCoefficientDef def = valueEntry.getKey();
            Object newValue = valueEntry.getValue();
            TrBrsCoefficientValue oldValue = getByNaturalId(new TrBrsCoefficientValue.NaturalId(owner, def));

            if (null == newValue) {
                if (null != oldValue)
                    delete(oldValue);
                continue;
            }

            if (null == oldValue)
                oldValue = new TrBrsCoefficientValue(owner, def);

            oldValue.setValue(newValue);
            saveOrUpdate(oldValue);
        }
    }

    @Override
    public boolean isCanEditFcaGroup(TrJournal journal)
    {
        EppRegistryElementPart discipline = journal.getRegistryElementPart();
        IEppRegElPartWrapper discWrapper = IEppRegistryDAO.instance.get().getRegistryElementPartDataMap(Collections.singletonList(discipline.getId())).get(discipline.getId());

        boolean hasExam = discWrapper.hasActions4Groups(EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM);
        boolean hasSetoff = discWrapper.hasActions4Groups(EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_SETOFF);

        return hasExam && hasSetoff;
    }

    @Override
    public EppFControlActionGroup getFcaGroup(TrJournal journal)
    {
        EppRegistryElementPart discipline = journal.getRegistryElementPart();
        IEppRegElPartWrapper discWrapper = IEppRegistryDAO.instance.get().getRegistryElementPartDataMap(Collections.singletonList(discipline.getId())).get(discipline.getId());

        boolean hasExam = discWrapper.hasActions4Groups(EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM);
        boolean hasSetoff = discWrapper.hasActions4Groups(EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_SETOFF);

        if (journal.getFcaGroup() != null) return journal.getFcaGroup();
        else if (hasExam && hasSetoff)
            throw new ApplicationException("Расчет рейтинга невозможен: по части дисциплины в реестре предусмотрен и экзамен, и зачет, при этом для реализации дисциплины не указано, к какому из мероприятий ее относить. Укажите для реализации дисциплины мероприятие в сессии на форме редактирования реализации.");

        if (hasExam)
            return getCatalogItem(EppFControlActionGroup.class, EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM);

        return getCatalogItem(EppFControlActionGroup.class, EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_SETOFF);
    }
}
