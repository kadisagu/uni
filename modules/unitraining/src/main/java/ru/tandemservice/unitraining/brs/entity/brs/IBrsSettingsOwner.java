/* $Id:$ */
package ru.tandemservice.unitraining.brs.entity.brs;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;

/**
 * @author oleyba
 * @since 8/16/12
 */
public interface IBrsSettingsOwner extends IEntity
{
    String getDisplayableTitle();
    
    String getTypeTitle();

    boolean isAllowEditSettings();
}
