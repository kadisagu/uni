/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.EventActionEditLoadType;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.dao.IUniBaseDao;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAction;

/**
 * @author oleyba
 * @since 8/23/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "event.id", required = true)
})
public class TrJournalEventActionEditLoadTypeUI extends UIPresenter
{
    private TrEventAction event = new TrEventAction();
    private ISelectModel loadTypeModel;

    @Override
    public void onComponentRefresh()
    {
        setEvent(IUniBaseDao.instance.get().get(TrEventAction.class, getEvent().getId()));
        setLoadTypeModel(new LazySimpleSelectModel<EppALoadType>(EppALoadType.class, EppALoadType.P_TITLE));
    }

    public void onClickApply()
    {
        IUniBaseDao.instance.get().saveOrUpdate(getEvent());
        deactivate();
    }

    public TrEventAction getEvent()
    {
        return event;
    }

    public void setEvent(TrEventAction event)
    {
        this.event = event;
    }

    public ISelectModel getLoadTypeModel()
    {
        return loadTypeModel;
    }

    public void setLoadTypeModel(ISelectModel loadTypeModel)
    {
        this.loadTypeModel = loadTypeModel;
    }
}
