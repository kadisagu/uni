/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * @author oleyba
 * @since 8/4/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitHolder.id", required = true),
    @Bind(key = TrOrgUnitJournalAddUI.YEARPART_ID, binding = "yearPartHolder.id")
})

public class TrOrgUnitJournalAddUI extends UIPresenter {
    public static final String YEARPART_ID = "yearPartId";

    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    private final EntityHolder<EppYearPart> yearPartHolder = new EntityHolder<EppYearPart>();
    private boolean controlOnlyMode = TrJournalManager.instance().dao().isGlobalControlOnlyMode();
    private TrJournal journal = new TrJournal();
    private PpsEntrySelectBlockData ppsData;

    @Override
    public void onComponentRefresh() {
        this.getOrgUnitHolder().refresh(OrgUnit.class);
        this.getYearPartHolder().refresh();
        getJournal().setYearPart(getYearPart());

        PpsEntrySelectBlockData ppsData = new PpsEntrySelectBlockData(getOrgUnit().getId());
        ppsData.setPpsFieldTitle("Ответственный преподаватель");
        ppsData.setPpsSourceFilterFieldTitle("Выбор ответственного преподавателя");
        setPpsData(ppsData);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (TrOrgUnitManager.TR_ORGUNIT_ACTIVITY_ELEMENT_PART_DS.equals(dataSource.getName()))
            dataSource.put(TrOrgUnitManager.TR_ORGUNIT_ORGUNIT_KEY, getOrgUnit());
    }

    public void onClickApply() {
        getJournal().setYearPart(getYearPart());
        getJournal().setResponsible(getPpsData().getSingleSelectedPps());
        TrJournalManager.instance().dao().doCreateTrJournal(getJournal(), isControlOnlyMode());
        deactivate();
    }

    // accessors

    public TrJournal getJournal() {
        return journal;
    }

    public void setJournal(TrJournal journal) {
        this.journal = journal;
    }

    public OrgUnit getOrgUnit() {
        return this.getOrgUnitHolder().getValue();
    }

    public EntityHolder<EppYearPart> getYearPartHolder() {
        return this.yearPartHolder;
    }

    public EppYearPart getYearPart() {
        return getYearPartHolder().getValue();
    }

    public void setYearPart(final EppYearPart yearPart) {
        getYearPartHolder().setValue(yearPart);
    }

    public OrgUnitHolder getOrgUnitHolder() {
        return this.orgUnitHolder;
    }

    public PpsEntrySelectBlockData getPpsData()
    {
        return ppsData;
    }

    public void setPpsData(PpsEntrySelectBlockData ppsData)
    {
        this.ppsData = ppsData;
    }

    public boolean isControlOnlyMode() {
        return this.controlOnlyMode;
    }

    public void setControlOnlyMode(boolean controlOnlyMode) {
        this.controlOnlyMode = controlOnlyMode;
    }
}
