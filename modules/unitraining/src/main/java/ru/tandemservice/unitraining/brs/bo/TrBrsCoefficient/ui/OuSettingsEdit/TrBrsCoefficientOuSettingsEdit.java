/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuSettingsEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 8/17/12
 */
@Configuration
public class TrBrsCoefficientOuSettingsEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .create();
    }
}
