package ru.tandemservice.unitraining.base.entity.journal.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Реализация дисциплины
 *
 * Реализация дисциплины, содержит набор событий по чтению дисциплины.
 * К реализации прикреплены УГС (по разным видам АН), которые будут обучаться изложенным в реализации способом: журналы УГС.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrJournalGen extends EntityBase
 implements IBrsSettingsOwner, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.base.entity.journal.TrJournal";
    public static final String ENTITY_NAME = "trJournal";
    public static final int VERSION_HASH = 1438287107;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_ELEMENT_PART = "registryElementPart";
    public static final String L_YEAR_PART = "yearPart";
    public static final String P_TITLE = "title";
    public static final String P_NUMBER = "number";
    public static final String P_DURATION_BEGIN_DATE = "durationBeginDate";
    public static final String P_DURATION_END_DATE = "durationEndDate";
    public static final String L_STATE = "state";
    public static final String P_COMMENT = "comment";
    public static final String L_FCA_GROUP = "fcaGroup";
    public static final String L_GRADE_SCALE = "gradeScale";
    public static final String L_RESPONSIBLE = "responsible";
    public static final String P_CONTROL_ONLY_MODE = "controlOnlyMode";
    public static final String P_HIDE_ATTENDANCE_FOR_CONTROL_EVENTS = "hideAttendanceForControlEvents";
    public static final String P_CALCULATED_TITLE = "calculatedTitle";
    public static final String P_DISPLAYABLE_TITLE = "displayableTitle";
    public static final String P_DURATION_PERIOD_STRING = "durationPeriodString";

    private EppRegistryElementPart _registryElementPart;     // Часть элемента реестра
    private EppYearPart _yearPart;     // Часть учебного года
    private String _title;     // Название
    private String _number;     // Номер
    private Date _durationBeginDate;     // Дата начала обучения
    private Date _durationEndDate;     // Дата окончания обучения
    private EppState _state;     // Состояние
    private String _comment;     // Комментарий
    private EppFControlActionGroup _fcaGroup;     // Тип ФИК
    private EppGradeScale _gradeScale;     // Шкала оценки по которой будет учтен рейтинг студента
    private PpsEntry _responsible;     // Ответственный (за формирование)
    private boolean _controlOnlyMode;     // Не включать в структуру чтения аудиторные занятия
    private boolean _hideAttendanceForControlEvents;     // Не отмечать посещение для контрольных мероприятий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * То, что будет читаться студентам (определяет к какому подразделению относится журнал)
     *
     * @return Часть элемента реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getRegistryElementPart()
    {
        return _registryElementPart;
    }

    /**
     * @param registryElementPart Часть элемента реестра. Свойство не может быть null.
     */
    public void setRegistryElementPart(EppRegistryElementPart registryElementPart)
    {
        dirty(_registryElementPart, registryElementPart);
        _registryElementPart = registryElementPart;
    }

    /**
     * Определяет учебный год и его часть, в рамках которых будет проходить обучение
     *
     * @return Часть учебного года. Свойство не может быть null.
     */
    @NotNull
    public EppYearPart getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть учебного года. Свойство не может быть null.
     */
    public void setYearPart(EppYearPart yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * Название журнала, служит идентификатором журнала для пользователя
     *
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата начала обучения.
     */
    public Date getDurationBeginDate()
    {
        return _durationBeginDate;
    }

    /**
     * @param durationBeginDate Дата начала обучения.
     */
    public void setDurationBeginDate(Date durationBeginDate)
    {
        dirty(_durationBeginDate, durationBeginDate);
        _durationBeginDate = durationBeginDate;
    }

    /**
     * @return Дата окончания обучения.
     */
    public Date getDurationEndDate()
    {
        return _durationEndDate;
    }

    /**
     * @param durationEndDate Дата окончания обучения.
     */
    public void setDurationEndDate(Date durationEndDate)
    {
        dirty(_durationEndDate, durationEndDate);
        _durationEndDate = durationEndDate;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public EppState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(EppState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        initLazyForGet("comment");
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        initLazyForSet("comment");
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * Если по части дисциплины предусмотрены для сдачи в сессии
     * и мероприятие с типом «Зачет», и мероприятие с типом «Экзамен»,
     * то это поле указывает, к какому из них относится рейтинг,
     * накопленный студентами в рамках данной реализации дисциплины.
     *
     * @return Тип ФИК.
     */
    public EppFControlActionGroup getFcaGroup()
    {
        return _fcaGroup;
    }

    /**
     * @param fcaGroup Тип ФИК.
     */
    public void setFcaGroup(EppFControlActionGroup fcaGroup)
    {
        dirty(_fcaGroup, fcaGroup);
        _fcaGroup = fcaGroup;
    }

    /**
     * @return Шкала оценки по которой будет учтен рейтинг студента.
     */
    public EppGradeScale getGradeScale()
    {
        return _gradeScale;
    }

    /**
     * @param gradeScale Шкала оценки по которой будет учтен рейтинг студента.
     */
    public void setGradeScale(EppGradeScale gradeScale)
    {
        dirty(_gradeScale, gradeScale);
        _gradeScale = gradeScale;
    }

    /**
     * Позволяет делегировать заполнение первоначальной информации по журналу на личную страницу ППС
     *
     * @return Ответственный (за формирование).
     */
    public PpsEntry getResponsible()
    {
        return _responsible;
    }

    /**
     * @param responsible Ответственный (за формирование).
     */
    public void setResponsible(PpsEntry responsible)
    {
        dirty(_responsible, responsible);
        _responsible = responsible;
    }

    /**
     * true, если в журналах не нужны события по ауд. занятиям. Опция влияет на автоматическое заполнение реализации событиями.
     *
     * @return Не включать в структуру чтения аудиторные занятия. Свойство не может быть null.
     */
    @NotNull
    public boolean isControlOnlyMode()
    {
        return _controlOnlyMode;
    }

    /**
     * @param controlOnlyMode Не включать в структуру чтения аудиторные занятия. Свойство не может быть null.
     */
    public void setControlOnlyMode(boolean controlOnlyMode)
    {
        dirty(_controlOnlyMode, controlOnlyMode);
        _controlOnlyMode = controlOnlyMode;
    }

    /**
     * true, если в графах для событий по контролю не нужно выводить поле для отметки посещения.
     *
     * @return Не отмечать посещение для контрольных мероприятий. Свойство не может быть null.
     */
    @NotNull
    public boolean isHideAttendanceForControlEvents()
    {
        return _hideAttendanceForControlEvents;
    }

    /**
     * @param hideAttendanceForControlEvents Не отмечать посещение для контрольных мероприятий. Свойство не может быть null.
     */
    public void setHideAttendanceForControlEvents(boolean hideAttendanceForControlEvents)
    {
        dirty(_hideAttendanceForControlEvents, hideAttendanceForControlEvents);
        _hideAttendanceForControlEvents = hideAttendanceForControlEvents;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TrJournalGen)
        {
            setRegistryElementPart(((TrJournal)another).getRegistryElementPart());
            setYearPart(((TrJournal)another).getYearPart());
            setTitle(((TrJournal)another).getTitle());
            setNumber(((TrJournal)another).getNumber());
            setDurationBeginDate(((TrJournal)another).getDurationBeginDate());
            setDurationEndDate(((TrJournal)another).getDurationEndDate());
            setState(((TrJournal)another).getState());
            setComment(((TrJournal)another).getComment());
            setFcaGroup(((TrJournal)another).getFcaGroup());
            setGradeScale(((TrJournal)another).getGradeScale());
            setResponsible(((TrJournal)another).getResponsible());
            setControlOnlyMode(((TrJournal)another).isControlOnlyMode());
            setHideAttendanceForControlEvents(((TrJournal)another).isHideAttendanceForControlEvents());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrJournalGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrJournal.class;
        }

        public T newInstance()
        {
            return (T) new TrJournal();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "registryElementPart":
                    return obj.getRegistryElementPart();
                case "yearPart":
                    return obj.getYearPart();
                case "title":
                    return obj.getTitle();
                case "number":
                    return obj.getNumber();
                case "durationBeginDate":
                    return obj.getDurationBeginDate();
                case "durationEndDate":
                    return obj.getDurationEndDate();
                case "state":
                    return obj.getState();
                case "comment":
                    return obj.getComment();
                case "fcaGroup":
                    return obj.getFcaGroup();
                case "gradeScale":
                    return obj.getGradeScale();
                case "responsible":
                    return obj.getResponsible();
                case "controlOnlyMode":
                    return obj.isControlOnlyMode();
                case "hideAttendanceForControlEvents":
                    return obj.isHideAttendanceForControlEvents();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "registryElementPart":
                    obj.setRegistryElementPart((EppRegistryElementPart) value);
                    return;
                case "yearPart":
                    obj.setYearPart((EppYearPart) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "durationBeginDate":
                    obj.setDurationBeginDate((Date) value);
                    return;
                case "durationEndDate":
                    obj.setDurationEndDate((Date) value);
                    return;
                case "state":
                    obj.setState((EppState) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "fcaGroup":
                    obj.setFcaGroup((EppFControlActionGroup) value);
                    return;
                case "gradeScale":
                    obj.setGradeScale((EppGradeScale) value);
                    return;
                case "responsible":
                    obj.setResponsible((PpsEntry) value);
                    return;
                case "controlOnlyMode":
                    obj.setControlOnlyMode((Boolean) value);
                    return;
                case "hideAttendanceForControlEvents":
                    obj.setHideAttendanceForControlEvents((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "registryElementPart":
                        return true;
                case "yearPart":
                        return true;
                case "title":
                        return true;
                case "number":
                        return true;
                case "durationBeginDate":
                        return true;
                case "durationEndDate":
                        return true;
                case "state":
                        return true;
                case "comment":
                        return true;
                case "fcaGroup":
                        return true;
                case "gradeScale":
                        return true;
                case "responsible":
                        return true;
                case "controlOnlyMode":
                        return true;
                case "hideAttendanceForControlEvents":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "registryElementPart":
                    return true;
                case "yearPart":
                    return true;
                case "title":
                    return true;
                case "number":
                    return true;
                case "durationBeginDate":
                    return true;
                case "durationEndDate":
                    return true;
                case "state":
                    return true;
                case "comment":
                    return true;
                case "fcaGroup":
                    return true;
                case "gradeScale":
                    return true;
                case "responsible":
                    return true;
                case "controlOnlyMode":
                    return true;
                case "hideAttendanceForControlEvents":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "registryElementPart":
                    return EppRegistryElementPart.class;
                case "yearPart":
                    return EppYearPart.class;
                case "title":
                    return String.class;
                case "number":
                    return String.class;
                case "durationBeginDate":
                    return Date.class;
                case "durationEndDate":
                    return Date.class;
                case "state":
                    return EppState.class;
                case "comment":
                    return String.class;
                case "fcaGroup":
                    return EppFControlActionGroup.class;
                case "gradeScale":
                    return EppGradeScale.class;
                case "responsible":
                    return PpsEntry.class;
                case "controlOnlyMode":
                    return Boolean.class;
                case "hideAttendanceForControlEvents":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrJournal> _dslPath = new Path<TrJournal>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrJournal");
    }
            

    /**
     * То, что будет читаться студентам (определяет к какому подразделению относится журнал)
     *
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getRegistryElementPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
    {
        return _dslPath.registryElementPart();
    }

    /**
     * Определяет учебный год и его часть, в рамках которых будет проходить обучение
     *
     * @return Часть учебного года. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getYearPart()
     */
    public static EppYearPart.Path<EppYearPart> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * Название журнала, служит идентификатором журнала для пользователя
     *
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата начала обучения.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getDurationBeginDate()
     */
    public static PropertyPath<Date> durationBeginDate()
    {
        return _dslPath.durationBeginDate();
    }

    /**
     * @return Дата окончания обучения.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getDurationEndDate()
     */
    public static PropertyPath<Date> durationEndDate()
    {
        return _dslPath.durationEndDate();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getState()
     */
    public static EppState.Path<EppState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * Если по части дисциплины предусмотрены для сдачи в сессии
     * и мероприятие с типом «Зачет», и мероприятие с типом «Экзамен»,
     * то это поле указывает, к какому из них относится рейтинг,
     * накопленный студентами в рамках данной реализации дисциплины.
     *
     * @return Тип ФИК.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getFcaGroup()
     */
    public static EppFControlActionGroup.Path<EppFControlActionGroup> fcaGroup()
    {
        return _dslPath.fcaGroup();
    }

    /**
     * @return Шкала оценки по которой будет учтен рейтинг студента.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getGradeScale()
     */
    public static EppGradeScale.Path<EppGradeScale> gradeScale()
    {
        return _dslPath.gradeScale();
    }

    /**
     * Позволяет делегировать заполнение первоначальной информации по журналу на личную страницу ППС
     *
     * @return Ответственный (за формирование).
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getResponsible()
     */
    public static PpsEntry.Path<PpsEntry> responsible()
    {
        return _dslPath.responsible();
    }

    /**
     * true, если в журналах не нужны события по ауд. занятиям. Опция влияет на автоматическое заполнение реализации событиями.
     *
     * @return Не включать в структуру чтения аудиторные занятия. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#isControlOnlyMode()
     */
    public static PropertyPath<Boolean> controlOnlyMode()
    {
        return _dslPath.controlOnlyMode();
    }

    /**
     * true, если в графах для событий по контролю не нужно выводить поле для отметки посещения.
     *
     * @return Не отмечать посещение для контрольных мероприятий. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#isHideAttendanceForControlEvents()
     */
    public static PropertyPath<Boolean> hideAttendanceForControlEvents()
    {
        return _dslPath.hideAttendanceForControlEvents();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getCalculatedTitle()
     */
    public static SupportedPropertyPath<String> calculatedTitle()
    {
        return _dslPath.calculatedTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getDisplayableTitle()
     */
    public static SupportedPropertyPath<String> displayableTitle()
    {
        return _dslPath.displayableTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getDurationPeriodString()
     */
    public static SupportedPropertyPath<String> durationPeriodString()
    {
        return _dslPath.durationPeriodString();
    }

    public static class Path<E extends TrJournal> extends EntityPath<E>
    {
        private EppRegistryElementPart.Path<EppRegistryElementPart> _registryElementPart;
        private EppYearPart.Path<EppYearPart> _yearPart;
        private PropertyPath<String> _title;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _durationBeginDate;
        private PropertyPath<Date> _durationEndDate;
        private EppState.Path<EppState> _state;
        private PropertyPath<String> _comment;
        private EppFControlActionGroup.Path<EppFControlActionGroup> _fcaGroup;
        private EppGradeScale.Path<EppGradeScale> _gradeScale;
        private PpsEntry.Path<PpsEntry> _responsible;
        private PropertyPath<Boolean> _controlOnlyMode;
        private PropertyPath<Boolean> _hideAttendanceForControlEvents;
        private SupportedPropertyPath<String> _calculatedTitle;
        private SupportedPropertyPath<String> _displayableTitle;
        private SupportedPropertyPath<String> _durationPeriodString;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * То, что будет читаться студентам (определяет к какому подразделению относится журнал)
     *
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getRegistryElementPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
        {
            if(_registryElementPart == null )
                _registryElementPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_REGISTRY_ELEMENT_PART, this);
            return _registryElementPart;
        }

    /**
     * Определяет учебный год и его часть, в рамках которых будет проходить обучение
     *
     * @return Часть учебного года. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getYearPart()
     */
        public EppYearPart.Path<EppYearPart> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new EppYearPart.Path<EppYearPart>(L_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * Название журнала, служит идентификатором журнала для пользователя
     *
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(TrJournalGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(TrJournalGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата начала обучения.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getDurationBeginDate()
     */
        public PropertyPath<Date> durationBeginDate()
        {
            if(_durationBeginDate == null )
                _durationBeginDate = new PropertyPath<Date>(TrJournalGen.P_DURATION_BEGIN_DATE, this);
            return _durationBeginDate;
        }

    /**
     * @return Дата окончания обучения.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getDurationEndDate()
     */
        public PropertyPath<Date> durationEndDate()
        {
            if(_durationEndDate == null )
                _durationEndDate = new PropertyPath<Date>(TrJournalGen.P_DURATION_END_DATE, this);
            return _durationEndDate;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getState()
     */
        public EppState.Path<EppState> state()
        {
            if(_state == null )
                _state = new EppState.Path<EppState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(TrJournalGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * Если по части дисциплины предусмотрены для сдачи в сессии
     * и мероприятие с типом «Зачет», и мероприятие с типом «Экзамен»,
     * то это поле указывает, к какому из них относится рейтинг,
     * накопленный студентами в рамках данной реализации дисциплины.
     *
     * @return Тип ФИК.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getFcaGroup()
     */
        public EppFControlActionGroup.Path<EppFControlActionGroup> fcaGroup()
        {
            if(_fcaGroup == null )
                _fcaGroup = new EppFControlActionGroup.Path<EppFControlActionGroup>(L_FCA_GROUP, this);
            return _fcaGroup;
        }

    /**
     * @return Шкала оценки по которой будет учтен рейтинг студента.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getGradeScale()
     */
        public EppGradeScale.Path<EppGradeScale> gradeScale()
        {
            if(_gradeScale == null )
                _gradeScale = new EppGradeScale.Path<EppGradeScale>(L_GRADE_SCALE, this);
            return _gradeScale;
        }

    /**
     * Позволяет делегировать заполнение первоначальной информации по журналу на личную страницу ППС
     *
     * @return Ответственный (за формирование).
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getResponsible()
     */
        public PpsEntry.Path<PpsEntry> responsible()
        {
            if(_responsible == null )
                _responsible = new PpsEntry.Path<PpsEntry>(L_RESPONSIBLE, this);
            return _responsible;
        }

    /**
     * true, если в журналах не нужны события по ауд. занятиям. Опция влияет на автоматическое заполнение реализации событиями.
     *
     * @return Не включать в структуру чтения аудиторные занятия. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#isControlOnlyMode()
     */
        public PropertyPath<Boolean> controlOnlyMode()
        {
            if(_controlOnlyMode == null )
                _controlOnlyMode = new PropertyPath<Boolean>(TrJournalGen.P_CONTROL_ONLY_MODE, this);
            return _controlOnlyMode;
        }

    /**
     * true, если в графах для событий по контролю не нужно выводить поле для отметки посещения.
     *
     * @return Не отмечать посещение для контрольных мероприятий. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#isHideAttendanceForControlEvents()
     */
        public PropertyPath<Boolean> hideAttendanceForControlEvents()
        {
            if(_hideAttendanceForControlEvents == null )
                _hideAttendanceForControlEvents = new PropertyPath<Boolean>(TrJournalGen.P_HIDE_ATTENDANCE_FOR_CONTROL_EVENTS, this);
            return _hideAttendanceForControlEvents;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getCalculatedTitle()
     */
        public SupportedPropertyPath<String> calculatedTitle()
        {
            if(_calculatedTitle == null )
                _calculatedTitle = new SupportedPropertyPath<String>(TrJournalGen.P_CALCULATED_TITLE, this);
            return _calculatedTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getDisplayableTitle()
     */
        public SupportedPropertyPath<String> displayableTitle()
        {
            if(_displayableTitle == null )
                _displayableTitle = new SupportedPropertyPath<String>(TrJournalGen.P_DISPLAYABLE_TITLE, this);
            return _displayableTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournal#getDurationPeriodString()
     */
        public SupportedPropertyPath<String> durationPeriodString()
        {
            if(_durationPeriodString == null )
                _durationPeriodString = new SupportedPropertyPath<String>(TrJournalGen.P_DURATION_PERIOD_STRING, this);
            return _durationPeriodString;
        }

        public Class getEntityClass()
        {
            return TrJournal.class;
        }

        public String getEntityName()
        {
            return "trJournal";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getCalculatedTitle();

    public abstract String getDisplayableTitle();

    public abstract String getDurationPeriodString();
}
