package ru.tandemservice.unitraining.base.bo.TrJournal.event;

import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.apache.log4j.Logger;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;

/**
 * @author vdanilov
 */
public class TrJournalModuleRefreshDSetListener implements IDSetEventListener {

    public void init()
    {
        // меняется состав модулей на дисциплину
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppRegistryElementPartModule.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppRegistryElementPartModule.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, EppRegistryElementPartModule.class, this);

        // меняется журнал
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, TrJournal.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, TrJournal.class, this);
    }

    @Override
    public void onEvent(DSetEvent event)
    {
        // проверяем, что все модули в нужных частях
        if (EppRegistryElementPartModule.class.isAssignableFrom(event.getMultitude().getEntityMeta().getEntityClass())) {

            Debug.begin("event");
            try {
                Number count = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPartModule.class, "rm")
                .fromEntity(TrJournalModule.class, "jm")
                .where(eq(property(TrJournalModule.module().fromAlias("jm")), property("rm")))
                .where(ne(property(TrJournalModule.journal().registryElementPart().fromAlias("jm")), property(EppRegistryElementPartModule.part().fromAlias("rm"))))
                .where(in(property("rm.id"), event.getMultitude().getInExpression()))
                .column(count(property("jm.id")))
                .createStatement(event.getContext()).uniqueResult();

                if (null != count && count.intValue() > 0) {
                    throw new ApplicationException(TrJournalManager.instance().getProperty("error-module-part-mismatch"));
                }
            } finally {
                Logger.getLogger(this.getClass()).info(Debug.end().toFullString());
            }
        }

        // созраняем отложенное событие
        TrJournalDaemonBean.DAEMON.registerAfterCompleteWakeUp(event);
    }

}
