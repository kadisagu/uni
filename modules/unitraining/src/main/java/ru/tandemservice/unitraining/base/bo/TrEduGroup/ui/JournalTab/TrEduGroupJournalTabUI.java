/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrEduGroup.ui.JournalTab;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.runtime.SecurityRuntime;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.component.edugroup.pub.GroupContent.IGroupContentParameters;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.unitraining.base.bo.TrEduGroup.ui.JournalLink.TrEduGroupJournalLink;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

/**
 * @author oleyba
 * @since 9/15/11
 */
@Input({
    @Bind(key="parameters", binding="parameters", required=true)
})
@Output({
    @Bind(key = TrJournalGroupMarkEditUI.GROUP_BINDING, binding = "journalGroupId"),
    @Bind(key = TrJournalGroupMarkEditUI.VIEW_MODE_BINDING, binding = "viewMode"),
    @Bind(key="parameters", binding="parameters")
})
public class TrEduGroupJournalTabUI extends UIPresenter
{
    private IGroupContentParameters parameters;

    private TrJournalGroup journalGroup;
    private Set<PpsEntry> tutors;
    private boolean viewMode;

    private String selectedJournalTabId;

    // actions

    @Override
    public void onComponentRefresh()
    {
        setJournalGroup(IUniBaseDao.instance.get().get(TrJournalGroup.class, TrJournalGroup.group().s(), getParameters().getGroup()));
        Object secObject = getParameters().getOrgUnitHolder().getValue();
        if (null == secObject)
            secObject = SecurityRuntime.getInstance().getCommonSecurityObject();
        boolean hasPermission = CoreServices.securityService().check((ISecured) secObject, ContextLocal.getUserContext().getPrincipalContext(), getSec().getPermission("pubJournalMark_eppEduGroup"));
        boolean stateAllowsEdit = getJournalGroup() != null && getJournalGroup().getJournal().getState().getCode().equals(EppState.STATE_ACCEPTED);
        setViewMode(!hasPermission || !stateAllowsEdit);

        List<PpsEntry> pps = new DQLSelectBuilder()
        .fromEntity(EppPpsCollectionItem.class, "rel")
        .column(property(EppPpsCollectionItem.pps().fromAlias("rel")))
        .where(eq(property(EppPpsCollectionItem.list().fromAlias("rel")), value(getParameters().getGroup())))
        .order(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("rel")))
        .createStatement(_uiSupport.getSession()).list();
        setTutors(new LinkedHashSet<PpsEntry>(pps));
    }

    public void onUnlinkJournal()
    {
        IUniBaseDao.instance.get().delete(getJournalGroup());
        deactivate();
    }

    public void onLinkJournal()
    {
        this._uiActivation.asRegionDialog(TrEduGroupJournalLink.class)
        .parameter(IUIPresenter.PUBLISHER_ID, this.getParameters().getGroup().getId())
        .activate();
    }

    // presenter

    public CommonPostfixPermissionModelBase getSec()
    {
        return getParameters().getOrgUnitHolder().getSecModel();
    }

    public boolean isShowJournal()
    {
        return getJournalGroup() != null;
    }

    public TrJournal getJournal()
    {
        return getJournalGroup().getJournal();
    }

    public Long getJournalGroupId()
    {
        return getJournalGroup() == null ? null : getJournalGroup().getId();
    }

    public boolean isUnlinkDisabled()
    {
        return getJournalGroup() == null || getJournalGroup().getJournal().getState().isArchived();
    }

    public boolean isJournalActive()
    {
        return getJournalGroup() != null && !(getJournal().getState().isFormative() || getJournal().getState().isAcceptable());
    }

    // getters and setters


    public TrJournalGroup getJournalGroup()
    {
        return journalGroup;
    }

    public void setJournalGroup(TrJournalGroup journalGroup)
    {
        this.journalGroup = journalGroup;
    }

    public boolean isViewMode()
    {
        return viewMode;
    }

    public void setViewMode(boolean viewMode)
    {
        this.viewMode = viewMode;
    }

    public IGroupContentParameters getParameters()
    {
        return parameters;
    }

    public void setParameters(IGroupContentParameters parameters)
    {
        this.parameters = parameters;
    }

    public Set<PpsEntry> getTutors()
    {
        return tutors;
    }

    public void setTutors(Set<PpsEntry> tutors)
    {
        this.tutors = tutors;
    }

    public String getSelectedJournalTabId()
    {
        return selectedJournalTabId;
    }

    public void setSelectedJournalTabId(String selectedJournalTabId)
    {
        this.selectedJournalTabId = selectedJournalTabId;
    }
}
