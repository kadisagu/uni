package ru.tandemservice.unitraining.base.entity.journal;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.unitraining.base.entity.journal.gen.TrJournalGroupGen;

/**
 * УГС(АН) в реализации дисциплины
 *
 * Определяет из каких УГС состоит журнал (по разным видам назгузки)
 * Все УГС считаются равноправными и не поточатся (на уровне журналов)
 */
public class TrJournalGroup extends TrJournalGroupGen implements ITitled
{
    @Override
    public String getTitle()
    {
        if (getGroup() == null) {
            return this.getClass().getSimpleName();
        }
        return "УГС " + getGroup().getTitle() + " в реализации " + getJournal().getCalculatedTitle();
    }
}