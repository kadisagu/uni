/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.StructureView;

import java.util.List;

/**
 * @author oleyba
 * @since 8/30/11
 */
public interface ITrJournalStructureOwner
{
    interface IButtonInfo
    {
        String getId();
        String getTitle();
        String getIcon();
        boolean isHasAlert();
        String getAlert(Long entityId);
        void onClick(Long rowEntityId);
        boolean isDisabled(Long rowEntityId);
    }

    String getPermissionKey();
    boolean isEditable();

    List<IButtonInfo> getEventButtonList();
    List<IButtonInfo> getModuleButtonList();

    public static abstract class ButtonInfo implements IButtonInfo
    {
        private String id;
        private String title;
        private String icon;

        public ButtonInfo(String id, String title, String icon)
        {
            this.id = id;
            this.title = title;
            this.icon = icon;
        }

        @Override
        public String getId() { return id; }
        @Override
        public String getTitle() { return title; }
        @Override
        public String getIcon() { return icon; }
        @Override
        public boolean isHasAlert() { return false; }
        @Override
        public String getAlert(Long entityId) { return null; }
        @Override
        public boolean isDisabled(Long rowEntityId) { return false; }
    }
}
