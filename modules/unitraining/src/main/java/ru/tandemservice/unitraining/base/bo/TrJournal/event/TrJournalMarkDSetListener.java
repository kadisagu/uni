/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.event;

import org.hibernate.Session;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.dql.dset.IAffectedDSet;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteAction;
import ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/12/11
 * 
 * Сохраняем того, кто выставил отметку и отмечаем события с отметками
 */
public class TrJournalMarkDSetListener implements IDSetEventListener
{
    public void init()
    {
        // создаются или обновляются события студентов
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, TrEduGroupEventStudent.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, TrEduGroupEventStudent.class, this);
    }

    @Override
    public void onEvent(final DSetEvent event)
    {
        final UserContext instance = UserContext.getInstance();
        if (null == instance) return;

        IAffectedDSet multitude = event.getMultitude();
        
        // если запрос на апдейт - смотрим, меняли балл-отметку о неявке-комментарий, или нет
        if (event.getEventType().equals(DSetEventType.afterUpdate))
        {
            // если запрос на апдейт - смотрим, меняли балл-отметку о неявке, или нет
            if (!multitude.getAffectedProperties().contains(TrEduGroupEventStudent.P_ABSENT)
            && !multitude.getAffectedProperties().contains(TrEduGroupEventStudent.P_GRADE_AS_LONG))
                return;
        }
        
        final Set<Long> entityIds = new HashSet<>(multitude.isSingular() ?
            Arrays.asList(multitude.getSingularEntity().getId()) :
                new DQLSelectBuilder().fromDataSource(multitude.getSource(), "e").column("e.id").createStatement(event.getContext()).<Long>list());

        final Set<Long> marked = new HashSet<>();
        marked.addAll(entityIds);
        BatchUtils.execute(entityIds, 50, new BatchUtils.Action<Long>() {
            @Override
            public void execute(Collection<Long> elements)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(TrEduGroupEventStudent.class, "e").column("e.id")
                    .where(in(property("e.id"), elements))
                    .where(isNull(property(TrEduGroupEventStudent.absent().fromAlias("e"))))
                    .where(isNull(property(TrEduGroupEventStudent.gradeAsLong().fromAlias("e"))))
                    ;
                marked.removeAll(dql.createStatement(event.getContext()).<Long>list());
            }
        });

        TrJournalMarkDSetListener.setMarkedBy.register(event, marked);
        TrJournalMarkDSetListener.setHasMarks.register(event, marked);

        TrJournalDaemonBean.MARK_DAEMON.registerAfterCompleteWakeUp(event);
    }

    private static final ParamTransactionCompleteAction<Long, Void> setMarkedBy = new ParamTransactionCompleteAction<Long, Void>()
    {
        @Override
        public Void beforeCompletion(final Session session, Collection<Long> params)
        {
            BatchUtils.execute(params, 256, new BatchUtils.Action<Long>()
                {
                @Override
                public void execute(final Collection<Long> ids)
                {
                    new DQLUpdateBuilder(TrEduGroupEventStudent.class)
                        .set(TrEduGroupEventStudent.L_MARKED_BY, value(UserContext.getInstance().getPrincipalContext()))
                        .where(in(TrEduGroupEventStudent.id(), ids))
                        .createStatement(session).execute();
                }
                });
            return null;
        }
    };

    private static final ParamTransactionCompleteAction<Long, Void> setHasMarks = new ParamTransactionCompleteAction<Long, Void>()
    {
        @Override
        public Void beforeCompletion(final Session session, Collection<Long> params)
        {
            BatchUtils.execute(params, 256, new BatchUtils.Action<Long>() {
                @Override public void execute(final Collection<Long> ids) {
                    new DQLUpdateBuilder(TrJournalEvent.class)
                        .set(TrJournalEvent.P_HAS_MARKS, value(Boolean.TRUE))
                        .where(in(
                            property("id"),
                            new DQLSelectBuilder()
                                .fromEntity(TrJournalEvent.class, "je")
                                .column(property("je.id"))
                                .joinEntity("je", DQLJoinType.inner, TrEduGroupEvent.class, "ge", eq(property(TrEduGroupEvent.journalEvent().fromAlias("ge")), property("je")))
                                .joinEntity("ge", DQLJoinType.inner, TrEduGroupEventStudent.class, "ges", eq(property(TrEduGroupEventStudent.event().fromAlias("ges")), property("ge")))
                                .where(in(property(TrEduGroupEventStudent.id().fromAlias("ges")), ids))
                                .buildQuery()
                        ))
                    .createStatement(session).execute();
                }
            });
            return null;
        }
    };
}
