/* $Id:$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit;

import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

/**
 * @author oleyba
 * @since 3/13/13
 */
public interface IJournalGroupMarkEditAdditionalButton {
    String getId();
    String getTitle();
    void onClickAction(TrJournalGroup group);
}
