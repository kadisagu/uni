package ru.tandemservice.unitraining.base.entity.journal.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unitraining.base.entity.journal.TrEventLoad;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Событие в реализации (ауд. занятие)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrEventLoadGen extends TrJournalEvent
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.base.entity.journal.TrEventLoad";
    public static final String ENTITY_NAME = "trEventLoad";
    public static final int VERSION_HASH = -1964434266;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TrEventLoadGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrEventLoadGen> extends TrJournalEvent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrEventLoad.class;
        }

        public T newInstance()
        {
            return (T) new TrEventLoad();
        }
    }
    private static final Path<TrEventLoad> _dslPath = new Path<TrEventLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrEventLoad");
    }
            

    public static class Path<E extends TrEventLoad> extends TrJournalEvent.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return TrEventLoad.class;
        }

        public String getEntityName()
        {
            return "trEventLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
