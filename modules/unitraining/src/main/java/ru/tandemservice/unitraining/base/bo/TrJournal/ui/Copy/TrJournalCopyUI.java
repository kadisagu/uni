/**
 *$Id$
 */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.Copy;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * @author Alexander Shaburov
 * @since 12.03.13
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "journalHolder.id", required = true)
})
public class TrJournalCopyUI extends UIPresenter
{
    public static final String PARAM_JOURNAL = "journal";
    public static final String PARAM_SAME_ORG_UNIT = "sameOrgUnit";
    public static final String PARAM_SAME_REGISTRY_ELEMENT = "sameRegistryElement";
    public static final String PARAM_SAME_REGISTRY_ELEMENT_PART = "sameRegistryElementPart";
    public static final String PARAM_SAME_PPS = "samePps";

    private EntityHolder<TrJournal> _journalHolder = new EntityHolder<>();

    private boolean _sameOrgUnit = true;
    private boolean _sameRegistryElement = true;
    private boolean _sameRegistryElementPart = true;
    private boolean _samePps = true;
    private TrJournal _journal;

    @Override
    public void onComponentRefresh()
    {
        _journalHolder.refresh();
    }

    public void onClickApply()
    {
        TrJournalManager.instance().dao().doCopy(_journal, _journalHolder.getValue());
        deactivate();
    }

    public boolean isHasMarks()
    {
        return TrJournalManager.instance().dao().hasMarks(_journalHolder.getValue());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_SAME_ORG_UNIT, _sameOrgUnit);
        dataSource.put(PARAM_SAME_REGISTRY_ELEMENT, _sameRegistryElement);
        dataSource.put(PARAM_SAME_REGISTRY_ELEMENT_PART, _sameRegistryElementPart);
        dataSource.put(PARAM_SAME_PPS, _samePps);
        dataSource.put(PARAM_JOURNAL, _journalHolder.getValue());
    }

    // Getters & Setters

    public EntityHolder<TrJournal> getJournalHolder()
    {
        return _journalHolder;
    }

    public void setJournalHolder(EntityHolder<TrJournal> journalHolder)
    {
        _journalHolder = journalHolder;
    }

    public boolean isSameOrgUnit()
    {
        return _sameOrgUnit;
    }

    public void setSameOrgUnit(boolean sameOrgUnit)
    {
        _sameOrgUnit = sameOrgUnit;
    }

    public boolean isSameRegistryElement()
    {
        return _sameRegistryElement;
    }

    public void setSameRegistryElement(boolean sameRegistryElement)
    {
        _sameRegistryElement = sameRegistryElement;
    }

    public boolean isSameRegistryElementPart()
    {
        return _sameRegistryElementPart;
    }

    public void setSameRegistryElementPart(boolean sameRegistryElementPart)
    {
        _sameRegistryElementPart = sameRegistryElementPart;
    }

    public boolean isSamePps()
    {
        return _samePps;
    }

    public void setSamePps(boolean samePps)
    {
        _samePps = samePps;
    }

    public TrJournal getJournal()
    {
        return _journal;
    }

    public void setJournal(TrJournal journal)
    {
        _journal = journal;
    }
}
