package ru.tandemservice.unitraining.base.bo.TrJournal.logic;

import com.google.common.collect.Iterables;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppStateGen;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.*;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class TrJournalDao extends UniBaseDao implements ITrJournalDao {

    private static final String GLOBAL_SETTINGS = "trJournalGlobalSettings.";
    private static final String GLOBAL_SETTINGS__CONTROL_ONLY = "controlOnlyMode";
    private static final String GLOBAL_SETTINGS__HIDE_ATTENDANCE_FOR_CONTROL_EVENTS = "hideAttendanceForCE";

    private static final String CACHE_GLOBAL_CONTROL_ONLY_MODE = "TrJournalDao.isGlobalControlOnlyMode";
    private static final String CACHE_GLOBAL_HIDE_ATTENDANCE_FOR_CONTROL_EVENTS = "TrJournalDao.isHideAttendanceForCE";

    @Override
    public boolean isGlobalControlOnlyMode() {
        {
            Boolean value = DaoCache.get(CACHE_GLOBAL_CONTROL_ONLY_MODE);
            if (null != value) { return value; }
        }

        final IDataSettings settings = DataSettingsFacade.getSettings(null, GLOBAL_SETTINGS);
        final boolean value = Boolean.TRUE.equals(settings.get(GLOBAL_SETTINGS__CONTROL_ONLY));
        DaoCache.put(CACHE_GLOBAL_CONTROL_ONLY_MODE, value);
        return value;
    }

    @Override
    public void setGlobalControlOnlyMode(boolean globalControlOnlyMode) {
        final IDataSettings settings = DataSettingsFacade.getSettings(null, GLOBAL_SETTINGS);
        settings.set(GLOBAL_SETTINGS__CONTROL_ONLY, globalControlOnlyMode);
        DataSettingsFacade.saveSettings(settings);
        DaoCache.put(CACHE_GLOBAL_CONTROL_ONLY_MODE, null); // сбрасываем
    }

    @Override
    public boolean isHideAttendanceForControlEvents() {
        {
            Boolean value = DaoCache.get(CACHE_GLOBAL_HIDE_ATTENDANCE_FOR_CONTROL_EVENTS);
            if (null != value) { return value; }
        }

        final IDataSettings settings = DataSettingsFacade.getSettings(null, GLOBAL_SETTINGS);
        final boolean value = Boolean.TRUE.equals(settings.get(GLOBAL_SETTINGS__HIDE_ATTENDANCE_FOR_CONTROL_EVENTS));
        DaoCache.put(CACHE_GLOBAL_HIDE_ATTENDANCE_FOR_CONTROL_EVENTS, value);
        return value;
    }

    @Override
    public void setHideAttendanceForControlEvents(boolean value) {
        final IDataSettings settings = DataSettingsFacade.getSettings(null, GLOBAL_SETTINGS);
        settings.set(GLOBAL_SETTINGS__HIDE_ATTENDANCE_FOR_CONTROL_EVENTS, value);
        DataSettingsFacade.saveSettings(settings);
        DaoCache.put(CACHE_GLOBAL_HIDE_ATTENDANCE_FOR_CONTROL_EVENTS, null); // сбрасываем
    }

    @Override
    public INumberGenerationRule<TrJournal> getNumberGenerationRule() {
        return new SimpleNumberGenerationRule<TrJournal>() {
            @Override public String getNumberQueueName(final TrJournal object) {
                return ClassUtils.getUserClass(object).getSimpleName()/*+"."+object.getYear().getEducationYear().getIntValue()*/;
            }

            @Override public Set<String> getUsedNumbers(final TrJournal object) {
                return new HashSet<>(
                new DQLSelectBuilder().fromEntity(ClassUtils.getUserClass(object), "o")
                .column(property(TrJournal.number().fromAlias("o")))
                /*.where(eq(property(TrJournal.year().educationYear().fromAlias("o")), value(object.getYear().getEducationYear())))*/
                .createStatement(TrJournalDao.this.getSession()).<String>list()
                );
            }
        };
    }

    @Override
    public TrJournal doCreateTrJournal(final EppRegistryElementPart registryElementPart, final EppYearPart yearPart, Boolean controlOnlyMode) {
        return this.doCreateTrJournal(new TrJournal(registryElementPart, yearPart), controlOnlyMode);
    }

    @Override
    public TrJournal doCreateTrJournal(final TrJournal journal, Boolean controlOnlyMode) {

        if (null == controlOnlyMode) {
            controlOnlyMode = isGlobalControlOnlyMode();
        }

        journal.setNumber(INumberQueueDAO.instance.get().getNextNumber(journal));
        journal.setControlOnlyMode(controlOnlyMode); // заполнение по умолчанию из настройки
        journal.setHideAttendanceForControlEvents(isHideAttendanceForControlEvents());
        journal.setTitle(journal.getNumber());
        journal.setState(this.<EppState>getByNaturalId(new EppStateGen.NaturalId(EppState.STATE_FORMATIVE)));

        List<EppGradeScale> scales = getGradeScales(journal);
        if (CollectionUtils.isNotEmpty(scales))
            journal.setGradeScale(scales.get(0));

        this.save(journal);
        return journal;
    }

    @Override
    public void doLinkEduGroups(TrJournal journal, List<EppRealEduGroup> groupList)
    {
        for (EppRealEduGroup group : groupList)
        {
            if (!journal.getRegistryElementPart().equals(group.getActivityPart()))
                continue;
            TrJournalGroup rel = getByNaturalId(new TrJournalGroup.NaturalId(journal, group));
            if (null != rel)
                continue;
            rel = new TrJournalGroup();
            rel.setGroup(group);
            rel.setJournal(journal);
            save(rel);
        }
    }

    @Override
    public Collection<EppStudentWorkPlanElement> getStudentList(TrJournalGroup group)
    {
        Set<EppStudentWorkPlanElement> students = new TreeSet<>(EppStudentWorkPlanElement.FULL_FIO_COMPARATOR);
        DQLSelectBuilder groupDQl = new DQLSelectBuilder()
        .fromEntity(EppRealEduGroupRow.class, "e")
        .where(eq(property(EppRealEduGroupRow.group().fromAlias("e")), value(group.getGroup())))
        .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("e"))))
        .column(property(EppRealEduGroupRow.studentWpePart().studentWpe().fromAlias("e")))
        ;
        students.addAll(groupDQl.createStatement(getSession()).<EppStudentWorkPlanElement>list());
        DQLSelectBuilder studentDQL = new DQLSelectBuilder()
        .fromEntity(TrEduGroupEventStudent.class, "event").column(property(TrEduGroupEventStudent.studentWpe().fromAlias("event")))
        .where(eq(property(TrEduGroupEventStudent.event().group().fromAlias("event")), value(group.getGroup())))
        .where(eq(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("event")), value(group.getJournal())))
        ;
        students.addAll(studentDQL.createStatement(getSession()).<EppStudentWorkPlanElement>list());
        return students;
    }

    @Override
    public Set<Long> getStudentGroupOUs(TrJournalGroup group)
    {
        Set<Long> ids = new HashSet<>();
        DQLSelectBuilder groupDQl = new DQLSelectBuilder()
        .fromEntity(EppRealEduGroupRow.class, "e")
        .where(eq(property(EppRealEduGroupRow.group().fromAlias("e")), value(group.getGroup())))
        .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("e"))))
        .column(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().educationOrgUnit().groupOrgUnit().id().fromAlias("e")))
        ;
        ids.addAll(groupDQl.createStatement(getSession()).<Long>list());
        DQLSelectBuilder studentDQL = new DQLSelectBuilder()
        .fromEntity(TrEduGroupEventStudent.class, "event")
        .column(property(TrEduGroupEventStudent.studentWpe().student().educationOrgUnit().groupOrgUnit().id().fromAlias("event")))
        .where(eq(property(TrEduGroupEventStudent.event().group().fromAlias("event")), value(group.getGroup())))
        .where(eq(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("event")), value(group.getJournal())))
        ;
        ids.addAll(studentDQL.createStatement(getSession()).<Long>list());
        return ids;
    }

    @Override
    public Map<EppStudentWorkPlanElement, Map<EppFControlActionGroup, TrJournal>> fillJournalMap(final Collection<EppStudentWorkPlanElement> studentSlots, final EppRegistryElementPart discipline) throws IllegalStateException
    {
        if (studentSlots.isEmpty())
            return Collections.emptyMap();

        EppStudentWorkPlanElement exampleStudent = studentSlots.iterator().next();

        final EppYearEducationProcess year = exampleStudent.getYear();
        final YearDistributionPart part = exampleStudent.getPart();

        final Map<EppStudentWorkPlanElement, Map<EppFControlActionGroup, TrJournal>> map = new HashMap<>();

        final EppRegistryElementPart actualDisc = null == discipline ? exampleStudent.getRegistryElementPart() : discipline;

        final Map<TrJournal, EppFControlActionGroup> fcaGroupMap = SafeMap.get(TrBrsCoefficientManager.instance().coefDao()::getFcaGroup);

        for (List<EppStudentWorkPlanElement> elements : Iterables.partition(studentSlots, DQL.MAX_VALUES_ROW_NUMBER)) {

            List<Object[]> rowList = new ArrayList<>();

            // студенты, актуальные в УГС и их журналы
            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(TrJournalGroup.class, "jg")
                    .where(eq(property(TrJournalGroup.journal().registryElementPart().fromAlias("jg")), value(actualDisc)))
                    .where(eq(property(TrJournalGroup.journal().yearPart().year().fromAlias("jg")), value(year)))
                    .where(eq(property(TrJournalGroup.journal().yearPart().part().fromAlias("jg")), value(part)))
                    .fromEntity(EppRealEduGroup4LoadTypeRow.class, "e")
                    .where(eq(property(EppRealEduGroup4LoadTypeRow.group().fromAlias("e")), property(TrJournalGroup.group().fromAlias("jg"))))
                    .where(isNull(property(EppRealEduGroup4LoadTypeRow.removalDate().fromAlias("e"))))
                    .where(in(property(EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().fromAlias("e")), elements))
                    .column(property(TrJournalGroup.journal().fromAlias("jg")))
                    .column(property(EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().fromAlias("e")))
                    ;

            rowList.addAll(dql.createStatement(getSession()).<Object[]>list());

            // студенты, актуальные в журналах и их журналы
            dql = new DQLSelectBuilder()
                    .fromEntity(TrJournalGroupStudent.class, "s")
                    .joinPath(DQLJoinType.inner, TrJournalGroupStudent.group().journal().fromAlias("s"), "j")
                    .where(eq(property(TrJournal.registryElementPart().fromAlias("j")), value(actualDisc)))
                    .where(eq(property(TrJournal.yearPart().year().fromAlias("j")), value(year)))
                    .where(eq(property(TrJournal.yearPart().part().fromAlias("j")), value(part)))
                    .where(isNull(TrJournalGroupStudent.transferDate().fromAlias("s")))
                    .where(in(property(TrJournalGroupStudent.studentWpe().fromAlias("s")), elements))
                    .column(property("j"))
                    .column(property(TrJournalGroupStudent.studentWpe().fromAlias("s")))
            ;

            rowList.addAll(dql.createStatement(getSession()).<Object[]>list());


            for (Object[] row : rowList) {
                TrJournal journal = (TrJournal) row[0];
                EppStudentWorkPlanElement slot = (EppStudentWorkPlanElement) row[1];
                EppFControlActionGroup fcaGroup = fcaGroupMap.get(journal);
                TrJournal oldJournal = SafeMap.safeGet(map, slot, HashMap.class).put(fcaGroup, journal);
                if (null != oldJournal && !oldJournal.equals(journal)) {
                    throw new ApplicationException("Расчет рейтинга невозможен: студент «" + slot.getStudent().getFio() + "» присутствует в двух реализациях, отнесенных к одному и тому же контролю в рамках сессии: «" + journal.getDisplayableTitle() + "», «" + oldJournal.getCalculatedTitle()+"».");
                }
            }
        }

        return map;
    }

    @Override
    public Collection<EppStudentWorkPlanElement> getActualStudentList(TrJournal journal)
    {
        Set<EppStudentWorkPlanElement> students = new TreeSet<>(EppStudentWorkPlanElement.FULL_FIO_COMPARATOR);

        //long time = new Date().getTime();

        DQLSelectBuilder groupDQl = new DQLSelectBuilder()
        .fromEntity(TrJournalGroup.class, "jg")
        .where(eq(property(TrJournalGroup.journal().fromAlias("jg")), value(journal)))
        .fromEntity(EppRealEduGroupRow.class, "e")
        .where(eq(property(EppRealEduGroupRow.group().fromAlias("e")), property(TrJournalGroup.group().fromAlias("jg"))))
        .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("e"))))
        .column(property(EppRealEduGroupRow.studentWpePart().studentWpe().fromAlias("e")))
        ;

        students.addAll(groupDQl.createStatement(getSession()).<EppStudentWorkPlanElement>list());

        //        System.out.println("groupDQL " + (new Date().getTime() - time)); time = new Date().getTime();

        DQLSelectBuilder studentDQL = new DQLSelectBuilder()
        .fromEntity(TrEduGroupEventStudent.class, "event").column(property(TrEduGroupEventStudent.studentWpe().fromAlias("event")))
        .where(eq(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("event")), value(journal)))
        .where(isNull(property(TrEduGroupEventStudent.transferDate().fromAlias("event"))))
        ;
        students.addAll(studentDQL.createStatement(getSession()).<EppStudentWorkPlanElement>list());

        //        System.out.println("studentDQL " + (new Date().getTime() - time)); time = new Date().getTime();

        return students;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean hasMarks(TrJournal journal)
    {
        final List<Boolean> resultList = new DQLSelectBuilder().fromEntity(TrJournalEvent.class, "e").column(property(TrJournalEvent.hasMarks().fromAlias("e")))
        .where(eq(property(TrJournalEvent.journalModule().journal().fromAlias("e")), value(journal)))
        .createStatement(getSession()).list();

        return resultList.contains(Boolean.TRUE);
    }

    @Override
    public void doCopy(TrJournal sourceJournal, TrJournal targetJournal)
    {
        /*
            1. Поднимаем и подготавливаем необходимые данные
         */
        final List<TrJournalModule> existModuleList = new DQLSelectBuilder().fromEntity(TrJournalModule.class, "m").column(property("m"))
        .where(eq(property(TrJournalModule.journal().fromAlias("m")), value(targetJournal)))
        .createStatement(getSession()).list();

        final List<EppRegistryElementPartModule> registryModuleList = new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "m").column(property("m"))
        .where(eq(property(EppRegistryElementPartModule.part().fromAlias("m")), value(targetJournal.getRegistryElementPart())))
        .createStatement(getSession()).list();

        final List<EppRegistryElementPartModule> existRegElementModuleList = CommonBaseUtil.getPropertiesList(existModuleList, TrJournalModule.module().s());
        final List<TrJournalModule> tagertModuleList = new ArrayList<>();
        for (EppRegistryElementPartModule module : registryModuleList)
        {
            if (!existRegElementModuleList.contains(module))
            {
                final TrJournalModule journal = new TrJournalModule(targetJournal, module);
                save(journal);
                tagertModuleList.add(journal);
            }
        }
        for (TrJournalModule module : existModuleList)
        {
            if (registryModuleList.contains(module.getModule()))
                tagertModuleList.add(module);
            else
                new DQLDeleteBuilder(TrJournalModule.class)
            .where(eq(property(TrJournalModule.id()), value(module.getId())))
            .createStatement(getSession()).execute();
        }

        final Map<TrJournalModule, List<EppGroupType>> targetModuleLoadTypeMap = new HashMap<>();
        for (TrJournalModule module : tagertModuleList)
        {
            targetModuleLoadTypeMap.put(module,
                                        new DQLSelectBuilder().fromEntity(EppRegistryModuleALoad.class, "l")
                                                .column(property(EppRegistryModuleALoad.loadType().eppGroupType().fromAlias("l")))
                                                .where(eq(property(EppRegistryModuleALoad.module().fromAlias("l")), value(module.getModule().getModule())))
                                                .createStatement(getSession()).<EppGroupType>list());
        }

        final List<TrJournalEvent> sourceEventList = new DQLSelectBuilder().fromEntity(TrJournalEvent.class, "e").column(property("e"))
        .where(eq(property(TrJournalEvent.journalModule().journal().fromAlias("e")), value(sourceJournal)))
        .order(property(TrJournalEvent.journalModule().module().number().fromAlias("e")), OrderDirection.asc)
        .createStatement(getSession()).list();

        final Map<TrJournalModule, List<TrJournalEvent>> sourceModuleEventMap = new LinkedHashMap<>();
        for (TrJournalEvent event : sourceEventList)
        {
            SafeMap.safeGet(sourceModuleEventMap, event.getJournalModule(), ArrayList.class)
            .add(event);
        }
        for (Map.Entry<TrJournalModule, List<TrJournalEvent>> sourceEntry : sourceModuleEventMap.entrySet())
            Collections.sort(sourceEntry.getValue(), new EntityComparator<>(new EntityOrder(TrJournalEvent.number().s(), OrderDirection.asc)));

        final DQLSelectBuilder sourceEventDQL = new DQLSelectBuilder().fromEntity(TrJournalEvent.class, "e").column(property("e"))
        .where(eq(property(TrJournalEvent.journalModule().journal().fromAlias("e")), value(sourceJournal)));

        final List<TrBrsCoefficientValue> sourceBrsValueList = new DQLSelectBuilder().fromEntity(TrBrsCoefficientValue.class, "v").column(property("v"))
        .where(or(
            in(property(TrBrsCoefficientValue.owner().fromAlias("v")), sourceEventDQL.buildQuery()),
            in(property(TrBrsCoefficientValue.owner().fromAlias("v")), sourceModuleEventMap.keySet()),
            eq(property(TrBrsCoefficientValue.owner().fromAlias("v")), value(sourceJournal))))
            .createStatement(getSession()).list();

        final Map<IBrsSettingsOwner, List<TrBrsCoefficientValue>> sourceBrsValueMap = SafeMap.get(ArrayList.class);
        for (TrBrsCoefficientValue brsValue : sourceBrsValueList)
        {
            sourceBrsValueMap.get(brsValue.getOwner())
            .add(brsValue);
        }

        /*
            2. Удаляем все события в текущей реализации
         */
        doDeleteJournalEvents(targetJournal);

        /*
            3. Создаем события, копируя полностью события реализации-источника.
               Копируются Значения коэф. БРС Реализации, Модуля и Событий.
         */
        {
            final List<TrBrsCoefficientDef> targetBrsDefList = new DQLSelectBuilder().fromEntity(TrBrsCoefficientValue.class, "v").column(property(TrBrsCoefficientValue.definition().fromAlias("v")))
            .where(eq(property(TrBrsCoefficientValue.owner().fromAlias("v")), value(targetJournal)))
            .createStatement(getSession()).list();
            final Map<TrBrsCoefficientDef, Object> map = new HashMap<>();
            for (TrBrsCoefficientValue value : sourceBrsValueMap.get(sourceJournal))
            {
                if (!targetBrsDefList.contains(value.getDefinition()))
                    map.put(value.getDefinition(), value.getValue());
            }
            TrBrsCoefficientManager.instance().coefDao().doSaveCoefficients(targetJournal, map);
        }

        for (Map.Entry<TrJournalModule, List<TrJournalEvent>> sourceEntry : sourceModuleEventMap.entrySet())
        {
            final TrJournalModule sourceModule = sourceEntry.getKey();

            final TrJournalModule targetModule = getTargetModule(sourceModule, tagertModuleList, true);
            targetModule.update(sourceModule, false);

            {
                final TrJournalModule module = getTargetModule(sourceModule, tagertModuleList, false);
                if (module != null)
                {
                    final Map<TrBrsCoefficientDef, Object> map = new HashMap<>();
                    for (TrBrsCoefficientValue value : sourceBrsValueMap.get(sourceModule))
                        map.put(value.getDefinition(), value.getValue());
                    TrBrsCoefficientManager.instance().coefDao().doSaveCoefficients(module, map);
                }
            }

            final List<EppGroupType> targetEventLoadTypeList = new ArrayList<>();
            final List<EppGroupType> groupTypeList = targetModuleLoadTypeMap.get(targetModule);
            for (TrJournalEvent sourceEvent : sourceEntry.getValue())
            {
                TrJournalEvent targetEvent = null;
                if (sourceEvent instanceof TrEventLoad)
                {
                    if (!groupTypeList.contains(sourceEvent.getType()))
                        continue;

                    targetEvent = new TrEventLoad();
                    targetEvent.update(sourceEvent);
                }
                else if (sourceEvent instanceof TrEventAction)
                {
                    targetEvent = new TrEventAction();
                    targetEvent.update(sourceEvent);

                    if (!groupTypeList.contains(sourceEvent.getType()))
                    {
                        final EppGroupTypeALT groupType = getGroupType(groupTypeList);
                        if (groupType == null)
                            throw new ApplicationException(TrJournalManager.instance().getProperty("trJournalDao.doCopy.not-loadType", targetModule.getDisplayableTitle()));
                        targetEvent.setType(groupType);
                    }
                }

                if (targetEvent != null)
                {
                    targetEvent.setHasMarks(false);
                    TrJournalManager.instance().eventDao().saveNewEvent(targetEvent, targetModule, null);
                    targetEventLoadTypeList.add(targetEvent.getType());

                    final Map<TrBrsCoefficientDef, Object> map = new HashMap<>();
                    for (TrBrsCoefficientValue value : sourceBrsValueMap.get(sourceEvent))
                        map.put(value.getDefinition(), value.getValue());
                    TrBrsCoefficientManager.instance().coefDao().doSaveCoefficients(targetEvent, map);
                }
            }

            // если в реестре у модуля указаны часы по некоторому УГС, и при этом в итоге копирования в модуле нет ни одного события по этому УГС - создаем такое событие, ровно одно
            for (EppGroupType type : groupTypeList)
            {
                if (!targetEventLoadTypeList.contains(type))
                {
                    TrEventLoad event = new TrEventLoad(targetModule, 1, type);
                    TrJournalManager.instance().eventDao().saveNewEvent(event, targetModule, null);
                    targetEventLoadTypeList.add(type);
                }
            }
        }
    }

    protected void doDeleteJournalEvents(TrJournal journal)
    {
        new DQLDeleteBuilder(TrJournalEvent.class)
        .where(eq(property(TrJournalEvent.journalModule().journal()), value(journal)))
        .createStatement(getSession()).execute();
    }

    /**
     * Выбирает подходящий вид УГС из списка, по приоритету: лекции-практики-лабораторные.
     * @param groupTypeList список УГС
     * @return подходящий вид УГС
     */
    protected EppGroupTypeALT getGroupType(List<EppGroupType> groupTypeList)
    {
        final List<String> codeList = CommonBaseUtil.getPropertiesList(groupTypeList, EppGroupType.code().s());

        if (codeList.contains(EppGroupTypeALTCodes.TYPE_LECTURES))
            return getCatalogItem(EppGroupTypeALT.class, EppGroupTypeALTCodes.TYPE_LECTURES);

        if (codeList.contains(EppGroupTypeALTCodes.TYPE_PRACTICE))
            return getCatalogItem(EppGroupTypeALT.class, EppGroupTypeALTCodes.TYPE_PRACTICE);

        if (codeList.contains(EppGroupTypeALTCodes.TYPE_LABS))
            return getCatalogItem(EppGroupTypeALT.class, EppGroupTypeALTCodes.TYPE_LABS);

        return null;
    }

    /**
     * Из списка модулей выбирет подходящий модуль для модуля-источника:
     * если совпадает уч. модуль, то берем его;
     * если совпадает порядковый номер, то берем его;
     * берем последний модуль;
     * @param sourceModule модуль-источник
     * @param moduleList список модулей из которых выбирается
     * @param lastModule возвращать ли последний в списке модуль, если по первым двум признакам не нашлось модуля
     * @return модуль реализации, который подходит модулю-источнику
     */
    protected TrJournalModule getTargetModule(TrJournalModule sourceModule, List<TrJournalModule> moduleList, boolean lastModule)
    {
        Collections.sort(moduleList, new EntityComparator<>(new EntityOrder(TrJournalModule.module().number().s())));

        for (TrJournalModule module : moduleList)
        {
            if (module.getModule().equals(sourceModule.getModule()))
            {
                return module;
            }
        }

        for (TrJournalModule module : moduleList)
        {
            if (module.getModule().getNumber() == sourceModule.getModule().getNumber())
            {
                return module;
            }
        }

        return lastModule ? moduleList.get(moduleList.size() - 1) : null;
    }

    @Override
    public List<PpsEntry> getTrJournalEppPps(Long trJournalId)
    {
        return new DQLSelectBuilder()
        .fromEntity(EppPpsCollectionItem.class, "rel")
        .column(property(EppPpsCollectionItem.pps().fromAlias("rel")))
        .joinEntity("rel", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "grp", eq(property(EppPpsCollectionItem.list().fromAlias("rel")), property("grp")))
        .joinEntity("grp", DQLJoinType.inner, TrJournalGroup.class, "jRel", eq(property(TrJournalGroup.group().fromAlias("jRel")), property("grp")))
        .where(eq(property(TrJournalGroup.journal().fromAlias("jRel")), value(trJournalId)))
        .order(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("rel")))
        .createStatement(getSession()).list();
    }

    @Override
    public boolean canEditBellsEntries(Long sppBellScheduleId)
    {
        return !existsEntity(TrEduGroupEvent.class, TrEduGroupEvent.bellScheduleEntry().schedule().id().s(), sppBellScheduleId);
    }

    @Override
    public List<EppGradeScale> getGradeScales(TrJournal journal)
    {
        return getList(EppRegistryElementPartFControlAction.class, EppRegistryElementPartFControlAction.part().s(), journal.getRegistryElementPart())
                .stream()
                .map(EppRegistryElementPartFControlAction::getGradeScale)
                .distinct()
                .sorted((o1, o2) -> Integer.compare(o2.getPriority(), o1.getPriority()))
                .collect(Collectors.toList());
    }
}
