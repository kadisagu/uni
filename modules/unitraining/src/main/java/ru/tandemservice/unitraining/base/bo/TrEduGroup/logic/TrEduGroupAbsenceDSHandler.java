/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrEduGroup.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

/**
 * @author oleyba
 * @since 9/23/11
 */
public class TrEduGroupAbsenceDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String COLUMN_SELECT = "select";

    public static final String PARAM_GROUP = "group";
    public static final String PARAM_STUDENT = "student";
    public static final String PARAM_NOT_FILLED = "filled";
    public static final String PARAM_VALID = "valid";

    public TrEduGroupAbsenceDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        TrJournalGroup group = context.get(PARAM_GROUP);
        Student student = context.get(PARAM_STUDENT);
        Boolean notFilled = context.get(PARAM_NOT_FILLED);
        Boolean valid = context.get(PARAM_VALID);

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(TrEduGroupEventStudent.class, "e").column("e")
            .where(eq(property(TrEduGroupEventStudent.event().group().fromAlias("e")), value(group.getGroup())))
            .where(eq(property(TrEduGroupEventStudent.studentWpe().student().fromAlias("e")), value(student)))
            .where(eq(property(TrEduGroupEventStudent.absent().fromAlias("e")), value(Boolean.TRUE)))
            .where(isNull(TrEduGroupEventStudent.transferDate().fromAlias("e")))
            .order(property(TrEduGroupEventStudent.event().journalEvent().number().fromAlias("e")));

        if (Boolean.TRUE.equals(notFilled))
            dql.where(isNull(property(TrEduGroupEventStudent.absenceReason().fromAlias("e"))));
        else if (valid != null)
            dql.where(eq(property(TrEduGroupEventStudent.absenceReason().valid().fromAlias("e")), value(valid)));

        return DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
    }
}
