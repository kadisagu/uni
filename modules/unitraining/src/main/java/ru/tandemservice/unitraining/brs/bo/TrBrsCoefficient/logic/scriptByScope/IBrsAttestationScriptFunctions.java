/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope;

import ru.tandemservice.uni.util.groovy.IScriptInstance;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;

/**
 * @author oleyba
 * @since 10/17/12
 */
public interface IBrsAttestationScriptFunctions extends IScriptInstance
{
    /**
     * Вычисление результатов прохождения межсессионной аттестации
     * @return если возвращается объект типа {@link IBrsDao.IStudentAttestationMarkData}, то результат метода {@link IBrsDao.IStudentAttestationData#passed()} игнорируется и
     * используется метод {@link IBrsDao.IStudentAttestationMarkData#mark()} для сохранения оценки в ведомость. Иначе {@link IBrsDao.IStudentAttestationData#passed()} переводится в
     * оценку атт./не атт. И если шкала атт./не атт. в этот момент не активна, то бросается исключение.
     */
    IBrsDao.IStudentAttestationData calculateAttestation(SessionAttestationSlot slot, IBrsDao.IStudentCurrentRatingData ratingCalc, IBrsDao.IBrsPreparedSettings preparedBrsSettings);
}
