package ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit;

import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;

import java.util.List;

/**
 * User: amakarova
 */
public interface ITrJournalGroupSortEvents {

    void sortEvents(List<TrEduGroupEvent> events);
}
