/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.logic;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/20/11
 */
public class TrJournalPrintDao extends UniBaseDao implements ITrJournalPrintDao
{
    @Override
    public byte[] printSessionScaleSheet(Long journalGroupId)
    {
        final UnisessionCommonTemplate template = this.get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.BRS_SESSION_SCALE_BULLETIN);
        RtfDocument rtf = new RtfReader().read(template.getContent());

        TrJournalGroup journalGroup = get(TrJournalGroup.class, journalGroupId);
        Collection<EppStudentWorkPlanElement> studentList = TrJournalManager.instance().dao().getStudentList(journalGroup);

        Collection<IBrsDao.ISessionMarkCalc> calcs = TrBrsCoefficientManager.instance().brsDao().getCalculatedSessionMark(journalGroup);

        if (calcs.isEmpty()) {
            throw new IllegalStateException();
        }

        RtfDocument result = null;

        for (final IBrsDao.ISessionMarkCalc calc : calcs)
        {
            final RtfDocument document = rtf.getClone();
            fillHeader(document, journalGroup, studentList, calc);
            fillStudentTable(document, studentList, calc);

            if (null == result) {
                result = document;
            } else
            {
                RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                result.getElementList().addAll(document.getElementList());
            }
        }
        if (null == result) {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }

        return RtfUtil.toByteArray(result);
    }

    private void fillStudentTable(RtfDocument rtf, Collection<EppStudentWorkPlanElement> studentList, IBrsDao.ISessionMarkCalc calculatedRating)
    {
        EppGradeScale gradeScale = calculatedRating.getGradeScale();

        final List<SessionMarkGradeValueCatalogItem> scaleMarks = new DQLSelectBuilder()
                    .fromEntity(SessionMarkGradeValueCatalogItem.class, "c").column("c")
                    .where(eq(property("c", SessionMarkGradeValueCatalogItem.scale()), value(gradeScale)))
                    .where(eq(property("c", SessionMarkGradeValueCatalogItem.positive()), value(Boolean.TRUE)))
                    .order(property("c", SessionMarkGradeValueCatalogItem.priority()), OrderDirection.desc)
            .createStatement(getSession()).list();

        List<String[]> table = new ArrayList<>();
        int rowNumber = 1;
        for (EppStudentWorkPlanElement student : studentList) {
            IBrsDao.IStudentSessionMarkData studentSessionMark = calculatedRating.getSessionMark(student);

            List<String> tableRow = new ArrayList<>();
            tableRow.add(String.valueOf(rowNumber++));
            tableRow.add(student.getStudent().getFio());

            if (studentSessionMark != null) {
                tableRow.add(studentSessionMark.getCurrentRatingValue() == null || studentSessionMark.getCurrentRatingValue().getValue() == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(studentSessionMark.getCurrentRatingValue().getValue()));
                tableRow.add(YesNoFormatter.INSTANCE.format(Boolean.TRUE.equals(studentSessionMark.isAllowed())));
                for (SessionMarkGradeValueCatalogItem mark : scaleMarks) {
                    Double ratingMissingForMark = studentSessionMark.getRatingMissingForMark(mark);
                    if (ratingMissingForMark == null) {
                        tableRow.add("скрипт расчета не возвратил значение");
                    }
                    else {
                        tableRow.add(ratingMissingForMark < 0 ? "-" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(ratingMissingForMark));
                    }
                }
                tableRow.add(studentSessionMark.getRatingValue().getMessage());
            }

            table.add(tableRow.toArray(new String[tableRow.size()]));
        }

        new RtfTableModifier()
            .put("T", table.toArray(new String[table.size()][]))
            .put("T", new RtfRowIntercepterBase()
            {
                @Override
                public void beforeModify(RtfTable table, int currentRowIndex)
                {
                    int[] parts = new int[scaleMarks.size()];
                    Arrays.fill(parts, 1);
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 4, null, parts);
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 4, null, parts);
                    for (int i = 0; i < scaleMarks.size(); i++) {
                        table.getRowList().get(currentRowIndex - 1).getCellList().get(4 + i).getElementList().addAll(new RtfString().append(scaleMarks.get(i).getShortTitle()).toList());
                    }
                }
            })
            .modify(rtf);
    }

    private void fillHeader(RtfDocument rtf, TrJournalGroup journalGroup, Collection<EppStudentWorkPlanElement> studentList, IBrsDao.ISessionMarkCalc calc)
    {
        TrJournal journal = journalGroup.getJournal();
        EppRegistryElementPart discipline = journal.getRegistryElementPart();
        List<EppPpsCollectionItem> tutors = getList(EppPpsCollectionItem.class, EppPpsCollectionItem.list().s(), journalGroup.getGroup());
        final TopOrgUnit academy = TopOrgUnit.getInstance();
        EmployeePost cathedraHead = EmployeeManager.instance().dao().getHead(discipline.getTutorOu());

        final RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("academyTitle", academy.getPrintTitle());
        modifier.put("ouTitle", discipline.getTutorOu().getPrintTitle());
        modifier.put("discipline", "№" + journal.getNumber() + " " + journal.getRegistryElementPart().getTitle());
        modifier.put("eduGroup", journalGroup.getGroup().getTitle());
        modifier.put("educationOrgUnitTitle", getEduOUs(studentList));
        modifier.put("eduYear", journal.getYearPart().getYear().getEducationYear().getTitle());
        modifier.put("term", journal.getYearPart().getPart().getShortTitle());
        modifier.put("group", getGroups(studentList));
        modifier.put("course", getCourses(studentList));
        modifier.put("ratingInfo", calc.getPrintableRatingFormula());
        modifier.put("tutors", UniStringUtils.joinUnique(tutors, EppPpsCollectionItem.pps().person().fio().s(), ", "));
        modifier.put("cathedra", discipline.getTutorOu().getPrintTitle());
        modifier.put("cathedraHead", cathedraHead == null ? "" : cathedraHead.getPerson().getFio());
        modifier.modify(rtf);
    }

    private String getEduOUs(Collection<EppStudentWorkPlanElement> studentList)
    {
        return CommonBaseStringUtil.joinUniqueSorted(studentList, EppStudentWorkPlanElement.student().educationOrgUnit().educationLevelHighSchool().title().s(), ", ");
    }

    private String getCourses(Collection<EppStudentWorkPlanElement> studentList)
    {
        return CommonBaseStringUtil.joinUniqueSorted(studentList, EppStudentWorkPlanElement.course().title().s(), ", ");
    }

    private String getGroups(Collection<EppStudentWorkPlanElement> studentList)
    {
        return CommonBaseStringUtil.joinUniqueSorted(studentList, EppStudentWorkPlanElement.student().group().title().s(), ", ");
    }
}
