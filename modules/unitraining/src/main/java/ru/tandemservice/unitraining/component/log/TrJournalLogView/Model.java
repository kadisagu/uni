/* $Id$ */
package ru.tandemservice.unitraining.component.log.TrJournalLogView;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

/**
 * @author oleyba
 * @since 9/19/11
 */
@State({ @Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding="entityId") })
public class Model extends ru.tandemservice.uni.component.log.EntityLogViewBase.Model
{
    @Override
    public String getListSettingsKey()
    {
        return "logView.trJournal.list.";
    }
}
