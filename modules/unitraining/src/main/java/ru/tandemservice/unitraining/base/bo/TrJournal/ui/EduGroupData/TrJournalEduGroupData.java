/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.EduGroupData;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

/**
 * @author oleyba
 * @since 8/12/11
 */
@Configuration
public class TrJournalEduGroupData extends BusinessComponentManager
{
    public static final String GROUP_LIST_COL_GROUP = "group";
    public static final String GROUP_LIST_COL_AGREEMENT = "agreement";
    public static final String GROUP_LIST_COL_GROUP_TYPE = "groupType";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(TrJournalManager.TR_GROUP_WITH_PPS_DS, groupListDSColumnExtPoint(),
                                            TrJournalManager.instance().trJournalGroupWithPpsDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint groupListDSColumnExtPoint()
    {
        final IPublisherLinkResolver resolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(final IEntity entity)
            {
                return new ParametersMap()
                        .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getProperty(TrJournalGroup.group().id()))
                        .add("orgUnitId", entity.getProperty(TrJournalGroup.journal().registryElementPart().registryElement().owner().id()));
            }

            @Override
            public String getComponentName(final IEntity entity)
            {
                return "ru.tandemservice.uniepp.component.edugroup.pub.TutorOrgUnitGroupPub";
            }
        };

        return columnListExtPointBuilder(TrJournalManager.TR_GROUP_WITH_PPS_DS)
                .addColumn(publisherColumn(GROUP_LIST_COL_GROUP, TrJournalGroup.group().title().s()).publisherLinkResolver(resolver))
                .addColumn(textColumn(GROUP_LIST_COL_AGREEMENT, TrJournalGroup.group().level().displayableTitle().s()))
                .addColumn(textColumn(GROUP_LIST_COL_GROUP_TYPE, TrJournalGroup.group().type().title().s()))
                .addColumn(textColumn(TrJournalManager.TR_GROUP_DS_PPS_LIST_PROPERTY, TrJournalManager.TR_GROUP_DS_PPS_LIST_PROPERTY))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onDeleteGroup")
                                   .permissionKey("deleteEduGroupTrJournal")
                                   .alert(alert("Убрать группу «{0}» из реализации дисциплины?", TrJournalGroup.group().title().s()))
                                   .visible("ui:linkNotArchived")
                )
                .create();
    }
}
