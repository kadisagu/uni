/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrHomePage;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;

import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.BaseRegistryElementPartDSHandler;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.unitraining.base.bo.TrHomePage.logic.ITrHomePageDao;
import ru.tandemservice.unitraining.base.bo.TrHomePage.logic.TrHomePageDao;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

/**
 * @author oleyba
 * @since 8/30/11
 */
@Configuration
public class TrHomePageManager extends BusinessObjectManager
{
    public static final String TR_HOME_PAGE_JOURNAL_GROUP_YEAR_PART_DS = "trHomePageJournalGroupYearPartDS";
    public static final String TR_HOME_PAGE_JOURNAL_GROUP_ACTIVITY_ELEMENT_PART_DS = "trHomePageJournalGroupregistryElementPartDS";
    public static final String TR_HOME_PAGE_JOURNAL_GROUP_TYPE_DS = "trHomePageJournalGroupTypeDS";
    public static final String TR_HOME_PAGE_JOURNAL_GROUP_EDU_GROUP_DS = "trHomePageJournalGroupEduGroupDS";
    public static final String TR_HOME_PAGE_JOURNAL_DS = "trHomePageJournalDS";

    public static final String TR_HOME_PAGE_PERSON_KEY = "trHomePagePerson";
    public static final String TR_HOME_PAGE_GROUP_TYPE_KEY = "trHomePageLoadType";
    public static final String TR_HOME_PAGE_ACTIVITY_ELEMENT_PART_KEY = "trHomePageregistryElementPart";
    public static final String TR_HOME_PAGE_YEAR_PART_KEY = "trHomePageYearPart";

    public static final List<String> ALLOW_EDIT_EVENT_CONFIG_STATES = Arrays.asList(EppState.STATE_FORMATIVE, EppState.STATE_ACCEPTABLE, EppState.STATE_ACCEPTED);

    public static TrHomePageManager instance() {
        return instance(TrHomePageManager.class);
    }

    @Bean
    public ITrHomePageDao dao() {
        return new TrHomePageDao();
    }

    @Bean
    public UIDataSourceConfig trHomePageJournalGroupTypeDSConfig()
    {
        return SelectDSConfig.with(TrHomePageManager.TR_HOME_PAGE_JOURNAL_GROUP_TYPE_DS, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.trHomePageJournalGroupTypeDSHandler())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trHomePageJournalGroupTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppGroupTypeALT.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                DQLSelectBuilder groupTypes = new DQLSelectBuilder()
                    .fromEntity(TrJournal.class, "j")
                    .where(eq(property(TrJournal.yearPart().fromAlias("j")), commonValue(context.<Object>get(TR_HOME_PAGE_YEAR_PART_KEY))))
                    .where(eq(property(TrJournal.registryElementPart().fromAlias("j")), commonValue(context.<Object>get(TR_HOME_PAGE_ACTIVITY_ELEMENT_PART_KEY))))
                    .where(eq(property(TrJournal.state().code().fromAlias("j")), value(EppState.STATE_ACCEPTED)))
                    .joinEntity("j", DQLJoinType.inner, TrJournalGroup.class, "jg", eq(property(TrJournalGroup.journal().fromAlias("jg")), property("j")))
                    .joinPath(DQLJoinType.inner, TrJournalGroup.group().fromAlias("jg"), "g")
                    .joinEntity("g", DQLJoinType.inner, EppPpsCollectionItem.class, "pps", eq(property(EppPpsCollectionItem.list().fromAlias("pps")), property("g")))
                    .where(eq(property(EppPpsCollectionItem.pps().person().fromAlias("pps")), commonValue(context.<Object>get(TR_HOME_PAGE_PERSON_KEY))))
                    .column(property(EppRealEduGroup4LoadType.type().id().fromAlias("g")))
                    .predicate(DQLPredicateType.distinct);

                dql.where(in(property(EppGroupTypeALT.id().fromAlias(alias)), groupTypes.buildQuery()));
            }
        }
        .order(EppGroupTypeALT.priority())
        .filter(EppGroupTypeALT.title())
        ;
    }

    @Bean
    public UIDataSourceConfig trHomePageJournalGroupEduGroupDSConfig()
    {
        return SelectDSConfig.with(TrHomePageManager.TR_HOME_PAGE_JOURNAL_GROUP_EDU_GROUP_DS, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.trHomePageJournalGroupEduGroupDSHandler())
        .addColumn(TrJournalGroup.group().title().s())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trHomePageJournalGroupEduGroupDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), TrJournalGroup.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                final DQLSelectBuilder sub = new DQLSelectBuilder()
                    .fromEntity(EppPpsCollectionItem.class, "pps")
                    .where(eq(property(EppPpsCollectionItem.list().fromAlias("pps")), property(TrJournalGroup.group().fromAlias(alias))))
                    .where(eq(property(EppPpsCollectionItem.pps().person().fromAlias("pps")), commonValue(context.<Object>get(TR_HOME_PAGE_PERSON_KEY))));
                dql.where(DQLExpressions.exists(sub.buildQuery()));
            }
        }
        .where(TrJournalGroup.journal().yearPart(), TrHomePageManager.TR_HOME_PAGE_YEAR_PART_KEY)
        .where(TrJournalGroup.journal().registryElementPart(), TrHomePageManager.TR_HOME_PAGE_ACTIVITY_ELEMENT_PART_KEY)
        .where(TrJournalGroup.group().type(), TrHomePageManager.TR_HOME_PAGE_GROUP_TYPE_KEY)
        .order(TrJournalGroup.group().title())
        .filter(TrJournalGroup.group().title())
        ;
    }

    @Bean
    public UIDataSourceConfig trHomePageJournalGroupregistryElementPartDSConfig()
    {
        return SelectDSConfig.with(TrHomePageManager.TR_HOME_PAGE_JOURNAL_GROUP_ACTIVITY_ELEMENT_PART_DS, this.getName())
            .addColumn(EppRegistryElementPart.titleWithNumber().s())
            .dataSourceClass(SelectDataSource.class)
            .handler(this.trHomePageJournalGroupregistryElementPartDSHandler())
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trHomePageJournalGroupregistryElementPartDSHandler()
    {
        return new BaseRegistryElementPartDSHandler(this.getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder disciplines = new DQLSelectBuilder()
                    .fromEntity(TrJournal.class, "j")
                    .where(eq(property(TrJournal.state().code().fromAlias("j")), value(EppState.STATE_ACCEPTED)))
                    .where(eq(property(TrJournal.yearPart().fromAlias("j")), commonValue(context.<Object>get(TR_HOME_PAGE_YEAR_PART_KEY))))
                    .joinEntity("j", DQLJoinType.inner, TrJournalGroup.class, "jg", eq(property(TrJournalGroup.journal().fromAlias("jg")), property("j")))
                    .joinPath(DQLJoinType.inner, TrJournalGroup.group().fromAlias("jg"), "g")
                    .joinEntity("g", DQLJoinType.inner, EppPpsCollectionItem.class, "pps", eq(property(EppPpsCollectionItem.list().fromAlias("pps")), property("g")))
                    .where(eq(property(EppPpsCollectionItem.pps().person().fromAlias("pps")), commonValue(context.<Object>get(TR_HOME_PAGE_PERSON_KEY))))
                    .column(property(TrJournal.registryElementPart().id().fromAlias("j")))
                    .predicate(DQLPredicateType.distinct);

                dql.where(in(EppRegistryElementPart.id().fromAlias(alias), disciplines.buildQuery()));
            }
        }
        ;
    }

    @Bean
    public UIDataSourceConfig trHomePageJournalDSConfig()
    {
        return SelectDSConfig.with(TrHomePageManager.TR_HOME_PAGE_JOURNAL_DS, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.trHomePageJournalDSHandler())
        .addColumn(TrJournal.P_CALCULATED_TITLE)
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trHomePageJournalDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), TrJournal.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(in(property(TrJournal.state().code().fromAlias(alias)), Arrays.asList(EppState.STATE_FORMATIVE, EppState.STATE_ACCEPTABLE, EppState.STATE_ACCEPTED)));
            }
        }
        .where(TrJournal.responsible().person(), TR_HOME_PAGE_PERSON_KEY)
        .order(TrJournal.registryElementPart().registryElement().title())
        .order(TrJournal.title())
        .filter(TrJournal.title())
        .filter(TrJournal.registryElementPart().registryElement().title())
        ;
    }

    @Bean
    public UIDataSourceConfig trHomePageJournalGroupYearPartDSConfig() {
        return SelectDSConfig.with(TrHomePageManager.TR_HOME_PAGE_JOURNAL_GROUP_YEAR_PART_DS, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .handler(this.trHomePageJournalGroupYearPartDSHandler())
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trHomePageJournalGroupYearPartDSHandler() {
        return new EntityComboDataSourceHandler(this.getName(), EppYearPart.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder parts = new DQLSelectBuilder()
                    .fromEntity(TrJournal.class, "j")
                    .where(eq(property(TrJournal.state().code().fromAlias("j")), value(EppState.STATE_ACCEPTED)))
                    .joinEntity("j", DQLJoinType.inner, TrJournalGroup.class, "jg", eq(property(TrJournalGroup.journal().fromAlias("jg")), property("j")))
                    .joinPath(DQLJoinType.inner, TrJournalGroup.group().fromAlias("jg"), "g")
                    .joinEntity("g", DQLJoinType.inner, EppPpsCollectionItem.class, "pps", eq(property(EppPpsCollectionItem.list().fromAlias("pps")), property("g")))
                    .where(eq(property(EppPpsCollectionItem.pps().person().fromAlias("pps")), commonValue(context.<Object>get(TR_HOME_PAGE_PERSON_KEY))))
                    .column(property(TrJournal.yearPart().id().fromAlias("j")))
                    .predicate(DQLPredicateType.distinct);

                dql.where(in(EppYearPart.id().fromAlias(alias), parts.buildQuery()));
            }
        }
        .order(EppYearPart.year().educationYear().intValue(), OrderDirection.desc)
        .order(EppYearPart.part().yearDistribution().code())
        .order(EppYearPart.part().number(), OrderDirection.desc)
        .filter(EppYearPart.year().educationYear().title())
        .filter(EppYearPart.part().title())
        ;
    }
}
