/* $Id$ */
package ru.tandemservice.unitraining.component.log.TrJournalLogView;

import java.util.Arrays;
import java.util.List;

import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;

/**
 * @author oleyba
 * @since 9/19/11
 */
public class DAO extends ru.tandemservice.uni.component.log.EntityLogViewBase.DAO
{
    private static final List<String> _entityClassNames = Arrays.asList(
            TrJournalGroup.ENTITY_CLASS,
            TrJournalModule.ENTITY_CLASS,
            TrJournalEvent.ENTITY_CLASS,
            TrEduGroupEvent.ENTITY_CLASS,
            TrEduGroupEventStudent.ENTITY_CLASS
    );

    @Override
    protected List<String> getEntityClassNames()
    {
        return _entityClassNames;
    }
}
