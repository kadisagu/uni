/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.DisplayMode;
import org.tandemframework.core.view.PrintMode;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsIRatingValueFormatter;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.AddEvent.TrJournalAddEvent;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.AddEvent.TrJournalAddEventUI;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.EventThemeEdit.TrJournalEventThemeEdit;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkInfo.TrJournalMarkInfo;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkCell;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkColumn;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkColumnGroup;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkRow;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.ScheduleEventEdit.TrJournalScheduleEventEdit;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/1/11
 */
@State({
    @Bind(key = TrJournalGroupMarkEditUI.GROUP_BINDING, binding = "group.id", required = true)
})
@Input({
    @Bind(key = TrJournalGroupMarkEditUI.EVENT_BINDING, binding = "journalEventForEditId"),
    @Bind(key = TrJournalGroupMarkEditUI.VIEW_MODE_BINDING, binding = "viewModeOnly"),
    @Bind(key = TrJournalGroupMarkEditUI.ADDITIONAL_BUTTONS_BINDING, binding = "additionalButtons")
})
public class TrJournalGroupMarkEditUI extends UIPresenter
{
    public static final String GROUP_BINDING = "groupId";
    public static final String EVENT_BINDING = "eventId";
    public static final String VIEW_MODE_BINDING = "viewMode";
    public static final String ADDITIONAL_BUTTONS_BINDING = "additionalButtons";
    
    public static final List<String> BOTTOM_BUTTONS_POSITION_HOLDER = ImmutableList.of("bottomRow");
    public static final List<String> TOP_BUTTONS_POSITION_HOLDER = ImmutableList.of("topRow");

    private TrJournalGroup group = new TrJournalGroup();

    private List<IJournalGroupMarkEditAdditionalButton> additionalButtons = new ArrayList<>();
    private IJournalGroupMarkEditAdditionalButton currentAdditButton;
    
    private List<TrJournalMarkColumnGroup> columnGroupList = new ArrayList<>();
    private List<TrJournalMarkRow> rowList = new ArrayList<>();
    private List<ISessionBrsDao.IRatingAdditParamDef> additParamList = new ArrayList<>();

    private TrJournalMarkColumnGroup currentColumnGroup;
    private ColumnWrapper currentColumn;
    private TrJournalMarkRow currentRow;
    private String columnButtonPosition;
    private ISessionBrsDao.IRatingAdditParamDef currentAdditParam;
    private ISessionBrsDao.IRatingAdditParamDef currentStatParam;

    private Map<MultiKey, TrJournalMarkCell> cellMap = new HashMap<>();

    private TrEduGroupEvent eventEdited = null;
    private Long journalEventForEditId;

    private boolean viewModeOnly;

    private String tutor;

    private IBrsDao.ICurrentRatingCalc calculatedRating;
    private IBrsDao.IJournalEventStatCalc calculatedStat;

    private TrOrgUnitSettings brsSettings;
    
    // actions

    @Override
    public void onComponentRefresh()
    {
        setGroup(IUniBaseDao.instance.get().get(TrJournalGroup.class, getGroup().getId()));

        getColumnGroupList().clear();
        DQLSelectBuilder eventDQL = new DQLSelectBuilder()
            .fromEntity(TrEduGroupEvent.class, "rel").column("rel").order(property(TrEduGroupEvent.journalEvent().journalModule().module().number().fromAlias("rel"))).order(property(TrEduGroupEvent.journalEvent().number().fromAlias("rel")))
            .where(eq(property(TrEduGroupEvent.group().fromAlias("rel")), value(getGroup().getGroup())))
            .where(eq(property(TrEduGroupEvent.journalEvent().journalModule().journal().fromAlias("rel")), value(getGroup().getJournal())))
            ;
        final List<TrEduGroupEvent> events = IUniBaseDao.instance.get().getList(eventDQL);

        // для расширений
        for (IUIAddon addon : getConfig().getAddons().values())
            if (addon instanceof ITrJournalGroupSortEvents)
                ((ITrJournalGroupSortEvents) addon).sortEvents(events);

        for (TrJournalModule module : UniBaseUtils.<TrJournalModule>getPropertiesSet(events, TrEduGroupEvent.journalEvent().journalModule().s()))
        {
            TrJournalMarkColumnGroup wrapper = new TrJournalMarkColumnGroup(module);
            for (TrEduGroupEvent event : events)
            {
                if (event.getJournalEvent().getJournalModule().equals(module))
                {
                    final ColumnWrapper columnWrapper = new ColumnWrapper(event);
                    wrapper.getColumnList().add(columnWrapper);
                    if (event.getJournalEvent().getId().equals(getJournalEventForEditId()) && columnWrapper.isCanEdit())
                        setEventEdited(event);
                }
            }
            getColumnGroupList().add(wrapper);
        }

        Set<Long> studentEpvSlotIds = new HashSet<>(
        IUniBaseDao.instance.get().<Long>getList(
            new DQLSelectBuilder()
            .fromEntity(EppRealEduGroupRow.class, "r")
            .column(property(EppRealEduGroupRow.studentWpePart().studentWpe().id().fromAlias("r")))
            .where(eq(property(EppRealEduGroupRow.group().fromAlias("r")), value(this.getGroup().getGroup())))
        )
        );

        getRowList().clear();
        for (EppStudentWorkPlanElement student : TrJournalManager.instance().dao().getStudentList(getGroup()))
            getRowList().add(new TrJournalMarkRow(student, studentEpvSlotIds.contains(student.getId())));

        fillCellMap();

        fillTutor();

        setCalculatedRating(TrBrsCoefficientManager.instance().brsDao().getCalculatedRating(getGroup().getJournal()));
        setCalculatedStat(TrBrsCoefficientManager.instance().brsDao().getCalculatedStat(getGroup()));

        fillAbsenceNoteByDefault();

        setAdditParamList(new ArrayList<>());
        Map<String, ISessionBrsDao.IRatingAdditParamDef> additParamDefMap = TrBrsCoefficientManager.instance().brsDao().getRatingAdditionalParamDefinitions(getGroup().getJournal());
        for (String key : TrBrsCoefficientManager.instance().brsDao().getDisplayableAdditParams(getGroup().getJournal())) {
            ISessionBrsDao.IRatingAdditParamDef def = additParamDefMap.get(key);
            if (null != def)
                getAdditParamList().add(def);
        }

        brsSettings = TrBrsCoefficientManager.instance().brsDao().getOrgUnitBrsSettings(getGroup().getJournal().getYearPart(), getGroup().getJournal().getOrgUnit(), null);
    }

    public void onClickEditColumn()
    {
        setEventEdited(IUniBaseDao.instance.get().get(TrEduGroupEvent.class, getListenerParameterAsLong()));
        fillAbsenceNoteByDefault();
    }

    public void onClickSaveColumn()
    {
        if (eventEdited != null) {
            DataAccessServices.dao().doInTransaction(session -> {
                for (TrJournalMarkRow row : getRowList()) {
                    TrJournalMarkCell cell = getCellMap().get(createCellKey(eventEdited, row.getStudent()));
                    TrJournalManager.instance().eventDao().saveJournalMark(row.getStudent(), eventEdited, cell);
                }
                return Boolean.TRUE;
            });
        }

        // сбрасываем колонку и перегружаем
        onClickCancelColumn();
    }

    public void onClickCancelColumn()
    {
        eventEdited = null;
        journalEventForEditId = null;
        _uiSupport.setRefreshScheduled(true);
    }


    public void onClickEditDate()
    {
        final TrEduGroupEvent trGroupEvent = IUniBaseDao.instance.get().get(TrEduGroupEvent.class, getListenerParameterAsLong());
        _uiActivation.asRegionDialog(TrJournalScheduleEventEdit.class)
        .parameter(IUIPresenter.PUBLISHER_ID, trGroupEvent.getId())
        .activate();
    }

    public void onClickEditTheme()
    {
        final TrEduGroupEvent trGroupEvent = IUniBaseDao.instance.get().get(TrEduGroupEvent.class, getListenerParameterAsLong());

        _uiActivation.asRegionDialog(TrJournalEventThemeEdit.class)
            .parameter(IUIPresenter.PUBLISHER_ID, trGroupEvent.getJournalEvent().getId())
            .activate();
    }

    public void onClickShowInfo()
    {
        _uiActivation.asRegion(TrJournalMarkInfo.class, "dialog")
        .parameter(IUIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
        .activate();
    }

    public void onClickPrint()
    {
        this._uiActivation.asDesktopRoot(TrJournalGroupMarkEdit.class).displayMode(DisplayMode.edit, PrintMode.page)
        .parameter(TrJournalGroupMarkEditUI.GROUP_BINDING, getGroup().getId())
        .parameter(TrJournalGroupMarkEditUI.VIEW_MODE_BINDING, Boolean.TRUE)
        .activate();
    }

    public void onClickAddAdditionalEvent()
    {
        _uiActivation.asRegionDialog(TrJournalAddEvent.class)
        .parameter(IUIPresenter.PUBLISHER_ID, getGroup().getJournal().getId())
        .parameter(TrJournalAddEventUI.BIND_EDU_GROUP, getGroup().getId())
        .parameter(TrJournalAddEventUI.BIND_IS_ADDITIONAL, Boolean.TRUE)
        .activate();

    }

    public void onClickPrintSessionScaleSheet()
    {
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(TrJournalManager.instance().printDao().printSessionScaleSheet(getGroup().getId()), "Ведомость.rtf");
        getActivationBuilder()
            .asDesktopRoot(IUniComponents.PRINT_REPORT)
            .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "rtf"))
            .activate();
    }
    
    public void onClickDeleteColumn()
    {
        TrJournalManager.instance().eventDao().deleteAdditionalEvent(getListenerParameterAsLong());
        _uiSupport.setRefreshScheduled(true);
    }
    
    public void onClickAdditionalButton() {
        String buttonId = getListenerParameter();
        if (getAdditionalButtons() != null && buttonId != null) {
            for (IJournalGroupMarkEditAdditionalButton button : getAdditionalButtons()) {
                if (buttonId.equals(button.getId())) {
                    button.onClickAction(getGroup());
                }
            }
        }
    }

    // presenter


    public TrOrgUnitSettings getBrsSettings()
    {
        return brsSettings;
    }

    public boolean isPrintSessionScaleSheetVisible()
    {
        return getGroup() != null && brsSettings != null && brsSettings.isUseCurrentRatingInSession();
    }

    public boolean isCanEditDate()
    {
        return !isEditMode() && !isViewModeOnly();
    }

    public TrJournalMarkCell getCurrentCell()
    {
        return getCellMap().get(createCellKey(getCurrentColumn().getEvent(), getCurrentRow().getStudent()));
    }

    public boolean isShowModuleRating()
    {
        return getCalculatedRating().isModuleRatingCalculated();
    }

    public String getCurrentRowClassName()
    {
        TrJournalMarkRow row = getCurrentRow();
        String indexStyle = (getRowList().indexOf(row) % 2 == 0 ? "list-row-odd" : "list-row-even");
        return "list-row " + indexStyle;
    }

    public String getCurrentStatRowClassName()
    {
        String indexStyle = (getCalculatedStat().getStatParamDefinitions().indexOf(getCurrentStatParam()) % 2 == 0 ? "list-row-odd" : "list-row-even");
        return "list-row " + indexStyle;
    }

    public String getCurrentRowNumber() {
        return String.valueOf(getRowList().indexOf(getCurrentRow()) + 1);
    }

    public String getCurrentRowStyle()
    {
        TrJournalMarkRow row = getCurrentRow();
        return row.isActive() ? "" : "color: gray";
    }

    public boolean isEditMode()
    {
        return getEventEdited() != null && !isViewModeOnly();
    }

    public boolean isCurrentColumnInEditMode()
    {
        return getCurrentColumn().getEvent().equals(getEventEdited()) && !isViewModeOnly();
    }

    public int getTableRowCount()
    {
        return getHeaderRowCount() + getRowList().size() + getCalculatedStat().getStatParamDefinitions().size() + (isViewModeOnly() ? 0 : 1);
    }

    public int getHeaderRowCount()
    {
        return 4 + (isViewModeOnly() ? 0 : 2);
    }

    public ISessionBrsDao.IRatingValue getRowModuleRating()
    {
        if (!isShowModuleRating())
            return null;
        IBrsDao.IStudentCurrentRatingData studentRating = getCalculatedRating().getCurrentRating(getCurrentRow().getStudent());
        return studentRating == null ? null : studentRating.getModuleRating(getCurrentColumnGroup().getModule().getModule().getModule());
    }

    public ISessionBrsDao.IRatingValue getRowRating()
    {
        IBrsDao.IStudentCurrentRatingData studentRating = getCalculatedRating().getCurrentRating(getCurrentRow().getStudent());
        return studentRating == null ? null : studentRating.getRatingValue();
    }

    public String getRatingValue(ISessionBrsDao.IRatingValue rating)
    {
        if (rating == null)
            return "-";
        if (rating.getValue() == null && rating.getMessage() != null)
            return "!";
        if (rating.getValue() == null)
            return "-";
        return DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(rating.getValue());
    }

    public String getRatingErrorText(ISessionBrsDao.IRatingValue rating)
    {
        return null == rating ? null : rating.getMessage();
    }

    public Boolean isRatingHasError(ISessionBrsDao.IRatingValue rating)
    {
        return null != rating && (rating.getValue() == null || rating.getMessage() != null);
    }

    public List<TrJournalMarkColumn> getColumnList()
    {
        return getCurrentColumnGroup().getColumnList();
    }
    
    public String getEditButtonId() {
        return "editColumn" + getCurrentColumn().getEvent().getId() + "." + getColumnButtonPosition();
    }

    public String getDeleteButtonId() {
        return "deleteColumn" + getCurrentColumn().getEvent().getId() + "." + getColumnButtonPosition();
    }
    
    public String getDeleteColumnAlert() {
        return "Удалить графу «" +getCurrentColumn().getJEvent().getTitle() +"»? Удаление графы приведет к удалению всех выставленных в нее баллов и отметок о посещении.";
    }

    public String getAdditParamValue() {
        IBrsDao.IStudentCurrentRatingData studentRating = getCalculatedRating().getCurrentRating(getCurrentRow().getStudent());
        if (studentRating == null || getCurrentAdditParam() == null)
            return "";
        ISessionBrsDao.IRatingValue paramValue = studentRating.getRatingAdditParam(getCurrentAdditParam().getKey());
        return BrsIRatingValueFormatter.instance.format(paramValue);

    }

    public String getStatParamValue() {
        if (getCurrentColumn() == null || getCurrentStatParam() == null)
            return "";
        ISessionBrsDao.IRatingValue paramValue = getCalculatedStat().getStatValue(getCurrentColumn().getJEvent(), getCurrentStatParam().getKey());
        return BrsIRatingValueFormatter.instance.format(paramValue);
    }

    public String getAggregatedRatingValue()
    {
        if (getCurrentStatParam() == null)
            return "";
        IBrsDao.IAggregatedRatingValue paramValue = getCalculatedStat().getStatValueAggregated(getCurrentStatParam().getKey(), IBrsDao.RATING_COLUMN_KEY);
        return paramValue.isApplicable() ? BrsIRatingValueFormatter.instance.format(paramValue) : "";
    }

    public String getAggregatedAdditParamValue()
    {
        if (getCurrentStatParam() == null || getCurrentAdditParam() == null)
            return "";
        IBrsDao.IAggregatedRatingValue paramValue = getCalculatedStat().getStatValueAggregated(getCurrentStatParam().getKey(), getCurrentAdditParam().getKey());
        return paramValue.isApplicable() ? BrsIRatingValueFormatter.instance.format(paramValue) : "";
    }

    // util

    private MultiKey createCellKey(TrEduGroupEvent event, EppStudentWorkPlanElement studentSlot)
    {
        return new MultiKey(event.getId(), studentSlot.getId());
    }
    
    public List<String> getBottomButtonsPositionHolder() {
        return BOTTOM_BUTTONS_POSITION_HOLDER;
    }

    public List<String> getTopButtonsPositionHolder() {
        return TOP_BUTTONS_POSITION_HOLDER;
    }

    // data access


    public String getColumnButtonPosition()
    {
        return columnButtonPosition;
    }

    public void setColumnButtonPosition(String columnButtonPosition)
    {
        this.columnButtonPosition = columnButtonPosition;
    }

    public void fillCellMap()
    {
        Map<MultiKey, TrJournalMarkCell> cellMap = getCellMap();
        cellMap.clear();

        // создаются заглушки
        Map<Long, TrJournalMarkColumnGroup> groupMap = new HashMap<>();
        for (TrJournalMarkColumnGroup group : getColumnGroupList())
        {
            groupMap.put(group.getId(), group);
            for (TrJournalMarkColumn column : group.getColumnList())
                for (TrJournalMarkRow row : getRowList()) {
                    cellMap.put(createCellKey(column.getEvent(), row.getStudent()), new TrJournalMarkCell(column.getEvent().getJournalEvent()));
                }
        }

        // создаются ячейки по существующим событиям
        DQLSelectBuilder eventDQL = new DQLSelectBuilder()
            .fromEntity(TrEduGroupEventStudent.class, "event", true).column("event")
            .where(eq(property(TrEduGroupEventStudent.event().group().fromAlias("event")), value(getGroup().getGroup())))
            .where(eq(property(TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("event")), value(getGroup().getJournal())))
            ;
        final List<TrEduGroupEventStudent> events = IUniBaseDao.instance.get().<TrEduGroupEventStudent>getList(eventDQL);
        for (TrEduGroupEventStudent event : events) {
            cellMap.put(createCellKey(event.getEvent(), event.getStudentWpe()), new TrJournalMarkCell(event));
            groupMap.get(event.getEvent().getJournalEvent().getJournalModule().getId()).incTotal(event);
        }
    }

    private void fillTutor()
    {
        List<String> pps = new DQLSelectBuilder()
            .fromEntity(EppPpsCollectionItem.class, "rel")
            .column(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("rel")))
            .predicate(DQLPredicateType.distinct)
            .joinEntity("rel", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "grp", eq(property(EppPpsCollectionItem.list().fromAlias("rel")), property("grp")))
            .joinEntity("grp", DQLJoinType.inner, TrJournalGroup.class, "jRel", eq(property(TrJournalGroup.group().fromAlias("jRel")), property("grp")))
            .where(eq(property("jRel"), value(getGroup())))
            .order(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("rel")))
            .createStatement(_uiSupport.getSession()).list();
        setTutor(UniStringUtils.joinNotEmpty(pps, ", "));
    }

    private void fillAbsenceNoteByDefault()
    {
        if (null == getEventEdited())
            return;

        final List<TrJournalMarkCell> cells = new ArrayList<>(getRowList().size());
        for (TrJournalMarkRow row: getRowList()) {
            TrJournalMarkCell cell = getCellMap().get(createCellKey(getEventEdited(), row.getStudent()));
            if (null != cell.isAbsent()) { return; } // если хотябы у одного есть признак неявки
            if (null != cell.getGrade()) { return; } // если хотябы у одного есть балл
            cells.add(cell);
        }

        for (TrJournalMarkCell cell: cells) {
            cell.setAbsenceNote(TrJournalMarkCell.IDW_PRESENT);
        }
    }

    // wrapper classes

    public class ColumnWrapper extends TrJournalMarkColumn
    {
        private static final long serialVersionUID = 1L;

        public ColumnWrapper(TrEduGroupEvent event)
        {
            super(event);
        }

        public boolean isCanEdit() { return getEvent().getScheduleEvent() != null && !TrJournalGroupMarkEditUI.this.isEditMode(); }
        public boolean isCanDelete() { return isAdditional(); } // { return isAdditional() && !getJEvent().isHasMarks(); }
        public String getEditButtonId() { return "editColumn" + getEvent().getId(); }
        public String getBottomEditButtonId() { return "editColumnBottom" + getEvent().getId(); }
        public String getEditDateButtonId() { return "editDate" + getEvent().getId(); }
        public String getEditThemeButtonId() { return "editTheme" + getEvent().getId(); }
        public String getEditAddMarkButtonId() { return "editMark" + getEvent().getId(); }
    }

    // getters and setters

    public TrJournalGroup getGroup()
    {
        return group;
    }

    public void setGroup(TrJournalGroup group)
    {
        this.group = group;
    }

    public List<TrJournalMarkColumnGroup> getColumnGroupList()
    {
        return columnGroupList;
    }

    public void setColumnGroupList(List<TrJournalMarkColumnGroup> columnGroupList)
    {
        this.columnGroupList = columnGroupList;
    }

    public TrJournalMarkColumnGroup getCurrentColumnGroup()
    {
        return currentColumnGroup;
    }

    public void setCurrentColumnGroup(TrJournalMarkColumnGroup currentColumnGroup)
    {
        this.currentColumnGroup = currentColumnGroup;
    }

    public List<TrJournalMarkRow> getRowList()
    {
        return rowList;
    }

    public void setRowList(List<TrJournalMarkRow> rowList)
    {
        this.rowList = rowList;
    }

    public ColumnWrapper getCurrentColumn()
    {
        return currentColumn;
    }

    public void setCurrentColumn(ColumnWrapper currentColumn)
    {
        this.currentColumn = currentColumn;
    }

    public TrJournalMarkRow getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(TrJournalMarkRow currentRow)
    {
        this.currentRow = currentRow;
    }

    public TrEduGroupEvent getEventEdited()
    {
        return eventEdited;
    }

    public void setEventEdited(TrEduGroupEvent eventEdited)
    {
        this.eventEdited = eventEdited;
    }

    public Map<MultiKey, TrJournalMarkCell> getCellMap()
    {
        return cellMap;
    }

    public Long getJournalEventForEditId()
    {
        return journalEventForEditId;
    }

    public void setJournalEventForEditId(Long journalEventForEditId)
    {
        this.journalEventForEditId = journalEventForEditId;
    }

    public ISelectModel getAbsenceModel()
    {
        return TrJournalMarkCell.getAbsenceModel();
    }

    public boolean isViewModeOnly()
    {
        return viewModeOnly;
    }

    public void setViewModeOnly(boolean viewModeOnly)
    {
        this.viewModeOnly = viewModeOnly;
    }

    public String getTutor()
    {
        return tutor;
    }

    public void setTutor(String tutor)
    {
        this.tutor = tutor;
    }

    public IBrsDao.ICurrentRatingCalc getCalculatedRating()
    {
        return calculatedRating;
    }

    public void setCalculatedRating(IBrsDao.ICurrentRatingCalc calculatedRating)
    {
        this.calculatedRating = calculatedRating;
    }

    public List<ISessionBrsDao.IRatingAdditParamDef> getAdditParamList() {
        return additParamList;
    }

    public void setAdditParamList(List<ISessionBrsDao.IRatingAdditParamDef> additParamList) {
        this.additParamList = additParamList;
    }

    public ISessionBrsDao.IRatingAdditParamDef getCurrentAdditParam() {
        return currentAdditParam;
    }

    public void setCurrentAdditParam(ISessionBrsDao.IRatingAdditParamDef currentAdditParam) {
        this.currentAdditParam = currentAdditParam;
    }

    public List<IJournalGroupMarkEditAdditionalButton> getAdditionalButtons() {
        return additionalButtons;
    }

    public void setAdditionalButtons(List<IJournalGroupMarkEditAdditionalButton> additionalButtons) {
        this.additionalButtons = additionalButtons;
    }

    public IJournalGroupMarkEditAdditionalButton getCurrentAdditButton() {
        return currentAdditButton;
    }

    public void setCurrentAdditButton(IJournalGroupMarkEditAdditionalButton currentAdditButton) {
        this.currentAdditButton = currentAdditButton;
    }

    public IBrsDao.IJournalEventStatCalc getCalculatedStat()
    {
        return calculatedStat;
    }

    public void setCalculatedStat(IBrsDao.IJournalEventStatCalc calculatedStat)
    {
        this.calculatedStat = calculatedStat;
    }

    public ISessionBrsDao.IRatingAdditParamDef getCurrentStatParam()
    {
        return currentStatParam;
    }

    public void setCurrentStatParam(ISessionBrsDao.IRatingAdditParamDef currentStatParam)
    {
        this.currentStatParam = currentStatParam;
    }

    public void onSortEvents(List<TrEduGroupEvent> events) {

    }
}
