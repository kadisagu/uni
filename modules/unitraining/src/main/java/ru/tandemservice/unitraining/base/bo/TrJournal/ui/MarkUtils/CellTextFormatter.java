/* $Id:$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils;

import org.tandemframework.core.view.formatter.DoubleFormatter;

/**
 * @author oleyba
 * @since 11/9/12
 */
public class CellTextFormatter
{
    public static String format(TrJournalMarkCell cell)
    {
        StringBuilder sb = new StringBuilder();
        if (null != cell.isAbsent() && cell.isShowAttendance())
            sb.append(cell.isAbsent() ? "н"  : ".&nbsp;");
        else
            sb.append("&nbsp;");
        if (cell.getGrade() != null)
            sb.append("&nbsp;").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(cell.getGrade()));
        return sb.toString();
    }
}
