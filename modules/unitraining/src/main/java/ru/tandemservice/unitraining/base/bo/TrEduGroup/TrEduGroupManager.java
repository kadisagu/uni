/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrEduGroup;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;

/**
 * @author oleyba
 * @since 9/15/11
 */
@Configuration
public class TrEduGroupManager extends BusinessObjectManager
{
    public static final String DS_TR_EDU_GROUP_STUDENT = "trEduGroupAbsentStudentDS";
    public static final String KEY_EDU_GROUP = "eduGroup";

    public static TrEduGroupManager instance() {
        return instance(TrEduGroupManager.class);
    }

    @Bean
    public UIDataSourceConfig trEduGroupAbsentStudentDSConfig()
    {
        return SelectDSConfig.with(TrEduGroupManager.DS_TR_EDU_GROUP_STUDENT, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .addColumn("title", Student.person().identityCard().fullFio().s())
        .handler(this.trEduGroupAbsentStudentDSHandler())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trEduGroupAbsentStudentDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), Student.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                final DQLSelectBuilder studentDQL = new DQLSelectBuilder()
                .fromEntity(TrEduGroupEventStudent.class, "rel").column("rel.id")
                .where(eq(property(TrEduGroupEventStudent.studentWpe().student().fromAlias("rel")), property(alias)))
                .where(eq(property(TrEduGroupEventStudent.event().group().fromAlias("rel")), commonValue(context.<Object>get(KEY_EDU_GROUP))))
                .where(eq(property(TrEduGroupEventStudent.absent().fromAlias("rel")), value(Boolean.TRUE)))
                ;
                dql.where(exists(studentDQL.buildQuery()));
            }
        }
        .order(Student.person().identityCard().fullFio())
        .filter(Student.person().identityCard().fullFio())
        ;
    }
}
