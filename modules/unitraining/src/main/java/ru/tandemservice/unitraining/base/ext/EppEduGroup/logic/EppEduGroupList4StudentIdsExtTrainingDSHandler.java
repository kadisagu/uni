/**
 *$Id$
 */
package ru.tandemservice.unitraining.base.ext.EppEduGroup.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unisession.base.ext.EppEduGroup.logic.EppEduGroupList4StudentIdsExtSessionDSHandler;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexander Shaburov
 * @since 17.07.13
 */
public class EppEduGroupList4StudentIdsExtTrainingDSHandler extends EppEduGroupList4StudentIdsExtSessionDSHandler
{
    public EppEduGroupList4StudentIdsExtTrainingDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public static final String COLUMN_JOURNAL = "columnJournal";

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DSOutput output = super.execute(input, context);

        final Map<Long, TrJournal> journalMap = new HashMap<>();
        BatchUtils.execute(output.getRecordIds(), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            final List<TrJournalGroup> list = new DQLSelectBuilder().fromEntity(TrJournalGroup.class, "j").column(property("j"))
                    .where(in(property(TrJournalGroup.group().id().fromAlias("j")), elements))
                    .createStatement(context.getSession()).list();

            for (TrJournalGroup journal : list)
                if (journalMap.put(journal.getGroup().getId(), journal.getJournal()) != null)
                    throw new IllegalStateException();
        });

        for (DataWrapper wrapper : output.<DataWrapper>getRecordList())
        {
            wrapper.setProperty(COLUMN_JOURNAL, journalMap.get(wrapper.getId()));
        }

        return output;
    }
}
