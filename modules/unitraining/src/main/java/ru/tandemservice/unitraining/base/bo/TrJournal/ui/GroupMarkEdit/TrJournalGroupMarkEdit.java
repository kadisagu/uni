/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 9/1/11
 */
@Configuration
public class TrJournalGroupMarkEdit extends BusinessComponentManager
{
    public static final String BL_JOURNAL_GROUP_MARK_EDIT = "blJournalGroupMarkEdit";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .create();
    }

    @Bean
    public ButtonListExtPoint actionsButtonList()
    {
        return buttonListExtPointBuilder(BL_JOURNAL_GROUP_MARK_EDIT)
              .addButton(submitButton("addEvent", "onClickAddAdditionalEvent").create())
              .addButton(submitButton("print", "onClickPrint").create())
              .addButton(submitButton("printSessionScaleSheet", "onClickPrintSessionScaleSheet").visible("ui:printSessionScaleSheetVisible").create())
        .create();
    }
}
