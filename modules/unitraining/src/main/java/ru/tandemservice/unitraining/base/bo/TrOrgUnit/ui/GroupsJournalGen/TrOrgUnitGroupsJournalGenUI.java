/**
 *$Id$
 */
package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.GroupsJournalGen;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.Zlo;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.EduGroupLink.TrOrgUnitEduGroupLinkUI;

/**
 * @author Alexander Shaburov
 * @since 18.03.13
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitHolder.id", required = true),
    @Bind(key = TrOrgUnitEduGroupLinkUI.GROUP_ID_LIST, binding = "groupIds")
})
public class TrOrgUnitGroupsJournalGenUI extends UIPresenter
{

    private OrgUnitHolder _orgUnitHolder = new OrgUnitHolder();
    private Collection<Long> _groupIds;

    private EducationYear _year;
    private YearDistributionPart _yearPart;

    @Zlo private boolean _journalOnDisc;
    @Zlo private boolean _journalOnLead;
    @Zlo private boolean _journalOnGroup;

    private boolean controlOnlyMode = TrJournalManager.instance().dao().isGlobalControlOnlyMode();

    @Override
    public void onComponentRefresh()
    {
        _orgUnitHolder.refresh();

        if (CollectionUtils.isEmpty(_groupIds))
            throw new ApplicationException(getConfig().getProperty("error.groups-is-empty"));
    }

    @Zlo
    public void onChangeOption()
    {
        final GroupsJournalGenOption parameter = getListenerParameter();

        if (GroupsJournalGenOption.journalOnDisc.equals(parameter))
        {
            _journalOnLead = false;
            _journalOnGroup = false;
        }
        else if (GroupsJournalGenOption.journalOnLead.equals(parameter))
        {
            _journalOnDisc = false;
            _journalOnGroup = false;
        }
        else if (GroupsJournalGenOption.journalOnGroup.equals(parameter))
        {
            _journalOnDisc = false;
            _journalOnLead = false;
        }
    }

    public void onClickApply()
    {
        TrOrgUnitManager.instance().dao().doGroupsJournalGen(getOption(), _groupIds, isControlOnlyMode());
        deactivate();
    }

    @Zlo
    public GroupsJournalGenOption getOption()
    {
        if (_journalOnDisc)
            return GroupsJournalGenOption.journalOnDisc;
        else if (_journalOnLead)
            return GroupsJournalGenOption.journalOnLead;
        else if (_journalOnGroup)
            return GroupsJournalGenOption.journalOnGroup;
        else
            throw new ApplicationException("Необходимо выбрать опцию.");
    }

    // Accessers

    public GroupsJournalGenOption getJournalOnDiscOption()
    {
        return GroupsJournalGenOption.journalOnDisc;
    }

    public GroupsJournalGenOption getJournalOnLeadOption()
    {
        return GroupsJournalGenOption.journalOnLead;
    }

    public GroupsJournalGenOption getJournalOnGroupOption()
    {
        return GroupsJournalGenOption.journalOnGroup;
    }

    // Getters & Setters

    public EducationYear getYear()
    {
        return _year;
    }

    public void setYear(EducationYear year)
    {
        _year = year;
    }

    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    public void setYearPart(YearDistributionPart yearPart)
    {
        _yearPart = yearPart;
    }

    public OrgUnitHolder getOrgUnitHolder()
    {
        return _orgUnitHolder;
    }

    public void setOrgUnitHolder(OrgUnitHolder orgUnitHolder)
    {
        _orgUnitHolder = orgUnitHolder;
    }

    public Collection<Long> getGroupIds()
    {
        return _groupIds;
    }

    public void setGroupIds(Collection<Long> groupIds)
    {
        this._groupIds = groupIds;
    }

    public boolean isJournalOnDisc()
    {
        return _journalOnDisc;
    }

    public void setJournalOnDisc(boolean journalOnDisc)
    {
        _journalOnDisc = journalOnDisc;
    }

    public boolean isJournalOnLead()
    {
        return _journalOnLead;
    }

    public void setJournalOnLead(boolean journalOnLead)
    {
        _journalOnLead = journalOnLead;
    }

    public boolean isJournalOnGroup()
    {
        return _journalOnGroup;
    }

    public void setJournalOnGroup(boolean journalOnGroup)
    {
        _journalOnGroup = journalOnGroup;
    }

    public boolean isControlOnlyMode() {
        return this.controlOnlyMode;
    }

    public void setControlOnlyMode(boolean controlOnlyMode) {
        this.controlOnlyMode = controlOnlyMode;
    }

}
