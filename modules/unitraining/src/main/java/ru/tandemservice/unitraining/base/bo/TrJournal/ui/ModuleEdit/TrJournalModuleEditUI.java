/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.ModuleEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.EditBase.TrBrsCoefficientEditBaseUI;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;

/**
 * @author oleyba
 * @since 8/11/11
 */
@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true)
})
@Output({
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_HOLDER, binding = "holder", required = true),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType", required = true)
})
public class TrJournalModuleEditUI extends UIPresenter
{
    private EntityHolder<TrJournalModule> holder = new EntityHolder<>();
    private TrBrsCoefficientOwnerType ownerType = new TrBrsCoefficientOwnerType();

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        setOwnerType(DataAccessServices.dao().get(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerType.code(), TrBrsCoefficientOwnerTypeCodes.JOURNAL_MODULE));

        RenderComponent.deactivateAll();
    }

    public void onClickApply()
    {
        DataAccessServices.dao().doInTransaction(session -> {
            IUniBaseDao.instance.get().saveOrUpdate(getModule());
            ((TrBrsCoefficientEditBaseUI) _uiSupport.getChildrenUI().entrySet().iterator().next().getValue()).save();
            return getModule();
        });
        deactivate();
    }

    public boolean isCanEditBrsCoefficient()
    {
        return getModule().getJournal().getState().isFormative();
    }

    // accessors

    public TrJournalModule getModule()
    {
        return getHolder().getValue();
    }

    public EntityHolder<TrJournalModule> getHolder()
    {
        return holder;
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(TrBrsCoefficientOwnerType ownerType)
    {
        this.ownerType = ownerType;
    }
}
