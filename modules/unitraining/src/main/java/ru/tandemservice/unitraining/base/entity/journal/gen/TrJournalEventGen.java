package ru.tandemservice.unitraining.base.entity.journal.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;
import ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Событие в реализации дисциплины
 *
 * Определяет порядок прохождения событий (встречь студентов и ппс) по разным видам нагрузки и контроля
 * (на данном этапе подразумевается, что все события независимы)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrJournalEventGen extends EntityBase
 implements IBrsSettingsOwner, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent";
    public static final String ENTITY_NAME = "trJournalEvent";
    public static final int VERSION_HASH = -136773887;
    private static IEntityMeta ENTITY_META;

    public static final String L_JOURNAL_MODULE = "journalModule";
    public static final String P_NUMBER = "number";
    public static final String L_TYPE = "type";
    public static final String P_THEME = "theme";
    public static final String P_COMMENT = "comment";
    public static final String P_HAS_MARKS = "hasMarks";

    private TrJournalModule _journalModule;     // Модуль учебной группы
    private int _number;     // Порядковый номер события
    private EppGroupType _type;     // Вид УГС
    private String _theme;     // Тема
    private String _comment;     // Комментарий
    private boolean _hasMarks;     // Есть оценки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Модуль учебной группы. Свойство не может быть null.
     */
    @NotNull
    public TrJournalModule getJournalModule()
    {
        return _journalModule;
    }

    /**
     * @param journalModule Модуль учебной группы. Свойство не может быть null.
     */
    public void setJournalModule(TrJournalModule journalModule)
    {
        dirty(_journalModule, journalModule);
        _journalModule = journalModule;
    }

    /**
     * @return Порядковый номер события. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Порядковый номер события. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Вид УГС. Свойство не может быть null.
     */
    @NotNull
    public EppGroupType getType()
    {
        return _type;
    }

    /**
     * @param type Вид УГС. Свойство не может быть null.
     */
    public void setType(EppGroupType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Тема.
     */
    public String getTheme()
    {
        return _theme;
    }

    /**
     * @param theme Тема.
     */
    public void setTheme(String theme)
    {
        dirty(_theme, theme);
        _theme = theme;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        initLazyForGet("comment");
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        initLazyForSet("comment");
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * Есть оценки = true, если по событию журнала выставлена отметка о явке/неявке и/или балл. Поле автообновляемое.
     *
     * @return Есть оценки. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasMarks()
    {
        return _hasMarks;
    }

    /**
     * @param hasMarks Есть оценки. Свойство не может быть null.
     */
    public void setHasMarks(boolean hasMarks)
    {
        dirty(_hasMarks, hasMarks);
        _hasMarks = hasMarks;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TrJournalEventGen)
        {
            setJournalModule(((TrJournalEvent)another).getJournalModule());
            setNumber(((TrJournalEvent)another).getNumber());
            setType(((TrJournalEvent)another).getType());
            setTheme(((TrJournalEvent)another).getTheme());
            setComment(((TrJournalEvent)another).getComment());
            setHasMarks(((TrJournalEvent)another).isHasMarks());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrJournalEventGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrJournalEvent.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("TrJournalEvent is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "journalModule":
                    return obj.getJournalModule();
                case "number":
                    return obj.getNumber();
                case "type":
                    return obj.getType();
                case "theme":
                    return obj.getTheme();
                case "comment":
                    return obj.getComment();
                case "hasMarks":
                    return obj.isHasMarks();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "journalModule":
                    obj.setJournalModule((TrJournalModule) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "type":
                    obj.setType((EppGroupType) value);
                    return;
                case "theme":
                    obj.setTheme((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "hasMarks":
                    obj.setHasMarks((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "journalModule":
                        return true;
                case "number":
                        return true;
                case "type":
                        return true;
                case "theme":
                        return true;
                case "comment":
                        return true;
                case "hasMarks":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "journalModule":
                    return true;
                case "number":
                    return true;
                case "type":
                    return true;
                case "theme":
                    return true;
                case "comment":
                    return true;
                case "hasMarks":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "journalModule":
                    return TrJournalModule.class;
                case "number":
                    return Integer.class;
                case "type":
                    return EppGroupType.class;
                case "theme":
                    return String.class;
                case "comment":
                    return String.class;
                case "hasMarks":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrJournalEvent> _dslPath = new Path<TrJournalEvent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrJournalEvent");
    }
            

    /**
     * @return Модуль учебной группы. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent#getJournalModule()
     */
    public static TrJournalModule.Path<TrJournalModule> journalModule()
    {
        return _dslPath.journalModule();
    }

    /**
     * @return Порядковый номер события. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Вид УГС. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent#getType()
     */
    public static EppGroupType.Path<EppGroupType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Тема.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent#getTheme()
     */
    public static PropertyPath<String> theme()
    {
        return _dslPath.theme();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * Есть оценки = true, если по событию журнала выставлена отметка о явке/неявке и/или балл. Поле автообновляемое.
     *
     * @return Есть оценки. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent#isHasMarks()
     */
    public static PropertyPath<Boolean> hasMarks()
    {
        return _dslPath.hasMarks();
    }

    public static class Path<E extends TrJournalEvent> extends EntityPath<E>
    {
        private TrJournalModule.Path<TrJournalModule> _journalModule;
        private PropertyPath<Integer> _number;
        private EppGroupType.Path<EppGroupType> _type;
        private PropertyPath<String> _theme;
        private PropertyPath<String> _comment;
        private PropertyPath<Boolean> _hasMarks;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Модуль учебной группы. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent#getJournalModule()
     */
        public TrJournalModule.Path<TrJournalModule> journalModule()
        {
            if(_journalModule == null )
                _journalModule = new TrJournalModule.Path<TrJournalModule>(L_JOURNAL_MODULE, this);
            return _journalModule;
        }

    /**
     * @return Порядковый номер события. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(TrJournalEventGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Вид УГС. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent#getType()
     */
        public EppGroupType.Path<EppGroupType> type()
        {
            if(_type == null )
                _type = new EppGroupType.Path<EppGroupType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Тема.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent#getTheme()
     */
        public PropertyPath<String> theme()
        {
            if(_theme == null )
                _theme = new PropertyPath<String>(TrJournalEventGen.P_THEME, this);
            return _theme;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(TrJournalEventGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * Есть оценки = true, если по событию журнала выставлена отметка о явке/неявке и/или балл. Поле автообновляемое.
     *
     * @return Есть оценки. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent#isHasMarks()
     */
        public PropertyPath<Boolean> hasMarks()
        {
            if(_hasMarks == null )
                _hasMarks = new PropertyPath<Boolean>(TrJournalEventGen.P_HAS_MARKS, this);
            return _hasMarks;
        }

        public Class getEntityClass()
        {
            return TrJournalEvent.class;
        }

        public String getEntityName()
        {
            return "trJournalEvent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
