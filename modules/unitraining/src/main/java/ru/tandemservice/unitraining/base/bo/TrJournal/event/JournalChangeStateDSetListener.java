/* $Id:$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.event;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;

import java.util.Collections;
import java.util.List;

/**
 * @author oleyba
 * @since 8/24/12
 */
public class JournalChangeStateDSetListener implements IDSetEventListener
{

    public void init()
    {
        // меняется журнал
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, TrJournal.class, this);
    }

    @Override
    public void onEvent(DSetEvent event)
    {
        if (TrJournal.class.isAssignableFrom(event.getMultitude().getEntityMeta().getEntityClass()) && event.getMultitude().getAffectedProperties().contains(TrJournal.L_STATE)) {
            final List<TrJournal> journals = event.getMultitude().isSingular() ?
                        Collections.singletonList((TrJournal) event.getMultitude().getSingularEntity()) :
                            new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "e").column("e").createStatement(event.getContext()).<TrJournal>list();

            for (TrJournal journal : journals)
                if (journal.getState().isAcceptable())
                    TrBrsCoefficientManager.instance().brsDao().validateOnAcceptance(journal);
        }
    }

}
