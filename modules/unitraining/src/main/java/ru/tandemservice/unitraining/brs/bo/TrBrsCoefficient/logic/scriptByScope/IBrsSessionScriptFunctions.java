/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope;

import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.uni.util.groovy.IScriptInstance;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;

import java.util.Map;

/**
 * @author oleyba
 * @since 11/15/12
 */
public interface IBrsSessionScriptFunctions extends IScriptInstance
{
    RtfString getPrintableRatingFormula(EppGradeScale gradeScale, EppFControlActionGroup caGroup);

    ISessionBrsDao.ISessionRatingSettings getRatingSettings(ISessionBrsDao.ISessionRatingSettingsKey key, IBrsDao.IBrsPreparedSettings brsSettings, boolean useRating);

    IBrsDao.IStudentSessionMarkData calculateSessionMark(ISessionBrsDao.IStudentCurrentRatingData ratingData, Double pointsForSessionCA, ISessionBrsDao.ISessionRatingSettings ratingSettings);

    Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions();
}
