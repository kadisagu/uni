/* $Id$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic;

import com.google.common.base.Preconditions;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.unisession.brs.dao.ISessionBrsSettings;

/**
 * @author Nikolay Fedorovskih
 * @since 21.01.2015
 */
public class SessionBrsSettingsWrapper implements ISessionBrsSettings
{
    private static final String GLOBAL_BRS_SESSION_SETTINGS_OWNER = "brsGlobalSessionSettings";
    private static final String KEY_USE_RATING_FOR_TRANSFER_DOC = "useRatingForTransferDocument";
    private static final String KEY_REQUIRED_RATING_FOR_TRANSFER_DOC = "requiredRatingForTransferDocument";
    private static final String KEY_USE_RATING_FOR_LIST_DOC = "useRatingForListDocument";
    private static final String KEY_REQUIRED_RATING_FOR_LIST_DOC = "requiredRatingForListDocument";

    private final boolean _readOnly;
    private final IDataSettings _settings;

    public SessionBrsSettingsWrapper(boolean readOnly)
    {
        _settings = DataSettingsFacade.getSettings(GLOBAL_BRS_SESSION_SETTINGS_OWNER);
        _readOnly = readOnly;
    }

    private <T> T get(String key, T defaultValue)
    {
        T ret = _settings.get(key);
        return ret != null ? ret : defaultValue;
    }

    private void set(String key, Object value)
    {
        Preconditions.checkState(!_readOnly);
        _settings.set(key, value);
    }

    public IDataSettings getDataSettings()
    {
        Preconditions.checkState(!_readOnly);
        return _settings;
    }

    @Override
    public boolean isUseCurrentRatingForTransferDocument()
    {
        return this.get(KEY_USE_RATING_FOR_TRANSFER_DOC, false);
    }

    public void setUseCurrentRatingForTransferDocument(boolean value)
    {
        set(KEY_USE_RATING_FOR_TRANSFER_DOC, value);
    }

    @Override
    public boolean isCurrentRatingRequiredForTransferDocument()
    {
        return this.get(KEY_REQUIRED_RATING_FOR_TRANSFER_DOC, false);
    }

    public void setCurrentRatingRequiredForTransferDocument(boolean value)
    {
        this.set(KEY_REQUIRED_RATING_FOR_TRANSFER_DOC, value);
    }

    @Override
    public boolean isUseCurrentRatingForListDocument()
    {
        return this.get(KEY_USE_RATING_FOR_LIST_DOC, false);
    }

    public void setUseCurrentRatingForListDocument(boolean value)
    {
        this.set(KEY_USE_RATING_FOR_LIST_DOC, value);
    }

    @Override
    public boolean isCurrentRatingRequiredForListDocument()
    {
        return this.get(KEY_REQUIRED_RATING_FOR_LIST_DOC, false);
    }

    public void setCurrentRatingRequiredForListDocument(boolean value)
    {
        this.set(KEY_REQUIRED_RATING_FOR_LIST_DOC, value);
    }
}