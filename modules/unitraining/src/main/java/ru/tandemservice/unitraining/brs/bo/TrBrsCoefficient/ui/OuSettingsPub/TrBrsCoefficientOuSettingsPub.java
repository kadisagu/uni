/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuSettingsPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;

/**
 * @author oleyba
 * @since 8/17/12
 */
@Configuration
public class TrBrsCoefficientOuSettingsPub extends BusinessComponentManager
{
    public static final String BIND_OU = "orgUnit";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(trEduYearPartDSConfig())
        .create();
    }

    @Bean
    public UIDataSourceConfig trEduYearPartDSConfig()
    {
        return SelectDSConfig.with("trEduYearPartDS", this.getName())
        .dataSourceClass(SelectDataSource.class)
        .addColumn(TrOrgUnitSettings.eduYearPart().title().s())
        .handler(this.trEduYearPartDSHandler())
        .valueStyleSource(EducationCatalogsManager.getEduYearValueStyleSource())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trEduYearPartDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), TrOrgUnitSettings.class)
        .where(TrOrgUnitSettings.orgUnit(), TrBrsCoefficientOuSettingsPub.BIND_OU)
        .order(TrOrgUnitSettings.eduYearPart().year().educationYear().intValue())
        .order(TrOrgUnitSettings.eduYearPart().part().number())
        .filter(TrOrgUnitSettings.eduYearPart().year().educationYear().title())
        .filter(TrOrgUnitSettings.eduYearPart().part().title())
        ;
    }
}
