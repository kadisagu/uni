/* $Id$ */
package ru.tandemservice.unitraining.base.ext.Person;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Vasily Zhukov
 * @since 17.11.2011
 */
@Configuration
public class PersonExtManager extends BusinessObjectExtensionManager
{
}
