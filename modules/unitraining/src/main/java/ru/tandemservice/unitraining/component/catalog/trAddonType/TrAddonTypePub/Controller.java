/* $Id: Controller.java 22487 2012-04-04 13:16:00Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unitraining.component.catalog.trAddonType.TrAddonTypePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unitraining.base.entity.catalog.TrAddonType;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;

/**
 * @author AutoGenerator
 * Created on 06.10.2012
 */
public class Controller extends DefaultCatalogPubController<TrAddonType, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<TrAddonType> createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);

        DynamicListDataSource<TrAddonType> dataSource = new DynamicListDataSource<TrAddonType>(context, this);
        dataSource.addColumn(new SimpleColumn("Код", TrBrsCoefficientDef.P_USER_CODE));
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", TrAddonType.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Аббревиатура", TrAddonType.abbreviation().s()));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", TrAddonType.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        return dataSource;
    }
}
