/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils;

import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAddon;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

import java.util.Date;

/**
 * @author oleyba
 * @since 9/7/11
 */
public class TrJournalMarkColumn extends IdentifiableWrapper<TrEduGroupEvent>
{
    private static final DateFormatter DATE_FORMATTER_DAY_AND_MONTH = new DateFormatter("dd.MM");

    private TrEduGroupEvent event;
    private TrJournalEvent jEvent;
    private String tooltip;

    public TrJournalMarkColumn(TrEduGroupEvent event)
    {
        this(event.getJournalEvent());
        this.event = event;
    }

    public TrJournalMarkColumn(TrJournalEvent event)
    {
        super(event.getId(), event.getAbbreviationTitle());
        this.jEvent = event;
        initTooltip();
    }

    public String getDate()
    {
        return getEvent() != null && getEvent().getScheduleEvent() == null ? "       " : DATE_FORMATTER_DAY_AND_MONTH.format(event.getScheduleEvent().getDurationBegin());
    }

    public String getEndDate()
    {
        return getEvent() != null && getEvent().getScheduleEvent() == null ? "       " : DATE_FORMATTER_DAY_AND_MONTH.format(event.getScheduleEvent().getDurationEnd());
    }

    public boolean isDatesDiffer()
    {
        if (null == getEvent() || null == getEvent().getScheduleEvent()) return false;

        Date durationBegin = getEvent().getScheduleEvent().getDurationBegin();
        Date durationEnd = getEvent().getScheduleEvent().getDurationEnd();
        return durationBegin != null && durationEnd != null && !DateUtils.isSameDay(durationBegin, durationEnd);
    }

    public TrEduGroupEvent getEvent()
    {
        return event;
    }

    public TrJournalEvent getJEvent()
    {
        if (event != null)
            return event.getJournalEvent();
        return jEvent;
    }

    public String getTitleId()
    {
        return "columnTitle" + getJEvent().getId();
    }

    public String getTooltip()
    {
        return tooltip;
    }

    public boolean isAdditional()
    {
        return getJEvent() instanceof TrEventAddon;
    }

    private void initTooltip()
    {
        StringBuilder tooltipBuilder = new StringBuilder();
        tooltipBuilder.append("<div>").append(getJEvent().getShortTitle()).append("</div>");
        if (getJEvent().getTheme() != null)
            tooltipBuilder.append("<div>Тема: ").append(getJEvent().getTheme()).append("</div>");
        for (TrBrsCoefficientValue value : DataAccessServices.dao().getList(TrBrsCoefficientValue.class, TrBrsCoefficientValue.owner().s(), getJEvent(), TrBrsCoefficientValue.definition().title().s()))
            tooltipBuilder.append("<div>").append(value.getDefinition().getShortTitle()).append(": ").append(value.getValueStr()).append("</div>");
        this.tooltip = tooltipBuilder.toString();
    }
}
