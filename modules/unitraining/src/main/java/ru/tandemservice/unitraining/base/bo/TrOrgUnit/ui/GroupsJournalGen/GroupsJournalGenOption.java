/**
 *$Id$
 */
package ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.GroupsJournalGen;

/**
 * Опции(способы) формирования реализаций для УГС.
 */
public enum GroupsJournalGenOption
{
    /**
     * На все уникальные дисциплины УГС будет сформирована новая реализация, после чего УГС по данной дисциплине будут прикреплены к этой реализации.
     */
    journalOnDisc,

    /**
     * По каждой дисциплине на ведущие УГС (по наиболее приоритетному виду нагрузки в порядке: лекции-практики-лабораторные) будут сформированы реализации, по одной на каждую ведущую УГС.
     * К сформированным реализациям будет прикреплена ведущая УГС, а затем все входящие УГС (т.е. те, которые по составу студентов полностью входят в ведущую).
     */
    journalOnLead,

    /**
     * На каждую УГС будет сформирована новая реализация, затем УГС будет к ней прикреплена.
     */
    journalOnGroup
}
