package ru.tandemservice.unitraining.base.entity.journal;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.unitraining.base.entity.journal.gen.TrJournalModuleGen;

/**
 * Модуль в журнале
 *
 * Определяет, что в рамках журнала будет читаться определенный модуль
 * (объекты создаются автоматом по данным из части элемента реестра)
 */
public class TrJournalModule extends TrJournalModuleGen
{
    public TrJournalModule() {}

    public TrJournalModule(TrJournal journal, EppRegistryElementPartModule module) {
        this.setJournal(journal);
        this.setModule(module);
    }

    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1671209")
    public String getTitle()
    {
        String title = getModule().getTitle();
        if (null != getModuleBeginDate() || null != getModuleEndDate())
            title = title + " (" + getPeriod() + ")";
        return title;
    }

    @Override
    public String getDisplayableTitle()
    {
        return getTitle();
    }

    public String getPeriod()
    {
        if (null != getModuleBeginDate() || null != getModuleEndDate())
            return DateFormatter.DEFAULT_DATE_FORMATTER.format(getModuleBeginDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getModuleEndDate());
        return "";
    }

    @Override
    public String getTypeTitle()
    {
        return "Модуль в реализации дисциплины";
    }

    @Override
    public boolean isAllowEditSettings()
    {
        return true;
    }
}