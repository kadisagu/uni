/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrOrgUnit.logic;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.TitledFormatter;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/12/11
 */
public class TrOrgUnitEduGroupDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String COLUMN_SELECT = "select";
    public static final String COLUMN_JOURNAL = "journal";
    public static final String COLUMN_COURSE = "course";
    public static final String COLUMN_GROUP = "group";
    public static final String COLUMN_EDU_OU = "eduOu";
    public static final String COLUMN_PPS = "pps";

    public static final String PARAM_ORG_UNIT = "orgUnit";
    public static final String PARAM_YEAR_PART = "yearPart";
    public static final String PARAM_ACTIVITY_ELEMENT_PART = "registryElementPart";
    public static final String PARAM_LINKED = "linked";
    public static final String PARAM_STATE = "state";
    public static final String PARAM_TUTOR = "tutor";
    public static final String PARAM_GROUP_LIST = "groupList";

    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(EppRealEduGroup4LoadType.class, "e");
    }

    public TrOrgUnitEduGroupDSHandler(final String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final Session session = context.getSession();

        // обязательные параметры
        final OrgUnit orgUnit = context.get(TrOrgUnitEduGroupDSHandler.PARAM_ORG_UNIT);
        final EppYearPart yearPart = context.get(TrOrgUnitEduGroupDSHandler.PARAM_YEAR_PART);

        DQLSelectBuilder dql = registry.buildDQLSelectBuilder().column(property("e"))
        .where(DQLExpressions.eq(DQLExpressions.property(EppRealEduGroup4LoadType.activityPart().registryElement().owner().fromAlias("e")), DQLExpressions.value(orgUnit)))
        .where(DQLExpressions.eq(DQLExpressions.property(EppRealEduGroup4LoadType.summary().yearPart().fromAlias("e")), DQLExpressions.value(yearPart)));

        dql = this.applyFilters(dql, input, context);
        registry.applyOrder(dql, input.getEntityOrder());
        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, session).build();

        for (List<DataWrapper> wrappersPart : Lists.partition(DataWrapper.wrap(output, "id", "title"), DQL.MAX_VALUES_ROW_NUMBER)) {

            Collection<Long> ids = CommonDAO.ids(wrappersPart);

            // связь с журналом
            final List<Object[]> rows = new DQLSelectBuilder()
                    .fromEntity(TrJournalGroup.class, "j2g")
                    .where(in(property(TrJournalGroup.group().id().fromAlias("j2g")), ids))
                    .column(property(TrJournalGroup.group().id().fromAlias("j2g")))
                    .column(property(TrJournalGroup.journal().fromAlias("j2g")))
                    .createStatement(session).list();
            final Map<Long, TrJournal> journalMap = new HashMap<>();
            for (final Object[] row : rows)
                journalMap.put((Long) row[0], (TrJournal) row[1]);

            // курс, группа, напр. подг
            final List<EppRealEduGroup4LoadTypeRow> groupRows = new DQLSelectBuilder()
                    .fromEntity(EppRealEduGroup4LoadTypeRow.class, "r")
                    .where(in(property(EppRealEduGroup4LoadTypeRow.group().id().fromAlias("r")), ids))
                    .column("r")
                    .createStatement(session).list();
            final Map<Long, Set<Integer>> courseMap = SafeMap.get(HashSet.class);
            final Map<Long, Set<String>> groupMap = SafeMap.get(HashSet.class);
            final Map<Long, Set<EducationOrgUnit>> eduOrgUnitMap = SafeMap.get(HashSet.class);
            for (EppRealEduGroup4LoadTypeRow row : groupRows)
            {
                courseMap.get(row.getGroup().getId()).add(row.getStudentWpePart().getStudentWpe().getCourse().getIntValue());
                groupMap.get(row.getGroup().getId()).add(row.getStudentGroupTitle());
                eduOrgUnitMap.get(row.getGroup().getId()).add(row.getStudentEducationOrgUnit());
            }

            // ППС
            List<Object[]> ppsRows = new DQLSelectBuilder()
                    .fromEntity(EppPpsCollectionItem.class, "rel")
                    .column(DQLExpressions.property(EppPpsCollectionItem.list().id().fromAlias("rel")))
                    .column(DQLExpressions.property(EppPpsCollectionItem.pps().fromAlias("rel")))
                    .where(DQLExpressions.in(DQLExpressions.property(EppPpsCollectionItem.list().id().fromAlias("rel")), ids))
                    .createStatement(context.getSession()).list();
            final Map<Long, Set<PpsEntry>> ppsMap = SafeMap.get(HashSet.class);
            for (Object[] row : ppsRows)
                ppsMap.get((Long) row[0]).add((PpsEntry) row[1]);

            for (final DataWrapper wrapper : wrappersPart)
            {
                wrapper.put(TrOrgUnitEduGroupDSHandler.COLUMN_JOURNAL, journalMap.get(wrapper.getId()));
                wrapper.put(TrOrgUnitEduGroupDSHandler.COLUMN_SELECT, Boolean.FALSE);
                wrapper.put(TrOrgUnitEduGroupDSHandler.COLUMN_GROUP, TrOrgUnitEduGroupDSHandler.this.collect(groupMap, wrapper.getId(), null));
                wrapper.put(TrOrgUnitEduGroupDSHandler.COLUMN_COURSE, collect(courseMap, wrapper.getId(), null));
                wrapper.put(TrOrgUnitEduGroupDSHandler.COLUMN_EDU_OU, collect(eduOrgUnitMap, wrapper.getId(), educationOrgUnitFormatter));
                wrapper.put(TrOrgUnitEduGroupDSHandler.COLUMN_PPS, TrOrgUnitEduGroupDSHandler.this.collect(ppsMap, wrapper.getId(), TitledFormatter.INSTANCE));
            }
        }

        return output;
    }

    protected DQLSelectBuilder applyFilters(final DQLSelectBuilder dql, final DSInput input, final ExecutionContext context)
    {
        final EppRegistryElementPart activityElement = context.get(PARAM_ACTIVITY_ELEMENT_PART);
        if (null != activityElement)
            dql.where(eq(property(EppRealEduGroup4LoadType.activityPart().fromAlias("e")), value(activityElement)));

        final Boolean linked = context.get(PARAM_LINKED);
        if (Boolean.TRUE.equals(linked))
            dql.joinEntity("e", DQLJoinType.inner, TrJournalGroup.class, "jRel", eq(property(TrJournalGroup.group().fromAlias("jRel")), property("e")));

        final PpsEntry tutor = context.get(PARAM_TUTOR);
        if (null != tutor)
        {
            dql.joinEntity("e", DQLJoinType.inner, EppPpsCollectionItem.class, "ppsC", eq(property(EppPpsCollectionItem.list().fromAlias("ppsC")), property("e")));
            dql.where(eq(property(EppPpsCollectionItem.pps().fromAlias("ppsC")), value(tutor)));
        }

        final List<DataWrapper> groupList = context.get(PARAM_GROUP_LIST);
        if (groupList != null && !groupList.isEmpty())
        {
            DQLSelectBuilder groupDQL = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "g").column(property(EppRealEduGroup4LoadTypeRow.group().id().fromAlias("g")));

            final List<String> groupTitleList = CommonBaseUtil.getPropertiesList(groupList, DataWrapper.TITLE);
            if (groupTitleList.contains(""))
            {
                groupDQL.where(or(
                    isNull(property(EppRealEduGroup4LoadTypeRow.studentGroupTitle().fromAlias("g"))),
                    in(property(EppRealEduGroup4LoadTypeRow.studentGroupTitle().fromAlias("g")), groupTitleList)));
            }
            else
            {
                groupDQL.where(in(property(EppRealEduGroup4LoadTypeRow.studentGroupTitle().fromAlias("g")), groupTitleList));
            }
            dql.where(in(property(EppRealEduGroup4LoadType.id().fromAlias("e")), groupDQL.buildQuery()));
        }

        // todo тут другой класс
        //        final EppState state = context.get(PARAM_STATE);
        //        if (null != state)
        //            dql.where(eq(property(EppRealEduGroup4LoadType.level().fromAlias("e")), value(state)));

        return dql;
    }

    protected final IFormatter<EducationOrgUnit> educationOrgUnitFormatter = eduOu -> (eduOu.getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix() + " " + eduOu.getShortTitle() + " " + eduOu.getDevelopCombinationTitle());

    @SuppressWarnings("unchecked")
    protected <T> String collect(final Map<Long, Set<T>> map, final Long id, final IFormatter formatter) {
        final Set<T> set = map.get(id);
        if (null == set) { return ""; }
        final List<String> elements = new ArrayList<>(set.size());
        for (final T i: set) {
            final Object obj = (null == formatter) ? i : formatter.format(i);
            final String value = (null == obj ? "" : String.valueOf(obj));
            elements.add(value);
        }
        Collections.sort(elements);
        return StringUtils.join(elements, "\n");
    }
}
