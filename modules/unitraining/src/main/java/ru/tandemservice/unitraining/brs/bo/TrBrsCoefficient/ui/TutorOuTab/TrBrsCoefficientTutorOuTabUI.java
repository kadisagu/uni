/* $Id:$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.TutorOuTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;

/**
 * @author oleyba
 * @since 8/17/12
 */
@Input({
    @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
@Output({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitHolder.id"),
    @Bind(key = TrBrsCoefficientManager.BIND_OWNER_TYPE, binding = "ownerType.id")
})
public class TrBrsCoefficientTutorOuTabUI extends UIPresenter
{
    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    private TrBrsCoefficientOwnerType ownerType;

    @Override
    public void onComponentRefresh()
    {
        getOrgUnitHolder().refresh();
        setOwnerType(DataAccessServices.dao().get(TrBrsCoefficientOwnerType.class, TrBrsCoefficientOwnerType.code(), TrBrsCoefficientOwnerTypeCodes.TUTOR_OU));
    }

    public OrgUnitHolder getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return ownerType;
    }

    public void setOwnerType(TrBrsCoefficientOwnerType ownerType)
    {
        this.ownerType = ownerType;
    }
}
