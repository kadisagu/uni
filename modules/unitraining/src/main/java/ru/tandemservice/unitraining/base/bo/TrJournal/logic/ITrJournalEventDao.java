/* $Id$ */
package ru.tandemservice.unitraining.base.bo.TrJournal.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unitraining.base.entity.catalog.TrAbsenceReason;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 8/11/11
 */
public interface ITrJournalEventDao extends INeedPersistenceSupport
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteEvent(Long evendId);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void moveEvent(Long evendId, boolean down);

    /**
     * Копирование события в журнале
     * @param sourceEventId - id исходного события
     * @return id нового события
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Long copyEvent(Long sourceEventId);

    /**
     * Сохранение вновь созданного события в реализации и в журнале группы с присвоением следующего по порядку номера
     * @param newEvent вновь созданное событие
     * @param journalModule модуль в реализации, в котором создано событие
     * @param journalGroup журнал группы
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void saveNewEvent(TrJournalEvent newEvent, TrJournalModule journalModule, TrJournalGroup journalGroup);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void renumerate(List<TrJournalEvent> events);

    /**
     * Устанавливает событиям в журнале для студента причину пропуска занятий
     * @param ids - id событий
     * @param reason - причина
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void updateReason(Collection<Long> ids, TrAbsenceReason reason);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void saveJournalMark(EppStudentWorkPlanElement slot, TrEduGroupEvent event, IJournalMarkData mark);

    /**
     * Сохраняет\апдейтит событие расписания для события группы
     * @param groupEvent
     * @param schEvent
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void saveOrUpdateScheduleEvent(TrEduGroupEvent groupEvent, ScheduleEvent schEvent, Collection<UniplacesPlace> places);

    /**
     * Удаляет событие расписания для события группы
     * @param groupEvent
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void deleteScheduleEvent(TrEduGroupEvent groupEvent);

    /**
     * Удаляет дополнительное событие (если оно дополнительное, и в нем нет оценок)
     * @param evendId TrEduGroupEvent.id
     * @throws IllegalArgumentException если событие не дополнительное, или в нем есть оценки
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void deleteAdditionalEvent(Long evendId) throws IllegalArgumentException;

    /** Интерфейс для передачи данных об отметке в журнале */
    public interface IJournalMarkData
    {
        Boolean isAbsent();
        Double getGrade();
        String getComment();
        Date getScheduledDate();
        Date getActualDate(); // todo: в данный момент не сохраняется и не работает!
    }
}
