package ru.tandemservice.unitraining.base.entity.journal.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unitraining.base.entity.catalog.TrAddonType;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAddon;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Событие в реализации (необязательное дополнительное мероприятие)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TrEventAddonGen extends TrJournalEvent
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitraining.base.entity.journal.TrEventAddon";
    public static final String ENTITY_NAME = "trEventAddon";
    public static final int VERSION_HASH = -1072112420;
    private static IEntityMeta ENTITY_META;

    public static final String L_ADDON_TYPE = "addonType";

    private TrAddonType _addonType;     // Тип мероприятия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип мероприятия. Свойство не может быть null.
     */
    @NotNull
    public TrAddonType getAddonType()
    {
        return _addonType;
    }

    /**
     * @param addonType Тип мероприятия. Свойство не может быть null.
     */
    public void setAddonType(TrAddonType addonType)
    {
        dirty(_addonType, addonType);
        _addonType = addonType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TrEventAddonGen)
        {
            setAddonType(((TrEventAddon)another).getAddonType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TrEventAddonGen> extends TrJournalEvent.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TrEventAddon.class;
        }

        public T newInstance()
        {
            return (T) new TrEventAddon();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "addonType":
                    return obj.getAddonType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "addonType":
                    obj.setAddonType((TrAddonType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "addonType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "addonType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "addonType":
                    return TrAddonType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TrEventAddon> _dslPath = new Path<TrEventAddon>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TrEventAddon");
    }
            

    /**
     * @return Тип мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrEventAddon#getAddonType()
     */
    public static TrAddonType.Path<TrAddonType> addonType()
    {
        return _dslPath.addonType();
    }

    public static class Path<E extends TrEventAddon> extends TrJournalEvent.Path<E>
    {
        private TrAddonType.Path<TrAddonType> _addonType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.unitraining.base.entity.journal.TrEventAddon#getAddonType()
     */
        public TrAddonType.Path<TrAddonType> addonType()
        {
            if(_addonType == null )
                _addonType = new TrAddonType.Path<TrAddonType>(L_ADDON_TYPE, this);
            return _addonType;
        }

        public Class getEntityClass()
        {
            return TrEventAddon.class;
        }

        public String getEntityName()
        {
            return "trEventAddon";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
