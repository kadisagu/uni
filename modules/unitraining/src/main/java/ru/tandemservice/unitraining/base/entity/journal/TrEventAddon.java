package ru.tandemservice.unitraining.base.entity.journal;

import org.tandemframework.core.view.formatter.StringLimitFormatter;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateMutable;
import ru.tandemservice.unitraining.base.entity.journal.gen.TrEventAddonGen;

/**
 * Планируемое событие журнала (неоьязательное дополнительное мероприятие)
 */
@EppStateMutable(disable=true)
public class TrEventAddon extends TrEventAddonGen
{
    @Override
    public String getTitle() {
        if (getType() == null) {
            return this.getClass().getSimpleName();
        }
        String title = getNumber() + ". (" + getType().getAbbreviation() + ") " + getAddonType().getTitle();
        if (null != getTheme())
            title = title + " (" + StringLimitFormatter.NON_BROKEN_WORDS.format(getTheme()) + ")";
        return title;
    }

    @Override
    public String getDisplayableTitle()
    {
        return getTitle();
    }

    @Override
    public TrJournalEvent getClone() {
        TrEventAddon newEvent = new TrEventAddon();
        newEvent.setJournalModule(getJournalModule());
        newEvent.setType(getType());
        newEvent.setNumber(this.getNumber() + 1);
        return newEvent;
    }

    @Override
    public String getShortTitle() {
        return getNumber() + ". " + getAddonType().getTitle();
    }

    @Override
    public String getAbbreviationTitle() {
        return getAddonType().getAbbreviation() + getNumber();
    }

    @Override
    public String getTypeTitle()
    {
        return "Графа для доп. балла";
    }
}