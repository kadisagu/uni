/* $Id$ */
package ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic;

import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;

/**
 * @author Nikolay Fedorovskih
 * @since 18.02.2014
 */
public interface IBrsCoefficientRowWrapper
{
    TrBrsCoefficientDef getDef();

    Object getValue();
}