package ru.tandemservice.unitraining.base.entity.journal;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateMutable;
import ru.tandemservice.uniepp.base.bo.EppState.EppStatePath;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.unitraining.base.entity.journal.gen.TrJournalEventGen;

/**
 * Планируемое событие журнала
 */
@EppStatePath(TrJournalEvent.L_JOURNAL_MODULE+"."+TrJournalModule.L_JOURNAL)
@EppStateMutable({ TrJournalEvent.P_THEME, TrJournalEvent.L_TYPE, TrJournalEvent.P_COMMENT, TrJournalEvent.P_HAS_MARKS })
public abstract class TrJournalEvent extends TrJournalEventGen implements ITitled, IEppStateObject
{
    public abstract TrJournalEvent getClone();

    public abstract String getShortTitle();

    public abstract String getAbbreviationTitle();

    @Override
    public EppState getState() {
        return getJournalModule().getJournal().getState();
    }

    @Override
    public boolean isAllowEditSettings()
    {
        return true;
    }
}