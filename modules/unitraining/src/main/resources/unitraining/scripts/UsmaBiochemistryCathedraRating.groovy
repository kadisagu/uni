/* $Id$ */
package unitraining.scripts
import com.google.common.base.Preconditions
import com.google.common.collect.*
import org.tandemframework.core.info.ErrorCollector
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.rtf.util.RtfString
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent
import ru.tandemservice.unitraining.base.entity.journal.*
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByLevel.IBrsGlobalScript
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.DefaultSessionPointsValidator
import ru.tandemservice.unitraining.brs.dao.IBrsDao
import ru.tandemservice.unitraining.brs.entity.catalogs.IBrsCoefficientDef
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef

import javax.validation.constraints.NotNull

import static ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes.*
import static ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes.*
import static ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils.OwnerType.EVENT
import static ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils.OwnerType.JOURNAL
/**
 * Настройка рейтинга для кафедры биохимии (УГМА)
 *
 * @author Nikolay Fedorovskih
 * @since 08.09.2014
 */
return new IBrsGlobalScript() {

    @Override
    boolean validateOnJournalAcceptance(ErrorCollector errors, Map<TrJournalModule, List<TrJournalEvent>> journalContent, IBrsDao.IBrsPreparedSettings brsSettings)
    {
        return true
    }

    boolean checkMark(Double grade)
    {
        if (grade - Math.floor(grade) != 0d) return false
        for (char c in String.valueOf(grade.intValue()).toCharArray())
        {
            if (c < ('1' as char) || c > ('5' as char)) return false
        }
        return true
    }

    @Override
    boolean validateMark(ErrorCollector errors, Student student, TrEduGroupEvent event, ITrJournalEventDao.IJournalMarkData mark, IBrsDao.IBrsPreparedSettings brsSettings)
    {
        if (mark?.grade == null)
            return true

        if (event.journalEvent instanceof TrEventLoad && !(event.journalEvent.type.code in [TYPE_LABS, TYPE_PRACTICE]))
        {
            errors.add('Для событий по аудиторной нагрузке выставлять балл можно только для практических и лабораторных занятий.')
            return false
        }

        if (!checkMark(mark.grade))
        {
            errors.add('Балл может содержащее только цифры от 1 до 5.')
            return false
        }

        return true
    }

    @Override
    List<TrBrsCoefficientDef> filterCoefficientDefListForEvent(TrJournalEvent event, List<TrBrsCoefficientDef> defList)
    {
        final excludeCoefCodes = new HashSet<String>()

        // Флаг "Мин. балл" нужен только для событий пром. контроля
        if (!(event instanceof TrEventAction))
            excludeCoefCodes.addAll(['min_points'])

        // Флаг "Учитывать при расчете текущего рейтинга" нужен только для доп. мероприятий
        if (!(event instanceof TrEventAddon))
            excludeCoefCodes.add('include_in_current_rating')

        if (!excludeCoefCodes.empty)
            defList.removeAll { coef -> excludeCoefCodes.contains(coef.userCode) }

        return defList
    }

    @Override
    List<IBrsCoefficientDef> getCoefficientDefList()
    {
        [BrsScriptUtils.coeffDefinition('allowance_threshold', JOURNAL, 'Порог на допуск'),
         BrsScriptUtils.coeffDefinition('min_points', EVENT, 'Мин. балл'),
         BrsScriptUtils.coeffDefinition('include_in_current_rating', EVENT, 'Учитывать при расчете текущего рейтинга', true, true),
         BrsScriptUtils.coeffDefinition('setoff_threshold', JOURNAL, 'Порог на зачет'),
         BrsScriptUtils.coeffDefinition('attestation_threshold', JOURNAL, 'Порог аттестации')]
    }

    @Override
    Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions()
    {
        [prevGrade: BrsScriptUtils.additParamDef('prevGrade', 'Балл за пред. семестр', 'БзПС', 'Балл за предыдущий семестр.'),
         yearGrade: BrsScriptUtils.additParamDef('yearGrade', 'Балл за год', 'БзГ', 'Балл за год.')]
    }

    @Override
    List<String> getDisplayableAdditParams()
    {
        [/*'prevGrade', */ 'yearGrade']
    }

    public static class RatingData
    {
        boolean calculated // Посчитан ли уже текущий рейтинг или нет. Для lazy-инициализации расчета
        Double currentRating // Текущий рейтинг
        double prevTermGrade // Сумма баллов за предыдущие семестры (или просто балл за пред. семетр)
        double anyAdditionalEventGrade // Сумма баллов за дополнительные события, кроме доп.события с польз. кодом previous_term_rating
    }

    public static class StudentRatingData
    {
        // Есть ли данные по баллу за пред. семестр
        boolean prevTermGradeAllowed = false
        // Рейтинг и доп. параметры
        final Map<EppRegistryModule, RatingData> data = SafeMap.get(RatingData.class)
        // Допуск к сесии - условие "для всех событий по пром. контролю, где задан min_points, студент получил баллов >= min_points"
        boolean sessionAllowed = false

        // статистика по посещениям и оценкам
        final Map<EppRegistryModule, Multiset<String>> stat = SafeMap.get(new SafeMap.Callback<EppRegistryModule, Multiset<String>>() {
            @Override
            Multiset<String> resolve(EppRegistryModule key)
            {
                return HashMultiset.create()
            }
        })

        /**
         * Добавление статистики по событию и оценке (если указана).
         * @param module модуль. Обязательно должен быть указан.
         * @param eventCode код события
         * @param mark оценка (3, 4 или 5)
         * @param count количество упоминаний
         */
        void addStat(@NotNull EppRegistryModule module, String eventCode, String mark, int count)
        {
            Preconditions.checkNotNull(module)

            // Ключом для суммирования количества оценок по событию является строка в виде кода события + оценка (3, 4 или 5)
            // Если оценка не указана, то это посещение по данному типу события и ключом является просто код события
            this.stat.get(module).add((mark ?: '') + eventCode, count)
        }

        /**
         * Добавление оценки за доп. мероприятие
         *
         * @param module модуль. Обязательно должен быть указан
         * @prevTermParam это доп. мероприятие с кодом previous_term_rating или нет
         * @param grade оценка
         */
        void addAdditionalEventGrade(@NotNull EppRegistryModule module, double grade, boolean prevTermParam)
        {
            Preconditions.checkNotNull(module)
            if (prevTermParam)
            {
                this.data.get(module).prevTermGrade += grade
                prevTermGradeAllowed = true
            }
            else
            {
                this.data.get(module).anyAdditionalEventGrade += grade
            }
        }

        /**
         * Расчет текущего рейтинга студента. Действует lazy-инициализация (т.е. будет рассчитан в момент первого обращения).
         *
         * тек. рейтинг =
         * <количество оценок «3» по e-test_control> * 3 +
         * <количество оценок «4» по e-test_control> * 5 +
         * <количество оценок «5» по e-test_control> * 7 +
         * <количество оценок «3» по mark_control> * 8 +
         * <количество оценок «4» по mark_control> * 11 +
         * <количество оценок «5» по mark_control> * 14 +
         * <количество посещений лекций> * 0,4 +
         * <количество посещений практик> * 0,2 +
         * <количество оценок «3» по практикам> * 0,4 +
         * <количество оценок «4» по практикам> * 0,7 +
         * <количество оценок «5» по практикам > * 1 +
         * баллы по доп.событиям, кроме доп.события с польз. кодом previous_term_rating!!
         *
         * @param module Модуль. Если null - считается рейтинг по всему журналу (всем модулям)
         * @return тек. рейтинг
         */
        Double getCurrentRating(EppRegistryModule module)
        {
            // Если модуль всего один, то рейтинг в нем равен рейтингу по журналу
            final RatingData ratingData = this.data.size() == 1 ? this.data.values().iterator().next() : this.data.get(module)

            // Если рейтинг для студента уже считался, то сразу его везвращаем
            if (ratingData.calculated)
                return ratingData.currentRating

            // Рейтинг ещё не считался. Чтобы его посчитать, нужно получить статистику по журналу
            final Multiset<String> moduleStat
            if (module == null && !this.data.containsKey(null))
            {
                // Если запрошен рейтинг для журнала, но статистика по журналу еще не суммировалась - суммируем её (опять же lazy-иницализация)
                moduleStat = HashMultiset.create()
                this.stat.values().each { moduleStat.addAll(it) }
                this.stat.put(null, moduleStat)
            }
            else
            {
                moduleStat = this.stat.get(module)
            }

            if (moduleStat.isEmpty())
            {
                ratingData.currentRating = null
            }
            else
            {
                ratingData.currentRating = moduleStat.count('3' + 'e-test_control') * 3 +  // <количество оценок «3» по e-test_control> * 3 +
                        moduleStat.count('4' + 'e-test_control') * 5 + // <количество оценок «4» по e-test_control> * 5 +
                        moduleStat.count('5' + 'e-test_control') * 7 + // <количество оценок «5» по e-test_control> * 7 +
                        moduleStat.count('3' + 'mark_control') * 8 +   // <количество оценок «3» по mark_control> * 8 +
                        moduleStat.count('4' + 'mark_control') * 11 +  // <количество оценок «4» по mark_control> * 11 +
                        moduleStat.count('5' + 'mark_control') * 14 +  // <количество оценок «5» по mark_control> * 14 +
                        moduleStat.count(TrEventLoad.ENTITY_NAME + TYPE_LECTURES) * 0.4 +        // <количество посещений лекций> * 0,4 +
                        moduleStat.count(TrEventLoad.ENTITY_NAME + TYPE_PRACTICE) * 0.2 +        // <количество посещений практик> * 0,2 +
                        moduleStat.count('3' + TrEventLoad.ENTITY_NAME + TYPE_PRACTICE) * 0.4 +  // <количество оценок «3» по практикам> * 0,4 +
                        moduleStat.count('4' + TrEventLoad.ENTITY_NAME + TYPE_PRACTICE) * 0.7 +  // <количество оценок «4» по практикам> * 0,7 +
                        moduleStat.count('5' + TrEventLoad.ENTITY_NAME + TYPE_PRACTICE) +        // <количество оценок «5» по практикам> * 1 +
                        getAdditionalGradeSum(module, false)           // баллы по доп.событиям, кроме доп.события с польз. кодом previous_term_rating!!
            }

            ratingData.calculated = true
            return ratingData.currentRating
        }

        /**
         * Получение суммы баллов доп. мероприятий.
         *
         * @param module модуль. Если не указан (null), считается по всему журналу (всем модулям)
         * @prevTermParam true - сумма только по доп. мероприятиям с кодом previous_term_rating. false - по всем доп. мероприятиям, кроме previous_term_rating.
         * @return сумма балло доп. мероприятий
         */
        Double getAdditionalGradeSum(EppRegistryModule module, boolean prevTermParam)
        {
            if (prevTermParam && !prevTermGradeAllowed)
                return null

            // Если модуль всего один, то сразу везвращаем данные из него - это значение будет таким же и для журнала вцелом
            RatingData ratingData
            if (this.data.size() == 1)
            {
                ratingData = this.data.values().iterator().next()
            }
            else
            {
                // lazy-инициализация - если запрошен балл для журнала, и он ещё не считался - считаем суммарный балл для журнала
                if (module == null && !this.data.containsKey(null))
                {
                    double sum = 0d
                    // суммируем баллы по всем модулям
                    if (prevTermParam)
                    {
                        this.data.values().each { sum += it.prevTermGrade }
                        this.data.get(null).prevTermGrade = sum
                    }
                    else
                    {
                        this.data.values().each { sum += it.anyAdditionalEventGrade }
                        this.data.get(null).anyAdditionalEventGrade = sum
                    }
                    return sum
                }

                ratingData = this.data.get(module)
            }

            return prevTermParam ? ratingData.prevTermGrade : ratingData.anyAdditionalEventGrade
        }

        /**
         * Получение доп. показателя "Балл за предыдущий семестр".  Равен сумме баллов за доп. события с кодом previous_term_rating.
         *
         * @param module модуль. Если не указан (null), считается по всему журналу (всем модулям)
         * @return балл за пред. семест
         */
        Double getPrevTermGrade(EppRegistryModule module)
        {
            return prevTermGradeAllowed ? getAdditionalGradeSum(module, true) : null
        }

        /**
         * Получение доп. показателя "Балл за год". Равен (балл за пред. семестры> + <текущий рейтинг> / 2)
         *
         * @param module модуль. Если не указан (null), считается по всему журналу (всем модулям)
         * @return балл за год
         */
        Double getYearGrade(EppRegistryModule module)
        {
            if (!prevTermGradeAllowed)
                return null

            Double currentRating = getCurrentRating(module)
            return currentRating != null ? ((currentRating + (getPrevTermGrade(module) ?: 0d)) * 0.5d) : null
        }
    }

    @Override
    IBrsDao.ICurrentRatingCalc calculateCurrentRating(TrJournal journal, Map<TrJournalModule, List<TrJournalEvent>> journalContent, Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> markMap,
                                                      final IBrsDao.IBrsPreparedSettings preparedBrsSettings)
    {
        String formula = "<p>Для студента считаем:<ul>" +
                "<li>количество оценок «3», «4» и «5» по форме контроля e-test_control</li>" +
                "<li>количество оценок «3», «4» и «5» по форме контроля mark_control</li>" +
                "<li>количество оценок «3», «4» и «5» по ауд. нагрузке (по практике)</li>" +
                "<li>количество посещений лекций и количество посещений практик</li>" +
                "</ul></p>" +
                "<p>Количество оценок, например «3» — это количество цифр «3» в баллах, выставленных студенту за соответствующие события.</p>" +
                "<p>Тек. рейтинг = [количество оценок «3» по e-test_control] *3 + [количество оценок «4» по e-test_control]*5 + " +
                "[количество оценок «5» по e-test_control]*7 + [количество оценок «3» по mark_control]*8 + " +
                "[количество оценок «4» по mark_control]*11 + [количество оценок «5» по mark_control]*14 + " +
                "[количество посещений лекций]*0,4 + " + "[количество посещений практик]*0,2 + [количество оценок «3» по практикам]*0,4 + " +
                "[количество оценок «4» по практикам]*0,7 + [количество оценок «5» по практикам]*1 + баллы по доп.событиям, кроме доп.события с польз. кодом previous_term_rating.</p>"

        /* было в задаче зачем-то.
        // вычисляем общее число лекций и общее число практик в журнале
        int numberOfLectures = 0
        int numberOfPractices = 0
        for (module in journalContent)
        {
            for (event in module.value)
            {
                switch (event.type.code)
                {
                    case TYPE_PRACTICE: numberOfPractices++
                        break
                    case TYPE_LECTURES: numberOfLectures++
                        break
                }
            }
        }
        */

        Double allowanceThreshold = preparedBrsSettings.getSettings('allowance_threshold')?.valueAsDouble

        final Multimap<TrJournalModule, TrJournalEvent> eventMap = ArrayListMultimap.create()
        final Map<TrJournalEvent, Double> minPoints = new HashMap<>()
        for (entry in journalContent)
        {
            // Фильтруем доп. события по признаку "include_in_current_rating"
            // Фильтруем формы контроля по коду "e-test_control" и "mark_control"
            // Фильтруем ауд. нагрузку - нужны только лекции и практики
            // Остальные события в расчете рейтинга не участвуют
            eventMap.putAll(entry.key, entry.value.findAll { event ->
                ((event instanceof TrEventLoad) && (event.type.code in [TYPE_LECTURES, TYPE_PRACTICE])) ||
                        ((event instanceof TrEventAction) && ((event as TrEventAction).actionType.userCode in ['e-test_control', 'mark_control'])) ||
                        ((event instanceof TrEventAddon) && preparedBrsSettings.getEventSettings(event, 'include_in_current_rating')?.valueAsBoolean)
            })

            for (event in entry.value)
            {
                if (!(event instanceof TrEventAction)) continue

                final minPoint = preparedBrsSettings.getEventSettings(event, 'min_points')?.valueAsDouble
                if (minPoint != null)
                {
                    minPoints.put(event, minPoint)
                }
            }
        }

        // для студента считаем:
        // количество оценок «3», «4» и «5» по форме контроля e-test_control
        // количество оценок «3», «4» и «5» по форме контроля mark_control
        // количество оценок «3», «4» и «5» по ауд. нагрузке (по практике)
        // количество посещений лекций и количество посещений практик
        //
        // Количество оценок, например «3» — это количество цифр «3» в баллах, выставленных студенту за соответствующие события (ппц)

        final Map<EppStudentWorkPlanElement, StudentRatingData> ratingMap = Maps.newHashMapWithExpectedSize(markMap.size())
        for (student in markMap)
        {
            final data = new StudentRatingData()
            ratingMap.put(student.key, data)

            // Считаем статистику по модулям
            for (moduleEntry in eventMap.asMap())
            {
                final eppModule = moduleEntry.key.module.module
                for (event in moduleEntry.value)
                {
                    final mark = student.value.get(event)
                    if (!mark) continue

                    final eventCode
                    if (event instanceof TrEventLoad)
                    {
                        eventCode = TrEventLoad.ENTITY_NAME + event.type.code // приписываем название сущности, чтобы не путать с другими видами нагрузки
                        if (!mark.isAbsent())
                            data.addStat(eppModule, eventCode, null, 1) // Прибавляем посещение лекций или практик
                    }
                    else if (event instanceof TrEventAction)
                    {
                        eventCode = (event as TrEventAction).actionType.userCode
                    }
                    else if (event instanceof TrEventAddon)
                    {
                        eventCode = (event as TrEventAddon).addonType.userCode
                    }
                    else
                    {
                        throw new IllegalStateException()
                    }

                    final grade = mark.grade
                    if (!grade) continue

                    if (event instanceof TrEventAddon)
                    {
                        // Суммируем баллы по все доп. событиям, кроме событий с кодом previous_term_rating - их суммируем отдельно
                        data.addAdditionalEventGrade(eppModule, grade, eventCode == 'previous_term_rating')
                    }
                    else
                    {
                        // Считаем количество цифр (sic!) 3, 4, 5 в оценке
                        int c3 = 0, c4 = 0, c5 = 0
                        for (c in String.valueOf(grade.intValue()).toCharArray())
                        {
                            switch (c)
                            {
                                case '3': c3++; break
                                case '4': c4++; break
                                case '5': c5++; break
                            }
                        }
                        if (c3 > 0) data.addStat(eppModule, eventCode, '3', c3)
                        if (c4 > 0) data.addStat(eppModule, eventCode, '4', c4)
                        if (c5 > 0) data.addStat(eppModule, eventCode, '5', c5)
                    }
                }
            }

            // Второе условие допуска к сессии - сдача всех мероприятий с мин. баллом
            def sessionAllowed = true
            for (event in minPoints)
            {
                final mark = student.value.get(event.key)?.grade
                if (mark == null || mark < event.value)
                {
                    sessionAllowed = false
                    break
                }
            }
            data.sessionAllowed = sessionAllowed
        }
        return new IBrsDao.ICurrentRatingCalc() {
            @Override
            TrJournal getJournal()
            { return journal }

            @Override
            public boolean isModuleRatingCalculated()
            { return true }

            @Override
            String getDisplayableRatingFormula()
            { return formula }

            @Override
            public IBrsDao.IStudentCurrentRatingData getCurrentRating(EppStudentWorkPlanElement student)
            {
                return new IBrsDao.IStudentCurrentRatingData() {
                    @Override
                    public ISessionBrsDao.IRatingValue getModuleRating(EppRegistryModule module)
                    {
                        return BrsScriptUtils.ratingValue(ratingMap.get(student)?.getCurrentRating(module), null)
                    }

                    @Override
                    public ISessionBrsDao.IRatingValue getRatingValue()
                    {
                        return getModuleRating(null)
                    }

                    @Override
                    public boolean isAllowed()
                    {
                        def rating = getRatingValue()?.value
                        return rating != null && (allowanceThreshold == null || rating >= allowanceThreshold)
                    }

                    @Override
                    public ISessionBrsDao.IRatingValue getRatingAdditParam(String key)
                    {
                        def rating = ratingMap.get(student)
                        switch (key)
                        {
                            case 'prevGrade': return BrsScriptUtils.ratingValue(rating.getPrevTermGrade(null), null)
                            case 'yearGrade': return BrsScriptUtils.ratingValue(rating.getYearGrade(null), null)
                            case 'sessionAllowed': return rating.sessionAllowed ? BrsScriptUtils.ratingValue(1d, null) : null
                            default: return null
                        }
                    }
                }
            }
        }
    }

    @Override
    IBrsDao.IStudentAttestationData calculateAttestation(SessionAttestationSlot slot, IBrsDao.IStudentCurrentRatingData ratingCalc, IBrsDao.IBrsPreparedSettings preparedBrsSettings)
    {
        return new IBrsDao.IStudentAttestationData() {
            @Override
            Boolean passed()
            {
                // критерий прохождения аттестации: тек. рейтинг >= attestation_threshold
                def rating = ratingCalc.ratingValue
                if (rating.value != null)
                {
                    Double threshold = preparedBrsSettings.getSettings('attestation_threshold')?.valueAsDouble
                    return threshold != null && rating.value >= threshold
                }
                return false
            }

            @Override
            SessionAttestationSlotAdditionalData getAdditionalData()
            { return null }
        }
    }

    @Override
    RtfString getPrintableRatingFormula(EppGradeScale gradeScale, EppFControlActionGroup caGroup)
    {
        if (gradeScale == null || !(gradeScale.code in [EppGradeScaleCodes.SCALE_2, EppGradeScaleCodes.SCALE_5]))
            return new RtfString().append('Для используемой в мероприятии шкалы оценок не реализован расчет оценки по БРС.')

        def info = new RtfString()
        if (gradeScale.code == EppGradeScaleCodes.SCALE_2)
        {
            info.append('Зачет, если рейт. эквивалент >= порога на зачет (setoff_threshold). Рейт. эквивалент = тек. рейтинг.')
        }
        else
        {
            info.append('Рейт. эквивалент = (значение показателя «Балл за год» + балл за сессию) / 2.').par()
                    .append('Традиционная оценка вычисляется по рейт. эквиваленту, по таблице перевода:').par()
                    .append('«Неудовлетворительно»: 0 - 49 баллов;').par()
                    .append('«Удовлетворительно»: 50 - 69 баллов;').par()
                    .append('«Хорошо»: 70 - 84 баллов;').par()
                    .append('«Отлично»: >= 85 баллов.')
        }
        return info
    }

    @Override
    ISessionBrsDao.ISessionRatingSettings getRatingSettings(final ISessionBrsDao.ISessionRatingSettingsKey key, IBrsDao.IBrsPreparedSettings brsSettings, boolean useRating)
    {
        final regElType = key.discipline.registryElement.class

        return new ISessionBrsDao.ISessionRatingSettings() {
            @Override
            ISessionBrsDao.ISessionRatingSettingsKey key()
            { return key }

            @Override
            public boolean usePoints()
            { return useRating && (EppRegistryDiscipline.isAssignableFrom(regElType)) }

            @Override
            public boolean useCurrentRating()
            { return useRating && (EppRegistryDiscipline.isAssignableFrom(regElType)) && !EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_PROJECT.equals(key.getCaGroup().getCode()) }

            @Override
            public ISessionBrsDao.ISessionPointsValidator pointsValidator()
            { return new DefaultSessionPointsValidator(0, 100, 0) }

            @Override
            public ISessionBrsDao.IBrsCoefficientValue getCoefficientValue(String defCode)
            { return brsSettings.getSettings(defCode) }
        }
    }

    @Override
    IBrsDao.IStudentSessionMarkData calculateSessionMark(ISessionBrsDao.IStudentCurrentRatingData ratingData, final Double pointsForSessionCA, ISessionBrsDao.ISessionRatingSettings ratingSettings)
    {
        final gradeScale = ratingSettings.key().scale
        final currentRating = ratingData?.ratingValue?.value
        final message = ratingSettings.useCurrentRating() ? ratingData?.ratingValue?.message : null
        final exam = gradeScale.code == EppGradeScaleCodes.SCALE_5
        final setoff = !exam && gradeScale.code == EppGradeScaleCodes.SCALE_2
        final setoffThreshold = (setoff ? ratingSettings.getCoefficientValue('setoff_threshold')?.valueAsDouble : null) as Double
        final allowanceThreshold = ratingSettings.getCoefficientValue('allowance_threshold')?.valueAsDouble
        final yearGrade = exam ? ratingData.getRatingAdditParam('yearGrade')?.value : null
        final prevTermGrade = (exam && yearGrade != null) ? ratingData.getRatingAdditParam('prevGrade')?.value : null
        def rating = null as Double

        if (ratingSettings.useCurrentRating() && currentRating != null && pointsForSessionCA != null)
        {
            if (setoff)
            {
                // рейт. эквивалент = тек. рейтинг
                rating = currentRating
            }
            else if (exam)
            {
                // рейт. эквивалент = (значение показателя «Балл за год» + балл за сессию)/2
                rating = yearGrade != null ? ((yearGrade + pointsForSessionCA) * 0.5d) : null
            }
        }
        else if (!ratingSettings.useCurrentRating())
            rating = pointsForSessionCA

        return new IBrsDao.IStudentSessionMarkData() {
            @Override
            public SessionMarkGradeValueCatalogItem getSessionMark()
            {
                if (null == rating || rating < 0)
                    return null

                if (setoff)
                {
                    // если рейт. экв. >= setoff_threshold и это зачет , то студент получает «зачет»
                    return setoffThreshold != null ? BrsScriptUtils.getSessionMark(rating >= setoffThreshold ? ZACHTENO : NE_ZACHTENO) : null
                }
                else if (exam)
                {
                    final String markCode

                    // «Неудовлетворительно»: 0 - 49 баллов;
                    // «Удовлетворительно»: 50 - 69 баллов;
                    // «Хорошо»: 70 - 84 баллов;
                    // «Отлично»: >= 85 баллов
                    if (rating < 50.0) markCode = NEUDOVLETVORITELNO
                    else if (rating < 70.0) markCode = UDOVLETVORITELNO
                    else if (rating < 85.0) markCode = HOROSHO
                    else markCode = OTLICHNO

                    return BrsScriptUtils.getSessionMark(markCode)
                }
                return null
            }

            @Override
            public Double getRatingMissingForMark(SessionMarkGradeValueCatalogItem mark)
            {
                if (null == rating || !gradeScale.equals(mark.scale))
                    return null

                final double threshold
                switch (mark.code)
                {
                    case ZACHTENO: return Math.max(0, setoffThreshold - rating)
                    case OTLICHNO: threshold = 85d; break
                    case HOROSHO: threshold = 70d; break
                    case UDOVLETVORITELNO: threshold = 50d; break
                    default: return null
                }

                // Каким должен быть тек. рейтинг, чтобы хватило до нужной оценки - разворачиваем формулу расчета оценки в обратную сторону:
                // Считаем тек. рейтинг, которого хватит для получения рейтингового коэффициента на нужную оценку = ((threshold * 4) - prevTermGrade - pointsForSessionCA * 2)
                // и вычитаем из этого реальный тек. рейтинг.
                return Math.max(0, ((threshold * 4) - prevTermGrade - pointsForSessionCA * 2) - currentRating)
            }

            @Override
            public ISessionBrsDao.IRatingValue getRatingValue()
            {
                return BrsScriptUtils.ratingValue(rating, message)
            }

            @Override
            public ISessionBrsDao.IRatingValue getCurrentRatingValue()
            {
                return BrsScriptUtils.ratingValue(currentRating, message)
            }

            @Override
            ISessionBrsDao.IRatingValue getRatingAdditParam(String key)
            {
                return ratingData?.getRatingAdditParam(key)
            }

            @Override
            public Boolean isAllowed()
            {
                // допуск: выполняются два условия
                // 1) тек. рейтинг >= allowance_threshold
                // 2) для всех событий по пром. контролю, где задан min_points, студент получил баллов >= min_points
                return ratingData?.allowed && allowanceThreshold && (currentRating >= allowanceThreshold) && ratingData.getRatingAdditParam('sessionAllowed')
            }
        }
    }
}