package unitraining.scripts
import com.google.common.collect.ImmutableMap
import org.tandemframework.core.info.ErrorCollector
import org.tandemframework.rtf.util.RtfString
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent
import ru.tandemservice.unitraining.base.entity.journal.TrEventLoad
import ru.tandemservice.unitraining.base.entity.journal.TrJournal
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByLevel.IBrsGlobalScript
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.DefaultSessionPointsValidator
import ru.tandemservice.unitraining.brs.dao.IBrsDao
import ru.tandemservice.unitraining.brs.entity.catalogs.IBrsCoefficientDef
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef

return new IBrsGlobalScript() {
    @Override
    boolean validateOnJournalAcceptance(ErrorCollector errors, Map<TrJournalModule, List<TrJournalEvent>> journalContent, IBrsDao.IBrsPreparedSettings brsSettings) {
        return true
    }

    @Override
    boolean validateMark(ErrorCollector errors, Student student, TrEduGroupEvent event, ITrJournalEventDao.IJournalMarkData mark, IBrsDao.IBrsPreparedSettings brsSettings)
    {
        def checkMin = brsSettings.getEventSettings(event.getJournalEvent(), "min_points")
        def checkMax = brsSettings.getEventSettings(event.getJournalEvent(), "max_points")
        if (null != checkMin && mark.grade < checkMin.valueAsDouble) {
            errors.add("Балл, выставленный студенту, не может быть меньше установленного для данного события: " + checkMin.valueStr);
            return false;
        }
        if (null != checkMax && mark.grade > checkMax.valueAsDouble) {
            errors.add("Балл, выставленный студенту, не может превышать максимальный, установленный для данного события: " + checkMax.valueStr);
            return false;
        }
        return true
    }
    @Override
    List<TrBrsCoefficientDef> filterCoefficientDefListForEvent(TrJournalEvent event, List<TrBrsCoefficientDef> defList) {
        return defList
    }

    @Override
    List<IBrsCoefficientDef> getCoefficientDefList() {
        return Arrays.asList(
                BrsScriptUtils.coeffDefinition("allowance_threshold", BrsScriptUtils.OwnerType.JOURNAL, "Порог на допуск"),
                BrsScriptUtils.coeffDefinition("max_points", BrsScriptUtils.OwnerType.EVENT, "Макс. балл"),
                BrsScriptUtils.coeffDefinition("min_points", BrsScriptUtils.OwnerType.EVENT, "Мин. балл"),
        );
    }

    @Override
  Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions() {
    return ImmutableMap.of(
            "absence", BrsScriptUtils.additParamDef("absence", "Пропуски", "н", "Число пропусков студента по всем занятиям по реализации."))
  }

  @Override
  List<String> getDisplayableAdditParams() {
    return Collections.singletonList("absence");
  }

    @Override
    IBrsDao.ICurrentRatingCalc calculateCurrentRating(TrJournal journal, Map<TrJournalModule, List<TrJournalEvent>> journalContent, Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> markMap, IBrsDao.IBrsPreparedSettings preparedBrsSettings)
    {
        String formula = "<div>Расчет текущего рейтинга:</div>" +
                "<div>рейтинг студента = сумма баллов студента</div>" +
                "<div>рейтинг студента по модулю = сумма баллов студента по данному модулю</div>";

        double allowance_threshold = 0;

        def allowanceThresholdSettings = preparedBrsSettings.getSettings("allowance_threshold")
        if (null != allowanceThresholdSettings)
            allowance_threshold = allowanceThresholdSettings.getValueAsDouble();

        Map<EppStudentWorkPlanElement, Map<EppRegistryModule, Double>> ratingMap = new HashMap<EppStudentWorkPlanElement, Map<EppRegistryModule, Double>>();
        Map<EppStudentWorkPlanElement, Double> absenceMap = new HashMap<EppStudentWorkPlanElement, Double>();
        Map<EppStudentWorkPlanElement, Boolean> allowedMap = new HashMap<EppStudentWorkPlanElement, Boolean>();

        for (EppStudentWorkPlanElement student : markMap.keySet()) {
            Map<EppRegistryModule, Double> studentMap = new HashMap<EppRegistryModule, Double>()
            studentMap.put(null, 0);
            ratingMap.put(student, studentMap);
            absenceMap.put(student, 0);
            for (TrJournalModule module : journalContent.keySet())
                studentMap.put(module.getModule().getModule(), 0);
        }

        for (Map.Entry<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> studentEntry : markMap.entrySet()) {

            EppStudentWorkPlanElement student = studentEntry.getKey();
            Map<EppRegistryModule, Double> studentMap = ratingMap.get(student);

            double total_points = 0;
            boolean allowed = true;

            for (Map.Entry<TrJournalEvent, ITrJournalEventDao.IJournalMarkData> markEntry : studentEntry.value.entrySet()) {

                def event = markEntry.key
                def value = markEntry.value.grade == null ? 0 : markEntry.value.grade
                double points = event.type.code.equals("entranceСontrol") ? 0 : value
                def module = event.getJournalModule().getModule().getModule()

                studentMap.put(null, studentMap.get(null) + points)
                studentMap.put(module, studentMap.get(module) + points)

                if (event instanceof TrEventLoad && markEntry.value.isAbsent())
                    absenceMap.put(student, absenceMap.get(student) + 1);

                if (!event.type.code.equals("entranceСontrol"))
                    total_points = total_points + points

                def minPointsSettings = preparedBrsSettings.getEventSettings(event, "min_points")
                if (event instanceof TrEventLoad && EppGroupTypeALTCodes.TYPE_LABS.equals(event.type.code) && null != minPointsSettings && minPointsSettings.valueAsDouble > points)
                    allowed = false;
            }

            allowed = allowed && total_points >= allowance_threshold;

            allowedMap.put(student, allowed);
        }

        return new IBrsDao.ICurrentRatingCalc() {
            @Override TrJournal getJournal() { return journal }
            @Override public boolean isModuleRatingCalculated() { return true; }
            @Override String getDisplayableRatingFormula() { return formula; }
            @Override public IBrsDao.IStudentCurrentRatingData getCurrentRating(EppStudentWorkPlanElement student) {
                return new IBrsDao.IStudentCurrentRatingData() {
                    @Override public ISessionBrsDao.IRatingValue getModuleRating(EppRegistryModule module) {
                        def rating = ratingMap.get(student)?.get(module)
                        return BrsScriptUtils.ratingValue(rating, null);
                    }
                    @Override public ISessionBrsDao.IRatingValue getRatingValue() {
                        def rating = ratingMap.get(student)?.get(null)
                        return BrsScriptUtils.ratingValue(rating == null ? 0d : Math.min(rating, 100d), null);
                    }
                    @Override public boolean isAllowed() {
                        return allowedMap.get(student)
                    }
                    @Override public ISessionBrsDao.IRatingValue getRatingAdditParam(String key) { return "absence".equals(key) ? BrsScriptUtils.ratingValue(absenceMap.get(student), null) : null ; }
                };
            }
        }
    }

    @Override
    IBrsDao.IStudentAttestationData calculateAttestation(SessionAttestationSlot slot, IBrsDao.IStudentCurrentRatingData ratingCalc, IBrsDao.IBrsPreparedSettings preparedBrsSettings) {
        return new IBrsDao.IStudentAttestationData() {
            @Override Boolean passed() {
                def rating = ratingCalc.getRatingValue()
                return rating.value != null && rating.value > 29;
            }
            @Override  SessionAttestationSlotAdditionalData getAdditionalData() { return null }
        }
    }

    @Override
    RtfString getPrintableRatingFormula(EppGradeScale gradeScale, EppFControlActionGroup caGroup) {
        if (gradeScale == null || !(gradeScale.getCode().equals(EppGradeScaleCodes.SCALE_2) || gradeScale.getCode().equals(EppGradeScaleCodes.SCALE_5)))
            return new RtfString().append("Для используемой в мероприятии шкалы оценок не реализован расчет оценки по БРС.")
        def info = new RtfString()
                .append("Рейтинг студента для суммарной оценки в сессии вычисляется по формуле:")
                .par()
                .append("Рейтинг студента за экзамен: 0,6*[тек. рейтинг студента]+0,4*[балл за сдачу мероприятия в сессии]")
                .par()
                .append("Рейтинг студента за зачет: 0,8*[тек. рейтинг студента]+0,2*[балл за сдачу мероприятия в сессии]")
                .par()
                .append("Суммарная оценка по традиционной шкале определяется по рейтингу по таблице перевода:")
                .par()
        if (gradeScale.getCode().equals(EppGradeScaleCodes.SCALE_2))
            info.append("0-49 - незачет, 50-100 - зачет");
        else
            info.append("0-49 - неуд., 50-74 - уд., 75-84 - хор., 85-100 - отл.");
        return info;
        ;
    }

    @Override
    ISessionBrsDao.ISessionRatingSettings getRatingSettings(final ISessionBrsDao.ISessionRatingSettingsKey key, IBrsDao.IBrsPreparedSettings brsSettings, boolean useRating) {
        def regElType = key.discipline.registryElement.class

        return new ISessionBrsDao.ISessionRatingSettings() {
            @Override
            ISessionBrsDao.ISessionRatingSettingsKey key() { return key }

            @Override public boolean usePoints() {
                return useRating && (EppRegistryDiscipline.isAssignableFrom(regElType));
            }

            @Override public boolean useCurrentRating() {
                return useRating && (EppRegistryDiscipline.isAssignableFrom(regElType)) && !EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_PROJECT.equals(key.getCaGroup().getCode());
            }

            @Override public ISessionBrsDao.ISessionPointsValidator pointsValidator() {
                return new DefaultSessionPointsValidator(0, 100, 0);
            }

            @Override public ISessionBrsDao.IBrsCoefficientValue getCoefficientValue(String defCode) {
                return brsSettings.getSettings(defCode);
            }
        };
    }

    @Override
    IBrsDao.IStudentSessionMarkData calculateSessionMark(ISessionBrsDao.IStudentCurrentRatingData ratingData, Double pointsForSessionCA, ISessionBrsDao.ISessionRatingSettings ratingSettings) {
        def gradeScale = ratingSettings.key().scale

        def currentRating = ratingData?.ratingValue?.value;

        def rating = null as Double
        def message = ratingSettings.useCurrentRating() ? ratingData?.ratingValue?.message : null

        if (ratingSettings.useCurrentRating() && currentRating != null && pointsForSessionCA != null) {
            if (EppGradeScaleCodes.SCALE_2.equals(gradeScale.getCode()))
                rating = 0.8 * currentRating + 0.2 * pointsForSessionCA;
            else if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()))
                rating = 0.6 * currentRating + 0.4 * pointsForSessionCA;
        } else if (!ratingSettings.useCurrentRating())
            rating = pointsForSessionCA

        if (ratingSettings.useCurrentRating() && message == null && rating != null && rating > 79) {
            if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_SETOFF.equals(gradeScale.getCode()))
                message = "Студент имеет право на сдачу зачета в форме «автомат».";
            if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(gradeScale.getCode()))
                message = "Студент имеет право на сдачу экзамена в форме «автомат».";
        }

        return new IBrsDao.IStudentSessionMarkData() {
            @Override public SessionMarkGradeValueCatalogItem getSessionMark() {
                if (null == rating)
                    return null;
                if (EppGradeScaleCodes.SCALE_2.equals(gradeScale.getCode()) && rating >= 50)
                    return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.ZACHTENO);
                if (EppGradeScaleCodes.SCALE_2.equals(gradeScale.getCode()))
                    return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO);
                if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()) && rating >= 85)
                    return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.OTLICHNO);
                if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()) && rating >= 70)
                    return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.HOROSHO);
                if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()) && rating >= 50)
                    return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO);
                if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()))
                    return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO);
                return null;
            }

            @Override public ISessionBrsDao.IRatingValue getRatingValue() { return BrsScriptUtils.ratingValue(rating, message); }
            @Override public ISessionBrsDao.IRatingValue getCurrentRatingValue() { return BrsScriptUtils.ratingValue(currentRating, message); }
            @Override ISessionBrsDao.IRatingValue getRatingAdditParam(String key) { return ratingData?.getRatingAdditParam(key); }
            @Override public Boolean isAllowed() { return ratingData == null ? false : ratingData.allowed; }

            @Override public Double getRatingMissingForMark(SessionMarkGradeValueCatalogItem mark) {
                if (!gradeScale.equals(mark.getScale()))
                    return null;
                if (null == rating)
                    return null;
                if (SessionMarkGradeValueCatalogItemCodes.ZACHTENO.equals(mark.getCode()))
                    return Math.max(0, Math.ceil((50 - rating) / 0.2));
                if (SessionMarkGradeValueCatalogItemCodes.OTLICHNO.equals(mark.getCode()))
                    return Math.max(0, Math.ceil((85 - rating) / 0.4));
                if (SessionMarkGradeValueCatalogItemCodes.HOROSHO.equals(mark.getCode()))
                    return Math.max(0, Math.ceil((70 - rating) / 0.4));
                if (SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO.equals(mark.getCode()))
                    return Math.max(0, Math.ceil((50 - rating) / 0.4));
                return null;
            }
        };
    }
}