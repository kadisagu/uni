package unitraining.scripts

import com.google.common.collect.ImmutableList
import com.google.common.collect.ImmutableMap
import org.tandemframework.core.info.ErrorCollector
import org.tandemframework.rtf.util.RtfString
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent
import ru.tandemservice.unitraining.base.entity.journal.*
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByLevel.IBrsGlobalScript
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalEventStatScriptFunctions
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.DefaultSessionPointsValidator
import ru.tandemservice.unitraining.brs.dao.IBrsDao
import ru.tandemservice.unitraining.brs.entity.catalogs.IBrsCoefficientDef
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef

class ScriptImpl implements IBrsGlobalScript, IBrsJournalEventStatScriptFunctions {

    @Override
    boolean validateOnJournalAcceptance(ErrorCollector errors, Map<TrJournalModule, List<TrJournalEvent>> journalContent, IBrsDao.IBrsPreparedSettings brsSettings) {
        /*
         Метод для валидации значений настроек БРС перед согласованием журнала.
         Пример реализации:

         List<String> noMaxPointsTitles = new ArrayList<String>()
         for (Map.Entry<TrJournalModule, List<TrJournalEvent>> e : journalContent.entrySet()) {
           for (TrJournalEvent event : e.value) {
             if (null == brsSettings.getEventSettings(event, "max_points"))
               noMaxPointsTitles.add(String.valueOf(event.number));
           }
         }
         if (noMaxPointsTitles.empty)
           return true;
         errors.add("Согласование журнала невозможно: для событий с номерами " + noMaxPointsTitles.join(", ") + " не указан максимальный балл. Укажите максимальный балл для всех событий перед согласованием.")
         return false
        */
        return true
    }

    @Override
    boolean validateMark(ErrorCollector errors, Student student, TrEduGroupEvent event, ITrJournalEventDao.IJournalMarkData mark, IBrsDao.IBrsPreparedSettings brsSettings) {
        return true
    }

    @Override
    List<TrBrsCoefficientDef> filterCoefficientDefListForEvent(TrJournalEvent event, List<TrBrsCoefficientDef> defList) {
        return defList
    }

    @Override
    List<IBrsCoefficientDef> getCoefficientDefList() {
        return Collections.emptyList();
    }

  @Override
  Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions() {
    return ImmutableMap.of(
            "absence", BrsScriptUtils.additParamDef("absence", "Пропуски", "н", "Число пропусков студента по всем занятиям по реализации."))
  }

  @Override
  List<String> getDisplayableAdditParams() {
    return Collections.singletonList("absence");
  }

  @Override
    IBrsDao.ICurrentRatingCalc calculateCurrentRating(TrJournal journal, Map<TrJournalModule, List<TrJournalEvent>> journalContent, Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> markMap, IBrsDao.IBrsPreparedSettings preparedBrsSettings) {
        String formula = "<div>Расчет рейтинга:</div>" +
                "<div>рейтинг студента = сумма баллов студента, но не более 100</div>" +
                "<div>рейтинг студента по модулю = сумма баллов студента по событиям данного модуля, включая дополнительные графы</div>";

        Map<EppStudentWorkPlanElement, Map<EppRegistryModule, Double>> ratingMap = new HashMap<EppStudentWorkPlanElement, Map<EppRegistryModule, Double>>();
        Map<EppStudentWorkPlanElement, Double> absenceMap = new HashMap<EppStudentWorkPlanElement, Double>();
        for (EppStudentWorkPlanElement student: markMap.keySet()) {
            Map<EppRegistryModule, Double> studentMap = new HashMap<EppRegistryModule, Double>()
            studentMap.put(null, 0);
            ratingMap.put(student, studentMap);
            absenceMap.put(student, 0);
            for (TrJournalModule module: journalContent.keySet())
                studentMap.put(module.getModule().getModule(), 0);
        }

        for (Map.Entry<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> studentEntry: markMap.entrySet()) {

            EppStudentWorkPlanElement student = studentEntry.getKey();
            Map<EppRegistryModule, Double> studentMap = ratingMap.get(student);

            for (Map.Entry<TrJournalEvent, ITrJournalEventDao.IJournalMarkData> markEntry: studentEntry.value.entrySet()) {

                def event = markEntry.key
                double points = markEntry.value.grade == null ? 0 : markEntry.value.grade
                def module = event.getJournalModule().getModule().getModule()

                studentMap.put(null, studentMap.get(null) + points)
                studentMap.put(module, studentMap.get(module) + points)

                if (event instanceof TrEventLoad && markEntry.value.isAbsent())
                  absenceMap.put(student, absenceMap.get(student) + 1);
            }
        }

        return new IBrsDao.ICurrentRatingCalc() {

            @Override TrJournal getJournal() { return journal }
            @Override public boolean isModuleRatingCalculated() { return true; }
            @Override String getDisplayableRatingFormula() { return formula; }
            @Override public IBrsDao.IStudentCurrentRatingData getCurrentRating(EppStudentWorkPlanElement student) {
                  return new IBrsDao.IStudentCurrentRatingData() {
                      @Override public ISessionBrsDao.IRatingValue getModuleRating(EppRegistryModule module) { return BrsScriptUtils.ratingValue(ratingMap.get(student)?.get(module), null); }
                      @Override public ISessionBrsDao.IRatingValue getRatingValue() {
                        def rating = ratingMap.get(student)?.get(null)
                        return BrsScriptUtils.ratingValue(rating == null ? 0d : Math.min(rating, 100d), null);
                      }
                      @Override public boolean isAllowed() {
                        def rating = ratingMap.get(student)?.get(null)
                        return rating == null ? false : rating > 49;
                      }
                      @Override public ISessionBrsDao.IRatingValue getRatingAdditParam(String key) { return "absence".equals(key) ? BrsScriptUtils.ratingValue(absenceMap.get(student), null) : null ; }
                  };
                }

        }
    }

    @Override
    IBrsDao.IStudentAttestationData calculateAttestation(SessionAttestationSlot slot, IBrsDao.IStudentCurrentRatingData ratingCalc, IBrsDao.IBrsPreparedSettings preparedBrsSettings) {
        return new IBrsDao.IStudentAttestationData() {
            @Override Boolean passed() {
                def rating = ratingCalc.getRatingValue()
                return rating.value != null && rating.value > 29;
            }
            @Override  SessionAttestationSlotAdditionalData getAdditionalData() { return null }
        }
    }

    @Override
    RtfString getPrintableRatingFormula(EppGradeScale gradeScale, EppFControlActionGroup caGroup) {
        if (gradeScale == null || !(gradeScale.getCode().equals(EppGradeScaleCodes.SCALE_2) || gradeScale.getCode().equals(EppGradeScaleCodes.SCALE_5)))
            return new RtfString().append("Для используемой в мероприятии шкалы оценок не реализован расчет оценки по БРС.")
        def info = new RtfString()
                .append("Рейтинг студента для суммарной оценки в сессии вычисляется по формуле:")
                .par()
                .append("Рейтинг студента за экзамен: 0,6*[тек. рейтинг студента]+0,4*[балл за сдачу мероприятия в сессии]")
                .par()
                .append("Рейтинг студента за зачет: 0,8*[тек. рейтинг студента]+0,2*[балл за сдачу мероприятия в сессии]")
                .par()
                .append("Суммарная оценка по традиционной шкале определяется по рейтингу по таблице перевода:")
                .par()
        if (gradeScale.getCode().equals(EppGradeScaleCodes.SCALE_2))
            info.append("0-49 - незачет, 50-100 - зачет");
        else
            info.append("0-49 - неуд., 50-74 - уд., 75-84 - хор., 85-100 - отл.");
        return info;
        ;
    }

    @Override
    ISessionBrsDao.ISessionRatingSettings getRatingSettings(final ISessionBrsDao.ISessionRatingSettingsKey key, IBrsDao.IBrsPreparedSettings brsSettings, boolean useRating) {
        def regElType = key.discipline.registryElement.class

        return new ISessionBrsDao.ISessionRatingSettings() {
            @Override
            ISessionBrsDao.ISessionRatingSettingsKey key() { return key }

            @Override public boolean usePoints() {
                return useRating && (EppRegistryDiscipline.isAssignableFrom(regElType));
            }

            @Override public boolean useCurrentRating() {
                return useRating && (EppRegistryDiscipline.isAssignableFrom(regElType)) && !EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_PROJECT.equals(key.getCaGroup().getCode());
            }

            @Override public ISessionBrsDao.ISessionPointsValidator pointsValidator() {
                return new DefaultSessionPointsValidator(0, 100, 0);
            }

            @Override public ISessionBrsDao.IBrsCoefficientValue getCoefficientValue(String defCode) {
                return brsSettings.getSettings(defCode);
            }
        };
    }

    @Override
    IBrsDao.IStudentSessionMarkData calculateSessionMark(ISessionBrsDao.IStudentCurrentRatingData ratingData, Double pointsForSessionCA, ISessionBrsDao.ISessionRatingSettings ratingSettings) {
        def gradeScale = ratingSettings.key().scale

        def currentRating = ratingData?.ratingValue?.value;

        Double rating = null
        def message = ratingSettings.useCurrentRating() ? ratingData?.ratingValue?.message : null

        if (ratingSettings.useCurrentRating() && currentRating != null && pointsForSessionCA != null) {
            if (EppGradeScaleCodes.SCALE_2.equals(gradeScale.getCode()))
                rating = 0.8 * currentRating + 0.2 * pointsForSessionCA;
            else if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()))
                rating = 0.6 * currentRating + 0.4 * pointsForSessionCA;
        } else if (!ratingSettings.useCurrentRating())
            rating = pointsForSessionCA

        if (ratingSettings.useCurrentRating() && message == null && rating != null && rating > 79) {
            if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_SETOFF.equals(gradeScale.getCode()))
                message = "Студент имеет право на сдачу зачета в форме «автомат».";
            if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(gradeScale.getCode()))
                message = "Студент имеет право на сдачу экзамена в форме «автомат».";
        }

        return new IBrsDao.IStudentSessionMarkData() {
            @Override public SessionMarkGradeValueCatalogItem getSessionMark() {
                if (null == rating)
                    return null;
                if (EppGradeScaleCodes.SCALE_2.equals(gradeScale.getCode()) && rating >= 50)
                    return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.ZACHTENO);
                if (EppGradeScaleCodes.SCALE_2.equals(gradeScale.getCode()))
                    return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO);
                if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()) && rating >= 85)
                    return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.OTLICHNO);
                if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()) && rating >= 70)
                    return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.HOROSHO);
                if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()) && rating >= 50)
                    return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO);
                if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()))
                    return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO);
                return null;
            }

            @Override public ISessionBrsDao.IRatingValue getRatingValue() { return BrsScriptUtils.ratingValue(rating, message); }
            @Override public ISessionBrsDao.IRatingValue getCurrentRatingValue() { return BrsScriptUtils.ratingValue(currentRating, message); }
            @Override ISessionBrsDao.IRatingValue getRatingAdditParam(String key) { return ratingData?.getRatingAdditParam(key); }
            @Override public Boolean isAllowed() { return ratingData == null ? false : ratingData.allowed; }

            @Override public Double getRatingMissingForMark(SessionMarkGradeValueCatalogItem mark) {
                if (!gradeScale.equals(mark.getScale()))
                    return null;
                if (null == rating)
                    return null;
                if (SessionMarkGradeValueCatalogItemCodes.ZACHTENO.equals(mark.getCode()))
                    return Math.max(0, Math.ceil((50 - rating) / 0.2));
                if (SessionMarkGradeValueCatalogItemCodes.OTLICHNO.equals(mark.getCode()))
                    return Math.max(0, Math.ceil((85 - rating) / 0.4));
                if (SessionMarkGradeValueCatalogItemCodes.HOROSHO.equals(mark.getCode()))
                    return Math.max(0, Math.ceil((70 - rating) / 0.4));
                if (SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO.equals(mark.getCode()))
                    return Math.max(0, Math.ceil((50 - rating) / 0.4));
                return null;
            }
        };
    }

    private static final ISessionBrsDao.IRatingAdditParamDef ADDIT_PARAM_VALID = BrsScriptUtils.additParamDef("valid", "Аттестовано", "атт.", "Число аттестованных студентов.")
    private static final ISessionBrsDao.IRatingAdditParamDef ADDIT_PARAM_MEDIAN = BrsScriptUtils.additParamDef("median", "Медиана", "мед.", "Медианный балл.")
    private static final ISessionBrsDao.IRatingAdditParamDef ADDIT_PARAM_MEDIUM = BrsScriptUtils.additParamDef("medium", "Среднее", "ср.", "Средний балл.")

    private static final List<ISessionBrsDao.IRatingAdditParamDef> statParams = ImmutableList.of(
        ADDIT_PARAM_VALID,
        ADDIT_PARAM_MEDIAN,
        ADDIT_PARAM_MEDIUM
    )

    public static Double calcMedian(List<Double> items)
    {
        if (!items.empty)
        {
            items.sort()
            int m = items.size() / 2 // middle
            return (items.size() % 2 == 1) ? items[m] : ((items[m] + items[m - 1]) * 0.5)
        }
        return null
    }

    public static Double calcAvg(List<Double> items)
    {
        return items.size() == 0 ? null : items.sum() / items.size()
    }

    @Override
    IBrsDao.IJournalEventStatCalc calculateEventStat(TrJournal journal, TrJournalGroup group, Map<TrJournalModule, List<TrJournalEvent>> journalContent, Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> markMap, IBrsDao.IBrsPreparedSettings brsSettings)
    {
        Map<TrJournalEvent, Map<String, Double>> values = new HashMap<>();
        for (List<TrJournalEvent> events : journalContent.values()) {
            for (TrJournalEvent event : events) {
                List<Double> marks = new ArrayList<>()
                int count = 0;
                for (Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData> studentMarkMap : markMap.values()) {
                    ITrJournalEventDao.IJournalMarkData mark = studentMarkMap[event]
                    if (mark != null) {
                        if (mark.getGrade() != null || ((event instanceof TrEventLoad || !journal.hideAttendanceForControlEvents) && Boolean.FALSE.equals(mark.isAbsent()))) {
                            count ++
                        }
                        if (mark.getGrade() != null) {
                            marks.add(mark.getGrade());
                        }
                    }
                }

                values.put(event, [
                        (ADDIT_PARAM_VALID.key) : count == 0 ? null : (double)count,
                        (ADDIT_PARAM_MEDIAN.key) : calcMedian(marks),
                        (ADDIT_PARAM_MEDIUM.key) : calcAvg(marks)
                ])
            }
        }

        // Статистика по итоговым колонкам
        def ratingCalc = calculateCurrentRating(journal, journalContent, markMap, brsSettings)
        List<Double> absenceList = []
        List<Double> ratingList = []
        markMap.each {
            IBrsDao.IStudentCurrentRatingData rating = ratingCalc.getCurrentRating(it.key)
            absenceList.add(rating.getRatingAdditParam("absence")?.value)
            ratingList.add(rating.ratingValue?.value)
        }
        Map<String, Map<String, Double>> aggregatedMap = [
                "absence": [(ADDIT_PARAM_MEDIAN.key): calcMedian(absenceList), (ADDIT_PARAM_MEDIUM.key): calcAvg(absenceList)],
                (IBrsDao.RATING_COLUMN_KEY): [(ADDIT_PARAM_MEDIAN.key): calcMedian(ratingList), (ADDIT_PARAM_MEDIUM.key): calcAvg(ratingList)]
        ]

        return new IBrsDao.IJournalEventStatCalc() {
            @Override public TrJournal getJournal() { return journal; }
            @Override public TrJournalGroup getJournalGroup() { return journalGroup; }
            @Override public Set<EppStudentWorkPlanElement> getIncludedStudents() { return markMap.keySet(); }
            @Override public List<ISessionBrsDao.IRatingAdditParamDef> getStatParamDefinitions() { return statParams; }
            @Override public ISessionBrsDao.IRatingValue getStatValue(TrJournalEvent event, String statKey) { return BrsScriptUtils.ratingValue(values.get(event)?.get(statKey), null); }
            @Override public IBrsDao.IAggregatedRatingValue getStatValueAggregated(String statKey, String columnKey) {
                def value = aggregatedMap.get(columnKey)?.get(statKey)
                return new IBrsDao.IAggregatedRatingValue() {
                    @Override
                    boolean isApplicable()
                    { value != null }

                    @Override
                    String getMessage()
                    { null }

                    @Override
                    Double getValue()
                    { value }
                }
            }

        };
    }
}

return new ScriptImpl();
