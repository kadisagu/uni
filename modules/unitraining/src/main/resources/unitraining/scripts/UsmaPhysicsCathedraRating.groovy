/* $Id$ */
package unitraining.scripts

import com.google.common.collect.Maps
import com.google.common.collect.Sets
import org.tandemframework.core.info.ErrorCollector
import org.tandemframework.rtf.util.RtfString
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent
import ru.tandemservice.unitraining.base.entity.journal.*
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByLevel.IBrsGlobalScript
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.DefaultSessionPointsValidator
import ru.tandemservice.unitraining.brs.dao.IBrsDao
import ru.tandemservice.unitraining.brs.entity.catalogs.IBrsCoefficientDef
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef

import static ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes.*
import static ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes.*
import static ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils.OwnerType.EVENT
import static ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils.OwnerType.JOURNAL

/**
 * Настройка рейтинга для кафедры физики (УГМА)
 *
 * @author Nikolay Fedorovskih
 * @since 10.09.2014
 */

return new IBrsGlobalScript() {


    @Override
    boolean validateOnJournalAcceptance(ErrorCollector errors, Map<TrJournalModule, List<TrJournalEvent>> journalContent, IBrsDao.IBrsPreparedSettings brsSettings)
    { return true }

    @Override
    boolean validateMark(ErrorCollector errors, Student student, TrEduGroupEvent event, ITrJournalEventDao.IJournalMarkData mark, IBrsDao.IBrsPreparedSettings brsSettings)
    {
        if (mark?.grade == null)
            return true

        if (event.journalEvent instanceof TrEventLoad)
        {
            if (!(event.journalEvent.type.code in [TYPE_LABS, TYPE_PRACTICE]))
            {
                errors.add('Для событий по аудиторной нагрузке выставлять оценку можно только для практических и лабораторных занятий.')
                return false
            }
        }
        else if (event.journalEvent instanceof TrEventAction)
        {
            if (mark.grade > 100d)
            {
                errors.add("Оценка студента ${student.person.fio} за «${event.journalEvent.title}» превышает 100 баллов.")
                return false
            }
        }
        else
        {
            final max = brsSettings.getEventSettings(event.journalEvent, 'max_points')?.valueAsDouble
            if (max && max < mark.grade)
            {
                errors.add("Оценка студента ${student.person.fio} за «${event.journalEvent.title}» превышает максимальный балл для данного мероприятия.")
                return false
            }
        }

        return true
    }

    List<TrBrsCoefficientDef> filterCoefficientDefListForEvent(TrJournalEvent event, List<TrBrsCoefficientDef> defList)
    {
        final excludeCoefCodes = new HashSet<String>()

        if (!(event instanceof TrEventAction))
            excludeCoefCodes.addAll(['coeff_icontrol', 'min_points'])

        if (!(event instanceof TrEventAction || event instanceof TrEventAddon))
            excludeCoefCodes.add('include_in_current_rating')

        if (!(event instanceof TrEventAddon))
            excludeCoefCodes.add('max_points')

        if (!excludeCoefCodes.empty)
            defList.removeAll { coef -> excludeCoefCodes.contains(coef.userCode) }

        return defList
    }

    @Override
    List<IBrsCoefficientDef> getCoefficientDefList()
    {
        [BrsScriptUtils.coeffDefinition('allowance_threshold', JOURNAL, 'Порог на допуск'),
         BrsScriptUtils.coeffDefinition('min_points', EVENT, 'Мин. балл'),
         BrsScriptUtils.coeffDefinition('max_points', EVENT, 'Макс. балл'),
         BrsScriptUtils.coeffDefinition('coeff_icontrol', EVENT, 'Коэффициент контрольного мероприятия', false, false),
         BrsScriptUtils.coeffDefinition('lectures_attendance_control', JOURNAL, 'Учет посещения лекций', true, true),
         BrsScriptUtils.coeffDefinition('lectures_attendance_point', JOURNAL, 'Доп. балл за посещение лекций'),
         BrsScriptUtils.coeffDefinition('practices_attendance_control', JOURNAL, 'Учет посещения практик', true, true),
         BrsScriptUtils.coeffDefinition('practices_attendance_point', JOURNAL, 'Доп. балл за посещение практик'),
         BrsScriptUtils.coeffDefinition('include_in_current_rating', EVENT, 'Учитывать при расчете текущего рейтинга', true, true),
         BrsScriptUtils.coeffDefinition('setoff_threshold', JOURNAL, 'Порог на зачет'),
         BrsScriptUtils.coeffDefinition('attestation_threshold', JOURNAL, 'Порог аттестации'),
         BrsScriptUtils.coeffDefinition('coeff_credit', JOURNAL, 'Коэффициент зачета при расчете итогового рейтинга'),
         BrsScriptUtils.coeffDefinition('coeff_exam', JOURNAL, 'Коэффициент экзамена при расчете итогового рейтинга'),
        ]
    }

    @Override
    Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions()
    { [:] }

    @Override
    List<String> getDisplayableAdditParams()
    { [] }

    class StudentData
    {
        Double gradeSum // сумма баллов (из различных компонентов формулы рейтинга)
        int lectureAttendance = 0 // число посещений лекций и число посещений практик
        int practiceAttendance = 0 // сумму баллов по событиям по практикам

        /**
         * Вычисление текущего рейтинга. К сумме баллов {@link #gradeSum} (в которую входят: сумма баллов по событиям пром. контроля,
         * сумма баллов по событиям по ауд. нагрузке, сумма баллов по остальным событиям) прибавляются доп. баллы за посещение лекций и практик,
         * если количество пропусков для каждого из вида мероприятий не больше одного.
         *
         * @param lecturesAttendancePoint доп. балл за посещение лекций
         * @param practiceAttendancePoint доп. балл за посещение практик
         * @param allLecturesCount общее число лекций
         * @param allPracticeCount общее число практик
         * @return тек. рейтинг студента
         */
        Double calcCurrentRating(Double lecturesAttendancePoint, Double practiceAttendancePoint, int allLecturesCount, int allPracticeCount)
        {
            Double bonus = 0d
            if ((allLecturesCount > 0) && allLecturesCount - lectureAttendance <= 1)
                bonus += lecturesAttendancePoint
            if ((allPracticeCount > 0) && allPracticeCount - practiceAttendance <= 1)
                bonus += practiceAttendancePoint

            return gradeSum ? (gradeSum + bonus) : (bonus > 0 ? bonus : null)
        }

        void addGradeToSum(Double value)
        {
            if (!gradeSum)
                gradeSum = value
            else
                gradeSum += value
        }

        void addData(StudentData otherData)
        {
            addGradeToSum(otherData.gradeSum)
            lectureAttendance += otherData.lectureAttendance
            practiceAttendance += otherData.practiceAttendance
        }
    }

    @Override
    IBrsDao.ICurrentRatingCalc calculateCurrentRating(TrJournal journal, Map<TrJournalModule, List<TrJournalEvent>> journalContent, Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> markMap,
                                                      final IBrsDao.IBrsPreparedSettings preparedBrsSettings)
    {
        final formula = "<p><ul><li>сумма баллов по пром. контролю = сумма (по всем событиям, по которым include_in_current_rating = «да») балл * coeff_icontrol события (если coeff_icontrol не задан, то принимаем его равным 1)</li>" +
                "<li>число проп. лекций = общее число лекций - число посещений лекций студента,</li>" +
                "<li>число пропущенных практик = число практик - число посещений практик,</li></ul></p>" +
                "<p>Тек. рейтинг = сумма баллов по событиям пром. контроля + " +
                "(lectures_attendance_point, если lectures_attendance_control = «да» и пропущенных лекций не более одной) + " +
                "(practice_attendance_point, если practices_attendance_control = «да» и пропущенных практик не более одной) + " +
                "сумма баллов по событиям по ауд. нагрузке + " +
                "сумма баллов по остальным событиям, для которых указан коэффициент include_in_current_rating = «да».</p>"

        // Параметры журнала

        final lecturesAttendanceControl = preparedBrsSettings.getSettings('lectures_attendance_control')?.valueAsBoolean // Учет посещения лекций
        final lecturesAttendancePoint = lecturesAttendanceControl ? preparedBrsSettings.getSettings('lectures_attendance_point')?.valueAsDouble : 0d // Доп.балл за посещение лекций
        final practicesAttendanceControl = preparedBrsSettings.getSettings('practices_attendance_control')?.valueAsBoolean // Учет посещения практик
        final practiceAttendancePoint = practicesAttendanceControl ? preparedBrsSettings.getSettings('practices_attendance_point')?.valueAsDouble : 0d // Доп.балл за посещение практик
        final allowanceThreshold = preparedBrsSettings.getSettings('allowance_threshold')?.valueAsDouble // Попрог на допуск

        // Параметры событий и статистика по модулям

        final coeffIControl = new HashMap<TrJournalEvent, Double>() // коэффициенты контрольных мероприятий текущего контроля
        final includeInCurrentRatingEvents = new HashSet<TrJournalEvent>() // события, для которых указан параметр include_in_current_rating
        final Map<TrJournalModule, Integer> lecturesCountMap = Maps.newHashMapWithExpectedSize(journalContent.size()) // число лекций в модуле
        final Map<TrJournalModule, Integer> practiceCountMap = Maps.newHashMapWithExpectedSize(journalContent.size()) // число практик в модуле
        int allLecturesCount = 0 // Общее число лекций
        int allPracticeCount = 0 // Общее число практик
        final Map<TrJournalEvent, Double> minPoints = new HashMap<>()
        for (module in journalContent)
        {
            int lecturesCount = 0
            int practiceCount = 0
            for (event in module.value)
            {
                if (event instanceof TrEventLoad)
                {
                    // аудиторная нагрузка

                    switch (event.type.code)
                    {
                        case TYPE_LECTURES: lecturesCount++; break
                        case TYPE_PRACTICE: practiceCount++; break
                    }
                }
                else
                {
                    // текщий (промежуточный) контроль и доп. мероприятия

                    final eventAction = event instanceof TrEventAction
                    if (preparedBrsSettings.getEventSettings(event, 'include_in_current_rating')?.valueAsBoolean)
                    {
                        includeInCurrentRatingEvents.add(event)

                        if (eventAction)
                        {
                            // Коэффициент лдя событий пром. контроля
                            def coeff = preparedBrsSettings.getEventSettings(event, 'coeff_icontrol')?.valueAsDouble
                            if (coeff != null)
                                coeffIControl.put(event, coeff)
                        }
                    }

                    if (eventAction)
                    {
                        final minPoint = preparedBrsSettings.getEventSettings(event, 'min_points')?.valueAsDouble
                        if (minPoint != null)
                            minPoints.put(event, minPoint)
                    }
                }
            }

            lecturesCountMap.put(module.key, lecturesCount)
            practiceCountMap.put(module.key, practiceCount)
            allLecturesCount += lecturesCount
            allPracticeCount += practiceCount
        }

        // Считаем текущий рейтинг
        final Map<EppStudentWorkPlanElement, Map<EppRegistryModule, Double>> ratingMap = Maps.newHashMapWithExpectedSize(markMap.size())
        final Set<EppStudentWorkPlanElement> sessionAllowedStudents = Sets.newHashSetWithExpectedSize(markMap.size())
        for (student in markMap)
        {
            final List<StudentData> modulesStatList = new ArrayList<>(journalContent.size())
            final Map<EppRegistryModule, Double> moduleRatingMap = Maps.newHashMapWithExpectedSize(journalContent.size() + 1)
            def sessionAllowed = true // Второе условие допуска к сессии - сдача всех мероприятий с мин. баллом
            for (module in journalContent)
            {
                final data = new StudentData()
                for (event in module.value)
                {
                    final mark = student.value.get(event)

                    if (event in minPoints.keySet() && (!mark?.grade || mark?.grade < minPoints.get(event)))
                        sessionAllowed = false

                    if (!mark)
                        continue

                    Double grade = null
                    if (event instanceof TrEventLoad)
                    {
                        // Событие аудиторной нагрузки
                        if (!mark.isAbsent())
                        {
                            // Если нет пропуска, прибавляем посещение лекции или практики
                            switch (event.type.code)
                            {
                                case TYPE_LECTURES: data.lectureAttendance++; break
                                case TYPE_PRACTICE: data.practiceAttendance++; break
                            }
                        }

                        // Прибавляем баллы по аудиторной нагрузке
                        grade = mark.grade
                    }
                    else if (mark.grade && includeInCurrentRatingEvents.contains(event))
                    {
                        // События пром. контроля и прочие, для которых указано учитывать в расчете рейтинга
                        grade = mark.grade
                        if (event instanceof TrEventAction)
                        {
                            // Для событий пром. контроля надо умножить баллы на коэффициент, если он задан
                            grade *= coeffIControl.get(event) ?: 1d
                        }
                    }

                    // Прибавляем баллы в общую сумму
                    data.addGradeToSum(grade ?: 0d)
                }

                // Считаем текущий рейтинг по модулю
                final moduleRating = data.calcCurrentRating(lecturesAttendancePoint, practiceAttendancePoint, lecturesCountMap.get(module.key) ?: 0, practiceCountMap.get(module.key) ?: 0)
                moduleRatingMap.put(module.key.module.module, moduleRating)

                // Складируем данные по модулю, чтобы потом посчитать рейтинг по журналу
                modulesStatList.add(data)

                if (sessionAllowed)
                    sessionAllowedStudents.add(student.key)
            }

            if (moduleRatingMap.size() == 1)
            {
                //  Если модулей всего один, то рейтинг по модулю равен рейтингу по журналу
                moduleRatingMap.put(null, moduleRatingMap.values().iterator().next())
            }
            else
            {
                // Суммируем данные по модулям, если их больше одного
                final journalData = new StudentData()
                modulesStatList.each { data -> journalData.addData(data) }

                // Считаем рейтинг по журналу
                final journalRating = journalData.calcCurrentRating(lecturesAttendancePoint, practiceAttendancePoint, allLecturesCount, allPracticeCount)
                moduleRatingMap.put(null, journalRating)
            }

            ratingMap.put(student.key, moduleRatingMap)
        }

        return new IBrsDao.ICurrentRatingCalc() {
            @Override
            TrJournal getJournal()
            { return journal }

            @Override
            public boolean isModuleRatingCalculated()
            { return true }

            @Override
            String getDisplayableRatingFormula()
            { return formula }

            @Override
            public IBrsDao.IStudentCurrentRatingData getCurrentRating(EppStudentWorkPlanElement student)
            {
                return new IBrsDao.IStudentCurrentRatingData() {
                    @Override
                    public ISessionBrsDao.IRatingValue getModuleRating(EppRegistryModule module)
                    {
                        def moduleMap = ratingMap.get(student)
                        return BrsScriptUtils.ratingValue(moduleMap ? moduleMap.get(module) : null, null)
                    }

                    @Override
                    public ISessionBrsDao.IRatingValue getRatingValue()
                    { return getModuleRating(null) }

                    @Override
                    public boolean isAllowed()
                    {
                        def rating = getRatingValue()?.value
                        return rating != null && (allowanceThreshold == null || rating >= allowanceThreshold)
                    }

                    @Override
                    public ISessionBrsDao.IRatingValue getRatingAdditParam(String key)
                    {
                        switch (key)
                        {
                            case 'sessionAllowed': return sessionAllowedStudents.contains(student) ? BrsScriptUtils.ratingValue(1d, null) : null
                            default: return null
                        }
                    }
                }
            }
        }
    }

    @Override
    IBrsDao.IStudentAttestationData calculateAttestation(SessionAttestationSlot slot, IBrsDao.IStudentCurrentRatingData ratingCalc, IBrsDao.IBrsPreparedSettings preparedBrsSettings)
    {
        return new IBrsDao.IStudentAttestationData() {
            @Override
            Boolean passed()
            {
                // критерий прохождения аттестации: тек. рейтинг >= attestation_threshold
                def rating = ratingCalc.ratingValue
                if (rating.value != null)
                {
                    Double threshold = preparedBrsSettings.getSettings('attestation_threshold')?.valueAsDouble
                    return threshold != null && rating.value >= threshold
                }
                return false
            }

            @Override
            SessionAttestationSlotAdditionalData getAdditionalData()
            { return null }
        }
    }

    @Override
    RtfString getPrintableRatingFormula(EppGradeScale gradeScale, EppFControlActionGroup caGroup)
    {
        if (gradeScale == null || !(gradeScale.code in [EppGradeScaleCodes.SCALE_2, EppGradeScaleCodes.SCALE_5]))
            return new RtfString().append('Для используемой в мероприятии шкалы оценок не реализован расчет оценки по БРС.')

        def info = new RtfString()
        if (gradeScale.code == EppGradeScaleCodes.SCALE_2)
        {
            info.append('Рейт. эквивалент = тек.рейтинг + coeff_credit * балл за сессию.').par()
            info.append('Для зачета необходимо, чтобы рейт. эквивалент был не меньше 50.').par()
            info.append('Если рейт. эквивалент ≥ setoff_threshold, то студент получает право на автомат.')
        }
        else
        {
            info.append('Рейт. эквивалент = тек.рейтинг + coeff_exam * балл за сессию.').par()
                    .append('Традиционная оценка вычисляется по рейт. эквиваленту, по таблице перевода:').par()
                    .append('«Неудовлетворительно»: 0 - 50 баллов;').par()
                    .append('«Удовлетворительно»: 51 - 64 баллов;').par()
                    .append('«Хорошо»: 65 - 79 баллов;').par()
                    .append('«Отлично»: >= 80 баллов.')
        }
        return info
    }

    @Override
    ISessionBrsDao.ISessionRatingSettings getRatingSettings(final ISessionBrsDao.ISessionRatingSettingsKey key, IBrsDao.IBrsPreparedSettings brsSettings, boolean useRating)
    {
        final regElType = key.discipline.registryElement.class

        return new ISessionBrsDao.ISessionRatingSettings() {
            @Override
            ISessionBrsDao.ISessionRatingSettingsKey key()
            { return key }

            @Override
            public boolean usePoints()
            { return useRating && (EppRegistryDiscipline.isAssignableFrom(regElType)) }

            @Override
            public boolean useCurrentRating()
            { return useRating && (EppRegistryDiscipline.isAssignableFrom(regElType)) && !EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_PROJECT.equals(key.getCaGroup().getCode()) }

            @Override
            public ISessionBrsDao.ISessionPointsValidator pointsValidator()
            { return new DefaultSessionPointsValidator(0, 100, 0) }

            @Override
            public ISessionBrsDao.IBrsCoefficientValue getCoefficientValue(String defCode)
            { return brsSettings.getSettings(defCode) }
        }
    }

    @Override
    IBrsDao.IStudentSessionMarkData calculateSessionMark(ISessionBrsDao.IStudentCurrentRatingData ratingData, final Double pointsForSessionCA, ISessionBrsDao.ISessionRatingSettings ratingSettings)
    {
        final currentRating = ratingData?.ratingValue?.value
        final gradeScale = ratingSettings.key().scale
        final exam = gradeScale.code == EppGradeScaleCodes.SCALE_5
        final setoff = !exam && gradeScale.code == EppGradeScaleCodes.SCALE_2
        final allowanceThreshold = ratingSettings.getCoefficientValue('allowance_threshold')?.valueAsDouble
        final rateCoeff = ratingSettings.getCoefficientValue(exam ? 'coeff_exam' : 'coeff_credit')?.valueAsDouble
        def rating = null as Double
        def message = null as String

        if (ratingSettings.useCurrentRating())
        {
            if (currentRating && pointsForSessionCA && (exam || setoff))
            {
                // рейт. эквивалент = тек.рейтинг + coeff_credit * балл за сессию (если сдается зачет)
                // рейт. эквивалент = тек.рейтинг + coeff_exam * балл за сессию (если сдается экзамен)
                rating = currentRating + (rateCoeff ? (pointsForSessionCA * rateCoeff) : 0d)
            }

            message = ratingData?.ratingValue?.message
            if (setoff && !message && rating)
            {
                // если рейт. экв. >= setoff_threshold и это зачет - то студент получает право на автомат
                final setoffThreshold = ratingSettings.getCoefficientValue('setoff_threshold')?.valueAsDouble
                if (setoffThreshold && rating >= setoffThreshold)
                    message = "Студент имеет право на сдачу зачета в форме «автомат»."
            }
        }
        else
        {
            rating = pointsForSessionCA
        }

        return new IBrsDao.IStudentSessionMarkData() {
            @Override
            public SessionMarkGradeValueCatalogItem getSessionMark()
            {
                if (!rating || rating < 0)
                    return null

                if (setoff)
                {
                    return BrsScriptUtils.getSessionMark(rating < 50.0 ? NE_ZACHTENO : ZACHTENO)
                }
                else if (exam)
                {
                    final String markCode

                    // «Неудовлетворительно»: 0 - 50 баллов;
                    // «Удовлетворительно»: 51 - 64 баллов;
                    // «Хорошо»: 65 - 69 баллов;
                    // «Отлично»: >= 80 баллов
                    if (rating < 51.0) markCode = NEUDOVLETVORITELNO
                    else if (rating < 65.0) markCode = UDOVLETVORITELNO
                    else if (rating < 80.0) markCode = HOROSHO
                    else markCode = OTLICHNO

                    return BrsScriptUtils.getSessionMark(markCode)
                }
                return null
            }

            @Override
            public Double getRatingMissingForMark(SessionMarkGradeValueCatalogItem mark)
            {
                if (null == rating || !gradeScale.equals(mark.scale))
                    return null

                final double threshold
                switch (mark.code)
                {
                    case ZACHTENO: threshold = 50d; break
                    case UDOVLETVORITELNO: threshold = 51d; break
                    case HOROSHO: threshold = 65d; break
                    case OTLICHNO: threshold = 80d; break
                    default: return null
                }

                // Каким должен быть тек. рейтинг, чтобы хватило до нужной оценки - разворачиваем формулу расчета оценки в обратную сторону:
                // Считаем тек. рейтинг, которого хватит для получения рейтингового коэффициента на нужную оценку,
                // и вычитаем из этого реальный тек. рейтинг.
                return Math.max(0, (threshold - pointsForSessionCA * rateCoeff) - currentRating)
            }

            @Override
            public ISessionBrsDao.IRatingValue getRatingValue()
            { return BrsScriptUtils.ratingValue(rating, message) }

            @Override
            public ISessionBrsDao.IRatingValue getCurrentRatingValue()
            { return BrsScriptUtils.ratingValue(currentRating, message) }

            @Override
            ISessionBrsDao.IRatingValue getRatingAdditParam(String key)
            { return ratingData?.getRatingAdditParam(key) }

            @Override
            public Boolean isAllowed()
            {
                // допуск: выполняются два условия
                // 1) тек. рейтинг >= allowance_threshold
                // 2) для всех событий по пром. контролю, где задан min_points, студент получил баллов >= min_points
                return ratingData?.allowed && allowanceThreshold && (currentRating >= allowanceThreshold) && ratingData.getRatingAdditParam('sessionAllowed')
            }
        }
    }
}