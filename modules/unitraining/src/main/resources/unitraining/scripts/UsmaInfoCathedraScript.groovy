package unitraining.scripts
import org.tandemframework.core.info.ErrorCollector
import org.tandemframework.rtf.util.RtfString
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent
import ru.tandemservice.unitraining.base.entity.journal.*
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByLevel.IBrsTutorOuScript
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.DefaultSessionPointsValidator
import ru.tandemservice.unitraining.brs.dao.IBrsDao
import ru.tandemservice.unitraining.brs.entity.catalogs.IBrsCoefficientDef
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef

return new IBrsTutorOuScript() {
  @Override
  boolean validateOnJournalAcceptance(ErrorCollector errors, Map<TrJournalModule, List<TrJournalEvent>> journalContent, IBrsDao.IBrsPreparedSettings brsSettings)
  {
    /*
      Метод для валидации значений настроек БРС перед согласованием журнала.
      Пример реализации:

      List<String> noMaxPointsTitles = new ArrayList<String>()
      for (Map.Entry<TrJournalModule, List<TrJournalEvent>> e : journalContent.entrySet()) {
        for (TrJournalEvent event : e.value) {
          if (null == brsSettings.getEventSettings(event, "max_points"))
            noMaxPointsTitles.add(String.valueOf(event.number));
        }
      }
      if (noMaxPointsTitles.empty)
        return true;
      errors.add("Согласование журнала невозможно: для событий с номерами " + noMaxPointsTitles.join(", ") + " не указан максимальный балл. Укажите максимальный балл для всех событий перед согласованием.")
      return false
     */
    return true
  }

  @Override
  boolean validateMark(ErrorCollector errors, Student student, TrEduGroupEvent event, ITrJournalEventDao.IJournalMarkData mark, IBrsDao.IBrsPreparedSettings brsSettings)
  {
    if (event.getJournalEvent() instanceof TrEventLoad && !EppGroupTypeALTCodes.TYPE_PRACTICE.equals(event.journalEvent.type.code)) {
      errors.add("Выставление баллов на занятиях разрешено только для практик.");
      return false;
    }
    if (event.getJournalEvent() instanceof TrEventLoad && mark.grade > 5) {
      errors.add("Балл, выставленный студенту на практическом занятии, не может превышать 5.");
      return false;
    }
    def settings = brsSettings.getEventSettings(event.getJournalEvent(), "max_points")
    if (null != settings && settings.valueAsDouble < mark.grade) {
      errors.add("Балл, выставленный студенту, не может превышать максимальный, установленный для данного мероприятия: " + settings.valueStr);
      return false;
    }
    return true
  }

  @Override
  List<TrBrsCoefficientDef> filterCoefficientDefListForEvent(TrJournalEvent event, List<TrBrsCoefficientDef> defList)
  {
    if (event instanceof TrEventAction)
      return defList;
    return Collections.emptyList();
  }

  @Override
  List<IBrsCoefficientDef> getCoefficientDefList() {
    return Arrays.asList(
        BrsScriptUtils.coeffDefinition("allowance_threshold", BrsScriptUtils.OwnerType.JOURNAL, "Порог на допуск"),
        BrsScriptUtils.coeffDefinition("max_points", BrsScriptUtils.OwnerType.EVENT, "Макс. балл"),
        BrsScriptUtils.coeffDefinition("min_points", BrsScriptUtils.OwnerType.EVENT, "Мин. балл"),
        BrsScriptUtils.coeffDefinition("setoff_threshold", BrsScriptUtils.OwnerType.JOURNAL, "Порог на зачет")
    );
  }

  @Override
  Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions() {
    return Collections.emptyMap();
  }

  @Override
  List<String> getDisplayableAdditParams() {
    return Collections.emptyList();
  }

  @Override
  IBrsDao.ICurrentRatingCalc calculateCurrentRating(TrJournal journal, Map<TrJournalModule, List<TrJournalEvent>> journalContent, Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> markMap, IBrsDao.IBrsPreparedSettings preparedBrsSettings)
  {
    String formula = "<div>Расчет текущего рейтинга:</div>" +
            "<div>рейтинг студента = " +
            "сумма баллов студента за контрольные и рефераты + " +
            "активность на практических занятиях (5 баллов) + " +
            "самостоятельная работа (5 баллов) + " +
            "посещение лекций (5 баллов) + " +
            "посещение семинаров (5 баллов) </div>" +
            "<div>рейтинг студента по модулю = сумма баллов студента за по данному модулю, без учета посещений и практик</div>";

    double lec_count = 0;
    double prac_count = 0;
    double allowance_threshold = 0;

    def allowanceThresholdSettings = preparedBrsSettings.getSettings("allowance_threshold")
    if (null != allowanceThresholdSettings)
      allowance_threshold = allowanceThresholdSettings.getValueAsDouble();

    for (List<TrJournalEvent> events : journalContent.values())
      for (TrJournalEvent event : events) {
        if (event instanceof TrEventLoad && !EppGroupTypeALTCodes.TYPE_PRACTICE.equals(event.type.code))
          prac_count = prac_count + 1;
        if (event instanceof TrEventLoad && !EppGroupTypeALTCodes.TYPE_LECTURES.equals(event.type.code))
          lec_count = lec_count + 1;
      }
    
    Map<EppStudentWorkPlanElement, Map<EppRegistryModule, Double>> ratingMap = new HashMap<EppStudentWorkPlanElement, Map<EppRegistryModule, Double>>();
    Map<EppStudentWorkPlanElement, Boolean> allowedMap = new HashMap<EppStudentWorkPlanElement, Boolean>();

    for (EppStudentWorkPlanElement student : markMap.keySet()) {
      Map<EppRegistryModule, Double> studentMap = new HashMap<EppRegistryModule, Double>()
      studentMap.put(null, 0);
      ratingMap.put(student, studentMap);
      for (TrJournalModule module : journalContent.keySet())
        studentMap.put(module.getModule().getModule(), 0);
    }

    for (Map.Entry<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> studentEntry : markMap.entrySet()) {

      EppStudentWorkPlanElement student = studentEntry.getKey();
      Map<EppRegistryModule, Double> studentMap = ratingMap.get(student);

      def ca_points = 0;
      def lec_presence = 0;
      def practice_activity = 0;
      boolean allowed = true;

      for (Map.Entry<TrJournalEvent, ITrJournalEventDao.IJournalMarkData> markEntry : studentEntry.value.entrySet()) {

        def event = markEntry.key
        def points = markEntry.value.grade == null ? 0 : markEntry.value.grade
        def module = event.getJournalModule().getModule().getModule()

        if (event instanceof TrEventAction)
          studentMap.put(module, studentMap.get(module) + points)

        if (event instanceof TrEventLoad && !EppGroupTypeALTCodes.TYPE_PRACTICE.equals(event.type.code))
          practice_activity = practice_activity + points;
        else if (event instanceof TrEventLoad && !EppGroupTypeALTCodes.TYPE_LECTURES.equals(event.type.code))
          lec_presence = lec_presence + (markEntry.value.isAbsent() ? 0 : 1)
        else if (event instanceof TrEventAction) {
          ca_points = ca_points + points;

          def minPointsSettings = preparedBrsSettings.getEventSettings(event, "min_points")
          if (null != minPointsSettings && minPointsSettings.valueAsDouble > points)
            allowed = false;
        }
      }

      def lec_points = lec_presence >= lec_count - 1 ? 5 : 0
      def practice_average = ((double) practice_activity) / (5 * prac_count)
      def practice_points = practice_average >= 4 ? 5 : 0
      def total_points = ca_points + lec_points + practice_points

      studentMap.put(null, total_points);

      allowed = allowed && total_points >= allowance_threshold;

      allowedMap.put(student, allowed);
    }

    return new IBrsDao.ICurrentRatingCalc() {
      @Override TrJournal getJournal() { return journal }
      @Override public boolean isModuleRatingCalculated() { return true; }
      @Override String getDisplayableRatingFormula() { return formula; }
      @Override public IBrsDao.IStudentCurrentRatingData getCurrentRating(EppStudentWorkPlanElement student) {
            return new IBrsDao.IStudentCurrentRatingData() {
                @Override public ISessionBrsDao.IRatingValue getModuleRating(EppRegistryModule module) {
                  def rating = ratingMap.get(student)?.get(module)
                  return BrsScriptUtils.ratingValue(rating, null);
                }
                @Override public ISessionBrsDao.IRatingValue getRatingValue() {
                  def rating = ratingMap.get(student)?.get(null)
                  return BrsScriptUtils.ratingValue(rating == null ? 0 : Math.min(rating, 100), null);
                }
                @Override public boolean isAllowed() {
                  return allowedMap.get(student)
                }
                @Override public ISessionBrsDao.IRatingValue getRatingAdditParam(String key) { return null; }
            };
          }
    }
  }

  @Override
  IBrsDao.IStudentAttestationData calculateAttestation(SessionAttestationSlot slot, IBrsDao.IStudentCurrentRatingData ratingCalc, IBrsDao.IBrsPreparedSettings preparedBrsSettings) {
      return new IBrsDao.IStudentAttestationData() {
          @Override Boolean passed() {
              def rating = ratingCalc.getRatingValue()
            return rating.value != null && rating.value > 29;
          }
          @Override  SessionAttestationSlotAdditionalData getAdditionalData() { return null }
      }
  }

  @Override
  RtfString getPrintableRatingFormula(EppGradeScale gradeScale, EppFControlActionGroup caGroup)
  {
    if (gradeScale == null || !(gradeScale.getCode().equals(EppGradeScaleCodes.SCALE_2) || gradeScale.getCode().equals(EppGradeScaleCodes.SCALE_5)))
      return new RtfString().append("Для используемой в мероприятии шкалы оценок не реализован расчет оценки по БРС.")
    def info = new RtfString()
            .append("Рейтинг студента для суммарной оценки в сессии вычисляется по формуле:")
            .par()
            .append("Рейтинг студента за экзамен: [тек. рейтинг студента]+0,4*[балл за сдачу мероприятия в сессии]")
            .par()
            .append("Рейтинг студента за зачет: [тек. рейтинг студента]+0,2*[балл за сдачу мероприятия в сессии]")
            .par()
            .append("Суммарная оценка по традиционной шкале определяется по достижению порога сдачи зачета для зачета, и по таблице перевода для экзамена:")
            .par()
            .append("<= 50 – неудовлетворительно")
            .par()
            .append("50 – 64 – удовлетворительно")
            .par()
            .append("65 – 79 – хорошо")
            .par()
            .append(">= 80 - отлично")
            .par()
    return info;
    ;
  }

  @Override
  ISessionBrsDao.ISessionRatingSettings getRatingSettings(final ISessionBrsDao.ISessionRatingSettingsKey key, IBrsDao.IBrsPreparedSettings brsSettings, boolean useRating)
  {
    def regElType = key.discipline.registryElement.class

    return new ISessionBrsDao.ISessionRatingSettings() {
      @Override ISessionBrsDao.ISessionRatingSettingsKey key() { return key }
      @Override public boolean usePoints() {
        return useRating && (EppRegistryDiscipline.isAssignableFrom(regElType));
      }
      @Override public boolean useCurrentRating() {
        return useRating && (EppRegistryDiscipline.isAssignableFrom(regElType)) && !EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_PROJECT.equals(key.getCaGroup().getCode());
      }
      @Override public ISessionBrsDao.ISessionPointsValidator pointsValidator() {
        return new DefaultSessionPointsValidator(0, 100, 0);
      }
      @Override public ISessionBrsDao.IBrsCoefficientValue getCoefficientValue(String defCode) {
        return brsSettings.getSettings(defCode);
      }
    };
  }

  @Override
  IBrsDao.IStudentSessionMarkData calculateSessionMark(ISessionBrsDao.IStudentCurrentRatingData ratingData, Double pointsForSessionCA, ISessionBrsDao.ISessionRatingSettings ratingSettings)
  {
    def gradeScale = ratingSettings.key().scale

    double setoff_threshold = 0;

    def setoff_threshold_settings = ratingSettings.getCoefficientValue("setoff_threshold_settings")
    if (null != setoff_threshold_settings)
      setoff_threshold = setoff_threshold_settings.getValueAsDouble();

    def currentRating = ratingData?.ratingValue?.value;
    def allowed = ratingData?.allowed
    
    def rating = null
    def message = ratingSettings.useCurrentRating() ? ratingData?.ratingValue?.message : null

    if (ratingSettings.useCurrentRating() && currentRating != null && pointsForSessionCA != null) {
      if (EppGradeScaleCodes.SCALE_2.equals(gradeScale.getCode()))
        rating = currentRating + 0.2*pointsForSessionCA;
      else if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()))
        rating = currentRating + 0.4*pointsForSessionCA;
    } else if (!ratingSettings.useCurrentRating())
      rating = pointsForSessionCA
    
    if (ratingSettings.useCurrentRating() && setoff_threshold != 0 && message == null && rating != null && rating > setoff_threshold) {
      if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_SETOFF.equals(gradeScale.getCode()))
        message = "Студент имеет право на сдачу зачета в форме «автомат».";
    }

    return new IBrsDao.IStudentSessionMarkData() {
      @Override public SessionMarkGradeValueCatalogItem getSessionMark() {
        if (null == rating)
          return null;
        if (EppGradeScaleCodes.SCALE_2.equals(gradeScale.getCode()) && rating >= setoff_threshold)
          return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.ZACHTENO);
        if (EppGradeScaleCodes.SCALE_2.equals(gradeScale.getCode()))
          return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO);
        if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()) && rating >= 80)
          return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.OTLICHNO);
        if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()) && rating >= 65)
          return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.HOROSHO);
        if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()) && rating >= 50)
          return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO);
        if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()))
          return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO);
        return null;
      }

      @Override public ISessionBrsDao.IRatingValue getRatingValue() { return BrsScriptUtils.ratingValue(rating, message); }
      @Override public ISessionBrsDao.IRatingValue getCurrentRatingValue() { return BrsScriptUtils.ratingValue(currentRating, message); }
      @Override ISessionBrsDao.IRatingValue getRatingAdditParam(String key) { return ratingData?.getRatingAdditParam(key); }
      @Override public Boolean isAllowed() { return allowed; }

      @Override public Double getRatingMissingForMark(SessionMarkGradeValueCatalogItem mark) {
        if (!gradeScale.equals(mark.getScale()))
          return -1;
        if (null == rating)
          return -1;
        if (!allowed)
          return -1;
        if (SessionMarkGradeValueCatalogItemCodes.ZACHTENO.equals(mark.getCode())) {
          def requiredRating = Math.ceil((setoff_threshold - rating) / 0.2)
          if (requiredRating > 100)
            return -1;
          return Math.max(0, requiredRating);
        }
        return -1;
      }
    };
  }
}