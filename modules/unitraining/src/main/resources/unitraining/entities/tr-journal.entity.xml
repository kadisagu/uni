<entity-config name="tr-journal"
               package-name="ru.tandemservice.unitraining.base.entity.journal"
               xmlns="http://www.tandemframework.org/meta/entity"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xsi:schemaLocation="http://www.tandemframework.org/meta/entity http://www.tandemframework.org/schema/meta/meta-entity.xsd">
	

	<entity name="trJournal" title="Реализация дисциплины" table-name="tr_journal">
		<comment>
			Реализация дисциплины, содержит набор событий по чтению дисциплины.
            К реализации прикреплены УГС (по разным видам АН), которые будут обучаться изложенным в реализации способом: журналы УГС.
		</comment>

		<!-- KEY -->
		<many-to-one name="registryElementPart" entity-ref="eppRegistryElementPart" required="true" title="Часть элемента реестра" index="idx_trj_regelpart,idx_trj_key" unique="idx_trj_uniq_title">
			<comment>То, что будет читаться студентам (определяет к какому подразделению относится журнал)</comment>
		</many-to-one>
		<many-to-one name="yearPart" entity-ref="eppYearPart" required="true" title="Часть учебного года" index="idx_trj_yearpart,idx_trj_key" unique="idx_trj_uniq_title">
			<comment>Определяет учебный год и его часть, в рамках которых будет проходить обучение</comment>
		</many-to-one> 
		<property name="title" type="trimmedstring" required="true" title="Название" unique="idx_trj_uniq_title">
			<comment>Название журнала, служит идентификатором журнала для пользователя</comment>
		</property>
		<!-- /KEY -->

		<property name="number" type="trimmedstring" required="true" title="Номер" />

		<property name="durationBeginDate" type="date" required="false" title="Дата начала обучения"/>
		<property name="durationEndDate" type="date" required="false" title="Дата окончания обучения"/>

		<many-to-one name="state" entity-ref="eppState" required="true" />
		<property name="comment" type="text" title="Комментарий" lazy="true"/>

        <many-to-one name="fcaGroup" entity-ref="eppFControlActionGroup" title="Тип ФИК">
            <comment>
                Если по части дисциплины предусмотрены для сдачи в сессии
                и мероприятие с типом «Зачет», и мероприятие с типом «Экзамен»,
                то это поле указывает, к какому из них относится рейтинг,
                накопленный студентами в рамках данной реализации дисциплины.
            </comment>
        </many-to-one>
		<many-to-one name="gradeScale" entity-ref="eppGradeScale" title="Шкала оценки по которой будет учтен рейтинг студента"/>

		<many-to-one name="responsible" entity-ref="ppsEntry" required="false" title="Ответственный (за формирование)">
			<comment>Позволяет делегировать заполнение первоначальной информации по журналу на личную страницу ППС</comment>
		</many-to-one>
		
		<property name="controlOnlyMode" type="boolean" required="true" title="Не включать в структуру чтения аудиторные занятия">
			<comment>true, если в журналах не нужны события по ауд. занятиям. Опция влияет на автоматическое заполнение реализации событиями.</comment>
		</property>

        <property name="hideAttendanceForControlEvents" type="boolean" required="true" title="Не отмечать посещение для контрольных мероприятий">
            <comment>true, если в графах для событий по контролю не нужно выводить поле для отметки посещения.</comment>
        </property>

        <implement interface-ref="ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner"/>
	</entity>

	<entity name="trJournalModule" title="Модуль в реализации дисциплины" table-name="tr_journal_module">
		<comment>
			Определяет, что в рамках журнала будет читаться определенный модуль
			(объекты создаются автоматом по данным из части элемента реестра)
		</comment>
		
		<natural-id>
			<many-to-one name="journal" entity-ref="trJournal" required="true" title="Журнал" backward-cascade="delete"/>
			<many-to-one name="module" entity-ref="eppRegistryElementPartModule" required="true" title="Учебный модуль (для части элемента реестра)"/>
		</natural-id>
		
		<check-constraint name="trJournalModuleConstraint" message="Модуль не из дисциплины реализации.">
           	<![CDATA[ (journal.registryElementPart=module.part) ]]>
       	</check-constraint>
		
		<property name="moduleBeginDate" type="date" required="false" title="Дата начала обучения по модулю"/>
		<property name="moduleEndDate" type="date" required="false" title="Дата окончания обучения по модулю"/>

        <implement interface-ref="ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner"/>
	</entity>

    <entity abstract="true" name="trJournalEvent" title="Событие в реализации дисциплины" table-name="tr_journal_event">
    	<comment>
    		Определяет порядок прохождения событий (встречь студентов и ппс) по разным видам нагрузки и контроля 
    		(на данном этапе подразумевается, что все события независимы) 
    	</comment>
    	
    	<!-- KEY -->
    	<many-to-one name="journalModule" entity-ref="trJournalModule" required="true" title="Модуль учебной группы" unique="idx_trjevent_num" backward-cascade="delete"/>
    	<property name="number" type="int" required="true" title="Порядковый номер события" unique="idx_trjevent_num"/>
    	<!-- /KEY -->
    	
    	<many-to-one name="type" entity-ref="eppGroupType" required="true" title="Вид УГС"/>
        
        <property name="theme"   type="text" title="Тема"/>
        <property name="comment" type="text" title="Комментарий" lazy="true"/>

        <property name="hasMarks" type="boolean" title="Есть оценки" required="true">
            <comment>
                Есть оценки = true, если по событию журнала выставлена отметка о явке/неявке и/или балл. Поле автообновляемое.
            </comment>
        </property>

		<child-entity name="trEventLoad" title="Событие в реализации (ауд. занятие)" table-name="tr_journal_event_load">
		</child-entity>    	

		<child-entity name="trEventAction" title="Событие в реализации (текущий контроль)" table-name="tr_journal_event_action">
			<many-to-one name="actionType" entity-ref="eppIControlActionType" required="true" title="Форма текущего контроля"/>
		</child-entity>

		<child-entity name="trEventAddon" title="Событие в реализации (необязательное дополнительное мероприятие)" table-name="tr_journal_event_addon">
			<many-to-one name="addonType" entity-ref="trAddonType" required="true" title="Тип мероприятия"/>
		</child-entity>

        <implement interface-ref="ru.tandemservice.unitraining.brs.entity.brs.IBrsSettingsOwner"/>
    </entity>



    <entity name="trJournalGroup" title="Журнал (УГС в реализации дисциплины)" table-name="tr_journal_group">
		<comment>
			Определяет из каких УГС состоит журнал (по разным видам нагрузки)
			Все УГС считаются равноправными и не делятся на потоки (на уровне журналов)
		</comment>

		<natural-id>
			<many-to-one name="journal" entity-ref="trJournal" required="true" title="Журнал" backward-cascade="delete"/>
			<many-to-one name="group" entity-ref="eppRealEduGroup" required="true" title="Учебная группа" unique="tr_journal_group_edugroup"/>
		</natural-id>
	</entity>

    <entity name="trJournalGroupStudent" title="Запись студента в журнале" table-name="tr_journal_group_student">
		<comment>
			Определяет, в какой группе находился или находится студент в журнале.
		</comment>

		<natural-id>
            <many-to-one name="group" entity-ref="trJournalGroup" required="true" title="Журнал (УГС в реализации)" backward-cascade="delete"/>
            <many-to-one name="studentWpe" entity-ref="eppStudentWorkPlanElement" required="true" backward-cascade="delete" title="Студент (МСРП)"/>
		</natural-id>

        <property name="transferDate" type="timestamp" required="false" title="Дата утраты актуальности записи в журнале (в связи с переводом)">
        	<comment>См. ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean</comment>
        </property>
	</entity>

 <!-- 
	<entity name="trAdditionalMark" title="Дополнительный балл" table-name="tr_additional_mark">
		<comment>
			Дополнительный балл, ставится преподавателем для корректировки суммы по дисциплине для студента.
		</comment>

        <many-to-one name="group" entity-ref="trJournalGroup" required="true" title="УГС реализации" backward-cascade="delete"/>
        <many-to-one name="studentSlot" entity-ref="eppStudentEpvSlot" required="true" backward-cascade="delete" title="Студент (МСРП)"/>

        <property name="gradeAsLong" required="true" type="long" class-name="java.lang.Long" title="Балл"/>
        <property name="markDate" type="date" required="false" title="Дата выставления балла"/>
        <many-to-one name="reason" entity-ref="trAdditionalMarkReason" required="true" title="Основание"/>
        <property name="comment" type="text" title="Комментарий"/>

        <property name="transferDate" type="date" required="false" title="Дата утраты актуальности записи в журнале (в связи с переводом)">
        	<comment>См. ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean</comment>
        </property>

        <many-to-one name="markedBy" entity-ref="org.tandemframework.core.sec.IPrincipalContext" title="Автор отметки">
            <comment>Системное поле, показывает, кем в последний раз были обновлены данные об отметке - балл, комментарий.</comment>
        </many-to-one>

        <implement interface-ref="ru.tandemservice.unitraining.base.entity.journal.ITrJournalMark"/>
        <property name="date" type="date" formula="markDate"/>
        <many-to-one name="eduGroup" entity-ref="eppRealEduGroup4LoadType" formula="group.group"/>
        <many-to-one name="journal" entity-ref="trJournal" title="Журнал" formula="group.journal"/>

	</entity>
!-->

</entity-config>