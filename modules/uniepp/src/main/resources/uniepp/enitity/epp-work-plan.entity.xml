<?xml version="1.0" encoding="UTF-8"?>

<entity-config name="uni-epp-workplan" package-name="ru.tandemservice.uniepp.entity.workplan" xmlns="http://www.tandemframework.org/meta/entity" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.tandemframework.org/meta/entity http://www.tandemframework.org/schema/meta/meta-entity.xsd">
	

	<entity abstract="true" name="eppWorkPlanBase" title="Рабочий учебный план (базовый)" table-name="epp_workplan_base_t">
		<comment>Базовый рабочий учебный план - объект, содержащий только набор дисциплин</comment>
		
		<child-entity name="eppWorkPlan" title="Рабочий учебный план" table-name="epp_workplan_t">
			<comment>Рабочий учебный план, содержащий в том числе и распределение недель по частям семестра</comment>
			
			<many-to-one name="parent" entity-ref="eppEduPlanVersionBlock" required="true" title="УП (блок версии)" />
			<many-to-one name="year" entity-ref="eppYearEducationProcess" required="true" title="ПУПнаГ" unique="idx_epp_workplan_number" />
			<many-to-one name="term" entity-ref="term" required="true" title="Семестр" />
			<property name="number" type="trimmedstring" required="true" title="Номер" unique="idx_epp_workplan_number" />
			<many-to-one entity-ref="eppCustomEduPlan" name="customEduPlan" required="false" title="Индивидуальный учебный план"/>

			<many-to-one name="cachedGridTerm" entity-ref="developGridTerm" required="true" title="Семестр сетки (кэш для запросов)"/>
			
			<check-constraint name="cachedGridTermCheck" message="Поле «Семестр сетки» заполнено неверно.">
                (customEduPlan is not null or cachedGridTerm.developGrid=parent.eduPlanVersion.eduPlan.developGrid) and cachedGridTerm.term=term
    		</check-constraint>
			
		</child-entity>

		<child-entity name="eppWorkPlanVersion" title="Рабочий учебный план (версия)" table-name="epp_workplan_version_t">
			<comment>Версия РУП, хранит только набор строк (распределение недель по частям семестра хранится в РУПе)</comment>
			<many-to-one name="parent" entity-ref="eppWorkPlan" required="true" title="РУП" unique="idx_epp_workplan_version_number" backward-cascade="delete" />
			<property name="number" type="trimmedstring" required="true" title="Номер" unique="idx_epp_workplan_version_number" />
		</child-entity>

		<property name="registrationNumber" type="trimmedstring" required="true" title="Номер регистрации" />
		<many-to-one name="state" entity-ref="eppState" required="true" />
		<property name="confirmDate" type="date" required="false" title="Дата утверждения" />
		<property name="titlePostfix" type="trimmedstring" title="Постфикс" />
		<property name="comment" type="text" title="Комментарий" />

	
	</entity>

	<entity name="eppWorkPlanPart" title="Часть РУП" table-name="epp_wppart_t">
		<natural-id>
			<many-to-one name="workPlan" entity-ref="eppWorkPlan" required="true" title="РУП" backward-cascade="delete" />
			<property name="number" type="integer" required="true" title="Номер части" />
		</natural-id>
		<property name="totalWeeks" type="integer" required="true" title="Общее количество недель (в части семестре)" />
		
		<property name="auditWeeks" type="integer" required="true" title="Количество недель аудиторного обучения (в части семестре)" />
		<property name="selfworkWeeks" type="integer" required="true" title="Количество недель самостоятельного обучения (в части семестре)" />
	</entity>


	<!-- ========================================================== -->
	<!-- ====================== строки РУП ======================== -->


	<entity abstract="true" name="eppWorkPlanRow" title="Строка РУП" table-name="epp_wprow_t">
		<many-to-one name="workPlan" entity-ref="eppWorkPlanBase" required="true" title="РУП" backward-cascade="delete" />
		
		<many-to-one name="kind" entity-ref="eppWorkPlanRowKind" required="true" title="Вид дисциплины"/>

		<property name="number" type="trimmedstring" required="false" title="Номер (индекс)" />
		<property name="title" type="trimmedstring" required="true" title="Название в РУП" />
		<property name="titleAppendix" type="trimmedstring" required="false" title="Дополнение к названию" />
		<property name="comment" type="trimmedstring" required="false" title="Комментарий" />
		
		<property name="beginDate" type="date" required="false" title="Дата начала" />
		<property name="endDate" type="date" required="false" title="Дата окончания" />

        <property name="needRetake" type="boolean" required="false" title="Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение)">
            <comment>
                true  - переаттестация: необходимо пересдавать контрольные мероприятия.
                false - перезачтение: изучать и сдавать ничего не нужно.
                null  - обычное освоение: необходимо изучать дисциплину и сдавать контрольные мероприятия, как обычно.
            </comment>
        </property>
		
		<child-entity name="eppWorkPlanTemplateRow" title="Строка РУП: шаблон строки" table-name="epp_wprow_tmpl_t">
			<many-to-one name="type" entity-ref="eppRegistryStructure" required="true" title="Тип элемента реестра"/>
			<property name="part" type="integer" required="true" title="Номер части элемента реестра" default-value="1" />
			
			<property name="eduplanTermCount" type="integer" required="false" title="Семестров по УП" />
			<property name="eduplanTotalSize" type="long" required="false" title="Часов по УП"/>
			<property name="eduplanTotalLabor" type="long" required="false" title="Трудоемкость по УП"/>
			<property name="eduplanTotalWeeks" type="long" required="false" title="Недель по УП"/>
		</child-entity>

		<child-entity name="eppWorkPlanRegistryElementRow" title="Строка РУП: часть элемента реестра" table-name="epp_wprow_regel_t">
			<comment>
				Строка РУП "часть элемента реестра" определяет, какая часть элемента реестра используется в РУП
				и задает по ней распределение нагрузки и форм контроля
			</comment>
			<many-to-one name="registryElementPart" entity-ref="eppRegistryElementPart" required="true" title="Часть элемента реестра"/>
			
			<!-- 
			<check-constraint name="eppWorkPlanRegistryElementRowUnique" message="Дисциплина в рамках РУП не должна повторяться.">
			 	<![CDATA[
				    not exists (
				    	select r.workPlan.id as wp_id, r.registryElementPart.id as relp_id
				    	from eppWorkPlanRegistryElementRow as r
				    	group by r.workPlan.id, r.registryElementPart.id
				    	having count(r.id) > 1
				    )
			    ]]>
    		</check-constraint>
    		-->
			
		</child-entity>
	</entity>

	<entity name="eppWorkPlanRowPartLoad" title="Нагрузка для строки РУП в части (по видам теоретической нагрузки)" table-name="epp_wprow_part_load_t">
		<natural-id>
			<many-to-one name="row" entity-ref="eppWorkPlanRow" required="true" backward-cascade="delete" />
			<many-to-one name="loadType" entity-ref="eppLoadType" required="true" /> <!-- TODO: epp-A-LoadType: распределять только по видам аудиторной нагрузки -->
			<property name="part" type="integer" required="true" />
		</natural-id>
		<property name="load" type="long" required="true" title="Число часов" />

		<property name="days" type="long" title="Число дней (цикловая нагрузка)" required="false" />
		<property name="studyPeriodStartDate" type="date" title="Дата начала учебной деятельности (цикловая нагрузка)" required="false"/>
		<property name="studyPeriodFinishDate" type="date" title="Дата окончания учебной деятельности (цикловая нагрузка)" required="false"/>
	</entity>

</entity-config>