package uniepp.scripts.edu_plan

import com.google.common.collect.Iterables
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.Alignment
import jxl.format.Border
import jxl.format.BorderLineStyle
import jxl.format.CellFormat
import jxl.format.Colour
import jxl.format.Orientation
import jxl.format.VerticalAlignment
import jxl.write.Label
import jxl.write.Number
import jxl.write.WritableCellFormat
import jxl.write.WritableFont
import jxl.write.WritableSheet
import jxl.write.WritableWorkbook
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.collections.Predicate
import org.apache.commons.collections.functors.InstanceofPredicate
import org.hibernate.Session
import org.tandemframework.core.CoreCollectionUtils
import ru.tandemservice.uni.dao.UniDaoFacade
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO
import ru.tandemservice.uni.entity.education.DevelopGridTerm
import ru.tandemservice.uniepp.UniEppUtils
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO
import ru.tandemservice.uniepp.dao.eduplan.data.EppCustomPlanWrapper
import ru.tandemservice.uniepp.dao.eduplan.data.IEppCustomPlanRowWrapper
import ru.tandemservice.uniepp.entity.catalog.EppALoadType
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType
import ru.tandemservice.uniepp.entity.catalog.EppELoadType
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType
import ru.tandemservice.uniepp.entity.catalog.EppLoadType
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement

import java.awt.Rectangle
import java.util.stream.Collectors

/**
 * @author Ekaterina Zvereva
 * @since 30.01.2016
 */



return new EppCustomEduPlanPrint(
        session: session,                           //сессия
        customEduPlan: session.get(EppCustomEduPlan.class, object),           // индивидуальный учебный план
).print()


class EppCustomEduPlanPrint
{
    private Session session;
    private EppCustomEduPlan customEduPlan;

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    private EppCustomPlanWrapper planWrapper;
    private Map<Integer, DevelopGridTerm> gridTermMap;
    private Map<String, Double> totalResultMap = new HashMap<>();

    Collection<EppALoadType> aLoadTypes;
    List<EppControlActionType> disciplineCATypes;
    Collection<Map.Entry<Integer, Collection<IEppCustomPlanRowWrapper>>> termSet;


    /**
     * Основной метод печати.
     */
    def print()
    {
        // создаем печатную форму
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        def workbook = Workbook.createWorkbook(out, ws);
        printData(workbook);

        workbook.write();
        workbook.close();

        // стандартные выходные параметры скрипта
        return [document: out.toByteArray(), fileName: createFileName() + ".xls"]
    }

    def printData(WritableWorkbook workbook)
    {
        CellFormats formats = new CellFormats();
        def sheet = workbook.createSheet(customEduPlan.titleWithGrid, 0);

        int rowNum = 0;
        rowNum = createEduPlanData(sheet, formats, rowNum);

        prepareCustomEduPlanData();
        printDisciplines(sheet, rowNum, formats);
    }

    /**
     * Данные УП
     */
    private int createEduPlanData(WritableSheet sheet, CellFormats formats, int rowNum)
    {
        //УП
        addTextCell(sheet, 0, rowNum, 3, 1, "Учебный план", formats.dataLabel);
        addTextCell(sheet, 4, rowNum++, 3, 1, customEduPlan.epvBlock.eduPlanVersion.fullNumber, formats.dataLabel);

        //Направление подготовки
        addTextCell(sheet, 0, rowNum, 3, 1, "Направление подготовки", formats.dataLabel);
        addTextCell(sheet, 4, rowNum++, 3, 1,  customEduPlan.getEpvBlock().getEduPlanVersion().getEduPlan().getEducationElementSimpleTitle(), formats.dataLabel);

        //Блок
        addTextCell(sheet, 0, rowNum, 3, 1, "Блок", formats.dataLabel);
        addTextCell(sheet, 4, rowNum++, 3, 1, customEduPlan.epvBlock.title, formats.dataLabel);

        //Индивидуальный УП
        addTextCell(sheet, 0, rowNum, 3, 1, "Индивидуальный УП", formats.dataLabel);
        addTextCell(sheet, 4, rowNum++, 3, 1, customEduPlan.titleWithGrid, formats.dataLabel);

        //Форма обучения
        addTextCell(sheet, 0, rowNum, 3, 1, "Форма обучения", formats.dataLabel);
        addTextCell(sheet, 4, rowNum++, 3, 1, customEduPlan.programForm.title, formats.dataLabel);

        return rowNum+2;
    }

    /**
     * Подгрузить данные инд УП
     */
    private void prepareCustomEduPlanData()
    {
        planWrapper = new EppCustomPlanWrapper(customEduPlan);
        gridTermMap = IDevelopGridDAO.instance.get().getDevelopGridMap(customEduPlan.developGrid.id);
        termSet = planWrapper.getRowMap().entrySet();

        List<EppLoadType> loadTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppLoadType.class);
        aLoadTypes = CollectionUtils.select(loadTypes, new InstanceofPredicate(EppALoadType.class));

        List<EppControlActionType> controlActionTypes = IEppEduPlanVersionDataDAO.instance.get().getActiveControlActionTypes(null).collect(Collectors.toList());

        disciplineCATypes = (List<EppControlActionType>) CollectionUtils.select(controlActionTypes, DISCIPLINE_CA_TYPE_PREDICATE);

    }

    /**
     *  Название семестра
     */
    private String getCurrentCustomTerm(Map.Entry<Integer, Collection<IEppCustomPlanRowWrapper>> termEntry)
    {
        final int term = termEntry.getKey();
        return String.valueOf(term) + " " + this.gridTermMap.get(term).getPart().getTitle();
    }

    /**
     *  Создание и печать таблицы
     */
    private int printDisciplines(WritableSheet sheet, int rowIndex, CellFormats formats)
    {
        int headHeight = 4;
        //создаем столбцы таблицы
        Column termColumn = new Column(1, "Часть учебного года", headHeight, 1, formats.tableHeader, formats.tableValue)
        Column indexCol = new Column(1, "Индекс", headHeight, 1, formats.tableHeader, formats.tableValue);
        Column disciplineTitleColumn = new Column(5, "Название", headHeight, 1, formats.tableHeader, formats.tableValue);
        Column numberRegistryCol = new Column(2, "№ реестра", headHeight, 1, formats.tableHeader, formats.tableValue);

        //Нагрузка
        Column loadTermColumn = new Column("Нагрузка", 1, formats.tableHeader);

        Column loadLabourColumn = new Column(1, "Труд-сть", headHeight - 1, 1, formats.tableHeader_vertical, formats.tableValue_num);
        Column loadSizeCol = new Column(1, "Часов (всего)", headHeight - 1, 1, formats.tableHeader_vertical, formats.tableValue_num);
        loadTermColumn.addColumn(loadLabourColumn)
        loadTermColumn.addColumn(loadSizeCol)

        Map<String, Column> loadColumns = new HashMap<>();
        loadColumns.put(EppLoadType.FULL_CODE_LABOR, loadLabourColumn)
        loadColumns.put(EppLoadType.FULL_CODE_TOTAL_HOURS, loadSizeCol)

        for (EppALoadType aLoadType : aLoadTypes)
        {
            Column column = new Column(1, aLoadType.getShortTitle().replace(".", ". "), headHeight - 1, 1, formats.tableHeader_vertical, formats.tableValue_num);
            loadColumns.put(aLoadType.fullCode, column);
            loadTermColumn.addColumn(column);
        }
        Column selfWorkColumn = new Column(1, "Самост.", headHeight - 1, 1, formats.tableHeader_vertical, formats.tableValue_num)
        loadColumns.put(EppELoadType.FULL_CODE_SELFWORK, selfWorkColumn)
        loadTermColumn.addColumn(selfWorkColumn)

        //Контроль
        Column controlColumn = new Column(1, "Контроль", headHeight - 1, 1, formats.tableHeader_vertical, formats.tableValue_num)
        loadColumns.put(EppELoadType.FULL_CODE_CONTROL, controlColumn)
        loadTermColumn.addColumn(controlColumn)

        Column controlActionsCol = new Column("Контроль", 1, formats.tableHeader);

        Column currentActionCol = new Column("Текущий", 1, formats.tableHeader);
        controlActionsCol.addColumn(currentActionCol);

        Column finalActionsCol = new Column("Итоговый", 1, formats.tableHeader);
        controlActionsCol.addColumn(finalActionsCol);

        Map<EppControlActionType, Column> actionTypeCols = new HashMap<>();
        for (EppControlActionType actionType : disciplineCATypes)
        {
            Column column = new Column(1, actionType.getShortTitle(), 2, 1, formats.tableHeader_vertical, formats.tableValue_num);
            if (actionType instanceof EppFControlActionType)
            {
                finalActionsCol.addColumn(column);
            }
            else
            {
                currentActionCol.addColumn(column);
            }
            actionTypeCols.put(actionType, column);
        }

        Column ownerCol = new Column(2, "Читающее подр.", headHeight, 1, formats.tableHeader, formats.tableValue);
        Column epTermCol = new Column(1, "Семестр базового УП", headHeight, 1, formats.tableHeader_vertical, formats.tableValue_num);
        Column reActionColumn = new Column(2, "Перезачет и переаттестация", headHeight, 1, formats.tableHeader, formats.tableValue)

        //заполняем данными
        for (Map.Entry<Integer, Collection<IEppCustomPlanRowWrapper>> currentTerm : termSet)
        {
            for (IEppCustomPlanRowWrapper row : getCurrentTermValues(currentTerm))
            {
                if (row == null || isFirstRowInTerm(currentTerm, row))
                {
                    termColumn.addValue(getCurrentCustomTerm(currentTerm))
                    termColumn.addValueHeight(currentTerm.getValue().size())
                }
                indexCol.addValue(row? row.getSourceRow().getIndex():"")
                disciplineTitleColumn.addValue(row? row.title:"")

                EppRegistryElement element = getCurrentRowRegistryElement(row);
                numberRegistryCol.addValue(element != null ? "№" + element.number : "")
                WritableCellFormat format = row!= null && row.getSourceRow().hasParentDistributedRow()? formats.tableValue_grey : null
                for (Map.Entry<String, Column> currentLoadColumn : loadColumns.entrySet())
                {
                    currentLoadColumn.value.addValue(getCurrentLoadValue(currentLoadColumn.key, row), format)
                }

                for (EppControlActionType currentCA : disciplineCATypes)
                {
                    int size = row? row.getSourceRow().getActionSize(row.getSourceTermNumber(), currentCA.fullCode):0
                    actionTypeCols.get(currentCA).addValue(size > 0 ? String.valueOf(size) : "", format)
                }

                ownerCol.addValue(getCurrentOwnerOrgUnitTitle(row))
                epTermCol.addValue(row?String.valueOf(row.getSourceTermNumber()):"")
                reActionColumn.addValue(row?row.getCustomActionTitle(): "")
            }

            //Строка "Итого" по семестру
            termColumn.addValue("", formats.tableResult)
            indexCol.addValue("", formats.tableResult)
            disciplineTitleColumn.addValue("Итого", formats.tableResult)
            numberRegistryCol.addValue("", formats.tableResult)

            for (Map.Entry<String, Column> currentLoadColumn : loadColumns.entrySet())
            {
                currentLoadColumn.value.addValue(getCurrentTotalLoadValue(currentLoadColumn.key, currentTerm.key.intValue()), formats.tableResult)
            }

            for (EppControlActionType currentCA : disciplineCATypes)
            {
                actionTypeCols.get(currentCA).addValue(getCurrentTotalActionSize(currentCA.fullCode, currentTerm.key.intValue()), formats.tableResult)
            }
            ownerCol.addValue("", formats.tableResult)
            epTermCol.addValue("", formats.tableResult)
            reActionColumn.addValue(getCurrentTotalReattestationSize(currentTerm.key.intValue()), formats.tableResult)
        }

        //Строка "Всего по ИУП"
        termColumn.addValue("", formats.tableResult)
        indexCol.addValue("", formats.tableResult)
        disciplineTitleColumn.addValue("Всего по инд. УП", formats.tableResult)
        numberRegistryCol.addValue("", formats.tableResult)

        for (Map.Entry<String, Column> currentLoadColumn : loadColumns.entrySet())
        {
            currentLoadColumn.value.addValue(UniEppUtils.formatLoad(totalResultMap.get(currentLoadColumn.key), false), formats.tableResult)
        }

        for (EppControlActionType currentCA : disciplineCATypes)
        {
            actionTypeCols.get(currentCA).addValue(UniEppUtils.formatLoad(totalResultMap.get(currentCA.fullCode), false), formats.tableResult)
        }
        ownerCol.addValue("", formats.tableResult)
        epTermCol.addValue("", formats.tableResult)
        reActionColumn.addValue(UniEppUtils.formatLoad(totalResultMap.get("reActionColumn"), false), formats.tableResult)



        Column disciplinesTable = new Column("Мероприятия индивидуального учебного плана", 1, formats.tableHeader);
        disciplinesTable.addColumn(termColumn);
        disciplinesTable.addColumn(indexCol)
        disciplinesTable.addColumn(disciplineTitleColumn);
        disciplinesTable.addColumn(numberRegistryCol);
        disciplinesTable.addColumn(loadTermColumn);
        disciplinesTable.addColumn(controlActionsCol);
        disciplinesTable.addColumn(ownerCol);
        disciplinesTable.addColumn(epTermCol)
        disciplinesTable.addColumn(reActionColumn)

        Rectangle rect = disciplinesTable.print(sheet, 0, rowIndex);
        return rowIndex + rect.height;

    }

    /**
     * Генерация имени файла
     */
    private String createFileName()
    {
        StringBuilder builder = new StringBuilder("ИндУП-");
        builder.append(customEduPlan.epvBlock.getEduPlanVersion().getFullNumber())
            .append(".")
            .append(customEduPlan.number);
        return builder.toString();
    }

    /**
     * Итоговое значение по виду нагрузки
     */
    public String getCurrentTotalLoadValue(String loadFullCode, int term)
    {
        double result = planWrapper.calcTotalLoadInTerm(term, loadFullCode);
        Double item = totalResultMap.get(loadFullCode)
        if (item == null)
            item = 0;
        totalResultMap.put(loadFullCode, item + result)
        return UniEppUtils.formatLoad(result, false);
    }


    private Collection<IEppCustomPlanRowWrapper> getCurrentTermValues(Map.Entry<Integer, Collection<IEppCustomPlanRowWrapper>> currentTerm)
    {
        return currentTerm.value.empty? Collections.singletonList(null) : currentTerm.value
    }

    /**
     * Итоговое значение по виду контроля
     */
    public String getCurrentTotalActionSize(String caFullCode, int term)
    {
        double result = planWrapper.calcFinalControlActionCountInTerm(term, caFullCode);
        Double item = totalResultMap.get(caFullCode)
        if (item == null)
            item = 0;
        totalResultMap.put(caFullCode, item + result)
        return String.valueOf(result);
    }

    /**
     * Итоговое значение по перезачтению
     */
    public String getCurrentTotalReattestationSize(int term)
    {
        double result = planWrapper.calcTotalReattestationSizeInTerm(term);
        Double item = totalResultMap.get("reActionColumn")
        if (item == null)
            item = 0;
        totalResultMap.put("reActionColumn", item + result)
        return UniEppUtils.formatLoad(result, false);
    }

    protected static final Predicate DISCIPLINE_CA_TYPE_PREDICATE = new Predicate() {
        @Override
        public boolean evaluate(final Object object)
        {
            return (((EppControlActionType) object).isUsedWithDisciplines());
        }
    };

    private static boolean isFirstRowInTerm(Map.Entry<Integer, Collection<IEppCustomPlanRowWrapper>> term, IEppCustomPlanRowWrapper row)
    {
        return Iterables.getFirst(term.value, null) == row;
    }


    private static EppRegistryElement getCurrentRowRegistryElement(IEppCustomPlanRowWrapper rowWrapper)
    {
        if (rowWrapper == null) {
            return null;
        }
        return rowWrapper.getSourceRow().getRow() instanceof EppEpvRegistryRow ? ((EppEpvRegistryRow) rowWrapper.getSourceRow().getRow()).getRegistryElement() : null;
    }

    /**
     * Читающее подразделение
     */
    private static String getCurrentOwnerOrgUnitTitle(IEppCustomPlanRowWrapper rowWrapper)
    {
        if (rowWrapper == null) {
            return "";
        }
        if (rowWrapper.getSourceRow().getRow() instanceof EppEpvRegistryRow) {
            final EppEpvRegistryRow row = (EppEpvRegistryRow) rowWrapper.getSourceRow().getRow();
            if (row.getRegistryElementOwner() != null) {
                return row.getRegistryElementOwner().getShortTitle();
            }
        }
        return "";
    }

    /**
     * Значение по виду нагрузки
     */
    private static String getCurrentLoadValue(String loadFullCode, IEppCustomPlanRowWrapper rowWrapper)
    {
        if (rowWrapper == null) {
            return "";
        }
        return UniEppUtils.formatLoad(rowWrapper.getTotalLoad(loadFullCode), true);
    }


    private static class CellFormats
    {
        final WritableCellFormat dataLabel; //текст

        final WritableCellFormat tableHeader;
        final WritableCellFormat tableHeader_vertical;
        final WritableCellFormat tableValue;
        final WritableCellFormat tableValue_num;
        final WritableCellFormat tableValue_grey;
        final WritableCellFormat tableResult;

        private CellFormats() throws Throwable
        {
            WritableFont baseFont = new WritableFont(WritableFont.ARIAL, 9)
            WritableFont greyFont = new WritableFont(WritableFont.ARIAL, 9)
            greyFont.setColour(Colour.GRAY_50)
            WritableFont boldFont = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD)

            dataLabel = new WritableCellFormat(baseFont);
            dataLabel.setBorder(Border.NONE, BorderLineStyle.NONE);
            dataLabel.setAlignment(Alignment.LEFT);
            dataLabel.setVerticalAlignment(VerticalAlignment.CENTRE);
            dataLabel.setWrap(true)

            tableValue = new WritableCellFormat(baseFont);
            tableValue.setBorder(Border.ALL, BorderLineStyle.DOTTED);
            tableValue.setAlignment(Alignment.LEFT);
            tableValue.setVerticalAlignment(VerticalAlignment.CENTRE);
            tableValue.setWrap(true);

            tableValue_num = new WritableCellFormat(baseFont);
            tableValue_num.setBorder(Border.ALL, BorderLineStyle.DOTTED);
            tableValue_num.setAlignment(Alignment.CENTRE);
            tableValue_num.setVerticalAlignment(VerticalAlignment.CENTRE);

            tableValue_grey = new WritableCellFormat(greyFont);
            tableValue_grey.setBorder(Border.ALL, BorderLineStyle.DOTTED);
            tableValue_grey.setAlignment(Alignment.CENTRE);
            tableValue_grey.setVerticalAlignment(VerticalAlignment.CENTRE);

            tableHeader = new WritableCellFormat(boldFont);
            tableHeader.setBorder(Border.ALL, BorderLineStyle.DOTTED);
            tableHeader.setAlignment(Alignment.CENTRE);
            tableHeader.setVerticalAlignment(VerticalAlignment.CENTRE);
            tableHeader.setWrap(true);

            tableHeader_vertical = new WritableCellFormat(boldFont);
            tableHeader_vertical.setBorder(Border.ALL, BorderLineStyle.DOTTED);
            tableHeader_vertical.setAlignment(Alignment.CENTRE);
            tableHeader_vertical.setVerticalAlignment(VerticalAlignment.CENTRE);
            tableHeader_vertical.setOrientation(Orientation.PLUS_90)
            tableHeader_vertical.setWrap(true);

            tableResult = new WritableCellFormat(boldFont)
            tableResult.setBorder(Border.ALL, BorderLineStyle.DOTTED)
            tableResult.setAlignment(Alignment.CENTRE);
            tableResult.setVerticalAlignment(VerticalAlignment.CENTRE);
            tableResult.setBackground(Colour.GRAY_25)
        }
    }


    class Column
    {
        int startWidth = 0;
        String title;
        int titleHeight;
        int valueHeight;
        WritableCellFormat titleFormat = new WritableCellFormat();
        WritableCellFormat valueFormat = new WritableCellFormat();
        private List<Column> columns = new ArrayList<>();
        private List<CoreCollectionUtils.Pair<Object, WritableCellFormat>> values = new ArrayList<>();
        private List<Integer> valuesHeight = new ArrayList<>();

        Column(String title, int titleHeight, WritableCellFormat titleFormat)
        {
            this.title = title;
            this.titleHeight = titleHeight;
            this.titleFormat = titleFormat;
        }

        Column(int startWidth,
               String title, int titleHeight, int valueHeight,
               WritableCellFormat titleFormat, WritableCellFormat valueFormat
        )
        {
            this.startWidth = startWidth
            this.title = title
            this.titleHeight = titleHeight
            this.valueHeight = valueHeight
            this.titleFormat = titleFormat
            this.valueFormat = valueFormat
        }

        public boolean addColumn(Column c)
        {
            return columns.add(c);
        }

        public void addValueHeight(int height)
        {
            valuesHeight.add(values.size() -1, height);
        }

        public boolean addValue(Object o)
        {

            return addValue(o, null)
        }

        public boolean addValue(Object o, WritableCellFormat format)
        {
            valuesHeight.add(null);
            return values.add(new CoreCollectionUtils.Pair<Object, WritableCellFormat>(o, format));
        }

        int getColumnWidth()
        {
            if (columns.isEmpty()) return startWidth;

            int width = 0;
            for (Column column : columns) {
                if (column == null) continue;
                width += column.getColumnWidth();
            }
            return width;
        }

        Rectangle print(WritableSheet sheet, int colIndex, int rowIndex)
        {
            Rectangle rect = new Rectangle(colIndex, rowIndex, 0, 0);

            int row = rowIndex + titleHeight

            if (valueHeight > 0 && startWidth > 0 && !values.isEmpty()) {
                for (CoreCollectionUtils.Pair<Object, WritableCellFormat> value : values) {
                    row += printValue(sheet, colIndex, row, value);
                }
                rect.width = startWidth;
                rect.height = row - rowIndex;
            }

            for (Column column : columns) {
                if (column == null) continue;
                Rectangle r = column.print(sheet, rect.x + rect.width as int, rowIndex + titleHeight);
                rect.width += r.width;
                rect.height = Math.max(rect.height, r.height);
            }

            if (title != null && titleHeight > 0 && rect.width > 0)
                addTextCell(sheet, colIndex, rowIndex, rect.width as int, titleHeight, title, titleFormat);

            return rect;
        }

        private int printValue(WritableSheet sheet, int left, int top, CoreCollectionUtils.Pair<Object, WritableCellFormat> value)
        {
            if (value != null)
            {
                WritableCellFormat format = value.y? value.y : valueFormat
                Object x = value.x
                int index = values.indexOf(value)
                int height = valuesHeight.get(index)? valuesHeight.get(index).intValue(): valueHeight
                if (x instanceof String)
                    addTextCell(sheet, left, top, startWidth, height, (String) x, format);
                else if (x instanceof Double)
                    addNumberCell(sheet, left, top, startWidth, height, x, format);
                else if (x instanceof Integer)
                    addNumberCell(sheet, left, top, startWidth, height, x, format);
                else
                    addTextCell(sheet, left, top, startWidth, height, x.toString(), format);
                return height;
            }
            return 0;
        }
    }


    private static void addTextCell(WritableSheet sheet, int left, int top, int width, int height, String text, CellFormat format) {
        sheet.addCell(new Label(left, top, text, format));
        sheet.mergeCells(left, top, left + width - 1, top + height - 1);
    }

    private static void addNumberCell(WritableSheet sheet, int left, int top, int width, int height, double number, CellFormat format) {
        sheet.addCell(new Number(left, top, number, format));
        sheet.mergeCells(left, top, left + width - 1, top + height - 1);
    }





}