/* $Id$ */
package uniepp.scripts.edu_plan

import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.CellFormat
import jxl.format.Orientation
import jxl.format.PageOrientation
import jxl.write.*
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.collections.functors.InstanceofPredicate
import org.apache.commons.collections15.Predicate
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.IFormatter
import org.tandemframework.hibsupport.builder.MQBuilder
import org.tandemframework.hibsupport.builder.expression.MQExpression
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.dao.UniDaoFacade
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO
import ru.tandemservice.uni.entity.catalog.Course
import ru.tandemservice.uni.entity.education.DevelopGridTerm
import ru.tandemservice.uni.util.FilterUtils
import ru.tandemservice.uni.util.jxl.ColourMap
import ru.tandemservice.uniepp.UniEppUtils
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvTotalRow
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper
import ru.tandemservice.uniepp.entity.catalog.*
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType
import ru.tandemservice.uniepp.entity.plan.data.EppEpvHierarchyRow
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionBlockGen
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement
import ru.tandemservice.uniepp.util.EppDevelopCombinationTitleUtil

import java.awt.*
import java.util.List
import java.util.stream.Collectors

import static org.tandemframework.hibsupport.dql.DQLExpressions.property

/**
 * @author Andrey Andreev
 * @since 19.08.2015
 */


return new EppEduPlanVersionPrint(
        session: session,
        epvIds: epvIds,
).print()

/**
 * Печать Версии учебного плана
 */
class EppEduPlanVersionPrint {

    Session session
    List<Long> epvIds

    List<EppWeek> eppWeeks;
    List<EppWeekType> eppWeekTypes;
    List<EppControlActionType> disciplineCATypes;
    List<EppControlActionType> actionsCATypes;
    List<EppLoadType> loadTypes;
    Collection<EppELoadType> eLoadTypes;
    Collection<EppALoadType> aLoadTypes;
    EppELoadType eLoadTotalAuditType;
    EppELoadType eLoadSelfWorkType;

    boolean showTotalLabor;


    ByteArrayOutputStream out = new ByteArrayOutputStream();


    def print() {
        def epv = IUniBaseDao.instance.get().getList(EppEduPlanVersion.class, epvIds);

        if (epv.size() == 0) {
            throw new ApplicationException("Нет Версий учебных планов для печати");
        } else {
            init();

            WorkbookSettings ws = new WorkbookSettings();
            ws.setMergedCellChecking(false);
            ws.setRationalization(false);
            ws.setGCDisabled(true);
            def workbook = Workbook.createWorkbook(out, ws);

            body(workbook, getWrappers());

            workbook.write();
            workbook.close();

            StringBuilder fn = new StringBuilder().append("ВерсияУП");
            if (epv.size() == 1) {
                fn.append("-").append(epv.get(0).getFullNumber().replace('/', '-').replace(' ', '-'));
            } else {
                fn.append("-").append((new Date().format("yyyy.MM.dd")));
            }
            fn.append(".xls");

            return [document: out.toByteArray(), fileName: fn.toString()]
        }
    }

    private void init() {
        initFormats();
        eppWeeks = IUniBaseDao.instance.get().getCatalogItemListOrderByCode(EppWeek.class);
        eppWeekTypes = IUniBaseDao.instance.get().getCatalogItemListOrderByCode(EppWeekType.class);
        List<EppControlActionType> controlActionTypes = IEppEduPlanVersionDataDAO.instance.get().getActiveControlActionTypes(null).collect(Collectors.toList());
        disciplineCATypes = (List<EppControlActionType>) CollectionUtils.select(
                controlActionTypes,
                new org.apache.commons.collections.Predicate() {
                    @Override
                    public boolean evaluate(final Object object) {
                        return (((EppControlActionType) object).isUsedWithDisciplines());
                    }
                });
        actionsCATypes = (List<EppControlActionType>) CollectionUtils.select(
                controlActionTypes,
                new org.apache.commons.collections.Predicate() {
                    @Override
                    public boolean evaluate(final Object object) {
                        return (((EppControlActionType) object).isUsedWithAttestation() || ((EppControlActionType) object).isUsedWithPractice());
                    }
                });
        loadTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppLoadType.class);
        eLoadTypes = CollectionUtils.select(loadTypes, new InstanceofPredicate(EppELoadType.class));
        aLoadTypes = CollectionUtils.select(loadTypes, new InstanceofPredicate(EppALoadType.class));
        eLoadTotalAuditType = (EppELoadType) CollectionUtils.find(
                eLoadTypes,
                new org.apache.commons.collections.Predicate() {
                    @Override
                    public boolean evaluate(final Object object) {
                        return (((EppELoadType) object).isAuditTotal());
                    }
                });
        eLoadSelfWorkType = (EppELoadType) CollectionUtils.find(
                eLoadTypes,
                new org.apache.commons.collections.Predicate() {
                    @Override
                    public boolean evaluate(final Object object) {
                        return (((EppELoadType) object).isSelfWork());
                    }
                });
    }

    def body(final WritableWorkbook book, List<IEppEpvBlockWrapper> wrappers) {
        for (final IEppEpvBlockWrapper wrapper : wrappers) {
            String title = "№" + wrapper.getVersion().getFullNumber() + " (" + wrapper.getBlock().getEducationElementSimpleTitle() + ")";
            WritableSheet sheet = createWritableSheet(book, title, 0);

            int rowIndex = printSheetHeader(sheet, wrapper, 0);

            // данные по УГ
            rowIndex += 1;
            rowIndex = printWeekDistribution(book, sheet, wrapper, ++rowIndex);

            // берем по умолчанию данные из УП
            Long blockId = wrapper.getBlock().getId();
            EppEduPlanVersionBlock block = UniDaoFacade.getCoreDao().getNotNull(EppEduPlanVersionBlock.class, blockId);
            EppGeneration generation = block.getEduPlanVersion().getEduPlan().getGeneration();
            showTotalLabor = generation.showTotalLabor();

            Long gridId = wrapper.getBlock().getEduPlanVersion().getEduPlan().getDevelopGrid().getId();
            Map<Integer, DevelopGridTerm> gridMap = IDevelopGridDAO.instance.get().getDevelopGridMap(gridId);

            rowIndex += 1;
            rowIndex = printDisciplinesBlocks(sheet, wrapper, gridMap, ++rowIndex);

            rowIndex += 1;
            rowIndex = printActionsBlock(sheet, wrapper, gridMap, ++rowIndex);

            rowIndex += 1;
            printTotalData(sheet, wrapper, gridMap, ++rowIndex);
        }
    }


    private static WritableSheet createWritableSheet(final WritableWorkbook book, final String title, final int index) {
        final WritableSheet sheet = book.createSheet(title, index);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);
        sheet.getSettings().setScaleFactor(60);
        sheet.getSettings().setBottomMargin(0.3937);
        sheet.getSettings().setTopMargin(0.3937);
        sheet.getSettings().setLeftMargin(0.3937);
        sheet.getSettings().setRightMargin(0.3937);

        for (int i = 0; i < 256; i++)
            sheet.setColumnView(i, 2);

        return sheet;
    }

    int printSheetHeader(WritableSheet sheet, IEppEpvBlockWrapper wrapper, int rowIndex) {
        addTextCell(sheet, 0, rowIndex, 15, 1, "Учебный план: ", STYLE_TABLE_BODY);
        addTextCell(sheet, 20, rowIndex, 60, 1, wrapper.getVersion().getFullTitle(), STYLE_TABLE_BODY);

        rowIndex++;

        addTextCell(sheet, 0, rowIndex, 15, 1, "Направление подготовки: ", STYLE_TABLE_BODY);
        addTextCell(sheet, 20, rowIndex, 60, 1, wrapper.getVersion().getEduPlan().getEducationElementSimpleTitle(), STYLE_TABLE_BODY);

        rowIndex++;

        addTextCell(sheet, 0, rowIndex, 15, 1, "Блок: ", STYLE_TABLE_BODY);
        addTextCell(sheet, 20, rowIndex, 60, 1, wrapper.getBlock().getTitle(), STYLE_TABLE_BODY);

        rowIndex++;

        addTextCell(sheet, 0, rowIndex, 15, 1, "Форма обучения: ", STYLE_TABLE_BODY);
        addTextCell(sheet, 20, rowIndex, 60, 1, EppDevelopCombinationTitleUtil.getTitle(wrapper.getVersion().getEduPlan()), STYLE_TABLE_BODY);
        return rowIndex;
    }


    def Map<String, jxl.format.Colour> getGraphColourMap(final WritableWorkbook book) {
        Map<String, jxl.format.Colour> graphColourMap = new HashMap<String, jxl.format.Colour>();
        Map<Long, jxl.format.Colour> map = ColourMap.getColourMap(book);
        for (EppWeekType wt : eppWeekTypes) {
            String abbreviation = StringUtils.trimToNull(wt.getAbbreviationView());
            Long c = wt.getColor();
            if ((c != null) && (null != abbreviation)) {
                if (graphColourMap.put(abbreviation, map.get(c)) != null)
                    println("WeekType.abbreviation: `" + abbreviation + "` is not unique");
            }
        }
        return graphColourMap;
    }

    private List<IEppEpvBlockWrapper> getWrappers() {
        String alias = "alias";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersionBlock.class, alias)
                .column(property(alias, EppEduPlanVersionBlock.id()))
                .order(property(alias, EppEduPlanVersionBlock.eduPlanVersion().eduPlan().number()))
                .order(property(alias, EppEduPlanVersionBlock.eduPlanVersion().number()))
                .order(property(alias, EppEduPlanVersionBlock.id()));
        FilterUtils.applySelectFilter(dql, alias, EppEduPlanVersionBlock.eduPlanVersion().id(), epvIds);
        List<Long> epvBlockIds = dql.createStatement(getSession()).list();

        final Map<Long, IEppEpvBlockWrapper> epvBlockDataMap = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(epvBlockIds, true);
        final List<IEppEpvBlockWrapper> wrappers = new ArrayList<>();
        wrappers.addAll(epvBlockDataMap.values());
        Collections.sort(wrappers);

        return wrappers;
    }


    int printWeekDistribution(WritableWorkbook book, WritableSheet sheet, IEppEpvBlockWrapper wrapper, int rowIndex) {
        final Map<Course, Map<Integer, EppEduPlanVersionWeekType>> weekDistribution = new TreeMap<Course, Map<Integer, EppEduPlanVersionWeekType>>(Course.COURSE_COMPARATOR);

        Map<String, jxl.format.Colour> graphColourMap = getGraphColourMap(book);

        List<EppEduPlanVersionWeekType> eduPlanVersionWeekTypeList = IUniBaseDao.instance.get().getList(
                EppEduPlanVersionWeekType.class, EppEduPlanVersionWeekType.eduPlanVersion().id().toString(), wrapper.getId());
        final Set<String> usedTypeCodes = new HashSet<String>(eppWeeks.size());
        for (EppEduPlanVersionWeekType eppEduPlanVersionWeekType : eduPlanVersionWeekTypeList) {
            Map<Integer, EppEduPlanVersionWeekType> map =
                    SafeMap.safeGet(weekDistribution, eppEduPlanVersionWeekType.getCourse(), HashMap.class);
            map.put(eppEduPlanVersionWeekType.getWeek().getNumber(), eppEduPlanVersionWeekType);
            usedTypeCodes.add(eppEduPlanVersionWeekType.getWeekType().getCode());
        }


        Column<Course> weekDistribution_Table = new Column<>("Учебный график", 1, STYLE_TABLE_BODY);

        Column<Course> course_Col = new Column(5, "Курс", 3, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        course_Col.clValue = { Course course -> course_Col.addValue(course.getIntValue()); }
        weekDistribution_Table.addColumn(course_Col);

        Column<Course> distribution_Col = new Column<>("Распределение учебных недель", 1, STYLE_TABLE_HEAD_NORM);
        weekDistribution_Table.addColumn(distribution_Col);
        for (EppWeek week : eppWeeks) {
            Column<Course> column = new Column<>(1, week.getTitle(), 2, 1, STYLE_TABLE_HEAD_VERT_SMALL, STYLE_TABLE_BODY);
            int weekNumber = week.getNumber();
            column.clValue = { Course course ->
                EppEduPlanVersionWeekType epvWt = weekDistribution.get(course).get(weekNumber);
                if (epvWt != null) {
                    String value = epvWt.getTerm().getIntValue() + "." + epvWt.getWeekType().getAbbreviationView();
                    String term = StringUtils.substringBefore(value, ".");
                    boolean odd = (1 == (Integer.parseInt(term) % 2));

                    WritableCellFormat format = new WritableCellFormat(odd ? STYLE_ODD : STYLE_EVEN);
                    String abbreviation = StringUtils.trimToNull(value.substring(1 + term.length()));
                    jxl.format.Colour color = graphColourMap.get(abbreviation);
                    if (color != null) format.setBackground(color);

                    column.addValue(new ColumnsValue(abbreviation, format));
                } else column.addValue("");
            }
            distribution_Col.addColumn(column);
        }


        Column<Course> timeBudget_Col = new Column<>("Данные по бюджету времени", 1, STYLE_TABLE_HEAD_NORM);
        weekDistribution_Table.addColumn(timeBudget_Col);



        for (EppWeekType wt : eppWeekTypes) {
            if ((!EppWeekType.CODE_NOT_USED.equals(wt.getCode())) && usedTypeCodes.contains(wt.getCode())) {
                String abbreviation = StringUtils.trimToNull(wt.getAbbreviationView());
                Long weekTypeId = wt.getId();
                String title = wt.getShortTitle() + (abbreviation != null ? " (" + abbreviation + ")" : "");
                Column<Course> column = new Column<>(2, title, 2, 1, STYLE_TABLE_HEAD_VERT_SMALL, STYLE_TABLE_BODY);
                column.clValue = { Course course ->
                    Map<Integer, EppEduPlanVersionWeekType> map = weekDistribution.get(course);
                    int i = 0;
                    for (EppEduPlanVersionWeekType t : map.values()) {
                        if (weekTypeId.equals(t.getWeekType().getId()))
                            i++;
                    }
                    column.addValue(i > 0 ? i : "");
                }
                timeBudget_Col.addColumn(column);
            }
        }


        for (Course course : weekDistribution.keySet())
            weekDistribution_Table.clValue(course);

        Rectangle rect = weekDistribution_Table.print(sheet, 0, rowIndex);
        rowIndex = rect.y + rect.height

        return rowIndex;
    }

    int printDisciplinesBlocks(WritableSheet sheet, IEppEpvBlockWrapper wrapper, Map<Integer, DevelopGridTerm> gridMap, int rowIndex) {
        MQBuilder builder = new MQBuilder(EppEduPlanVersionBlockGen.ENTITY_CLASS, "block")
                .add(MQExpression.eq("block", EppEduPlanVersionBlockGen.eduPlanVersion().id().s(), wrapper.getId()));
        List<EppEduPlanVersionBlock> databaseBlockList = builder.getResultList(session);
        Collections.sort(databaseBlockList);

        boolean isExtendedView = wrapper.getVersion().getViewTableDiscipline().isExtend();

        for (final EppEduPlanVersionBlock block : databaseBlockList) {

            List<IEppEpvRowWrapper> blockRows = filterEduPlanBlockRows(
                    wrapper.getRowMap(),
                    new Predicate<IEppEpvRowWrapper>() {
                        @Override
                        public boolean evaluate(final IEppEpvRowWrapper object) {
                            return block.getId().equals(object.getOwner().getId()) && IEppEpvRowWrapper.DISCIPLINES_ROWS.evaluate(object);
                        }
                    });
            Collections.sort(blockRows, new Comparator<IEppEpvRowWrapper>() {

                @Override
                int compare(IEppEpvRowWrapper o1, IEppEpvRowWrapper o2) {
                    return o1.getIndex().compareTo(o2.getIndex());
                }
            })

            boolean isSelfWorkPresent = isSelfWorkPresent(blockRows);
            boolean multipleLoadColumns = isSelfWorkPresent || isExtendedView || showTotalLabor;

            Column<IEppEpvRowWrapper> blockTable = new Column(block.getEducationElementSimpleTitle(), 1, STYLE_BLOCK_TITLE);

            Column<IEppEpvRowWrapper> number_Col = new Column(2, "№", 5, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
            int idx = 1;
            number_Col.clValue = { IEppEpvRowWrapper epvRow ->
                if (epvRow.getRow() instanceof EppEpvHierarchyRow) number_Col.addValue("");
                else number_Col.addValue(idx++);
            }
            blockTable.addColumn(number_Col);

            blockTable.addColumn(doPrepare_mainBlockColumns("Блоки, модули, дисциплины"));
            blockTable.addColumn(doPrepare_controlActionsColumns(disciplineCATypes));

            Column<IEppEpvRowWrapper> load_Col = new Column("Нагрузка", 1, STYLE_TABLE_HEAD_NORM);
            blockTable.addColumn(load_Col);

            if (showTotalLabor) {
                Column<IEppEpvRowWrapper> totalLaborLoad_SubCol = new Column(3, "Труд-сть (всего)", 4, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
                totalLaborLoad_SubCol.clValue = { IEppEpvRowWrapper epvRow ->
                    double value = epvRow.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                    totalLaborLoad_SubCol.addValue(epvRow.getRow() instanceof EppEpvHierarchyRow ? new ColumnsValue(value, STYLE_TABLE_BODY_BOLD) : value);
                }
                load_Col.addColumn(totalLaborLoad_SubCol);
            }

            Column<IEppEpvRowWrapper> totalSizeLoad_SubCol = new Column(3, "Часов (всего)", 4, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
            totalSizeLoad_SubCol.clValue = { IEppEpvRowWrapper epvRow ->
                double value = epvRow.getTotalLoad(0, EppLoadType.FULL_CODE_TOTAL_HOURS);
                totalSizeLoad_SubCol.addValue(epvRow.getRow() instanceof EppEpvHierarchyRow ? new ColumnsValue(value, STYLE_TABLE_BODY_BOLD) : value);
            }
            load_Col.addColumn(totalSizeLoad_SubCol);

            Column<IEppEpvRowWrapper> aLoad_SubCol = new Column("Аудиторных", 1, STYLE_TABLE_HEAD_NORM);
            load_Col.addColumn(aLoad_SubCol);

            Column<IEppEpvRowWrapper> totalALoad_SubSubCol = new Column(3, eLoadTotalAuditType.getShortTitle(), 3, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
            totalALoad_SubSubCol.clValue = { IEppEpvRowWrapper epvRow ->
                setTotalRowLoad_byEppLoadType(epvRow.getRow() instanceof EppEpvHierarchyRow, epvRow, totalALoad_SubSubCol, eLoadTotalAuditType.getFullCode());
            }
            aLoad_SubCol.addColumn(totalALoad_SubSubCol);

            for (EppALoadType aLoadType : aLoadTypes) {
                Column<IEppEpvRowWrapper> column = new Column(3, aLoadType.getShortTitle(), 3, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
                String loadTypeFullCode = aLoadType.getFullCode();
                column.clValue = { IEppEpvRowWrapper epvRow ->
                    setTotalRowLoad_byEppLoadType(epvRow.getRow() instanceof EppEpvHierarchyRow, epvRow, column, loadTypeFullCode);
                }
                aLoad_SubCol.addColumn(column);
            }

            for (EppELoadType eLoadType : eLoadTypes) {
                if (eLoadTotalAuditType != eLoadType) {
                    Column<IEppEpvRowWrapper> column = new Column(3, eLoadType.getShortTitle(), 4, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
                    String loadTypeFullCode = eLoadType.getFullCode();
                    column.clValue = { IEppEpvRowWrapper epvRow ->
                        setTotalRowLoad_byEppLoadType(epvRow.getRow() instanceof EppEpvHierarchyRow, epvRow, column, loadTypeFullCode);
                    }
                    load_Col.addColumn(column);
                }
            }

            Column<IEppEpvRowWrapper> termLoad_Col = new Column("Нагрузка по семестрам", 1, STYLE_TABLE_HEAD_NORM);
            blockTable.addColumn(termLoad_Col);

            Map<Integer, Column> courseGridCols = new TreeMap<>();
            Map<Integer, Column> termGridCols = new TreeMap<>();
            for (Map.Entry<Integer, DevelopGridTerm> entry : gridMap.entrySet()) {
                DevelopGridTerm gridTerm = entry.getValue();
                int course = gridTerm.getCourseNumber();
                int term = entry.getKey();

                Column<IEppEpvRowWrapper> courseGridCol = courseGridCols.get(course);
                if (courseGridCol == null) {
                    courseGridCol = new Column(String.valueOf(course) + " курс", 1, STYLE_TABLE_HEAD_NORM);
                    courseGridCols.put(course, courseGridCol);
                    termLoad_Col.addColumn(courseGridCol);
                }

                Column<IEppEpvRowWrapper> termGridCol = termGridCols.get(term);
                if (termGridCol == null) {
                    int termSize = wrapper.getTermSize(IEppEpvBlockWrapper.ELOAD_FULL_CODE_ALL, term);
                    termGridCol = new Column(String.valueOf(term) + (multipleLoadColumns ? (" (" + termSize + ")") : ""), 1, STYLE_TABLE_HEAD_NORM);
                    if (showTotalLabor) {
                        Column<IEppEpvRowWrapper> column = new Column(2, "Труд-сть", 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY_WITHOUT_ZERO);
                        column.clValue = { IEppEpvRowWrapper epvRow ->
                            column.addValue(epvRow.getTotalLoad(term, EppLoadType.FULL_CODE_LABOR));
                        }
                        termGridCol.addColumn(column);
                    }

                    if (isExtendedView) {
                        // если расширенная форма - то разбиваем аудиторную нагрузку
                        for (EppALoadType aLoadType : aLoadTypes) {
                            Column<IEppEpvRowWrapper> column = new Column(2, aLoadType.getShortTitle(), 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY_WITHOUT_ZERO);
                            String loadFullCode = aLoadType.getFullCode();
                            column.clValue = { IEppEpvRowWrapper epvRow ->
                                column.addValue(epvRow.isTermDataOwner() ? epvRow.getTotalLoad(term, loadFullCode) : "");
                            }
                            termGridCol.addColumn(column);
                        }
                    } else {
                        // если форма простая - то выводим общуюаудиторную нагрузку
                        if (multipleLoadColumns) {
                            // если самостоятельная (или иная нагрузка) нагрузка есть - то выводим название (вертикально), а число недель не выводим (оно уже вверху)
                            Column<IEppEpvRowWrapper> column = new Column(2, eLoadTotalAuditType.getShortTitle(), 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY_WITHOUT_ZERO);
                            column.clValue = { IEppEpvRowWrapper epvRow ->
                                addTermRowLoad_totalAuditLoad(epvRow, term, column);
                            }
                            termGridCol.addColumn(column);
                        } else {
                            // если самостоятельной нагрузки нет - то это одна колонка (отображение простое) - выводим только число недель (вверху оно не выводилось)
                            Column<IEppEpvRowWrapper> column = new Column(2, "(" + termSize + ")", 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY_WITHOUT_ZERO);
                            column.clValue = { IEppEpvRowWrapper epvRow ->
                                addTermRowLoad_totalAuditLoad(epvRow, term, column);
                            }
                            termGridCol.addColumn(column);
                        }
                    }

                    if (isSelfWorkPresent) {
                        Column<IEppEpvRowWrapper> column = new Column(2, eLoadSelfWorkType.getShortTitle(), 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY_WITHOUT_ZERO);
                        column.clValue = { IEppEpvRowWrapper epvRow ->
                            column.addValue(epvRow.isTermDataOwner() ? epvRow.getTotalLoad(term, EppELoadType.FULL_CODE_SELFWORK) : "");
                        }
                        termGridCol.addColumn(column);
                    }
                    termGridCols.put(term, termGridCol);
                    courseGridCol.addColumn(termGridCol);
                }
            }

            Column<IEppEpvRowWrapper> rowTitleEnd_Col = new Column(2, "Строка", 5, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
            rowTitleEnd_Col.clValue = { IEppEpvRowWrapper epvRow ->
                if (epvRow.getRow() instanceof EppEpvHierarchyRow) {
                    rowTitleEnd_Col.addValue(new ColumnsValue(epvRow.getIndex() + " " + epvRow.getDisplayableTitle(), STYLE_TABLE_BODY_BOLD));
                } else {
                    rowTitleEnd_Col.addValue(epvRow.getIndex() + " " + epvRow.getDisplayableTitle());
                }
            }
            blockTable.addColumn(rowTitleEnd_Col);

            Column<IEppEpvRowWrapper> owner_Col = new Column(25, "Читающее подразделение", 5, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
            owner_Col.clValue = { IEppEpvRowWrapper epvRow ->
                IEppEpvRow row = epvRow.getRow();
                if (row instanceof EppEpvRegistryRow) {
                    OrgUnit owner = row.getRegistryElementOwner();
                    if (owner instanceof TopOrgUnit) {
                        // не показывать title для академии - это все равно фэйк
                        owner_Col.addValue("");
                    } else owner_Col.addValue(owner != null ? owner.getFullTitle() : "");
                } else owner_Col.addValue("");
            }
            blockTable.addColumn(owner_Col);


            for (IEppEpvRowWrapper epvRow : blockRows)
                blockTable.clValue(epvRow);

            Rectangle rect = blockTable.print(sheet, 0, ++rowIndex);
            rowIndex = 1 + rect.y + rect.height
        }

        return rowIndex;
    }

    int printActionsBlock(WritableSheet sheet, IEppEpvBlockWrapper wrapper, Map<Integer, DevelopGridTerm> gridMap, int rowIndex) {

        List<IEppEpvRowWrapper> blockRows = filterEduPlanBlockRows(
                wrapper.getRowMap(),
                IEppEpvRowWrapper.ACTIONS_ROWS);
        Collections.sort(blockRows, new Comparator<IEppEpvRowWrapper>() {

            @Override
            int compare(IEppEpvRowWrapper o1, IEppEpvRowWrapper o2) {
                return o1.getIndex().compareTo(o2.getIndex());
            }
        })

        Column<IEppEpvRowWrapper> blockTable = new Column("", 1, STYLE_BLOCK_TITLE);

        Column<IEppEpvRowWrapper> number_Col = new Column(2, "№", 5, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        int idx = 1;
        number_Col.clValue = { IEppEpvRowWrapper epvRow ->
            if (epvRow.getRow() instanceof EppEpvHierarchyRow) number_Col.addValue("");
            else number_Col.addValue(idx++);
        }
        blockTable.addColumn(number_Col);

        blockTable.addColumn(doPrepare_mainBlockColumns("Блоки, модули, мероприятия"));
        blockTable.addColumn(doPrepare_controlActionsColumns(actionsCATypes));

        Column<IEppEpvRowWrapper> load_Col = new Column("Нагрузка", 1, STYLE_TABLE_HEAD_NORM);
        blockTable.addColumn(load_Col);

        if (showTotalLabor) {
            Column<IEppEpvRowWrapper> totalLaborLoad_SubCol = new Column(3, "Труд-сть (всего)", 4, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
            totalLaborLoad_SubCol.clValue = { IEppEpvRowWrapper epvRow ->
                double value = epvRow.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                totalLaborLoad_SubCol.addValue(epvRow.getRow() instanceof EppEpvHierarchyRow ? new ColumnsValue(value, STYLE_TABLE_BODY_BOLD) : value);
            }
            load_Col.addColumn(totalLaborLoad_SubCol);
        }

        Column<IEppEpvRowWrapper> totalSizeLoad_SubCol = new Column(3, "Часов (всего)", 4, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
        totalSizeLoad_SubCol.clValue = { IEppEpvRowWrapper epvRow ->
            double value = epvRow.getTotalLoad(0, EppLoadType.FULL_CODE_TOTAL_HOURS);
            totalSizeLoad_SubCol.addValue(epvRow.getRow() instanceof EppEpvHierarchyRow ? new ColumnsValue(value, STYLE_TABLE_BODY_BOLD) : value);
        }
        load_Col.addColumn(totalSizeLoad_SubCol);

        Column<IEppEpvRowWrapper> termLoad_Col = new Column("Нагрузка по семестрам", 1, STYLE_TABLE_HEAD_NORM);
        blockTable.addColumn(termLoad_Col);

        Map<Integer, Column> courseGridCols = new TreeMap<>();
        Map<Integer, Column> termGridCols = new TreeMap<>();
        for (Map.Entry<Integer, DevelopGridTerm> entry : gridMap.entrySet()) {
            DevelopGridTerm gridTerm = entry.getValue();
            int course = gridTerm.getCourseNumber();
            int term = entry.getKey();

            Column<IEppEpvRowWrapper> courseGridCol = courseGridCols.get(course);
            if (courseGridCol == null) {
                courseGridCol = new Column(String.valueOf(course) + " курс", 1, STYLE_TABLE_HEAD_NORM);
                courseGridCols.put(course, courseGridCol);
                termLoad_Col.addColumn(courseGridCol);
            }

            Column<IEppEpvRowWrapper> termGridCol = termGridCols.get(term);
            if (termGridCol == null) {
                termGridCol = new Column(String.valueOf(term), 1, STYLE_TABLE_HEAD_NORM);
                if (showTotalLabor) {
                    Column<IEppEpvRowWrapper> totalLabor_SubSubCol = new Column(2, "Труд-сть", 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY_WITHOUT_ZERO);
                    totalLabor_SubSubCol.clValue = { IEppEpvRowWrapper epvRow ->
                        totalLabor_SubSubCol.addValue(epvRow.getTotalLoad(term, EppLoadType.FULL_CODE_LABOR));
                    }
                    termGridCol.addColumn(totalLabor_SubSubCol);
                }

                Column<IEppEpvRowWrapper> totalSize_SubSubCol = new Column(2, "Часов", 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY_WITHOUT_ZERO);
                totalSize_SubSubCol.clValue = { IEppEpvRowWrapper epvRow ->
                    totalSize_SubSubCol.addValue(epvRow.getTotalLoad(term, EppLoadType.FULL_CODE_TOTAL_HOURS));
                }
                termGridCol.addColumn(totalSize_SubSubCol);

                Column<IEppEpvRowWrapper> totalTermWeeks_SubSubCol = new Column(2, "Недель", 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY_WITHOUT_ZERO);
                totalTermWeeks_SubSubCol.clValue = { IEppEpvRowWrapper epvRow ->
                    totalTermWeeks_SubSubCol.addValue(epvRow.getTotalLoad(term, EppLoadType.FULL_CODE_WEEKS));
                }
                termGridCol.addColumn(totalTermWeeks_SubSubCol);

                termGridCols.put(term, termGridCol);
                courseGridCol.addColumn(termGridCol);
            }
        }

        Column<IEppEpvRowWrapper> rowTitleEnd_Col = new Column(2, "Строка", 5, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
        rowTitleEnd_Col.clValue = { IEppEpvRowWrapper epvRow ->
            if (epvRow.getRow() instanceof EppEpvHierarchyRow) {
                rowTitleEnd_Col.addValue(new ColumnsValue(epvRow.getIndex() + " " + epvRow.getDisplayableTitle(), STYLE_TABLE_BODY_BOLD));
            } else {
                rowTitleEnd_Col.addValue(epvRow.getIndex() + " " + epvRow.getDisplayableTitle());
            }
        }
        blockTable.addColumn(rowTitleEnd_Col);

        Column<IEppEpvRowWrapper> owner_Col = new Column(25, "Читающее подразделение", 5, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        owner_Col.clValue = { IEppEpvRowWrapper epvRow ->
            IEppEpvRow row = epvRow.getRow();
            if (row instanceof EppEpvRegistryRow) {
                OrgUnit owner = row.getRegistryElementOwner();
                if (owner instanceof TopOrgUnit) {
                    // не показывать title для академии - это все равно фэйк
                    owner_Col.addValue("");
                } else owner_Col.addValue(owner != null ? owner.getFullTitle() : "");
            } else owner_Col.addValue("");
        }
        blockTable.addColumn(owner_Col);


        for (IEppEpvRowWrapper epvRow : blockRows)
            blockTable.clValue(epvRow);

        Rectangle rect = blockTable.print(sheet, 0, ++rowIndex);
        rowIndex = rect.y + rect.height

        return rowIndex;
    }

    int printTotalData(WritableSheet sheet, IEppEpvBlockWrapper wrapper, Map<Integer, DevelopGridTerm> gridMap, int rowIndex) {
        Collection<IEppEpvRowWrapper> blockRowsRaw = wrapper.getRowMap().values();
        boolean isExtendedView = wrapper.getVersion().getViewTableDiscipline().isExtend();
        boolean isSelfWorkPresent = isSelfWorkPresent(blockRowsRaw);
        Collection<EppEpvTotalRow> blockRows = IEppEduPlanVersionDataDAO.instance.get().getTotalRows(wrapper, blockRowsRaw)


        Column<EppEpvTotalRow> blockTable = new Column<>("", 1, STYLE_BLOCK_TITLE);

        Column<EppEpvTotalRow> title_Col = new Column<>(30, "Название характеристики", 5, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        title_Col.clValue = { EppEpvTotalRow epvRow -> title_Col.addValue(epvRow.getTitle()); }
        blockTable.addColumn(title_Col);

        Column<EppEpvTotalRow> controlActions_Col = new Column<>("Формы контроля", 1, STYLE_TABLE_HEAD_NORM);
        for (EppControlActionType tp : disciplineCATypes) {
            Column<EppEpvTotalRow> column = new Column<>(2, tp.getShortTitle(), 4, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
            String code = tp.getFullCode();
            column.clValue = { EppEpvTotalRow epvRow ->
                column.addValue(epvRow.getControlActionValue(code))
            }
            controlActions_Col.addColumn(column);
        }
        blockTable.addColumn(controlActions_Col);

        Column<EppEpvTotalRow> load_Col = new Column<>("Нагрузка", 1, STYLE_TABLE_HEAD_NORM);
        blockTable.addColumn(load_Col);

        if (showTotalLabor) {
            Column<EppEpvTotalRow> totalLaborLoad_SubCol = new Column<>(3, "Труд-сть (всего)", 4, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
            totalLaborLoad_SubCol.clValue = { EppEpvTotalRow epvRow ->
                totalLaborLoad_SubCol.addValue(epvRow.getLoadValue(0, EppLoadType.FULL_CODE_LABOR));
            }
            load_Col.addColumn(totalLaborLoad_SubCol);
        }

        Column<EppEpvTotalRow> totalSizeLoad_SubCol = new Column<>(3, "Часов (всего)", 4, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
        totalSizeLoad_SubCol.clValue = { EppEpvTotalRow epvRow ->
            totalSizeLoad_SubCol.addValue(epvRow.getLoadValue(0, EppLoadType.FULL_CODE_TOTAL_HOURS));
        }
        load_Col.addColumn(totalSizeLoad_SubCol);

        Column<EppEpvTotalRow> aLoad_SubCol = new Column<>("Аудиторных", 1, STYLE_TABLE_HEAD_NORM);
        load_Col.addColumn(aLoad_SubCol);

        Column<EppEpvTotalRow> totalALoad_SubSubCol = new Column<>(3, eLoadTotalAuditType.getShortTitle(), 3, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
        totalALoad_SubSubCol.clValue = { EppEpvTotalRow epvRow ->
            totalALoad_SubSubCol.addValue(epvRow.getLoadValue(0, eLoadTotalAuditType.getFullCode()));
        }
        aLoad_SubCol.addColumn(totalALoad_SubSubCol);

        for (EppALoadType aLoadType : aLoadTypes) {
            Column<EppEpvTotalRow> column = new Column<>(3, aLoadType.getShortTitle(), 3, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
            String loadTypeFullCode = aLoadType.getFullCode();
            column.clValue = { EppEpvTotalRow epvRow ->
                column.addValue(epvRow.getLoadValue(0, loadTypeFullCode))
            }
            aLoad_SubCol.addColumn(column);
        }

        for (EppELoadType eLoadType : eLoadTypes) {
            if (eLoadTotalAuditType != eLoadType) {
                Column<EppEpvTotalRow> column = new Column<>(3, eLoadType.getShortTitle(), 4, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
                String loadTypeFullCode = eLoadType.getFullCode();
                column.clValue = { EppEpvTotalRow epvRow ->
                    column.addValue(epvRow.getLoadValue(0, loadTypeFullCode))
                }
                load_Col.addColumn(column);
            }
        }

        Column<EppEpvTotalRow> termLoad_Col = new Column<>("Нагрузка по семестрам (в семестр)", 1, STYLE_TABLE_HEAD_NORM);
        blockTable.addColumn(termLoad_Col);

        Map<Integer, Column> courseGridCols = new TreeMap<>();
        Map<Integer, Column> termGridCols = new TreeMap<>();
        for (Map.Entry<Integer, DevelopGridTerm> entry : gridMap.entrySet()) {
            DevelopGridTerm gridTerm = entry.getValue();
            int course = gridTerm.getCourseNumber();
            int term = entry.getKey();

            Column<EppEpvTotalRow> courseGridCol = courseGridCols.get(course);
            if (courseGridCol == null) {
                courseGridCol = new Column<>(String.valueOf(course) + " курс", 1, STYLE_TABLE_HEAD_NORM);
                courseGridCols.put(course, courseGridCol);
                termLoad_Col.addColumn(courseGridCol);
            }

            Column<EppEpvTotalRow> termGridCol = termGridCols.get(term);
            if (termGridCol == null) {
                int termSize = wrapper.getTermSize(IEppEpvBlockWrapper.ELOAD_FULL_CODE_ALL, term);
                termGridCol = new Column<>(String.valueOf(term) + " (" + String.valueOf(termSize) + ")", 1, STYLE_TABLE_HEAD_NORM);
                if (showTotalLabor) {
                    Column<EppEpvTotalRow> column = new Column(3, "Труд-сть", 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY_WITHOUT_ZERO);
                    column.clValue = { EppEpvTotalRow epvRow ->
                        column.addValue(epvRow.getLoadValue(term, EppLoadType.FULL_CODE_LABOR));
                    }
                    termGridCol.addColumn(column);
                }

                if (isExtendedView) {
                    // если расширенная форма - то разбиваем аудиторную нагрузку
                    for (EppALoadType aLoadType : aLoadTypes) {
                        Column<EppEpvTotalRow> column = new Column<>(3, aLoadType.getShortTitle(), 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY_WITHOUT_ZERO);
                        String loadFullCode = aLoadType.getFullCode();
                        column.clValue = { EppEpvTotalRow epvRow ->
                            column.addValue(epvRow.getLoadValue(term, loadFullCode));
                        }
                        termGridCol.addColumn(column);
                    }
                } else {
                    // если форма простая - то выводим общуюаудиторную нагрузку
                    Column<EppEpvTotalRow> column = new Column<>(3, eLoadTotalAuditType.getShortTitle(), 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY_WITHOUT_ZERO);
                    String loadFullCode = eLoadTotalAuditType.getFullCode();
                    column.clValue = { EppEpvTotalRow epvRow ->
                        column.addValue(epvRow.getLoadValue(term, loadFullCode));
                    }
                    termGridCol.addColumn(column);
                }

                if (isSelfWorkPresent) {
                    Column<EppEpvTotalRow> column = new Column<>(3, eLoadSelfWorkType.getShortTitle(), 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY_WITHOUT_ZERO);
                    column.clValue = { EppEpvTotalRow epvRow ->
                        column.addValue(epvRow.getLoadValue(term, EppELoadType.FULL_CODE_SELFWORK));
                    }
                    termGridCol.addColumn(column);
                }
                termGridCols.put(term, termGridCol);
                courseGridCol.addColumn(termGridCol);
            }
        }

        for (EppEpvTotalRow epvRow : blockRows)
            blockTable.clValue(epvRow);

        Rectangle rect = blockTable.print(sheet, 0, ++rowIndex);
        rowIndex = rect.y + rect.height

        return rowIndex;
    }


    boolean isSelfWorkPresent(Collection<IEppEpvRowWrapper> blockRows) {
        boolean isSelfWorkPresent = false;
        for (IEppEpvRowWrapper row : blockRows) {
            if (row.getTotalLoad(null, eLoadSelfWorkType.getFullCode()) > 0) {
                // если есть хоть в одном семестре (вычисленная > 0)
                isSelfWorkPresent = true;
                break;
            }
        }
        return isSelfWorkPresent;
    }

    Column<IEppEpvRowWrapper> doPrepare_mainBlockColumns(String title) {
        Column<IEppEpvRowWrapper> mainBlockColumn_Col = new Column(title, 1, STYLE_TABLE_HEAD_NORM);

        Column<IEppEpvRowWrapper> index_SubCol = new Column(7, "Индекс", 4, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        index_SubCol.clValue = { IEppEpvRowWrapper epvRow ->
            if (epvRow.getRow() instanceof EppEpvHierarchyRow) {
                index_SubCol.addValue(new ColumnsValue(epvRow.getIndex(), STYLE_TABLE_BODY_BOLD));
            } else {
                index_SubCol.addValue(epvRow.getIndex());
            }
        }
        mainBlockColumn_Col.addColumn(index_SubCol);

        Column<IEppEpvRowWrapper> displayableTitle_SubCol = new Column(30, "Название", 4, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        displayableTitle_SubCol.clValue = { IEppEpvRowWrapper epvRow ->
            if (epvRow.getRow() instanceof EppEpvHierarchyRow) {
                displayableTitle_SubCol.addValue(new ColumnsValue(epvRow.getDisplayableTitle(), STYLE_TABLE_BODY_BOLD));
            } else {
                displayableTitle_SubCol.addValue(epvRow.getDisplayableTitle());
            }
        }
        mainBlockColumn_Col.addColumn(displayableTitle_SubCol);

        Column<IEppEpvRowWrapper> rowType_SubCol = new Column(2, "Тип", 4, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
        rowType_SubCol.clValue = { IEppEpvRowWrapper epvRow ->
            if (epvRow.getRow() instanceof EppEpvHierarchyRow) {
                rowType_SubCol.addValue(new ColumnsValue(epvRow.getRowType(), STYLE_TABLE_BODY_BOLD));
            } else {
                rowType_SubCol.addValue(epvRow.getRowType());
            }
        }
        mainBlockColumn_Col.addColumn(rowType_SubCol);

        Column<IEppEpvRowWrapper> registryNumber_SubCol = new Column(6, "№ (реестр)", 4, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        registryNumber_SubCol.clValue = { IEppEpvRowWrapper epvRow ->
            IEppEpvRow row = epvRow.getRow();
            if (row instanceof EppEpvRegistryRow) {
                EppRegistryElement registryElement = row.getRegistryElement();
                registryNumber_SubCol.addValue(registryElement != null ? registryElement.getNumberWithAbbreviation() : "")
            } else registryNumber_SubCol.addValue("")
        }
        mainBlockColumn_Col.addColumn(registryNumber_SubCol);

        Column<IEppEpvRowWrapper> parts_SubCol = new Column(2, "Частей", 4, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
        parts_SubCol.clValue = { IEppEpvRowWrapper epvRow ->
            IEppEpvRow row = epvRow.getRow();
            if (row instanceof EppEpvRegistryRow) {
                int rowSize = Math.max(0, epvRow.getActiveTermSet().size());
                EppRegistryElement registryElement = row.getRegistryElement();
                if (registryElement != null) {
                    int regSize = Math.max(1, registryElement.getParts());
                    if (rowSize != regSize) {
                        WritableCellFormat format = new WritableCellFormat(STYLE_TABLE_BODY);
                        format.setBackground(jxl.format.Colour.RED);
                        parts_SubCol.addValue(new ColumnsValue(String.valueOf(regSize) + "(" + String.valueOf(rowSize) + ")", format))
                    } else parts_SubCol.addValue(rowSize);
                } else {
                    parts_SubCol.addValue(rowSize)
                }
            } else {
                parts_SubCol.addValue("")
            }
        }
        mainBlockColumn_Col.addColumn(parts_SubCol);

        return mainBlockColumn_Col;
    }

    Column<IEppEpvRowWrapper> doPrepare_controlActionsColumns(List<EppControlActionType> controlActionTypes) {
        Column<IEppEpvRowWrapper> controlActions_Col = new Column("Формы контроля", 1, STYLE_TABLE_HEAD_NORM);
        for (EppControlActionType tp : controlActionTypes) {
            Column<IEppEpvRowWrapper> column = new Column(2, tp.getShortTitle(), 4, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
            String code = tp.getFullCode();
            column.clValue = { IEppEpvRowWrapper epvRow ->
                column.addValue(epvRow.isTermDataOwner() ? TermsFormatter.format(epvRow.getActionTermSet(code)) : "")
            }
            controlActions_Col.addColumn(column);
        }
        return controlActions_Col;
    }

    void setTotalRowLoad_byEppLoadType(boolean isHierarchyRow, IEppEpvRowWrapper epvRow, Column column, String typeCode) {
        WritableCellFormat format = isHierarchyRow ? STYLE_TABLE_BODY_BOLD : column.getValueFormat();
        double registryValue = epvRow.getTotalLoad(0, typeCode);
        ColumnsValue value;
        if (epvRow.isTermDataOwner()) {
            /* если элемент содержит данные по нагрузке. придется считать соответствие */
            double calculatedValue = epvRow.getTotalLoad(null, typeCode);
            if ((calculatedValue <= 0) || UniEppUtils.eq(registryValue, calculatedValue)) {
                // если не распределено или совпало - то выводим то, что есть
                value = new ColumnsValue(registryValue, format);
            } else {
                format = new WritableCellFormat(format);
                format.setBackground(jxl.format.Colour.RED);
                String calculatedValueStr = UniEppUtils.formatLoad(calculatedValue, false);
                String registryValueStr = UniEppUtils.formatLoad(registryValue, false);
                value = new ColumnsValue(calculatedValueStr + " (" + registryValueStr + ")", format);
            }
        } else {
            value = new ColumnsValue(registryValue, format);
        }
        column.addValue(value);
    }

    void addTermRowLoad_totalAuditLoad(IEppEpvRowWrapper row, int term, Column column) {
        if (row.isTermDataOwner()) {
            /* если элемент содержит данные по нагрузке. придется считать  */
            double cellValue = row.getTotalLoad(term, EppELoadType.FULL_CODE_AUDIT);
            double calculatedValue = row.getTotalLoad(term, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
            if ((calculatedValue <= 0) || UniEppUtils.eq(cellValue, calculatedValue)) {
                // если не распределено или совпало - то выводим то, что есть
                column.addValue(cellValue);
            } else {
                WritableCellFormat format = new WritableCellFormat(column.getValueFormat());
                format.setBackground(jxl.format.Colour.RED);
                String calculatedValueStr = UniEppUtils.formatLoad(calculatedValue, false);
                String registryValueStr = UniEppUtils.formatLoad(cellValue, false);
                column.addValue(new ColumnsValue(calculatedValueStr + " (" + registryValueStr + ")", format));
            }
        } else column.addValue("");
    }


    private static <T extends IEppEpvRowWrapper> List<IEppEpvRowWrapper> filterEduPlanBlockRows(
            Map<Long, T> versionDataMap, Predicate<IEppEpvRowWrapper> predicate) {
        final Map<Long, IEppEpvRowWrapper> result = new TreeMap<>();
        if (versionDataMap != null) {
            for (IEppEpvRowWrapper row : versionDataMap.values()) {
                if (predicate.evaluate(row)) {
                    // добавляем элемент и всех его родителей
                    while ((row != null) && (result.put(row.getId(), (T) row) == null))
                        row = row.getHierarhyParent();
                }
            }
        }
        return result.values().asList();
    }


    class Column<T> {

        int startWidth = 0;
        String title;
        int titleHeight;
        int valueHeight;
        WritableCellFormat titleFormat;
        WritableCellFormat valueFormat;
        List<Column<T>> columns = new ArrayList<>();
        List<Object> values = new ArrayList<>();
        Closure clValue = { T any ->
            if (columns.isEmpty()) {
                values.add(any);
            } else
                for (Column<T> column : columns) {
                    column.clValue(any);
                }
        };

        Column(String title, int titleHeight, WritableCellFormat titleFormat) {
            this.title = title;
            this.titleHeight = titleHeight;
            this.titleFormat = titleFormat;
        }

        Column(int startWidth,
               String title, int titleHeight, int valueHeight,
               WritableCellFormat titleFormat, WritableCellFormat valueFormat
        ) {
            this.startWidth = startWidth
            this.title = title
            this.titleHeight = titleHeight
            this.valueHeight = valueHeight
            this.titleFormat = titleFormat
            this.valueFormat = valueFormat
        }

        public boolean addColumn(Column<T> c) {
            return columns.add(c);
        }

        public boolean addValue(Object o) {
            return values.add(o);
        }

        int getColumnWidth() {
            if (columns.isEmpty()) return startWidth;

            int width = 0;
            for (Column column : columns) {
                if (column == null) continue;
                width += column.getColumnWidth();
            }
            return width;
        }

        Rectangle print(WritableSheet sheet, int colIndex, int rowIndex) {
            Rectangle rect = new Rectangle(colIndex, rowIndex, 0, 0);

            int row = rowIndex + titleHeight

            if (valueHeight > 0 && startWidth > 0) {
                for (Object value : values) {
                    printValue(sheet, colIndex, row, value)
                    row += valueHeight;
                }
                rect.width = startWidth;
                rect.height = row - rowIndex;
            }

            for (Column column : columns) {
                if (column == null) continue;
                Rectangle r = column.print(sheet, rect.x + rect.width as int, rowIndex + titleHeight);
                rect.width += r.width;
                rect.height = Math.max(rect.height, r.height);
            }

            if (title != null && titleHeight > 0 && rect.width > 0)
                addTextCell(sheet, colIndex, rowIndex, rect.width as int, titleHeight, title, titleFormat);

            return rect;
        }

        private void printValue(WritableSheet sheet, int left, int top, Object value) {
            if (value != null) {
                if (value instanceof ColumnsValue) printValue(sheet, left, top, value.value, value.format);
                else printValue(sheet, left, top, value, valueFormat);
            } else addTextCell(sheet, left, top, startWidth, valueHeight, "", valueFormat);
        }

        private void printValue(WritableSheet sheet, int left, int top, Object value, WritableCellFormat format) {
            if (value != null) {
                if (value instanceof String)
                    addTextCell(sheet, left, top, startWidth, valueHeight, (String) value, format);
                else if (value instanceof Double)
                    addNumberCell(sheet, left, top, startWidth, valueHeight, value, format);
                else if (value instanceof Integer)
                    addNumberCell(sheet, left, top, startWidth, valueHeight, value, format);
                else
                    addTextCell(sheet, left, top, startWidth, valueHeight, value.toString(), format);
            } else addTextCell(sheet, left, top, startWidth, valueHeight, "", format);
        }
    }

    class ColumnsValue {
        WritableCellFormat format;
        Object value;

        ColumnsValue(Object value, WritableCellFormat format) {
            this.value = value;
            this.format = format;
        }
    }

    private static void addTextCell(WritableSheet sheet, int left, int top, int width, int height, String text, CellFormat format) {
        sheet.addCell(new jxl.write.Label(left, top, text, format));
        sheet.mergeCells(left, top, left + width - 1, top + height - 1);
    }

    private static void addNumberCell(WritableSheet sheet, int left, int top, int width, int height, double number, CellFormat format) {
        sheet.addCell(new Number(left, top, number, format));
        sheet.mergeCells(left, top, left + width - 1, top + height - 1);
    }

//шрифты и форматы
    private final WritableFont FONT_NORM = new WritableFont(WritableFont.ARIAL, 10);
    private final WritableFont FONT_NORM_SMALL = new WritableFont(WritableFont.ARIAL, 6);
    private final WritableFont FONT_BOLD = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

    private final WritableCellFormat STYLE_BLOCK_TITLE = new WritableCellFormat(FONT_NORM);

    private final NumberFormat NUMBER_FORMAT_WITH_ZERO = new NumberFormat("0");
    private final NumberFormat NUMBER_FORMAT_WITHOUT_ZERO = new NumberFormat("#");

    private final WritableCellFormat STYLE_TABLE_HEAD_NORM = new WritableCellFormat(FONT_BOLD);
    private final WritableCellFormat STYLE_TABLE_HEAD_VERT = new WritableCellFormat(FONT_BOLD);
    private final WritableCellFormat STYLE_TABLE_HEAD_VERT_SMALL = new WritableCellFormat(FONT_NORM_SMALL);
    private final WritableCellFormat STYLE_TABLE_BODY = new WritableCellFormat(FONT_NORM, NUMBER_FORMAT_WITH_ZERO);
    private final WritableCellFormat STYLE_TABLE_BODY_WITHOUT_ZERO = new WritableCellFormat(FONT_NORM, NUMBER_FORMAT_WITHOUT_ZERO);
    private final WritableCellFormat STYLE_TABLE_BODY_BOLD = new WritableCellFormat(FONT_BOLD, NUMBER_FORMAT_WITH_ZERO);

    private final WritableCellFormat STYLE_ODD = new WritableCellFormat(FONT_NORM);
    private final WritableCellFormat STYLE_EVEN = new WritableCellFormat(FONT_NORM);

    private void initFormats() {
        STYLE_BLOCK_TITLE.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);

        STYLE_TABLE_HEAD_NORM.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        STYLE_TABLE_HEAD_NORM.setAlignment(jxl.format.Alignment.CENTRE);
        STYLE_TABLE_HEAD_NORM.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.DOTTED);
        STYLE_TABLE_HEAD_NORM.setWrap(true);

        STYLE_TABLE_HEAD_VERT.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        STYLE_TABLE_HEAD_VERT.setAlignment(jxl.format.Alignment.CENTRE);
        STYLE_TABLE_HEAD_VERT.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.DOTTED);
        STYLE_TABLE_HEAD_VERT.setOrientation(Orientation.PLUS_90);
        STYLE_TABLE_HEAD_VERT.setShrinkToFit(true);
//        STYLE_TABLE_HEAD_VERT.setWrap(true);

        STYLE_TABLE_HEAD_VERT_SMALL.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        STYLE_TABLE_HEAD_VERT_SMALL.setAlignment(jxl.format.Alignment.CENTRE);
        STYLE_TABLE_HEAD_VERT_SMALL.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.DOTTED);
        STYLE_TABLE_HEAD_VERT_SMALL.setOrientation(Orientation.PLUS_90);
        STYLE_TABLE_HEAD_VERT_SMALL.setWrap(true);

        STYLE_TABLE_BODY.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        STYLE_TABLE_BODY.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY.setWrap(true);

        STYLE_TABLE_BODY_WITHOUT_ZERO.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        STYLE_TABLE_BODY_WITHOUT_ZERO.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_WITHOUT_ZERO.setWrap(true);

        STYLE_TABLE_BODY_BOLD.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        STYLE_TABLE_BODY_BOLD.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_BOLD.setWrap(true);

        STYLE_ODD.setAlignment(jxl.format.Alignment.LEFT);
        STYLE_ODD.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        STYLE_ODD.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.DOTTED);

        STYLE_EVEN.setAlignment(jxl.format.Alignment.RIGHT);
        STYLE_EVEN.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        STYLE_EVEN.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.DOTTED);
    }

    final IFormatter TermsFormatter = new IFormatter<List<Integer>>() {
        @Override
        public String format(List<Integer> source) {
            if (source == null || source.isEmpty()) {
                return "";
            }

            StringBuilder result = new StringBuilder();
            int i = 0;
            while (i < source.size()) {
                int first = source.get(i);
                int last = first;
                i++;
                while (i < source.size() && source.get(i) - last <= 1) {
                    last = source.get(i);
                    i++;
                }

                if (result.length() > 0) {
                    result.append(", ");
                }
                result.append(first);
                if (first < last) {
                    result.append("-").append(last);
                }
            }
            return result.toString();
        }
    }


}


