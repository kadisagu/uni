/* $Id$ */
package uniepp.scripts.edu_plan

import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.*
import jxl.write.*
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.collections.Predicate
import org.apache.commons.collections.functors.InstanceofPredicate
import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.view.formatter.CollectionFormatter
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.dao.UniDaoFacade
import ru.tandemservice.uniepp.UniEppUtils
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper
import ru.tandemservice.uniepp.entity.catalog.*
import ru.tandemservice.uniepp.entity.workplan.*
import ru.tandemservice.uniepp.util.EppDevelopCombinationTitleUtil

import java.awt.Rectangle

/**
 * @author Andrey Andreev
 * @since 24.08.2015
 */
return new EppWorkPlanPrint(
        session: session,
        wpIds: wpIds,
        ).print()

/**
 * Печать Рабочего учебного плана
 */
class EppWorkPlanPrint
{

    Session session
    List<Long> wpIds

    private ByteArrayOutputStream out = new ByteArrayOutputStream();

    private final List<EppControlActionType> controlActionTypes = IEppWorkPlanDAO.instance.get().getActiveControlActionTypes(null);

    def print()
    {
        def wpList = IUniBaseDao.instance.get().getList(EppWorkPlan.class, wpIds);

        if (wpList.size() == 0) {
            throw new ApplicationException("Нет Рабочих учебных планов для печати");
        } else {
            initFormats();

            WorkbookSettings ws = new WorkbookSettings();
            ws.setMergedCellChecking(false);
            ws.setRationalization(false);
            ws.setGCDisabled(true);
            def workbook = Workbook.createWorkbook(out, ws);

            printRegistryWorkPlan(workbook, wpIds);

            workbook.write();
            workbook.close();

            StringBuilder fn = new StringBuilder().append("РУП");
            if (wpList.size() == 1) {
                fn.append("-").append(wpList.get(0).getFullNumber());
            } else {
                fn.append("-").append((new Date().format("yyyy.MM.dd")));
            }
            fn.append(".xls");

            return [document: out.toByteArray(), fileName: fn.toString()]

        }
    }

    public void printRegistryWorkPlan(WritableWorkbook book, Collection<Long> wpIds)
    {
        Map<Long, IEppWorkPlanWrapper> wpDataMap = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(wpIds);
        for (IEppWorkPlanWrapper wrapper : wpDataMap.values()) {
            WritableSheet sheet = createWritableSheet(book, "№" + wrapper.getWorkPlan().getFullNumber(), 0);
            int rowIndex = printSheetHeader(sheet, wrapper, 0);
            rowIndex++;
            rowIndex = printDisciplines(sheet, wrapper, ++rowIndex);
            printActions(sheet, wrapper, ++rowIndex);
        }
    }


    private static WritableSheet createWritableSheet(WritableWorkbook book, String title, int index)
    {
        final WritableSheet sheet = book.createSheet(title, index);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);
        sheet.getSettings().setScaleFactor(70);
        sheet.getSettings().setBottomMargin(0.3937);
        sheet.getSettings().setTopMargin(0.3937);
        sheet.getSettings().setLeftMargin(0.3937);
        sheet.getSettings().setRightMargin(0.3937);

        for (int i = 0; i < 256; i++) {
            sheet.setColumnView(i, 2);
        }
        return sheet;
    }

    private static int printSheetHeader(WritableSheet sheet, IEppWorkPlanWrapper wrapper, int rowIndex)
    {
        addTextCell(sheet, 0, rowIndex, 15, 1, "Рабочий учебный план: ", STYLE_TABLE_BODY);
        addTextCell(sheet, 20, rowIndex, 40, 1, wrapper.getTitle(), STYLE_TABLE_BODY);

        rowIndex++;

        String dir = wrapper.getWorkPlan().getBlock().getEduPlanVersion().getEduPlan().getEducationElementSimpleTitle();
        dir += wrapper.getWorkPlan().getBlock().isRootBlock() ? "" :
               ", " + wrapper.getWorkPlan().getBlock().getEducationElementSimpleTitle()
        addTextCell(sheet, 0, rowIndex, 15, 1, "Направление (специальность, профессия) и направленность (профиль, специализация): ",
                    STYLE_TABLE_BODY);
        addTextCell(sheet, 20, rowIndex, 60, 1, dir, STYLE_TABLE_BODY);

        rowIndex++;

        addTextCell(sheet, 0, rowIndex, 15, 1, "Форма обучения: ", STYLE_TABLE_BODY);
        addTextCell(sheet, 20, rowIndex, 60, 1, EppDevelopCombinationTitleUtil.getTitle(wrapper.getWorkPlan().getEduPlan()),
                    STYLE_TABLE_BODY);
        return rowIndex;
    }


    private int printDisciplines(WritableSheet sheet, IEppWorkPlanWrapper wrapper, int rowIndex)
    {
        int headHeight = 4;

        List<IEppWorkPlanRowWrapper> disciplines = new ArrayList<>()
        disciplines.addAll(CollectionUtils.select(wrapper.getRowMap().values(), IEppWorkPlanRowWrapper.DISCIPLINE_ROWS));
        if (disciplines.size() == 0) return rowIndex;

        EppWorkPlanBase workPlan = wrapper.getWorkPlan();
        boolean showTotalLabor = workPlan.getEduPlan().getGeneration().showTotalLabor();
        boolean useCyclicLoading = IEppSettingsDAO.instance.get().getGlobalSettings().isUseCyclicLoading();

        List<EppWorkPlanPart> workPlanPartsList = UniDaoFacade.getCoreDao().getList(
                EppWorkPlanPart.class, EppWorkPlanPart.workPlan().id().s(), workPlan.getId(), EppWorkPlanPart.number().s());

        List<EppLoadType> loadTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppLoadType.class);
        Collection<EppELoadType> eLoadTypes = CollectionUtils.select(loadTypes, new InstanceofPredicate(EppELoadType.class));
        Collection<EppALoadType> aLoadTypes = CollectionUtils.select(loadTypes, new InstanceofPredicate(EppALoadType.class));
        EppELoadType eLoadTotalAuditType = (EppELoadType) CollectionUtils.find(eLoadTypes, ELOAD_TOTALAUDIT_PREDICATE);
        List<EppControlActionType> disciplineCATypes = (List<EppControlActionType>) CollectionUtils.select(controlActionTypes,
                                                                                                           DISCIPLINE_CA_TYPE_PREDICATE);


        Column index_Col = new Column(5, "Индекс", headHeight, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        Column displayableTitle_Col = new Column(18, "Название", headHeight, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        Column NumberRegistry_Col = new Column(6, "№ (реестр)", headHeight, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        Column LoadTermCol = new Column("Выдать часов за семестр", 1, STYLE_TABLE_HEAD_NORM);

        Column totalLabor_SubCol = new Column(2, "Трудоемкость", headHeight - 1, 1, STYLE_TABLE_HEAD_VERT_SMALL, STYLE_TABLE_BODY);
        Column totalSize_SubCol = new Column(2, "Часов (всего)", headHeight - 1, 1, STYLE_TABLE_HEAD_VERT_SMALL, STYLE_TABLE_BODY);
        if (showTotalLabor) {
            LoadTermCol.addColumn(totalLabor_SubCol);
        }
        LoadTermCol.addColumn(totalSize_SubCol);

        Column colAuditType_SubCol = new Column("Аудиторных", 1, STYLE_TABLE_HEAD_NORM);
        LoadTermCol.addColumn(colAuditType_SubCol);

        Column totalAuditType_SubSubCol = new Column(2, eLoadTotalAuditType.getShortTitle(), headHeight - 2, 1, STYLE_TABLE_HEAD_VERT_SMALL,
                                                     STYLE_TABLE_BODY);
        colAuditType_SubCol.addColumn(totalAuditType_SubSubCol);

        Map<EppALoadType, Column> auditTypeSubSubCols = new HashMap<>();
        for (EppALoadType aLoadType : aLoadTypes) {
            Column column = new Column(2, aLoadType.getShortTitle().replace(".", ". "), headHeight - 2, 1, STYLE_TABLE_HEAD_VERT_SMALL,
                                       STYLE_TABLE_BODY);
            auditTypeSubSubCols.put(aLoadType, column);
            colAuditType_SubCol.addColumn(column);
        }

        Map<EppELoadType, Column> eLoadTotalAuditSubCols = new HashMap<>();
        for (EppELoadType eLoadType : eLoadTypes) {
            if (eLoadTotalAuditType != eLoadType) {
                Column column = new Column(2, eLoadType.getShortTitle(), headHeight - 1, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
                eLoadTotalAuditSubCols.put(eLoadType, column);
                LoadTermCol.addColumn(column);
            }
        }

        List<Map<EppLoadType, Column>> loadTotalAuditworkPlanPartSubCols = new ArrayList<>();
        for (final EppWorkPlanPart workPlanPart : workPlanPartsList) {
            Column loadTotalAuditworkPlanPartSubCol = new Column(String.valueOf(workPlanPart.getNumber()) + "-ая часть", 1,
                                                                 STYLE_TABLE_HEAD_NORM);
            Map<EppLoadType, Column> loadTotalAuditworkPlanPartSubSubCols = new HashMap<>();
            for (EppALoadType aLoadType : aLoadTypes) {
                Column column = new Column(2, aLoadType.getShortTitle().replace(".", ". "), headHeight - 2, 1, STYLE_TABLE_HEAD_VERT_SMALL,
                                           STYLE_TABLE_BODY);
                loadTotalAuditworkPlanPartSubCol.addColumn(column);
                loadTotalAuditworkPlanPartSubSubCols.put(aLoadType, column);
            }
            for (EppELoadType eLoadType : eLoadTypes) {
                if (eLoadTotalAuditType != eLoadType) {
                    Column column = new Column(2, eLoadType.getShortTitle(), headHeight - 2, 1, STYLE_TABLE_HEAD_VERT_SMALL,
                                               STYLE_TABLE_BODY);
                    loadTotalAuditworkPlanPartSubSubCols.put(eLoadType, column);
                    loadTotalAuditworkPlanPartSubCol.addColumn(column);
                }
            }
            loadTotalAuditworkPlanPartSubCols.add(loadTotalAuditworkPlanPartSubSubCols);
            LoadTermCol.addColumn(loadTotalAuditworkPlanPartSubCol);
        }

        Column controlActions_Col = new Column("Формы контроля", 1, STYLE_TABLE_HEAD_NORM);

        Column intermediateActions_SubCol = new Column("Текущий контроль", 1, STYLE_TABLE_HEAD_NORM);
        controlActions_Col.addColumn(intermediateActions_SubCol);
        Column finalActions_SubCol = new Column("Итоговый контроль", 1, STYLE_TABLE_HEAD_NORM);
        controlActions_Col.addColumn(finalActions_SubCol);
        Map<EppControlActionType, Column> actionTypeCols = new HashMap<>();
        for (EppControlActionType actionType : disciplineCATypes) {
            Column column = new Column(2, actionType.getShortTitle(), 2, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);
            if (actionType instanceof EppFControlActionType) {
                finalActions_SubCol.addColumn(column);
            } else {
                intermediateActions_SubCol.addColumn(column);
            }
            actionTypeCols.put(actionType, column);
        }

        Column owner_Col = new Column(32, "Читающее подразделение", headHeight, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        Column workPlanRowKind_Col = new Column(2, "Вид дисциплины", headHeight, 1, STYLE_TABLE_HEAD_VERT, STYLE_TABLE_BODY);


        for (IEppWorkPlanRowWrapper discipline : disciplines) {
            IEppRegElPartWrapper regElPart = discipline.getRegistryElementPart();

            index_Col.addValue(discipline.getNumber());
            displayableTitle_Col.addValue(discipline.getTitle());

            String regElNumber = "";
            if (discipline.getRow() instanceof EppWorkPlanRegistryElementRow)
                regElNumber = ((EppWorkPlanRegistryElementRow) discipline.getRow()).getRegistryElement().getNumberWithAbbreviation();
            NumberRegistry_Col.addValue(regElNumber);

            totalSize_SubCol.addValue((regElPart != null) ? regElPart.getItem().getSizeAsDouble() : 0);
            if (showTotalLabor) totalLabor_SubCol.addValue((regElPart != null) ? regElPart.getItem().getLaborAsDouble() : 0);

            totalAuditType_SubSubCol.addValue(discipline.getTotalPartLoad(null, eLoadTotalAuditType.getFullCode()));

            for (EppALoadType aLoadType : aLoadTypes) {
                double partLoad = discipline.getTotalPartLoad(null, aLoadType.getFullCode());
                Column column = auditTypeSubSubCols.get(aLoadType);
                if (regElPart == null) {
                    column.addValue(partLoad);
                } else {
                    double registryLoad = regElPart.getLoadAsDouble(aLoadType.getFullCode());
                    if (partLoad == registryLoad) {
                        column.addValue(partLoad);
                    } else {
                        String load = UniEppUtils.formatLoad((partLoad), false) + "(" + UniEppUtils.formatLoad((registryLoad), false) + ")"
                        column.addValue(load);
                    }
                }
            }

            for (EppELoadType eLoadType : eLoadTypes) {
                if (eLoadTotalAuditType != eLoadType) {
                    eLoadTotalAuditSubCols.get(eLoadType).addValue(discipline.getTotalPartLoad(null, eLoadType.getFullCode()));
                }
            }

            int index = 0;
            for (EppWorkPlanPart workPlanPart : workPlanPartsList) {
                Map<EppLoadType, Column> loadTotalAuditworkPlanPartSubSubCols = loadTotalAuditworkPlanPartSubCols.get(index);
                for (EppALoadType aLoadType : aLoadTypes) {
                    if (useCyclicLoading) {
                        double partLoad = discipline.getTotalPartLoad(workPlanPart.getNumber(), aLoadType.getFullCode());
                        // цикловая нагрузка
                        double partDays = discipline.getTotalPartLoad(
                                workPlanPart.getNumber(), aLoadType.getFullCode() + IEppWorkPlanRowWrapper.DAYS_LOAD_POSTFIX);
                        if (partLoad <= 0 && partDays <= 0) {
                            loadTotalAuditworkPlanPartSubSubCols.get(aLoadType).addValue("");
                        } else {
                            String formatDays = (partDays <= 0) ? "" : "/" + UniEppUtils.formatLoad(partDays, false);
                            loadTotalAuditworkPlanPartSubSubCols.get(aLoadType).addValue(
                                    UniEppUtils.formatLoad(partLoad, true) + formatDays);
                        }
                    } else {
                        loadTotalAuditworkPlanPartSubSubCols.get(aLoadType).addValue(
                                discipline.getTotalPartLoad(workPlanPart.getNumber(), aLoadType.getFullCode()));
                    }
                }
                for (EppELoadType eLoadType : eLoadTypes) {
                    if (eLoadTotalAuditType != eLoadType) {
                        loadTotalAuditworkPlanPartSubSubCols.get(eLoadType).addValue(
                                discipline.getTotalPartLoad(workPlanPart.getNumber(), eLoadType.getFullCode()));
                    }
                }
                index++;
            }

            for (EppControlActionType actionType : disciplineCATypes) {
                actionTypeCols.get(actionType).addValue(discipline.getActionSize(actionType.getFullCode()));
            }

            OrgUnit owner = discipline.getOwner()
            if (owner == null || owner instanceof TopOrgUnit) owner_Col.addValue("");
            else owner_Col.addValue(owner.getFullTitle());

            workPlanRowKind_Col.addValue(discipline.getRow().getKind().getShortTitle());
        }


        Column disciplinesTable = new Column(null, 0, null);
        disciplinesTable.addColumn(index_Col);
        disciplinesTable.addColumn(displayableTitle_Col);
        disciplinesTable.addColumn(NumberRegistry_Col);
        disciplinesTable.addColumn(LoadTermCol);
        disciplinesTable.addColumn(controlActions_Col);
        disciplinesTable.addColumn(owner_Col);
        disciplinesTable.addColumn(workPlanRowKind_Col);

        Rectangle rect = disciplinesTable.print(sheet, 0, rowIndex);
        return rowIndex + rect.height;
    }

    private int printActions(WritableSheet sheet, IEppWorkPlanWrapper wrapper, int rowIndex)
    {
        int headHeight = 2;

        List<IEppWorkPlanRowWrapper> actions = new ArrayList<>()
        actions.addAll(CollectionUtils.select(wrapper.getRowMap().values(), IEppWorkPlanRowWrapper.NON_DISCIPLINE_ROWS));
        if (actions.size() == 0) return rowIndex;

        List<EppControlActionType> actionsCATypes = CollectionUtils.select(controlActionTypes, ACTIONS_CA_TYPE_PREDICATE);
        CollectionFormatter collectionFormatter = CollectionFormatter.COLLECTION_FORMATTER;

        Column index_Col = new Column(5, "Индекс", headHeight, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        Column displayableTitle_Col = new Column(18, "Название", headHeight, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        Column NumberRegistry_Col = new Column(6, "№ (реестр)", headHeight, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);

        Column dateFromTo_Col = new Column("Время проведения", 1, STYLE_TABLE_HEAD_NORM);
        Column dateFrom_SubCol = new Column(9, "Дата начала", headHeight - 1, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        dateFromTo_Col.addColumn(dateFrom_SubCol);
        Column dateTo_SubCol = new Column(9, "Дата окончания", headHeight - 1, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        dateFromTo_Col.addColumn(dateTo_SubCol);

        Column controlAction_Col = new Column(15, "Контрольное мероприятие", headHeight, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        Column owner_Col = new Column(32, "Читающее подразделение", headHeight, 1, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);

        for (IEppWorkPlanRowWrapper action : actions) {

            index_Col.addValue(action.getNumber());
            displayableTitle_Col.addValue(action.getTitle());

            String regElNumber = "";
            if (action.getRow() instanceof EppWorkPlanRegistryElementRow)
                regElNumber = ((EppWorkPlanRegistryElementRow) action.getRow()).getRegistryElement().getNumberWithAbbreviation();
            NumberRegistry_Col.addValue(regElNumber);

            dateFrom_SubCol.addValue(DateFormatter.DEFAULT_DATE_FORMATTER.format(((EppWorkPlanRow) action.getRow()).getBeginDate()));
            dateTo_SubCol.addValue(DateFormatter.DEFAULT_DATE_FORMATTER.format(((EppWorkPlanRow) action.getRow()).getEndDate()));

            if (action.getRow() instanceof EppWorkPlanRegistryElementRow) {
                final List<EppControlActionType> actionList = new ArrayList<>();
                for (final EppControlActionType tp : actionsCATypes) {
                    if (action.getActionSize(tp.getFullCode()) > 0) {
                        actionList.add(tp);
                    }
                }
                controlAction_Col.addValue(collectionFormatter.format(actionList));
            }

            OrgUnit owner = action.getOwner()
            if (owner == null || owner instanceof TopOrgUnit) owner_Col.addValue("");
            else owner_Col.addValue(owner.getFullTitle());
        }



        Column actionsTable = new Column(null, 0, null);
        actionsTable.addColumn(index_Col);
        actionsTable.addColumn(displayableTitle_Col);
        actionsTable.addColumn(NumberRegistry_Col);
        actionsTable.addColumn(dateFromTo_Col);
        actionsTable.addColumn(controlAction_Col);
        actionsTable.addColumn(owner_Col);

        Rectangle rect = actionsTable.print(sheet, 0, rowIndex);
        return rowIndex + rect.height;
    }


    class Column
    {

        int startWidth = 0;
        String title;
        int titleHeight;
        int valueHeight;
        WritableCellFormat titleFormat = new WritableCellFormat();
        WritableCellFormat valueFormat = new WritableCellFormat();
        private List<Column> columns = new ArrayList<>();
        private List<Object> values = new ArrayList<>();

        Column(String title, int titleHeight, WritableCellFormat titleFormat)
        {
            this.title = title;
            this.titleHeight = titleHeight;
            this.titleFormat = titleFormat;
        }

        Column(int startWidth,
               String title, int titleHeight, int valueHeight,
               WritableCellFormat titleFormat, WritableCellFormat valueFormat
              )
        {
            this.startWidth = startWidth
            this.title = title
            this.titleHeight = titleHeight
            this.valueHeight = valueHeight
            this.titleFormat = titleFormat
            this.valueFormat = valueFormat
        }

        public boolean addColumn(Column c)
        {
            return columns.add(c);
        }

        public boolean addValue(Object o)
        {
            return values.add(o);
        }

        int getColumnWidth()
        {
            if (columns.isEmpty()) return startWidth;

            int width = 0;
            for (Column column : columns) {
                if (column == null) continue;
                width += column.getColumnWidth();
            }
            return width;
        }

        Rectangle print(WritableSheet sheet, int colIndex, int rowIndex)
        {
            Rectangle rect = new Rectangle(colIndex, rowIndex, 0, 0);

            int row = rowIndex + titleHeight

            if (valueHeight > 0 && startWidth > 0 && !values.isEmpty()) {
                for (Object value : values) {
                    printValue(sheet, colIndex, row, value)
                    row += valueHeight;
                }
                rect.width = startWidth;
                rect.height = row - rowIndex;
            }

            for (Column column : columns) {
                if (column == null) continue;
                Rectangle r = column.print(sheet, rect.x + rect.width as int, rowIndex + titleHeight);
                rect.width += r.width;
                rect.height = Math.max(rect.height, r.height);
            }

            if (title != null && titleHeight > 0 && rect.width > 0)
                addTextCell(sheet, colIndex, rowIndex, rect.width as int, titleHeight, title, titleFormat);

            return rect;
        }

        private void printValue(WritableSheet sheet, int left, int top, Object value)
        {
            if (value != null) {
                if (value instanceof String)
                    addTextCell(sheet, left, top, startWidth, valueHeight, (String) value, valueFormat);
                else if (value instanceof Double)
                    addNumberCell(sheet, left, top, startWidth, valueHeight, value, valueFormat);
                else if (value instanceof Integer)
                    addNumberCell(sheet, left, top, startWidth, valueHeight, value, valueFormat);
                else
                    addTextCell(sheet, left, top, startWidth, valueHeight, value.toString(), valueFormat);
            }
        }
    }

    private static void addTextCell(WritableSheet sheet, int left, int top, int width, int height, String text, CellFormat format)
    {
        sheet.addCell(new Label(left, top, text, format));
        sheet.mergeCells(left, top, left + width - 1, top + height - 1);
    }

    private static void addNumberCell(WritableSheet sheet, int left, int top, int width, int height, double number, CellFormat format)
    {
        sheet.addCell(new Number(left, top, number, format));
        sheet.mergeCells(left, top, left + width - 1, top + height - 1);
    }

    //шрифты и форматы
    private static final WritableFont FONT_NORM = new WritableFont(WritableFont.ARIAL, 10);
    private static final WritableFont FONT_NORM_SMALL = new WritableFont(WritableFont.ARIAL, 6);
    private static final WritableFont FONT_BOLD = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

    private static final WritableCellFormat STYLE_TABLE_HEAD_NORM = new WritableCellFormat(FONT_BOLD);
    private static final WritableCellFormat STYLE_TABLE_HEAD_VERT = new WritableCellFormat(FONT_BOLD);
    private static final WritableCellFormat STYLE_TABLE_HEAD_VERT_SMALL = new WritableCellFormat(FONT_NORM_SMALL);
    private static final WritableCellFormat STYLE_TABLE_BODY = new WritableCellFormat(FONT_NORM, new NumberFormat("#"));

    private static void initFormats()
    {
        STYLE_TABLE_HEAD_NORM.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        STYLE_TABLE_HEAD_NORM.setAlignment(jxl.format.Alignment.CENTRE);
        STYLE_TABLE_HEAD_NORM.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.DOTTED);
        STYLE_TABLE_HEAD_NORM.setWrap(true);

        STYLE_TABLE_HEAD_VERT.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        STYLE_TABLE_HEAD_VERT.setAlignment(jxl.format.Alignment.CENTRE);
        STYLE_TABLE_HEAD_VERT.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.DOTTED);
        STYLE_TABLE_HEAD_VERT.setOrientation(Orientation.PLUS_90);
        STYLE_TABLE_HEAD_VERT.setShrinkToFit(true);
        //        STYLE_TABLE_HEAD_VERT.setWrap(true);

        STYLE_TABLE_HEAD_VERT_SMALL.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        STYLE_TABLE_HEAD_VERT_SMALL.setAlignment(jxl.format.Alignment.CENTRE);
        STYLE_TABLE_HEAD_VERT_SMALL.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.DOTTED);
        STYLE_TABLE_HEAD_VERT_SMALL.setOrientation(Orientation.PLUS_90);
        STYLE_TABLE_HEAD_VERT_SMALL.setWrap(true);

        STYLE_TABLE_BODY.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        STYLE_TABLE_BODY.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY.setWrap(true);
    }

    //предикаты
    protected static final Predicate ELOAD_TOTALAUDIT_PREDICATE = new Predicate() {
        @Override
        public boolean evaluate(final Object object)
        { return (((EppELoadType) object).isAuditTotal()); }
    };

    protected static final Predicate DISCIPLINE_CA_TYPE_PREDICATE = new Predicate() {
        @Override
        public boolean evaluate(final Object object)
        {
            return (((EppControlActionType) object).isUsedWithDisciplines());
        }
    };

    protected static final Predicate ACTIONS_CA_TYPE_PREDICATE = new Predicate() {
        @Override
        public boolean evaluate(final Object object)
        {
            return (((EppControlActionType) object).isUsedWithAttestation() || ((EppControlActionType) object).isUsedWithPractice());
        }
    };

}