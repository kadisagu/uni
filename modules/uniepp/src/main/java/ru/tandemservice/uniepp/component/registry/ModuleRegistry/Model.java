package ru.tandemservice.uniepp.component.registry.ModuleRegistry;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniepp.component.base.SimpleOrgUnitBasedModel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;

/**
 * @author vdanilov
 */
@State({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="orgUnitHolder.id")
})
public class Model extends SimpleOrgUnitBasedModel {

    private final EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<OrgUnit>();
    public EntityHolder<OrgUnit> getOrgUnitHolder() { return this.orgUnitHolder; }

    public Long getOrgUnitId() { return this.getOrgUnitHolder().getId(); }
    @Override public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }


    private DynamicListDataSource<EppRegistryModule> dataSource;
    public DynamicListDataSource<EppRegistryModule> getDataSource() { return this.dataSource; }
    public void setDataSource(final DynamicListDataSource<EppRegistryModule> dataSource) { this.dataSource = dataSource; }

    private IDataSettings settings;
    public IDataSettings getSettings() { return this.settings; }
    public void setSettings(final IDataSettings settings) { this.settings = settings; }

    private ISelectModel ownerSelectModel;
    public ISelectModel getOwnerSelectModel() { return this.ownerSelectModel; }
    public void setOwnerSelectModel(final ISelectModel ownerSelectModel) { this.ownerSelectModel = ownerSelectModel; }

    private ISelectModel stateListModel;
    public ISelectModel getStateListModel() { return this.stateListModel; }
    public void setStateListModel(final ISelectModel stateListModel) { this.stateListModel = stateListModel; }

    private CommonPostfixPermissionModelBase sec = new CommonPostfixPermissionModel(""); //todo заглушка
    public CommonPostfixPermissionModelBase getSec() { return this.sec; }
    public void setSec(final CommonPostfixPermissionModelBase sec) { this.sec = sec; }
}
