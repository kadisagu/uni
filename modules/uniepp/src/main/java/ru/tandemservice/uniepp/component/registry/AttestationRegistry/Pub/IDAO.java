package ru.tandemservice.uniepp.component.registry.AttestationRegistry.Pub;

import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;

/**
 * 
 * @author nkokorina
 *
 */

public interface IDAO extends ru.tandemservice.uniepp.component.registry.base.Pub.IDAO<EppRegistryAttestation>
{

}
