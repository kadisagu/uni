package ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit;

import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;

/**
 * @author vdanilov
 */
public class DAO extends ru.tandemservice.uniepp.component.registry.base.AddEdit.DAO<EppRegistryDiscipline> implements IDAO {
    @Override public void prepare(final ru.tandemservice.uniepp.component.registry.base.AddEdit.Model<EppRegistryDiscipline> model) {
        super.prepare(model);


    }

    @Override
    public void save(final ru.tandemservice.uniepp.component.registry.base.AddEdit.Model<EppRegistryDiscipline> model) {
        super.save(model);


    }
}
