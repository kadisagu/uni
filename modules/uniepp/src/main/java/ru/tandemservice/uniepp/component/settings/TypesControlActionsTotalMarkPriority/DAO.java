package ru.tandemservice.uniepp.component.settings.TypesControlActionsTotalMarkPriority;

import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.hibsupport.builder.MQBuilder;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;

/**
 * 
 * @author nkokorina
 * @since 16.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(final Model model)
    {
        final MQBuilder builder = new MQBuilder(EppFControlActionType.ENTITY_CLASS, "eyep");
        builder.addOrder("eyep", EppFControlActionType.P_TOTAL_MARK_PRIORITY);
        Session session = getSession();
        long count = builder.getResultCount(session);
        model.getDataSource().setTotalSize(count);
        model.getDataSource().setCountRow(count);
        UniBaseUtils.createPage(model.getDataSource(), builder, session);

        for (final ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource())) {
            wrapper.setViewProperty("finalAction", wrapper.getEntity() instanceof EppFControlActionType);
        }
    }
 @Override
    public void updatePriorityUp(final Long markValueId)
    {
        this.updatePriority(this.get(markValueId), -1);
    }

    @Override
    public void updatePriorityVeryUp(final Long markValueId)
    {
        final Session session = this.getSession();
        final EppFControlActionType markValue = this.get(markValueId);
        final MQBuilder builder = new MQBuilder(EppFControlActionType.ENTITY_CLASS, "v");
        builder.addOrder("v", EppFControlActionType.P_TOTAL_MARK_PRIORITY);
        final List<EppFControlActionType> result = builder.getResultList(this.getSession());
        markValue.setTotalMarkPriority(result.get(0).getTotalMarkPriority()-1);
        session.update(markValue);
        session.flush();
    }

    @Override
    public void updatePriorityDown(final Long markValueId)
    {
        this.updatePriority(this.get(markValueId), 1);
    }

    @Override
    public void updatePriorityVeryDown(final Long markValueId)
    {
        final Session session = this.getSession();
        final EppFControlActionType markValue = this.get(markValueId);
        final MQBuilder builder = new MQBuilder(EppFControlActionType.ENTITY_CLASS, "v");
        builder.addOrder("v", EppFControlActionType.P_TOTAL_MARK_PRIORITY);
        final List<EppFControlActionType> result = builder.getResultList(this.getSession());
        markValue.setTotalMarkPriority(result.get(result.size()-1).getTotalMarkPriority()+1);
        session.update(markValue);
        session.flush();
    }

    private void updatePriority(final EppFControlActionType markValue, final int direction)
    {
        final Session session = this.getSession();
        final MQBuilder builder = new MQBuilder(EppFControlActionType.ENTITY_CLASS, "v");
        builder.addOrder("v", EppFControlActionType.P_TOTAL_MARK_PRIORITY);
        final List<EppFControlActionType> result = builder.getResultList(this.getSession());
        final int near = result.indexOf(markValue)+direction;
        if (near>=0 && near<result.size())
        {
            final int priorNear = result.get(near).getTotalMarkPriority(), priorCur = markValue.getTotalMarkPriority();
            result.get(near).setTotalMarkPriority(result.get(0).getTotalMarkPriority()-1);
            session.update(result.get(near));
            session.flush();
            markValue.setTotalMarkPriority(priorNear);
            session.update(markValue);
            session.flush();
            result.get(near).setTotalMarkPriority(priorCur);
            session.update(result.get(near));
            session.flush();
        }
    }

    @Override
    public IEntityHandler getUpDisabledEntityHandler()
    {
        return (entity ->
            {
                final MQBuilder builder = new MQBuilder(EppFControlActionType.ENTITY_CLASS, "v");
                builder.addOrder("v", EppFControlActionType.P_TOTAL_MARK_PRIORITY);
                final List<EppFControlActionType> result = builder.getResultList(DAO.this.getSession());
                return ((EppFControlActionType)((ViewWrapper) entity).getEntity()).getTotalMarkPriority()==result.get(0).getTotalMarkPriority();
            });
    }

    @Override
    public IEntityHandler getDownDisabledEntityHandler()
    {
        return (entity ->
            {
                final MQBuilder builder = new MQBuilder(EppFControlActionType.ENTITY_CLASS, "v");
                builder.addOrder("v", EppFControlActionType.P_TOTAL_MARK_PRIORITY);
                final List<EppFControlActionType> result = builder.getResultList(DAO.this.getSession());
                return ((EppFControlActionType)((ViewWrapper) entity).getEntity()).getTotalMarkPriority()==result.get(result.size()-1).getTotalMarkPriority();
            });
    }
}
