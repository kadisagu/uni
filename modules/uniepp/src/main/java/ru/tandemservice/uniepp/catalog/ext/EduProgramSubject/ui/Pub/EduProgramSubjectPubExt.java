/* $Id$ */
package ru.tandemservice.uniepp.catalog.ext.EduProgramSubject.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.Pub.EduProgramSubjectPub;
import ru.tandemservice.uniepp.catalog.bo.EppEduProgramSubjectDevelopResult.ui.Tab.EppEduProgramSubjectDevelopResultTab;

/**
 * @author Igor Belanov
 * @since 22.02.2017
 */
@Configuration
public class EduProgramSubjectPubExt extends BusinessComponentExtensionManager
{
    public static final String EPP_DEVELOP_RESULT_TAB = "eppDevelopResultTab";

    @Autowired
    public EduProgramSubjectPub _eduProgramSubjectPub;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_eduProgramSubjectPub.eduProgramSubjectTabPanelExtPoint())
                .addTab(componentTab(EPP_DEVELOP_RESULT_TAB, EppEduProgramSubjectDevelopResultTab.class))
                .create();
    }
}
