package ru.tandemservice.uniepp.entity.std.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.std.data.EppStdHierarchyRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdStructureRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись ГОС (индекс, цикл, компонент, ...)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStdStructureRowGen extends EppStdHierarchyRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.std.data.EppStdStructureRow";
    public static final String ENTITY_NAME = "eppStdStructureRow";
    public static final int VERSION_HASH = -307043873;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";
    public static final String L_VALUE = "value";

    private EppStdStructureRow _parent;     // Запись ГОС (индекс, цикл, компонент, ...)
    private EppPlanStructure _value;     // Структура ГОС/УП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись ГОС (индекс, цикл, компонент, ...).
     */
    public EppStdStructureRow getParent()
    {
        return _parent;
    }

    /**
     * @param parent Запись ГОС (индекс, цикл, компонент, ...).
     */
    public void setParent(EppStdStructureRow parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Структура ГОС/УП. Свойство не может быть null.
     */
    @NotNull
    public EppPlanStructure getValue()
    {
        return _value;
    }

    /**
     * @param value Структура ГОС/УП. Свойство не может быть null.
     */
    public void setValue(EppPlanStructure value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppStdStructureRowGen)
        {
            setParent(((EppStdStructureRow)another).getParent());
            setValue(((EppStdStructureRow)another).getValue());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStdStructureRowGen> extends EppStdHierarchyRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStdStructureRow.class;
        }

        public T newInstance()
        {
            return (T) new EppStdStructureRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return obj.getParent();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "parent":
                    obj.setParent((EppStdStructureRow) value);
                    return;
                case "value":
                    obj.setValue((EppPlanStructure) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return EppStdStructureRow.class;
                case "value":
                    return EppPlanStructure.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStdStructureRow> _dslPath = new Path<EppStdStructureRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStdStructureRow");
    }
            

    /**
     * @return Запись ГОС (индекс, цикл, компонент, ...).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdStructureRow#getParent()
     */
    public static EppStdStructureRow.Path<EppStdStructureRow> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Структура ГОС/УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdStructureRow#getValue()
     */
    public static EppPlanStructure.Path<EppPlanStructure> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EppStdStructureRow> extends EppStdHierarchyRow.Path<E>
    {
        private EppStdStructureRow.Path<EppStdStructureRow> _parent;
        private EppPlanStructure.Path<EppPlanStructure> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись ГОС (индекс, цикл, компонент, ...).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdStructureRow#getParent()
     */
        public EppStdStructureRow.Path<EppStdStructureRow> parent()
        {
            if(_parent == null )
                _parent = new EppStdStructureRow.Path<EppStdStructureRow>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Структура ГОС/УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdStructureRow#getValue()
     */
        public EppPlanStructure.Path<EppPlanStructure> value()
        {
            if(_value == null )
                _value = new EppPlanStructure.Path<EppPlanStructure>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EppStdStructureRow.class;
        }

        public String getEntityName()
        {
            return "eppStdStructureRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
