/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask.ui.AddEdit;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicItemAddEdit.CatalogDynamicItemAddEditUI;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask.EppProfessionalTaskManager;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.catalog.EppProfessionalTask;

/**
 * @author Igor Belanov
 * @since 23.03.2017
 */
@State({
        @Bind(key = EppProfessionalTaskAddEditUI.EDU_PROGRAM_SUBJECT_ID, binding = "programSubjectId")
})
public class EppProfessionalTaskAddEditUI extends CatalogDynamicItemAddEditUI<EppProfessionalTask>
{
    // как и в случае списка, к нам может прийти конкретное направление
    public static final String EDU_PROGRAM_SUBJECT_ID = "programSubjectId";

    private Long _programSubjectId;

    // поля, которые не будут записаны в сущность
    private EduProgramKind _programKind;
    private EduProgramSubjectIndex _programSubjectIndex;
    private EduProgramSubject _programSubject;
    private Boolean _inDepthStudy;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        EppProfActivityType profActivityType = getCatalogItem().getProfActivityType();
        if (profActivityType != null)
            setInDepthStudy(profActivityType.getInDepthStudy());
        EduProgramSubject programSubject = profActivityType != null ? profActivityType.getProgramSubject() : null;
        if (programSubject != null || _programSubjectId != null)
        {
            if (programSubject == null)
                programSubject = DataAccessServices.dao().getNotNull(_programSubjectId);
            setProgramSubject(programSubject);
            setProgramSubjectIndex(programSubject.getSubjectIndex());
            setProgramKind(programSubject.getSubjectIndex().getProgramKind());
        }
        onChangeProgramKind();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppProfessionalTaskAddEdit.PROGRAM_SUBJECT_INDEX_DS.equals(dataSource.getName()) ||
                EppProfessionalTaskAddEdit.PROGRAM_SUBJECT_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppProfessionalTaskManager.PROP_PROGRAM_KIND, _programKind);
            dataSource.put(EppProfessionalTaskManager.PROP_PROGRAM_SUBJECT_INDEX, _programSubjectIndex);
        }
        else if (EppProfessionalTaskAddEdit.PROF_ACTIVITY_TYPE_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppProfActivityType.PROP_PROGRAM_KIND, _programKind);
            dataSource.put(EppProfActivityType.PROP_PROGRAM_SUBJECT_INDEX, _programSubjectIndex);
            dataSource.put(EppProfActivityType.PROP_PROGRAM_SUBJECT, _programSubject);
            dataSource.put(EppProfActivityType.PROP_IN_DEPTH_STUDY, _inDepthStudy);
        }
    }

    public boolean isDisableProgramSubjectFields()
    {
        // поля выключены либо если это форма редактирования
        // либо если мы работаем в пределах конкретного направления подготовки
        return !isAddForm() || (_programSubjectId != null);
    }

    public boolean isVisibleInDepthStudy()
    {
        return getInDepthStudy() != null;
    }

    public boolean isDisableProfActivityType()
    {
        return !isAddForm();
    }

    // listeners
    public void onChangeProgramKind()
    {
        // если вид ОП не установлен либо установлен не СПО, то чекбокс будет задизаблен
        if (_programKind == null || !_programKind.isProgramSecondaryProf())
        {
            setInDepthStudy(null);
        }
        // в противном случае (если мы установили вид ОП) этот чекбокс будет доступен, поэтому поставим дефолтный false
        else if (_programKind != null && getInDepthStudy() == null)
        {
            setInDepthStudy(false);
        }
    }

    // getters & setters
    public Long getProgramSubjectId()
    {
        return _programSubjectId;
    }

    public void setProgramSubjectId(Long programSubjectId)
    {
        _programSubjectId = programSubjectId;
    }

    public EduProgramKind getProgramKind()
    {
        return _programKind;
    }

    public void setProgramKind(EduProgramKind programKind)
    {
        _programKind = programKind;
    }

    public EduProgramSubjectIndex getProgramSubjectIndex()
    {
        return _programSubjectIndex;
    }

    public void setProgramSubjectIndex(EduProgramSubjectIndex programSubjectIndex)
    {
        _programSubjectIndex = programSubjectIndex;
    }

    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    public void setProgramSubject(EduProgramSubject programSubject)
    {
        _programSubject = programSubject;
    }

    public Boolean getInDepthStudy()
    {
        return _inDepthStudy;
    }

    public void setInDepthStudy(Boolean inDepthStudy)
    {
        _inDepthStudy = inDepthStudy;
    }
}
