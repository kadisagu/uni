package ru.tandemservice.uniepp.entity.workplan;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;

/**
 * @author vdanilov
 */
public interface IEppWorkPlanRow extends IEntity, ITitled {

    /** @return номер/индекс записи */
    String getNumber();

    /** @return вычисленное название элемента (с учетом уточнений, но без индекса) */
    String getDisplayableTitle();

    /** @return вычисленное название элемента (с учетом уточнений и индекса) */
    String getTitleWithIndex();

    /** @return вид дисциплины */
    EppWorkPlanRowKind getKind();

    /** @return тип записи реестра */
    EppRegistryStructure getType();

    /** @return число частей (семестров)  */
    int getRegistryElementPartAmount();

    /** @return номер части (семестра) */
    int getRegistryElementPartNumber();

    /** @return читающее подразделение */
    OrgUnit getOwner();

    /** @return текстовое описание (содержит в себе все значимые свойства объекта, определяющие его уникальность) */
    String getDescriptionString();

    /**
     * @return переаттестация
     */
    boolean isReattestation();

    /**
     * @return перезачтение
     */
    boolean isReexamination();
}
