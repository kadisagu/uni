package ru.tandemservice.uniepp.entity.registry;

import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.gen.*;

/** @see ru.tandemservice.uniepp.entity.registry.gen.EppRegElementSplitByTypeEduGroupRelGen */
public class EppRegElementSplitByTypeEduGroupRel extends EppRegElementSplitByTypeEduGroupRelGen
{

    public EppRegElementSplitByTypeEduGroupRel() {
    }

    public EppRegElementSplitByTypeEduGroupRel(EppRegistryElement element, EppGroupType groupType) {
        setRegistryElement(element);
        setGroupType(groupType);
    }
}