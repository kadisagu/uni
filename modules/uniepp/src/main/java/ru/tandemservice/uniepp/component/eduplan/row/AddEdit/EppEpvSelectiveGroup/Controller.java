package ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvSelectiveGroup;

import java.util.Collections;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.BaseAddEditDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
        this.onChangeParent(component);
    }

    public void onChangeParent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        final EppEpvGroupImRow row = model.getRow();
        row.setNumber(IEppEduPlanVersionDataDAO.instance.get().getNextNumber(row, Collections.singleton(model.getTemplateId())));
    }

    public void onClickApply(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        BaseAddEditDao.checkUniqTitle(model.getRow());

        try {
            this.getDao().save(model);
        } catch (Throwable t) {
            // component.getDesktop().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        this.deactivate(component);
    }

}
