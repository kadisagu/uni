/**
 *$Id: EppWorkPlanStudentList.java 26167 2013-02-14 14:19:39Z ashaburov $
 */
package ru.tandemservice.uniepp.base.bo.EppEduGroup.ui.List4StudentIds;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.SimpleMergeIdResolver;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp.base.bo.EppEduGroup.EppEduGroupManager;
import ru.tandemservice.uniepp.base.bo.EppEduGroup.logic.EppEduGroupList4StudentIdsDSHandler;
import ru.tandemservice.uniepp.base.bo.EppEduGroup.logic.EppEduGroupRegistryElement4StudentIdsDSHandler;
import ru.tandemservice.uniepp.base.bo.EppEduGroup.logic.EppEduGroupTutorOrgUnit4StudentIdsDSHandler;
import ru.tandemservice.uniepp.component.edugroup.EppEduGroupPublisherLinkResolver;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;

@Configuration
public class EppEduGroupList4StudentIds extends BusinessComponentManager
{
    public static final String EDUGROUP_LIST_DS = "eduGroupListDS";

    public static final String YEARPART_DS = "yearPartDS";
    public static final String GROUPTYPE_DS = "groupTypeDS";
    public static final String TUTORORGUNIT_DS = "tutorOrgUnitDS";
    public static final String REGISTRYELEMENT_DS = "registryElementDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(this.searchListDS(EDUGROUP_LIST_DS, this.eduGroupListDSColumnExtPoint(), EppEduGroupManager.instance().groupList4StudentIdsDSHandler()))
        .addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEARPART_DS, getName(), EppYearPart.defaultSelectDSHandler(getName())))
        .addDataSource(CommonBaseStaticSelectDataSource.selectDS(GROUPTYPE_DS, getName(), EppGroupType.defaultSelectDSHandler(getName())))
        .addDataSource(this.selectDS(TUTORORGUNIT_DS, this.tutorOrgUnitDSHandler()))
        .addDataSource(this.selectDS(REGISTRYELEMENT_DS, this.registryElementDSHandler()).addColumn(EppRegistryElement.titleWithNumber().s()).addColumn(EppRegistryElement.owner().shortTitle().s()))
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> tutorOrgUnitDSHandler() {
        return new EppEduGroupTutorOrgUnit4StudentIdsDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> registryElementDSHandler() {
        return new EppEduGroupRegistryElement4StudentIdsDSHandler(getName());
    }


    @Bean
    public ColumnListExtPoint eduGroupListDSColumnExtPoint() {

        final IPublisherLinkResolver eduGroupLinkResolver = new EppEduGroupPublisherLinkResolver() {
            @Override protected Long getOrgUnitId(IEntity entity) {
                return ((EppEduGroupList4StudentIdsUI)ContextLocal.getComponent().getPresenter()).getGroupOrgUnitId();
            }
            @Override public String getComponentName(final IEntity entity) {
                if (null == getOrgUnitId(entity)) {
                    // если деканата нет (ну не настроили они его), показываем глобальный публикатор УГС
                    return ru.tandemservice.uniepp.component.edugroup.pub.GlobalGroupPub.Model.COMPONENT_NAME;
                }
                return ru.tandemservice.uniepp.component.edugroup.pub.GroupOrgUnitGroupPub.Model.COMPONENT_NAME;
            }
        };

        return this.columnListExtPointBuilder(EDUGROUP_LIST_DS)
        .addColumn(textColumn("yearPart", EppRealEduGroup.summary().yearPart().title()).merger(new SimpleMergeIdResolver(EppRealEduGroup.summary().yearPart().id())))
        .addColumn(textColumn("tutorOrgUnit", EppRealEduGroup.activityPart().registryElement().owner().shortTitle())/*.merger(new SimpleMergeIdResolver(EppRealEduGroup.activityPart().registryElement().owner().id()))*/)
        .addColumn(publisherColumn("activityPart", EppRealEduGroup.activityPart().titleWithNumber()).publisherLinkResolver(new SimplePublisherLinkResolver(EppRealEduGroup.activityPart().registryElement().id())))
        .addColumn(publisherColumn("title", EppRealEduGroup.title()).publisherLinkResolver(eduGroupLinkResolver))
        .addColumn(textColumn("type", EppRealEduGroup.type().title()))
        .addColumn(textColumn("courses", EppEduGroupList4StudentIdsDSHandler.COLUMN_COURSES))
        .addColumn(textColumn("groups", EppEduGroupList4StudentIdsDSHandler.COLUMN_GROUPS))
        .addColumn(textColumn("level", EppRealEduGroup.level().displayableTitle()))
        .create();
    }

}
