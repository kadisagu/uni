package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

import java.sql.SQLException;

/**
 * Автоматически сгенерированная миграция
 */
public class MS_uniepp_2x9x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0")
		};
    }

	private void moveQualifications(DBTool tool, String srtTable) throws SQLException
	{
		final String destTable = "epp_eduplan_prof_t";
		final SQLUpdateQuery upd = new SQLUpdateQuery(destTable, "d");
		upd.set("programqualification_id", "s.programqualification_id");
		upd.from(SQLFrom.table(srtTable, "s"));
		upd.where("d.id=s.id");

		final int ret = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(upd));
		if (Debug.isEnabled()) {
			System.out.println(this.getClass().getSimpleName() + ": " + ret + " qualifications moved to " + destTable + " from " + srtTable);
		}
	}

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// Вынести поле с квалификацией из УП СПО (eppEduPlanSecondaryProf) и УП ВО (eppEduPlanHigherProf)
		// в их общую базовую сущность "Учебный план проф. образования" (eppEduPlanProf).
		{
			tool.createColumn("epp_eduplan_prof_t", new DBColumn("programqualification_id", DBType.LONG));

			moveQualifications(tool, "epp_eduplan_prof_higher_t");
			tool.dropTable("epp_eduplan_prof_higher_t", false);

			moveQualifications(tool, "epp_eduplan_prof_secondary_t");
			tool.dropColumn("epp_eduplan_prof_secondary_t", "programqualification_id");
		}


		////////////////////////////////////////////////////////////////////////////////
		// сущность eppCustomEduPlan

		// создана новая сущность
		if (!tool.tableExists("epp_custom_eduplan_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_custom_eduplan_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eppcustomeduplan"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("epvblock_id", DBType.LONG).setNullable(false),
				new DBColumn("plan_id", DBType.LONG).setNullable(false),
				new DBColumn("developgrid_id", DBType.LONG).setNullable(false), 
				new DBColumn("developcondition_id", DBType.LONG).setNullable(false), 
				new DBColumn("state_id", DBType.LONG).setNullable(false), 
				new DBColumn("number_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("comment_p", DBType.TEXT)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			tool.entityCodes().ensure("eppCustomEduPlan");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppCustomEpvRowTerm

		// создана новая сущность
		if (!tool.tableExists("epp_custom_eduplan_row_term_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_custom_eduplan_row_term_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eppcustomepvrowterm"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("customeduplan_id", DBType.LONG).setNullable(false), 
				new DBColumn("epvrowterm_id", DBType.LONG).setNullable(false), 
				new DBColumn("newterm_id", DBType.LONG).setNullable(false), 
				new DBColumn("reattestation_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("reexamination_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("excluded_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			tool.entityCodes().ensure("eppCustomEpvRowTerm");
		}
    }
}