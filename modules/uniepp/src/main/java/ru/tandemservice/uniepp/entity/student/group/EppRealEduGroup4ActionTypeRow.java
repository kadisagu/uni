package ru.tandemservice.uniepp.entity.student.group;

import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroup4ActionTypeRowGen;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;

import java.util.Date;

/**
 * Студент в группе (по форме контроля)
 */
public class EppRealEduGroup4ActionTypeRow extends EppRealEduGroup4ActionTypeRowGen
{
    public static final IEppRealEduGroupRowDescription RELATION = new IEppRealEduGroupRowDescription()
    {
        @Override public Class<EppStudentWpeCAction> studentClass() { return EppStudentWpeCAction.class; }
        @Override public Class<EppRealEduGroup4ActionType> groupClass() { return EppRealEduGroup4ActionType.class; }
        @Override public Class<EppRealEduGroup4ActionTypeRow> relationClass() { return EppRealEduGroup4ActionTypeRow.class; }
        @Override public Class<? extends EppGroupType> typeKlass() { return EppGroupTypeFCA.class; }

        @Override public EppRealEduGroup buildGroup(final EppRealEduGroupSummary summary, final EppRegistryElementPart activityPart, final EppGroupType groupType, final EppRealEduGroupCompleteLevel level, final String title, final Date creationDate) {
            final EppRealEduGroup4ActionType group = new EppRealEduGroup4ActionType(summary, activityPart, groupType);
            group.setLevel(level);
            group.setTitle(title);
            group.setCreationDate(creationDate);
            group.setModificationDate(creationDate);
            return group;
        }

        @Override public EppRealEduGroupRow buildRelation(final EppRealEduGroup group, final EppStudentWpePart student) {
            return new EppRealEduGroup4ActionTypeRow((EppRealEduGroup4ActionType)group, (EppStudentWpeCAction)student);
        }
    };


    public EppRealEduGroup4ActionTypeRow() {}

    public EppRealEduGroup4ActionTypeRow(final EppRealEduGroup4ActionType group, final EppStudentWpeCAction student) {
        this.setGroup(group);
        this.setStudentWpePart(student);
    }

    @Override
    public EppRealEduGroup4ActionTypeRow clone(final EppRealEduGroup group) {
        final EppRealEduGroup4ActionTypeRow rel = new EppRealEduGroup4ActionTypeRow();
        rel.update(this);
        rel.setGroup(group);
        rel.setRemovalDate(null);
        rel.setConfirmDate(null);
        return rel;
    }

    @Override
    public EppRealEduGroup4ActionType getGroup()
    {
        return (EppRealEduGroup4ActionType) super.getGroup();
    }

    @Override
    public EppStudentWpeCAction getStudentWpePart()
    {
        return (EppStudentWpeCAction) super.getStudentWpePart();
    }
}