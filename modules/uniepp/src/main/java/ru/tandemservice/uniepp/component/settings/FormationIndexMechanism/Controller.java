package ru.tandemservice.uniepp.component.settings.FormationIndexMechanism;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent context) {
        this.getDao().update(this.getModel(context));
    }

    public void onClickSave(final IBusinessComponent context) {
        this.onClickApply(context);
        this.deactivate(context);
    }
}
