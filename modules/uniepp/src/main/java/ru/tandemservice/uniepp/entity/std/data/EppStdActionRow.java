package ru.tandemservice.uniepp.entity.std.data;

import java.util.Map;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.IEppNumberRow;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.std.data.gen.EppStdActionRowGen;

/**
 * Запись ГОС (доп мероприятие)
 */
public class EppStdActionRow extends EppStdActionRowGen implements IEppNumberRow
{
    @Override public EppStdHierarchyRow getHierarhyParent() { return this.getParent(); }
    @Override public void setHierarhyParent(final EppStdRow parent) { this.setParent((EppStdHierarchyRow) parent); }

    @Override public int getComparatorClassIndex() { return Integer.MAX_VALUE; }
    @Override public String getComparatorString() { return this.getSelfIndexPart(); }
    @Override public String getSelfIndexPart() { return IEppNumberRow.NUMBER_FORMATTER.format(this); }

    @Override public boolean isStructureElement() { return false; }

    @Override
    @EntityDSLSupport
    public String getTotalWeeks()
    {
        if ((null != this.getTotalWeeksMin()) && (null != this.getTotalWeeksMax())) {
            return this.formatLoad(this.getTotalWeeksMin().longValue()) + " - " + this.formatLoad(this.getTotalWeeksMax().longValue());
        }

        if (null != this.getTotalWeeksMin()) {
            return "⩾ " + this.formatLoad(this.getTotalWeeksMin().longValue());
        }
        if (null != this.getTotalLaborMax()) {
            return "⩽ " + this.formatLoad(this.getTotalWeeksMax().longValue());
        }
        return "";
    }


    @Override
    public Map<String, Double> getLoadMap() {
        final Map<String, Double> map = super.getLoadMap();
        map.put(EppLoadType.FULL_CODE_WEEKS, UniEppUtils.wrap(this.getTotalWeeksMin()));
        return map;
    }

}