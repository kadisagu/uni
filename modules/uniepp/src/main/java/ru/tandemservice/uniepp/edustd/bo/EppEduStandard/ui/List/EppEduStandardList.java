/* $Id:$ */
package ru.tandemservice.uniepp.edustd.bo.EppEduStandard.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.bo.EduProgram.EduProgramManager;
import ru.tandemservice.uniepp.edustd.bo.EppEduStandard.EppEduStandardManager;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

/**
 * @author oleyba
 * @since 9/10/14
 */
@Configuration
public class EppEduStandardList extends BusinessComponentManager
{

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EduProgramManager.instance().programKindDSConfig())
            .addDataSource(selectDS("programSubjectDS", EppEduStandardManager.instance().programSubjectFromEppStateEduStandartDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
            .addDataSource(searchListDS("eduStdDS", eduPlanDSColumns(), eduPlanDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint eduPlanDSColumns(){
        return columnListExtPointBuilder("eduStdDS")
            .addColumn(checkboxColumn("select"))
            .addColumn(publisherColumn("title", EppStateEduStandard.title().s()).order())
            .addColumn(textColumn("generation", EppStateEduStandard.generationTitle()))
            .addColumn(textColumn("programKind", EppStateEduStandard.programSubject().subjectIndex().programKind().shortTitle()).order())
            .addColumn(textColumn("programSubject", EppStateEduStandard.programSubject().titleWithCode()).order())
            .addColumn(textColumn("state", EppStateEduStandard.state().title().s()).order())
            .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEdit")
                .permissionKey("ui:sec.edit"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDelete")
                .alert(new FormattedMessage("eduStdDS.delete.alert", EppStateEduStandard.title().s()))
                .permissionKey("ui:sec.delete"))
            .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduPlanDSHandler()
    {
        return new EppEduStandardDSHandler(getName());
    }
}