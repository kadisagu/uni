package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Блок версии УП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanVersionBlockGen extends EntityBase
 implements IEppEducationElement, ICtrPriceElement{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock";
    public static final String ENTITY_NAME = "eppEduPlanVersionBlock";
    public static final int VERSION_HASH = -1139914829;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String P_EDUCATION_ELEMENT_SIMPLE_TITLE = "educationElementSimpleTitle";
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_FULL_TITLE_EXTENDED = "fullTitleExtended";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_FOR_LIST_WITH_VERSION_AND_SPECIALIZATION = "titleForListWithVersionAndSpecialization";
    public static final String P_TITLE_WITH_FULL_NUMBER = "titleWithFullNumber";
    public static final String P_VERSION_FULL_TITLE_WITH_BLOCK_TITLE = "versionFullTitleWithBlockTitle";

    private EppEduPlanVersion _eduPlanVersion;     // Версия учебного плана

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion Версия учебного плана. Свойство не может быть null.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEduPlanVersionBlockGen)
        {
            setEduPlanVersion(((EppEduPlanVersionBlock)another).getEduPlanVersion());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanVersionBlockGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlanVersionBlock.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppEduPlanVersionBlock is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlanVersion":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlanVersion":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlanVersionBlock> _dslPath = new Path<EppEduPlanVersionBlock>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlanVersionBlock");
    }
            

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getEducationElementSimpleTitle()
     */
    public static SupportedPropertyPath<String> educationElementSimpleTitle()
    {
        return _dslPath.educationElementSimpleTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getFullTitle()
     */
    public static SupportedPropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getFullTitleExtended()
     */
    public static SupportedPropertyPath<String> fullTitleExtended()
    {
        return _dslPath.fullTitleExtended();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getTitleForListWithVersionAndSpecialization()
     */
    public static SupportedPropertyPath<String> titleForListWithVersionAndSpecialization()
    {
        return _dslPath.titleForListWithVersionAndSpecialization();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getTitleWithFullNumber()
     */
    public static SupportedPropertyPath<String> titleWithFullNumber()
    {
        return _dslPath.titleWithFullNumber();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getVersionFullTitleWithBlockTitle()
     */
    public static SupportedPropertyPath<String> versionFullTitleWithBlockTitle()
    {
        return _dslPath.versionFullTitleWithBlockTitle();
    }

    public static class Path<E extends EppEduPlanVersionBlock> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;
        private SupportedPropertyPath<String> _educationElementSimpleTitle;
        private SupportedPropertyPath<String> _fullTitle;
        private SupportedPropertyPath<String> _fullTitleExtended;
        private SupportedPropertyPath<String> _title;
        private SupportedPropertyPath<String> _titleForListWithVersionAndSpecialization;
        private SupportedPropertyPath<String> _titleWithFullNumber;
        private SupportedPropertyPath<String> _versionFullTitleWithBlockTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getEducationElementSimpleTitle()
     */
        public SupportedPropertyPath<String> educationElementSimpleTitle()
        {
            if(_educationElementSimpleTitle == null )
                _educationElementSimpleTitle = new SupportedPropertyPath<String>(EppEduPlanVersionBlockGen.P_EDUCATION_ELEMENT_SIMPLE_TITLE, this);
            return _educationElementSimpleTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getFullTitle()
     */
        public SupportedPropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new SupportedPropertyPath<String>(EppEduPlanVersionBlockGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getFullTitleExtended()
     */
        public SupportedPropertyPath<String> fullTitleExtended()
        {
            if(_fullTitleExtended == null )
                _fullTitleExtended = new SupportedPropertyPath<String>(EppEduPlanVersionBlockGen.P_FULL_TITLE_EXTENDED, this);
            return _fullTitleExtended;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EppEduPlanVersionBlockGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getTitleForListWithVersionAndSpecialization()
     */
        public SupportedPropertyPath<String> titleForListWithVersionAndSpecialization()
        {
            if(_titleForListWithVersionAndSpecialization == null )
                _titleForListWithVersionAndSpecialization = new SupportedPropertyPath<String>(EppEduPlanVersionBlockGen.P_TITLE_FOR_LIST_WITH_VERSION_AND_SPECIALIZATION, this);
            return _titleForListWithVersionAndSpecialization;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getTitleWithFullNumber()
     */
        public SupportedPropertyPath<String> titleWithFullNumber()
        {
            if(_titleWithFullNumber == null )
                _titleWithFullNumber = new SupportedPropertyPath<String>(EppEduPlanVersionBlockGen.P_TITLE_WITH_FULL_NUMBER, this);
            return _titleWithFullNumber;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock#getVersionFullTitleWithBlockTitle()
     */
        public SupportedPropertyPath<String> versionFullTitleWithBlockTitle()
        {
            if(_versionFullTitleWithBlockTitle == null )
                _versionFullTitleWithBlockTitle = new SupportedPropertyPath<String>(EppEduPlanVersionBlockGen.P_VERSION_FULL_TITLE_WITH_BLOCK_TITLE, this);
            return _versionFullTitleWithBlockTitle;
        }

        public Class getEntityClass()
        {
            return EppEduPlanVersionBlock.class;
        }

        public String getEntityName()
        {
            return "eppEduPlanVersionBlock";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getEducationElementSimpleTitle();

    public abstract String getFullTitle();

    public abstract String getFullTitleExtended();

    public abstract String getTitle();

    public abstract String getTitleForListWithVersionAndSpecialization();

    public abstract String getTitleWithFullNumber();

    public abstract String getVersionFullTitleWithBlockTitle();
}
