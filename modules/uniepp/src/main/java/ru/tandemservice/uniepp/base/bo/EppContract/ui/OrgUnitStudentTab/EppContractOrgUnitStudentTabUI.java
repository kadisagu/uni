package ru.tandemservice.uniepp.base.bo.EppContract.ui.OrgUnitStudentTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplate;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.base.bo.EppContract.logic.EppContractOrgUnitStudentListHandler;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public class EppContractOrgUnitStudentTabUI extends OrgUnitUIPresenter {

    private BaseSearchListDataSource studentListDataSource;
    public BaseSearchListDataSource getStudentListDataSource() { return this.studentListDataSource; }

    private EppCtrEducationResult currentStudentContract;
    public EppCtrEducationResult getCurrentStudentContract() { return this.currentStudentContract; }
    public void setCurrentStudentContract(final EppCtrEducationResult currentStudentContract) { this.currentStudentContract = currentStudentContract; }

    @SuppressWarnings("unchecked")
    public List<EppCtrEducationResult> getCurrentStudentContractList() {
        return (List<EppCtrEducationResult>) this.studentListDataSource.<IEntity>getCurrent().getProperty(EppContractOrgUnitStudentListHandler.P_CONTRACT_RESULT_LIST);
    }

    @Override public void onComponentRefresh() {
        this.studentListDataSource = (BaseSearchListDataSource)this.getConfig().getDataSource(EppContractOrgUnitStudentTab.STUDENT_LIST_DS);
    }

    @Override
    public void onBeforeDataSourceFetch(final IUIDataSource dataSource) {
        if (EppContractOrgUnitStudentTab.STUDENT_LIST_DS.equals(dataSource.getName())) {
            dataSource.put(EppContractOrgUnitStudentListHandler.PARAM_ORGUNIT, this.getOrgUnit());
            final Map<String, Object> settingMap = this._uiSettings.getAsMap(
                EppContractOrgUnitStudentListHandler.PARAM_PERSON_LAST_NAME,
                EppContractOrgUnitStudentListHandler.PARAM_PERSONAL_NUMBER,
                EppContractOrgUnitStudentListHandler.PARAM_PERSONAL_FILE_NUMBER,
                EppContractOrgUnitStudentListHandler.PARAM_BOOK_NUMBER,
                EppContractOrgUnitStudentListHandler.PARAM_STUDENT_CATEGORY,
                EppContractOrgUnitStudentListHandler.PARAM_STUDENT_STATUS,
                EppContractOrgUnitStudentListHandler.PARAM_COURSE,
                EppContractOrgUnitStudentListHandler.PARAM_DEVELOP_FORM,
                EppContractOrgUnitStudentListHandler.PARAM_DEVELOP_CONDITION,
                EppContractOrgUnitStudentListHandler.PARAM_DEVELOP_PERIOD
            );
            dataSource.putAll(settingMap);
        }
    }

    public void onClickEditStudentContract()
    {
        // редактируем следующую версию договора
        final EppCtrEducationResult result = DataAccessServices.dao().getNotNull(this.getListenerParameterAsLong());
        final CtrContractVersion version = CtrContractVersionManager.instance().dao().getNextVersion(result.getContract()); /* !next! */
        if (null == version) {
            throw new ApplicationException(getConfig().getProperty("error.contract.edit.no-next-version", result.getContract().getNumber()));
        }

        CtrContractVersionTemplateData templateData = IUniBaseDao.instance.get().get(CtrContractVersionTemplateData.class, CtrContractVersionTemplateData.owner(), version);

        if (templateData != null && templateData.getManager().getDataEditComponent() != null) {
            this._uiActivation
            .asRegionDialog(templateData.getManager().getDataEditComponent())
            .parameter(PUBLISHER_ID, templateData.getId())
            .activate();
            return;
        }

        if (templateData == null || templateData.getManager().isAllowEditInWizard())
            CtrContractVersionManager.openWizard(version);
        else
            throw new ApplicationException(getConfig().getProperty("error.contract.edit.edit-not-allowed", result.getContract().getNumber()));
    }

    public void onClickPrintStudentCurrentVersion()
    {
        final EppCtrEducationResult result = DataAccessServices.dao().getNotNull(this.getListenerParameterAsLong());
        final CtrContractVersion version = CtrContractVersionManager.instance().dao().getLastActiveVersion(result.getContract()); /* !current! */
        if (null == version) {
            throw new ApplicationException(getConfig().getProperty("error.contract.print.no-current-version", result.getContract().getNumber()));
        }

        // печатаем
        CtrContractVersionManager.downloadPrintVersion(version);
    }

    public void onClickPrintStudentNextVersion()
    {
        final EppCtrEducationResult result = DataAccessServices.dao().getNotNull(this.getListenerParameterAsLong());
        final CtrContractVersion version = CtrContractVersionManager.instance().dao().getNextVersion(result.getContract()); /* !next! */
        if (null == version) {
            throw new ApplicationException(getConfig().getProperty("error.contract.print.no-next-version", result.getContract().getNumber()));
        }

        // печатаем
        CtrContractVersionManager.downloadPrintVersion(version);
    }

    public void onClickAddStudentContract()
    {
        this._uiActivation.asRegionDialog(CtrContractVersionAddByTemplate.class).parameter(IUIPresenter.PUBLISHER_ID, getStudentListDataSource().getListenerParameter()).activate();
    }

}
