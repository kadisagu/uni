/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.ui.RegistryElementCorrection;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.Hashtable;
import java.util.Map;

/**
 * @author rsizonenko
 * @since 30.03.2016
 */
public class EppReorganizationRegistryElementCorrectionUI extends UIPresenter {

    private String selectedTab;

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

    public Map<String, Object> getJournalParams()
    {
        Map<String, Object> params = new Hashtable<>();

        params.put("meta", EntityRuntime.getMeta(EppRegistryElement.class));
        params.put("hideSelect", Boolean.TRUE);
        return params;
    }
}