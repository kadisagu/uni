package ru.tandemservice.uniepp.component.workplan.WorkPlanTotalPartLoadEdit;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;

/**
 * 
 * @author nkokorina
 *
 */

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "rowHolder.id") })
@SuppressWarnings("unchecked")
public class Model
{

    private final EntityHolder<EppWorkPlanRegistryElementRow> rowHolder = new EntityHolder<EppWorkPlanRegistryElementRow>();
    public EntityHolder<EppWorkPlanRegistryElementRow> getRowHolder() { return this.rowHolder; }
    public EppWorkPlanRegistryElementRow getRow() { return this.getRowHolder().getValue(); }
    public Long getId() { return this.getRowHolder().getId(); }

    public Boolean getShowCyclicLoading() { return IEppSettingsDAO.instance.get().getGlobalSettings().isUseCyclicLoading(); }

    private IEppRegElPartWrapper registryElementPartWrapper;
    public IEppRegElPartWrapper getRegistryElementPartWrapper() { return this.registryElementPartWrapper; }
    public void setRegistryElementPartWrapper(final IEppRegElPartWrapper registryElementPartWrapper) { this.registryElementPartWrapper = registryElementPartWrapper; }

    public final static long FIRST_OPTION_ID = 0L;
    public final static long SECOND_OPTION_ID = 1L;
    public static final IdentifiableWrapper FIRST_OPTION_WRAPPER = new IdentifiableWrapper(Model.FIRST_OPTION_ID, "Фронтальная нагрузка");
    public static final IdentifiableWrapper SECOND_OPTION_WRAPPER = new IdentifiableWrapper(Model.SECOND_OPTION_ID, "Цикловая нагрузка");

    private final static List<IdentifiableWrapper> useCyclicLoadingList = Arrays.asList(Model.FIRST_OPTION_WRAPPER, Model.SECOND_OPTION_WRAPPER);
    public List<IdentifiableWrapper> getUseCyclicLoadingList() { return Model.useCyclicLoadingList; }

    private List<EppLoadType> loadTypeList = Collections.emptyList();
    public List<EppLoadType> getLoadTypeList() { return this.loadTypeList; }
    public void setLoadTypeList(final List<EppLoadType> loadTypeList) { this.loadTypeList = loadTypeList; }

    private EppLoadType loadType;
    public EppLoadType getLoadType() { return this.loadType; }
    public void setLoadType(final EppLoadType loadType) { this.loadType = loadType; }

    public String getLoadTypeRegistryLoad() {
        final IEppRegElPartWrapper registryElementPartWrapper = this.getRegistryElementPartWrapper();
        if (null == registryElementPartWrapper) { return ""; }
        return UniEppUtils.formatLoad(registryElementPartWrapper.getLoadAsDouble(this.getLoadType().getFullCode()), false);
    }


    private List<EppWorkPlanPart> partsList = Collections.emptyList();
    public List<EppWorkPlanPart> getPartsList() { return this.partsList; }
    public void setPartsList(final List<EppWorkPlanPart> partsList) { this.partsList = partsList; }

    private EppWorkPlanPart part;
    public EppWorkPlanPart getPart() { return this.part; }
    public void setPart(final EppWorkPlanPart part) { this.part = part; }

    private final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> partLoadMap = SafeMap.get(HashMap.class);
    public Map<Integer, Map<String, EppWorkPlanRowPartLoad>> getPartLoadMap() { return this.partLoadMap; }

    private EppWorkPlanRowPartLoad getEppWorkPlanRowPartLoad()
    {
        final Map<String, EppWorkPlanRowPartLoad> map = this.partLoadMap.get(this.getPart().getNumber());
        EppWorkPlanRowPartLoad eppWorkPlanRowPartLoad = map.get(this.getLoadType().getFullCode());
        if (null == eppWorkPlanRowPartLoad)
        {
            eppWorkPlanRowPartLoad = new EppWorkPlanRowPartLoad();
            eppWorkPlanRowPartLoad.setRow(this.getRow());
            eppWorkPlanRowPartLoad.setPart(this.getPart().getNumber());
            eppWorkPlanRowPartLoad.setLoadType(this.getLoadType());
            eppWorkPlanRowPartLoad.setLoadAsDouble(null);
            eppWorkPlanRowPartLoad.setDaysAsDouble(null);

            map.put(this.getLoadType().getFullCode(), eppWorkPlanRowPartLoad);
        }

        return eppWorkPlanRowPartLoad;
    }

    public Double getValue() {
        return this.getEppWorkPlanRowPartLoad().getLoadAsDouble();
    }
    public void setValue(final Double value) {
        this.getEppWorkPlanRowPartLoad().setLoadAsDouble(value);
    }

    public Double getDayValue() {
        return this.getEppWorkPlanRowPartLoad().getDaysAsDouble();
    }
    public void setDayValue(final Double value) {
        this.getEppWorkPlanRowPartLoad().setDaysAsDouble(value);
    }

    public static String getInputId(final Integer part, final String fullCode) {
        return "load_"+part+"_"+fullCode.replace('.', '_');
    }
    public String getInputId() {
        return Model.getInputId(this.getPart().getNumber(), this.getLoadType().getFullCode());
    }

    public static String getInputDayId(final Integer part, final String fullCode) {
        return "load_"+part+"_"+fullCode.replace('.', '_')+"_day";
    }
    public String getInputDayId() {
        return Model.getInputDayId(this.getPart().getNumber(), this.getLoadType().getFullCode());
    }

    private Map<String, IdentifiableWrapper> loadCode2useCycle = new HashMap<String, IdentifiableWrapper>();
    public Map<String, IdentifiableWrapper> getLoadCode2useCycle() { return this.loadCode2useCycle; }
    public void setLoadCode2useCycle(final Map<String, IdentifiableWrapper> loadCode2useCycle) { this.loadCode2useCycle = loadCode2useCycle; }

    public IdentifiableWrapper getLoadUseCycle(){
        final IdentifiableWrapper wrapper = this.getLoadCode2useCycle().get(this.getLoadType().getFullCode());
        return (null != wrapper) ? wrapper: Model.FIRST_OPTION_WRAPPER;
    }

    public void setLoadUseCycle(final IdentifiableWrapper wrapper){
        this.getLoadCode2useCycle().put(this.getLoadType().getFullCode(),  wrapper);
    }

    public boolean isShowLoadTypeCyclicLoading(){
        return (null != this.getLoadUseCycle()) ? Model.SECOND_OPTION_ID == this.getLoadUseCycle().getId() : false;
    }

    public String getTitle() {
        return UniEppUtils.getEppWorkPlanRowFormTitle(this.getId());
    }
}
