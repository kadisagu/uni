// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.sec;

import java.util.Map;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.util.CtrContractVersionUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;

/**
 * @author oleyba
 * @since 01.12.2010
 */
public class UnieppCtrOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uniepp");
        config.setName("uniepp-ctr-orgunit-sec-config");
        config.setTitle("");

        // права на сам объект «договор со студентом»

//        PermissionGroupMeta pg = CtrContractVersionUtil.registerPermissionGroup(config, EppCtrEducationResult.class);
//        PermissionMetaUtil.createGroupRelation(config, pg.getName(), "eppCtrEducationResultCG");
//        PermissionMetaUtil.createGroupRelation(config, pg.getName(), "eppCtrEducationResultLC");

        // для каждого типа подразделения добавляем права на вкладку «студенты / договоры» (вкладка непосредственно подразделения)

//        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
//        {
//            final String code = description.getCode();
//
//            // "Вкладка «Студенты»"
//            final PermissionGroupMeta permissionGroupStudentsTab = PermissionMetaUtil.createPermissionGroup(config, code + "StudentsPermissionGroup", "Вкладка «Студенты»");
//
//            // "Подвкладка «Договоры»"
//            final PermissionGroupMeta permissionGroupStudentContracts = PermissionMetaUtil.createPermissionGroup(permissionGroupStudentsTab, code + "StudentsContractTabPG", "Вкладка «Договоры»»");
//            PermissionMetaUtil.createPermission(permissionGroupStudentContracts, "orgUnit_viewStudentContractTab_" + code, "Просмотр");
//            PermissionMetaUtil.createPermission(permissionGroupStudentContracts, "orgUnit_addStudentContract_" + code, "Создание договора на обучение");
//            PermissionMetaUtil.createPermission(permissionGroupStudentContracts, "orgUnit_editStudentContract_" + code, "Изменение договора на обучение");
//            PermissionMetaUtil.createPermission(permissionGroupStudentContracts, "orgUnit_printStudentContract_" + code, "Печать договора на обучение");
//
//        }

        securityConfigMetaMap.put(config.getName(), config);
    }

}
