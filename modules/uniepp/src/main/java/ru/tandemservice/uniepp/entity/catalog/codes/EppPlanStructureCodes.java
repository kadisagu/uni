package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Структура ГОС/УП"
 * Имя сущности : eppPlanStructure
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppPlanStructureCodes
{
    /** Константа кода (code) элемента : ГОС (title) */
    String GOS = "2g";
    /** Константа кода (code) элемента : Циклы (title) */
    String GOS_CYCLES = "2g.index";
    /** Константа кода (code) элемента : Общеобразовательный цикл (title) */
    String GOS_CYCLES_GENERAL = "2g.index.00";
    /** Константа кода (code) элемента : Общие гуманитарные и социально-экономические дисциплины (title) */
    String GOS_CYCLES_GENERAL_HUMANITARIAN_AND_SOCIO_ECONOMIC = "2g.index.01";
    /** Константа кода (code) элемента : Общие математические и естественнонаучные дисциплины (title) */
    String GOS_CYCLES_GENERAL_MATHEMATICAL_AND_NATURAL_SCIENCES = "2g.index.02";
    /** Константа кода (code) элемента : Общепрофессиональные дисциплины (title) */
    String GOS_CYCLES_GENERAL_PROFESSIONAL_DISCIPLINES = "2g.index.03";
    /** Константа кода (code) элемента : Дисциплины направления (title) */
    String GOS_CYCLES_DIRECTION_DISCIPLINES = "2g.index.04";
    /** Константа кода (code) элемента : Научно-исследовательская работа (title) */
    String GOS_CYCLES_RESEARCH_WORK = "2g.index.05";
    /** Константа кода (code) элемента : Дисциплины дополнительной специальности (title) */
    String GOS_CYCLES_ADDITIONAL_SPECIALITY_DISCIPLINES = "2g.index.06";
    /** Константа кода (code) элемента : Дисциплины предметной подготовки (title) */
    String GOS_CYCLES_SUBJECT_PREPARATION_DISCIPLINES = "2g.index.07";
    /** Константа кода (code) элемента : Факультативные дисциплины (title) */
    String GOS_CYCLES_OPTIONAL_DISCIPLINES = "2g.index.08";
    /** Константа кода (code) элемента : Дисциплины профильной подготовки (title) */
    String GOS_CYCLES_PROFILE_PREPARATION_DISCIPLINES = "2g.index.09";
    /** Константа кода (code) элемента : Специальные дисциплины (title) */
    String GOS_CYCLES_SPECIAL_DISCIPLINES = "2g.index.10";
    /** Константа кода (code) элемента : Дисциплины специализации (title) */
    String GOS_CYCLES_SPECIALIZATION_DISCIPLINES = "2g.index.11";
    /** Константа кода (code) элемента : Учебная практика (title) */
    String GOS_CYCLES_INSTRUCTIONAL_PRACTICE = "2g.index.12";
    /** Константа кода (code) элемента : Производственная практика (title) */
    String GOS_CYCLES_WORKING_PRACTICE = "2g.index.13";
    /** Константа кода (code) элемента : Практика (title) */
    String GOS_CYCLES_PRACTICE = "2g.index.14";
    /** Константа кода (code) элемента : Итоговая государственная аттестация (title) */
    String GOS_CYCLES_FINAL_STATE_ATTESTATION = "2g.index.15";
    /** Константа кода (code) элемента : Гуманитарный цикл (title) */
    String GOS_CYCLES_HUMANITARIAN = "2g.index.16";
    /** Константа кода (code) элемента : Естественнонаучный цикл (title) */
    String GOS_CYCLES_NATURAL_SCIENCE = "2g.index.17";
    /** Константа кода (code) элемента : Общетехнический цикл (title) */
    String GOS_CYCLES_GENERAL_TECHNICAL = "2g.index.18";
    /** Константа кода (code) элемента : Общепрофессиональный цикл (title) */
    String GOS_CYCLES_GENERAL_PROFESSIONAL = "2g.index.19";
    /** Константа кода (code) элемента : Профессиональный цикл (title) */
    String GOS_CYCLES_PROFESSIONAL = "2g.index.20";
    /** Константа кода (code) элемента : Специальные факультативные дисциплины (title) */
    String GOS_CYCLES_SPECIAL_OPTIONAL_DISCIPLINES = "2g.index.21";
    /** Константа кода (code) элемента : Стажировка (title) */
    String GOS_CYCLES_PLACEMENT = "2g.index.22";
    /** Константа кода (code) элемента : Компоненты (title) */
    String GOS_COMPONENTS = "2g.component";
    /** Константа кода (code) элемента : Федеральный компонент (title) */
    String GOS_COMPONENTS_FEDERAL = "2g.component.01";
    /** Константа кода (code) элемента : Национально-региональный (вузовский) компонент (title) */
    String GOS_COMPONENTS_REGIONAL = "2g.component.02";
    /** Константа кода (code) элемента : Дисциплины и курсы по выбору студента, устанавливаемые ОУ (title) */
    String GOS_COMPONENTS_SELECTED_DISCIPLINES = "2g.component.03";
    /** Константа кода (code) элемента : Специальные дисциплины (title) */
    String GOS_COMPONENTS_SPECIAL_DISCIPLINES = "2g.component.04";
    /** Константа кода (code) элемента : Дисциплины специализации (title) */
    String GOS_COMPONENTS_SPECIALIZATION_DISCIPLINES = "2g.component.05";
    /** Константа кода (code) элемента : ФГОС 2009 (title) */
    String FGOS = "3g";
    /** Константа кода (code) элемента : Циклы (title) */
    String FGOS_CYCLES = "3g.cycle";
    /** Константа кода (code) элемента : Общеобразовательный цикл (title) */
    String FGOS_CYCLES_GENERAL = "3g.cycle.00";
    /** Константа кода (code) элемента : Общий гуманитарный и социально-экономический цикл (title) */
    String FGOS_CYCLES_GENERAL_HUMANITARIAN_AND_SOCIO_ECONOMIC = "3g.cycle.01";
    /** Константа кода (code) элемента : Математический и общий естественно-научный цикл (title) */
    String FGOS_CYCLES_GENERAL_MATHEMATICAL_AND_NATURAL_SCIENCES = "3g.cycle.02";
    /** Константа кода (code) элемента : Профессиональный цикл (title) */
    String FGOS_CYCLES_PROFESSIONAL = "3g.cycle.03";
    /** Константа кода (code) элемента : Гуманитарный, социальный и экономический цикл (title) */
    String FGOS_CYCLES_HUMANITARIAN_SOCIAL_AND_ECONOMIC = "3g.cycle.04";
    /** Константа кода (code) элемента : Математический и естественно-научный цикл (title) */
    String FGOS_CYCLES_MATHEMATICAL_AND_NATURAL_SCIENCES = "3g.cycle.05";
    /** Константа кода (code) элемента : Учебная и производственная практики (title) */
    String FGOS_CYCLES_INSTRUCTIONAL_AND_WORKING_PRACTICE = "3g.cycle.07";
    /** Константа кода (code) элемента : Общенаучный цикл (title) */
    String FGOS_CYCLES_GENERAL_SCIENTIFIC = "3g.cycle.09";
    /** Константа кода (code) элемента : Учебная и производственная практики, научно-исследовательская работа (title) */
    String FGOS_CYCLES_INSTRUCTIONAL_AND_WORKING_PRACTICE_RESEARCH_WORK = "3g.cycle.11";
    /** Константа кода (code) элемента : Практика (title) */
    String FGOS_CYCLES_PRACTICE = "3g.cycle.12";
    /** Константа кода (code) элемента : Практика и научно-исследовательская работа (title) */
    String FGOS_CYCLES_PRACTICE_AND_RESEARCH_WORK = "3g.cycle.13";
    /** Константа кода (code) элемента : Учебная практика (title) */
    String FGOS_CYCLES_INSTRUCTIONAL_PRACTICE = "3g.cycle.14";
    /** Константа кода (code) элемента : Производственная практика (практика по профилю специальности) (title) */
    String FGOS_CYCLES_WORKING_PROFILE_PRACTICE = "3g.cycle.15";
    /** Константа кода (code) элемента : Производственная практика (преддипломная практика) (title) */
    String FGOS_CYCLES_WORKING_PREGRADUATION_PRACTICE = "3g.cycle.16";
    /** Константа кода (code) элемента : Итоговая государственная аттестация (title) */
    String FGOS_CYCLES_FINAL_STATE_ATTESTATION = "3g.cycle.17";
    /** Константа кода (code) элемента : Государственная (итоговая) аттестация (title) */
    String FGOS_CYCLES_STATE_FINAL_ATTESTATION = "3g.cycle.18";
    /** Константа кода (code) элемента : Общепрофессиональный цикл (title) */
    String FGOS_CYCLES_GENERAL_PROFESSIONAL = "3g.cycle.19";
    /** Константа кода (code) элемента : Учебная практика (производственное обучение) (title) */
    String FGOS_CYCLES_INSTRUCTIONAL_PRACTICE_WORKING_STUDY = "3g.cycle.20";
    /** Константа кода (code) элемента : Производственная практика (title) */
    String FGOS_CYCLES_WORKING_PRACTICE = "3g.cycle.21";
    /** Константа кода (code) элемента : Промежуточная аттестация (title) */
    String FGOS_CYCLES_INTERMEDIATE_PRACTICE = "3g.cycle.22";
    /** Константа кода (code) элемента : Физическая культура (title) */
    String FGOS_CYCLES_PHYSICAL_TRAINING = "3g.cycle.23";
    /** Константа кода (code) элемента : Специальные дисциплины (title) */
    String FGOS_CYCLES_SPECIAL_DISCIPLINES = "3g.cycle.24";
    /** Константа кода (code) элемента : Специальные факультативные дисциплины (title) */
    String FGOS_CYCLES_SPECIAL_OPTIONAL_DISCIPLINES = "3g.cycle.25";
    /** Константа кода (code) элемента : Общепрофессиональные дисциплины (title) */
    String FGOS_CYCLES_GENERAL_PROFESSIONAL_DISCIPLINES = "3g.cycle.26";
    /** Константа кода (code) элемента : Дисциплины специализации (title) */
    String FGOS_CYCLES_SPECIALIZATION_DISCIPLINES = "3g.cycle.27";
    /** Константа кода (code) элемента : Стажировка (title) */
    String FGOS_CYCLES_PLACEMENT = "3g.cycle.28";
    /** Константа кода (code) элемента : Учебная и производственная практики, супервизия, научно-исследовательская работа (title) */
    String FGOS_CYCLES_INSTRUCTIONAL_AND_WORKING_PRACTICE_SUPERVISION_RESEARCH_WORK = "3g.cycle.29";
    /** Константа кода (code) элемента : Естественнонаучный (title) */
    String FGOS_CYCLES_NATURAL_SCIENCE = "3g.cycle.30";
    /** Константа кода (code) элемента : Информационно-правовой (title) */
    String FGOS_CYCLES_LEGAL_INFORMATIONAL = "3g.cycle.31";
    /** Константа кода (code) элемента : Цикл истории и теории музыкального искусства (title) */
    String FGOS_CYCLES_HISTORY_AND_THEORY_OF_MUSICAL_ARTS = "3g.cycle.32";
    /** Константа кода (code) элемента : Цикл истории и теории мировой художественной культуры (title) */
    String FGOS_CYCLES_HISTORY_AND_THEORY_OF_WORLD_CULTURE = "3g.cycle.33";
    /** Константа кода (code) элемента : Информационно-коммуникационный цикл (title) */
    String FGOS_CYCLES_INFORMATIVE_COMMUNICATIVE = "3g.cycle.34";
    /** Константа кода (code) элемента : Этнокультурный, этнохудожественный и этнопедагогический циклы (title) */
    String FGOS_CYCLES_ETHNIC = "3g.cycle.35";
    /** Константа кода (code) элемента : Художественный цикл (title) */
    String FGOS_CYCLES_ART = "3g.cycle.36";
    /** Константа кода (code) элемента : Федеральный компонент среднего (полного) общего образования (title) */
    String FGOS_CYCLES_FEDERAL_SPO_COMPONENT = "3g.cycle.37";
    /** Константа кода (code) элемента : Дополнительная работа над завершением программного задания под руководством преподавателя (title) */
    String FGOS_CYCLES_ADDITIONAL_WORK_WITH_TEACHER = "3g.cycle.38";
    /** Константа кода (code) элемента : Профессиональные модули (title) */
    String FGOS_CYCLES_PROFESSIONAL_MODULES = "3g.cycle.39";
    /** Константа кода (code) элемента : Факультативные дисциплины (title) */
    String FGOS_CYCLES_OPTIONAL_DISCIPLINES = "3g.cycle.40";
    /** Константа кода (code) элемента : Части (title) */
    String FGOS_PARTS = "3g.part";
    /** Константа кода (code) элемента : Базовая часть (title) */
    String FGOS_PARTS_BASE = "3g.part.01";
    /** Константа кода (code) элемента : Вариативная часть (title) */
    String FGOS_PARTS_VARIATIVE = "3g.part.02";
    /** Константа кода (code) элемента : Общепрофессиональные дисциплины (title) */
    String FGOS_PARTS_GENERAL_PROFESSIONAL_DISCIPLINES = "3g.part.03";
    /** Константа кода (code) элемента : Профессиональные модули (title) */
    String FGOS_PARTS_PROFESSIONAL_MODULES = "3g.part.04";
    /** Константа кода (code) элемента : ФГОС 2013 (title) */
    String FGOS_2013 = "3pg";
    /** Константа кода (code) элемента : Блоки (title) */
    String FGOS_2013_BLOCKS = "3pg.block";
    /** Константа кода (code) элемента : Дисциплины (модули) (title) */
    String FGOS_2013_DISCIPLINES = "3pg.block.01";
    /** Константа кода (code) элемента : Практики (title) */
    String FGOS_2013_PRACTICE = "3pg.block.02";
    /** Константа кода (code) элемента : Научно-исследовательская работа (title) */
    String FGOS_2013_RESEARCH_WORK = "3pg.block.03";
    /** Константа кода (code) элемента : Государственная итоговая аттестация (title) */
    String FGOS_2013_FINAL_STATE_ATTESTATION = "3pg.block.04";
    /** Константа кода (code) элемента : Факультативные дисциплины (title) */
    String FGOS_2013_OPTIONAL_DISCIPLINES = "3pg.block.05";
    /** Константа кода (code) элемента : Практики, в том числе научно-исследовательская работа (НИР) (title) */
    String FGOS_2013_PRACTICE_2 = "3pg.block.06";
    /** Константа кода (code) элемента : Научные исследования (title) */
    String FGOS_2013_RESEARCH_WORK_2 = "3pg.block.07";
    /** Константа кода (code) элемента : Государственная итоговая аттестация (title) */
    String FGOS_2013_FINAL_STATE_ATTESTATION_2 = "3pg.block.08";
    /** Константа кода (code) элемента : Части (title) */
    String FGOS_2013_PARTS = "3pg.part";
    /** Константа кода (code) элемента : Базовая часть (title) */
    String FGOS_2013_PARTS_BASE = "3pg.part.01";
    /** Константа кода (code) элемента : Вариативная часть (title) */
    String FGOS_2013_PARTS_VARIATIVE = "3pg.part.02";

    Set<String> CODES = ImmutableSet.of(GOS, GOS_CYCLES, GOS_CYCLES_GENERAL, GOS_CYCLES_GENERAL_HUMANITARIAN_AND_SOCIO_ECONOMIC, GOS_CYCLES_GENERAL_MATHEMATICAL_AND_NATURAL_SCIENCES, GOS_CYCLES_GENERAL_PROFESSIONAL_DISCIPLINES, GOS_CYCLES_DIRECTION_DISCIPLINES, GOS_CYCLES_RESEARCH_WORK, GOS_CYCLES_ADDITIONAL_SPECIALITY_DISCIPLINES, GOS_CYCLES_SUBJECT_PREPARATION_DISCIPLINES, GOS_CYCLES_OPTIONAL_DISCIPLINES, GOS_CYCLES_PROFILE_PREPARATION_DISCIPLINES, GOS_CYCLES_SPECIAL_DISCIPLINES, GOS_CYCLES_SPECIALIZATION_DISCIPLINES, GOS_CYCLES_INSTRUCTIONAL_PRACTICE, GOS_CYCLES_WORKING_PRACTICE, GOS_CYCLES_PRACTICE, GOS_CYCLES_FINAL_STATE_ATTESTATION, GOS_CYCLES_HUMANITARIAN, GOS_CYCLES_NATURAL_SCIENCE, GOS_CYCLES_GENERAL_TECHNICAL, GOS_CYCLES_GENERAL_PROFESSIONAL, GOS_CYCLES_PROFESSIONAL, GOS_CYCLES_SPECIAL_OPTIONAL_DISCIPLINES, GOS_CYCLES_PLACEMENT, GOS_COMPONENTS, GOS_COMPONENTS_FEDERAL, GOS_COMPONENTS_REGIONAL, GOS_COMPONENTS_SELECTED_DISCIPLINES, GOS_COMPONENTS_SPECIAL_DISCIPLINES, GOS_COMPONENTS_SPECIALIZATION_DISCIPLINES, FGOS, FGOS_CYCLES, FGOS_CYCLES_GENERAL, FGOS_CYCLES_GENERAL_HUMANITARIAN_AND_SOCIO_ECONOMIC, FGOS_CYCLES_GENERAL_MATHEMATICAL_AND_NATURAL_SCIENCES, FGOS_CYCLES_PROFESSIONAL, FGOS_CYCLES_HUMANITARIAN_SOCIAL_AND_ECONOMIC, FGOS_CYCLES_MATHEMATICAL_AND_NATURAL_SCIENCES, FGOS_CYCLES_INSTRUCTIONAL_AND_WORKING_PRACTICE, FGOS_CYCLES_GENERAL_SCIENTIFIC, FGOS_CYCLES_INSTRUCTIONAL_AND_WORKING_PRACTICE_RESEARCH_WORK, FGOS_CYCLES_PRACTICE, FGOS_CYCLES_PRACTICE_AND_RESEARCH_WORK, FGOS_CYCLES_INSTRUCTIONAL_PRACTICE, FGOS_CYCLES_WORKING_PROFILE_PRACTICE, FGOS_CYCLES_WORKING_PREGRADUATION_PRACTICE, FGOS_CYCLES_FINAL_STATE_ATTESTATION, FGOS_CYCLES_STATE_FINAL_ATTESTATION, FGOS_CYCLES_GENERAL_PROFESSIONAL, FGOS_CYCLES_INSTRUCTIONAL_PRACTICE_WORKING_STUDY, FGOS_CYCLES_WORKING_PRACTICE, FGOS_CYCLES_INTERMEDIATE_PRACTICE, FGOS_CYCLES_PHYSICAL_TRAINING, FGOS_CYCLES_SPECIAL_DISCIPLINES, FGOS_CYCLES_SPECIAL_OPTIONAL_DISCIPLINES, FGOS_CYCLES_GENERAL_PROFESSIONAL_DISCIPLINES, FGOS_CYCLES_SPECIALIZATION_DISCIPLINES, FGOS_CYCLES_PLACEMENT, FGOS_CYCLES_INSTRUCTIONAL_AND_WORKING_PRACTICE_SUPERVISION_RESEARCH_WORK, FGOS_CYCLES_NATURAL_SCIENCE, FGOS_CYCLES_LEGAL_INFORMATIONAL, FGOS_CYCLES_HISTORY_AND_THEORY_OF_MUSICAL_ARTS, FGOS_CYCLES_HISTORY_AND_THEORY_OF_WORLD_CULTURE, FGOS_CYCLES_INFORMATIVE_COMMUNICATIVE, FGOS_CYCLES_ETHNIC, FGOS_CYCLES_ART, FGOS_CYCLES_FEDERAL_SPO_COMPONENT, FGOS_CYCLES_ADDITIONAL_WORK_WITH_TEACHER, FGOS_CYCLES_PROFESSIONAL_MODULES, FGOS_CYCLES_OPTIONAL_DISCIPLINES, FGOS_PARTS, FGOS_PARTS_BASE, FGOS_PARTS_VARIATIVE, FGOS_PARTS_GENERAL_PROFESSIONAL_DISCIPLINES, FGOS_PARTS_PROFESSIONAL_MODULES, FGOS_2013, FGOS_2013_BLOCKS, FGOS_2013_DISCIPLINES, FGOS_2013_PRACTICE, FGOS_2013_RESEARCH_WORK, FGOS_2013_FINAL_STATE_ATTESTATION, FGOS_2013_OPTIONAL_DISCIPLINES, FGOS_2013_PRACTICE_2, FGOS_2013_RESEARCH_WORK_2, FGOS_2013_FINAL_STATE_ATTESTATION_2, FGOS_2013_PARTS, FGOS_2013_PARTS_BASE, FGOS_2013_PARTS_VARIATIVE);
}
