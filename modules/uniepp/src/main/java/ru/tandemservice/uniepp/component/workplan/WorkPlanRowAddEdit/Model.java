package ru.tandemservice.uniepp.component.workplan.WorkPlanRowAddEdit;

import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import java.util.Collections;
import java.util.List;

@Input({ @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"),
         @Bind(key = "isRowAction", binding = "rowAction")
})
public class Model
{
    private Long id;
    private ISelectModel retakeSelectModel;
    private IIdentifiable needRetakeValue;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    private boolean _isRowAction;
    public boolean isRowAction() { return _isRowAction; }
    public void setRowAction(boolean action) { _isRowAction = action; }

    private EppWorkPlanRegistryElementRow row = new EppWorkPlanRegistryElementRow();
    public EppWorkPlanRegistryElementRow getRow() { return this.row; }
    public void setRow(final EppWorkPlanRegistryElementRow row) { this.row = row; }

    private final SelectModel<EppRegistryStructure> registryStructureSelectModel = new SelectModel<>();
    public SelectModel<EppRegistryStructure> getRegistryStructureSelectModel() { return this.registryStructureSelectModel; }

    private final SelectModel<EppRegistryElement> registryElementSelectModel = new SelectModel<>();
    public SelectModel<EppRegistryElement> getRegistryElementSelectModel() { return this.registryElementSelectModel; }

    private final SelectModel<EppRegistryElementPart> registryElementPartSelectModel = new SelectModel<>();
    public SelectModel<EppRegistryElementPart> getRegistryElementPartSelectModel() { return this.registryElementPartSelectModel; }

    public ISelectModel getRetakeSelectModel() { return retakeSelectModel; }
    public void setRetakeSelectModel(ISelectModel retakeSelectModel) { this.retakeSelectModel = retakeSelectModel; }

    public IIdentifiable getNeedRetakeValue() { return needRetakeValue; }
    public void setNeedRetakeValue(IIdentifiable needRetakeValue) { this.needRetakeValue = needRetakeValue; }

    private List<EppWorkPlanRowKind> kindList = Collections.emptyList();
    public List<EppWorkPlanRowKind> getKindList() { return this.kindList; }
    public void setKindList(final List<EppWorkPlanRowKind> kindList) { this.kindList = kindList; }

    public boolean isEditForm() {
        final IEntityMeta meta = EntityRuntime.getMeta(this.getId());
        return ((null != meta) && EppWorkPlanRegistryElementRow.class.isAssignableFrom(meta.getEntityClass()));
    }

    public String getTitle() {
        return UniEppUtils.getEppWorkPlanRowFormTitle(this.getId());
    }

    public String getRegElementTitle() {
        if (isRowAction()) return "Мероприятие реестра";
        return "Дисциплина";
    }
}
