package ru.tandemservice.uniepp.catalog.bo.EppPlanStructure.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author avedernikov
 * @since 28.08.2015
 */

@Configuration
public class EppPlanStructureAddEdit extends BusinessComponentManager
{
	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
			.create();
	}
}