package ru.tandemservice.uniepp.component.group.GroupWorkPlanTab;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

/**
 * 
 * @author nkokorina
 *
 */
public class BlockGridWrapper
{
    private String title;

    private StaticListDataSource<IEntity> dataSource = new StaticListDataSource<IEntity>();

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public StaticListDataSource<IEntity> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final StaticListDataSource<IEntity> dataSource)
    {
        this.dataSource = dataSource;
    }
}
