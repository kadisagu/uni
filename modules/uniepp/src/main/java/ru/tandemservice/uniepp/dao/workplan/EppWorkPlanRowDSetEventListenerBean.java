package ru.tandemservice.uniepp.dao.workplan;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import static org.tandemframework.hibsupport.dql.DQLFunctions.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;

import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

/**
 * @author vdanilov
 */
public class EppWorkPlanRowDSetEventListenerBean {

    /* проверяет, что в РУПе нет одинаковых дисциплиночастей (проверка на уровне entity.xml невозможна в силу ограничений платформы - по хорошему это надо перенести в entity.xml) */
    private static class EppWorkPlanRegistryElementRowUniqueConstraint extends TransactionCompleteAction<Boolean> implements IDSetEventListener {
        private static final EppWorkPlanRegistryElementRowUniqueConstraint INSTANCE = new EppWorkPlanRegistryElementRowUniqueConstraint();

        @Override public void onEvent(DSetEvent event) {
            register(event);
        }

        @Override public Boolean beforeCompletion(Session session)
        {
            // способ с группировкой нагло сперт из MSSQL - там все unique-constraint делаются именно так
            // проверка будет выполняться перед завершением транзакции один раз

            final List<Object[]> rows = new DQLSelectBuilder()
            .fromEntity(EppWorkPlanRegistryElementRow.class, "x")
            .column(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias("x")), "wp_id")
            .column(property(EppWorkPlanRegistryElementRow.registryElementPart().id().fromAlias("x")), "relp_id")
            .group(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias("x")))
            .group(property(EppWorkPlanRegistryElementRow.registryElementPart().id().fromAlias("x")))
            .having(gt(count(property("x.id")), value(1)))
            .createStatement(session).list();

            if (rows.isEmpty()) { return Boolean.FALSE; /* все ок */ }

            // не ок - формируем сообщение об ошибке

            final Set<Long> wpIds = new HashSet<>();
            for (Object[] row: rows) { wpIds.add((Long)row[0]); }

            final StringBuilder sb = new StringBuilder();
            for (EppWorkPlanBase wp : DataAccessServices.dao().getList(EppWorkPlanBase.class, wpIds)) {
                if (sb.length() > 0) { sb.append(", "); }
                sb.append(wp.getShortTitle()).append(" УПв ").append(wp.getEduPlanVersion().getFullNumber());
            }

            throw new ApplicationException(
                "Невозможно выполнить действие: в РУП(ы) "+sb+" добавлены дубликаты дисциплин. "
            );
        }
    }


    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppWorkPlanRegistryElementRow.class, EppWorkPlanRegistryElementRowUniqueConstraint.INSTANCE);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppWorkPlanRegistryElementRow.class, EppWorkPlanRegistryElementRowUniqueConstraint.INSTANCE);
    }

}
