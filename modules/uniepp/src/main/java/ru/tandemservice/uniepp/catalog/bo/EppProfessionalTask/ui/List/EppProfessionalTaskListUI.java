/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.IRootComponentActivationBuilder;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicPub.CatalogDynamicPubUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask.EppProfessionalTaskManager;
import ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask.ui.AddEdit.EppProfessionalTaskAddEditUI;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.catalog.EppProfessionalTask;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Igor Belanov
 * @since 23.03.2017
 */
@State({
        @Bind(key = DefaultCatalogPubModel.CATALOG_CODE, binding = "catalogCode"),
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "programSubjectId")
})
public class EppProfessionalTaskListUI extends CatalogDynamicPubUI
{
    // этот публикатор справочника может открываться из элемента справочника "Направление подготовки",
    // поэтому, возможно, оттуда придёт конкретное направление
    private Long _programSubjectId; // not null если зашли из справочника направления
    private EduProgramSubject _programSubject; // not null если зашли из справочника направления

    @Override
    public void onComponentRefresh()
    {
        // если к нам пришёл id направления, то вытащим направление
        if (_programSubjectId != null)
            _programSubject = DataAccessServices.dao().getNotNull(_programSubjectId);
        super.onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppProfessionalTaskList.PROGRAM_SUBJECT_INDEX_DS.equals(dataSource.getName()) ||
                EppProfessionalTaskList.PROGRAM_SUBJECT_DS.equals(dataSource.getName()))
        {
            EduProgramKind programKind = getProgramKind();
            EduProgramSubjectIndex programSubjectIndex = getProgramSubjectIndex();
            if (programKind != null)
                dataSource.put(EppProfessionalTaskManager.PROP_PROGRAM_KIND, programKind);
            if (programSubjectIndex != null)
                dataSource.put(EppProfessionalTaskManager.PROP_PROGRAM_SUBJECT_INDEX, programSubjectIndex);
        }
    }

    @Override
    public String getSettingsKey()
    {
        String additionalKey = "_" + (_programSubjectId != null ? Long.toString(_programSubjectId) : "common");
        return super.getSettingsKey() + additionalKey;
    }

    @Override
    public String getItemDsSettingsKey()
    {
        String additionalKey = "_" + (_programSubjectId != null ? Long.toString(_programSubjectId) : "common");
        return super.getSettingsKey() + additionalKey;
    }

    @Override
    protected void prepareItemDS()
    {
        super.prepareItemDS();

        // костыль (чтобы платформа не мерджила ячейки в столбце с нумерацией)
        getItemDS().getColumn(ActionColumn.DELETE).setMergeRowIdResolver(entity -> String.valueOf(entity.getId()));
    }

    @Override
    protected void addTitleColumns(DynamicListDataSource<ICatalogItem> dataSource)
    {
        // если мы в рамках конкретного направления, то колонка направления не нужна
        if (_programSubjectId == null)
        {
            // мердж направлений
            final IMergeRowIdResolver programSubjectResolver = entity -> {
                EppProfessionalTask professionalTask = ((EppProfessionalTask) entity);
                return String.valueOf(professionalTask.getProfActivityType().getProgramSubject().getId());
            };
            dataSource.addColumn(
                    new PublisherLinkColumn("Направление подготовки", EduProgramSubject.titleWithCode().s(), EppProfessionalTask.profActivityType().programSubject())
                            .setOrderable(false).setMergeRowIdResolver(programSubjectResolver));
        }
        // мердж видов проф.деятельности
        final IMergeRowIdResolver profActivityTypeResolver = entity -> {
            EppProfessionalTask professionalTask = ((EppProfessionalTask) entity);
            return String.valueOf(professionalTask.getProfActivityType().getId());
        };
        dataSource.addColumn(
                new PublisherLinkColumn("Вид профессиональной деятельности", EppProfActivityType.title().s(), EppProfessionalTask.profActivityType())
                        .setOrderable(false).setMergeRowIdResolver(profActivityTypeResolver));
        super.addTitleColumns(dataSource);
    }

    @Override
    protected void addDynamicColumns(DynamicListDataSource<ICatalogItem> dataSource)
    {
        // если зашли из направления, то углублённое изучение только для СПО
        if (_programSubjectId != null && getProgramKind().isProgramSecondaryProf())
        {
            dataSource.addColumn(new SimpleColumn("Углубленное изучение", EppProfessionalTask.profActivityType().inDepthStudy().s())
                    .setFormatter(YesNoFormatter.STRICT).setOrderable(false));
        }
        super.addDynamicColumns(dataSource);
    }

    @Override
    public String getAdditionalFiltersPageName()
    {
        return EppProfessionalTaskListUI.class.getPackage() + ".AdditionalFilters";
    }

    @Override
    protected void applyFilters(DQLSelectBuilder itemDql, String alias)
    {
        EduProgramKind programKind = getProgramKind();
        EduProgramSubjectIndex programSubjectIndex = getProgramSubjectIndex();
        EduProgramSubject programSubject = getProgramSubject();

        if (programKind != null)
            CommonBaseFilterUtil.applySelectFilter(itemDql, alias, EppProfessionalTask.profActivityType().programSubject().subjectIndex().programKind().s(), programKind);
        if (programSubjectIndex != null)
            CommonBaseFilterUtil.applySelectFilter(itemDql, alias, EppProfessionalTask.profActivityType().programSubject().subjectIndex().s(), programSubjectIndex);
        if (programSubject != null)
            CommonBaseFilterUtil.applySelectFilter(itemDql, alias, EppProfessionalTask.profActivityType().programSubject().s(), programSubject);
    }

    @Override
    protected void applyOrders(DQLSelectBuilder itemDql, String alias, DQLOrderDescriptionRegistry orderDescriptionRegistry, EntityOrder entityOrder)
    {
        // это не совсем приоритезированный справочник, его элементы группируются по нескольким полям
        // и приоритезируются в этих пределах, поэтому и сортировка несколько другая
        itemDql.order(property(EppProfessionalTask.profActivityType().programSubject().subjectCode().fromAlias(alias)));
        itemDql.order(property(EppProfessionalTask.profActivityType().programSubject().title().fromAlias(alias)));
        itemDql.order(property(EppProfessionalTask.profActivityType().programSubject().id().fromAlias(alias))); // foolproof (если code+title совпадёт)
        itemDql.order(property(EppProfessionalTask.profActivityType().priority().fromAlias(alias)));
        itemDql.order(property(EppProfessionalTask.profActivityType().id().fromAlias(alias))); // foolproof (если приоритет совпадёт)
        itemDql.order(property(EppProfessionalTask.priority().fromAlias(alias)));
        itemDql.order(property(EppProfessionalTask.id().fromAlias(alias))); // foolproof (если приоритет совпадёт)
    }

    public boolean isShowProgramSubjectFilters()
    {
        // выключаем фильтры если мы работаем в пределах конкретного направления подготовки
        return _programSubjectId == null;
    }

    @Override
    public void onClickAddItem()
    {
        IRootComponentActivationBuilder activationBuilder = _uiActivation
                .asDesktopRoot(CommonManager.catalogComponentProvider().getAddEditComponent(getItemClass()))
                .parameter(DefaultCatalogAddEditModel.CATALOG_CODE, getCatalogCode());
        if (_programSubjectId != null)
            activationBuilder.parameter(EppProfessionalTaskAddEditUI.EDU_PROGRAM_SUBJECT_ID, _programSubjectId);
        activationBuilder.activate();
    }

    @Override
    public void onClickEditItem()
    {
        _uiActivation.asDesktopRoot(CommonManager.catalogComponentProvider().getAddEditComponent(getItemClass()))
                .parameter(DefaultCatalogAddEditModel.CATALOG_ITEM_ID, getListenerParameterAsLong())
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()) // немножко костыль
                .activate();
    }

    @Override
    public boolean isStickerVisible()
    {
        return _programSubjectId == null && super.isStickerVisible();
    }

    @Override
    public String getListCaption()
    {
        return _programSubjectId != null ? "Профессиональные задачи" : super.getListCaption();
    }

    private EduProgramKind getProgramKind()
    {
        return _programSubjectId != null ? _programSubject.getEduProgramKind() : getSettings().get("programKind");
    }

    private EduProgramSubjectIndex getProgramSubjectIndex()
    {
        return _programSubjectId != null ? _programSubject.getSubjectIndex() : getSettings().get("programSubjectIndex");
    }

    private EduProgramSubject getProgramSubject()
    {
        return _programSubjectId != null ? _programSubject : getSettings().get("programSubject");
    }

    // getters & setters
    public Long getProgramSubjectId()
    {
        return _programSubjectId;
    }

    public void setProgramSubjectId(Long programSubjectId)
    {
        _programSubjectId = programSubjectId;
    }
}
