/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.edustd.EduStdDisciplinePub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.HibSupportUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniepp.dao.eduStd.data.IEppStdRowWrapper;
import ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineBaseRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineComment;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation;
import ru.tandemservice.uniepp.ui.ObligatoryMapFormatter;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author nkokorina
 * Created on: 08.09.2010
 */

@State( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id") })
public class Model
{
    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    private IEppStdRowWrapper element;
    public IEppStdRowWrapper getElement() { return this.element; }
    public void setElement(final IEppStdRowWrapper element) { this.element = element; }

    private DynamicListDataSource<EppStdRow2SkillRelation> dataSource;
    public void setDataSource(final DynamicListDataSource<EppStdRow2SkillRelation> _dataSource) {  this.dataSource = _dataSource; }
    public DynamicListDataSource<EppStdRow2SkillRelation> getDataSource() { return this.dataSource; }

    public String getComment()
    {
        return UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
            final EppStdDisciplineBaseRow row = HibSupportUtils.getEntityById(session, Model.this.getElement().getRow().getId());
            final EppStdDisciplineComment comment = UniDaoFacade.getCoreDao().getByNaturalId(new EppStdDisciplineComment.NaturalId(row));
            return StringUtils.trimToEmpty((null == comment) ? "" : comment.getComment());
        });
    }

    public String getObligatory()
    {
        final List<DevelopForm> formList = UniDaoFacade.getCoreDao().getCatalogItemList(DevelopForm.class);
        final LinkedHashMap<String, String> code2shortTitle = new LinkedHashMap<>(formList.size());
        for (final DevelopForm form : formList)
        {
            code2shortTitle.put(form.getCode(), form.getShortTitle());
        }

        return new ObligatoryMapFormatter(code2shortTitle).format(this.getElement().getObligatoryMap());
    }

    public boolean isShowCodesCompetencies()
    {
        return this.getElement().getEduStd().getStateEduStandard().getGeneration().showCompetencies();
    }
}
