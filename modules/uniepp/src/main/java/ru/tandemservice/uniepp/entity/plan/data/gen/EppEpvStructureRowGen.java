package ru.tandemservice.uniepp.entity.plan.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvHierarchyRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка УП (структурная)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvStructureRowGen extends EppEpvHierarchyRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow";
    public static final String ENTITY_NAME = "eppEpvStructureRow";
    public static final int VERSION_HASH = -1199815615;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";
    public static final String L_VALUE = "value";

    private EppEpvStructureRow _parent;     // Строка УП (структурная)
    private EppPlanStructure _value;     // Структура ГОС/УП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка УП (структурная).
     */
    public EppEpvStructureRow getParent()
    {
        return _parent;
    }

    /**
     * @param parent Строка УП (структурная).
     */
    public void setParent(EppEpvStructureRow parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Структура ГОС/УП. Свойство не может быть null.
     */
    @NotNull
    public EppPlanStructure getValue()
    {
        return _value;
    }

    /**
     * @param value Структура ГОС/УП. Свойство не может быть null.
     */
    public void setValue(EppPlanStructure value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEpvStructureRowGen)
        {
            setParent(((EppEpvStructureRow)another).getParent());
            setValue(((EppEpvStructureRow)another).getValue());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvStructureRowGen> extends EppEpvHierarchyRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvStructureRow.class;
        }

        public T newInstance()
        {
            return (T) new EppEpvStructureRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return obj.getParent();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "parent":
                    obj.setParent((EppEpvStructureRow) value);
                    return;
                case "value":
                    obj.setValue((EppPlanStructure) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return EppEpvStructureRow.class;
                case "value":
                    return EppPlanStructure.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvStructureRow> _dslPath = new Path<EppEpvStructureRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvStructureRow");
    }
            

    /**
     * @return Строка УП (структурная).
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow#getParent()
     */
    public static EppEpvStructureRow.Path<EppEpvStructureRow> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Структура ГОС/УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow#getValue()
     */
    public static EppPlanStructure.Path<EppPlanStructure> value()
    {
        return _dslPath.value();
    }

    public static class Path<E extends EppEpvStructureRow> extends EppEpvHierarchyRow.Path<E>
    {
        private EppEpvStructureRow.Path<EppEpvStructureRow> _parent;
        private EppPlanStructure.Path<EppPlanStructure> _value;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка УП (структурная).
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow#getParent()
     */
        public EppEpvStructureRow.Path<EppEpvStructureRow> parent()
        {
            if(_parent == null )
                _parent = new EppEpvStructureRow.Path<EppEpvStructureRow>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Структура ГОС/УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow#getValue()
     */
        public EppPlanStructure.Path<EppPlanStructure> value()
        {
            if(_value == null )
                _value = new EppPlanStructure.Path<EppPlanStructure>(L_VALUE, this);
            return _value;
        }

        public Class getEntityClass()
        {
            return EppEpvStructureRow.class;
        }

        public String getEntityName()
        {
            return "eppEpvStructureRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
