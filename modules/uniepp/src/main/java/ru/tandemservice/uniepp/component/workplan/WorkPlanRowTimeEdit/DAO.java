package ru.tandemservice.uniepp.component.workplan.WorkPlanRowTimeEdit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;

/**
 * @author vdanilov
 */

public class DAO extends UniDao<Model> implements IDAO
{


    @Override
    public void prepare(final Model model)
    {
        final Collection<Long> rowIds = this.getRowIds(model);
        model.setRowPartLoadMap(IEppWorkPlanDataDAO.instance.get().getRowPartLoadDataMap(rowIds));
        model.setPartsList(this.getPartList(model.getRowPartLoadMap()));

        if (rowIds.size() <= 1)
        {
            // если у нас только одна строка - то рисуем всю нагрузку
            final Set<EppLoadType> loadTypes = new LinkedHashSet<EppLoadType>();
            loadTypes.add(this.getCatalogItem(EppELoadType.class, EppELoadType.TYPE_TOTAL_AUDIT));
            loadTypes.addAll(this.getCatalogItemListOrderByCode(EppALoadType.class));
            loadTypes.addAll(this.getCatalogItemListOrderByCode(EppELoadType.class));
            model.setLoadTypeList(new ArrayList<EppLoadType>(loadTypes));
        }
        else
        {
            // если строк много - рисуем только аудиторную
            model.setLoadTypeList(new ArrayList<EppLoadType>(this.getCatalogItemListOrderByCode(EppALoadType.class)));
        }

    }

    private Collection<Long> getRowIds(final Model model) {

        final IEntity entity = this.getNotNull(model.getId());
        if (entity instanceof EppWorkPlanBase) {
            // если это план - берем все строки
            return UniBaseDao.ids(this.getList(EppWorkPlanRow.class, EppWorkPlanRow.workPlan().id(), entity.getId()));
        }
        if (entity instanceof EppWorkPlanRow) {
            // если это строка - то ее и берем
            return Collections.singleton(entity.getId());
        }

        throw new ClassCastException(entity.getClass().getName());
    }

    private List<Integer> getPartList(final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> rowPartLoadMap) {
        int last = 0;
        for (final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> partLoadMap : rowPartLoadMap.values()) {
            last = Math.max(last, Collections.max(partLoadMap.keySet()));
        }

        final List<Integer> list = new ArrayList<Integer>(last);
        for (int i=0;i<last;i++) { list.add(1+i); }
        return list;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(final Model model)
    {
        IEppWorkPlanDataDAO.instance.get().doUpdateRowPartLoadPeriodsMap(model.getRowPartLoadMap());
    }
}
