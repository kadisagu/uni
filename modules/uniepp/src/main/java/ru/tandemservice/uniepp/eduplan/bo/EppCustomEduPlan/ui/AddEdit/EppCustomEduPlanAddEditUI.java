/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.eduplan.IEppCustomEduPlanDAO;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.Pub.EppCustomEduPlanPub;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.Pub.EppCustomEduPlanPubUI;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import static ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.EppCustomEduPlanManager.*;

/**
 * @author Nikolay Fedorovskih
 * @since 17.08.2015
 */
@Input({
        @Bind(key=UIPresenter.PUBLISHER_ID, binding="customPlan.id"),
        @Bind(key = EppCustomEduPlanAddEditUI.BIND_PROGRAM_KIND_ID, binding = "programKind.id")
})
public class EppCustomEduPlanAddEditUI extends UIPresenter
{
    public static final String BIND_PROGRAM_KIND_ID = "programKind";

    private EppCustomEduPlan customPlan = new EppCustomEduPlan();
    private EduProgramKind programKind = new EduProgramKind();
    private EduProgramSubject programSubject;
    private EppEduPlanVersion eduPlanVersion;
    private boolean editForm;

    @Override
    public void onComponentRefresh()
    {
        final IUniBaseDao dao = IUniBaseDao.instance.get();
        editForm = getCustomPlan().getId() != null;

        if (isEditForm()) {
            setCustomPlan(dao.getNotNull(getCustomPlan().getId()));
            if (getCustomPlan().getState().isReadOnlyState()) {
                throw new ApplicationException("Индивидуальный учебный план находится в состоянии «" + getCustomPlan().getState().getTitle() + "», редактирование запрещено.");
            }
            if (getProgramKind().getId() != null) {
                throw new IllegalArgumentException();
            }
            setProgramKind(getCustomPlan().getPlan().getProgramKind());
            setProgramSubject(getCustomPlan().getPlan().getProgramSubject());
            setEduPlanVersion(getCustomPlan().getEpvBlock().getEduPlanVersion());

        } else {
            if (getProgramKind().getId() != null) {
                setProgramKind(dao.get(EduProgramKind.class, getProgramKind().getId()));
            }
            getCustomPlan().setDevelopCondition(dao.getCatalogItem(DevelopCondition.class, DevelopConditionCodes.ACCELERATED_PERIOD));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case PROGRAM_SUBJECT_DS:
                dataSource.put(PARAM_PROGRAM_KIND, getProgramKind());
                break;
            case EDU_PLAN_VERSION_DS:
                dataSource.put(PARAM_PROGRAM_SUBJECT, getProgramSubject());
                break;
            case EPV_BLOCK_DS:
                dataSource.put(PARAM_EDU_PLAN_VERSION, getEduPlanVersion());
                break;
        }
    }

    // Listeners

    public void onClickApply()
    {
        IEppCustomEduPlanDAO.instance.get().saveCustomEduPlan(getCustomPlan());

        deactivate();

        if (!isEditForm()) {
            // Сначала активируем карточку созданного ИУП, чтобы с формы редактирования семестров попадать в неё
            getActivationBuilder().asDesktopRoot(EppCustomEduPlanPub.class)
                    .parameter(UIPresenter.PUBLISHER_ID, getCustomPlan().getId())
                    .parameter(EppCustomEduPlanPubUI.OPEN_EDIT_CONTENT_FORM_BIND, true)
                    .activate();
        }
    }

    // Getters & Setters

    public boolean isEditForm()
    {
        return editForm;
    }

    public boolean isDisabled()
    {
        return isEditForm();
    }

    public EppCustomEduPlan getCustomPlan()
    {
        return this.customPlan;
    }

    public void setCustomPlan(EppCustomEduPlan customPlan)
    {
        this.customPlan = customPlan;
    }

    public EduProgramKind getProgramKind()
    {
        return this.programKind;
    }

    public void setProgramKind(EduProgramKind programKind)
    {
        this.programKind = programKind;
    }

    public EduProgramSubject getProgramSubject()
    {
        return programSubject;
    }

    public void setProgramSubject(EduProgramSubject programSubject)
    {
        this.programSubject = programSubject;
    }

    public EppEduPlanVersion getEduPlanVersion()
    {
        return eduPlanVersion;
    }

    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        this.eduPlanVersion = eduPlanVersion;
    }
}