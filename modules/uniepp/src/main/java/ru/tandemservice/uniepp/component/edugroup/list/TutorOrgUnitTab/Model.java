package ru.tandemservice.uniepp.component.edugroup.list.TutorOrgUnitTab;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRealEduGroupCompleteLevelCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppRealEduGroupCompleteLevelGen;


/**
 * @author vdanilov
 */
public class Model extends ru.tandemservice.uniepp.component.edugroup.list.GroupListOwnerTabBase.Model {
    @Override public String getGroupContentComponentName() {
        return ru.tandemservice.uniepp.component.edugroup.pub.TutorOrgUnitGroupPub.Model.COMPONENT_NAME;
    }
    @Override public String getSecModelPostfix(final String postfix) {
        return (null == postfix ? null : ("tutor_"+postfix));
    }
    @Override public EppRealEduGroupCompleteLevel getLevel() {
        return IUniBaseDao.instance.get().getByNaturalId(new EppRealEduGroupCompleteLevelGen.NaturalId(EppRealEduGroupCompleteLevelCodes.LEVEL_ACTIVITY_OWNER_ORG_UNIT));
    }
}
