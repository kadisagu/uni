/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.ElementDuplicationMerge;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 15.05.2015
 */
public class RowWrapper extends RowWrapperBase
{
    static class PartLoadInfo
    {
        private String _load;
        private String _controlActions;
        public PartLoadInfo(String load, String ca)
        {
            _load = load;
            _controlActions = ca;
        }
        public String getLoad()
        {
            return _load;
        }
        public String getControlActions()
        {
            return _controlActions;
        }
    }
    private final int index;
    private final EppRegistryElement element;
    private boolean selected;
    private List<PartLoadInfo> partsLoad;
    private String splitVariant;

    public RowWrapper(EppRegistryElement element, List<PartLoadInfo> partsLoad, int index, String splitVariant)
    {
        this.element = element;
        this.partsLoad = partsLoad;
        this.index = index;
        this.splitVariant = splitVariant;
    }

    public Long getId()
    {
        return element.getId();
    }

    public int getIndex()
    {
        return index;
    }

    public EppRegistryElement getElement()
    {
        return element;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public List<PartLoadInfo> getPartsLoad()
    {
        return partsLoad;
    }

    public String getLoadSize()
    {
        return UniEppUtils.LOAD_FORMATTER.format(getElement().getSizeAsDouble());
    }

    public String getLabor()
    {
        return UniEppUtils.LOAD_FORMATTER.format(getElement().getLaborAsDouble());
    }

    public String getSplitVariant() {
        return splitVariant;
    }

    @Override public boolean isSplitterByKey() { return false; }
    @Override public boolean isSplitterByTitle() { return false; }
    @Override public String getSplitterStyle() { return ""; }
}