package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_2x11x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppEpvRegRowProfessionalTask

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_epvregrow_prof_task_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_38651278"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("epvregistryrow_id", DBType.LONG).setNullable(false), 
				new DBColumn("professionaltask_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppEpvRegRowProfessionalTask");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppProfessionalTask

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_c_prof_task_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eppprofessionaltask"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200)).setNullable(false), 
				new DBColumn("shorttitle_p", DBType.createVarchar(255)), 
				new DBColumn("profactivitytype_id", DBType.LONG).setNullable(false), 
				new DBColumn("priority_p", DBType.INTEGER).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppProfessionalTask");

		}


    }
}