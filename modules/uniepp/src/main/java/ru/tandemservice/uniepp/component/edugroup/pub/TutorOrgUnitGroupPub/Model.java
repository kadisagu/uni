/* $Id: Model.java 19933 2011-09-13 11:50:59Z oleyba $ */
package ru.tandemservice.uniepp.component.edugroup.pub.TutorOrgUnitGroupPub;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRealEduGroupCompleteLevelCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppRealEduGroupCompleteLevelGen;


/**
 * @author oleyba
 * @since 9/13/11
 */
public class Model extends ru.tandemservice.uniepp.component.edugroup.pub.GroupPubBase.Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();

    @Override public String getSecModelPostfix(final String postfix) {
        return (null == postfix ? null : ("tutor_"+postfix));
    }
    @Override public EppRealEduGroupCompleteLevel getLevel() {
        return IUniBaseDao.instance.get().getByNaturalId(new EppRealEduGroupCompleteLevelGen.NaturalId(EppRealEduGroupCompleteLevelCodes.LEVEL_ACTIVITY_OWNER_ORG_UNIT));
    }
}
