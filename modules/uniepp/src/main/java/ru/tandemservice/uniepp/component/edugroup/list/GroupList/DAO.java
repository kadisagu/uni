package ru.tandemservice.uniepp.component.edugroup.list.GroupList;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.gen.CourseGen;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.component.edugroup.GroupViewWrapperBatchAction;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.ui.EppOrgUnitSelectModel;
import ru.tandemservice.uniepp.ui.EppRegistryElementSelectModel;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    @Override
    public void doChangeLevel(final Model model, final Collection<Long> ids, final Predicate<EppRealEduGroup> denyPredicate, final String message, final String code)
    {
        if (CollectionUtils.exists(getList(EppRealEduGroup.class, "id", ids), denyPredicate)) {
            UserContext.getInstance().getErrorCollector().add(message);
            return;
        }

        IEppRealGroupRowDAO.instance.get().checkGroupLockedLevel(ids, model.getLevel());
        IEppRealGroupRowDAO.instance.get().doSetLevel(ids, getCatalogItem(EppRealEduGroupCompleteLevel.class, code));
    }

    @Override
    public void prepare(final Model model) {
        {
            model.getOrgUnitHolder().refresh();

            final String settingsKey = "epp.groups." + model.getOrgUnitId();
            model.setSettings(DataSettingsFacade.getSettings(UserContext.getInstance().getPrincipal().getId().toString(), settingsKey));
        }

        // степень готовности (все)
        {
            final List<HSelectOption> levelHierarchyList = new ArrayList<>();
            final EppRealEduGroupCompleteLevel fake = new EppRealEduGroupCompleteLevel();
            fake.setId(0L);
            fake.setTitle("Этап не указан");
            levelHierarchyList.add(new HSelectOption(fake, 0, true));
            levelHierarchyList.addAll(HierarchyUtil.listHierarchyNodesWithParents(getList(EppRealEduGroupCompleteLevel.class, EppRealEduGroupCompleteLevel.P_CODE), null, true));
            for (final HSelectOption s: levelHierarchyList) {
                final EppRealEduGroupCompleteLevel o = (EppRealEduGroupCompleteLevel)s.getObject();
                s.setObject(new IdentifiableWrapper<>(o.getId(), o.getDisplayableTitle()));
            }
            model.setLevelHierarchyList(levelHierarchyList);
        }

        // формирующие подразделение НПП студента на момент сохранения в слот
        model.setFormativeOrgUnitModel(new EppOrgUnitSelectModel() {
            @Override protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = model.getDqlFactory().create()
                .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))))
                .column(property(EppRealEduGroupRow.studentEducationOrgUnit().formativeOrgUnit().id().fromAlias("rel")));

                return super.query(alias, filter).where(in(property(alias, "id"), s.buildQuery()));
            }
        });

        // территориальное подразделение НПП студента на момент сохранения в слот
        model.setTerritorialOrgUnitModel(new DQLFullCheckSelectModel(OrgUnit.territorialFullTitle().s()) {
            @Override protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = model.getDqlFactory().create()
                .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))))
                .column(property(EppRealEduGroupRow.studentEducationOrgUnit().territorialOrgUnit().id().fromAlias("rel")));

                final Collection<OrgUnit> formativeOrgUnits = model.getFormativeOrgUnits();
                if (null != formativeOrgUnits) {
                    s.where(in(property(EppRealEduGroupRow.studentEducationOrgUnit().formativeOrgUnit().fromAlias("rel")), formativeOrgUnits));
                }

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnit.class, alias);
                dql.where(in(property(alias, "id"), s.buildQuery()));
                if (null != filter) {
                    dql.where(or(
                        like(OrgUnitGen.territorialTitle().fromAlias(alias), filter),
                        like(OrgUnitGen.territorialShortTitle().fromAlias(alias), filter),
                        like(OrgUnitGen.territorialFullTitle().fromAlias(alias), filter)
                    ));
                }

                dql.order(property(OrgUnitGen.title().fromAlias(alias)));
                return dql;
            }
        });

        // направление подготовки (из НПП)
        model.setEducationHighSchoolModel(new DQLFullCheckSelectModel(EducationLevelsHighSchool.fullTitle().s()) {
            @Override protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = model.getDqlFactory().create()
                .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))))
                .column(property(EppRealEduGroupRow.studentEducationOrgUnit().educationLevelHighSchool().id().fromAlias("rel")));

                final Collection<OrgUnit> formativeOrgUnits = model.getFormativeOrgUnits();
                if (null != formativeOrgUnits) {
                    s.where(in(property(EppRealEduGroupRow.studentEducationOrgUnit().formativeOrgUnit().fromAlias("rel")), formativeOrgUnits));
                }

                final Collection<OrgUnit> territorialOrgUnits = model.getTerritorialOrgUnits();
                if (null != territorialOrgUnits) {
                    s.where(in(property(EppRealEduGroupRow.studentEducationOrgUnit().territorialOrgUnit().fromAlias("rel")), territorialOrgUnits));
                }

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, alias);
                dql.where(in(property(alias, "id"), s.buildQuery()));
                if (null != filter) {
                    dql.where(or(
                        like(EducationLevelsHighSchool.fullTitle().fromAlias(alias), filter),
                        like(EducationLevelsHighSchool.title().fromAlias(alias), filter),
                        like(EducationLevelsHighSchool.shortTitle().fromAlias(alias), filter)
                    ));
                }

                dql.order(property(EducationLevelsHighSchool.fullTitle().fromAlias(alias)));
                return dql;
            }
        });


        // читающие подразделения (на которые есть УГС)
        model.setRegistryOwnerModel(new EppOrgUnitSelectModel() {
            @Override protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder s = model.getDqlFactory().create()
                .column(property(EppRealEduGroup.activityPart().registryElement().owner().id().fromAlias("g")));

                return super.query(alias, filter).where(in(property(alias, "id"), s.buildQuery()));
            }
        });

        // дисциплины (на которые есть УГС)
        model.setRegistryElementModel(new EppRegistryElementSelectModel<EppRegistryElement>(EppRegistryElement.class) {
            @Override protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder dql = super.query(alias, filter);

                {
                    final DQLSelectBuilder s = model.getDqlFactory().create()
                    .column(property(EppRealEduGroup.activityPart().registryElement().id().fromAlias("g")));

                    dql.where(in(property(alias, "id"), s.buildQuery()));
                }

                {
                    final Collection registryOwners = model.getRegistryOwners();
                    if (null != registryOwners) {
                        dql.where(in(property(EppRegistryElementGen.owner().fromAlias(alias)), registryOwners));
                    }
                }

                return dql;
            }
        });


        // курсы из УГС
        {
            model.setCourseModel(new DQLFullCheckSelectModel("title") {

                // если надо обновить - нажмут f5
                private final SingleValueCache<List<Long>> idsCache = new SingleValueCache<List<Long>>() {
                    @Override protected List<Long> resolve() {
                        final DQLSelectBuilder dql = model.getDqlFactory().create()
                        .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))))
                        .column(property(EppRealEduGroupRow.studentWpePart().studentWpe().course().id().fromAlias("rel")))
                        .predicate(DQLPredicateType.distinct);
                        return dql.createStatement(getSession()).list();
                    }
                };

                @Override protected DQLSelectBuilder query(final String alias, final String filter)
                {
                    final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Course.class, alias);
                    dql.where(in(property(alias, "id"), idsCache.get()));
                    if (null != filter) {
                        dql.where(like(CourseGen.title().fromAlias(alias), filter));
                    }
                    dql.order(property(CourseGen.intValue().fromAlias(alias)));
                    return dql;
                }
            });
        }

        // названия академических групп, на которые есть УГС
        model.setGroupModel(UniEppUtils.getTitleSelectModel(new SingleValueCache<List<String>>() {
            @Override protected List<String> resolve() {
                final DQLSelectBuilder s = model.getDqlFactory().create()
                .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))))
                .column(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("rel")))
                .order(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("rel")))
                .predicate(DQLPredicateType.distinct);

                return s.createStatement(getSession()).list(); // если надо обновить - нажмут f5
            }
        }));

        // ФИО студентов, на которые есть УГС
        model.setStudentTitleModel(new BaseSingleSelectModel("", "") {
            @Override public Object getPrimaryKey(final Object value) { return value; }
            @Override public Object getValue(final Object primaryKey) { return primaryKey; }
            @Override public String getFullLabel(final Object value) { return String.valueOf(value); }
            @Override public String getLabelFor(final Object value, final int columnIndex) { return getFullLabel(value); }
            @Override public ListResult findValues(String filter) {
                final DQLSelectBuilder s = model.getDqlFactory().create()
                .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))))
                .joinPath(DQLJoinType.inner, EppRealEduGroupRow.studentWpePart().studentWpe().student().person().identityCard().fromAlias("rel"), "idc")
                .column(property(IdentityCard.fullFio().fromAlias("idc")))
                .order(property(IdentityCard.fullFio().fromAlias("idc")))
                .predicate(DQLPredicateType.distinct);

                filter = StringUtils.trimToNull(filter);
                if (null != filter) {
                    s.where(like(DQLFunctions.upper(property(IdentityCard.fullFio().fromAlias("idc"))), value(filter.toUpperCase().replace(" ", "% ") + "%")));
                }

                // выводим всех! нельзя выводить только первые 100! (иначе работать не будет)
                return new ListResult<>(s.createStatement(getSession()).<String>list());
            }
        });

        // ФИО ППС, связанных с УГС
        model.setPpsTitleModel(new BaseSingleSelectModel("", "") {
            @Override public Object getPrimaryKey(final Object value) { return value; }
            @Override public Object getValue(final Object primaryKey) { return primaryKey; }
            @Override public String getFullLabel(final Object value) { return String.valueOf(value); }
            @Override public String getLabelFor(final Object value, final int columnIndex) { return getFullLabel(value); }
            @Override public ListResult findValues(String filter) {
                final DQLSelectBuilder s = new DQLSelectBuilder()
                .fromEntity(EppPpsCollectionItem.class, "pps")
                .predicate(DQLPredicateType.distinct)
                .joinPath(DQLJoinType.inner, EppPpsCollectionItem.pps().person().identityCard().fromAlias("pps"), "idc")
                .column(property(IdentityCard.fullFio().fromAlias("idc")))
                .order(property(IdentityCard.fullFio().fromAlias("idc")))

                .joinDataSource(
                    "pps",
                    DQLJoinType.inner,
                    model.getDqlFactory().create()
                    .column(property("g.id"), "id").buildQuery(),
                    "xxx",
                    eq(property("xxx.id"), property(EppPpsCollectionItem.list().id().fromAlias("pps")))
                );

                filter = StringUtils.trimToNull(filter);
                if (null != filter) {
                    s.where(like(DQLFunctions.upper(property(IdentityCard.fullFio().fromAlias("idc"))), value(filter.toUpperCase().replace(" ", "% ") + "%")));
                }

                // выводим всех! нельзя выводить только первые 100! (иначе работать не будет)
                return new ListResult<>(s.createStatement(getSession()).<String>list());
            }
        });


        // TODO: WTF? переписать
        final int currentLevel = Integer.valueOf(model.getLevel().getRoot().getCode());
        final List<String> levels = new ArrayList<>();
        for (int i = 1; i < currentLevel; i++) { levels.add(String.valueOf(i)); }
        model.setLevels(getList(EppRealEduGroupCompleteLevel.class, "code", levels));
    }


    @Override
    public void prepareDataSource(final Model model) {
        final Session session = getSession();
        Debug.begin("prepareDataSource");
        try {
            final DynamicListDataSource<EppRealEduGroup> dataSource = model.getDataSource();

            Debug.begin("prepareDataSource.main");
            try {
                final DQLSelectBuilder groupWithRelationsDql = model.getDqlFactory().create();

                // фильтры
                {
                    applyfilter_fillstatus(model, groupWithRelationsDql);
                    applyfilter_level(model, groupWithRelationsDql);
                    applyfiler_registry(model, groupWithRelationsDql);
                    applyfiler_course(model, groupWithRelationsDql);
                    applyfiler_group(model, groupWithRelationsDql);
                    applyfiler_student(model, groupWithRelationsDql);
                    applyfiler_pps(model, groupWithRelationsDql);
                    applyfiler_eduou(model, groupWithRelationsDql);
                }

                final EntityOrder entityOrder = dataSource.getEntityOrder();
                if ("course".equals(entityOrder.getKey())) {
                    createPage(dataSource, groupWithRelationsDql, EppRealEduGroupRow.studentWpePart().studentWpe().course().intValue());
                } else if ("group".equals(entityOrder.getKey())) {
                    createPage(dataSource, groupWithRelationsDql, EppRealEduGroupRow.studentGroupTitle());
                } else if ("educationOrgUnit".equals(entityOrder.getKey())) {
                    createPage(
                        dataSource, groupWithRelationsDql,
                        EppRealEduGroupRow.studentEducationOrgUnit().educationLevelHighSchool().code(),
                        EppRealEduGroupRow.studentEducationOrgUnit().developForm().code(),
                        EppRealEduGroupRow.studentEducationOrgUnit().developCondition().code(),
                        EppRealEduGroupRow.studentEducationOrgUnit().developTech().code(),
                        EppRealEduGroupRow.studentEducationOrgUnit().developPeriod().code()
                    );
                } else {
                    DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(model.getRelationDescription().groupClass(), "grp");
                    DQLSelectBuilder dql = order.buildDQLSelectBuilder().where(in(property("grp.id"), groupWithRelationsDql.column(property(EppRealEduGroupRow.group().id().fromAlias("rel"))).buildQuery()));
                    order.applyOrderWithLeftJoins(dql, entityOrder);
                    UniBaseUtils.createPage(dataSource, dql.column(property("grp")), session);
                }

            } finally {
                Debug.end();
            }

            final List<ViewWrapper<EppRealEduGroup>> list = ViewWrapper.getPatchedList(dataSource);
            BatchUtils.execute(list, 200, new GroupViewWrapperBatchAction<EppRealEduGroup>(model.getRelationDescription().relationClass(), getSession()) {
                @Override public void execute(final Collection<ViewWrapper<EppRealEduGroup>> list)
                {
                    Debug.begin("prepareDataSource.patch("+list.size()+")");
                    try {

                        final Collection<Long> ids = UniBaseDao.ids(list);

                        Debug.begin("preload-registry-elements("+ids.size()+")");
                        try {
                            final DQLSelectBuilder dql = new DQLSelectBuilder();
                            dql.fromEntity(EppRegistryElementPart.class, "rep").column(property("rep"));
                            dql.fetchPath(DQLJoinType.inner, EppRegistryElementPart.registryElement().fromAlias("rep"), "re");
                            dql.where(in(
                                property("rep.id"),
                                new DQLSelectBuilder()
                                .fromEntity(EppRealEduGroup.class, "g").column(property(EppRealEduGroup.activityPart().id().fromAlias("g")))
                                .predicate(DQLPredicateType.distinct)
                                .where(in(property("g.id"), ids))
                                .buildQuery()
                            ));
                            dql.createStatement(session).list();
                        } finally {
                            Debug.end();
                        }

                        final Map<Long, Map<Integer, Integer>> courseMap = getRelationMap(ids, EppRealEduGroupRow.studentWpePart().studentWpe().course().intValue());
                        final Map<Long, Map<String, Integer>> eppStudentGroupMap = getRelationMap(ids, EppRealEduGroupRow.studentGroupTitle());
                        final Map<Long, Map<Long, Integer>> eppEduOrgUnitMap = getRelationMap(ids, EppRealEduGroupRow.studentEducationOrgUnit().id());
                        final Map<Long, Map<Long, Integer>> tutorMap = getTutorMap(ids);

                        for (final ViewWrapper<EppRealEduGroup> grp: list)
                        {
                            grp.setViewProperty("group", collect(eppStudentGroupMap, grp.getId(), groupFormatter, true));
                            grp.setViewProperty("course", collect(courseMap, grp.getId(), null, false));
                            grp.setViewProperty("educationOrgUnit", collect(eppEduOrgUnitMap, grp.getId(), educationOrgUnitFormatter, true));
                            grp.setViewProperty("tutor", collect(tutorMap, grp.getId(), ppsFormatter, false));

                            {
                                final Map<String, Integer> grpDistribution = eppStudentGroupMap.get(grp.getId());
                                if (null != grpDistribution)
                                {

                                    int students = 0;
                                    for (final Integer s : grpDistribution.values()) {
                                        if (null != s) { students += s; }
                                    }

                                    //grp.setViewProperty("load_t", Boolean.valueOf(grpDistribution.size() > 1));
                                    grp.setViewProperty("load_g", grpDistribution.size());
                                    grp.setViewProperty("load_s", students);
                                    grp.setViewProperty("empty", students <= 0);
                                }
                                else
                                {
                                    //grp.setViewProperty("load_t", null);
                                    grp.setViewProperty("load_g", null);
                                    grp.setViewProperty("load_s", null);
                                    grp.setViewProperty("empty", Boolean.TRUE);
                                }
                            }
                        }
                    } finally {
                        Debug.end();
                    }
                }
            });

        } finally {
            Debug.end();
        }
    }


    protected void applyfilter_fillstatus(final Model model, final DQLSelectBuilder groupWithRelationsDql) {
        final IdentifiableWrapper<IEntity> fillStatus = model.getFilterFillStatus();
        if ((null == fillStatus) || (null == fillStatus.getId())) { return; }

        if (Model.FILL_STATUS_EMPTY.equals(fillStatus.getId()))
        {
            // нет инодной активной связи (важно, что активной - придется делать join)
            groupWithRelationsDql.joinEntity("g", DQLJoinType.left, model.getRelationDescription().relationClass(), "grp_rel_null", and(
                eq(property(EppRealEduGroupRow.group().fromAlias("grp_rel_null")), property("g")),
                isNull(property(EppRealEduGroupRow.removalDate().fromAlias("grp_rel_null")))
            ));
            groupWithRelationsDql.where(isNull(property("grp_rel_null.id")));
        }
        else if (Model.FILL_STATUS_FILLED.equals(fillStatus.getId()))
        {
            // есть хотябы одна активная связь
            groupWithRelationsDql.where(isNotNull("rel.id"));
            groupWithRelationsDql.where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))));
        }

    }

    protected void applyfilter_level(final Model model, final DQLSelectBuilder groupWithRelationsDql)
    {
        final IEntity filterLevel = model.getFilterLevel();
        if ((null == filterLevel) ||(null == filterLevel.getId())) { return; }

        if (0L == filterLevel.getId()) {
            groupWithRelationsDql.where(isNull(property(EppRealEduGroup.level().fromAlias("g"))));
        } else {
            final List<EppRealEduGroupCompleteLevel> list = getList(EppRealEduGroupCompleteLevel.class);
            final EppRealEduGroupCompleteLevel selected = (EppRealEduGroupCompleteLevel)getSession().load(EppRealEduGroupCompleteLevel.class, filterLevel.getId());
            final Collection select = CollectionUtils.select(list, object -> object.isChildOf(selected));
            groupWithRelationsDql.where(in(property(EppRealEduGroup.level().id().fromAlias("g")), select));
        }
    }

    protected void applyfiler_registry(final Model model, final DQLSelectBuilder groupWithRelationsDql)
    {
        final Collection registryElements = model.getRegistryElements();
        if (null != registryElements) {
            groupWithRelationsDql.where(in(property(EppRealEduGroup.activityPart().registryElement().fromAlias("g")), registryElements));
        } else {
            final Collection registryOwners = model.getRegistryOwners();
            if (null != registryOwners) {
                groupWithRelationsDql.where(in(property(EppRealEduGroup.activityPart().registryElement().owner().fromAlias("g")), registryOwners));
            }
        }
    }

    protected void applyfiler_course(final Model model, final DQLSelectBuilder groupWithRelationsDql)
    {
        final Collection courses = model.getCourses();
        if (null != courses) {
            groupWithRelationsDql.where(in(property(EppRealEduGroupRow.studentWpePart().studentWpe().course().fromAlias("rel")) , courses));
        }
    }

    protected void applyfiler_group(final Model model, final DQLSelectBuilder groupWithRelationsDql)
    {
        final Collection groups = model.getGroups();
        if (null != groups) {
            if (groups.contains("")) {
                groupWithRelationsDql.where(or(
                    in(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("rel")), groups),
                    isNull(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("rel")))
                ));
            } else {
                groupWithRelationsDql.where(in(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("rel")), groups));
            }
        }
    }

    protected void applyfiler_student(final Model model, final DQLSelectBuilder groupWithRelationsDql)
    {
        final String title = model.getStudentTitle();
        if (null != title) {
            groupWithRelationsDql.where(
                likeUpper(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().person().identityCard().fullFio().fromAlias("rel")),
                    value(CoreStringUtils.escapeLike(title, true))
                ));
        }
    }

    protected void applyfiler_pps(final Model model, final DQLSelectBuilder groupWithRelationsDql)
    {
        final String title = model.getPpsTitle();
        if (null != title) {
            groupWithRelationsDql.where(exists(
                new DQLSelectBuilder()
                .fromEntity(EppPpsCollectionItem.class, "pps").column("pps.id")
                .where(eq(property("g.id"), property(EppPpsCollectionItem.list().id().fromAlias("pps"))))
                .where(likeUpper(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("pps")),
                    value(CoreStringUtils.escapeLike(title, true))
                ))
                .buildQuery()
            ));
        }
    }

    protected void applyfiler_eduou(final Model model, final DQLSelectBuilder groupWithRelationsDql)
    {
        final Collection<OrgUnit> formativeOrgUnits = model.getFormativeOrgUnits();
        if (null != formativeOrgUnits) {
            groupWithRelationsDql.where(in(property(EppRealEduGroupRow.studentEducationOrgUnit().formativeOrgUnit().fromAlias("rel")), formativeOrgUnits));
        }

        final Collection<OrgUnit> territorialOrgUnits = model.getTerritorialOrgUnits();
        if (null != territorialOrgUnits) {
            groupWithRelationsDql.where(in(property(EppRealEduGroupRow.studentEducationOrgUnit().territorialOrgUnit().fromAlias("rel")), territorialOrgUnits));
        }

        final Collection<EducationLevelsHighSchool> educationHighSchools = model.getEducationHighSchools();
        if (null != educationHighSchools) {
            groupWithRelationsDql.where(in(property(EppRealEduGroupRow.studentEducationOrgUnit().educationLevelHighSchool().fromAlias("rel")), educationHighSchools));
        }
    }



    @SuppressWarnings("unchecked")
    private void createPage(final DynamicListDataSource<EppRealEduGroup> dataSource, final DQLSelectBuilder groupIdsDql, final PropertyPath ...path) {
        Debug.begin("createPage("+Arrays.asList(path).toString()+")");
        try {
            List<Long> ids = getComparedIds(groupIdsDql, path);

            if (dataSource.getEntityOrder().getDirection() == OrderDirection.desc) { Collections.reverse(ids); }
            dataSource.setTotalSize(ids.size());

            final int startRow = (int) dataSource.getStartRow();
            final int countRow = (int) dataSource.getCountRow();
            ids = ids.subList(startRow, Math.min(startRow+countRow, ids.size()));

            Debug.begin("list-by-ids");
            dataSource.createPage((List)CommonDAO.sort(getListByIds(ids), ids));
            Debug.end();
        } finally {
            Debug.end();
        }
    }

    private List<Long> getComparedIds(final DQLSelectBuilder groupIdsDql, final PropertyPath ...propertyPath) {

        // id группы
        groupIdsDql.column(property(EppRealEduGroup.id().fromAlias("g")));
        groupIdsDql.order(property(EppRealEduGroup.id().fromAlias("g")));

        // все остальные поля
        for (final PropertyPath path: propertyPath) {
            groupIdsDql.column(property(path.fromAlias("rel")));
            groupIdsDql.order(property(path.fromAlias("rel")));
        }

        groupIdsDql.predicate(DQLPredicateType.distinct);
        return getSortedIds(groupIdsDql.createStatement(getSession()).<Object[]>list(), propertyPath.length);
    }

}
