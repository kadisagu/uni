package ru.tandemservice.uniepp.entity.contract;

import ru.tandemservice.uniepp.entity.contract.gen.*;

/**
 * Данные шаблона версии договора на обучение
 *
 * Данные базового шаблона для создания договора или доп.соглашения на обучение.
 */
public abstract class EppCtrContractVersionTemplateData extends EppCtrContractVersionTemplateDataGen
{
}