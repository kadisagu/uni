package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused"})
public class MS_uniepp_2x7x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppEduPlanVersion

		// создано обязательное свойство hoursI
		if (!tool.columnExists("epp_eduplan_ver_t", "hoursi_p"))
		{
			// создать колонку
			tool.createColumn("epp_eduplan_ver_t", new DBColumn("hoursi_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update epp_eduplan_ver_t set hoursi_p=? where hoursi_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_eduplan_ver_t", "hoursi_p", false);
		}

		// создано обязательное свойство hoursE
		if (!tool.columnExists("epp_eduplan_ver_t", "hourse_p"))
		{
			// создать колонку
			tool.createColumn("epp_eduplan_ver_t", new DBColumn("hourse_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update epp_eduplan_ver_t set hourse_p=? where hourse_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_eduplan_ver_t", "hourse_p", false);
		}

    }
}