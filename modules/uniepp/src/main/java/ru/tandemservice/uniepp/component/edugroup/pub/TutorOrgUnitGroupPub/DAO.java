/* $Id: DAO.java 19933 2011-09-13 11:50:59Z oleyba $ */
package ru.tandemservice.uniepp.component.edugroup.pub.TutorOrgUnitGroupPub;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;

/**
 * @author oleyba
 * @since 9/13/11
 */
public class DAO extends ru.tandemservice.uniepp.component.edugroup.pub.GroupPubBase.DAO implements IDAO
{
    @Override public boolean checkAvailability(final ru.tandemservice.uniepp.component.edugroup.pub.GroupPubBase.Model model, final EppRealEduGroup eduGroup, final OrgUnit orgUnit)
    {
        return null != orgUnit && orgUnit.equals(eduGroup.getActivityPart().getTutorOu());
    }
}
