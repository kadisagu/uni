package ru.tandemservice.uniepp.component.workplan.WorkPlanRowTimeTab;

import ru.tandemservice.uni.dao.IPrepareable;


/**
 * 
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model>
{

    void doFillFromWorkGraph(Model model);

}
