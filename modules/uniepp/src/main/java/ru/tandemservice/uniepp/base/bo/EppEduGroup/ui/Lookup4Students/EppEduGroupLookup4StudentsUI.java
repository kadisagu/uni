/**
 *$Id: EppWorkPlanStudentListUI.java 26089 2013-02-11 08:34:37Z vdanilov $
 */
package ru.tandemservice.uniepp.base.bo.EppEduGroup.ui.Lookup4Students;

import org.apache.commons.collections15.CollectionUtils;
import org.hibernate.Session;
import org.hibernate.UnresolvableObjectException;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow.IEppRealEduGroupRowDescription;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary;

import java.util.*;
import java.util.Map.Entry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

@Input({
    @Bind(key=EppEduGroupLookup4StudentsUI.PARAM_ROW_IDS, binding="eduGroupRowIds", required=true)
})
public class EppEduGroupLookup4StudentsUI extends UIPresenter
{
    public static final String PARAM_ROW_IDS = "eduGroupRowIds";

    private Collection<Long> eduGroupRowIds;
    public Collection<Long> getEduGroupRowIds() { return this.eduGroupRowIds; }
    public void setEduGroupRowIds(final Collection<Long> eduGroupRowIds) { this.eduGroupRowIds = eduGroupRowIds; }

    public static class Key implements Comparable<Key> {

        private final EppRealEduGroupSummary summary;
        private final Group group;
        private final EppRegistryElementPart registryElementPart;
        private final EppGroupType type;

        public Key(final EppRealEduGroupSummary summary, final Group group, final EppRegistryElementPart registryElementPart, final EppGroupType type) {
            this.summary = summary;
            this.group = group;
            this.registryElementPart = registryElementPart;
            this.type = type;
        }

        public Key(final EppRealEduGroupRow row) {
            this(
                row.getGroup().getSummary(),
                row.getStudentWpePart().getStudentWpe().getStudent().getGroup(),
                row.getStudentWpePart().getStudentWpe().getRegistryElementPart(),
                row.getStudentWpePart().getType()
            );
        }

        public EppRealEduGroupSummary getSummary() { return this.summary; }
        public Group getGroup() { return this.group; }
        public EppRegistryElementPart getRegistryElementPart() { return this.registryElementPart;}
        public EppGroupType getType() { return this.type; }

        @Override public int hashCode() {
            int hashCode = 0;
            hashCode = (hashCode<<1) ^ EntityBase.hashCode(this.getSummary());
            hashCode = (hashCode<<1) ^ EntityBase.hashCode(this.getGroup());
            hashCode = (hashCode<<1) ^ EntityBase.hashCode(this.getRegistryElementPart());
            hashCode = (hashCode<<1) ^ EntityBase.hashCode(this.getType());
            return hashCode;
        }

        @Override public boolean equals(final Object obj) {
            if (this == obj) { return true; }
            if (!(obj instanceof Key)) { return false; }
            final Key o = (Key)obj;
            if (!EntityBase.equals(this.getSummary(), o.getSummary())) { return false; }
            if (!EntityBase.equals(this.getGroup(), o.getGroup())) { return false; }
            if (!EntityBase.equals(this.getRegistryElementPart(), o.getRegistryElementPart())) { return false; }
            if (!EntityBase.equals(this.getType(), o.getType())) { return false; }
            return true;
        }

        @Override public int compareTo(final Key o) {
            int i;
            if (0 != (i = EppRealEduGroupSummary.COMPARATOR.compare(this.getSummary(), o.getSummary()))) { return i; }
            if (0 != (i = this.getGroup().getTitle().compareTo(o.getGroup().getTitle()))) { return i; }
            if (0 != (i = this.getGroup().getId().compareTo(o.getGroup().getId()))) { return i; }
            if (0 != (i = EppRegistryElementPart.FULL_COMPARATOR.compare(this.getRegistryElementPart(), o.getRegistryElementPart()))) { return i; }
            if (0 != (i = EppGroupType.COMPARATOR.compare(this.getType(), o.getType()))) { return i; }
            return 0;
        }

        public String getId() {
            return (
            "id_"+
            Long.toString(this.getSummary().getId(), Character.MAX_RADIX)+"_"+
            Long.toString(this.getGroup().getId(), Character.MAX_RADIX)+"_"+
            Long.toString(this.getRegistryElementPart().getId(), Character.MAX_RADIX)+"_"+
            Long.toString(this.getType().getId(), Character.MAX_RADIX)
            );
        }
    }

    // сортируем по ФИО студента
    public static final Comparator<EppRealEduGroupRow> GROUP_ROW_COMPARATOR = CommonCollator.comparing(r -> r.getStudentWpePart().getStudentWpe().getStudent().getFullFio(), true);

    public static class KeyData {

        // студент и выбранные значения по ним (отсортированный)
        private final Map<EppRealEduGroupRow, EppRealEduGroup> row2targetMap = new TreeMap<>(GROUP_ROW_COMPARATOR);
        public Map<EppRealEduGroupRow, EppRealEduGroup> getRow2targetMap() { return this.row2targetMap; }
        public Collection<EppRealEduGroupRow> getRows() { return this.getRow2targetMap().keySet(); }

        // список потенциально подходящих групп
        private final List<EppRealEduGroup> targetList = new ArrayList<>();
        public List<EppRealEduGroup> getTargetList() { return this.targetList; }

        // общее выбранное значение (для всего ключа)
        private EppRealEduGroup commonTarget;
        public EppRealEduGroup getCommonTarget() { return this.commonTarget; }
        public void setCommonTarget(final EppRealEduGroup commonTarget) { this.commonTarget = commonTarget; }

        // выставляет значение себе и всем детям
        public void setupCommonTarget(final EppRealEduGroup commonTarget) {
            setCommonTarget(commonTarget);
            for (Map.Entry<EppRealEduGroupRow, EppRealEduGroup> e: getRow2targetMap().entrySet()) {
                e.setValue(commonTarget);
            }
        }

        // true, если требуется уточнение по студентам
        private boolean explicit;
        public boolean isExplicit() { return this.explicit && (getTargetList().size() > 0); }
        public void setExplicit(final boolean explicit) { this.explicit = explicit; }

        // { target -> { row } } - строки, сгруппированные по группе, в которую их требуется переместить
        public Map<EppRealEduGroup, List<EppRealEduGroupRow>> buildTarget2RowListMap() {
            final Map<EppRealEduGroup, List<EppRealEduGroupRow>> result = new HashMap<>();
            for (final Map.Entry<EppRealEduGroupRow, EppRealEduGroup> e: this.getRow2targetMap().entrySet()) {
                final EppRealEduGroup value = this.isExplicit() ? e.getValue() : this.getCommonTarget();
                if (null != value) { SafeMap.safeGet(result, value, ArrayList.class).add(e.getKey()); }
            }
            return result;
        }
    }


    private Map<Key, KeyData> studentDataMap;
    public Map<Key, KeyData> getStudentDataMap() { return this.studentDataMap; }
    public void setStudentDataMap(final Map<Key, KeyData> studentDataMap) { this.studentDataMap = studentDataMap; }

    // список сводок
    public Collection<EppRealEduGroupSummary> getSummaries() {
        final Set<EppRealEduGroupSummary> result = new LinkedHashSet<>();
        for (Key key: this.getStudentDataMap().keySet()) { result.add(key.getSummary()); }
        return result;
    }

    private EppRealEduGroupSummary summary;
    public EppRealEduGroupSummary getSummary() { return this.summary; }
    public void setSummary(EppRealEduGroupSummary summary) { this.summary = summary; }


    public Collection<Map.Entry<Key, KeyData>> getEntries() {
        return CollectionUtils.select(this.getStudentDataMap().entrySet(), e -> e.getKey().getSummary().equals(getSummary()));
    }

    private Map.Entry<Key, KeyData> entry;
    public Map.Entry<Key, KeyData> getEntry() { return this.entry; }
    public void setEntry(Map.Entry<Key, KeyData> entry) { this.entry = entry; }

    public Key getKey() {
        if (null == getEntry()) { return null; }
        return getEntry().getKey();
    }
    public KeyData getKeyData() {
        if (null == getEntry()) { return null; }
        return getEntry().getValue();
    }

    private EppRealEduGroupRow row;
    public EppRealEduGroupRow getRow() { return this.row; }
    public void setRow(final EppRealEduGroupRow row) { this.row = row; }

    public EppRealEduGroup getRowTarget()
    {
        final EppRealEduGroupRow row = this.getRow();
        if (null == row) { return null; }

        final KeyData keyData = getKeyData();
        if (keyData.isExplicit()) { return keyData.getRow2targetMap().get(row); }
        return keyData.getCommonTarget();
    }

    public void setRowTarget(final EppRealEduGroup target)
    {
        final EppRealEduGroupRow row = this.getRow();
        if (null == row) { throw new IllegalStateException(); }

        final KeyData keyData = getKeyData();
        keyData.getRow2targetMap().put(row, target);
    }

    private Map.Entry<Key, KeyData> getStudentDataEntry(final String id) {
        return CollectionUtils.find(this.getStudentDataMap().entrySet(), o -> id.equals(o.getKey().getId()));
    }

    public void onClickUnsetExplicit() {
        KeyData keyData = this.getStudentDataEntry(this.getSupport().<String>getListenerParameter()).getValue();
        keyData.setExplicit(false);
        keyData.setupCommonTarget(keyData.getCommonTarget());
    }

    public void onClickSetExplicit() {
        KeyData keyData = this.getStudentDataEntry(this.getSupport().<String>getListenerParameter()).getValue();
        keyData.setExplicit(true);
        keyData.setupCommonTarget(keyData.getCommonTarget());
    }

    public void onChangeCommonTarget() {
        KeyData keyData = this.getStudentDataEntry(this.getSupport().<String>getListenerParameter()).getValue();
        keyData.setupCommonTarget(keyData.getCommonTarget());
    }


    @Override
    public void onComponentRefresh()
    {
        final Map<Key, KeyData> globalDataMap = new TreeMap<>();
        final Collection<IEppRealEduGroupRowDescription> relations = IEppRealGroupRowDAO.instance.get().getRelations();
        final Session session = this.getSupport().getSession();

        for (final IEppRealEduGroupRowDescription dsc: relations) {

            final Map<Key, KeyData> dataMap = new TreeMap<>();

            // грузим строки
            {
                final Collection<Long> ids = new ArrayList<>();
                for (final Long id: this.getEduGroupRowIds()) {
                    final IEntityMeta meta = EntityRuntime.getMeta(id);
                    if ((null != meta) && dsc.relationClass().isAssignableFrom(meta.getEntityClass())) {
                        ids.add(id);
                    }
                }

                BatchUtils.execute(ids, DQL.MAX_VALUES_ROW_NUMBER, idsPart -> {
                    final List<EppRealEduGroupRow> rows = new DQLSelectBuilder()
                    .fromEntity(dsc.relationClass(), "x")
                    .column(property("x"))
                    .fetchPath(DQLJoinType.inner, EppRealEduGroupRow.group().summary().fromAlias("x"), "summary")
                    .fetchPath(DQLJoinType.inner, EppRealEduGroupRow.studentWpePart().studentWpe().student().fromAlias("x"), "student")
                    .where(in(property("x.id"), idsPart))
                    .createStatement(session).list();

                    for (final EppRealEduGroupRow row: rows) {
                        final KeyData keyData = SafeMap.safeGet(dataMap, new Key(row), KeyData.class);
                        keyData.getRow2targetMap().put(row, null);
                    }
                });
            }

            // грузим «подходящие» группы
            for (final Map.Entry<Key, KeyData> e: dataMap.entrySet()) {

                final List<Object[]> lookupRows = new DQLSelectBuilder()

                .fromEntity(dsc.relationClass(), "y").where(and(
                    isNull(property(EppRealEduGroupRow.removalDate().fromAlias("y"))),
                    isNotNull(property(EppRealEduGroupRow.group().level().fromAlias("y"))),
                    eq(property(EppRealEduGroupRow.group().summary().fromAlias("y")), value(e.getKey().getSummary())),
                    eq(property(EppRealEduGroupRow.group().type().fromAlias("y")), value(e.getKey().getType())),
                    eq(property(EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart().fromAlias("y")), value(e.getKey().getRegistryElementPart())),
                    eq(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().group().fromAlias("y")), value(e.getKey().getGroup()))
                ))

                .column(property(EppRealEduGroupRow.group().id().fromAlias("y")))
                .group(property(EppRealEduGroupRow.group().id().fromAlias("y")))

                .column(count(property("y.id")))

                .createStatement(session)
                .list();

                // сортируем по релевантности
                Collections.sort(lookupRows, (o1, o2) -> {
                    final int n1 = (null == o1[1] ? 0 : ((Number)o1[1]).intValue());
                    final int n2 = (null == o2[1] ? 0 : ((Number)o2[1]).intValue());
                    return n2-n1;
                });

                // загружаем группы из запроса
                final KeyData value = e.getValue();
                for (final Object[] row: lookupRows) {
                    value.getTargetList().add((EppRealEduGroup)session.get(dsc.groupClass(), (Long)row[0]));
                }

                // выставляем значение по умолчанию (первое, оно же - наиболее подходящее)
                if (!value.getTargetList().isEmpty()) {
                    value.setupCommonTarget(value.getTargetList().iterator().next());
                }

                // добавляем в глобальный справочник (убеждаемся, что ничего не перетераем)
                if (null != globalDataMap.put(e.getKey(), value)) {
                    throw new IllegalStateException();
                }
            }
        }

        // сохраняем данные на форму
        this.setStudentDataMap(new LinkedHashMap<>(globalDataMap));

        //        // проверка на то, что форму вообще имеет смысл показывать
        //        boolean hasTargets = false;
        //        for (KeyData data: globalDataMap.values()) {
        //            hasTargets |= (data.getTargetList().size() > 0);
        //        }
        //        if (!hasTargets) {
        //            throw new ApplicationException("Для выбранных студентов не найденно ни одной подходящей УГС");
        //        }
    }


    public void onClickApply()
    {
        try {
            // выполняем операцию
            DataAccessServices.dao().doInTransaction(session -> {
                final IEppRealGroupRowDAO dao = IEppRealGroupRowDAO.instance.get();
                dao.lock(); // блокируем работу с УГС до окончания транзакции

                for (final KeyData data: EppEduGroupLookup4StudentsUI.this.getStudentDataMap().values()) {
                    final Map<EppRealEduGroup, List<EppRealEduGroupRow>> map = data.buildTarget2RowListMap();
                    for (final Entry<EppRealEduGroup, List<EppRealEduGroupRow>> e: map.entrySet())
                    {
                        final EppRealEduGroup target = e.getKey();
                        try {
                            session.refresh(target); // здесь может быть ошибка (объект может быть удален или изменен демоном)
                        } catch (UnresolvableObjectException t) {
                            throw new ApplicationException("Данные устарели. УГС «"+target.getFullTitle()+"» более не существует.", t);
                        }

                        // кто-то изменил УГС
                        if (null == target.getLevel()) {
                            throw new ApplicationException("Данные устарели. УГС «"+target.getFullTitle()+"» не обладает согласованием.");
                        }

                        final List<EppRealEduGroupRow> rows = e.getValue();
                        for (final EppRealEduGroupRow row: rows) {
                            try {
                                session.refresh(row); // здесь может быть ошибка (объект может быть удален или изменен демоном)
                            } catch (UnresolvableObjectException t) {
                                throw new ApplicationException("Данные устарели. Студент «"+row.getStudentWpePart().getStudentWpe().getStudent().getFio()+"» более не находится в УГС «"+target.getFullTitle()+"».");
                            }

                            // кто-то уже перенес студента
                            if (null != row.getRemovalDate()) {
                                throw new ApplicationException("Данные устарели. Студент «"+row.getStudentWpePart().getStudentWpe().getStudent().getFio()+"» более не находится в УГС «"+target.getFullTitle()+"».");
                            }

                            // студент уже в УГС с согласованием
                            if (null != row.getGroup().getLevel()) {
                                throw new ApplicationException("Данные устарели. Студент «"+row.getStudentWpePart().getStudentWpe().getStudent().getFio()+"» уже находится в согласованной УГС «"+target.getFullTitle()+"».");
                            }
                        }

                        dao.doMove(target.getId(), CommonDAO.ids(rows), target.getLevel());
                    }
                }
                return null;
            });

            // закрываем компонент
            deactivate();

        } catch (Throwable t) {
            this.getSupport().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

}
