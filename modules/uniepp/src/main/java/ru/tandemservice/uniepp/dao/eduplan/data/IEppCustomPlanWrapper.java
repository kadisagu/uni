/* $Id$ */
package ru.tandemservice.uniepp.dao.eduplan.data;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Враппер над индивидуальных УП
 *
 * @author Nikolay Fedorovskih
 * @since 21.08.2015
 */
public interface IEppCustomPlanWrapper
{
    /** @return враппер блока УПв */
    IEppEpvBlockWrapper getBlockWrapper();

    /** @return учебная сетка инд. УП */
    DevelopGrid getDevelopGrid();

    /** @return условие освоения инд. УП */
    DevelopCondition getDevelopCondition();

    /** @return Количество семестров в сетке ИУП */
    int getGridSize();

    /** @return { [new term number] -> [[custom term row wrapper]] }*/
    Map<Integer, Collection<IEppCustomPlanRowWrapper>> getRowMap();

    /** @return { [srcEpvRow.id, source term number] -> [custom row wrapper] } */
    Map<PairKey<Long, Integer>, IEppCustomPlanRowWrapper> getSourceMap();

    /** @return есть ли нагрузка (в т.ч. по ФК) по строке УПв в указанном семестре. С учетом перемещений и удалений. */
    default boolean hasInTermActivity(IEppEpvRowWrapper epvRowWrapper, int termNumber)
    {
        final Collection<IEppCustomPlanRowWrapper> termRows = this.getRowMap().get(termNumber);
        return CollectionUtils.isNotEmpty(termRows) && termRows.stream()
                .anyMatch(customTermRow -> !customTermRow.isExcluded() && epvRowWrapper.equals(customTermRow.getSourceRow()));
    }

    /** @return Суммарная нагрузка по семестру: сумма часов семестров строк, не подчиненных строкам с нагрузкой и без удаленных факультативных строк. */
    default double calcTotalLoadInTerm(int term, String loadFullCode)
    {
        return this.getRowMap().get(term).stream()
                .filter(row -> !row.isExcluded()  // удаленные факультативные дисциплины не считаем
                            && !row.getSourceRow().hasParentDistributedRow()) // вложенные в дисциплину по выбору не считаем
                .mapToDouble(row -> row.getTotalLoad(loadFullCode))
                .sum();
    }

    /** @return Суммарная нагрузка по семестру для строки УПв: сумма часов семестров строк, не подчиненных строкам с нагрузкой и без удаленных факультативных строк. */
    default double calcTotalLoadInTerm(Long epvRowId, int term, String loadFullCode)
    {
        return this.getRowMap().get(term).stream()
                .filter(row -> !row.isExcluded()  // удаленные факультативные дисциплины не считаем
                            && epvRowId.equals(row.getSourceRow().getId()) // Фильтруем по нужной строке УПв
                            && !row.getSourceRow().hasParentDistributedRow()) // вложенные в дисциплину по выбору не считаем
                .mapToDouble(row -> row.getTotalLoad(loadFullCode))
                .sum();
    }

    /** @return Количество форм итогового контроля в семестре: количество ФИК семестров строк, не подчиненных строкам с нагрузкой и без удаленных факультативных строк */
    default int calcFinalControlActionCountInTerm(int term, String finalActionTypeFullCode)
    {
        return (int) this.getRowMap().get(term).stream()
                .filter(row -> !row.isExcluded() // удаленные факультативные дисциплины не считаем
                            && !row.getSourceRow().hasParentDistributedRow() // вложенные в дисциплину по выбору не считаем
                            && row.hasControlAction(finalActionTypeFullCode))
                .count();
    }

    /** @return сумма всего часов семестров строк, не подчиненных строкам с нагрузкой и без удаленных факультативных строк,
     *  и для которых в перемещение указано либо перезачет, либо переаттестация. */
    default double calcTotalReattestationSizeInTerm(int term)
    {
        final Set<EppEpvGroupImRow> includedSelectGroups = new HashSet<>();
        return this.getRowMap().get(term).stream()
                .filter(row -> {
                    if (row.getNeedRetake() == null || row.isExcluded()) {
                        return false; // Если нет необходимости пересдачи или строка исключена из ИУП, её пропускаем.
                    }
                    final IEppEpvRowWrapper epvRow = row.getSourceRow();
                    final EppEpvGroupImRow selectedGroupRow = epvRow.getRow() instanceof EppEpvGroupImRow ? (EppEpvGroupImRow) epvRow.getRow() : epvRow.getParentSelectedGroupRow();
                    if (selectedGroupRow != null) {
                        // Это дисциплина по выбору или одна из её строк - нагрузка должна попасть в сумму только один раз
                        return includedSelectGroups.add(selectedGroupRow);
                    } else {
                        // В противном случае это должна быть любая другая строка, не подчиненная строке с нагрузкой
                        return !epvRow.hasParentDistributedRow();
                    }
                })
                .mapToDouble(row -> row.getTotalLoad(EppLoadType.FULL_CODE_TOTAL_HOURS))
                .sum();
    }
}