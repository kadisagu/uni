/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphAdd;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearEducationProcessGen;

import java.util.List;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final List<EppYearEducationProcess> yearList = this.getList(EppYearEducationProcess.class, EppYearEducationProcessGen.title().s());
        model.setYearEducationProcessListModel(new LazySimpleSelectModel<>(yearList));
        if (null == model.getWorkGraph().getYear())
        {
            final EppYearEducationProcess year = (EppYearEducationProcess)CollectionUtils.find(yearList, object -> ((EppYearEducationProcess)object).getId().equals(model.getSelectedYearId()));
            model.getWorkGraph().setYear(year);
        }
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(new LazySimpleSelectModel<>(DevelopCondition.class));
        model.setDevelopTechListModel(new LazySimpleSelectModel<>(DevelopTech.class));
        model.setDevelopGridListModel(new LazySimpleSelectModel<>(DevelopGrid.class));
        model.setDevelopCombinationListModel(new LazySimpleSelectModel<>(DevelopCombination.class));
    }

    @Override
    public void update(final Model model)
    {
        model.getWorkGraph().setState(this.getCatalogItem(EppState.class, EppState.STATE_FORMATIVE));
        this.getSession().save(model.getWorkGraph());
    }
}
