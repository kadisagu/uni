package ru.tandemservice.uniepp.component.registry.base.Pub;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author vdanilov
 */
public interface RegistryElementPubContext
{
    String REGISTRY_ELEMENT_PUB_CONTEXT_PARAM = "eppRegistryElementPubContext";

    EppRegistryElement getElement();

    CommonPostfixPermissionModel getSec();
}
