package ru.tandemservice.uniepp.entity.plan.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Итоговая нагрузка строки УП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvRowLoadGen extends EntityBase
 implements INaturalIdentifiable<EppEpvRowLoadGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad";
    public static final String ENTITY_NAME = "eppEpvRowLoad";
    public static final int VERSION_HASH = 196272517;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW = "row";
    public static final String L_LOAD_TYPE = "loadType";
    public static final String P_HOURS = "hours";
    public static final String P_HOURS_I = "hoursI";
    public static final String P_HOURS_E = "hoursE";
    public static final String P_HOURS_AS_DOUBLE = "hoursAsDouble";

    private EppEpvTermDistributedRow _row;     // Строка УП (с нагрузкой)
    private EppALoadType _loadType;     // Вид аудиторной нагрузки (типы аудиторных занятий)
    private long _hours;     // Число часов
    private long _hoursI;     // Из них в интерактивной форме
    private long _hoursE;     // Из них в электронной форме

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка УП (с нагрузкой). Свойство не может быть null.
     */
    @NotNull
    public EppEpvTermDistributedRow getRow()
    {
        return _row;
    }

    /**
     * @param row Строка УП (с нагрузкой). Свойство не может быть null.
     */
    public void setRow(EppEpvTermDistributedRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Вид аудиторной нагрузки (типы аудиторных занятий). Свойство не может быть null.
     */
    @NotNull
    public EppALoadType getLoadType()
    {
        return _loadType;
    }

    /**
     * @param loadType Вид аудиторной нагрузки (типы аудиторных занятий). Свойство не может быть null.
     */
    public void setLoadType(EppALoadType loadType)
    {
        dirty(_loadType, loadType);
        _loadType = loadType;
    }

    /**
     * @return Число часов. Свойство не может быть null.
     */
    @NotNull
    public long getHours()
    {
        return _hours;
    }

    /**
     * @param hours Число часов. Свойство не может быть null.
     */
    public void setHours(long hours)
    {
        dirty(_hours, hours);
        _hours = hours;
    }

    /**
     * @return Из них в интерактивной форме. Свойство не может быть null.
     */
    @NotNull
    public long getHoursI()
    {
        return _hoursI;
    }

    /**
     * @param hoursI Из них в интерактивной форме. Свойство не может быть null.
     */
    public void setHoursI(long hoursI)
    {
        dirty(_hoursI, hoursI);
        _hoursI = hoursI;
    }

    /**
     * @return Из них в электронной форме. Свойство не может быть null.
     */
    @NotNull
    public long getHoursE()
    {
        return _hoursE;
    }

    /**
     * @param hoursE Из них в электронной форме. Свойство не может быть null.
     */
    public void setHoursE(long hoursE)
    {
        dirty(_hoursE, hoursE);
        _hoursE = hoursE;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEpvRowLoadGen)
        {
            if (withNaturalIdProperties)
            {
                setRow(((EppEpvRowLoad)another).getRow());
                setLoadType(((EppEpvRowLoad)another).getLoadType());
            }
            setHours(((EppEpvRowLoad)another).getHours());
            setHoursI(((EppEpvRowLoad)another).getHoursI());
            setHoursE(((EppEpvRowLoad)another).getHoursE());
        }
    }

    public INaturalId<EppEpvRowLoadGen> getNaturalId()
    {
        return new NaturalId(getRow(), getLoadType());
    }

    public static class NaturalId extends NaturalIdBase<EppEpvRowLoadGen>
    {
        private static final String PROXY_NAME = "EppEpvRowLoadNaturalProxy";

        private Long _row;
        private Long _loadType;

        public NaturalId()
        {}

        public NaturalId(EppEpvTermDistributedRow row, EppALoadType loadType)
        {
            _row = ((IEntity) row).getId();
            _loadType = ((IEntity) loadType).getId();
        }

        public Long getRow()
        {
            return _row;
        }

        public void setRow(Long row)
        {
            _row = row;
        }

        public Long getLoadType()
        {
            return _loadType;
        }

        public void setLoadType(Long loadType)
        {
            _loadType = loadType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppEpvRowLoadGen.NaturalId) ) return false;

            EppEpvRowLoadGen.NaturalId that = (NaturalId) o;

            if( !equals(getRow(), that.getRow()) ) return false;
            if( !equals(getLoadType(), that.getLoadType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRow());
            result = hashCode(result, getLoadType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRow());
            sb.append("/");
            sb.append(getLoadType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvRowLoadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvRowLoad.class;
        }

        public T newInstance()
        {
            return (T) new EppEpvRowLoad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "row":
                    return obj.getRow();
                case "loadType":
                    return obj.getLoadType();
                case "hours":
                    return obj.getHours();
                case "hoursI":
                    return obj.getHoursI();
                case "hoursE":
                    return obj.getHoursE();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "row":
                    obj.setRow((EppEpvTermDistributedRow) value);
                    return;
                case "loadType":
                    obj.setLoadType((EppALoadType) value);
                    return;
                case "hours":
                    obj.setHours((Long) value);
                    return;
                case "hoursI":
                    obj.setHoursI((Long) value);
                    return;
                case "hoursE":
                    obj.setHoursE((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "row":
                        return true;
                case "loadType":
                        return true;
                case "hours":
                        return true;
                case "hoursI":
                        return true;
                case "hoursE":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "row":
                    return true;
                case "loadType":
                    return true;
                case "hours":
                    return true;
                case "hoursI":
                    return true;
                case "hoursE":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "row":
                    return EppEpvTermDistributedRow.class;
                case "loadType":
                    return EppALoadType.class;
                case "hours":
                    return Long.class;
                case "hoursI":
                    return Long.class;
                case "hoursE":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvRowLoad> _dslPath = new Path<EppEpvRowLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvRowLoad");
    }
            

    /**
     * @return Строка УП (с нагрузкой). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad#getRow()
     */
    public static EppEpvTermDistributedRow.Path<EppEpvTermDistributedRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Вид аудиторной нагрузки (типы аудиторных занятий). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad#getLoadType()
     */
    public static EppALoadType.Path<EppALoadType> loadType()
    {
        return _dslPath.loadType();
    }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad#getHours()
     */
    public static PropertyPath<Long> hours()
    {
        return _dslPath.hours();
    }

    /**
     * @return Из них в интерактивной форме. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad#getHoursI()
     */
    public static PropertyPath<Long> hoursI()
    {
        return _dslPath.hoursI();
    }

    /**
     * @return Из них в электронной форме. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad#getHoursE()
     */
    public static PropertyPath<Long> hoursE()
    {
        return _dslPath.hoursE();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad#getHoursAsDouble()
     */
    public static SupportedPropertyPath<Double> hoursAsDouble()
    {
        return _dslPath.hoursAsDouble();
    }

    public static class Path<E extends EppEpvRowLoad> extends EntityPath<E>
    {
        private EppEpvTermDistributedRow.Path<EppEpvTermDistributedRow> _row;
        private EppALoadType.Path<EppALoadType> _loadType;
        private PropertyPath<Long> _hours;
        private PropertyPath<Long> _hoursI;
        private PropertyPath<Long> _hoursE;
        private SupportedPropertyPath<Double> _hoursAsDouble;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка УП (с нагрузкой). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad#getRow()
     */
        public EppEpvTermDistributedRow.Path<EppEpvTermDistributedRow> row()
        {
            if(_row == null )
                _row = new EppEpvTermDistributedRow.Path<EppEpvTermDistributedRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Вид аудиторной нагрузки (типы аудиторных занятий). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad#getLoadType()
     */
        public EppALoadType.Path<EppALoadType> loadType()
        {
            if(_loadType == null )
                _loadType = new EppALoadType.Path<EppALoadType>(L_LOAD_TYPE, this);
            return _loadType;
        }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad#getHours()
     */
        public PropertyPath<Long> hours()
        {
            if(_hours == null )
                _hours = new PropertyPath<Long>(EppEpvRowLoadGen.P_HOURS, this);
            return _hours;
        }

    /**
     * @return Из них в интерактивной форме. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad#getHoursI()
     */
        public PropertyPath<Long> hoursI()
        {
            if(_hoursI == null )
                _hoursI = new PropertyPath<Long>(EppEpvRowLoadGen.P_HOURS_I, this);
            return _hoursI;
        }

    /**
     * @return Из них в электронной форме. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad#getHoursE()
     */
        public PropertyPath<Long> hoursE()
        {
            if(_hoursE == null )
                _hoursE = new PropertyPath<Long>(EppEpvRowLoadGen.P_HOURS_E, this);
            return _hoursE;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad#getHoursAsDouble()
     */
        public SupportedPropertyPath<Double> hoursAsDouble()
        {
            if(_hoursAsDouble == null )
                _hoursAsDouble = new SupportedPropertyPath<Double>(EppEpvRowLoadGen.P_HOURS_AS_DOUBLE, this);
            return _hoursAsDouble;
        }

        public Class getEntityClass()
        {
            return EppEpvRowLoad.class;
        }

        public String getEntityName()
        {
            return "eppEpvRowLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getHoursAsDouble();
}
