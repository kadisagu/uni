package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x9x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppScriptItem

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_script_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eppscriptitem"), 
				new DBColumn("printpdf_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("printabletopdf_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppScriptItem");

		}


    }
}