/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.VersionTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.logic.CustomEduPlanDSHandler;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;

/**
 * @author Nikolay Fedorovskih
 * @since 11.09.2015
 */
@Configuration
public class EppCustomEduPlanVersionTab extends BusinessComponentManager
{
    public static final String CUSTOM_EDU_PLAN_DS = "customEduPlanDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(CUSTOM_EDU_PLAN_DS, customEduPlanDSColumnExtPoint(), customEduPlanDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint customEduPlanDSColumnExtPoint()
    {
        return columnListExtPointBuilder(CUSTOM_EDU_PLAN_DS)
                .addColumn(UniEppUtils.stateColumn(EppCustomEduPlan.state().code()))
                .addColumn(publisherColumn("title", EppCustomEduPlan.title()).order())
                .addColumn(publisherColumn("block", EppCustomEduPlan.epvBlock().title()).primaryKeyPath(EppCustomEduPlan.epvBlock().id()))
                .addColumn(textColumn("developGrid", EppCustomEduPlan.developGrid().title()).order())
                .addColumn(publisherColumn("students", "title")
                                   .entityListProperty(CustomEduPlanDSHandler.VIEW_PROP_STUDENT)
                                   .parameters("mvel:['selectedStudentTab':'studentTab', 'selectedDataTab':'studentEppEduplanTab']")
                                   .formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler customEduPlanDSHandler()
    {
        return new CustomEduPlanDSHandler(getName());
    }
}