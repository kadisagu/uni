/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.AdditionalProfAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditional;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.EppEduPlanManager;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 9/4/14
 */
@Configuration
public class EppEduPlanAdditionalProfAddEdit extends BusinessComponentManager
{
    public static final String BIND_PROGRAM_KIND_ID = "programKindId";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("programDS", programDSHandler()).addColumn(EduProgramAdditional.title().s()))
            .addDataSource(EducationCatalogsManager.instance().programFormDSConfig())
            .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
            .addDataSource(EducationCatalogsManager.instance().programTraitDSConfig())
            .addDataSource(EducationCatalogsManager.instance().developGridDSConfig())
            .addDataSource(EducationCatalogsManager.instance().developCombinationDSConfig())
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler programDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramAdditional.class)
            .where(EduProgramAdditional.kind().id(), BIND_PROGRAM_KIND_ID)
            .filter(EduProgramAdditional.title())
            .order(EduProgramAdditional.title())
            ;
    }
}