/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphEduPlansTab;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRow2EduPlanGen;

import java.util.*;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setWorkGraph(this.getNotNull(EppWorkGraph.class, model.getWorkGraph().getId()));
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        final MQBuilder builder = new MQBuilder(EppWorkGraphRow2EduPlanGen.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", EppWorkGraphRow2EduPlanGen.row().graph().s(), model.getWorkGraph()));
        final Map<EppEduPlanVersion, Set<Integer>> map = new HashMap<>();
        for (final EppWorkGraphRow2EduPlan rel : builder.<EppWorkGraphRow2EduPlan>getResultList(this.getSession()))
        {
            Set<Integer> set = map.get(rel.getEduPlanVersion());
            if (set == null) {
                map.put(rel.getEduPlanVersion(), set = new HashSet<>());
            }
            set.add(rel.getRow().getCourse().getIntValue());
        }

        final List<WorkGraphEduPlansRow> list = new ArrayList<>();
        final long id = 0;
        for (final Map.Entry<EppEduPlanVersion, Set<Integer>> entry : map.entrySet()) {
            list.add(new WorkGraphEduPlansRow(id, entry.getKey(), StringUtils.join(entry.getValue(), ", ")));
        }
        Collections.sort(list, new EntityComparator<>(new EntityOrder("eduPlanVersion.eduPlan.number"), new EntityOrder("eduPlanVersion.number")));

        UniBaseUtils.createPage(model.getDataSource(), list);
    }
}
