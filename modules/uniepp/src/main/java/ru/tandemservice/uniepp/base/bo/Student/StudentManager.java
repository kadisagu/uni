package ru.tandemservice.uniepp.base.bo.Student;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniepp.base.bo.Student.ui.LifecycleTab.logic.IStudentWpeDAO;
import ru.tandemservice.uniepp.base.bo.Student.ui.LifecycleTab.logic.StudentWpeDAO;

@Configuration
public class StudentManager extends BusinessObjectManager
{
	public static StudentManager instance()
	{
		return instance(StudentManager.class);
	}

	@Bean
	public IStudentWpeDAO dao()
	{
		return new StudentWpeDAO();
	}
}