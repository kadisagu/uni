package ru.tandemservice.uniepp.entity.catalog;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.catalog.bo.EppProfActivityType.ui.AddEdit.EppProfActivityTypeAddEdit;
import ru.tandemservice.uniepp.entity.catalog.gen.EppProfActivityTypeGen;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/** @see ru.tandemservice.uniepp.entity.catalog.gen.EppProfActivityTypeGen */
public class EppProfActivityType extends EppProfActivityTypeGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    // параметры для дефолтного хэндлера
    public static final String PROP_PROGRAM_KIND = "programKind";
    public static final String PROP_PROGRAM_SUBJECT_INDEX = "programSubjectIndex";
    public static final String PROP_PROGRAM_SUBJECT = "programSubject";
    public static final String PROP_IN_DEPTH_STUDY = "inDepthStudy";

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EppProfActivityType.class)
                .titleProperty(EppProfActivityType.title().s())
                .where(EppProfActivityType.programSubject().subjectIndex().programKind(), PROP_PROGRAM_KIND)
                .where(EppProfActivityType.programSubject().subjectIndex(), PROP_PROGRAM_SUBJECT_INDEX)
                .where(EppProfActivityType.programSubject(), PROP_PROGRAM_SUBJECT)
                .where(EppProfActivityType.inDepthStudy(), PROP_IN_DEPTH_STUDY)
                .customize((alias, dql, context, filter) -> dql
                        // с проставленной датой утраты актуальности в конец
                        .order(caseExpr(isNull(property(alias, EppProfActivityType.removalDate())), value(0), value(1)))
                        .order(EppProfActivityType.priority().fromAlias(alias).s()))
                .filter(EppProfActivityType.title());
    }

    public static IDynamicCatalogDesc getUiDesc()
    {
        return new BaseDynamicCatalogDesc()
        {
            @Override
            public String getAddEditComponentName()
            {
                return EppProfActivityTypeAddEdit.class.getSimpleName();
            }

            @Override
            public String getAdditionalPropertiesAddEditPageName()
            {
                return EppProfActivityTypeAddEdit.class.getPackage().getName() + ".AdditionalProps";
            }

            @Override
            public Collection<String> getHiddenFields()
            {
                // эти поля будут добавлены в интерфейс справочника руками (т.к. нужна их кастомизация)
                return ImmutableSet.of(L_PROGRAM_SUBJECT, P_IN_DEPTH_STUDY, P_FROM_I_M_C_A);
            }

            @Override
            public Collection<String> getHiddenFieldsAddEditForm()
            {
                return ImmutableSet.of();
            }

            @Override
            public Collection<String> getHiddenFieldsItemPub()
            {
                return ImmutableSet.of();
            }

            @Override
            public Set<Long> getEditDisabledItems(Set<Long> itemIds)
            {
                return new HashSet<>(IUniBaseDao.instance.get().<Long>getList(
                        new DQLSelectBuilder().fromEntity(EppProfActivityType.class, "e")
                                .column(property("e.id"))
                                .where(in(property("e", EppProfActivityType.id()), itemIds))
                                .where(eq(property("e", EppProfActivityType.fromIMCA()), value(Boolean.TRUE)))
                ));
            }

            @Override
            public Set<Long> getDeleteDisabledItems(Set<Long> itemIds)
            {
                return new HashSet<>(IUniBaseDao.instance.get().<Long>getList(
                        new DQLSelectBuilder().fromEntity(EppProfActivityType.class, "e")
                                .column(property("e.id"))
                                .where(in(property("e", EppProfActivityType.id()), itemIds))
                                .where(eq(property("e", EppProfActivityType.fromIMCA()), value(Boolean.TRUE)))
                ));
            }

            @Override
            public MetaDSLPath[] getPriorityGroupFields()
            {
                return new MetaDSLPath[]{EppProfActivityType.programSubject().id()};
            }
        };
    }
}
