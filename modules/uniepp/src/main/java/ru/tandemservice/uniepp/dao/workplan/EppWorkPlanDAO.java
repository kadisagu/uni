package ru.tandemservice.uniepp.dao.workplan;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.*;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWorkPlanRowKindCodes;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRow2EduPlanGen;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRowWeekGen;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.*;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanGen;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanPartGen;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanRowGen;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanVersionGen;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class EppWorkPlanDAO extends UniBaseDao implements IEppWorkPlanDAO
{
    private static final String CACHE_KEY_WORK_PLAN_DAO_WORK_PLAN_EMPTY_MAP = EppWorkPlanDAO.class.getSimpleName()+".work-plan-empty-map";


    public static void joinWorkPlan(final DQLSelectBuilder dql, final Boolean throughVersion, final String wpAlias, final String rootAlias, final String workPlanIdPath) {
        final String wpvAlias = wpAlias+"_version";
        if (null == throughVersion)
        {
            dql.joinEntity(rootAlias, DQLJoinType.left, EppWorkPlanVersion.class, wpvAlias,
                eq(
                    property(wpvAlias, "id"),
                    property(rootAlias, workPlanIdPath)
                )
            );

            dql.joinEntity(wpvAlias,  DQLJoinType.left, EppWorkPlan.class, wpAlias, or(
                eq(
                    property(wpAlias, "id"),
                    property(rootAlias, workPlanIdPath)
                ),
                eq(
                    property(wpAlias, "id"),
                    property(EppWorkPlanVersionGen.parent().id().fromAlias(wpvAlias))
                )
            ));
        }
        else
        {
            if (throughVersion)
            {
                dql.joinEntity(rootAlias, DQLJoinType.left, EppWorkPlanVersion.class, wpvAlias,
                    eq(
                        property(wpvAlias, "id"),
                        property(rootAlias, workPlanIdPath)
                    )
                );
                dql.joinEntity(wpvAlias,  DQLJoinType.left, EppWorkPlan.class, wpAlias,
                    eq(
                        property(wpAlias, "id"),
                        property(EppWorkPlanVersionGen.parent().id().fromAlias(wpvAlias))
                    )
                );
            }
            else
            {
                dql.joinEntity(rootAlias,  DQLJoinType.left, EppWorkPlan.class, wpAlias,
                    eq(
                        property(wpAlias, "id"),
                        property(rootAlias, workPlanIdPath)
                    )
                );
            }
        }
    }




    protected static <T extends EppWorkPlanBase> T check_editable(final T workPlan) throws IllegalStateException {
        final EppState state = (null == workPlan ? null : workPlan.getState());
        if (null != state) { state.check_editable(workPlan); }
        return workPlan;
    }


    public static boolean canCreateFakeRowFromGroupRow(final int currentTerm, final IEppEpvRowWrapper wrapper)
    {
/*
        final IEppEpvRow row = wrapper.getRow();

        if (row instanceof EppEpvGroupReRow)
        {
            // для группы - если пустая
            return wrapper.getChilds().isEmpty();
        }
        else if (row instanceof EppEpvGroupImRow)
        {
            // # 5008: (не пытайтесь понять этот бред)
            // если в ДпоВ нет дисциплин реестра вообще, либо есть, но у них нет нагрузки и КМ на нужный семестр,
            // то нужно формировать фейк, если для самой ДпоВ указана нагрузка или КМ в семестре

            if (wrapper.hasInTermActivity(currentTerm)) {
                if (wrapper.getChilds().isEmpty()) {
                    // если дочерних строк нет - то всегда делаем фэйк
                    return true;
                }

                for (final IEppEpvRowWrapper childWrapper : wrapper.getChilds()) {
                    if (childWrapper.hasInTermActivity(currentTerm)) {
                        // если у дисциплины есть нагрузка - она будет добавлена как дисциплина (поэтому фэйк не добаяляем)
                        return false;
                    }
                }

                // если у всех нет активности (см #5008) - то добавляем фэйк для группы
                return true;
            }
        }
*/
        return false;
    }

    /**
     * Список строк УПв доступных для рабочего плана (по семестру)
     * В цикле не вызывать! Для индивидуальных планов создается очень дорогой враппер.
     */
    public static Predicate<IEppEpvRowWrapper> getPossibleEpvRowWrapperPredicate(final EppWorkPlanBase workplan)
    {
        final int currentTerm = workplan.getTerm().getIntValue();
        final IEppCustomPlanWrapper customPlanWrapper = workplan.getWorkPlan().getCustomEduPlan() != null
                ? new EppCustomPlanWrapper(workplan.getWorkPlan().getCustomEduPlan())
                : null;


        return customPlanWrapper == null
                ? wrapper -> wrapper.hasInTermActivity(currentTerm) || EppWorkPlanDAO.canCreateFakeRowFromGroupRow(currentTerm, wrapper)
                : wrapper -> customPlanWrapper.hasInTermActivity(wrapper, currentTerm);
    }


    @Override
    public Collection<IEntity> getSecLocalEntities(final EppWorkPlanBase eppWorkPlanBase) {
        return new ArrayList<>(eppWorkPlanBase.getEduPlanVersion().getSecLocalEntities());
    }


    @Override
    @SuppressWarnings("unchecked")
    public INumberGenerationRule<EppWorkPlan> getWorkPlanNumberGenerationRule()
    {
        return new SimpleNumberGenerationRule<EppWorkPlan>()
        {
            @Override
            public Set<String> getUsedNumbers(final EppWorkPlan object)
            {
                final List list = EppWorkPlanDAO.this.getSession()
                .createCriteria(ClassUtils.getUserClass(object))
                .add(Restrictions.eq(EppWorkPlanGen.year().toString(), object.getYear()))
                .setProjection(Projections.property(EppWorkPlanGen.number().toString())).list();
                return new HashSet<>(list);
            }

            @Override
            public String getNumberQueueName(final EppWorkPlan object)
            {
                return ClassUtils.getUserClass(object).getSimpleName() + "." + (object).getYear().getId();
            }
        };
    }

    @Override
    @SuppressWarnings("unchecked")
    public INumberGenerationRule<EppWorkPlanVersion> getWorkPlanVersionNumberGenerationRule()
    {
        return new SimpleNumberGenerationRule<EppWorkPlanVersion>()
        {
            @Override
            public Set<String> getUsedNumbers(final EppWorkPlanVersion object)
            {
                final List list = EppWorkPlanDAO.this.getSession().createCriteria(ClassUtils.getUserClass(object)).add(Restrictions.eq(EppWorkPlanVersionGen.parent().toString(), object.getParent())).setProjection(Projections.property(EppWorkPlanVersionGen.number().toString())).list();
                return new HashSet<>(list);
            }

            @Override
            public String getNumberQueueName(final EppWorkPlanVersion object)
            {
                return ClassUtils.getUserClass(object).getSimpleName() + "." + (object).getParent().getId();
            }
        };
    }

    @Override
    public void doUpdateStudentEpvWorkPlan(final Map<Long, Set<EppWorkPlanBase>> studentEpv2workPlanSet)
    {
        final Session session = this.getSession();

        // глобальная блокировка
        NamedSyncInTransactionCheckLocker.register(session, "EppWorkPlanDAO.doUpdateStudentEpvWorkPlan");

        final Date removalDate = new Date();

        final Map<Long, Set<EppStudent2WorkPlan>> currentWorkPlanMap = this.getStudentEpvWorkPlanMap(studentEpv2workPlanSet.keySet(), false);
        for (final Entry<Long, Set<EppWorkPlanBase>> entry : studentEpv2workPlanSet.entrySet())
        {
            final Long sepvId = entry.getKey();
            final Set<EppWorkPlanBase> workPlanSet = entry.getValue();

            {
                final Set<Object> states = new HashSet<>();
                for (final EppWorkPlanBase wp: workPlanSet) {
                    if (null != wp) {

                        // проверяем уникальность семестров
                        if (!states.add(wp.getGridTerm().getTerm().getId())) {
                            throw new IllegalStateException("duplicate term number ("+wp.getGridTerm().getTermNumber()+")");
                        }

                        // проверяем уникальность года и части года
                        if (!states.add(new MultiKey(wp.getYear().getEducationYear().getId(), wp.getGridTerm().getPart().getId()))) {
                            throw new IllegalStateException("duplicate year+part combination("+wp.getYear().getEducationYear().getIntValue()+", "+wp.getGridTerm().getPart().getTitleWithParent()+")");
                        }
                    }
                }
            }


            final EppStudent2EduPlanVersion s2epv = (EppStudent2EduPlanVersion)session.load(EppStudent2EduPlanVersion.class, sepvId);
            final Set<EppStudent2WorkPlan> currentWorkPlanSet = currentWorkPlanMap.get(sepvId);

            // ищем в старых связях новые РУП
            for (final EppStudent2WorkPlan rel : currentWorkPlanSet) {
                if (workPlanSet.remove(rel.getWorkPlan())) {
                    // если такая связь есть делаем ее активной (и удаляем из мапа новых РУП этот РУП)
                    rel.setRemovalDate(null);
                } else {
                    // для остальных ставим дату удаления
                    rel.setRemovalDate(removalDate);
                }
            }

            // для оставшихся создаем новые связи и ложим в currentWorkPlanSet
            for (final EppWorkPlanBase plan : workPlanSet) {
                if (null != plan) {
                    currentWorkPlanSet.add(new EppStudent2WorkPlan(s2epv, plan));
                }
            }

            currentWorkPlanSet.forEach(session::saveOrUpdate);
        }

        // важно все это тутже положить в базу
        session.flush();
    }

    @Override
    public Map<Long, Set<EppStudent2WorkPlan>> getStudentEpvWorkPlanMap(final Collection<Long> studentEpvIds, final boolean filterRelations)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EppStudent2WorkPlan.class, "s2wp")
        .column(property("s2wp"))
        .where(in(property(EppStudent2WorkPlan.studentEduPlanVersion().id().fromAlias("s2wp")), studentEpvIds));

        if (filterRelations) {
            dql.where(isNull(property(EppStudent2WorkPlan.removalDate().fromAlias("s2wp"))));
        }

        final List<EppStudent2WorkPlan> student2WorkPlan = dql.createStatement(this.getSession()).list();
        final Map<Long, Set<EppStudent2WorkPlan>> currentWorkPlanMap = SafeMap.get(HashSet.class);
        for (final EppStudent2WorkPlan wp : student2WorkPlan) {
            currentWorkPlanMap.get(wp.getStudentEduPlanVersion().getId()).add(wp);
        }
        return currentWorkPlanMap;
    }


    @Override
    @SuppressWarnings("unchecked")
    public Map<Integer, EppWorkPlanPart> getWorkPlanPartMap(final Long workPlanId) {
        final IEntityMeta meta = EntityRuntime.getMeta(workPlanId);
        if (null == meta) { return Collections.emptyMap(); }

        final Class klass = meta.getEntityClass();
        if (EppWorkPlan.class.isAssignableFrom(klass))
        {
            // если это РП - то берем его строки
            final List<EppWorkPlanPart> partList = this.getList(EppWorkPlanPart.class, EppWorkPlanPartGen.workPlan().id().s(), workPlanId, EppWorkPlanPart.P_NUMBER);
            return this.getWorkPlanPartMap(partList);
        }
        else if (EppWorkPlanVersion.class.isAssignableFrom(klass))
        {
            // если это версия - берем ее РП, и у него строки
            final EppWorkPlan workplan = this.getNotNull(EppWorkPlanBase.class, workPlanId).getWorkPlan();
            final List<EppWorkPlanPart> partList = this.getList(EppWorkPlanPart.class, EppWorkPlanPartGen.workPlan().id().s(), workplan.getId(), EppWorkPlanPart.P_NUMBER);
            return this.getWorkPlanPartMap(partList);
        }

        // иначе громко ругаемся
        try { throw new IllegalStateException("Unsupported entity ["+workPlanId+"] class:"+klass.getName()); }
        catch (final IllegalStateException e) { this.logger.error(e); }
        return Collections.emptyMap();

    }


    @Override
    public void doUpdateWorkPlanPartSet(final Long workPlanId, final Collection<EppWorkPlanPart> planPartSet)
    {
        final Session session = this.getSession();
        final EppWorkPlan workPlan = EppWorkPlanDAO.check_editable((EppWorkPlan)session.load(EppWorkPlan.class, workPlanId));

        // блокируем РУП
        NamedSyncInTransactionCheckLocker.register(session, String.valueOf(workPlan.getId()));
        final Map<Integer, EppWorkPlanPart> partMap = this.getWorkPlanPartMap(workPlanId);

        // обновляем в базе существующие части
        for (final EppWorkPlanPart param : planPartSet) {
            if (!param.isEmpty()) {
                // если есть значение - то считаем, что есть такая часть
                // если же в значении чушь, то считаем что части нет
                EppWorkPlanPart dbPart = partMap.remove(param.getNumber());
                if (null == dbPart) {
                    dbPart = new EppWorkPlanPart();
                    dbPart.setNumber(param.getNumber());
                    dbPart.setWorkPlan(workPlan);
                }
                dbPart.updateSize(param);
                session.saveOrUpdate(dbPart);
            }
        }

        // удаляем устаревшие
        partMap.values().forEach(session::delete);

        session.flush();
    }





    public interface IPartWeekMapSourceResolver {
        Set<EppWorkPlanPart> resolveWorkPlanPartSet(final EppWorkPlan eppWorkPlan, Map<Integer, String> graphWeekCodeMap);
    }

    /** @return { part.number -> part.total-weeks } */
    protected Map<Integer, IEppWorkPlanPart> fillWorkPlanParts(final EppWorkPlanBase eppWorkPlan, final IPartWeekMapSourceResolver partSetResolver)
    {
        Collection<EppWorkPlanPart> parts = this.getList(EppWorkPlanPart.class, EppWorkPlanPartGen.workPlan().id().s(), eppWorkPlan.getWorkPlan().getId(), EppWorkPlanPart.P_NUMBER);
        if ((null != partSetResolver) && parts.isEmpty() && eppWorkPlan.getWorkPlan().equals(eppWorkPlan))
        {
            /* если распределения нет и это сам РУП (а не его версия, скажем), то пробуем создать распределение на базе ГУПа */

            List<EppWorkGraphRowWeek> graphWeeks;
            try {
                graphWeeks = this.getWorkGraphWeeks4Plan(eppWorkPlan.getId());
            } catch (final IllegalStateException t) {
                // если есть ошибки в ГУП - то мы просто ничего не делаем, считаеп что ничего не нашли
                graphWeeks = Collections.emptyList();
            }

            final Map<Integer, String> graphWeekCodeMap = new HashMap<>(graphWeeks.size());
            int index = 0;
            for (final EppWorkGraphRowWeek graphWeek: graphWeeks) {
                graphWeekCodeMap.put(index++, graphWeek.getType().getCode());
            }

            parts = partSetResolver.resolveWorkPlanPartSet(eppWorkPlan.getWorkPlan(), graphWeekCodeMap);
            if ((null != parts) && (parts.size() > 0))
            {
                EduProgramForm programForm = eppWorkPlan.getEduPlan().getProgramForm();
                DevelopForm developForm = get(DevelopForm.class, DevelopForm.programForm(), programForm);
                if (null == developForm) throw new IllegalStateException("Develop form is not found for programForm with code ='" + programForm.getCode() + "'.");
                final Map<String, Set<String>> eLoadWeekTypeMap = IEppSettingsDAO.instance.get().getELoadWeekTypeMap().get(developForm.getCode());
                EppWorkPlanDAO.recalculateWorkPlanPartWeeks(eLoadWeekTypeMap, graphWeekCodeMap, parts);
                this.doUpdateWorkPlanPartSet(eppWorkPlan.getWorkPlan().getId(), parts);
            }
        }

        return this.getWorkPlanPartMap(parts);
    }

    /* важно, что список упорядочен */
    @SuppressWarnings("unchecked")
    protected <T extends IEppWorkPlanPart> Map<Integer, T> getWorkPlanPartMap(final Collection<EppWorkPlanPart> parts) {
        if ((null == parts) || parts.isEmpty()) {
            return Collections.emptyMap();
        }

        final Map<Integer, EppWorkPlanPart> result = new LinkedHashMap<>(parts.size());
        for (final EppWorkPlanPart part: parts) {
            if (null != result.put(part.getNumber(), part)) {
                throw new IllegalStateException("Duplicate part number: " + part.getNumber());
            }
        }
        return (Map)result;
    }

    protected boolean isWorkPlanEmpty_internal(final Long workPlanId) {
        return !existsEntity(EppWorkPlanRow.class, EppWorkPlanRowGen.L_WORK_PLAN, workPlanId);
    }

    /** не переопределяйте этом метод, нужно переопределять isWorkPlanEmpty_internal */
    @Override
    public boolean isWorkPlanEmpty(final Long workPlanId)
    {
        Map<Long, Boolean> cache = DaoCache.get(EppWorkPlanDAO.CACHE_KEY_WORK_PLAN_DAO_WORK_PLAN_EMPTY_MAP);
        if (null == cache) {
            DaoCache.put(EppWorkPlanDAO.CACHE_KEY_WORK_PLAN_DAO_WORK_PLAN_EMPTY_MAP, cache = new HashMap<>());
        }

        Boolean value = cache.get(workPlanId);
        if (null == value) {
            cache.put(workPlanId, value = this.isWorkPlanEmpty_internal(workPlanId));
        }

        return value;
    }


    @Override
    public boolean doGenerateWorkPlanRows(final Long workPlanId, final boolean lookupLastYear)
    {
        final Session session = this.getSession();
        final EppWorkPlanBase eppWorkPlan = EppWorkPlanDAO.check_editable((EppWorkPlanBase)session.get(EppWorkPlanBase.class, workPlanId));

        NamedSyncInTransactionCheckLocker.register(session, String.valueOf(workPlanId));
        if (!this.isWorkPlanEmpty(workPlanId)) { return false; }

        final EppEduPlanVersionBlock eduPlanVersionBlock = eppWorkPlan.getBlock();
        final int currentYear = eppWorkPlan.getYear().getEducationYear().getIntValue();
        final Term currentTerm = eppWorkPlan.getTerm();

        if (lookupLastYear) {
            /* ищем наиболее актуальный подходящий РУП (именно основной) */

            final DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(EppWorkPlan.class, "wp").column(property(EntityBase.id().fromAlias("wp")));
            dql.where(eq(property(EppWorkPlanGen.parent().id().fromAlias("wp")), value(eduPlanVersionBlock.getId())));
            dql.where(eq(property(EppWorkPlanGen.term().fromAlias("wp")), value(currentTerm)));
            dql.where(lt(property(EppWorkPlanGen.year().educationYear().intValue().fromAlias("wp")), value(currentYear)));
            dql.order(property(EppWorkPlanGen.year().educationYear().intValue().fromAlias("wp")), OrderDirection.desc); // наиболее актуальный в учебного года
            dql.order(property(EntityBase.id().fromAlias("wp")), OrderDirection.desc); // наиболее актуальный в плане времени создания (в рамках года)
            final Long sourceWpId = dql.createStatement(session).setMaxResults(1).uniqueResult();

            if (null != sourceWpId)
            {
                /* если есть, то генерируем из него */
                return this.doGenerateWorkPlanRowsFromWorkplan(eppWorkPlan.getId(), sourceWpId);
            }
        }

        final WorkPlanRowSource rowSource = new WorkPlanRowSource(currentTerm.getIntValue(), null, null);
        return this.doGenerateWorkPlanRowsFromEpvBlock(eppWorkPlan.getId(), eppWorkPlan.getBlock().getId(), key -> rowSource);
    }

    @Override
    public boolean doGenerateWorkPlanRowsFromEpvBlock(final Long workPlanId, final Long sourceEduplanVersionBlockId, final SafeMap.Callback<Long, WorkPlanRowSource> sourceTermResolver)
    {
        // TODO подумать, как лучше переделать на универсальные интерфейсы


        final Session session = this.getSession();
        final EppWorkPlanBase eppWorkPlan = EppWorkPlanDAO.check_editable((EppWorkPlanBase)session.get(EppWorkPlanBase.class, workPlanId));
        final EppWorkPlanRowKind wpRowKind = this.getByNaturalId(new EppWorkPlanRowKind.NaturalId(EppWorkPlanRowKindCodes.MAIN));
        final EppEduPlanVersionBlock eduPlanVersionBlock = null == sourceEduplanVersionBlockId ? eppWorkPlan.getBlock() : (EppEduPlanVersionBlock)session.get(EppEduPlanVersionBlock.class, sourceEduplanVersionBlockId);
        final Long blockId = eduPlanVersionBlock.getId();

        final EppCustomEduPlan customEduPlan = eppWorkPlan.getWorkPlan().getCustomEduPlan();
        final IEppCustomPlanWrapper customPlanWrapper = customEduPlan != null ? new EppCustomPlanWrapper(customEduPlan) : null;

        final IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(blockId, true);
        final Map<Integer, IEppWorkPlanPart> partMap = this.fillWorkPlanParts(eppWorkPlan, (eppWorkPlan1, graphWeekCodeMap) -> {
            if (graphWeekCodeMap.size() >= 1) {
                // должна быть хотябы одна неделя
                return ImmutableSet.of(new EppWorkPlanPart(eppWorkPlan1.getWorkPlan(), 1, graphWeekCodeMap.size()));
            }
            // если недель нет - то не судьба
            return null;
        });
        final EppWorkPlanPartLoadDistributor loadDistributor = new EppWorkPlanPartLoadDistributor(partMap);
        //final Map<String, EppControlActionType> actionTypeMap = EppEduPlanVersionDataDAO.getActionTypeMap();

        int rowsChangeCount = 0;
        //final Map<Long, Map<String, Integer>> actionMap = new HashMap<>(16);
        final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> loadMap = new HashMap<>(16);

        // Список строк РУП созданных по элементам реестра мероприятий
        List<EppWorkPlanRegistryElementRow> createdWorkPlanRegElRows = Lists.newArrayList();

        /////////////////////////////////
        // данные по элементам реестра //
        /////////////////////////////////
        {
            final Predicate<IEppEpvRowWrapper> regElPredicate = wrapper -> {
                final WorkPlanRowSource rowSource = sourceTermResolver.resolve(wrapper.getId());
                if (null == rowSource) { return false; }

                // проверяем, что это дисциплина и у нее есть активность в указанном семестре
                final IEppEpvRow row = wrapper.getRow();
                return (row instanceof EppEpvRegistryRow) && (customPlanWrapper == null
                        ? wrapper.hasInTermActivity(rowSource.getTermNumber())
                        : customPlanWrapper.hasInTermActivity(wrapper, rowSource.getTermNumber()));
            };

            final Collection<IEppEpvRowWrapper> eduPlanRegElBlockRows = EppEduPlanVersionDataDAO.filterEduPlanBlockRows(blockWrapper.getRowMap(), regElPredicate, false);


            // закэшируем дисциплины
            final Set<Long> ids = new HashSet<>(eduPlanRegElBlockRows.size());
            for (final IEppEpvRowWrapper wrapper : eduPlanRegElBlockRows) {
                final WorkPlanRowSource rowSource = sourceTermResolver.resolve(wrapper.getId());
                if (null != rowSource) {
                    final EppRegistryElement registryElement = ((EppEpvRegistryRow) wrapper.getRow()).getRegistryElement();
                    if (null != registryElement)
                        ids.add(registryElement.getId());
                }
            }
            final Map<Long, IEppRegElWrapper> registryElementDataMap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(ids);

            final Map<Integer, Collection<IEppCustomPlanRowWrapper>> customPlanRowMap = customPlanWrapper != null ? customPlanWrapper.getRowMap() : null;
            final Map<PairKey<Long, Integer>, IEppCustomPlanRowWrapper> customPlanSourceMap = customPlanWrapper != null ? customPlanWrapper.getSourceMap() : null;

            for (final IEppEpvRowWrapper epvRowWrapper : eduPlanRegElBlockRows)
            {
                final WorkPlanRowSource rowSource = sourceTermResolver.resolve(epvRowWrapper.getId());
                if (null == rowSource) {
                    continue;
                }

                // Собираем список частей элемента реестра, для которых будем создавать строки РУПа
                final Map<Integer, Integer> partNumberToSrcEpvRowTerm;
                if (customPlanWrapper != null) {

                    // Для индивидуального плана одна строка УПв может более одного раза присутствовать в одном семестре,
                    // поэтому собираем все исходные семестры строки УПв, попавшие в заданный семестр РУПа, согласно инд. плану.

                    final Collection<IEppCustomPlanRowWrapper> customRowsInTerm = customPlanRowMap.get(rowSource.getTermNumber());
                    if (customRowsInTerm == null || customRowsInTerm.isEmpty()) {
                        continue;
                    }

                    partNumberToSrcEpvRowTerm = new TreeMap<>();
                    for (IEppCustomPlanRowWrapper customRow : customRowsInTerm) {
                        if (!customRow.isExcluded() && customRow.getSourceRow().equals(epvRowWrapper)) {
                            final int srcTerm = customRow.getSourceTermNumber();
                            final int partNumber = Math.max(1, epvRowWrapper.getActiveTermNumber(srcTerm));
                            partNumberToSrcEpvRowTerm.put(partNumber, srcTerm);
                        }
                    }

                    if (partNumberToSrcEpvRowTerm.isEmpty()) {
                        continue;
                    }

                } else {
                    final int partNumber = Math.max(1, epvRowWrapper.getActiveTermNumber(rowSource.getTermNumber()));
                    partNumberToSrcEpvRowTerm = ImmutableMap.of(partNumber, rowSource.getTermNumber());
                }

                final EppEpvRegistryRow epvRegistryRow = (EppEpvRegistryRow) epvRowWrapper.getRow();
                final EppRegistryElement registryElement = epvRegistryRow.getRegistryElement();
                final Map<Integer, IEppRegElPartWrapper> regElPartMap = registryElement != null
                        ? registryElementDataMap.get(registryElement.getId()).getPartMap()
                        : null;

                for (final Map.Entry<Integer, Integer> partEntry : partNumberToSrcEpvRowTerm.entrySet())
                {
                    final int partNumber = partEntry.getKey();
                    final int srcTermNumber = partEntry.getValue();

                    final IEppCustomPlanRowWrapper customPlanRowWrapper = customPlanSourceMap != null
                            ? customPlanSourceMap.get(new PairKey<>(epvRegistryRow.getId(), srcTermNumber))
                            : null;

                    final IEppRegElPartWrapper part = regElPartMap != null ? regElPartMap.get(partNumber) : null;
                    if (part != null)
                    {
                        final EppWorkPlanRegistryElementRow row = new EppWorkPlanRegistryElementRow();
                        row.setWorkPlan(eppWorkPlan);
                        row.setRegistryElementPart(part.getItem());
                        row.setNumber(epvRowWrapper.getIndex());
                        row.setKind(wpRowKind);
                        row.setBeginDate(rowSource.getDateStart());
                        row.setEndDate(rowSource.getDateEnd());

                        // если в группе - то указываем название группы
                        final IEppEpvRowWrapper parentWrapper = epvRowWrapper.getHierarhyParent();
                        if (null != parentWrapper)
                        {
                            if (parentWrapper.getRow() instanceof EppEpvGroupReRow) {
                                row.setTitleAppendix(parentWrapper.getTitle());
                            } else if (parentWrapper.getRow() instanceof EppEpvGroupImRow) {
                                row.setTitleAppendix(parentWrapper.getTitle());
                            }
                        }

                        // Если инд. план, то переносим из него признак перезачтения/переаттестации
                        row.setNeedRetake(customPlanRowWrapper != null ? customPlanRowWrapper.getNeedRetake() : null);

                        session.saveOrUpdate(row);
                        rowsChangeCount++;

                        createdWorkPlanRegElRows.add(row);

                        // сохраняем нагрузку (из реестра)
                        loadMap.put(row.getId(), loadDistributor.getPartLoadDistributionMap(row, part));

                        // сохраняем контрольные мероприятия
                        /*actionMap.put(row.getId(), this.doGenerateWorkPlanRowsFromEpvBlock_getRowActionMap(epvRowWrapper, srcTermNumber, actionTypeMap)); */

                        // если удалось сохранить строку - переходим к следующей
                        continue;
                    }


                    // если не случилось - создаем фэйк
                    final EppWorkPlanTemplateRow row = new EppWorkPlanTemplateRow();
                    row.setWorkPlan(eppWorkPlan);
                    row.setPart(partNumber);
                    row.setBeginDate(rowSource.getDateStart());
                    row.setEndDate(rowSource.getDateEnd());
                    row.setNumber(epvRowWrapper.getIndex());
                    row.setTitle(epvRegistryRow.getTitle());
                    row.setKind(wpRowKind);
                    row.setType(epvRegistryRow.getRegistryElementType());
                    row.setNeedRetake(customPlanRowWrapper != null ? customPlanRowWrapper.getNeedRetake() : null);

                    session.saveOrUpdate(row);
                    rowsChangeCount++;

                    // сохраняем контрольные мероприятия
                    /*actionMap.put(row.getId(), this.doGenerateWorkPlanRowsFromEpvBlock_getRowActionMap(epvRowWrapper, srcTermNumber, actionTypeMap));*/
                }
            }
        }

        //////////////////////////////
        // данные по пустым группам //
        //////////////////////////////
/*
        {
            final EppRegistryStructure disciplineStructure = this.getCatalogItem(EppRegistryStructure.class, EppRegistryStructureCodes.REGISTRY_DISCIPLINE);

            // теперь берем пустые группы из УП(в)
            final Predicate<IEppEpvRowWrapper> groupPredicate = wrapper -> {
                final WorkPlanRowSource rowSource = sourceTermResolver.resolve(wrapper.getId());
                return null != rowSource && EppWorkPlanDAO.canCreateFakeRowFromGroupRow(rowSource.getTermNumber(), wrapper);

            };

            final Collection<IEppEpvRowWrapper> eduPlanGroupBlockRows = EppEduPlanVersionDataDAO.filterEduPlanBlockRows(blockWrapper.getRowMap(), groupPredicate, false);
            for (final IEppEpvRowWrapper wrapper : eduPlanGroupBlockRows)
            {
                final WorkPlanRowSource rowSource = sourceTermResolver.resolve(wrapper.getId());
                if (null != rowSource) {
                    final IEppEpvRow groupRow = wrapper.getRow();
                    final EppWorkPlanTemplateRow row = new EppWorkPlanTemplateRow();
                    row.setWorkPlan(eppWorkPlan);
                    row.setPart(Math.max(1, wrapper.getActiveTermNumber(rowSource.getTermNumber())));
                    row.setBeginDate(rowSource.getDateStart());
                    row.setEndDate(rowSource.getDateEnd());
                    row.setNumber(wrapper.getIndex());
                    row.setTitle(groupRow.getTitle());
                    row.setKind(wpRowKind);
                    row.setType(disciplineStructure);

                    session.saveOrUpdate(row);
                    rowsChangeCount++;

                    // сохраняем контрольные мероприятия
                    */
/*actionMap.put(row.getId(), this.doGenerateWorkPlanRowsFromEpvBlock_getRowActionMap(wrapper, rowSource.getTermNumber(), actionTypeMap)); *//*

                }
            }
        }
*/

        IEppWorkPlanDataDAO.instance.get().doUpdateRowPartLoadElementsMap(loadMap);
        processCreatedWorkPlanRegElRows(createdWorkPlanRegElRows);

        return rowsChangeCount > 0;
    }

    // Метод обработки созданных строк РУП по строкам УПв (для возможности переопределения в проектном слое)
    protected void processCreatedWorkPlanRegElRows(List<EppWorkPlanRegistryElementRow> rows)
    {
    }


//    private Map<String, Integer> doGenerateWorkPlanRowsFromEpvBlock_getRowActionMap(final IEppEpvRowWrapper epvRowWrapper, final int currentTerm, final Map<String, EppControlActionType> actionTypeMap) {
//        final Map<String, Integer> result = new HashMap<>(4);
//        for (final Map.Entry<String, EppControlActionType> actionTypeEntry : actionTypeMap.entrySet()) {
//            final int size = epvRowWrapper.getActionSize(currentTerm, actionTypeEntry.getKey());
//            if (size > 0) { result.put(actionTypeEntry.getKey(), size); }
//        }
//        return result;
//    }


    public interface IWorkPlanPartLoadDistributor {
        Map<Integer, Map<String, EppWorkPlanRowPartLoad>> getPartLoadDistributionMap(EppWorkPlanRow targetRow, IEppWorkPlanRowWrapper sourceRow, Map<Integer, IEppWorkPlanPart> sourcePartMap);
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean doGenerateWorkPlanRowsFromWorkplan(final Long workPlanId, final Long sourceWpId) {
        final Session session = this.getSession();
        final EppWorkPlanBase targetWorkPlan = EppWorkPlanDAO.check_editable((EppWorkPlanBase)session.get(EppWorkPlanBase.class, workPlanId));

        NamedSyncInTransactionCheckLocker.register(session, String.valueOf(workPlanId));
        if (!this.isWorkPlanEmpty(workPlanId)) { return false; }

        final IEppWorkPlanWrapper sourceWrapper = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(Collections.singleton(sourceWpId)).get(sourceWpId);
        final EppWorkPlanBase sourceWorkPlan = sourceWrapper.getWorkPlan();

        final Map<Integer, IEppWorkPlanPart> sourcePartMap = this.getWorkPlanPartMap(this.getList(EppWorkPlanPart.class, EppWorkPlanPartGen.workPlan().id().s(), sourceWorkPlan.getWorkPlan().getId()));
        final Map<Integer, IEppWorkPlanPart> targetPartMap = EppWorkPlanDAO.this.fillWorkPlanParts(targetWorkPlan, (eppWorkPlan, graphWeekCodeMap) -> sourcePartMap.values().stream()
                .map(sourcePart -> new EppWorkPlanPart(eppWorkPlan, sourcePart.getNumber()).updateSize(sourcePart))
                .collect(Collectors.toSet()));

        final IWorkPlanPartLoadDistributor loadDistributor;
        if (targetWorkPlan.getWorkPlan().equals(sourceWorkPlan.getWorkPlan()) || this.isSamePartMap(sourcePartMap, targetPartMap))
        {
            // если РУП(ы) в одном базовом РУП(е) или же у них одинаковые части
            // то нагрузку копировать очень просто - не надо пересчитывать ее для частей - они точно одинаковые

            loadDistributor = new IWorkPlanPartLoadDistributor() {
                final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> sourceLoadMap = IEppWorkPlanDataDAO.instance.get().getRowPartLoadDataMap(sourceWrapper.getRowMap().keySet());

                @Override public Map<Integer, Map<String, EppWorkPlanRowPartLoad>> getPartLoadDistributionMap(final EppWorkPlanRow targetRow, final IEppWorkPlanRowWrapper sourceRow, final Map<Integer, IEppWorkPlanPart> sourcePartMap) {
                    final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> source = this.sourceLoadMap.get(sourceRow.getId());
                    if (null == source) { return null; }

                    final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> target = new HashMap<>(source.size());
                    for (final Map.Entry<Integer, Map<String, EppWorkPlanRowPartLoad>> e0: source.entrySet()) {
                        if (null != e0.getValue()) {
                            final Map<String, EppWorkPlanRowPartLoad> t = new HashMap<>(e0.getValue().size());
                            for (final Map.Entry<String, EppWorkPlanRowPartLoad> e1: e0.getValue().entrySet()) {
                                t.put(e1.getKey(), new EppWorkPlanRowPartLoad(targetRow, e1.getValue()));
                            }
                            target.put(e0.getKey(), t);
                        }
                    }
                    return target;
                }
            };
        }
        else
        {
            // если РУП(ы) разные - начинаем пляски с бубном (здесь, увы, ничего не скопировать - нагрузка берется из реестра)

            loadDistributor = new IWorkPlanPartLoadDistributor() {
                final EppWorkPlanPartLoadDistributor distributor = new EppWorkPlanPartLoadDistributor(targetPartMap);
                @Override public Map<Integer, Map<String, EppWorkPlanRowPartLoad>> getPartLoadDistributionMap(final EppWorkPlanRow targetRow, final IEppWorkPlanRowWrapper sourceRow, final Map<Integer, IEppWorkPlanPart> sourcePartMap) {
                    return this.distributor.getPartLoadDistributionMap(targetRow, sourceRow.getRegistryElementPart());
                }
            };
        }

        //final Map<String, EppControlActionType> actionTypeMap = EppEduPlanVersionDataDAO.getActionTypeMap();

        int rowsChangeCount = 0;
        //final Map<Long, Map<String, Integer>> actionMap = new HashMap<>();
        final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> loadMap = new HashMap<>(16);


        for (final IEppWorkPlanRowWrapper rowWrapper: sourceWrapper.getRowMap().values()) {
            try {
                final EppWorkPlanRow cloneRow = ((Class<EppWorkPlanRow>)ClassUtils.getUserClass(rowWrapper.getRow())).newInstance();
                cloneRow.update(rowWrapper.getRow());
                cloneRow.setWorkPlan(targetWorkPlan);
                session.save(cloneRow);
                rowsChangeCount++;

                // нагрузка
                {
                    final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> map = loadDistributor.getPartLoadDistributionMap(cloneRow, rowWrapper, sourcePartMap);
                    if (null != map) {
                        loadMap.put(cloneRow.getId(), map);
                    }
                }

                // контроль
                {
                    //final Map<String, Integer> map = new HashMap<>();
                    //for (final Map.Entry<String, EppControlActionType> actionTypeEntry : actionTypeMap.entrySet()) {
                    //    final int size = rowWrapper.getActionSize(actionTypeEntry.getKey());
                    //    if (size > 0) { map.put(actionTypeEntry.getKey(), size); }
                    //}
                    //actionMap.put(cloneRow.getId(), map);
                }

            } catch (final Exception e) {
                throw CoreExceptionUtils.getRuntimeException(e);
            }
        }

        IEppWorkPlanDataDAO.instance.get().doUpdateRowPartLoadElementsMap(loadMap);
        IEppWorkPlanDataDAO.instance.get().doUpdateRowPartLoadPeriodsMap(loadMap);
        return (rowsChangeCount>0);
    }

    @SuppressWarnings("unchecked")
    private boolean isSamePartMap(final Map<Integer, IEppWorkPlanPart> sourcePartMap, final Map<Integer, IEppWorkPlanPart> targetPartMap) {
        final Collection<Integer> keys = CollectionUtils.union(sourcePartMap.keySet(), targetPartMap.keySet());
        for (final Integer key: keys) {
            if (0 != IEppWorkPlanPart.EQUALS_COMPARATOR.compare(sourcePartMap.get(key), targetPartMap.get(key))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void doGenerateWorkPlanVersionRows(final EppWorkPlanVersion version)
    {
        this.doGenerateWorkPlanRowsFromWorkplan(version.getId(), version.getWorkPlan().getId());
    }


    @Override
    public List<EppWorkGraphRowWeek> getWorkGraphWeeks4Plan(final Long workplanId) {
        final EppWorkPlan workplan = this.getNotNull(EppWorkPlanBase.class, workplanId).getWorkPlan();
        final EppEduPlanVersion eduPlanVersion = workplan.getBlock().getEduPlanVersion();

        final MQBuilder gupRowIdsBuilder = new MQBuilder(EppWorkGraphRow2EduPlanGen.ENTITY_NAME, "g2p", new String[] { EppWorkGraphRow2EduPlanGen.row().id().s() });
        gupRowIdsBuilder.add(MQExpression.eq("g2p", EppWorkGraphRow2EduPlanGen.row().graph().year().toString(), workplan.getYear()));
        gupRowIdsBuilder.add(MQExpression.eq("g2p", EppWorkGraphRow2EduPlanGen.eduPlanVersion().toString(), eduPlanVersion));

        //if (true)
        {
            gupRowIdsBuilder.add(MQExpression.eq("g2p", EppWorkGraphRow2EduPlanGen.row().graph().developForm().programForm().toString(), eduPlanVersion.getEduPlan().getProgramForm()));
            gupRowIdsBuilder.add(MQExpression.eq("g2p", EppWorkGraphRow2EduPlanGen.row().graph().developCondition().toString(), eduPlanVersion.getEduPlan().getDevelopCondition()));
            if (null == eduPlanVersion.getEduPlan().getProgramTrait()) {
                gupRowIdsBuilder.add(MQExpression.isNull("g2p", EppWorkGraphRow2EduPlanGen.row().graph().developTech().programTrait().toString()));
            } else {
                gupRowIdsBuilder.add(MQExpression.eq("g2p", EppWorkGraphRow2EduPlanGen.row().graph().developTech().programTrait().toString(), eduPlanVersion.getEduPlan().getProgramTrait()));
            }
            gupRowIdsBuilder.add(MQExpression.eq("g2p", EppWorkGraphRow2EduPlanGen.row().graph().developGrid().toString(), eduPlanVersion.getEduPlan().getDevelopGrid()));
        }

        final MQBuilder builder = new MQBuilder(EppWorkGraphRowWeekGen.ENTITY_NAME, "w");
        builder.setNeedDistinct(true);
        builder.add(MQExpression.in("w", EppWorkGraphRowWeekGen.row().id().s(), gupRowIdsBuilder));
        builder.add(MQExpression.eq("w", EppWorkGraphRowWeekGen.row().graph().year().toString(), workplan.getYear()));
        builder.add(MQExpression.eq("w", EppWorkGraphRowWeekGen.term().toString(), workplan.getTerm()));
        builder.addOrder("w", EppWorkGraphRowWeekGen.week().s());

        final List<EppWorkGraphRowWeek> graphWeeks = builder.getResultList(this.getSession());

        if (graphWeeks.size() > 0) {
            final EppWorkGraphRowWeek firstWeek = graphWeeks.get(0);
            final EppWorkGraphRowWeek lastWeek = graphWeeks.get(graphWeeks.size()-1);
            if (graphWeeks.size() != ((lastWeek.getWeek()-firstWeek.getWeek())+1)) {
                throw new IllegalStateException();
            }
        }

        return graphWeeks;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<Integer, List<EppWorkGraphRowWeek>> getWorkGraphWeeks4PlanParts(final Long workplanId) {

        final Map<Integer, Object> partMap = (Map)this.getWorkPlanPartMap(workplanId);
        if (partMap.isEmpty()) { return Collections.emptyMap(); }

        final List<EppWorkGraphRowWeek> weeks = this.getWorkGraphWeeks4Plan(workplanId);
        if (weeks.isEmpty()) { return Collections.emptyMap(); }

        final Iterator<EppWorkGraphRowWeek> it = weeks.iterator();
        for (final Map.Entry<Integer, Object> e: partMap.entrySet()) {
            final List<EppWorkGraphRowWeek> result = new ArrayList<>(1+(weeks.size()/2));
            for (int i=((EppWorkPlanPart)e.getValue()).calculateTotalWeeks(); i>0; i--) {
                if (it.hasNext()) { result.add(it.next()); }
            }
            e.setValue(result);
        }

        return (Map)partMap;
    }

    /**
     * пересчитывает число аудиторных недель и недель самостоятельного обучения для частей РУП
     * на основе кодов недель
     * @param eLoadWeekTypeMap - настройка расчета { eload.fillCode -> { week.code } }
     * @param graphWeekCodeMap - недели { index -> week.code }, index=0 - начало семестра
     * @param parts - части РУП
     */
    public static void recalculateWorkPlanPartWeeks(final Map<String, Set<String>> eLoadWeekTypeMap, final Map<Integer, String> graphWeekCodeMap, final Collection<EppWorkPlanPart> parts)
    {
        final Set<String> auditWeekTypes = (null == eLoadWeekTypeMap ? null : eLoadWeekTypeMap.get(EppELoadType.TYPE_TOTAL_AUDIT));
        final Set<String> selfworkWeekTypes = (null == eLoadWeekTypeMap ? null : eLoadWeekTypeMap.get(EppELoadType.TYPE_TOTAL_SELFWORK));

        int index = 0;
        for (final EppWorkPlanPart part: parts) {
            int auditWeeks = 0;
            int selfworkWeeks = 0;
            for (int i=0;i<part.getTotalWeeks();i++) {
                final String weekTypeCode = graphWeekCodeMap.get(index++);

                if ((null != auditWeekTypes) && auditWeekTypes.contains(weekTypeCode)) { auditWeeks ++; }
                if ((null != selfworkWeekTypes) && selfworkWeekTypes.contains(weekTypeCode)) { selfworkWeeks ++; }
            }
            part.setAuditWeeks(auditWeeks);
            part.setSelfworkWeeks(selfworkWeeks);
        }
    }


    @Override
    public List<EppControlActionType> getActiveControlActionTypes(final EppRegistryStructure eppRegistryStructure)
    {
        return EppEduPlanVersionDataDAO.getActionTypeMap().values().stream()
                .filter(EppControlActionType.getPredicate(eppRegistryStructure))
                .collect(Collectors.toList());
    }

//    @Override
//    public void doMergeWorkPlanVersions(final Collection<Long> workPlanVersionsIds)
//    {
//        if ((null == workPlanVersionsIds) || workPlanVersionsIds.isEmpty()) {
//            return;
//        }
//
//        final Session session = this.getSession();
//        final EppState archived = this.getByNaturalId(new EppStateGen.NaturalId(EppState.STATE_ARCHIVED));
//        final Map<Long, IEppWorkPlanWrapper> dataMap = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(workPlanVersionsIds);
//        final Map<Long, Map<Set<String>, IEppWorkPlanWrapper>> globalMap = new HashMap<>(4);
//
//        for (final IEppWorkPlanWrapper wrapper: dataMap.values())
//        {
//            final Map<Set<String>, IEppWorkPlanWrapper> uniqueVersionMap = SafeMap.safeGet(globalMap, wrapper.getWorkPlan().getWorkPlan().getId(), HashMap.class);
//
//            final Set<String> key = wrapper.getDescriptionSet();
//            final IEppWorkPlanWrapper uniqueVersion = uniqueVersionMap.get(key);
//            if (null == uniqueVersion)
//            {
//                uniqueVersionMap.put(key, wrapper);
//            }
//            else
//            {
//                new DQLUpdateBuilder(EppStudent2WorkPlan.class)
//                .where(eq(property(EppStudent2WorkPlan.workPlan()), value(wrapper.getWorkPlan())))
//                .set(EppStudent2WorkPlan.workPlan().s(), value(uniqueVersion.getWorkPlan()))
//                .createStatement(session).execute();
//
//                if (wrapper.getWorkPlan().getState().isReadOnlyState())
//                {
//                    // РУП менять нельзя - меняем ему состояние на архивное
//                    wrapper.getWorkPlan().setState(archived);
//                    session.saveOrUpdate(wrapper);
//                }
//                else
//                {
//                    // РУП можно изменять - удаляем его
//                    session.delete(wrapper.getWorkPlan());
//                }
//            }
//        }
//    }

    @Override
    public void doCleanUpWorkPlanTemporaryVersions(final Long workplanId) {

        // FIXME: TODO: implement me

        // TODO: объединение и переброска всех связей на новый РУП(в)
        // TODO: удаление временных РУП(в)

    }

    @Override
    public void doCreateEppYearPartToWP(EppWorkPlan wp)
    {
        DevelopGridTerm term = getByNaturalId(new DevelopGridTerm.NaturalId(wp.getEduPlan().getDevelopGrid(), wp.getTerm()));
        if (term == null) return; // may be need excepion?

        EppYearPart eppPart = getByNaturalId(new EppYearPart.NaturalId(wp.getYear(), term.getPart()));
        if (eppPart == null)
        {
            eppPart = new EppYearPart(wp.getYear(), term.getPart());
            save(eppPart);
        }
    }
}
