package ru.tandemservice.uniepp.component.eduplan.row.AddEdit;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

/**
 * @author vdanilov
 */
public class BaseAddEditDao<T extends EppEpvRow, M extends BaseAddEditModel<T>> extends UniBaseDao implements IPrepareable<M> {

    public static void disablePossibleLoop(final List<HSelectOption> parentList, final Collection<Long> rowIds)
    {
        // дизаблим все, что нельзя выбирать - а выбирать нельзя то, что образует рекурсию
        final Set<Long> ids = new HashSet<Long>(rowIds);
        for (final HSelectOption option : parentList) {
            final IEppEpvRowWrapper row = (IEppEpvRowWrapper)option.getObject();

            IEppEpvRowWrapper current = row;
            while (null != current) {
                if (ids.contains(current.getId())) { option.setCanBeSelected(false); }
                current = current.getHierarhyParent();
            }
        }

        // удаляем все, во что не можем перемещать
        for (final Iterator<HSelectOption> it=parentList.iterator(); it.hasNext(); ) {
            if (!it.next().isCanBeSelected()) { it.remove(); }
        }
    }

    @Override
    public void prepare(M model) {
        model.getRowHolder().refresh();

        //T row = model.getRowHolder().getValue();
        //T dbRow = model.getRowHolder().refresh();
        //dbRow.update(row);
    }

    public T doSaveRow(final M model)
    {
        final T row = model.getRow();

        // проверяем уникальность названия
        if (row instanceof EppEpvTermDistributedRow) {
            checkUniqTitle((EppEpvTermDistributedRow)row);
        }

        // созраняем объект
        if (StringUtils.isBlank(row.getStoredIndex())) { row.setStoredIndex("-"); }
        this.getSession().saveOrUpdate(row);


        // теперь убираем мусор
        final Long templateId = model.getTemplateId();
        if ((null != templateId) && !templateId.equals(row.getId())) {
            this.delete(templateId);
        }

        // пересчитываем индекс
        IEppEduPlanVersionDataDAO.instance.get().doUpdateEpvRowStoredIndex(row.getOwner().getEduPlanVersion().getId());

        return row;
    }

    /**
     * DEV-4188: Необходимо на форме добавления элемента в блок УПв проверять уникальность названия элемента, вводимого пользователем. Если в базовом блоке, или в блоке, в который добавляется элемент, есть элемент с таким же названием (без учета регистра), то выводить сообщение "Элемент с таким названием уже существует в базовом, или текущем блоке версии учебного плана".
     * @param row
     */
    public static void checkUniqTitle(final EppEpvTermDistributedRow row)
    {
        if (IEppSettingsDAO.instance.get().getGlobalSettings().isAllowNotUniqueEpvRowName()) return;

        final String title = StringUtils.trimToEmpty(row.getTitle());
        final Collection<IEppEpvRowWrapper> rows = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockRowMap(row.getOwner().getId(), false).values();
        for (final IEppEpvRowWrapper wrapper: rows)
        {
            if (EntityBase.equals(row.getId(), wrapper.getId())) {
                // текущий объект проверять не надо :)
                continue;
            }

            if (wrapper.getRow() instanceof EppEpvTermDistributedRow)
            {
                final String t = StringUtils.trimToEmpty(wrapper.getRow().getTitle());
                if (t.equalsIgnoreCase(title)) {
                    throw new ApplicationException("Элемент с таким названием уже существует в базовом или текущем блоке версии учебного плана.");
                }
            }
        }
    }
}
