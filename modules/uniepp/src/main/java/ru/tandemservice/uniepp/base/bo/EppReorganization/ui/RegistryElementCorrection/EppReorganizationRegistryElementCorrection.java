/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.ui.RegistryElementCorrection;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.ReorganizationJournal.OrgUnitReorganizationJournal;
import ru.tandemservice.uniepp.base.bo.EppReorganization.ui.RegistryElementCorrectionElementsTab.EppReorganizationRegistryElementCorrectionElementsTab;

/**
 * @author rsizonenko
 * @since 30.03.2016
 */
@Configuration
public class EppReorganizationRegistryElementCorrection extends BusinessComponentManager {

    public static final String TAB_PANEL = "registryElementCorrectionTabPanel";


    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab("elementsTab", EppReorganizationRegistryElementCorrectionElementsTab.class))
                .addTab(componentTab("journalTab", OrgUnitReorganizationJournal.class).permissionKey("orgUnitReorganizationJournal").after("elementsTab").parameters("ui:journalParams"))
                .create();
    }

}
