package ru.tandemservice.uniepp.entity.workplan;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanTemplateRowGen;

/**
 * Запись РУП (шаблон строки)
 */
public class EppWorkPlanTemplateRow extends EppWorkPlanTemplateRowGen
{
    @Override public String getFormattedLoad() {
        Long size = this.getEduplanTotalSize();
        if ((null == size) || (size < 0)) { size = 0L; }

        return UniEppUtils.formatLoad(UniEppUtils.wrap(size), false) + " ч.";
    }

    @Override public OrgUnit getOwner() {
        return null;
    }

    @Override public int getRegistryElementPartAmount() {
        final Integer i = this.getEduplanTermCount();
        return (null == i ? -1 : i);
    }

    @Override public int getRegistryElementPartNumber() {
        return this.getPart();
    }

    @Override
    public String getDescriptionString() {
        return (super.getDescriptionString()+'\n'+this.getRegistryElementPartNumber()+'\n'+this.getType().getCode());
    }
}