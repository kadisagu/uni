package ru.tandemservice.uniepp.component.orgunit.OrgUnitRealGroupTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.sec.OrgUnitHolder;

/**
 * @author vdanilov
 */

@Input({
    @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
@State({
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class Model
{
    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    public OrgUnitHolder getOrgUnitHolder() { return this.orgUnitHolder; }
    public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }
    public Long getOrgUnitId() { return this.getOrgUnitHolder().getId(); }

    public CommonPostfixPermissionModelBase getSec() { return this.getOrgUnitHolder().getSecModel(); }

    private String selectedTab;
    public String getSelectedTab() { return this.selectedTab; }
    public void setSelectedTab(final String selectedTab) { this.selectedTab = selectedTab; }
}
