package ru.tandemservice.uniepp.entity.plan.data;

import org.tandemframework.core.entity.IEntity;

/**
 * @author vdanilov
 */
public interface IEppEpvRowTermDetail extends IEntity {
    EppEpvRowTerm getRowTerm();
    String getRelationTargetCode();
}
