package ru.tandemservice.uniepp.component.student.StudentWorkPlanEdit;

import ru.tandemservice.uni.dao.IPrepareable;

public interface IDAO extends IPrepareable<Model>
{

    void doAccept(Model model);

    void doReject(Model model);

}
