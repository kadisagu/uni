/* $Id: $ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.CheckPrint.logic;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import java.util.List;
import java.util.Map;

/**
 * @author Andrey Andreev
 * @since 16.03.2017
 */
public class EppWorkPlanCheckPrintData
{
    private List<EducationYear> _educationYearList;
    private List<EppState> _workPlanStateList;
    private List<OrgUnit> _orgUnitList;
    private List<EppState> _regStateList;
    private List<EppRegistryStructure> _regTypeList;

    private Map<EducationYear, List<EppWorkPlanRegistryElementRow>> rows;
    private Map<Long, String> _groupsByRowMap;


    public List<EducationYear> getEducationYearList()
    {
        return _educationYearList;
    }

    public void setEducationYearList(List<EducationYear> educationYearList)
    {
        _educationYearList = educationYearList;
    }

    public List<EppState> getWorkPlanStateList()
    {
        return _workPlanStateList;
    }

    public void setWorkPlanStateList(List<EppState> workPlanStateList)
    {
        _workPlanStateList = workPlanStateList;
    }

    public List<OrgUnit> getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(List<OrgUnit> orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public List<EppState> getRegStateList()
    {
        return _regStateList;
    }

    public void setRegStateList(List<EppState> regStateList)
    {
        _regStateList = regStateList;
    }

    public List<EppRegistryStructure> getRegTypeList()
    {
        return _regTypeList;
    }

    public void setRegTypeList(List<EppRegistryStructure> regTypeList)
    {
        _regTypeList = regTypeList;
    }


    public Map<EducationYear, List<EppWorkPlanRegistryElementRow>> getRows()
    {
        return rows;
    }

    public void setRows(Map<EducationYear, List<EppWorkPlanRegistryElementRow>> rows)
    {
        this.rows = rows;
    }

    public Map<Long, String> getGroupsByRowMap()
    {
        return _groupsByRowMap;
    }

    public void setGroupsByRowMap(Map<Long, String> groupsByRowMap)
    {
        _groupsByRowMap = groupsByRowMap;
    }
}
