package ru.tandemservice.uniepp.events;

import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import org.tandemframework.hibsupport.event.single.listener.IHibernateEventListener;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;

/**
 * @author vdanilov
 */
public class EppStudentEducationOrgUnitValidatorListener implements IHibernateEventListener<ISingleEntityEvent>
{
    @Override public void onEvent(final ISingleEntityEvent event) {
        if (event.getEntity() instanceof Student) {
            // если изменился студент - то проверяем все связи (могли изменится НПП), если что-то поменялось - запускаем демон
            EppStudentSlotDAO.TRANSACTION_AFTER_COMPLETE_REFRESH_STUDENT_AND_WAKEUP.register(event.getSession());
        }
    }
}
