/* $Id$ */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionTrajectoryList;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dao.CommonDAO;

import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

/**
 * @author oleyba
 * @since 5/5/11
 */
public class EppTrajectoryWrapper extends IdentifiableWrapper
{
    private static final long serialVersionUID = 1L;

    private final Set<Long> studentIds;
    private final Collection<Long> workplanIds;
    private final List<EppWorkPlanBase> workplanList;
    private boolean hasErrors;
    private String errorColor;

    public static final String VIEW_PROPERTY_TITLE = "title";
    public static final String VIEW_PROPERTY_STUDENT_COUNT = "studentCount";
    public static final String VIEW_PROPERTY_STATE = "state";

    public EppTrajectoryWrapper(final List<EppWorkPlanBase> workplanList, final Long studentId)
    {
        super(studentId, UniStringUtils.join(workplanList, "title", ", "));
        this.workplanList = workplanList;
        this.studentIds = new HashSet<Long>(Collections.singleton(studentId));
        this.workplanIds = CommonDAO.ids(this.getWorkplanList());
    }

    public Collection<Long> getWorkplanIds()
    {
        return this.workplanIds;
    }

    public int getStudentCount()
    {
        return this.getStudentIds().size();
    }

    public String getState()
    {
        return this.hasErrors ? "есть ошибки" : "ошибок нет";
    }

    // accessors

    public Set<Long> getStudentIds()
    {
        return this.studentIds;
    }

    public List<EppWorkPlanBase> getWorkplanList()
    {
        return this.workplanList;
    }

    public boolean isHasErrors()
    {
        return this.hasErrors;
    }

    public void setHasErrors(final boolean hasErrors)
    {
        this.hasErrors = hasErrors;
    }

    public String getErrorColor()
    {
        return this.errorColor;
    }

    public void setErrorColor(final String errorColor)
    {
        this.errorColor = errorColor;
    }
}
