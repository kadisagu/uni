package ru.tandemservice.uniepp.component.edustd.EduStdBlockAddEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id") })
public class Model
{
    private Long id;

    private EppStateEduStandard element;
    private ISelectModel specModel;
    private List<EduProgramSpecialization> values;

    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public EppStateEduStandard getElement()
    {
        return this.element;
    }

    public void setElement(final EppStateEduStandard element)
    {
        this.element = element;
    }

    public ISelectModel getSpecModel()
    {
        return specModel;
    }

    public void setSpecModel(ISelectModel specModel)
    {
        this.specModel = specModel;
    }

    public List<EduProgramSpecialization> getValues()
    {
        return values;
    }

    public void setValues(List<EduProgramSpecialization> values)
    {
        this.values = values;
    }
}
