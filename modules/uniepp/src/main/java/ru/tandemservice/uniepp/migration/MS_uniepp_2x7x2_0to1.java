package ru.tandemservice.uniepp.migration;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.catalog.CColumn;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLInsertQuery;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchInsertBuilder;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchUpdater;

import java.sql.SQLException;

/**
 * Автоматически сгенерированная миграция
 */
public class MS_uniepp_2x7x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.16"),
                        new ScriptDependency("org.tandemframework.shared", "1.7.2")
                };
    }

    private void _run(DBTool tool) throws Exception
    {
        // новая сущность eppGroupType
        {
            // персистентный интерфейс ru.tandemservice.uniepp.entity.catalog.IEppGroupType - персистентный интерфейс был удален
            tool.dropView("ieppgrouptype_v");

            DBTable dbt = new DBTable("epp_group_type_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("priority_p", DBType.INTEGER).setNullable(false),
                                      new DBColumn("shorttitle_p", DBType.createVarchar(255)),
                                      new DBColumn("abbreviation_p", DBType.createVarchar(255)),
                                      new DBColumn("title_p", DBType.createVarchar(1200)).setNullable(false)
            );
            tool.createTable(dbt);
            tool.entityCodes().ensure("eppGroupType");

            // сущность eppGroupTypeALT - создана новая сущность
            {
                // создано обязательное свойство eppGroupType
                tool.createColumn("epp_c_loadtype_a_t", new DBColumn("eppgrouptype_id", DBType.LONG));

                BatchInsertBuilder insertBuilder = new BatchInsertBuilder("code_p", "priority_p", "title_p", "shorttitle_p", "abbreviation_p");
                insertBuilder.setEntityCode(tool, "eppGroupTypeALT");

                new BatchUpdater("update epp_c_loadtype_a_t set eppgrouptype_id=? where typeuniquecode_p=?", DBType.LONG, DBType.createVarchar(255))
                        .addBatch(insertBuilder.addRow("alt.lectures", 1, "Лекции", "Лекции", "Л"), "001")
                        .addBatch(insertBuilder.addRow("alt.practice", 2, "Практические занятия", "Практ.зан-я", "П"), "002")
                        .addBatch(insertBuilder.addRow("alt.labs", 3, "Лабораторные занятия", "Лаб.зан-я", "ЛБ"), "003")
                        .executeUpdate(tool);

                insertBuilder.executeInsert(tool, "epp_group_type_t");

                // сделать колонку NOT NULL
                tool.setColumnNullable("epp_c_loadtype_a_t", "eppgrouptype_id", false);

                // сущность eppALoadType - удалено свойство typeUniqueCode
                tool.dropColumn("epp_c_loadtype_a_t", "typeuniquecode_p");
            }

            // сущность eppGroupTypeFCA - создана новая сущность
            {
                // создано обязательное свойство eppGroupType
                tool.createColumn("epp_c_cactiontype_f_t", new DBColumn("eppgrouptype_id", DBType.LONG));

                BatchInsertBuilder insertBuilder = new BatchInsertBuilder("code_p", "priority_p", "title_p", "shorttitle_p", "abbreviation_p");
                insertBuilder.setEntityCode(tool, "eppGroupTypeFCA");

                new BatchUpdater("update epp_c_cactiontype_f_t set eppgrouptype_id=? where typeuniquecode_p=?", DBType.LONG, DBType.createVarchar(255))
                        .addBatch(insertBuilder.addRow("fca.exam", 121, "Экзамен", "Э", "Э"), "121")
                        .addBatch(insertBuilder.addRow("fca.setoff", 112, "Зачет", "З", "З"), "112")
                        .addBatch(insertBuilder.addRow("fca.setoffDiff", 111, "Зачет дифференцированный", "ЗД", "ЗД"), "111")
                        .addBatch(insertBuilder.addRow("fca.courseWork", 102, "Курсовая работа", "КР", "КР"), "102")
                        .addBatch(insertBuilder.addRow("fca.courseProject", 101, "Курсовой проект", "КП", "КП"), "101")
                        .addBatch(insertBuilder.addRow("fca.examAccum", 122, "Экзамен накопительный", "ЭН", "ЭН"), "122")
                        .executeUpdate(tool);

                insertBuilder.executeInsert(tool, "epp_group_type_t");

                // сделать колонку NOT NULL
                tool.setColumnNullable("epp_c_cactiontype_f_t", "eppgrouptype_id", false);

                // сущность eppFControlActionType - удалено свойство typeUniqueCode
                tool.dropColumn("epp_c_cactiontype_f_t", "typeuniquecode_p");
            }
        }

        // новая сущность eppRealEduGroup
        {
            // персистентный интерфейс ru.tandemservice.uniepp.entity.student.group.IEppRealEduGroup - персистентный интерфейс был удален
            tool.dropView("iepprealedugroup_v");
            DBTable dbt = new DBTable("epp_real_edu_group_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("summary_id", DBType.LONG).setNullable(false),
                                      new DBColumn("activitypart_id", DBType.LONG).setNullable(false),
                                      new DBColumn("type_id", DBType.LONG).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("creationdate_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("modificationdate_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("level_id", DBType.LONG),
                                      new DBColumn("workplanrow_id", DBType.LONG)
            );
            tool.createTable(dbt);
            tool.entityCodes().ensure("eppRealEduGroup");

            // Сливаем две старые таблицы в одну новую, сразу обновляя ссылку на вид УГС ссылкой на новую таблицу видов УГС
            moveAllFromTableToTableWithReplaceGroupTypeLinkAndDropSrcTable(tool, "epp_rgrp_g4ld_t", "epp_real_edu_group_t", "epp_c_loadtype_a_t");
            moveAllFromTableToTableWithReplaceGroupTypeLinkAndDropSrcTable(tool, "epp_rgrp_g4ca_t", "epp_real_edu_group_t", "epp_c_cactiontype_f_t");

            // персистентный интерфейс ru.tandemservice.uniepp.entity.pps.IEppPPSCollectionOwner - персистентный интерфейс изменился, он будет создан заново после выполнения всех миграций
            tool.dropView("ieppppscollectionowner_v");
        }

        // новая сущность eppRealEduGroupRow
        {
            // персистентный интерфейс ru.tandemservice.uniepp.entity.student.group.IEppRealEduGroupRow - персистентный интерфейс был удален
            tool.dropView("iepprealedugrouprow_v");
            tool.createTable(new DBTable("epp_rgrp_row_t",
                                         new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                         new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                         new DBColumn("studentwpepart_id", DBType.LONG).setNullable(false),
                                         new DBColumn("group_id", DBType.LONG).setNullable(false),
                                         new DBColumn("confirmdate_p", DBType.DATE),
                                         new DBColumn("removaldate_p", DBType.DATE),
                                         new DBColumn("studenteducationorgunit_id", DBType.LONG),
                                         new DBColumn("studentgrouptitle_p", DBType.createVarchar(255))
            ));
            tool.entityCodes().ensure("eppRealEduGroupRow");

            // Сливаем две старые таблицы в одну новую
            moveAllFromTableToTableAndDropSrcTable(tool, "epp_rgrp_g4ld_row_t", "epp_rgrp_row_t");
            moveAllFromTableToTableAndDropSrcTable(tool, "epp_rgrp_g4ca_row_t", "epp_rgrp_row_t");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // новая сущность eppStudentWpePart
        {
            // персистентный интерфейс ru.tandemservice.uniepp.entity.student.slot.IEppStudentWpePart - персистентный интерфейс был удален
            tool.dropView("ieppstudentwpepart_v");
            tool.createTable(new DBTable("epp_student_wpe_part_t",
                                         new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                         new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                         new DBColumn("studentwpe_id", DBType.LONG).setNullable(false),
                                         new DBColumn("type_id", DBType.LONG).setNullable(false),
                                         new DBColumn("modificationdate_p", DBType.TIMESTAMP).setNullable(false),
                                         new DBColumn("removaldate_p", DBType.TIMESTAMP)
            ));
            tool.entityCodes().ensure("eppStudentWpePart");

            // Сливаем две старые таблицы в одну новую, сразу обновляя ссылку на вид УГС ссылкой на новую таблицу видов УГС
            moveAllFromTableToTableWithReplaceGroupTypeLinkAndDropSrcTable(tool, "epp_student_wpe_aload_t", "epp_student_wpe_part_t", "epp_c_loadtype_a_t");
            moveAllFromTableToTableWithReplaceGroupTypeLinkAndDropSrcTable(tool, "epp_student_wpe_caction_t", "epp_student_wpe_part_t", "epp_c_cactiontype_f_t");
        }
    }

    public static void migrateGroupTypeLinks(DBTool tool, String tableName, String linkColumn) throws SQLException
    {
        Preconditions.checkArgument(tool.columnExists(tableName, linkColumn));

        final SQLUpdateQuery upd = new SQLUpdateQuery(tableName, "dest");
        upd.set(linkColumn, "gt.gt_id");
        upd.getUpdatedTableFrom().innerJoin(
                SQLFrom.select("select id as old_id, eppgrouptype_id as gt_id from epp_c_loadtype_a_t union all select id, eppgrouptype_id from epp_c_cactiontype_f_t", "gt"),
                "gt.old_id=dest." + linkColumn
        );
        final String sql = tool.getDialect().getSQLTranslator().toSql(upd);
        final int ret = tool.executeUpdate(sql);
        if (Debug.isEnabled())
            System.out.println(ret + " rows rows in " + tableName+ " migrated to new eppGroupType catalog. SQL: " + sql);
    }

    private static void moveAllFromTableToTableAndDropSrcTable(DBTool tool, String srcTable, String destTable) throws SQLException
    {
        final SQLInsertQuery ins = new SQLInsertQuery(destTable);
        for (CColumn column : tool.table(srcTable).columns())
        {
            ins.set(column.name(), "src." + column.name());
        }
        ins.from(SQLFrom.table(srcTable, "src"));
        final String sql = tool.getDialect().getSQLTranslator().toSql(ins);
        final int ret = tool.executeUpdate(sql);

        if (Debug.isEnabled())
            System.out.println(ret + " rows total moved from " + srcTable + " to " + destTable + ". SQL: " + sql);
    }

    private static void moveAllFromTableToTableWithReplaceGroupTypeLinkAndDropSrcTable(DBTool tool, String srcTable, String destTable, String oldGroupTypeTable) throws SQLException
    {
        final SQLInsertQuery ins = new SQLInsertQuery(destTable);
        for (CColumn column : tool.table(srcTable).columns())
        {
            ins.set(column.name(), "src." + column.name());
        }
        ins.set("type_id", "old_gt.eppgrouptype_id");
        ins.from(SQLFrom.table(srcTable, "src").innerJoin(SQLFrom.table(oldGroupTypeTable, "old_gt"), "old_gt.id=src.type_id"));
        final String sql = tool.getDialect().getSQLTranslator().toSql(ins);
        final int ret = tool.executeUpdate(sql);

        if (Debug.isEnabled())
            System.out.println(ret + " rows total moved from " + srcTable + " to " + destTable + " with update links to eppGroupType by " + oldGroupTypeTable + ". SQL: " + sql);

        tool.dropTable(srcTable, true);
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        final Stopwatch timer = Stopwatch.createStarted();
        _run(tool);
        if (Debug.isEnabled())
            System.out.println(this.getClass().getSimpleName() + " performed in " + timer.stop());
    }
}