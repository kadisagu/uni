package ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvSimpleGroup;

import java.util.Collections;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
        this.onChangeParent(component);
    }

    public void onChangeParent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        final EppEpvGroupReRow row = model.getRow();
        row.setNumber(IEppEduPlanVersionDataDAO.instance.get().getNextNumber(row, Collections.singleton(model.getTemplateId())));
    }

    public void onClickApply(final IBusinessComponent component) {
        try {
            this.getDao().save(this.getModel(component));
        } catch (Throwable t) {
            // component.getDesktop().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        this.deactivate(component);
    }

}
