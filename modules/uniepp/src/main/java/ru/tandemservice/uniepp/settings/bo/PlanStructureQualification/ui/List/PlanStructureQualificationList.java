package ru.tandemservice.uniepp.settings.bo.PlanStructureQualification.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;

/**
 * @author avedernikov
 * @since 07.09.2015
 */

@Configuration
public class PlanStructureQualificationList extends BusinessComponentManager
{
	public static final String STANDARD_DS = "standardDS";

	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
			.addDataSource(selectDS(STANDARD_DS, standardDSHandler()))
			.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler standardDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), EppPlanStructure.class)
			.where(EppPlanStructure.parent(), (Object)null)
			.order(EppPlanStructure.priority());
	}
}
