/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util;

import java.util.Map;

/**
 * Методы, используемые при импорте.
 * @author azhebko
 * @since 12.11.2014
 */
public final class ImtsaUtils
{
    private ImtsaUtils() { }

    /** Суммирует аргументы, null пропускаются. Возвращает null, если значинмых аргументов нет. */
    public static Double safeSum(Double ... values)
    {
        double result = 0L;
        boolean v = false;
        for (Double value: values)
            if (value != null) { v = true; result += value; }

        return v ? result : null;
    }

    /**
     * В мапу по ключу кладется сумма старого значения и <code>value</code>.
     * @see #safeSum */
    public static <T> void addValue(Map<T, Double> valueMap, T key, Double value)
    {
        valueMap.put(key, safeSum(valueMap.get(key), value));
    }
}