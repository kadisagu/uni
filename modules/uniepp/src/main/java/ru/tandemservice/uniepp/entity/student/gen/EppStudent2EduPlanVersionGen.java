package ru.tandemservice.uniepp.entity.student.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебный план студента
 *
 * Определяет УП(в) студента в текущий момент
 * Показывает, какие УП(в) были привязаны к студенту (для неактивных связей)
 * removalDate - дата, с которой связь утрачивает актуальность
 * (все запросы к актуальным данным должны содержать условие removalDate is null)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStudent2EduPlanVersionGen extends EntityBase
 implements INaturalIdentifiable<EppStudent2EduPlanVersionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion";
    public static final String ENTITY_NAME = "eppStudent2EduPlanVersion";
    public static final int VERSION_HASH = -421527497;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String L_BLOCK = "block";
    public static final String L_CUSTOM_EDU_PLAN = "customEduPlan";
    public static final String P_CONFIRM_DATE = "confirmDate";
    public static final String P_REMOVAL_DATE = "removalDate";

    private Student _student;     // Студент
    private EppEduPlanVersion _eduPlanVersion;     // Версия УП
    private EppEduPlanVersionBlock _block;     // Блок УП(в)
    private EppCustomEduPlan _customEduPlan;     // Индивидуальный УП
    private Date _confirmDate;     // Дата утверждения
    private Date _removalDate;     // Дата утраты актуальности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Версия УП. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion Версия УП. Свойство не может быть null.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    /**
     * @return Блок УП(в).
     */
    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    /**
     * @param block Блок УП(в).
     */
    public void setBlock(EppEduPlanVersionBlock block)
    {
        dirty(_block, block);
        _block = block;
    }

    /**
     * @return Индивидуальный УП.
     */
    public EppCustomEduPlan getCustomEduPlan()
    {
        return _customEduPlan;
    }

    /**
     * @param customEduPlan Индивидуальный УП.
     */
    public void setCustomEduPlan(EppCustomEduPlan customEduPlan)
    {
        dirty(_customEduPlan, customEduPlan);
        _customEduPlan = customEduPlan;
    }

    /**
     * @return Дата утверждения.
     */
    public Date getConfirmDate()
    {
        return _confirmDate;
    }

    /**
     * @param confirmDate Дата утверждения.
     */
    public void setConfirmDate(Date confirmDate)
    {
        dirty(_confirmDate, confirmDate);
        _confirmDate = confirmDate;
    }

    /**
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppStudent2EduPlanVersionGen)
        {
            if (withNaturalIdProperties)
            {
                setStudent(((EppStudent2EduPlanVersion)another).getStudent());
                setEduPlanVersion(((EppStudent2EduPlanVersion)another).getEduPlanVersion());
            }
            setBlock(((EppStudent2EduPlanVersion)another).getBlock());
            setCustomEduPlan(((EppStudent2EduPlanVersion)another).getCustomEduPlan());
            setConfirmDate(((EppStudent2EduPlanVersion)another).getConfirmDate());
            setRemovalDate(((EppStudent2EduPlanVersion)another).getRemovalDate());
        }
    }

    public INaturalId<EppStudent2EduPlanVersionGen> getNaturalId()
    {
        return new NaturalId(getStudent(), getEduPlanVersion());
    }

    public static class NaturalId extends NaturalIdBase<EppStudent2EduPlanVersionGen>
    {
        private static final String PROXY_NAME = "EppStudent2EduPlanVersionNaturalProxy";

        private Long _student;
        private Long _eduPlanVersion;

        public NaturalId()
        {}

        public NaturalId(Student student, EppEduPlanVersion eduPlanVersion)
        {
            _student = ((IEntity) student).getId();
            _eduPlanVersion = ((IEntity) eduPlanVersion).getId();
        }

        public Long getStudent()
        {
            return _student;
        }

        public void setStudent(Long student)
        {
            _student = student;
        }

        public Long getEduPlanVersion()
        {
            return _eduPlanVersion;
        }

        public void setEduPlanVersion(Long eduPlanVersion)
        {
            _eduPlanVersion = eduPlanVersion;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppStudent2EduPlanVersionGen.NaturalId) ) return false;

            EppStudent2EduPlanVersionGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudent(), that.getStudent()) ) return false;
            if( !equals(getEduPlanVersion(), that.getEduPlanVersion()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudent());
            result = hashCode(result, getEduPlanVersion());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudent());
            sb.append("/");
            sb.append(getEduPlanVersion());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStudent2EduPlanVersionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStudent2EduPlanVersion.class;
        }

        public T newInstance()
        {
            return (T) new EppStudent2EduPlanVersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
                case "block":
                    return obj.getBlock();
                case "customEduPlan":
                    return obj.getCustomEduPlan();
                case "confirmDate":
                    return obj.getConfirmDate();
                case "removalDate":
                    return obj.getRemovalDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
                case "block":
                    obj.setBlock((EppEduPlanVersionBlock) value);
                    return;
                case "customEduPlan":
                    obj.setCustomEduPlan((EppCustomEduPlan) value);
                    return;
                case "confirmDate":
                    obj.setConfirmDate((Date) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "eduPlanVersion":
                        return true;
                case "block":
                        return true;
                case "customEduPlan":
                        return true;
                case "confirmDate":
                        return true;
                case "removalDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "eduPlanVersion":
                    return true;
                case "block":
                    return true;
                case "customEduPlan":
                    return true;
                case "confirmDate":
                    return true;
                case "removalDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
                case "block":
                    return EppEduPlanVersionBlock.class;
                case "customEduPlan":
                    return EppCustomEduPlan.class;
                case "confirmDate":
                    return Date.class;
                case "removalDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStudent2EduPlanVersion> _dslPath = new Path<EppStudent2EduPlanVersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStudent2EduPlanVersion");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Версия УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    /**
     * @return Блок УП(в).
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion#getBlock()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
    {
        return _dslPath.block();
    }

    /**
     * @return Индивидуальный УП.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion#getCustomEduPlan()
     */
    public static EppCustomEduPlan.Path<EppCustomEduPlan> customEduPlan()
    {
        return _dslPath.customEduPlan();
    }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion#getConfirmDate()
     */
    public static PropertyPath<Date> confirmDate()
    {
        return _dslPath.confirmDate();
    }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    public static class Path<E extends EppStudent2EduPlanVersion> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _block;
        private EppCustomEduPlan.Path<EppCustomEduPlan> _customEduPlan;
        private PropertyPath<Date> _confirmDate;
        private PropertyPath<Date> _removalDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Версия УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

    /**
     * @return Блок УП(в).
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion#getBlock()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
        {
            if(_block == null )
                _block = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK, this);
            return _block;
        }

    /**
     * @return Индивидуальный УП.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion#getCustomEduPlan()
     */
        public EppCustomEduPlan.Path<EppCustomEduPlan> customEduPlan()
        {
            if(_customEduPlan == null )
                _customEduPlan = new EppCustomEduPlan.Path<EppCustomEduPlan>(L_CUSTOM_EDU_PLAN, this);
            return _customEduPlan;
        }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion#getConfirmDate()
     */
        public PropertyPath<Date> confirmDate()
        {
            if(_confirmDate == null )
                _confirmDate = new PropertyPath<Date>(EppStudent2EduPlanVersionGen.P_CONFIRM_DATE, this);
            return _confirmDate;
        }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(EppStudent2EduPlanVersionGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

        public Class getEntityClass()
        {
            return EppStudent2EduPlanVersion.class;
        }

        public String getEntityName()
        {
            return "eppStudent2EduPlanVersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
