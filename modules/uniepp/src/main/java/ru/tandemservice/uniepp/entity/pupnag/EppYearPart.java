package ru.tandemservice.uniepp.entity.pupnag;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearPartGen;

import java.util.Comparator;

/**
 * Часть года (в учебном году)
 */
public class EppYearPart extends EppYearPartGen implements ITitled
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId) {
        return new EntityComboDataSourceHandler(ownerId, EppYearPart.class)
                .filter(EppYearPart.year().title())
                .filter(EppYearPart.part().title())
                .order(EppYearPart.year().educationYear().intValue())
                .order(EppYearPart.part().yearDistribution().amount())
                .order(EppYearPart.part().number())
                .titleProperty(EppYearPartGen.P_TITLE);
    }

    public static final Comparator<EppYearPart> COMPARATOR = (o1, o2) -> {
        int i;
        if (0 != (i = o1.getYear().compareTo(o2.getYear()))) { return i; }
        if (0 != (i = o1.getPart().compareTo(o2.getPart()))) { return i; }
        return 0;
    };

    public EppYearPart() {}

    public EppYearPart(final EppYearEducationProcess year, final YearDistributionPart part) {
        this.setYear(year);
        this.setPart(part);
    }

    @Override
    @EntityDSLSupport(parts = {
            EppYearPart.L_YEAR + "." + EppYearEducationProcess.L_EDUCATION_YEAR + "." + EducationYear.P_INT_VALUE,
            EppYearPart.L_PART + "." + YearDistributionPart.P_NUMBER
    })
    public String getTitle() {
        if (getPart() == null) {
            return this.getClass().getSimpleName();
        }
        return this.getPart().getTitle() + " " + this.getYear().getTitle();
    }
}