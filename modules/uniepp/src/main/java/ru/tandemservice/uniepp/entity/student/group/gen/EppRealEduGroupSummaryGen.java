package ru.tandemservice.uniepp.entity.student.group.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводка УГС
 *
 * Ведет фактический учет студентов (реальных студентов) в рамках года и чего части на подразделении
 * (не версионируется)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRealEduGroupSummaryGen extends EntityBase
 implements INaturalIdentifiable<EppRealEduGroupSummaryGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary";
    public static final String ENTITY_NAME = "eppRealEduGroupSummary";
    public static final int VERSION_HASH = -1028495576;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String L_YEAR_PART = "yearPart";
    public static final String P_MODIFICATION_DATE = "modificationDate";

    private OrgUnit _owner;     // Диспетчерская
    private EppYearPart _yearPart;     // Часть учебного года
    private Date _modificationDate;     // Дата изменения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Подразделение, ответственное за распределение аудиторного фонда и сведение общего расписания (как правило - ВУЗ в целом, либо его территориальное подразделение)
     *
     * @return Диспетчерская. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Диспетчерская. Свойство не может быть null.
     */
    public void setOwner(OrgUnit owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     */
    @NotNull
    public EppYearPart getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть учебного года. Свойство не может быть null.
     */
    public void setYearPart(EppYearPart yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getModificationDate()
    {
        return _modificationDate;
    }

    /**
     * @param modificationDate Дата изменения. Свойство не может быть null.
     */
    public void setModificationDate(Date modificationDate)
    {
        dirty(_modificationDate, modificationDate);
        _modificationDate = modificationDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRealEduGroupSummaryGen)
        {
            if (withNaturalIdProperties)
            {
                setOwner(((EppRealEduGroupSummary)another).getOwner());
                setYearPart(((EppRealEduGroupSummary)another).getYearPart());
            }
            setModificationDate(((EppRealEduGroupSummary)another).getModificationDate());
        }
    }

    public INaturalId<EppRealEduGroupSummaryGen> getNaturalId()
    {
        return new NaturalId(getOwner(), getYearPart());
    }

    public static class NaturalId extends NaturalIdBase<EppRealEduGroupSummaryGen>
    {
        private static final String PROXY_NAME = "EppRealEduGroupSummaryNaturalProxy";

        private Long _owner;
        private Long _yearPart;

        public NaturalId()
        {}

        public NaturalId(OrgUnit owner, EppYearPart yearPart)
        {
            _owner = ((IEntity) owner).getId();
            _yearPart = ((IEntity) yearPart).getId();
        }

        public Long getOwner()
        {
            return _owner;
        }

        public void setOwner(Long owner)
        {
            _owner = owner;
        }

        public Long getYearPart()
        {
            return _yearPart;
        }

        public void setYearPart(Long yearPart)
        {
            _yearPart = yearPart;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppRealEduGroupSummaryGen.NaturalId) ) return false;

            EppRealEduGroupSummaryGen.NaturalId that = (NaturalId) o;

            if( !equals(getOwner(), that.getOwner()) ) return false;
            if( !equals(getYearPart(), that.getYearPart()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOwner());
            result = hashCode(result, getYearPart());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOwner());
            sb.append("/");
            sb.append(getYearPart());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRealEduGroupSummaryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRealEduGroupSummary.class;
        }

        public T newInstance()
        {
            return (T) new EppRealEduGroupSummary();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "yearPart":
                    return obj.getYearPart();
                case "modificationDate":
                    return obj.getModificationDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((OrgUnit) value);
                    return;
                case "yearPart":
                    obj.setYearPart((EppYearPart) value);
                    return;
                case "modificationDate":
                    obj.setModificationDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "yearPart":
                        return true;
                case "modificationDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "yearPart":
                    return true;
                case "modificationDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return OrgUnit.class;
                case "yearPart":
                    return EppYearPart.class;
                case "modificationDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRealEduGroupSummary> _dslPath = new Path<EppRealEduGroupSummary>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRealEduGroupSummary");
    }
            

    /**
     * Подразделение, ответственное за распределение аудиторного фонда и сведение общего расписания (как правило - ВУЗ в целом, либо его территориальное подразделение)
     *
     * @return Диспетчерская. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary#getOwner()
     */
    public static OrgUnit.Path<OrgUnit> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary#getYearPart()
     */
    public static EppYearPart.Path<EppYearPart> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary#getModificationDate()
     */
    public static PropertyPath<Date> modificationDate()
    {
        return _dslPath.modificationDate();
    }

    public static class Path<E extends EppRealEduGroupSummary> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _owner;
        private EppYearPart.Path<EppYearPart> _yearPart;
        private PropertyPath<Date> _modificationDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Подразделение, ответственное за распределение аудиторного фонда и сведение общего расписания (как правило - ВУЗ в целом, либо его территориальное подразделение)
     *
     * @return Диспетчерская. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary#getOwner()
     */
        public OrgUnit.Path<OrgUnit> owner()
        {
            if(_owner == null )
                _owner = new OrgUnit.Path<OrgUnit>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary#getYearPart()
     */
        public EppYearPart.Path<EppYearPart> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new EppYearPart.Path<EppYearPart>(L_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary#getModificationDate()
     */
        public PropertyPath<Date> modificationDate()
        {
            if(_modificationDate == null )
                _modificationDate = new PropertyPath<Date>(EppRealEduGroupSummaryGen.P_MODIFICATION_DATE, this);
            return _modificationDate;
        }

        public Class getEntityClass()
        {
            return EppRealEduGroupSummary.class;
        }

        public String getEntityName()
        {
            return "eppRealEduGroupSummary";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
