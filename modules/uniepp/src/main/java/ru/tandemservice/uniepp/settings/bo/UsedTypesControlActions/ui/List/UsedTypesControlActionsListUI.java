package ru.tandemservice.uniepp.settings.bo.UsedTypesControlActions.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uniepp.settings.bo.UsedTypesControlActions.UsedTypesControlActionsManager;

/**
 * @author avedernikov
 * @since 03.09.2015
 */

public class UsedTypesControlActionsListUI extends UIPresenter
{

	public void onEnableType()
	{
		UsedTypesControlActionsManager.instance().modifyDAO().doEnable(getListenerParameterAsLong());
	}

	public void onDisableType()
	{
		UsedTypesControlActionsManager.instance().modifyDAO().doDisable(getListenerParameterAsLong());
	}

	public void onToggledActiveInEduPlan()
	{
		UsedTypesControlActionsManager.instance().modifyDAO().doToggleActiveInEduPlan(getListenerParameterAsLong());
	}

	public void onToggledUsedWithDisciplines()
	{
		UsedTypesControlActionsManager.instance().modifyDAO().doToggleUsedWithDisciplines(getListenerParameterAsLong());
	}

	public void onToggledUsedWithPractice()
	{
		UsedTypesControlActionsManager.instance().modifyDAO().doToggleUsedWithPractice(getListenerParameterAsLong());
	}

	public void onToggledUsedWithAttestation()
	{
		UsedTypesControlActionsManager.instance().modifyDAO().doToggleUsedWithAttestation(getListenerParameterAsLong());
	}
}
