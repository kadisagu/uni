package ru.tandemservice.uniepp.component.edustd.EduStdBlockAddEdit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniepp.dao.eduStd.IEppEduStdDAO;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import ru.tandemservice.uniepp.entity.std.gen.EppStateEduStandardBlockGen;

public class DAO extends UniDao<Model> implements IDAO
{
    protected static final EducationLevels educationLevel = null;

    @Override
    public void prepare(final Model model)
    {
        model.setElement(this.getNotNull(EppStateEduStandard.class, model.getId()));
        List<EduProgramSpecialization> specList = DataAccessServices.dao().getList(EduProgramSpecialization.class, EduProgramSpecialization.programSubject(), model.getElement().getProgramSubject(), EduProgramSpecialization.title().s());
        Collections.sort(specList);
        model.setSpecModel(new LazySimpleSelectModel<EduProgramSpecialization>(specList, EduProgramSpecialization.displayableTitle().s()));

        if (model.getValues() == null) {
            model.setValues(new ArrayList<EduProgramSpecialization>());
            for (EppStateEduStandardBlock block : DataAccessServices.dao().getList(EppStateEduStandardBlock.class, EppStateEduStandardBlock.stateEduStandard(), model.getElement(), EppStateEduStandardBlock.programSpecialization().title().s())) {
                if (block.getProgramSpecialization() != null) {
                    model.getValues().add(block.getProgramSpecialization());
                }
            }
        }
    }

    @Override
    public void update(final Model model)
    {
        final List<EduProgramSpecialization> eduLevList = model.getValues();
        IEppEduStdDAO.instance.get().doUpdateBlockList(model.getId(), eduLevList);
    }

}
