package ru.tandemservice.uniepp.entity.catalog_ctr.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Шаблон документа, используемый в договорах на обучение"
 * Имя сущности : eppCtrDocTemplate
 * Файл data.xml : epp-ctr-catalogs.data.xml
 */
public interface EppCtrDocTemplateCodes
{
    /** Константа кода (code) элемента : Извещение для договора на обучение (title) */
    String IZVETSHENIE_DLYA_DOGOVORA_NA_OBUCHENIE = "epp.ctr.paymentorder";

    Set<String> CODES = ImmutableSet.of(IZVETSHENIE_DLYA_DOGOVORA_NA_OBUCHENIE);
}
