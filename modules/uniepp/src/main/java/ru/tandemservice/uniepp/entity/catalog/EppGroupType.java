package ru.tandemservice.uniepp.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uniepp.entity.catalog.gen.EppGroupTypeGen;

import java.util.Comparator;

/** @see ru.tandemservice.uniepp.entity.catalog.gen.EppGroupTypeGen */
public abstract class EppGroupType extends EppGroupTypeGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
    {
        return new EntityComboDataSourceHandler(name, EppGroupType.class)
                .titleProperty(EppGroupType.title().s())
                .filter(EppGroupType.title())
                .order(EppGroupType.priority());
    }

    public static final Comparator<EppGroupType> COMPARATOR = ((o1, o2) -> Integer.compare(o1.getPriority(), o2.getPriority()));
}