/* $Id$ */
package ru.tandemservice.uniepp.remoteDocument.ext.RemoteDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.remoteDocument.bo.RemoteDocument.RemoteDocumentManager;

import java.util.Arrays;
import java.util.List;

/**
 * @author azhebko
 * @since 29.01.2015
 */
@Configuration
public class RemoteDocumentExtManager extends BusinessObjectExtensionManager
{
    public static final String REMOTE_DOCUMENT_MODULE = "uniepp";

    // Задачи
    public static final String EPP_REGISTRY_WORK_PROGRAM_ANNOTATION_SERVICE_NAME = "eppRegistryWorkProgramAnnotation";

    @Autowired
    private RemoteDocumentManager _remoteDocumentManager;

    @Bean
    public ItemListExtension<List<String>> remoteDocumentModuleExtension()
    {
        return itemListExtension(_remoteDocumentManager.moduleExtPoint())
                .add(EPP_REGISTRY_WORK_PROGRAM_ANNOTATION_SERVICE_NAME, Arrays.asList(REMOTE_DOCUMENT_MODULE))
                .create();
    }

    @Bean
    @SuppressWarnings("unchecked")
    public ItemListExtension<String> remoteDocumentTaskExtension()
    {
        return itemListExtension(_remoteDocumentManager.tasksExtPoint())
            .add(EPP_REGISTRY_WORK_PROGRAM_ANNOTATION_SERVICE_NAME, "Файлы и аннотации рабочих программ элементов реестра")
            .create();
    }
}