/* $Id:$ */
package ru.tandemservice.uniepp.component.indicators.EppRegistryCheck;

import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.UniBaseDao;

/**
 * @author oleyba
 * @since 5/30/12
 */
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=8945678")
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (model.getOrgUnitHolder().getId() != null) {
            model.getOrgUnitHolder().refresh();
            model.setSec(new OrgUnitSecModel(model.getOrgUnit()));
        }
    }
}
