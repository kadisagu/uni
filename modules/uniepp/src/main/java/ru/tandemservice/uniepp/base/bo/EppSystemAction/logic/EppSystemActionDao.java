/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppSystemAction.logic;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.meta.entity.IRelationMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractRole;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.dao.eduStd.IEppEduStdDataDAO;
import ru.tandemservice.uniepp.dao.eduStd.data.IEppStdRowWrapper;
import ru.tandemservice.uniepp.dao.eduStd.data.IEppStdWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;
import ru.tandemservice.uniepp.entity.settings.EppPlanOrgUnit;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public class EppSystemActionDao extends UniBaseDao implements IEppSystemActionDao
{
    @Override
    public RtfDocument doGetFileNames4ImportToMdb()
    {
        InputStream in = UniBaseUtils.class.getClassLoader().getResourceAsStream("uniepp/templates/import/fileNames.rtf");
        byte[] templateContent = null;
        try
        {
            templateContent = IOUtils.toByteArray(in);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        RtfDocument rtfDocument = new RtfReader().read(templateContent).getClone();
        RtfTableModifier blockTableModifier = new RtfTableModifier();
        List<String[]> blockDataLines = new ArrayList<>();

        List<EppEduPlanVersionBlock> blocks = getList(EppEduPlanVersionBlock.class);
        // todo DEV-6000
        for (EppEduPlanVersionBlock block : blocks)
            blockDataLines.add(new String[]{block.getEduPlanVersion().getFullTitle(), block.getEducationElementSimpleTitle(),  Long.toHexString(block.getEduPlanVersion().getId()), Long.toHexString(block.getId()), Long.toHexString(block.getEduPlanVersion().getId()) + "-" + Long.toHexString(block.getId()) + ".xml"});

        blockTableModifier.put("T", blockDataLines.toArray(new String[][]{}));
        blockTableModifier.modify(rtfDocument);
        return rtfDocument;
    }

    @Override
    public void doCorrectEduGroupLevels()
    {
        final Object[] levels = new Long[3];
        for (int i = 0; i<3; i++) {
            levels[i] = this.get(EppRealEduGroupCompleteLevel.class, EppRealEduGroupCompleteLevel.code(), ""+(i+1)).getId();
        }
        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppRealEduGroup.class, "rel")
                .where(in(property("rel", EppRealEduGroup.L_LEVEL), levels))
                .column("rel");
        final List<EppRealEduGroup> groups = dql.createStatement(this.getSession()).list();
        final EppRealEduGroupCompleteLevel[] newLevels = new EppRealEduGroupCompleteLevel[3];
        for (int i = 0; i<3; i++) {
            newLevels[i] = this.get(EppRealEduGroupCompleteLevel.class, EppRealEduGroupCompleteLevel.code(), ""+(i+1)+".1");
        }
        for (final EppRealEduGroup group : groups)
        {
            group.setLevel(newLevels[Integer.valueOf(group.getLevel().getCode())-1]);
            this.save(group);
            this.getSession().flush();
        }
    }

    @Override
    public void doShowEppForTutorOrgUnits()
    {
        final HashSet<OrgUnit> orgUnits = new HashSet<>();
        final List<EducationLevelsHighSchool> educationLevelsHighSchools =  this.getCatalogItemList(EducationLevelsHighSchool.class);
        for (final EducationLevelsHighSchool educationLevelsHighSchool : educationLevelsHighSchools)
        {
            orgUnits.add(educationLevelsHighSchool.getOrgUnit());
        }
        for (final OrgUnit orgUnit : orgUnits)
        {
            final List<EppPlanOrgUnit> planList = this.getList(EppPlanOrgUnit.class, EppPlanOrgUnit.orgUnit().id().s(), orgUnit.getId());

            if (planList.isEmpty())
            {
                final EppPlanOrgUnit eppOrgUnit = new EppPlanOrgUnit();
                eppOrgUnit.setOrgUnit(orgUnit);
                this.save(eppOrgUnit);
            }
        }
    }
    @Override
    public void doMergeRegistryElements(final Collection<Map.Entry<Long, Collection<Long>>> entrySet)
    {
        final Session session = this.getSession();
        for (final Map.Entry<Long, Collection<Long>> e: entrySet) {
            final Long targetId = e.getKey();
            final EppRegistryElement row = (EppRegistryElement)session.get(EppRegistryElement.class, targetId);

            {
                final Map<Integer, EppRegistryElementPart> partMap = new HashMap<>();
                for (final EppRegistryElementPart p: this.getList(EppRegistryElementPart.class, EppRegistryElementPart.registryElement(), row)) {
                    partMap.put(p.getNumber(), p);
                }

                BatchUtils.execute(e.getValue(), 256, new BatchUtils.Action<Long>()
                    {
                    @Override
                    public void execute(final Collection<Long> sourceIds)
                    {
                        for (final EppRegistryElementPart p : getList(EppRegistryElementPart.class, EppRegistryElementPart.registryElement().id(), sourceIds))
                        {
                            EppRegistryElementPart part = partMap.get(p.getNumber());
                            if (null == part)
                            {
                                partMap.put(p.getNumber(), part = new EppRegistryElementPart(row, p.getNumber()));
                                session.save(part);
                                session.flush();
                            }

                            for (final IRelationMeta relation : EntityRuntime.getMeta(EppRegistryElementPart.class).getIncomingRelations())
                            {
                                if (relation.getForeignEntity().getEntityClass().equals(EppRegistryElementPartFControlAction.class))
                                {
                                    continue;
                                }
                                if (relation.getForeignEntity().getEntityClass().equals(EppRegistryElementPartModule.class))
                                {
                                    continue;
                                }
                                final DQLUpdateBuilder dql = new DQLUpdateBuilder(relation.getForeignEntity().getEntityClass());
                                dql.set(relation.getForeignPropertyName(), value(part));
                                dql.where(eq(property(relation.getForeignPropertyName()), value(p)));
                                final int updated = dql.createStatement(session).execute();
                                if (updated > 0)
                                {
                                    Debug.message(String.valueOf(relation.getForeignEntity() + "." + relation.getForeignPropertyName() + ": " + updated));
                                }
                            }
                        }
                    }
                    });

                if (partMap.size() > 0) {
                    row.setParts(Math.max(row.getParts(), Collections.max(new ArrayList<>(partMap.keySet()))));
                    session.saveOrUpdate(row);
                }
                session.flush();
            }

            BatchUtils.execute(e.getValue(), 256, new BatchUtils.Action<Long>() {
                @Override public void execute(final Collection<Long> sourceIds)
                {
                    {
                        final DQLUpdateBuilder dql = new DQLUpdateBuilder(EppEpvRegistryRow.class);
                        dql.set("registryElement", value(row));
                        dql.where(in(property("registryElement.id"), sourceIds));
                        final int updated = dql.createStatement(session).execute();
                        if (updated > 0) {
                            Debug.message(String.valueOf(EppEpvRegistryRow.class.getName()+": "+updated));
                        }
                    }

                    final DQLDeleteBuilder dql = new DQLDeleteBuilder(EntityRuntime.getMeta(targetId).getEntityClass());
                    dql.where(in(property(EntityBase.id().s()), sourceIds));
                    dql.createStatement(session).execute();
                }
            });

            session.flush();
            session.clear();
        }
    }

    @Override
    public Map<Long, Collection<Long>> getProcessMap(final Long ownerId) {
        final Session session = this.getSession();
        final Map<Long, Collection<Long>> processMap = new HashMap<>();

        final MQBuilder builder = new MQBuilder(EppRegistryElementGen.ENTITY_CLASS, "e", new String[] { "id" });
        builder.add(MQExpression.eq("e", EppRegistryElementGen.owner().id().s(), ownerId));
        // builder.add(MQExpression.eq("e", EppRegistryElementGen.state().code().toString(), EppState.STATE_FORMATIVE));

        final List<Long> ids = builder.getResultList(session);
        if (ids.isEmpty()) { return null; }

        this.logger.info(ownerId+":"+ids.size()+", "+ Debug.getRootSection().getQueryCount());
        final Map<String, Long> uniqueMap = new HashMap<>();
        BatchUtils.execute(ids, 128, new BatchUtils.Action<Long>() {
            @Override public void execute(final Collection<Long> ids)
            {
                final Map<Long, Map<String, EppRegistryElementLoad>> loadMap = IEppRegistryDAO.instance.get().getRegistryElementTotalLoadMap(ids);
                final List<EppRegistryElement> elements = new MQBuilder(EppRegistryElementGen.ENTITY_CLASS, "e").add(MQExpression.in("e", "id", ids)).getResultList(session);

                for (final EppRegistryElement e: elements) {
                    final StringBuilder sb = new StringBuilder(e.getDescriptionString());

                    final Map<String, EppRegistryElementLoad> localLoadMap = loadMap.get(e.getId());
                    if ((null != localLoadMap) && (localLoadMap.size() > 0)) {
                        final List<EppRegistryElementLoad> values = new ArrayList<>(localLoadMap.values());
                        Collections.sort(values, EppRegistryElementLoad.BY_TYPE_COMPARATOR);
                        for (final EppRegistryElementLoad load : values) {
                            if (load.getLoad() > 0) {
                                sb.append('\n').append(load.getLoadType().getFullCode()).append(':').append(load.getLoad());
                            }
                        }
                    }

                    final String key = sb.toString().toLowerCase();
                    final Long targetId = uniqueMap.get(key);
                    if (null == targetId) {
                        uniqueMap.put(key, e.getId());
                    } else {
                        SafeMap.safeGet(processMap, targetId, ArrayList.class).add(e.getId());
                    }
                }

                elements.clear();
                loadMap.clear();
                session.clear();
            }
        });
        uniqueMap.clear();
        return processMap;
    }


    //    @Override
    //    public void doImportEduStdBlockFromEduPlan()
    //    {
    //        final Session session = this.getSession();
    //
    //        // создание блоков ГОС
    //        // берем ранее созданные блоки
    //        final List<EppStateEduStandardBlock> oldBlockList = this.getList(EppStateEduStandardBlock.class);
    //        final Set<INaturalId<EppStateEduStandardBlockGen>> oldBlockSet = new HashSet<INaturalId<EppStateEduStandardBlockGen>>();
    //        for (final EppStateEduStandardBlock block : oldBlockList)
    //        {
    //            oldBlockSet.add(block.getNaturalId());
    //        }
    //
    //        final MQBuilder eduLevelBuilder = new MQBuilder(EppEduPlanVersionBlockGen.ENTITY_NAME, "epb", new String[] { EntityBase.id().s(), EppEduPlanVersionBlockGen.educationLevelHighSchool().educationLevel().id().s() });
    //        eduLevelBuilder.addDomain("std", EppStateEduStandardGen.ENTITY_NAME);
    //        eduLevelBuilder.addSelect("std", new String[] { EntityBase.id().s() });
    //        eduLevelBuilder.add(MQExpression.eqProperty("epb", EppEduPlanVersionBlockGen.eduPlanVersion().eduPlan().parent().id().s(), "std", EntityBase.id().s()));
    //        eduLevelBuilder.setNeedDistinct(true);
    //
    //        final List<Object[]> level2stdList = eduLevelBuilder.getResultList(session);
    //
    //        // мап id блока версии уп -> Naturalid блока гос (разным блокам в уп может соответствовать один блок в гос)
    //        final Map<Long, INaturalId<EppStateEduStandardBlockGen>> idPlanBlock2idStdBlock = new HashMap<Long, INaturalId<EppStateEduStandardBlockGen>>();
    //
    //        for (final Object[] row : level2stdList)
    //        {
    //            final Long versionBlockId = (Long)row[0];
    //            final Long eduLevelId = (Long)row[1];
    //            final Long sdtId = (Long)row[2];
    //
    //            final EducationLevels eduLevel = (EducationLevels)session.load(EducationLevels.class, eduLevelId);
    //            final EppStateEduStandard sdt = (EppStateEduStandard)session.load(EppStateEduStandard.class, sdtId);
    //
    //            final EppStateEduStandardBlock block = new EppStateEduStandardBlock();
    //            block.setEducationLevel(eduLevel);
    //            block.setStateEduStandard(sdt);
    //            idPlanBlock2idStdBlock.put(versionBlockId, block.getNaturalId());
    //
    //            if (oldBlockSet.add(block.getNaturalId()))
    //            {
    //                session.saveOrUpdate(block);
    //            }
    //        }
    //
    //        session.flush();
    //
    //        // создание строк ГОС
    //
    //        // мап блоков гос: id -> EppStateEduStandardBlock
    //        final Map<INaturalId<EppStateEduStandardBlockGen>, EppStateEduStandardBlock> blockMap = new HashMap<INaturalId<EppStateEduStandardBlockGen>, EppStateEduStandardBlock>();
    //        for (final EppStateEduStandardBlock block : this.getList(EppStateEduStandardBlock.class))
    //        {
    //            blockMap.put(block.getNaturalId(), block);
    //        }
    //
    //        // идентификаторы уп для котрых есть госы
    //        final MQBuilder eduPlanIdsBuilder = new MQBuilder(EppEduPlanVersionBlockGen.ENTITY_NAME, "epb", new String[] { EppEduPlanVersionBlockGen.eduPlanVersion().id().s() });
    //        eduPlanIdsBuilder.addDomain("std", EppStateEduStandardGen.ENTITY_NAME);
    //        eduPlanIdsBuilder.add(MQExpression.eqProperty("epb", EppEduPlanVersionBlockGen.eduPlanVersion().eduPlan().parent().id().s(), "std", EntityBase.id().s()));
    //        eduPlanIdsBuilder.setNeedDistinct(true);
    //        final List<Long> eduPlanIdsList = eduPlanIdsBuilder.getResultList(session);
    //
    //        // { epv-row.hierarchical-path -> std-row }
    //        final Map<String, IEppStdRow> path2stdRow = new LinkedHashMap<String, IEppStdRow>();
    //
    //        final Map<Long, IEppEpvWrapper> idVersion2row = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionDataMap(eduPlanIdsList, false);
    //        for (final IEppEpvWrapper wrapper : idVersion2row.values())
    //        {
    //            for (final IEppEpvRowWrapper eppEpvrow : wrapper.getRowMap().values())
    //            {
    //                final IEppEpvRow row = eppEpvrow.getRow();
    //                final INaturalId<EppStateEduStandardBlockGen> key = idPlanBlock2idStdBlock.get(row.getOwner().getId());
    //                final EppStateEduStandardBlock owner = blockMap.get(key);
    //
    //                this.fillIEppStdRow(eppEpvrow, path2stdRow, owner);
    //            }
    //
    //            // проставляем парентов
    //            for (final IEppEpvRowWrapper eppEpvrow : wrapper.getRowMap().values())
    //            {
    //                if (null != eppEpvrow.getHierarhyParent())
    //                {
    //                    final IEppEpvRow row = eppEpvrow.getRow();
    //                    final INaturalId<EppStateEduStandardBlockGen> key = idPlanBlock2idStdBlock.get(row.getOwner().getId());
    //                    final EppStateEduStandardBlock owner = blockMap.get(key);
    //
    //                    final EppStdRow stdRow = (EppStdRow)path2stdRow.get(owner.getId().toString() + eppEpvrow.getHierarhyPath());
    //                    if (null != stdRow)
    //                    {
    //                        final IEppEpvRowWrapper parentRow = eppEpvrow.getHierarhyParent();
    //                        try {
    //                            stdRow.setHierarhyParent(null == parentRow ? null : ((EppStdRow)path2stdRow.get(owner.getId().toString() + parentRow.getHierarhyPath())));
    //                        } catch (final ClassCastException e) {
    //                            // just skip it
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        // сохраняет это
    //        for (final IEppStdRow row : path2stdRow.values())
    //        {
    //            session.save(row);
    //        }
    //    }
    //
    //    private void fillIEppStdRow(final IEppEpvRowWrapper eppEpvrow, final Map<String, IEppStdRow> path2stdRow, final EppStateEduStandardBlock owner)
    //    {
    //        final String path = owner.getId().toString() + eppEpvrow.getHierarhyPath();
    //        if (path2stdRow.containsKey(path))
    //        {
    //            return;
    //        }
    //        final IEppEpvRow row = eppEpvrow.getRow();
    //        final String title = row.getTitle();
    //
    //        if (row instanceof EppEpvDisciplineTopRow)
    //        {
    //            final EppStdDisciplineTopRow stdRow = new EppStdDisciplineTopRow();
    //            stdRow.setType(((EppEpvDisciplineTopRow)row).getRegistryElement().getParent());
    //            stdRow.setNumber(((EppEpvDisciplineTopRow)row).getNumber());
    //            stdRow.setTitle(title);
    //            stdRow.setOwner(owner);
    //
    //            path2stdRow.put(path, stdRow);
    //            return;
    //        }
    //        if (row instanceof EppEpvDisciplineNestedRow)
    //        {
    //            final EppStdDisciplineNestedRow stdRow = new EppStdDisciplineNestedRow();
    //            stdRow.setType(((EppEpvDisciplineNestedRow)row).getRegistryElement().getParent());
    //            stdRow.setNumber(((EppEpvDisciplineNestedRow)row).getNumber());
    //            stdRow.setOwner(owner);
    //            stdRow.setTitle(title);
    //
    //            path2stdRow.put(path, stdRow);
    //            return;
    //        }
    //        if (row instanceof EppEpvGroupReRow)
    //        {
    //            final EppStdGroupReRow stdRow = new EppStdGroupReRow();
    //            stdRow.setNumber(((EppEpvGroupRow)row).getNumber());
    //            stdRow.setOwner(owner);
    //            stdRow.setTitle(title);
    //
    //            path2stdRow.put(path, stdRow);
    //            return;
    //        }
    //        if (row instanceof EppEpvGroupImRow)
    //        {
    //            final EppStdGroupImRow stdRow = new EppStdGroupImRow();
    //            stdRow.setNumber(((EppEpvGroupRow)row).getNumber());
    //            stdRow.setSize(((EppEpvGroupImRow)row).getSize());
    //            stdRow.setOwner(owner);
    //            stdRow.setTitle(title);
    //
    //            path2stdRow.put(path, stdRow);
    //            return;
    //        }
    //        if (row instanceof EppEpvStructureRow)
    //        {
    //            final EppStdStructureRow stdRow = new EppStdStructureRow();
    //            stdRow.setOwner(owner);
    //            stdRow.setValue(((EppEpvStructureRow)row).getValue());
    //
    //            path2stdRow.put(path, stdRow);
    //            return;
    //        }
    //        if (row instanceof EppEpvActionRow)
    //        {
    //            final EppStdActionRow stdRow = new EppStdActionRow();
    //            stdRow.setType(((EppEpvActionRow)row).getRegistryElement().getParent());
    //            stdRow.setTitle(((EppEpvActionRow)row).getRegistryElement().getTitle());
    //            stdRow.setNumber(((EppEpvActionRow)row).getNumber());
    //            stdRow.setOwner(owner);
    //
    //            path2stdRow.put(path, stdRow);
    //            return;
    //        }
    //    }

    @Override
    public void doCreateSkills()
    {
        final Session session = this.getSession();

        final List<EppSkillGroup> group = this.getCatalogItemList(EppSkillGroup.class);

        final List<EppStateEduStandard> stdList = this.getList(EppStateEduStandard.class);

        final Map<Long, IEppStdWrapper> map = IEppEduStdDataDAO.instance.get().getEduStdDataMap(UniBaseDao.ids(stdList), false);

        for (final EppStateEduStandard std : stdList)
        {
            if (std.getGeneration().showCompetencies())
            {
                final Collection<IEppStdRowWrapper> wrapList = map.get(std.getId()).getRowMap().values();

                final EppStateEduStandardSkill skill = new EppStateEduStandardSkill();
                skill.setCode(String.valueOf(RandomUtils.nextInt()));
                skill.setParent(std);
                skill.setSkillGroup(group.get(1));

                session.save(skill);

                for (final IEppStdRowWrapper wrap : wrapList)
                {
                    final EppStdRow2SkillRelation rel = new EppStdRow2SkillRelation();
                    rel.setRow((EppStdRow)wrap.getRow());
                    rel.setSkill(skill);

                    session.save(rel);

                }
            }
        }
    }

    @Override
    public void doFixEduPlanNumbers() {
        final char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            final Session session = this.getSession();

            // обвобождаем номера
            new DQLUpdateBuilder(EppEduPlan.class)
            .set(EppEduPlan.P_NUMBER, DQL.parseExpression("number || '-tmp'"))
            .createStatement(session).execute();

            // освобождаем номера
            new DQLUpdateBuilder(EppEduPlanVersion.class)
            .set(EppEduPlanVersion.P_NUMBER, DQL.parseExpression("number || '-tmp'"))
            .createStatement(session).execute();


            final Set<String> uniqueNumbers = new HashSet<>(128);
            BatchUtils.execute(CommonDAO.ids(this.getList(EppEduPlan.class)), 100, new BatchUtils.Action<Long>() {
                int number = 1;
                @Override public void execute(final Collection<Long> ids) {
                    for (final EppEduPlan p: getList(EppEduPlan.class, "id", ids, "id")) {
                        {

                            String s = StringUtils.trimToEmpty(p.getNumber()).split(" ")[0];
                            try { s = String.valueOf(Integer.parseInt(s)); }
                            catch (final Throwable t) { s = String.valueOf(this.number++); }

                            String n = s;
                            int x = 0;
                            while (!uniqueNumbers.add(n)) {
                                int t = (x++);
                                n = "";
                                while (t > 0) {
                                    n = chars[t % chars.length] + n;
                                    t = t / chars.length;
                                }
                                n = s + n;
                            }

                            p.setComment(p.getComment() + "\n" + p.getNumber() + "\n" + p.getTitlePostfix());
                            p.setNumber(n);
                            p.setTitlePostfix("");
                            session.saveOrUpdate(p);
                        }

                        final Set<Integer> versionNumbers = new HashSet<>(8);
                        for (final EppEduPlanVersion v: getList(EppEduPlanVersion.class, EppEduPlanVersion.eduPlan(), p, "id")) {

                            int i = 1;

                            final String s = StringUtils.trimToEmpty(v.getNumber()).split(" ")[0];
                            if (s.length() < 5) {
                                try { i = Integer.parseInt(s); }
                                catch (final Throwable t) {/* nop */ }
                            }

                            while (!versionNumbers.add(i)) { i++; }
                            v.setComment(v.getComment() + "\n" + v.getNumber() + "\n" + v.getTitlePostfix());
                            v.setNumber(String.valueOf(i));
                            v.setTitlePostfix("");
                            session.saveOrUpdate(v);
                        }
                    }

                    session.flush();
                    session.clear();
                }
            });

            session.flush();

        } finally {
            eventLock.release();
        }
    }

    @Override
    public void doSetContractRoles()
    {
        final Session session = getSession();

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            for (CtrContractRole role : new CtrContractRole[] {getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER), getCatalogItem(CtrContractRole.class, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER)}) {

                Class<? extends CtrContractPromice> promiceClass = CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER.equals(role.getCode()) ? CtrPaymentPromice.class : EppCtrEducationPromice.class;

                DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(CtrContractVersionContractor.class, "c").column("c")
                .where(notExists(new DQLSelectBuilder()
                .fromEntity(CtrContractVersionContractorRole.class, "r")
                .where(eq(property(CtrContractVersionContractorRole.contactor().owner().fromAlias("r")), property(CtrContractVersionContractor.owner().fromAlias("c"))))
                .where(eq(property(CtrContractVersionContractorRole.role().fromAlias("r")), value(role)))
                .buildQuery()))
                .joinEntity("c", DQLJoinType.inner, promiceClass, "p", eq(property(CtrContractPromice.src().fromAlias("p")), property("c")))
                .order(property(CtrContractPromice.deadlineDate().fromAlias("p")))
                ;

                Map<CtrContractVersion, CtrContractVersionContractorRole> roleMap = new HashMap<>();
                for (CtrContractVersionContractor ctr : dql.createStatement(session).<CtrContractVersionContractor>list()) {
                    if (roleMap.containsKey(ctr.getOwner()))
                        continue;
                    roleMap.put(ctr.getOwner(), new CtrContractVersionContractorRole(ctr, role));
                }

                BatchUtils.execute(roleMap.values(), 256, new BatchUtils.Action<CtrContractVersionContractorRole>()
                    {
                    @Override
                    public void execute(Collection<CtrContractVersionContractorRole> elements)
                    {
                        for (CtrContractVersionContractorRole rel : elements)
                            session.save(rel);
                        session.flush();
                    }
                    });

            }
        } finally {
            eventLock.release();
        }
    }

    @Override
    public int moveWorkPlans2specializationBlock(Logger logger)
    {
        /* DEV-6296
           Обрабатываем все РУП в системе, которые ссылаются на корневой блок, меняя им блок следующим образом:
            - если в УПв есть только один блок специализации, то выбираем для РУП этот блок
            - если в УПв несколько блоков специализации, и среди них есть блок на общую направленность, выбираем этот блок
            - если в УПв несколько блоков специализации и среди них нет блока общей направленности, пропускаем такой РУП, в логе фиксируем номер РУП.
         */

        final List<Object[]> items = new DQLSelectBuilder().fromEntity(EppWorkPlan.class, "wp")
                .column("wp.id")
                .column("ab")
                .joinPath(DQLJoinType.inner, EppWorkPlan.parent().fromAlias("wp"), "b")
                .joinEntity("b", DQLJoinType.inner, EppEduPlanVersionBlock.class, "ab", eq(property("ab", EppEduPlanVersionBlock.L_EDU_PLAN_VERSION), property("b", EppEduPlanVersionBlock.L_EDU_PLAN_VERSION)))
                .where(instanceOf("b", EppEduPlanVersionRootBlock.class))
                .createStatement(getSession()).list();

        if (items.isEmpty())
            return 0;

        final Multimap<Long, EppEduPlanVersionSpecializationBlock> map = HashMultimap.create();
        final Map<Long, EppEduPlanVersionSpecializationBlock> rootSpecBlockMap = Maps.newHashMap();
        for(Object[] item : items)
        {
            if (!(item[1] instanceof EppEduPlanVersionSpecializationBlock))
            {
                // Базовые блоки пропускаем. В итоге, если в УПв всего один блок (базовый), то он и останется для РУПа
                continue;
            }

            final Long workPLanId = (Long) item[0];
            final EppEduPlanVersionSpecializationBlock block = (EppEduPlanVersionSpecializationBlock) item[1];

            map.put(workPLanId, block);

            if (block.getProgramSpecialization().isRootSpecialization())
            {
                if (rootSpecBlockMap.put(workPLanId, block) != null)
                    throw new IllegalStateException(); // В УПв оказалось более одного сблока с на общую направленность - так не должно быть!
            }
        }

        if (map.isEmpty())
            return 0;

        final IDQLStatement updateWorkPlanStatement = new DQLUpdateBuilder(EppWorkPlan.class)
                .set(EppWorkPlan.L_PARENT, parameter("parent", PropertyType.ANY))
                .where(eq(property("id"), parameter("wpId", PropertyType.LONG)))
                .createStatement(getSession());

        int fixedWorkPlansCount = 0;
        try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler()))
        {
            for (Map.Entry<Long, Collection<EppEduPlanVersionSpecializationBlock>> entry : map.asMap().entrySet())
            {
                final Long workPLanId = entry.getKey();
                final EppEduPlanVersionSpecializationBlock newBlock;
                if (entry.getValue().size() == 1)
                {
                    // В УПв всего один блок на направленность - берем его
                    newBlock = entry.getValue().iterator().next();
                }
                else
                {
                    newBlock = rootSpecBlockMap.get(workPLanId);
                    if (newBlock == null)
                    {
                        // Блока с общей направленностью в УПв не оказалось
                        logger.error("Edu plan version has no block for root specialization. Work plan id=" + workPLanId + " skiped.");
                        continue;
                    }
                }

                updateWorkPlanStatement.setParameter("parent", newBlock);
                updateWorkPlanStatement.setLong("wpId", workPLanId);
                updateWorkPlanStatement.execute();
                fixedWorkPlansCount++;
            }
        }
        return fixedWorkPlansCount;
    }
}
