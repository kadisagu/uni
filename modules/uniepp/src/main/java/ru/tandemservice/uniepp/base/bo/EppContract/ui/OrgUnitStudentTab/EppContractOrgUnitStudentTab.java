package ru.tandemservice.uniepp.base.bo.EppContract.ui.OrgUnitStudentTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.BusinessComponentManagerBase;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.EppContract.logic.EppContractOrgUnitStudentListHandler;

/**
 * @author vdanilov
 */
@Configuration
public class EppContractOrgUnitStudentTab extends BusinessComponentManager {

    public static final String STUDENT_LIST_DS = "studentListDS";

    public static final String STUDENT_CATEGORY_DS = "studentCategoryDS";
    public static final String STUDENT_STATUS_DS = "studentStatusDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(this.searchListDS(EppContractOrgUnitStudentTab.STUDENT_LIST_DS, this.studentListDSColumns(), this.studentListDSHandler()))
        .addDataSource(this.selectDS(EppContractOrgUnitStudentTab.STUDENT_CATEGORY_DS, this.studentCategoryDSHandler()).create())
        .addDataSource(this.selectDS(EppContractOrgUnitStudentTab.STUDENT_STATUS_DS, this.studentStatusDSHandler()).create())
        .addDataSource(UniStudentManger.instance().courseDSConfig())
        .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
        .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
        .addDataSource(EducationCatalogsManager.instance().developPeriodDSConfig())
        .create();
    }

    @Bean
    public ColumnListExtPoint studentListDSColumns() {
        return this.columnListExtPointBuilder(EppContractOrgUnitStudentTab.STUDENT_LIST_DS)
        .addColumn(BusinessComponentManagerBase.textColumn("personalNumber", Student.personalNumber()).width("1").order().create())
        .addColumn(BusinessComponentManagerBase.textColumn("bookNumber", Student.bookNumber()).width("1").order().create())
        .addColumn(BusinessComponentManagerBase.publisherColumn("fio", Student.person().fullFio()).publisherLinkResolver(new SimplePublisherLinkResolver(Student.id()).param("selectedStudentTab", "eppContractStudentTab")).order().create())
        .addColumn(BusinessComponentManagerBase.textColumn("course", Student.course().title()).width("1").order().create())
        .addColumn(BusinessComponentManagerBase.publisherColumn("group", Student.group().title()).publisherLinkResolver(new SimplePublisherLinkResolver(Student.group().id())).width("1").order().create())
        .addColumn(BusinessComponentManagerBase.textColumn("category", Student.studentCategory().title()).width("1").order().create())
        .addColumn(BusinessComponentManagerBase.textColumn("develop.orgUnits", Student.educationOrgUnit().developOrgUnitShortTitle()).order().create())
        .addColumn(BusinessComponentManagerBase.textColumn("develop.eduhs", Student.educationOrgUnit().educationLevelHighSchool().displayableTitle()).order().create())
        .addColumn(BusinessComponentManagerBase.textColumn("develop.combination", Student.educationOrgUnit().developCombinationTitle()).width("1").order().create())
        .addColumn(BusinessComponentManagerBase.textColumn("status", Student.status().title()).width("1").order().create())
        .addColumn(BusinessComponentManagerBase.blockColumn("contract", "contractBlock").create())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentListDSHandler() {
        return new EppContractOrgUnitStudentListHandler(this.getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentCategoryDSHandler() {
        return new EntityComboDataSourceHandler(this.getName(), StudentCategory.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentStatusDSHandler() {
        return new EntityComboDataSourceHandler(this.getName(), StudentStatus.class);
    }

}
