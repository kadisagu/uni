/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppState.util;

import ru.tandemservice.uniepp.entity.IEppStateObject;

import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 17.02.2016
 */
public interface IEppStateConfig
{
    /**
     * @return Возвращает мап возможных переходов между состояниями объекта
     */
    Map<String, List<String>> getPossibleTransitionsMap();

    /**
     * Проверка возможности перехода между состояниями объекта.
     *
     * @param fromStateCode исходное состояние
     * @param toStateCode новое состояние
     * @return true - есть возможность, иначе - нет.
     */
    boolean canChangeState(String fromStateCode, String toStateCode);

    /**
     * Возвращает коды состояний объекта, в которые доступен переход из текущего.
     *
     * @param stateObject объект
     * @return коды состояний
     */
    List<String> getPossibleTransitions(IEppStateObject stateObject);

    /**
     * Возвращает мап, где для каждого кода состояния объекта указан список состояний, из которых можно перейти в это состояние
     * @return мап состояний
     */
    Map<String, List<String>> getFromPossibleTransitions();
}
