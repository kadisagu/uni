package ru.tandemservice.uniepp.dao.eduplan;

import com.google.common.base.Predicates;
import com.google.common.collect.*;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dao.MergeAction.SessionMergeAction;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.shared.commonbase.utils.SharedDQLUtils;
import org.tandemframework.shared.sandbox.utils.FastBeanFunction;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvTotalRow;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.index.EppIndexedRowWrapper;
import ru.tandemservice.uniepp.dao.index.IEppIndexedRow;
import ru.tandemservice.uniepp.dao.index.IEppIndexedRowWrapper;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockContent.EppElectronicRowWrapper;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockContent.EppFakeRowWrapper;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockContent.EppInteractiveRowWrapper;
import ru.tandemservice.uniepp.entity.IEppNumberRow;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.data.*;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvRowGen;
import ru.tandemservice.uniepp.entity.settings.EppELoadWeekType;
import ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EppEduPlanVersionDataDAO extends UniBaseDao implements IEppEduPlanVersionDataDAO
{
    // for override

    protected EppEpvBlockWrapperLoader newEppEduPlanVersionWrapperLoader(boolean loadDetails) {
        return new EppEpvBlockWrapperLoader(getSession(), loadDetails);
    }

    // демон

    public static final SyncDaemon DAEMON = new SyncDaemon(EppEduPlanVersionDataDAO.class.getName(), 120) {
        @Override protected void initDaemon() {
            registerWakeUpListener4Class(EppEpvRow.class);
        }
        @Override protected void main() {
            IEppEduPlanVersionDataDAO dao = IEppEduPlanVersionDataDAO.instance.get();
            List<Long> epvIds = dao.getEpvIds4UpdateRowStoredIndex();
            // отдельная транзакция (чтобы кэш был отдельным на каждый УП(в))
            epvIds.forEach(dao::doUpdateEpvRowStoredIndex);
        }
    };

    // статика

    // обертки строк (для генерации индекса по алгоритмам)
    public static Map<EppEpvRow, IEppIndexedRowWrapper> buildIndexRuleRowWrapperMap(Collection<EppEpvRow> rowList)
    {
        final Map<EppEpvRow, IEppIndexedRowWrapper> wrapperMap = new LinkedHashMap<>(rowList.size());
        for (EppEpvRow row: rowList) {
            IEppIndexedRowWrapper wrapper = new EppIndexedRowWrapper<EppEpvRow>(row) {
                @Override public IEppIndexedRow getHierarhyParent() {
                    if (null == getRow().getHierarhyParent()) { return null; }
                    return wrapperMap.get(getRow().getHierarhyParent());
                }
                @Override protected String getIndexInternal() {
                    final String index = StringUtils.trimToNull(getRow().getOwner().getRule().getIndex(this));
                    if (null == index) { return "?"; }
                    return index;
                }
                @Override protected int getSequenceNumberInternal() {
                    return EppEpvBlockWrapper.getRecordHierarchyIndex(wrapperMap.keySet(), getRow());
                }
            };
            wrapperMap.put(row, wrapper);
        }
        return wrapperMap;
    }

    /**
     * Загружает все элементы, относящиеся к блоку (+всех их родителей, если требуется)
     * @param versionDataMap { row.id -> wrapper(row) }
     * @param predicate условие отбора
     * @param withParents включать в результат родительский элемент
     */
    public static <T extends IEppEpvRowWrapper> Collection<IEppEpvRowWrapper> filterEduPlanBlockRows(final Map<Long, T> versionDataMap, final Predicate<IEppEpvRowWrapper> predicate, final boolean withParents)
    {
        final Map<Long, IEppEpvRowWrapper> result = new LinkedHashMap<>();
        if (null != versionDataMap)
        {
            for (IEppEpvRowWrapper row : versionDataMap.values()) {
                if (predicate.evaluate(row)) {
                    if (withParents)
                    {
                        // добавляем элемент и всех его родителей
                        while ((null != row) && (null == result.put(row.getId(), row))) {
                            row = row.getHierarhyParent();
                        }
                    }
                    else
                    {
                        // добавляем только элемент
                        result.put(row.getId(), row);
                    }
                }
            }
        }
        return result.values();
    }

    // форма контроля по full-code
    public static Map<String, EppControlActionType> getActionTypeMap() {
        return UniDaoFacade.getCoreDao().getCalculatedValue(EppEduPlanVersionDataDAO.ACTION_MAP_CALLBACK);
    }

    // нагрузка по full-code
    public static Map<String, EppLoadType> getLoadTypeMap() {
        return UniDaoFacade.getCoreDao().getCalculatedValue(EppEduPlanVersionDataDAO.LOAD_MAP_CALLBACK);
    }

    // public

    @Override
    @SuppressWarnings("unchecked")
    public Map<Long, IEppEpvBlockWrapper> getEduPlanVersionBlockDataMap(Collection<Long> epvBlockIds, boolean loadDetails)
    {
        Debug.begin("EppEduPlanVersionDataDAO.getEduPlanVersionBlockDataMap(size="+epvBlockIds.size()+", loadDetails="+loadDetails+")");
        try
        {
            Debug.begin("preload");
            try
            {
                this.preload(EppPlanStructure.class);
                this.preload(EppRegistryStructure.class);
            }
            finally
            {
                Debug.end();
            }

            final EppEpvBlockWrapperLoader loader = newEppEduPlanVersionWrapperLoader(loadDetails);
            return DaoCacheFacade.getEntry(loader).getRecords(epvBlockIds);
        }
        finally
        {
            Debug.end();
        }
    }

    @Override
    public IEppEpvBlockWrapper getEduPlanVersionBlockData(Long epvBlockId, boolean loadDetails)
    {
        Map<Long, IEppEpvBlockWrapper> map = getEduPlanVersionBlockDataMap(ImmutableList.of(epvBlockId), loadDetails);
        return map.get(epvBlockId);
    }

    @Override
    public Map<Long, IEppEpvRowWrapper> getEduPlanVersionBlockRowMap(Long epvBlockId, boolean loadDetails)
    {
        IEppEpvBlockWrapper blockWrapper = getEduPlanVersionBlockData(epvBlockId, loadDetails);
        return blockWrapper.getRowMap();
    }

    @Override
    public IEppEpvRowWrapper getEpvRowWrapper(EppEpvRow row, boolean loadDetails)
    {
        return getEduPlanVersionBlockRowMap(row.getOwner().getId(), loadDetails).get(row.getId());
    }

    @Override
    public Stream<EppControlActionType> getActiveControlActionTypes(final EppRegistryStructure eppRegistryStructure)
    {
        return EppEduPlanVersionDataDAO.getActionTypeMap().values().stream()
                .filter(EppControlActionType::isActiveInEduPlan)
                .filter(EppControlActionType.getPredicate(eppRegistryStructure));
    }

    @Override
    public Map<String, Double> getRowLoad(Long rowId)
    {
        final Map<String, Double> detailMap = new HashMap<>();
        for (final EppEpvRowLoad detail : getList(EppEpvRowLoad.class, EppEpvRowLoad.row().id(), rowId)) {
            String fullCode = detail.getLoadType().getFullCode();
            detailMap.put(fullCode, detail.getHoursAsDouble());
            detailMap.put(EppALoadType.iFullCode(fullCode), EppEpvRowLoad.wrap(detail.getHoursI()));
            detailMap.put(EppALoadType.eFullCode(fullCode), EppEpvRowLoad.wrap(detail.getHoursE()));
        }
        EppEpvTermDistributedRow row = get(EppEpvTermDistributedRow.class, rowId);
        row.addLoad(detailMap);
        return detailMap;
    }

    @Override
    public IRowLoadWrapper getRowTermLoad(Long rowId)
    {
        final Map<String, Double> loadMap = getRowLoad(rowId);
        final Map<Integer, Map<String, Double>> tLoadMap = new HashMap<>();
        final Map<Integer, Map<String, Integer>> tActionMap = new HashMap<>();

        for (final EppEpvRowTerm detail : getList(EppEpvRowTerm.class, EppEpvRowTerm.row().id(), rowId)) {
            Map<String, Double> map = new HashMap<>();
            tLoadMap.put(detail.getTerm().getIntValue(), map);
            detail.addLoad(map);
        }

        for (EppEpvRowTermAction ca : getList(EppEpvRowTermAction.class, EppEpvRowTermAction.rowTerm().row().id(), rowId)) {
            SafeMap.safeGet(tActionMap, ca.getRowTerm().getTerm().getIntValue(), HashMap.class).put(ca.getControlActionType().getFullCode(), ca.getSize());
        }

        for (EppEpvRowTermLoad l : getList(EppEpvRowTermLoad.class, EppEpvRowTermLoad.rowTerm().row().id(), rowId)) {
            Map<String, Double> loadMapLocal = SafeMap.safeGet(tLoadMap, l.getRowTerm().getTerm().getIntValue(), HashMap.class);
            String fullCode = l.getLoadType().getFullCode();
            loadMapLocal.put(fullCode, l.getHoursAsDouble());
            loadMapLocal.put(EppALoadType.iFullCode(fullCode), EppEpvRowTermLoad.wrap(l.getHoursI()));
            loadMapLocal.put(EppALoadType.eFullCode(fullCode), EppEpvRowTermLoad.wrap(l.getHoursE()));
        }

        final EppEpvTermDistributedRow row = get(EppEpvTermDistributedRow.class, rowId);

        return new IRowLoadWrapper() {
            @Override public EppEpvTermDistributedRow row() { return row; }
            @Override public Map<String, Double> rowLoadMap() { return loadMap; }
            @Override public Map<Integer, Map<String, Double>> rowTermLoadMap() { return tLoadMap; }
            @Override public Map<Integer, Map<String, Integer>> rowTermActionMap() { return tActionMap; }
        };
    }

    @Override
    public void doUpdateRowLoad(Map<Long, IRowLoadWrapper> rowLoad, boolean loadWithSelfworkWoControl)
    {
        EppEduPlanVersionDataDAO.check_editable4rows(getSession(), rowLoad.keySet());

        final Map<String, EppLoadType> loadTypeMap = getLoadTypeMap();
        final Map<String, EppControlActionType> actionTypeMap = getActionTypeMap();
        final Map<Integer, Term> termMap = DevelopGridDAO.getTermMap();

        // обновляем итоговые нагрузки по строке
        for (Map.Entry<Long, IRowLoadWrapper> e: rowLoad.entrySet()) {

            EppEpvTermDistributedRow row = e.getValue().row();

            update(row);
            row.updateLoad(e.getValue().rowLoadMap(), loadWithSelfworkWoControl);
            // update(row);

            final Map<String, Double> loadMap = e.getValue().rowLoadMap();
            List<EppEpvRowLoad> targetRecords = new ArrayList<>();
            if (loadMap != null) {

                for (EppLoadType loadType: loadTypeMap.values())
                {
                    if (!(loadType instanceof EppALoadType))
                        continue;

                    String fullCode = loadType.getFullCode();
                    String iFullCode = EppALoadType.iFullCode(fullCode);
                    String eFullCode = EppALoadType.eFullCode(fullCode);

                    if (loadMap.containsKey(fullCode) || loadMap.containsKey(iFullCode) || loadMap.containsKey(eFullCode))
                    {
                        EppEpvRowLoad epvRowLoad = new EppEpvRowLoad(row, ((EppALoadType) loadType));
                        epvRowLoad.setHours(EppEpvRowLoad.unwrap(loadMap.get(fullCode)));
                        epvRowLoad.setHoursI(EppEpvRowLoad.unwrap(loadMap.get(iFullCode)));
                        epvRowLoad.setHoursE(EppEpvRowLoad.unwrap(loadMap.get(eFullCode)));

                        targetRecords.add(epvRowLoad);
                    }
                }
            }
            new SessionMergeAction<INaturalId, EppEpvRowLoad>() {
                        @Override protected INaturalId key(EppEpvRowLoad source) { return source.getNaturalId(); }
                        @Override protected EppEpvRowLoad buildRow(EppEpvRowLoad source) { return source; }
                        @Override protected void fill(EppEpvRowLoad target, EppEpvRowLoad source) { target.update(source, false); }
            }.merge(
                getList(EppEpvRowLoad.class, EppEpvRowLoad.row().id(), e.getKey()),
                targetRecords
            );

        }

        // обновляем набор семестров строки
        final Map<Long, Set<Integer>> rowTermNumbersMap = new HashMap<>();
        for (Map.Entry<Long, IRowLoadWrapper> e : rowLoad.entrySet()) {
            Long rowId = e.getKey();
            IRowLoadWrapper w = e.getValue();

            Set<Integer> rowTerms = new HashSet<>();
            for (Integer term : termMap.keySet()) {
                Map<String, Integer> aMap = w.rowTermActionMap().get(term);
                if (aMap != null && !aMap.isEmpty()) { rowTerms.add(term); }
                Map<String, Double> lMap = w.rowTermLoadMap().get(term);
                if (lMap != null && !lMap.isEmpty()) { rowTerms.add(term); }
            }
            rowTermNumbersMap.put(rowId, rowTerms);
        }

        final Map<Long, List<EppEpvRowTerm>> existingRowTermMap = new HashMap<>();
        BatchUtils.execute(rowTermNumbersMap.keySet(), 100, ids -> {
            for (final EppEpvRowTerm e1 : getList(EppEpvRowTerm.class, EppEpvRowTerm.row().id().s(), ids)) {
                SafeMap.safeGet(existingRowTermMap, e1.getRow().getId(), ArrayList.class).add(e1);
            }
        });
        final Map<Long, Map<Integer, EppEpvRowTerm>> updatedRowTermMap = new HashMap<>();

        for (final Map.Entry<Long, Set<Integer>> e : rowTermNumbersMap.entrySet()) {
            List<EppEpvRowTerm> termList = new ArrayList<>();
            for (Integer term : e.getValue()) {
                termList.add(new EppEpvRowTerm(get(EppEpvTermDistributedRow.class, e.getKey()), termMap.get(term)));
            }

            List<EppEpvRowTerm> newTermList = new SessionMergeAction<Integer, EppEpvRowTerm>() {
                @Override protected Integer key(final EppEpvRowTerm source) {
                    return source.getTerm().getIntValue();
                }
                @Override protected void fill(final EppEpvRowTerm target, final EppEpvRowTerm source) {
                    target.update(source);
                }
                @Override protected EppEpvRowTerm buildRow(final EppEpvRowTerm source) {
                    return source;
                }
            }.merge(SafeMap.safeGet(existingRowTermMap, e.getKey(), ArrayList.class), termList);

            Map<Integer, EppEpvRowTerm> newTermMap = new HashMap<>();
            updatedRowTermMap.put(e.getKey(), newTermMap);
            for (EppEpvRowTerm term : newTermList) {
                newTermMap.put(term.getTerm().getIntValue(), term);
            }
        }

        // обновляем нагрузки и КМ по семестрам строки
        final Map<Long, List<EppEpvRowTermLoad>> rowTermLoadMap = new HashMap<>();
        final Map<Long, List<EppEpvRowTermAction>> rowTermActionMap = new HashMap<>();
        BatchUtils.execute(rowLoad.keySet(), 100, ids -> {
            for (final EppEpvRowTermLoad e : getList(EppEpvRowTermLoad.class, EppEpvRowTermLoad.rowTerm().row().id().s(), ids)) {
                SafeMap.safeGet(rowTermLoadMap, e.getRowTerm().getId(), ArrayList.class).add(e);
            }
            for (final EppEpvRowTermAction e : getList(EppEpvRowTermAction.class, EppEpvRowTermAction.rowTerm().row().id().s(), ids)) {
                SafeMap.safeGet(rowTermActionMap, e.getRowTerm().getId(), ArrayList.class).add(e);
            }

        });

        for (final Map.Entry<Long, IRowLoadWrapper> e : rowLoad.entrySet()) {
            for (final EppEpvRowTerm rowTerm : updatedRowTermMap.get(e.getKey()).values()) {

                IRowLoadWrapper loadWrapper = e.getValue();

                Map<String, Double> termLoadMap = loadWrapper.rowTermLoadMap().get(rowTerm.getTerm().getIntValue());

                List<EppEpvRowTermLoad> termLoadList = new ArrayList<>();
                if (null != termLoadMap) {

                    rowTerm.updateLoad(termLoadMap, loadWithSelfworkWoControl);

                    for (EppLoadType loadType: loadTypeMap.values())
                    {
                        if (!(loadType instanceof EppALoadType))
                            continue;

                        String fullCode = loadType.getFullCode();
                        Double hours = termLoadMap.get(fullCode);
                        Double hoursI = termLoadMap.get(EppALoadType.iFullCode(fullCode));
                        Double hoursE = termLoadMap.get(EppALoadType.eFullCode(fullCode));

                        if ((hours != null && hours > 0) || (hoursI != null && hoursI > 0) || (hoursE != null && hoursE > 0))
                        {
                            EppEpvRowTermLoad rowTermLoad = new EppEpvRowTermLoad(rowTerm, ((EppALoadType) loadType));
                            rowTermLoad.setHours(EppEpvRowTermLoad.unwrap(hours));
                            rowTermLoad.setHoursI(EppEpvRowTermLoad.unwrap(hoursI));
                            rowTermLoad.setHoursE(EppEpvRowTermLoad.unwrap(hoursE));

                            termLoadList.add(rowTermLoad);
                        }
                    }
                }

                new MergeAction.SessionMergeAction<String, EppEpvRowTermLoad>() {
                    @Override protected String key(final EppEpvRowTermLoad source) {
                        return source.getLoadType().getFullCode();
                    }
                    @Override protected void fill(final EppEpvRowTermLoad target, final EppEpvRowTermLoad source) {
                        target.update(source);
                    }
                    @Override protected EppEpvRowTermLoad buildRow(final EppEpvRowTermLoad source) {
                        return source;
                    }
                }.merge(SafeMap.safeGet(rowTermLoadMap, rowTerm.getId(), ArrayList.class), termLoadList);

                Map<String, Integer> termActionMap = loadWrapper.rowTermActionMap().get(rowTerm.getTerm().getIntValue());
                List<EppEpvRowTermAction> termActionList = new ArrayList<>();
                if (null != termActionMap) {
                    for (Map.Entry<String, Integer> eI : termActionMap.entrySet()) {
                        Number value = eI.getValue();
                        if (null == value || value.intValue() <= 0) continue;
                        EppControlActionType actionType = actionTypeMap.get(eI.getKey());
                        if (null == actionType) continue;
                        termActionList.add(new EppEpvRowTermAction(rowTerm, actionType, value.intValue()));
                    }
                }

                new MergeAction.SessionMergeAction<String, EppEpvRowTermAction>() {
                    @Override protected String key(final EppEpvRowTermAction source) { return source.getControlActionType().getFullCode(); }
                    @Override protected void fill(final EppEpvRowTermAction target, final EppEpvRowTermAction source) { target.update(source); }
                    @Override protected EppEpvRowTermAction buildRow(final EppEpvRowTermAction source) { return source; }
                }.merge(SafeMap.safeGet(rowTermActionMap, rowTerm.getId(), ArrayList.class), termActionList);
            }
        }
    }

    @Override
    public List<HSelectOption> getEpvRowHierarchyListFromBlock(final EppEduPlanVersionBlock block, final Predicate<IEppEpvRow> predicate)
    {
        final ArrayList<IEppEpvRowWrapper> elements = new ArrayList<>(IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockRowMap(block.getId(), false).values());

        CollectionUtils.filter(elements, predicate);
        return HierarchyUtil.listHierarchyNodesWithParents(elements, null, false);
    }

    @Override
    public List<HSelectOption> getPlanStructureHierarchyList4Add(final EppEduPlanVersionBlock block)
    {
        final EppEduPlan eduPlan = block.getEduPlanVersion().getEduPlan();
        final EduProgramSubjectIndex subjectIndex;
        if (eduPlan instanceof EppEduPlanHigherProf) {
            subjectIndex = ((EppEduPlanHigherProf) eduPlan).getProgramSubject().getSubjectIndex();
        } else if (eduPlan instanceof EppEduPlanSecondaryProf) {
            subjectIndex = ((EppEduPlanSecondaryProf) eduPlan).getProgramSubject().getSubjectIndex();
        } else {
            subjectIndex = null;
        }

        final List<EppPlanStructure> structureList;
        if (subjectIndex != null) {
            structureList = getPropertiesList(EppPlanStructure4SubjectIndex.class, EppPlanStructure4SubjectIndex.eduProgramSubjectIndex(), subjectIndex, false, EppPlanStructure4SubjectIndex.eppPlanStructure());
        } else {
            structureList = getList(EppPlanStructure.class);
        }

        final List<HSelectOption> valueList = HierarchyUtil.listHierarchyNodesWithParents(structureList, CommonBaseUtil.PRIORITY_SIMPLE_SAFE_COMPARATOR, true);

        // убираем все, кроме листьев
        final Set<Long> parentIds = new HashSet<>();
        for (final HSelectOption value : valueList) {
            final EppPlanStructure parent = ((EppPlanStructure)value.getObject()).getParent();
            if (null != parent) {
                parentIds.add(parent.getId());
            }
        }

        for (final HSelectOption value : valueList) {
            value.setCanBeSelected(!parentIds.contains(((EppPlanStructure)value.getObject()).getId()));
        }

        return valueList;
    }

    @Override
    @SuppressWarnings("unchecked")
    public String getNextNumber(final IEppEpvRow row, final Iterable<Long> excludeRowIdsIterable)
    {
        if (row instanceof IEppNumberRow)
        {
            final Set<Long> excludeRowIds = new HashSet<>(8);

            // исключаем то, что пришло из параметров
            if (null != excludeRowIdsIterable) {
                for (final Long excludeId: excludeRowIdsIterable) {
                    if (null != excludeId) {
                        excludeRowIds.add(excludeId);
                    }
                }
            }

            // исключаем сам объект, если он есть в базе
            if (null != row.getId()) {
                excludeRowIds.add(row.getId());
            }

            final List list = this.selectSameLevelElements(row, excludeRowIds);

            CollectionUtils.transform(list, input -> {
                // теперь берем из элементов их номера
                return (input instanceof IEppNumberRow ? StringUtils.stripStart(((IEppNumberRow)input).getNumber(), "0") : null);
            });

            final Set<String> numbers = new HashSet<>(list);

            // если номер уже есть и он свободный, то возвращаем его
            final String currentNumber = StringUtils.trimToNull(StringUtils.stripStart(((IEppNumberRow)row).getNumber(), "0"));
            if ((null != currentNumber) && !numbers.contains(currentNumber)) {
                return currentNumber;
            }

            // подбираем номера подряд
            int i = 1;
            String number;
            while (numbers.contains(number = String.valueOf(i))) {
                i++;
            }
            return number;
        }
        return "0";
    }


    @Override
    public Collection<IEppEpvRow> getTopLevelRows(final Collection<IEppEpvRow> rows)
    {
        final Set<Long> ids = CommonBaseEntityUtil.getIdSet(rows);
        final Set<IEppEpvRow> result = new LinkedHashSet<>();

        for (IEppEpvRow row : rows)  {
            IEppEpvRow current = row;
            while (null != current) {
                if (ids.contains(current.getId()))  { row = current; /* откладываем - сразу добавлять нельзя, вдруг есть еще кто-то выше */ }
                current = current.getHierarhyParent();
            }
            result.add(row /*последний отложенный или сама строка*/);
        }

        return result;
    }

    @Override
    public boolean isEmpty(final Long eppEduPlanVersionId) {
        final DQLSelectBuilder sel = new DQLSelectBuilder();
        sel.fromEntity(EppEpvRow.class, "row");
        sel.where(eq(property("row.owner.eduPlanVersion.id"), value(eppEduPlanVersionId)));
        sel.column(DQLFunctions.count(property("row.id")));
        return (sel.createStatement(this.getSession()).<Number>uniqueResult().intValue() <= 0);
    }

    @Override
    public EppEduPlanVersionRootBlock getRootBlock(EppEduPlanVersion version) {
        return new DQLSelectBuilder()
        .fromEntity(EppEduPlanVersionRootBlock.class, "b")
        .column(property("b"))
        .where(eq(property(EppEduPlanVersionRootBlock.eduPlanVersion().fromAlias("b")), value(version)))
        .order(property(EppEduPlanVersionRootBlock.id().fromAlias("b")))
        .createStatement(getSession()).setMaxResults(1).uniqueResult();
    }

    @Override
    public List<Long> getEpvIds4UpdateRowStoredIndex() {
        return new DQLSelectBuilder()
        .fromEntity(EppEpvRow.class, "r")
        .column(property(EppEpvRow.owner().eduPlanVersion().id().fromAlias("r")))
        .predicate(DQLPredicateType.distinct)
        .where(or(
            like(property(EppEpvRow.storedIndex().fromAlias("r")), value("-%")),
            isNull(property(EppEpvRow.storedIndex().fromAlias("r")))
        ))
        .createStatement(getSession()).list();
    }

    @Override
    public void doUpdateEpvRowStoredIndex(Long epvId)
    {
        final Session session = lock(EppEpvRow.class.getName()+"."+EppEpvRow.P_STORED_INDEX+"."+epvId);
        try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler())) {

            List<EppEduPlanVersionBlock> blockList = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion().id(), epvId);

            Comparator<EppEduPlanVersionBlock> blockComparator = Ordering.compound(Arrays.asList(
                Ordering.natural().onResultOf(new FastBeanFunction<EppEduPlanVersionBlock, Boolean>("rootBlock")).reverse(),
                Ordering.natural().onResultOf(new FastBeanFunction<EppEduPlanVersionBlock, Long>("id"))
            ));
            Collections.sort(blockList, blockComparator);

            final List<EppEpvRow> allRowList = getList(EppEpvRow.class, EppEpvRow.owner().eduPlanVersion().id(), epvId);

            for (EppEpvRow row: allRowList) {
                if (null != row.getHierarhyParent()) {
                    final Set<EppEpvRow> set = new HashSet<>();
                    set.add(row);
                    while (null != row.getHierarhyParent()) {
                        EppEpvRow parent = row.getHierarhyParent();

                        if (!set.add(parent)) {
                            row.setHierarhyParent(null);
                            break;
                        }

                        if (!parent.getOwner().isRootBlock() && !parent.getOwner().equals(row.getOwner())) {
                            row.setHierarhyParent(null);
                            break;
                        }

                        row = row.getHierarhyParent();
                    }
                }
            }

            Collections.sort(allRowList, EppEpvRow.GLOBAL_ROW_COMPARATOR);

            for (EppEduPlanVersionBlock block: blockList)
            {
                // формируем перечень строк (общего и текущего блока) - они нужны для построения индексов по алгоритмам
                final List<EppEpvRow> blockRowList = new ArrayList<>(allRowList.size());
                for (EppEpvRow row: allRowList) {
                    if (row.getOwner().isRootBlock() || row.getOwner().equals(block)) { blockRowList.add(row); }
                }
                Collections.sort(blockRowList, EppEpvRow.GLOBAL_ROW_COMPARATOR);
                Map<EppEpvRow, IEppIndexedRowWrapper> wrapperMap = buildIndexRuleRowWrapperMap(blockRowList);

                // далее, обрабатываем строки только текущего блока (общий блок будет обработан отдельно)
                for (EppEpvRow row: blockRowList) {
                    if (row.getOwner().equals(block)) {
                        String userIndex = StringUtils.trimToNull(row.getUserIndex());
                        if (null != userIndex) {
                            row.setStoredIndex(userIndex);
                        } else {
                            String calculatedIndex = StringUtils.trimToNull(wrapperMap.get(row).getIndex());
                            if (null != calculatedIndex) {
                                row.setStoredIndex(calculatedIndex);
                            } else {
                                row.setStoredIndex("?");
                            }
                        }
                        session.saveOrUpdate(row);
                    }
                }
            }

            // засовываем все в базу
            session.flush();
        }
    }


    @Override
    public Collection<EppEpvTotalRow> getTotalRows(final IEppEpvBlockWrapper versionWrapper, final Collection<IEppEpvRowWrapper> versionRows) {

        Collection<IEppEpvRowWrapper> filteredRows = EppEpvTotalRow.filterRowsForTotalStats(versionRows);

        final Collection<IEppEpvRowWrapper> filteredRowsI = versionRows.stream().filter(w -> w instanceof EppInteractiveRowWrapper && !(w.getHierarhyParent().getHierarhyParent() != null && w.getHierarhyParent().getHierarhyParent().getRow() instanceof EppEpvGroupImRow)).collect(Collectors.toList());
        final Collection<IEppEpvRowWrapper> filteredRowsE = versionRows.stream().filter(w -> w instanceof EppElectronicRowWrapper && !(w.getHierarhyParent().getHierarhyParent() != null && w.getHierarhyParent().getHierarhyParent().getRow() instanceof EppEpvGroupImRow)).collect(Collectors.toList());

        // Убираем фэйковые строки (на результат они не влияют, но расчеты замедляют)
        filteredRows = Collections2.filter(filteredRows, Predicates.not(Predicates.instanceOf(EppFakeRowWrapper.class)));

        final List<EppEpvTotalRow> result = new ArrayList<>();

        result.add(EppEpvTotalRow.getTotalRowTotal(filteredRows, "Всего"));
        result.add(EppEpvTotalRow.getTotalRowTotal(filteredRowsI, "Из них в интерактивной форме"));
        result.add(EppEpvTotalRow.getTotalRowTotal(filteredRowsE, "Из них в электронной форме"));
        result.add(EppEpvTotalRow.getTotalRowAvgLoad(versionWrapper, filteredRows));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, "Всего экзаменов", EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM_ACCUM));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, "Всего зачетов", EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, "Всего курсовых работ", EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, "Всего курсовых проектов", EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT));
        result.add(new EppEpvTotalRow.EppEpvTotalControlRow(filteredRows, "Всего контрольных работ", EppControlActionType.FULL_CODE_CONTROL_ACTION_CONTROL_WORK));

        return result;
    }

    @Override
    public void doSaveOrUpdateRow(EppEpvRow row, IRowLoadWrapper load)
    {
        if (StringUtils.isBlank(row.getStoredIndex())) { row.setStoredIndex("-"); }
        saveOrUpdate(row);
        update(row);
        doUpdateEpvRowStoredIndex(row.getOwner().getEduPlanVersion().getId());
        if (null != load) {
            IEppEduPlanVersionDataDAO.instance.get().doUpdateRowLoad(Collections.singletonMap(row.getId(), load), true);
        }
    }

    @Override
    public Map<IEppEpvRow, List<EppProfessionalTask>> getProfessionalTaskMap(List<IEppEpvRow> rowList)
    {
        if (rowList != null && !rowList.isEmpty())
        {
            List<Long> idsList = rowList.stream().map(IIdentifiable::getId).collect(Collectors.toList());
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(EppEpvRegRowProfessionalTask.class, "epvr_pt").column("epvr_pt")
                    .fetchPath(DQLJoinType.inner, EppEpvRegRowProfessionalTask.professionalTask().fromAlias("epvr_pt"), "pt")
                    .fetchPath(DQLJoinType.inner, EppEpvRegRowProfessionalTask.epvRegistryRow().fromAlias("epvr_pt"), "r")
                    .where(in(property("epvr_pt", EppEpvRegRowProfessionalTask.epvRegistryRow().id()), idsList));
            Map<IEppEpvRow, List<EppProfessionalTask>> result = getList(builder).stream()
                    .map(obj -> (EppEpvRegRowProfessionalTask) obj)
                    .collect(Collectors.groupingBy(EppEpvRegRowProfessionalTask::getEpvRegistryRow,
                            Collectors.mapping(EppEpvRegRowProfessionalTask::getProfessionalTask, Collectors.toList())));

            result.values().forEach(o -> Collections.sort(o, (e1, e2) -> (Integer.compare(e1.getPriority(), e2.getPriority()))));
            return result;
        }
        return Collections.emptyMap();
    }

    @Override
    public void doSaveOrUpdateRegRowProfessionalTask(List<EppProfessionalTask> professionalTaskList, EppEpvRegistryRow row)
    {
        if (row != null && row.getId() != null)
        {
            // поудаляем все связи проф.задачи с переданной строкой
            DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(EppEpvRegRowProfessionalTask.class);
            deleteBuilder.where(eq(property(EppEpvRegRowProfessionalTask.epvRegistryRow().id()), value(row.getId())));
            if (professionalTaskList != null && !professionalTaskList.isEmpty())
                deleteBuilder.where(notIn(property(EppEpvRegRowProfessionalTask.professionalTask()), professionalTaskList));
            createStatement(deleteBuilder).execute();
            getSession().flush();

            if (professionalTaskList != null)
            {
                List<EppProfessionalTask> alreadyExist = getProfessionalTaskMap(Collections.singletonList(row)).get(row);
                if (alreadyExist != null && !alreadyExist.isEmpty())
                    professionalTaskList.removeAll(alreadyExist);
                for (EppProfessionalTask professionalTask : professionalTaskList)
                {
                    EppEpvRegRowProfessionalTask eppEpvRegRowProfessionalTask = new EppEpvRegRowProfessionalTask();
                    eppEpvRegRowProfessionalTask.setEpvRegistryRow(row);
                    eppEpvRegRowProfessionalTask.setProfessionalTask(professionalTask);
                    save(eppEpvRegRowProfessionalTask);
                }
            }
        }
    }

    @Override
    public void specifyControlHoursForAllRows(Long eppEpvBlockId, Double value)
    {
        EppEduPlanVersionBlock block = getNotNull(EppEduPlanVersionBlock.class, eppEpvBlockId);
        // блокируем УП(в)
        NamedSyncInTransactionCheckLocker.register(getSession(), ImmutableList.of(block.getEduPlanVersion().getId()));
        // Проверяем, можно ли его редактировать
        EppEduPlanDAO.check_editable(block.getEduPlanVersion());

        final DQLSelectBuilder termsDQL = new DQLSelectBuilder().fromEntity(EppEpvRowTerm.class, "rt");
        termsDQL.where(eq(property("rt", EppEpvRowTerm.row().owner()), value(eppEpvBlockId)));
        termsDQL.where(// Исключить ГИА
                       not(exists(EppEpvRegistryRow.class,
                                  EppEpvRegistryRow.P_ID, property("rt", EppEpvRowTerm.row().id()),
                                  EppEpvRegistryRow.registryElementType().parent().code().s(), EppRegistryStructureCodes.REGISTRY_ATTESTATION))
        );

        // Выставляем контроли по семестрам
        termsDQL.column(property("rt.id"));

        final DQLUpdateBuilder termUpdate = new DQLUpdateBuilder(EppEpvRowTerm.class);
        termUpdate.where(in(property(EppEpvRowTerm.P_ID), termsDQL.buildQuery()));
        termUpdate.set(EppEpvRowTerm.P_HOURS_CONTROL, value(UniEppUtils.unwrap(value)));
        termUpdate.createStatement(getSession()).execute();

        // Суммируем по строкам
        termsDQL.resetColumns();
        termsDQL.column(DQLFunctions.sum(property("rt", EppEpvRowTerm.P_HOURS_CONTROL)), "cnt");
        termsDQL.column(property("rt", EppEpvRowTerm.row().id()), "row_id");
        termsDQL.group(property("rt", EppEpvRowTerm.row().id()));

        final DQLUpdateBuilder rowsUpdate = new DQLUpdateBuilder(EppEpvTermDistributedRow.class);
        rowsUpdate.fromDataSource(termsDQL.buildQuery(), "terms");
        rowsUpdate.where(eq(property("id"), property("terms", "row_id")));
        rowsUpdate.set(EppEpvTermDistributedRow.P_HOURS_CONTROL, property("terms", "cnt"));
        rowsUpdate.createStatement(getSession()).execute();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static final String CACHE_KEY_EPP_ACTION_TYPE_MAP = "epp-action-type-map";
    private final static HibernateCallback<Map<String, EppControlActionType>> ACTION_MAP_CALLBACK = session -> {
        Map<String, EppControlActionType> actionMap = DaoCache.get(CACHE_KEY_EPP_ACTION_TYPE_MAP);
        if (null == actionMap) {
            final ImmutableMap.Builder<String, EppControlActionType> builder = ImmutableMap.builder();
            SharedDQLUtils.builder(EppControlActionType.class, "a", property("a"))
                    .createStatement(session).<EppControlActionType>list().stream()
                    .sorted(EppControlActionType.COMPARATOR)
                    .forEachOrdered(action -> builder.put(action.getFullCode(), action));

            DaoCache.put(CACHE_KEY_EPP_ACTION_TYPE_MAP, actionMap = builder.build());
        }
        return actionMap;
    };

    private static final String CACHE_KEY_EPP_LOAD_TYPE_MAP = "epp-load-type-map";
    private final static HibernateCallback<Map<String, EppLoadType>> LOAD_MAP_CALLBACK = session -> {
        Map<String, EppLoadType> loadTypeMap = DaoCache.get(CACHE_KEY_EPP_LOAD_TYPE_MAP);
        if (null == loadTypeMap) {
            final ImmutableMap.Builder<String, EppLoadType> builder = ImmutableMap.builder();
            SharedDQLUtils.builder(EppLoadType.class, "e", property("e"))
                    .createStatement(session).<EppLoadType>list().stream()
                    .sorted(EppLoadType.COMPARATOR)
                    .forEachOrdered(load -> builder.put(load.getFullCode(), load));

            DaoCache.put(CACHE_KEY_EPP_LOAD_TYPE_MAP, loadTypeMap = builder.build());
        }
        return loadTypeMap;
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected static Session check_editable4rows(final Session session, final Collection<Long> rowIds)
    {
        final Set<Long> epvIds = new HashSet<>();
        BatchUtils.execute(rowIds, 192, rowIds1 -> epvIds.addAll(
            new DQLSelectBuilder()
            .fromEntity(EppEpvRow.class, "r")
            .predicate(DQLPredicateType.distinct)
            .column(property(EppEpvRowGen.owner().eduPlanVersion().id().fromAlias("r")))
            .where(in(property("r.id"), rowIds1))
            .createStatement(session).<Long>list()
        ));

        // сначала блокируем все УП(в)
        NamedSyncInTransactionCheckLocker.register(session, epvIds);

        for (final Long epvId: epvIds) {
            EppEduPlanDAO.check_editable((EppEduPlanVersion)session.get(EppEduPlanVersion.class, epvId));
        }

        return session;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * выбирает элементы (из базы) версии УП основного и текущего блока того же уровня, что и переданный элемент
     */
    @SuppressWarnings("unchecked")
    private List<EppEpvRow> selectSameLevelElements(final IEppEpvRow row, final Set<Long> excludeRowIds)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EppEpvRow.class, "r")
        .column(property("r"))
        .joinPath(DQLJoinType.inner, EppEpvRow.owner().fromAlias("r"), "o")
        .where(or(
            eq(property("r", EppEpvRow.owner()), value(row.getOwner())),
            and(
                instanceOf("o", EppEduPlanVersionRootBlock.class),
                eq(property("r", EppEpvRow.owner().eduPlanVersion()), value(row.getOwner().getEduPlanVersion()))
            )
        ));
        final List list = dql.createStatement(getSession()).list();

        final IEppEpvRow parent = row.getHierarhyParent();
        if (null == parent)
        {
            CollectionUtils.filter(list, object -> {
                final EppEpvRow eppEpvRow = (EppEpvRow)object;
                return (null == eppEpvRow.getHierarhyParent()) && !excludeRowIds.contains(eppEpvRow.getId());
            });
        }
        else
        {
            CollectionUtils.filter(list, object -> {
                final EppEpvRow eppEpvRow = (EppEpvRow)object;
                return (parent.equals(eppEpvRow.getHierarhyParent())) && !excludeRowIds.contains(eppEpvRow.getId());
            });
        }
        return list;
    }

    public int getTermSize(Long version, String code, Integer term)
    {
        if (term <= 0) { return 0; }

        final DQLSelectBuilder sel = new DQLSelectBuilder();
        sel.fromEntity(EppWeekPart.class, "rel");
        sel.fromEntity(EppELoadWeekType.class, "cfg");
        sel.where(eq(property("cfg", EppELoadWeekType.weekType().id()), property("rel", EppWeekPart.weekType().id())));
        if (!StringUtils.isEmpty(code))
        {
            //Вычисляем код из полного кода.
            code = code.substring(code.length() - 1);
            sel.where(eq(property("cfg", EppELoadWeekType.loadType().code()), value(code)));
        }
        sel.where(eq(property("rel", EppWeekPart.week().eduPlanVersion().eduPlan().programForm()), property("cfg", EppELoadWeekType.developForm().programForm())));
        sel.where(eq(property("rel", EppWeekPart.week().term().intValue()), value(term)));
        sel.where(eq(property("rel", EppWeekPart.week().eduPlanVersion().id()), value(version)));
        sel.column(DQLFunctions.countStar());

        int count = 0;
        List<Long> list = sel.createStatement(getSession()).list();
        if (null != list && !list.isEmpty())
        {
            count =  list.get(0).intValue();
        }

        return count;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
