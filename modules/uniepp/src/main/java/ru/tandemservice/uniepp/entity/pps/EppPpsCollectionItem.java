package ru.tandemservice.uniepp.entity.pps;

import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.pps.gen.EppPpsCollectionItemGen;

/**
 * Элемент списка ППС
 */
public class EppPpsCollectionItem extends EppPpsCollectionItemGen
{

    public EppPpsCollectionItem() {}
    public EppPpsCollectionItem(final IEppPPSCollectionOwner list, final PpsEntry pps) {
        this.setList(list);
        this.setPps(pps);
    }
}