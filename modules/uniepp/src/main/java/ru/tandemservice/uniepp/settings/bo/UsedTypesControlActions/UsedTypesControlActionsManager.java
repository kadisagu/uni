package ru.tandemservice.uniepp.settings.bo.UsedTypesControlActions;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniepp.settings.bo.UsedTypesControlActions.logic.IUsedTypesModifyDAO;
import ru.tandemservice.uniepp.settings.bo.UsedTypesControlActions.logic.UsedTypesModifyDAO;

/**
 * @author avedernikov
 * @since 03.09.2015
 */

@Configuration
public class UsedTypesControlActionsManager extends BusinessObjectManager
{
	public static UsedTypesControlActionsManager instance()
	{
		return instance(UsedTypesControlActionsManager.class);
	}

	@Bean
	public IUsedTypesModifyDAO modifyDAO()
	{
		return new UsedTypesModifyDAO();
	}
}
