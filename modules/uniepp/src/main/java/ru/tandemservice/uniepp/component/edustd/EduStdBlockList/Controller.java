package ru.tandemservice.uniepp.component.edustd.EduStdBlockList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickSwitchUsedInLoad(final IBusinessComponent componen)
    {
        this.getDao().doSwitchUsedInLoad((Long)componen.getListenerParameter());
    }

    public void onClickSwitchUsedInActions(final IBusinessComponent componen)
    {
        this.getDao().doSwitchUsedInActions((Long)componen.getListenerParameter());
    }
}
