package ru.tandemservice.uniepp.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.gen.EppGroupTypeALTGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.instanceOf;

/** @see ru.tandemservice.uniepp.entity.catalog.gen.EppGroupTypeALTGen */
public class EppGroupTypeALT extends EppGroupTypeALTGen
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
    {
        return EppGroupType.defaultSelectDSHandler(name).customize((alias, dql, context, filter) -> dql.where(instanceOf(alias, EppGroupTypeALT.class)));
    }
}