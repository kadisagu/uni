package ru.tandemservice.uniepp.component.base.ChangeState;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {
    void changeState(Model model, String stateCode);
}
