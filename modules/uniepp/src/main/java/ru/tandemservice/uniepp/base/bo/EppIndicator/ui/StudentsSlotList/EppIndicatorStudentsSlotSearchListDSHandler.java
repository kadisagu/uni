/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsSlotList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 01.03.13
 */
public class EppIndicatorStudentsSlotSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public static final String STUDENT_2_SLOT_LIST = "student2slotList";

    public static final String PROP_SLOT_LIST = "slotList";

    public EppIndicatorStudentsSlotSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);

        // только неархивные студенты в активном состоянии
        builder.where(eq(property(Student.archival().fromAlias(alias)), value(Boolean.FALSE)));
        builder.where(eq(property(Student.status().active().fromAlias(alias)), value(Boolean.TRUE)));

        // существует МСРП
        builder.where(exists(
            new DQLSelectBuilder()
            .fromEntity(EppStudentWorkPlanElement.class, "slot").column(property("slot.id"))
            .where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("slot"))))
            .where(eq(property(EppStudentWorkPlanElement.student().fromAlias("slot")), property(alias)))
            .buildQuery()
        ));

        final Map<Long, List<EppStudentWorkPlanElement>> student2slotList = SafeMap.get(ArrayList.class);

        final DQLSelectBuilder slotBuilder = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "slot")
        .where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("slot"))))
        .where(in(property(EppStudentWorkPlanElement.student().fromAlias("slot")), builder.buildQuery()))
        .order(property(EppStudentWorkPlanElement.year().educationYear().intValue().fromAlias("slot")))
        .order(property(EppStudentWorkPlanElement.term().intValue().fromAlias("slot")))
        .order(property(EppStudentWorkPlanElement.sourceRow().workPlan().state().code().fromAlias("slot")))
        .order(property(EppStudentWorkPlanElement.sourceRow().number().fromAlias("slot")))
        .order(property(EppStudentWorkPlanElement.registryElementPart().registryElement().fromAlias("slot")));

        for (final EppStudentWorkPlanElement slot: slotBuilder.createStatement(context.getSession()).<EppStudentWorkPlanElement>list())
            student2slotList.get(slot.getStudent().getId()).add(slot);

        context.put(STUDENT_2_SLOT_LIST, student2slotList);
    }

    @Override
    protected void wrap(DataWrapper wrapper, ExecutionContext context)
    {
        super.wrap(wrapper, context);

        final Map<Long, List<EppStudentWorkPlanElement>> student2slotList = context.get(STUDENT_2_SLOT_LIST);

        final StringBuilder builder = new StringBuilder();

        int term = 0;
        for (final EppStudentWorkPlanElement slot: student2slotList.get(wrapper.getId()))
        {
            final int slotTerm = slot.getTerm().getIntValue();
            if (term != slotTerm) {
                term = slotTerm;
                if (builder.length() > 0) { builder.append('\n'); }
            }
            builder.append(slot.getYear().getEducationYear().getTitle()).append(' ').append(slot.getPart().getShortTitle());
            builder.append(' ').append(slotTerm).append(" с. (").append(slot.getCourse().getIntValue()).append(" к.").append(")");
            final EppWorkPlanRow sourceRow = slot.getSourceRow();
            if (null != sourceRow) {
                builder.append(" [").append(StringUtils.left(sourceRow.getWorkPlan().getState().getTitle(), 4)).append("]");
                builder.append(' ').append(StringUtils.trimToEmpty(sourceRow.getNumber()));
                final String realTitle = sourceRow.getTitle();
                final int i = realTitle.indexOf(' ', 20);
                final String trimmedTitle = StringUtils.left(realTitle, (i < 1 ? 20 : i));
                if ((trimmedTitle.length()+3) < realTitle.length()) {
                    builder.append(' ').append(trimmedTitle).append("...");
                } else {
                    builder.append(' ').append(realTitle);
                }
            }
            builder.append(" (").append(slot.getRegistryElementPart().getNumber()).append("/").append(slot.getRegistryElementPart().getRegistryElement().getParts()).append(", ").append(UniEppUtils.formatLoad(slot.getRegistryElementPart().getRegistryElement().getSizeAsDouble(), false)).append(" час.)");
            builder.append("\n");
        }

        wrapper.setProperty(PROP_SLOT_LIST, builder.toString());
    }
}
