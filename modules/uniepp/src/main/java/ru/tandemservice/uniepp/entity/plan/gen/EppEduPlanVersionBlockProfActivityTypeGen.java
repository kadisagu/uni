package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlockProfActivityType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид профессиональной деятельности в блоке версии УП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanVersionBlockProfActivityTypeGen extends EntityBase
 implements INaturalIdentifiable<EppEduPlanVersionBlockProfActivityTypeGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlockProfActivityType";
    public static final String ENTITY_NAME = "eppEduPlanVersionBlockProfActivityType";
    public static final int VERSION_HASH = 1552977018;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN_VERSION_BLOCK = "eduPlanVersionBlock";
    public static final String L_PROF_ACTIVITY_TYPE = "profActivityType";

    private EppEduPlanVersionBlock _eduPlanVersionBlock;     // Блок версии УП
    private EppProfActivityType _profActivityType;     // Вид профессиональной деятельности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок версии УП. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlock getEduPlanVersionBlock()
    {
        return _eduPlanVersionBlock;
    }

    /**
     * @param eduPlanVersionBlock Блок версии УП. Свойство не может быть null.
     */
    public void setEduPlanVersionBlock(EppEduPlanVersionBlock eduPlanVersionBlock)
    {
        dirty(_eduPlanVersionBlock, eduPlanVersionBlock);
        _eduPlanVersionBlock = eduPlanVersionBlock;
    }

    /**
     * @return Вид профессиональной деятельности. Свойство не может быть null.
     */
    @NotNull
    public EppProfActivityType getProfActivityType()
    {
        return _profActivityType;
    }

    /**
     * @param profActivityType Вид профессиональной деятельности. Свойство не может быть null.
     */
    public void setProfActivityType(EppProfActivityType profActivityType)
    {
        dirty(_profActivityType, profActivityType);
        _profActivityType = profActivityType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEduPlanVersionBlockProfActivityTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setEduPlanVersionBlock(((EppEduPlanVersionBlockProfActivityType)another).getEduPlanVersionBlock());
                setProfActivityType(((EppEduPlanVersionBlockProfActivityType)another).getProfActivityType());
            }
        }
    }

    public INaturalId<EppEduPlanVersionBlockProfActivityTypeGen> getNaturalId()
    {
        return new NaturalId(getEduPlanVersionBlock(), getProfActivityType());
    }

    public static class NaturalId extends NaturalIdBase<EppEduPlanVersionBlockProfActivityTypeGen>
    {
        private static final String PROXY_NAME = "EppEduPlanVersionBlockProfActivityTypeNaturalProxy";

        private Long _eduPlanVersionBlock;
        private Long _profActivityType;

        public NaturalId()
        {}

        public NaturalId(EppEduPlanVersionBlock eduPlanVersionBlock, EppProfActivityType profActivityType)
        {
            _eduPlanVersionBlock = ((IEntity) eduPlanVersionBlock).getId();
            _profActivityType = ((IEntity) profActivityType).getId();
        }

        public Long getEduPlanVersionBlock()
        {
            return _eduPlanVersionBlock;
        }

        public void setEduPlanVersionBlock(Long eduPlanVersionBlock)
        {
            _eduPlanVersionBlock = eduPlanVersionBlock;
        }

        public Long getProfActivityType()
        {
            return _profActivityType;
        }

        public void setProfActivityType(Long profActivityType)
        {
            _profActivityType = profActivityType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppEduPlanVersionBlockProfActivityTypeGen.NaturalId) ) return false;

            EppEduPlanVersionBlockProfActivityTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getEduPlanVersionBlock(), that.getEduPlanVersionBlock()) ) return false;
            if( !equals(getProfActivityType(), that.getProfActivityType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEduPlanVersionBlock());
            result = hashCode(result, getProfActivityType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEduPlanVersionBlock());
            sb.append("/");
            sb.append(getProfActivityType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanVersionBlockProfActivityTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlanVersionBlockProfActivityType.class;
        }

        public T newInstance()
        {
            return (T) new EppEduPlanVersionBlockProfActivityType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlanVersionBlock":
                    return obj.getEduPlanVersionBlock();
                case "profActivityType":
                    return obj.getProfActivityType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlanVersionBlock":
                    obj.setEduPlanVersionBlock((EppEduPlanVersionBlock) value);
                    return;
                case "profActivityType":
                    obj.setProfActivityType((EppProfActivityType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlanVersionBlock":
                        return true;
                case "profActivityType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlanVersionBlock":
                    return true;
                case "profActivityType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlanVersionBlock":
                    return EppEduPlanVersionBlock.class;
                case "profActivityType":
                    return EppProfActivityType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlanVersionBlockProfActivityType> _dslPath = new Path<EppEduPlanVersionBlockProfActivityType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlanVersionBlockProfActivityType");
    }
            

    /**
     * @return Блок версии УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlockProfActivityType#getEduPlanVersionBlock()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> eduPlanVersionBlock()
    {
        return _dslPath.eduPlanVersionBlock();
    }

    /**
     * @return Вид профессиональной деятельности. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlockProfActivityType#getProfActivityType()
     */
    public static EppProfActivityType.Path<EppProfActivityType> profActivityType()
    {
        return _dslPath.profActivityType();
    }

    public static class Path<E extends EppEduPlanVersionBlockProfActivityType> extends EntityPath<E>
    {
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _eduPlanVersionBlock;
        private EppProfActivityType.Path<EppProfActivityType> _profActivityType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок версии УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlockProfActivityType#getEduPlanVersionBlock()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> eduPlanVersionBlock()
        {
            if(_eduPlanVersionBlock == null )
                _eduPlanVersionBlock = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_EDU_PLAN_VERSION_BLOCK, this);
            return _eduPlanVersionBlock;
        }

    /**
     * @return Вид профессиональной деятельности. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlockProfActivityType#getProfActivityType()
     */
        public EppProfActivityType.Path<EppProfActivityType> profActivityType()
        {
            if(_profActivityType == null )
                _profActivityType = new EppProfActivityType.Path<EppProfActivityType>(L_PROF_ACTIVITY_TYPE, this);
            return _profActivityType;
        }

        public Class getEntityClass()
        {
            return EppEduPlanVersionBlockProfActivityType.class;
        }

        public String getEntityName()
        {
            return "eppEduPlanVersionBlockProfActivityType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
