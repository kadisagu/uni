package ru.tandemservice.uniepp.component.registry.base.AddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.Return;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.component.base.SimpleAddEditModel;
import ru.tandemservice.uniepp.component.registry.base.ModelBase;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.EppLoadFormatter;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad;

import java.util.*;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id"),
    @Bind(key="inline", binding="inline"),
    @Bind(key="loadMap", binding="defaultLoadMap")
})
@Return({
    @Bind(key="eppRegistryElementId", binding="element.id")
})
public abstract class Model<T extends EppRegistryElement> extends ModelBase<T> {

    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    public boolean isEditForm() { return SimpleAddEditModel.isEditForm(this.getElement()); }
    public boolean isReadOnly() { return SimpleAddEditModel.isReadOnly(this.getElement()); }

    private boolean inline = false;
    public Boolean getInline() { return this.inline; }
    public void setInline(final Boolean inline) { this.inline = Boolean.TRUE.equals(inline); }

    private Map<String, Double> defaultLoadMap = Collections.emptyMap();
    public void setDefaultLoadMap(final Map<String, Double> defaultLoadMap) { this.defaultLoadMap = defaultLoadMap; }

    public Map<String, Double> getDefaultLoadMap() {
        final Map<String, Double> map = this.defaultLoadMap;
        return (null != map ? map : Collections.<String, Double>emptyMap());
    }

    private Map<String, EppLoadType> loadTypeMap;
    public Map<String, EppLoadType> getLoadTypeMap() { return this.loadTypeMap; }
    public void setLoadTypeMap(Map<String, EppLoadType> loadTypeMap) { this.loadTypeMap = loadTypeMap; }

    public Collection<EppALoadType> getEppALoadTypes() { return EppLoadTypeUtils.getALoadTypeList(getLoadTypeMap()); }
    public Collection<EppELoadType> getEppELoadTypes() { return EppLoadTypeUtils.getELoadTypeList(getLoadTypeMap()); }

    private Map<String, EppRegistryElementLoad> loadMap;
    public Map<String, EppRegistryElementLoad> getLoadMap() { return this.loadMap; }
    public void setLoadMap(final Map<String, EppRegistryElementLoad> loadMap) { this.loadMap = loadMap; }

    private EppLoadType currentLoadType;
    public EppLoadType getCurrentLoadType() { return this.currentLoadType; }
    public void setCurrentLoadType(final EppLoadType currentLoadType) { this.currentLoadType = currentLoadType; }

    public EppRegistryElementLoad getCurrentDisciplineLoad() {
        return this.getDisciplineLoad(this.getCurrentLoadType());
    }

    protected EppRegistryElementLoad getDisciplineLoad(final EppLoadType loadType) {
        return SafeMap.safeGet(this.getLoadMap(), loadType.getFullCode(), key -> {
            final EppRegistryElementLoad load = new EppRegistryElementLoad();
            load.setLoadAsDouble(Model.this.getDefaultLoadMap().get(key));
            return load;
        });
    }

    private T element;
    public T getElement() { return this.element; }
    public void setElement(final T element) { this.element = element; }

    public void setupElementDefaultLoad() {
        final T element = this.getElement();
        element.setSizeAsDouble(this.defaultLoadMap.get(EppLoadType.FULL_CODE_TOTAL_HOURS));
        element.setLaborAsDouble(this.defaultLoadMap.get(EppLoadType.FULL_CODE_LABOR));
    }

    @Override
    public OrgUnit getOrgUnit() {
        return (null == this.getElement() ? null : this.getElement().getOwner());
    }

    private ISelectModel ownerSelectModel;
    public ISelectModel getOwnerSelectModel() { return this.ownerSelectModel; }
    public void setOwnerSelectModel(final ISelectModel ownerSelectModel) { this.ownerSelectModel = ownerSelectModel; }

    private List<HSelectOption> parentList = Collections.emptyList();
    public List<HSelectOption> getParentList() { return this.parentList; }
    public void setParentList(final List<HSelectOption> parentList) { this.parentList = parentList; }

    @Override public String getPermissionPrefix() { return ""; }


    public boolean isFillLoadFromPartsAllowed() {
        return ((null != this.getElement().getId()) && !this.isReadOnly());
    }

    public void fillLoadFromParts() {
        if (!this.isFillLoadFromPartsAllowed()) { return; }

        final T element = this.getElement();
        final Long id = element.getId();
        if (null == id) { return; }

        final IEppRegElWrapper w = IEppRegistryDAO.instance.get().getRegistryElementDataMap(Collections.singleton(id)).get(id);
        if (null == w) { return; }

        element.setSizeAsDouble(w.getChildrenLoadAsDouble(EppLoadType.FULL_CODE_TOTAL_HOURS));
        element.setLaborAsDouble(w.getChildrenLoadAsDouble(EppLoadType.FULL_CODE_LABOR));

        // аудиторка
        for (final EppLoadType currentLoadType: this.getEppALoadTypes()) {
            this.getDisciplineLoad(currentLoadType).setLoadAsDouble(w.getChildrenLoadAsDouble(currentLoadType.getFullCode()));
        }

        // учебная нагрузка
        for (final EppLoadType currentLoadType: this.getEppELoadTypes()) {
            this.getDisciplineLoad(currentLoadType).setLoadAsDouble(w.getChildrenLoadAsDouble(currentLoadType.getFullCode()));
        }
    }

    public String getRegistryElementPartsLoadAsString() {
        if (!this.isFillLoadFromPartsAllowed()) { return ""; }

        final T element = this.getElement();
        final Long id = element.getId();
        if (null == id) { return ""; }

        final IEppRegElWrapper w = IEppRegistryDAO.instance.get().getRegistryElementDataMap(Collections.singleton(id)).get(id);
        if (null == w) { return ""; }

        return new EppLoadFormatter<IEppRegElWrapper>(getLoadTypeMap()) {
            @Override protected Double load(IEppRegElWrapper element, String loadFullCode) {
                return element.getChildrenLoadAsDouble(loadFullCode);
            }
        }.format(w);
    }

    private IUploadFile _workProgramFile;
    private IUploadFile _workProgramAnnotation;

    public IUploadFile getWorkProgramFile() { return _workProgramFile; }
    public void setWorkProgramFile(IUploadFile workProgramFile) { _workProgramFile = workProgramFile; }

    public IUploadFile getWorkProgramAnnotation() { return _workProgramAnnotation; }
    public void setWorkProgramAnnotation(IUploadFile workProgramAnnotation) { _workProgramAnnotation = workProgramAnnotation; }

    private ISelectModel _eduGroupSplitVariantModel;
    private List<EppGroupType> _eduGroupTypes = new ArrayList<>();
    private IMultiSelectModel _eduGroupTypeModel;


    public ISelectModel getEduGroupSplitVariantModel() {
        return _eduGroupSplitVariantModel;
    }

    public void setEduGroupSplitVariantModel(ISelectModel eduGroupSplitVariantModel) {
        _eduGroupSplitVariantModel = eduGroupSplitVariantModel;
    }

    public List<EppGroupType> getEduGroupTypes() {
        return _eduGroupTypes;
    }

    public void setEduGroupTypes(List<EppGroupType> eduGroupTypes) {
        _eduGroupTypes = eduGroupTypes;
    }

    public IMultiSelectModel getEduGroupTypeModel() {
        return _eduGroupTypeModel;
    }

    public void setEduGroupTypeModel(IMultiSelectModel eduGroupTypeModel) {
        _eduGroupTypeModel = eduGroupTypeModel;
    }
}
