package ru.tandemservice.uniepp.entity.pupnag.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphWeekPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часть недели (ГУП)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkGraphWeekPartGen extends EntityBase
 implements INaturalIdentifiable<EppWorkGraphWeekPartGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphWeekPart";
    public static final String ENTITY_NAME = "eppWorkGraphWeekPart";
    public static final int VERSION_HASH = -178876170;
    private static IEntityMeta ENTITY_META;

    public static final String L_WEEK = "week";
    public static final String P_NUMBER = "number";
    public static final String L_WEEK_TYPE = "weekType";

    private EppWorkGraphRowWeek _week;     // ГУП (Неделя строки курса)
    private int _number;     // Номер части учебной недели
    private EppWeekType _weekType;     // Тип учебной деятельности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ГУП (Неделя строки курса). Свойство не может быть null.
     */
    @NotNull
    public EppWorkGraphRowWeek getWeek()
    {
        return _week;
    }

    /**
     * @param week ГУП (Неделя строки курса). Свойство не может быть null.
     */
    public void setWeek(EppWorkGraphRowWeek week)
    {
        dirty(_week, week);
        _week = week;
    }

    /**
     * @return Номер части учебной недели. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер части учебной недели. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Тип учебной деятельности. Свойство не может быть null.
     */
    @NotNull
    public EppWeekType getWeekType()
    {
        return _weekType;
    }

    /**
     * @param weekType Тип учебной деятельности. Свойство не может быть null.
     */
    public void setWeekType(EppWeekType weekType)
    {
        dirty(_weekType, weekType);
        _weekType = weekType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppWorkGraphWeekPartGen)
        {
            if (withNaturalIdProperties)
            {
                setWeek(((EppWorkGraphWeekPart)another).getWeek());
                setNumber(((EppWorkGraphWeekPart)another).getNumber());
            }
            setWeekType(((EppWorkGraphWeekPart)another).getWeekType());
        }
    }

    public INaturalId<EppWorkGraphWeekPartGen> getNaturalId()
    {
        return new NaturalId(getWeek(), getNumber());
    }

    public static class NaturalId extends NaturalIdBase<EppWorkGraphWeekPartGen>
    {
        private static final String PROXY_NAME = "EppWorkGraphWeekPartNaturalProxy";

        private Long _week;
        private int _number;

        public NaturalId()
        {}

        public NaturalId(EppWorkGraphRowWeek week, int number)
        {
            _week = ((IEntity) week).getId();
            _number = number;
        }

        public Long getWeek()
        {
            return _week;
        }

        public void setWeek(Long week)
        {
            _week = week;
        }

        public int getNumber()
        {
            return _number;
        }

        public void setNumber(int number)
        {
            _number = number;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppWorkGraphWeekPartGen.NaturalId) ) return false;

            EppWorkGraphWeekPartGen.NaturalId that = (NaturalId) o;

            if( !equals(getWeek(), that.getWeek()) ) return false;
            if( !equals(getNumber(), that.getNumber()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getWeek());
            result = hashCode(result, getNumber());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getWeek());
            sb.append("/");
            sb.append(getNumber());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkGraphWeekPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkGraphWeekPart.class;
        }

        public T newInstance()
        {
            return (T) new EppWorkGraphWeekPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "week":
                    return obj.getWeek();
                case "number":
                    return obj.getNumber();
                case "weekType":
                    return obj.getWeekType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "week":
                    obj.setWeek((EppWorkGraphRowWeek) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "weekType":
                    obj.setWeekType((EppWeekType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "week":
                        return true;
                case "number":
                        return true;
                case "weekType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "week":
                    return true;
                case "number":
                    return true;
                case "weekType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "week":
                    return EppWorkGraphRowWeek.class;
                case "number":
                    return Integer.class;
                case "weekType":
                    return EppWeekType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkGraphWeekPart> _dslPath = new Path<EppWorkGraphWeekPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkGraphWeekPart");
    }
            

    /**
     * @return ГУП (Неделя строки курса). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphWeekPart#getWeek()
     */
    public static EppWorkGraphRowWeek.Path<EppWorkGraphRowWeek> week()
    {
        return _dslPath.week();
    }

    /**
     * @return Номер части учебной недели. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphWeekPart#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Тип учебной деятельности. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphWeekPart#getWeekType()
     */
    public static EppWeekType.Path<EppWeekType> weekType()
    {
        return _dslPath.weekType();
    }

    public static class Path<E extends EppWorkGraphWeekPart> extends EntityPath<E>
    {
        private EppWorkGraphRowWeek.Path<EppWorkGraphRowWeek> _week;
        private PropertyPath<Integer> _number;
        private EppWeekType.Path<EppWeekType> _weekType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ГУП (Неделя строки курса). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphWeekPart#getWeek()
     */
        public EppWorkGraphRowWeek.Path<EppWorkGraphRowWeek> week()
        {
            if(_week == null )
                _week = new EppWorkGraphRowWeek.Path<EppWorkGraphRowWeek>(L_WEEK, this);
            return _week;
        }

    /**
     * @return Номер части учебной недели. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphWeekPart#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(EppWorkGraphWeekPartGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Тип учебной деятельности. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphWeekPart#getWeekType()
     */
        public EppWeekType.Path<EppWeekType> weekType()
        {
            if(_weekType == null )
                _weekType = new EppWeekType.Path<EppWeekType>(L_WEEK_TYPE, this);
            return _weekType;
        }

        public Class getEntityClass()
        {
            return EppWorkGraphWeekPart.class;
        }

        public String getEntityName()
        {
            return "eppWorkGraphWeekPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
