package ru.tandemservice.uniepp.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.entity.catalog.EppIndexRule;
import ru.tandemservice.uniepp.entity.settings.EppIndexRule4SubjectIndex;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Правило формирования индекса для записи ГОСов / УП
 *
 * Для перечня направлений определяет правило формирования индекса записи в ГОС / УП / РУП (в интерфейсе)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppIndexRule4SubjectIndexGen extends EntityBase
 implements INaturalIdentifiable<EppIndexRule4SubjectIndexGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.settings.EppIndexRule4SubjectIndex";
    public static final String ENTITY_NAME = "eppIndexRule4SubjectIndex";
    public static final int VERSION_HASH = -730662011;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PROGRAM_SUBJECT_INDEX = "eduProgramSubjectIndex";
    public static final String L_EPP_INDEX_RULE = "eppIndexRule";

    private EduProgramSubjectIndex _eduProgramSubjectIndex;     // Перечень направлений подготовки и квалификаций профессионального образования
    private EppIndexRule _eppIndexRule;     // Алгоритм формирования индексов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Перечень направлений подготовки и квалификаций профессионального образования. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EduProgramSubjectIndex getEduProgramSubjectIndex()
    {
        return _eduProgramSubjectIndex;
    }

    /**
     * @param eduProgramSubjectIndex Перечень направлений подготовки и квалификаций профессионального образования. Свойство не может быть null и должно быть уникальным.
     */
    public void setEduProgramSubjectIndex(EduProgramSubjectIndex eduProgramSubjectIndex)
    {
        dirty(_eduProgramSubjectIndex, eduProgramSubjectIndex);
        _eduProgramSubjectIndex = eduProgramSubjectIndex;
    }

    /**
     * @return Алгоритм формирования индексов. Свойство не может быть null.
     */
    @NotNull
    public EppIndexRule getEppIndexRule()
    {
        return _eppIndexRule;
    }

    /**
     * @param eppIndexRule Алгоритм формирования индексов. Свойство не может быть null.
     */
    public void setEppIndexRule(EppIndexRule eppIndexRule)
    {
        dirty(_eppIndexRule, eppIndexRule);
        _eppIndexRule = eppIndexRule;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppIndexRule4SubjectIndexGen)
        {
            if (withNaturalIdProperties)
            {
                setEduProgramSubjectIndex(((EppIndexRule4SubjectIndex)another).getEduProgramSubjectIndex());
            }
            setEppIndexRule(((EppIndexRule4SubjectIndex)another).getEppIndexRule());
        }
    }

    public INaturalId<EppIndexRule4SubjectIndexGen> getNaturalId()
    {
        return new NaturalId(getEduProgramSubjectIndex());
    }

    public static class NaturalId extends NaturalIdBase<EppIndexRule4SubjectIndexGen>
    {
        private static final String PROXY_NAME = "EppIndexRule4SubjectIndexNaturalProxy";

        private Long _eduProgramSubjectIndex;

        public NaturalId()
        {}

        public NaturalId(EduProgramSubjectIndex eduProgramSubjectIndex)
        {
            _eduProgramSubjectIndex = ((IEntity) eduProgramSubjectIndex).getId();
        }

        public Long getEduProgramSubjectIndex()
        {
            return _eduProgramSubjectIndex;
        }

        public void setEduProgramSubjectIndex(Long eduProgramSubjectIndex)
        {
            _eduProgramSubjectIndex = eduProgramSubjectIndex;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppIndexRule4SubjectIndexGen.NaturalId) ) return false;

            EppIndexRule4SubjectIndexGen.NaturalId that = (NaturalId) o;

            if( !equals(getEduProgramSubjectIndex(), that.getEduProgramSubjectIndex()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEduProgramSubjectIndex());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEduProgramSubjectIndex());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppIndexRule4SubjectIndexGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppIndexRule4SubjectIndex.class;
        }

        public T newInstance()
        {
            return (T) new EppIndexRule4SubjectIndex();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduProgramSubjectIndex":
                    return obj.getEduProgramSubjectIndex();
                case "eppIndexRule":
                    return obj.getEppIndexRule();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduProgramSubjectIndex":
                    obj.setEduProgramSubjectIndex((EduProgramSubjectIndex) value);
                    return;
                case "eppIndexRule":
                    obj.setEppIndexRule((EppIndexRule) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduProgramSubjectIndex":
                        return true;
                case "eppIndexRule":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduProgramSubjectIndex":
                    return true;
                case "eppIndexRule":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduProgramSubjectIndex":
                    return EduProgramSubjectIndex.class;
                case "eppIndexRule":
                    return EppIndexRule.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppIndexRule4SubjectIndex> _dslPath = new Path<EppIndexRule4SubjectIndex>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppIndexRule4SubjectIndex");
    }
            

    /**
     * @return Перечень направлений подготовки и квалификаций профессионального образования. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.settings.EppIndexRule4SubjectIndex#getEduProgramSubjectIndex()
     */
    public static EduProgramSubjectIndex.Path<EduProgramSubjectIndex> eduProgramSubjectIndex()
    {
        return _dslPath.eduProgramSubjectIndex();
    }

    /**
     * @return Алгоритм формирования индексов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppIndexRule4SubjectIndex#getEppIndexRule()
     */
    public static EppIndexRule.Path<EppIndexRule> eppIndexRule()
    {
        return _dslPath.eppIndexRule();
    }

    public static class Path<E extends EppIndexRule4SubjectIndex> extends EntityPath<E>
    {
        private EduProgramSubjectIndex.Path<EduProgramSubjectIndex> _eduProgramSubjectIndex;
        private EppIndexRule.Path<EppIndexRule> _eppIndexRule;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Перечень направлений подготовки и квалификаций профессионального образования. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.settings.EppIndexRule4SubjectIndex#getEduProgramSubjectIndex()
     */
        public EduProgramSubjectIndex.Path<EduProgramSubjectIndex> eduProgramSubjectIndex()
        {
            if(_eduProgramSubjectIndex == null )
                _eduProgramSubjectIndex = new EduProgramSubjectIndex.Path<EduProgramSubjectIndex>(L_EDU_PROGRAM_SUBJECT_INDEX, this);
            return _eduProgramSubjectIndex;
        }

    /**
     * @return Алгоритм формирования индексов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppIndexRule4SubjectIndex#getEppIndexRule()
     */
        public EppIndexRule.Path<EppIndexRule> eppIndexRule()
        {
            if(_eppIndexRule == null )
                _eppIndexRule = new EppIndexRule.Path<EppIndexRule>(L_EPP_INDEX_RULE, this);
            return _eppIndexRule;
        }

        public Class getEntityClass()
        {
            return EppIndexRule4SubjectIndex.class;
        }

        public String getEntityName()
        {
            return "eppIndexRule4SubjectIndex";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
