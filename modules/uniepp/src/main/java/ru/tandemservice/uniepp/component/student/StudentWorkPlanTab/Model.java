/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.student.StudentWorkPlanTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

/**
 * @author vip_delete
 * @since 24.03.2010
 */
@Input({
    @Bind(key = "studentId", binding = "holder.id", required = true)
})
public class Model
{
    private final EntityHolder<Student> holder = new EntityHolder<Student>();
    public EntityHolder<Student> getHolder() { return this.holder; }
    public Student getStudent() { return this.getHolder().getValue(); }

    private final SelectModel<ViewWrapper<DevelopGridTerm>> termModel = new SelectModel<ViewWrapper<DevelopGridTerm>>();
    public SelectModel<ViewWrapper<DevelopGridTerm>> getTermModel() { return this.termModel; }

    private EppWorkPlanBase selectedWorkPlan;
    public EppWorkPlanBase getSelectedWorkPlan() { return this.selectedWorkPlan; }
    public void setSelectedWorkPlan(final EppWorkPlanBase selectedWorkPlan) { this.selectedWorkPlan = selectedWorkPlan; }

    public EppWorkPlanBase getWorkplan() { return this.getSelectedWorkPlan(); }

}
