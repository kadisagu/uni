/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionCopyScheduleData;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author nkokorina
 * Created on: 24.09.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final EppEduPlanVersion version = this.getNotNull(EppEduPlanVersion.class, model.getEduplanVersion().getId());
        version.getState().check_editable(version);

        model.setEduplanVersion(version);
        model.setSourceVersionSelectModel(new UniQueryFullCheckSelectModel("title", EppEduPlanVersion.state().title().s())
        {
            @Override
            protected MQBuilder query(final String alias, final String filter)
            {
                final MQBuilder builder = new MQBuilder(EppEduPlanVersion.ENTITY_NAME, alias);
                builder.add(MQExpression.notEq(alias, EppEduPlanVersion.id().s(), version.getId()));
                builder.add(MQExpression.eq(alias, EppEduPlanVersion.eduPlan().developGrid().s(), version.getEduPlan().getDevelopGrid()));

                if (null != filter)
                    builder.add(MQExpression.or(
                        MQExpression.like(alias, EppEduPlanVersion.number(), CoreStringUtils.escapeLike(filter)),
                        MQExpression.like(alias, EppEduPlanVersion.eduPlan().number(), CoreStringUtils.escapeLike(filter)),
                        MQExpression.like(alias, EppEduPlanVersion.state().title(), CoreStringUtils.escapeLike(filter))
                        ));

                //                builder.add(MQExpression.exists(
                //                        new MQBuilder(EppEduPlanVersionBlock.ENTITY_NAME, "block1")
                //                        .add(MQExpression.eqProperty("block1", EppEduPlanVersionBlock.eduPlanVersion().id(), alias, EppEduPlanVersion.id()))
                //                        .add(MQExpression.exists(new MQBuilder(EppEduPlanVersionBlock.ENTITY_NAME, "block2")
                //                        .add(MQExpression.eq("block2", EppEduPlanVersionBlock.eduPlanVersion().id(), model.getEduplanVersion().getId()))
                //                        .add(MQExpression.eqProperty("block1", EppEduPlanVersionBlock.educationLevelHighSchool(), "block2", EppEduPlanVersionBlock.educationLevelHighSchool()))))
                //                ));
                return builder;
            }
        });
        this.prepareCourseList(model);
    }

    @Override
    public void prepareCourseList(final Model model)
    {
        if (null == model.getSourceVersion())
        {
            model.setCourseList(null);
            model.getCourseSelectedMap().clear();
            return;
        }
        model.getCourseSelectedMap().clear();
        final MQBuilder courses = new MQBuilder(Course.ENTITY_NAME, "course");
        courses.add(MQExpression.exists(new MQBuilder(EppEduPlanVersionWeekType.ENTITY_NAME, "plan_week")
        .add(MQExpression.eqProperty("plan_week", EppEduPlanVersionWeekType.course().id(), "course", Course.id()))
        .add(MQExpression.eq("plan_week", EppEduPlanVersionWeekType.eduPlanVersion(), model.getSourceVersion()))));
        courses.addOrder("course", Course.intValue());
        model.setCourseList(courses.<Course>getResultList(this.getSession()));
        for (final Course course : model.getCourseList()) {
            model.getCourseSelectedMap().put(course, true);
        }
    }

    @Override
    public void validate(final Model model, final ErrorCollector errors)
    {
        if (CollectionUtils.isEmpty(model.getCourseList())) {
            errors.add("У выбранной версии учебного плана не заполнен учебный график.");
            return;
        }
        boolean selected = false;
        for (final boolean courseSelected : model.getCourseSelectedMap().values()) {
            selected = selected || courseSelected;
        }
        if (!selected) {
            errors.add("Выберите хотя бы один курс для копирования.");
        }
        if (!model.getEduplanVersion().getEduPlan().getDevelopGrid().equals(model.getSourceVersion().getEduPlan().getDevelopGrid())) {
            errors.add("Учебные сетки текущего УП и УП выбранной версии должны совпадать.", "source");
        }
    }

    @Override
    public void update(final Model model)
    {
        final Session session = this.getSession();

        for (final Map.Entry<Course, Boolean> entry : model.getCourseSelectedMap().entrySet())
        {
            if (!entry.getValue()) {
                continue;
            }
            final Course course = entry.getKey();

            final Map<EppWeek, EppEduPlanVersionWeekType> existingMap = new HashMap<EppWeek, EppEduPlanVersionWeekType>();

            final MQBuilder existing = new MQBuilder(EppEduPlanVersionWeekType.ENTITY_CLASS, "plan_week");
            existing.add(MQExpression.eq("plan_week", EppEduPlanVersionWeekType.eduPlanVersion(), model.getEduplanVersion()));
            existing.add(MQExpression.eq("plan_week", EppEduPlanVersionWeekType.course(), course));
            for (final EppEduPlanVersionWeekType planWeek : existing.<EppEduPlanVersionWeekType>getResultList(session)) {
                existingMap.put(planWeek.getWeek(), planWeek);
            }

            final MQBuilder source = new MQBuilder(EppEduPlanVersionWeekType.ENTITY_CLASS, "plan_week");
            source.add(MQExpression.eq("plan_week", EppEduPlanVersionWeekType.eduPlanVersion(), model.getSourceVersion()));
            source.add(MQExpression.eq("plan_week", EppEduPlanVersionWeekType.course(), course));
            for (final EppEduPlanVersionWeekType sourcePlanWeek : source.<EppEduPlanVersionWeekType>getResultList(session))
            {
                EppEduPlanVersionWeekType planWeek = existingMap.get(sourcePlanWeek.getWeek());
                existingMap.remove(sourcePlanWeek.getWeek());
                if (planWeek == null)
                {
                    planWeek = new EppEduPlanVersionWeekType();
                    planWeek.setEduPlanVersion(model.getEduplanVersion());
                    planWeek.setCourse(course);
                    planWeek.setWeek(sourcePlanWeek.getWeek());
                }
                planWeek.setWeekType(sourcePlanWeek.getWeekType());
                planWeek.setTerm(sourcePlanWeek.getTerm());
                session.saveOrUpdate(planWeek);
            }
            for (final EppEduPlanVersionWeekType planWeek : existingMap.values()) {
                this.delete(planWeek);
            }
        }
    }
}
