package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused"})
public class MS_uniepp_2x7x2_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.16"),
                new ScriptDependency("org.tandemframework.shared", "1.7.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppWorkPlanRowObligation
        tool.renameTable("epp_c_wp_obligation_t", "epp_c_wp_row_kind_t");
        tool.entityCodes().delete("eppWorkPlanRowKind");
        tool.entityCodes().rename("eppWorkPlanRowObligation", "eppWorkPlanRowKind");

        // создано обязательное свойство shortTitle
        tool.createColumn("epp_c_wp_row_kind_t", new DBColumn("shorttitle_p", DBType.createVarchar(255)));

        new MigrationUtils.BatchUpdater("update epp_c_wp_row_kind_t set shorttitle_p=? where code_p=?", DBType.EMPTY_STRING, DBType.EMPTY_STRING)
                .addBatch("О", "1")
                .addBatch("Э", "2")
                .addBatch("Ф", "3")
                .addBatch("А", "4")
                .executeUpdate(tool);

        // сделать колонку NOT NULL
        tool.setColumnNullable("epp_c_wp_row_kind_t", "shorttitle_p", false);


        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppWorkPlanRow
        tool.renameColumn("epp_wprow_t", "obligation_id", "kind_id");
    }
}