/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.Collection;

/**
 * @author rsizonenko
 * @since 31.03.2016
 */
public interface IEppReorganizationDao extends INeedPersistenceSupport {

    void doChangeEppRegistryElementOwner(Collection<EppRegistryElement> input, OrgUnit owner);

    void doChangeEduPlanVersionSpecializationBlockOwner(Collection<EppEduPlanVersionSpecializationBlock> input, OrgUnit owner);

}
