package ru.tandemservice.uniepp.component.workplan.WorkPlanData;

import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.ui.WorkPlanDataSourceGenerator;

/**
 * 
 * @author nkokorina
 * Created on: 05.08.2010
 */
public abstract class WorkPlanDataBaseDAO<T extends WorkPlanDataBaseModel> extends UniBaseDao implements IPrepareable<T>
{
    @Override
    public void prepare(final T model)
    {
        final WorkPlanDataSourceGenerator generator = new WorkPlanDataSourceGenerator() {
            @Override protected String getPermissionKeyEdit() {
                return model.getPermissionKeyEdit();
            }
            @Override protected Long getWorkPlanBaseId() {
                return model.getWorkPlanId();
            }
            @Override protected boolean isEditable() {
                return model.isEditable();
            }
        };
        generator.fillDisciplineDataSource(model.getDisciplineDataSource());
        generator.fillActionDataSource(model.getActionDataSource());
    }
}
