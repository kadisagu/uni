package ru.tandemservice.uniepp.entity.plan;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateMutable;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionGen;

import java.util.Collection;

/**
 * Версия учебного плана
 */
@EppStateMutable({ EppEduPlanVersion.L_DEVELOP_GRID_TERM, EppEduPlanVersion.L_VIEW_TABLE_DISCIPLINE, EppEduPlanVersion.P_HOURS_I, EppEduPlanVersion.P_HOURS_E })
public class EppEduPlanVersion extends EppEduPlanVersionGen implements ITitled, IHierarchyItem, IEppStateObject, INumberObject, ISecLocalEntityOwner
{
    private Collection<IEntity> _cache_sec_local_entities = null;
    @Override public Collection<IEntity> getSecLocalEntities() {
        if (null != this._cache_sec_local_entities) { return this._cache_sec_local_entities; }
        return (this._cache_sec_local_entities = this.getEduPlan().getSecLocalEntities());
    }

    @Override
    public INumberGenerationRule<EppEduPlanVersion> getNumberGenerationRule() {
        return IEppEduPlanDAO.instance.get().getEduPlanVersionNumberGenerationRule();
    }

    @Override
    public EppEduPlan getHierarhyParent() {
        return this.getEduPlan();
    }

    @EntityDSLSupport(parts={EppEduPlanVersionGen.L_EDU_PLAN+"."+EppEduPlan.P_NUMBER, EppEduPlanVersionGen.P_NUMBER})
    @Override
    public String getFullNumber() {
        return (this.getEduPlan().getNumber()+"."+this.getNumber());
    }

    /**
     * @return № [номер УП].[номер УП(в)] [дополнительное название, если есть]
     */
    @EntityDSLSupport(parts={EppEduPlanVersionGen.L_EDU_PLAN+"."+EppEduPlan.P_NUMBER, EppEduPlanVersionGen.P_NUMBER})
    @Override
    public String getTitle() {
        if (getEduPlan() == null) {
            return this.getClass().getSimpleName();
        }
        return "№" + this.getFullNumber() + (StringUtils.isEmpty(this.getTitlePostfix()) ? "" : (" " + this.getTitlePostfix()));
    }

    @EntityDSLSupport
    @Override
    public String getTitleWithArchive() {
        return getTitle() + (EppState.STATE_ARCHIVED.equals(getState().getCode()) ? " архив" : "");
    }

    /**
     * @return № [номер УП].[номер УП(в)] ([название НПВ, на которое создан УП]) [дополнительное название, если есть]
     */
    @EntityDSLSupport(parts={EppEduPlanVersionGen.L_EDU_PLAN+"."+EppEduPlan.P_NUMBER, EppEduPlanVersionGen.P_NUMBER})
    @Override
    public String getFullTitle() {
        return  "№" + this.getFullNumber() +
            " " + getEduPlan().getEducationElementShortTitle() +
            " " + this.getEduPlan().getDevelopCombinationTitle() +
            (StringUtils.isEmpty(this.getTitlePostfix()) ? "" : (" " + this.getTitlePostfix()));
    }

    /**
     *
     * @return № [номер УП].[номер УП(в)] [код направления проф. образ.] [сокр. назв. направления проф. образ.]
     * [сокр. назв. формы обуч.], [сокр. назв. особен. реализ.], [учебная сетка] [дополнительное название, если есть].
     */
    @EntityDSLSupport
    @Override
    public String getFullTitleWithEducationsCharacteristics()
    {
        return "№" + this.getFullNumber() +
                (this.getEduPlan() instanceof EppEduPlanProf ? " " + ((EppEduPlanProf) this.getEduPlan()).getProgramSubject().getShortTitleWithCode() : "") +
                " " + this.getEduPlan().getProgramForm().getShortTitle() +
                (this.getEduPlan().getProgramTrait() == null ? "" : ", " + this.getEduPlan().getProgramTrait().getShortTitle()) +
                ", " + this.getEduPlan().getDevelopGrid().getTitle() +
                (StringUtils.isEmpty(this.getTitlePostfix()) ? "" : (" " + this.getTitlePostfix()))
        ;
    }

    /**
     * @return № [номер УП].[номер УП(в)] ([название НПВ, на которое создан УП]) [дополнительное название, если есть]
     */
    @EntityDSLSupport(parts={EppEduPlanVersionGen.L_EDU_PLAN+"."+EppEduPlan.P_NUMBER, EppEduPlanVersionGen.P_NUMBER})
    @Override
    public String getEducationElementTitle() {
        return this.getFullTitle();
    }

    public String getEducationElementSimpleTitle()
    {
        return this.getEduPlan().getEducationElementSimpleTitle();
    }
}