package ru.tandemservice.uniepp.component.edugroup.list.OwnerOrgUnitTab;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.component.edugroup.EduGroupOwnerModel;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DAO extends ru.tandemservice.uniepp.component.edugroup.list.GroupListOwnerTabBase.DAO implements IDAO
{
    @Override protected String getSettingsName() {
        return "owner";
    }

    /**
     * сводки с текущей диспетчерской
     */
    @Override
    protected List<EppRealEduGroupSummary> summaryList(EduGroupOwnerModel model) {
        return new DQLSelectBuilder()
        .fromEntity(EppRealEduGroupSummary.class, "s")
        .column(property("s"))
        .where(eq(property(EppRealEduGroupSummary.owner().fromAlias("s")), value(model.getOrgUnit())))
        .createStatement(getSession()).list();
    }

    /**
     * УГС, сводки которых с текущей диспетчерской
     */
    @Override
    protected DQLSelectBuilder groupWithRelationsDql(EduGroupOwnerModel model, EppRealEduGroupSummary summary, Class<? extends EppRealEduGroup> groupClass) {
        // ничего делать не надо, поскольку summary (сводка) - уже параметр
        // и гарантируется, что она с текущей диспетчерской
        if (!model.getOrgUnit().equals(summary.getOwner())) {
            throw new IllegalStateException();
        }
        return super.groupWithRelationsDql(model, summary, groupClass);
    }
}
