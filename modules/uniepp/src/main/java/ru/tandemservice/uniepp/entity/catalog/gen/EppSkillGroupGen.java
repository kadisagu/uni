package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Группа компетенций
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppSkillGroupGen extends EntityBase
 implements INaturalIdentifiable<EppSkillGroupGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppSkillGroup";
    public static final String ENTITY_NAME = "eppSkillGroup";
    public static final int VERSION_HASH = 484523373;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_PRIORITY = "priority";

    private String _code;     // Системный код
    private String _title;     // Название
    private String _shortTitle;     // Сокращенное название
    private int _priority;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * Указывает на порядок вывода групп и компетенций в списках и при печати.
     *
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppSkillGroupGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EppSkillGroup)another).getCode());
            }
            setTitle(((EppSkillGroup)another).getTitle());
            setShortTitle(((EppSkillGroup)another).getShortTitle());
            setPriority(((EppSkillGroup)another).getPriority());
        }
    }

    public INaturalId<EppSkillGroupGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EppSkillGroupGen>
    {
        private static final String PROXY_NAME = "EppSkillGroupNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppSkillGroupGen.NaturalId) ) return false;

            EppSkillGroupGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppSkillGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppSkillGroup.class;
        }

        public T newInstance()
        {
            return (T) new EppSkillGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "shortTitle":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "shortTitle":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppSkillGroup> _dslPath = new Path<EppSkillGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppSkillGroup");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkillGroup#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkillGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkillGroup#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * Указывает на порядок вывода групп и компетенций в списках и при печати.
     *
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkillGroup#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends EppSkillGroup> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkillGroup#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EppSkillGroupGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkillGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppSkillGroupGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkillGroup#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EppSkillGroupGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * Указывает на порядок вывода групп и компетенций в списках и при печати.
     *
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkillGroup#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EppSkillGroupGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return EppSkillGroup.class;
        }

        public String getEntityName()
        {
            return "eppSkillGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
