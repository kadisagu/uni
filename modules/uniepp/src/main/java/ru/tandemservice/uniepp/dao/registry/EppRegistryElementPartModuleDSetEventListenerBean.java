package ru.tandemservice.uniepp.dao.registry;

import org.hibernate.Session;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteAction;
import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.entity.catalog.EppModuleStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.codes.EppModuleStructureCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppModuleStructureGen;
import ru.tandemservice.uniepp.entity.catalog.gen.EppStateGen;
import ru.tandemservice.uniepp.entity.registry.*;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EppRegistryElementPartModuleDSetEventListenerBean {

    // добавление новой части в дисциплину
    // надо добавлять новый "фэйковый" модуль, если модулей еще нет
    private static class InsertRegistryElementPartListener extends ParamTransactionCompleteAction<Long, Void> implements IDSetEventListener {
        private static final InsertRegistryElementPartListener INSTANCE = new InsertRegistryElementPartListener();

        @Override public Void beforeCompletion(final Session session, final Collection<Long> ids)
        {
            if (ids.isEmpty()) { return null; }

            BatchUtils.execute(ids, 512, new BatchUtils.Action<Long>() {

                final EppState state = DataAccessServices.dao().getByNaturalId(new EppStateGen.NaturalId(EppState.STATE_FORMATIVE));
                final EppModuleStructure parent = DataAccessServices.dao().getByNaturalId(new EppModuleStructureGen.NaturalId(EppModuleStructureCodes.BASE_MODULES));

                @Override public void execute(final Collection<Long> ids)
                {
                    final List<EppRegistryElementPart> relps = new DQLSelectBuilder()
                    .fromEntity(EppRegistryElementPart.class, "relp").column(property("relp"))
                    .where(eq(property(EppRegistryElementPart.registryElement().state().code().fromAlias("relp")), value(EppState.STATE_FORMATIVE)))
                    .where(in(property("relp.id"), ids))
                    .where(notExists(
                        new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "m").column(property("m.id"))
                        .where(eq(property(EppRegistryElementPartModule.part().fromAlias("m")), property("relp")))
                        .buildQuery()
                    ))
                    .createStatement(session).list();

                    if (relps.size() > 0) {

                        for (final EppRegistryElementPart relp: relps) {
                            final EppRegistryModule mod = new EppRegistryModule();
                            mod.setState(this.state);
                            mod.setParent(this.parent);
                            mod.setNumber(EppRegistryModuleDAO.getDefaultModuleNumber(relp));
                            mod.setTitle(EppRegistryModuleDAO.getDefaultModuleTitle(relp));
                            mod.setOwner(relp.getTutorOu());
                            session.save(mod);

                            final EppRegistryElementPartModule rel = new EppRegistryElementPartModule();
                            rel.setNumber(1);
                            rel.setModule(mod);
                            rel.setPart(relp);
                            session.save(rel);

                        }

                        // явно сохраняем объекты в базу
                        session.flush();
                        session.clear();
                    }
                }
            });

            return null;
        }

        @Override public void onEvent(final DSetEvent event) {
            this.register(
                event,
                new DQLSelectBuilder()
                .fromDataSource(event.getMultitude().getSource(), "relp").column(property("relp.id"))
                .createStatement(event.getContext()).<Long>list()
            );
        }
    }

    // удаление модуля из дисциплины
    // если модуль в стостоянии "формируется" и не привязан больее ни к одной дисциплине, то переводим его в архив, если в нем нет нагрузки
    // если есть модули в состоянии "архив" и на них никто (не только дисциплины) не ссылается - удаляем
    public static class DeleteRegistryElementPartModuleListener extends TransactionCompleteAction<Void> implements IDSetEventListener {
        private static final DeleteRegistryElementPartModuleListener INSTANCE = new DeleteRegistryElementPartModuleListener();

        @Override public Void beforeCompletion(final Session session)
        {
            final EventListenerLocker.Lock lock = EppDSetUpdateCheckEventListener.LOCKER.lock(new EventListenerLocker.Handler<IDSetEventListener>() {
                @Override public boolean handle(IDSetEventListener listener, IEntityMeta entityMeta, Set<String> properties, Number count, DSetEvent event) {
                    return EppRegistryModule.class.isAssignableFrom(entityMeta.getEntityClass()); /* если это модуль, то ошибки нет */
                }
            });
            try {
                // удаляем, если надо
                new DQLDeleteBuilder(EppRegistryModule.class)
                .where(in(
                    property("id"),
                    new DQLSelectBuilder().fromEntity(EppRegistryModule.class, "module").column(property("module.id"))

                    // нужное состояние
                    .where(eq(property(EppRegistryModule.state().code().fromAlias("module")), value(EppState.STATE_ARCHIVED)))

                    // можем удалять
                    .where(new DQLCanDeleteExpressionBuilder(EppRegistryModule.class, "id").getExpression())

                    // нет нагрузки
                    .where(notExists(
                        new DQLSelectBuilder().fromEntity(EppRegistryModuleALoad.class, "rel").column(property("rel.id"))
                        .where(lt(property(EppRegistryModuleALoad.load().fromAlias("rel")), value(0)))
                        .where(eq(property(EppRegistryModuleALoad.module().id().fromAlias("rel")), property("module.id")))
                        .buildQuery()
                    ))

                    // нет контроля
                    .where(notExists(
                        new DQLSelectBuilder().fromEntity(EppRegistryModuleIControlAction.class, "rel").column(property("rel.id"))
                        .where(lt(property(EppRegistryModuleIControlAction.amount().fromAlias("rel")), value(0)))
                        .where(eq(property(EppRegistryModuleIControlAction.module().id().fromAlias("rel")), property("module.id")))
                        .buildQuery()
                    ))

                    .buildQuery()
                ))
                .createStatement(session).execute();
            } finally {
                lock.close();
            }
            return null;
        }

        @Override public void onEvent(final DSetEvent event)
        {

            // переводим в архив (только для тех, которые только что потеряли связь с дисциплиной)
            new DQLUpdateBuilder(EppRegistryModule.class)
            .where(in(
                property("id"),
                new DQLSelectBuilder().fromEntity(EppRegistryModule.class, "module").column(property("module.id"))

                // состояние
                .where(eq(property(EppRegistryModule.state().code().fromAlias("module")), value(EppState.STATE_FORMATIVE)))

                // только что удалили связь
                .where(in(
                    property("module.id"),
                    new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "rel")
                    .column(property(EppRegistryElementPartModule.module().id().fromAlias("rel")))
                    .where(in(property("rel.id"), event.getMultitude().getInExpression()))
                    .buildQuery()
                ))

                // нет связей ни с одной из дисциплин (кроме той, которую мы сейчас удаляем)
                .where(notExists(
                    new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "rel").column(property("rel.id"))
                    .where(eq(property(EppRegistryElementPartModule.module().id().fromAlias("rel")), property("module.id")))
                    .where(notIn(property("rel.id"), event.getMultitude().getInExpression()))
                    .buildQuery()
                ))

                // нет нагрузки
                .where(notExists(
                    new DQLSelectBuilder().fromEntity(EppRegistryModuleALoad.class, "rel").column(property("rel.id"))
                    .where(lt(property(EppRegistryModuleALoad.load().fromAlias("rel")), value(0)))
                    .where(eq(property(EppRegistryModuleALoad.module().id().fromAlias("rel")), property("module.id")))
                    .buildQuery()
                ))

                // нет контроля
                .where(notExists(
                    new DQLSelectBuilder().fromEntity(EppRegistryModuleIControlAction.class, "rel").column(property("rel.id"))
                    .where(lt(property(EppRegistryModuleIControlAction.amount().fromAlias("rel")), value(0)))
                    .where(eq(property(EppRegistryModuleIControlAction.module().id().fromAlias("rel")), property("module.id")))
                    .buildQuery()
                ))

                .buildQuery()
            ))
            .set(
                EppRegistryModule.L_STATE,
                new DQLSelectBuilder().fromEntity(EppState.class, "s").column(property("s.id"))
                .where(eq(property(EppState.code().fromAlias("s")), value(EppState.STATE_ARCHIVED)))
                .buildQuery()
            )
            .createStatement(event.getContext()).execute();

            this.register(event);
        }
    }

    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppRegistryElementPart.class, InsertRegistryElementPartListener.INSTANCE);
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, EppRegistryElementPartModule.class, DeleteRegistryElementPartModuleListener.INSTANCE);
    }

}
