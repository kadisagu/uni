/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.util;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;

import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public interface IYearEducationProcessSelectModel
{
    String YEAR_EDUCATION_PROCESS_FILTER_NAME = "yearEducationProcess";

    List<EppYearEducationProcess> getYearEducationProcessList();

    void setYearEducationProcessList(List<EppYearEducationProcess> yearEducationProcessList);

    IDataSettings getSettings();

    EppYearEducationProcess getYearEducationProcess();

    void setYearEducationProcess(EppYearEducationProcess yearEducationProcess);
}
