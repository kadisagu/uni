package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.EppDevelopResultManager;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvTotalRow;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.AttachRegElements.EppEduPlanVersionAttachRegElements;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockContent.EppEduPlanVersionBlockContentUIPresenter;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockContent.EppFakeRowWrapper;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockIndexEdit.EppEduPlanVersionBlockIndexEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.ControlHoursEdit.EppEduPlanVersionControlHoursEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.GroupRowAddEdit.EppEduPlanVersionGroupRowAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RegRowAddEdit.EppEduPlanVersionRegRowAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAdd.EppEduPlanVersionRowAdd;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAddEditBase.EppEduPlanVersionRowAddEditBaseUI;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.SelGroupRowAddEdit.EppEduPlanVersionSelGroupRowAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.StructRowAddEdit.EppEduPlanVersionStructRowAddEdit;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.gen.EppProfActivityTypeGen;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.data.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.ui.Form.ImtsaImportForm;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@State({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="blockHolder.id", required=true)
})
@Input({
    @Bind(key = "highlightRowId", binding = "highlightRowId"),
    @Bind(key = "scrollRowId", binding = "scrollRowId"),
    @Bind(key = "scroll", binding = "scroll"),
    @Bind(key = "scrollPos", binding = "scrollPos")
})
public class EppEduPlanVersionBlockPubUI extends EppEduPlanVersionBlockContentUIPresenter
{
    private final EntityHolder<EppEduPlanVersionBlock> blockHolder = new EntityHolder<>();
    private Collection<EppEpvTotalRow> totalRowList = Collections.emptyList();
    private Map<Long, IEppRegElWrapper> registryElementMap = null;

    private EppEpvTotalRow totalRow;

    public EntityHolder<EppEduPlanVersionBlock> getBlockHolder() { return blockHolder; }
    @Override public EppEduPlanVersionBlock getBlock() { return getBlockHolder().getValue(); }

    @Override
    public void onComponentRefresh()
    {
        // обновляем блок
        getBlockHolder().refresh(EppEduPlanVersionBlock.class);

        // грузим строки
        super.onComponentRefresh();

        // получем список элементов реестра
        this.setRegistryElementMap(this.getRegistryElementsWrappers(this.getRowWrapperList()));

        // итоговые данные

        this.setTotalRowList(IEppEduPlanVersionDataDAO.instance.get().getTotalRows(this.getBlockWrapper(), this.getAllRowWrapperList()));
    }

    @Override
    public void onComponentBindReturnParameters(String s, Map<String, Object> map)
    {
        Object warningMessage = map.get("warningMessage");
        if (warningMessage != null && warningMessage instanceof String && ((String) warningMessage).length() > 0)
            getSupport().info((String) warningMessage);
    }

    // actions

    public void onClickAddBlockElementNew() {
        if (!isEditable()) { return; }

        _uiActivation.asRegion(EppEduPlanVersionRowAdd.class).top()
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, getBlock().getId())
            .activate();
    }

    public void onClickEditRowNew() {
        if (!isEditable()) { return; }

        IEntity row = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        if (row instanceof EppEpvStructureRow) {
            _uiActivation.asRegion(EppEduPlanVersionStructRowAddEdit.class).top()
                .parameter(EppEduPlanVersionRowAddEditBaseUI.BIND_ROW_ID, getListenerParameterAsLong())
                .activate();
        } else if (row instanceof EppEpvGroupReRow) {
            _uiActivation.asRegion(EppEduPlanVersionGroupRowAddEdit.class).top()
                .parameter(EppEduPlanVersionRowAddEditBaseUI.BIND_ROW_ID, getListenerParameterAsLong())
                .activate();
        } else if (row instanceof EppEpvGroupImRow) {
            _uiActivation.asRegion(EppEduPlanVersionSelGroupRowAddEdit.class).top()
                .parameter(EppEduPlanVersionRowAddEditBaseUI.BIND_ROW_ID, getListenerParameterAsLong())
                .activate();
        } else if (row instanceof EppEpvRegistryRow) {
            _uiActivation.asRegion(EppEduPlanVersionRegRowAddEdit.class).top()
                .parameter(EppEduPlanVersionRowAddEditBaseUI.BIND_ROW_ID, getListenerParameterAsLong())
                .activate();
        }
    }

    public void onClickEditRow() {
        if (!isEditable()) { return; }
        getActivationBuilder().asRegion(ru.tandemservice.uniepp.component.eduplan.row.AddEdit.Model.class.getPackage().getName(), "dialog")
        .parameter(PublisherActivator.PUBLISHER_ID_KEY, getSupport().getListenerParameterAsLong())
        .activate();
    }

    public void onClickEditLoad() {
        if (!isEditable()) { return; }
        getActivationBuilder().asRegion(ru.tandemservice.uniepp.component.eduplan.row.TermDistribution.Model.class.getPackage().getName(), "dialog")
        .parameter(PublisherActivator.PUBLISHER_ID_KEY, getSupport().getListenerParameterAsLong())
        .activate();
    }

    public void onClickToggleCountInLoad() {
        if (!isEditable()) { return; }

        getSupport().setRefreshScheduled(true);
        DataAccessServices.dao().doInTransaction(session -> {
            final EppEpvRow row = DataAccessServices.dao().get(EppEpvRow.class, getSupport().getListenerParameterAsLong());
            row.setUsedInLoad(!row.getUsedInLoad());
            DataAccessServices.dao().update(row);
            return null;
        });
    }

    public void onClickToggleCountInActions() {
        if (!isEditable()) { return; }

        getSupport().setRefreshScheduled(true);
        DataAccessServices.dao().doInTransaction(session -> {
            final EppEpvRow row = DataAccessServices.dao().get(EppEpvRow.class, getSupport().getListenerParameterAsLong());
            row.setUsedInActions(!row.getUsedInActions());
            DataAccessServices.dao().update(row);
            return null;
        });
    }

    public void onClickAddBlockElement() {
        if (!isEditable()) { return; }

        getActivationBuilder().asRegion(ru.tandemservice.uniepp.component.eduplan.row.AddEdit.Model.class.getPackage().getName(), "dialog")
        .parameter(PublisherActivator.PUBLISHER_ID_KEY, getBlock().getId())
        .activate();
    }

    public void onClickMoveBlockElement() {
        if (!isEditable()) { return; }

        Collection<Long> rowIds = resetSelectedElements();
        if (rowIds.isEmpty()) {
            ContextLocal.getErrorCollector().add("Не выбрано ни одного элемента.");
            return;
        }

        getActivationBuilder().asRegion(ru.tandemservice.uniepp.component.eduplan.EduPlanVersionMoveBlockRow.Model.class.getPackage().getName(), "dialog")
        .parameter(PublisherActivator.PUBLISHER_ID_KEY, getBlock().getId())
        .parameter("rowIds", rowIds)
        .activate();
    }

    public void onClickDeleteBlockElement() {
        if (!isEditable()) { return; }

        final Collection<Long> rowIds = resetSelectedElements();
        if (rowIds.isEmpty()) {
            ContextLocal.getErrorCollector().add("Не выбрано ни одного элемента.");
            return;
        }

        getSupport().setRefreshScheduled(true);
        DataAccessServices.dao().doInTransaction(session -> {

            // грузим все строки блока (удалять я могу только свое)
            List<EppEpvRow> rows = HierarchyUtil.listHierarchyItemsWithParents(
                    new DQLSelectBuilder()
                            .fromEntity(EppEpvRow.class, "r").column("r")
                            .where(eq(property(EppEpvRow.owner().fromAlias("r").id()), value(getBlock().getId())))
                            .createStatement(session).<EppEpvRow>list()
            );
            Collections.reverse(rows);

            // удаляем в обратном порядке
            for (EppEpvRow row : rows)
            {
                EppEpvRow tmp = row;
                while (null != tmp)
                {
                    if (!getBlock().equals(tmp.getOwner()))
                    {
                        break; /* сразу нет*/
                    }
                    if (rowIds.contains(tmp.getId()))
                    {
                        session.delete(row);
                        break;
                    }
                    tmp = tmp.getHierarhyParent();
                }
            }

            // пересчитываем индекс
            IEppEduPlanVersionDataDAO.instance.get().doUpdateEpvRowStoredIndex(getBlock().getEduPlanVersion().getId());

            return null;
        });
    }

    public void onClickAttachRegElements()
    {
        if (!isEditable()) { return; }

        getActivationBuilder().asRegion(EppEduPlanVersionAttachRegElements.class.getSimpleName()).top()
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, getBlock().getId())
                .activate();
    }

    public void onClickAddRowFromEduPlanVersion() {
        if (!isEditable()) { return; }

        _uiActivation.asRegion(ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddRowFromOther.Model.class.getPackage().getName()).top()
        .parameter(PublisherActivator.PUBLISHER_ID_KEY, getBlock().getId())
        .activate();
    }

    public void onClickSetupRowOwners() {
        if (!isEditable()) { return; }

        _uiActivation.asRegion(ru.tandemservice.uniepp.component.eduplan.EduPlanVersionSetupRowsOwners.Model.class.getPackage().getName()).top()
        .parameter(PublisherActivator.PUBLISHER_ID_KEY, getBlock().getId())
        .activate();
    }

    public void onClickSetupIndexes() {
        if (!isEditable()) { return; }

        _uiActivation.asRegion(EppEduPlanVersionBlockIndexEdit.class).top()
        .parameter(PublisherActivator.PUBLISHER_ID_KEY, getBlock().getId())
        .activate();
    }

    public void onClickImportImtsa()
    {
        this.getActivationBuilder().asRegionDialog(ImtsaImportForm.class).parameter("block", getBlock().getId()).activate();
    }

    public void onClickSwitchToSimple() {
        getEduPlanVersion().setViewTableDiscipline(DataAccessServices.dao().get(EppViewTableDisciplines.class, EppViewTableDisciplines.code().s(), EppViewTableDisciplines.CODE_SIMPLE));
        DataAccessServices.dao().saveOrUpdate(getEduPlanVersion());
        onComponentRefresh();
    }

    public void onClickSwitchToExtended() {
        getEduPlanVersion().setViewTableDiscipline(DataAccessServices.dao().get(EppViewTableDisciplines.class, EppViewTableDisciplines.code().s(), EppViewTableDisciplines.CODE_EXTEND));
        DataAccessServices.dao().saveOrUpdate(getEduPlanVersion());
        onComponentRefresh();
    }

    public void onClickInteractiveHoursHide() {
        IUniBaseDao.instance.get().setProperty(getEduPlanVersion(), EppEduPlanVersion.P_HOURS_I, false);
        onComponentRefresh();
    }

    public void onClickInteractiveHoursShow() {
        IUniBaseDao.instance.get().setProperty(getEduPlanVersion(), EppEduPlanVersion.P_HOURS_I, true);
        onComponentRefresh();
    }

    public void onClickElectronicHoursHide() {
        IUniBaseDao.instance.get().setProperty(getEduPlanVersion(), EppEduPlanVersion.P_HOURS_E, false);
        onComponentRefresh();
    }

    public void onClickElectronicHoursShow() {
        IUniBaseDao.instance.get().setProperty(getEduPlanVersion(), EppEduPlanVersion.P_HOURS_E, true);
        onComponentRefresh();
    }

    public void onClickSpecifyControlHours() {
        if (!isEditable()) { return; }

        getActivationBuilder().asRegionDialog(EppEduPlanVersionControlHoursEdit.class)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, getBlock().getId())
                .activate();
    }

    // presenter

    @Override
    public boolean isViewOnly() {
        return false;
    }

    public boolean isCheckMode() {
        return true;
    }

    public String getBlockContentPageName() {
        if (isViewExtended()) {
            return EppEduPlanVersionBlockContentUIPresenter.class.getPackage().getName()+"."+"ModeExtended";
        }
        return EppEduPlanVersionBlockContentUIPresenter.class.getPackage().getName()+"."+"ModeBasic";
    }

    public boolean isViewExtended() { return getBlock().getEduPlanVersion().getViewTableDiscipline().isExtend(); }

    public OrgUnit getOwnerOrgUnit() {
        if (getBlock() instanceof EppEduPlanVersionSpecializationBlock) {
            return ((EppEduPlanVersionSpecializationBlock)getBlock()).getOwnerOrgUnit().getOrgUnit();
        }
        return null;
    }

    // columns

    @Override
    protected String getRowWrapperTermLoadRaw(String fullCode)
    {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        if (isCheckMode() && wrapper.isTermDataOwner())
        {
            final DevelopGridTerm gridTerm = this.getGridTerm();
            final int termNumber = gridTerm.getTermNumber();
            final double databaseValue = wrapper.getTotalInTermLoad(termNumber, fullCode);

            String databaseValueString = UniEppUtils.formatLoad(databaseValue, true);

            if ((null != registryElementMap) && (wrapper.getRow() instanceof EppEpvRegistryRow))
            {
                final EppRegistryElement regel = ((EppEpvRegistryRow)wrapper.getRow()).getRegistryElement();
                if (null != regel)
                {
                    final IEppRegElWrapper w = registryElementMap.get(regel.getId());
                    if ((null != w) && wrapper.hasInTermActivity(termNumber))
                    {
                        final int activeTermNumber = wrapper.getActiveTermNumber(termNumber);
                        if (activeTermNumber > regel.getSize())
                        {
                            // если части нет... это ахтунг
                            if (EppALoadType.FULL_CODE_TOTAL_LECTURES.equals(fullCode)) {
                                // ошибку пишем только для лекций
                                databaseValueString += " (нет части "+activeTermNumber+")";
                            }
                        }
                        else
                        {
                            final IEppRegElPartWrapper pw = w.getPartMap().get(activeTermNumber);
                            if (null != pw) {
                                // если часть есть
                                final double registryInTermValue = w.getLoadAsDouble(fullCode);
                                final double databaseInTermValue = wrapper.getTotalInTermLoad(termNumber, fullCode);
                                final double diff = registryInTermValue - databaseInTermValue;
                                if (!UniEppUtils.eq(diff, 0d))
                                {
                                    final String registryValueString = UniEppUtils.formatLoad(registryInTermValue, true);

                                    if (UniEppUtils.eq(databaseValue, databaseInTermValue)) {
                                        // если в часах в семестр показываем - то так и показываем
                                        databaseValueString = error(databaseValueString, registryValueString);
                                    } else {
                                        // придется показывать два значения - то, что на странице, и то, что в часах в семестр
                                        final String databaseInTermValueString = UniEppUtils.formatLoad(databaseInTermValue, true);
                                        databaseValueString = error(databaseInTermValueString+" ["+databaseValueString+"]", registryValueString);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return databaseValueString;
        }

        return super.getRowWrapperTermLoadRaw(fullCode);
    }

    @Override
    public String getRowWrapperTotalSizeRaw() {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        if (isCheckMode() && wrapper.isTermDataOwner() && !(wrapper instanceof EppFakeRowWrapper) && isSelfWorkPresent())
        {
            // доп. проверка, что сумма самостоятельных и аудиторных дает "Часов (всего)"
            final double totalLoad = wrapper.getTotalLoad(0, EppLoadType.FULL_CODE_TOTAL_HOURS);
            final double totalLoadCalculated = wrapper.getTotalLoad(0, EppELoadType.FULL_CODE_AUDIT) + wrapper.getTotalLoad(0, EppELoadType.FULL_CODE_SELFWORK);
            if (!UniEppUtils.eq(totalLoadCalculated, totalLoad)) {
                final String valueString = super.getRowWrapperTotalSize();
                final String calculatedValueString = UniEppUtils.formatLoad(totalLoadCalculated, true);
                return error(valueString, calculatedValueString);
            }
        }
        return super.getRowWrapperTotalSize();
    }

    @Override
    public String getRowWrapperTermLecturesRaw()
    {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        if (isCheckMode() && wrapper.isTermDataOwner())
        {
            // доп.проверка в лекциях (для расширенной формы)
            final DevelopGridTerm gridTerm = this.getGridTerm();
            final double calculatedTotalAuditValue = wrapper.getTotalInTermLoad(gridTerm.getTermNumber(), IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
            final double diff = (wrapper.getTotalInTermLoad(gridTerm.getTermNumber(), EppELoadType.FULL_CODE_AUDIT) - calculatedTotalAuditValue);
            if (!UniEppUtils.eq(diff, 0d)) {
                final double databaseValue = wrapper.getTotalInTermLoad(gridTerm.getTermNumber(), EppALoadType.FULL_CODE_TOTAL_LECTURES);
                final String calculatedValueString = UniEppUtils.formatLoad(databaseValue + diff, true);
                final String databaseValueString = super.getRowWrapperTermLecturesRaw();
                return error(databaseValueString, calculatedValueString);
            }
        }

        return super.getRowWrapperTermLecturesRaw();
    }

    @Override
    public String getRowWrapperTermAuditRaw() {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        if (isCheckMode() && wrapper.isTermDataOwner())
        {
            // доп.проверка в общих аудиторных (для простой формы)
            final DevelopGridTerm gridTerm = this.getGridTerm();
            final double databaseValue = wrapper.getTotalInTermLoad(gridTerm.getTermNumber(), EppELoadType.FULL_CODE_AUDIT);
            final double calculatedValue = wrapper.getTotalInTermLoad(gridTerm.getTermNumber(), IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
            final double diff = (databaseValue - calculatedValue);
            if (!UniEppUtils.eq(diff, 0d)) {
                final String calculatedValueString = UniEppUtils.formatLoad(calculatedValue, true);
                final String databaseValueString = UniEppUtils.formatLoad(databaseValue, true);
                return error(databaseValueString, calculatedValueString);
            }
        }

        return super.getRowWrapperTermAuditRaw();
    }

    public boolean isImtsaImportAvailable() { return this.getEduPlan() instanceof EppEduPlanHigherProf; }

    public boolean isShowProfActivityTypes()
    {
        return (getEduPlan() instanceof EppEduPlanProf);
    }

    public String getProfActivityTypes()
    {
        List<EppProfActivityType> profActivityTypeList = EppDevelopResultManager.instance().dao().getProfActivityTypes(blockHolder.getId());
        return profActivityTypeList.stream().map(EppProfActivityTypeGen::getTitle).collect(Collectors.joining("\n"));
    }

    // utils

    // возвращяет набор оболочек элементов реестра для проверки
    @SuppressWarnings("unused")
    protected Map<Long, IEppRegElWrapper> getRegistryElementsWrappers(final Collection<IEppEpvRowWrapper> blockRows) {

        // пока не делаем такую проверку (она взорвет им мозг)
        if (true) { return null; }

        if (!this.isCheckMode()) { return null; }

        final Set<Long> regElIds = new HashSet<>(blockRows.size());
        for (final IEppEpvRowWrapper wrapper: blockRows) {
            if (wrapper.getRow() instanceof EppEpvRegistryRow) {
                final EppRegistryElement registryElement = ((EppEpvRegistryRow)wrapper.getRow()).getRegistryElement();
                if (null != registryElement) { regElIds.add(registryElement.getId()); }
            }
        }

        return IEppRegistryDAO.instance.get().getRegistryElementDataMap(regElIds);
    }

    // columns (total-row)

    public String getTotalRowTitle() {
        return getTotalRow().getTitle();
    }

    public String getTotalRowControlAction() {
        return getTotalRow().getControlActionValue(getControlAction().getFullCode());
    }

    public String getTotalRowTotalLabor() { return getTotalRow().getLoadValue(0, EppLoadType.FULL_CODE_LABOR); }
    public String getTotalRowTotalSize() { return getTotalRow().getLoadValue(0, EppLoadType.FULL_CODE_TOTAL_HOURS); }
    public String getTotalRowTotalAudit() { return getTotalRow().getLoadValue(0, EppELoadType.FULL_CODE_AUDIT); }
    public String getTotalRowTotalSelfWork() { return getTotalRow().getLoadValue(0, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL); }
    public String getTotalRowTotalControl() { return getTotalRow().getLoadValue(0, EppLoadType.FULL_CODE_CONTROL); }
    public String getTotalRowTotalLectures() { return getTotalRow().getLoadValue(0, EppALoadType.FULL_CODE_TOTAL_LECTURES); }
    public String getTotalRowTotalPractices() { return getTotalRow().getLoadValue(0, EppALoadType.FULL_CODE_TOTAL_PRACTICE); }
    public String getTotalRowTotalLabs() { return getTotalRow().getLoadValue(0, EppALoadType.FULL_CODE_TOTAL_LABS); }

    public String getTotalRowTermLabor() { return getTotalRow().getLoadValue(getGridTerm().getTermNumber(), EppLoadType.FULL_CODE_LABOR); }
    public String getTotalRowTermAudit() { return getTotalRow().getLoadValue(getGridTerm().getTermNumber(), EppELoadType.FULL_CODE_AUDIT); }
    public String getTotalRowTermSelfWork() { return getTotalRow().getLoadValue(getGridTerm().getTermNumber(), EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL); }
    public String getTotalRowTermControl() { return getTotalRow().getLoadValue(getGridTerm().getTermNumber(), EppLoadType.FULL_CODE_CONTROL); }
    public String getTotalRowTermLectures() { return getTotalRow().getLoadValue(getGridTerm().getTermNumber(), EppALoadType.FULL_CODE_TOTAL_LECTURES); }
    public String getTotalRowTermPractices() { return getTotalRow().getLoadValue(getGridTerm().getTermNumber(), EppALoadType.FULL_CODE_TOTAL_PRACTICE); }
    public String getTotalRowTermLabs() { return getTotalRow().getLoadValue(getGridTerm().getTermNumber(), EppALoadType.FULL_CODE_TOTAL_LABS); }

    public boolean isTotalRowMergeTermCells() { return getTotalRow().isMergeTermCells(); }

    // getter & setters

    public Map<Long, IEppRegElWrapper> getRegistryElementMap() { return this.registryElementMap; }
    public void setRegistryElementMap(Map<Long, IEppRegElWrapper> registryElementMap) { this.registryElementMap = registryElementMap; }

    public Collection<EppEpvTotalRow> getTotalRowList() { return this.totalRowList; }
    public void setTotalRowList(Collection<EppEpvTotalRow> totalRowList) { this.totalRowList = totalRowList; }

    public EppEpvTotalRow getTotalRow() { return this.totalRow; }
    public void setTotalRow(EppEpvTotalRow totalRow) { this.totalRow = totalRow; }
}
