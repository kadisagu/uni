/* $Id:$ */
package ru.tandemservice.uniepp.component.indicators.EppRegistryCheck;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.sec.OrgUnitHolder;

/**
 * @author oleyba
 * @since 5/30/12
 */
@State
({
    @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=8945678")
public class Model
{
    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    private CommonPostfixPermissionModel sec;

    public OrgUnit getOrgUnit()
    {
        return getOrgUnitHolder().getValue();
    }

    public OrgUnitHolder getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public CommonPostfixPermissionModel getSec()
    {
        return sec;
    }

    public void setSec(CommonPostfixPermissionModel sec)
    {
        this.sec = sec;
    }
}
