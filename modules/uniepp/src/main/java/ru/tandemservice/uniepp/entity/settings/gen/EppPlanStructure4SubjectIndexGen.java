package ru.tandemservice.uniepp.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь элементов ГОСов / УП с перечнями направлений
 *
 * Определяет какие структурные элементы (индексы, компоненты) доступны для каких перечней направлений
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppPlanStructure4SubjectIndexGen extends EntityBase
 implements INaturalIdentifiable<EppPlanStructure4SubjectIndexGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex";
    public static final String ENTITY_NAME = "eppPlanStructure4SubjectIndex";
    public static final int VERSION_HASH = -1540618126;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPP_PLAN_STRUCTURE = "eppPlanStructure";
    public static final String L_EDU_PROGRAM_SUBJECT_INDEX = "eduProgramSubjectIndex";

    private EppPlanStructure _eppPlanStructure;     // Структура ГОС/УП
    private EduProgramSubjectIndex _eduProgramSubjectIndex;     // Перечень направлений подготовки и квалификаций профессионального образования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Структура ГОС/УП. Свойство не может быть null.
     */
    @NotNull
    public EppPlanStructure getEppPlanStructure()
    {
        return _eppPlanStructure;
    }

    /**
     * @param eppPlanStructure Структура ГОС/УП. Свойство не может быть null.
     */
    public void setEppPlanStructure(EppPlanStructure eppPlanStructure)
    {
        dirty(_eppPlanStructure, eppPlanStructure);
        _eppPlanStructure = eppPlanStructure;
    }

    /**
     * @return Перечень направлений подготовки и квалификаций профессионального образования. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubjectIndex getEduProgramSubjectIndex()
    {
        return _eduProgramSubjectIndex;
    }

    /**
     * @param eduProgramSubjectIndex Перечень направлений подготовки и квалификаций профессионального образования. Свойство не может быть null.
     */
    public void setEduProgramSubjectIndex(EduProgramSubjectIndex eduProgramSubjectIndex)
    {
        dirty(_eduProgramSubjectIndex, eduProgramSubjectIndex);
        _eduProgramSubjectIndex = eduProgramSubjectIndex;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppPlanStructure4SubjectIndexGen)
        {
            if (withNaturalIdProperties)
            {
                setEppPlanStructure(((EppPlanStructure4SubjectIndex)another).getEppPlanStructure());
                setEduProgramSubjectIndex(((EppPlanStructure4SubjectIndex)another).getEduProgramSubjectIndex());
            }
        }
    }

    public INaturalId<EppPlanStructure4SubjectIndexGen> getNaturalId()
    {
        return new NaturalId(getEppPlanStructure(), getEduProgramSubjectIndex());
    }

    public static class NaturalId extends NaturalIdBase<EppPlanStructure4SubjectIndexGen>
    {
        private static final String PROXY_NAME = "EppPlanStructure4SubjectIndexNaturalProxy";

        private Long _eppPlanStructure;
        private Long _eduProgramSubjectIndex;

        public NaturalId()
        {}

        public NaturalId(EppPlanStructure eppPlanStructure, EduProgramSubjectIndex eduProgramSubjectIndex)
        {
            _eppPlanStructure = ((IEntity) eppPlanStructure).getId();
            _eduProgramSubjectIndex = ((IEntity) eduProgramSubjectIndex).getId();
        }

        public Long getEppPlanStructure()
        {
            return _eppPlanStructure;
        }

        public void setEppPlanStructure(Long eppPlanStructure)
        {
            _eppPlanStructure = eppPlanStructure;
        }

        public Long getEduProgramSubjectIndex()
        {
            return _eduProgramSubjectIndex;
        }

        public void setEduProgramSubjectIndex(Long eduProgramSubjectIndex)
        {
            _eduProgramSubjectIndex = eduProgramSubjectIndex;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppPlanStructure4SubjectIndexGen.NaturalId) ) return false;

            EppPlanStructure4SubjectIndexGen.NaturalId that = (NaturalId) o;

            if( !equals(getEppPlanStructure(), that.getEppPlanStructure()) ) return false;
            if( !equals(getEduProgramSubjectIndex(), that.getEduProgramSubjectIndex()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEppPlanStructure());
            result = hashCode(result, getEduProgramSubjectIndex());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEppPlanStructure());
            sb.append("/");
            sb.append(getEduProgramSubjectIndex());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppPlanStructure4SubjectIndexGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppPlanStructure4SubjectIndex.class;
        }

        public T newInstance()
        {
            return (T) new EppPlanStructure4SubjectIndex();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eppPlanStructure":
                    return obj.getEppPlanStructure();
                case "eduProgramSubjectIndex":
                    return obj.getEduProgramSubjectIndex();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eppPlanStructure":
                    obj.setEppPlanStructure((EppPlanStructure) value);
                    return;
                case "eduProgramSubjectIndex":
                    obj.setEduProgramSubjectIndex((EduProgramSubjectIndex) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eppPlanStructure":
                        return true;
                case "eduProgramSubjectIndex":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eppPlanStructure":
                    return true;
                case "eduProgramSubjectIndex":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eppPlanStructure":
                    return EppPlanStructure.class;
                case "eduProgramSubjectIndex":
                    return EduProgramSubjectIndex.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppPlanStructure4SubjectIndex> _dslPath = new Path<EppPlanStructure4SubjectIndex>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppPlanStructure4SubjectIndex");
    }
            

    /**
     * @return Структура ГОС/УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex#getEppPlanStructure()
     */
    public static EppPlanStructure.Path<EppPlanStructure> eppPlanStructure()
    {
        return _dslPath.eppPlanStructure();
    }

    /**
     * @return Перечень направлений подготовки и квалификаций профессионального образования. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex#getEduProgramSubjectIndex()
     */
    public static EduProgramSubjectIndex.Path<EduProgramSubjectIndex> eduProgramSubjectIndex()
    {
        return _dslPath.eduProgramSubjectIndex();
    }

    public static class Path<E extends EppPlanStructure4SubjectIndex> extends EntityPath<E>
    {
        private EppPlanStructure.Path<EppPlanStructure> _eppPlanStructure;
        private EduProgramSubjectIndex.Path<EduProgramSubjectIndex> _eduProgramSubjectIndex;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Структура ГОС/УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex#getEppPlanStructure()
     */
        public EppPlanStructure.Path<EppPlanStructure> eppPlanStructure()
        {
            if(_eppPlanStructure == null )
                _eppPlanStructure = new EppPlanStructure.Path<EppPlanStructure>(L_EPP_PLAN_STRUCTURE, this);
            return _eppPlanStructure;
        }

    /**
     * @return Перечень направлений подготовки и квалификаций профессионального образования. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex#getEduProgramSubjectIndex()
     */
        public EduProgramSubjectIndex.Path<EduProgramSubjectIndex> eduProgramSubjectIndex()
        {
            if(_eduProgramSubjectIndex == null )
                _eduProgramSubjectIndex = new EduProgramSubjectIndex.Path<EduProgramSubjectIndex>(L_EDU_PROGRAM_SUBJECT_INDEX, this);
            return _eduProgramSubjectIndex;
        }

        public Class getEntityClass()
        {
            return EppPlanStructure4SubjectIndex.class;
        }

        public String getEntityName()
        {
            return "eppPlanStructure4SubjectIndex";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
