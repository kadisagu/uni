package ru.tandemservice.uniepp.component.registry.ModuleRegistry.Pub;


import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 03.08.11 19:09
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void setShared(final Model model)
    {
        final EppRegistryModule module = model.getModule();
        module.setShared(!module.isShared());
        this.save(module);
    }

    @Override
    public void prepare(final Model model)
    {
        model.setModule(this.getNotNull(EppRegistryModule.class, model.getModule().getId()));
        this.prepareListDataSource(model);
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        final DynamicListDataSource<EppRegistryModuleALoad> loadTypeDataSource = model.getLoadTypeDataSource();
        final List<EppRegistryModuleALoad> loadTypeList = new DQLSelectBuilder().fromEntity(EppRegistryModuleALoad.class, "lt")
        .where(eq(property(EppRegistryModuleALoad.module().fromAlias("lt")), value(model.getModule())))
        .column(property("lt"))
        .createStatement(this.getSession()).list();
        loadTypeDataSource.setCountRow(loadTypeList.size());
        model.setHasLoad(loadTypeList.size()>0);
        UniBaseUtils.createPage(loadTypeDataSource, loadTypeList);

        final DynamicListDataSource<EppRegistryModuleIControlAction> controlActionDataSource = model.getControlActionDataSource();
        final List<EppRegistryModuleIControlAction> controlActionList = new DQLSelectBuilder().fromEntity(EppRegistryModuleIControlAction.class, "ca")
        .where(eq(property(EppRegistryModuleIControlAction.module().fromAlias("ca")), value(model.getModule())))
        .column(property("ca"))
        .createStatement(this.getSession()).list();
        controlActionDataSource.setCountRow(controlActionList.size());
        model.setHasCA(controlActionList.size()>0);
        UniBaseUtils.createPage(controlActionDataSource, controlActionList);

        final DynamicListDataSource<EppRegistryElementPartModule> disciplineActionDataSource = model.getDisciplineDataSource();
        final List<EppRegistryElementPartModule> disciplineList = new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "d")
        .where(eq(property(EppRegistryElementPartModule.module().fromAlias("d")), value(model.getModule())))
        .column(property("d"))
        .createStatement(this.getSession()).list();
        disciplineActionDataSource.setCountRow(disciplineList.size());
        UniBaseUtils.createPage(disciplineActionDataSource, disciplineList);

        final List<ViewWrapper<IEntity>> wrappers = ViewWrapper.getPatchedList(model.getDisciplineDataSource());
        BatchUtils.execute(wrappers, 100, new BatchUtils.Action<ViewWrapper<IEntity>>()
                {
            @Override
            public void execute(final Collection<ViewWrapper<IEntity>> elements)
            {
                for (final ViewWrapper<IEntity> element : elements)
                {
                    final EppRegistryElement regElement = ((EppRegistryElementPartModule) element.getEntity()).getPart().getRegistryElement();
                    Map<String, Double> loadMap = new HashMap<>();
                    Map<String, Integer> fcaMap = new HashMap<>();

                    final List<EppRegistryModuleALoad> aLoadList = new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "d")
                        .where(eq(property(EppRegistryElementPartModule.part().registryElement().fromAlias("d")), value(regElement)))
                        .joinEntity("d", DQLJoinType.inner, EppRegistryModuleALoad.class, "al", DQLExpressions.eq(property(EppRegistryElementPartModule.module().fromAlias("d")), property(EppRegistryModuleALoad.module().fromAlias("al"))))
                        .column(property("al"))
                        .createStatement(getSession()).list();

                    for (final EppRegistryModuleALoad al : aLoadList)
                    {
                        if (loadMap.containsKey(al.getLoadType().getFullCode()))
                            loadMap.put(al.getLoadType().getFullCode(), loadMap.get(al.getLoadType().getFullCode())+al.getLoadAsDouble());
                        else
                            loadMap.put(al.getLoadType().getFullCode(), al.getLoadAsDouble());
                    }

                    for (final EppALoadType t : getCatalogItemListOrderByCode(EppALoadType.class))
                        if (loadMap.containsKey(t.getFullCode()))
                            element.setViewProperty(t.getFullCode(), loadMap.get(t.getFullCode()));
                        else
                            element.setViewProperty(t.getFullCode(), 0.0);

                    //----
                    final List<EppRegistryElementPartFControlAction> fcaList = new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, "d")
                        .where(eq(property(EppRegistryElementPartFControlAction.part().registryElement().fromAlias("d")), value(regElement)))
                        .column(property("d"))
                        .createStatement(getSession()).list();

                    for (final EppRegistryElementPartFControlAction fca : fcaList)
                    {
                        if (fcaMap.containsKey(fca.getControlAction().getFullCode()))
                            fcaMap.put(fca.getControlAction().getFullCode(), fcaMap.get(fca.getControlAction().getFullCode())+1);
                        else
                            fcaMap.put(fca.getControlAction().getFullCode(), 1);
                    }

                    for (String fcaCode : fcaMap.keySet())
                        element.setViewProperty(fcaCode, fcaMap.get(fcaCode));

                }

            }
        });
    }

}
