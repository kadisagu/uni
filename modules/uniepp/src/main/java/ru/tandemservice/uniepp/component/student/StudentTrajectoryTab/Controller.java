package ru.tandemservice.uniepp.component.student.StudentTrajectoryTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        component.createChildRegion("trajectoryRegion", new ComponentActivator(
                ru.tandemservice.uniepp.component.workplan.WorkPlanTrajectoryPub.Model.COMPONENT,
                new ParametersMap().add("ids", this.getDao().getWorkPlanIds(model)).add("showWorkplans", true)
        ));
    }
}
