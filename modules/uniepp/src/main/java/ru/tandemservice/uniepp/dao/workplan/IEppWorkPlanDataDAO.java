package ru.tandemservice.uniepp.dao.workplan;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;

import java.util.Collection;
import java.util.Map;

/**
 * @author vdanilov
 */
public interface IEppWorkPlanDataDAO {
    SpringBeanCache<IEppWorkPlanDataDAO> instance = new SpringBeanCache<IEppWorkPlanDataDAO>(IEppWorkPlanDataDAO.class.getName());

    /**
     * @param eppWorkPlanIds - идентификаторы РУП (not-null)
     * @return { workplan.id -> wrapper(workplan) }
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<Long, IEppWorkPlanWrapper> getWorkPlanDataMap(Collection<Long> eppWorkPlanIds);

    /**
     * Загружает нагрузку для указанных строк РУП
     * @return rowLoadMap { row.id -> { part.number -> { action.code -> EppWorkPlanRowPartLoad } } }
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> getRowPartLoadDataMap(final Collection<Long> rowIds);

    /**
     * Обновляет нагрузку для частей строки РУП (но не даты реализации нагрузки)
     * @param partLoadMap { row.id -> { part.number -> { loadType.fullcode -> EppWorkPlanRowPartLoad } } }
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateRowPartLoadElementsMap(Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> partLoadMap);

    /**
     * Обновляет сроки обучения для частей строки РУП (но не состав и величину нагрузки)
     * @param partLoadPeriodMap { row.id -> { part.number -> { loadType.fullcode -> EppWorkPlanRowPartLoad } } }
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateRowPartLoadPeriodsMap(Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> partLoadPeriodMap);



    /**
     * Возвращает модель для выбора элементов реестра дисциплин
     * которые можно добавлять в РУП
     * 
     * @param workplan - РУП
     * @param registryDisciplineIdsIterable - список цисциплин, которые необходимо включить не смотря ни на какие условия (например, при редактировании включать саму дисциплину)
     * @return ISelectModel
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    ISelectModel getRegistryDisciplineSelectModel4Add(EppWorkPlanBase workplan, Iterable<Long> registryDisciplineIdsIterable);


    /**
     * Возвращает модель для выбора элементов реестра мероприятий (практик и ГИА)
     * которые можно добавлять в РУП
     * 
     * @param workplan - РУП
     * @param registryStructureIdsIterable - список элементов структуры реестра, из которых необходимо выбирать значения
     * @param registryActionIdsIterable - список мероприятий, которые необходимо включить не смотря ни на какие условия (например, при редактировании)
     * @return ISelectModel
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    ISelectModel getRegistryActionSelectModel4Add(EppWorkPlanBase workplan, Iterable<Long> registryStructureIdsIterable, Iterable<Long> registryActionIdsIterable);


}
