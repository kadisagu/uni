package ru.tandemservice.uniepp.component.workplan.WorkPlanDisciplineAddFromEduPlan;

import org.apache.commons.collections15.Predicate;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.EppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.WorkPlanRowSource;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.ui.EduPlanVersionBlockDataSourceGenerator;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO
{
    private static final String TERM_SELECT_COLUMN = "termSelectColumn";
    private static final String DATE_START_COLUMN = "dateStartColumn";
    private static final String DATE_END_COLUMN = "dateEndColumn";

    @Override
    public void prepare(final Model model)
    {
        final EppWorkPlanBase eppWorkPlan = model.getWorkPlanHolder().refresh(EppWorkPlanBase.class);
        eppWorkPlan.getState().check_editable(eppWorkPlan);

        final EduPlanVersionBlockDataSourceGenerator gen = new EduPlanVersionBlockDataSourceGenerator() {
            @Override public boolean isExtendedView() { return false; }
            @Override public boolean isCheckMode() { return false; }
            @Override protected SimpleColumn getColumn_number() { return null; }
            @Override protected Long getVersionBlockId() { return eppWorkPlan.getBlock().getId(); }
            @Override protected String getPermissionKeyEdit() { return null; }
            @Override protected boolean isEditable() { return false; }

            @Override protected AbstractColumn wrapTermLoadColumn(final DevelopGridTerm term, final AbstractColumn column) {
                column.setStyleResolver(rowEntity -> {
                    final IEppEpvRowWrapper rowWrapper = (IEppEpvRowWrapper)rowEntity;
                    if (rowWrapper.getRow() instanceof IEppEpvTermDistributedRow) {
                        if (rowWrapper.hasInTermActivity(term.getTermNumber())) {
                            return "background-color:"+ UniDefines.COLOR_BLUE;
                        }
                    }
                    return null;
                });
                return column;
            }
        };

        final EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext renderContext = new EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext() {

            private final Predicate<IEppEpvRowWrapper> workPlanRowPredicate = new Predicate<IEppEpvRowWrapper>()
            {
                private final Set<Long> workPlanDisciplineIds = new HashSet<>(
                        new DQLSelectBuilder()
                                .fromEntity(EppWorkPlanRegistryElementRow.class, "r")
                                .where(eq(property(EppWorkPlanRegistryElementRow.workPlan().fromAlias("r")), value(eppWorkPlan)))
                                .column(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias("r")))
                                .createStatement(DAO.this.getSession()).<Long>list()
                );

                @Override public boolean evaluate(final IEppEpvRowWrapper row) {
                    if (row.getRow() instanceof EppEpvRegistryRow)
                    {
                        final EppRegistryElement registryElement = ((EppEpvRegistryRow)row.getRow()).getRegistryElement();
                        if (null == registryElement) {
                            // проверяем, что элемент указан
                            return false;
                        }

                        if (this.workPlanDisciplineIds.contains(registryElement.getId())) {
                            // проаеряем, что дисциплина не добавлена
                            return false;
                        }

                        // проверяем, что это action или не action
                        return model.isActions() ? IEppEpvRowWrapper.ACTIONS_ROWS.evaluate(row) : IEppEpvRowWrapper.DISCIPLINES_ROWS.evaluate(row);
                    }
                    return false;
                }
            };

            @Override public IEntityHandler getAdditionalEditDisabler() { return null; }
            @Override public Collection<IEppEpvRowWrapper> getBlockRows(final IEppEpvBlockWrapper versionWrapper) {
                return EppEduPlanVersionDataDAO.filterEduPlanBlockRows(versionWrapper.getRowMap(), this.workPlanRowPredicate, false);
            }
        };

        if (model.isActions()) {
            gen.doPrepareBlockActionsDataSource(model.getRowDataSource(), renderContext);
        } else {
            gen.doPrepareBlockDisciplineDataSource(model.getRowDataSource(), renderContext);
        }

        {
            final BlockColumn<SelectModel<Integer>> column = new BlockColumn<>(DAO.TERM_SELECT_COLUMN, "Семестр", "termSelectBlock");
            column.setValueMap(new HashMap<>());

            final DevelopGridTerm gridTerm = eppWorkPlan.getGridTerm();
            final Long developGridId = gridTerm.getDevelopGrid().getId();
            final Collection<Integer> availableTerm = IDevelopGridDAO.instance.get().getDevelopGridMap(developGridId).keySet();
            for (final IEppEpvRowWrapper wrapper : model.getRowDataSource().getEntityList()) {
                final Set<Integer> activeTerms = new TreeSet<>(wrapper.getActiveTermSet());
                for (final Integer term: availableTerm) {
                    if ((!activeTerms.contains(term)) && EppWorkPlanDAO.canCreateFakeRowFromGroupRow(term, wrapper)) {
                        activeTerms.add(term);
                    }
                }

                if (activeTerms.size() > 0) {
                    final SelectModel<Integer> selector = new SelectModel<>(new BaseSingleSelectModel("","") {
                        @Override public Object getValue(final Object primaryKey) { return primaryKey; }
                        @Override public ListResult findValues(final String filter) { return new ListResult<>(activeTerms); }
                    });
                    if (activeTerms.contains(gridTerm.getTermNumber())) {
                        selector.setValue(gridTerm.getTermNumber());
                    }
                    column.getValueMap().put(wrapper.getId(), selector);
                }
            }
            column.setStyle("margin:0px");
            model.getRowDataSource().addColumn(column.setWidth(1));
        }

        if (model.isActions())
        {
            final BlockColumn<Date> dateStartColumn = new BlockColumn<>(DAO.DATE_START_COLUMN, "Дата начала", "dateStartBlock");
            dateStartColumn.setValueMap(new HashMap<>());
            dateStartColumn.setStyle("margin:0px");
            model.getRowDataSource().addColumn(dateStartColumn.setWidth(1));

            final BlockColumn<Date> dateEndColumn = new BlockColumn<>(DAO.DATE_END_COLUMN, "Дата окончания", "dateEndBlock");
            dateEndColumn.setValueMap(new HashMap<>());
            dateEndColumn.setStyle("margin:0px");
            model.getRowDataSource().addColumn(dateEndColumn.setWidth(1));
        }
    }


    @Override
    @SuppressWarnings("unchecked")
    public void update(final Model model)
    {
        final BlockColumn<SelectModel<Integer>> column = (BlockColumn<SelectModel<Integer>>) model.getRowDataSource().getColumn(DAO.TERM_SELECT_COLUMN);
        final BlockColumn<Date> dateStartColumn = model.isActions() ? (BlockColumn<Date>) model.getRowDataSource().getColumn(DAO.DATE_START_COLUMN) : null;
        final BlockColumn<Date> dateEndColumn = model.isActions() ? (BlockColumn<Date>) model.getRowDataSource().getColumn(DAO.DATE_END_COLUMN) : null;

        final ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        final Map<Long, WorkPlanRowSource> map = new HashMap<>(model.getRowDataSource().getEntityList().size());

        // validate

        for (final IEppEpvRowWrapper wrapper : model.getRowDataSource().getEntityList()) {

            final Long id = wrapper.getId();

            final SelectModel<Integer> selector = column.getValueMap().get(id);
            final Date dateStart = null == dateStartColumn ? null : dateStartColumn.getValueMap().get(id);
            final Date dateEnd = null == dateEndColumn ? null : dateEndColumn.getValueMap().get(id);
            final Integer term = null != selector ? selector.getValue() : null;

            if (dateStart != null || dateEnd != null) {
                if (dateStart != null && dateEnd != null && dateEnd.before(dateStart)) {
                    errorCollector.add("Дата начала должна быть не позже даты окончания.", "dateStart_" + id, "dateEnd_"  + id);
                }

                if (null == term) {
                    errorCollector.add("Для создания строки необходимо указать семестр.", "term_" + id);
                }
            }

            if (null != term) {
                map.put(id, new WorkPlanRowSource(term, dateStart, dateEnd));
            }
        }

        // save

        if (!errorCollector.hasErrors() && !map.isEmpty()) {
            final EppWorkPlanBase eppWorkPlan = model.getWorkPlanHolder().refresh(EppWorkPlanBase.class);
            IEppWorkPlanDAO.instance.get().doGenerateWorkPlanRowsFromEpvBlock(eppWorkPlan.getId(), eppWorkPlan.getBlock().getId(), map::get);
        }
    }
}
