/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.ui.RegistryElementCorrectionElementsTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.ReorganizationCodeEdit.OrgUnitReorganizationCodeEdit;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IHierarchicalSelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import org.tandemframework.tapsupport.component.selection.hselect.IHSelectModel;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractListUI;
import ru.tandemservice.uniepp.base.bo.EppReorganization.ui.EditRegistryElementOwner.EppReorganizationEditRegistryElementOwner;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author rsizonenko
 * @since 30.03.2016
 */
public class EppReorganizationRegistryElementCorrectionElementsTabUI extends EppRegistryAbstractListUI {

    @Override public String getMenuPermission() { return "unieppReorganizationRegistryElementCorrection"; }
    @Override public String getPermissionPrefix() { return null; }
    @Override public Class getElementClass() { return EppRegistryElement.class; }

    private List<HSelectOption> structureModel;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        structureModel = HierarchyUtil.listHierarchyNodesWithParents(DataAccessServices.dao().getList(EppRegistryStructure.class), true);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put("structure", getSettings().get("structure"));
        saveSettings();
    }

    public boolean isOwnerSelected()
    {
        return null != getSettings().get("owner");
    }

    public void onClickEditReorganizationId()
    {
        final OrgUnit owner = getSettings().get("owner");
        getActivationBuilder().asRegionDialog(OrgUnitReorganizationCodeEdit.class).parameter(UIPresenter.PUBLISHER_ID, owner.getId()).activate();
    }

    public void onClickChangeRegistryElementOwner()
    {
        final Collection<IEntity> selected = ((PageableSearchListDataSource) getConfig().getDataSource(EppReorganizationRegistryElementCorrectionElementsTab.ELEMENT_DS)).getOptionColumnSelectedObjects("checkboxColumn");
        if (selected.size() == 0) throw new ApplicationException("Не выбран ни один элемент из списка.");
        getActivationBuilder().asRegionDialog(EppReorganizationEditRegistryElementOwner.class)
                .parameter("multiValue",
                        selected.stream().collect(Collectors.toList())
                ).activate();
    }

    public void onClickResetFilters()
    {
        CommonBaseSettingsUtil.clearSettingsExcept(getSettings(), SETTING_OWNER);
    }

    public List<HSelectOption> getStructureModel() {
        return structureModel;
    }
}
