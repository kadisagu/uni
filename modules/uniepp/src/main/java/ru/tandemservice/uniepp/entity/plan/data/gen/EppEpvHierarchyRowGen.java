package ru.tandemservice.uniepp.entity.plan.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvHierarchyRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка УП (группирующая)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvHierarchyRowGen extends EppEpvRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.data.EppEpvHierarchyRow";
    public static final String ENTITY_NAME = "eppEpvHierarchyRow";
    public static final int VERSION_HASH = 1279139947;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEpvHierarchyRowGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvHierarchyRowGen> extends EppEpvRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvHierarchyRow.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppEpvHierarchyRow is abstract");
        }
    }
    private static final Path<EppEpvHierarchyRow> _dslPath = new Path<EppEpvHierarchyRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvHierarchyRow");
    }
            

    public static class Path<E extends EppEpvHierarchyRow> extends EppEpvRow.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EppEpvHierarchyRow.class;
        }

        public String getEntityName()
        {
            return "eppEpvHierarchyRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
