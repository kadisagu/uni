package ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit;

import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;

/**
 * @author vdanilov
 */
public interface IDAO extends ru.tandemservice.uniepp.component.registry.base.AddEdit.IDAO<EppRegistryDiscipline> {
}
