package ru.tandemservice.uniepp.base.bo.EppContract.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface IEppContractDaemonBean
{

    public static final String GLOBAL_DAEMON_LOCK = "eppContractDao.lock";

    public static final SpringBeanCache<IEppContractDaemonBean> instance = new SpringBeanCache<IEppContractDaemonBean>(IEppContractDaemonBean.class.getName());

    /**
     * обновляет объекты eppCtrEducationResult по текущим студентам
     * @return true, если что-то изменилось
     **/
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    boolean doUpdateEppCtrEducationResult();

}
