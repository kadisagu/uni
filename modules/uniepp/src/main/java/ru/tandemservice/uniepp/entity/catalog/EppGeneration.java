package ru.tandemservice.uniepp.entity.catalog;

/**
 * Поколение (ГОС/УП/РУП)
 */
public enum EppGeneration
{
    GENERATION_2 (2, "2-е поколение"), GENERATION_3 (3, "3-е поколение");

    private int number;
    private String title;

    public int getNumber()
    {
        return number;
    }

    public String getTitle()
    {
        return title;
    }

    EppGeneration(int number, String title)
    {
        this.number = number;
        this.title = title;
    }

    public boolean showTotalLabor() {
        return number == 3;
    }

    public boolean showCompetencies() {
        return number == 3;
    }

    public String getShortTitle() {
        return getNumber() == 2 ? "ГОС 2" : "ФГОС 3";
    }
}