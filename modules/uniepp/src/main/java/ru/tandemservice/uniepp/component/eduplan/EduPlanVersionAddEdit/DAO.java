package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit;

import com.google.common.collect.Iterables;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.EppViewTableDisciplines;
import ru.tandemservice.uniepp.entity.catalog.gen.EppStateGen;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        final IEntity entity = (null == model.getContextId() ? null : this.get(model.getContextId()));

        if (entity instanceof EppEduPlanVersion) {
            model.setElement((EppEduPlanVersion) entity);
        } else if (entity instanceof EppEduPlan && null == model.getElement()) {
            model.setElement(new EppEduPlanVersion());
            model.getElement().setEduPlan((EppEduPlan) entity);
            model.getElement().setNumber(INumberQueueDAO.instance.get().getNextNumber(model.getElement()));
        } else {
            throw new IllegalStateException(null == entity ? null : entity.getClass().getName());
        }

        if (null == model.getElement().getState()) {
            model.getElement().setState(this.get(EppState.class, EppStateGen.P_CODE, EppState.STATE_FORMATIVE));
        }

        model.setDevelopGridTermList(IDevelopGridDAO.instance.get().getDevelopGridTermList(model.getElement().getEduPlan().getDevelopGrid()));
        if (model.getElement().getDevelopGridTerm() == null && !model.getDevelopGridTermList().isEmpty()) {
            model.getElement().setDevelopGridTerm(Iterables.getFirst(model.getDevelopGridTermList(), null));
        }
    }

    @Override
    public void save(final Model model)
    {
        if (null == model.getElement().getViewTableDiscipline()) {
            model.getElement().setViewTableDiscipline(DataAccessServices.dao().get(EppViewTableDisciplines.class, EppViewTableDisciplines.code().s(), EppViewTableDisciplines.CODE_SIMPLE));
        }
        this.getSession().saveOrUpdate(model.getElement());
    }

}
