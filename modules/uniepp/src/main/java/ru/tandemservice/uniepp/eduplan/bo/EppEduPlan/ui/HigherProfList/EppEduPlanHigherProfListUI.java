/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.EppEduPlanManager;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfAddEdit.EppEduPlanHigherProfAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.List.EppEduPlanListBaseUI;

/**
 * @author oleyba
 * @since 8/28/14
 */
@State({
    @Bind(key = "programKindId", binding = "programKindHolder.id"),
    @Bind(key="orgUnitId", binding="orgUnitHolder.id")
})
public class EppEduPlanHigherProfListUI extends EppEduPlanListBaseUI
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);

        if (EppEduPlanHigherProfList.PROGRAM_SPECIALIZATION_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppEduPlanHigherProfList.PARAM_PROGRAM_SUBJECT, getSettings().get(EppEduPlanHigherProfList.PARAM_PROGRAM_SUBJECT));
        }
    }

    public boolean isShowOrientation() {
        final Long programKindId = getProgramKindHolder().getId();
        return programKindId != null && IUniBaseDao.instance.get().existsEntity(EduProgramOrientation.class, EduProgramOrientation.subjectIndex().programKind().s(), programKindId);
    }

    public void onClickAddEduPlan() {
        _uiActivation
            .asRegionDialog(EppEduPlanHigherProfAddEdit.class)
            .parameter(UIPresenter.PUBLISHER_ID, null)
            .parameter(EppEduPlanManager.BIND_PROGRAM_KIND_ID, getProgramKindHolder().getId())
            .parameter(EppEduPlanManager.BIND_ORG_UNIT_ID, getOrgUnitHolder().getId())
            .activate();
    }

    public void onClickEditEduPlan() {
        _uiActivation
            .asRegionDialog(EppEduPlanHigherProfAddEdit.class)
            .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
            .activate();
    }
}