package ru.tandemservice.uniepp.entity.workplan;

import java.util.Date;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanRowPartLoadGen;

/**
 * Нагрузка для строки РУП в части (по видам теоретической нагрузки)
 */
public class EppWorkPlanRowPartLoad extends EppWorkPlanRowPartLoadGen implements IEppWorkPlanRowPartLoad
{
    public EppWorkPlanRowPartLoad() {

    }

    public EppWorkPlanRowPartLoad(final EppWorkPlanRow row, final int part, final EppLoadType loadType) {
        this.setRow(row);
        this.setPart(part);
        this.setLoadType(loadType);
    }


    public EppWorkPlanRowPartLoad(final EppWorkPlanRow row, final EppWorkPlanRowPartLoad source) {
        this.update(source);
        this.setRow(row);
    }

    @Override
    @EntityDSLSupport(parts={EppWorkPlanRowPartLoadGen.P_LOAD})
    public Double getLoadAsDouble() {
        final long load = this.getLoad();
        return UniEppUtils.wrap(load < 0 ? null : load);
    }

    public void setLoadAsDouble(final Double value) {
        final Long load = UniEppUtils.unwrap(value);
        this.setLoad(null == load ? -1 : load.longValue());
    }

    public EppWorkPlanRowPartLoad setupLoadAsDouble(final Double value) {
        this.setLoadAsDouble(value);
        return this;
    }

    @Override
    @EntityDSLSupport(parts={EppWorkPlanRowPartLoadGen.P_DAYS})
    public Double getDaysAsDouble() {
        final Long days = this.getDays();
        return UniEppUtils.wrap(days);
    }

    public void setDaysAsDouble(final Double value) {
        final Long days = UniEppUtils.unwrap(value);
        this.setDays(null == days ? null : days.longValue());
    }

    public EppWorkPlanRowPartLoad setupDaysAsDouble(final Double value) {
        this.setDaysAsDouble(value);
        return this;
    }


    public boolean isEmptyLoad() {
        final Double load = this.getLoadAsDouble();
        if ((null != load) && (load > 0d)) { return false; }

        final Double days = this.getDaysAsDouble();
        return !((null != days) && (days > 0d));

    }

    public void fillLoad(final EppWorkPlanRowPartLoad source) {
        this.setLoad(source.getLoad());
        this.setDays(source.getDays());
    }

    public void fillStudyPeriod(final EppWorkPlanRowPartLoad source) {
        this.setStudyPeriodStartDate(source.getStudyPeriodStartDate());
        this.setStudyPeriodFinishDate(source.getStudyPeriodFinishDate());
        this.correctStudyPeriod();
    }


    public void expandStudyPeriod(final EppWorkPlanRowPartLoad source) {

        if (null != this.getStudyPeriodStartDate()) {
            final Date studyPeriodStartDate = source.getStudyPeriodStartDate();
            if ((null == studyPeriodStartDate) || studyPeriodStartDate.before(this.getStudyPeriodStartDate())) {
                this.setStudyPeriodStartDate(studyPeriodStartDate);
            }
        }

        if (null != this.getStudyPeriodFinishDate()) {
            final Date studyPeriodFinishDate = source.getStudyPeriodFinishDate();
            if ((null == studyPeriodFinishDate) || studyPeriodFinishDate.after(this.getStudyPeriodFinishDate())) {
                this.setStudyPeriodFinishDate(studyPeriodFinishDate);
            }
        }

        this.correctStudyPeriod();
    }

    public void intersectStudyPeriod(final Date studyPeriodStartDate, final Date studyPeriodFinishDate) {

        if (null != studyPeriodStartDate) {
            if ((null == this.getStudyPeriodStartDate()) || this.getStudyPeriodStartDate().before(studyPeriodStartDate)) {
                this.setStudyPeriodStartDate(studyPeriodStartDate);
            }
        }

        if (null != studyPeriodFinishDate) {
            if ((null == this.getStudyPeriodFinishDate()) || this.getStudyPeriodFinishDate().after(studyPeriodFinishDate)) {
                this.setStudyPeriodFinishDate(studyPeriodFinishDate);
            }
        }

        this.correctStudyPeriod();
    }


    public void correctStudyPeriod() {
        final Date studyPeriodStartDate = this.getStudyPeriodStartDate();
        final Date studyPeriodFinishDate = this.getStudyPeriodFinishDate();
        if ((null != studyPeriodStartDate) && (null != studyPeriodFinishDate)) {
            if (studyPeriodFinishDate.before(studyPeriodStartDate)) {
                // если даты перепутаны - то корректируем их
                this.setStudyPeriodFinishDate(studyPeriodStartDate);
                this.setStudyPeriodStartDate(studyPeriodFinishDate);
            }
        }

    }



}