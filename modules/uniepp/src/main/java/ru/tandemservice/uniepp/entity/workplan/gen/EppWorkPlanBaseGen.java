package ru.tandemservice.uniepp.entity.workplan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Рабочий учебный план (базовый)
 *
 * Базовый рабочий учебный план - объект, содержащий только набор дисциплин
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkPlanBaseGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase";
    public static final String ENTITY_NAME = "eppWorkPlanBase";
    public static final int VERSION_HASH = -588559014;
    private static IEntityMeta ENTITY_META;

    public static final String P_REGISTRATION_NUMBER = "registrationNumber";
    public static final String L_STATE = "state";
    public static final String P_CONFIRM_DATE = "confirmDate";
    public static final String P_TITLE_POSTFIX = "titlePostfix";
    public static final String P_COMMENT = "comment";
    public static final String P_EDUCATION_ELEMENT_TITLE = "educationElementTitle";
    public static final String P_PART_FULL_TITLE = "partFullTitle";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE = "title";

    private String _registrationNumber;     // Номер регистрации
    private EppState _state;     // Состояние
    private Date _confirmDate;     // Дата утверждения
    private String _titlePostfix;     // Постфикс
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер регистрации. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRegistrationNumber()
    {
        return _registrationNumber;
    }

    /**
     * @param registrationNumber Номер регистрации. Свойство не может быть null.
     */
    public void setRegistrationNumber(String registrationNumber)
    {
        dirty(_registrationNumber, registrationNumber);
        _registrationNumber = registrationNumber;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public EppState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(EppState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Дата утверждения.
     */
    public Date getConfirmDate()
    {
        return _confirmDate;
    }

    /**
     * @param confirmDate Дата утверждения.
     */
    public void setConfirmDate(Date confirmDate)
    {
        dirty(_confirmDate, confirmDate);
        _confirmDate = confirmDate;
    }

    /**
     * @return Постфикс.
     */
    @Length(max=255)
    public String getTitlePostfix()
    {
        return _titlePostfix;
    }

    /**
     * @param titlePostfix Постфикс.
     */
    public void setTitlePostfix(String titlePostfix)
    {
        dirty(_titlePostfix, titlePostfix);
        _titlePostfix = titlePostfix;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppWorkPlanBaseGen)
        {
            setRegistrationNumber(((EppWorkPlanBase)another).getRegistrationNumber());
            setState(((EppWorkPlanBase)another).getState());
            setConfirmDate(((EppWorkPlanBase)another).getConfirmDate());
            setTitlePostfix(((EppWorkPlanBase)another).getTitlePostfix());
            setComment(((EppWorkPlanBase)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkPlanBaseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkPlanBase.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppWorkPlanBase is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "registrationNumber":
                    return obj.getRegistrationNumber();
                case "state":
                    return obj.getState();
                case "confirmDate":
                    return obj.getConfirmDate();
                case "titlePostfix":
                    return obj.getTitlePostfix();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "registrationNumber":
                    obj.setRegistrationNumber((String) value);
                    return;
                case "state":
                    obj.setState((EppState) value);
                    return;
                case "confirmDate":
                    obj.setConfirmDate((Date) value);
                    return;
                case "titlePostfix":
                    obj.setTitlePostfix((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "registrationNumber":
                        return true;
                case "state":
                        return true;
                case "confirmDate":
                        return true;
                case "titlePostfix":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "registrationNumber":
                    return true;
                case "state":
                    return true;
                case "confirmDate":
                    return true;
                case "titlePostfix":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "registrationNumber":
                    return String.class;
                case "state":
                    return EppState.class;
                case "confirmDate":
                    return Date.class;
                case "titlePostfix":
                    return String.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkPlanBase> _dslPath = new Path<EppWorkPlanBase>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkPlanBase");
    }
            

    /**
     * @return Номер регистрации. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getRegistrationNumber()
     */
    public static PropertyPath<String> registrationNumber()
    {
        return _dslPath.registrationNumber();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getState()
     */
    public static EppState.Path<EppState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getConfirmDate()
     */
    public static PropertyPath<Date> confirmDate()
    {
        return _dslPath.confirmDate();
    }

    /**
     * @return Постфикс.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getTitlePostfix()
     */
    public static PropertyPath<String> titlePostfix()
    {
        return _dslPath.titlePostfix();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getEducationElementTitle()
     */
    public static SupportedPropertyPath<String> educationElementTitle()
    {
        return _dslPath.educationElementTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getPartFullTitle()
     */
    public static SupportedPropertyPath<String> partFullTitle()
    {
        return _dslPath.partFullTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getShortTitle()
     */
    public static SupportedPropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EppWorkPlanBase> extends EntityPath<E>
    {
        private PropertyPath<String> _registrationNumber;
        private EppState.Path<EppState> _state;
        private PropertyPath<Date> _confirmDate;
        private PropertyPath<String> _titlePostfix;
        private PropertyPath<String> _comment;
        private SupportedPropertyPath<String> _educationElementTitle;
        private SupportedPropertyPath<String> _partFullTitle;
        private SupportedPropertyPath<String> _shortTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер регистрации. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getRegistrationNumber()
     */
        public PropertyPath<String> registrationNumber()
        {
            if(_registrationNumber == null )
                _registrationNumber = new PropertyPath<String>(EppWorkPlanBaseGen.P_REGISTRATION_NUMBER, this);
            return _registrationNumber;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getState()
     */
        public EppState.Path<EppState> state()
        {
            if(_state == null )
                _state = new EppState.Path<EppState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getConfirmDate()
     */
        public PropertyPath<Date> confirmDate()
        {
            if(_confirmDate == null )
                _confirmDate = new PropertyPath<Date>(EppWorkPlanBaseGen.P_CONFIRM_DATE, this);
            return _confirmDate;
        }

    /**
     * @return Постфикс.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getTitlePostfix()
     */
        public PropertyPath<String> titlePostfix()
        {
            if(_titlePostfix == null )
                _titlePostfix = new PropertyPath<String>(EppWorkPlanBaseGen.P_TITLE_POSTFIX, this);
            return _titlePostfix;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EppWorkPlanBaseGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getEducationElementTitle()
     */
        public SupportedPropertyPath<String> educationElementTitle()
        {
            if(_educationElementTitle == null )
                _educationElementTitle = new SupportedPropertyPath<String>(EppWorkPlanBaseGen.P_EDUCATION_ELEMENT_TITLE, this);
            return _educationElementTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getPartFullTitle()
     */
        public SupportedPropertyPath<String> partFullTitle()
        {
            if(_partFullTitle == null )
                _partFullTitle = new SupportedPropertyPath<String>(EppWorkPlanBaseGen.P_PART_FULL_TITLE, this);
            return _partFullTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getShortTitle()
     */
        public SupportedPropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new SupportedPropertyPath<String>(EppWorkPlanBaseGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EppWorkPlanBaseGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EppWorkPlanBase.class;
        }

        public String getEntityName()
        {
            return "eppWorkPlanBase";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getEducationElementTitle();

    public abstract String getPartFullTitle();

    public abstract String getShortTitle();

    public abstract String getTitle();
}
