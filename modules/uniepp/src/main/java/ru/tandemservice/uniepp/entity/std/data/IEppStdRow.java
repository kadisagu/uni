package ru.tandemservice.uniepp.entity.std.data;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import ru.tandemservice.uniepp.dao.index.IEppIndexedRow;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;

/**
 * @author vdanilov
 */
public interface IEppStdRow extends IEppIndexedRow, IEntity, IHierarchyItem, ITitled {

    /** @return блок ГОС (направление подготовки), в рамках которого создан элемент */
    EppStateEduStandardBlock getOwner();

    /** @return родительский элемент */
    @Override IEppStdRow getHierarhyParent();

    /** @return указатель того, что данный элемент несет в себе только функции аггрегирования */
    boolean isStructureElement();

    /** @return всего часов    */
    String getTotalSize();

    /** @return всего трудоемкости    */
    String getTotalLabor();

    /** @return Учитывать в нагрузке*/
    Boolean getUsedInLoad();

    /** @return Учитывать в контрольных мероприятиях*/
    Boolean getUsedInActions();

    /** @return Уникальный идентификатор строки */
    String getUuid();
}
