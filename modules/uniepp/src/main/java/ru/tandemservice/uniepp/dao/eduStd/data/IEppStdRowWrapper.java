package ru.tandemservice.uniepp.dao.eduStd.data;

import java.util.Map;

import ru.tandemservice.uniepp.dao.index.IEppIndexedRowWrapper;
import ru.tandemservice.uniepp.entity.std.data.IEppStdRow;

/**
 * @author vdanilov
 */
public interface IEppStdRowWrapper extends IEppStdRow, IEppIndexedRowWrapper
{

    /** @return оболочка ГОС */
    IEppStdWrapper getEduStd();

    /** @return строка ГОС */
    @Override IEppStdRow getRow();

    /** @return вышестоящий wrapper (соответствует row.parent) */
    @Override IEppStdRowWrapper getHierarhyParent();

    /**
     * @return обязательность дисциплины для конкретной формы освоения { developForm.code -> obligatoryStatus }
     * True - обязательна (для указанной формы), False - не обязательна (для указанной формы), null - без статуса
     * */
    Map<String, Boolean> getObligatoryMap();

    @Override String getTotalSize();

    @Override String getTotalLabor();

    String getCompetenciesCodes();

}
