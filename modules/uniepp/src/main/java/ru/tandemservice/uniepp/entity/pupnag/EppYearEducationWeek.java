package ru.tandemservice.uniepp.entity.pupnag;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearEducationWeekGen;

import java.util.Calendar;
import java.util.Date;

/**
 * Неделя (в учебном году)
 */
public class EppYearEducationWeek extends EppYearEducationWeekGen
{
    /**
     * Сдвигает дату до ближайшего начала недели (но не менее 1 дня)
     */
    public static void moveToNextDayOfWeek(final Calendar calendar)
    {
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        final int firstDayOfWeek = calendar.getFirstDayOfWeek();
        while (calendar.get(Calendar.DAY_OF_WEEK) != firstDayOfWeek) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }
    }

    private Date _cache_endDate = null;

    /**
     * @return дата окончания недели
     */
    @Override
    @EntityDSLSupport
    public Date getEndDate()
    {
        final Date date = this._cache_endDate;
        if (null != date) { return date; }

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.getDate());
        EppYearEducationWeek.moveToNextDayOfWeek(calendar);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        return (this._cache_endDate = calendar.getTime());
    }

    @Override
    @EntityDSLSupport
    public String getCalculatedTitle()
    {
        final Date startDate = this.getDate();
        final Date endDate = this.getEndDate();

        final int monthStartDate = CommonBaseDateUtil.getMonthStartingWithOne(startDate);
        final int monthEndDate = CommonBaseDateUtil.getMonthStartingWithOne(endDate);

        final int dayStartDate = CoreDateUtils.getDayOfMonth(startDate);
        final int dayEndDate = CoreDateUtils.getDayOfMonth(endDate);

        if (monthStartDate == monthEndDate) {
            return dayStartDate + " - " + dayEndDate + " " + RussianDateFormatUtils.getMonthName(startDate, false);
        }
        return dayStartDate + " " + RussianDateFormatUtils.getMonthName(startDate, false) + " - " + dayEndDate + " " + RussianDateFormatUtils.getMonthName(endDate, false);

    }



}