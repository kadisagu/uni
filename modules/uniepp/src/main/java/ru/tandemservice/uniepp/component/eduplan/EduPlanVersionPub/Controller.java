/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.caf.IUIPresenterAdapter;
import org.tandemframework.core.component.*;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import org.tandemframework.tapsupport.component.tab.TabPanelEvent;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uniepp.catalog.entity.EppScriptItem;
import ru.tandemservice.uniepp.catalog.entity.codes.EppScriptItemCodes;
import ru.tandemservice.uniepp.dao.eduplan.export.IEppEduPlanVersionExportDAO;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockListEdit.EppEduPlanVersionBlockListEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.ScheduleGrid.EppEduPlanVersionScheduleGridUI;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import java.util.Collections;
import java.util.Map;

/**
 * @author vip_delete
 * @since 27.02.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onChangeTab(final IBusinessComponent component)
    {
        TabPanelEvent parameter = component.getListenerParameter();
        if ("eduPlanVersionScheduleGrid".equals(parameter.getOldTabId())) {
            IComponentRegion region = component.getChildRegion("eduPlanVersionScope_eduPlanVersionScheduleGrid");
            if (region != null)
            {
                IUIPresenterAdapter iPresenter = region.getActiveComponent().getPresenter();
                if (iPresenter != null)
                {
                    EppEduPlanVersionScheduleGridUI presenter = (EppEduPlanVersionScheduleGridUI) iPresenter;
                    presenter.onClickCancel();
                }
            }
        }
    }

    public void onClickExportToFormatIMCA(final IBusinessComponent component)
    {
        try {
            final EppEduPlanVersion epv = this.getModel(component).getEduplanVersion();
            final byte[] export = IEppEduPlanVersionExportDAO.instance.get().export(Collections.singleton(epv.getId()));
            if (null == export) {
                return;
            }
            final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(export, "version.zip");
            this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "zip")));
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        IScriptItem scriptItem = DataAccessServices.dao().getByCode(EppScriptItem.class, EppScriptItemCodes.UNIEPP_EDU_PLAN_VERSION);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(
                scriptItem,
                IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                "epvIds", Collections.singletonList(this.getModel(component).getEduplanVersion().getId())
        );

        byte[] document = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
        String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

        if (null == document) {
            throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");
        }

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(filename).document(document), false);
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getEduplanVersion().getId())
        ));
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        this.getDao().delete(this.getModel(component).getEduplanVersion());
        this.deactivate(component);
    }

    public void onClickChangeDirectionBlocks(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                EppEduPlanVersionBlockListEdit.class.getSimpleName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getEduplanVersion().getId())
        ));
    }
}