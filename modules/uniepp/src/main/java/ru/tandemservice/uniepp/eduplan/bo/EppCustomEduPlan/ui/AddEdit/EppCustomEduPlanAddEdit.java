/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.EduProgramHigherProfManager;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.EppCustomEduPlanManager;

/**
 * @author Nikolay Fedorovskih
 * @since 17.08.2015
 */
@Configuration
public class EppCustomEduPlanAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EduProgramHigherProfManager.instance().programKindHigherProfDSConfig())
                .addDataSource(EppCustomEduPlanManager.instance().programSubjectDSConfig())
                .addDataSource(EppCustomEduPlanManager.instance().eduPlanVersionDSConfig())
                .addDataSource(EppCustomEduPlanManager.instance().epvBlockDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developGridDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
                .create();
    }
}