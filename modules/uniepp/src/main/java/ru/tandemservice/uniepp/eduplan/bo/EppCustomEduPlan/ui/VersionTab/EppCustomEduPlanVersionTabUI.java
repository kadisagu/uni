/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.VersionTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.logic.CustomEduPlanDSHandler;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author Nikolay Fedorovskih
 * @since 11.09.2015
 */
@Input({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class EppCustomEduPlanVersionTabUI extends UIPresenter
{
    private final EntityHolder<EppEduPlanVersion> holder = new EntityHolder<>();

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppCustomEduPlanVersionTab.CUSTOM_EDU_PLAN_DS.equals(dataSource.getName()))
        {
            final EppEduPlanVersion version = getEduPlanVersion();
            dataSource.put(CustomEduPlanDSHandler.PARAM_EDU_PLAN_VERSION, version);
            dataSource.put(CustomEduPlanDSHandler.PARAM_PROGRAM_KIND, version.getEduPlan().getProgramKind());
        }
    }

    public EntityHolder<EppEduPlanVersion> getHolder()
    {
        return holder;
    }

    public EppEduPlanVersion getEduPlanVersion()
    {
        return getHolder().getValue();
    }
}