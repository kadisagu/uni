package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.base.entity.IReorganizationObject;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент реестра
 *
 * Элемент реестра дисциплин
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegistryElementGen extends EntityBase
 implements IEppEducationElement, ICtrPriceElement, IReorganizationObject{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppRegistryElement";
    public static final String ENTITY_NAME = "eppRegistryElement";
    public static final int VERSION_HASH = 488639702;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String P_TITLE = "title";
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_COMMENT = "comment";
    public static final String P_LABOR = "labor";
    public static final String P_SIZE = "size";
    public static final String L_OWNER = "owner";
    public static final String L_PARENT = "parent";
    public static final String L_STATE = "state";
    public static final String L_EDU_GROUP_SPLIT_VARIANT = "eduGroupSplitVariant";
    public static final String P_EDU_GROUP_SPLIT_BY_TYPE = "eduGroupSplitByType";
    public static final String P_PARTS = "parts";
    public static final String P_WORK_PROGRAM_ANNOTATION = "workProgramAnnotation";
    public static final String P_WORK_PROGRAM_FILE = "workProgramFile";
    public static final String P_EDUCATION_ELEMENT_SIMPLE_TITLE = "educationElementSimpleTitle";
    public static final String P_EDUCATION_ELEMENT_TITLE = "educationElementTitle";
    public static final String P_FORMATTED_LOAD = "formattedLoad";
    public static final String P_NUMBER_WITH_ABBREVIATION = "numberWithAbbreviation";
    public static final String P_TITLE_WITH_F_CONTROLS = "titleWithFControls";
    public static final String P_TITLE_WITH_LOAD = "titleWithLoad";
    public static final String P_TITLE_WITH_NUMBER = "titleWithNumber";
    public static final String P_TITLE_WITH_NUMBER_AND_LOAD = "titleWithNumberAndLoad";
    public static final String P_TITLE_WITH_SPLIT_VARIANT = "titleWithSplitVariant";

    private String _number;     // Номер
    private String _title;     // Название
    private String _fullTitle;     // Полное название
    private String _shortTitle;     // Сокращенное название
    private String _comment;     // Комментарий
    private long _labor;     // Трудоемкость (в единицах трудоемкости)
    private long _size;     // Нагрузка (в часах)
    private OrgUnit _owner;     // Читающее подразделение
    private EppRegistryStructure _parent;     // Группа
    private EppState _state;     // Состояние
    private EppEduGroupSplitVariant _eduGroupSplitVariant;     // Способ деления потоков
    private boolean _eduGroupSplitByType = false;     // Ограничение по виду потока
    private int _parts = 1;     // Число частей (семестров)
    private Long _workProgramAnnotation;     // Аннотация рабочей программы
    private Long _workProgramFile;     // Файл рабочей программы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Полное название.
     */
    @Length(max=255)
    public String getFullTitle()
    {
        return _fullTitle;
    }

    /**
     * @param fullTitle Полное название.
     */
    public void setFullTitle(String fullTitle)
    {
        dirty(_fullTitle, fullTitle);
        _fullTitle = fullTitle;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Трудоемкость (в единицах трудоемкости). Свойство не может быть null.
     */
    @NotNull
    public long getLabor()
    {
        return _labor;
    }

    /**
     * @param labor Трудоемкость (в единицах трудоемкости). Свойство не может быть null.
     */
    public void setLabor(long labor)
    {
        dirty(_labor, labor);
        _labor = labor;
    }

    /**
     * @return Нагрузка (в часах). Свойство не может быть null.
     */
    @NotNull
    public long getSize()
    {
        return _size;
    }

    /**
     * @param size Нагрузка (в часах). Свойство не может быть null.
     */
    public void setSize(long size)
    {
        dirty(_size, size);
        _size = size;
    }

    /**
     * @return Читающее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Читающее подразделение. Свойство не может быть null.
     */
    public void setOwner(OrgUnit owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryStructure getParent()
    {
        return _parent;
    }

    /**
     * @param parent Группа. Свойство не может быть null.
     */
    public void setParent(EppRegistryStructure parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public EppState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(EppState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Способ деления потоков.
     */
    public EppEduGroupSplitVariant getEduGroupSplitVariant()
    {
        return _eduGroupSplitVariant;
    }

    /**
     * @param eduGroupSplitVariant Способ деления потоков.
     */
    public void setEduGroupSplitVariant(EppEduGroupSplitVariant eduGroupSplitVariant)
    {
        dirty(_eduGroupSplitVariant, eduGroupSplitVariant);
        _eduGroupSplitVariant = eduGroupSplitVariant;
    }

    /**
     * Показывает, что способ деления действует только для выбранных видов потоков
     *
     * @return Ограничение по виду потока. Свойство не может быть null.
     */
    @NotNull
    public boolean isEduGroupSplitByType()
    {
        return _eduGroupSplitByType;
    }

    /**
     * @param eduGroupSplitByType Ограничение по виду потока. Свойство не может быть null.
     */
    public void setEduGroupSplitByType(boolean eduGroupSplitByType)
    {
        dirty(_eduGroupSplitByType, eduGroupSplitByType);
        _eduGroupSplitByType = eduGroupSplitByType;
    }

    /**
     * @return Число частей (семестров). Свойство не может быть null.
     */
    @NotNull
    public int getParts()
    {
        return _parts;
    }

    /**
     * @param parts Число частей (семестров). Свойство не может быть null.
     */
    public void setParts(int parts)
    {
        dirty(_parts, parts);
        _parts = parts;
    }

    /**
     * @return Аннотация рабочей программы.
     */
    public Long getWorkProgramAnnotation()
    {
        return _workProgramAnnotation;
    }

    /**
     * @param workProgramAnnotation Аннотация рабочей программы.
     */
    public void setWorkProgramAnnotation(Long workProgramAnnotation)
    {
        dirty(_workProgramAnnotation, workProgramAnnotation);
        _workProgramAnnotation = workProgramAnnotation;
    }

    /**
     * @return Файл рабочей программы.
     */
    public Long getWorkProgramFile()
    {
        return _workProgramFile;
    }

    /**
     * @param workProgramFile Файл рабочей программы.
     */
    public void setWorkProgramFile(Long workProgramFile)
    {
        dirty(_workProgramFile, workProgramFile);
        _workProgramFile = workProgramFile;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRegistryElementGen)
        {
            setNumber(((EppRegistryElement)another).getNumber());
            setTitle(((EppRegistryElement)another).getTitle());
            setFullTitle(((EppRegistryElement)another).getFullTitle());
            setShortTitle(((EppRegistryElement)another).getShortTitle());
            setComment(((EppRegistryElement)another).getComment());
            setLabor(((EppRegistryElement)another).getLabor());
            setSize(((EppRegistryElement)another).getSize());
            setOwner(((EppRegistryElement)another).getOwner());
            setParent(((EppRegistryElement)another).getParent());
            setState(((EppRegistryElement)another).getState());
            setEduGroupSplitVariant(((EppRegistryElement)another).getEduGroupSplitVariant());
            setEduGroupSplitByType(((EppRegistryElement)another).isEduGroupSplitByType());
            setParts(((EppRegistryElement)another).getParts());
            setWorkProgramAnnotation(((EppRegistryElement)another).getWorkProgramAnnotation());
            setWorkProgramFile(((EppRegistryElement)another).getWorkProgramFile());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegistryElementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegistryElement.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppRegistryElement is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "title":
                    return obj.getTitle();
                case "fullTitle":
                    return obj.getFullTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "comment":
                    return obj.getComment();
                case "labor":
                    return obj.getLabor();
                case "size":
                    return obj.getSize();
                case "owner":
                    return obj.getOwner();
                case "parent":
                    return obj.getParent();
                case "state":
                    return obj.getState();
                case "eduGroupSplitVariant":
                    return obj.getEduGroupSplitVariant();
                case "eduGroupSplitByType":
                    return obj.isEduGroupSplitByType();
                case "parts":
                    return obj.getParts();
                case "workProgramAnnotation":
                    return obj.getWorkProgramAnnotation();
                case "workProgramFile":
                    return obj.getWorkProgramFile();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "fullTitle":
                    obj.setFullTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "labor":
                    obj.setLabor((Long) value);
                    return;
                case "size":
                    obj.setSize((Long) value);
                    return;
                case "owner":
                    obj.setOwner((OrgUnit) value);
                    return;
                case "parent":
                    obj.setParent((EppRegistryStructure) value);
                    return;
                case "state":
                    obj.setState((EppState) value);
                    return;
                case "eduGroupSplitVariant":
                    obj.setEduGroupSplitVariant((EppEduGroupSplitVariant) value);
                    return;
                case "eduGroupSplitByType":
                    obj.setEduGroupSplitByType((Boolean) value);
                    return;
                case "parts":
                    obj.setParts((Integer) value);
                    return;
                case "workProgramAnnotation":
                    obj.setWorkProgramAnnotation((Long) value);
                    return;
                case "workProgramFile":
                    obj.setWorkProgramFile((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "title":
                        return true;
                case "fullTitle":
                        return true;
                case "shortTitle":
                        return true;
                case "comment":
                        return true;
                case "labor":
                        return true;
                case "size":
                        return true;
                case "owner":
                        return true;
                case "parent":
                        return true;
                case "state":
                        return true;
                case "eduGroupSplitVariant":
                        return true;
                case "eduGroupSplitByType":
                        return true;
                case "parts":
                        return true;
                case "workProgramAnnotation":
                        return true;
                case "workProgramFile":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "title":
                    return true;
                case "fullTitle":
                    return true;
                case "shortTitle":
                    return true;
                case "comment":
                    return true;
                case "labor":
                    return true;
                case "size":
                    return true;
                case "owner":
                    return true;
                case "parent":
                    return true;
                case "state":
                    return true;
                case "eduGroupSplitVariant":
                    return true;
                case "eduGroupSplitByType":
                    return true;
                case "parts":
                    return true;
                case "workProgramAnnotation":
                    return true;
                case "workProgramFile":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return String.class;
                case "title":
                    return String.class;
                case "fullTitle":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "comment":
                    return String.class;
                case "labor":
                    return Long.class;
                case "size":
                    return Long.class;
                case "owner":
                    return OrgUnit.class;
                case "parent":
                    return EppRegistryStructure.class;
                case "state":
                    return EppState.class;
                case "eduGroupSplitVariant":
                    return EppEduGroupSplitVariant.class;
                case "eduGroupSplitByType":
                    return Boolean.class;
                case "parts":
                    return Integer.class;
                case "workProgramAnnotation":
                    return Long.class;
                case "workProgramFile":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRegistryElement> _dslPath = new Path<EppRegistryElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegistryElement");
    }
            

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Полное название.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getFullTitle()
     */
    public static PropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Трудоемкость (в единицах трудоемкости). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getLabor()
     */
    public static PropertyPath<Long> labor()
    {
        return _dslPath.labor();
    }

    /**
     * @return Нагрузка (в часах). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getSize()
     */
    public static PropertyPath<Long> size()
    {
        return _dslPath.size();
    }

    /**
     * @return Читающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getOwner()
     */
    public static OrgUnit.Path<OrgUnit> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getParent()
     */
    public static EppRegistryStructure.Path<EppRegistryStructure> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getState()
     */
    public static EppState.Path<EppState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Способ деления потоков.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getEduGroupSplitVariant()
     */
    public static EppEduGroupSplitVariant.Path<EppEduGroupSplitVariant> eduGroupSplitVariant()
    {
        return _dslPath.eduGroupSplitVariant();
    }

    /**
     * Показывает, что способ деления действует только для выбранных видов потоков
     *
     * @return Ограничение по виду потока. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#isEduGroupSplitByType()
     */
    public static PropertyPath<Boolean> eduGroupSplitByType()
    {
        return _dslPath.eduGroupSplitByType();
    }

    /**
     * @return Число частей (семестров). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getParts()
     */
    public static PropertyPath<Integer> parts()
    {
        return _dslPath.parts();
    }

    /**
     * @return Аннотация рабочей программы.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getWorkProgramAnnotation()
     */
    public static PropertyPath<Long> workProgramAnnotation()
    {
        return _dslPath.workProgramAnnotation();
    }

    /**
     * @return Файл рабочей программы.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getWorkProgramFile()
     */
    public static PropertyPath<Long> workProgramFile()
    {
        return _dslPath.workProgramFile();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getEducationElementSimpleTitle()
     */
    public static SupportedPropertyPath<String> educationElementSimpleTitle()
    {
        return _dslPath.educationElementSimpleTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getEducationElementTitle()
     */
    public static SupportedPropertyPath<String> educationElementTitle()
    {
        return _dslPath.educationElementTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getFormattedLoad()
     */
    public static SupportedPropertyPath<String> formattedLoad()
    {
        return _dslPath.formattedLoad();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getNumberWithAbbreviation()
     */
    public static SupportedPropertyPath<String> numberWithAbbreviation()
    {
        return _dslPath.numberWithAbbreviation();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getTitleWithFControls()
     */
    public static SupportedPropertyPath<String> titleWithFControls()
    {
        return _dslPath.titleWithFControls();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getTitleWithLoad()
     */
    public static SupportedPropertyPath<String> titleWithLoad()
    {
        return _dslPath.titleWithLoad();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getTitleWithNumber()
     */
    public static SupportedPropertyPath<String> titleWithNumber()
    {
        return _dslPath.titleWithNumber();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getTitleWithNumberAndLoad()
     */
    public static SupportedPropertyPath<String> titleWithNumberAndLoad()
    {
        return _dslPath.titleWithNumberAndLoad();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getTitleWithSplitVariant()
     */
    public static SupportedPropertyPath<String> titleWithSplitVariant()
    {
        return _dslPath.titleWithSplitVariant();
    }

    public static class Path<E extends EppRegistryElement> extends EntityPath<E>
    {
        private PropertyPath<String> _number;
        private PropertyPath<String> _title;
        private PropertyPath<String> _fullTitle;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _comment;
        private PropertyPath<Long> _labor;
        private PropertyPath<Long> _size;
        private OrgUnit.Path<OrgUnit> _owner;
        private EppRegistryStructure.Path<EppRegistryStructure> _parent;
        private EppState.Path<EppState> _state;
        private EppEduGroupSplitVariant.Path<EppEduGroupSplitVariant> _eduGroupSplitVariant;
        private PropertyPath<Boolean> _eduGroupSplitByType;
        private PropertyPath<Integer> _parts;
        private PropertyPath<Long> _workProgramAnnotation;
        private PropertyPath<Long> _workProgramFile;
        private SupportedPropertyPath<String> _educationElementSimpleTitle;
        private SupportedPropertyPath<String> _educationElementTitle;
        private SupportedPropertyPath<String> _formattedLoad;
        private SupportedPropertyPath<String> _numberWithAbbreviation;
        private SupportedPropertyPath<String> _titleWithFControls;
        private SupportedPropertyPath<String> _titleWithLoad;
        private SupportedPropertyPath<String> _titleWithNumber;
        private SupportedPropertyPath<String> _titleWithNumberAndLoad;
        private SupportedPropertyPath<String> _titleWithSplitVariant;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppRegistryElementGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppRegistryElementGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Полное название.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getFullTitle()
     */
        public PropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new PropertyPath<String>(EppRegistryElementGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EppRegistryElementGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EppRegistryElementGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Трудоемкость (в единицах трудоемкости). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getLabor()
     */
        public PropertyPath<Long> labor()
        {
            if(_labor == null )
                _labor = new PropertyPath<Long>(EppRegistryElementGen.P_LABOR, this);
            return _labor;
        }

    /**
     * @return Нагрузка (в часах). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getSize()
     */
        public PropertyPath<Long> size()
        {
            if(_size == null )
                _size = new PropertyPath<Long>(EppRegistryElementGen.P_SIZE, this);
            return _size;
        }

    /**
     * @return Читающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getOwner()
     */
        public OrgUnit.Path<OrgUnit> owner()
        {
            if(_owner == null )
                _owner = new OrgUnit.Path<OrgUnit>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getParent()
     */
        public EppRegistryStructure.Path<EppRegistryStructure> parent()
        {
            if(_parent == null )
                _parent = new EppRegistryStructure.Path<EppRegistryStructure>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getState()
     */
        public EppState.Path<EppState> state()
        {
            if(_state == null )
                _state = new EppState.Path<EppState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Способ деления потоков.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getEduGroupSplitVariant()
     */
        public EppEduGroupSplitVariant.Path<EppEduGroupSplitVariant> eduGroupSplitVariant()
        {
            if(_eduGroupSplitVariant == null )
                _eduGroupSplitVariant = new EppEduGroupSplitVariant.Path<EppEduGroupSplitVariant>(L_EDU_GROUP_SPLIT_VARIANT, this);
            return _eduGroupSplitVariant;
        }

    /**
     * Показывает, что способ деления действует только для выбранных видов потоков
     *
     * @return Ограничение по виду потока. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#isEduGroupSplitByType()
     */
        public PropertyPath<Boolean> eduGroupSplitByType()
        {
            if(_eduGroupSplitByType == null )
                _eduGroupSplitByType = new PropertyPath<Boolean>(EppRegistryElementGen.P_EDU_GROUP_SPLIT_BY_TYPE, this);
            return _eduGroupSplitByType;
        }

    /**
     * @return Число частей (семестров). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getParts()
     */
        public PropertyPath<Integer> parts()
        {
            if(_parts == null )
                _parts = new PropertyPath<Integer>(EppRegistryElementGen.P_PARTS, this);
            return _parts;
        }

    /**
     * @return Аннотация рабочей программы.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getWorkProgramAnnotation()
     */
        public PropertyPath<Long> workProgramAnnotation()
        {
            if(_workProgramAnnotation == null )
                _workProgramAnnotation = new PropertyPath<Long>(EppRegistryElementGen.P_WORK_PROGRAM_ANNOTATION, this);
            return _workProgramAnnotation;
        }

    /**
     * @return Файл рабочей программы.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getWorkProgramFile()
     */
        public PropertyPath<Long> workProgramFile()
        {
            if(_workProgramFile == null )
                _workProgramFile = new PropertyPath<Long>(EppRegistryElementGen.P_WORK_PROGRAM_FILE, this);
            return _workProgramFile;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getEducationElementSimpleTitle()
     */
        public SupportedPropertyPath<String> educationElementSimpleTitle()
        {
            if(_educationElementSimpleTitle == null )
                _educationElementSimpleTitle = new SupportedPropertyPath<String>(EppRegistryElementGen.P_EDUCATION_ELEMENT_SIMPLE_TITLE, this);
            return _educationElementSimpleTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getEducationElementTitle()
     */
        public SupportedPropertyPath<String> educationElementTitle()
        {
            if(_educationElementTitle == null )
                _educationElementTitle = new SupportedPropertyPath<String>(EppRegistryElementGen.P_EDUCATION_ELEMENT_TITLE, this);
            return _educationElementTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getFormattedLoad()
     */
        public SupportedPropertyPath<String> formattedLoad()
        {
            if(_formattedLoad == null )
                _formattedLoad = new SupportedPropertyPath<String>(EppRegistryElementGen.P_FORMATTED_LOAD, this);
            return _formattedLoad;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getNumberWithAbbreviation()
     */
        public SupportedPropertyPath<String> numberWithAbbreviation()
        {
            if(_numberWithAbbreviation == null )
                _numberWithAbbreviation = new SupportedPropertyPath<String>(EppRegistryElementGen.P_NUMBER_WITH_ABBREVIATION, this);
            return _numberWithAbbreviation;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getTitleWithFControls()
     */
        public SupportedPropertyPath<String> titleWithFControls()
        {
            if(_titleWithFControls == null )
                _titleWithFControls = new SupportedPropertyPath<String>(EppRegistryElementGen.P_TITLE_WITH_F_CONTROLS, this);
            return _titleWithFControls;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getTitleWithLoad()
     */
        public SupportedPropertyPath<String> titleWithLoad()
        {
            if(_titleWithLoad == null )
                _titleWithLoad = new SupportedPropertyPath<String>(EppRegistryElementGen.P_TITLE_WITH_LOAD, this);
            return _titleWithLoad;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getTitleWithNumber()
     */
        public SupportedPropertyPath<String> titleWithNumber()
        {
            if(_titleWithNumber == null )
                _titleWithNumber = new SupportedPropertyPath<String>(EppRegistryElementGen.P_TITLE_WITH_NUMBER, this);
            return _titleWithNumber;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getTitleWithNumberAndLoad()
     */
        public SupportedPropertyPath<String> titleWithNumberAndLoad()
        {
            if(_titleWithNumberAndLoad == null )
                _titleWithNumberAndLoad = new SupportedPropertyPath<String>(EppRegistryElementGen.P_TITLE_WITH_NUMBER_AND_LOAD, this);
            return _titleWithNumberAndLoad;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElement#getTitleWithSplitVariant()
     */
        public SupportedPropertyPath<String> titleWithSplitVariant()
        {
            if(_titleWithSplitVariant == null )
                _titleWithSplitVariant = new SupportedPropertyPath<String>(EppRegistryElementGen.P_TITLE_WITH_SPLIT_VARIANT, this);
            return _titleWithSplitVariant;
        }

        public Class getEntityClass()
        {
            return EppRegistryElement.class;
        }

        public String getEntityName()
        {
            return "eppRegistryElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getEducationElementSimpleTitle();

    public abstract String getEducationElementTitle();

    public abstract String getFormattedLoad();

    public abstract String getNumberWithAbbreviation();

    public abstract String getTitleWithFControls();

    public abstract String getTitleWithLoad();

    public abstract String getTitleWithNumber();

    public abstract String getTitleWithNumberAndLoad();

    public abstract String getTitleWithSplitVariant();
}
