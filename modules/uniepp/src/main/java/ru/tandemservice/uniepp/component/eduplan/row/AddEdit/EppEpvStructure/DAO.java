package ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvStructure;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.BaseAddEditDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvRowGen;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvStructureRowGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends BaseAddEditDao<EppEpvStructureRow, Model> implements IDAO {

    @Override public void prepare(final Model model)
    {
        super.prepare(model);

        final List<HSelectOption> parentList = IEppEduPlanVersionDataDAO.instance.get().getEpvRowHierarchyListFromBlock(model.getBlock(), EppEpvStructureRow.PARENT_PREDICATE);
        BaseAddEditDao.disablePossibleLoop(parentList, Collections.singleton(model.getRow().getId()));
        model.setParentList(parentList);

        final Set<Long> usedValueIds = this.getUsedValueIds(model);
        final List<HSelectOption> valueList = IEppEduPlanVersionDataDAO.instance.get().getPlanStructureHierarchyList4Add(model.getBlock());
        for (final HSelectOption option: valueList) {
            option.setCanBeSelected(option.isCanBeSelected() && (!usedValueIds.contains(((IEntity)option.getObject()).getId())));
        }
        model.setValueList(valueList);
    }

    private Set<Long> getUsedValueIds(final Model model) {
        final EppEpvStructureRow row = model.getRow();

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EppEpvStructureRow.class, "r").column(property("r", EppEpvStructureRow.value().id()))
            .where(eq(property("r", EppEpvStructureRow.owner().eduPlanVersion()), value(row.getOwner().getEduPlanVersion())))
            .where(or(
                eq(property("r", EppEpvStructureRow.owner()), value(row.getOwner())),
                instanceOf(EppEpvStructureRow.owner().fromAlias("r").s(), EppEduPlanVersionRootBlock.class)
            ));

        if (model.getParent() != null) {
            dql.where(eq(property("r", EppEpvStructureRow.parent()), value(model.getParent().getRow())));
        } else {
            dql.where(isNull(property("r", EppEpvStructureRow.parent())));
        }


        return new HashSet<Long>(dql.createStatement(getSession()).<Long>list());
    }

    @Override public void save(final Model model) {
        this.doSaveRow(model);
    }
}
