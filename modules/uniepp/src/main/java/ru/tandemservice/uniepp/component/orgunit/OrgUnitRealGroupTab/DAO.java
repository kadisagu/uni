package ru.tandemservice.uniepp.component.orgunit.OrgUnitRealGroupTab;

import ru.tandemservice.uni.dao.UniDao;

/**
 * @author vdanilov
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override public void prepare(final Model model) {
        model.getOrgUnitHolder().refresh();
    }
}
