package ru.tandemservice.uniepp.component.registry.RegElementPartList;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.component.base.SimpleAddEditModel;
import ru.tandemservice.uniepp.component.registry.base.Pub.RegistryElementPubContext;
import ru.tandemservice.uniepp.dao.registry.data.IEppBaseRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key = RegistryElementPubContext.REGISTRY_ELEMENT_PUB_CONTEXT_PARAM, binding = "context", required = true)
})
public class Model
{
    private RegistryElementPubContext context;
    public RegistryElementPubContext getContext() { return this.context; }
    public void setContext(final RegistryElementPubContext context) { this.context = context; }

    public EppRegistryElement getElement() { return this.getContext().getElement(); }
    public CommonPostfixPermissionModel getSec() { return this.getContext().getSec(); }

    public boolean isReadOnly() { return SimpleAddEditModel.isReadOnly(this.getElement()); }

    public static class Row extends ViewWrapper<IEppBaseRegElWrapper> {
        private final Row parent;
        @Override public Row getHierarhyParent() { return this.parent; }

        public Row(final IEppBaseRegElWrapper i, final Row parent) {
            super(i);
            this.parent = parent;
        }
    }

    private StaticListDataSource<Row> dataSource;
    public void setDataSource(final StaticListDataSource<Row> dataSource) { this.dataSource = dataSource; }
    public StaticListDataSource<Row> getDataSource() { return this.dataSource; }

    public String getCurrentBlockName() {
        return "rowActionsBlock4"+ClassUtils.getUserClass(this.getDataSource().getCurrentEntity().getEntity()).getSimpleName();
    }



    public String getLoadString(final IEppBaseRegElWrapper wrapper)
    {
        final StringBuilder sb = new StringBuilder();

        final double sz = wrapper.getLoadAsDouble(EppLoadType.FULL_CODE_TOTAL_HOURS);
        final double lb = wrapper.getLoadAsDouble(EppLoadType.FULL_CODE_LABOR);
        if ((sz > 0) || (lb > 0)) {
            sb.append(UniEppUtils.formatLoad(sz, false)).append(" ч. (").append(UniEppUtils.formatLoad(lb, false)).append(" з.ед.)");
        }

        final StringBuilder loadDetails = new StringBuilder();
        for (final String fullCode : EppALoadType.FULL_CODES) {
            final double load = wrapper.getLoadAsDouble(fullCode);
            if (loadDetails.length() > 0) { loadDetails.append(" / "); }
            loadDetails.append(UniEppUtils.formatLoad(Math.max(0, load), false));
        }
        sb.append(loadDetails);

        return sb.toString();
    }

    public String getTitleWithAppender(final String string, final IEppBaseRegElWrapper wrapper)
    {
        final StringBuilder sb = new StringBuilder(string);

        final String loadString = StringUtils.trimToNull(this.getLoadString(wrapper));
        if (null != loadString) {
            if (sb.length() > 0) { sb.append("; "); }
            sb.append(loadString);
        }

        //        final String controlActionString = StringUtils.trimToNull(this.getControlActionString(wrapper));
        //        if (null != controlActionString) {
        //            if (sb.length() > 0) { sb.append("; "); }
        //            sb.append(controlActionString);
        //        }

        return sb.toString();
    }

}
