package ru.tandemservice.uniepp.ui;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.NOPTransformer;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.Wiki;

import ru.tandemservice.uniepp.dao.registry.EppRegistryDAO;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author vdanilov
 */
@Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
public class EppRegistryElementSelectModel<T extends EppRegistryElement> extends DQLFullCheckSelectModel
{
    private static final String[] BASE_PROPERTIES = { "title", "parts", "load", "fcontrol", "owner" };
    private final Class<? extends T> registryElementClass;

    public EppRegistryElementSelectModel(final Class<? extends T> registryElementClass, String ...labelProperties) {
        super(getLabelProperties(labelProperties));
        this.registryElementClass = registryElementClass;
    }

    private static String[] getLabelProperties(String[] labelProperties) {
        if (null == labelProperties || 0 == labelProperties.length) {
            return BASE_PROPERTIES;
        }

        List<String> properties = new ArrayList<String>(Arrays.asList(BASE_PROPERTIES));
        properties.addAll(Arrays.asList(labelProperties));
        return properties.toArray(new String[0]);
    }

    @Override protected DQLSelectBuilder query(final String alias, final String filter) {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(this.registryElementClass, alias);

        if (null != filter) {
            dql.where(this.getFilterCondition(alias, filter));
        }

        dql.order(property(EppRegistryElement.title().fromAlias(alias)));
        dql.order(property(EppRegistryElement.owner().title().fromAlias(alias)));
        return dql;
    }

    protected IDQLExpression getFilterCondition(final String alias, final String filter) {
        return or(
            this.like(EppRegistryElement.title().fromAlias(alias), filter),
            this.like(EppRegistryElement.shortTitle().fromAlias(alias), filter),
            this.like(EppRegistryElement.fullTitle().fromAlias(alias), filter),
            this.like(EppRegistryElement.number().fromAlias(alias), filter)
        );
    }

    @SuppressWarnings("unchecked")
    @Override protected List wrap(final List resultList) {
        return EppRegistryDAO.fillRegistryElementWrappers(ViewWrapper.getPatchedList(resultList));
    }

    @Override public String getLabelFor(final Object value, final int columnIndex)
    {
        final ViewWrapper wrapper = this.getWrapper(value);
        final EppRegistryElement row = (EppRegistryElement)wrapper.getEntity();
        switch (columnIndex)
        {
            case 0:
                return row.getTitle() + " ("+row.getParent().getAbbreviation()+" №"+row.getNumber()+")";
            case 1:
                if (row instanceof EppRegistryAction) { return ""; /* не имеет смысла */ }
                return String.valueOf(row.getParts());
            case 2:
                return String.valueOf(wrapper.getProperty("load"));
            case 3:
                return row.getTitleWithFControls();
            case 4:
                return row.getOwner().getShortTitle();
        }
        return super.getLabelFor(wrapper, columnIndex);
    }

    @Override public String getFullLabel(final Object value) {
        final ViewWrapper wrapper = this.getWrapper(value);
        final EppRegistryElement row = (EppRegistryElement)wrapper.getEntity();
        return row.getEducationElementTitle();
    }


    @SuppressWarnings("unchecked")
    protected Collection<Long> ids(final Iterable it) {
        if (null == it) { return Collections.emptyList(); }
        final Iterator<Long> iterator = it.iterator();
        if (null == iterator) { return Collections.emptyList(); }
        return CollectionUtils.collect(iterator, NOPTransformer.INSTANCE);
    }
}
