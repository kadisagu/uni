/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithDisciplineGaps;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListPresenter;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;
import static org.tandemframework.hibsupport.dql.DQLFunctions.max;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
public class EppIndicatorStudentsWithDisciplineGapsSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public static final String VIEW_PROP_BROKEN_SLOT_LIST = "brokenSlotList";

    private static final String STUDENT_2_SLOT_LIST_MAP = "student2slotListMap";

    public EppIndicatorStudentsWithDisciplineGapsSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);

        // только неархивные студенты в активном состоянии
        builder.where(eq(property(Student.archival().fromAlias(alias)), value(Boolean.FALSE)));
        builder.where(eq(property(Student.status().active().fromAlias(alias)), value(Boolean.TRUE)));

        // этого деканата
        final OrgUnit orgUnit = context.get(AbstractEppIndicatorStudentListPresenter.PROP_ORG_UNIT);
        if (null != orgUnit && null != orgUnit.getId()) {
            builder.where(eq(property(Student.educationOrgUnit().groupOrgUnit().id().fromAlias(alias)), value(orgUnit.getId())));
        }

        // адовое условие на наличие дыр а дисциплиночастях
        builder.where(exists(
            getGapsSlotDql()
            .where(eq(property(EppStudentWorkPlanElement.student().fromAlias("slot")), property(alias)))
            .buildQuery())
        );
    }

    @Override
    protected void prepareWrappData(List<Long> recordIds, DSInput input, ExecutionContext context)
    {
        super.prepareWrappData(recordIds, input, context);

        final Map<Long, List<EppStudentWorkPlanElement>> student2slotListMap = SafeMap.get(ArrayList.class);

        for (List<Long> elements : Lists.partition(recordIds, DQL.MAX_VALUES_ROW_NUMBER)) {
            final DQLSelectBuilder slotBuilder = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "xslot").column(property("xslot"))
                    .where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("xslot"))))
                    .where(in(property(EppStudentWorkPlanElement.student().id().fromAlias("xslot")), elements))

                    .fetchPath(DQLJoinType.inner, EppStudentWorkPlanElement.sourceRow().fromAlias("xslot"), "source")
                    .fetchPath(DQLJoinType.inner, EppStudentWorkPlanElement.registryElementPart().fromAlias("xslot"), "regelpart")
                    .fetchPath(DQLJoinType.inner, EppRegistryElementPart.registryElement().fromAlias("regelpart"), "regel")

                    .order(property(EppRegistryElement.title().fromAlias("regel")))
                    .order(property(EppRegistryElement.id().fromAlias("regel")))
                    .order(property(EppWorkPlanRow.number().fromAlias("source")))
                    .order(property(EppStudentWorkPlanElement.year().educationYear().intValue().fromAlias("xslot")))
                    .order(property(EppStudentWorkPlanElement.term().intValue().fromAlias("xslot")));

            // адовое условие на наличие дыр а дисциплиночастях
            slotBuilder.where(exists(
                    getGapsSlotDql()
                            .where(eq(property(EppStudentWorkPlanElement.student().fromAlias("xslot")), property(EppStudentWorkPlanElement.student().fromAlias("slot"))))
                            .where(eq(property(EppStudentWorkPlanElement.registryElementPart().fromAlias("xslot")), property(EppStudentWorkPlanElement.registryElementPart().fromAlias("slot"))))
                            .buildQuery()
            ));

            for (final EppStudentWorkPlanElement slot: slotBuilder.createStatement(context.getSession()).<EppStudentWorkPlanElement>list())
                student2slotListMap.get(slot.getStudent().getId())
                        .add(slot);
        }

        context.put(STUDENT_2_SLOT_LIST_MAP, student2slotListMap);
    }

    @Override
    protected void wrap(DataWrapper wrapper, ExecutionContext context)
    {
        super.wrap(wrapper, context);

        final Map<Long, List<EppStudentWorkPlanElement>> student2slotListMap = context.get(STUDENT_2_SLOT_LIST_MAP);

        final StringBuilder builder = new StringBuilder();

        long id = 0;
        for (final EppStudentWorkPlanElement slot: student2slotListMap.get(wrapper.getId()))
        {
            if (id != slot.getRegistryElementPart().getRegistryElement().getId()) {
                id = slot.getRegistryElementPart().getRegistryElement().getId();
                if (builder.length() > 0) { builder.append('\n'); }
            }
            builder.append(slot.getYear().getEducationYear().getTitle()).append(' ').append(slot.getPart().getShortTitle());
            builder.append(' ').append(slot.getTerm().getIntValue()).append(" с. (").append(slot.getCourse().getIntValue()).append(" к.").append(")");
            final EppWorkPlanRow sourceRow = slot.getSourceRow();
            if (null != sourceRow) {
                builder.append(" [").append(StringUtils.left(sourceRow.getWorkPlan().getState().getTitle(), 4)).append("]");
                builder.append(' ').append(StringUtils.trimToEmpty(sourceRow.getNumber()));
                final String realTitle = sourceRow.getTitle();
                final int i = realTitle.indexOf(' ', 20);
                final String trimmedTitle = StringUtils.left(realTitle, (i < 1 ? 20 : i));
                if ((trimmedTitle.length()+3) < realTitle.length()) {
                    builder.append(' ').append(trimmedTitle).append("...");
                } else {
                    builder.append(' ').append(realTitle);
                }
            }
            builder.append(" (").append(slot.getRegistryElementPart().getNumber()).append("/").append(slot.getRegistryElementPart().getRegistryElement().getParts()).append(", ").append(UniEppUtils.formatLoad(slot.getRegistryElementPart().getRegistryElement().getSizeAsDouble(), false)).append(" час.)");
            builder.append("\n");
        }

        wrapper.setProperty(VIEW_PROP_BROKEN_SLOT_LIST, builder.toString());
    }

    protected DQLSelectBuilder getGapsSlotDql()
    {
        return new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, "slot")
        .column(property(EppStudentWorkPlanElement.student().id().fromAlias("slot")), "student_id")
        .column(property(EppStudentWorkPlanElement.registryElementPart().registryElement().id().fromAlias("slot")), "regel_id")
        .where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("slot"))))
        .group(property(EppStudentWorkPlanElement.student().id().fromAlias("slot")))
        .group(property(EppStudentWorkPlanElement.registryElementPart().registryElement().id().fromAlias("slot")))
        .having(not(
            and(
                eq(
                    count(property(EppStudentWorkPlanElement.registryElementPart().number().fromAlias("slot"))),
                    max(property(EppStudentWorkPlanElement.registryElementPart().number().fromAlias("slot")))
                ),
                eq(
                    count(DQLPredicateType.distinct, property(EppStudentWorkPlanElement.registryElementPart().number().fromAlias("slot"))),
                    count(property(EppStudentWorkPlanElement.registryElementPart().number().fromAlias("slot")))
                )
            )
        ));
    }
}
