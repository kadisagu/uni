package ru.tandemservice.uniepp.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.tapestry.json.JSONArray;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.ui.formatters.CourseRomanFormatter;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.plan.EppWeekPart;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;

import java.text.ParseException;
import java.util.*;

/**
 * @author nsvetlov
 */
public abstract class EppEduPlanVersionScheduleGridUtils extends EppEduPlanVersionScheduleUtils
{
    public static final String CUSTOM_GRID_STYLE =
            "        /* All headers */\n" +
            "        .handsontable th {\n" +
            "            background-color: #d8e5f3;\n" +
            "            border: 1px solid #9fb6d0;\n" +
            "            font-weight: bold;\n" +
            "        }\n" +
            "       .ht_master tr td {\n" +
            "            border: 1px solid #9fb6d0;" +
            "        }\n" +
            "        /* Column headers */\n" +
            "        .ht_clone_top th  {\n" +
            "            background-color: #d8e5f3;\n" +
            "        }\n" +
            "       /* Every odd row */\n" +
            "       .ht_master tr:nth-of-type(odd) > td {\n" +
            "         background-color: #f7fbff;\n" +
            "        }\n";

    public static final EppWeekType THEORY = UniDaoFacade.getCoreDao().getCatalogItem(EppWeekType.class, EppWeekType.CODE_THEORY);
    public static final String COURSE = "course";
    public static final int DAYS_IN_WEEK = 6;
    public static final String FIRST = "first";
    public static final String LAST = "last";
    public static final String DAY = "wn";
    public static final String DIFF = "diff";// { showError('Форма заполнена с ошибками!'); } else
    public static final String JS = "doSave(event, this)";
    public static final String NO_MERGE = "m_";
    private Map<TripletKey<Long, Long, String>, EppWeekPart> _dataMap;
    private SimpleListDataSource<ViewWrapper<EppWeekPart>> _scheduleDataSource;
    private Map<Long, TermsBorders> _termsBordersByCourse;
    private Map<PairKey<Long, String>, EppEduPlanVersionWeekType> _versionWeekByNum;


    public EppEduPlanVersionScheduleGridUtils()
    {
        super();
    }

    protected boolean isViewMode()
    {
        return !isEditMode() || isReadOnly();
    }

    ;

    protected boolean isReadOnly()
    {
        if (!CoreServices.securityService().check(getEduPlanVersion(), ContextLocal.getUserContext().getPrincipalContext(), "editSchedule_eppEduPlanVersion"))
        {
            return true;
        }

        return getEduPlanVersion().getState().isReadOnlyState();
    }

    public Map<TripletKey<Long, Long, String>, EppWeekPart> getDataMap()
    {
        if (_dataMap == null)
        {
            _dataMap = new HashMap<>();
        }
        return _dataMap;
    }


    private SimpleListDataSource<ViewWrapper<EppWeekPart>> buildWeekPartDataSource()
    {
        List<ViewWrapper<EppWeekPart>> list = new ArrayList<>();

        fillRowList(list, createRowList(list));

        SimpleListDataSource<ViewWrapper<EppWeekPart>> dataSource = new SimpleListDataSource<>();

        dataSource.addColumn(getHeadColumn());
        dataSource.setEntityCollection(list);
        dataSource.setCountRow(list.size());

        return dataSource;
    }

    public HeadColumn getHeadColumn()
    {
        HeadColumn header = new HeadColumn("header", "Учебный график");

        IMergeRowIdResolver corseResolver = entity -> String.valueOf(((ViewWrapper<EppWeekPart>) entity).getViewProperty(COURSE));
        IMergeRowIdResolver dayResolver = entity -> String.valueOf(((ViewWrapper<EppWeekPart>) entity).getViewProperty(null == ((ViewWrapper<EppWeekPart>) entity).getViewProperty(DIFF) ? COURSE : DAY));
        SimpleColumn weekNum = new SimpleColumn("День недели", DAY);
        weekNum.setWidth(24)
                .setAlign("center")
                .setVerticalHeader(true)
                .setDisabled(true)
                .setMergeRowIdResolver(dayResolver);
        ;
        if (!isReadOnly())
        { // TODO: исправить логику отображения заголовков
            weekNum.setHeaderAlign("left");
        }
        HeadColumn weekNumHead = new HeadColumn("", "");
        weekNumHead.setWidth(24);
        weekNumHead.addColumn(weekNum);
        header.addColumn(weekNumHead);

        HeadColumn courseBlank = new HeadColumn("courseBlank", "");
        AbstractColumn course = new PublisherLinkColumn("Курс", COURSE)
                .setFormatter(CourseRomanFormatter.INSTANCE_PLAIN).setRequired(true)
                .setWidth(56)
                .setAlign("right")
                .setMergeRowIdResolver(corseResolver);
        courseBlank.addColumn(course);
        header.addColumn(courseBlank);

        header.addColumn(new SimpleColumn("", FIRST).setVisible(false));
        header.addColumn(new SimpleColumn("", LAST).setVisible(false));

        HeadColumn monthHead = null;
        String lastHeadName = null;

        for (final EppWeek week : getGlobalWeekList())
        {
            final String headName = "month." + week.getMonth();
            if ((null == monthHead) || (!lastHeadName.equals(headName)))
            {
                if (monthHead != null)
                {
                    header.addColumn(monthHead);
                }
                monthHead = new HeadColumn(headName, RussianDateFormatUtils.getMonthName(week.getMonth(), true));
                lastHeadName = headName;
            }
            HeadColumn weekColumn = new HeadColumn("week." + week.getCode(), week.getTitle());
            weekColumn.setWidth(24);
            weekColumn.setVerticalHeader(true);

            final String weekKey = getWeekCode(week);
            final String noMergeKey = NO_MERGE + weekKey;
            SimpleColumn column = new SimpleColumn(String.valueOf(week.getNumber()), weekKey);
            column.setWidth(24).setAlign("center");
            column.setMergeRowIdResolver(entity -> String.valueOf(((ViewWrapper<EppWeekPart>) entity).getViewProperty(COURSE)) + "-" +
                    String.valueOf(((ViewWrapper<EppWeekPart>) entity).getViewProperty(null == ((ViewWrapper<EppWeekPart>) entity).getViewProperty(noMergeKey) ? weekKey : DAY)));
            weekColumn.addColumn(column);
            monthHead.addColumn(weekColumn);
        }

        if (monthHead != null)
        {
            header.addColumn(monthHead);
        }

        String headMenu = getHeadMenu();
        if (StringUtils.isNotEmpty(headMenu))
        {
            return new HeadColumn("", headMenu).addColumn(header);
        }
        return header;
    }

    private String getHeadMenu()
    {
        return getHeadMenu(isReadOnly(), isViewMode());
    }

    public static String getHeadMenu(boolean readOnly, boolean viewMode)
    {
        return readOnly ? null :
                (viewMode ? "" : "<div style=\"display: none;\" class=\"sticky error\" id=\"errorContainerGrid\"></div>") +
                "<span align='left' style='width: 100%'>" +
                        (viewMode ? getDefaultButtonString("Edit", "Редактировать") :
                                getButtonString("Save", "Сохранить",
                                        JS) +
                                getButtonString("Apply", "Применить",
                                        JS) +
                                getDefaultButtonString("Cancel", "Отменить") +
                                        "</span><br><span>" +
                                getButtonIconString("split", "Разбить неделю") +
                                getButtonIconString("merge", "Объединить неделю") +
                                getButtonIconString("first", "Первая неделя семестра") +
                                getButtonIconString("last", "Последняя неделя семестра")
                        ) + "</span>";
    }

    private static String getButtonIconString(String id, String label)
    {
        return "<a href='javasrcript:void(0);' onclick='return false;'><img src='img/" + id + ".png' + title='" + label + "' onmouseup='emuClick(hot,\"" +id + "\");' />&nbsp&nbsp&nbsp&nbsp</a>";
    }


    private static String getDefaultButtonString(String id, String label)
    {
        return getButtonString(id, label, "buttonClick(event, this);");
    }

    private static String getButtonString(String id, String label, String js)
    {
        return "<div class='tf-btn' " +
                "onmouseup=\"javascript:" +
                js +
                ";\" href='javascript:void(0);' name='_b" + id + "' id='b" + id +
                "' lostFocus='true' mcValidate='false'>" + label + "</div>&nbsp;\n";
    }

    private int[] getTermBorders(RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> rangeModel, Course course)
    {
        int[] boderNums = rangeModel.getRow2points().get(course.getId());
        int[] emptyTerms = EppEduPlanVersionScheduleUtils.getPoints4EmptyGridRow(getEduPlanVersion().getEduPlan().getDevelopGrid(), course);
        if (boderNums != null)
        {
            for (int i = 0; i < boderNums.length; i++)
            {
                emptyTerms[i] = boderNums[i];
            }
        }
        return emptyTerms;
    }

    private void fillRowList(List<ViewWrapper<EppWeekPart>> list, Set<PairKey<Long, String>> nonCollapsedWeek)
    {
        for (final ViewWrapper<EppWeekPart> row : list)
        {
            EppWeekPart weekPart = row.getEntity();
            Course course = row.getEntity().getWeek().getCourse();
            row.setViewProperty(COURSE, course.getIntValue());

            TermsBorders termsBorders = new TermsBorders(getDevelopGridDetail().get(course), getTermBorders(getRangeSelectionListDataSource(), course));
            getTermsBordersByCourse().put(course.getId(), termsBorders);

            int number = weekPart.getNumber();
            if (1 == number)
            {
                JSONArray firstArr = new JSONArray();
                JSONArray lastArr = new JSONArray();
                for (int i = 0; i < termsBorders.borders.length; i++) {
                    if (i % 2 == 0)
                        firstArr.put(termsBorders.borders[i]);
                    else
                        lastArr.put(termsBorders.borders[i]);
                }
                row.setViewProperty(FIRST, firstArr.toString());
                row.setViewProperty(LAST, lastArr.toString());
            }
            for (EppWeek week : getGlobalWeekList())
            {
                TripletKey<Long, Long, String> key = TripletKey.create(weekPart.getWeek().getCourse().getId(), (long) number, getWeekCode(week));
                EppWeekType value = _dataMap.containsKey(key) ? _dataMap.get(key).getWeekType() : null;
                row.setViewProperty(key.getThird(), null == value ? "" : isViewMode() ? value.getAbbreviationView() : value.getAbbreviationEdit());
                if (nonCollapsedWeek.contains(new PairKey<>(key.getFirst(), key.getThird())))
                {
                    row.setViewProperty(NO_MERGE + key.getThird(), "1");
                }
            }
        }
    }

    private Set<PairKey<Long, String>> createRowList(List<ViewWrapper<EppWeekPart>> list)
    {

        Map<Long, EppEduPlanVersionWeekType> firstWeekTypes = new HashMap<>();
        Set<Long> nonCollapsed = new HashSet<>();
        Set<PairKey<Long, String>> nonCollapsedWeek = new HashSet<>();
        long newVersionWeekId = 0;
        for (final EppEduPlanVersionWeekType item : this.epvWeekTypeList)
        {
            final EppWeek week = item.getWeek();
            final Long courseId = item.getCourse().getId();
            String weekCode = getWeekCode(week);
            getVersionWeekByNum().put(new PairKey<>(courseId, weekCode), item);

            final List<EppWeekPart> epvWeekPartList = UniDaoFacade.getCoreDao().getList( //TODO: свести множественные запросы в один, по версии учебного плана
                    EppWeekPart.class,
                    EppWeekPart.week(), item
            );

            EppWeekType value = null;
            for (final EppWeekPart weekPart : epvWeekPartList)
            {
                TripletKey<Long, Long, String> key = TripletKey.create(courseId, (long) weekPart.getNumber(), weekCode);
                getDataMap().put(key, weekPart);
                if (value == null)
                {
                    value = weekPart.getWeekType();
                } else
                {
                    if (value.getId() != weekPart.getWeekType().getId())
                    {
                        nonCollapsed.add(courseId);
                        nonCollapsedWeek.add(new PairKey<>(key.getFirst(), key.getThird()));
                    }
                }
            }

            if (week.getNumber() == 1)
            { // Первая неделя, будет ключевой сущностью в строке
                firstWeekTypes.put(courseId, item);
            }
        }
        EppWeek firstWeek = getGlobalWeekList().get(0);
        for (Course course : getDevelopGridDetail().keySet())
        {
            for (int i = 1; i <= DAYS_IN_WEEK; i++)
            {
                ViewWrapper<EppWeekPart> newRow;
                TripletKey<Long, Long, String> firstWeekKey = TripletKey.create(course.getId(), (long) i, getWeekCode(firstWeek));

                if (!getDataMap().containsKey(firstWeekKey))
                {
                    EppWeekPart firstWeekPart = new EppWeekPart();
                    firstWeekPart.setNumber(i);

                    EppEduPlanVersionWeekType versionWeek;
                    if (firstWeekTypes.containsKey(course.getId()))
                    {
                        versionWeek = firstWeekTypes.get(course.getId());
                    } else
                    {
                        versionWeek = new EppEduPlanVersionWeekType(this.getEduPlanVersion(), course, firstWeek);
                        versionWeek.setWeekType(THEORY);
                        versionWeek.setId(--newVersionWeekId);
                        firstWeekTypes.put(course.getId(), versionWeek);
                        getVersionWeekByNum().put(new PairKey<>(course.getId(), getWeekCode(firstWeek)), versionWeek);
                    }
                    firstWeekPart.setWeek(versionWeek);
                    firstWeekPart.setId(--newVersionWeekId);
                    getDataMap().put(firstWeekKey, firstWeekPart);
                    newRow = new ViewWrapper<>(firstWeekPart);
                    list.add(newRow);
                } else
                {
                    newRow = new ViewWrapper<>(getDataMap().get(firstWeekKey));
                    list.add(newRow);
                }
                newRow.setViewProperty(DAY, i);
                if (nonCollapsed.contains(course.getId()))
                {
                    newRow.setViewProperty(DIFF, "1");
                } else if (i == 1) {
                    newRow.setViewProperty(DAY, "");
                }
            }
        }
        return nonCollapsedWeek;
    }

    public static String getWeekCode(EppWeek week)
    {
        return "w" + week.getNumber();
    }

    public static Integer parseWeekCode(String code)
    {
        if (!code.startsWith("w"))
        {
            return null;
        }
        return Integer.parseInt(code.substring(1));
    }

    public static boolean isNewEntity(IEntity entity)
    {
        return entity == null || entity.getId() == null || entity.getId() < 0;
    }

    public SimpleListDataSource<ViewWrapper<EppWeekPart>> getScheduleDataSource()
    {
        if (_scheduleDataSource == null)
        {
            _scheduleDataSource = buildWeekPartDataSource();
        }
        return _scheduleDataSource;
    }


    @Override
    public List<EppWeekType> getEppWeekTypes()
    {
        Set<EppWeekType> types = new HashSet<>();
        for(EppWeekPart part : _dataMap.values())
        {
            if (part.getWeekType() != null )
                types.add(part.getWeekType());
        }
        List<EppWeekType> result = new ArrayList<>(types);
        Collections.sort(result, (a, b) -> (a.getPriority() - b.getPriority()) );
        return result;
    }

    @Override
    protected Map<String, Map<String, Number>> buildWeekTypeAggregationMap()
    {
        getScheduleDataSource();
        // подсчет сводных данных
        // номер_курса -> код_типа_недели  -> количество_недель_типа
        final Map<String, Map<String, MutableInt>> weekTypeSum = SafeMap.get(key -> new HashMap<>());
        final Map<String, MutableInt> columnCount = new HashMap<>();

        // подсчет значений по курсам
        // подсчет колонки всего горизонтальной
        for (EppWeekPart part : getDataMap().values())
        {
            final String courseNumber = String.valueOf(part.getWeek().getCourse().getIntValue());
            if (part.getWeekType() == null)
            {
                continue;
            }
            final String weekTypeCode = part.getWeekType().getCode();

            final Map<String, MutableInt> rowMap = weekTypeSum.get(courseNumber);
            if (rowMap.containsKey(weekTypeCode))
            {
                rowMap.get(weekTypeCode).increment();
            } else
            {
                rowMap.put(weekTypeCode, new PartWeekMutableInt(1));
            }
            // подсчет колонки всего горизонтальной
            if (columnCount.containsKey(weekTypeCode))
            {
                columnCount.get(weekTypeCode).increment();
            } else
            {
                columnCount.put(weekTypeCode, new PartWeekMutableInt(1));
            }
        }
        weekTypeSum.put("columnCount", columnCount);

        // подсчет колонки всего вертикальной
        for (final Map.Entry<String, Map<String, MutableInt>> row : weekTypeSum.entrySet())
        {
            final MutableInt value = new PartWeekMutableInt(0);
            for (final MutableInt intValue : row.getValue().values())
            {
                value.add(intValue);
            }
            row.getValue().put("rowCount", value);
        }

        return (Map) weekTypeSum;
    }

    public abstract Boolean isEditMode();

    public Map<Long, TermsBorders> getTermsBordersByCourse()
    {
        if (_termsBordersByCourse == null)
        {
            _termsBordersByCourse = new HashMap<>();
        }
        return _termsBordersByCourse;
    }

    public TermsBorders findBorder(EppWeekPart weekPart)
    {
        return _termsBordersByCourse.get(weekPart.getWeek().getCourse().getId());
    }

    public Map<PairKey<Long, String>, EppEduPlanVersionWeekType> getVersionWeekByNum()
    {
        if (_versionWeekByNum == null)
        {
            _versionWeekByNum = new HashMap<>();
        }
        return _versionWeekByNum;
    }


    public static int[] parseBorders(final String newValue) throws ParseException
    {
        if(null != newValue && !newValue.isEmpty())
        {
            JSONArray jsonArray = new JSONArray(newValue);
            int length = jsonArray.length();
            int[] numbers = new int[length];
            for (int i = 0; i < length; i++) {
                numbers[i] = jsonArray.getInt(i);
            }
            return numbers;
        }
        return new int[0];
    }


    private class PartWeekMutableInt extends MutableInt
    {
        public PartWeekMutableInt(int i)
        {
            super(i);
        }

        @Override
        public String toString()
        {
            int i = intValue() / DAYS_IN_WEEK;
            int r = intValue() % DAYS_IN_WEEK;
            String result = getResult(i);
            if (r > 0)
            {
                result += " " + r + "/6";
            }
            return result;
        }

        protected String getResult(int i)
        {
            return i == 0 ? "" : Integer.toString(i);
        }
    }

    private class PartWeekWithZeroMutableInt extends PartWeekMutableInt {
        public PartWeekWithZeroMutableInt(int i) { super(i); }

        @Override
        protected String getResult(int i)
        {
            return Integer.toString(i);
        }
    }

    @Override
    protected MutableInt getTermSize(Integer term, IEppEpvBlockWrapper epvWrapper, String code)
    {
        return new PartWeekWithZeroMutableInt(IEppEduPlanVersionDataDAO.instance.get().getTermSize(version.getId(), code, term));
    }

    @Override
    protected String getDivisor()
    {
        return "; самост. - ";
    }

    @Override
    protected String getBracket()
    {
        return " (ауд. - ";
    }

}
