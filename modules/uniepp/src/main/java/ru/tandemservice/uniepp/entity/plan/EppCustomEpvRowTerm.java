package ru.tandemservice.uniepp.entity.plan;

import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.gen.EppCustomEpvRowTermGen;

/** @see ru.tandemservice.uniepp.entity.plan.gen.EppCustomEpvRowTermGen */
public class EppCustomEpvRowTerm extends EppCustomEpvRowTermGen
{
    public EppCustomEpvRowTerm() {
    }

    public EppCustomEpvRowTerm(EppEpvRowTerm eppEpvRowTerm, EppCustomEduPlan customEduPlan) {
        setEpvRowTerm(eppEpvRowTerm);
        setCustomEduPlan(customEduPlan);
    }
}