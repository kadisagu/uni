/* $Id$ */
package ru.tandemservice.uniepp.component.registry.RegElementGradeScales;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author oleyba
 * @since 11/15/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickEditTable(final IBusinessComponent component)
    {
        this.getModel(component).setEditMode(true);
    }

    public void onClickSaveTable(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().update(model);
        model.setEditMode(false);
    }

}
