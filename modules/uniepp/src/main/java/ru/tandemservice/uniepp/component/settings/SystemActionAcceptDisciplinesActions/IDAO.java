package ru.tandemservice.uniepp.component.settings.SystemActionAcceptDisciplinesActions;

import java.util.Set;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
    void doAcceptDisciplinesAndActions(Set<Long> selectedIds);
}
