/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uni.sec.OrgUnitHolder;

/**
 * @author oleyba
 * @since 9/16/14
 */
public abstract class AbstractEppIndicatorStudentListPresenter extends AbstractUniStudentListUI
{
    public static final String PROP_ORG_UNIT = "orgUnit";

    private final OrgUnitHolder _orgUnitHolder = new OrgUnitHolder();

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        getOrgUnitHolder().refresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(PROP_ORG_UNIT, getOrgUnitHolder().getValue());
    }

    public boolean isOrgUnitPage() {
        return getOrgUnitHolder().getValue() != null;
    }

    public String getViewPermissionKey() {
        if (null != getOrgUnitHolder().getValue()) {
            return getOrgUnitHolder().getSecModel().getPermission("eppGroupOuIndicatorBlockView");
        }
        return "";
    }

    // Getters & Setters

    public OrgUnitHolder getOrgUnitHolder()
    {
        return _orgUnitHolder;
    }
}
