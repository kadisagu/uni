package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.ScheduleGrid;

import org.apache.tapestry.json.JSONArray;
import org.apache.tapestry.json.JSONLiteral;
import org.apache.tapestry.json.JSONObject;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.*;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.commonbase.tapestry.component.spreadsheet.ISpreadsheetCellDataValueResolver;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.plan.EppWeekPart;
import ru.tandemservice.uniepp.util.EppEduPlanVersionScheduleGridUtils;
import ru.tandemservice.uniepp.util.TermsBorders;
import ru.tandemservice.uniepp.util.WeekTypeLegendRow;

import java.text.ParseException;
import java.util.*;

import static ru.tandemservice.uniepp.util.EppEduPlanVersionScheduleGridUtils.*;


/**
 * Created by nsvetlov on 30.05.2016.
 */
@Input({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})

public class EppEduPlanVersionScheduleGridUI extends UIPresenter
{
    private EntityHolder<EppEduPlanVersion> _holder = new EntityHolder<>();
    private EppEduPlanVersionScheduleGridUtils _schedule;
    private Map<String, EppWeek> _weekIdByNum = new HashMap<>();
    private final Map<String, EppWeekType> _weekTypeMap = IEppSettingsDAO.instance.get().getWeekTypeEditAbbreviationMap();
    private Boolean _editMode = false;
    private SimpleListDataSource<ViewWrapper<EppWeekPart>> _dataSource;
    private StaticListDataSource<ViewWrapper<Course>> _timesDataSource;
    private List<WeekTypeLegendRow> _weekTypeLegendList;          // строки легенды
    private WeekTypeLegendRow _weekTypeLegendItem;                // текущее значение цикла
    private Map<TripletKey<Long, Long, String>, EppWeekPart> changes;
    private boolean lastEditableState = false;
    private boolean hasDeletedWeeks = false;

    public final ISpreadsheetCellDataValueResolver CELL_DATA_VALUE_RESOLVER = new ISpreadsheetCellDataValueResolver()
    {
        @Override
        public CoreCollectionUtils.Pair<ResolverResult, Object> resolve(int col, int row, AbstractColumn column, IEntity rowEntity, String newValue)
        {
            try
            {
                boolean isNotEmpty = newValue != null && newValue.length() > 0;
                EppWeekPart weekPart = ((ViewWrapper<EppWeekPart>) rowEntity).getEntity();
                TermsBorders border = _schedule.findBorder(weekPart);
                if (FIRST.equals(column.getKey()))
                {
                    int[] numbers = parseBorders(newValue);
                    for(int i = 0; i < numbers.length; i++)
                        border.setFirst(i, numbers[i]);
                } else if (LAST.equals(column.getKey()))
                {
                    int[] numbers = parseBorders(newValue);
                    for(int i = 0; i < numbers.length; i++)
                        border.setLast(i, numbers[i]);
                } else
                {
                    if (border.out(column.getKey().toString())) {
                        if (isNotEmpty)
                        {
                            return returnError("Заполнено значение за пределами семестра");
                        }
                    } else if (!isNotEmpty) { // Заменяем пустые значения на тип "теория"
                        newValue = THEORY.getAbbreviationEdit();
                        isNotEmpty = true;
                    }
                    if (isNotEmpty && !getWeekTypeMap().containsKey(newValue.toUpperCase()))
                    {
                        return returnError("Неизвестный тип недели «" + newValue + "»");
                    }
                    prepareWeekPartForSave(column.getKey().toString(), weekPart, newValue, isNotEmpty);
                }
                return null;
            } catch (Exception ex)
            {
                return returnError(ex.getMessage() == null ? "Ошибка сохранения ячейки" : ex.getMessage());
            }
        }

        private CoreCollectionUtils.Pair<ResolverResult, Object> returnError(String errorText)
        {
            ContextLocal.getErrorCollector().add(errorText);
            return new CoreCollectionUtils.Pair<>(ResolverResult.ERROR, errorText);
        }
    };

    @Override
    public void onComponentRefresh()
    {
        setWeekTypeLegendList(IEppEduPlanDAO.instance.get().getWeekTypeLegendRowList(null));
        initSchedule();
    }

    private void initSchedule()
    {
        if (_schedule == null || lastEditableState != getCanEdit())
        {
            lastEditableState = getCanEdit();
            _schedule = new EppEduPlanVersionScheduleGridUtils()
            {
                @Override
                public Boolean isEditMode()
                {
                    return getEditMode();
                }

                @Override
                protected boolean isTotalCourseRowPresent()
                {
                    return false;
                }

                @Override
                protected EppEduPlanVersion getEduPlanVersion()
                {
                    return getHolder().getValue();
                }
            };
        }

        for (EppWeek globalWeek : _schedule.getGlobalWeekList())
        {
            _weekIdByNum.put(getWeekCode(globalWeek), globalWeek);
        }
        _dataSource = _schedule.getScheduleDataSource();
        _timesDataSource = _schedule.getTimesDataSource();
        _timesDataSource.getColumn(0).setHeaderAlign("center").setAlign("right").setWidth(6);
    }

    public SimpleListDataSource<ViewWrapper<EppWeekPart>> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(SimpleListDataSource<ViewWrapper<EppWeekPart>> dataSource)
    {
        _dataSource = dataSource;
    }

    public ISpreadsheetCellDataValueResolver getCellDataValueResolver()
    {
        return CELL_DATA_VALUE_RESOLVER;
    }

    public EntityHolder<EppEduPlanVersion> getHolder()
    {
        return _holder;
    }

    public JSONObject getUserOptions() throws ParseException
    {
        JSONObject options = new JSONObject()
                .put("autoRowSize", false)
                .put("autoColumnSize", false)
                .put("manualColumnResize", false)
                .put("manualColumnMove", false)
                .put("manualRowResize", false)
                .put("fillHandle", true)
                .put("renderAllRows", true)
                .put("currentRowClassName", "'currentRow'")
                .put("currentColClassName", "'currentCol'")
                .put("customBorders", getCustomBorders())
                .put("maxRows", _dataSource.getCountRow())
                .put("beforeChange", new JSONLiteral("beforeChangeValue") );

        if (getEditMode())
        {
            options.put("contextMenu", getContextMenu());
        } else
        {            options.put("readOnly", true);
            options.put("contextMenu", false);
        }

        return options;
    }

    JSONArray getCustomBorders() throws ParseException
    {
        JSONArray borders = new JSONArray();
        JSONObject borderStyle = new JSONObject("{width: 3, color: \"#456fa9\"}");

        int i = 0;
        List<ViewWrapper<EppWeekPart>> entityList = getDataSource().getEntityList();
        for (ViewWrapper<EppWeekPart> row : entityList)
        {
            int j = 0;
            int lastCol = -1;
            for (Integer col : _schedule.getTermsBordersByCourse().get(row.getEntity().getWeek().getCourse().getId()).borders)
            {
                row.setViewProperty("t" + j, col);
                JSONObject border = new JSONObject()
                        .put("row", i)
                        .put("col", col + 2) // +2 т.к. у нас первые колонки - номер дня и курс
                        .put(j++ % 2 == 0 ? "left" : "right", borderStyle);
                if (j % 2 == 0 && lastCol == col) // Случай, когда семестр длится одну неделю.
                    border.put("left", borderStyle);
                lastCol = col;
                borders.put(border);
            }
            i++;
        }

        return borders;
    }


    JSONObject getContextMenu() throws ParseException
    {
        JSONObject contextMenu = new JSONObject();
        JSONObject items = new JSONObject("{'undo': { name: 'Отменить действие'},'redo': { name: 'Повторить'}, " +
                "'s1': '---------'}");
        items.put("split", new JSONLiteral("{name: 'Разбить неделю'   , disabled: nonWeek }"));
        items.put("merge", new JSONLiteral("{name: 'Объединить неделю', disabled: nonWeek }"));
        items.put("s2", "---------");
        items.put("first", new JSONLiteral("{name: 'Первая неделя семестра'   , disabled: nonWeek }"));
        items.put("last",  new JSONLiteral("{name: 'Последняя неделя семестра', disabled: nonWeek }"));


        contextMenu.put("items", items);

        contextMenu.put("callback", new JSONLiteral("menuClick"));
        return contextMenu;
    }

    public StaticListDataSource<ViewWrapper<Course>> getTimesDataSource()
    {
        return this._timesDataSource;
    }

    public List<WeekTypeLegendRow> getWeekTypeLegendList()
    {
        return this._weekTypeLegendList;
    }

    public void setWeekTypeLegendList(final List<WeekTypeLegendRow> weekTypeLegendList)
    {
        this._weekTypeLegendList = weekTypeLegendList;
    }

    public WeekTypeLegendRow getWeekTypeLegendItem()
    {
        return this._weekTypeLegendItem;
    }

    public void setWeekTypeLegendItem(final WeekTypeLegendRow weekTypeLegendItem)
    {
        this._weekTypeLegendItem = weekTypeLegendItem;
    }

    public Boolean getCanEdit()
    {
        if (!CoreServices.securityService().check(_holder.getValue(), ContextLocal.getUserContext().getPrincipalContext(), "editSchedule_eppEduPlanVersion"))
        {
            return false;
        }

        final EppEduPlanVersion version = _holder.refresh(EppEduPlanVersion.class);
        if (version.getState().isReadOnlyState())
        {
            return false;
        }
        return !getEditMode();
    }

    public void onClickEdit()
    {
        if (getCanEdit())
        {
            setEditMode(true);
            changes = new HashMap<>();
            _dataSource.getColumns().clear();
            _dataSource.addColumn(_schedule.getHeadColumn());

            prepareEdit();
        }
    }

    public Boolean getCanSave()
    {
        return getEditMode();
    }

    public void onClickSave()
    {
        saveChanges();
        setEditMode(false);
        refreshDataSource();
    }

    public void onClickApply()
    {
        saveChanges();
    }

    public void onClickCopyScheduleData()
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.eduplan.EduPlanVersionCopyScheduleData.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, _holder.getId())
        ));
    }

    public void onClickCancel()
    {
        setEditMode(false);
        refreshDataSource();
    }

    @Override
    public void onComponentDeactivate() {
        setEditMode(false);

    }

    public String getCustomStyle()
    {
        if (getEditMode())
        {
            return null;
        }
        return CUSTOM_GRID_STYLE;
    }

    private void refreshDataSource()
    {
        _dataSource.getColumns().clear();
        _dataSource.addColumn(_schedule.getHeadColumn());
        _schedule = null;
        initSchedule();
    }


    private void prepareEdit()
    {
        Map<TripletKey<Long, Long, String>, EppWeekPart> dataMap = _schedule.getDataMap();
        for (ViewWrapper<EppWeekPart> row : _dataSource.getEntityList())
        {
            Course course = row.getEntity().getWeek().getCourse();
            TermsBorders borders = _schedule.getTermsBordersByCourse().get(course.getId());
            for (int nTerm = 0; nTerm < borders.terms.length; nTerm++)
            {
                if (borders.borders.length > nTerm * 2)
                {
                    for (int nWeek = borders.borders[nTerm * 2]; nWeek <= borders.borders[nTerm * 2 + 1]; nWeek++)
                    {
                        String weekCode = "w" + (nWeek + 1);
                        TripletKey<Long, Long, Object> key = new TripletKey<>(course.getId(), (long) row.getEntity().getNumber(), weekCode);
                        EppWeekPart weekPart = dataMap.get(key);
                        row.setViewProperty(weekCode, (null == weekPart || null == weekPart.getWeekType()) ? THEORY.getAbbreviationEdit() : weekPart.getWeekType().getAbbreviationEdit());
                    }
                }
            }
        }
    }

    private void saveChanges()
    {
        List<EppEduPlanVersionWeekType> weeksToDelete = updateVersionWeeks();
        IUniBaseDao coreDao = UniDaoFacade.getCoreDao();
        for (Map.Entry<TripletKey<Long, Long, String>, EppWeekPart> entry : changes.entrySet())
        {
            EppWeekPart toChange = _schedule.getDataMap().get(entry.getKey());
            EppWeekPart newValue = entry.getValue();
            if (toChange == null || newValue == null)
            {
                if (toChange != null)
                { // Удаляем часть недели
                    if (null != toChange.getWeekType())
                    {
                        coreDao.delete(toChange);
                        _schedule.getDataMap().remove(entry.getKey());
                    }
                } else if (newValue != null)
                { // Созаем новую часть недели
                    coreDao.save(newValue);
                    _schedule.getDataMap().put(entry.getKey(), newValue);
                }
            } else
            { // Часть недели уже сущесвует, просто меняем значение
                if (isNewEntity(toChange) || !toChange.getWeekType().getId().equals(newValue.getWeekType().getId()))
                {
                    toChange.setWeekType(newValue.getWeekType());
                    if (isNewEntity(toChange))
                    {
                        toChange.setId(null);
                    }
                    coreDao.saveOrUpdate(toChange);
                }
            }
        }
        coreDao.executeFlush();
        changes.clear();
        if (weeksToDelete.isEmpty())
        {
            hasDeletedWeeks = false;
            return;
        }
        hasDeletedWeeks = true;
        for (EppEduPlanVersionWeekType week : weeksToDelete)
        {
            if (week != null && week.getId() != null && week.getId() > 0)
            {
                week = coreDao.get(EppEduPlanVersionWeekType.class, week.getId());
                if (week != null)
                    coreDao.delete(week);
            }
        }
        coreDao.executeFlush();
    }

    private List<EppEduPlanVersionWeekType> updateVersionWeeks()
    {
        Map<Integer, Term> termMap = DevelopGridDAO.getTermMap();
        List<EppEduPlanVersionWeekType> toDelete = new ArrayList<>();

        for (Map.Entry<PairKey<Long, String>, EppEduPlanVersionWeekType> entry : _schedule.getVersionWeekByNum().entrySet())
        {
            EppEduPlanVersionWeekType week = entry.getValue();
            String weekCode = entry.getKey().getSecond();
            Term term = _schedule.getTermsBordersByCourse().get(entry.getKey().getFirst()).getTerm(termMap, parseWeekCode(weekCode));
            if (term == null)
            {
                if (!isNewEntity(week))
                {
                    toDelete.add(week);
                }
            } else
            {
                if (week.getTerm() == null || isNewEntity(week) || week.getTerm().getIntValue() != term.getIntValue())
                {
                    if (isNewEntity(week))
                    {
                        week.setId(null);
                    }
                    week.setTerm(term);
                    UniDaoFacade.getCoreDao().saveOrUpdate(week);
                }
            }
        }
        return toDelete;
    }

    private EppEduPlanVersionWeekType getVersionWeek(EppEduPlanVersionWeekType firstWeek, EppWeek globalWeek, PairKey<Long, String> key, EppWeekType newWeekType)
    {
        EppEduPlanVersionWeekType week;
        week = _schedule.getVersionWeekByNum().get(key);
        if (hasDeletedWeeks && week != null && week.getId() != null && week.getId() > 0) //Возможно мы уже удалили неделю, надо ее перегрузить
            week = UniDaoFacade.getCoreDao().get(EppEduPlanVersionWeekType.class, week.getId());

        if (week == null)
        {
            week = new EppEduPlanVersionWeekType(firstWeek.getEduPlanVersion(), firstWeek.getCourse(), globalWeek);
            week.setWeekType(newWeekType);
            _schedule.getVersionWeekByNum().put(key, week);
        }
        return week;
    }

    private Map<String, EppWeekType> getWeekTypeMap()
    {
        return _weekTypeMap;
    }

    private boolean prepareWeekPartForSave(String columnKey, EppWeekPart firstWeekPart, String newValue, boolean isNotEmpty)
    {
        EppWeek globalWeek = _weekIdByNum.get(columnKey);
        EppEduPlanVersionWeekType week = firstWeekPart.getWeek();
        long weekPartNumber = firstWeekPart.getNumber();
        TripletKey<Long, Long, String> key = new TripletKey<>(week.getCourse().getId(), weekPartNumber, columnKey);
        EppWeekPart weekPart = _schedule.getDataMap().get(key);
        EppWeekPart changedPart = null;

        if (isNotEmpty)
        {
            EppWeekType newWeekType = getWeekTypeMap().get(newValue.toUpperCase());
            changedPart = changes.get(key);
            if (changedPart != null)
            {
                changedPart.setWeekType(newWeekType);
            } else
            {
                changedPart = new EppWeekPart();
                changedPart.setWeekType(newWeekType);

                if (weekPart == null)
                { // Новое значение
                    week = getVersionWeek(firstWeekPart.getWeek(), globalWeek, new PairKey<>(key.getFirst(), key.getThird()), newWeekType);
                    changedPart.setWeek(week);
                    changedPart.setNumber((int) weekPartNumber);
                } else
                {
                    changedPart.setWeek(weekPart.getWeek());
                    changedPart.setNumber(weekPart.getNumber());
                }
            }
        }
        changes.put(key, changedPart);

        return false;
    }

    public Boolean getEditMode()
    {
        return _editMode;
    }

    public void setEditMode(Boolean _editMode)
    {
        this._editMode = _editMode;
    }
}
