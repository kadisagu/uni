/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.EppEpvRegRowProfessionalTaskList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.logic.EppEpvRegRowProfessionalTaskSearchDSHandler;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author Igor Belanov
 * @since 27.03.2017
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduPlanVersion.id", required = true)
})
public class EppDevelopResultEppEpvRegRowProfessionalTaskListUI extends UIPresenter
{
    // версия УП в рамках которой работаем
    private EppEduPlanVersion _eduPlanVersion = new EppEduPlanVersion();

    @Override
    public void onComponentRefresh()
    {
        if (_eduPlanVersion != null && _eduPlanVersion.getId() != null)
        {
            setEduPlanVersion(DataAccessServices.dao().get(_eduPlanVersion.getId()));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppDevelopResultEppEpvRegRowProfessionalTaskList.EPP_EPV_REG_ROW_PROFESSIONAL_TASK_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppEpvRegRowProfessionalTaskSearchDSHandler.PROP_EDU_PLAN_VERSION_ID, _eduPlanVersion.getId());
        }
    }

    // getters & setters
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        _eduPlanVersion = eduPlanVersion;
    }
}
