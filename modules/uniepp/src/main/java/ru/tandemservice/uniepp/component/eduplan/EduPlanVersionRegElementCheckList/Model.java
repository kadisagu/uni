/* $Id$ */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionRegElementCheckList;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author oleyba
 * @since 5/23/11
 */
@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id") })
public class Model
{
    private Long id;
    private StaticListDataSource<RowWrapper> dataSource = new StaticListDataSource<RowWrapper>();

    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public StaticListDataSource<RowWrapper> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final StaticListDataSource<RowWrapper> dataSource)
    {
        this.dataSource = dataSource;
    }

    protected static class RowWrapper extends IdentifiableWrapper<EppRegistryElement>
    {
        private static final long serialVersionUID = 1L;
        private int versionCount;
        private boolean hasErrors;

        public RowWrapper(final EppRegistryElement i) throws ClassCastException
        {
            super(i);
        }

        public int getVersionCount()
        {
            return this.versionCount;
        }

        public void setVersionCount(final int versionCount)
        {
            this.versionCount = versionCount;
        }

        public boolean isHasErrors()
        {
            return this.hasErrors;
        }

        public void setHasErrors(final boolean hasErrors)
        {
            this.hasErrors = hasErrors;
        }

        public String getState()
        {
            return this.hasErrors ? "есть отличия" : "нет отличий";
        }
    }
}
