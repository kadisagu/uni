/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask.EppProfessionalTaskManager;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;

/**
 * @author Igor Belanov
 * @since 23.03.2017
 */
@Configuration
public class EppProfessionalTaskAddEdit extends BusinessComponentManager
{
    public static final String PROGRAM_KIND_DS = "programKindDS";
    public static final String PROGRAM_SUBJECT_INDEX_DS = "programSubjectIndexDS";
    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
    public static final String PROF_ACTIVITY_TYPE_DS = "profActivityTypeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(PROGRAM_KIND_DS, EppProfessionalTaskManager.instance().programKindDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_INDEX_DS, EppProfessionalTaskManager.instance().programSubjectIndexDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_DS, EppProfessionalTaskManager.instance().programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(PROF_ACTIVITY_TYPE_DS, getName(), EppProfActivityType.defaultSelectDSHandler(getName())))
                .create();
    }
}
