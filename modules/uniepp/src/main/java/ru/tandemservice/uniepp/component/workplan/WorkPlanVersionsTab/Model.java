package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionsTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.component.base.SimpleOrgUnitBasedModel;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;


@State( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "workPlanHolder.id") })
public class Model extends SimpleOrgUnitBasedModel
{
    public static class BaseWorkplanNode extends IdentifiableWrapper<IEntity> implements IHierarchyItem, IEppStateObject
    {
        private static final long serialVersionUID = 1L;

        private final BaseWorkplanNode parent;
        @Override public IHierarchyItem getHierarhyParent() { return this.parent; }
        @Override public EppState getState() { return null; }

        public EppWorkPlanBase getPlan() {
            if (null != this.parent) { return this.parent.getPlan(); }
            return null;
        }

        public BaseWorkplanNode(final Long id, final BaseWorkplanNode parent, final String title) {
            super(id, title);
            this.parent = parent;
        }
    }

    public static class WorkPlanNode extends BaseWorkplanNode
    {
        private static final long serialVersionUID = 1L;

        private final EppWorkPlanBase plan;
        @Override public EppWorkPlanBase getPlan() { return this.plan; }
        @Override public EppState getState() { return this.getPlan().getState(); }

        public WorkPlanNode(final EppWorkPlanBase plan, final BaseWorkplanNode parent) {
            super(plan.getId(), parent, plan.getTitle());
            this.plan = plan;
        }

    }


    private final EntityHolder<EppWorkPlan> workPlanHolder = new EntityHolder<EppWorkPlan>();
    public EntityHolder<EppWorkPlan> getWorkPlanHolder() { return this.workPlanHolder; }
    public EppWorkPlan getWorkPlan() { return this.workPlanHolder.getValue(); }

    private final StaticListDataSource<BaseWorkplanNode> versionDistributionDataSource = new StaticListDataSource<BaseWorkplanNode>();
    public StaticListDataSource<BaseWorkplanNode> getVersionDistributionDataSource() { return this.versionDistributionDataSource; }

    @Override
    public OrgUnit getOrgUnit() {
        return null;
    }

    public static final long EMPTY_GROUP_ID = 1L;
    public static IdentifiableWrapper<Group> emptyGroupOption = new IdentifiableWrapper<Group>(Model.EMPTY_GROUP_ID, "вне групп");

    private IDataSettings settings;
    public IDataSettings getSettings() { return this.settings; }
    public void setSettings(final IDataSettings settings) { this.settings = settings; }

    private ISelectModel formativeOrgUnitListModel;
    public ISelectModel getFormativeOrgUnitListModel() { return this.formativeOrgUnitListModel; }
    public void setFormativeOrgUnitListModel(final ISelectModel formativeOrgUnitListModel) { this.formativeOrgUnitListModel = formativeOrgUnitListModel; }

    private ISelectModel groupSelectModel;
    public ISelectModel getGroupSelectModel() { return this.groupSelectModel; }
    public void setGroupSelectModel(final ISelectModel groupSelectModel) { this.groupSelectModel = groupSelectModel; }
}
