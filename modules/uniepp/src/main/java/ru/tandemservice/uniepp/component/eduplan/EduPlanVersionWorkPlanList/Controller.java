package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionWorkPlanList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanGen;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        model.setSettings(UniBaseUtils.getDataSettings(component, "eduPlanVersionWorkPlanList"));

        if (null == model.getDataSource()) {
            final DynamicListDataSource<EppWorkPlan> dataSource = new DynamicListDataSource<>(component, component1 -> {
                Controller.this.getDao().prepareDataSource(model);
            });

            dataSource.addColumn(UniEppUtils.getStateColumn());
            dataSource.addColumn(new SimpleColumn("Год", EppWorkPlanGen.year().educationYear().title().s()).setClickable(false).setWidth(1));
            dataSource.addColumn(new SimpleColumn("Название", EppWorkPlanGen.title().s()).setClickable(true));
            dataSource.addColumn(new SimpleColumn("Направление (специальность, профессия)", EppWorkPlanGen.parent().eduPlanVersion().eduPlan().educationElementSimpleTitle()).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Направленность", EppWorkPlanGen.parent().title()).setClickable(false).setOrderable(false));
            model.setDataSource(dataSource);
        }
    }


}
