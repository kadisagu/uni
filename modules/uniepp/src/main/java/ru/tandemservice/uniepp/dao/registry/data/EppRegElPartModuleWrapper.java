package ru.tandemservice.uniepp.dao.registry.data;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.EppModuleStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;

import java.util.Collection;
import java.util.Collections;

/**
 * @author vdanilov
 */
public class EppRegElPartModuleWrapper extends EppBaseRegElWrapper<EppRegistryElementPartModule> implements IEppRegElPartModuleWrapper
{
    public EppRegElPartModuleWrapper(final EppRegistryElementPartModule item) { super(item); }

    @Override protected Collection<? extends EppBaseRegElWrapper> getChildren() { return Collections.emptyList(); }

    @Override protected long resolveLocalLoad(String fullCode) {

        if (EppELoadType.FULL_CODE_AUDIT.equals(fullCode)) {
            // полная аудиторная по части - есть сумма по видам аудиторной (по части)
            long totalAudit = 0;
            for (String fullACode: EppALoadType.FULL_CODES) {
                long load = this.load(fullACode);
                totalAudit += Math.max(0, load);
            }
            return totalAudit;
        }
        else if (EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL.equals(fullCode))
        {
            // самостоятельная без контроля есть разность самостоятельной (в базе) и контроля
            return Math.max(0, this.load(EppELoadType.FULL_CODE_SELFWORK) - this.load(EppLoadType.FULL_CODE_CONTROL));
        }

        return super.resolveLocalLoad(fullCode);
    }

    @Override
    public int getNumber()
    {
        return getItem().getNumber();
    }

    @Override
    public boolean shared()
    {
        return this.getItem().getModule().isShared();
    }

    @Override
    public EppModuleStructure getModuleParent()
    {
        return getItem().getModule().getParent();
    }

    @Override
    public OrgUnit getModuleOwner()
    {
        return getItem().getModule().getOwner();
    }
}
