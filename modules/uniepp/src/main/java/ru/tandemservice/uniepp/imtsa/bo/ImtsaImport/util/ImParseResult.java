/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util;

import com.google.common.collect.Lists;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.dto.ImEpvRowDTO;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Результат разбора плана ИМЦА. Вложенные компоненты имеюют схожую с uni структуру.
 *
 * @author azhebko
 * @since 31.10.2014
 */
public class ImParseResult
{
    private Map<String, String> _cycleAbbr2CycleTitleMap;
    private Collection<ImEpvRowDTO> _rootRows;
    private String _programFormCode; // lower case
    private List<Integer> _courses;
    private Integer _coursePartsNumber;
    private String _planStructureCode;
    private PairKey<Integer, Integer> _eduProgramDuration; // year, month
    private Integer _lastCourse;
    private List<Integer> _lastCourseSessionNumbers = Lists.newArrayList();

    public Map<String, String> getCycleAbbr2CycleTitleMap()
    {
        return _cycleAbbr2CycleTitleMap;
    }

    public void setCycleAbbr2CycleTitleMap(Map<String, String> cycleAbbr2CycleTitleMap)
    {
        _cycleAbbr2CycleTitleMap = cycleAbbr2CycleTitleMap;
    }

    public Collection<ImEpvRowDTO> getRootRows()
    {
        return _rootRows;
    }

    public void setRootRows(Collection<ImEpvRowDTO> rootRows)
    {
        _rootRows = rootRows;
    }

    public String getProgramFormCode()
    {
        return _programFormCode;
    }

    public void setProgramFormCode(String programFormCode)
    {
        _programFormCode = programFormCode;
    }

    public List<Integer> getCourses()
    {
        return _courses;
    }

    public void setCourses(List<Integer> courses)
    {
        _courses = courses;
    }

    public Integer getCoursePartsNumber()
    {
        return _coursePartsNumber;
    }

    public void setCoursePartsNumber(Integer coursePartsNumber)
    {
        _coursePartsNumber = coursePartsNumber;
    }

    public Integer getLastCourse()
    {
        return _lastCourse;
    }

    public void setLastCourse(Integer lastCourse)
    {
        _lastCourse = lastCourse;
    }

    public List<Integer> getLastCourseSessionNumbers()
    {
        return _lastCourseSessionNumbers;
    }

    public void setLastCourseSessionNumbers(List<Integer> lastCourseSessionNumbers)
    {
        _lastCourseSessionNumbers = lastCourseSessionNumbers;
    }

    public PairKey<Integer, Integer> getEduProgramDuration()
    {
        return _eduProgramDuration;
    }

    public void setEduProgramDuration(PairKey<Integer, Integer> eduProgramDuration)
    {
        _eduProgramDuration = eduProgramDuration;
    }

    public String getPlanStructureCode()
    {
        return _planStructureCode;
    }

    public void setPlanStructureCode(String planStructureCode)
    {
        _planStructureCode = planStructureCode;
    }
}