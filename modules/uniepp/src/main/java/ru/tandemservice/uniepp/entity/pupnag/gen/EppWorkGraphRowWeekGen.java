package ru.tandemservice.uniepp.entity.pupnag.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ГУП (Неделя строки курса)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkGraphRowWeekGen extends EntityBase
 implements INaturalIdentifiable<EppWorkGraphRowWeekGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek";
    public static final String ENTITY_NAME = "eppWorkGraphRowWeek";
    public static final int VERSION_HASH = -1064542783;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW = "row";
    public static final String P_WEEK = "week";
    public static final String L_TYPE = "type";
    public static final String L_TERM = "term";

    private EppWorkGraphRow _row;     // Строка ГУП
    private int _week;     // Номер недели
    private EppWeekType _type;     // Тип недели
    private Term _term;     // Семестр

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка ГУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkGraphRow getRow()
    {
        return _row;
    }

    /**
     * @param row Строка ГУП. Свойство не может быть null.
     */
    public void setRow(EppWorkGraphRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Номер недели. Свойство не может быть null.
     */
    @NotNull
    public int getWeek()
    {
        return _week;
    }

    /**
     * @param week Номер недели. Свойство не может быть null.
     */
    public void setWeek(int week)
    {
        dirty(_week, week);
        _week = week;
    }

    /**
     * @return Тип недели. Свойство не может быть null.
     */
    @NotNull
    public EppWeekType getType()
    {
        return _type;
    }

    /**
     * @param type Тип недели. Свойство не может быть null.
     */
    public void setType(EppWeekType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Семестр. Свойство не может быть null.
     */
    @NotNull
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр. Свойство не может быть null.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppWorkGraphRowWeekGen)
        {
            if (withNaturalIdProperties)
            {
                setRow(((EppWorkGraphRowWeek)another).getRow());
                setWeek(((EppWorkGraphRowWeek)another).getWeek());
            }
            setType(((EppWorkGraphRowWeek)another).getType());
            setTerm(((EppWorkGraphRowWeek)another).getTerm());
        }
    }

    public INaturalId<EppWorkGraphRowWeekGen> getNaturalId()
    {
        return new NaturalId(getRow(), getWeek());
    }

    public static class NaturalId extends NaturalIdBase<EppWorkGraphRowWeekGen>
    {
        private static final String PROXY_NAME = "EppWorkGraphRowWeekNaturalProxy";

        private Long _row;
        private int _week;

        public NaturalId()
        {}

        public NaturalId(EppWorkGraphRow row, int week)
        {
            _row = ((IEntity) row).getId();
            _week = week;
        }

        public Long getRow()
        {
            return _row;
        }

        public void setRow(Long row)
        {
            _row = row;
        }

        public int getWeek()
        {
            return _week;
        }

        public void setWeek(int week)
        {
            _week = week;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppWorkGraphRowWeekGen.NaturalId) ) return false;

            EppWorkGraphRowWeekGen.NaturalId that = (NaturalId) o;

            if( !equals(getRow(), that.getRow()) ) return false;
            if( !equals(getWeek(), that.getWeek()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRow());
            result = hashCode(result, getWeek());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRow());
            sb.append("/");
            sb.append(getWeek());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkGraphRowWeekGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkGraphRowWeek.class;
        }

        public T newInstance()
        {
            return (T) new EppWorkGraphRowWeek();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "row":
                    return obj.getRow();
                case "week":
                    return obj.getWeek();
                case "type":
                    return obj.getType();
                case "term":
                    return obj.getTerm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "row":
                    obj.setRow((EppWorkGraphRow) value);
                    return;
                case "week":
                    obj.setWeek((Integer) value);
                    return;
                case "type":
                    obj.setType((EppWeekType) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "row":
                        return true;
                case "week":
                        return true;
                case "type":
                        return true;
                case "term":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "row":
                    return true;
                case "week":
                    return true;
                case "type":
                    return true;
                case "term":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "row":
                    return EppWorkGraphRow.class;
                case "week":
                    return Integer.class;
                case "type":
                    return EppWeekType.class;
                case "term":
                    return Term.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkGraphRowWeek> _dslPath = new Path<EppWorkGraphRowWeek>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkGraphRowWeek");
    }
            

    /**
     * @return Строка ГУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek#getRow()
     */
    public static EppWorkGraphRow.Path<EppWorkGraphRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Номер недели. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek#getWeek()
     */
    public static PropertyPath<Integer> week()
    {
        return _dslPath.week();
    }

    /**
     * @return Тип недели. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek#getType()
     */
    public static EppWeekType.Path<EppWeekType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    public static class Path<E extends EppWorkGraphRowWeek> extends EntityPath<E>
    {
        private EppWorkGraphRow.Path<EppWorkGraphRow> _row;
        private PropertyPath<Integer> _week;
        private EppWeekType.Path<EppWeekType> _type;
        private Term.Path<Term> _term;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка ГУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek#getRow()
     */
        public EppWorkGraphRow.Path<EppWorkGraphRow> row()
        {
            if(_row == null )
                _row = new EppWorkGraphRow.Path<EppWorkGraphRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Номер недели. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek#getWeek()
     */
        public PropertyPath<Integer> week()
        {
            if(_week == null )
                _week = new PropertyPath<Integer>(EppWorkGraphRowWeekGen.P_WEEK, this);
            return _week;
        }

    /**
     * @return Тип недели. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek#getType()
     */
        public EppWeekType.Path<EppWeekType> type()
        {
            if(_type == null )
                _type = new EppWeekType.Path<EppWeekType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

        public Class getEntityClass()
        {
            return EppWorkGraphRowWeek.class;
        }

        public String getEntityName()
        {
            return "eppWorkGraphRowWeek";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
