package ru.tandemservice.uniepp.dao.workplan;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap.Callback;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

import java.util.*;

public interface IEppWorkPlanDAO
{
    SpringBeanCache<IEppWorkPlanDAO> instance = new SpringBeanCache<>(IEppWorkPlanDAO.class.getName());

    /**
     * @param eppWorkPlanBase УП
     * @return список объектов, на которых надо проверять локальные права для учебного плана
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Collection<IEntity> getSecLocalEntities(EppWorkPlanBase eppWorkPlanBase);

    /**
     * @return правило генерации номеров РУП
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    INumberGenerationRule<EppWorkPlan> getWorkPlanNumberGenerationRule();

    /**
     * @return правило генерации номеров версий РУП
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    INumberGenerationRule<EppWorkPlanVersion> getWorkPlanVersionNumberGenerationRule();

    /**
     * Сюда должен передаваться итоговый набор РУП которые должны бать у студента,
     * других кроме переданных в метод РУП у студента не будет
     * @param studentEpv2workPlanSet s2epv.id -> { planTermSet }, где student.id - идентификаторы студентов,
     *                               которым изменяют РУП, planTermSet - набор из РУП, выбранных пользователем
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateStudentEpvWorkPlan(final Map<Long, Set<EppWorkPlanBase>> studentEpv2workPlanSet);

    /**
     * возвращает список частей РУП (в порядке следования)
     * @param workPlanId id РУПа
     * @return { part.number -> part }
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<Integer, EppWorkPlanPart> getWorkPlanPartMap(final Long workPlanId);

    /**
     * @param workPlanId  - идентификатор РУП
     * @param planPartSet - набор частей РУП
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateWorkPlanPartSet(Long workPlanId, Collection<EppWorkPlanPart> planPartSet);

    /**
     * Проверяет, что в РУП нет записей
     * @param workPlanId идентификатор РУП
     * @return { row.id }.isEmpty()
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    boolean isWorkPlanEmpty(Long workPlanId);

    /**
     * Генерирует строки РУП на основе РУП прошлых лет или на основе УП(в)
     * @param eppWorkPlanId РУП, в котором надо создать строки
     * @param lookupLastYear искать РУП прошлых лет перед тем, как брать данные из УП(в)
     * @return true, если РУП был изменен
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    boolean doGenerateWorkPlanRows(Long eppWorkPlanId, boolean lookupLastYear);

    /**
     * Генерирует строки РУП на основе РУП прошлых лет или на основе УП(в)
     * @param eppWorkPlanId - РУП, в котором надо создать строки
     * @param sourceEduplanVersionBlockId - id блока, откуда брать данные
     * @param sourceTermResolver { epvRow.id -> IWorkPlanRowDataSource } - семестр и другие данные, на базе которых генерировать данные по дисциплине
     * @return true, если РУП был изменен
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    boolean doGenerateWorkPlanRowsFromEpvBlock(Long eppWorkPlanId, Long sourceEduplanVersionBlockId, Callback<Long, WorkPlanRowSource> sourceTermResolver);

    /**
     * Генерирует строки РУП на основе строк иругого РУП
     * @param workPlanId - РУП, в котором надо создать строки
     * @param sourceWpId - РУП, на базе которого будут созданы строки в РУП
     * @return true, если РУП был изменен
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    boolean doGenerateWorkPlanRowsFromWorkplan(Long workPlanId, Long sourceWpId);

    /**
     * ПОКА ТАК, копирует строки РУП в переданную версию РУП
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    void doGenerateWorkPlanVersionRows(EppWorkPlanVersion version);

    /**
     *  Возвращает мап s2epv.id -> { planTermSet }, где s2epv.id - идентификаторы связи студента с УП(в)
     *  @param studentEpvIds список идентификаторов связей студента с УП(в) eppStudent2EduPlanVersion
     *  @param filterRelations фильтровать (показывать только актуальные)
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<Long, Set<EppStudent2WorkPlan>> getStudentEpvWorkPlanMap(Collection<Long> studentEpvIds, boolean filterRelations);

    /**
     * @param eppRegistryStructure элемент структуры реестра (null - для всех)
     * @return используемые контрольные мероприятия
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    List<EppControlActionType> getActiveControlActionTypes(EppRegistryStructure eppRegistryStructure);


    /**
     * @return недели из ГУПа, соответсвующие РУП (или версии)
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    List<EppWorkGraphRowWeek> getWorkGraphWeeks4Plan(Long workplanId);

    /**
     * @return недели из ГУПа (в каждой части), соответсвующие РУП (или версии)
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<Integer, List<EppWorkGraphRowWeek>> getWorkGraphWeeks4PlanParts(Long workplanId);

//    /**
//     * объединяет РУП(в)
//     */
//    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
//    void doMergeWorkPlanVersions(Collection<Long> workPlanVersionsIds);

    /**
     * удаляет временные версии РУП, объединяет все одинаковые временные версии РУП
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doCleanUpWorkPlanTemporaryVersions(Long workplanId);

    /**
     * Создание части года (в учебном году) для учебного процесса на основе данных РУПа.
     * Эти части года используются для создания МСРП, настроек БРС и прочего.
     * Следует вызывать при создании РУП.
     *
     * @param wp рабочий учебный план
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doCreateEppYearPartToWP(EppWorkPlan wp);
}
