package ru.tandemservice.uniepp.component.settings.EppControlActionTypeGrade;

import java.util.ArrayList;
import java.util.List;

import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;

/**
 * Created by IntelliJ IDEA.
 * User: Tandem
 * Date: 02.03.11
 * Time: 11:45
 * To change this template use File | Settings | File Templates.
 */
public class Model {
    private List<EppGradeScale> _gradeScaleList = new ArrayList<EppGradeScale>();
    private EppGradeScale _gradeScale;
    private List<EppFControlActionType> _controlActionTypeList = new ArrayList<EppFControlActionType>();
    private EppFControlActionType _controlActionType;

    public boolean isWithDescription()
    {
        for (final EppGradeScale gradeScale : this.getGradeScaleList()) {
            if (gradeScale.getDescription() != null) {
                return true;
            }
        }
        return false;
    }

    public boolean isItemWithDescription()
    {
        return this.getGradeScale().getDescription() != null;
    }

    public List<EppGradeScale> getGradeScaleList() {
        return this._gradeScaleList;
    }

    public void setGradeScaleList(final List<EppGradeScale> gradeScaleList) {
        this._gradeScaleList = gradeScaleList;
    }

    public EppGradeScale getGradeScale() {
        return this._gradeScale;
    }

    public void setGradeScale(final EppGradeScale gradeScale) {
        this._gradeScale = gradeScale;
    }

    public List<EppFControlActionType> getControlActionTypeList() {
        return this._controlActionTypeList;
    }

    public void setControlActionTypeList(final List<EppFControlActionType> controlActionTypeList) {
        this._controlActionTypeList = controlActionTypeList;
    }

    public EppFControlActionType getControlActionType() {
        return this._controlActionType;
    }

    public void setControlActionType(final EppFControlActionType controlActionType) {
        this._controlActionType = controlActionType;
    }
}
