package ru.tandemservice.uniepp.component.settings.WeekType2LoadTypeAddEdit;

import java.util.Collections;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;

/**
 * 
 * @author nkokorina
 *
 */

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "developFormId") })
public class Model
{
    private Long developFormId;

    private DevelopForm developForm;

    private ISelectModel developFormSelectModel;
    private ISelectModel weekTypeSelectModel;

    private List<EppWeekType> weekTypeAuditList = Collections.emptyList();
    private List<EppWeekType> weekTypeSelfWorkList = Collections.emptyList();

    public Long getDevelopFormId()
    {
        return this.developFormId;
    }

    public void setDevelopFormId(final Long developFormId)
    {
        this.developFormId = developFormId;
    }

    public DevelopForm getDevelopForm()
    {
        return this.developForm;
    }

    public void setDevelopForm(final DevelopForm developForm)
    {
        this.developForm = developForm;
    }

    public ISelectModel getWeekTypeSelectModel()
    {
        return this.weekTypeSelectModel;
    }

    public void setWeekTypeSelectModel(final ISelectModel weekTypeSelectModel)
    {
        this.weekTypeSelectModel = weekTypeSelectModel;
    }

    public List<EppWeekType> getWeekTypeAuditList()
    {
        return this.weekTypeAuditList;
    }

    public ISelectModel getDevelopFormSelectModel()
    {
        return this.developFormSelectModel;
    }

    public void setDevelopFormSelectModel(final ISelectModel developFormSelectModel)
    {
        this.developFormSelectModel = developFormSelectModel;
    }

    public void setWeekTypeAuditList(final List<EppWeekType> weekTypeAuditList)
    {
        this.weekTypeAuditList = weekTypeAuditList;
    }

    public List<EppWeekType> getWeekTypeSelfWorkList()
    {
        return this.weekTypeSelfWorkList;
    }

    public void setWeekTypeSelfWorkList(final List<EppWeekType> weekTypeSelfWorkList)
    {
        this.weekTypeSelfWorkList = weekTypeSelfWorkList;
    }

}
