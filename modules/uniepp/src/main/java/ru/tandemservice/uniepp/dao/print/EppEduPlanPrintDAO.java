package ru.tandemservice.uniepp.dao.print;

import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.PageOrientation;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.util.jxl.ColourMap;
import ru.tandemservice.uni.util.jxl.ListDataSourcePrinter;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvTotalRow;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvHierarchyRow;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionBlockGen;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionWeekTypeGen;
import ru.tandemservice.uniepp.ui.EduPlanVersionBlockDataSourceGenerator;
import ru.tandemservice.uniepp.util.EppDevelopCombinationTitleUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class EppEduPlanPrintDAO extends UniBaseDao implements IEppEduPlanPrintDAO {

    protected int addTextLineCell(final WritableSheet sheet, final int top, final int left, int width, final String text) {
        width = Math.max(1, width);
        try {
            final Label cell = new Label(left, top, text);
            sheet.addCell(cell);
            sheet.mergeCells(cell.getColumn(), cell.getRow(), cell.getColumn()+(width-1), cell.getRow());
            return (left+width);
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    protected WritableSheet createWritableSheet(final WritableWorkbook book, final String title, final int index) {
        final WritableSheet sheet = book.createSheet(title, index);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);
        sheet.getSettings().setScaleFactor(60);
        sheet.getSettings().setBottomMargin(.3937);
        sheet.getSettings().setTopMargin(.3937);
        sheet.getSettings().setLeftMargin(.3937);
        sheet.getSettings().setRightMargin(.3937);

        for (int i=0;i<256;i++) {
            sheet.setColumnView(i, 2);
        }
        return sheet;
    }

    @SuppressWarnings("unchecked")
    protected ListDataSourcePrinter buildListDataSourcePrinter(final WritableSheet sheet, final AbstractListDataSource source) {
        return new EppListDataSourcePrinter(sheet, source) {

            @Override protected WritableCellFormat getBodyCellFormat(final AbstractColumn column, final IEntity e, final String bodyCellContent) {
                final WritableCellFormat calculateBodyCellFormat = this.calculateBodyCellFormat(column, e, bodyCellContent);
                if (e instanceof IEppEpvRowWrapper) {
                    final boolean s = (((IEppEpvRowWrapper)e).getRow() instanceof EppEpvHierarchyRow);
                    return (s ? this.bold(calculateBodyCellFormat) : calculateBodyCellFormat);
                }
                return calculateBodyCellFormat;
            }

            private WritableCellFormat calculateBodyCellFormat(final AbstractColumn column, final IEntity e, final String bodyCellContent) {
                final String name = column.getName();

                if (name.startsWith("load.")) { return this.STYLE_TABLE_BODY_NUMBER; }
                if (name.startsWith("xload.")) { return this.STYLE_TABLE_BODY_NUMBER; }
                if (name.startsWith("term.")) { return this.STYLE_TABLE_BODY_NUMBER; }
                if ("title".equals(name)) { return this.STYLE_TABLE_BODY_TITLE; }
                if ("owner".equals(name)) { return this.STYLE_TABLE_BODY_TITLE; }

                return super.getBodyCellFormat(column, e, bodyCellContent);
            }

            @Override protected int getColumnWidth(final AbstractColumn column) {
                final String name = column.getName();

                if (name.startsWith("action.")) { return 2; }
                if (name.startsWith("load.")) { return 3; }
                if (name.startsWith("xload.")) { return 3; }
                if (name.startsWith("term.")) { return 2; }

                if ("index".equals(name)) { return 7; }
                if ("title".equals(name)) { return 30; }
                if ("owner".equals(name)) { return 25; }
                if (StringUtils.contains(name, "registryNumber")) { return 6; }

                return 2;
            }
        };
    }

    @SuppressWarnings("unchecked")
    protected ListDataSourcePrinter buildGraphDataSourcePrinter(final WritableSheet sheet, final AbstractListDataSource source, final Map<String, Colour> graphColourMap) {
        return new EppListDataSourcePrinter(sheet, source) {

            protected final WritableCellFormat STYLE_ODD = this.buildStyle(true);
            protected final WritableCellFormat STYLE_EVEN = this.buildStyle(false);
            protected WritableCellFormat buildStyle(final boolean odd) {
                try {
                    final WritableCellFormat format = this.buildStyleTableNorm();
                    format.setAlignment(odd ? Alignment.LEFT : Alignment.RIGHT);
                    format.setVerticalAlignment(VerticalAlignment.TOP);
                    return format;
                } catch (final Throwable t) {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
            }

            @Override
            protected Label buildLabel(final int top, final int left, final AbstractColumn column, final IEntity e) {
                if (column.getName().startsWith("week.")) {
                    final String bodyCellContent = StringUtils.trimToNull(this.getBodyCellContent(column, e));
                    if (null == bodyCellContent) {
                        // если ничего нет - то рисуем стандартным стилем
                        return super.buildLabel(top, left, "", this.STYLE_TABLE_BODY);
                    }

                    final String term = StringUtils.substringBefore(bodyCellContent, ".");
                    final boolean odd = (1 == (Integer.parseInt(term) % 2));

                    final String abbreviation = StringUtils.trimToNull(bodyCellContent.substring(1+term.length()));
                    try {
                        final WritableCellFormat color = new WritableCellFormat(odd ? this.STYLE_ODD : this.STYLE_EVEN);
                        final Colour c = graphColourMap.get(abbreviation);
                        if (null != c) { color.setBackground(c); }

                        return super.buildLabel(top, left, abbreviation, color);
                    } catch (final Throwable t) {
                        throw CoreExceptionUtils.getRuntimeException(t);
                    }
                }
                return super.buildLabel(top, left, column, e);
            }

            @Override
            protected WritableCellFormat getBodyCellFormat(final AbstractColumn column, final IEntity e, final String bodyCellContent) {
                final String name = column.getName();
                if (name.startsWith("times.")) { return this.STYLE_TABLE_BODY_NUMBER; }

                return super.getBodyCellFormat(column, e, bodyCellContent);
            }

            @Override protected int getColumnWidth(final AbstractColumn column) {
                final String name = column.getName();
                if (name.startsWith("week.")) { return 1; }
                if ("intValue".equals(name)) { return 5; }
                return 2;
            }
        };
    }


    @Override
    public void printRegistryEduPlanVersion(final WritableWorkbook book, final Collection<Long> epvIds, final Map<String, Object> context) {
        final List<EppWeek> eppWeeks = this.getCatalogItemListOrderByCode(EppWeek.class);
        final List<EppWeekType> eppWeekTypes = this.getCatalogItemListOrderByCode(EppWeekType.class);

        final Map<String, Colour> graphColourMap = new HashMap<>();
        {
            final Map<Long, Colour> map = ColourMap.getColourMap(book);
            for (final EppWeekType wt: eppWeekTypes) {
                final String abbreviation = StringUtils.trimToNull(wt.getAbbreviationView());
                final Long c = wt.getColor();
                if ((null != c) && (null != abbreviation)) {
                    if (null != graphColourMap.put(abbreviation, map.get(c))) {
                        this.logger.warn("WeekType.abbreviation: `"+abbreviation+"` is not unique");
                    }
                }
            }
        }

        List<Long> epvBlockIds = new DQLSelectBuilder()
        .fromEntity(EppEduPlanVersionBlock.class, "b")
        .column(property("b.id"))
        .where(in(property("b", EppEduPlanVersionBlock.eduPlanVersion().id()), epvIds))
        .order(property("b", EppEduPlanVersionBlock.eduPlanVersion().eduPlan().number()))
        .order(property("b", EppEduPlanVersionBlock.eduPlanVersion().number()))
        .order(property("b", EppEduPlanVersionBlock.id()))
        .createStatement(getSession()).list();

        final Map<Long, IEppEpvBlockWrapper> epvBlockDataMap = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(epvBlockIds, true);
        final List<IEppEpvBlockWrapper> wrappers = new ArrayList<>();
        wrappers.addAll(epvBlockDataMap.values());
        Collections.sort(wrappers);
        for (final IEppEpvBlockWrapper wrapper: wrappers) {
            final WritableSheet sheet = this.createWritableSheet(book, "№" + wrapper.getVersion().getFullNumber() + " (" + wrapper.getBlock().getEducationElementSimpleTitle() + ")", 0);
            {
                int top = 0;

                {
                    this.addTextLineCell(sheet, top, 0, 15, "Учебный план: ");
                    this.addTextLineCell(sheet, top, 20, 60, wrapper.getVersion().getFullTitle());
                    top++;
                }

                {
                    this.addTextLineCell(sheet, top, 0, 15, "Направление подготовки: ");
                    this.addTextLineCell(sheet, top, 20, 60, wrapper.getVersion().getEduPlan().getEducationElementSimpleTitle());
                    top++;
                }

                {
                    this.addTextLineCell(sheet, top, 0, 15, "Блок: ");
                    this.addTextLineCell(sheet, top, 20, 60, wrapper.getBlock().getTitle());
                    top++;
                }

                {
                    this.addTextLineCell(sheet, top, 0, 15, "Форма обучения: ");
                    this.addTextLineCell(sheet, top, 20, 60, EppDevelopCombinationTitleUtil.getTitle(wrapper.getVersion().getEduPlan()));
                    top++;
                }

                // данные по УГ
                final boolean printWeekDistribution = true;
                if (printWeekDistribution) {
                    top++;

                    final Set<String> usedTypeCodes = new HashSet<>(eppWeeks.size());
                    final Map<Course, Map<Integer, EppEduPlanVersionWeekType>> weekDistribution = new TreeMap<>(Course.COURSE_COMPARATOR);
                    for (final EppEduPlanVersionWeekType epvWt: this.getList(EppEduPlanVersionWeekType.class,  EppEduPlanVersionWeekTypeGen.eduPlanVersion().id().toString(), wrapper.getId())) {
                        final Map<Integer, EppEduPlanVersionWeekType> map = SafeMap.safeGet(weekDistribution, epvWt.getCourse(), HashMap.class);
                        map.put(epvWt.getWeek().getNumber(), epvWt);
                        usedTypeCodes.add(epvWt.getWeekType().getCode());
                    }

                    final StaticListDataSource<Course> source = new StaticListDataSource<>(weekDistribution.keySet());
                    source.addColumn(new SimpleColumn("Курс", "intValue"));
                    final HeadColumn weeks = new HeadColumn("weeks", "Распределение учебных недель");

                    for (final EppWeek week: eppWeeks) {
                        weeks.addColumn(new SimpleColumn(week.getTitle(), "week."+week.getNumber()) {
                            @Override public String getContent(final IEntity entity) {
                                final EppEduPlanVersionWeekType epvWt = weekDistribution.get(entity).get(week.getNumber());
                                return (null == epvWt ? "" : (epvWt.getTerm().getIntValue()+"."+epvWt.getWeekType().getAbbreviationView()));
                            }
                        }.setVerticalHeader(true));
                    }
                    source.addColumn(weeks);

                    final HeadColumn times = new HeadColumn("times", "Данные по бюджету времени");
                    for (final EppWeekType wt: eppWeekTypes) {
                        if ((!EppWeekType.CODE_NOT_USED.equals(wt.getCode())) && usedTypeCodes.contains(wt.getCode())) {
                            final String abbreviation = StringUtils.trimToNull(wt.getAbbreviationView());
                            final String caption = wt.getShortTitle() + (null == abbreviation ? "" : " ("+abbreviation+")");
                            times.addColumn(new SimpleColumn(caption, "times."+wt.getCode()) {
                                @Override public String getContent(final IEntity entity) {
                                    final Map<Integer, EppEduPlanVersionWeekType> map = weekDistribution.get(entity);
                                    int i = 0;
                                    for (final EppEduPlanVersionWeekType t: map.values()) {
                                        if (wt.equals(t.getWeekType())) { i++; }
                                    }
                                    return (i > 0 ? String.valueOf(i) : "");
                                }
                            }.setVerticalHeader(true));
                        }
                    }
                    source.addColumn(times);

                    {
                        this.addTextLineCell(sheet, top, 0, 60, "Учебный график");
                        top++;
                    }
                    top = this.buildGraphDataSourcePrinter(sheet, source, graphColourMap).print(top, 0);
                    top++;
                }



                // берем по умолчанию данные из УП
                final boolean extendedView = wrapper.getVersion().getViewTableDiscipline().isExtend();

                final MQBuilder builder = new MQBuilder(EppEduPlanVersionBlockGen.ENTITY_CLASS, "block");
                builder.add(MQExpression.eq("block", EppEduPlanVersionBlockGen.eduPlanVersion().id().s(), wrapper.getId()));

                final List<EppEduPlanVersionBlock> databaseBlockList = builder.getResultList(this.getSession());
                Collections.sort(databaseBlockList);

                final EduPlanVersionBlockDataSourceGenerator generator = new EduPlanVersionBlockDataSourceGenerator() {
                    @Override public boolean isCheckMode() { return false; }
                    @Override public boolean isExtendedView() { return extendedView; }
                    @Override protected boolean isEditable() { return false; }
                    @Override protected String getPermissionKeyEdit() { return null; }
                    @Override protected Long getVersionBlockId() { return wrapper.getBlock().getId(); }
                    @Override public void doPrepareBlockDisciplineDataSource(final StaticListDataSource<IEppEpvRowWrapper> dataSource, final IBlockRenderContext context) {
                        super.doPrepareBlockDisciplineDataSource(dataSource, context);
                        dataSource.addColumn(wrap(this.getColumn_owner(true)));
                    }
                    @Override public void doPrepareBlockActionsDataSource(final StaticListDataSource<IEppEpvRowWrapper> dataSource, final IBlockRenderContext context) {
                        super.doPrepareBlockActionsDataSource(dataSource, context);
                        dataSource.addColumn(wrap(this.getColumn_owner(true)));
                    }
                };

                for (final EppEduPlanVersionBlock block: databaseBlockList)
                {
                    final EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext renderContext = new EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext() {
                        @Override public IEntityHandler getAdditionalEditDisabler() { return null; /* no edit actions */ }
                        @Override public Collection<IEppEpvRowWrapper> getBlockRows(final IEppEpvBlockWrapper versionWrapper) {
                            return EppEduPlanVersionDataDAO.filterEduPlanBlockRows(versionWrapper.getRowMap(), object -> block.getId().equals(object.getOwner().getId()) && IEppEpvRowWrapper.DISCIPLINES_ROWS.evaluate(object), true);
                        }
                    };

                    top += 1;

                    {
                        this.addTextLineCell(sheet, top, 0, 60, block.getEducationElementSimpleTitle());
                        top++;
                    }

                    final StaticListDataSource<IEppEpvRowWrapper> source = new StaticListDataSource<>();
                    generator.doPrepareBlockDisciplineDataSource(source, renderContext);
                    top = this.buildListDataSourcePrinter(sheet, source).print(top, 0);
                }

                {
                    final EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext renderContext = new EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext() {
                        @Override public IEntityHandler getAdditionalEditDisabler() { return null; /* no edit actions */ }
                        @Override public Collection<IEppEpvRowWrapper> getBlockRows(final IEppEpvBlockWrapper versionWrapper) {
                            return EppEduPlanVersionDataDAO.filterEduPlanBlockRows(versionWrapper.getRowMap(), IEppEpvRowWrapper.ACTIONS_ROWS, true);
                        }
                    };

                    top += 1;
                    final StaticListDataSource<IEppEpvRowWrapper> source = new StaticListDataSource<>();
                    generator.doPrepareBlockActionsDataSource(source, renderContext);
                    top = this.buildListDataSourcePrinter(sheet, source).print(top, 0);
                }

                {
                    final EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext renderContext = new EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext() {
                        @Override public IEntityHandler getAdditionalEditDisabler() { return null; /* no edit actions */ }
                        @Override public Collection<IEppEpvRowWrapper> getBlockRows(final IEppEpvBlockWrapper versionWrapper) {
                            return versionWrapper.getRowMap().values();
                        }
                    };

                    top += 1;
                    final StaticListDataSource<EppEpvTotalRow> source = new StaticListDataSource<>();
                    generator.doPrepareTotalDataSource(source, renderContext);
                    top = this.buildListDataSourcePrinter(sheet, source).print(top, 0);
                }
            }
        }
    }


}
