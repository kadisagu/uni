package ru.tandemservice.uniepp.component.settings.WeekType2LoadType;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * 
 * @author nkokorina
 *
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        final DynamicListDataSource<DevelopForm> dataSource = UniBaseUtils.createDataSource(component, this.getDao());
        dataSource.addColumn(new SimpleColumn("Форма освоения", DevelopForm.title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Аудиторные занятия", "audit").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Самостоятельная работа", "selfwork").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
        model.setDataSource(dataSource);
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ru.tandemservice.uniepp.component.settings.WeekType2LoadTypeAddEdit.Model.class.getPackage().getName(), new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())));
    }

}
