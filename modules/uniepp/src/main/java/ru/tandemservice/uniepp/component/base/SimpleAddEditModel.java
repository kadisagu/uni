package ru.tandemservice.uniepp.component.base;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="contextId")
})
public abstract class SimpleAddEditModel<T extends IEntity> {

    private Long contextId;
    public Long getContextId() { return this.contextId; }
    public void setContextId(final Long id) { this.contextId = id; }

    private T element;
    public T getElement() { return this.element; }
    public void setElement(final T element) { this.element = element; }

    public IEntity getSecuredObject() { return this.getElement(); }

    public static boolean isEditForm(final IEntity element) {
        return ((null != element) && (null != element.getId()));
    }

    public static boolean isReadOnly(final IEntity element) {
        if (SimpleAddEditModel.isEditForm(element)) {
            final EppState state = ((element instanceof IEppStateObject) ? ((IEppStateObject)element).getState() : null);
            return ((null != state) && (state.isReadOnlyState()));
        }
        return false;
    }

    public boolean isEditForm() { return SimpleAddEditModel.isEditForm(this.getElement()); }
    public boolean isReadOnly() { return SimpleAddEditModel.isReadOnly(this.getElement()); }
}
