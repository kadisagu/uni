/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionScheduleTab;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.RichCancelColumn;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.RichSaveEditColumn;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.gen.CourseGen;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniepp.tapestry.richTableList.RichRangeSelection;
import ru.tandemservice.uniepp.util.EppEduPlanVersionScheduleUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class DAO extends UniBaseDao implements IDAO
{

    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        model.setWeekTypeLegendList(IEppEduPlanDAO.instance.get().getWeekTypeLegendRowList(null));

        final EppEduPlanVersion version = model.getEduplanVersionHolder().refresh(EppEduPlanVersion.class);

        final EppEduPlanVersionScheduleUtils schedule = new EppEduPlanVersionScheduleUtils() {
            @Override protected boolean isTotalCourseRowPresent() { return false; }
            @Override protected EppEduPlanVersion getEduPlanVersion() { return version; }
        };

        model.setScheduleDataSource(schedule.getRangeSelectionListDataSource());
        final RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> rangeModel = model.getScheduleDataSource();

        {
            final AbstractListDataSource<ViewWrapper<Course>> dataSource = rangeModel.getDataSource();

            if (!model.getEduplanVersion().getState().isReadOnlyState())
            {
                dataSource.addColumn(new RichSaveEditColumn(rangeModel, "onClickRichRowSave", "onClickRichRowEdit").setPermissionKey("editSchedule_eppEduPlanVersion"));
                dataSource.addColumn(new RichCancelColumn(rangeModel, "onClickRichRowCancel").setPermissionKey("editSchedule_eppEduPlanVersion"));
            }
            model.setScheduleDataSource(rangeModel);
        }

        {
            model.setTimesDataSource(schedule.getTimesDataSource());
        }
    }

    @Override
    public void prepareEditRow(final Model model, final Long rowId)
    {
        final Session session = this.getSession();
        final Course course = this.getNotNull(Course.class, rowId);
        final RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> scheduleDataSource = model.getScheduleDataSource();

        if (DAO.getRelationList(session, model.getEduplanVersion(), rowId).isEmpty())
        {
            final DevelopGrid developGrid = model.getEduplanVersion().getEduPlan().getDevelopGrid();
            scheduleDataSource.setSelection(new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, EppEduPlanVersionScheduleUtils.getPoints4EmptyGridRow(developGrid, course)));
        }
        else
        {
            // уже есть связи => можно установить границы семестров
            scheduleDataSource.setSelection(new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, scheduleDataSource.getRow2points().get(rowId)));
        }
    }


    @Override
    public void updateScheduleRow(final Long courseId, final Model model)
    {
        final Session session = this.getSession();
        final Course course = this.getNotNull(Course.class, courseId);


        final List<EppWeek> weekList = this.getCatalogItemListOrderByCode(EppWeek.class);
        final Map<PairKey<Long, Long>, EppWeekType> dataMap = model.getScheduleDataSource().getDataMap();

        // U P D A T E

        final Map<Integer, Term> termMap = DevelopGridDAO.getTermMap();
        final List<Integer> terms = EppEduPlanVersionScheduleUtils.getDevelopGridDetailTerms(model.getEduplanVersion().getEduPlan().getDevelopGrid(), course);

        final EppWeekType theory = this.getCatalogItem(EppWeekType.class, EppWeekType.CODE_THEORY);

        // существующие в базе связи для данной строки
        final Map<PairKey<Long, Long>, EppEduPlanVersionWeekType> map = new HashMap<PairKey<Long, Long>, EppEduPlanVersionWeekType>();
        for (final EppEduPlanVersionWeekType item : DAO.getRelationList(session, model.getEduplanVersion(), courseId))
        {
            map.put(PairKey.create(item.getCourse().getId(), item.getWeek().getId()), item);
        }

        final int[] ranges = model.getScheduleDataSource().getSelection().getRanges();
        for (final EppWeek week : weekList)
        {
            final PairKey<Long, Long> key = PairKey.create(courseId, week.getId());
            EppEduPlanVersionWeekType item = map.get(key);

            final int partNumber = ranges[week.getNumber() - 1];
            if (partNumber == -1)
            {
                // неделя не попала ни в один диапазон. надо удалить связь
                if (item != null){
                    session.delete(item);
                }
            }
            else
            {
                final Term term = termMap.get(terms.get(partNumber));
                final EppWeekType weekType = dataMap.get(key);

                if (null == term) {
                    if (null != item) {
                        session.delete(item);
                    }
                } else {
                    if (item == null) {
                        item = new EppEduPlanVersionWeekType(model.getEduplanVersion(), course, week);
                    }

                    // надо обновить
                    item.setTerm(term);
                    item.setWeekType(null == weekType ? theory : weekType);
                    session.saveOrUpdate(item);
                }
            }
        }

    }

    // Component specific API function

    private static List<EppEduPlanVersionWeekType> getRelationList(final Session session, final EppEduPlanVersion eduPlanVersion, final Long courseId)
    {
        final MQBuilder builder = new MQBuilder(EppEduPlanVersionWeekType.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", EppEduPlanVersionWeekType.L_EDU_PLAN_VERSION, eduPlanVersion));
        if (courseId != null)
        {
            builder.add(MQExpression.eq("r", EppEduPlanVersionWeekType.course().id().s(), courseId));
        }
        return builder.getResultList(session);
    }


}
