package ru.tandemservice.uniepp.entity.std.data;

import ru.tandemservice.uniepp.entity.IEppNumberRow;
import ru.tandemservice.uniepp.entity.std.data.gen.EppStdDisciplineNestedRowGen;

/**
 * Вложенная дисциплина версии ГОС
 */
public class EppStdDisciplineNestedRow extends EppStdDisciplineNestedRowGen implements IEppNumberRow
{
    @Override public EppStdDisciplineTopRow getHierarhyParent() { return this.getParent(); }
    @Override public void setHierarhyParent(final EppStdRow parent) { this.setParent((EppStdDisciplineTopRow) parent); }

    @Override public int getComparatorClassIndex() { return 4; }
    @Override public String getComparatorString() { return this.getSelfIndexPart(); }
    @Override public String getSelfIndexPart() { return IEppNumberRow.NUMBER_FORMATTER.format(this); }

}