package ru.tandemservice.uniepp.entity.workplan;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;

import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanVersionGen;

/**
 * Рабочий план (версия)
 */
public class EppWorkPlanVersion extends EppWorkPlanVersionGen implements INumberObject
{
    public EppWorkPlanVersion() {

    }

    public EppWorkPlanVersion(final EppWorkPlan eppWorkPlan) {
        this.setParent(eppWorkPlan);
    }

    @Override public INumberGenerationRule<EppWorkPlanVersion> getNumberGenerationRule() {
        return IEppWorkPlanDAO.instance.get().getWorkPlanVersionNumberGenerationRule();
    }

    @Override public EppWorkPlan getWorkPlan() {
        return this.getParent();
    }

    @Override public IHierarchyItem getHierarhyParent() {
        return this.getWorkPlan();
    }

    @Override public String getFullNumber() {
        return this.getWorkPlan().getNumber() + "." + this.getNumber();
    }

    @Override public Term getTerm() {
        return this.getWorkPlan().getTerm();
    }

    @Override public DevelopGridTerm getGridTerm() {
        return this.getWorkPlan().getGridTerm();
    }

    @Override public EppYearEducationProcess getYear() {
        return this.getWorkPlan().getYear();
    }

    @Override public EppEduPlanVersionBlock getBlock() {
        return this.getWorkPlan().getBlock();
    }

    @EntityDSLSupport(parts={EppWorkPlanVersionGen.L_PARENT+".number", EppWorkPlanVersionGen.P_NUMBER})
    @Override public String getTitle() {
        if (getTerm() == null) {
            return this.getClass().getSimpleName();
        }
        return super.getTitleBase();
    }


}