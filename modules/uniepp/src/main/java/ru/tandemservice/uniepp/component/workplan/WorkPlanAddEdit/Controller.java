package ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;

/**
 * 
 * @author nkokorina
 * @since 16.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }


    public void onClickApply(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().validate(model, UserContext.getInstance().getErrorCollector());
        if (UserContext.getInstance().getErrorCollector().hasErrors()) { return; }

        this.getDao().save(model);
        if (UserContext.getInstance().getErrorCollector().hasErrors()) { return; }

        this.deactivate(component);
    }


    public void onChangeEppYear(final IBusinessComponent component)
    {
        this.getDao().onChangeEppYear(this.getModel(component));
    }
}
