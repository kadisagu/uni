package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Степень сформированности учебной группы"
 * Имя сущности : eppRealEduGroupCompleteLevel
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppRealEduGroupCompleteLevelCodes
{
    /** Константа кода (code) элемента : Читающее подразделение (title) */
    String LEVEL_ACTIVITY_OWNER_ORG_UNIT = "1";
    /** Константа кода (code) элемента : Формируется (title) */
    String LEVEL_ACTIVITY_OWNER_ORG_UNIT_TAKED = "1.1";
    /** Константа кода (code) элемента : Утверждена (title) */
    String LEVEL_ACTIVITY_OWNER_ORG_UNIT_APPROVED = "1.2";
    /** Константа кода (code) элемента : Деканат (title) */
    String LEVEL_GROUP_ORG_UNIT = "2";
    /** Константа кода (code) элемента : Формируется (title) */
    String LEVEL_GROUP_ORG_UNIT_TAKED = "2.1";
    /** Константа кода (code) элемента : Утверждена (title) */
    String LEVEL_GROUP_ORG_UNIT_APPROVED = "2.2";
    /** Константа кода (code) элемента : Диспетчерская (title) */
    String LEVEL_OPERATION_ORG_UNIT = "3";
    /** Константа кода (code) элемента : Формируется (title) */
    String LEVEL_OPERATION_ORG_UNIT_TAKED = "3.1";
    /** Константа кода (code) элемента : Утверждена (title) */
    String LEVEL_OPERATION_ORG_UNIT_APPROVED = "3.2";

    Set<String> CODES = ImmutableSet.of(LEVEL_ACTIVITY_OWNER_ORG_UNIT, LEVEL_ACTIVITY_OWNER_ORG_UNIT_TAKED, LEVEL_ACTIVITY_OWNER_ORG_UNIT_APPROVED, LEVEL_GROUP_ORG_UNIT, LEVEL_GROUP_ORG_UNIT_TAKED, LEVEL_GROUP_ORG_UNIT_APPROVED, LEVEL_OPERATION_ORG_UNIT, LEVEL_OPERATION_ORG_UNIT_TAKED, LEVEL_OPERATION_ORG_UNIT_APPROVED);
}
