package ru.tandemservice.uniepp.settings.bo.PlanStructureQualification.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 14.09.2015
 */

public class PlanStructureQualificationModifyDAO extends UniBaseDao implements IPlanStructureQualificationModifyDAO
{
	@Override
	public void doSwitchPlanStructureQualification(final Long planStructureId, final Long indexId)
	{
		DQLSelectBuilder dql = new DQLSelectBuilder()
			.fromEntity(EppPlanStructure4SubjectIndex.class, "rel")
			.where(and(
				eq(property("rel", EppPlanStructure4SubjectIndex.eppPlanStructure().id()), value(planStructureId)),
				eq(property("rel", EppPlanStructure4SubjectIndex.eduProgramSubjectIndex().id()), value(indexId))
			));
		List<EppPlanStructure4SubjectIndex> singletoneRelationList = IUniBaseDao.instance.get().getList(dql);
		if (singletoneRelationList.isEmpty())
		{
			final EppPlanStructure4SubjectIndex element = new EppPlanStructure4SubjectIndex();
			element.setEppPlanStructure(IUniBaseDao.instance.get().get(planStructureId));
			element.setEduProgramSubjectIndex(IUniBaseDao.instance.get().get(indexId));
			save(element);
		}
		else
			delete(singletoneRelationList.get(0));
	}
}
