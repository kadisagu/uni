/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAddEditBase;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author oleyba
 * @since 11/18/14
 */
public abstract class EppEduPlanVersionTermDistrRowAddEditBaseUI<T extends EppEpvTermDistributedRow> extends EppEduPlanVersionRowAddEditBaseUI<T>
{
    public static final List<String> LOAD_FULL_CODE_LIST = Arrays.asList(
            EppLoadType.FULL_CODE_TOTAL_HOURS,
            EppLoadType.FULL_CODE_LABOR,
            EppLoadType.FULL_CODE_WEEKS,
            EppELoadType.FULL_CODE_AUDIT,
            EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL,
            EppLoadType.FULL_CODE_CONTROL,
            EppLoadType.FULL_CODE_CONTROL_E,
            EppALoadType.FULL_CODE_TOTAL_LECTURES,
            EppALoadType.FULL_CODE_TOTAL_LECTURES_I,
            EppALoadType.FULL_CODE_TOTAL_LECTURES_E,
            EppALoadType.FULL_CODE_TOTAL_PRACTICE,
            EppALoadType.FULL_CODE_TOTAL_PRACTICE_I,
            EppALoadType.FULL_CODE_TOTAL_PRACTICE_E,
            EppALoadType.FULL_CODE_TOTAL_LABS,
            EppALoadType.FULL_CODE_TOTAL_LABS_I,
            EppALoadType.FULL_CODE_TOTAL_LABS_E
    );

    protected Map<String, Double> loadMap = new HashMap<>();
    protected Map<Integer, Map<String, Double>> termLoadMap = new HashMap<>();
    protected Map<Integer, Map<String, Integer>> termActionMap = new HashMap<>();

    protected List<ColumnWrapper> columns = new ArrayList<>();
    protected ColumnWrapper column;

    protected List<LoadTypeWrapper> loadRows = new ArrayList<>();
    protected LoadTypeWrapper loadRow;

    protected List<ControlActionWrapper> caRows = new ArrayList<>();
    protected ControlActionWrapper caRow;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        setColumns(new ArrayList<>());

        if (isEditForm()) {

            IEppEduPlanVersionDataDAO.IRowLoadWrapper rowLoadWrapper = IEppEduPlanVersionDataDAO.instance.get().getRowTermLoad(getRow().getId());

            setLoadMap(rowLoadWrapper.rowLoadMap());

            Map<Integer, Map<String, Double>> tLoadMap = rowLoadWrapper.rowTermLoadMap();
            Map<Integer, Map<String, Integer>> tActionMap = rowLoadWrapper.rowTermActionMap();

            setTermLoadMap(new HashMap<>());
            setTermActionMap(new HashMap<>());

            final List<Integer> rowTerms = new ArrayList<>();
            for (Integer term : DevelopGridDAO.getTermMap().keySet()) {
                Map<String, Integer> aMap = tActionMap.get(term);
                if (aMap != null && !aMap.isEmpty()) { rowTerms.add(term);  continue; }
                Map<String, Double> lMap = tLoadMap.get(term);
                if (lMap != null && !lMap.isEmpty()) { rowTerms.add(term); }
            }

            int pos = 0;
            for (Integer term : rowTerms) {
                getColumns().add(new ColumnWrapper(++pos, term));
                getTermLoadMap().put(pos, tLoadMap.get(term));
                getTermActionMap().put(pos, tActionMap.get(term));
            }


        }
        if (getColumns().isEmpty()) {
            getColumns().add(new ColumnWrapper(1, 1));
        }

        for (ColumnWrapper column : getColumns()) {
            if (!getTermActionMap().containsKey(column.getId().intValue())) getTermActionMap().put(column.getId().intValue(), new HashMap<>());
            if (!getTermLoadMap().containsKey(column.getId().intValue())) getTermLoadMap().put(column.getId().intValue(), new HashMap<>());
        }

        if (getLoadRows().isEmpty()) {
            long id = 0L;
            getLoadRows().add(new LoadTypeWrapper(id++, "Всего часов", EppLoadType.FULL_CODE_TOTAL_HOURS));
            getLoadRows().add(new LoadTypeWrapper(id++, "Всего недель", EppLoadType.FULL_CODE_WEEKS));
            getLoadRows().add(new LoadTypeWrapper(id++, "Трудоемкость", EppLoadType.FULL_CODE_LABOR));
            getLoadRows().add(new LoadTypeWrapper(id++, "Аудиторная", EppELoadType.FULL_CODE_AUDIT));
            final String lev2 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            {
                final String lev3 = lev2 + lev2 + lev2;
                final String levI = lev3 + "в интерактивной форме";
                final String levE = lev3 + "в электронной форме";

                getLoadRows().add(new LoadTypeWrapper(id++, lev2 + "Лекции", EppALoadType.FULL_CODE_TOTAL_LECTURES));
                if (isShowHoursI(EppALoadType.FULL_CODE_TOTAL_LECTURES_I))
                    getLoadRows().add(new LoadTypeWrapper(id++, levI, EppALoadType.FULL_CODE_TOTAL_LECTURES_I));
                if (isShowHoursE(EppALoadType.FULL_CODE_TOTAL_LECTURES_E))
                    getLoadRows().add(new LoadTypeWrapper(id++, levE, EppALoadType.FULL_CODE_TOTAL_LECTURES_E));

                getLoadRows().add(new LoadTypeWrapper(id++, lev2 + "Практические занятия", EppALoadType.FULL_CODE_TOTAL_PRACTICE));
                if (isShowHoursI(EppALoadType.FULL_CODE_TOTAL_PRACTICE_I))
                    getLoadRows().add(new LoadTypeWrapper(id++, levI, EppALoadType.FULL_CODE_TOTAL_PRACTICE_I));
                if (isShowHoursE(EppALoadType.FULL_CODE_TOTAL_PRACTICE_E))
                    getLoadRows().add(new LoadTypeWrapper(id++, levE, EppALoadType.FULL_CODE_TOTAL_PRACTICE_E));

                getLoadRows().add(new LoadTypeWrapper(id++, lev2 + "Лабораторные занятия", EppALoadType.FULL_CODE_TOTAL_LABS));
                if (isShowHoursI(EppALoadType.FULL_CODE_TOTAL_LABS_I))
                    getLoadRows().add(new LoadTypeWrapper(id++, levI, EppALoadType.FULL_CODE_TOTAL_LABS_I));
                if (isShowHoursE(EppALoadType.FULL_CODE_TOTAL_LABS_E))
                    getLoadRows().add(new LoadTypeWrapper(id++, levE, EppALoadType.FULL_CODE_TOTAL_LABS_E));

            }
            getLoadRows().add(new LoadTypeWrapper(id++, "Самостоятельная", EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL));
            getLoadRows().add(new LoadTypeWrapper(id++, "Контроль", EppLoadType.FULL_CODE_CONTROL));
            if (isShowHoursE(EppLoadType.FULL_CODE_CONTROL_E))
                getLoadRows().add(new LoadTypeWrapper(id, lev2 + "в электронной форме", EppLoadType.FULL_CODE_CONTROL_E));
        }

        onChangeRowType();
    }

    private boolean hasLoad(Double value)
    {
        return value != null && value > 0.0;
    }

    protected boolean hasAdvHours(String fullCode)
    {
        if (hasLoad(getLoadMap().get(fullCode)))
            return true;

        for (Map<String, Double> termMap : getTermLoadMap().values())
        {
            if (hasLoad(termMap.get(fullCode)))
                return true;
        }

        return false;
    }

    protected boolean isShowHoursI(String fullCode)
    {
        return getBlock().getEduPlanVersion().isHoursI() || hasAdvHours(fullCode);
    }

    protected boolean isShowHoursE(String fullCode)
    {
        return getBlock().getEduPlanVersion().isHoursE() || hasAdvHours(fullCode);
    }

    protected void fillLoadFromRegistryElement(EppRegistryElement regEl, int multiplier) {
        if (null == regEl) { return; }

        final IEppRegElWrapper regElWrap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(Collections.singleton(regEl.getId())).get(regEl.getId());
        if (null == regElWrap) { return; }

        getRow().setTitle(regEl.getTitle());

        for (String loadFullCode : LOAD_FULL_CODE_LIST) {
            final Double value = omitZero(regElWrap.getLoadAsDouble(loadFullCode));
            getLoadMap().put(loadFullCode, value != null ? value * multiplier : null);
        }

        int maxTerm = 1;
        Map<Integer, Integer> termNumMap = new HashMap<>();
        for (ColumnWrapper c : getColumns()) {
            termNumMap.put(c.getId().intValue(), c.getTermNumber());
            maxTerm = Math.max(maxTerm, c.getTermNumber());
        }

        getColumns().clear();
        for (int partNum = 1; partNum <= regElWrap.getItem().getParts(); partNum++) {
            Integer term = termNumMap.get(partNum);
            if (null == term) term = ++maxTerm;
            getColumns().add(new ColumnWrapper(partNum, term));

            final IEppRegElPartWrapper partWrap = regElWrap.getPartMap().get(partNum);

            Map<String, Double> loadMap = new HashMap<>();
            getTermLoadMap().put(partNum, loadMap);
            for (String loadFullCode : LOAD_FULL_CODE_LIST) {
                final Double value = omitZero(partWrap.getLoadAsDouble(loadFullCode));
                loadMap.put(loadFullCode, value != null ? value * multiplier : null);
            }

            Map<String, Integer> actionMap = new HashMap<>();
            getTermActionMap().put(partNum, actionMap);
            for (ControlActionWrapper ca : getCaRows()) {
                actionMap.put(ca.getFullCode(), omitZero(partWrap.getActionSize(ca.getFullCode())));
            }
        }
    }

    public void onChangeRowType() {
        setCaRows(IEppEduPlanVersionDataDAO.instance.get().getActiveControlActionTypes(getRow().getType())
                          .map(ControlActionWrapper::new)
                          .collect(Collectors.toList()));
    }

    public void onClickDeleteTerm() {
        // единственную не удаляем
        if (getColumns().size() <= 1) return;
        // ищем колонку для удаления
        ColumnWrapper forDelete = null;
        for (ColumnWrapper c : getColumns()) {
            if (c.getId().equals(getListenerParameterAsLong())) {
                forDelete = c;
            }
        }
        // удаляем ее
        if (null != forDelete) {
            getColumns().remove(forDelete);
            getTermActionMap().remove(forDelete.getId().intValue());
            getTermLoadMap().remove(forDelete.getId().intValue());
        }
        Map<Integer, Map<String, Integer>> caMap = new HashMap<>();
        Map<Integer, Map<String, Double>> loadMap = new HashMap<>();
        List<ColumnWrapper> columns = new ArrayList<>();
        // и перенумеровываем колонки с единицы
        for (int newId = 1; newId <= getColumns().size(); newId++) {
            ColumnWrapper column = getColumns().get(newId - 1);
            caMap.put(newId, getTermActionMap().get(column.getId().intValue()));
            loadMap.put(newId, getTermLoadMap().get(column.getId().intValue()));
            columns.add(new ColumnWrapper(newId, column.getTermNumber()));
        }
        setTermActionMap(caMap);
        setTermLoadMap(loadMap);
        setColumns(columns);
    }

    public void onClickSumHours()
    {
        Map<String, Double> loadMap = new HashMap<>();

        // недели не меняются
        loadMap.put(EppLoadType.FULL_CODE_WEEKS, getLoadMap().get(EppLoadType.FULL_CODE_WEEKS));

        // виды нагрузки, которые суммируется по семестрам в нагрузку строки
        Collection<String> loadTypesToCopy = Arrays.asList(
            EppALoadType.FULL_CODE_TOTAL_LECTURES, EppALoadType.FULL_CODE_TOTAL_LECTURES_I, EppALoadType.FULL_CODE_TOTAL_LECTURES_E,
            EppALoadType.FULL_CODE_TOTAL_PRACTICE, EppALoadType.FULL_CODE_TOTAL_PRACTICE_I, EppALoadType.FULL_CODE_TOTAL_PRACTICE_E,
            EppALoadType.FULL_CODE_TOTAL_LABS, EppALoadType.FULL_CODE_TOTAL_LABS_I, EppALoadType.FULL_CODE_TOTAL_LABS_E,
            EppELoadType.FULL_CODE_AUDIT,
            EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL,
            EppLoadType.FULL_CODE_CONTROL,
            EppLoadType.FULL_CODE_CONTROL_E,
            EppLoadType.FULL_CODE_TOTAL_HOURS,
            EppLoadType.FULL_CODE_LABOR);

        for (ColumnWrapper column: getColumns())
        {
            Map<String, Double> localTermLoadMap = getTermLoadMap().get(column.getId().intValue());

            // вычисляем аудиторную нагрузку (лекции + практики + лаб. занятия) и часы (ауд. нагрузка + сам. нагрузка + контроль) каждого семестра
            localTermLoadMap.put(EppELoadType.FULL_CODE_AUDIT, nullSafeSum(localTermLoadMap.get(EppALoadType.FULL_CODE_TOTAL_LECTURES), localTermLoadMap.get(EppALoadType.FULL_CODE_TOTAL_PRACTICE), localTermLoadMap.get(EppALoadType.FULL_CODE_TOTAL_LABS)));
            localTermLoadMap.put(EppLoadType.FULL_CODE_TOTAL_HOURS, nullSafeSum(localTermLoadMap.get(EppELoadType.FULL_CODE_AUDIT), localTermLoadMap.get(EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL), localTermLoadMap.get(EppLoadType.FULL_CODE_CONTROL)));

            // вычисляем часы, трудоемкость, нагрузку строки как суммы соответствующих видов по семестрам
            for (String loadType: loadTypesToCopy)
                loadMap.put(loadType, nullSafeSum(loadMap.get(loadType), localTermLoadMap.get(loadType)));
        }

        setLoadMap(loadMap);
    }

    // utils

    private <N extends Number> N omitZero(N amount)
    {
        if (null == amount) return null;
        if (UniEppUtils.eq(0, amount.doubleValue())) return null;
        return amount;
    }

    private static Double nullSafeSum(Double... values)
    {
        Double result = null;
        for (Double value: values)
        {
            if (value != null)
            {
                if (result == null)
                    result = 0d;

                result += value;
            }
        }

        return result;
    }

    public void onClickAddTerm() {
        int termNumber = 1;
        for (ColumnWrapper c : getColumns()) {
            termNumber = Math.max(termNumber, c.getTermNumber());
        }
        getColumns().add(new ColumnWrapper(getColumns().size() + 1, termNumber + 1));
    }

    @Override
    public void onChangeParent()
    {
        if (!isEditForm()) getRow().setNumber(null);
        getRow().setNumber(IEppEduPlanVersionDataDAO.instance.get().getNextNumber(getRow(), null));
    }

    // override

    @Override
    protected void saveData()
    {
        final Map<Integer, Map<String, Double>> tLoadMap = new HashMap<>();
        final Map<Integer, Map<String, Integer>> tActionMap = new HashMap<>();

        for (ColumnWrapper c : getColumns()) {
            tLoadMap.put(c.getTermNumber(), getTermLoadMap().get(c.getId().intValue()));
            tActionMap.put(c.getTermNumber(), getTermActionMap().get(c.getId().intValue()));
        }

        IEppEduPlanVersionDataDAO.IRowLoadWrapper loadWrapper = new IEppEduPlanVersionDataDAO.IRowLoadWrapper() {
            @Override public EppEpvTermDistributedRow row() { return getRow(); }
            @Override public Map<String, Double> rowLoadMap() { return getLoadMap(); }
            @Override public Map<Integer, Map<String, Double>> rowTermLoadMap() { return tLoadMap; }
            @Override public Map<Integer, Map<String, Integer>> rowTermActionMap() { return tActionMap; }
        };

        IEppEduPlanVersionDataDAO.instance.get().doSaveOrUpdateRow(getRow(), loadWrapper);
    }

    @Override
    protected void validate()
    {
        if (getColumns().isEmpty()) {
            throw new ApplicationException("Не задана нагрузка по семестрам.");
        }

        Set<Integer> terms = new HashSet<>(CollectionUtils.collect(getColumns(), ColumnWrapper::getTermNumber));
        if (terms.size() != getColumns().size()) {
            throw new ApplicationException("Номера семестров не должны повторяться.");
        }

        int maxTerm = 1;
        for (ColumnWrapper c : getColumns()) {
            maxTerm = Math.max(maxTerm, c.getTermNumber());
            Double loadSum = .0;
            for (LoadTypeWrapper l : getLoadRows()) {
                loadSum = loadSum + UniBaseUtils.nullToZero(getTermLoadMap().get(c.getId().intValue()).get(l.getFullCode()));
            }
            for (ControlActionWrapper ca : getCaRows()) {
                loadSum = loadSum + UniBaseUtils.nullToZero(getTermActionMap().get(c.getId().intValue()).get(ca.getFullCode()));
            }
            if (UniEppUtils.eq(0, loadSum)) {
                throw new ApplicationException("По " + c.getTermNumber() + " семестру не задана нагрузка.");
            }
        }

        int maxTermAllowed = DataAccessServices.dao().getCount(DevelopGridTerm.class, DevelopGridTerm.developGrid().s(), getRow().getOwner().getEduPlanVersion().getEduPlan().getDevelopGrid());
        if (maxTerm > maxTermAllowed) {
            throw new ApplicationException("Введенный семестр: " + maxTerm + " - превышает последний семестр сетки: " + maxTermAllowed + ".");
        }
    }

    // presenter

    public int getRowspanTotal() { return 2 + getCaRows().size() + getLoadRows().size(); }
    public int getColspanTotal() { return 3 + getColumns().size(); }

    public String getTermFieldId() { return "term." + getColumn().getId(); }
    public String getTermDeleteId() { return "term.delete." + getColumn().getId(); }
    public String getLoadFieldId() { return "load." + getLoadRow().getFullCode() + "." + getColumn().getId();}
    public String getLoadTotalFieldId() { return "load." + getLoadRow().getFullCode();}
    public String getCaFieldId() { return "ca." + getCaRow().getFullCode() + "." + getColumn().getId();}

    public boolean isShowLoadTotalField() { return !EppLoadType.FULL_CODE_WEEKS.equals(getLoadRow().getFullCode()); }

    public Double getLoadValue() {
        return SafeMap.safeGet(getTermLoadMap(), getColumn().getId().intValue(), HashMap.class).get(getLoadRow().getFullCode());
    }
    public void setLoadValue(final Double value) {
        SafeMap.safeGet(getTermLoadMap(), getColumn().getId().intValue(), HashMap.class).put(getLoadRow().getFullCode(), value);
    }

    public Double getLoadTotalValue() {
        return getLoadMap().get(getLoadRow().getFullCode());
    }
    public void setLoadTotalValue(final Double value) {
        getLoadMap().put(getLoadRow().getFullCode(), value);
    }

    public Integer getCaValueAsInteger() {
        return (Integer) SafeMap.safeGet(getTermActionMap(), getColumn().getId().intValue(), HashMap.class).get(getCaRow().getFullCode());
    }
    public void setCaValueAsInteger(final Integer value) {
        SafeMap.safeGet(getTermActionMap(), getColumn().getId().intValue(), HashMap.class).put(getCaRow().getFullCode(), value);
    }

    public Boolean getCaValueAsBoolean() {
        final Integer value = this.getCaValueAsInteger();
        return (null == value ? null : value > 0);
    }
    public void setCaValueAsBoolean(final Boolean value) {
        this.setCaValueAsInteger(null == value ? null : value ? 1 : 0);
    }

    public boolean isCaFieldBoolean() {
        return getCaRow().getCa().isFlagValue();
    }

    public boolean isNeedBreakAfterLoadRow() {
        return getLoadRow().getFullCode().equals(EppLoadType.FULL_CODE_LABOR);
    }

    public String getLoadFieldDisplayName() {return getLoadRow().getTitle() + " (" + getColumn().getTermNumber() + " семестр)" ;}
    public String getCaFieldDisplayName() {return getCaRow().getCa().getTitle() + " (" + getColumn().getTermNumber() + " семестр)" ;}

    // getters and setters

    public Map<String, Double> getLoadMap()
    {
        return loadMap;
    }

    public void setLoadMap(Map<String, Double> loadMap)
    {
        this.loadMap = loadMap;
    }

    public Map<Integer, Map<String, Integer>> getTermActionMap()
    {
        return termActionMap;
    }

    public void setTermActionMap(Map<Integer, Map<String, Integer>> termActionMap)
    {
        this.termActionMap = termActionMap;
    }

    public Map<Integer, Map<String, Double>> getTermLoadMap()
    {
        return termLoadMap;
    }

    public void setTermLoadMap(Map<Integer, Map<String, Double>> termLoadMap)
    {
        this.termLoadMap = termLoadMap;
    }

    public ColumnWrapper getColumn()
    {
        return column;
    }

    public void setColumn(ColumnWrapper column)
    {
        this.column = column;
    }

    public List<ColumnWrapper> getColumns()
    {
        return columns;
    }

    public void setColumns(List<ColumnWrapper> columns)
    {
        this.columns = columns;
    }

    public LoadTypeWrapper getLoadRow()
    {
        return loadRow;
    }

    public void setLoadRow(LoadTypeWrapper loadRow)
    {
        this.loadRow = loadRow;
    }

    public List<LoadTypeWrapper> getLoadRows()
    {
        return loadRows;
    }

    public void setLoadRows(List<LoadTypeWrapper> loadRows)
    {
        this.loadRows = loadRows;
    }

    public ControlActionWrapper getCaRow()
    {
        return caRow;
    }

    public void setCaRow(ControlActionWrapper caRow)
    {
        this.caRow = caRow;
    }

    public List<ControlActionWrapper> getCaRows()
    {
        return caRows;
    }

    public void setCaRows(List<ControlActionWrapper> caRows)
    {
        this.caRows = caRows;
    }


    // inner classes

    public static class ControlActionWrapper extends IdentifiableWrapper
        {
        private EppControlActionType ca;
        public ControlActionWrapper(EppControlActionType ca) {
            super(ca.getId(), ca.getTitle() + " (" + ca.getShortTitle() + ")");
            this.ca = ca;
        }
        public EppControlActionType getCa() { return ca; }
        public String getFullCode() { return ca.getFullCode(); }
    }

    public static class LoadTypeWrapper extends IdentifiableWrapper {
        private String fullCode;
        public LoadTypeWrapper(Long id, String title, String fullCode) {
            super(id, title);
            this.fullCode = fullCode;
        }
        public String getFullCode() { return fullCode; }
    }

    public static class ColumnWrapper extends IdentifiableWrapper {
        private int termNumber;
        public ColumnWrapper(int pos, int termNumber) {
            super((long) pos, String.valueOf(termNumber));
            this.termNumber = termNumber;
        }
        public int getTermNumber() { return termNumber; }
        public void setTermNumber(int termNumber) { this.termNumber = termNumber; }
    }
}
