/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithoutEduPlanBlock;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListPresenter;

/**
 * @author oleyba
 * @since 9/15/14
 */
@State ({
    @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
public class EppIndicatorStudentsWithoutEduPlanBlockUI extends AbstractEppIndicatorStudentListPresenter
{
    @Override
    public String getSettingsKey()
    {
        return "epp.StudentsWithoutEduPlanBlock.filter";
    }
}