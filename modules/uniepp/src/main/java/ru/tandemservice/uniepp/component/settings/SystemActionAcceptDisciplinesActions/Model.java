package ru.tandemservice.uniepp.component.settings.SystemActionAcceptDisciplinesActions;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

public class Model
{
    private DynamicListDataSource<OrgUnit> dataSource;
    public DynamicListDataSource<OrgUnit> getDataSource() { return this.dataSource; }
    public void setDataSource(final DynamicListDataSource<OrgUnit> dataSource) { this.dataSource = dataSource; }

}
