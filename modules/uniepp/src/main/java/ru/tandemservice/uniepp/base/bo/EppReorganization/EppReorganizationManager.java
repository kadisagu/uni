/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization;

import org.springframework.context.annotation.Bean;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.springframework.context.annotation.Configuration;
import ru.tandemservice.uniepp.base.bo.EppReorganization.logic.EppReorganizationDao;
import ru.tandemservice.uniepp.base.bo.EppReorganization.logic.IEppReorganizationDao;

/**
 * @author rsizonenko
 * @since 30.03.2016
 */
@Configuration
public class EppReorganizationManager extends BusinessObjectManager {

    public static EppReorganizationManager instance() {
        return instance(EppReorganizationManager.class);
    }

    @Bean
    public IEppReorganizationDao dao()
    {
        return new EppReorganizationDao();
    }

}
