/* $Id$ */
package ru.tandemservice.uniepp.dao.eduplan;

import com.google.common.base.Preconditions;
import org.springframework.util.ClassUtils;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.dao.eduplan.data.EppCustomPlanWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppCustomPlanWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 21.08.2015
 */
public class EppCustomEduPlanDAO extends UniBaseDao implements IEppCustomEduPlanDAO
{
    @Override
    public INumberGenerationRule<EppCustomEduPlan> getCustomEduPlanNumberGenerationRule() {
        return new SimpleNumberGenerationRule<EppCustomEduPlan>() {
            @Override public Set<String> getUsedNumbers(final EppCustomEduPlan object) {
                final List<String> list = EppCustomEduPlanDAO.this.getPropertiesList(EppCustomEduPlan.class, EppCustomEduPlan.epvBlock().eduPlanVersion(), object.getEpvBlock().getEduPlanVersion(), false, EppCustomEduPlan.number());
                return new HashSet<>(list);
            }
            @Override public String getNumberQueueName(final EppCustomEduPlan object) {
                return ClassUtils.getUserClass(object).getSimpleName() + "." + object.getEpvBlock().getEduPlanVersion().getId();
            }
        };
    }

    @Override
    public void saveCustomEduPlan(EppCustomEduPlan customEduPlan)
    {
        final boolean newPlan = (null == customEduPlan.getId());

        Preconditions.checkNotNull(customEduPlan);
        Preconditions.checkNotNull(customEduPlan.getEpvBlock());

        customEduPlan.setPlan((EppEduPlanProf) customEduPlan.getEpvBlock().getEduPlanVersion().getEduPlan());
        customEduPlan.setState(this.getCatalogItem(EppState.class, EppState.STATE_FORMATIVE));
        customEduPlan.setNumber(INumberQueueDAO.instance.get().getNextNumber(customEduPlan));

        this.saveOrUpdate(customEduPlan);

        if (newPlan) {
            this.saveCustomEduPlanContent(customEduPlan, new EppCustomPlanWrapper(customEduPlan));
        }
    }

    @Override
    public void saveCustomEduPlanContent(final EppCustomEduPlan customEduPlan, final IEppCustomPlanWrapper planWrapper)
    {
        final MergeAction.SessionMergeAction<EppCustomEpvRowTerm.NaturalId, EppCustomEpvRowTerm> mergeAction = new MergeAction.SessionMergeAction<EppCustomEpvRowTerm.NaturalId, EppCustomEpvRowTerm>() {
            @Override protected EppCustomEpvRowTerm.NaturalId key(EppCustomEpvRowTerm source) { return new EppCustomEpvRowTerm.NaturalId(source.getEpvRowTerm(), customEduPlan); }
            @Override protected EppCustomEpvRowTerm buildRow(EppCustomEpvRowTerm source) { return new EppCustomEpvRowTerm(source.getEpvRowTerm(), customEduPlan); }
            @Override protected void fill(EppCustomEpvRowTerm target, EppCustomEpvRowTerm source) { target.update(source, false); }
        };

        final Map<Integer, Term> termMap = DevelopGridDAO.getTermMap();
        final Map<Long, Integer> termNumberMap = DevelopGridDAO.getIdToTermNumberMap();

        // { [epvTerm.intNumber, epvRow.id] -> proxy(epvRowTerm) }
        final Map<PairKey<Integer, Long>, EppEpvRowTerm> epvRowTermMap =  new DQLSelectBuilder().fromEntity(EppEpvRowTerm.class, "t")
                .column(property("t", EppEpvRowTerm.term().id()))
                .column(property("t", EppEpvRowTerm.row().id()))
                .column(property("t", EppEpvRowTerm.id()))
                .joinPath(DQLJoinType.inner, EppEpvRowTerm.row().owner().fromAlias("t"), "b")
                .where(eq(property("b", EppEduPlanVersionBlock.eduPlanVersion()), value(customEduPlan.getEpvBlock().getEduPlanVersion())))
                .where(or(
                        eq(property("b"), value(customEduPlan.getEpvBlock())),
                        instanceOf("b", EppEduPlanVersionRootBlock.class)
                ))
                .createStatement(getSession()).<Object[]>list().stream()
                .collect(Collectors.toMap(item -> new PairKey<>(termNumberMap.get((Long) item[0]), (Long) item[1]),
                                          item -> proxy((Long) item[2])));

        final List<EppCustomEpvRowTerm> dbRecords = getList(EppCustomEpvRowTerm.class, EppCustomEpvRowTerm.customEduPlan(), customEduPlan);
        final List<EppCustomEpvRowTerm> targetRecords = planWrapper.getRowMap().values().stream()
                .flatMap(Collection::stream)
                .map(rowWrapper -> {
                    final EppEpvRowTerm eppEpvRowTerm = epvRowTermMap.get(new PairKey<>(rowWrapper.getSourceTermNumber(), rowWrapper.getSourceRow().getId()));
                    final EppCustomEpvRowTerm newCustomRow = new EppCustomEpvRowTerm(eppEpvRowTerm, customEduPlan);
                    newCustomRow.setNewTerm(termMap.get(rowWrapper.getNewTermNumber()));
                    newCustomRow.setNeedRetake(rowWrapper.getNeedRetake());
                    newCustomRow.setExcluded(rowWrapper.isExcluded());
                    return newCustomRow;
                })
                .collect(Collectors.toList());

        mergeAction.merge(dbRecords, targetRecords);
    }
}