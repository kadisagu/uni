package ru.tandemservice.uniepp.entity.std.data;

import ru.tandemservice.uniepp.entity.IEppNumberRow;
import ru.tandemservice.uniepp.entity.std.data.gen.EppStdDisciplineTopRowGen;

/**
 * Дисциплина версии ГОС
 */
public class EppStdDisciplineTopRow extends EppStdDisciplineTopRowGen implements IEppNumberRow
{
    @Override public EppStdHierarchyRow getHierarhyParent() { return this.getParent(); }
    @Override public void setHierarhyParent(final EppStdRow parent) { this.setParent((EppStdHierarchyRow) parent); }

    @Override public int getComparatorClassIndex() { return 3; }
    @Override public String getComparatorString() { return this.getSelfIndexPart(); }
    @Override public String getSelfIndexPart() { return IEppNumberRow.NUMBER_FORMATTER.format(this); }
}