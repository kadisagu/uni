/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.AddEditResult;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.base.bo.EppState.util.IEppStateConfig;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.EppWorkPlanManager;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.logic.List.EppWorkPlanListDSHandler;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.sec.UnieppStateChangePermissionHolder;
import ru.tandemservice.uniepp.sec.UnieppStateChangePermissionModifier;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Denis Katkov
 * @since 01.03.2016
 */
public class EppWorkPlanListUI extends OrgUnitUIPresenter
{
    private EppState _transition;
    private List<EppState> _transitionList;

    @Override
    protected String getSecModelPostfix() {
        String postfix = UnieppStateChangePermissionModifier.POSTFIX_WORK_PLAN_LIST;
        if (this.getOrgUnit() != null) {
            postfix = postfix + "_" + super.getSecModelPostfix();
        }
        return postfix;
    }

    @Override
    public void onComponentRefresh()
    {
        getOrgUnitHolder().getSecModel().addException(
                "view",
                propertyName ->  {
                    if (this.getOrgUnit() != null) { return ""; }
                    return "menuEppWorkPlanList";
                });

        setTransitionList(IUniBaseDao.instance.get().getCatalogItemListOrderByCode(EppState.class));
    }

    @Override
    public String getSettingsKey() {
        return "epp.eduWorkPlanList." + this.getOrgUnitHolder().getId();
    }

    @Bean
    public void onBeforeDataSourceFetch(final IUIDataSource dataSource)
    {
        dataSource.put("orgUnit", getOrgUnit());
        if (getSettings().get(EppWorkPlanListDSHandler.PARAM_YEAR_EDUCATION_PROCESS) == null) {
            EppYearEducationProcess currentYear = UniDaoFacade.getCoreDao().get(EppYearEducationProcess.class, EppYearEducationProcess.educationYear().current().s(), true);
            getSettings().set(EppWorkPlanListDSHandler.PARAM_YEAR_EDUCATION_PROCESS, currentYear);
        }
        if (dataSource.getName().equals(EppWorkPlanList.EPP_WORK_PLAN_LIST_DS)) {
            Map<String, Object> settingMap = _uiSettings.getAsMap(
                    EppWorkPlanListDSHandler.PARAM_NUMBER,
                    EppWorkPlanListDSHandler.PARAM_YEAR_EDUCATION_PROCESS,
                    EppWorkPlanListDSHandler.PARAM_PROGRAM_SUBJECT,
                    EppWorkPlanListDSHandler.PARAM_PROGRAM_SPECIALIZATION_LIST,
                    EppWorkPlanListDSHandler.PARAM_PROGRAM_FORM_LIST,
                    EppWorkPlanListDSHandler.PARAM_DEVELOP_CONDITION_LIST,
                    EppWorkPlanListDSHandler.PARAM_PROGRAM_TRAIT_LIST,
                    EppWorkPlanListDSHandler.PARAM_TERM,
                    EppWorkPlanListDSHandler.PARAM_STATE,
                    EppWorkPlanListDSHandler.PARAM_PROGRAM_KIND
            );
            dataSource.putAll(settingMap);
        } else if (dataSource.getName().equals(EppWorkPlanManager.PROGRAM_SUBJECT_DS)) {
            dataSource.put(EppWorkPlanListDSHandler.PARAM_PROGRAM_KIND, getSettings().get(EppWorkPlanListDSHandler.PARAM_PROGRAM_KIND));
            dataSource.put(EppWorkPlanListDSHandler.PARAM_YEAR_EDUCATION_PROCESS, getSettings().get(EppWorkPlanListDSHandler.PARAM_YEAR_EDUCATION_PROCESS));
        } else if (dataSource.getName().equals(EppWorkPlanList.PROGRAM_SPECIALIZATION_DS)) {
            dataSource.put(EppWorkPlanListDSHandler.PARAM_PROGRAM_SUBJECT, getSettings().get(EppWorkPlanListDSHandler.PARAM_PROGRAM_SUBJECT));
        }
    }

    public void onEditEntityFromList()
    {
        getActivationBuilder().asRegionDialog(ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit.Model.class.getPackage().getName())
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }


    public void onClickAddWorkPlan()
    {
        getActivationBuilder().asRegionDialog(ru.tandemservice.uniepp.component.workplan.WorkPlanWizard.Model.class.getPackage().getName())
                .parameter(UIPresenter.PUBLISHER_ID, getOrgUnitHolder().getId())
                .parameter("selectedYear", getSettings().get(EppWorkPlanListDSHandler.PARAM_YEAR_EDUCATION_PROCESS))
                .activate();
    }

    public void onClickAddByGUP()
    {
        getActivationBuilder().asRegionDialog(ru.tandemservice.uniepp.component.workplan.AutocreateWorkPlanByGUP.Model.class.getPackage().getName())
                .parameter(UIPresenter.PUBLISHER_ID, getOrgUnitHolder().getId())
                .parameter("selectedYear", ((EppYearEducationProcess) getSettings().get(EppWorkPlanListDSHandler.PARAM_YEAR_EDUCATION_PROCESS)).getEducationYear().getIntValue())
                .activate();
    }

    public List<EppState> getTransitionList()
    {
        return _transitionList;
    }

    public void setTransitionList(List<EppState> transitionList){
        _transitionList = transitionList;
    }

    public boolean isProgramKindNotSelected()
    {
        return getSettings().get(EppWorkPlanListDSHandler.PARAM_PROGRAM_KIND) == null;
    }

    public EppState getTransition()
    {
        return _transition;
    }

    public void setTransition(EppState transition)
    {
        _transition = transition;
    }

    public String getTransitionTitle()
    {
        final EppState transition = this.getTransition();
        if (null == transition) {
            return "";
        }

        final String title = StringUtils.trimToNull(transition.getTransitionTitle());
        if (null != title) {
            return title;
        }

        return ("Перевести в состояние «" + transition.getTitle() + "»");
    }

    public void onClickMassChangeState()
    {
        DynamicListDataSource ds = ((PageableSearchListDataSource) getConfig().getDataSource(EppWorkPlanList.EPP_WORK_PLAN_LIST_DS)).getLegacyDataSource();
        CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn("checkbox"));


        BaseSearchListDataSource representDS = getConfig().getDataSource(EppWorkPlanList.EPP_WORK_PLAN_LIST_DS);

        Collection<IEntity> records = ((CheckboxColumn) representDS.getLegacyDataSource()
                .getColumn(EppWorkPlanList.CHECKBOX_COLUMN)).getSelectedObjects();

        final EppState newState = IUniBaseDao.instance.get().getCatalogItem(EppState.class, getListenerParameter());
        final int editedCount = EppStateManager.instance().eppStateDao().massChangeState(newState.getCode(), CommonBaseEntityUtil.getIdList(records));


        if (records.isEmpty() || editedCount <= 0) {
            IEppStateConfig stateConfig = EppStateManager.instance().eppStateDao().getStateConfig(EppWorkPlanBase.class);
            final List<String> fromStateCode = stateConfig.getFromPossibleTransitions().get(newState.getCode());
            Collection<String> fromStateTitles = IUniBaseDao.instance.get().getPropertiesList(EppState.class, EppState.code(), fromStateCode, true, EppState.title());
            throw new ApplicationException(
                    (records.isEmpty() ? "Выберите РУП в состоянии " : "РУП должен иметь состояние ") + fromStateTitles.stream().map(s -> "«" + s + "»").collect(Collectors.joining(" или ")) + ".");
        } else {
            UserContext.getInstance().getInfoCollector().add(
                    "Изменено состояние на «" + newState.getTitle() + "» для " +
                            CommonBaseStringUtil.numberWithPostfixCase(editedCount, "рабочего учебного плана", "рабочих учебных планов", "рабочих учебных планов") + ".");
            checkboxColumn.setSelectedObjects(Collections.<IEntity>emptyList());
        }

    }

    public void onClearSettings()
    {
        CommonBaseSettingsUtil.clearAndSaveSettingsExcept(getSettings(), EppWorkPlanListDSHandler.PARAM_PROGRAM_KIND, EppWorkPlanListDSHandler.PARAM_YEAR_EDUCATION_PROCESS);
    }

    public String getTransitionPermissionKey()
    {
        return UnieppStateChangePermissionHolder.getMassPermission(this.getTransition(), getSec().getPostfix());
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        super.onComponentBindReturnParameters(childRegionName, returnedData);
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((PageableSearchListDataSource) getConfig().getDataSource(EppWorkPlanList.EPP_WORK_PLAN_LIST_DS)).getOptionColumnSelectedObjects(EppWorkPlanList.CHECKBOX_COLUMN);
            selected.clear();
        }
        AddEditResult.processReturned(returnedData);
    }
}