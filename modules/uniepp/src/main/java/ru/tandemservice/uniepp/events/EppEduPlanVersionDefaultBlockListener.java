package ru.tandemservice.uniepp.events;

import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import org.tandemframework.hibsupport.event.single.listener.IHibernateEventListener;
import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;

import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionBlockGen;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.notExists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class EppEduPlanVersionDefaultBlockListener implements IHibernateEventListener<ISingleEntityEvent>
{

    private static final TransactionCompleteAction<Void> action = new TransactionCompleteAction<Void>() {
        @Override public Void beforeCompletion(final Session session) {

            // создаем рутовый блок для версий УПв, если его там нет
            DQLSelectBuilder vDql = new DQLSelectBuilder().fromEntity(EppEduPlanVersion.class, "v").column("v.id")
                .where(notExists(new DQLSelectBuilder()
                    .fromEntity(EppEduPlanVersionRootBlock.class, "r")
                    .where(eq(property("r", EppEduPlanVersionRootBlock.eduPlanVersion()), property("v")))
                    .buildQuery()));

            for (final Long versionId: vDql.createStatement(session).<Long>list()) {
                final EppEduPlanVersionBlock block = new EppEduPlanVersionRootBlock();
                block.setEduPlanVersion((EppEduPlanVersion)session.load(EppEduPlanVersion.class, versionId));
                session.save(block);
            }

            // нужно, потому что инече данные в базу не попадут
            session.flush();

            return null;
        }
    };

    @Override
    public void onEvent(final ISingleEntityEvent event) {
        if (event.getEntity() instanceof EppEduPlanVersion) {
            EppEduPlanVersionDefaultBlockListener.action.register(event.getSession());
        }
    }

}
