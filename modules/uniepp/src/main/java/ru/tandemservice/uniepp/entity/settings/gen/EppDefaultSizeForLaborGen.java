package ru.tandemservice.uniepp.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Правило пересчета трудоемкости (при форм. УП из ГОСа)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppDefaultSizeForLaborGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor";
    public static final String ENTITY_NAME = "eppDefaultSizeForLabor";
    public static final int VERSION_HASH = -1194004823;
    private static IEntityMeta ENTITY_META;

    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_DEVELOP_TECH = "developTech";
    public static final String L_DEVELOP_GRID = "developGrid";
    public static final String P_VALUE = "value";
    public static final String P_DOUBLE_VALUE = "doubleValue";
    public static final String P_TITLE = "title";

    private DevelopForm _developForm;     // Форма освоения
    private DevelopCondition _developCondition;     // Условие освоения
    private DevelopTech _developTech;     // Технология освоения
    private DevelopGrid _developGrid;     // Учебная сетка
    private long _value;     // Число часов на единицу трудоемкости

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Форма освоения.
     */
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(DevelopTech developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Учебная сетка.
     */
    public DevelopGrid getDevelopGrid()
    {
        return _developGrid;
    }

    /**
     * @param developGrid Учебная сетка.
     */
    public void setDevelopGrid(DevelopGrid developGrid)
    {
        dirty(_developGrid, developGrid);
        _developGrid = developGrid;
    }

    /**
     * @return Число часов на единицу трудоемкости. Свойство не может быть null.
     */
    @NotNull
    public long getValue()
    {
        return _value;
    }

    /**
     * @param value Число часов на единицу трудоемкости. Свойство не может быть null.
     */
    public void setValue(long value)
    {
        dirty(_value, value);
        _value = value;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppDefaultSizeForLaborGen)
        {
            setDevelopForm(((EppDefaultSizeForLabor)another).getDevelopForm());
            setDevelopCondition(((EppDefaultSizeForLabor)another).getDevelopCondition());
            setDevelopTech(((EppDefaultSizeForLabor)another).getDevelopTech());
            setDevelopGrid(((EppDefaultSizeForLabor)another).getDevelopGrid());
            setValue(((EppDefaultSizeForLabor)another).getValue());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppDefaultSizeForLaborGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppDefaultSizeForLabor.class;
        }

        public T newInstance()
        {
            return (T) new EppDefaultSizeForLabor();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developGrid":
                    return obj.getDevelopGrid();
                case "value":
                    return obj.getValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((DevelopTech) value);
                    return;
                case "developGrid":
                    obj.setDevelopGrid((DevelopGrid) value);
                    return;
                case "value":
                    obj.setValue((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developGrid":
                        return true;
                case "value":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developGrid":
                    return true;
                case "value":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "developForm":
                    return DevelopForm.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "developTech":
                    return DevelopTech.class;
                case "developGrid":
                    return DevelopGrid.class;
                case "value":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppDefaultSizeForLabor> _dslPath = new Path<EppDefaultSizeForLabor>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppDefaultSizeForLabor");
    }
            

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getDevelopTech()
     */
    public static DevelopTech.Path<DevelopTech> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Учебная сетка.
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getDevelopGrid()
     */
    public static DevelopGrid.Path<DevelopGrid> developGrid()
    {
        return _dslPath.developGrid();
    }

    /**
     * @return Число часов на единицу трудоемкости. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getValue()
     */
    public static PropertyPath<Long> value()
    {
        return _dslPath.value();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getDoubleValue()
     */
    public static SupportedPropertyPath<Double> doubleValue()
    {
        return _dslPath.doubleValue();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EppDefaultSizeForLabor> extends EntityPath<E>
    {
        private DevelopForm.Path<DevelopForm> _developForm;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private DevelopTech.Path<DevelopTech> _developTech;
        private DevelopGrid.Path<DevelopGrid> _developGrid;
        private PropertyPath<Long> _value;
        private SupportedPropertyPath<Double> _doubleValue;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getDevelopTech()
     */
        public DevelopTech.Path<DevelopTech> developTech()
        {
            if(_developTech == null )
                _developTech = new DevelopTech.Path<DevelopTech>(L_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Учебная сетка.
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getDevelopGrid()
     */
        public DevelopGrid.Path<DevelopGrid> developGrid()
        {
            if(_developGrid == null )
                _developGrid = new DevelopGrid.Path<DevelopGrid>(L_DEVELOP_GRID, this);
            return _developGrid;
        }

    /**
     * @return Число часов на единицу трудоемкости. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getValue()
     */
        public PropertyPath<Long> value()
        {
            if(_value == null )
                _value = new PropertyPath<Long>(EppDefaultSizeForLaborGen.P_VALUE, this);
            return _value;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getDoubleValue()
     */
        public SupportedPropertyPath<Double> doubleValue()
        {
            if(_doubleValue == null )
                _doubleValue = new SupportedPropertyPath<Double>(EppDefaultSizeForLaborGen.P_DOUBLE_VALUE, this);
            return _doubleValue;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EppDefaultSizeForLaborGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EppDefaultSizeForLabor.class;
        }

        public String getEntityName()
        {
            return "eppDefaultSizeForLabor";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getDoubleValue();

    public abstract String getTitle();
}
