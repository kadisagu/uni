/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphPub;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.tapestry.json.JSONObject;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.RichDataSourceModel;
import org.tandemframework.shared.commonbase.tapestry.component.spreadsheet.ISpreadsheetCellDataValueResolver;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphWeekPart;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniepp.ui.WorkGraphEduProgramSubjectListModel;
import ru.tandemservice.uniepp.util.WeekTypeLegendRow;

import static ru.tandemservice.uniepp.util.EppEduPlanVersionScheduleGridUtils.CUSTOM_GRID_STYLE;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "workGraph.id"),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
@SuppressWarnings("unchecked")
public class Model implements WorkGraphEduProgramSubjectListModel.CoursesProvider
{
    private EppWorkGraph _workGraph = new EppWorkGraph();
    private String _selectedTab;                          // текущий таб
    private IDataSettings _settings;                      // сеттинги
    private ISelectModel _courseListModel;                // курс
    private ISelectModel _programSubjectModel;
    private RichDataSourceModel<IEntity> _headerDataSource;                               // шапка всех таблиц ГУП
    private Map<EduProgramSubject, RangeSelectionWeekTypeListDataSource> _id2dataSource;  // таблицы гуп (группировка по направлению подготовки)
    private EduProgramSubject _currentProgramSubject;                  // текущее значение цикла
    private List<WeekTypeLegendRow> _weekTypeLegendList;  // строки легенды
    private WeekTypeLegendRow _weekTypeLegendItem;        // текущее значение цикла
    private EppYearEducationWeek[] _weekData;             // массив учебных недель в гуп

    private SimpleListDataSource<ViewWrapper<EppWorkGraphWeekPart>> _dataSource;
    private IMergeRowIdResolver _columnMergeResolver;

    // Filter Methods

    public void setCourses(final Collection<Course> courses)
    {
        this._settings.set("courses" + this._workGraph.getId(), courses);
    }

    @Override
    public Collection<Course> getCourses()
    {
        return (Collection<Course>) this._settings.get("courses" + this._workGraph.getId());
    }

    public EduProgramSubject getProgramSubject()
    {
        return (EduProgramSubject) this._settings.get("programSubject" + this._workGraph.getId());
    }

    public void setProgramSubject(final EduProgramSubject programSubject)
    {
        this._settings.set("programSubject" + this._workGraph.getId(), programSubject);
    }

    // Getters & Setters

    public EppWorkGraph getWorkGraph()
    {
        return this._workGraph;
    }

    public void setWorkGraph(final EppWorkGraph workGraph)
    {
        this._workGraph = workGraph;
    }

    public String getSelectedTab()
    {
        return this._selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        this._selectedTab = selectedTab;
    }

    public IDataSettings getSettings()
    {
        return this._settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this._settings = settings;
    }

    public ISelectModel getCourseListModel()
    {
        return this._courseListModel;
    }

    public void setCourseListModel(final ISelectModel courseListModel)
    {
        this._courseListModel = courseListModel;
    }

    public ISelectModel getProgramSubjectModel()
    {
        return _programSubjectModel;
    }

    public void setProgramSubjectModel(ISelectModel programSubjectModel)
    {
        _programSubjectModel = programSubjectModel;
    }

    public RichDataSourceModel<IEntity> getHeaderDataSource()
    {
        return this._headerDataSource;
    }

    public void setHeaderDataSource(final RichDataSourceModel<IEntity> headerDataSource)
    {
        this._headerDataSource = headerDataSource;
    }

    public EduProgramSubject getCurrentProgramSubject()
    {
        return _currentProgramSubject;
    }

    public void setCurrentProgramSubject(EduProgramSubject currentProgramSubject)
    {
        _currentProgramSubject = currentProgramSubject;
    }

    public Map<EduProgramSubject, RangeSelectionWeekTypeListDataSource> getId2dataSource()
    {
        return _id2dataSource;
    }

    public void setId2dataSource(Map<EduProgramSubject, RangeSelectionWeekTypeListDataSource> id2dataSource)
    {
        _id2dataSource = id2dataSource;
    }

    public List<WeekTypeLegendRow> getWeekTypeLegendList()
    {
        return this._weekTypeLegendList;
    }

    public void setWeekTypeLegendList(final List<WeekTypeLegendRow> weekTypeLegendList)
    {
        this._weekTypeLegendList = weekTypeLegendList;
    }

    public WeekTypeLegendRow getWeekTypeLegendItem()
    {
        return this._weekTypeLegendItem;
    }

    public void setWeekTypeLegendItem(final WeekTypeLegendRow weekTypeLegendItem)
    {
        this._weekTypeLegendItem = weekTypeLegendItem;
    }

    public EppYearEducationWeek[] getWeekData()
    {
        return this._weekData;
    }

    public void setWeekData(final EppYearEducationWeek[] weekData)
    {
        this._weekData = weekData;
    }

    public Boolean getIsGridTabVisible() {
        return Debug.isEnabled();
    }

    public SimpleListDataSource<ViewWrapper<EppWorkGraphWeekPart>> getDataSource() {
        return _dataSource;
    }

    public void setDataSource(SimpleListDataSource<ViewWrapper<EppWorkGraphWeekPart>> _dataSource) {
        this._dataSource = _dataSource;
    }

    public JSONObject getUserOptions() throws ParseException {
        JSONObject options = new JSONObject()
                .put("autoRowSize", false)
                .put("autoColumnSize", false)
                .put("manualColumnResize", false)
                .put("manualRowResize", false)
                .put("manualColumnMove", false)
                .put("fillHandle", true)
                .put("currentRowClassName", "'currentRow'")
                .put("currentColClassName", "'currentCol'")
                .put("maxRows", _dataSource.getCountRow());

        options.put("readOnly", true);
        options.put("contextMenu", false);

        return options;
    }

    public String getCustomStyle()
    {
        return CUSTOM_GRID_STYLE;
    }

    public IMergeRowIdResolver getColumnMergeResolver() {
        return _columnMergeResolver;
    };

    public void setColumnMergeResolver(IMergeRowIdResolver resolver) {
        _columnMergeResolver = resolver;
    }
}
