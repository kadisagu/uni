package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебный план ВО
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanHigherProfGen extends EppEduPlanProf
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf";
    public static final String ENTITY_NAME = "eppEduPlanHigherProf";
    public static final int VERSION_HASH = 902376046;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_ORIENTATION = "programOrientation";

    private EduProgramOrientation _programOrientation;     // Ориентация ОП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Ориентация ОП.
     */
    public EduProgramOrientation getProgramOrientation()
    {
        return _programOrientation;
    }

    /**
     * @param programOrientation Ориентация ОП.
     */
    public void setProgramOrientation(EduProgramOrientation programOrientation)
    {
        dirty(_programOrientation, programOrientation);
        _programOrientation = programOrientation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEduPlanHigherProfGen)
        {
            setProgramOrientation(((EppEduPlanHigherProf)another).getProgramOrientation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanHigherProfGen> extends EppEduPlanProf.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlanHigherProf.class;
        }

        public T newInstance()
        {
            return (T) new EppEduPlanHigherProf();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "programOrientation":
                    return obj.getProgramOrientation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "programOrientation":
                    obj.setProgramOrientation((EduProgramOrientation) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "programOrientation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "programOrientation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "programOrientation":
                    return EduProgramOrientation.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlanHigherProf> _dslPath = new Path<EppEduPlanHigherProf>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlanHigherProf");
    }
            

    /**
     * @return Ориентация ОП.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf#getProgramOrientation()
     */
    public static EduProgramOrientation.Path<EduProgramOrientation> programOrientation()
    {
        return _dslPath.programOrientation();
    }

    public static class Path<E extends EppEduPlanHigherProf> extends EppEduPlanProf.Path<E>
    {
        private EduProgramOrientation.Path<EduProgramOrientation> _programOrientation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Ориентация ОП.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf#getProgramOrientation()
     */
        public EduProgramOrientation.Path<EduProgramOrientation> programOrientation()
        {
            if(_programOrientation == null )
                _programOrientation = new EduProgramOrientation.Path<EduProgramOrientation>(L_PROGRAM_ORIENTATION, this);
            return _programOrientation;
        }

        public Class getEntityClass()
        {
            return EppEduPlanHigherProf.class;
        }

        public String getEntityName()
        {
            return "eppEduPlanHigherProf";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
