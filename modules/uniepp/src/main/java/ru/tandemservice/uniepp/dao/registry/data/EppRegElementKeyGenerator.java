/* $Id$ */
package ru.tandemservice.uniepp.dao.registry.data;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.text.StrBuilder;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;

import java.util.*;

/**
 * Генератор ключа для враппера элемента реестра
 *
 * @author Nikolay Fedorovskih
 * @since 19.05.2015
 */
public class EppRegElementKeyGenerator
{
    /**
     * Правило генерации ключа
     */
    public enum KeyGenRule
    {
        FULL("С учетом форм промежуточного контроля"),
        FULL_WITHOUT_I_CONTROL("Без учета форм промежуточного контроля");

        public final String title;
        KeyGenRule(String title)
        {
            this.title = title;
        }
    }

    /**
     * Превращение мапы код->значение в строку для добавления в ключ.
     * [[код1]:[значение1];[код2]:[значение2]...]
     * Мапа должна быть отсортирована!
     */
    private static boolean appendMap(StrBuilder str, Map<String, Object> map)
    {
        if (map.isEmpty()) {
            return false;
        }
        final Iterator<Map.Entry<String, Object>> iterator = map.entrySet().iterator();
        str.append('[');
        while (iterator.hasNext())
        {
            final Map.Entry<String, Object> entry = iterator.next();
            str.append(entry.getKey());
            str.append(':');
            str.append(entry.getValue());
            if (iterator.hasNext()) {
                str.append(';');
            }
        }
        str.append(']');
        return true;
    }

    /**
     * Дописывание произвольных объектов в ключ.
     * Делается toString(), разделитель точка.
     */
    private static boolean appendObjects(StrBuilder str, Collection<Object> objects)
    {
        final Iterator<Object> iterator = objects.iterator();
        while (iterator.hasNext())
        {
            str.append(iterator.next().toString());
            if (iterator.hasNext()) {
                str.append('.');
            }
        }
        return !objects.isEmpty();
    }

    /**
     * Дописываение нагрузок, значение которых у враппера больше нуля.
     * Записывается в целом виде:
     * [[код_вида_нагрузки]:[значение]]
     */
    private static boolean appendLoads(StrBuilder str, Collection<String> typeFullCodes, IEppBaseRegElWrapper wrapper)
    {
        final Map<String, Object> loadMap = new TreeMap<>();
        for (String code : typeFullCodes)
        {
            double value = wrapper.getLoadAsDouble(code);
            if (value > 0.0d) {
                final Long size = UniEppUtils.unwrap(value);
                loadMap.put(code, size);
            }
        }
        return appendMap(str, loadMap);
    }

    /**
     * Убираем точку в конце
     */
    private static void cutLastDot(StrBuilder str)
    {
        if (str.endsWith(".")) {
            str.deleteCharAt(str.size() - 1);
        }
    }

    /**
     * Ключ модуля:
     * [номер].[признак_разделяемости].[идентификатор_подразделения].[[код_нагрузки_1:размер_нагрузки_1];[код_нагрузки_1:размер_нагрузки_2]...].[[код_тек_контроля_1:количество];[код_тек_контроля_2:количество]...]
     * Нагрузки сортируются по коду, формы текущего контроля - тоже по коду.
     * Текущий контроль добавляется только если укзазано соответствующее правило генерации ключа.
     */
    private static void appendModule(StrBuilder str, IEppRegElPartModuleWrapper wrapper, KeyGenRule rule)
    {
        appendObjects(str, ImmutableList.<Object>of(
                wrapper.getNumber(),
                wrapper.shared() ? "1" : "0",
                wrapper.getModuleOwner().getId()
        ));

        str.append('.');

        // Нагрузки (пока поддерживаются только аудиторные)
        if (appendLoads(str, EppALoadType.FULL_CODES, wrapper)) {
            str.append('.');
        }

        // Формы текущего контроля
        if (rule != KeyGenRule.FULL_WITHOUT_I_CONTROL) {
            final Map<String, Object> actionMap = new TreeMap<>();
            for (Map.Entry<String, EppControlActionType> entry : EppEduPlanVersionDataDAO.getActionTypeMap().entrySet())
            {
                if (entry.getValue() instanceof EppIControlActionType)
                {
                    final String actionFullCode = entry.getKey();
                    int size = wrapper.getActionSize(actionFullCode);
                    if (size > 0) {
                        actionMap.put(actionFullCode, size);
                    }
                }
            }

            if (appendMap(str, actionMap)) {
                str.append('.');
            }
        }

        cutLastDot(str);
    }

    /**
     * Ключ части элемента реестра:
     * [номер].[код_нагрузки_1:размер_нагрузки_2]...].[[ФИК_1:код_шкалы_оценки];[ФИК_2:код_шкалы_оценки]...].[[ключ_модуля_1];[клуч_модуля_2]...]
     * Формы контроля сортируются по коду, модули - по номеру.
     */
    private static void appendPart(StrBuilder str, IEppRegElPartWrapper wrapper, KeyGenRule rule)
    {
        str.append(wrapper.getNumber());
        str.append('.');

        // Нагрузки
        if (appendLoads(str, IEppRegElPartWrapper.PART_LOAD_CODES, wrapper)) {
            str.append('.');
        }

        // Формы контроля и шкалы оценок
        final Map<String, Object> actionMap = new TreeMap<>();
        for (Map.Entry<String, EppControlActionType> entry : EppEduPlanVersionDataDAO.getActionTypeMap().entrySet())
        {
            if (entry.getValue() instanceof EppFControlActionType)
            {
                final String actionFullCode = entry.getKey();
                int size = wrapper.getActionSize(actionFullCode);
                if (size > 0) {
                    String gradeScaleCode = wrapper.getGradeScale(actionFullCode);
                    if (gradeScaleCode == null) {
                        gradeScaleCode = ((EppFControlActionType) entry.getValue()).getDefaultGradeScale().getCode();
                    }
                    actionMap.put(actionFullCode, gradeScaleCode);
                }
            }
        }

        if (appendMap(str, actionMap)) {
            str.append('.');
        }

        // Модули
        final Collection<IEppRegElPartModuleWrapper> modules = wrapper.getModuleMap().values();
        if (!modules.isEmpty()) {
            str.append('[');
            final Iterator<IEppRegElPartModuleWrapper> iterator = modules.iterator();
            while (iterator.hasNext())
            {
                appendModule(str, iterator.next(), rule);
                if (iterator.hasNext()) {
                    str.append(';');
                }
            }
            str.append(']');
        }

        cutLastDot(str);
    }

    /**
     * Ключ элемента реестра:
     * [код_типа].[нагрузка].[трудоемкость].[число_недель(для дисциплин всегда 0)].[число_частей].[[код_нагрузки_1:размер_нагрузки_1];[код_нагрузки_1:размер_нагрузки_2]...].[[ключ_части_1];[ключ_части_2]...]
     * Нагрузки сортируются по коду, части - по номеру.
     * Для элементов, где указываются коды справочников (нагрузки, формы тек. контроля), если значение равно нулю, то значение не пишется (нагрузки нет).
     */
    public static String generate(IEppRegElWrapper wrapper, KeyGenRule rule)
    {
        final StrBuilder str = new StrBuilder(512);
        final Collection<IEppRegElPartWrapper> parts = wrapper.getPartMap().values();

        // [код_типа].[нагрузка].[трудоемкость].[число_недель(для дисциплин всегда 0)].[число_частей]
        appendObjects(str, ImmutableList.<Object>of(
                wrapper.getRegistryElementType().getCode(),
                wrapper.getLoadSize(),
                wrapper.getLabor(),
                wrapper.getWeeks(),
                parts.size()
        ));

        str.append('.');

        // Общие нагрузки
        if (appendLoads(str, EppEduPlanVersionDataDAO.getLoadTypeMap().keySet(), wrapper)) {
            str.append('.');
        }

        // Части
        if (!parts.isEmpty())
        {
            str.append('[');
            final Iterator<IEppRegElPartWrapper> iterator = parts.iterator();
            while (iterator.hasNext())
            {
                appendPart(str, iterator.next(), rule);
                if (iterator.hasNext()) {
                    str.append(';');
                }
            }
            str.append(']');
        }

        cutLastDot(str);
        return str.toString();
    }

}