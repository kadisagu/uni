/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.EduPlansWithoutWorkPlans;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanAdditionalProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanSecondaryProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/17/14
 */
public class EppEduPlanWithoutWorkplanDSHandler extends DefaultSearchDataSourceHandler
{
    public EppEduPlanWithoutWorkplanDSHandler(String ownerID)
    {
        super(ownerID);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {

        DQLOrderDescriptionRegistry orderDescription = new DQLOrderDescriptionRegistry(EppEduPlanVersion.class, "v");
        DQLSelectBuilder builder = orderDescription.buildDQLSelectBuilder().column("v");
        builder.joinPath(DQLJoinType.inner, EppEduPlanVersion.eduPlan().fromAlias("v"), "p");

        IDataSettings settings = context.get("settings");

        FilterUtils.applySelectFilter(builder, "p", EppEduPlan.owner(), context.get("orgUnit"));
        FilterUtils.applySelectFilter(builder, "p", EppEduPlan.programKind(), settings.get("programKind"));
        FilterUtils.applySelectFilter(builder, "p", EppEduPlanSecondaryProf.programSubject(), settings.get("programSubject"));
        FilterUtils.applySelectFilter(builder, "p", EppEduPlanAdditionalProf.programForm(), settings.get("programForm"));
        FilterUtils.applySelectFilter(builder, "p", EppEduPlanAdditionalProf.developCondition(), settings.get("developCondition"));
        FilterUtils.applySelectFilter(builder, "p", EppEduPlanAdditionalProf.programTrait(), settings.get("programTrait"));

        builder.where(eq(property("v", EppEduPlanVersion.state().code()), value(EppState.STATE_ACCEPTED)));

        EppYearEducationProcess year = settings.get("yearEducationProcess");
        YearDistributionPart part = settings.get("yearPart");
        Boolean onlyAccepted = settings.get("onlyAccepted");

        if (null == year || null == part) {
            builder.where(isNull("v.id"));
        } else {
            DQLSelectBuilder wpDql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlan.class, "wp")
                .where(eq(property("wp", EppWorkPlan.parent().eduPlanVersion()), property("v")))
                .where(eq(property("wp", EppWorkPlan.year()), value(year)))
                .where(eq(property("wp", EppWorkPlan.cachedGridTerm().part()), value(part)))
                ;

            if (onlyAccepted) {
                wpDql.where(eq(property("wp", EppWorkPlan.state().code()), value(EppState.STATE_ACCEPTED)));
            }

            builder.where(notExists(wpDql.buildQuery()));
        }

        orderDescription.applyOrder(builder, input.getEntityOrder());

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }
}


