package ru.tandemservice.uniepp.dao.registry;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.functors.NotNullPredicate;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IRelationMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.statement.DQLTempDataSourceBuilder;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.EntityRenumerator;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;
import org.tandemframework.shared.commonbase.events.CommonbaseImmutableNaturalIdListener;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.base.bo.EppRegistry.EppRegistryManager;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.data.*;
import ru.tandemservice.uniepp.dao.registry.data.EppRegElementKeyGenerator.KeyGenRule;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppModuleStructureCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementLoadGen;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementPartGen;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author vdanilov
 */
public class EppRegistryDAO extends UniBaseDao implements IEppRegistryDAO {

    private static final String DAO_SIMPLE_NAME = EppRegistryDAO.class.getSimpleName();

    @Override
    @SuppressWarnings("unchecked")
    public SimpleNumberGenerationRule<EppRegistryElement> getNumberGenerationRule()
    {
        return new SimpleNumberGenerationRule<EppRegistryElement>() {
            @Override public Set<String> getUsedNumbers(final EppRegistryElement object) {
                final List list = EppRegistryDAO.this.getSession().createCriteria(ClassUtils.getUserClass(object)).setProjection(Projections.property(EppRegistryElement.number().toString())).list();
                return new HashSet(list);
            }
            @Override public String getNumberQueueName(final EppRegistryElement object) {
                return ClassUtils.getUserClass(object).getSimpleName();
            }
        };
    }

    @Override
    public void doSaveRegistryElement(final EppRegistryElement element)
    {
        doSaveRegistryElement(element, null);
    }

    @Override
    public void doSaveRegistryElement(EppRegistryElement element, List<EppGroupType> eduGroupTypes) {

        final Session session = this.lock(element);

        session.saveOrUpdate(element);

        if (element.getEduGroupSplitVariant() == null) {
            element.setEduGroupSplitByType(false);
            removeRegElementSplitByTypeEduGroupRel(element);
        }

        if(element.isEduGroupSplitByType()){
            createRegElementSplitByTypeEduGroupRel(element,eduGroupTypes);
        } else {
            removeRegElementSplitByTypeEduGroupRel(element);
        }

        final int parts = element.getParts();

        // TODO: check in ui (create settings )
        if (parts < 0) { throw new IllegalStateException("parts-is-negative"); }

        int maxParts;
        try {
            maxParts = Integer.parseInt(EppRegistryManager.instance().getProperty("domain.restriction.max-parts"));
        } catch (NumberFormatException e) {
            throw new ApplicationException("Некорректно задано ограничение на число частей элемента реестра, обратитесь к администратору системы.");
        }
        if (parts > maxParts) { throw new ApplicationException(EppRegistryManager.instance().getProperty("ui.error.text.too-many-parts", maxParts)); }

        for (int part=0; part<parts; part++) {
            this.doGetRegistryElementPart(element, 1+part);
        }

        // нельзя этого делать // this.doRecalculateRegistryElementNumbers(element);
    }

    private void removeRegElementSplitByTypeEduGroupRel(EppRegistryElement element) {
        if (element.getId() == null) {
            return;
        }

        // C DQLDeleteBuilder возникали какие-то проблемы
        new DQLSelectBuilder()
                .fromEntity(EppRegElementSplitByTypeEduGroupRel.class, "rel")
                .where(eq(property("rel", EppRegElementSplitByTypeEduGroupRel.registryElement()), value(element)))
                .createStatement(getSession()).<IEntity>list()
                .forEach(l -> getSession().delete(l));
    }


    private void createRegElementSplitByTypeEduGroupRel(EppRegistryElement element, List<EppGroupType> eduGroupTypes) {

        List<EppRegElementSplitByTypeEduGroupRel> databaseRecords = this.getList(EppRegElementSplitByTypeEduGroupRel.class, EppRegElementSplitByTypeEduGroupRel.registryElement(), element);
        List<EppRegElementSplitByTypeEduGroupRel> targetRecords = eduGroupTypes == null ? Collections.emptyList() : eduGroupTypes.stream()
                .map(type -> new EppRegElementSplitByTypeEduGroupRel(element, type))
                .collect(Collectors.toList());

        new MergeAction.SessionMergeAction<EppRegElementSplitByTypeEduGroupRel.NaturalId, EppRegElementSplitByTypeEduGroupRel>() {
            @Override
            protected EppRegElementSplitByTypeEduGroupRel.NaturalId key(EppRegElementSplitByTypeEduGroupRel source) {
                return new EppRegElementSplitByTypeEduGroupRel.NaturalId(source.getRegistryElement(), source.getGroupType());
            }
            @Override
            protected EppRegElementSplitByTypeEduGroupRel buildRow(EppRegElementSplitByTypeEduGroupRel source) {
                return new EppRegElementSplitByTypeEduGroupRel(element, source.getGroupType());
            }
            @Override
            protected void fill(EppRegElementSplitByTypeEduGroupRel target, EppRegElementSplitByTypeEduGroupRel source) {
                target.update(source, false);
            }
        }.merge(databaseRecords, targetRecords);
    }

    @Override
    public EppRegistryElementPart doGetRegistryElementPart(final EppRegistryElement registryElement, final int number)
    {
        this.lock(registryElement);
        EppRegistryElementPart part = this.getByNaturalId(new EppRegistryElementPartGen.NaturalId(registryElement, number));
        if (null == part) {
            registryElement.getState().check_editable(registryElement);
            this.doSaveRegistryElementPart(part = new EppRegistryElementPart(registryElement, number));
        }
        return part;
    }


    @Override
    public Map<Long, Map<String, EppRegistryElementLoad>> getRegistryElementTotalLoadMap(final Collection<Long> disciplineIds)
    {
        final Map<Long, Map<String, EppRegistryElementLoad>> result = SafeMap.get(HashMap.class);
        BatchUtils.executeNullable(disciplineIds, 128, ids -> {
            final MQBuilder builder = new MQBuilder(EppRegistryElementLoadGen.ENTITY_CLASS, "load");
            if (null != ids) { builder.add(MQExpression.in("load", EppRegistryElementLoadGen.registryElement().id(), ids)); }
            for (final EppRegistryElementLoad load: builder.<EppRegistryElementLoad>getResultList(EppRegistryDAO.this.getSession())) {
                result.get(load.getRegistryElement().getId()).put(load.getLoadType().getFullCode(), load);
            }
        });
        return result;
    }

    protected void validateRegistryElementTotalLoadMap(final Map<Long, Map<String, EppRegistryElementLoad>> loadMap)
    {
        final Session session = this.getSession();
        final List<EppLoadType> loadTypes = this.getList(EppLoadType.class);

        for (final Map.Entry<Long, Map<String, EppRegistryElementLoad>> regElEntry: loadMap.entrySet()) {
            final EppRegistryElement regEl = (EppRegistryElement) session.load(EppRegistryElement.class, regElEntry.getKey());
            for (final EppLoadType loadType: loadTypes) {
                final EppRegistryElementLoad load = regElEntry.getValue().get(loadType.getFullCode());
                if ((null != load) && (null == load.getId())) {
                    load.setLoadType(loadType);
                    load.setRegistryElement(regEl);
                }
            }

            for (final Map.Entry<String, EppRegistryElementLoad> regElLoadEntry: regElEntry.getValue().entrySet()) {
                final EppRegistryElementLoad newLoad = regElLoadEntry.getValue();
                if (null == newLoad) { continue; /* no-value */ }

                final String code = newLoad.getLoadType().getFullCode();
                if (!regElLoadEntry.getKey().equals(code)) { throw new IllegalArgumentException("EppRegistryElementLoad("+newLoad.getId()+").load("+code+") != "+regElLoadEntry.getKey()); }

                final Long regElId = newLoad.getRegistryElement().getId();
                if (!regElEntry.getKey().equals(regElId)) { throw new IllegalArgumentException("EppRegistryElementLoad("+newLoad.getId()+").dsc("+regElId+") != "+regElEntry.getKey()); }
            }
        }
    }

    @Override
    public void saveRegistryElementTotalLoadMap(final Map<Long, Map<String, EppRegistryElementLoad>> newMap)
    {
        this.validateRegistryElementTotalLoadMap(newMap);
        final Session session = this.lock("eppRegistryElementLoadMap");

        final Map<Long, Map<String, EppRegistryElementLoad>> currentMap = this.getRegistryElementTotalLoadMap(newMap.keySet());

        // обновляем существующие
        for (final Map.Entry<Long, Map<String, EppRegistryElementLoad>> currentRegElEntry: currentMap.entrySet()) {
            final Map<String, EppRegistryElementLoad> currentRegElMap = currentRegElEntry.getValue();
            final Map<String, EppRegistryElementLoad> newRegElMap = newMap.remove(currentRegElEntry.getKey());

            for (final Map.Entry<String, EppRegistryElementLoad> newRegElLoadEntry: newRegElMap.entrySet()) {
                final EppRegistryElementLoad newLoad = newRegElLoadEntry.getValue();
                if (null == newLoad) { continue; /* no-value */ }
                if (newLoad.getLoad() < 0) { continue; /* no-value */ }

                EppRegistryElementLoad dbLoad = currentRegElMap.remove(newRegElLoadEntry.getKey());
                if (null == dbLoad) { dbLoad = new EppRegistryElementLoad(); }
                dbLoad.update(newLoad);
                session.saveOrUpdate(dbLoad);
            }

            for (final Map.Entry<String, EppRegistryElementLoad> currentRegElLoadEntry: currentRegElMap.entrySet()) {
                session.delete(currentRegElLoadEntry.getValue());
            }
        }

        // все удаления должны попасть в базу ДО добавлений
        session.flush();

        // создаем новые
        for (final Map.Entry<Long, Map<String, EppRegistryElementLoad>> newRegElEntry: newMap.entrySet()) {
            final Map<String, EppRegistryElementLoad> newRegElMap = newRegElEntry.getValue();
            for (final Map.Entry<String, EppRegistryElementLoad> newRegElLoadEntry: newRegElMap.entrySet()) {
                if (newRegElLoadEntry.getValue().getLoad() < 0) { continue; /* no-value */ }

                final EppRegistryElementLoad load = new EppRegistryElementLoad();
                load.update(newRegElLoadEntry.getValue());
                session.saveOrUpdate(load);
            }
        }

        session.flush();
    }


    @Override
    public List<EppRegistryStructure> getParentItems4Class(final Class<? extends EppRegistryElement> klass)
    {
        final String topElementCode = null == klass ? null : klass.getSimpleName();

        final List<EppRegistryStructure> items = this.getCatalogItemListOrderByCode(EppRegistryStructure.class);
        final List<EppRegistryStructure> result = new ArrayList<>(items.size() / 2);
        for (final EppRegistryStructure item: items) {
            EppRegistryStructure top = item;
            if (null == topElementCode) {
                result.add(item);
            } else {
                while (null != top.getParent()) { top = top.getParent(); }
                if (topElementCode.equalsIgnoreCase(top.getCode()))
                {
                    if (item.getCode().equals(EppRegistryStructureCodes.REGISTRY_ATTESTATION)) continue;
                    result.add(item);
                }
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public static <T extends IEntity> List<ViewWrapper<T>> fillRegistryElementWrappers(final List<ViewWrapper<T>> registryElementWrappers)
    {
        if (registryElementWrappers.isEmpty()) { return registryElementWrappers; }

        final Map<Long, Map<String, EppRegistryElementLoad>> loadMap = IEppRegistryDAO.instance.get().getRegistryElementTotalLoadMap(UniBaseDao.ids(registryElementWrappers));
        final EppLoadFormatter<EppRegistryElement> formatter = new EppLoadFormatter<EppRegistryElement>() {
            @Override protected Double load(final EppRegistryElement element, final String loadFullCode) {

                if (EppLoadType.FULL_CODE_TOTAL_HOURS.equals(loadFullCode)) {
                    return element.getSizeAsDouble();
                }

                if (EppLoadType.FULL_CODE_LABOR.equals(loadFullCode)) {
                    return element.getLaborAsDouble();
                }

                if (EppLoadType.FULL_CODE_WEEKS.equals(loadFullCode) && element instanceof EppRegistryPractice) {
                    return ((EppRegistryPractice)element).getWeeksAsDouble();
                }

                final Map<String, EppRegistryElementLoad> rowLoadMap = loadMap.get(element.getId());
                if (null == rowLoadMap) { return null; }
                final EppRegistryElementLoad load = rowLoadMap.get(loadFullCode);
                return (null == load ? null : load.getLoadAsDouble());
            }
        };

        for (final ViewWrapper wrapper: registryElementWrappers) {
            final IEntity entity = wrapper.getEntity();
            wrapper.setViewProperty("load", (entity instanceof EppRegistryElement) ? formatter.format((EppRegistryElement)entity) : "");
        }
        return registryElementWrappers;
    }

    @Override
    public void doSaveRegistryElementPart(final EppRegistryElementPart element)
    {
        final Session session = this.lock(element.getRegistryElement());
        session.saveOrUpdate(element);
        // НЕЛЬЗЯ ЭТОГО ДЕЛАТЬ // this.doRecalculateRegistryElementNumbers(element.getRegistryElement());
    }

    @Override
    public Map<EppFControlActionType, EppRegistryElementPartFControlAction> getRegistryElementPartControlActions(final EppRegistryElementPart element) {
        final List<EppRegistryElementPartFControlAction> actions = this.getList(EppRegistryElementPartFControlAction.class, EppRegistryElementPartFControlAction.part(), element);
        final Map<EppFControlActionType, EppRegistryElementPartFControlAction> result = new HashMap<>(actions.size());
        for (final EppRegistryElementPartFControlAction a: actions) {
            if (null != result.put(a.getControlAction(), a)) {
                throw new IllegalStateException("duplicate finalControlActionType for eppRegistryElementPartFControlAction");
            }
        }
        return result;
    }

    @Override
    public Map<String, Integer> getRegistryElementControlActionCountByAbbreviation(final EppRegistryElement element) {
        List<Object[]> list = new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, "ca")
                .column(property(EppRegistryElementPartFControlAction.controlAction().abbreviation().fromAlias("ca")))
                .column(count(property(EppRegistryElementPartFControlAction.controlAction().abbreviation().fromAlias("ca"))))
                .column(property(EppRegistryElementPartFControlAction.controlAction().priority().fromAlias("ca")), "pri")
                .where(eq(property(EppRegistryElementPartFControlAction.part().registryElement().fromAlias("ca")), value(element)))
                .group(property(EppRegistryElementPartFControlAction.controlAction().abbreviation().fromAlias("ca")))
                .group(property(EppRegistryElementPartFControlAction.controlAction().priority().fromAlias("ca")))
                .order("pri")
                .createStatement(getSession()).list();

        Map<String, Integer> result = new LinkedHashMap<>();
        for (Object[] item: list)
        {
            result.put(item[0].toString(), ((Long)item[1]).intValue());
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EppRegistryElementPartFControlAction> doSaveRegistryElementPartControlActions(final EppRegistryElementPart element, final Collection<EppRegistryElementPartFControlAction> actions)
    {
        this.lock(element.getRegistryElement());
        return new MergeAction.SessionMergeAction<EppFControlActionType, EppRegistryElementPartFControlAction>() {
            @Override protected EppFControlActionType key(final EppRegistryElementPartFControlAction source) {
                return source.getControlAction();
            }
            @Override protected void fill(final EppRegistryElementPartFControlAction target, final EppRegistryElementPartFControlAction source) {
                target.update(source, false);
            }
            @Override protected EppRegistryElementPartFControlAction buildRow(final EppRegistryElementPartFControlAction source) {
                return new EppRegistryElementPartFControlAction(element, source.getControlAction());
            }
        }.merge(
            this.getRegistryElementPartControlActions(element).values(),
            CollectionUtils.select(actions, NotNullPredicate.getInstance())
        );
    }


    /////////////////////////////////////////////////

    private final DaoCacheFacade.CacheEntryDefinition<Long, IEppRegElWrapper> CACHE_REGEL_DATA = new DaoCacheFacade.CacheEntryDefinition<Long, IEppRegElWrapper>(
    EppRegistryDAO.DAO_SIMPLE_NAME+".regElData", 128
    ) {
        @Override public void fill(final Map<Long, IEppRegElWrapper> cache, final Collection<Long> registryElementIdSlice)
        {
            final Session session = EppRegistryDAO.this.getSession();

            // грузим данные элементов реестра и их нагрузки
            final List<EppRegistryElementLoad> loads = EppRegistryDAO.this.getList(EppRegistryElementLoad.class, EppRegistryElementLoad.registryElement().id(), registryElementIdSlice);
            final Map<Long, Map<String, Long>> loadMap = SafeMap.get(key -> new HashMap<>());
            for (EppRegistryElementLoad load : loads) {
                loadMap.get(load.getRegistryElement().getId()).put(load.getLoadType().getFullCode(), load.getLoad());
            }
            final List<EppRegistryElement> relElList = EppRegistryDAO.this.getList(EppRegistryElement.class, EppRegistryElement.id(), registryElementIdSlice);
            final Map<Long, EppRegElWrapper> regElWrappers = new HashMap<>(relElList.size());
            for (final EppRegistryElement regEl: relElList) {
                final List<EppGroupType> groupTypes = getRegElementSplitGroupTypes(regEl);
                final EppRegElWrapper wrapper = EppRegistryDAO.this.buildEppRegElWrapper(regEl, loadMap.get(regEl.getId()), groupTypes);
                if (null != regElWrappers.put(wrapper.getId(), wrapper)) { throw new IllegalStateException(); }
            }

            // грузим по частям данные
            final Map<Long, IEppRegElPartWrapper> regElPartsWrappers = EppRegistryDAO.this.getRegistryElementPartDataMap(
                new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPart.class, "x").column(property("x.id"))
                .where(in(EppRegistryElementPart.registryElement().id().fromAlias("x"), registryElementIdSlice))
                .createStatement(session).<Long>list()
            );

            // сохраняем данные в элементах реестра
            for (final IEppRegElPartWrapper wrapper: regElPartsWrappers.values()) {
                final Map<Integer, IEppRegElPartWrapper> partMap = regElWrappers.get(wrapper.getItem().getRegistryElement().getId()).getPartMap();
                final int number = wrapper.getItem().getNumber();
                if (number <= 0) { continue; /* error */ }
                if (null != partMap.put(number, wrapper)) { throw new IllegalStateException(); }
            }

            // сохраняем данные в кэш
            cache.putAll(regElWrappers);
        }
    };

    @Override
    @SuppressWarnings("unchecked")
    public Map<Long, IEppRegElWrapper> getRegistryElementDataMap(final Collection<Long> registryElementIds)
    {
        Debug.begin("EppRegistryDAO.getRegistryElementDataMap(size=" + registryElementIds.size() + ")");
        try {
            return DaoCacheFacade.getEntry(this.CACHE_REGEL_DATA).getRecords(registryElementIds);
        } finally {
            Debug.end();
        }
    }


    /////////////////////////////////////////////////

    private final DaoCacheFacade.CacheEntryDefinition<Long, IEppRegElPartWrapper> CACHE_REGEL_PART_DATA = new DaoCacheFacade.CacheEntryDefinition<Long, IEppRegElPartWrapper>(
    EppRegistryDAO.DAO_SIMPLE_NAME+".regElPartData", 128
    ) {
        @Override public void fill(final Map<Long, IEppRegElPartWrapper> cache, final Collection<Long> registryElementPartIdSlice)
        {
            // грузим части элементов реестра
            final List<EppRegistryElementPart> regElPartList = EppRegistryDAO.this.getList(EppRegistryElementPart.class, EppRegistryElementPart.id(), registryElementPartIdSlice);
            final Map<Long, EppRegElPartWrapper> regElPartsWrappers = new HashMap<>(regElPartList.size());
            for (final EppRegistryElementPart regElPart: regElPartList) {
                final EppRegElPartWrapper wrapper = EppRegistryDAO.this.buildEppRegElPartWrapper(regElPart);
                if (null != regElPartsWrappers.put(wrapper.getId(), wrapper)) { throw new IllegalStateException(); }

                // данные по нагрузке из самих элементов
                wrapper.getLocalLoadMap().put(EppLoadType.FULL_CODE_TOTAL_HOURS, regElPart.getSize());
                wrapper.getLocalLoadMap().put(EppLoadType.FULL_CODE_LABOR, regElPart.getLabor());
                wrapper.getLocalLoadMap().put(EppLoadType.FULL_CODE_WEEKS, regElPart.getWeeks());
            }

            // сохраняем обработанные данные в кэш
            cache.putAll(EppRegistryDAO.this.fillRegElPartsWrappers(regElPartsWrappers));
        }
    };

    @Override
    @SuppressWarnings("unchecked")
    public Map<Long, IEppRegElPartWrapper> getRegistryElementPartDataMap(final Collection<Long> registryElementPartIds)
    {
        Debug.begin("EppRegistryDAO.getRegistryElementPartDataMap(size=" + registryElementPartIds.size() + ")");
        try {
            return DaoCacheFacade.getEntry(this.CACHE_REGEL_PART_DATA).getRecords(registryElementPartIds);
        } finally {
            Debug.end();
        }
    }

    protected final Map<Long, EppRegElPartWrapper> fillRegElPartsWrappers(final Map<Long, EppRegElPartWrapper> regElPartsWrappers)
    {
        final Session session = this.getSession();
        for (List<Long> partIds : Iterables.partition(regElPartsWrappers.keySet(), 256)) {

            // грузим итоговый контроль для частей реестра
            List<EppRegistryElementPartFControlAction> fControlActionList = EppRegistryDAO.this.getList(EppRegistryElementPartFControlAction.class, EppRegistryElementPartFControlAction.part().id(), partIds);
            for (final EppRegistryElementPartFControlAction c: fControlActionList) {
                regElPartsWrappers.get(c.getPart().getId()).process(c);
            }

            // грузим модули
            final List<EppRegistryElementPartModule> regElPartModuleList = EppRegistryDAO.this.getList(EppRegistryElementPartModule.class, EppRegistryElementPartModule.part().id(), partIds);
            final Map<Long, EppRegElPartModuleWrapper> eppRegElPartModuleWrappers = new HashMap<>(regElPartModuleList.size());
            for (final EppRegistryElementPartModule m: regElPartModuleList)
            {
                // создаем оболочку
                final EppRegElPartModuleWrapper wrapper = EppRegistryDAO.this.buildEppRegElPartModuleWrapper(m);
                if (null != eppRegElPartModuleWrappers.put(wrapper.getId(), wrapper)) { throw new IllegalStateException(); }

                // регистрируем оболочку в элементе реестра
                final EppRegElPartWrapper eppRegElPartWrapper = regElPartsWrappers.get(wrapper.getItem().getPart().getId());
                final Map<Integer, IEppRegElPartModuleWrapper> moduleMap = eppRegElPartWrapper.getModuleMap();
                if (null != moduleMap.put(wrapper.getItem().getNumber(), wrapper)) { throw new IllegalStateException(); }
            }

            // грузим текущий контроль по модулям
            {
                final List<Object[]> rows = new DQLSelectBuilder()
                        .fromEntity(EppRegistryElementPartModule.class, "pm").column(property("pm.id"))
                        .where(in(property(EppRegistryElementPartModule.part().id().fromAlias("pm")), partIds))
                        .fromEntity(EppRegistryModuleIControlAction.class, "ml").column(property("ml"))
                        .where(eq(property(EppRegistryModuleIControlAction.module().fromAlias("ml")), property(EppRegistryElementPartModule.module().fromAlias("pm"))))
                        .createStatement(session).list();
                for (final Object[] row: rows)
                {
                    final EppRegistryModuleIControlAction a = (EppRegistryModuleIControlAction)row[1];
                    final EppRegElPartModuleWrapper eppRegElPartModuleWrapper = eppRegElPartModuleWrappers.get((Long) row[0]);
                    final Map<String, Long> loadMap = eppRegElPartModuleWrapper.getLocalLoadMap();
                    if (null != loadMap.put(a.getControlAction().getFullCode(), (long) a.getAmount())) { throw new IllegalStateException(); }
                }
            }

            // грузим аудиторную нагрузку по модулям
            {
                final List<Object[]> rows = new DQLSelectBuilder()
                        .fromEntity(EppRegistryElementPartModule.class, "pm").column(property("pm.id"))
                        .where(in(property(EppRegistryElementPartModule.part().id().fromAlias("pm")), partIds))
                        .fromEntity(EppRegistryModuleALoad.class, "ml").column(property("ml"))
                        .where(eq(property(EppRegistryModuleALoad.module().fromAlias("ml")), property(EppRegistryElementPartModule.module().fromAlias("pm"))))
                        .createStatement(session).list();
                for (final Object[] row: rows)
                {
                    final EppRegistryModuleALoad a = (EppRegistryModuleALoad)row[1];
                    final EppRegElPartModuleWrapper eppRegElPartModuleWrapper = eppRegElPartModuleWrappers.get((Long) row[0]);
                    final Map<String, Long> loadMap = eppRegElPartModuleWrapper.getLocalLoadMap();
                    if (null != loadMap.put(a.getLoadType().getFullCode(), a.getLoad())) { throw new IllegalStateException(); }
                }
            }
        }

        return regElPartsWrappers;
    }


    protected EppRegElPartModuleWrapper buildEppRegElPartModuleWrapper(final EppRegistryElementPartModule m) {
        return new EppRegElPartModuleWrapper(m);
    }

    protected EppRegElPartWrapper buildEppRegElPartWrapper(final EppRegistryElementPart p) {
        return new EppRegElPartWrapper(p);
    }

    protected EppRegElWrapper buildEppRegElWrapper(final EppRegistryElement e, final Map<String, Long> loadMap, List<EppGroupType> splitGroupTypes) {
        return new EppRegElWrapper(e, loadMap, splitGroupTypes);
    }


    @Override
    public void doAddRegistryElementPartModule(final EppRegistryElementPart element, final EppRegistryModule module)
    {
        final Session session = this.lock(element.getRegistryElement());

        final Number max = new DQLSelectBuilder()
        .fromEntity(EppRegistryElementPartModule.class, "x")
        .column(DQLFunctions.max(property(EppRegistryElementPartModule.number().fromAlias("x"))))
        .where(eq(property(EppRegistryElementPartModule.part().fromAlias("x")), value(element)))
        .createStatement(session).uniqueResult();

        final EppRegistryElementPartModule object = new EppRegistryElementPartModule();
        object.setPart(element);
        object.setModule(module);
        object.setNumber(1 + (null == max ? 0 : max.intValue()));
        session.save(object);

        this.doRecalculateRegistryElementNumbers(element.getRegistryElement());
    }


    @Override
    public void doRecalculateRegistryElementNumbers(final EppRegistryElement registryElement)
    {
        final Session session = this.lock(registryElement);
        session.flush();

        final List<EppRegistryElementPart> parts = this.getList(
            EppRegistryElementPart.class,
            EppRegistryElementPart.registryElement(),
            registryElement,
            EppRegistryElementPart.P_NUMBER
        );

        int number = 1;
        for (final EppRegistryElementPart part: parts) {

            while (number < part.getNumber()) {
                this.doGetRegistryElementPart(registryElement, number);
                number++;
            }

            List<EppRegistryElementPartModule> modules = this.getList(
                EppRegistryElementPartModule.class,
                EppRegistryElementPartModule.part(),
                part,
                EppRegistryElementPartModule.P_NUMBER
            );

            new EntityRenumerator<EppRegistryElementPartModule>() {
                @Override protected int getNumber(final EppRegistryElementPartModule e) { return e.getNumber(); }
                @Override protected void setNumber(final EppRegistryElementPartModule e, final int number) { e.setNumber(number); session.update(e); session.flush(); }
            }.execute(modules);
        }

        session.flush();
    }

    @Override
    public EppRegistryElement doCreateRegistryElement(IEppRegElWrapper regElWrapper, OrgUnit newOwner, String newTitle, EppState newState)
    {
        // dao
        final IEppRegistryModuleDAO iEppRegistryModuleDAO = IEppRegistryModuleDAO.instance.get();
        final IEppRegistryDAO iEppRegistryDAO = IEppRegistryDAO.instance.get();

        // вспомогательные переменные
        final Map<String, EppLoadType> loadTypeMap = EppEduPlanVersionDataDAO.getLoadTypeMap();
        final Map<String, EppControlActionType> controlActionTypeMap = EppEduPlanVersionDataDAO.getActionTypeMap();
        final Map<String, Long> gradeScaleMap = this.getSystemCatalogIdMap(EppGradeScale.class);
        final EppState formingState = this.getCatalogItem(EppState.class, EppState.STATE_FORMATIVE);
        final EppModuleStructure moduleParentDefault = this.getCatalogItem(EppModuleStructure.class, EppModuleStructureCodes.BASE_MODULES);

        // перечень активных семестров (номера семестров, в которых есть нагрузка)
        final Map<Integer, IEppRegElPartWrapper> partMap = regElWrapper.getPartMap();

        // формируем элемент реестра требуемого типа
        final EppRegistryElement result;
        final Class klass = getRegistryElementClass(regElWrapper.getRegistryElementType());
        try {
            result = (EppRegistryElement) klass.newInstance();
        } catch (final Exception t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

        result.setTitle(newTitle == null ? regElWrapper.getTitle() : newTitle);
        result.setFullTitle(result.getTitle());
        result.setOwner(newOwner == null ? regElWrapper.getOwner() : newOwner);
        result.setParent(regElWrapper.getRegistryElementType());
        result.setSize(regElWrapper.getLoadSize());
        result.setLabor(regElWrapper.getLabor());
        if (result instanceof EppRegistryAction) {
            ((EppRegistryAction) result).setWeeks(regElWrapper.getWeeks());
        }
        result.setParts(partMap.size());
        result.setNumber(INumberQueueDAO.instance.get().getNextNumber(result));
        result.setState(formingState);
        result.setEduGroupSplitVariant(regElWrapper.getEduGroupSplitVariant());
        result.setEduGroupSplitByType(regElWrapper.isEduGroupSplitByType());
        iEppRegistryDAO.doSaveRegistryElement(result, regElWrapper.getSplitGroupTypes());

        // общая нагрузка для элемента реестра
        final Map<String, EppRegistryElementLoad> totalLoadMap = new HashMap<>();
        for (EppLoadType loadType: loadTypeMap.values())
        {
            final String fullCode = loadType.getFullCode();
            totalLoadMap.put(fullCode, new EppRegistryElementLoad(regElWrapper.getLoadAsDouble(fullCode)));
        }
        iEppRegistryDAO.saveRegistryElementTotalLoadMap(Collections.singletonMap(result.getId(), totalLoadMap));

        // нагрузка по частям
        for (Map.Entry<Integer, IEppRegElPartWrapper> partEntry : partMap.entrySet())
        {
            final int partNumber = partEntry.getKey();
            final IEppRegElPartWrapper partWrapper = partEntry.getValue();

            // берем часть и устанавливаем по ней нагрузку
            final EppRegistryElementPart relp = iEppRegistryDAO.doGetRegistryElementPart(result, partNumber);
            relp.setSizeAsDouble(partWrapper.getLoadAsDouble(EppLoadType.FULL_CODE_TOTAL_HOURS));
            relp.setLaborAsDouble(partWrapper.getLoadAsDouble(EppLoadType.FULL_CODE_LABOR));
            relp.setWeeksAsDouble(partWrapper.getLoadAsDouble(EppLoadType.FULL_CODE_WEEKS));
            this.saveOrUpdate(relp);

            // цепляем итоговые формы контроля
            final Collection<EppRegistryElementPartFControlAction> finalActions = new ArrayList<>();
            for (Map.Entry<String, EppControlActionType> actionTypeEntry : controlActionTypeMap.entrySet())
            {
                if (actionTypeEntry.getValue() instanceof EppFControlActionType) {
                    final int size = partWrapper.getActionSize(actionTypeEntry.getKey());
                    if (size > 0) {
                        final EppRegistryElementPartFControlAction newPartFCA = new EppRegistryElementPartFControlAction(relp, (EppFControlActionType) actionTypeEntry.getValue());
                        final String gradeScaleCode = partWrapper.getGradeScale(actionTypeEntry.getKey());
                        if (gradeScaleCode != null && !gradeScaleCode.equals(newPartFCA.getControlAction().getDefaultGradeScale().getCode())) {
                            newPartFCA.setGradeScale(EppRegistryDAO.<EppGradeScale>proxy(gradeScaleMap.get(gradeScaleCode)));
                        }
                        finalActions.add(newPartFCA);
                    }
                }
            }
            iEppRegistryDAO.doSaveRegistryElementPartControlActions(relp, finalActions);

            // создаем модули
            for (Map.Entry<Integer, IEppRegElPartModuleWrapper> moduleWrapperEntry : partWrapper.getModuleMap().entrySet())
            {
                final IEppRegElPartModuleWrapper moduleWrapper = moduleWrapperEntry.getValue();
                final EppRegistryModule mod;
                if (!moduleWrapper.shared())
                {
                    mod = new EppRegistryModule();
                    mod.setState(formingState);
                    final EppModuleStructure moduleParent = moduleWrapper.getModuleParent();
                    mod.setParent(moduleParent == null ? moduleParentDefault : moduleParent);
                    mod.setNumber(EppRegistryModuleDAO.getDefaultModuleNumber(relp));
                    mod.setTitle(EppRegistryModuleDAO.getDefaultModuleTitle(relp));
                    mod.setOwner(newOwner == null ? moduleWrapper.getModuleOwner() : newOwner);
                    mod.setShared(false);
                    this.save(mod);

                    // формы текущего контроля
                    final Collection<EppRegistryModuleIControlAction> currentActions = new ArrayList<>();
                    for (Map.Entry<String, EppControlActionType> actionTypeEntry : controlActionTypeMap.entrySet())
                    {
                        if (actionTypeEntry.getValue() instanceof EppIControlActionType) {
                            final int size = moduleWrapper.getActionSize(actionTypeEntry.getKey());
                            if (size > 0) {
                                currentActions.add(new EppRegistryModuleIControlAction(mod, (EppIControlActionType) actionTypeEntry.getValue(), size));
                            }
                        }
                    }
                    iEppRegistryModuleDAO.doSaveRegistryModuleControl(mod, currentActions);

                    // аудиторная нагрузка
                    final Collection<EppRegistryModuleALoad> moduleLoads = new ArrayList<>();
                    for (Map.Entry<String, EppLoadType> loadTypeEntry : loadTypeMap.entrySet())
                    {
                        if (loadTypeEntry.getValue() instanceof EppALoadType) {
                            final double size = moduleWrapper.getLoadAsDouble(loadTypeEntry.getKey());
                            if (size > 0d) {
                                moduleLoads.add(new EppRegistryModuleALoad(mod, (EppALoadType) loadTypeEntry.getValue(), size));
                            }
                        }
                    }
                    iEppRegistryModuleDAO.doSaveRegistryModuleLoad(mod, moduleLoads);
                }
                else
                {
                    // Разделяемый модуль просто прицепляем к части
                    mod = moduleWrapper.getItem().getModule();
                }

                // прицепляем модуль к части
                final EppRegistryElementPartModule rel = new EppRegistryElementPartModule();
                rel.setNumber(moduleWrapperEntry.getKey());
                rel.setModule(mod);
                rel.setPart(relp);
                this.save(rel);
            }
        }

        if (newState != null && !newState.equals(formingState)) {
            try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler())) {
                result.setState(newState);
                update(result);
                executeFlush();
            }
        }

        return result;
    }

    @Override
    public void doUpdateRegistryElement(Long registryElementId, IEppRegElWrapper regElWrapper)
    {
        // Реализовать, когда понадобится
        throw new RuntimeException("Method not implemented so far.");
    }

    /**
     * По типу элемента реестра (справочник структура элемента реестра) определяет соответствующий класс элемента реестра
     */
    protected Class getRegistryElementClass(EppRegistryStructure registryStructure)
    {
        Preconditions.checkNotNull(registryStructure);

        if (registryStructure.isDisciplineElement()) { return EppRegistryDiscipline.class; }
        if (registryStructure.isPracticeElement()) { return  EppRegistryPractice.class; }
        if (registryStructure.isAttestationElement()) { return  EppRegistryAttestation.class; }

        throw new IllegalStateException();
    }

    @SuppressWarnings("unchecked")
    public static IDQLExpression existLinksExpression(Class<? extends IEntity> clazz, String pathToEntity, boolean skipBackwardCascadeDelete)
    {
        final Collection<IRelationMeta> relations = EntityRuntime.getMeta(clazz).getIncomingRelations();

        if (relations.isEmpty()) {
            // Ссылок быть не может
            return nothing();
        }

        final List<IDQLExpression> existsExpressions = new ArrayList<>(relations.size());
        for (final IRelationMeta rel : relations)
        {
            if (skipBackwardCascadeDelete && "delete".equals(rel.getCascade().getStyle("delete", false))) {
                // Пропускаем ссылки с backward-cascade="delete", если нужно
                continue;
            }

            existsExpressions.add(
                    exists(rel.getForeignEntity().getEntityClass(),
                           rel.getForeignPropertyName(),
                           property(pathToEntity))
            );
        }
        return or(existsExpressions.toArray(new IDQLExpression[existsExpressions.size()]));

    }

    //TODO move to shared and write javadoc
    private abstract class MoveLinksAction
    {
        private final Class<? extends IEntity> _clazz;

        public MoveLinksAction(Class<? extends IEntity> сlazz)
        {
            _clazz = сlazz;
        }

        public void move(boolean skipBackwardCascadeDelete)
        {
            final List<IRelationMeta> relations = new ArrayList<>();
            for (final IRelationMeta rel : EntityRuntime.getMeta(this._clazz).getIncomingRelations())
            {
                if (skipBackwardCascadeDelete && "delete".equals(rel.getCascade().getStyle("delete", false))) {
                    continue;
                }
                relations.add(rel);
            }

            if (relations.isEmpty()) {
                // Ссылок нет или они все с каскадным удалением и их не нужно перекидывать (вложенные объекты)
                return;
            }

            // Создаем временную таблицу
            final DQLExecutionContext context = new DQLExecutionContext(getSession());
            final DQLTempDataSourceBuilder tdsBuilder = new DQLTempDataSourceBuilder(context, "tmp_t");
            tdsBuilder.select(getReplacementDataSource("src", "dest"));
            final int rows = tdsBuilder.execute();

            if (rows <= 0) {
                // Похоже, что нет ссылок, которые стоило бы переносить
                return;
            }

            // Проверим, что всем есть эквивалентная замена
            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity("tmp_t", "t");
            dql.top(1);
            dql.column(value(1));
            dql.where(isNull(property("t", "dest")));
            if (dql.createStatement(context).uniqueResult() != null) {
                missingReplacementException();
            }

            // Умышленно и осторожно обходим органичение на невозможность менять поля в составе натурального идентификатора
            // Если где-то сработает констрейнт - так тому и быть - пусть падает.
            try (EventListenerLocker.Lock ignored = CommonbaseImmutableNaturalIdListener.IMMUTABLE_NATURAL_ID_LOCKER.lock(EventListenerLocker.noopHandler()))
            {
                for (final IRelationMeta rel : relations)
                {
                    final DQLUpdateBuilder upd = new DQLUpdateBuilder(rel.getForeignEntity().getEntityClass());
                    upd.fromEntity("tmp_t", "tmp");
                    upd.set(rel.getForeignPropertyName(), property("tmp", "dest"));
                    upd.where(eq(property(rel.getForeignPropertyName()), property("tmp", "src")));
                    upd.createStatement(context).execute();
                }
            }

            context.close();
        }

        protected abstract IDQLSelectableQuery getReplacementDataSource(String srcIdColumn, String destIdColumn);
        protected abstract void missingReplacementException();
    }

    private void _doDuplicatesMerge(final Long templateId, final Collection<Long> duplicateIds)
    {
        final List<Long> eppModulesToDelete;

        // Модули частей элемента реестра
        {
            final MoveLinksAction modulesAction = new MoveLinksAction(EppRegistryElementPartModule.class) {
                @Override
                protected IDQLSelectableQuery getReplacementDataSource(String srcIdColumn, String destIdColumn) {
                    // Создаем датасорс с двумя колонками: [модуль части дубликата], [модуль части шаблонного элемента]
                    // Если для какого-то модуля дубля нет соответствующего модуля шаблонного элемента, то во второй колонке должен быть null.
                    // Поэтому вторая колонка сделана подзапросом, а не джойном. (эта беда из-за несделанной TF-89)
                    // Соответствующий модуль ищется по номеру части и номеру модуля.

                    final DQLSelectBuilder dql = new DQLSelectBuilder();
                    dql.fromEntity(EppRegistryElementPartModule.class, "src");

                    final DQLSelectBuilder subQuery = new DQLSelectBuilder().fromEntity(EppRegistryElementPartModule.class, "sub")
                            .column(property("sub.id"))
                            .where(eq(property("sub", EppRegistryElementPartModule.part().registryElement()), value(templateId)))
                            .where(eq(property("src", EppRegistryElementPartModule.part().number()), property("sub", EppRegistryElementPartModule.part().number())))
                            .where(eq(property("src", EppRegistryElementPartModule.number()), property("sub", EppRegistryElementPartModule.number())));

                    dql.column(property("src.id"), srcIdColumn);
                    dql.column(subQuery.buildQuery(), destIdColumn);

                    dql.where(in(property("src", EppRegistryElementPartModule.part().registryElement()), duplicateIds));
                    dql.where(existLinksExpression(EppRegistryElementPartModule.class, "src", true));

                    return dql.buildQuery();
                }

                @Override
                protected void missingReplacementException() {
                    throw new ApplicationException("Набор модулей элемента реестра, на основе которого производится объединение, недостаточен для объединения со всеми выбранными дублями.");
                }
            };

            modulesAction.move(true); // вложенных объектов пока нет, но скипаем из на всякий случай, если появятся

            // После всего нужно удалить учебные модули, на которые ссылаются только модули дублей частей реестра
            // Каких-то внешних ссылок на учебные модули пока нет. Если появятся надо перекидывать (причем как-то отличать внешнюю сылку от дочерних объектов - нагрузки перебрасывать не надо)
            final DQLSelectBuilder modulesDQL = new DQLSelectBuilder().fromEntity(EppRegistryModule.class, "m");
            modulesDQL.column(property("m.id"));
            modulesDQL.where(eq(property("m", EppRegistryModule.shared()), value(false))); // Разделяемые же не трогаем?
            modulesDQL.where(
                    exists(EppRegistryElementPartModule.class, // На это модуль ссылаютсятся дубли
                           EppRegistryElementPartModule.module().s(), property("m"),
                           EppRegistryElementPartModule.part().registryElement().s(), duplicateIds));
            modulesDQL.where(notExists(
                    new DQLSelectBuilder() // И на этот модуль не ссылаются какие-то другие элементы
                            .fromEntity(EppRegistryElementPartModule.class, "mp")
                            .where(eq(property("mp", EppRegistryElementPartModule.module()), property("m")))
                            .where(notIn(property("mp", EppRegistryElementPartModule.part().registryElement()), duplicateIds))
                            .buildQuery()
            ));
            eppModulesToDelete = modulesDQL.createStatement(getSession()).list();
        }


        // Формы контроля частей элемента реестра
        {
            final MoveLinksAction partFcaAction = new MoveLinksAction(EppRegistryElementPartFControlAction.class) {
                @Override
                protected IDQLSelectableQuery getReplacementDataSource(String srcIdColumn, String destIdColumn) {
                    // Создаем датасорс с двумя колонками: [форма итогового контроля (ФИК) части дубликата, на которую (ФИК) есть ссылки], [ФИК части шаблонного элемента]
                    // Если для какой-то ФИК дубля нет соответствующей ФИК шаблонного элемента, то во второй колонке должен быть null.
                    // Поэтому вторая колонка сделана подзапросом, а не джойном. (эта беда из-за несделанной TF-89)
                    // Соответствующая ФИК ищется по форме контроля и номеру части.

                    final DQLSelectBuilder dql = new DQLSelectBuilder();
                    dql.fromEntity(EppRegistryElementPartFControlAction.class, "src");

                    final DQLSelectBuilder subQuery = new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, "sub")
                            .column(property("sub.id"))
                            .where(eq(property("sub", EppRegistryElementPartFControlAction.part().registryElement()), value(templateId)))
                            .where(eq(property("src", EppRegistryElementPartFControlAction.part().number()), property("sub", EppRegistryElementPartFControlAction.part().number())))
                            .where(eq(property("src", EppRegistryElementPartFControlAction.controlAction()), property("sub", EppRegistryElementPartFControlAction.controlAction())));

                    dql.column(property("src.id"), srcIdColumn);
                    dql.column(subQuery.buildQuery(), destIdColumn);

                    dql.where(in(property("src", EppRegistryElementPartFControlAction.part().registryElement()), duplicateIds));
                    dql.where(existLinksExpression(EppRegistryElementPartFControlAction.class, "src", true));

                    return dql.buildQuery();
                }

                @Override
                protected void missingReplacementException() {
                    throw new ApplicationException("Набор форм итогового контроля элемента реестра, на основе которого производится объединение, недостаточен для объединения со всеми выбранными дублями.");
                }
            };

            partFcaAction.move(true); // вложенных объектов пока нет, но скипаем из на всякий случай, если появятся
        }

        // Части элемента реестра
        {
            final MoveLinksAction modulesAction = new MoveLinksAction(EppRegistryElementPart.class) {
                @Override
                protected IDQLSelectableQuery getReplacementDataSource(String srcIdColumn, String destIdColumn) {
                    // Создаем датасорс с двумя колонками: [часть дубликата, на которую есть ссылки], [часть шаблонного элемента]
                    // Соответствующая часть ищется по номеру части.

                    final DQLSelectBuilder dql = new DQLSelectBuilder();
                    dql.fromEntity(EppRegistryElementPart.class, "src");
                    dql.joinEntity("src", DQLJoinType.left, EppRegistryElementPart.class, "dest", and(
                            eq(property("dest", EppRegistryElementPart.registryElement()), value(templateId)),
                            eq(property("src", EppRegistryElementPart.number()), property("dest", EppRegistryElementPart.number()))
                    ));

                    dql.column(property("src.id"), srcIdColumn);
                    dql.column(property("dest.id"), destIdColumn);

                    dql.where(in(property("src", EppRegistryElementPart.registryElement()), duplicateIds));
                    dql.where(existLinksExpression(EppRegistryElementPart.class, "src", true));

                    return dql.buildQuery();
                }

                @Override
                protected void missingReplacementException() {
                    throw new ApplicationException("Набор частей элемента реестра, на основе которого производится объединение, недостаточен для объединениея со всеми выбранными дублями.");
                }
            };

            // вложенные объекты есть (модули и ФИК)! Их надо пропускать - они удалятся автоматом вместе с частями дублей элеемнтов реестра
            modulesAction.move(true);
        }

        // Ну и наконец сами элементы реестра
        {
            final MoveLinksAction modulesAction = new MoveLinksAction(EppRegistryElement.class) {
                @Override
                protected IDQLSelectableQuery getReplacementDataSource(String srcIdColumn, String destIdColumn) {
                    // Создаем датасорс с двумя колонками: [дубликат элемента, на который есть ссылки], [шаблонный элемент]

                    final DQLSelectBuilder dql = new DQLSelectBuilder();
                    dql.fromEntity(EppRegistryElement.class, "src");

                    dql.column(property("src.id"), srcIdColumn);
                    dql.column(value(templateId), destIdColumn);

                    dql.where(in(property("src"), duplicateIds));
                    dql.where(existLinksExpression(EppRegistryElement.class, "src", true));

                    return dql.buildQuery();
                }

                @Override
                protected void missingReplacementException() {
                    throw new IllegalStateException();
                }
            };

            // вложенные объекты есть (части и нагрузки)! Их надо пропускать - они удалятся автоматом вместе с элеемнтами реестра
            modulesAction.move(true);
        }

        // Теперь удаляем дубли элемента реестра. Все вложенные объекты (части, модули частей, ФИК) удалятся автоматически.
        final DQLDeleteBuilder dqlDeleteDuplicates = new DQLDeleteBuilder(EppRegistryElement.class);
        dqlDeleteDuplicates.where(in(property("id"), duplicateIds));
        dqlDeleteDuplicates.createStatement(getSession()).execute();

        // И удаляем учебные модули, которые были эксклюзивно созданы для дублей
        for (final List<Long> idsPart : Lists.partition(eppModulesToDelete, DQL.MAX_VALUES_ROW_NUMBER))
        {
            final DQLDeleteBuilder dqlEppModuleDelete = new DQLDeleteBuilder(EppRegistryModule.class);
            dqlEppModuleDelete.where(in(property("id"), idsPart));
            dqlEppModuleDelete.createStatement(getSession()).execute();
        }
    }

    @Override
    public void doRegElementDuplicatesMerge(Long templateId, Collection<Long> duplicateIds)
    {
        Preconditions.checkNotNull(templateId);
        Preconditions.checkArgument(!duplicateIds.isEmpty(), "Duplicates are missing");
        Preconditions.checkArgument(!duplicateIds.contains(templateId), "Duplicate can't contains template");

        EppRegistryElement element = get(templateId);
        EppEduGroupSplitVariant splitVariant = element.getEduGroupSplitVariant();
        List<EppGroupType> splitGroupTypes = getRegElementSplitGroupTypes(element);

        duplicateIds.stream().forEach(duplicateId -> {
            if (!splitVariantsAndGroupTypesSame(splitVariant, splitGroupTypes, get(duplicateId)))
                throw new ApplicationException("Элементы имеют разные способы деления потоков или разные ограничения способов по видам потоков.");
        });

        // Не взирая на состояния объектов..
        try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler()))
        {
            for (List<Long> idsPart : Iterables.partition(duplicateIds, DQL.MAX_VALUES_ROW_NUMBER))
            {
                _doDuplicatesMerge(templateId, idsPart);
            }
        }
    }

    private boolean splitVariantsAndGroupTypesSame(EppEduGroupSplitVariant splitVariant, List<EppGroupType> splitGroupTypes, EppRegistryElement element) {
        if (splitVariant != element.getEduGroupSplitVariant())
            return false;

        List<EppGroupType> elementSplitGroupTypes = getRegElementSplitGroupTypes(element);
        return ((elementSplitGroupTypes.size() == splitGroupTypes.size()) && elementSplitGroupTypes.containsAll(splitGroupTypes));
    }

    @Override
    public Map<Long, EppRegistryElement> createAndAttachRegElements(Collection<IEppEpvRowWrapper> wrappers, EppState newState, boolean alwaysCreateNewRegElement)
    {
        final Map<Long, EppRegistryElement> resultMap = new LinkedHashMap<>();
        for (final IEppEpvRowWrapper epvRowWrapper : wrappers)
        {
            if (!(epvRowWrapper.getRow() instanceof EppEpvRegistryRow)
                    || (!alwaysCreateNewRegElement && ((EppEpvRegistryRow) epvRowWrapper.getRow()).getRegistryElement() != null))
            {
                continue;
            }

            final IEppRegElWrapper epvRowRegElementWrapper = new EppEpvRegElWrapper(epvRowWrapper);
            final EppRegistryElement newElement = doCreateRegistryElement(epvRowRegElementWrapper, epvRowRegElementWrapper.getOwner(), epvRowRegElementWrapper.getTitle(), newState);

            if (newElement != null)
            {
                final EppEpvRegistryRow epvRow = (EppEpvRegistryRow) epvRowWrapper.getRow();
                epvRow.setRegistryElement(newElement);
                update(epvRow);

                resultMap.put(epvRowWrapper.getRow().getId(), newElement);
            }
        }
        return resultMap;
    }

    @Override
    public void doAutoAttachRegElements(Collection<IEppEpvRowWrapper> wrappers, boolean createIfMissing, KeyGenRule keyGenRule, Collection<EppState> filterSates, EppState newState)
    {
        // Запрос поиска элементов реестра нужного типа, на том же читающем, что и дисциплина, с тем же названием. Фильтрация по состояниям.
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, "e");
        dql.column(property("e.id"));
        dql.where(or(
                eq(property("e", EppRegistryElement.parent()), parameter("regType", PropertyType.LONG)),
                eq(property("e", EppRegistryElement.parent().parent()), parameter("regType", PropertyType.LONG))
        ));
        dql.where(eq(property("e", EppRegistryElement.owner()), parameter("owner", PropertyType.LONG)));
        dql.where(eq(DQLFunctions.upper(property("e", EppRegistryElement.title())), parameter("title", PropertyType.STRING)));
        FilterUtils.applySelectFilter(dql, "e", EppRegistryElement.state(), filterSates);

        final IDQLStatement statement = dql.createStatement(getSession());

        for (final IEppEpvRowWrapper epvRowWrapper : wrappers)
        {
            if (!(epvRowWrapper.getRow() instanceof EppEpvRegistryRow) || ((EppEpvRegistryRow) epvRowWrapper.getRow()).getRegistryElement() != null) {
                continue;
            }

            final EppEpvRegistryRow epvRow = (EppEpvRegistryRow) epvRowWrapper.getRow();

            // Ищем подходящие по названию, типу и подразделению элементы реестра
            statement.setLong("regType", epvRow.getType().getId());
            statement.setLong("owner", epvRow.getRegistryElementOwner().getId());
            statement.setString("title", epvRow.getTitle().toUpperCase());
            final List<Long> elements = statement.list();

            // Создаем враппер элемента реестра на основе строки УПв
            final IEppRegElWrapper epvRowRegElementWrapper = new EppEpvRegElWrapper(epvRowWrapper);
            EppRegistryElement rightElement = null;

            if (!elements.isEmpty()) {
                // Ищем по ключу подходящие элементы
                final Map<Long, IEppRegElWrapper> rightElementMap = new TreeMap<>(); // Если подходящих элементов будет несколько, возьмем первй по id (т.е. самый старый)
                final String epvRowKey = epvRowRegElementWrapper.generateKey(keyGenRule);
                for (Map.Entry<Long, IEppRegElWrapper> entry : getRegistryElementDataMap(elements).entrySet())
                {
                    if (epvRowKey.equals(entry.getValue().generateKey(keyGenRule))) {
                        rightElementMap.put(entry.getKey(), entry.getValue());
                    }
                }

                if (!rightElementMap.isEmpty()) {
                    rightElement = rightElementMap.values().iterator().next().getItem();
                }
            }

            if (rightElement == null && createIfMissing) {
                // Подходящих элементов не нашлось в реестре, создаем свой на основе строки УПв
                rightElement = doCreateRegistryElement(epvRowRegElementWrapper, epvRowRegElementWrapper.getOwner(), epvRowRegElementWrapper.getTitle(), newState);
            }

            if (rightElement != null) {
                // Цепляем к строке найденный или созданный элемент
                epvRow.setRegistryElement(rightElement);
                update(epvRow);
            }
        }

    }

    @Override
    public List<EppGroupType> getRegElementSplitGroupTypes(EppRegistryElement registryElement) {
        return new DQLSelectBuilder()
                .fromEntity(EppRegElementSplitByTypeEduGroupRel.class,"rel")
                .where(eq(property("rel", EppRegElementSplitByTypeEduGroupRel.registryElement()),value(registryElement)))
                .column(property("rel", EppRegElementSplitByTypeEduGroupRel.groupType()))
                .order(property("rel", EppRegElementSplitByTypeEduGroupRel.groupType().priority()))
                .createStatement(getSession())
                .list();
    }

    @Override
    public Map<Long, List<EppGroupType>> getRegElementSplitGroupTypes(List<Long> registryElementIds) {
        Map<Long, List<EppGroupType>> result = SafeMap.get(key -> new ArrayList<>());

        for (List<Long> idsPart : Lists.partition(registryElementIds, DQL.MAX_VALUES_ROW_NUMBER)) {

            final DQLSelectBuilder selectBuilder = new DQLSelectBuilder()
                    .fromEntity(EppRegElementSplitByTypeEduGroupRel.class, "rel")
                    .where(in(property("rel", EppRegElementSplitByTypeEduGroupRel.registryElement().id()), idsPart))
                    .order(property("rel", EppRegElementSplitByTypeEduGroupRel.groupType().priority()));

            List<EppRegElementSplitByTypeEduGroupRel> relations = selectBuilder.createStatement(getSession()).list();

            for (final EppRegElementSplitByTypeEduGroupRel relation : relations) {
                Long registryElementId = relation.getRegistryElement().getId();
                result.get(registryElementId).add(relation.getGroupType());
            }
        }

        return result;
    }

    @Override
    public Map<Long, Map<Long, List<Integer>>> getRegistryElement2FControlActionPartNumberMap(Collection<Long> disciplineIds) {
        Map<Long, Map<Long, List<Integer>>> resultMap = Maps.newHashMap();
        if (disciplineIds.isEmpty()) return resultMap;

        for (List<Long> idsPart : Iterables.partition(disciplineIds, DQL.MAX_VALUES_ROW_NUMBER)) {

            List<EppRegistryElementPartFControlAction> fControlActionList = new DQLSelectBuilder()
                    .fromEntity(EppRegistryElementPartFControlAction.class, "fa")
                    .column(property("fa"))
                    .joinPath(DQLJoinType.inner, EppRegistryElementPartFControlAction.part().fromAlias("fa"), "p")
                    .where(in(property("p", EppRegistryElementPart.registryElement().id()), idsPart))
                    .createStatement(getSession()).list();

            for (EppRegistryElementPartFControlAction fControlAction : fControlActionList) {
                EppRegistryElementPart part = fControlAction.getPart();
                EppRegistryElement element = part.getRegistryElement();

                if (null != element) {
                    Long id = element.getId();
                    Long controlActionId = fControlAction.getControlAction().getId();

                    Map<Long, List<Integer>> actionListMap = resultMap.get(id);

                    if (null == actionListMap) {
                        actionListMap = Maps.newHashMap();
                        resultMap.put(id, actionListMap);
                    }
                    List<Integer> partList = actionListMap.get(controlActionId);
                    if (null == partList) {
                        partList = Lists.newArrayList();
                        actionListMap.put(controlActionId, partList);
                    }
                    actionListMap.get(controlActionId).add(part.getNumber());
                }
            }
        }
        return resultMap;
    }


}
