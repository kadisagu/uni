/* $Id$ */
package ru.tandemservice.uniepp.component.registry.RegElementGradeScales;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniepp.component.registry.base.Pub.RegistryElementPubContext;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;

/**
 * @author oleyba
 * @since 11/15/11
 */
@Input({
    @Bind(key = RegistryElementPubContext.REGISTRY_ELEMENT_PUB_CONTEXT_PARAM, binding = "context", required = true)
})
public class Model
{
    private RegistryElementPubContext context;
    public RegistryElementPubContext getContext() { return this.context; }
    public void setContext(final RegistryElementPubContext context) { this.context = context; }

    public EppRegistryElement getElement() { return this.getContext().getElement(); }
    public CommonPostfixPermissionModel getSec() { return this.getContext().getSec(); }


    private List<IEntity> rowList = new ArrayList<IEntity>();
    private IEntity currentRow;

    private boolean editMode;

    private ISelectModel scaleModel;

    public boolean isCurrentRowDisc()
    {
        return this.getCurrentRow() instanceof EppRegistryElementPart;
    }

    public boolean isListEmpty()
    {
        return CollectionUtils.isEmpty(this.getRowList());
    }

    public boolean isReadOnly()
    {
        return this.getElement().getState().isReadOnlyState();
    }

    // getters and setters

    public List<IEntity> getRowList()
    {
        return this.rowList;
    }

    public void setRowList(final List<IEntity> rowList)
    {
        this.rowList = rowList;
    }

    public IEntity getCurrentRow()
    {
        return this.currentRow;
    }

    public void setCurrentRow(final IEntity currentRow)
    {
        this.currentRow = currentRow;
    }

    public boolean isEditMode()
    {
        return this.editMode;
    }

    public void setEditMode(final boolean editMode)
    {
        this.editMode = editMode;
    }

    public ISelectModel getScaleModel()
    {
        return this.scaleModel;
    }

    public void setScaleModel(final ISelectModel scaleModel)
    {
        this.scaleModel = scaleModel;
    }

}
