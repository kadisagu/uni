package ru.tandemservice.uniepp.component.workplan.WorkPlanPub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

/**
 * 
 * @author nkokorina
 * @since 18.03.2010
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setEppWorkPlan(this.getNotNull(EppWorkPlan.class, model.getEppWorkPlan().getId()));
    }
}
