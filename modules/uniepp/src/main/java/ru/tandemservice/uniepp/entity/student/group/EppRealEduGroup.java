package ru.tandemservice.uniepp.entity.student.group;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroupGen;

import java.util.Comparator;

/** @see ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroupGen */
public abstract class EppRealEduGroup extends EppRealEduGroupGen
{
    /** @return EppRealEduGroupTitleUtil.getActivityTitle */
    @EntityDSLSupport
    public String getActivityTitle()
    {
        return EppRealEduGroupTitleUtil.getActivityTitle(this);
    }

    /** @return EppRealEduGroupTitleUtil.getFullTitle */
    @EntityDSLSupport
    public String getFullTitle()
    {
        return EppRealEduGroupTitleUtil.getFullTitle(this);
    }

    /** { group-type.class -> group.class } */
    @SuppressWarnings("unchecked")
    public static Class<? extends EppRealEduGroup> getGroupClass(final Class<? extends EppGroupType> type)
    {
        if (EppGroupTypeALT.class.equals(type))
            return EppRealEduGroup4LoadType.class;
        if (EppGroupTypeFCA.class.equals(type))
            return EppRealEduGroup4ActionType.class;

        throw new IllegalStateException("No EppRealEduGroup with `type` property of (`" + type.getSimpleName() + "`)");
    }

    public static final Comparator<EppRealEduGroup> FULL_COMPARATOR = new Comparator<EppRealEduGroup>() {
        @Override public int compare(EppRealEduGroup o1, EppRealEduGroup o2) {
            int i;
            if (0 != (i = o1.getSummary().getYearPart().getPart().compareTo(o2.getSummary().getYearPart().getPart()))) { return i; }
            if (0 != (i = EppRegistryElementPart.FULL_COMPARATOR.compare(o1.getActivityPart(), o2.getActivityPart()))) { return i; }
            if (0 != (i = EppGroupType.COMPARATOR.compare(o1.getType(), o2.getType()))) { return i; }
            return o1.getTitle().compareTo(o2.getTitle());
        }
    };
}