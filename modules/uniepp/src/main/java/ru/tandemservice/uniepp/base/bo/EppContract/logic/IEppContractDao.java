package ru.tandemservice.uniepp.base.bo.EppContract.logic;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * @author vdanilov
 */
public interface IEppContractDao extends INeedPersistenceSupport
{
    /**
     * @return студенты, связанные с договором (через факты исполнения)
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Collection<Student> listStudents(CtrContractObject contract);

    /**
     * @param orgUnit  подразделение
     * @return список контрагентов  представителей подразделения при закгючении договора на обучение
     * @deprecated use EduProgramContractManager.instance().dao().getAcademyPresenters(orgUnit)
     */
    @Deprecated
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Collection<? extends ContactorPerson> getAcademyPresenters(OrgUnit orgUnit);

    /**
     * @param studentId  Student.id
     * @return true, если для студента требуется обображать вкладку с договорами
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    boolean canShowContractStudentTab(Long studentId);

    /**
     * @return тип договора "Образовательные услуги"
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    CtrContractType getEppContractRootType();


    /**
     * @return тип договора "Образовательные услуги" и все его дочерние
     * @deprecated use EduProgramContractManager.instance().dao().getContractTypes()
     */
    @Deprecated
    List<CtrContractType> getEppContractTypes();

    /**
     * @param eduPlanVersion версия учебного плана
     * @return тип договора из ветки "Образовательные услуги", соответствующий уровню образования из НПВ
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    CtrContractType getEppContractType(EppEduPlanVersion eduPlanVersion);

    /**
     * Формирует договор по текущим данным студента {@link EppContractObjectFactory}
     * @param student  студент
     * @param contractNumber  номер договора
     * @param date  дата, формирования договора
     * @param cost  цена
     * @return договор + версия
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    CtrContractVersion doCreateContractObject(EppStudent2EduPlanVersion student, String contractNumber, Date date, CtrPriceElementCost cost, CtrContractType contractType);

    /**
     * @param educationResult
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doDeleteContract(EppCtrEducationResult educationResult);

    /**
     * Выдает договору новый номер из очереди номеров для договоров на обучение, на переданный год.
     * Генерирует по шаблону [год].[четыре цифры инкрементом].
     * Внимание: увеличение номера происходит каждый раз при обращении к очереди.
     * @param eduYear учебный год
     * @deprecated use EduProgramContractManager.instance().dao().getNextNumber()
     */
    @Deprecated
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    String getNewNumber(int eduYear);
}
