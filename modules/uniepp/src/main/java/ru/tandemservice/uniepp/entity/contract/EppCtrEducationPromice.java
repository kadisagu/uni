package ru.tandemservice.uniepp.entity.contract;

import ru.tandemservice.unieductr.base.entity.IEducationPromise;
import ru.tandemservice.uniepp.entity.contract.gen.EppCtrEducationPromiceGen;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * Обязательство обучения
 *
 * Обязательство обучить человека (dst - обучаемый)
 */
public class EppCtrEducationPromice extends EppCtrEducationPromiceGen implements IEducationPromise<EppEduPlanVersion>
{
    @Override
    public EppEduPlanVersion getEducationPromiseTarget() {
        return getEduPlanVersion();
    }

    @Override
    public String getEducationPromiseDescription() {
        return new StringBuilder()
        .append("УП ").append(getEduPlanVersion().getTitle())
        .append(", ")
        .append(getEducationOrgUnit().getTitleWithFormAndCondition())
        .append(" ").append(getEducationOrgUnit().getDevelopOrgUnitShortTitle())
        .toString();
    }
}