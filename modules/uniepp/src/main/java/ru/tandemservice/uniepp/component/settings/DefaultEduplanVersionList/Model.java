// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.settings.DefaultEduplanVersionList;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;

/**
 * @author oleyba
 * @since 04.12.2010
 */
public class Model
{
    DynamicListDataSource<EducationOrgUnit> dataSource;
    private IDataSettings _settings;

    ISelectModel displayOptionList;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _territorialOrgUnitListModel;
    private ISelectModel _producingOrgUnitListModel;
    private ISelectModel _educationLevelsModel;
    private ISelectModel _qualificationListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developPeriodListModel;
    private ISelectModel _developConditionListModel;

    public String getAlert()
    {
        return "Для незаполненных строк система установит соответствующую версию учебного плана (при условии, что найдется только одна актуальная версия). Для строк, в которых указана версия учебного плана, система проверит актуальность версии учебного плана. Если версия учебного плана актуальна - система оставит строку неизменной, если версия неактуальна и найдется только одна актуальная версия - система заменит неактуальную версию УП на актуальную, иначе строки с неактуальными версиями станут незаполненными. Выполнить действие?";
    }

    public boolean isCurrentRowHasSettings()
    {
        return ((ViewWrapper)getDataSource().getCurrentValueEntity()).getViewProperty("version") != null;
    }

    public DynamicListDataSource<EducationOrgUnit> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<EducationOrgUnit> dataSource)
    {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return this._settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this._settings = settings;
    }

    public ISelectModel getDisplayOptionList()
    {
        return this.displayOptionList;
    }

    public void setDisplayOptionList(final ISelectModel displayOptionList)
    {
        this.displayOptionList = displayOptionList;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return this._formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(final ISelectModel formativeOrgUnitListModel)
    {
        this._formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return this._territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(final ISelectModel territorialOrgUnitListModel)
    {
        this._territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public ISelectModel getProducingOrgUnitListModel()
    {
        return this._producingOrgUnitListModel;
    }

    public void setProducingOrgUnitListModel(final ISelectModel producingOrgUnitListModel)
    {
        this._producingOrgUnitListModel = producingOrgUnitListModel;
    }

    public ISelectModel getEducationLevelsModel()
    {
        return this._educationLevelsModel;
    }

    public void setEducationLevelsModel(final ISelectModel educationLevelsModel)
    {
        this._educationLevelsModel = educationLevelsModel;
    }

    public ISelectModel getQualificationListModel()
    {
        return this._qualificationListModel;
    }

    public void setQualificationListModel(final ISelectModel qualificationListModel)
    {
        this._qualificationListModel = qualificationListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return this._developFormListModel;
    }

    public void setDevelopFormListModel(final ISelectModel developFormListModel)
    {
        this._developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return this._developPeriodListModel;
    }

    public void setDevelopPeriodListModel(final ISelectModel developPeriodListModel)
    {
        this._developPeriodListModel = developPeriodListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return this._developConditionListModel;
    }

    public void setDevelopConditionListModel(final ISelectModel developConditionListModel)
    {
        this._developConditionListModel = developConditionListModel;
    }
}
