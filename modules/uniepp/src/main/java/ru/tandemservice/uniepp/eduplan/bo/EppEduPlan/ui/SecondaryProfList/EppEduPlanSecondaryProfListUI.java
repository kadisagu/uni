/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.SecondaryProfList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.EppEduPlanManager;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.List.EppEduPlanListBaseUI;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.SecondaryProfAddEdit.EppEduPlanSecondaryProfAddEdit;

/**
 * @author oleyba
 * @since 8/28/14
 */
@State({
    @Bind(key = "programKindId", binding = "programKindHolder.id"),
    @Bind(key="orgUnitId", binding="orgUnitHolder.id")
})
public class EppEduPlanSecondaryProfListUI extends EppEduPlanListBaseUI
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);

        if (EppEduPlanSecondaryProfList.PROGRAM_SPECIALIZATION_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppEduPlanSecondaryProfList.PARAM_PROGRAM_SUBJECT, getSettings().get(EppEduPlanSecondaryProfList.PARAM_PROGRAM_SUBJECT));
        }
    }

    public void onClickAddEduPlan() {
        _uiActivation
            .asRegionDialog(EppEduPlanSecondaryProfAddEdit.class)
            .parameter(UIPresenter.PUBLISHER_ID, null)
            .parameter(EppEduPlanManager.BIND_PROGRAM_KIND_ID, getProgramKindHolder().getId())
            .parameter(EppEduPlanManager.BIND_ORG_UNIT_ID, getOrgUnitHolder().getId())
            .activate();
    }

    public void onClickEditEduPlan() {
        _uiActivation
            .asRegionDialog(EppEduPlanSecondaryProfAddEdit.class)
            .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
            .activate();
    }
}