package ru.tandemservice.uniepp.component.registry.RegElementPartAddModule;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.component.base.SimpleAddEditModel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="holder.id", required = true)
})
public class Model {

    private final EntityHolder<EppRegistryElementPart> holder = new EntityHolder<EppRegistryElementPart>();
    public EntityHolder<EppRegistryElementPart> getHolder() { return this.holder; }
    public EppRegistryElementPart getElement() { return this.getHolder().getValue(); }

    public boolean isReadOnly() { return SimpleAddEditModel.isReadOnly(this.getElement()); }

    private final SelectModel<EppRegistryModule> module = new SelectModel<EppRegistryModule>();
    public SelectModel<EppRegistryModule> getModule() { return this.module; }

}
