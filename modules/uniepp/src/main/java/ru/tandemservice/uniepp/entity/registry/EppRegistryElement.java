package ru.tandemservice.uniepp.entity.registry;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.entity.dsl.IPropertyPath;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.base.entity.IReorganizationElement;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppContract.EppContractManager;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateMutable;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * Запись реестра (базовая)
 */
@EppStateMutable({EppRegistryElement.L_EDU_GROUP_SPLIT_VARIANT, EppRegistryElement.P_EDU_GROUP_SPLIT_BY_TYPE})
public abstract class EppRegistryElement extends EppRegistryElementGen implements ITitled, IEppStateObject, INumberObject, ISecLocalEntityOwner, IHierarchyItem, IEppRegistryElementItem, IEntityDebugTitled
{

    @Override public INumberGenerationRule<EppRegistryElement> getNumberGenerationRule() {
        return IEppRegistryDAO.instance.get().getNumberGenerationRule();
    }

    @Override public IHierarchyItem getHierarhyParent() {
        return this.getParent();
    }

    /** [тип] №[номер из реестра мероприятий (дисциплин)] */
    @Override
    @EntityDSLSupport(parts={EppRegistryElementGen.P_NUMBER})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
    public String getNumberWithAbbreviation() {
        return StringUtils.trimToEmpty(this.getParent().getAbbreviation())+" №"+this.getNumber();
    }

    /**
     * [Название дисциплины] ([тип] №[номер из реестра мероприятий (дисциплин)])
     */
    @Override
    @EntityDSLSupport(parts = {EppRegistryElementGen.P_TITLE, EppRegistryElementGen.L_OWNER + ".title", EppRegistryElementGen.P_NUMBER})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
    public String getEducationElementTitle() {
        return (this.getTitle()+" ("+this.getNumberWithAbbreviation()+")");
    }

    /**
     * [Название дисциплины]
     **/
    @Override
    @EntityDSLSupport(parts={EppRegistryElementGen.P_TITLE})
    public String getEducationElementSimpleTitle() {
        return (this.getTitle());
    }

    @Override
    @EntityDSLSupport(parts={EppRegistryElementGen.P_TITLE, EppRegistryElementGen.P_NUMBER})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
    public String getTitleWithNumber() {
        return (this.getTitle()+" ("+this.getNumberWithAbbreviation()+")");
    }

    @Override
    @EntityDSLSupport(parts={EppRegistryElementGen.P_TITLE, EppRegistryElementGen.P_SIZE})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
    public String getTitleWithLoad() {
        return (this.getTitle()+" ("+this.getFormattedLoad()+")");
    }

    @Override
    @EntityDSLSupport
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=26609442")
    public String getTitleWithSplitVariant() {
        final EppEduGroupSplitVariant groupSplitVariant = getEduGroupSplitVariant();
        String splitVariantAndGroupTypes = "";
        if (groupSplitVariant != null) {
            splitVariantAndGroupTypes = groupSplitVariant.getShortTitle();
            if (isEduGroupSplitByType()) {
                final List<EppGroupType> groupTypes = IEppRegistryDAO.instance.get().getRegElementSplitGroupTypes(this);
                splitVariantAndGroupTypes += (groupTypes.stream()
                        .map(EppGroupType::getShortTitle)
                        .collect(Collectors.joining(", ", " (", ")")));
            }
        }
        return (splitVariantAndGroupTypes);
    }

    @Override
    @EntityDSLSupport(parts={EppRegistryElementGen.P_TITLE_WITH_NUMBER, EppRegistryElementGen.P_SIZE})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
    public String getTitleWithNumberAndLoad() {
        return (this.getTitle()+" ("+this.getFormattedLoad()+", "+this.getNumberWithAbbreviation()+")");
    }

    @Override
    @EntityDSLSupport(parts={EppRegistryElementGen.P_TITLE, EppRegistryElementGen.P_SIZE})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
    public String getTitleWithFControls() {

        String result = IEppRegistryDAO.instance.get().getRegistryElementControlActionCountByAbbreviation(this).entrySet()
                .stream()
                .map(i -> i.getValue() == 1 ? i.getKey() : i.getValue() + " " + i.getKey())
                .collect(Collectors.joining(", "));
        return result;
    }


    @Override
    public CtrContractType getContractType() {
        return EppContractManager.instance().dao().getEppContractRootType();
    }

    @Override
    public String getPriceElementTitle() {
        return this.getEducationElementTitle();
    }

    @Override
    @EntityDSLSupport(parts={EppRegistryElementGen.P_SIZE})
    public String getFormattedLoad() {
        return UniEppUtils.formatLoad(this.getSizeAsDouble(), false) + " ч.";
    }

    public String getShortTitleSafe() {
        final String shortTitle = this.getShortTitle();
        return (null != shortTitle ? shortTitle : this.getTitle());
    }

    public Double getSizeAsDouble() {
        final long size = this.getSize();
        return UniEppUtils.wrap(size < 0 ? null : size);
    }

    public void setSizeAsDouble(final Double value) {
        final Long size = UniEppUtils.unwrap(value);
        this.setSize(null == size ? -1 : size.longValue());
    }

    public Double getLaborAsDouble() {
        final long labor = this.getLabor();
        return UniEppUtils.wrap(labor < 0 ? null : labor);
    }

    public void setLaborAsDouble(final Double value) {
        final Long labor = UniEppUtils.unwrap(value);
        this.setLabor(null == labor ? -1 : labor.longValue());
    }

    private String _description_string_cache = null;
    public String getDescriptionString() {
        if (null != this._description_string_cache) { return this._description_string_cache; }

        final StringBuilder sb = new StringBuilder(Long.toHexString(this.getParent().getId()));
        sb.append('\n').append(Long.toHexString(this.getOwner().getId()));
        sb.append('\n').append(this.getTitle());
        sb.append('\n').append(this.getParts());
        sb.append('\n').append(this.getSize());
        sb.append('\n').append(this.getLabor());
        return (this._description_string_cache = sb.toString());
    }

    @Override
    public String toString() {
        final OrgUnit owner = this.getOwner();
        return this.getClass().getSimpleName() + "@" + Long.toHexString(this.getId()) + ": " + this.getTitle() + " ("+this.getSizeAsDouble()+"/"+this.getLaborAsDouble()+") " + (null == owner ? "null" : owner.getId());
    }

    @Override
    public Collection<IEntity> getSecLocalEntities() {
        return Collections.singletonList((IEntity) this.getOwner());
    }

    @Override
    public String getEntityDebugTitle()
    {
        return getTitleWithNumber();
    }

    @Override
    public IPropertyPath getReorgranizationTitlePath() {
        return titleWithNumberAndLoad();
    }

    @Override
    public void changeReorganizationElement(IReorganizationElement element) {
        setOwner(((OrgUnit) element));
    }

    @Override
    public EventListenerLocker<IDSetEventListener> getLocker() {
        return EppDSetUpdateCheckEventListener.LOCKER;
    }

    @Override
    public List<IEntity> getLinkedEntitiesToUpdate(IReorganizationElement newOwner) {

        final OrgUnit orgUnitNewOwner = (OrgUnit) newOwner;

        final DQLSelectBuilder modulesBuilder = new DQLSelectBuilder().fromEntity(EppRegistryModule.class, "regModule")
                .where(exists(
                        new DQLSelectBuilder()
                                .fromEntity(EppRegistryElementPartModule.class, "e")
                                .joinPath(DQLJoinType.inner, EppRegistryElementPartModule.module().fromAlias("e"), "module")
                                .joinPath(DQLJoinType.inner, EppRegistryElementPartModule.part().fromAlias("e"), "part")
                                .joinPath(DQLJoinType.inner, EppRegistryModule.owner().fromAlias("module"), "moduleOwner")
                                .joinPath(DQLJoinType.inner, EppRegistryElementPart.registryElement().fromAlias("part"), "element")
                                .where(eq(property("element" ,EppRegistryElement.id()), value(this.getId())))
                                .where(eq(property("moduleOwner", OrgUnit.id()), value(this.getOwner().getId())))
                                .where(eq(property("module", EppRegistryModule.id()), property("regModule", EppRegistryModule.id())))
                                .buildQuery()
                ));


        final List<IEntity> result = new ArrayList<>();

        final List<EppRegistryModule> registryModules = DataAccessServices.dao().getList(modulesBuilder);
        final List<EppEpvRegistryRow> registryRows =  DataAccessServices.dao().getList(EppEpvRegistryRow.class, EppEpvRegistryRow.registryElement(), this);

        for (EppRegistryModule registryModule : registryModules) {
            registryModule.setOwner(orgUnitNewOwner);
            result.add(registryModule);
        }

        for (EppEpvRegistryRow registryRow : registryRows) {
            registryRow.setRegistryElementOwner(orgUnitNewOwner);
            result.add(registryRow);
        }

        return result;
    }
}