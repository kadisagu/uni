package ru.tandemservice.uniepp.component.edugroup.list.GroupOrgUnitTab;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.component.edugroup.EduGroupOwnerModel;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DAO extends ru.tandemservice.uniepp.component.edugroup.list.GroupListOwnerTabBase.DAO implements IDAO
{
    @Override protected String getSettingsName() {
        return "group";
    }

    /**
     * вывдоим все сводки, в УГС которых есть студенты, деканатом сохраненного НПП которых является текущее подразделение
     */
    @Override
    protected List<EppRealEduGroupSummary> summaryList(EduGroupOwnerModel model) {
        return new DQLSelectBuilder().fromEntity(EppRealEduGroupSummary.class, "s")
                .column(property("s"))
                .where(exists(
                        EppRealEduGroupRow.class,
                        EppRealEduGroupRow.group().summary().s(), property("s"),
                        EppRealEduGroupRow.studentEducationOrgUnit().groupOrgUnit().s(), model.getOrgUnit()
                ))
                .createStatement(getSession()).list();
    }

    /**
     * все УГС в которых есть студенты, деканатом сохраненного НПП которых является текущее подразделение
     */
    @Override
    protected DQLSelectBuilder groupWithRelationsDql(EduGroupOwnerModel model, EppRealEduGroupSummary summary, Class<? extends EppRealEduGroup> groupClass) {
        if (EppRealEduGroup.class.equals(groupClass)) {
            // всегда должен быть конкретный класс
            throw new IllegalStateException();
        }

        return super.groupWithRelationsDql(model, summary, groupClass)
                .where(eq(property(EppRealEduGroupRow.studentEducationOrgUnit().groupOrgUnit().fromAlias("rel")), value(model.getOrgUnit())));
    }
}