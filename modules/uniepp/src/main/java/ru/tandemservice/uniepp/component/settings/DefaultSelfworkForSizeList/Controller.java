// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.settings.DefaultSelfworkForSizeList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.IUnieppComponents;
import ru.tandemservice.uniepp.entity.settings.EppDefaultSelfworkForSize;

/**
 * @author oleyba
 * @since 08.10.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.prepareListDateSource(component);
    }

    private void prepareListDateSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDataSource() != null) {
            return;
        }

        final DynamicListDataSource<EppDefaultSelfworkForSize> dataSource = UniBaseUtils.createDataSource(component, this.getDao());
        dataSource.addColumn(new SimpleColumn("Форма освоения", EppDefaultSelfworkForSize.developForm().title()));
        dataSource.addColumn(new SimpleColumn("Условие освоения", EppDefaultSelfworkForSize.developCondition().title()));
        dataSource.addColumn(new SimpleColumn("Технология освоения", EppDefaultSelfworkForSize.developTech().title()));
        dataSource.addColumn(new SimpleColumn("Срок освоения", EppDefaultSelfworkForSize.developGrid().title()));
        dataSource.addColumn(new SimpleColumn("Доля самостоятельной работы", EppDefaultSelfworkForSize.doubleValue()));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditRule"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteRule", "Удалить элемент настройки «{0}»?", EppDefaultSelfworkForSize.title().s()));

        model.setDataSource(dataSource);
    }

    public void onClickAddRule(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUnieppComponents.DEFAULT_SELFWORK_FOR_SIZE_ADD_EDIT, new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, null)));
    }

    public void onClickEditRule(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUnieppComponents.DEFAULT_SELFWORK_FOR_SIZE_ADD_EDIT, new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())));
    }

    public void onClickDeleteRule(final IBusinessComponent component)
    {
        this.getDao().deleteRow(component);
    }
}