package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionsTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.CheckboxColumn;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRenderComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final String settingsKey = "epp.workPlanVersionList." + model.getWorkPlan().getId();
        model.setSettings(UniBaseUtils.getDataSettings(component, settingsKey));
        this.getDao().prepare(model);

    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        this.getDao().prepare(model);
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onClickAddVersion(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.workplan.WorkPlanVersionAddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getWorkPlanHolder().getId())
        ));
    }

    public void onClickChangeVersion(final IBusinessComponent component)
    {
        final Set<Long> selectedObject = ((CheckboxColumn)this.getModel(component).getVersionDistributionDataSource().getColumn("selected")).getSelected();

        final Set<Long> selectedIds = new HashSet<Long>(selectedObject.size());
        for (final Long entityId : selectedObject)
        {
            final IEntityMeta meta = EntityRuntime.getMeta(entityId);
            if (null != meta)
            {
                if (Student.class.isAssignableFrom(meta.getEntityClass()))
                {
                    selectedIds.add(entityId);
                }
            }
        }

        if (selectedIds.isEmpty())
        {
            ContextLocal.getErrorCollector().add("Невозможно привязать студентов. Ни один студент не выбран.");
            return;
        }
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.workplan.WorkPlanChangeVersion.Model.class.getPackage().getName(),
                new ParametersMap().add("ids", new ArrayList<Long>(selectedIds)).add("workPlanId", this.getModel(component).getWorkPlanHolder().getId())));
    }
}
