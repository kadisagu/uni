package ru.tandemservice.uniepp.entity.std.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.std.data.EppStdActionRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdHierarchyRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись ГОС (доп мероприятие)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStdActionRowGen extends EppStdRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.std.data.EppStdActionRow";
    public static final String ENTITY_NAME = "eppStdActionRow";
    public static final int VERSION_HASH = -2092497338;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";
    public static final String P_NUMBER = "number";
    public static final String P_TITLE = "title";
    public static final String P_TOTAL_WEEKS_MIN = "totalWeeksMin";
    public static final String P_TOTAL_WEEKS_MAX = "totalWeeksMax";
    public static final String L_TYPE = "type";
    public static final String P_TOTAL_WEEKS = "totalWeeks";

    private EppStdHierarchyRow _parent;     // Запись ГОС (иерархия элементов)
    private String _number;     // Номер записи
    private String _title;     // Номер записи
    private Long _totalWeeksMin;     // Число недель (min)
    private Long _totalWeeksMax;     // Число недель (max)
    private EppRegistryStructure _type;     // Структура реестра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись ГОС (иерархия элементов).
     */
    public EppStdHierarchyRow getParent()
    {
        return _parent;
    }

    /**
     * @param parent Запись ГОС (иерархия элементов).
     */
    public void setParent(EppStdHierarchyRow parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Номер записи. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер записи. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Номер записи. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Номер записи. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Число недель (min).
     */
    public Long getTotalWeeksMin()
    {
        return _totalWeeksMin;
    }

    /**
     * @param totalWeeksMin Число недель (min).
     */
    public void setTotalWeeksMin(Long totalWeeksMin)
    {
        dirty(_totalWeeksMin, totalWeeksMin);
        _totalWeeksMin = totalWeeksMin;
    }

    /**
     * @return Число недель (max).
     */
    public Long getTotalWeeksMax()
    {
        return _totalWeeksMax;
    }

    /**
     * @param totalWeeksMax Число недель (max).
     */
    public void setTotalWeeksMax(Long totalWeeksMax)
    {
        dirty(_totalWeeksMax, totalWeeksMax);
        _totalWeeksMax = totalWeeksMax;
    }

    /**
     * @return Структура реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryStructure getType()
    {
        return _type;
    }

    /**
     * @param type Структура реестра. Свойство не может быть null.
     */
    public void setType(EppRegistryStructure type)
    {
        dirty(_type, type);
        _type = type;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppStdActionRowGen)
        {
            setParent(((EppStdActionRow)another).getParent());
            setNumber(((EppStdActionRow)another).getNumber());
            setTitle(((EppStdActionRow)another).getTitle());
            setTotalWeeksMin(((EppStdActionRow)another).getTotalWeeksMin());
            setTotalWeeksMax(((EppStdActionRow)another).getTotalWeeksMax());
            setType(((EppStdActionRow)another).getType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStdActionRowGen> extends EppStdRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStdActionRow.class;
        }

        public T newInstance()
        {
            return (T) new EppStdActionRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return obj.getParent();
                case "number":
                    return obj.getNumber();
                case "title":
                    return obj.getTitle();
                case "totalWeeksMin":
                    return obj.getTotalWeeksMin();
                case "totalWeeksMax":
                    return obj.getTotalWeeksMax();
                case "type":
                    return obj.getType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "parent":
                    obj.setParent((EppStdHierarchyRow) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "totalWeeksMin":
                    obj.setTotalWeeksMin((Long) value);
                    return;
                case "totalWeeksMax":
                    obj.setTotalWeeksMax((Long) value);
                    return;
                case "type":
                    obj.setType((EppRegistryStructure) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                        return true;
                case "number":
                        return true;
                case "title":
                        return true;
                case "totalWeeksMin":
                        return true;
                case "totalWeeksMax":
                        return true;
                case "type":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return true;
                case "number":
                    return true;
                case "title":
                    return true;
                case "totalWeeksMin":
                    return true;
                case "totalWeeksMax":
                    return true;
                case "type":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return EppStdHierarchyRow.class;
                case "number":
                    return String.class;
                case "title":
                    return String.class;
                case "totalWeeksMin":
                    return Long.class;
                case "totalWeeksMax":
                    return Long.class;
                case "type":
                    return EppRegistryStructure.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStdActionRow> _dslPath = new Path<EppStdActionRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStdActionRow");
    }
            

    /**
     * @return Запись ГОС (иерархия элементов).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getParent()
     */
    public static EppStdHierarchyRow.Path<EppStdHierarchyRow> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Номер записи. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Номер записи. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Число недель (min).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getTotalWeeksMin()
     */
    public static PropertyPath<Long> totalWeeksMin()
    {
        return _dslPath.totalWeeksMin();
    }

    /**
     * @return Число недель (max).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getTotalWeeksMax()
     */
    public static PropertyPath<Long> totalWeeksMax()
    {
        return _dslPath.totalWeeksMax();
    }

    /**
     * @return Структура реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getType()
     */
    public static EppRegistryStructure.Path<EppRegistryStructure> type()
    {
        return _dslPath.type();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getTotalWeeks()
     */
    public static SupportedPropertyPath<String> totalWeeks()
    {
        return _dslPath.totalWeeks();
    }

    public static class Path<E extends EppStdActionRow> extends EppStdRow.Path<E>
    {
        private EppStdHierarchyRow.Path<EppStdHierarchyRow> _parent;
        private PropertyPath<String> _number;
        private PropertyPath<String> _title;
        private PropertyPath<Long> _totalWeeksMin;
        private PropertyPath<Long> _totalWeeksMax;
        private EppRegistryStructure.Path<EppRegistryStructure> _type;
        private SupportedPropertyPath<String> _totalWeeks;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись ГОС (иерархия элементов).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getParent()
     */
        public EppStdHierarchyRow.Path<EppStdHierarchyRow> parent()
        {
            if(_parent == null )
                _parent = new EppStdHierarchyRow.Path<EppStdHierarchyRow>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Номер записи. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppStdActionRowGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Номер записи. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppStdActionRowGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Число недель (min).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getTotalWeeksMin()
     */
        public PropertyPath<Long> totalWeeksMin()
        {
            if(_totalWeeksMin == null )
                _totalWeeksMin = new PropertyPath<Long>(EppStdActionRowGen.P_TOTAL_WEEKS_MIN, this);
            return _totalWeeksMin;
        }

    /**
     * @return Число недель (max).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getTotalWeeksMax()
     */
        public PropertyPath<Long> totalWeeksMax()
        {
            if(_totalWeeksMax == null )
                _totalWeeksMax = new PropertyPath<Long>(EppStdActionRowGen.P_TOTAL_WEEKS_MAX, this);
            return _totalWeeksMax;
        }

    /**
     * @return Структура реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getType()
     */
        public EppRegistryStructure.Path<EppRegistryStructure> type()
        {
            if(_type == null )
                _type = new EppRegistryStructure.Path<EppRegistryStructure>(L_TYPE, this);
            return _type;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdActionRow#getTotalWeeks()
     */
        public SupportedPropertyPath<String> totalWeeks()
        {
            if(_totalWeeks == null )
                _totalWeeks = new SupportedPropertyPath<String>(EppStdActionRowGen.P_TOTAL_WEEKS, this);
            return _totalWeeks;
        }

        public Class getEntityClass()
        {
            return EppStdActionRow.class;
        }

        public String getEntityName()
        {
            return "eppStdActionRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTotalWeeks();
}
