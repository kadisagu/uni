/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppSettings.logic;

import com.google.common.base.Preconditions;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;

/**
 * @author Nikolay Fedorovskih
 * @since 16.01.2015
 */
public class EppSettingsWrapper
{
    private static final String EPP_SETTINGS_OWNER = "eppGlobalSettings";

    private static final String EPP_SETTINGS_KEY_USE_CYCLIC_LOADING = "useCyclicLoading"; // Использовать цикловую нагрузку
    private static final String EPP_SETTINGS_KEY_ALLOW_WP_WO_WG = "allowWPwoWG"; // Разрешать РУП без ГУП
    private static final String EPP_SETTINGS_KEY_CHECK_ELEMENT_STATE = "checkElementState"; // Учитывать согласование объектов
    private static final String EPP_SETTINGS_KEY_ALLOW_EMPTY_WP = "allowEmptyWP"; // Разрешить согласование пустых РУП
    private static final String EPP_SETTINGS_KEY_ALLOW_NOT_UNIQUE_EPV_ROW_NAME = "allowNotUniqueEpvRowName"; // Разрешать дисциплины с одинаковыми названиями в УП

    private final IDataSettings _settings;
    private final boolean _readOnly;

    public EppSettingsWrapper(boolean readOnly)
    {
        _settings = DataSettingsFacade.getSettings(EPP_SETTINGS_OWNER);
        _readOnly = readOnly;
    }

    private <T> T get(String key, T defaultValue)
    {
        T ret = _settings.get(key);
        return ret != null ? ret : defaultValue;
    }

    private void set(String key, Object value)
    {
        Preconditions.checkState(!_readOnly);
        _settings.set(key, value);
    }

    public IDataSettings getDataSettings()
    {
        return _settings;
    }

    public boolean isUseCyclicLoading()
    {
        return get(EPP_SETTINGS_KEY_USE_CYCLIC_LOADING, false);
    }

    public void setUseCyclicLoading(boolean value)
    {
        set(EPP_SETTINGS_KEY_USE_CYCLIC_LOADING, value);
    }

    public boolean isAllowWPwoWG()
    {
        return get(EPP_SETTINGS_KEY_ALLOW_WP_WO_WG, true);
    }

    public void setAllowWPwoWG(boolean value)
    {
        set(EPP_SETTINGS_KEY_ALLOW_WP_WO_WG, value);
    }

    public boolean isCheckElementState()
    {
        return get(EPP_SETTINGS_KEY_CHECK_ELEMENT_STATE, true);
    }

    public void setCheckElementState(boolean value)
    {
        set(EPP_SETTINGS_KEY_CHECK_ELEMENT_STATE, value);
    }

    public boolean isAllowEmptyWP()
    {
        return get(EPP_SETTINGS_KEY_ALLOW_EMPTY_WP, false);
    }

    public void setAllowEmptyWP(boolean value)
    {
        set(EPP_SETTINGS_KEY_ALLOW_EMPTY_WP, value);
    }

    public boolean isAllowNotUniqueEpvRowName()
    {
        return get(EPP_SETTINGS_KEY_ALLOW_NOT_UNIQUE_EPV_ROW_NAME, false);
    }

    public void setAllowNotUniqueEpvRowName(boolean value)
    {
        set(EPP_SETTINGS_KEY_ALLOW_NOT_UNIQUE_EPV_ROW_NAME, value);
    }
}