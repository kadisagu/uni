package ru.tandemservice.uniepp.dao.eduplan.data;

import org.apache.commons.collections15.Predicate;
import ru.tandemservice.uniepp.dao.index.IEppIndexedRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.catalog.EppProfessionalTask;
import ru.tandemservice.uniepp.entity.plan.data.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Оболочка для строки УП (используется для хранения информации в готовой форме)
 * @author vdanilov
 */
public interface IEppEpvRowWrapper extends IEppEpvRow, IEppIndexedRowWrapper
{
    /** @return Порядковый номер в рамках УП для eppEpvRegistryRow */
    Integer getEpvRegistryRowNumber();

    // все, кроме доп. мероприятий
    Predicate<IEppEpvRowWrapper> DISCIPLINES_ROWS = rowWrapper -> {
        final IEppEpvRow row = rowWrapper.getRow();
        return !(row instanceof EppEpvRegistryRow) || row.getType().isDisciplineElement();
    };

    // только доп. мероприятия
    Predicate<IEppEpvRowWrapper> ACTIONS_ROWS = rowWrapper -> {
        final IEppEpvRow row = rowWrapper.getRow();
        return row instanceof EppEpvRegistryRow && !row.getType().isDisciplineElement();
    };

    /** вычисленная аудиторная нагрузка (сумма лекций, практик и лабораторных) */
    String TYPE_TOTAL_AUDIT_CALC_FULL_CODE = EppELoadType.FULL_CODE_AUDIT + "$calc";

    /** @return оболочка версии УП */
    IEppEpvBlockWrapper getVersion();

    /** @return строка УП */
    @Override IEppEpvRow getRow();

    /** @return вышестоящий wrapper (соответствует row.parent) */
    @Override IEppEpvRowWrapper getHierarhyParent();

    /** @return список дочерних элементов для строки (readonly) */
    List<IEppEpvRowWrapper> getChilds();

    /** @return указатель того, что для данного элемента задано нагрузки по семестрам */
    boolean isTermDataOwner();

    /**
     * Семестры, в которых есть указанная форма контроля. Возвращает данные, подготовленные к показу.
     * ВНИМАНИЕ - эти данные нужно использовать ТОЛЬКО для отображения.
     *
     * @param controlActionFullCode код формы контроля
     **/
    String getDisplayableControlActionUsage(String controlActionFullCode);

    /**
     * возвращает данные по теоретической нагрузке, введенные на форме (в том виде, как они вводятся - либо в темпах, либо в часах в семестр)
     * @param term - семестр (null - сумма по строке (всегда выводится в часах в семестр), 0 - итоговые данные, введенные в строке (всегда выводится в часах в семестр))
     * @param loadFullCode - полный код нагрузки
     * @return данные по нагрузке текущей строки (с учетом нагрузки дочерних элементов) NOT-NULL
     **/
    double getTotalLoad(Integer term, String loadFullCode);

    /**
     * возвращает данные по теоретической нагрузке, введенные на форме (в том виде, как они вводятся - либо в темпах, либо в часах в семестр)
     * ВНИМАНИЕ - эти данные нужно использовать ТОЛЬКО для отображения.
     *
     * @param term семестр (null - сумма по строке (всегда выводится в часах в семестр), 0 - итоговые данные, введенные в строке (всегда выводится в часах в семестр))
     * @param divider делитель, на который надо разделить нагрузку. Нужно для сравнения часов групп дисциплин по выбору и часами самих дисциплин.
     **/
    String getTotalDisplayableLoadString(Integer term, String loadFullCode, boolean hideZero, int divider);

    /**
     * Возвращает итоговые данные по нагрузке для отображения.
     * ВНИМАНИЕ - эти данные нужно использовать ТОЛЬКО для отображения.
     * @param loadFullCode код нагрузки
     * @param divider делитель, на который надо разделить нагрузку. Нужно для сравнения часов групп дисциплин по выбору и часами самих дисциплин.
     */
    String getRowWrapperTotalByLoadTypeRaw(final String loadFullCode, int divider);

    /**
     * @see ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper#getTotalLoad(Integer, String)
     * @param term - семестр (null - сумма по строке (всегда выводится в часах в семестр), 0 - итоговые данные, введенные в строке (всегда выводится в часах в семестр))
     * возвращает данные, подготовленные к показу (с учетом настройки ввода и отображения элементов УП)
     * ВНИМАНИЕ - эти данные нужно использовать ТОЛЬКО для отображения
     **/
    double getTotalDisplayableLoad(Integer term, String loadFullCode);

    /**
     * @see ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper#getTotalLoad(Integer, String)
     * @param term - семестр (null - сумма по строке (всегда выводится в часах в семестр), 0 - итоговые данные, введенные в строке (всегда выводится в часах в семестр))
     * возвращает данные в часах в семестр (эти данные используются для суммирования, вычисления)
     **/
    double getTotalInTermLoad(Integer term, String loadFullCode);

    List<EppProfessionalTask> getProfessionalTaskList();

    void setProfessionalTaskList(List<EppProfessionalTask> professionalTaskList);

    /**
     * @param term - семестр (null - сумма)
     * @param actionFullCode - полный код контрольного мероприятия
     * @return данные по контрольным мероприятиям текущей строки NOT-NULL
     **/
    int getActionSize(Integer term, String actionFullCode);

    /**
     * @param actionFullCode - полный код контрольного мероприятия
     * @return номера семестров по контрольным мероприятиям текущей строки NOT-NULL
     **/
    List<Integer> getActionTermSet(String actionFullCode);

    /**
     * @param term - семестр
     * @return проверяет, есть ли нагрузка (теоретическая или контроль) в семестре
     */
    boolean hasInTermActivity(Integer term);

    /**
     * @return номера семестров &gt; 0, в которых есть нагрузка или контроль (см. <code>hasInTermActivity</code>)
     */
    Set<Integer> getActiveTermSet();

    /**
     * @param term семестр
     * @return порядковый номер семестра (среди активных) (см. <code>getActiveTermSet</code>), 0 - семестр не найден
     */
    int getActiveTermNumber(int term);

    /**
     * @param activeTermNumber номер семестра (среди активных)
     * @return порядковый номер семестра (среди всех) (см. <code>getActiveTermSet</code>), 0 - нет активного семестра
     */
    int getTermNumber(int activeTermNumber);

    /**
     * Возвращает для данного враппера все нагрузки и контольные мероприятия за все семестры
     */
    Map<Integer, Map<String, Number>> getWrapperDataMap();

    /**
     * Строковый ключ содержания для конкретного семестра по ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper#getWrapperDataMap
     * Для сравнения способа чтения дисциплины в данной строке со способом ее чтения в другой строке - нагрузки, контрольные мероприятия
     * @param term семестр
     * @return строка с ключом
     */
    String getTermKey(Integer term);


    /** @return Возвращает группу дисциплин по выбору, если такая есть среди родительских строк */
    default EppEpvGroupImRow getParentSelectedGroupRow()
    {
        IEppEpvRowWrapper parent = this.getHierarhyParent();
        while (parent != null) {
            if (parent.getRow() instanceof EppEpvGroupImRow) {
                return (EppEpvGroupImRow) parent.getRow();
            }
            parent = parent.getHierarhyParent();
        }
        return null;
    }

    /** @return это строка из группы дисциплин по выбору */
    default boolean isSelectedItem()
    {
        return getParentSelectedGroupRow() != null;
    }

    /** @return это строка из группы факультативных дисциплин */
    default boolean isOptionalItem()
    {
        IEppEpvRowWrapper parent = this.getHierarhyParent();
        while (parent != null) {
            if (parent.getRow() instanceof EppEpvStructureRow) {
                final String typeCode = ((EppEpvStructureRow) parent.getRow()).getValue().getCode();
                if (EppPlanStructure.OPTIONAL_DISCIPLINES.contains(typeCode)) {
                    return true;
                }
            }
            parent = parent.getHierarhyParent();
        }
        return false;
    }

    /** @return является ли строка подчиненной строке с нагрузкой (как например, в группе дисциплин по выбору) */
    default boolean hasParentDistributedRow()
    {
        IEppEpvRowWrapper parent = this.getHierarhyParent();
        while (parent != null) {
            if (parent.getRow() instanceof EppEpvTermDistributedRow) {
                return true;
            }
            parent = parent.getHierarhyParent();
        }
        return false;
    }

    /** @return Является ли строка дочерней по отношению к указанной */
    default boolean isChildOf(IEppEpvRowWrapper checkedParent)
    {
        IEppEpvRowWrapper parent = this.getHierarhyParent();
        while (parent != null) {
            if (parent.equals(checkedParent)) {
                return true;
            }
            parent = parent.getHierarhyParent();
        }
        return false;
    }
}
