package ru.tandemservice.uniepp.base.bo.EppContract.ui.AddStudentContract;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.CtrPricePaymentGrid;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.EppContract.EppContractManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="studentHolder.id", required=true)
})
public class EppContractAddStudentContractUI extends UIPresenter {

    private final EntityHolder<Student> studentHolder = new EntityHolder<Student>();
    public EntityHolder<Student> getStudentHolder() { return this.studentHolder; }
    public Student getStudent() { return this.getStudentHolder().getValue(); }

    private EppEduPlanVersionBlock eduPlanVersionBlock;
    public EppEduPlanVersionBlock getEduPlanVersionBlock() { return this.eduPlanVersionBlock; }
    protected void setEduPlanVersionBlock(final EppEduPlanVersionBlock eduPlanVersionBlock) { this.eduPlanVersionBlock = eduPlanVersionBlock; }

    private EppStudent2EduPlanVersion student2EduPlanVersion;
    public EppStudent2EduPlanVersion getStudent2EduPlanVersion() { return this.student2EduPlanVersion; }
    protected void setStudent2EduPlanVersion(final EppStudent2EduPlanVersion student2EduPlanVersion) { this.student2EduPlanVersion = student2EduPlanVersion; }

    private CtrContractType contractType;
    public CtrContractType getContractType() { return this.contractType; }
    protected void setContractType(final CtrContractType contractType) { this.contractType = contractType; }

    // номер договора

    private String contractNumber;
    public String getContractNumber() { return this.contractNumber; }
    public void setContractNumber(final String contractNumber) { this.contractNumber = contractNumber; }

    // дата, на которую действует цена

    private Date priceDate = new Date();
    public Date getPriceDate() { return this.priceDate; }
    public void setPriceDate(final Date priceDate) { this.priceDate = priceDate; }

    // список всех сгрупперованных цен

    private Map<Currency, Map<CtrPricePaymentGrid, List<CtrPriceElementCost>>> currencyGridCostMap = Collections.emptyMap();
    protected Map<Currency, Map<CtrPricePaymentGrid, List<CtrPriceElementCost>>> getCurrencyGridCostMap() { return this.currencyGridCostMap; }
    protected void setCurrencyGridCostMap(final Map<Currency, Map<CtrPricePaymentGrid, List<CtrPriceElementCost>>> currencyGridCostMap) { this.currencyGridCostMap = currencyGridCostMap; }

    public Set<Map.Entry<Currency, Map<CtrPricePaymentGrid, List<CtrPriceElementCost>>>> getCurrencyGridCostEntrySet() { return this.getCurrencyGridCostMap().entrySet(); }

    // текуая валюта, список цен для указанной валюты

    private Map.Entry<Currency, Map<CtrPricePaymentGrid, List<CtrPriceElementCost>>> currencyGridCostEntry;
    public Map.Entry<Currency, Map<CtrPricePaymentGrid, List<CtrPriceElementCost>>> getCurrencyGridCostEntry() { return this.currencyGridCostEntry; }
    public void setCurrencyGridCostEntry(final Map.Entry<Currency, Map<CtrPricePaymentGrid, List<CtrPriceElementCost>>> currencyGridCostEntry) { this.currencyGridCostEntry = currencyGridCostEntry; }

    public Currency getCurrency() { return this.getCurrencyGridCostEntry().getKey(); }
    public Set<Map.Entry<CtrPricePaymentGrid, List<CtrPriceElementCost>>> getGridCostEntrySet() { return this.getCurrencyGridCostEntry().getValue().entrySet(); }

    // текущая сетка, список цен для указанной сетки

    private Map.Entry<CtrPricePaymentGrid, List<CtrPriceElementCost>> gridCostEntry;
    public Map.Entry<CtrPricePaymentGrid, List<CtrPriceElementCost>> getGridCostEntry() { return this.gridCostEntry; }
    public void setGridCostEntry(final Map.Entry<CtrPricePaymentGrid, List<CtrPriceElementCost>> gridCostEntry) { this.gridCostEntry = gridCostEntry; }

    public CtrPricePaymentGrid getPaymentGrid() { return this.getGridCostEntry().getKey(); }
    public List<CtrPriceElementCost> getCostList() { return this.getGridCostEntry().getValue(); }

    // текущая цена

    private CtrPriceElementCost cost;
    public CtrPriceElementCost getCost() { return this.cost; }
    public void setCost(final CtrPriceElementCost cost) { this.cost = cost; }

    // выбранная цена

    private CtrPriceElementCost selectedCost;
    public CtrPriceElementCost getSelectedCost() { return this.selectedCost; }
    public void setSelectedCost(final CtrPriceElementCost selectedCost) { this.selectedCost = selectedCost;

    }

    @Override
    public void onComponentRefresh()
    {
        final Session session = _uiSupport.getSession();
        final Student student = this.getStudentHolder().refresh(Student.class);

        if (null == StringUtils.trimToNull(this.getContractNumber())) {
            this.setContractNumber(student.getContractNumber());
        }

        final EppStudent2EduPlanVersion s2epv = new DQLSelectBuilder()
        .fromEntity(Student.class, "s")
        .where(eq(property("s"), value(student)))

        .fromEntity(EppStudent2EduPlanVersion.class, "s2epv").column(property("s2epv"))
        .where(eq(property(EppStudent2EduPlanVersion.student().fromAlias("s2epv")), property("s")))
        .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
        .order(property(EppStudent2EduPlanVersion.id().fromAlias("s2epv")))

        .createStatement(session)
        .setMaxResults(1).uniqueResult();

        if (null == s2epv.getBlock()) {
            // нет актуального блока
            throw new ApplicationException(this._uiConfig.getProperty("error.no-student-epv-block", student.getFullTitle() ));
        }

        this.setStudent2EduPlanVersion(s2epv);
        this.setEduPlanVersionBlock(getStudent2EduPlanVersion().getBlock());
        this.setContractType(this.getEduPlanVersionBlock().getContractType());

        // обновляем цену на текущую дату
        this.onChangePriceDate();
    }

    @SuppressWarnings("deprecation")
    public void onChangePriceDate()
    {
        final Date date = this.getPriceDate();
        final Map<Currency, Map<CtrPricePaymentGrid, List<CtrPriceElementCost>>> currencyGridCostMap = new LinkedHashMap<Currency, Map<CtrPricePaymentGrid, List<CtrPriceElementCost>>>(4);

        if (null != date)
        {
            final List<CtrContractType> types = this.getEppContractTypePath(this.getContractType());

            final List<CtrPriceElementCost> list = ISharedBaseDao.instance.get().getList(new DQLSelectBuilder()
                .fromEntity(CtrPriceElementCost.class, "c")
                .where(eq(property(CtrPriceElementCost.period().element().fromAlias("c")), value(this.getEduPlanVersionBlock())))
                .where(in(property(CtrPriceElementCost.paymentGrid().contractType().fromAlias("c")), types))
                .where(in(property(CtrPriceElementCost.category().contractType().fromAlias("c")), types))
                .where(and(
                    lt(property(CtrPriceElementCost.period().durationBegin().fromAlias("c")), valueDate(CoreDateUtils.getNextDayFirstTimeMoment(date, 1))),
                    lt(valueDate(CoreDateUtils.getDayFirstTimeMoment(date)), property(CtrPriceElementCost.period().durationEnd().fromAlias("c")))
                ))
                .order(property(CtrPriceElementCost.currency().iso4217AlphaCode().fromAlias("c")))
                .order(property(CtrPriceElementCost.paymentGrid().code().fromAlias("c")))
                .order(property(CtrPriceElementCost.category().code().fromAlias("c"))));


            for (final CtrPriceElementCost cost: list) {
                final Map<CtrPricePaymentGrid, List<CtrPriceElementCost>> gridCostMap = SafeMap.safeGet(currencyGridCostMap, cost.getCurrency(), LinkedHashMap.class);
                final List<CtrPriceElementCost> costList = SafeMap.safeGet(gridCostMap, cost.getPaymentGrid(), ArrayList.class);
                costList.add(cost);
            }
        }

        this.setCurrencyGridCostMap(currencyGridCostMap);
    }

    protected List<CtrContractType> getEppContractTypePath(CtrContractType type)
    {
        final List<CtrContractType> types = new ArrayList<CtrContractType>();
        while (null != type) {
            types.add(type);
            type = type.getParent();
        }
        return types;
    }

    public void onClickApply()
    {
        try {
            final CtrContractVersion contractVersion = EppContractManager.instance().dao().doCreateContractObject(
                this.getStudent2EduPlanVersion(),
                this.getContractNumber(),
                this.getPriceDate(),
                this.getSelectedCost(),
                getContractType()
            );
            if (null == contractVersion) {
                throw new IllegalStateException();
            }

            this.deactivate();
            CtrContractVersionManager.openWizard(contractVersion);

        } catch (final Throwable t) {
            this.onComponentRefresh();
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }


}

