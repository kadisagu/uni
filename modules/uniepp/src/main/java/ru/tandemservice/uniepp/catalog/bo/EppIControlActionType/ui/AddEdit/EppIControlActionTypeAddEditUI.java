/* $Id:$ */
package ru.tandemservice.uniepp.catalog.bo.EppIControlActionType.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;

/**
 * @author rsizonenko
 * @since 10.11.2015
 */
@Input({
            @Bind( key = IUIPresenter.PUBLISHER_ID, binding = "holder.id"),
            @Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EppIControlActionTypeAddEditUI extends BaseCatalogItemAddEditUI<EppIControlActionType>
{
    @Override
    public String getAdditionalPropertiesPageName() {
        return this.getClass().getPackage().getName() + ".EppIControlActionTypeAdditionalProperties";
    }
}