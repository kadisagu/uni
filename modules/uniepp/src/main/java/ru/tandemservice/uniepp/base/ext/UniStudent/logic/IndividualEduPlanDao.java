/* $Id$ */
package ru.tandemservice.uniepp.base.ext.UniStudent.logic;

import ru.tandemservice.uni.base.bo.UniStudent.logic.IndividualEduPlan.IIndividualEduPlanDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * @author Andrey Andreev
 * @since 10.12.2015
 */
public class IndividualEduPlanDao extends UniBaseDao implements IIndividualEduPlanDao
{
    @Override
    public String getIndividualEduPlan(Student student)
    {
        EppStudent2EduPlanVersion version = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());
        if (version == null) return null;

        EppCustomEduPlan eduPlan = version.getCustomEduPlan();
        if (eduPlan == null) return null;

        return eduPlan.getTitleWithGrid();
    }
}