/* $Id$ */
package ru.tandemservice.uniepp.reports.ext.EduCtrDebitorsPayReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.logic.IEduCtrDebitorsPayReportDao;
import ru.tandemservice.uniepp.reports.ext.EduCtrDebitorsPayReport.logic.EppDebitorsPayReportDao;

/**
 * @author Alexey Lopatin
 * @since 16.04.2015
 */
@Configuration
public class EduCtrDebitorsPayReportExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEduCtrDebitorsPayReportDao dao()
    {
        return new EppDebitorsPayReportDao();
    }
}