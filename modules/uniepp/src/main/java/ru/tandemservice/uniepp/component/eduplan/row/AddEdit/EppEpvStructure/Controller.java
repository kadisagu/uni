package ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvStructure;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
    }

    public void onChangeParent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent component) {
        try {
            this.getDao().save(this.getModel(component));
        } catch (Throwable t) {
            // component.getDesktop().setRefreshScheduled(true);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        this.deactivate(component);
    }

}
