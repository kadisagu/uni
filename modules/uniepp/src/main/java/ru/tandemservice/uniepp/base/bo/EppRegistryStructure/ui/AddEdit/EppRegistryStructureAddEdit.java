package ru.tandemservice.uniepp.base.bo.EppRegistryStructure.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 26.08.2015
 */
@Configuration
public class EppRegistryStructureAddEdit extends BusinessComponentManager
{
	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
			.addDataSource(selectDS("parentDS", parentDSHandler()))
			.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler parentDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), EppRegistryStructure.class)
		{
			@Override
			protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
			{
				super.applyWhereConditions(alias, dql, context);
				dql.where(isNull(property(alias, EppRegistryStructure.parent())));
				dql.where(ne(property(alias, EppRegistryStructure.code()), value(EppRegistryStructureCodes.REGISTRY_ATTESTATION)));
			}
		}
			.order(EppRegistryStructure.title())
			.filter(EppRegistryStructure.title());
	}
}