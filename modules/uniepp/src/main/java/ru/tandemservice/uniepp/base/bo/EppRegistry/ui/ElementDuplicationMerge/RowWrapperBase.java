/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.ElementDuplicationMerge;

/**
 * @author Nikolay Fedorovskih
 * @since 15.05.2015
 */
public abstract class RowWrapperBase
{
    public abstract boolean isSplitterByKey();
    public abstract boolean isSplitterByTitle();
    public abstract String getSplitterStyle();

    public boolean isSplitter()
    {
        return isSplitterByKey() || isSplitterByTitle();
    }
}