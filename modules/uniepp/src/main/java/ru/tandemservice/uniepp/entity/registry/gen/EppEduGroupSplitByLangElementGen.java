package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitByLangElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент деления потоков (ин. яз)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduGroupSplitByLangElementGen extends EppEduGroupSplitBaseElement
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitByLangElement";
    public static final String ENTITY_NAME = "eppEduGroupSplitByLangElement";
    public static final int VERSION_HASH = 776107962;
    private static IEntityMeta ENTITY_META;

    public static final String L_FOREIGN_LANGUAGE = "foreignLanguage";

    private ForeignLanguage _foreignLanguage;     // Иностранный язык

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Иностранный язык. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ForeignLanguage getForeignLanguage()
    {
        return _foreignLanguage;
    }

    /**
     * @param foreignLanguage Иностранный язык. Свойство не может быть null и должно быть уникальным.
     */
    public void setForeignLanguage(ForeignLanguage foreignLanguage)
    {
        dirty(_foreignLanguage, foreignLanguage);
        _foreignLanguage = foreignLanguage;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEduGroupSplitByLangElementGen)
        {
            setForeignLanguage(((EppEduGroupSplitByLangElement)another).getForeignLanguage());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduGroupSplitByLangElementGen> extends EppEduGroupSplitBaseElement.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduGroupSplitByLangElement.class;
        }

        public T newInstance()
        {
            return (T) new EppEduGroupSplitByLangElement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "foreignLanguage":
                    return obj.getForeignLanguage();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "foreignLanguage":
                    obj.setForeignLanguage((ForeignLanguage) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "foreignLanguage":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "foreignLanguage":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "foreignLanguage":
                    return ForeignLanguage.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduGroupSplitByLangElement> _dslPath = new Path<EppEduGroupSplitByLangElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduGroupSplitByLangElement");
    }
            

    /**
     * @return Иностранный язык. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitByLangElement#getForeignLanguage()
     */
    public static ForeignLanguage.Path<ForeignLanguage> foreignLanguage()
    {
        return _dslPath.foreignLanguage();
    }

    public static class Path<E extends EppEduGroupSplitByLangElement> extends EppEduGroupSplitBaseElement.Path<E>
    {
        private ForeignLanguage.Path<ForeignLanguage> _foreignLanguage;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Иностранный язык. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitByLangElement#getForeignLanguage()
     */
        public ForeignLanguage.Path<ForeignLanguage> foreignLanguage()
        {
            if(_foreignLanguage == null )
                _foreignLanguage = new ForeignLanguage.Path<ForeignLanguage>(L_FOREIGN_LANGUAGE, this);
            return _foreignLanguage;
        }

        public Class getEntityClass()
        {
            return EppEduGroupSplitByLangElement.class;
        }

        public String getEntityName()
        {
            return "eppEduGroupSplitByLangElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
