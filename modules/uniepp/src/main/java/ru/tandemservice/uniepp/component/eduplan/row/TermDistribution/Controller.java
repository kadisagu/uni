package ru.tandemservice.uniepp.component.eduplan.row.TermDistribution;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;

import java.util.Map;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {
    private static final String ELOAD_AUDIENCE_FULL_CODE = EppLoadType.getFullCode(EppELoadType.CATALOG_CODE, EppELoadType.TYPE_TOTAL_AUDIT);

    public void onClickFill(final IBusinessComponent component) {
        this.getDao().fill(this.getModel(component));
    }

    public void onClickMoveLeft(final IBusinessComponent component) {
        this.getDao().move(this.getModel(component), component.<Integer>getListenerParameter(), -1);
    }

    public void onClickMoveRight(final IBusinessComponent component) {
        this.getDao().move(this.getModel(component), component.<Integer>getListenerParameter(), 1);
    }

    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
    }

    @Override
    public void deactivate(final IBusinessComponent component) {
        super.deactivate(component);
        UserContext.getInstance().getDesktop().refresh();
    }

    public void onClickApply(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        for (final Map.Entry<EppEpvRowTerm, Map<String, Number>> e: model.getEntries()) {
            final Double audienceLoad = (Double) e.getValue().get(Controller.ELOAD_AUDIENCE_FULL_CODE);

            Number value = null;
            for (final String fullCode: EppALoadType.FULL_CODES) {
                final Number load = e.getValue().get(fullCode);
                if ((null != load) && (load.doubleValue() > 0)) {
                    value = ((null == value ? 0.0 : value.doubleValue()) + load.doubleValue());
                }
            }

            if (null == audienceLoad) {
                e.getValue().put(Controller.ELOAD_AUDIENCE_FULL_CODE, value);
            } else if (null != value) {
                if (!audienceLoad.equals(value)) {
                    component.getUserContext().getErrorCollector().add(
                            "Общая аудиторная нагрузка на "+e.getKey().getTerm().getIntValue()+"-й семестр не соответсвует введенным значениям.",
                            Model.getLoadInputId(e.getKey().getTerm().getIntValue(), Controller.ELOAD_AUDIENCE_FULL_CODE)
                    );
                }
            }
        }

        if (component.getUserContext().getErrorCollector().hasErrors()) {
            return;
        }

        this.getDao().save(model);
        if (!component.getUserContext().getErrorCollector().hasErrors()) {
            this.deactivate(component);
        }
    }


}
