package ru.tandemservice.uniepp.component.edugroup.list.GroupListOwnerTabBase;

import org.apache.commons.collections15.Factory;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.IComponentRegion;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.component.edugroup.list.GroupList.IGroupListParameters;
import ru.tandemservice.uniepp.dao.group.EppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;

import java.util.Collections;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    public void onClickRefresh(final IBusinessComponent component)
    {
        EppRealGroupRowDAO.DAEMON.wakeUpDaemon();
    }

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        this.reactivateGroupList(component, model, false);
    }

    public void onChangeSelector(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        this.reactivateGroupList(component, model, true);
    }

    @SuppressWarnings("unchecked")
    private void reactivateGroupList(final IBusinessComponent component, final Model model, final boolean force) {
        try {
            final IComponentRegion region = component.getChildRegion("groupList");
            if (null != region) {
                if (force) { region.deactivateComponent(); }
                else { return; }
            }
        } catch (final Throwable t) {
            // do nothing
        }

        final Factory<DQLSelectBuilder> dqlFactory = this.getDao().getRelationDqlFactory(model);
        if (null == dqlFactory) { return; }

        final String groupContentComponentName = model.getGroupContentComponentName();
        final OrgUnitHolder orgUnitHolder = model.getOrgUnitHolder();
        final EppRealEduGroupCompleteLevel level = model.getLevel();

        final Object groupType = model.getGroupType();
        if (null == groupType) { return; }

        final Class<? extends EppRealEduGroup> groupClass = EppRealEduGroup.getGroupClass((Class)ClassUtils.getUserClass(groupType));
        final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation = EppRealEduGroupRow.group2descriptionMap.get(groupClass);
        if (null == relation) { return; }

        component.createChildRegion(
                "groupList",
                new ComponentActivator(
                        ru.tandemservice.uniepp.component.edugroup.list.GroupList.Model.class.getPackage().getName(),
                        Collections.<String, Object>singletonMap(
                                "parameters",
                                new IGroupListParameters() {
                                    @Override public String getGroupContentComponentName() { return groupContentComponentName; }
                                    @Override public OrgUnitHolder getOrgUnitHolder() { return orgUnitHolder; }
                                    @Override public EppRealEduGroupCompleteLevel getLevel() { return level; }
                                    @Override public Factory<DQLSelectBuilder> getDqlFactory() { return dqlFactory; }
                                    @Override public EppRealEduGroupRow.IEppRealEduGroupRowDescription getRelationDescription() { return relation; }
                                }
                        )
                )
        );

    }



}
