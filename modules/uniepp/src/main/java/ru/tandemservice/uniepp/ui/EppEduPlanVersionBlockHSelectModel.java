package ru.tandemservice.uniepp.ui;

import java.util.List;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author vdanilov
 */
public abstract class EppEduPlanVersionBlockHSelectModel extends DQLFullCheckSelectModel
{

    private static final IFormatter<IEntity> FORMATTER = new IFormatter<IEntity>() {
        @Override public String format(final IEntity source) {
            if (source instanceof EppEduPlan) { return "№" + ((EppEduPlan) source).getNumber(); }
            if (source instanceof EppEduPlanVersion) { return "№" + ((EppEduPlanVersion) source).getFullNumber(); }
            if (source instanceof EppEduPlanVersionBlock) { return ((EppEduPlanVersionBlock) source).getEducationElementSimpleTitle(); }
            throw new IllegalArgumentException(null == source ? "null" : source.getClass().getName());
        }
    };

    public EppEduPlanVersionBlockHSelectModel() {
        super("");
        this.setLabelFormatters(new IFormatter[] { EppEduPlanVersionBlockHSelectModel.FORMATTER });
    }

    @SuppressWarnings("unchecked")
    @Override protected List wrap(final List resultList) {
        return HierarchyUtil.listHierarchyNodesWithParents(resultList, false);
    }

    @Override public Object getPrimaryKey(final Object object) {
        return super.getPrimaryKey(this.getRawValue(object));
    }

    @Override public String getLabelFor(final Object object, final int columnIndex) {
        return super.getLabelFor(this.getRawValue(object), columnIndex);
    }

    @Override public int getLevel(final Object value) {
        return ((HSelectOption) value).getLevel();
    }

    @Override public boolean isSelectable(final Object value) {
        return ((HSelectOption) value).isCanBeSelected();
    }

    @Override public Object getRawValue(final Object value) {
        return value instanceof HSelectOption ? ((HSelectOption) value).getObject() : value;
    }

    @Override public Object getPackedValue(final Object value) {
        return value instanceof HSelectOption ? ((HSelectOption) value) : new HSelectOption(value, 0, true);
    }
}
