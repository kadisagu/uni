/**
 *$Id: EppEduPlanVersionManager.java 26089 2013-02-11 08:34:37Z vdanilov $
 */
package ru.tandemservice.uniepp.base.bo.EppEduGroup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;

import ru.tandemservice.uniepp.base.bo.EppEduGroup.logic.EppEduGroupList4StudentIdsDSHandler;

@Configuration
public class EppEduGroupManager extends BusinessObjectManager
{
    public static EppEduGroupManager instance() {
        return instance(EppEduGroupManager.class);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> groupList4StudentIdsDSHandler() {
        return new EppEduGroupList4StudentIdsDSHandler(getName());
    }

}
