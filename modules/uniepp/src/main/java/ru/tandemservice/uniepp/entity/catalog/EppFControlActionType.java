package ru.tandemservice.uniepp.entity.catalog;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.ImmutableList;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppFControlActionTypeGen;

/**
 * Форма итогового контроля
 */
public class EppFControlActionType extends EppFControlActionTypeGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    private static final List<String> HIDDEN_PROPERTIES = ImmutableList.of(
        EppFControlActionType.P_ACTIVE_IN_EDU_PLAN,
        EppFControlActionType.L_EPP_GROUP_TYPE,
        EppFControlActionType.P_USED_WITH_ATTESTATION,
        EppFControlActionType.P_USED_WITH_DISCIPLINES,
        EppFControlActionType.P_USED_WITH_PRACTICE,
        EppFControlActionType.P_DISABLED,
        EppFControlActionType.P_TOTAL_MARK_PRIORITY,
        EppFControlActionType.L_DEFAULT_GRADE_SCALE
    );

    @Override public void setCode(final String code) {
        if (!EppFControlActionTypeCodes.CODES.contains(code)) { throw new IllegalStateException(); }
        super.setCode(code);
    }

    @Override protected int getClassComparatorPriority() {
        return 2;
    }
    @Override protected int getInClassComparatorPriority() {
        return this.getPriority();
    }

    public EppFControlActionType() {
        this.setCatalogCode(EppFControlActionTypeGen.ENTITY_NAME);
    }

    @Override
    public boolean isFlagValue() {
        return true;
    }

    public boolean isThemeRequired() {
        return getGroup().isThemeRequired();
    }

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public Collection<String> getHiddenFields()
            {
                return HIDDEN_PROPERTIES;
            }
        };
    }

}