/* $Id: DAO.java 11311 2010-02-03 11:27:19Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniepp.component.indicators.GroupsWithIncorrectCourse;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;


/**
 * @author vip_delete
 */
public class DAO extends ru.tandemservice.uni.component.group.AbstractGroupList.DAO<Model>
{

    @Override
    protected MQBuilder getFinalGroupsListBuilder(final Model model) {

        final MQBuilder b = new MQBuilder(Student.ENTITY_CLASS, "s", new String[] { Student.group().id().s() });
        b.add(MQExpression.eq("s", Student.status().code().s(), UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
        b.add(MQExpression.eq("s", Student.archival().s(), Boolean.FALSE));
        b.add(MQExpression.notEqProperty("s", Student.course().intValue().s(), "s", Student.group().course().intValue().s()));

        final MQBuilder builder = super.getFinalGroupsListBuilder(model);
        builder.add(MQExpression.in("g", "id", b));


        return builder;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void patchGroupList(final DynamicListDataSource dataSource) {
        super.patchGroupList(dataSource);


        final List<ViewWrapper<IEntity>> patchedList = ViewWrapper.getPatchedList(dataSource);

        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(Student.class, "s");
        dql.where(in(property(Student.group().id().fromAlias("s")), UniBaseDao.ids(patchedList)));
        dql.where(eq(property(Student.status().code().fromAlias("s")), value(UniDefines.CATALOG_STUDENT_STATUS_ACTIVE)));
        dql.column(property(Student.group().id().fromAlias("s")));
        dql.column(property(Student.course().intValue().fromAlias("s")));
        dql.column(DQLFunctions.count(property(Student.id().fromAlias("s"))));
        dql.group(property(Student.group().id().fromAlias("s")));
        dql.group(property(Student.course().intValue().fromAlias("s")));
        dql.order(property(Student.group().id().fromAlias("s")));
        dql.order(property(Student.course().intValue().fromAlias("s")));


        final Map<Long, Map<Integer, Integer>> group2courseMap = new HashMap<Long, Map<Integer, Integer>>();
        final List<Object[]> rows = dql.createStatement(this.getSession()).list();
        for (final Object[] row: rows) {
            SafeMap.safeGet(group2courseMap, (Long)row[0], LinkedHashMap.class).put(((Number)row[1]).intValue(), ((Number)row[2]).intValue());
        }


        for (final ViewWrapper wrapper : patchedList) {
            wrapper.setViewProperty("courseDistributionMap", group2courseMap.get(wrapper.getId()));
        }


    }




}
