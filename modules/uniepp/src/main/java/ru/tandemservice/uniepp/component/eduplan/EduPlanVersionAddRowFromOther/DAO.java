/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddRowFromOther;

import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.IRowCustomizer;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.CheckboxColumn;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.TreeColumn;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditionalProf;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.data.*;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.coalesce;
import static org.tandemframework.hibsupport.dql.DQLFunctions.concat;

/**
 * @author nkokorina
 * Created on: 20.09.2010
 */
public class DAO extends UniBaseDao implements IDAO
{
    final static IRowCustomizer<IEppEpvRowWrapper> rowCustomizer = new SimpleRowCustomizer<IEppEpvRowWrapper>()
    {
        @Override
        public String getRowStyle(final IEppEpvRowWrapper entity)
        {
            final StringBuilder sb = new StringBuilder();
            if (entity.getRow() instanceof EppEpvHierarchyRow) { sb.append("font-weight:bold;"); }
            return sb.toString();
        }
    };

    @Override
    public void prepare(final Model model)
    {
        final EppEduPlanVersionBlock currentBlock = this.getNotNull(EppEduPlanVersionBlock.class, model.getBlockId());
        currentBlock.getEduPlanVersion().getState().check_editable(currentBlock.getEduPlanVersion());

        final EduProgramSubject programSubject = currentBlock.getEduPlanVersion().getEduPlan() instanceof EppEduPlanProf ? (((EppEduPlanProf)currentBlock.getEduPlanVersion().getEduPlan()).getProgramSubject()) : null;
        final EduProgramSpecialization programSpec = currentBlock instanceof EppEduPlanVersionSpecializationBlock ? (((EppEduPlanVersionSpecializationBlock)currentBlock).getProgramSpecialization()) : null;

        model.getBlockModel().setSource(new DQLFullCheckSelectModel(EppEduPlanVersionBlock.fullTitleExtended().s()) {
            @Override protected DQLSelectBuilder query(final String alias, final String filter) {
                final IdentifiableWrapper source = model.getBlockSourceModel().getValue();
                if (null == source) { return null; }

                final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersionBlock.class, alias)
                .joinPath(DQLJoinType.inner, EppEduPlanVersionBlock.eduPlanVersion().fromAlias(alias), "v")
                .joinPath(DQLJoinType.inner, EppEduPlanVersion.eduPlan().fromAlias("v"), "p")
                .where(ne(property(EppEduPlanVersionBlock.id().fromAlias(alias)), value(currentBlock.getId())));

                if (Model.BLOCK_SOURCE_SAME_PROGRAM_KIND.equals(source.getId())) {
                    dql.where(eq(property("p", EppEduPlan.programKind()), value(currentBlock.getEduPlanVersion().getEduPlan().getProgramKind())));
                } else if (Model.BLOCK_SOURCE_SAME_SUBJECT.equals(source.getId()) && null != programSubject) {
                    dql.where(exists(new DQLSelectBuilder()
                        .fromEntity(EppEduPlanProf.class, "epv")
                        .where(eq(property("p"), property("epv")))
                        .where(eq(property("epv", EppEduPlanProf.programSubject()), value(programSubject)))
                        .buildQuery()));
                } else if (Model.BLOCK_SOURCE_SAME_SPEC.equals(source.getId())) {
                    if (null == programSpec) {
                        dql.where(nothing());
                    } else {
                        dql.where(exists(new DQLSelectBuilder()
                            .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                            .where(eq(property(alias), property("b")))
                            .where(eq(property("b", EppEduPlanVersionSpecializationBlock.programSpecialization()), value(programSpec)))
                            .buildQuery()));
                    }
                }

                dql.joinEntity(alias, DQLJoinType.left, EppEduPlanVersionSpecializationBlock.class, "spec_b", eq(alias, "spec_b"));
                dql.joinPath(DQLJoinType.left, EppEduPlanVersionSpecializationBlock.programSpecialization().fromAlias("spec_b"), "spec");
                // Далее немного sql-магии для правильной сортировки блоков в рамках версии
                dql.order(caseExpr(isNull("spec_b"),
                                   value("a"), // сначала общий блок
                                   caseExpr(instanceOf("spec", EduProgramSpecializationRoot.class),
                                            value("b"), // затем блок общей направленности
                                            concat(value("c"), property("spec", EduProgramSpecialization.title())) // затем блоки направленностей по названию направленности
                                   )
                ));

                if (null != filter) {
                    // Фильтровать можно по номеру версии, коду направления подготовки и наименованию блока (с учетом родительского направления)
                    List<IDQLExpression> likes = new ArrayList<>();

                    // По номеру версии
                    likes.add(concat(property(alias, EppEduPlanVersionBlock.eduPlanVersion().eduPlan().number()), value("."), property("v", EppEduPlanVersion.number())));

                    // По коду и названию направления подготовки
                    dql.joinEntity(alias, DQLJoinType.left, EppEduPlanProf.class, "profPlan", eq(property("p"), property("profPlan")));
                    dql.joinPath(DQLJoinType.left, EppEduPlanProf.programSubject().fromAlias("profPlan"), "subj");
                    // ДПО же еще есть..
                    dql.joinEntity(alias, DQLJoinType.left, EppEduPlanAdditionalProf.class, "addProfPlan", eq(property("p"), property("addProfPlan")));
                    dql.joinPath(DQLJoinType.left, EppEduPlanAdditionalProf.eduProgram().fromAlias("addProfPlan"), "prog");
                    likes.add(coalesce(concat(property("subj", EduProgramSubject.subjectCode()), value(" "), property("subj", EduProgramSubject.title())),
                                       property("prog", EduProgramAdditionalProf.title())));

                    // По названию блока
                    likes.add(caseExpr(isNull("spec_b"), value(EppEduPlanVersionRootBlock.HARD_TITLE),
                                       caseExpr(instanceOf("spec", EduProgramSpecializationRoot.class), concat(property("spec", EduProgramSpecializationRoot.title()), value(EduProgramSpecializationRoot.HARD_POSTFIX_TITLE)), property("spec", EduProgramSpecialization.title()))
                    ));


                    IDQLExpression[] likeExpressions = new IDQLExpression[likes.size() * 2 - 1];
                    Iterator<IDQLExpression> iterator = likes.iterator();
                    int idx = 0;
                    while (iterator.hasNext())
                    {
                        likeExpressions[idx++] = iterator.next();
                        if (iterator.hasNext()) {
                            likeExpressions[idx++] = value(" ");
                        }
                    }
                    dql.where(likeUpper(concat(likeExpressions), value(CoreStringUtils.escapeLike(filter, true))));
                }

                dql.order(property(EppEduPlan.number().fromAlias("p")));
                dql.order(property(EppEduPlanVersion.number().fromAlias("v")));

                return dql;
            }
        });

        if (null == model.getBlockSourceModel().getValue()) {
            model.getBlockSourceModel().setupFirstValue();
        }

        if (null == model.getOverrideModel().getValue()) {
            model.getOverrideModel().setupFirstValue();
        }

        this.prepareListDataSource(model);
    }

    @Override
    public void prepareListDataSource(final Model model) {

        final StaticListDataSource<IEppEpvRowWrapper> rowDataSource = model.getRowDataSource();
        rowDataSource.clear();
        rowDataSource.getColumns().clear();
        rowDataSource.setRowCustomizer(DAO.rowCustomizer);

        CheckboxColumn checkboxColumn = new CheckboxColumn("select", "");
        checkboxColumn.setScriptResolver(
                (tableName, columnNumber, rowEntity, entity, rowIndex)
                        -> "eduPlanVersionSourceCheckChildren(event, this, '" + tableName + "', " + columnNumber + ", '" + rowEntity.getId() + "');"
        );
        checkboxColumn.setWidth(1);
        rowDataSource.addColumn(checkboxColumn);

        rowDataSource.addColumn(this.wrap(new SimpleColumn("Индекс", "index").setWidth(1)));
        rowDataSource.addColumn(this.wrap(new TreeColumn("Название", "title")));
        rowDataSource.addColumn(this.wrap(new SimpleColumn("Нагрузка", "index") {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;
                if (row.getRow() instanceof EppEpvRegistryRow) {
                    return UniEppUtils.formatLoad(row.getTotalLoad(0, EppLoadType.FULL_CODE_TOTAL_HOURS), false);
                }
                return "";
            }

        }));


        final EppEduPlanVersionBlock source = model.getBlockModel().getValue();
        if (null != source)
        {
            final IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(source.getId(), true);

            final Predicate<IEppEpvRowWrapper> rowPredicate = wrapper -> wrapper.getOwner().getId().equals(source.getId());

            rowDataSource.setupRows(EppEduPlanVersionDataDAO.filterEduPlanBlockRows(blockWrapper.getRowMap(), rowPredicate, true));
        }
    }

    @SuppressWarnings("unchecked")
    protected AbstractColumn wrap(final AbstractColumn column) {
        return column.setHeaderAlign("center").setOrderable(false).setClickable(false).setRequired(true);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(final Model model)
    {
        final Session session = this.getSession();
        final EppEduPlanVersionBlock targetBlock = this.getNotNull(EppEduPlanVersionBlock.class, model.getBlockId());
        final Long blockId = targetBlock.getId();
        final IEppEduPlanVersionDataDAO dao = IEppEduPlanVersionDataDAO.instance.get();

        // строки изменяемого плана
        final Collection<IEppEpvRowWrapper> blockRows = dao.getEduPlanVersionBlockRowMap(blockId, true).values();

        // иерархические пути изменяемого плана (только корневого блока и текущего блока)
        final Map<String, IEppEpvRow> currentPath2row = new LinkedHashMap<>();
        for (final IEppEpvRowWrapper wr : blockRows) {
            currentPath2row.put(wr.getHierarhyPath(), wr.getRow());
        }

        // блок версии-источника строк
        final EppEduPlanVersionBlock sourceBlock = model.getBlockModel().getValue();
        if (null == sourceBlock) {
            throw new ApplicationException("Необходимо указать вурсию УП, из которой нужно скопировать строки.");
        }

        // выбранные строки (которые надо копировать)
        final Set<Long> selectedIds = ((CheckboxColumn)model.getRowDataSource().getColumn("select")).getSelected();

        // id версии плана из которого импортируют строки
        final Long sourceBlockId = sourceBlock.getId();
        final IEppEpvBlockWrapper sourceVersionWrapper = dao.getEduPlanVersionBlockData(sourceBlockId, true);

        final Predicate rowPredicate = object -> {
            IEppEpvRowWrapper wrapper = (IEppEpvRowWrapper)object;

            if (!wrapper.getOwner().getId().equals(sourceBlockId)) {
                return false; // не копировать объект, если он не в выбранном блоке (например, если копируем основной блок - не копируем подчиненные)
            }

            // копируем только тогда, когда элемент подчинен одному из выбранных на форме элементов
            while (null != wrapper) {
                if (selectedIds.contains(wrapper.getId())) { return true; }
                wrapper = wrapper.getHierarhyParent();
            }
            return false;
        };

        // берем все выбранные пользователем строки с родителями
        final Collection<IEppEpvRowWrapper> sourceRowsWithParents = EppEduPlanVersionDataDAO.filterEduPlanBlockRows(sourceVersionWrapper.getRowMap(), rowPredicate, true);
        final List<IEppEpvRowWrapper> treeOrdered = (List)new PlaneTree((Collection)sourceRowsWithParents).getFlatTreeObjectsHierarchicalSorted("hierarhyParent");

        // поехали
        final boolean isReplace = Model.OVERRIDE_YES.equals(model.getOverrideModel().getValue().getId());
        final boolean isSameGrid = model.isSameGrid(sourceVersionWrapper.getVersion(), targetBlock.getEduPlanVersion());

        final DevelopGrid developGrid = targetBlock.getEduPlanVersion().getEduPlan().getDevelopGrid();
        final int termCount = getCount(DevelopGridTerm.class, DevelopGridTerm.developGrid().s(), developGrid);


        // считаем, нужно ли смещение по семестрам при копировании строк
        // проверяем, можно ли скопировать строки (нельзя, если в строке больше семестров, чем в целевом УП)
        if (!isSameGrid) {
            List<String> unableToCopyRows = new ArrayList<>();

            for (IEppEpvRowWrapper sourceWrapper : treeOrdered) {
                final Map<Integer, Map<String, Number>> termDataMap = sourceWrapper.getWrapperDataMap();
                if (termDataMap.size() > termCount)
                    unableToCopyRows.add(sourceWrapper.getRow().getTitle());
            }
            if (unableToCopyRows.size() > 0) {
                throw new ApplicationException("Невозможно скопировать дисциплины: " + unableToCopyRows.stream().collect(Collectors.joining(", ")) + ", т.к. количество семестров в них превышает количество семестров текущего УП(в).");
            }
        }

        // скопированные строки
        Map<Long, Long> rowMap = new HashMap<>();
        for (final IEppEpvRowWrapper sourceWrapper : treeOrdered)
        {
            EppEpvRow currentRow = (EppEpvRow) currentPath2row.get(sourceWrapper.getHierarhyPath());

            if (null != currentRow) {
                if (!isReplace) { continue; }

                if (targetBlock.equals(currentRow.getOwner()) || targetBlock.isRootBlock())
                {
                    // если найдена строка в том же блоке или мы копируем строку в основной блок - то обновляем ее
                    currentRow.update(this.clone((EppEpvRow)sourceWrapper.getRow(), targetBlock));
                }
                else
                {
                    // если строка в ином блоке (это значит что я вставляю строку в дочерний блок, а кандидат найден в родительском или каком-то левом блоке)
                    // либо кандидат в родительском блоке - тогда мы ничего не делаем (оставляем ее)
                    // либо кандидат в левом блоке - тогда придется создавать ее в текущем блоке

                    if (!currentRow.getOwner().isRootBlock()) {
                        // // это означает, что такая строка найдена, но в другом болоке - не корневом
                        // throw new IllegalStateException("wtf: currentRow is not in root block");
                        currentRow = null; // не нашли ничего, надо создавать строку руками в текущем блоке
                    }
                }
            }

            if (null == currentRow) {
                currentRow = this.clone((EppEpvRow)sourceWrapper.getRow(), targetBlock);
            }

            // восстанавливаем иерархию, сохраняем строку
            final IEppEpvRowWrapper sourceHierarhyParent = sourceWrapper.getHierarhyParent();
            if (null != sourceHierarhyParent) {
                currentRow.setHierarhyParent((EppEpvRow)currentPath2row.get(sourceHierarhyParent.getHierarhyPath()));
            }

            if (StringUtils.isBlank(currentRow.getStoredIndex())) { currentRow.setStoredIndex("-"); }

            session.saveOrUpdate(currentRow);
            currentPath2row.put(sourceWrapper.getHierarhyPath(), currentRow);
            rowMap.put(sourceWrapper.getId(), currentRow.getId());


            // Если сетки не совпадают, подгоняем семестры.
            if (currentRow instanceof EppEpvTermDistributedRow) {
                final EppEpvTermDistributedRow currentRowDistr = (EppEpvTermDistributedRow) currentRow;
                final Map<String, Double> dataMap = dao.getRowLoad(sourceWrapper.getId());
                final Map<Integer, Map<String, Number>> termDataMap = sourceWrapper.getWrapperDataMap();
                final Map targetTermDataMap = new HashMap<Integer, Map<String, Number>>();
                if (!isSameGrid)
                {
                    List<Integer> termNumbers = new ArrayList<>();
                    termNumbers.addAll(termDataMap.keySet());
                    Collections.sort(termNumbers);

                    Map<Integer, Integer> shiftMap = new HashMap<>();

                    for (Integer number : termNumbers) {
                        final int shift = number - termCount + termNumbers.size() - shiftMap.size() - 1;
                        if (shift < 0) shiftMap.put(number, number);
                        else shiftMap.put(number, number - shift);
                    }

                    for (Map.Entry<Integer, Map<String, Number>> termEntry : termDataMap.entrySet()) {

                        targetTermDataMap.put(shiftMap.get(termEntry.getKey()), termEntry.getValue());
                    }
                }
                else targetTermDataMap.putAll(termDataMap);
                dao.doUpdateRowLoad(Collections.<Long, IEppEduPlanVersionDataDAO.IRowLoadWrapper>singletonMap(currentRow.getId(), new IEppEduPlanVersionDataDAO.IRowLoadWrapper() {
                    @Override public EppEpvTermDistributedRow row() { return currentRowDistr; }
                    @Override public Map<String, Double> rowLoadMap() { return dataMap; }
                    @Override public Map<Integer, Map<String, Double>> rowTermLoadMap() { return targetTermDataMap; }
                    @Override public Map<Integer, Map<String, Integer>> rowTermActionMap() { return targetTermDataMap; }
                }), true);
            }
        }

        updateCopiedRows(rowMap);

        // пересчитываем индекс
        EppEduPlanVersionBlock block = (EppEduPlanVersionBlock)session.get(EppEduPlanVersionBlock.class, model.getBlockId());
        IEppEduPlanVersionDataDAO.instance.get().doUpdateEpvRowStoredIndex(block.getEduPlanVersion().getId());

    }

    /**
     * Копирование дополнительных данных строк плана.
     * Обработка новых строк и существующих с опцией "С замещением".
     * @param rowMap source row -> target row
     */
    protected void updateCopiedRows(Map<Long, Long> rowMap)
    {

    }

    private EppEpvRow clone(final EppEpvRow sourceRow, final EppEduPlanVersionBlock targetBlock)
    {
        try {
            final EppEpvRow row = sourceRow.getClass().newInstance();
            row.update(sourceRow);
            row.setOwner(targetBlock);
            row.setHierarhyParent(null);
            return row;
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }
}
