package ru.tandemservice.uniepp.component.workgraph.WorkGraphPrint;

import jxl.write.WriteException;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;

import java.io.IOException;

/**
 * @author nkokorina
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));

        try { BusinessComponentUtils.downloadDocument(this.buildDocumentRenderer(component), true); }
        catch(final Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }

        this.deactivate(component);
    }

    public IDocumentRenderer buildDocumentRenderer(final IBusinessComponent component) throws IOException, WriteException
    {
        return this.getDao().createDocument(this.getModel(component));
    }

}
