package ru.tandemservice.uniepp.component.registry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;

import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.uniepp.entity.settings.gen.EppTutorOrgUnitGen;

public final class GlobalOwnerSelectModel extends DQLFullCheckSelectModel
{
    private final IDQLSelectableQuery query;

    public GlobalOwnerSelectModel(final IDQLSelectableQuery query) {
        this.query = query;
    }

    @Override protected DQLSelectBuilder query(final String alias, final String filter)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(OrgUnit.class, alias)
        .order(property(alias, OrgUnitGen.P_TITLE))
        .where(or(
                in(property(alias, "id"), this.query),
                in(
                        property(alias, "id"),
                        new DQLSelectBuilder()
                        .fromEntity(EppTutorOrgUnit.class, "r")
                        .column(property(EppTutorOrgUnitGen.orgUnit().id().fromAlias("r")))
                        .buildQuery()
                )
        ));

        FilterUtils.applyLikeFilter(
                dql,
                filter,
                OrgUnitGen.title().fromAlias(alias),
                OrgUnitGen.shortTitle().fromAlias(alias)
        );
        return dql;
    }
}