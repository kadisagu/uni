package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.plan.EppWeekPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часть учебной недели
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWeekPartGen extends EntityBase
 implements INaturalIdentifiable<EppWeekPartGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppWeekPart";
    public static final String ENTITY_NAME = "eppWeekPart";
    public static final int VERSION_HASH = 289799645;
    private static IEntityMeta ENTITY_META;

    public static final String L_WEEK = "week";
    public static final String P_NUMBER = "number";
    public static final String L_WEEK_TYPE = "weekType";

    private EppEduPlanVersionWeekType _week;     // Неделя строки курса в учебном графике
    private int _number;     // Номер части учебной недели
    private EppWeekType _weekType;     // Тип деятельности в учебном графике

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Неделя строки курса в учебном графике. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionWeekType getWeek()
    {
        return _week;
    }

    /**
     * @param week Неделя строки курса в учебном графике. Свойство не может быть null.
     */
    public void setWeek(EppEduPlanVersionWeekType week)
    {
        dirty(_week, week);
        _week = week;
    }

    /**
     * @return Номер части учебной недели. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер части учебной недели. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Тип деятельности в учебном графике. Свойство не может быть null.
     */
    @NotNull
    public EppWeekType getWeekType()
    {
        return _weekType;
    }

    /**
     * @param weekType Тип деятельности в учебном графике. Свойство не может быть null.
     */
    public void setWeekType(EppWeekType weekType)
    {
        dirty(_weekType, weekType);
        _weekType = weekType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppWeekPartGen)
        {
            if (withNaturalIdProperties)
            {
                setWeek(((EppWeekPart)another).getWeek());
                setNumber(((EppWeekPart)another).getNumber());
            }
            setWeekType(((EppWeekPart)another).getWeekType());
        }
    }

    public INaturalId<EppWeekPartGen> getNaturalId()
    {
        return new NaturalId(getWeek(), getNumber());
    }

    public static class NaturalId extends NaturalIdBase<EppWeekPartGen>
    {
        private static final String PROXY_NAME = "EppWeekPartNaturalProxy";

        private Long _week;
        private int _number;

        public NaturalId()
        {}

        public NaturalId(EppEduPlanVersionWeekType week, int number)
        {
            _week = ((IEntity) week).getId();
            _number = number;
        }

        public Long getWeek()
        {
            return _week;
        }

        public void setWeek(Long week)
        {
            _week = week;
        }

        public int getNumber()
        {
            return _number;
        }

        public void setNumber(int number)
        {
            _number = number;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppWeekPartGen.NaturalId) ) return false;

            EppWeekPartGen.NaturalId that = (NaturalId) o;

            if( !equals(getWeek(), that.getWeek()) ) return false;
            if( !equals(getNumber(), that.getNumber()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getWeek());
            result = hashCode(result, getNumber());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getWeek());
            sb.append("/");
            sb.append(getNumber());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWeekPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWeekPart.class;
        }

        public T newInstance()
        {
            return (T) new EppWeekPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "week":
                    return obj.getWeek();
                case "number":
                    return obj.getNumber();
                case "weekType":
                    return obj.getWeekType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "week":
                    obj.setWeek((EppEduPlanVersionWeekType) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "weekType":
                    obj.setWeekType((EppWeekType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "week":
                        return true;
                case "number":
                        return true;
                case "weekType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "week":
                    return true;
                case "number":
                    return true;
                case "weekType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "week":
                    return EppEduPlanVersionWeekType.class;
                case "number":
                    return Integer.class;
                case "weekType":
                    return EppWeekType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWeekPart> _dslPath = new Path<EppWeekPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWeekPart");
    }
            

    /**
     * @return Неделя строки курса в учебном графике. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppWeekPart#getWeek()
     */
    public static EppEduPlanVersionWeekType.Path<EppEduPlanVersionWeekType> week()
    {
        return _dslPath.week();
    }

    /**
     * @return Номер части учебной недели. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppWeekPart#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Тип деятельности в учебном графике. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppWeekPart#getWeekType()
     */
    public static EppWeekType.Path<EppWeekType> weekType()
    {
        return _dslPath.weekType();
    }

    public static class Path<E extends EppWeekPart> extends EntityPath<E>
    {
        private EppEduPlanVersionWeekType.Path<EppEduPlanVersionWeekType> _week;
        private PropertyPath<Integer> _number;
        private EppWeekType.Path<EppWeekType> _weekType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Неделя строки курса в учебном графике. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppWeekPart#getWeek()
     */
        public EppEduPlanVersionWeekType.Path<EppEduPlanVersionWeekType> week()
        {
            if(_week == null )
                _week = new EppEduPlanVersionWeekType.Path<EppEduPlanVersionWeekType>(L_WEEK, this);
            return _week;
        }

    /**
     * @return Номер части учебной недели. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppWeekPart#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(EppWeekPartGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Тип деятельности в учебном графике. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppWeekPart#getWeekType()
     */
        public EppWeekType.Path<EppWeekType> weekType()
        {
            if(_weekType == null )
                _weekType = new EppWeekType.Path<EppWeekType>(L_WEEK_TYPE, this);
            return _weekType;
        }

        public Class getEntityClass()
        {
            return EppWeekPart.class;
        }

        public String getEntityName()
        {
            return "eppWeekPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
