package ru.tandemservice.uniepp;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.form.translator.Translator;
import org.tandemframework.caf.ui.config.BusinessComponentManagerBase;
import org.tandemframework.caf.ui.config.datasource.column.IIndicatorColumnBuilder;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.IPropertyPath;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.core.view.formatter.BaseRawFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.tapestry.formatters.NobrCommaFormatter;
import org.tandemframework.tapsupport.component.selection.DefaultSelectValueStyle;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectValueStyle;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import org.tandemframework.tapsupport.translator.StrongNumberTranslator;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.base.bo.EppState.util.IEppStateConfig;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

/**
 * @author vdanilov
 */
public class UniEppUtils {

    private static final DoubleFormatter LOAD_DECIMAL_FORMATTER = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS;

    public static final IFormatter<Double> LOAD_FORMATTER = source -> UniEppUtils.formatLoad(source, true);

    public static final Translator LOAD_TRANSLATOR = new StrongNumberTranslator("pattern=0.##") {
/*  Теперь можно вводить нагрузки, не кратные 0.25 (DEV-7103)
        @Override protected Object parseText(IFormComponent field, ValidationMessages messages, String text) throws ValidatorException {

            Number result = (Number)super.parseText(field, messages, text);
            if (null == result) { return null; }

            // число должно быть кратно 0.25
            double value = result.doubleValue() * 4.0;
            if (eq(value, Math.round(value))) { return result; }

            // выводим сообщение об ошибке
            throw new ValidatorException(field.getDisplayName() + " не пропорционально 0.25", ValidationConstraint.NUMBER_FORMAT);

            return super.parseText(field, messages, text);
        }
*/
        @Override protected Object[] getMessageParameters(Locale locale, String label) {
            // Но форме редактирования строки УПв некоторые названия полей выводятся со смещением, с использованием &nbsp; - нужно убрать это
            return super.getMessageParameters(locale, StringUtils.trim(StringUtils.replace(label, "&nbsp;", " ")));
        }
    };

    public static final BaseRawFormatter<String> NEW_LINE_FORMATTER = NewLineFormatter.NOBR_IN_LINES;

    public static final BaseRawFormatter<String> NOBR_COMMA_FORMATTER = NobrCommaFormatter.INSTANCE;


    public static boolean eq(final double load1, final double load2) {
        return (Math.abs(load1 - load2) < 0.01d);
    }

    public static String formatLoad(final Double load, final boolean hideZero) {
        return ((null == load) ? "" : UniEppUtils.formatLoad(load.doubleValue(), hideZero));
    }

    public static String formatLoad(final double load, final boolean hideZero) {
        return ((load <= 0) ? (hideZero ? "" : "0") : UniEppUtils.LOAD_DECIMAL_FORMATTER.format(load));
    }


    public static double wrap(final long value) {
        return (0.01d * value);
    }

    public static long unwrap(final double value) {
        return Math.round(value * 100.0d);
    }

    public static Double wrap(final Long value) {
        return (null == value ? null : UniEppUtils.wrap(value.longValue()));
    }

    public static Long unwrap(final Double value) {
        return (null == value ? null : UniEppUtils.unwrap(value.doubleValue()));
    }

    /** @return indicatorColumn для отображения состояний объекта в списке */
    public static IndicatorColumn getStateColumn() {
        return UniEppUtils.getStateColumn(null);
    }

    /** @return indicatorColumn для отображения состояний объекта в списке */
    public static IndicatorColumn getStateColumn(final String prefixPropertyPath) {
        final IndicatorColumn ic = new IndicatorColumn("Состояние", (null == prefixPropertyPath ? "state" : (prefixPropertyPath + ".state"))) {
            @Override public Item getContent(final IEntity entity) {
                final EppState state = (null == entity ? null : this.getState(entity));
                return (null == state ? null : new Item("epp-state/state-" + state.getCode(), state.getTitle()));
            }

            @Override public String getListener(IEntity entity) {
                return super.getListener(null); // без зависимости от текущего объекта
            }

            @Override public String getPermissionKey(IEntity entity) {
                return super.getPermissionKey(null); // без зависимости от текущего объекта
            }

            private EppState getState(IEntity entity) {
                if (null != prefixPropertyPath) {
                    try { entity = (IEntity) entity.getProperty(prefixPropertyPath); }
                    catch (final Exception t) { return null; }
                    if (null == entity) { return null; }
                }

                if (entity instanceof IEppStateObject) { return ((IEppStateObject) entity).getState(); }

                final Object state = entity.getProperty("state");
                return ((state instanceof EppState) ? (EppState) state : null);
            }
        };
        ic.setOrderable(false);
        ic.setClickable(false);
        ic.setRequired(true);
        return ic;
    }

    /** @return selectModel для выбора состояний объекта */
    public static ISelectModel getStateSelectModel(final IEppStateObject stateObject) {
        return getStateSelectModel(stateObject, UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppState.class));
    }

    /** @return selectModel для выбора состояний объекта */
    public static ISelectModel getStateSelectModel(final IEppStateObject stateObject, final List<EppState> stateList) {
        return new StaticSelectModel("id", "title", stateList) {
            @Override public ISelectValueStyle getValueStyle(final Object value) {
                final EppState state = (EppState) value;
                return new DefaultSelectValueStyle() {
                    @Override
                    public String getRowStyle() {
                        return "background-image: url(img/general/epp-state/state-" + state.getCode() + ".png); background-repeat:no-repeat; background-position:top left; padding-left:25px;";
                    }

                    @Override
                    public boolean isDisabled() {
                        if (null == stateObject) { return false; /*проказываем все, усли нет текущего состояния (нет объекта) */ }
                        if (null == stateObject.getId()) { return false; /* показываем все, если объект еще не создан */ }

                        final EppState currentState = stateObject.getState();
                        if (null == currentState) { return false; /* показываем все, если текущего состояния нет */ }
                        if (currentState.equals(state)) { return false; /* текущее состояние выбирать можно всегда */ }

                        IEppStateConfig stateConfig = EppStateManager.instance().eppStateDao().getStateConfig(stateObject.getClass());
                        final Collection<String> possibleTransitions = stateConfig.getPossibleTransitions(stateObject);
                        if (possibleTransitions.contains(state.getCode())) { return false; /* можно выбирать доступные */ }

                        // иначе - увы, нельзя
                        return true;
                    }
                };
            }
        };
    }

    public static ISelectModel getTitleSelectModel(final SingleValueCache<List<String>> titles) {
        return UniBaseUtils.getTitleSelectModel(titles);
    }


    /**
     * заголовок формы для строки УП (или блока УП)
     *
     * @return заголовок (напр. подготовки + индекс + название)
     */
    public static String getEppEpvRowFormTitle(final Long entityId) {
        return UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
            final IEntity entity = UniDaoFacade.getCoreDao().getNotNull(entityId);

            if (entity instanceof EppEduPlanVersionBlock) {
                // если это блок - просто показываем его направление подготовки
                return ((EppEduPlanVersionBlock) entity).getEducationElementSimpleTitle();

            } else if (entity instanceof EppEpvRow) {
                // если это строка - то показываем направление подготовки блока
                // затем вычисляем ее индекс - тут ничего не поделать - нужно грузить все строки, чтобы посчитать корректно индекс
                // и добавляем название

                final EppEpvRow row = (EppEpvRow) entity;
                final IEppEpvRowWrapper wrapper = IEppEduPlanVersionDataDAO.instance.get().getEpvRowWrapper(row, false);
                return row.getOwner().getEducationElementSimpleTitle() + " " + (null == wrapper ? "" : wrapper.getIndex()) + " " + row.getTitle();
            }

            // если это неизвестный науке зверь, проверяем можно ли из него извлечь название
            if (entity instanceof ITitled) {
                // если можно - то извлекаем
                return ((ITitled) entity).getTitle();
            }

            return "";
        });
    }

    public static String getEppWorkPlanRowFormTitle(final Long entityId) {
        return UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
            final IEntity entity = UniDaoFacade.getCoreDao().getNotNull(entityId);

            if (entity instanceof EppWorkPlanBase) {
                final EppWorkPlanBase workPlan = (EppWorkPlanBase) entity;
                return workPlan.getTitle();
            }

            if (entity instanceof EppWorkPlanRow) {
                // если это строка - то показываем ее индекс и название
                final EppWorkPlanRow row = (EppWorkPlanRow) entity;
                return StringUtils.trimToEmpty(row.getNumber()) + " " + row.getDisplayableTitle();
            }

            // если это неизвестный науке зверь, проверяем можно ли из него извлечь название
            if (entity instanceof ITitled) {
                // если можно - то извлекаем
                return ((ITitled) entity).getTitle();
            }
            return "";
        });
    }

    // действие
    public static IndicatorColumn getColumn_action(final IEntityHandler editDisabler, final String permissionkey, final String actionIcon, final String actionTitle, final String listener) {
        return (IndicatorColumn) new IndicatorColumn("", "action." + listener, listener) {
            @Override public IndicatorColumn.Item getContent(final IEntity entity) {
                if (editDisabler.handle(entity)) {
                    return null;
                }
                return new IndicatorColumn.Item(actionIcon, actionTitle);
            }
        }.setImageHeader(false).setPermissionKey(permissionkey).setDisableHandler(editDisabler).setOrderable(false).setRequired(true).setClickable(true).setWidth(1);
    }

    // error decorator
    public static String error(final String documentValue, final String targetValue) {
        return "<span style=\"color:"+ UniDefines.COLOR_CELL_ERROR+"\"><nobr>"+(documentValue.isEmpty() ? "0" : documentValue)+" ("+(targetValue.isEmpty() ? "0" : targetValue)+")</nobr></span>";
    }


    public static Double toDouble(String str) {
        if (null == (str = StringUtils.trimToNull(str))) {
            return null;
        }

        final DecimalFormat decimalFormat = new DecimalFormat();
        final char decimalSeparator = decimalFormat.getDecimalFormatSymbols().getDecimalSeparator();

        try {
            return decimalFormat.parse(str.replace('.', decimalSeparator).replace(',', decimalSeparator)).doubleValue();
        } catch (final ParseException e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }


    /**
     * Добавляет к колонке-индикатору состояния из справочника "Состояние".
     * @param indicatorColumnBuilder колонка-индикатор
     * @return колонка-индикатор с состояниями
     */
    public static IIndicatorColumnBuilder addStateIndicators(IIndicatorColumnBuilder indicatorColumnBuilder)
    {
        String iconPrefix = "epp-state/state-";
        for (EppState state: DataAccessServices.dao().getList(EppState.class))
        {
            String stateCode = state.getCode();
            indicatorColumnBuilder.addIndicator(stateCode, new IndicatorColumn.Item(iconPrefix + stateCode, state.getTitle()));
        }

        return indicatorColumnBuilder;
    }

    /**
     * Создает колонку-индикатор, содержащую состояния из справочника "Состояние".
     * @param stateCodePropertyPath цепочка свойств до кода состояния
     * @return колонка-индикатор с состояниями
     */
    public static IIndicatorColumnBuilder stateColumn(IPropertyPath<String> stateCodePropertyPath)
    {
        IIndicatorColumnBuilder indicatorColumnBuilder = BusinessComponentManagerBase.indicatorColumn("state", stateCodePropertyPath);
        addStateIndicators(indicatorColumnBuilder);
        indicatorColumnBuilder.clickable(false);
        indicatorColumnBuilder.required(true);

        return indicatorColumnBuilder;
    }
}
