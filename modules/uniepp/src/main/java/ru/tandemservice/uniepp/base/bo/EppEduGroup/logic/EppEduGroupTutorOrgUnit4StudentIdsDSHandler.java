package ru.tandemservice.uniepp.base.bo.EppEduGroup.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * Список читающих подразделений для указанных студентов
 * @author vdanilov
 */
public class EppEduGroupTutorOrgUnit4StudentIdsDSHandler extends EntityComboDataSourceHandler
{
    public static final String PARAM_STUDENT_IDS = "studentIds";

    public EppEduGroupTutorOrgUnit4StudentIdsDSHandler(String ownerId) {
        super(ownerId, OrgUnit.class);
        this.order(OrgUnit.title());
        this.filter(OrgUnit.title());
    }

    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
        super.applyWhereConditions(alias, dql, context);

        final Collection<Long> studentIds = context.get(PARAM_STUDENT_IDS);
        final DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(EppRealEduGroup.class, "g");
        final DQLSelectBuilder idsDql = EppEduGroupList4StudentIdsDSHandler.getEduGroupBuilder(registry, studentIds).column(property(EppRealEduGroup.activityPart().registryElement().owner().id().fromAlias("g")));

        dql.where(in(property(alias, "id"), idsDql.buildQuery()));
    }
}
