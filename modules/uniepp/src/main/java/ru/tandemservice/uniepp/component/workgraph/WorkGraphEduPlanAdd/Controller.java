/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphEduPlanAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionGen;

/**
 * @author vip_delete
 * @since 09.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
        this.prepareListDataSource(component);
    }

    private void prepareListDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        final DynamicListDataSource<EppEduPlanVersion> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareListDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("УП", EppEduPlanVersionGen.eduPlan().title().s()).setOrderable(false).setClickable(false).setMergeRows(true)
                                     .setMergeRowIdResolver(entity -> ((EppEduPlanVersion) entity).getEduPlan().getId().toString()));
        dataSource.addColumn(new SimpleColumn("Версия УП", EppEduPlanVersionGen.title().s()).setOrderable(false).setClickable(false));

        final HeadColumn head = new HeadColumn("courses", "Курсы");
        head.setHeaderAlign("center");
        for (final Course course : model.getCourseList())
        {
            head.addColumn(new CourseBlockColumn(course));
        }

        dataSource.addColumn(head);
        model.setDataSource(dataSource);
    }

    public void onClickApply(final IBusinessComponent component)
    {
        this.getDao().update(this.getModel(component));
        this.deactivate(component);
    }

    @SuppressWarnings("unchecked")
    public static final class CourseBlockColumn extends BlockColumn
    {
        private final Long _columnId;

        public CourseBlockColumn(final Course course)
        {
            super(course.getCode(), course.getCode(), "course");
            this._columnId = course.getId();
            this.setHeaderAlign("center");
            this.setAlign("center");
        }

        public Long getColumnId()
        {
            return this._columnId;
        }
    }
}