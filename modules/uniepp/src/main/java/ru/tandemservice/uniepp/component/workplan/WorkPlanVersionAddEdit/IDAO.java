/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionAddEdit;

import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author nkokorina
 * Created on: 10.08.2010
 */
public interface IDAO extends IUpdateable<Model>
{

}
