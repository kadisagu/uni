/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphPub;

import org.hibernate.Session;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.gen.CourseGen;
import ru.tandemservice.uni.ui.formatters.CourseRomanFormatter;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.year.IEppYearDAO;
import ru.tandemservice.uniepp.dao.year.IEppYearWorkGraphDAO;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppWeekPart;
import ru.tandemservice.uniepp.entity.pupnag.*;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRowGen;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRowWeekGen;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniepp.tapestry.richTableList.RichRangeSelection;
import ru.tandemservice.uniepp.ui.WorkGraphEduProgramSubjectListModel;

import java.util.*;
import java.util.stream.Collectors;

import static ru.tandemservice.uniepp.util.EppEduPlanVersionScheduleGridUtils.COURSE;
import static ru.tandemservice.uniepp.util.EppEduPlanVersionScheduleGridUtils.DAY;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{

    public static final String MERGE = "merge";
    public static final String VERSION = "version";

    @SuppressWarnings("unchecked")
    @Override
    public void prepare(final Model model)
    {
        model.setWorkGraph(this.getNotNull(EppWorkGraph.class, model.getWorkGraph().getId()));

        if ((model.getSelectedTab() != null) && !("workGraphTab".equals(model.getSelectedTab()) || "workGraphGridTab".equals(model.getSelectedTab())))
        {
            return;
        }

        // список недель в гуп
        model.setWeekData(IEppYearDAO.instance.get().getYearEducationWeeks(model.getWorkGraph().getYear().getId()));

        // используемые курсы в гуп
        final MQBuilder builder = new MQBuilder(EppWorkGraphRowGen.ENTITY_CLASS, "row", new String[]{EppWorkGraphRowGen.L_COURSE});
        builder.add(MQExpression.eq("row", EppWorkGraphRowGen.L_GRAPH, model.getWorkGraph()));
        builder.setNeedDistinct(true);
        final List<Course> courseList = builder.getResultList(this.getSession());
        Collections.sort(courseList, new EntityComparator<>(new EntityOrder(CourseGen.P_INT_VALUE)));
        model.setCourseListModel(new LazySimpleSelectModel<>(courseList));

        // фильтр по направлению подготовки
        model.setProgramSubjectModel(new WorkGraphEduProgramSubjectListModel(model.getWorkGraph(), model));

        // rowId -> массив точек
        // rowId -> логическая карта строки
        // заполняем эти два важных массива согласно данным из базы
        final Map<Long, int[]> row2points = new HashMap<>();
        final Map<Long, int[]> row2data = new HashMap<>();
        DAO.prepareRowDataPoints(this.getSession(), model.getWorkGraph(), row2points, row2data);

        // текущие фильтры
        final Collection<Course> courseSet = model.getCourses();
        final EduProgramSubject programSubject = model.getProgramSubject();
        final Collection<EduProgramSubject> subjectSet = programSubject == null ? null : Collections.singletonList(programSubject);

        // создаем все RangeSelectionListDataSource
        final Map<EduProgramSubject, RangeSelectionWeekTypeListDataSource> id2dataSource = new TreeMap<>((o1, o2) -> {
            return o1.getTitleWithCode().compareTo(o2.getTitleWithCode());
        }
        );
        List<ViewWrapper<EppWorkGraphWeekPart>> list = new ArrayList<>();

        long subjectRowId = 0;
        for (final Map.Entry<EduProgramSubject, Map<EppWorkGraphRow, Map<EppYearEducationWeek, EppWeekType>>> entry : IEppYearWorkGraphDAO.instance.get().getWorkGraphMapGroupByProgramSubject(model.getWorkGraph(), courseSet, subjectSet).entrySet())
        {
            EppWorkGraphWeekPart subject = new EppWorkGraphWeekPart();
            subject.setId(--subjectRowId);
            ViewWrapper<EppWorkGraphWeekPart> subjectRow = new ViewWrapper<>(subject);
            subjectRow.setViewProperty(DAY, entry.getKey().getTitleWithCode());
            subjectRow.setViewProperty("merge", "1");
            list.add(subjectRow);
            final Map<PairKey<Long, Long>, EppWeekType> dataMap = new HashMap<>();
            for (final Map.Entry<EppWorkGraphRow, Map<EppYearEducationWeek, EppWeekType>> subEntry : entry.getValue().entrySet()) {
                EppWorkGraphRow eppWorkGraphRow = subEntry.getKey();

                final List<EppWorkGraphWeekPart> epvWeekPartList = UniDaoFacade.getCoreDao().getList(
                        EppWorkGraphWeekPart.class,
                        EppWorkGraphWeekPart.week().row(), eppWorkGraphRow, "number"
                );
                EppWorkGraphWeekPart firstWeek;
                if (epvWeekPartList.size() > 0 )
                {
                    firstWeek = epvWeekPartList.get(0);
                }
                else
                {
                    firstWeek = new EppWorkGraphWeekPart();
                    firstWeek.setId(--subjectRowId);
                }

                ViewWrapper<EppWorkGraphWeekPart> row = new ViewWrapper<>(firstWeek);
                for (EppWorkGraphWeekPart weekPart: epvWeekPartList)
                {
                    row.setViewProperty(getWeekCode(weekPart.getWeek()), weekPart.getWeekType().getAbbreviationView());
                }

                row.setViewProperty(COURSE, eppWorkGraphRow.getCourse().getIntValue());
                row.setViewProperty(VERSION,
                        UniDaoFacade.getCoreDao().getList(
                                EppWorkGraphRow2EduPlan.class,
                                EppWorkGraphRow2EduPlan.row(), eppWorkGraphRow)
                                .get(0).getEduPlanVersion().getFullTitleWithEducationsCharacteristics());

                list.add(row);


                for (final Map.Entry<EppYearEducationWeek, EppWeekType> deepEntry : subEntry.getValue().entrySet()) {
                    dataMap.put(PairKey.create(eppWorkGraphRow.getId(), deepEntry.getKey().getId()), deepEntry.getValue());
                }
            }

            final SimpleListDataSource<ViewWrapper<EppWorkGraphRow>> dataSource = new SimpleListDataSource<>(entry.getValue().keySet()
                    .stream().map(a -> new ViewWrapper<>(a)).collect(Collectors.toList()));
            dataSource.getEntityList().forEach(a -> a.setViewProperty(VERSION,
                    UniDaoFacade.getCoreDao().getList(
                            EppWorkGraphRow2EduPlan.class,
                            EppWorkGraphRow2EduPlan.row(), a.getEntity())
                            .get(0).getEduPlanVersion().getFullTitleWithEducationsCharacteristics()
            ));
            final RangeSelectionWeekTypeListDataSource<ViewWrapper<EppWorkGraphRow>> rangeModel = new RangeSelectionWeekTypeListDataSource<>(dataSource);
            rangeModel.setDataMap(dataMap);
            id2dataSource.put(entry.getKey(), rangeModel);

            rangeModel.setRow2points(row2points);
            rangeModel.setRow2data(row2data);
        }

        model.setId2dataSource(id2dataSource);

        // заполнение легенды
        model.setWeekTypeLegendList(IEppEduPlanDAO.instance.get().getWeekTypeLegendRowList(null));

        SimpleListDataSource<ViewWrapper<EppWorkGraphWeekPart>> dataSource = new SimpleListDataSource<>();
        model.setDataSource(dataSource);

        dataSource.addColumn(getHeadColumn(model));
        dataSource.setEntityCollection(list);
        dataSource.setCountRow(list.size());
        model.setColumnMergeResolver((iEntity ->  (((ViewWrapper<EppWorkGraphWeekPart>)iEntity).getViewProperty(MERGE)) != null ? "1" : null));
    }

    // Component specific API function

    private static void prepareRowDataPoints(final Session session, final EppWorkGraph workGraph, final Map<Long, int[]> row2points, final Map<Long, int[]> row2data)
    {
        final MQBuilder builder = new MQBuilder(EppWorkGraphRowWeekGen.ENTITY_CLASS, "rw");
        builder.addJoin("rw", EppWorkGraphRowWeekGen.L_ROW, "r");
        builder.add(MQExpression.eq("r", EppWorkGraphRowGen.L_GRAPH, workGraph));
        final List<EppWorkGraphRowWeek> list = builder.getResultList(session);

        // row -> номер части в году -> [минимальный номер недели,максимальный номер недели]
        final Map<Long, Map<Integer, int[]>> map = new HashMap<>();
        final Map<Course, Integer[]> gridDetailMap = IDevelopGridDAO.instance.get().getDevelopGridDetail(workGraph.getDevelopGrid());
        for (final EppWorkGraphRowWeek item : list)
        {
            // получаем данные
            final Integer[] gridDetail = gridDetailMap.get(item.getRow().getCourse());
            final Long rowId = item.getRow().getId();
            final int term = item.getTerm().getIntValue();
            final int weekNumber = item.getWeek();
            int partNumber = 0;
            while ((partNumber < gridDetail.length) && ((null == gridDetail[partNumber]) || (term != gridDetail[partNumber]))) {
                partNumber++;
            }
            partNumber++;


            // сохраняем в мапе
            Map<Integer, int[]> partMap = map.get(rowId);
            if (partMap == null)
            {
                map.put(rowId, partMap = new TreeMap<>());
            }

            final int[] points = partMap.get(partNumber);
            if (points == null)
            {
                partMap.put(partNumber, new int[]{weekNumber, weekNumber});
            }
            else if (weekNumber < points[0])
            {
                points[0] = weekNumber;
            }
            else if (weekNumber > points[1])
            {
                points[1] = weekNumber;
            }

            final List<EppWorkGraphWeekPart> epvWeekPartList = UniDaoFacade.getCoreDao().getList(
                    EppWorkGraphWeekPart.class,
                    EppWorkGraphWeekPart.week(), item
            );

        }

        for (final Map.Entry<Long, Map<Integer, int[]>> entry : map.entrySet())
        {
            final Map<Integer, int[]> partMap = entry.getValue();
            final int[] points = new int[partMap.size() * 2];
            int i = 0;
            for (final int[] pair : partMap.values())
            {
                points[i++] = pair[0] - 1;
                points[i++] = pair[1] - 1;
            }

            row2points.put(entry.getKey(), points);
            row2data.put(entry.getKey(), new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, points).getData());
        }
    }

    public HeadColumn getHeadColumn(Model model) {
        HeadColumn header = new HeadColumn("header", "График учебного процесса");

        IMergeRowIdResolver corseResolver = entity -> String.valueOf(((ViewWrapper<EppWeekPart>) entity).getViewProperty(COURSE));
        SimpleColumn weekNum = new SimpleColumn("День недели", "wn");
        weekNum.setWidth(24)
                .setAlign("center").
                setVerticalHeader(true).
                setDisabled(true)
                .setMergeRowIdResolver(corseResolver);
        ;
        HeadColumn weekNumHead = new HeadColumn("", "");
        weekNumHead.setWidth(24);
        weekNumHead.addColumn(weekNum);
        header.addColumn(weekNumHead);

        HeadColumn courseBlank = new HeadColumn("courseBlank", "");
        AbstractColumn course = new PublisherLinkColumn("Курс", COURSE)
                .setRequired(true)
                .setWidth(56)
                .setAlign("right")
                .setMergeRowIdResolver(corseResolver);
        courseBlank.addColumn(course);
        header.addColumn(courseBlank);

        HeadColumn monthHead = null;
        String lastHeadName = null;

        for (final EppYearEducationWeek week : model.getWeekData())
        {
            final String headName = "month."+week.getDate().getMonth();
            if ((null == monthHead) || (!lastHeadName.equals(headName))) {
                if (monthHead != null) {
                    header.addColumn(monthHead);
                }
                if (week.getDate().getMonth() <=0) {
                    continue;
                }
                monthHead = new HeadColumn(headName, RussianDateFormatUtils.getMonthName(week.getDate(), true));
                lastHeadName = headName;
            }
            HeadColumn weekColumn = new HeadColumn("week."+week.getNumber(), week.getTitle());
            weekColumn.setWidth(24);
            weekColumn.setVerticalHeader(true);

            final String weekKey = getWeekCode(week);
            SimpleColumn column = new SimpleColumn(String.valueOf(week.getNumber()), weekKey);
            column.setWidth(24).setAlign("center");
            column.setMergeRowIdResolver(entity -> String.valueOf(((ViewWrapper<EppWeekPart>) entity).getViewProperty(COURSE)) + "-" + String.valueOf(((ViewWrapper<EppWeekPart>) entity).getViewProperty(weekKey)));
            weekColumn.addColumn(column);
            monthHead.addColumn(weekColumn);
        }

        if (monthHead != null) {
            header.addColumn(monthHead);
        }

        SimpleColumn vsimple = new SimpleColumn("", VERSION);
        vsimple.setWidth(300);
        HeadColumn version = new HeadColumn(VERSION, "Версия УП");
        version.setWidth(300)
                .setAlign("center").
                setDisabled(true)
                .setMergeRowIdResolver(corseResolver);
        ;
        version.addColumn(vsimple);
        HeadColumn versionHead = new HeadColumn("", "");
        versionHead.setWidth(300);
        versionHead.addColumn(version);
        header.addColumn(versionHead);

        return header;
    }

    private String getWeekCode(EppYearEducationWeek week) {
        return "w" + week.getNumber();
    }

    private static String getWeekCode(EppWorkGraphRowWeek week)
    {
        return "w" + week.getWeek();
    }
}
