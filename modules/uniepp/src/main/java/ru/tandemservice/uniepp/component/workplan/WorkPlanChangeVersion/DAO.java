/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.workplan.WorkPlanChangeVersion;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanVersionGen;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author nkokorina
 * Created on: 12.08.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private final List<EppControlActionType> controlActionTypes = IEppWorkPlanDAO.instance.get().getActiveControlActionTypes(null);

    @SuppressWarnings("unchecked")
    @Override
    public void prepare(final Model model)
    {
        final EppWorkPlan workPlan = this.getNotNull(EppWorkPlan.class, model.getWorkPlanId());

        model.setCurrentWorkPlan(workPlan);

        final List<EppWorkPlanBase> workPlanList = new ArrayList<>();
        workPlanList.add(workPlan);
        workPlanList.addAll(this.getList(EppWorkPlanVersion.class, EppWorkPlanVersionGen.parent().toString(), workPlan));

        model.setVersionSelectModel(new LazySimpleSelectModel<>(workPlanList));

        model.setWorkPlanWrapperMap(IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(UniBaseDao.ids(workPlanList)));

        final Map<Long, IEppWorkPlanRowWrapper> workPlanRows = model.getWorkPlanWrapperMap().get(model.getCurrentWorkPlan().getId()).getRowMap();

        final StaticListDataSource<IEppWorkPlanRowWrapper> rowDataSource = model.getRowDataSource();
        final List<IEppWorkPlanRowWrapper> rowList = new ArrayList<>();
        rowList.addAll(CollectionUtils.select(workPlanRows.values(), IEppWorkPlanRowWrapper.DISCIPLINE_ROWS));
        rowList.addAll(CollectionUtils.select(workPlanRows.values(), IEppWorkPlanRowWrapper.NON_DISCIPLINE_ROWS));
        rowDataSource.setupRows(rowList);

        rowDataSource.getColumns().clear();
        rowDataSource.addColumn(this.wrap(new SimpleColumn("Индекс", "number").setWidth(1)));
        rowDataSource.addColumn(this.wrap(new SimpleColumn("Название, формы контроля", "properties")
        {
            @Override
            public String getContent(final IEntity entity)
            {
                final IEppWorkPlanRowWrapper row = ((IEppWorkPlanRowWrapper)entity);
                if (row.getRow() instanceof EppWorkPlanRegistryElementRow)
                {
                    final EppWorkPlanRegistryElementRow r = (EppWorkPlanRegistryElementRow)row.getRow();
                    final String actions = DAO.this.getActionsString(row);
                    return row.getTitle() + ", " + r.getRegistryElement().getFormattedLoad() + ", " + actions;
                }
                return row.getTitle();
            }

        }));
        rowDataSource.addColumn(this.wrap(new SimpleColumn("Читающее\nподразделение", "owner")
        {
            @Override
            public String getContent(final IEntity entity)
            {
                final OrgUnit owner = ((IEppWorkPlanRowWrapper)entity).getOwner();
                return (null == owner ? "" : owner.getShortTitle());
            }
        }));

    }

    @Override
    public void update(final Model model)
    {
        final EppWorkPlanBase currentWorkPlan = model.getCurrentWorkPlan();
        final Integer currentTerm = currentWorkPlan.getTerm().getIntValue();

        final Map<Long, Set<EppStudent2WorkPlan>> currentStudentWorkPlanMap = IEppWorkPlanDAO.instance.get().getStudentEpvWorkPlanMap(
            CommonDAO.ids(IEppEduPlanDAO.instance.get().getActiveStudentEduplanVersionRelationMap(model.getStudentIds()).values()), true
        );

        final Map<Long, Set<EppWorkPlanBase>> student2workPlanSet = SafeMap.get(HashSet.class);
        for (final Entry<Long, Set<EppStudent2WorkPlan>> entry : currentStudentWorkPlanMap.entrySet())
        {
            final Set<EppWorkPlanBase> set = student2workPlanSet.get(entry.getKey());
            for (final EppStudent2WorkPlan rel : entry.getValue())
            {
                if (currentTerm.equals(rel.getWorkPlan().getTerm().getIntValue())) {
                    set.add(currentWorkPlan);
                } else {
                    set.add(rel.getWorkPlan());
                }
            }
        }

        IEppWorkPlanDAO.instance.get().doUpdateStudentEpvWorkPlan(student2workPlanSet);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareRowsData(final Model model)
    {
        final StaticListDataSource<IEppWorkPlanRowWrapper> rowDataSource = model.getRowDataSource();

        final EppWorkPlanBase currentWorkPlan = model.getCurrentWorkPlan();
        if (null == currentWorkPlan) { throw new IllegalStateException(); }

        final Map<Long, IEppWorkPlanRowWrapper> workPlanRows = model.getWorkPlanWrapperMap().get(currentWorkPlan.getId()).getRowMap();
        final List<IEppWorkPlanRowWrapper> rowList = new ArrayList<>();
        rowList.addAll(CollectionUtils.select(workPlanRows.values(), IEppWorkPlanRowWrapper.DISCIPLINE_ROWS));
        rowList.addAll(CollectionUtils.select(workPlanRows.values(), IEppWorkPlanRowWrapper.NON_DISCIPLINE_ROWS));
        rowDataSource.setupRows(rowList);
    }

    private AbstractColumn wrap(final AbstractColumn column)
    {
        return column.setHeaderAlign("center").setOrderable(false).setClickable(false).setRequired(true);
    }

    private String getActionsString(final IEppWorkPlanRowWrapper row)
    {
        final List<String> actionList = new ArrayList<>();
        for (final EppControlActionType tp : this.controlActionTypes)
        {
            final int actionSize = row.getActionSize(tp.getFullCode());
            if (actionSize > 0)
            {
                final String size = (actionSize > 1) ? "(" + String.valueOf(actionSize) + ")" : "";
                actionList.add(tp.getShortTitle() + size);
            }
        }

        final StringBuilder sb = new StringBuilder();
        for (final Iterator<String> i = actionList.iterator(); i.hasNext();)
        {
            sb.append(i.next()).append(i.hasNext() ? ", " : "");
        }
        return sb.toString();
    }
}
