/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.ElementDuplicationMerge;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
/**
 * @author Nikolay Fedorovskih
 * @since 14.05.2015
 */
@Configuration
public class EppRegistryElementDuplicationMerge extends BusinessComponentManager
{
    public static final String ELEMENT_DS = "regElementDS";
    public static final String ELEMENT_TYPE_DS_HANDLER = "typeDS";
    public static final String ELEMENT_PARTS_COUNT_DS_HANDLER = "partsCountDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(ELEMENT_TYPE_DS_HANDLER, elementTypeDSHandler()))
                .addDataSource(EppStateManager.instance().eppStateDSConfig())
                .addDataSource(selectDS(ELEMENT_PARTS_COUNT_DS_HANDLER, partsCountDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler elementTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppRegistryStructure.class)
                .where(EppRegistryStructure.parent(), (Object) null)
                .filter(EppRegistryStructure.title())
                .order(EppRegistryStructure.id());
    }

    @Bean
    public IDefaultComboDataSourceHandler partsCountDSHandler()
    {
		return new SimpleTitledComboDataSourceHandler(getName())
		{
			@Override
			protected DSOutput execute(DSInput input, ExecutionContext context)
			{
                Set ids = input.getPrimaryKeys();
                if (ids != null && !ids.isEmpty())  {
                    DSOutput output = new DSOutput(input);
                    for (Object id : ids) {
                        output.addRecord(new IdentifiableWrapper(((Number) id).longValue(), String.valueOf(id)));
                    }
                    return output;
                }

				DQLSelectBuilder builder = new DQLSelectBuilder()
					.fromEntity(EppRegistryElement.class, "reg")
					.column(property("reg", EppRegistryElement.parts()))
					.distinct()
					.order(property("reg", EppRegistryElement.parts()));

                FilterUtils.applySelectFilter(builder, "reg", EppRegistryElement.parent(), context.get("type"));
                FilterUtils.applySelectFilter(builder, "reg", EppRegistryElement.owner(), context.get("owner"));

				List<Integer> partsCountList = builder.createStatement(context.getSession()).list();

				DSOutput output = new DSOutput(input);
                if (partsCountList.isEmpty())
                    output.addRecord(new IdentifiableWrapper(1L, String.valueOf(1)));
                else
                {
                    for (int parts : partsCountList)
                        output.addRecord(new IdentifiableWrapper((long) parts, String.valueOf(parts)));
                }
				return output;
			}
		};
    }
}