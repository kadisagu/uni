/* $Id$ */
package ru.tandemservice.uniepp.dao.student;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import ru.tandemservice.uni.base.bo.UniStudent.daemon.UniStudentDevelopPeriodDaemonBean;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 17.02.2016
 */
public class EppStudentDaemonBean extends UniStudentDevelopPeriodDaemonBean
{
    @Override
    protected void initDao()
    {
        super.initDao();

        // создание/изменение/удаление объектов Учебный план студента
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppStudent2EduPlanVersion.class, DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppStudent2EduPlanVersion.class, DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, EppStudent2EduPlanVersion.class, DAEMON.getAfterCompleteWakeUpListener());
    }

    @Override
    protected void addAdditionalExpression(DQLSelectBuilder builder)
    {
        super.addAdditionalExpression(builder);
        builder
                .joinEntity("s", DQLJoinType.left, EppStudent2EduPlanVersion.class, "s2epv", and(
                        eq(property("s.id"), property("s2epv", EppStudent2EduPlanVersion.student().id())),
                        isNull(property("s2epv", EppStudent2EduPlanVersion.removalDate()))
                ))
                .joinEntity("s2epv", DQLJoinType.left, EppCustomEduPlan.class, "cep", eq(property("cep.id"), property("s2epv", EppStudent2EduPlanVersion.customEduPlan().id())))
                .joinEntity("cep", DQLJoinType.left, DevelopGrid.class, "cep_dg", eq(property("cep_dg.id"), property("cep", EppCustomEduPlan.developGrid().id())))

                .joinEntity("s2epv", DQLJoinType.left, EppEduPlanVersion.class, "epv", eq(property("epv.id"), property("s2epv", EppStudent2EduPlanVersion.eduPlanVersion().id())))
                .joinEntity("epv", DQLJoinType.left, EppEduPlan.class, "ep", eq(property("ep.id"), property("epv", EppEduPlanVersion.eduPlan().id())))
                .joinEntity("ep", DQLJoinType.left, DevelopGrid.class, "ep_dg", eq(property("ep_dg.id"), property("ep", EppEduPlan.developGrid().id())));
    }

    @Override
    protected IDQLExpression[] getDevelopPeriodColumns()
    {
        return new IDQLExpression[]{
                property("s", Student.developPeriod()),
                property("cep_dg", DevelopGrid.developPeriod()),
                property("ep_dg", DevelopGrid.developPeriod()),
                property("eou", EducationOrgUnit.developPeriod())
        };
    }
}
