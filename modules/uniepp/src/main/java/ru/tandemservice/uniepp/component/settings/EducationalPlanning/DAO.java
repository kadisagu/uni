package ru.tandemservice.uniepp.component.settings.EducationalPlanning;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;

/**
 * 
 * @author nkokorina
 * @since 26.02.2010
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(final Model model)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppYearEducationProcess.class, "e")
                .column(DQLExpressions.property("e"))
                .order(DQLExpressions.property("e", EppYearEducationProcess.P_TITLE));
        UniBaseUtils.createPage(model.getDataSource(), dql, this.getSession());
    }
}
