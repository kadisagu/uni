package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_2x9x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppEduGroupSplitBaseElement

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_edu_group_split_element_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eppedugroupsplitbaseelement"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("splitvariant_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppEduGroupSplitBaseElement");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppEduGroupSplitByLangElement

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_edu_group_split_el_lang_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_9bf380ab"), 
				new DBColumn("personforeignlanguage_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppEduGroupSplitByLangElement");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppEduGroupSplitBySexElement

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_edu_group_split_el_sex_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_6f89b499"), 
				new DBColumn("sex_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppEduGroupSplitBySexElement");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppEduGroupSplitVariant

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_c_edu_group_split_var_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eppedugroupsplitvariant"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppEduGroupSplitVariant");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppRegElementSplitByTypeEduGroupRel

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_reg_el_split_load_type_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_772629b2"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("registryelement_id", DBType.LONG).setNullable(false), 
				new DBColumn("grouptype_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppRegElementSplitByTypeEduGroupRel");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppRegistryElement

		// создано свойство eduGroupSplitVariant
		{
			// создать колонку
			tool.createColumn("epp_reg_base_t", new DBColumn("edugroupsplitvariant_id", DBType.LONG));

		}

		// создано обязательное свойство eduGroupSplitByType
		{
			// создать колонку
			tool.createColumn("epp_reg_base_t", new DBColumn("edugroupsplitbytype_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultEduGroupSplitByType = false;
			tool.executeUpdate("update epp_reg_base_t set edugroupsplitbytype_p=? where edugroupsplitbytype_p is null", defaultEduGroupSplitByType);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_reg_base_t", "edugroupsplitbytype_p", false);

		}


    }
}