/* $Id$ */
package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * @author Nikolay Fedorovskih
 * @since 13.03.2015
 */
public class MS_uniepp_2x7x2_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.16"),
                new ScriptDependency("org.tandemframework.shared", "1.7.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Впендюрили блок "Научно-исследовательская работа" перед блоком "Государственная итоговая аттестация"
        // В итоге сместились коды и приоритеты

        if (tool.hasResultRows("select id from epp_c_plan_struct_t where code_p=? and title_p=?", "3pg.block.03", "Научно-исследовательская работа")
                || !tool.hasResultRows("select id from epp_c_plan_struct_t where code_p=?", "3pg.block.04")){
            return;
        }

        // Смещаем коды
        tool.executeUpdate("update epp_c_plan_struct_t set code_p=? where code_p=?", "3pg.block.05", "3pg.block.04");
        tool.executeUpdate("update epp_c_plan_struct_t set code_p=? where code_p=?", "3pg.block.04", "3pg.block.03");

        // Смещаем приоритеты
        Number priority = (Number) tool.getUniqueResult("select priority_p from epp_c_plan_struct_t where code_p=?", "3pg.block.04");
        tool.executeUpdate("update epp_c_plan_struct_t set priority_p=priority_p+1 where priority_p >= ? and code_p like ?", priority, "3pg.block.%");

        // Добавляем новый элемент
        Long parentId = tool.getNumericResult("select id from epp_c_plan_struct_t where code_p=?", "3pg.block");
        MigrationUtils.BatchInsertBuilder ins = new MigrationUtils.BatchInsertBuilder("code_p", "parent_id", "shorttitle_p", "priority_p", "title_p");
        ins.setEntityCode(tool, "eppPlanStructure");
        ins.addRow("3pg.block.03", parentId, "Б3", priority, "Научно-исследовательская работа");
        ins.executeInsert(tool, "epp_c_plan_struct_t");
    }
}