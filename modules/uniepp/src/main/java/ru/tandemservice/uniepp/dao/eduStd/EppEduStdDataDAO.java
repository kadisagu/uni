package ru.tandemservice.uniepp.dao.eduStd;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.eduStd.data.EppStdRowWrapper;
import ru.tandemservice.uniepp.dao.eduStd.data.EppStdWrapper;
import ru.tandemservice.uniepp.dao.eduStd.data.IEppStdRowWrapper;
import ru.tandemservice.uniepp.dao.eduStd.data.IEppStdWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation;
import ru.tandemservice.uniepp.entity.std.data.IEppStdRow;

/**
 * @author vdanilov
 */
public class EppEduStdDataDAO extends UniBaseDao implements IEppEduStdDataDAO
{

    /**
     * создает новую оболочку ГОС (метод для переопределения)
     * @param std ГОС
     * @return новая оболочка для ГОС
     */
    protected EppStdWrapper newEppStdWrapper(final EppStateEduStandard std)
    {
        return new EppStdWrapper(std);
    }

    /**
     * создает новую оболочку строки ГОС (метод для переопределения)
     * @param stdWrapper оболочка ГОС
     * @param parentRowWrapper оболочка вышестоящей строки
     * @param row сторка
     * @return новая оболочка строки ГОВ
     */
    protected EppStdRowWrapper newEppStdRowWrapper(final EppStdWrapper stdWrapper, final EppStdRowWrapper parentRowWrapper, final EppStdRow row)
    {
        return new EppStdRowWrapper(stdWrapper, parentRowWrapper, row);
    }

    /*
     *  сравнивает строки УП между собой
     */
    private static final Comparator<EppStdRow> ROW_COMPARATOR = new Comparator<EppStdRow>()
    {
        @Override
        public int compare(final EppStdRow o1, final EppStdRow o2)
        {
            final int diff = (o1.getComparatorClassIndex() - o2.getComparatorClassIndex());
            if (0 != diff)
            {
                return diff;
            }

            final String v1 = o1.getComparatorString();
            final String v2 = o2.getComparatorString();
            if (v1.length() == v2.length())
            {
                return v1.compareTo(v2);
            }

            final int max = Math.max(v1.length(), v2.length());
            final String p1 = StringUtils.leftPad(v1, max, '0');
            final String p2 = StringUtils.leftPad(v2, max, '0');
            return p1.compareTo(p2);
        }
    };

    /*
     * заполняет map оболочек ГОС для переданных идентификаторов (которых гарантированно мало)
     */
    private void getEduStdDataMapInternal(final Map<Long, EppStdWrapper> result, final Collection<Long> eppEduStdIdsSlice, final boolean loadDetails)
    {
        final Session session = this.getSession();
        /* грузим данные самих ГОСов */{
            final MQBuilder builder = new MQBuilder(EppStateEduStandard.ENTITY_CLASS, "std");
            builder.add(MQExpression.in("std", "id", eppEduStdIdsSlice));
            for (final EppStateEduStandard std : builder.<EppStateEduStandard> getResultList(session))
            {
                result.put(std.getId(), this.newEppStdWrapper(std));
            }
        }

        /* грузим строки ГОС и данные по ним */{
            final Map<Long, Map<Long, EppStdRow>> rowData = SafeMap.get(HashMap.class);

            final MQBuilder builder = new MQBuilder(EppStdRow.ENTITY_CLASS, "row");
            builder.addJoinFetch("row", EppStdRow.owner().toString(), "owner");
            builder.addJoin("owner", EppStateEduStandardBlock.stateEduStandard().toString(), "std");
            builder.add(MQExpression.in("std", "id", eppEduStdIdsSlice));

            final List<EppStdRow> rows = builder.getResultList(session);
            for (final EppStdRow row : rows)
            {
                rowData.get(row.getOwner().getStateEduStandard().getId()).put(row.getId(), row);
            }

            //final Map<Long, Map<String, Object>> rowDetailMap = (loadDetails ? this.getEduStdDataMapInternal_getRowTermDetailMap(eppEduStdIdsSlice) : new HashMap<Long, Map<String, Object>>());
            // TODO пока тупо заточено под компетенции
            final Map<Long, Set<EppStateEduStandardSkill>> rowSkillMap = loadDetails ? this.getEduStdDataRowSkillMap(eppEduStdIdsSlice) : new HashMap<Long, Set<EppStateEduStandardSkill>>();
            for (final Map.Entry<Long, Map<Long, EppStdRow>> e : rowData.entrySet())
            {
                final EppStdWrapper stdWrapper = result.get(e.getKey());

                // сначала грузим иерархически все строки в оболочку версии
                this.getEduStdDataMapInternal_collectData(stdWrapper, null, e.getValue());

                // затем для всех строк (которые уже загрузились) указываем данные
                for (final IEppStdRowWrapper rowWrapper : stdWrapper.getRowMap().values())
                {
                    final Set<EppStateEduStandardSkill> skills = rowSkillMap.get(rowWrapper.getId());
                    if (null != skills)
                    {
                        ((EppStdRowWrapper)rowWrapper).getSkillSet().addAll(skills);
                    }
                }
            }
        }
    }

    private Map<Long, Set<EppStateEduStandardSkill>> getEduStdDataRowSkillMap(final Collection<Long> eppEduStdIdsSlice)
    {
        final Session session = this.getSession();

        final Map<Long, Set<EppStateEduStandardSkill>> rowDetailMap = SafeMap.get(HashSet.class);

        final MQBuilder builder = new MQBuilder(EppStdRow2SkillRelation.ENTITY_NAME, "row2skr");
        builder.add(MQExpression.in("row2skr", EppStdRow2SkillRelation.row().owner().stateEduStandard().id().s(), eppEduStdIdsSlice));
        final List<EppStdRow2SkillRelation> rows = builder.getResultList(session);

        for (final EppStdRow2SkillRelation row : rows)
        {
            final Long rowId = row.getRow().getId();
            rowDetailMap.get(rowId).add(row.getSkill());
        }

        return rowDetailMap;
    }

    /*
     * иерархически собирает строки ГОС
     */
    private void getEduStdDataMapInternal_collectData(final EppStdWrapper stdWrapper, final EppStdRowWrapper parentRowWrapper, final Map<Long, EppStdRow> stdRows)
    {
        final Map<Long, IEppStdRowWrapper> map = stdWrapper.getRowMap();
        final IEppStdRow parentRow = null == parentRowWrapper ? null : parentRowWrapper.getRow();

        final List<EppStdRow> selectedRows = new ArrayList<EppStdRow>();
        for (final EppStdRow row : stdRows.values())
        {
            final boolean isSameParent = (null == parentRow ? (null == row.getHierarhyParent()) : parentRow.equals(row.getHierarhyParent()));
            if (isSameParent)
            {
                selectedRows.add(row);
            }
        }

        if (selectedRows.size() > 0)
        {
            Collections.sort(selectedRows, EppEduStdDataDAO.ROW_COMPARATOR);
            for (final EppStdRow row : selectedRows)
            {
                final EppStdRowWrapper rowWrapper = this.newEppStdRowWrapper(stdWrapper, parentRowWrapper, row);
                if (null != map.put(rowWrapper.getId(), rowWrapper))
                {
                    throw new IllegalStateException("Recursion detected");
                }
                if (null != parentRowWrapper)
                {
                    parentRowWrapper.getChildRows().add(rowWrapper);
                }
                this.getEduStdDataMapInternal_collectData(stdWrapper, rowWrapper, stdRows);
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<Long, IEppStdWrapper> getEduStdDataMap(final Collection<Long> eppEduStdIds, final boolean loadDetails)
    {
        Debug.begin("EppEduStdDataDAO.getEduStdDataMap(size=" + eppEduStdIds.size() + ")");
        try
        {
            Debug.begin("EppEduStdDataDAO.getEduStdDataMap.preload");
            try
            {
                this.getList(EppPlanStructure.class);
            }
            finally
            {
                Debug.end();
            }

            final Map<Long, EppStdWrapper> result = new HashMap<Long, EppStdWrapper>(eppEduStdIds.size());
            final BatchUtils.Action<Long> action = new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(final Collection<Long> ids)
                {
                    EppEduStdDataDAO.this.getEduStdDataMapInternal(result, ids, loadDetails);
                }
            };
            BatchUtils.execute(eppEduStdIds, 100, action);
            return (Map)result;
        }
        finally
        {
            Debug.end();
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends IEppStdRowWrapper> Collection<T> getEduStandardBlockRows(final Map<Long, T> stdDataMap, final Predicate<IEppStdRowWrapper> predicate)
    {
        final Map<Long, T> result = new LinkedHashMap<Long, T>();
        if (null != stdDataMap)
        {
            for (IEppStdRowWrapper row : stdDataMap.values())
            {
                if (predicate.evaluate(row))
                {
                    while ((null != row) && (null == result.put(row.getId(), (T)row)))
                    {
                        row = row.getHierarhyParent();
                    }
                }
            }
        }
        return result.values();
    }

}
