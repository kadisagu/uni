package ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.ui.SectionPromice;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.Section.CtrSectionPromiceListUIPresenter;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;

import java.util.Collection;
import java.util.List;

/**
 * @author vdanilov
 */
public class EppCtrEducationPromiceSectionPromiceUI extends CtrSectionPromiceListUIPresenter<EppCtrEducationPromice>
{
    @Override
    protected IdentifiableWrapper<EppCtrEducationPromice> buildElementEditWrapper(EppCtrEducationPromice selectedElement) {
        return new EppCtrEducationPromiceWrapper(selectedElement);
    }

    @SuppressWarnings("unchecked")
    public Collection<CtrContractVersionContractor> getEducationPromiceSrcList(final List<CtrContractVersionContractor> contractorList) {
        return CollectionUtils.select(contractorList, object -> (object.getContactor() instanceof EmployeePostContactor));
    }

    public Collection<CtrContractVersionContractor> getEducationPromiceSrcList() {
        return this.getEducationPromiceSrcList(this.getContractorList());
    }


}

