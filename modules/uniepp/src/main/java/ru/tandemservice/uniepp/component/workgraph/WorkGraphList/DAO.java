/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphList;

import java.util.Collections;
import java.util.List;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;

import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.gen.EppStateGen;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphGen;
import ru.tandemservice.uniepp.util.EppFilterUtil;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        EppFilterUtil.prepareYearEducationProcessFilter(model, this.getSession());
        model.setStateListModel(new LazySimpleSelectModel<EppState>(EppState.class).setSortProperty(EppStateGen.P_CODE));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(new LazySimpleSelectModel<DevelopCondition>(DevelopCondition.class));
        model.setDevelopTechListModel(new LazySimpleSelectModel<DevelopTech>(DevelopTech.class));
        model.setDevelopGridListModel(new LazySimpleSelectModel<DevelopGrid>(DevelopGrid.class));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(final Model model)
    {
        final IDataSettings settings = model.getSettings();
        final Object state = settings.get("state");
        final List developFormList = (List) settings.get("developFormList");
        final List developConditionList = (List) settings.get("developConditionList");
        final List developTechList = (List) settings.get("developTechList");
        final List developGridList = (List) settings.get("developGridList");

        final MQBuilder builder = new MQBuilder(EppWorkGraphGen.ENTITY_CLASS, "wg");
        builder.add(MQExpression.eq("wg", EppWorkGraphGen.L_YEAR, model.getYearEducationProcess()));
        if (state != null)
        {
            builder.add(MQExpression.eq("wg", EppWorkGraphGen.L_STATE, state));
        }
        if ((developFormList != null) && !developFormList.isEmpty())
        {
            builder.add(MQExpression.in("wg", EppWorkGraphGen.L_DEVELOP_FORM, developFormList));
        }
        if ((developConditionList != null) && !developConditionList.isEmpty())
        {
            builder.add(MQExpression.in("wg", EppWorkGraphGen.L_DEVELOP_CONDITION, developConditionList));
        }
        if ((developTechList != null) && !developTechList.isEmpty())
        {
            builder.add(MQExpression.in("wg", EppWorkGraphGen.L_DEVELOP_TECH, developTechList));
        }
        if ((developGridList != null) && !developGridList.isEmpty())
        {
            builder.add(MQExpression.in("wg", EppWorkGraphGen.L_DEVELOP_GRID, developGridList));
        }

        final List<EppWorkGraph> list = builder.getResultList(this.getSession());
        Collections.sort(list, ITitled.TITLED_COMPARATOR);

        UniBaseUtils.createPage(model.getDataSource(), list);
    }
}
