package ru.tandemservice.uniepp.component.edugroup;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.ui.EppRegistryElementPartSelectModel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public abstract class EppEduGroupRegElPartSelectModel extends EppRegistryElementPartSelectModel {

    public EppEduGroupRegElPartSelectModel() {
        super("count");
        this.setPageSize(-1);
    }

    protected abstract Class<? extends EppRealEduGroup> getGroupClass();
    protected abstract Collection<Long> getGroupIds();

    @SuppressWarnings("unchecked")
    @Override protected List wrap(final List resultList)
    {
        return DataAccessServices.dao().getCalculatedValue(session -> {

            final DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(EppRealEduGroupRow.class, "x");
            dql.column(property(EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart().id().fromAlias("x")));
            dql.column(DQLFunctions.count(property(EppRealEduGroupRow.id().fromAlias("x"))));
            dql.where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("x"))));
            dql.where(in(property(EppRealEduGroupRow.group().id().fromAlias("x")), getGroupIds()));
            dql.where(in(property(EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart().id().fromAlias("x")), UniBaseDao.ids(resultList)));
            dql.group(property(EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart().id().fromAlias("x")));
            dql.order(DQLFunctions.count(property(EppRealEduGroupRow.id().fromAlias("x"))), OrderDirection.desc);

            final List<Object[]> rows = dql.createStatement(session).list();
            final Map<Long, Integer> countMap = new LinkedHashMap<>(rows.size());
            for (final Object[] row: rows) { countMap.put((Long)row[0], ((Number)row[1]).intValue()); }

            final List<ViewWrapper<IEntity>> wrappers = ViewWrapper.getPatchedList(CommonDAO.sort(EppEduGroupRegElPartSelectModel.super.wrap(resultList), countMap.keySet()));
            for (final ViewWrapper<IEntity> wrapper: wrappers) {
                final Integer count = countMap.get(wrapper.getId());
                wrapper.setViewProperty("count", (null == count ? 0 : count));
            }

            return wrappers;
        });
    }

    @Override protected DQLSelectBuilder query(final String alias, final String filter)
    {
        // все текущие значения
        final DQLSelectBuilder regElPartsByGroups = new DQLSelectBuilder().column("x.id");
        regElPartsByGroups.fromEntity(getGroupClass(), "x");
        regElPartsByGroups.where(eq(property("x", EppRealEduGroup.activityPart().id()), property(alias, "id")));
        regElPartsByGroups.where(in(property("x.id"), getGroupIds()));

        // все значения по записям
        final DQLSelectBuilder regElPartsByGroupStudents = new DQLSelectBuilder().column("x.id");
        regElPartsByGroupStudents.fromEntity(EppRealEduGroupRow.class, "x");
        regElPartsByGroupStudents.where(eq(property("x", EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart().id()), property(alias, "id")));
        regElPartsByGroupStudents.where(isNull(property("x", EppRealEduGroupRow.removalDate())));
        regElPartsByGroupStudents.where(in(property("x", EppRealEduGroupRow.group().id()), getGroupIds()));

        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppRegistryElementPart.class, alias);

        if (null != filter) {
            dql.where(this.getFilterCondition(alias, filter));
        }

        dql.where(or(
                exists(regElPartsByGroups.buildQuery()),
                exists(regElPartsByGroupStudents.buildQuery())
        ));

        return dql;
    }
}
