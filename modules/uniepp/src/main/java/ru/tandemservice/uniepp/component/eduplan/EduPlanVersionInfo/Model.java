/* $Id$ */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionInfo;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author oleyba
 * @since 5/13/11
 */
@Input( keys = {PublisherActivator.PUBLISHER_ID_KEY, "hideVersionInfo"},
        bindings = {"eduPlanVersion.id", "hideVersionInfo"}
        )
public class Model
{
    public static final String COMPONENT = Model.class.getPackage().getName();

    private EppEduPlanVersion eduPlanVersion = new EppEduPlanVersion();

    private boolean _hideVersionInfo = false;

    public EppEduPlanVersion getEduPlanVersion()
    {
        return this.eduPlanVersion;
    }

    public void setEduPlanVersion(final EppEduPlanVersion eduPlanVersion)
    {
        this.eduPlanVersion = eduPlanVersion;
    }

    public EppEduPlan getEduPlan() {
        return getEduPlanVersion().getEduPlan();
    }

    public boolean isHideVersionInfo()
    {
        return _hideVersionInfo;
    }

    public void setHideVersionInfo(boolean hideVersionInfo)
    {
        _hideVersionInfo = hideVersionInfo;
    }

    public boolean isShowVersionInfo()
    {
        return !_hideVersionInfo;
    }
}
