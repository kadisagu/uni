/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;

/**
 * @author oleyba
 * @since 8/28/14
 */
public interface IEppEduPlanDao extends INeedPersistenceSupport
{
	/**
	 * Сохранить или обновить учебный план проф.образования с учетом констрейнта qualification_check
	 * (связь квалификации и направления подготовки должна быть согласована с соответствующими полями УП)
	 * @param plan - сохраняемый УП
	 */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void saveOrUpdateEduPlanProf(EppEduPlanProf plan);

	/**
	 * Скорректировать данные учебного плана проф.образования - задать для него новые значения направления подготовки, квалификации направления подготовки и ориентации ОП.
	 * Одновременно направленности ВПО в блоках направленностей УП меняются на аналогичные из нового направления подготовки (т.е. имеющие такое же название).
	 * Если для каких-то направленностей аналог из нового направления не найден, выбросится исключения, данные УП не изменятся.
	 * Корректировка может производиться в любом состоянии учебного плана.
	 * @param eduPlan - редактируемый учебный план
	 * @param newSubject - новое направление подготовки
	 * @param newQualification - новая квалификация направления подготовки
	 * @param newOrientation - новая ориентация образовательной программы (для учебных планов ВО)
	 */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void updateEduPlan(EppEduPlanProf eduPlan, EduProgramSubject newSubject, EduProgramQualification newQualification, EduProgramOrientation newOrientation);
}