package ru.tandemservice.uniepp.dao.year;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;

/**
 * @author vdanilov
 */
public class EppYearDAO extends UniBaseDao implements IEppYearDAO
{
    @Override
    public void doRefreshYearEducationWeeks(final Long pupnagId)
    {
        final Session session = this.getSession();
        final EppYearEducationProcess pupnag = this.getNotNull(EppYearEducationProcess.class, pupnagId);

        // все недели в ГУП
        final MQBuilder builder = new MQBuilder(EppYearEducationWeek.ENTITY_CLASS, "y");
        builder.add(MQExpression.eq("y", EppYearEducationWeek.L_YEAR, pupnag));
        builder.addOrder("y", EppYearEducationWeek.P_NUMBER);
        final List<EppYearEducationWeek> weekList = builder.getResultList(session);

        final Calendar startDate = Calendar.getInstance();
        startDate.setTime(pupnag.getStartEducationDate());

        if (weekList.isEmpty())
        {
            // недели надо создать
            for (int i = 0; i < EppWeek.YEAR_WEEK_COUNT; i++)
            {
                final EppYearEducationWeek week = new EppYearEducationWeek();

                week.setYear(pupnag);
                week.setNumber(i + 1);

                final Date time = startDate.getTime();
                week.setDate(time);
                week.setTitle(week.getCalculatedTitle());

                EppYearEducationWeek.moveToNextDayOfWeek(startDate);
                session.save(week);
            }
        }
        else if (weekList.size() == EppWeek.YEAR_WEEK_COUNT)
        {
            // надо обновить названия недель
            for (final EppYearEducationWeek week : weekList)
            {
                // TODO: test number

                final Date time = startDate.getTime();
                week.setDate(time);
                week.setTitle(week.getCalculatedTitle());

                EppYearEducationWeek.moveToNextDayOfWeek(startDate);
                session.update(week);
            }
        }
        else
        {
            throw new RuntimeException("Invalid EppYearEducationWeek Count (" + weekList.size() + ") in PUPnaG: " + pupnagId);
        }
    }

    @Override
    public EppYearEducationWeek[] getYearEducationWeeks(final Long pupnagId)
    {
        final MQBuilder builder = new MQBuilder(EppYearEducationWeek.ENTITY_CLASS, "y");
        builder.add(MQExpression.eq("y", EppYearEducationWeek.year().id().s(), pupnagId));
        builder.addOrder("y", EppYearEducationWeek.P_NUMBER);
        final List<EppYearEducationWeek> weekList = builder.getResultList(this.getSession());

        if (weekList.size() != EppWeek.YEAR_WEEK_COUNT) {
            throw new RuntimeException("Invalid EppYearEducationWeek Count (" + weekList.size() + ") in PUPnaG: " + pupnagId);
        }

        // все недели в ГУП в виде массива
        final EppYearEducationWeek[] result = new EppYearEducationWeek[weekList.size()];
        for (final EppYearEducationWeek week : weekList) {
            result[week.getNumber() - 1] = week;
        }
        return result;
    }

}
