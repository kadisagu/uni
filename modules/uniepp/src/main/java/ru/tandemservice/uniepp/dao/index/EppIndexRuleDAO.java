package ru.tandemservice.uniepp.dao.index;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.entity.catalog.EppIndexRule;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.settings.EppIndexRule4SubjectIndex;
import ru.tandemservice.uniepp.entity.settings.gen.EppIndexRule4SubjectIndexGen;

import java.util.HashMap;
import java.util.Map;

/**
 * @author vdanilov
 */
public class EppIndexRuleDAO extends UniBaseDao implements IEppIndexRuleDAO {

    private final Map<String, IEppIndexRule> RULE_MAP = this.getRuleMap();
    protected Map<String, IEppIndexRule> getRuleMap() {
        final Map<String, IEppIndexRule> result = new HashMap<String, IEppIndexRule>(16, 0.5f);
        result.put(EppIndexRule.CODE_SIMPLE_CONCAT, IEppIndexRule.RULE_SIMPLE_CONCAT);
        result.put(EppIndexRule.CODE_POSTFIX_CONCAT, IEppIndexRule.RULE_POSTFIX_CONCAT);
        result.put(EppIndexRule.CODE_PREFIX_HIERARCHY, IEppIndexRule.RULE_PREFIX_HIERARCHY);
        result.put(EppIndexRule.CODE_TRIMMED_CONCAT, IEppIndexRule.RULE_TRIMMED_CONCAT);
        result.put(EppIndexRule.CODE_PREFIX_COMPONENT_HIERARCHY, IEppIndexRule.RULE_PREFIX_COMPONENT_HIERARCHY);
        return result ;
    }

    @Override
    public IEppIndexRule getRule(final EduProgramSubjectIndex subjectIndex)
    {
        final EppIndexRule4SubjectIndex config = (EppIndexRule4SubjectIndex) this.getByNaturalId(new EppIndexRule4SubjectIndexGen.NaturalId(subjectIndex));
        if (null == config) {
            if (this.logger.isWarnEnabled()) {
                try { throw new NullPointerException("config ("+subjectIndex.getCode()+") is null"); }
                catch (final Throwable t) { this.logger.warn(t.getMessage(), t); }
            }
            return IEppIndexRule.RULE_SIMPLE_CONCAT;
        }

        final EppIndexRule rule = config.getEppIndexRule();
        final IEppIndexRule result = this.RULE_MAP.get(rule.getCode());
        if (null != result) { return result; }
        return IEppIndexRule.RULE_SIMPLE_CONCAT;
    }



}
