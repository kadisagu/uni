package ru.tandemservice.uniepp.entity.pupnag.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Начало учебного года
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppYearEducationProcessGen extends EntityBase
 implements INaturalIdentifiable<EppYearEducationProcessGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess";
    public static final String ENTITY_NAME = "eppYearEducationProcess";
    public static final int VERSION_HASH = -369013807;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String P_TITLE = "title";
    public static final String P_START_EDUCATION_DATE = "startEducationDate";

    private EducationYear _educationYear;     // Учебный год
    private String _title;     // Название
    private Date _startEducationDate;     // Дата начала обучения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null и должно быть уникальным.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Дата начала обучения. Свойство не может быть null.
     */
    @NotNull
    public Date getStartEducationDate()
    {
        return _startEducationDate;
    }

    /**
     * @param startEducationDate Дата начала обучения. Свойство не может быть null.
     */
    public void setStartEducationDate(Date startEducationDate)
    {
        dirty(_startEducationDate, startEducationDate);
        _startEducationDate = startEducationDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppYearEducationProcessGen)
        {
            if (withNaturalIdProperties)
            {
                setEducationYear(((EppYearEducationProcess)another).getEducationYear());
            }
            setTitle(((EppYearEducationProcess)another).getTitle());
            setStartEducationDate(((EppYearEducationProcess)another).getStartEducationDate());
        }
    }

    public INaturalId<EppYearEducationProcessGen> getNaturalId()
    {
        return new NaturalId(getEducationYear());
    }

    public static class NaturalId extends NaturalIdBase<EppYearEducationProcessGen>
    {
        private static final String PROXY_NAME = "EppYearEducationProcessNaturalProxy";

        private Long _educationYear;

        public NaturalId()
        {}

        public NaturalId(EducationYear educationYear)
        {
            _educationYear = ((IEntity) educationYear).getId();
        }

        public Long getEducationYear()
        {
            return _educationYear;
        }

        public void setEducationYear(Long educationYear)
        {
            _educationYear = educationYear;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppYearEducationProcessGen.NaturalId) ) return false;

            EppYearEducationProcessGen.NaturalId that = (NaturalId) o;

            if( !equals(getEducationYear(), that.getEducationYear()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEducationYear());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEducationYear());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppYearEducationProcessGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppYearEducationProcess.class;
        }

        public T newInstance()
        {
            return (T) new EppYearEducationProcess();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "educationYear":
                    return obj.getEducationYear();
                case "title":
                    return obj.getTitle();
                case "startEducationDate":
                    return obj.getStartEducationDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "startEducationDate":
                    obj.setStartEducationDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "educationYear":
                        return true;
                case "title":
                        return true;
                case "startEducationDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "educationYear":
                    return true;
                case "title":
                    return true;
                case "startEducationDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "educationYear":
                    return EducationYear.class;
                case "title":
                    return String.class;
                case "startEducationDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppYearEducationProcess> _dslPath = new Path<EppYearEducationProcess>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppYearEducationProcess");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Дата начала обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess#getStartEducationDate()
     */
    public static PropertyPath<Date> startEducationDate()
    {
        return _dslPath.startEducationDate();
    }

    public static class Path<E extends EppYearEducationProcess> extends EntityPath<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;
        private PropertyPath<String> _title;
        private PropertyPath<Date> _startEducationDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppYearEducationProcessGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Дата начала обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess#getStartEducationDate()
     */
        public PropertyPath<Date> startEducationDate()
        {
            if(_startEducationDate == null )
                _startEducationDate = new PropertyPath<Date>(EppYearEducationProcessGen.P_START_EDUCATION_DATE, this);
            return _startEducationDate;
        }

        public Class getEntityClass()
        {
            return EppYearEducationProcess.class;
        }

        public String getEntityName()
        {
            return "eppYearEducationProcess";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
