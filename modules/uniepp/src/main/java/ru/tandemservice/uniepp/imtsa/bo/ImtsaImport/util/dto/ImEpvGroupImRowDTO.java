/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.dto;

/**
 * @author azhebko
 * @since 28.10.2014
 */
public class ImEpvGroupImRowDTO extends ImEpvTermDistributedRowDTO
{
    private int _size = 1;     // Число дисциплин

    public ImEpvGroupImRowDTO() { }
    public ImEpvGroupImRowDTO(String title) { this.setTitle(title); }

    public int getSize() { return _size; }
    public void setSize(int size) { _size = size; }

    @Override
    public String toString()
    {
        return "Дисциплина по выбору: " + getSize() + '\n' + super.toString();
    }
}