/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.component.*;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.AddEditResult;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.MassSplitEdit.EppRegistryMassSplitEdit;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Irina Ugfeld
 * @since 03.03.2016
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitHolder.id"))
@Output(@Bind(key = "fromOrgUnit", binding = "orgUnitHolder.id"))
public abstract class EppRegistryAbstractListUI extends OrgUnitUIPresenter
{
    public static final String SETTING_OWNER = "owner";
    public static final String SETTING_TITLE = "title";
    public static final String SETTING_STATE = "state";
    public static final String SETTING_EDU_GROUP_SPLIT_VARIANT = "eduGroupSplitVariant";
    public static final String SETTING_ELEMENT_CLASS = "elementClass";

    public abstract String getMenuPermission();
    public abstract String getPermissionPrefix();
    public abstract Class getElementClass();

    @Override
    protected String getSecModelPostfix() {
        String postfix = this.getPermissionPrefix();
        if (this.getOrgUnit() != null && null != postfix) {
            postfix = postfix + "_" + super.getSecModelPostfix();
        }
        return postfix;
    }

    @Override
    public void onComponentRefresh() {
        getOrgUnitHolder().getSecModel().addException(
                "view",
                propertyName ->  {
                    if (this.getOrgUnit() != null) { return ""; }
                    return this.getMenuPermission();
                });
        DataWrapper dw = getSettings().get(SETTING_EDU_GROUP_SPLIT_VARIANT);
        if (dw != null) {
            Long id = dw.getId();
            getSettings().set(SETTING_EDU_GROUP_SPLIT_VARIANT, id == 0L ? new DataWrapper(0L, "Не задан") : new DataWrapper(DataAccessServices.dao().get(id).getId(), ((EppEduGroupSplitVariant)DataAccessServices.dao().get(id)).getTitle()));
        }
    }

    @Override
    public String getSettingsKey() {
        return "epp.registry." + this.getElementClass().getSimpleName() + this.getOrgUnitHolder().getId();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(SETTING_OWNER, getSettings().get(SETTING_OWNER));
        dataSource.put(SETTING_TITLE, getSettings().get(SETTING_TITLE));
        dataSource.put(SETTING_EDU_GROUP_SPLIT_VARIANT, getSettings().get(SETTING_EDU_GROUP_SPLIT_VARIANT));
        dataSource.put(SETTING_ELEMENT_CLASS, getElementClass());
        dataSource.put(SETTING_STATE, getSettings().get(SETTING_STATE));
        dataSource.put(INPUT_PARAM_ORG_UNIT_ID, getOrgUnitHolder().getId());
    }

    public void onClickEditElement() {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(((EppRegistryAbstractList) getConfig().getBusinessComponentMeta().getConfig())
                .getMVCBasePackage() + ".AddEdit",
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong())));
    }

    public void onClickDeleteElement() {

        IUniBaseDao dao = IUniBaseDao.instance.get();
        EppRegistryElement registryElement = dao.getNotNull(EppRegistryElement.class, getListenerParameterAsLong());
        dao.delete(registryElement);
    }

    public void onClickAddElement() {

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(((EppRegistryAbstractList) getConfig().getBusinessComponentMeta().getConfig())
                .getMVCBasePackage() + ".AddEdit",
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, getOrgUnitHolder().getId())));
    }

    public void onClickSearch() {
        saveSettings();
    }

    public void onClickClear() {
        clearSettings();
    }

    public boolean isFromOrgUnit() {
        return getOrgUnit() != null;
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        super.onComponentBindReturnParameters(childRegionName, returnedData);
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((BaseSearchListDataSource) getConfig().getDataSource(EppRegistryAbstractList.ELEMENT_DS)).getOptionColumnSelectedObjects(EppRegistryAbstractList.COLUMN_CHECKBOX);
            selected.clear();
        }
        AddEditResult.processReturned(returnedData);
    }

    public void onClickMassEdit()
    {
        CheckboxColumn checkboxColumn = (CheckboxColumn) ((BaseSearchListDataSource) getConfig().getDataSource(EppRegistryAbstractList.ELEMENT_DS)).getLegacyDataSource().getColumn(EppRegistryAbstractList.COLUMN_CHECKBOX);
        List<Long> registryElementIds= checkboxColumn.getSelectedObjects().stream().map(IIdentifiable::getId).collect(Collectors.toList());

        if (CollectionUtils.isEmpty(registryElementIds)) {
            ErrorCollector errorCollector = getUserContext().getErrorCollector();
            errorCollector.add("Не выбран ни один элемент.");
        } else {
            getActivationBuilder().asRegionDialog(EppRegistryMassSplitEdit.class)
                    .parameter("selectedRegistryElements", registryElementIds)
                    .activate();
        }
    }
}