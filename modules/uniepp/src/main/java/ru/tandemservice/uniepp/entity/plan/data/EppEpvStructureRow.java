package ru.tandemservice.uniepp.entity.plan.data;

import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.StringUtils;

import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.index.IEppStructureIndexedRow;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvStructureRowGen;

/**
 * Запись версии УП (индекс, компонент, ...)
 */
public class EppEpvStructureRow extends EppEpvStructureRowGen implements IEppStructureIndexedRow
{
    public static final Predicate<IEppEpvRow> PARENT_PREDICATE = IEppEduPlanVersionDataDAO.PREDICATE_STRUCTURE_ROW;

    public EppEpvStructureRow() {
        super();
    }

    public EppEpvStructureRow(final EppEduPlanVersionBlock block) {
        this();
        this.setOwner(block);
    }

    private String _comparator_string_cache;
    @Override public String getComparatorString() {
        if (null != this._comparator_string_cache) { return this._comparator_string_cache; }

        final StringBuilder sb = new StringBuilder();
        for(final Integer i : this.getValue().getPriorityTrace()) {
            if (sb.length() > 0) { sb.append(':'); }
            sb.append(StringUtils.leftPad(String.valueOf(i), 2, '0'));
        }

        return (this._comparator_string_cache = sb.toString());
    }

    @Override public String getTitle() {
        if (getValue() == null) {
            return this.getClass().getSimpleName();
        }
        return this.getValue().getTitle();
    }

    @Override public String getRowType() {
        final EppPlanStructure parent = this.getValue().getParent();
        if (null != parent) {
            final String shortTitle = StringUtils.trimToNull(parent.getShortTitle());
            if (null != shortTitle) {
                return shortTitle;
            }
        }
        return "";
    }
    @Override public String getDisplayableTitle() {
        return this.getTitle() /*+" ("+getRowType()+")"*/;
    }


    @Override public String getSelfIndexPart() { return StringUtils.trimToEmpty(this.getValue().getShortTitle()); }
    @Override public int getComparatorClassIndex() { return 1; }

    @Override public EppEpvStructureRow getHierarhyParent() { return this.getParent(); }
    @Override public void setHierarhyParent(final EppEpvRow parent) { this.setParent((EppEpvStructureRow) parent); }


}