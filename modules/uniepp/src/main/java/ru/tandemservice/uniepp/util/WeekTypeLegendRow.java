/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.util;

import org.tandemframework.core.bean.FastAnyObject;
import org.tandemframework.core.bean.IFastBean;
import org.tandemframework.core.entity.IdentifiableWrapper;

/**
 * @author vip_delete
 * @since 02.03.2010
 */
@SuppressWarnings({"serial", "unchecked"})
public class WeekTypeLegendRow extends IdentifiableWrapper
{
    private final String[] _title;

    public WeekTypeLegendRow(final Long id, final String[] shortTitle, final String[] title)
    {
        super(id, "");
        final int len = title.length;
        this._title = new String[len];
        for (int i = 0; i < len; i++)
        {
            if (null == shortTitle[i]) {
                this._title[i] = "<td>&nbsp;</td><td>&nbsp;</td>";
            } else {
                this._title[i] = "<td>" + shortTitle[i] + "</td><td>—&nbsp;&nbsp;" + title[i] + "</td>";
            }
        }
    }

    private static final IFastBean fastBean = new FastAnyObject()
    {
        @Override
        public Object getPropertyValue(final Object obj, final String propertyName)
        {
            final WeekTypeLegendRow row = (WeekTypeLegendRow) obj;
            final int index = Integer.parseInt(propertyName.substring(propertyName.length() - 1));
            return row._title[index];
        }
    };

    @Override
    public IFastBean getFastBean()
    {
        return WeekTypeLegendRow.fastBean;
    }
}
