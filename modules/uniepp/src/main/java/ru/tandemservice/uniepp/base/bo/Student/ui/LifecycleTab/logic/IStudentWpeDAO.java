package ru.tandemservice.uniepp.base.bo.Student.ui.LifecycleTab.logic;

import com.google.common.collect.Table;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;

import java.util.List;

/**
 * @author avedernikov
 * @since 07.12.2016
 */
public interface IStudentWpeDAO extends ISharedBaseDao
{
	/**
	 * Виды учебных групп из МСРП по нагрузке для указанного студента. Сортировка по приоритету вида учебной группы.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	List<EppGroupType> getStudentWpeGroupTypes(Student student);

	/**
	 * Список МСРП студента с фильтрацией по активности МСРП и части учебного года. Сортировка по учебному году, затем по числу частей учебного года, затем по номеру части в разбиении
	 * (т.е. в одном году сначала - правильно отсортированные семестры, потом триместры, потом четверти), затем, если есть строка РУП, по ее номеру и названию.
	 * @param student Студент, для которого ищутся МСРП.
	 * @param activeFilter Фильтр по активности МСРП (см. {@link IStudentWpeDAO#applyActiveWpeFilter}).
	 * @param yearPart Фильтр по части года. Если не задан, то без фильтрации, иначе - МСРП из соответствующей годочасти.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	List<EppStudentWorkPlanElement> getStudentWpe(Student student, Boolean activeFilter, EppYearPart yearPart);

	/**
	 * Применить к запросу фильтрацию по активности МСРП, если фильтр задан. Если {@code true}, то только активные МСРП (без даты утраты актуальности), если {@code false}, то только неактивные (с датой утраты актуальности).
	 * @param studentWpeDql Запрос к МСРП.
	 * @param studentWpeAlias Алиас МСРП в запросе.
	 * @param activeFilter Фильтр активности МСРП.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	void applyActiveWpeFilter(DQLSelectBuilder studentWpeDql, String studentWpeAlias, Boolean activeFilter);

	/**
	 * Получить таблицу (отображение по двойному ключу: {id МСРП, код вида группы}), со значениями "МСРП по нагрузке (с данным МСРП и данным кодом группы) актуально".
	 * Если такого МСРП по нагрузке нет, то ячейка таблицы пустая.
	 * @param student Студент, для МСРП которого строится таблица.
	 * @return {{@link EppStudentWorkPlanElement#id}, {@link EppGroupType#code}} -> {{@link EppStudentWpePart#removalDate} == null}.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	Table<Long, String, Boolean> getWpeIdAndGroupTypeCode2LoadExist(Student student);
}
