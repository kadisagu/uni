package ru.tandemservice.uniepp.base.bo.EppContract;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrPromiceManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContractVersionPrinter;
import org.tandemframework.shared.ctr.base.bo.CtrPaymentPromice.CtrPaymentPromiceManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentGarantorPromice;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;

import ru.tandemservice.uniepp.base.bo.EppContract.logic.EppContractDao;
import ru.tandemservice.uniepp.base.bo.EppContract.logic.EppContractPrintDao;
import ru.tandemservice.uniepp.base.bo.EppContract.logic.IEppContractDao;
import ru.tandemservice.uniepp.base.bo.EppContract.ui.VersionPub.EppContractVersionPub;
import ru.tandemservice.uniepp.base.bo.EppContract.ui.Wizard.EppContractWizard;
import ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.EppCtrEducationPromiceManager;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;

/**
 * @author vdanilov
 * @deprecated договоры будут помигрированы с УП(в) на ОП
 */
@Deprecated
@Configuration
public class EppContractManager extends BusinessObjectManager implements ICtrContractManager {

    public static EppContractManager instance() {
        return BusinessObjectManager.instance(EppContractManager.class);
    }

    @Bean
    public IEppContractDao dao() {
        return new EppContractDao();
    }

    @Bean
    @Override
    public Class<? extends BusinessComponentManager> versionInlinePublisher() {
        return EppContractVersionPub.class;
    }

    @Override
    @Bean
    public Class<? extends BusinessComponentManager> versionWizard() {
        return EppContractWizard.class;
    }

    @Bean
    @Override
    public ICtrContractVersionPrinter printer() {
        return new EppContractPrintDao();
    }

    @Override
    public boolean isPromiceManagerAllowed(ICtrPromiceManager promiceManager) {
        if (promiceManager instanceof CtrPaymentPromiceManager) { return true; }
        if (promiceManager instanceof EppCtrEducationPromiceManager) { return true; }
        if (CtrPaymentPromice.class.isAssignableFrom(promiceManager.promiceClass())) { return true; }
        if (CtrPaymentGarantorPromice.class.isAssignableFrom(promiceManager.promiceClass())) { return true; }
        if (EppCtrEducationPromice.class.isAssignableFrom(promiceManager.promiceClass())) { return true; }
        return false;
    }

}
