/* $Id$ */
package ru.tandemservice.uniepp.component.edugroup.pub.GroupOrgUnitGroupPub;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/13/11
 */
public class DAO extends ru.tandemservice.uniepp.component.edugroup.pub.GroupPubBase.DAO implements IDAO
{
    @Override
    public boolean checkAvailability(final ru.tandemservice.uniepp.component.edugroup.pub.GroupPubBase.Model model, final EppRealEduGroup eduGroup, final OrgUnit orgUnit)
    {
        if (null == orgUnit) return false;

        // проверяем наличие у группы студентов из данного деканата
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, "rel").column(property("rel.id"));
        dql.where(eq(property(EppRealEduGroupRow.group().fromAlias("rel")), value(model.getGroup())));

        // нужно проверять доступность УГС (как правило это по урлу) в том числе по текущим данным студента, в том числе по данным из группы (если она указана)
        dql.joinPath(DQLJoinType.inner, EppRealEduGroupRow.studentWpePart().studentWpe().student().fromAlias("rel"), "s");
        dql.joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("s"), "s_eduOu");
        dql.joinPath(DQLJoinType.left, Student.group().fromAlias("s"), "g");
        dql.joinPath(DQLJoinType.left, Group.educationOrgUnit().fromAlias("g"), "g_eduOu");
        dql.where(or(
                eq(property(EppRealEduGroupRow.studentEducationOrgUnit().groupOrgUnit().fromAlias("rel")), value(model.getOrgUnit())),
                eq(property(EducationOrgUnit.groupOrgUnit().fromAlias("s_eduOu")), value(model.getOrgUnit())),
                eq(property(EducationOrgUnit.groupOrgUnit().fromAlias("g_eduOu")), value(model.getOrgUnit()))
        ));

        return existsEntity(dql.buildQuery());
    }
}
