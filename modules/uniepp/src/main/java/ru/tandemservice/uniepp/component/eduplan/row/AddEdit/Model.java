package ru.tandemservice.uniepp.component.eduplan.row.AddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;

import java.util.List;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id", required=true),
    @Bind(key="group", binding="elementTypeGroupName")
})
public class Model {

    public static final String GROUP_DISCIPLINE = "discipline";
    public static final String GROUP_ACTION = "action";

    public static class ElementType extends IdentifiableWrapper<IEntity> {
        private static final long serialVersionUID = 1L;

        public ElementType(final String name, final String title) {
            super((long) System.identityHashCode(name), title);
            this.name = name;
        }

        private final String name;
        public String getName() { return this.name; }
    }

    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    private String elementTypeGroupName = Model.GROUP_DISCIPLINE;
    public String getElementTypeGroupName() { return this.elementTypeGroupName; }
    public void setElementTypeGroupName(final String elementTypeGroupName) { this.elementTypeGroupName = elementTypeGroupName; }

    private ElementType elementType;
    public ElementType getElementType() { return this.elementType; }
    public void setElementType(final ElementType elementType) { this.elementType = elementType; }

    private List<ElementType> elementTypeList;
    public List<ElementType> getElementTypeList() { return this.elementTypeList; }
    public void setElementTypeList(final List<ElementType> elementTypeList) { this.elementTypeList = elementTypeList; }

    public boolean isEditForm() {
        final IEntityMeta meta = EntityRuntime.getMeta(this.id);
        if (null == meta) { throw new IllegalStateException(); }

        if (EppEduPlanVersionBlock.class.isAssignableFrom(meta.getEntityClass())) {
            return false; // это блок - значит форма создания

        } else if (EppEpvRow.class.isAssignableFrom(meta.getEntityClass())) {
            return true; // обычная строка - редактирование

        } else {
            throw new IllegalArgumentException(meta.getEntityClass().getName());
        }
    }

    public String getTitle() {
        return UniEppUtils.getEppEpvRowFormTitle(this.getId());
    }

}
