/**
 *$Id: EppWorkPlanStudentListUI.java 26089 2013-02-11 08:34:37Z vdanilov $
 */
package ru.tandemservice.uniepp.base.bo.EppEduGroup.ui.List4Group;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Input({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="groupHolder.id", required=true)
})
public class EppEduGroupList4GroupUI extends UIPresenter
{
    private final EntityHolder<Group> groupHolder = new EntityHolder<Group>();
    public EntityHolder<Group> getGroupHolder() { return this.groupHolder; }

    private List<Long> studentIds;
    public List<Long> getStudentIds() { return this.studentIds; }

    @Override
    public void onComponentRefresh() {
        this.studentIds = new DQLSelectBuilder()
        .fromEntity(Student.class, "s").column(property("s.id"))
        .where(eq(property(Student.archival().fromAlias("s")), value(Boolean.FALSE)))
        .where(eq(property(Student.group().id().fromAlias("s")), value(getGroupHolder().getId())))
        .createStatement(getSupport().getSession()).list();
    }

    public Map<String, Object> getParameters() {
        OrgUnit groupOrgUnit = getGroupOrgUnit();
        return new ParametersMap().add("studentIds", getStudentIds()).add("groupOrgUnitId", null == groupOrgUnit ? null : groupOrgUnit.getId());
    }

    public OrgUnit getGroupOrgUnit() {
        return getGroupHolder().getValue().getEducationOrgUnit().getGroupOrgUnit();
    }

}
