package ru.tandemservice.uniepp.component.group.GroupPub;

import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.HashMap;
import java.util.Map;

public class Model
{
    private Group group = new Group();

    public Map<String, Object> getParameters()
    {
        final Map<String, Object> map = new HashMap<>();
        map.put("groupId", this.group.getId());
        return map;
    }

    public void setGroup(final Group group)
    {
        this.group = group;
    }

    public Group getGroup()
    {
        return this.group;
    }
}
