package ru.tandemservice.uniepp.component.edustd.EduStdBlockList;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.RichDataSourceModel;
import ru.tandemservice.uniepp.dao.eduStd.data.IEppStdRowWrapper;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;

import java.io.Serializable;
import java.util.Collection;

public class BlockWrapper extends EntityBase implements IIdentifiableWrapper, Serializable
{
    private static final long serialVersionUID = 1L;

    private final EppStateEduStandardBlock block;
    public BlockWrapper(final EppStateEduStandardBlock block) { this.block = block; }
    public EppStateEduStandardBlock getBlock() { return this.block;}

    private final StaticListDataSource<IEppStdRowWrapper> dataSource = new StaticListDataSource<>();
    public StaticListDataSource<IEppStdRowWrapper> getDataSource() { return this.dataSource; }

    protected void setupRows(final Collection<IEppStdRowWrapper> rows) { this.dataSource.setupRows(rows); }
    public RichDataSourceModel<IEppStdRowWrapper> getRichDataSourceModel() { return this.dataSource.getRichDataSourceModel(); }

    @Override
    public String getTitle() { return this.block.getTitle(); }

    @Override
    public Long getId() { return this.block.getId(); }

    @Override
    public void setId(final Long id) { throw new UnsupportedOperationException(); }
}
