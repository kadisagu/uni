package ru.tandemservice.uniepp.settings.bo.PlanStructureQualification;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniepp.settings.bo.PlanStructureQualification.logic.IPlanStructureQualificationModifyDAO;
import ru.tandemservice.uniepp.settings.bo.PlanStructureQualification.logic.PlanStructureQualificationModifyDAO;

/**
 * @author avedernikov
 * @since 07.09.2015
 */

@Configuration
public class PlanStructureQualificationManager extends BusinessObjectManager
{
	public static PlanStructureQualificationManager instance()
	{
		return instance(PlanStructureQualificationManager.class);
	}

	@Bean
	public IPlanStructureQualificationModifyDAO modifyDAO()
	{
		return new PlanStructureQualificationModifyDAO();
	}
}
