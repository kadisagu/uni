package ru.tandemservice.uniepp.dao.workplan;

import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.dao.workplan.data.EppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.EppWorkPlanWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;
import ru.tandemservice.uniepp.entity.workplan.*;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanRowPartLoadGen;
import ru.tandemservice.uniepp.ui.EppRegistryElementSelectModel;

import java.util.*;
import java.util.Map.Entry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EppWorkPlanDataDAO extends UniBaseDao implements IEppWorkPlanDataDAO
{
    /**
     * создает новую оболочку РУП (метод для переопределения)
     * @param workplan РУП
     * @return новая оболочка для РУП
     */
    protected EppWorkPlanWrapper newEppWorkPlanWrapper(final EppWorkPlanBase workplan)
    {
        return new EppWorkPlanWrapper(workplan);
    }

    /**
     * создает новую оболочку строки РУП (метод для переопределения)
     * @param workplanWrapper оболочка РУП
     * @param row строка
     * @return новая оболочка для строки РУП
     */
    protected EppWorkPlanRowWrapper newEppWorkPlanRowWrapper(final IEppWorkPlanWrapper workplanWrapper, final EppWorkPlanRow row, final IEppRegElPartWrapper registryElementPart)
    {
        return new EppWorkPlanRowWrapper(workplanWrapper, row, registryElementPart);
    }

    private HashMap<Long, Map<Integer, Map<String, Number>>> getWorkPlanDataMapInternal_getRowPartDetailMap(final Collection<Long> eppWorkPlanIdsSlice)
    {
        final Session session = this.getSession();
        final HashMap<Long, Map<Integer, Map<String, Number>>> rowDetailMap = new HashMap<>(eppWorkPlanIdsSlice.size());

        /* Нагрузка для строки РУП в части (по видам теоретической нагрузки) */ {
            final MQBuilder builder = new MQBuilder(EppWorkPlanRowPartLoadGen.ENTITY_CLASS, "rowPart");
            builder.add(MQExpression.in("rowPart", EppWorkPlanRowPartLoadGen.row().workPlan().id().s(), eppWorkPlanIdsSlice));
            final List<EppWorkPlanRowPartLoad> rows = builder.getResultList(session);
            for (final EppWorkPlanRowPartLoad rowPartLoad : rows)
            {
                final Map<Integer, Map<String, Number>> termDetailMap = SafeMap.safeGet(rowDetailMap, rowPartLoad.getRow().getId(), key -> new HashMap(4));
                final Map<String, Number> detailMap = SafeMap.safeGet(termDetailMap, rowPartLoad.getPart(), key -> new HashMap(8));
                if (this.hasValue(rowPartLoad.getLoadAsDouble())) {
                    detailMap.put(rowPartLoad.getLoadType().getFullCode(), rowPartLoad.getLoadAsDouble());
                }

                if (this.hasValue(rowPartLoad.getDaysAsDouble())) {
                    detailMap.put(rowPartLoad.getLoadType().getFullCode()+IEppWorkPlanRowWrapper.DAYS_LOAD_POSTFIX, rowPartLoad.getDaysAsDouble());
                }
            }
        }

        return rowDetailMap;
    }

    private boolean hasValue(final Double loadAsDouble) {
        return ((null != loadAsDouble) && (loadAsDouble > 0));
    }


    private static final Comparator<EppWorkPlanRow> ROW_COMPARATOR = new Comparator<EppWorkPlanRow>()
    {
        private String safe(final String s) {
            return (null == s ? StringUtils.EMPTY : s);
        }

        @Override
        public int compare(final EppWorkPlanRow o1, final EppWorkPlanRow o2)
        {
            int i;
            final String n1 = this.safe(o1.getNumber());
            final String n2 = this.safe(o2.getNumber());
            if (0 != (i = n1.compareTo(n2))) { return i; }

            final String t1 = this.safe(o1.getTitle());
            final String t2 = this.safe(o2.getTitle());
            if (0 != (i = t1.compareTo(t2))) { return i; }

            if (0 != (i = o1.getRegistryElementPartNumber() - o2.getRegistryElementPartNumber())) { return i; }

            return 0;
        }
    };

    private void getWorkPlanDataMapInternal(final Map<Long, IEppWorkPlanWrapper> result, final Collection<Long> eppWorkPlanIdsSlice)
    {
        final Map<Long, Map<Integer, EppWorkPlanPart>> workPlanPartMap = new HashMap<>(eppWorkPlanIdsSlice.size());

        /* грузим РУП и его части */ {
            for (final EppWorkPlanBase wp : this.getList(EppWorkPlanBase.class, "id", eppWorkPlanIdsSlice)) {
                result.put(wp.getId(), this.newEppWorkPlanWrapper(wp));
                workPlanPartMap.put(wp.getWorkPlan().getId(), new HashMap<>(4));
            }

            for (final EppWorkPlanPart part: this.getList(EppWorkPlanPart.class, EppWorkPlanPart.workPlan().id(), workPlanPartMap.keySet())) {
                workPlanPartMap.get(part.getWorkPlan().getId()).put(part.getNumber(), part);
            }
        }

        /* грузим строки */
        {
            final Map<Long, Map<Long, EppWorkPlanRow>> rowData = SafeMap.get(HashMap.class);
            for (final EppWorkPlanRow row : this.getList(EppWorkPlanRow.class, EppWorkPlanRow.workPlan().id(), eppWorkPlanIdsSlice)) {
                rowData.get(row.getWorkPlan().getId()).put(row.getId(), row);
            }

            final Map<Long, Map<Integer, Map<String, Number>>> rowPartDetailMap = this.getWorkPlanDataMapInternal_getRowPartDetailMap(eppWorkPlanIdsSlice);
            final Map<Long, IEppRegElPartWrapper> registryElementPartMap = IEppRegistryDAO.instance.get().getRegistryElementPartDataMap(
                new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, "rr")
                .where(in(property(EppWorkPlanRegistryElementRow.workPlan().fromAlias("rr")), eppWorkPlanIdsSlice))
                .predicate(DQLPredicateType.distinct)
                .column(property(EppWorkPlanRegistryElementRow.registryElementPart().fromAlias("rr").id()))
                .createStatement(this.getSession()).<Long>list()
            );

            // бежим по всем УП
            for (final Map.Entry<Long, Map<Long, EppWorkPlanRow>> e : rowData.entrySet())
            {
                final IEppWorkPlanWrapper workplanWrapper = result.get(e.getKey());

                // сначала грузим все строки в оболочку
                {
                    final List<EppWorkPlanRow> values = new ArrayList<>(e.getValue().values());
                    Collections.sort(values, EppWorkPlanDataDAO.ROW_COMPARATOR);

                    for (final EppWorkPlanRow row : values)
                    {
                        final IEppRegElPartWrapper regElPart = (
                        (row instanceof EppWorkPlanRegistryElementRow) ?
                            registryElementPartMap.get(((EppWorkPlanRegistryElementRow)row).getRegistryElementPart().getId()) : null
                        );
                        workplanWrapper.getRowMap().put(row.getId(), this.newEppWorkPlanRowWrapper(workplanWrapper, row, regElPart));
                    }
                }

                // затем для всех строк (которые уже загрузились) указываем нагрузку
                {
                    final Map<Integer, EppWorkPlanPart> partMap = SafeMap.safeGet(workPlanPartMap, workplanWrapper.getWorkPlan().getWorkPlan().getId(), key -> new HashMap(4));
                    for (final IEppWorkPlanRowWrapper rowWrapper : workplanWrapper.getRowMap().values()) {
                        final Map<Integer, Map<String, Number>> termDetailMap = rowPartDetailMap.get(rowWrapper.getId());
                        if (null != termDetailMap) {
                            final Map<Integer, Map<String, Number>> localPartLoadMap = ((EppWorkPlanRowWrapper)rowWrapper).getLocalPartLoadMap();
                            for (final Map.Entry<Integer, Map<String, Number>> termDetailEntry: termDetailMap.entrySet()) {
                                final Integer partNumber = termDetailEntry.getKey();
                                if ((null != partNumber) && (partNumber > 0)) {
                                    final EppWorkPlanPart part = partMap.get(partNumber);
                                    if (null != part) { localPartLoadMap.put(partNumber, termDetailEntry.getValue()); }
                                } else {
                                    localPartLoadMap.put(partNumber, termDetailEntry.getValue());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private final DaoCacheFacade.CacheEntryDefinition<Long, IEppWorkPlanWrapper> CACHE_WORKPLAN_DATA =
            new DaoCacheFacade.CacheEntryDefinition<Long, IEppWorkPlanWrapper>(EppWorkPlanDataDAO.class.getSimpleName()+".dataMap", 128) {
                @Override public void fill(Map<Long, IEppWorkPlanWrapper> cache, Collection<Long> eppWorkPlanIdsSlice) {
                    EppWorkPlanDataDAO.this.getWorkPlanDataMapInternal(cache, eppWorkPlanIdsSlice);
                }
            };


    @Override
    public Map<Long, IEppWorkPlanWrapper> getWorkPlanDataMap(final Collection<Long> eppWorkPlanIds)
    {
        Debug.begin("EppWorkPlanDataDAO.getWorkPlanDataMap(size=" + eppWorkPlanIds.size() + ")");
        try {
            return DaoCacheFacade.getEntry(CACHE_WORKPLAN_DATA).getRecords(eppWorkPlanIds);
        } finally {
            Debug.end();
        }
    }

    @Override
    public Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> getRowPartLoadDataMap(final Collection<Long> rowIds)
    {
        final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> rowDetailMap = new HashMap<>(rowIds.size());

        for (List<Long> idsPart : Iterables.partition(rowIds, DQL.MAX_VALUES_ROW_NUMBER)) {
            final MQBuilder builder = new MQBuilder(EppWorkPlanRowPartLoadGen.ENTITY_CLASS, "rowPart");
            builder.add(MQExpression.in("rowPart", EppWorkPlanRowPartLoadGen.row().id().s(), idsPart));

            final List<EppWorkPlanRowPartLoad> relations = getList(EppWorkPlanRowPartLoad.class, EppWorkPlanRowPartLoad.row().id(), idsPart);
            for (final EppWorkPlanRowPartLoad relation : relations)
            {
                final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> rowMap = SafeMap.safeGet(rowDetailMap, relation.getRow().getId(), key -> new HashMap(4));
                final Map<String, EppWorkPlanRowPartLoad> partMap = SafeMap.safeGet(rowMap, relation.getPart(), key -> new HashMap(8));
                partMap.put(relation.getLoadType().getFullCode(), relation);
            }
        }
        return rowDetailMap;
    }

    protected interface IEppWorkPlanRowPartLoadAction {
        boolean isPossible(EppWorkPlanRowPartLoad source);
        boolean isAppendable(EppWorkPlanRowPartLoad source);
        boolean fill(EppWorkPlanRowPartLoad target, EppWorkPlanRowPartLoad source);
    }

    protected void doUpdateRowPartLoadMap(final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> partLoadMap, final IEppWorkPlanRowPartLoadAction action) {
        final Session session = this.getSession();

        // текущие значения из базы
        final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> currentMap = this.getRowPartLoadDataMap(partLoadMap.keySet());

        for (final Entry<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> entry : partLoadMap.entrySet())
        {
            final Long rowId = entry.getKey();
            final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> currentRowMap = SafeMap.safeGet(currentMap, rowId, key -> new HashMap(4));

            for (final Entry<Integer, Map<String, EppWorkPlanRowPartLoad>> partEntry : entry.getValue().entrySet())
            {
                final Integer partNumber = partEntry.getKey();
                final Map<String, EppWorkPlanRowPartLoad> currentPartMap = SafeMap.safeGet(currentRowMap, partNumber, key -> new HashMap(8));

                for (final Entry<String, EppWorkPlanRowPartLoad> loadEntry : partEntry.getValue().entrySet())
                {
                    final String loadFullCode = loadEntry.getKey();
                    final EppWorkPlanRowPartLoad load = loadEntry.getValue();

                    if (!partNumber.equals(load.getPart())) { throw new IllegalStateException("Invalid part number"); }
                    if (!loadFullCode.equals(load.getLoadType().getFullCode())) { throw new IllegalStateException("Invalid full code"); }

                    if (action.isPossible(load)) {
                        EppWorkPlanRowPartLoad databaseLoad = currentPartMap.remove(loadFullCode);
                        if ((null == databaseLoad) && action.isAppendable(load)) {
                            databaseLoad = new EppWorkPlanRowPartLoad(load.getRow(), load.getPart(), load.getLoadType());
                        }

                        if ((null != databaseLoad) && action.fill(databaseLoad, load)) {
                            session.saveOrUpdate(databaseLoad);
                        }
                    }
                }
            }

            // удаляем старые неактуальные значения
            for (final Map<String, EppWorkPlanRowPartLoad> value : currentRowMap.values()) {
                for (final EppWorkPlanRowPartLoad load : value.values()) {
                    session.delete(load);
                }
            }
        }
    }


    @Override
    public void doUpdateRowPartLoadElementsMap(final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> partLoadMap)
    {
        //        // проверяем, что нагрузка суммируется (для общей аудиторной нагрузки)
        //        // если нагрузка не совпадает - то созраняем сумму
        //        for (final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> m0 : partLoadMap.values()) {
        //            for (Map<String, EppWorkPlanRowPartLoad> m1: m0.values()) {
        //                EppWorkPlanRowPartLoad totalAudit = m1.get(EppELoadType.FULL_CODE_TOTAL_AUDIT);
        //                if (null != totalAudit) {
        //                    for (String code: EppALoadType.TYPE_CODES) {
        //                        final EppWorkPlanRowPartLoad load = m1.get(EppALoadType.getFullCode(EppALoadType.CATALOG_CODE, code));
        //
        //                    }
        //                }
        //            }
        //        }

        // копируем ТОЛЬКО нагрузку и цикловую нагрузку
        // (диапазон дат здесь не копируется, ибо он не должен меняться в рамках данного процесса)
        this.doUpdateRowPartLoadMap(partLoadMap, new IEppWorkPlanRowPartLoadAction() {
            @Override public boolean isAppendable(final EppWorkPlanRowPartLoad source) { return true; }
            @Override public boolean isPossible(final EppWorkPlanRowPartLoad source) { return !source.isEmptyLoad(); }
            @Override public boolean fill(final EppWorkPlanRowPartLoad target, final EppWorkPlanRowPartLoad source) {
                target.fillLoad(source);
                return true;
            }
        });
    }


    @Override
    public void doUpdateRowPartLoadPeriodsMap(final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> partLoadPeriodMap)
    {
        // проверяем, что диапазоны дат (для общей аудиторной нагрузки) вкладываются
        // если необходимо - расширяем диапазоны
        for (final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> m0 : partLoadPeriodMap.values()) {
            for (final Map<String, EppWorkPlanRowPartLoad> m1: m0.values()) {
                for (final EppWorkPlanRowPartLoad load: m1.values()) { load.correctStudyPeriod(); }

                final EppWorkPlanRowPartLoad totalAudit = m1.get(EppELoadType.FULL_CODE_AUDIT);
                if (null != totalAudit) {
                    for (final String fullCode: EppALoadType.FULL_CODES) {
                        final EppWorkPlanRowPartLoad load = m1.get(fullCode);
                        if (null != load) { totalAudit.expandStudyPeriod(load); }
                    }
                }
            }
        }

        // копируем ТОЛЬКО даты и только в существующие элементы в базе
        this.doUpdateRowPartLoadMap(partLoadPeriodMap, new IEppWorkPlanRowPartLoadAction() {
            @Override public boolean isAppendable(final EppWorkPlanRowPartLoad source) { return false; }
            @Override public boolean isPossible(final EppWorkPlanRowPartLoad source) { return true; }
            @Override public boolean fill(final EppWorkPlanRowPartLoad target, final EppWorkPlanRowPartLoad source) {
                target.fillStudyPeriod(source);
                return true;
            }
        });
    }


    /** базовая модель для добавления элементов реестра в РУП */
    protected class EppWorkPlanRegistryElementsSelectModel<T extends EppRegistryElement> extends EppRegistryElementSelectModel<T>
    {
        private final EppWorkPlanBase workplan;
        private final Iterable<Long> seletedIds;
        private final Class<? extends EppWorkPlanRegistryElementRow> rowClass;

        public EppWorkPlanRegistryElementsSelectModel(final Class<? extends T> registryElementClass, final Class<? extends EppWorkPlanRegistryElementRow> rowClass, final EppWorkPlanBase workplan, final Iterable<Long> seletedIds)
        {
            super(registryElementClass);
            this.workplan = workplan;
            this.seletedIds = seletedIds;
            this.rowClass = rowClass;
        }

        @Override protected DQLSelectBuilder query(final String alias, final String filter)
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(this.rowClass, "r")
            .column(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias("r")))
            .where(eq(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias("r")), value(this.workplan.getId())));

            // условие по выбору дисциплин из реестра
            IDQLExpression registryElementCondition = notIn(property(EppRegistryElement.id().fromAlias(alias)), dql.buildQuery());
            if (IEppSettingsDAO.instance.get().getGlobalSettings().isCheckElementState()) {
                registryElementCondition = and(registryElementCondition, eq(property(EppRegistryElement.state().code().fromAlias(alias)), value(EppState.STATE_ACCEPTED)));
            }

            return super.query(alias, filter).where(or(
                in(property(alias, "id"), this.ids(this.seletedIds)),
                registryElementCondition
            ));
        }


        @Override
        protected IDQLExpression getFilterCondition(final String alias, final String filter)
        {
            // проверяем еще и по всем названиям дисциплины (в других РУП(ах))
            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(this.rowClass, "r")
            .column(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias("r")))
            .where(this.like(EppWorkPlanRegistryElementRow.title().fromAlias("r"), filter));

            return or(
                in(property(alias, "id"), dql.buildQuery()),
                super.getFilterCondition(alias, filter)
            );
        }
    }

    @Override
    public ISelectModel getRegistryDisciplineSelectModel4Add(final EppWorkPlanBase workplan, final Iterable<Long> selectedIds) {
        return new EppWorkPlanRegistryElementsSelectModel<>(EppRegistryDiscipline.class, EppWorkPlanRegistryElementRow.class, workplan, selectedIds);
    }

    @Override
    public ISelectModel getRegistryActionSelectModel4Add(final EppWorkPlanBase workplan, final Iterable<Long> registryStructureIdsIterable, final Iterable<Long> selectedIds) {
        return new EppWorkPlanRegistryElementsSelectModel<EppRegistryAction>(EppRegistryAction.class, EppWorkPlanRegistryElementRow.class, workplan, selectedIds) {
            @Override protected DQLSelectBuilder query(final String alias, final String filter) {
                return super.query(alias, filter).where(in(property(EppRegistryElementGen.parent().id().fromAlias(alias)), this.ids(registryStructureIdsIterable)));
            }
        };
    }


}
