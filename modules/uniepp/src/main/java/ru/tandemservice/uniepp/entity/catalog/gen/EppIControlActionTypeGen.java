package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Форма текущего контроля
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppIControlActionTypeGen extends EppControlActionType
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppIControlActionType";
    public static final String ENTITY_NAME = "eppIControlActionType";
    public static final int VERSION_HASH = -2123964149;
    private static IEntityMeta ENTITY_META;

    public static final String P_USER_CODE = "userCode";
    public static final String P_MAX_COUNT = "maxCount";
    public static final String L_DEFAULT_GROUP_TYPE = "defaultGroupType";
    public static final String P_ENABLED = "enabled";

    private String _userCode;     // Код
    private int _maxCount;     // Максимальное количество мероприятий в семестре данного типа по всем дисциплинам
    private EppGroupType _defaultGroupType;     // Вид УГС (по умолчанию), в рамках которого проводится данный контроль

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getUserCode()
    {
        return _userCode;
    }

    /**
     * @param userCode Код. Свойство не может быть null и должно быть уникальным.
     */
    public void setUserCode(String userCode)
    {
        dirty(_userCode, userCode);
        _userCode = userCode;
    }

    /**
     * @return Максимальное количество мероприятий в семестре данного типа по всем дисциплинам. Свойство не может быть null.
     */
    @NotNull
    public int getMaxCount()
    {
        return _maxCount;
    }

    /**
     * @param maxCount Максимальное количество мероприятий в семестре данного типа по всем дисциплинам. Свойство не может быть null.
     */
    public void setMaxCount(int maxCount)
    {
        dirty(_maxCount, maxCount);
        _maxCount = maxCount;
    }

    /**
     * Вид УГС, к которому по умолчанию будет отнесена данная форма текущего контроля
     *
     * @return Вид УГС (по умолчанию), в рамках которого проводится данный контроль. Свойство не может быть null.
     */
    @NotNull
    public EppGroupType getDefaultGroupType()
    {
        return _defaultGroupType;
    }

    /**
     * @param defaultGroupType Вид УГС (по умолчанию), в рамках которого проводится данный контроль. Свойство не может быть null.
     */
    public void setDefaultGroupType(EppGroupType defaultGroupType)
    {
        dirty(_defaultGroupType, defaultGroupType);
        _defaultGroupType = defaultGroupType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppIControlActionTypeGen)
        {
            setUserCode(((EppIControlActionType)another).getUserCode());
            setMaxCount(((EppIControlActionType)another).getMaxCount());
            setDefaultGroupType(((EppIControlActionType)another).getDefaultGroupType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppIControlActionTypeGen> extends EppControlActionType.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppIControlActionType.class;
        }

        public T newInstance()
        {
            return (T) new EppIControlActionType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "userCode":
                    return obj.getUserCode();
                case "maxCount":
                    return obj.getMaxCount();
                case "defaultGroupType":
                    return obj.getDefaultGroupType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "userCode":
                    obj.setUserCode((String) value);
                    return;
                case "maxCount":
                    obj.setMaxCount((Integer) value);
                    return;
                case "defaultGroupType":
                    obj.setDefaultGroupType((EppGroupType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "userCode":
                        return true;
                case "maxCount":
                        return true;
                case "defaultGroupType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "userCode":
                    return true;
                case "maxCount":
                    return true;
                case "defaultGroupType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "userCode":
                    return String.class;
                case "maxCount":
                    return Integer.class;
                case "defaultGroupType":
                    return EppGroupType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppIControlActionType> _dslPath = new Path<EppIControlActionType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppIControlActionType");
    }
            

    /**
     * @return Код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppIControlActionType#getUserCode()
     */
    public static PropertyPath<String> userCode()
    {
        return _dslPath.userCode();
    }

    /**
     * @return Максимальное количество мероприятий в семестре данного типа по всем дисциплинам. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppIControlActionType#getMaxCount()
     */
    public static PropertyPath<Integer> maxCount()
    {
        return _dslPath.maxCount();
    }

    /**
     * Вид УГС, к которому по умолчанию будет отнесена данная форма текущего контроля
     *
     * @return Вид УГС (по умолчанию), в рамках которого проводится данный контроль. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppIControlActionType#getDefaultGroupType()
     */
    public static EppGroupType.Path<EppGroupType> defaultGroupType()
    {
        return _dslPath.defaultGroupType();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppIControlActionType#isEnabled()
     */
    public static SupportedPropertyPath<Boolean> enabled()
    {
        return _dslPath.enabled();
    }

    public static class Path<E extends EppIControlActionType> extends EppControlActionType.Path<E>
    {
        private PropertyPath<String> _userCode;
        private PropertyPath<Integer> _maxCount;
        private EppGroupType.Path<EppGroupType> _defaultGroupType;
        private SupportedPropertyPath<Boolean> _enabled;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppIControlActionType#getUserCode()
     */
        public PropertyPath<String> userCode()
        {
            if(_userCode == null )
                _userCode = new PropertyPath<String>(EppIControlActionTypeGen.P_USER_CODE, this);
            return _userCode;
        }

    /**
     * @return Максимальное количество мероприятий в семестре данного типа по всем дисциплинам. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppIControlActionType#getMaxCount()
     */
        public PropertyPath<Integer> maxCount()
        {
            if(_maxCount == null )
                _maxCount = new PropertyPath<Integer>(EppIControlActionTypeGen.P_MAX_COUNT, this);
            return _maxCount;
        }

    /**
     * Вид УГС, к которому по умолчанию будет отнесена данная форма текущего контроля
     *
     * @return Вид УГС (по умолчанию), в рамках которого проводится данный контроль. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppIControlActionType#getDefaultGroupType()
     */
        public EppGroupType.Path<EppGroupType> defaultGroupType()
        {
            if(_defaultGroupType == null )
                _defaultGroupType = new EppGroupType.Path<EppGroupType>(L_DEFAULT_GROUP_TYPE, this);
            return _defaultGroupType;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppIControlActionType#isEnabled()
     */
        public SupportedPropertyPath<Boolean> enabled()
        {
            if(_enabled == null )
                _enabled = new SupportedPropertyPath<Boolean>(EppIControlActionTypeGen.P_ENABLED, this);
            return _enabled;
        }

        public Class getEntityClass()
        {
            return EppIControlActionType.class;
        }

        public String getEntityName()
        {
            return "eppIControlActionType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract boolean isEnabled();
}
