/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.student.StudentEduplanTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.Pub.EppCustomEduPlanPub;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.Pub.EppCustomEduPlanPubUI;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.Collections;
import java.util.Date;

/**
 * @author vip_delete
 * @since 24.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));

        final EppStudent2EduPlanVersion relation = this.getModel(component).getRelation();
        if (relation != null) {
            if (relation.getCustomEduPlan() == null)
            {
                component.createChildRegion("eduPlanVersionInfoRegion", new ComponentActivator(
                        ru.tandemservice.uniepp.component.eduplan.EduPlanVersionInfo.Model.COMPONENT,
                        new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, relation.getEduPlanVersion().getId())
                                .add("hideVersionInfo", true)
                ));
            }
            else
            {
                component.createChildRegion("eduPlanVersionInfoRegion", new ComponentActivator(
                        EppCustomEduPlanPub.class.getSimpleName(),
                        new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, relation.getCustomEduPlan().getId())
                                .add(EppCustomEduPlanPubUI.INLINE_MODE_BIND, true)
                                .add(EppCustomEduPlanPubUI.PERMISSION_KEY_BIND, "viewTab_eppEduPlan_student")
                ));
            }
        }
    }

    private void _onClickConnectEduplan(final IBusinessComponent component, boolean individual)
    {
        final ParametersMap params = new ParametersMap().add("ids", Collections.singletonList(this.getModel(component).getStudent().getId()));
        if (this.getModel(component).getRelation() != null)
            params
                    .add("eduPlanVersion", this.getModel(component).getRelation().getEduPlanVersion())
                    .add("eduPlan", this.getModel(component).getRelation().getEduPlanVersion().getEduPlan());

        params.add("individual", individual);
        component.createChildRegion("studentEduplanScope", new ComponentActivator(
                ru.tandemservice.uniepp.component.student.MassChangeEduPlanVersions.Model.class.getPackage().getName(),
                params
        ));
    }

    public void onClickConnectEduplan(final IBusinessComponent component)
    {
        _onClickConnectEduplan(component, false);
    }

    public void onClickConnectCustomEduplan(final IBusinessComponent component)
    {
        _onClickConnectEduplan(component, true);
    }

    public void onClickDisconnectEduplan(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        model.getRelation().setRemovalDate(new Date());
        DataAccessServices.dao().save(model.getRelation());
        component.refresh();
    }
}