package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x6x7_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.7")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppYearEducationProcess

		// создано обязательное свойство allowEmptyWP
        if (!tool.columnExists("epp_year_epp_t", "allowemptywp_p"))
		{
			// создать колонку
			tool.createColumn("epp_year_epp_t", new DBColumn("allowemptywp_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultAllowEmptyWP = true;
			tool.executeUpdate("update epp_year_epp_t set allowemptywp_p=? where allowemptywp_p is null", defaultAllowEmptyWP);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_year_epp_t", "allowemptywp_p", false);
		}
    }
}