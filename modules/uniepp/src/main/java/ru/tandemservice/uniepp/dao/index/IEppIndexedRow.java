package ru.tandemservice.uniepp.dao.index;

import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.uniepp.entity.catalog.EppGeneration;

/**
 * Строка (запись, элемент) ГОС\УП\..., обладающая вычислимым индексом
 * @author vdanilov
 */
public interface IEppIndexedRow extends IIdentifiable, ITitled, IHierarchyItem
{

    /** @return вышестоящий элемент */
    @Override IEppIndexedRow getHierarhyParent();

    /** @return поколение ГОС */
    EppGeneration getGeneration();

    /** @return код квалификации УП (если есть) */
    String getQualificationCode();

    /** @return состовная часть индекса, относящаяся непосредственно к этому элементу */
    String getSelfIndexPart();

    /** @return вычисленное название элемента (с учетом уточнений, но без индекса) */
    String getDisplayableTitle();

}