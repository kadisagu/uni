/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.logic;

import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Denis Katkov
 * @since 04.05.2016
 */
public class EppRegistryMassSplitDAO extends UniBaseDao implements IEppRegistryMassSplitDAO
{
    @Override
    public void saveRegistryElementsSplitByTypeEduGroupRel(List<EppRegistryElement> registryElements, List<EppGroupType> groupTypes)
    {
        for (EppRegistryElement registryElement : registryElements) {
            IEppRegistryDAO.instance.get().doSaveRegistryElement(registryElement, groupTypes);
        }
    }

    @Override
    public void saveRegistryElements(List<EppRegistryElement> registryElements)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, "e")
                .where(in(property(EppRegistryElement.id().fromAlias("e")), registryElements));

        new MergeAction.SessionMergeAction<Long, EppRegistryElement>()
        {
            @Override
            protected Long key(EppRegistryElement source)
            {
                return source.getId();
            }

            @Override
            protected EppRegistryElement buildRow(EppRegistryElement source)
            {
                return source;
            }

            @Override
            protected void fill(EppRegistryElement target, EppRegistryElement source)
            {
                target.update(source, false);
            }
        }.merge(dql.createStatement(getSession()).list(), registryElements);

    }
}