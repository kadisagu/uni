/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.EppEPVBlockProfActivityTypeList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.EppDevelopResultManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlockProfActivityType;

/**
 * @author Igor Belanov
 * @since 28.02.2017
 */
@Configuration
public class EppDevelopResultEppEPVBlockProfActivityTypeList extends BusinessComponentManager
{
    public static final String EPP_EPV_BLOCK_PROF_ACTIVITY_TYPE_DS = "eppEPVBlockProfActivityTypeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(EPP_EPV_BLOCK_PROF_ACTIVITY_TYPE_DS, eppEPVBlockProfActivityTypeCL(), EppDevelopResultManager.instance().eppEPVBlockProfActivityTypeSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eppEPVBlockProfActivityTypeCL()
    {
        final IMergeRowIdResolver eppEPVBlockMerge = entity -> {
            EppEduPlanVersionBlockProfActivityType eppEPVBlockProfActivityType = (EppEduPlanVersionBlockProfActivityType)entity;
            return String.valueOf(eppEPVBlockProfActivityType.getEduPlanVersionBlock().getId());
        };
        return columnListExtPointBuilder(EPP_EPV_BLOCK_PROF_ACTIVITY_TYPE_DS)
                .addColumn(textColumn("EppEPVBlock", EppEduPlanVersionBlockProfActivityType.eduPlanVersionBlock().title()).merger(eppEPVBlockMerge))
                .addColumn(textColumn("EppProfActivityType", EppEduPlanVersionBlockProfActivityType.profActivityType().title()))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                        .disabled("ui:disableAddEdit")
                        .permissionKey("deleteProfActivityType_eppEduPlanVersion")
                        .alert(new FormattedMessage("Удалить вид проф. деятельности «{0}» из версии УП?", EppEduPlanVersionBlockProfActivityType.profActivityType().title())))
                .create();
    }
}
