package ru.tandemservice.uniepp.component.orgunit.OrgUnitEppTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author nkokorina
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
    }

    public void onTabChange(final IBusinessComponent component) {
        component.getUserContext().getDesktop().setRefreshScheduled(true);
    }
}
