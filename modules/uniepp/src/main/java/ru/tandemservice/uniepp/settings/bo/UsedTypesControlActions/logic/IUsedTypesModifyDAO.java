package ru.tandemservice.uniepp.settings.bo.UsedTypesControlActions.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

/**
 * @author avedernikov
 * @since 04.09.2015
 */

public interface IUsedTypesModifyDAO extends INeedPersistenceSupport
{
	/**
	 * Устанавливает параметр "Использовать в системе" в значение true
	 * @param typeCAId    id типа формы контроля, для которого нужно установить параметр
	 */
	void doEnable(Long typeCAId);

	/**
	 * Устанавливает параметр "Использовать в системе" в значение false.
	 * Также устанавливает значение false параметрам "Активное (в УП)", "Использовать для дисциплин", "Использовать для практик" и "Использовать для мероприятий ГИА"
	 * @param typeCAId    id типа формы контроля, для которого нужно установить параметр
	 */
	void doDisable(Long typeCAId);

	/**
	 * Меняет значение параметра "Активное (в УП)" на противоположное
	 * @param typeCAId    id типа формы контроля, для которого нужно установить параметр
	 */
	void doToggleActiveInEduPlan(Long typeCAId);

	/**
	 * Меняет значение параметра "Использовать для дисциплин" на противоположное
	 * @param typeCAId    id типа формы контроля, для которого нужно установить параметр
	 */
	void doToggleUsedWithDisciplines(Long typeCAId);

	/**
	 * Меняет значение параметра "Использовать для практик" на противоположное
	 * @param typeCAId    id типа формы контроля, для которого нужно установить параметр
	 */
	void doToggleUsedWithPractice(Long typeCAId);

	/**
	 * Меняет значение параметра "Использовать для мероприятий ГИА" на противоположное
	 * @param typeCAId    id типа формы контроля, для которого нужно установить параметр
	 */
	void doToggleUsedWithAttestation(Long typeCAId);
}
