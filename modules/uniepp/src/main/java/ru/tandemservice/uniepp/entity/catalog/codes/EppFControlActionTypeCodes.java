package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Форма итогового контроля"
 * Имя сущности : eppFControlActionType
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppFControlActionTypeCodes
{
    /** Константа кода (code) элемента : Экзамен (title) */
    String CONTROL_ACTION_EXAM = "01";
    /** Константа кода (code) элемента : Зачет (title) */
    String CONTROL_ACTION_SETOFF = "02";
    /** Константа кода (code) элемента : Зачет дифференцированный (title) */
    String CONTROL_ACTION_SETOFF_DIFF = "03";
    /** Константа кода (code) элемента : Курсовая работа (title) */
    String CONTROL_ACTION_COURSE_WORK = "04";
    /** Константа кода (code) элемента : Курсовой проект (title) */
    String CONTROL_ACTION_COURSE_PROJECT = "05";
    /** Константа кода (code) элемента : Экзамен накопительный (title) */
    String CONTROL_ACTION_EXAM_ACCUM = "06";
    /** Константа кода (code) элемента : Контрольная работа (title) */
    String CONTROL_ACTION_CONTROL_WORK = "07";

    Set<String> CODES = ImmutableSet.of(CONTROL_ACTION_EXAM, CONTROL_ACTION_SETOFF, CONTROL_ACTION_SETOFF_DIFF, CONTROL_ACTION_COURSE_WORK, CONTROL_ACTION_COURSE_PROJECT, CONTROL_ACTION_EXAM_ACCUM, CONTROL_ACTION_CONTROL_WORK);
}
