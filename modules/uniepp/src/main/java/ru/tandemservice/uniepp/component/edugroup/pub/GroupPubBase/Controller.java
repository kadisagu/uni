/* $Id: Controller.java 19933 2011-09-13 11:50:59Z oleyba $ */
package ru.tandemservice.uniepp.component.edugroup.pub.GroupPubBase;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;

/**
 * @author oleyba
 * @since 9/13/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    private static final String GROUP_CONTENT_REGION = "groupContent";

    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);

        if (!this.getDao().prepare(model))
        {
            // если группы нет - закрываем компонент
            if (StringUtils.isBlank(ContextLocal.getSite().getPreviousUrl())) {
                // если компонент не знает, куда ему идти обратно - кидаем исключение
                // TODO: здесь надо что-то делать (вообще говоря, хз что, наверное, переходить в список УГС)
            }

            // если компонент знает как ему уйти обратно - то закрываем компонент
            this.deactivate(component);
            return;
        }

        // если все проверки прошли, то рисуем компонент
        if (null == component.getChildRegion(Controller.GROUP_CONTENT_REGION)) {
            component.createChildRegion(Controller.GROUP_CONTENT_REGION, new ComponentActivator(
                    ru.tandemservice.uniepp.component.edugroup.pub.GroupContent.Model.class.getPackage().getName(),
                    new ParametersMap()
                            .add("parameters", model)
            ));
        }

    }
}
