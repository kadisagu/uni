package ru.tandemservice.uniepp.dao.workplan.data;

import java.util.Map;
import java.util.Set;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

/**
 * @author vdanilov
 */
public interface IEppWorkPlanWrapper extends IEntity, ITitled {

    /** @return РУП (базовый) из базы */
    EppWorkPlanBase getWorkPlan();

    /** @return { row.id -> wrapper(row), отсортированный по индексу } */
    Map<Long, IEppWorkPlanRowWrapper> getRowMap();

    /** @return набор описаний строк (показывает уникальность версии) */
    Set<String> getDescriptionSet();

}
