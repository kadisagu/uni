package ru.tandemservice.uniepp.entity.student.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Рабочий учебный план студента
 *
 * Определяет набор РУПы студента в рамках его текущего УП(в)
 * Показывает, какие РУП были привязаны к студенту (для неактивных связей)
 * removalDate - дата, с которой связь утрачивает актуальность
 * (все запросы к актуальным данным должны содержать условие removalDate is null)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStudent2WorkPlanGen extends EntityBase
 implements INaturalIdentifiable<EppStudent2WorkPlanGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan";
    public static final String ENTITY_NAME = "eppStudent2WorkPlan";
    public static final int VERSION_HASH = 132302624;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_EDU_PLAN_VERSION = "studentEduPlanVersion";
    public static final String L_WORK_PLAN = "workPlan";
    public static final String P_CONFIRM_DATE = "confirmDate";
    public static final String P_REMOVAL_DATE = "removalDate";
    public static final String L_CACHED_GRID_TERM = "cachedGridTerm";
    public static final String L_CACHED_EPP_YEAR = "cachedEppYear";

    private EppStudent2EduPlanVersion _studentEduPlanVersion;     // Связь студента с УП(в)
    private EppWorkPlanBase _workPlan;     // РУП
    private Date _confirmDate;     // Дата утверждения
    private Date _removalDate;     // Дата утраты актуальности
    private DevelopGridTerm _cachedGridTerm;     // Семестр сетки из РУП (кэш для запросов)
    private EppYearEducationProcess _cachedEppYear;     // ПУПнаГ из РУП (кэш для запросов)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Связь студента с УП(в). Свойство не может быть null.
     */
    @NotNull
    public EppStudent2EduPlanVersion getStudentEduPlanVersion()
    {
        return _studentEduPlanVersion;
    }

    /**
     * @param studentEduPlanVersion Связь студента с УП(в). Свойство не может быть null.
     */
    public void setStudentEduPlanVersion(EppStudent2EduPlanVersion studentEduPlanVersion)
    {
        dirty(_studentEduPlanVersion, studentEduPlanVersion);
        _studentEduPlanVersion = studentEduPlanVersion;
    }

    /**
     * @return РУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkPlanBase getWorkPlan()
    {
        return _workPlan;
    }

    /**
     * @param workPlan РУП. Свойство не может быть null.
     */
    public void setWorkPlan(EppWorkPlanBase workPlan)
    {
        dirty(_workPlan, workPlan);
        _workPlan = workPlan;
    }

    /**
     * @return Дата утверждения.
     */
    public Date getConfirmDate()
    {
        return _confirmDate;
    }

    /**
     * @param confirmDate Дата утверждения.
     */
    public void setConfirmDate(Date confirmDate)
    {
        dirty(_confirmDate, confirmDate);
        _confirmDate = confirmDate;
    }

    /**
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    /**
     * Семестр сетки (для запросов)
     *
     * @return Семестр сетки из РУП (кэш для запросов). Свойство не может быть null.
     */
    @NotNull
    public DevelopGridTerm getCachedGridTerm()
    {
        return _cachedGridTerm;
    }

    /**
     * @param cachedGridTerm Семестр сетки из РУП (кэш для запросов). Свойство не может быть null.
     */
    public void setCachedGridTerm(DevelopGridTerm cachedGridTerm)
    {
        dirty(_cachedGridTerm, cachedGridTerm);
        _cachedGridTerm = cachedGridTerm;
    }

    /**
     * ПУПнаГ (для запросов)
     *
     * @return ПУПнаГ из РУП (кэш для запросов). Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getCachedEppYear()
    {
        return _cachedEppYear;
    }

    /**
     * @param cachedEppYear ПУПнаГ из РУП (кэш для запросов). Свойство не может быть null.
     */
    public void setCachedEppYear(EppYearEducationProcess cachedEppYear)
    {
        dirty(_cachedEppYear, cachedEppYear);
        _cachedEppYear = cachedEppYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppStudent2WorkPlanGen)
        {
            if (withNaturalIdProperties)
            {
                setStudentEduPlanVersion(((EppStudent2WorkPlan)another).getStudentEduPlanVersion());
                setWorkPlan(((EppStudent2WorkPlan)another).getWorkPlan());
            }
            setConfirmDate(((EppStudent2WorkPlan)another).getConfirmDate());
            setRemovalDate(((EppStudent2WorkPlan)another).getRemovalDate());
            setCachedGridTerm(((EppStudent2WorkPlan)another).getCachedGridTerm());
            setCachedEppYear(((EppStudent2WorkPlan)another).getCachedEppYear());
        }
    }

    public INaturalId<EppStudent2WorkPlanGen> getNaturalId()
    {
        return new NaturalId(getStudentEduPlanVersion(), getWorkPlan());
    }

    public static class NaturalId extends NaturalIdBase<EppStudent2WorkPlanGen>
    {
        private static final String PROXY_NAME = "EppStudent2WorkPlanNaturalProxy";

        private Long _studentEduPlanVersion;
        private Long _workPlan;

        public NaturalId()
        {}

        public NaturalId(EppStudent2EduPlanVersion studentEduPlanVersion, EppWorkPlanBase workPlan)
        {
            _studentEduPlanVersion = ((IEntity) studentEduPlanVersion).getId();
            _workPlan = ((IEntity) workPlan).getId();
        }

        public Long getStudentEduPlanVersion()
        {
            return _studentEduPlanVersion;
        }

        public void setStudentEduPlanVersion(Long studentEduPlanVersion)
        {
            _studentEduPlanVersion = studentEduPlanVersion;
        }

        public Long getWorkPlan()
        {
            return _workPlan;
        }

        public void setWorkPlan(Long workPlan)
        {
            _workPlan = workPlan;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppStudent2WorkPlanGen.NaturalId) ) return false;

            EppStudent2WorkPlanGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudentEduPlanVersion(), that.getStudentEduPlanVersion()) ) return false;
            if( !equals(getWorkPlan(), that.getWorkPlan()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudentEduPlanVersion());
            result = hashCode(result, getWorkPlan());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudentEduPlanVersion());
            sb.append("/");
            sb.append(getWorkPlan());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStudent2WorkPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStudent2WorkPlan.class;
        }

        public T newInstance()
        {
            return (T) new EppStudent2WorkPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentEduPlanVersion":
                    return obj.getStudentEduPlanVersion();
                case "workPlan":
                    return obj.getWorkPlan();
                case "confirmDate":
                    return obj.getConfirmDate();
                case "removalDate":
                    return obj.getRemovalDate();
                case "cachedGridTerm":
                    return obj.getCachedGridTerm();
                case "cachedEppYear":
                    return obj.getCachedEppYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentEduPlanVersion":
                    obj.setStudentEduPlanVersion((EppStudent2EduPlanVersion) value);
                    return;
                case "workPlan":
                    obj.setWorkPlan((EppWorkPlanBase) value);
                    return;
                case "confirmDate":
                    obj.setConfirmDate((Date) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
                case "cachedGridTerm":
                    obj.setCachedGridTerm((DevelopGridTerm) value);
                    return;
                case "cachedEppYear":
                    obj.setCachedEppYear((EppYearEducationProcess) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentEduPlanVersion":
                        return true;
                case "workPlan":
                        return true;
                case "confirmDate":
                        return true;
                case "removalDate":
                        return true;
                case "cachedGridTerm":
                        return true;
                case "cachedEppYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentEduPlanVersion":
                    return true;
                case "workPlan":
                    return true;
                case "confirmDate":
                    return true;
                case "removalDate":
                    return true;
                case "cachedGridTerm":
                    return true;
                case "cachedEppYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentEduPlanVersion":
                    return EppStudent2EduPlanVersion.class;
                case "workPlan":
                    return EppWorkPlanBase.class;
                case "confirmDate":
                    return Date.class;
                case "removalDate":
                    return Date.class;
                case "cachedGridTerm":
                    return DevelopGridTerm.class;
                case "cachedEppYear":
                    return EppYearEducationProcess.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStudent2WorkPlan> _dslPath = new Path<EppStudent2WorkPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStudent2WorkPlan");
    }
            

    /**
     * @return Связь студента с УП(в). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan#getStudentEduPlanVersion()
     */
    public static EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion> studentEduPlanVersion()
    {
        return _dslPath.studentEduPlanVersion();
    }

    /**
     * @return РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan#getWorkPlan()
     */
    public static EppWorkPlanBase.Path<EppWorkPlanBase> workPlan()
    {
        return _dslPath.workPlan();
    }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan#getConfirmDate()
     */
    public static PropertyPath<Date> confirmDate()
    {
        return _dslPath.confirmDate();
    }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    /**
     * Семестр сетки (для запросов)
     *
     * @return Семестр сетки из РУП (кэш для запросов). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan#getCachedGridTerm()
     */
    public static DevelopGridTerm.Path<DevelopGridTerm> cachedGridTerm()
    {
        return _dslPath.cachedGridTerm();
    }

    /**
     * ПУПнаГ (для запросов)
     *
     * @return ПУПнаГ из РУП (кэш для запросов). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan#getCachedEppYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> cachedEppYear()
    {
        return _dslPath.cachedEppYear();
    }

    public static class Path<E extends EppStudent2WorkPlan> extends EntityPath<E>
    {
        private EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion> _studentEduPlanVersion;
        private EppWorkPlanBase.Path<EppWorkPlanBase> _workPlan;
        private PropertyPath<Date> _confirmDate;
        private PropertyPath<Date> _removalDate;
        private DevelopGridTerm.Path<DevelopGridTerm> _cachedGridTerm;
        private EppYearEducationProcess.Path<EppYearEducationProcess> _cachedEppYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Связь студента с УП(в). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan#getStudentEduPlanVersion()
     */
        public EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion> studentEduPlanVersion()
        {
            if(_studentEduPlanVersion == null )
                _studentEduPlanVersion = new EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion>(L_STUDENT_EDU_PLAN_VERSION, this);
            return _studentEduPlanVersion;
        }

    /**
     * @return РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan#getWorkPlan()
     */
        public EppWorkPlanBase.Path<EppWorkPlanBase> workPlan()
        {
            if(_workPlan == null )
                _workPlan = new EppWorkPlanBase.Path<EppWorkPlanBase>(L_WORK_PLAN, this);
            return _workPlan;
        }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan#getConfirmDate()
     */
        public PropertyPath<Date> confirmDate()
        {
            if(_confirmDate == null )
                _confirmDate = new PropertyPath<Date>(EppStudent2WorkPlanGen.P_CONFIRM_DATE, this);
            return _confirmDate;
        }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(EppStudent2WorkPlanGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

    /**
     * Семестр сетки (для запросов)
     *
     * @return Семестр сетки из РУП (кэш для запросов). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan#getCachedGridTerm()
     */
        public DevelopGridTerm.Path<DevelopGridTerm> cachedGridTerm()
        {
            if(_cachedGridTerm == null )
                _cachedGridTerm = new DevelopGridTerm.Path<DevelopGridTerm>(L_CACHED_GRID_TERM, this);
            return _cachedGridTerm;
        }

    /**
     * ПУПнаГ (для запросов)
     *
     * @return ПУПнаГ из РУП (кэш для запросов). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan#getCachedEppYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> cachedEppYear()
        {
            if(_cachedEppYear == null )
                _cachedEppYear = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_CACHED_EPP_YEAR, this);
            return _cachedEppYear;
        }

        public Class getEntityClass()
        {
            return EppStudent2WorkPlan.class;
        }

        public String getEntityName()
        {
            return "eppStudent2WorkPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
