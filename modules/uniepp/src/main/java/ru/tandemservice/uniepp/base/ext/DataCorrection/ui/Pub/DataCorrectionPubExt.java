/**
 *$Id: SystemActionPubExt.java 4443 2014-05-21 08:10:28Z nvankov $
 */
package ru.tandemservice.uniepp.base.ext.DataCorrection.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.DataCorrection.ui.Pub.DataCorrectionPub;

/**
 * @author rsizonenko
 * @since 30/03/16
 */
@Configuration
public class DataCorrectionPubExt extends BusinessComponentExtensionManager
{
    public static final String ORG_DATA_CORRECTION_PUB_ADDON_NAME = "UnieppDataCorrectionPubAddon";

    @Autowired
    private DataCorrectionPub _dataCorrectionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_dataCorrectionPub.presenterExtPoint())
                .addAddon(uiAddon(ORG_DATA_CORRECTION_PUB_ADDON_NAME, OrgDataCorrectionPubAddon.class))
                .create();
    }
}
