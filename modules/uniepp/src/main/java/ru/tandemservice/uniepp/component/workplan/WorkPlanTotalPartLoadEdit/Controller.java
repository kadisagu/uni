package ru.tandemservice.uniepp.component.workplan.WorkPlanTotalPartLoadEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;

import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author nkokorina
 *
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    private static final String ELOAD_AUDIENCE_FULL_CODE = EppLoadType.getFullCode(EppELoadType.CATALOG_CODE, EppELoadType.TYPE_TOTAL_AUDIT);

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    @SuppressWarnings("unchecked")
    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final ErrorCollector errorCollector = component.getUserContext().getErrorCollector();

        final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> partLoadMap = model.getPartLoadMap();

        for (final Entry<Integer, Map<String, EppWorkPlanRowPartLoad>> e : partLoadMap.entrySet())
        {
            double value = 0.0;
            for (final String fullCode : EppALoadType.FULL_CODES)
            {
                final EppWorkPlanRowPartLoad loadObject = e.getValue().get(fullCode);
                if (null != loadObject) {
                    final Double load = loadObject.getLoadAsDouble();
                    if ((null != load) && (load > 0)) {
                        value += load;
                    }
                }
            }

            final EppWorkPlanRowPartLoad loadObject = e.getValue().get(Controller.ELOAD_AUDIENCE_FULL_CODE);
            if (null != loadObject) {
                final Double audienceLoad = loadObject.getLoadAsDouble();
                if ((null == audienceLoad) || (audienceLoad <= 0)) {
                    loadObject.setLoadAsDouble(value);
                } else if (!UniEppUtils.eq(audienceLoad.doubleValue(), value)) {
                    errorCollector.add("Общая аудиторная нагрузка для " + e.getKey() + "-й части не соответсвует введенным значениям.", Model.getInputId(e.getKey(), Controller.ELOAD_AUDIENCE_FULL_CODE));
                }
            }
        }

        final Map<String, IdentifiableWrapper> loadCode2useCycle = model.getLoadCode2useCycle();
        for (final Entry<Integer, Map<String, EppWorkPlanRowPartLoad>> entry : partLoadMap.entrySet())
        {
            for (final Entry<String, EppWorkPlanRowPartLoad> subEntry : entry.getValue().entrySet())
            {
                final String fullCode = subEntry.getValue().getLoadType().getFullCode();
                final IdentifiableWrapper wrapper = loadCode2useCycle.get(fullCode);
                if ((null != wrapper) && (Model.SECOND_OPTION_ID == wrapper.getId()))
                {
                    final Double loadValue = subEntry.getValue().getLoadAsDouble();
                    final Double loadAsDouble = (null == loadValue) ? 0d : loadValue;

                    final Double daysValue = subEntry.getValue().getDaysAsDouble();
                    final Double daysAsDouble = (null == daysValue) ? 0d : daysValue;

                    if ((loadAsDouble > 0) && (daysAsDouble <= 0))
                    {
                        errorCollector.add("Цикловая нагрузка должна быть больше нуля.", Model.getInputDayId(entry.getKey(), fullCode));
                    }
                }
            }
        }

        if (errorCollector.hasErrors())
        {
            return;
        }

        this.getDao().update(this.getModel(component));
        this.deactivate(component);
    }
}
