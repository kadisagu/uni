package ru.tandemservice.uniepp.entity.registry;

import ru.tandemservice.uniepp.base.bo.EppState.EppStatePath;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementPartFControlActionGen;

/**
 * Часть элемента реестра: итоговая форма контроля
 *
 * Указывает набор итоговый форм контроля по части элемента реестра
 */
@EppStatePath(EppRegistryElementPartFControlAction.L_PART+"."+EppRegistryElementPart.L_REGISTRY_ELEMENT)
public class EppRegistryElementPartFControlAction extends EppRegistryElementPartFControlActionGen implements IEppStateObject
{
    public EppRegistryElementPartFControlAction() {}

    public EppRegistryElementPartFControlAction(final EppRegistryElementPart part, final EppFControlActionType controlAction) {
        this.setPart(part);
        this.setControlAction(controlAction);
    }

    public EppRegistryElementPartFControlAction(final EppRegistryElementPart part, final EppFControlActionType controlAction, final EppGradeScale gradeScale) {
        this(part, controlAction);
        this.setGradeScale(gradeScale);
    }

    @Override public String getTitle() {
        if (getPart() == null) {
            return this.getClass().getSimpleName();
        }
        return this.getPart().getTitle() + ": " + this.getControlAction().getTitle();
    }

    @Override public EppState getState() {
        return this.getPart().getState();
    }

    @Override
    public EppGradeScale getGradeScale()
    {
        // если есть локальное значение - выводим его
        final EppGradeScale gradeScale = super.getGradeScale();
        if (null != gradeScale) { return gradeScale; }

        // если указана форма контроля - то берем из нее
        final EppFControlActionType controlAction = this.getControlAction();
        if (null != controlAction) { return controlAction.getDefaultGradeScale(); }

        // увы - у нас просто какой-то объект, причем не в базе
        return null;
    }

}