package ru.tandemservice.uniepp.entity.contract;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContextObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uniepp.entity.contract.gen.EppCtrEducationResultGen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Зачисление в контингент студентов
 *
 * Обязательство обучить человека (dst - обучаемый)
 */
public class EppCtrEducationResult extends EppCtrEducationResultGen implements ISecLocalEntityOwner, ICtrContextObject
{

    @Override
    public CtrContractObject getContractObject() {
        return getContract();
    }

    @Override
    public PersonRole getPersonRole() {
        return getStudent();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities() {
        final List<IEntity> result = new ArrayList<IEntity>(this.getTarget().getStudent().getSecLocalEntities());
        result.add(this.getContract().getOrgUnit());
        return result;
    }

    @Override
    public boolean isStillAlive() {
        return false; // удалять можно всегда
        //final Student student = getTarget().getStudent();
        // если студент уже стал архивным, то факт исполнения протух вместе со студентом
        //return !student.isArchival();
    }

}