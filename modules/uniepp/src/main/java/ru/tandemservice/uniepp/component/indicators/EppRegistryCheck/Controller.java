/* $Id:$ */
package ru.tandemservice.uniepp.component.indicators.EppRegistryCheck;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.CheckPrint.EppWorkPlanCheckPrint;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryCheckPrintDAO;

import java.util.Collections;
import java.util.Set;

/**
 * @author oleyba
 * @since 5/30/12
 */
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=8945678")
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        try {
            Long ouId = getModel(component).getOrgUnitHolder().getId();
            Set<Long> orgUnits = ouId == null ? null : Collections.singleton(ouId);
            final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(IEppRegistryCheckPrintDAO.instance.get().printReport(orgUnits), "RegistryCheck.xls");
            this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "xls")));
        }
        catch (Throwable throwable) {
            throwable.printStackTrace();
            throw new ApplicationException(ApplicationRuntime.getProperty(CoreExceptionUtils.APPLICATION_ERROR_MESSAGE_PROPETY));
        }
    }

    public void onClickPrintCheckWorkPlan(final IBusinessComponent component)
    {
        component.createDialogRegion(
                new ComponentActivator("EppWorkPlanCheckPrint", new ParametersMap().add(EppWorkPlanCheckPrint.ORG_UNIT_ID, getModel(component).getOrgUnitHolder().getId()))
        );
    }
}
