package ru.tandemservice.uniepp.entity.plan.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка УП (дисциплина по выбору)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvGroupImRowGen extends EppEpvTermDistributedRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow";
    public static final String ENTITY_NAME = "eppEpvGroupImRow";
    public static final int VERSION_HASH = -405028181;
    private static IEntityMeta ENTITY_META;

    public static final String P_SIZE = "size";

    private int _size = 1;     // Число дисциплин

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Число дисциплин. Свойство не может быть null.
     */
    @NotNull
    public int getSize()
    {
        return _size;
    }

    /**
     * @param size Число дисциплин. Свойство не может быть null.
     */
    public void setSize(int size)
    {
        dirty(_size, size);
        _size = size;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEpvGroupImRowGen)
        {
            setSize(((EppEpvGroupImRow)another).getSize());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvGroupImRowGen> extends EppEpvTermDistributedRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvGroupImRow.class;
        }

        public T newInstance()
        {
            return (T) new EppEpvGroupImRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "size":
                    return obj.getSize();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "size":
                    obj.setSize((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "size":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "size":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "size":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvGroupImRow> _dslPath = new Path<EppEpvGroupImRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvGroupImRow");
    }
            

    /**
     * @return Число дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow#getSize()
     */
    public static PropertyPath<Integer> size()
    {
        return _dslPath.size();
    }

    public static class Path<E extends EppEpvGroupImRow> extends EppEpvTermDistributedRow.Path<E>
    {
        private PropertyPath<Integer> _size;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Число дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow#getSize()
     */
        public PropertyPath<Integer> size()
        {
            if(_size == null )
                _size = new PropertyPath<Integer>(EppEpvGroupImRowGen.P_SIZE, this);
            return _size;
        }

        public Class getEntityClass()
        {
            return EppEpvGroupImRow.class;
        }

        public String getEntityName()
        {
            return "eppEpvGroupImRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
