package ru.tandemservice.uniepp.component.registry.ModuleRegistry.AddEdit;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.Return;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniepp.component.base.SimpleAddEditModel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id"),
    @Bind(key="inline", binding="inline")
})
@Return({
    @Bind(key="eppRegistryModuleId", binding="element.id")
})
public class Model {

    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    public boolean isEditForm() { return SimpleAddEditModel.isEditForm(this.getElement()); }
    public boolean isReadOnly() { return SimpleAddEditModel.isReadOnly(this.getElement()); }

    private boolean inline = false;
    public Boolean getInline() { return this.inline; }
    public void setInline(final Boolean inline) { this.inline = Boolean.TRUE.equals(inline); }

    private EppRegistryModule element;
    public EppRegistryModule getElement() { return this.element; }
    public void setElement(final EppRegistryModule element) { this.element = element; }

    private ISelectModel ownerSelectModel;
    public ISelectModel getOwnerSelectModel() { return this.ownerSelectModel; }
    public void setOwnerSelectModel(final ISelectModel ownerSelectModel) { this.ownerSelectModel = ownerSelectModel; }

    private List<HSelectOption> parentList = Collections.emptyList();;
    public List<HSelectOption> getParentList() { return this.parentList; }
    public void setParentList(final List<HSelectOption> parentList) { this.parentList = parentList; }

    private IEntity current;
    public IEntity getCurrent() { return this.current; }
    public void setCurrent(final IEntity current) { this.current = current; }

    private Collection<EppRegistryModuleIControlAction> controlActionList = Collections.emptyList();
    public Collection<EppRegistryModuleIControlAction> getControlActionList() { return this.controlActionList; }
    public void setControlActionList(final Collection<EppRegistryModuleIControlAction> controlActionList) { this.controlActionList = controlActionList; }

    private Collection<EppRegistryModuleALoad> loadList = Collections.emptyList();
    public Collection<EppRegistryModuleALoad> getLoadList() { return this.loadList; }
    public void setLoadList(final Collection<EppRegistryModuleALoad> loadList) { this.loadList = loadList; }

}
