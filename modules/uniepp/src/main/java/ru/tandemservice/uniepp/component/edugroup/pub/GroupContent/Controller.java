package ru.tandemservice.uniepp.component.edugroup.pub.GroupContent;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.BackgroundProcessBase;
import org.tandemframework.core.process.IBackgroundProcess;
import org.tandemframework.core.process.ProcessResult;
import org.tandemframework.core.process.ProcessState;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    private static final String COMPONENT_ATTACH_WORK_PLAN_ROW = ru.tandemservice.uniepp.component.edugroup.AttachWorkPlanRow.Model.class.getPackage().getName();
    private static final String COMPONENT_ATTACH_PPS = ru.tandemservice.uniepp.component.edugroup.AttachTutors.Model.class.getPackage().getName();

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        final DynamicListDataSource<EppRealEduGroupRow> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareDataSource(Controller.this.getModel(component1));
        });


        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new PublisherLinkColumn("Студент", Student.person().fio(), EppRealEduGroupRow.studentWpePart().studentWpe().student()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Курс", EppRealEduGroupRow.studentWpePart().studentWpe().course().title()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Группа", EppRealEduGroupRow.studentGroupTitle()).setClickable(false).setOrderable(true));

        {
            final AbstractColumn rowActivityPartColumn = new SimpleColumn("Дисциплина\nпо МСРП", EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart().titleWithNumber()).setClickable(false).setOrderable(true);
            final String path = EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart().toString();
            rowActivityPartColumn.setStyleResolver(rowEntity -> {
                final EppRegistryElementPart part = (EppRegistryElementPart) rowEntity.getProperty(path);
                if (EntityBase.equals(part, model.getGroup().getActivityPart())) { return null; }
                return "color:"+ UniDefines.COLOR_CELL_WARNING;
            });
            IStyleResolver resolver = rowEntity -> {
                EppRealEduGroupRow row = ((EppRealEduGroupRow) rowEntity);
                if (row.getGroup().getActivityPart().equals(row.getStudentWpePart().getStudentWpe().getRegistryElementPart()))
                    return "";
                return "background-color: #fcebd7;";
            };
            rowActivityPartColumn.setStyleResolver(resolver);
            dataSource.addColumn(rowActivityPartColumn);
        }

        dataSource.addColumn(new SimpleColumn("Часть", EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart().number()).setClickable(false).setOrderable(false));

//        dataSource.addColumn(new PublisherLinkColumn("УП(в)", EppEduPlanVersion.P_FULL_NUMBER, EppRealEduGroupRow.studentWpePart().studentWpe().student().eduPlanVersion()).setFormatter(nowrap).setOrderable(true));
        dataSource.addColumn(new PublisherLinkColumn("РУП", EppWorkPlanBase.P_FULL_NUMBER, EppRealEduGroupRow.studentWpePart().studentWpe().sourceRow().workPlan()).setFormatter(NoWrapFormatter.INSTANCE).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Формирующее\nподразделение", EppRealEduGroupRow.studentEducationOrgUnit().formativeOrgUnit().shortTitle()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Территориальное\nподразделение", EppRealEduGroupRow.studentEducationOrgUnit().territorialOrgUnit().territorialShortTitle()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Выпускающее\nподразделение", EppRealEduGroupRow.studentEducationOrgUnit().educationLevelHighSchool().orgUnit().shortTitle()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Направление подготовки\n(специальность)", EppRealEduGroupRow.studentEducationOrgUnit().educationLevelHighSchool().displayableTitle()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Характеристики\nобучения", EppRealEduGroupRow.studentEducationOrgUnit().developCombinationTitle()).setClickable(false).setOrderable(true));
        model.setDataSource(dataSource);
    }


    public void onClickResetLevel(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        IEppRealGroupRowDAO.instance.get().doResetLevel(Collections.singleton(model.getGroup().getId()));
        ContextLocal.getDesktop().setRefreshScheduled(true);
    }

    public void onClickAttachWorkPlanRow(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        IEppRealGroupRowDAO.instance.get().checkGroupLockedEdit(Collections.singleton(model.getGroup().getId()), model.getLevel());
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            Controller.COMPONENT_ATTACH_WORK_PLAN_ROW,
            new ParametersMap().add("ids", Collections.singleton(model.getGroup().getId())).add("levelId", model.getLevel().getId())
        ));
    }

    public void onClickAttachPPS(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        IEppRealGroupRowDAO.instance.get().checkGroupLockedEdit(Collections.singleton(model.getGroup().getId()), model.getLevel());
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            Controller.COMPONENT_ATTACH_PPS,
            new ParametersMap().add("ids", Collections.singleton(model.getGroup().getId())).add("levelId", model.getLevel().getId())
        ));
    }

    public void onClickSplitGroups(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final EppRealEduGroupCompleteLevel level = model.getLevel();
        IEppRealGroupRowDAO.instance.get().checkGroupLockedEdit(Collections.singleton(model.getGroup().getId()), level);

        final CheckboxColumn checkboxColumn = (CheckboxColumn)model.getDataSource().getColumn("select");
        final Collection<Long> ids = new HashSet<>(UniBaseDao.ids(checkboxColumn.getSelectedObjects()));

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try  {
            final String param = StringUtils.trimToNull(component.<String>getListenerParameter());
            if (null == param)
            {
                if (ids.isEmpty()) { throw new ApplicationException("Не выбран ни один студент."); }
                IEppRealGroupRowDAO.instance.get().doSplit(ids, level, level);
                this.onRefreshComponent(component);
            }
            else
            {
                final PropertyPath[] key;
                if ("split-by-groups".equals(param)) {
                    key = new PropertyPath[] { EppRealEduGroupRow.studentGroupTitle() };
                } else if ("split-by-owners".equals(param)) {
                    key = new PropertyPath[] { EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart().registryElement().owner().id(), EppRealEduGroupRow.studentGroupTitle() };
                } else if ("split-by-activity".equals(param)) {
                    key = new PropertyPath[] { EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart().id(), EppRealEduGroupRow.studentGroupTitle() };
                } else {
                    throw new IllegalArgumentException(param);
                }

                final Collection<Collection<Long>> values = new ArrayList<>(this.getDao().getRowDistributionMap(model, ids, key).values());
                if (values.size() > 0)
                {
                    final IBackgroundProcess process = new BackgroundProcessBase() {
                        @Override public ProcessResult run(final ProcessState state) {
                            try {
                                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                                try  {
                                    int currentValue = 0;
                                    for (final Collection<Long> xids: values) {
                                        IEppRealGroupRowDAO.instance.get().doSplit(xids, level, level);
                                        state.setMaxValue(100);
                                        state.setCurrentValue((100*(currentValue++))/values.size());

                                        Thread.yield();
                                    }
                                    return null; // закрываем диалог
                                } finally {
                                    eventLock.release();
                                }
                            } catch (final Throwable t) {
                                // TODO: логировать исключение
                                return null;
                            }
                        }
                    };

                    new BackgroundProcessHolder().start("Разбиение учебной группы", process);
                    model.setDataSource(null); // дабы на время показа дочерней формы не грузить список студентов еще раз
                }
            }

        } finally {
            eventLock.release();
        }

        checkboxColumn.getSelectedObjects().clear();

    }

    public void onClickEdit(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            ru.tandemservice.uniepp.component.edugroup.GroupEdit.Model.class.getPackage().getName(),
            new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getGroup().getId()).add("levelId", model.getLevel().getId())
        ));
    }

}
