package ru.tandemservice.uniepp.component.registry.base.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import ru.tandemservice.uni.util.AddEditResult;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public class Controller extends AbstractBusinessController<IDAO, Model> {

    public static Long executeSave(final IBusinessComponent component) {
        final Model model = component.getModel();
        if (Boolean.TRUE.equals(model.isReadOnly())) { return model.getElement().getId(); }

        ((ru.tandemservice.uniepp.component.registry.base.AddEdit.Controller)component.getController()).save(component);
        return model.getElement().getId();
    }


    @Override public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onClickApply(final IBusinessComponent component) {
        AddEditResult.saveAndDeactivate(component, this.getModel(component).getElement(), () -> Controller.this.save(component));
    }

    public void onClickFillLoadFromParts(final IBusinessComponent component) {
        getModel(component).fillLoadFromParts();
    }

    protected void save(final IBusinessComponent component) {
        this.getDao().save(this.getModel(component));
    }

    public void onEduGroupSplitVariantChange(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        if (model.getElement().getEduGroupSplitVariant() == null) {
            model.getEduGroupTypes().clear();
            model.getElement().setEduGroupSplitByType(false);
        }
    }

}
