/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockContent;

import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;

/**
 * @author Nikolay Fedorovskih
 * @since 14.01.2015
 */
public class EppInteractiveRowWrapper extends EppFakeRowWrapper
{
    public EppInteractiveRowWrapper(IEppEpvRowWrapper parentRow)
    {
        super(parentRow);
    }

    @Override
    public String wrapFullCode(String loadFullCode)
    {
        return EppALoadType.iFullCode(loadFullCode);
    }

    @Override
    public String getDisplayableTitle()
    {
        return "в интерактивной форме";
    }

    @Override
    public String getTotalDisplayableLoadString(Integer term, String loadFullCode, boolean hideZero, int divider)
    {
        if (EppLoadType.FULL_CODE_TOTAL_HOURS.equals(loadFullCode))
            return "";

        return super.getTotalDisplayableLoadString(term, loadFullCode, hideZero, divider);
    }

    @Override
    public String getRowWrapperTotalByLoadTypeRaw(String loadFullCode, int divider)
    {
        if (EppLoadType.FULL_CODE_CONTROL.equals(loadFullCode))
            return "";

        return super.getRowWrapperTotalByLoadTypeRaw(loadFullCode, divider);
    }
}