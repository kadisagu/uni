package ru.tandemservice.uniepp.component.edugroup.list.TutorOrgUnitTab;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.component.edugroup.EduGroupOwnerModel;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DAO extends ru.tandemservice.uniepp.component.edugroup.list.GroupListOwnerTabBase.DAO implements IDAO
{
    @Override protected String getSettingsName() {
        return "group";
    }

    /**
     * все сводки, на которых есть УГС, элементы реестров которых с текущего подразделения
     */
    @Override
    protected List<EppRealEduGroupSummary> summaryList(EduGroupOwnerModel model) {
        return new DQLSelectBuilder().fromEntity(EppRealEduGroupSummary.class, "s")
                .column(property("s"))
                .where(exists(
                        EppRealEduGroup.class,
                        EppRealEduGroup.summary().s(), property("s"),
                        EppRealEduGroup.activityPart().registryElement().owner().s(), model.getOrgUnit()
                ))
                .createStatement(getSession()).list();
    }

    /**
     * все УГС (с указанной сводки), элементы реестров которых с текущего подразделения
     */
    @Override
    protected DQLSelectBuilder groupWithRelationsDql(EduGroupOwnerModel model, EppRealEduGroupSummary summary, Class<? extends EppRealEduGroup> groupClass) {
        return super.groupWithRelationsDql(model, summary, groupClass)
                .where(eq(property(EppRealEduGroupRow.group().activityPart().registryElement().owner().fromAlias("rel")), value(model.getOrgUnit())));
    }
}
