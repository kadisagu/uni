package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_2x11x3_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppProfessionalTask

		//  свойство shortTitle стало обязательным
		{
			// задать значение по умолчанию
			// у клиентов таблица будет пустой (дефолт проставлен, чтобы на наших стендах ничего не затирать)
			java.lang.String defaultShortTitle = "ПЗ";
			tool.executeUpdate("update epp_c_prof_task_t set shorttitle_p=? where shorttitle_p is null", defaultShortTitle);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_c_prof_task_t", "shorttitle_p", false);

		}


    }
}