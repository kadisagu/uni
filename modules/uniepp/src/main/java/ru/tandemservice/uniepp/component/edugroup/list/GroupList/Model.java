package ru.tandemservice.uniepp.component.edugroup.list.GroupList;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections15.Factory;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.IDevelopCombination;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.component.base.SimpleOrgUnitBasedModel;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key="parameters", binding="parameters", required=true),
    @Bind(key="levelId", binding="levelHolder.id"),
    @Bind(key="dql", binding="dql"),
    @Bind(key="secPostfix", binding="secPostfix")
})
@SuppressWarnings("unchecked")
public class Model extends SimpleOrgUnitBasedModel
{

    private IGroupListParameters parameters;
    public IGroupListParameters getParameters() { return this.parameters; }
    public void setParameters(final IGroupListParameters parameters) { this.parameters = parameters; }

    public Factory<DQLSelectBuilder> getDqlFactory() { return this.getParameters().getDqlFactory(); }
    public EppRealEduGroupRow.IEppRealEduGroupRowDescription getRelationDescription() { return this.getParameters().getRelationDescription(); }
    public EppRealEduGroupCompleteLevel getLevel() { return this.getParameters().getLevel(); }
    public OrgUnitHolder getOrgUnitHolder() { return this.getParameters().getOrgUnitHolder(); }
    public CommonPostfixPermissionModelBase getSec() { return this.getOrgUnitHolder().getSecModel(); }

    public Long getOrgUnitId() { return this.getOrgUnitHolder().getId(); }
    @Override public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }

    private IDataSettings settings;
    public IDataSettings getSettings() { return this.settings; }
    public void setSettings(final IDataSettings settings) { this.settings = settings; }

    private DynamicListDataSource<EppRealEduGroup> dataSource;
    public DynamicListDataSource<EppRealEduGroup> getDataSource() { return this.dataSource; }
    public void setDataSource(final DynamicListDataSource<EppRealEduGroup> dataSource) { this.dataSource = dataSource; }

    ////////////////////
    //// уровни УГС ////

    private List<EppRealEduGroupCompleteLevel> _levels = new ArrayList<EppRealEduGroupCompleteLevel>();
    public List<EppRealEduGroupCompleteLevel> getLevels(){ return this._levels; }
    public void setLevels(final List<EppRealEduGroupCompleteLevel> levels) { this._levels = levels; }

    private EppRealEduGroupCompleteLevel _curLevel;
    public EppRealEduGroupCompleteLevel getCurLevel() { return this._curLevel; }
    public void setCurLevel(final EppRealEduGroupCompleteLevel curLevel) { this._curLevel = curLevel; }

    /////////////////
    //// фильтры ////

    public static final Long FILL_STATUS_EMPTY = 1L;
    public static final Long FILL_STATUS_FILLED = 2L;

    private static final IdentifiableWrapper<IEntity>[] FILL_STATUS_ARRAY = new IdentifiableWrapper[] {
        new IdentifiableWrapper<IEntity>(Model.FILL_STATUS_EMPTY, "Нет активных студентов"),
        new IdentifiableWrapper<IEntity>(Model.FILL_STATUS_FILLED, "Есть активные студенты")

    };
    public List<IdentifiableWrapper<IEntity>> getFillStatusList() { return ImmutableList.copyOf(Model.FILL_STATUS_ARRAY); }

    protected IdentifiableWrapper<IEntity> getFilterFillStatus() {
        return (IdentifiableWrapper<IEntity>) this.getSettings().get("filterFillStatus");
    }


    private List<HSelectOption> levelHierarchyList;
    public List<HSelectOption> getLevelHierarchyList() { return this.levelHierarchyList; }
    public void setLevelHierarchyList(final List<HSelectOption> levelHierarchyList) { this.levelHierarchyList = levelHierarchyList; }

    protected IEntity getFilterLevel() {
        return (IEntity) this.getSettings().get("filterLevel");
    }


    private ISelectModel formativeOrgUnitModel;
    public ISelectModel getFormativeOrgUnitModel() { return this.formativeOrgUnitModel; }
    public void setFormativeOrgUnitModel(final ISelectModel formativeOrgUnitModel) { this.formativeOrgUnitModel = formativeOrgUnitModel; }

    protected Collection<OrgUnit> getFormativeOrgUnits() {
        final Object registryOwner = this.getSettings().get("formativeOrgUnits");
        if ((registryOwner instanceof Collection) && (((Collection)registryOwner).size() > 0)) { return (Collection)registryOwner; }
        return null;
    }


    private ISelectModel territorialOrgUnitModel;
    public ISelectModel getTerritorialOrgUnitModel() { return this.territorialOrgUnitModel; }
    public void setTerritorialOrgUnitModel(final ISelectModel territorialOrgUnitModel) { this.territorialOrgUnitModel = territorialOrgUnitModel; }

    protected Collection<OrgUnit> getTerritorialOrgUnits() {
        final Object registryOwner = this.getSettings().get("territorialOrgUnits");
        if ((registryOwner instanceof Collection) && (((Collection)registryOwner).size() > 0)) { return (Collection)registryOwner; }
        return null;
    }


    private ISelectModel educationHighSchoolModel;
    public ISelectModel getEducationHighSchoolModel() { return this.educationHighSchoolModel; }
    public void setEducationHighSchoolModel(final ISelectModel educationHighSchoolModel) { this.educationHighSchoolModel = educationHighSchoolModel; }

    protected Collection<EducationLevelsHighSchool> getEducationHighSchools() {
        final Object registryOwner = this.getSettings().get("educationHighSchools");
        if ((registryOwner instanceof Collection) && (((Collection)registryOwner).size() > 0)) { return (Collection)registryOwner; }
        return null;
    }


    private ISelectModel developCombinationModel;
    public ISelectModel getDevelopCombinationModel() { return this.developCombinationModel; }
    public void setDevelopCombinationModel(final ISelectModel developCombinationModel) { this.developCombinationModel = developCombinationModel; }

    protected Collection<IDevelopCombination> getDevelopCombinations() {
        final Object registryOwner = this.getSettings().get("developCombinations");
        if ((registryOwner instanceof Collection) && (((Collection)registryOwner).size() > 0)) { return (Collection)registryOwner; }
        return null;
    }




    private ISelectModel registryOwnerModel;
    public ISelectModel getRegistryOwnerModel() { return this.registryOwnerModel; }
    public void setRegistryOwnerModel(final ISelectModel registryOwnerModel) { this.registryOwnerModel = registryOwnerModel; }

    protected Collection<OrgUnit> getRegistryOwners() {
        final Object registryOwner = this.getSettings().get("registryOwner");
        if ((registryOwner instanceof Collection) && (((Collection)registryOwner).size() > 0)) { return (Collection)registryOwner; }
        return null;
    }


    private ISelectModel registryElementModel;
    public ISelectModel getRegistryElementModel() { return this.registryElementModel; }
    public void setRegistryElementModel(final ISelectModel registryElementModel) { this.registryElementModel = registryElementModel; }

    protected Collection<EppRegistryElement> getRegistryElements() {
        final Object registryElement = this.getSettings().get("registryElement");
        if ((registryElement instanceof Collection) && (((Collection)registryElement).size() > 0)) { return (Collection)registryElement; }
        return null;
    }


    private ISelectModel courseModel;
    public ISelectModel getCourseModel() { return this.courseModel; }
    public void setCourseModel(final ISelectModel courseModel) { this.courseModel = courseModel; }

    protected Collection<Course> getCourses() {
        final Object course = this.getSettings().get("course");
        if ((course instanceof Collection) && (((Collection)course).size() > 0)) { return (Collection)course; }
        return null;
    }


    private ISelectModel groupModel;
    public ISelectModel getGroupModel() { return this.groupModel; }
    public void setGroupModel(final ISelectModel groupModel) { this.groupModel = groupModel; }

    protected Collection<String> getGroups() {
        final Object group = this.getSettings().get("group");
        if ((group instanceof Collection) && (((Collection)group).size() > 0)) { return (Collection)group; }
        return null;
    }


    private ISelectModel studentTitleModel;
    public ISelectModel getStudentTitleModel() { return this.studentTitleModel; }
    public void setStudentTitleModel(final ISelectModel studentTitleModel) { this.studentTitleModel = studentTitleModel; }

    protected String getStudentTitle() {
        final Object title = this.getSettings().get("filterStudentTitle");
        if (title instanceof String) { return StringUtils.trimToNull((String)title); }
        return null;
    }


    private ISelectModel ppsTitleModel;
    public ISelectModel getPpsTitleModel() { return this.ppsTitleModel; }
    public void setPpsTitleModel(final ISelectModel ppsTitleModel) { this.ppsTitleModel = ppsTitleModel; }

    protected String getPpsTitle() {
        final Object title = this.getSettings().get("filterPpsTitle");
        if (title instanceof String) { return StringUtils.trimToNull((String)title); }
        return null;
    }

    //// фильтры ////
    /////////////////


    public boolean isFormingButtonVisible()
    {
        return (
                (CoreServices.securityService().check(this.getOrgUnit(), ContextLocal.getUserContext().getPrincipalContext(), this.getSec().getPermission("sendFormingFromPrevious_eppEduGroup"))) ||
                (CoreServices.securityService().check(this.getOrgUnit(), ContextLocal.getUserContext().getPrincipalContext(), this.getSec().getPermission("sendForming_eppEduGroup")))
        );
    }
}
