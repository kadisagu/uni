package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniepp.component.base.SimpleOrgUnitBasedModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

/**
 * 
 * @author nkokorina
 *
 */

@State( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "workPlanVersion.id"),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class Model extends SimpleOrgUnitBasedModel {

    private String selectedTab;
    public String getSelectedTab() {
        return this.selectedTab;
    }
    public void setSelectedTab(final String selectedTab) {
        this.selectedTab = selectedTab;
    }

    private EppWorkPlanVersion workPlanVersion = new EppWorkPlanVersion();
    public EppWorkPlanVersion getWorkPlanVersion() {
        return this.workPlanVersion;
    }
    public void setWorkPlanVersion(final EppWorkPlanVersion workPlanVersion) {
        this.workPlanVersion = workPlanVersion;
    }

    @Override
    public OrgUnit getOrgUnit() {
        return null;
    }

    public EppEduPlanVersion getEduPlanVersion() {
        return getWorkPlanVersion().getParent().getEduPlanVersion();
    }

    public EppEduPlan getEduPlan() {
        return getWorkPlanVersion().getParent().getEduPlan();
    }
}
