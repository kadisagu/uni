package ru.tandemservice.uniepp.entity.contract.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData;
import ru.tandemservice.uniepp.entity.contract.EppCtrContractVersionTemplateData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные шаблона версии договора на обучение
 *
 * Данные базового шаблона для создания договора или доп.соглашения на обучение.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppCtrContractVersionTemplateDataGen extends CtrContractVersionTemplateData
 implements IEducationContractVersionTemplateData{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.contract.EppCtrContractVersionTemplateData";
    public static final String ENTITY_NAME = "eppCtrContractVersionTemplateData";
    public static final int VERSION_HASH = 1633373313;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";

    private EducationYear _educationYear;     // Учебный год, на который создана версия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год, на который создана версия.
     */
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год, на который создана версия.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppCtrContractVersionTemplateDataGen)
        {
            setEducationYear(((EppCtrContractVersionTemplateData)another).getEducationYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppCtrContractVersionTemplateDataGen> extends CtrContractVersionTemplateData.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppCtrContractVersionTemplateData.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppCtrContractVersionTemplateData is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return obj.getEducationYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return EducationYear.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppCtrContractVersionTemplateData> _dslPath = new Path<EppCtrContractVersionTemplateData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppCtrContractVersionTemplateData");
    }
            

    /**
     * @return Учебный год, на который создана версия.
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrContractVersionTemplateData#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    public static class Path<E extends EppCtrContractVersionTemplateData> extends CtrContractVersionTemplateData.Path<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год, на который создана версия.
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrContractVersionTemplateData#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

        public Class getEntityClass()
        {
            return EppCtrContractVersionTemplateData.class;
        }

        public String getEntityName()
        {
            return "eppCtrContractVersionTemplateData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
