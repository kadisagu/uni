package ru.tandemservice.uniepp.component.edustd.EduStdPub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setEppStateEduStandard(this.getNotNull(EppStateEduStandard.class, model.getEppStateEduStandard().getId()));
    }
}
