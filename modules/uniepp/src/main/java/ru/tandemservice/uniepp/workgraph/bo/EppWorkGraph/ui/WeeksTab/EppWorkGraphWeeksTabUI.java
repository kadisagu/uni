package ru.tandemservice.uniepp.workgraph.bo.EppWorkGraph.ui.WeeksTab;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.json.JSONArray;
import org.apache.tapestry.json.JSONLiteral;
import org.apache.tapestry.json.JSONObject;
import org.hibernate.Session;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.BaseRawFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.commonbase.tapestry.component.spreadsheet.ISpreadsheetCellDataValueResolver;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.gen.CourseGen;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.dao.year.IEppYearDAO;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppWeekPart;
import ru.tandemservice.uniepp.entity.pupnag.*;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRow2EduPlanGen;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRowGen;
import ru.tandemservice.uniepp.util.EppEduPlanVersionScheduleUtils;
import ru.tandemservice.uniepp.util.TermsBorders;
import ru.tandemservice.uniepp.util.WeekTypeLegendRow;

import java.text.ParseException;
import java.util.*;

import static ru.tandemservice.uniepp.util.EppEduPlanVersionScheduleGridUtils.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created by nsvetlov on 11.07.2016.
 */

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})

public class EppWorkGraphWeeksTabUI extends UIPresenter
{
    private static final String VERSION = "VERSION";
    private static final String NUMBER = "num";
    public static final String WN = "wn";
    private EntityHolder<EppWorkGraph> _holder = new EntityHolder<>();
    private Map<String, EppWeek> _weekIdByNum = new HashMap<>();
    private final Map<String, EppWeekType> _weekTypeMap = IEppSettingsDAO.instance.get().getWeekTypeEditAbbreviationMap();
    private boolean _editMode = false;
    private SimpleListDataSource<ViewWrapper<EppWorkGraphWeekPart>> _dataSource;
    private List<WeekTypeLegendRow> _weekTypeLegendList;          // строки легенды
    private WeekTypeLegendRow _weekTypeLegendItem;                // текущее значение цикла
    private Map<TripletKey<Long, Long, String>, EppWorkGraphWeekPart> changes;
    private List<Course> _courseList;                     // список курсов обучения в гуп, согласно учебной сетке
    private ISelectModel _programSubjectModel;
    private EppYearEducationWeek[] _weekData;
    private Set<Long> _filtedIds;                                            // мн-во версий УП в строке ГУП, которые удовлетворяют выбранным значениями фильтра НП(с)
    TermsBorders _borders;
    private Map<PairKey<Long, String>, EppWorkGraphRowWeek> _versionWeekByNum;
    private Map<TripletKey<Long, Long, String>, EppWorkGraphWeekPart> _dataMap;

    public final ISpreadsheetCellDataValueResolver CELL_DATA_VALUE_RESOLVER = new ISpreadsheetCellDataValueResolver()
    {
        @Override
        public CoreCollectionUtils.Pair<ResolverResult, Object> resolve(int col, int row, AbstractColumn column, IEntity rowEntity, String newValue)
        {
            try
            {
                boolean isNotEmpty = newValue != null && newValue.length() > 0;
                EppWorkGraphWeekPart weekPart = ((ViewWrapper<EppWorkGraphWeekPart>) rowEntity).getEntity();
                TermsBorders border = getBorders();
                if (FIRST.equals(column.getKey()))
                {
                    int[] numbers = parseBorders(newValue);
                    for(int i = 0; i < numbers.length; i++)
                        border.setFirst(i, numbers[i]);
                } else if (LAST.equals(column.getKey()))
                {
                    int[] numbers = parseBorders(newValue);
                    for(int i = 0; i < numbers.length; i++)
                        border.setLast(i, numbers[i]);
                } else
                {
                    if (border.out(column.getKey().toString())) {
                        if (isNotEmpty)
                        {
                            return returnError("Заполнено значение за пределами семестра");
                        }
                    } else if (!isNotEmpty) { // Заменяем пустые значения на тип "теория"
                        newValue = THEORY.getAbbreviationEdit();
                        isNotEmpty = true;
                    }
                    if (isNotEmpty && !getWeekTypeMap().containsKey(newValue.toUpperCase()))
                    {
                        return returnError("Неизвестный тип недели «" + newValue + "»");
                    }
                    prepareWeekPartForSave(column.getKey().toString(), weekPart, newValue, isNotEmpty);
                }
                return null;
            } catch (Exception ex)
            {
                return returnError(ex.getMessage() == null ? "Ошибка сохранения ячейки" : ex.getMessage());
            }
        }

        private CoreCollectionUtils.Pair<ResolverResult, Object> returnError(String errorText)
        {
            ContextLocal.getErrorCollector().add(errorText);
            return new CoreCollectionUtils.Pair<>(ResolverResult.ERROR, errorText);
        }
    };

    private Map<String, EppWeekType> getWeekTypeMap()
    {
        return _weekTypeMap;
    }

    public EntityHolder<EppWorkGraph>  getHolder()
    {
        return this._holder;
    }

    public EppWorkGraph getWorkGraph()
    {
        return getHolder().getValue();
    }

    public Set<Long> getFiltedIds()
    {
        return this._filtedIds;
    }

    public void setFiltedIds(final Set<Long> filtedIds)
    {
        this._filtedIds = filtedIds;
    }

    public List<WeekTypeLegendRow> getWeekTypeLegendList()
    {
        return this._weekTypeLegendList;
    }

    public void setWeekTypeLegendList(final List<WeekTypeLegendRow> weekTypeLegendList)
    {
        this._weekTypeLegendList = weekTypeLegendList;
    }

    public WeekTypeLegendRow getWeekTypeLegendItem()
    {
        return this._weekTypeLegendItem;
    }

    public void setWeekTypeLegendItem(final WeekTypeLegendRow weekTypeLegendItem)
    {
        this._weekTypeLegendItem = weekTypeLegendItem;
    }

    public ISelectModel getProgramSubjectModel()
    {
        return _programSubjectModel;
    }

    public void setProgramSubjectModel(ISelectModel programSubjectModel)
    {
        _programSubjectModel = programSubjectModel;
    }

    public SimpleListDataSource<ViewWrapper<EppWorkGraphWeekPart>> getDataSource()
    {
        return _dataSource;
    }


    public Boolean getCanEdit()
    {
        if (isReadOnlyState())
        {
            return false;
        }
        return !_editMode;
    }

    private boolean isReadOnlyState()
    {
        return getWorkGraph().getState().isReadOnlyState();
    }

    public void onClickEdit()
    {
        if (getCanEdit())
        {
            setEditMode(true);
            changes = new HashMap<>();
            refreshDataSource();
        }
    }

    public Boolean getCanSave()
    {
        return getEditMode();
    }

    public void onClickSave()
    {
        saveChanges();
        setEditMode(false);
        refreshDataSource();
    }

    public void onClickCancel()
    {
        setEditMode(false);
        refreshDataSource();
    }

    public void onClickApply()
    {
        saveChanges();
    }

    private void refreshDataSource()
    {
        _dataSource = buildDataSource();
    }


    public List<Course> getCourseList()
    {
        return this._courseList;
    }

    public void setCourseList(final List<Course> courseList)
    {
        this._courseList = courseList;
    }

    public Course getCourse()
    {
        Course course = getSettings().get("course" + getHolder().getId());
        if ((course == null) && !this._courseList.isEmpty()) {
            this.setCourse(course = this._courseList.get(0));
        }
        return course;
    }

    public void setCourse(final Course course)
    {
        getSettings().set("course" + getHolder().getId(), course);
    }

    public EduProgramSubject getProgramSubject()
    {
        return (EduProgramSubject) getSettings().get("programSubject" + getHolder().getId());
    }

    public void setProgramSubject(final EduProgramSubject programSubject)
    {
        getSettings().set("programSubject" + getHolder().getId(), programSubject);
    }

    public Collection<Course> getCourses()
    {
        final Course course = this.getCourse();
        return course == null ? null : Collections.singletonList(course);
    }

    public void onClickSearch()
    {
        saveSettings();
        refreshDataSource();
    }

    public ISpreadsheetCellDataValueResolver getCellDataValueResolver()
    {
        return CELL_DATA_VALUE_RESOLVER;
    }

    @Override
    public void onComponentRefresh()
    {
        // используемые курсы в гуп
        final MQBuilder builder = new MQBuilder(EppWorkGraphRow.ENTITY_CLASS, "row", new String[]{EppWorkGraphRow.L_COURSE});
        builder.add(MQExpression.eq("row", EppWorkGraphRow.L_GRAPH, getWorkGraph()));
        builder.setNeedDistinct(true);
        final List<Course> courseList = builder.getResultList(_uiSupport.getSession());
        Collections.sort(courseList, new EntityComparator<>(new EntityOrder(CourseGen.P_INT_VALUE)));

        // список курсов обучения согласно используемым версиям уп в гуп
        setCourseList(courseList);

        // обновляем фильтр курса актуальным значением
        getCourse();

        // заполнение легенды
        setWeekTypeLegendList(IEppEduPlanDAO.instance.get().getWeekTypeLegendRowList(null));
        _weekData = IEppYearDAO.instance.get().getYearEducationWeeks(getWorkGraph().getYear().getId());

        refreshDataSource();
    }

    private SimpleListDataSource<ViewWrapper<EppWorkGraphWeekPart>> buildDataSource()
    {
        SimpleListDataSource<ViewWrapper<EppWorkGraphWeekPart>> dataSource = new SimpleListDataSource<>();

        IUniBaseDao dao = IUniBaseDao.instance.get();
        List<EppWorkGraphWeekPart> items = dao.getList(new DQLSelectBuilder()
                .fromEntity(EppWorkGraphWeekPart.class, "e")
                .where(and(eq(property("e", EppWorkGraphWeekPart.week().row().graph().id()), value(getHolder().getId()))
                        , eq(property("e", EppWorkGraphWeekPart.week().row().course().intValue()), value(getCourse().getIntValue())))
                ));
        createRows(dataSource, items, dao.getComponentSession());

        return dataSource;
    }

    private void createRows(SimpleListDataSource<ViewWrapper<EppWorkGraphWeekPart>> dataSource, List<EppWorkGraphWeekPart> items, Session session)
    {
        final MQBuilder builder = new MQBuilder(EppWorkGraphRow2EduPlanGen.ENTITY_CLASS, "rel");
        builder.addJoin("rel", EppWorkGraphRow2EduPlanGen.L_ROW, "row");
        builder.add(MQExpression.eq("row", EppWorkGraphRowGen.L_GRAPH, getHolder().getValue()));
        builder.add(MQExpression.eq("row", EppWorkGraphRowGen.L_COURSE, getCourse()));
        final List<EppWorkGraphRow2EduPlan> relList = builder.getResultList(session);

        TreeMap<Long, List<EppWorkGraphWeekPart>> firstWeeks = new TreeMap<>();
        for(EppWorkGraphWeekPart item: items)
        {
            EppWorkGraphRowWeek week = item.getWeek();
            Long key = week.getRow().getId();
            List<EppWorkGraphWeekPart> partsList;
            if (!firstWeeks.containsKey(key))
            {
                partsList = new ArrayList<>(6);
                partsList.add(item);
                firstWeeks.put(key, partsList);
            } else {
                partsList = firstWeeks.get(key);
                partsList.add(item);
            }
        }

        ArrayList<ViewWrapper<EppWorkGraphWeekPart>> list = new ArrayList<>(firstWeeks.size());
        Set<Long> nonCollapsed = new HashSet<>();
        int i = 0;
        for (EppWorkGraphRow2EduPlan rel: relList)
        {
            for (Map.Entry<Long, List<EppWorkGraphWeekPart>> entry : firstWeeks.entrySet())
            {
                if (entry.getKey() != rel.getRow().getId())
                    continue;

                List<EppWorkGraphWeekPart> partList = entry.getValue();
                partList.sort((item, item1) -> (item.getWeek().getWeek() - item1.getWeek().getWeek()) * 10  + item.getNumber() - item1.getNumber());
                HashMap<Long, ViewWrapper<EppWorkGraphWeekPart>> weekParts = new HashMap<>();
                i++;
                EppWeekType value = null;
                int nWeekNum = -1;
                for (EppWorkGraphWeekPart part : partList)
                {
                    ViewWrapper<EppWorkGraphWeekPart> wrapper;
                    Long number = (long) part.getNumber();
                    if (!weekParts.containsKey(number))
                    {
                        weekParts.put(number, wrapper = new ViewWrapper<>(part));
                        wrapper.setViewProperty(NUMBER, i);
                        wrapper.setViewProperty(WN, number);
                        list.add(wrapper);
                        wrapper.setViewProperty(VERSION, BaseRawFormatter.encode(rel.getEduPlanVersion().getFullTitle()));
                    } else
                    {
                        wrapper = weekParts.get(number);
                    }
                    wrapper.setViewProperty(getWeekCode(part.getWeek()), getEditMode() ? part.getWeekType().getAbbreviationEdit() : part.getWeekType().getAbbreviationView());
                    getDataMap().put(new TripletKey<>(entry.getKey(), (long) part.getNumber(), getWeekCode(part.getWeek())), part);

                    if (part.getWeek().getWeek() != nWeekNum)
                    {
                        value = part.getWeekType();
                    } else
                    {
                        if (value.getId() != part.getWeekType().getId())
                        {
                            nonCollapsed.add((long)i);
                        }
                    }
                    nWeekNum = part.getWeek().getWeek();
                }
            }
        }
        Long one = new Long(1);
        for (int j = 0; j < list.size(); j++)
        {
            ViewWrapper<EppWorkGraphWeekPart> wrapper = list.get(j);
            if (nonCollapsed.contains(j))
            {
                wrapper.setViewProperty(DIFF, "1");
            } else if (one.equals(wrapper.getViewProperty(WN)))
            {
                wrapper.setViewProperty(DAY, "");
            }
        }


        dataSource.setEntityCollection(list);

        dataSource.addColumn(getHeadColumn());
        dataSource.setEntityCollection(list);
        dataSource.setCountRow(list.size());
    }

    public String getCustomStyle()
    {
        if (_editMode)
        {
            return null;
        }
        return CUSTOM_GRID_STYLE;
    }

    public JSONObject getUserOptions() throws ParseException
    {
        JSONObject options = new JSONObject()
                .put("autoRowSize", false)
                .put("autoColumnSize", false)
                .put("manualColumnResize", false)
                .put("manualRowResize", false)
                .put("manualColumnMove", false)
                .put("fillHandle", true)
                .put("currentRowClassName", "'currentRow'")
                .put("currentColClassName", "'currentCol'")
                .put("customBorders", getCustomBorders())
                .put("maxRows", _dataSource.getCountRow())
                .put("beforeChange", new JSONLiteral("beforeChangeValue") );

        if (_editMode)
        {
            options.put("contextMenu", getContextMenu());
        } else
        {            options.put("readOnly", true);
            options.put("contextMenu", false);
        }

        return options;
    }

    JSONArray getCustomBorders() throws ParseException
    {
        JSONArray borders = new JSONArray();
        JSONObject borderStyle = new JSONObject("{width: 6, color: \"#456fa9\"}");

        int i = 0;
        List<ViewWrapper<EppWorkGraphWeekPart>> entityList = getDataSource().getEntityList();
        for (ViewWrapper<EppWorkGraphWeekPart> row : entityList)
        {
            int j = 0;
            for (Integer col : getBorders().borders)
            {
                row.setViewProperty("t" + j, col);
                borders.put(new JSONObject()
                        .put("row", i)
                        .put("col", col + 2) // +2 т.к. у нас первые колонки - номер дня и курс
                        .put(j++ % 2 == 0 ? "left" : "right", borderStyle));
            }
            i++;
        }

        return borders;
    }

    private TermsBorders getBorders()
    {
        DevelopGrid developGrid = getHolder().getValue().getDevelopGrid();
        final Integer[] gridDetail = IDevelopGridDAO.instance.get().getDevelopGridDetail(developGrid).get(getCourse());
        int[] emptyTerms = EppEduPlanVersionScheduleUtils.getPoints4EmptyGridRow(developGrid, getCourse());
        return new TermsBorders(gridDetail, emptyTerms);
    }


    JSONObject getContextMenu() throws ParseException
    {
        JSONObject contextMenu = new JSONObject();
        JSONObject items = new JSONObject("{'undo': { name: 'Отменить действие'},'redo': { name: 'Повторить'}, " +
                "'s1': '---------'}");
        items.put("split", new JSONLiteral("{name: 'Разбить неделю'   , disabled: nonWeek }"));
        items.put("merge", new JSONLiteral("{name: 'Объединить неделю', disabled: nonWeek }"));
        items.put("s2", "---------");
        items.put("first", new JSONLiteral("{name: 'Первая неделя семестра'   , disabled: nonWeek }"));
        items.put("last",  new JSONLiteral("{name: 'Последняя неделя семестра', disabled: nonWeek }"));


        contextMenu.put("items", items);

        contextMenu.put("callback", new JSONLiteral("menuClick"));
        return contextMenu;
    }

    public HeadColumn getHeadColumn() {
        HeadColumn header = new HeadColumn("header", "График учебного процесса");
        IMergeRowIdResolver dayResolver = entity -> String.valueOf(((ViewWrapper<EppWeekPart>) entity).getViewProperty(null == ((ViewWrapper<EppWeekPart>) entity).getViewProperty(DIFF) ? NUMBER : DAY));

        SimpleColumn weekNum = new SimpleColumn("День недели", WN);
        weekNum.setWidth(24)
                .setAlign("center").
                setVerticalHeader(true).
                setDisabled(true)
                .setMergeRowIdResolver(dayResolver);
        ;
        if (!isReadOnlyState())
        { // TODO: исправить логику отображения заголовков
            weekNum.setHeaderAlign("left");
        }
        HeadColumn weekNumHead = new HeadColumn("", "");
        weekNumHead.setWidth(24);
        weekNumHead.addColumn(weekNum);
        header.addColumn(weekNumHead);

        HeadColumn numberBlank = new HeadColumn("numberBlank", "");
        AbstractColumn number = new PublisherLinkColumn("№", NUMBER)
                .setRequired(true)
                .setWidth(24)
                .setAlign("right")
                .setMergeRowIdResolver(entity -> String.valueOf(((ViewWrapper<EppWeekPart>) entity).getViewProperty(NUMBER)));
        numberBlank.addColumn(number);
        header.addColumn(numberBlank);

        HeadColumn monthHead = null;
        String lastHeadName = null;

        for (final EppYearEducationWeek week : _weekData)
        {
            final String headName = "month."+week.getDate().getMonth();
            if ((null == monthHead) || (!lastHeadName.equals(headName))) {
                if (monthHead != null) {
                    header.addColumn(monthHead);
                }
                if (week.getDate().getMonth() <=0) {
                    continue;
                }
                monthHead = new HeadColumn(headName, RussianDateFormatUtils.getMonthName(week.getDate(), true));
                lastHeadName = headName;
            }
            HeadColumn weekColumn = new HeadColumn("week."+week.getNumber(), week.getTitle());
            weekColumn.setWidth(20);
            weekColumn.setVerticalHeader(true);

            final String weekKey = getWeekCode(week);
            SimpleColumn column = new SimpleColumn(String.valueOf(week.getNumber()), weekKey);
            column.setWidth(20).setAlign("center");
            column.setMergeRowIdResolver(entity -> String.valueOf(((ViewWrapper<EppWeekPart>) entity).getViewProperty(NUMBER)) + "-" + String.valueOf(((ViewWrapper<EppWeekPart>) entity).getViewProperty(weekKey)));
            weekColumn.addColumn(column);
            monthHead.addColumn(weekColumn);
        }

        if (monthHead != null) {
            header.addColumn(monthHead);
        }

        SimpleColumn version = new SimpleColumn("",  VERSION);
        version.setWidth(1000)
                .setDisabled(true)
                .setMergeRowIdResolver(entity -> String.valueOf(((ViewWrapper<EppWeekPart>) entity).getViewProperty(VERSION)) );


        HeadColumn versionHead = new HeadColumn("", "Версия УП");
        versionHead.setWidth(1000)
                .setAlign("center");
        versionHead.addColumn(version);
        HeadColumn versionSupHead = new HeadColumn("", "");
        versionSupHead.setWidth(1000);
        versionSupHead.addColumn(versionHead);
        header.addColumn(versionSupHead);

        String headMenu = getHeadMenu(isReadOnlyState(), !_editMode);
        if (StringUtils.isNotEmpty(headMenu))
        {
            return new HeadColumn("", headMenu).addColumn(header);
        }
        return header;
    }

    private String getWeekCode(EppYearEducationWeek week) {
        return "w" + week.getNumber();
    }

    private static String getWeekCode(EppWorkGraphRowWeek week)
    {
        return "w" + week.getWeek();
    }

    public Map<TripletKey<Long, Long, String>, EppWorkGraphWeekPart> getDataMap()
    {
        if (_dataMap == null)
        {
            _dataMap = new HashMap<>();
        }
        return _dataMap;
    }

    private EppWorkGraphRowWeek getVersionWeek(EppWorkGraphRowWeek firstWeek, EppWeek globalWeek, PairKey<Long, String> key, EppWeekType newWeekType)
    {
        EppWorkGraphRowWeek week;
        week = getVersionWeekByNum().get(key);
        if (week == null)
        {
            week = new EppWorkGraphRowWeek();
            getVersionWeekByNum().put(key, week);
        }
        return week;
    }

    public Map<PairKey<Long, String>, EppWorkGraphRowWeek> getVersionWeekByNum()
    {
        if (_versionWeekByNum == null)
        {
            _versionWeekByNum = new HashMap<>();
        }
        return _versionWeekByNum;
    }

    private boolean prepareWeekPartForSave(String columnKey, EppWorkGraphWeekPart firstWeekPart, String newValue, boolean isNotEmpty)
    {
        EppWeek globalWeek = _weekIdByNum.get(columnKey);
        EppWorkGraphRowWeek week = firstWeekPart.getWeek();
        long weekPartNumber = firstWeekPart.getNumber();
        TripletKey<Long, Long, String> key = new TripletKey<>(week.getRow().getId(), weekPartNumber, columnKey);
        EppWorkGraphWeekPart weekPart = getDataMap().get(key);
        EppWorkGraphWeekPart changedPart = null;

        if (isNotEmpty)
        {
            EppWeekType newWeekType = getWeekTypeMap().get(newValue.toUpperCase());
            changedPart = changes.get(key);
            if (changedPart != null)
            {
                changedPart.setWeekType(newWeekType);
            } else
            {
                changedPart = new EppWorkGraphWeekPart();
                changedPart.setWeekType(newWeekType);

                if (weekPart == null)
                { // Новое значение
                    week = getVersionWeek(firstWeekPart.getWeek(), globalWeek, new PairKey<>(key.getFirst(), key.getThird()), newWeekType);
                    changedPart.setWeek(week);
                    changedPart.setNumber((int) weekPartNumber);
                } else
                {
                    changedPart.setWeek(weekPart.getWeek());
                    changedPart.setNumber(weekPart.getNumber());
                }
            }
        }
        changes.put(key, changedPart);

        return false;
    }

    public Boolean getEditMode()
    {
        return _editMode;
    }

    public void setEditMode(boolean editMode)
    {
        this._editMode = editMode;
    }

    private void saveChanges()
    {
        //List<EppEduPlanVersionWeekType> weeksToDelete = updateVersionWeeks();
        IUniBaseDao coreDao = UniDaoFacade.getCoreDao();
        for (Map.Entry<TripletKey<Long, Long, String>, EppWorkGraphWeekPart> entry : changes.entrySet())
        {
            EppWorkGraphWeekPart toChange = getDataMap().get(entry.getKey());
            EppWorkGraphWeekPart newValue = entry.getValue();
            if (toChange == null || newValue == null)
            {
                if (toChange != null)
                { // Удаляем часть недели
                    if (null != toChange.getWeekType())
                    {
                        coreDao.delete(toChange);
                    }
                } else if (newValue != null)
                { // Созаем новую часть недели
                    coreDao.save(newValue);
                }
            } else
            { // Часть недели уже сущесвует, просто меняем значение
                if (isNewEntity(toChange) || !toChange.getWeekType().getId().equals(newValue.getWeekType().getId()))
                {
                    toChange.setWeekType(newValue.getWeekType());
                    if (isNewEntity(toChange))
                    {
                        toChange.setId(null);
                    }
                    coreDao.saveOrUpdate(toChange);
                }
            }
        }
    }
}


