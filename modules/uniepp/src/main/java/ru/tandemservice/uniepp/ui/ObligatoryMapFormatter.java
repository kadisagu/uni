package ru.tandemservice.uniepp.ui;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.tandemframework.core.view.formatter.IFormatter;

/**
 * 
 * @author nkokorina
 *
 */

public class ObligatoryMapFormatter implements IFormatter<Map<String, Boolean>>
{
    private LinkedHashMap<String, String> code2shortTitle;

    public LinkedHashMap<String, String> getCode2shortTitle()
    {
        return this.code2shortTitle;
    }

    public void setCode2shortTitle(final LinkedHashMap<String, String> code2shortTitle)
    {
        this.code2shortTitle = code2shortTitle;
    }

    public ObligatoryMapFormatter(final LinkedHashMap<String, String> code2shortTitle)
    {
        this.code2shortTitle = code2shortTitle;
    }

    @Override
    public String format(final Map<String, Boolean> source)
    {
        if ((source == null) || source.isEmpty())
        {
            return "";
        }

        final int formSize = this.getCode2shortTitle().size();

        // краткие названия форм освоения обязательных и не обязательных соответственно
        final Set<String> trueElements = new LinkedHashSet<String>();
        final Set<String> falseElements = new LinkedHashSet<String>();

        for (final Entry<String, String> entry : this.getCode2shortTitle().entrySet())
        {
            if (null != source.get(entry.getKey()))
            {
                if (source.get(entry.getKey()))
                {
                    trueElements.add(entry.getValue());
                }
                else
                {
                    falseElements.add(entry.getValue());
                }
            }
        }

        if (trueElements.size() == formSize)
        {
            return "да";
        }
        if (falseElements.size() == formSize)
        {
            return "нет";
        }

        String result = "";

        if (!trueElements.isEmpty())
        {
            result = result.concat("да (" + this.formatCollection(trueElements) + ")");
        }
        if (!falseElements.isEmpty())
        {
            if (!result.isEmpty())
            {
                result = result.concat("; \n");
            }
            result = result.concat("нет (" + this.formatCollection(falseElements) + ")");
        }

        return result;
    }

    private String formatCollection(final Set<String> elements)
    {
        if ((elements == null) || elements.isEmpty())
        {
            return "";
        }
        final StringBuilder sb = new StringBuilder();
        for (final Iterator<String> i = elements.iterator(); i.hasNext(); )
        {
            final String item = i.next();
            sb.append(item + (i.hasNext() ? ", " : ""));
        }
        return sb.toString();
    }

}
