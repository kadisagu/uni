package ru.tandemservice.uniepp.catalog.bo.EppPlanStructure.ui.AddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import org.tandemframework.tapsupport.component.selection.hselect.IHSelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.HBaseSelectModel;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 28.08.2015
 */

@Input({
	@Bind(key = "catalogItemId", binding = "holder.id"),
	@Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EppPlanStructureAddEditUI extends BaseCatalogItemAddEditUI<EppPlanStructure>
{
    @Override
    public String getAdditionalPropertiesPageName()
    {
        return this.getClass().getPackage().getName() + ".AdditionalProps";
    }

    public boolean isParentEditDisabled() {
        return !isAddForm() && !CatalogManager.instance().modifyDao().isCatalogItemPropertyEditable(getCatalogItem().getId(), EppPlanStructure.L_PARENT);
    }

    public IHSelectModel getParentModel()
	{
		return new HBaseSelectModel() {
			@Override
			public ListResult findValues(String filter)
			{
				List<EppPlanStructure> planStructs = IUniBaseDao.instance.get().getList(new DQLSelectBuilder().fromEntity(EppPlanStructure.class, "planStruct"));
				List<EppPlanStructure> roots = planStructs.stream().filter(st -> st.getParent() == null).collect(Collectors.toList());
				List<HSelectOption> options = planStructs.stream()
					.map(st -> new HSelectOption(st, nesting(st), roots.contains(st.getParent())))
					.filter(opt -> opt.getLevel() < 2)
					.sorted(EppPlanStructureAddEditUI::compare)
					.collect(Collectors.toList());
				return new ListResult<>(options);
			}
		};
	}
	private static int nesting(EppPlanStructure st)
	{
		return st.getParent() == null ? 0 : 1 + nesting(st.getParent());
	}
	private static int compare(HSelectOption selOpt1, HSelectOption selOpt2)
	{
		int lvlDiff = selOpt1.getLevel() - selOpt2.getLevel();
		int stCmp = compare((EppPlanStructure) selOpt1.getObject(), (EppPlanStructure) selOpt2.getObject(), lvlDiff);
		if (stCmp != 0)
			return stCmp;
		return Integer.compare(selOpt1.getLevel(), selOpt2.getLevel());
	}
	private static int compare(EppPlanStructure st1, EppPlanStructure st2, int lvlDiff)
	{
		if (lvlDiff > 0)
			return compare(st1.getParent(), st2, lvlDiff - 1);
		if (lvlDiff < 0)
			return compare(st1, st2.getParent(), lvlDiff + 1);
		if (st1.getParent() == st2.getParent())
			return Integer.compare(st1.getPriority(), st2.getPriority());
		return compare(st1.getParent(), st2.getParent(), 0);
	}
	@Override
	public void onClickApply()
	{
		EppPlanStructure item = getCatalogItem();
		if (isAddForm())
		{
			List<EppPlanStructure> neighbours = IUniBaseDao.instance.get().<EppPlanStructure>getList(
				new DQLSelectBuilder()
					.fromEntity(EppPlanStructure.class, "planStruct")
					.where(eq(property("planStruct", EppPlanStructure.parent()), value(item.getParent())))
					.order(property("planStruct", EppPlanStructure.priority()), OrderDirection.desc));
			int lastPriorityInGroup = neighbours.get(0).getPriority();
			item.setPriority(lastPriorityInGroup + 1);
			setNextCatalogCode(item, neighbours);
		}
		// Вызов super.onClickApply установил бы значение code самостоятельно (в числовое значение)
		// Мы же хотим получить значение вроде "3g.cycle.40" (код родительского элемента 3g.cycle плюс уникальный среди соседей числовой суффикс)
		// Поэтому сами делаем все, что есть в super.onClickApply
		CatalogManager.instance().modifyDao().createOrUpdate(item, EppPlanStructure.class);
		deactivate();
	}
	private void setNextCatalogCode(EppPlanStructure item, List<EppPlanStructure> neighbours)
	{
		int maxCode = neighbours.stream()
			.map(EppPlanStructure::getCode)
			.map(str -> str.substring(str.lastIndexOf('.') + 1))
			.map(Integer::valueOf)
			.sorted(Comparator.reverseOrder())
			.collect(Collectors.toList()).get(0);
		String parentCode = item.getParent().getCode();
		String nextCode = String.format("%s.%02d", parentCode, maxCode + 1);
		item.setCode(nextCode);
	}
}