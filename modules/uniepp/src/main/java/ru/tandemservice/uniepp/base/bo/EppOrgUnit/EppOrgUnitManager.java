/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppOrgUnit;

import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author rsizonenko
 * @since 30.03.2016
 */
@Configuration
public class EppOrgUnitManager extends BusinessObjectManager {

    public static EppOrgUnitManager instance() {
        return instance(EppOrgUnitManager.class);
    }

    public EntityComboDataSourceHandler eppOrgUnitReorganizationDSHandler(String name) {
        return new EntityComboDataSourceHandler(name, OrgUnit.class)
                .customize((alias, dql, context, filter) -> dql.where(exists(EppRegistryElement.class, EppRegistryElement.owner().s(), property(alias))))
                .titleProperty(OrgUnit.fullTitle().s())
                .filter(OrgUnit.title())
                .filter(OrgUnit.orgUnitType().title())
                .order(OrgUnit.archival())
                .order(OrgUnit.title())
                .order(OrgUnit.orgUnitType().title())
                .order(OrgUnit.id());
    }

    public EntityComboDataSourceHandler eppOrgUnitDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, OrgUnit.class)
                .customize((alias, dql, context, filter) -> dql.where(exists(EppTutorOrgUnit.class, EppTutorOrgUnit.orgUnit().s(), property(alias))))
                .titleProperty(OrgUnit.fullTitle().s())
                .filter(OrgUnit.title())
                .filter(OrgUnit.orgUnitType().title())
                .order(OrgUnit.archival())
                .order(OrgUnit.title())
                .order(OrgUnit.orgUnitType().title())
                .order(OrgUnit.id());
    }

    public EntityComboDataSourceHandler eppOrgUnitDSHandlerForAddEditForm(String ownerId)
    {
        return eppOrgUnitDSHandler(ownerId).where(OrgUnit.archival(), Boolean.FALSE);
    }
}
