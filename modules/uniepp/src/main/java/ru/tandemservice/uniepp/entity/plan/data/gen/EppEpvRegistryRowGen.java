package ru.tandemservice.uniepp.entity.plan.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка УП (элемент реестра)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvRegistryRowGen extends EppEpvTermDistributedRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow";
    public static final String ENTITY_NAME = "eppEpvRegistryRow";
    public static final int VERSION_HASH = -203346707;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_ELEMENT_TYPE = "registryElementType";
    public static final String L_REGISTRY_ELEMENT_OWNER = "registryElementOwner";
    public static final String L_REGISTRY_ELEMENT = "registryElement";
    public static final String P_TITLE_WITH_INDEX = "titleWithIndex";

    private EppRegistryStructure _registryElementType;     // Тип элемента
    private OrgUnit _registryElementOwner;     // Читающее подразделение
    private EppRegistryElement _registryElement;     // Элемент реестра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Требуемый тип элемента реестра
     *
     * @return Тип элемента. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryStructure getRegistryElementType()
    {
        return _registryElementType;
    }

    /**
     * @param registryElementType Тип элемента. Свойство не может быть null.
     */
    public void setRegistryElementType(EppRegistryStructure registryElementType)
    {
        dirty(_registryElementType, registryElementType);
        _registryElementType = registryElementType;
    }

    /**
     * Указывает на каком подразделении будут обрабатывать строку
     *
     * @return Читающее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getRegistryElementOwner()
    {
        return _registryElementOwner;
    }

    /**
     * @param registryElementOwner Читающее подразделение. Свойство не может быть null.
     */
    public void setRegistryElementOwner(OrgUnit registryElementOwner)
    {
        dirty(_registryElementOwner, registryElementOwner);
        _registryElementOwner = registryElementOwner;
    }

    /**
     * Элемент реестра (указывается на кафедрах): показывает как какой именно элемент реестра будет реализовывать строку
     *
     * @return Элемент реестра.
     */
    public EppRegistryElement getRegistryElement()
    {
        return _registryElement;
    }

    /**
     * @param registryElement Элемент реестра.
     */
    public void setRegistryElement(EppRegistryElement registryElement)
    {
        dirty(_registryElement, registryElement);
        _registryElement = registryElement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEpvRegistryRowGen)
        {
            setRegistryElementType(((EppEpvRegistryRow)another).getRegistryElementType());
            setRegistryElementOwner(((EppEpvRegistryRow)another).getRegistryElementOwner());
            setRegistryElement(((EppEpvRegistryRow)another).getRegistryElement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvRegistryRowGen> extends EppEpvTermDistributedRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvRegistryRow.class;
        }

        public T newInstance()
        {
            return (T) new EppEpvRegistryRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "registryElementType":
                    return obj.getRegistryElementType();
                case "registryElementOwner":
                    return obj.getRegistryElementOwner();
                case "registryElement":
                    return obj.getRegistryElement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "registryElementType":
                    obj.setRegistryElementType((EppRegistryStructure) value);
                    return;
                case "registryElementOwner":
                    obj.setRegistryElementOwner((OrgUnit) value);
                    return;
                case "registryElement":
                    obj.setRegistryElement((EppRegistryElement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "registryElementType":
                        return true;
                case "registryElementOwner":
                        return true;
                case "registryElement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "registryElementType":
                    return true;
                case "registryElementOwner":
                    return true;
                case "registryElement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "registryElementType":
                    return EppRegistryStructure.class;
                case "registryElementOwner":
                    return OrgUnit.class;
                case "registryElement":
                    return EppRegistryElement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvRegistryRow> _dslPath = new Path<EppEpvRegistryRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvRegistryRow");
    }
            

    /**
     * Требуемый тип элемента реестра
     *
     * @return Тип элемента. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow#getRegistryElementType()
     */
    public static EppRegistryStructure.Path<EppRegistryStructure> registryElementType()
    {
        return _dslPath.registryElementType();
    }

    /**
     * Указывает на каком подразделении будут обрабатывать строку
     *
     * @return Читающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow#getRegistryElementOwner()
     */
    public static OrgUnit.Path<OrgUnit> registryElementOwner()
    {
        return _dslPath.registryElementOwner();
    }

    /**
     * Элемент реестра (указывается на кафедрах): показывает как какой именно элемент реестра будет реализовывать строку
     *
     * @return Элемент реестра.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow#getRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> registryElement()
    {
        return _dslPath.registryElement();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow#getTitleWithIndex()
     */
    public static SupportedPropertyPath<String> titleWithIndex()
    {
        return _dslPath.titleWithIndex();
    }

    public static class Path<E extends EppEpvRegistryRow> extends EppEpvTermDistributedRow.Path<E>
    {
        private EppRegistryStructure.Path<EppRegistryStructure> _registryElementType;
        private OrgUnit.Path<OrgUnit> _registryElementOwner;
        private EppRegistryElement.Path<EppRegistryElement> _registryElement;
        private SupportedPropertyPath<String> _titleWithIndex;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Требуемый тип элемента реестра
     *
     * @return Тип элемента. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow#getRegistryElementType()
     */
        public EppRegistryStructure.Path<EppRegistryStructure> registryElementType()
        {
            if(_registryElementType == null )
                _registryElementType = new EppRegistryStructure.Path<EppRegistryStructure>(L_REGISTRY_ELEMENT_TYPE, this);
            return _registryElementType;
        }

    /**
     * Указывает на каком подразделении будут обрабатывать строку
     *
     * @return Читающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow#getRegistryElementOwner()
     */
        public OrgUnit.Path<OrgUnit> registryElementOwner()
        {
            if(_registryElementOwner == null )
                _registryElementOwner = new OrgUnit.Path<OrgUnit>(L_REGISTRY_ELEMENT_OWNER, this);
            return _registryElementOwner;
        }

    /**
     * Элемент реестра (указывается на кафедрах): показывает как какой именно элемент реестра будет реализовывать строку
     *
     * @return Элемент реестра.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow#getRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> registryElement()
        {
            if(_registryElement == null )
                _registryElement = new EppRegistryElement.Path<EppRegistryElement>(L_REGISTRY_ELEMENT, this);
            return _registryElement;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow#getTitleWithIndex()
     */
        public SupportedPropertyPath<String> titleWithIndex()
        {
            if(_titleWithIndex == null )
                _titleWithIndex = new SupportedPropertyPath<String>(EppEpvRegistryRowGen.P_TITLE_WITH_INDEX, this);
            return _titleWithIndex;
        }

        public Class getEntityClass()
        {
            return EppEpvRegistryRow.class;
        }

        public String getEntityName()
        {
            return "eppEpvRegistryRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleWithIndex();
}
