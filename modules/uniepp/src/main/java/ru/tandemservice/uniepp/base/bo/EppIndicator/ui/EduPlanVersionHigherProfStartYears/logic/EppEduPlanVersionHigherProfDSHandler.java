/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.EduPlanVersionHigherProfStartYears.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/28/14
 */
public class EppEduPlanVersionHigherProfDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String  VIEW_PROP_VERSION = "version";

    public EppEduPlanVersionHigherProfDSHandler(String ownerID)
    {
        super(ownerID);
    }

    @Override
    protected final DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersion.class, "v")
                .joinEntity("v", DQLJoinType.inner, EppEduPlanHigherProf.class, "p", eq(property("v", EppEduPlanVersion.eduPlan().id()), property("p", EppEduPlanHigherProf.id())));

        DQLOrderDescriptionRegistry orderDescription = new DQLOrderDescriptionRegistry(EppEduPlanHigherProf.class, "p");
//
        filter(builder, "p", context);
//
        String key = input.getEntityOrder().getKeyString();

        EntityOrder entityOrder = new EntityOrder(key.startsWith("eduPlan.") ? key.replace("eduPlan.", "") : key, input.getEntityOrder().getDirection(), input.getEntityOrder().getColumnName());

        orderDescription.applyOrderWithLeftJoins(builder, entityOrder);


        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build().transform((Object[] row) -> {
            DataWrapper wrapper = null;
            if (row != null) {
                wrapper = new DataWrapper(row[0]);
                wrapper.setProperty("eduPlan", row[1]);
            }
            return wrapper;
        });

        return wrapOutput(output);
    }

    protected void filter(DQLSelectBuilder builder, String alias, ExecutionContext context)
    {
        IDataSettings settings = context.get("settings");
        builder.where(eq(property(alias, EppEduPlan.programKind().programHigherProf()), value(true)));
        FilterUtils.applySimpleLikeFilter(builder, alias, EppEduPlanHigherProf.number(), settings.<String>get("number"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanHigherProf.programSubject().subjectIndex(), settings.get("subjectIndex"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanHigherProf.programSubject(), settings.get("programSubject"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanHigherProf.programForm(), settings.get("programForm"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanHigherProf.developCondition(), settings.get("developCondition"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanHigherProf.programTrait(), settings.get("programTrait"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanHigherProf.developGrid(), settings.get("developGrid"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanHigherProf.state(), settings.get("state"));
        FilterUtils.applyLikeFilter(builder, (String) settings.get("programSubjectText"), EppEduPlanHigherProf.programSubject().subjectCode().fromAlias(alias), EppEduPlanHigherProf.programSubject().title().fromAlias(alias));

        List<EduProgramSpecialization> programSpecializations = settings.get("programSpecializations");
        if (programSpecializations != null && !programSpecializations.isEmpty())
        {
            builder.where(exists(
                    new DQLSelectBuilder()
                            .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                            .where(eq(property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan()), property(alias)))
                            .where(in(property("b", EppEduPlanVersionSpecializationBlock.programSpecialization()), programSpecializations))
                            .buildQuery()));
        }

        OrgUnit orgUnit = context.get("orgUnit");
        applyOrgUnitFilter(builder, alias, orgUnit);
    }

    protected void applyOrgUnitFilter(DQLSelectBuilder builder, String alias, OrgUnit orgUnit)
    {
        if (orgUnit != null) {
            builder.where(or(
                eq(property(alias, EppEduPlan.owner()), value(orgUnit)),
                exists(new DQLSelectBuilder()
                    .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                    .where(eq(property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan()), property(alias)))
                    .where(eq(property("b", EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit()), value(orgUnit)))
                    .buildQuery()),
                exists(new DQLSelectBuilder()
                    .fromEntity(EducationOrgUnit.class, "e")
                    .where(or(
                        eq(property("e", EducationOrgUnit.formativeOrgUnit()), value(orgUnit)),
                        eq(property("e", EducationOrgUnit.territorialOrgUnit()), value(orgUnit))
                    ))
                    .where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject()), property(alias, EppEduPlanHigherProf.programSubject())))
                    .where(eq(property("e", EducationOrgUnit.developForm().programForm()), property(alias, EppEduPlan.programForm())))
                    .where(eq(property("e", EducationOrgUnit.developCondition()), property(alias, EppEduPlan.developCondition())))
                    .where(eqNullSafe(property("e", EducationOrgUnit.developTech().programTrait()), property(alias, EppEduPlan.programTrait())))
                    .where(eq(property("e", EducationOrgUnit.developPeriod()), property(alias, EppEduPlan.developGrid().developPeriod())))
                    .buildQuery())
            ));
        }
    }

    protected DSOutput wrapOutput(DSOutput output)
    {
        final Map<Long, List<String>> blockMap = Maps.newHashMap();
        final Map<Long, Set<String>> wPMap = Maps.newHashMap();
        BatchUtils.execute(output.getRecordIds(), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            DQLSelectBuilder progSpecBuilder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, "block")
                    .fetchPath(DQLJoinType.inner, EppEduPlanVersionSpecializationBlock.programSpecialization().fromAlias("block"), true)
                    .where(in(property("block", EppEduPlanVersionSpecializationBlock.eduPlanVersion().id()), elements));
            for(EppEduPlanVersionSpecializationBlock block : createStatement(progSpecBuilder).<EppEduPlanVersionSpecializationBlock>list())
            {
                SafeMap.safeGet(blockMap, block.getEduPlanVersion().getId(), ArrayList.class).add(block.getProgramSpecialization().getDisplayableTitle());
            }

            DQLSelectBuilder yearsBuilder = new DQLSelectBuilder().fromEntity(EppStudent2WorkPlan.class, "swp")
                    .joinEntity("swp", DQLJoinType.left, EppWorkPlan.class, "wp", eq(property("wp", EppWorkPlan.id()), property("swp", EppStudent2WorkPlan.workPlan().id())))
                    .joinEntity("swp", DQLJoinType.left, EppWorkPlanVersion.class, "wpv", eq(property("wpv", EppWorkPlanVersion.id()), property("swp", EppStudent2WorkPlan.workPlan().id())))
                    .column(property("swp", EppStudent2WorkPlan.studentEduPlanVersion().eduPlanVersion().id()))
                    .column(property("wp"))
                    .column(property("wpv"))
                    .where(isNull(property("swp", EppStudent2WorkPlan.removalDate())))
                    .where(in(property("swp", EppStudent2WorkPlan.studentEduPlanVersion().eduPlanVersion().id()), elements));

            for(Object[] obj : createStatement(yearsBuilder).<Object[]>list())
            {
                Long eduPlanVersionId = (Long) obj[0];
                EppWorkPlan workPlan = (EppWorkPlan) obj[1];
                EppWorkPlanVersion workPlanVersion = (EppWorkPlanVersion) obj[2];
                if(workPlan == null)
                {
                    workPlan = workPlanVersion.getParent();
                }

                if(workPlan != null)
                {
                    int year = workPlan.getYear().getEducationYear().getIntValue();
                    int course = workPlan.getGridTerm().getCourseNumber();

                    int resultYear = year - course + 1;

                    SafeMap.safeGet(wPMap, eduPlanVersionId, HashSet.class).add(resultYear + "/" + (resultYear + 1));
                }
            }
        });

        for(DataWrapper wrapper : output.<DataWrapper>getRecordList())
        {
            if(blockMap.containsKey(wrapper.getId()))
            {
                List<String> progSpecs = blockMap.get(wrapper.getId());
                Collections.sort(progSpecs, CommonCollator.RUSSIAN_COLLATOR);
                wrapper.setProperty("programSpec", progSpecs);
            }

            if(wPMap.containsKey(wrapper.getId()))
            {
                List<String> years = Lists.newArrayList(wPMap.get(wrapper.getId()));
                Collections.sort(years);
                wrapper.setProperty("years", years);
            }
        }

        return output;
    }
}
