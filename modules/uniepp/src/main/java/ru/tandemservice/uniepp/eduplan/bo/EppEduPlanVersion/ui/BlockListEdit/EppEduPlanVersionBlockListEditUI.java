/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockListEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/10/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class EppEduPlanVersionBlockListEditUI extends UIPresenter
{
    private EntityHolder<EppEduPlanVersion> holder = new EntityHolder<>();

    private List<EduProgramSpecialization> specList;
    private EduProgramSpecialization currentSpec;

    private Set<EduProgramSpecialization> included;
    private Map<EduProgramSpecialization, EduOwnerOrgUnit> orgUnitMap;

    private ISelectModel orgUnitModel;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();

        if (!(getHolder().getValue().getEduPlan() instanceof EppEduPlanHigherProf)) {
            throw new ApplicationException("Создание нескольких блоков разрешено только для версий учебных планов высшего образования.");
        }

        EppEduPlanHigherProf eduPlanHigherProf = (EppEduPlanHigherProf) getHolder().getValue().getEduPlan();

        // При добавлении направленности в УПв (в блок) список вып. подразделений ограничивать перечнем таковых из НПв, если они имеются
        final List<EduOwnerOrgUnit> eduOwnerOrgUnits = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(EduOwnerOrgUnit.class, "o")
                        .where(exists(EducationLevelsHighSchool.class,
                                      EducationLevelsHighSchool.L_ORG_UNIT, property("o", EduOwnerOrgUnit.L_ORG_UNIT),
                                      EducationLevelsHighSchool.educationLevel().eduProgramSubject().s(), eduPlanHigherProf.getProgramSubject()))
        );
        if (eduOwnerOrgUnits.isEmpty())
            eduOwnerOrgUnits.addAll(DataAccessServices.dao().getList(EduOwnerOrgUnit.class));

        setOrgUnitModel(new LazySimpleSelectModel<>(eduOwnerOrgUnits));


        setSpecList(DataAccessServices.dao().getList(EduProgramSpecialization.class, EduProgramSpecialization.programSubject(), eduPlanHigherProf.getProgramSubject(), EduProgramSpecialization.title().s()));
        Collections.sort(getSpecList());

        if (getIncluded() == null) {
            setIncluded(new HashSet<>());
            setOrgUnitMap(new HashMap<>());
            for (EppEduPlanVersionSpecializationBlock block : DataAccessServices.dao().getList(EppEduPlanVersionSpecializationBlock.class, EppEduPlanVersionSpecializationBlock.eduPlanVersion(), getHolder().getValue(), EppStateEduStandardBlock.programSpecialization().title().s())) {
                if (block.getProgramSpecialization() != null) {
                    getIncluded().add(block.getProgramSpecialization());
                    getOrgUnitMap().put(block.getProgramSpecialization(), block.getOwnerOrgUnit());
                }
            }
        }
    }

    public void onClickApply() {
        IEppEduPlanDAO.instance.get().doUpdateBlockList(getHolder().getId(), getOrgUnitMap());
        deactivate();
    }

    public String getCheckboxFieldId()
    {
        return "checkbox." + getCurrentSpec().getId();
    }

    public String getOrgUnitFieldId()
    {
        return "orgUnit." + getCurrentSpec().getId();
    }

    public EduOwnerOrgUnit getOrgUnit()
    {
        EduOwnerOrgUnit selectedItem = getOrgUnitMap().get(getCurrentSpec());
        if (selectedItem == null && isInclude())
        {
            // в случае единственного подразделения подставлять его автоматически
            List items = getOrgUnitModel().findValues("").getObjects();
            if (items.size() == 1)
                selectedItem = (EduOwnerOrgUnit) items.get(0);
        }
        return selectedItem;
    }

    public void setOrgUnit(EduOwnerOrgUnit orgUnit)
    {
        getOrgUnitMap().put(getCurrentSpec(), orgUnit);
    }

    public boolean isInclude() {
        return getIncluded().contains(getCurrentSpec());
    }

    public void setInclude(boolean include) {
        if (include) {
            getIncluded().add(getCurrentSpec());
        }
        else
        {
            getIncluded().remove(getCurrentSpec());
            getOrgUnitMap().remove(getCurrentSpec());
        }
    }

    // getters and setters

    public EntityHolder<EppEduPlanVersion> getHolder()
    {
        return holder;
    }

    public EduProgramSpecialization getCurrentSpec()
    {
        return currentSpec;
    }

    public void setCurrentSpec(EduProgramSpecialization currentSpec)
    {
        this.currentSpec = currentSpec;
    }

    public Set<EduProgramSpecialization> getIncluded()
    {
        return included;
    }

    public void setIncluded(Set<EduProgramSpecialization> included)
    {
        this.included = included;
    }

    public Map<EduProgramSpecialization, EduOwnerOrgUnit> getOrgUnitMap()
    {
        return orgUnitMap;
    }

    public void setOrgUnitMap(Map<EduProgramSpecialization, EduOwnerOrgUnit> orgUnitMap)
    {
        this.orgUnitMap = orgUnitMap;
    }

    public ISelectModel getOrgUnitModel()
    {
        return orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel)
    {
        this.orgUnitModel = orgUnitModel;
    }

    public List<EduProgramSpecialization> getSpecList()
    {
        return specList;
    }

    public void setSpecList(List<EduProgramSpecialization> specList)
    {
        this.specList = specList;
    }
}