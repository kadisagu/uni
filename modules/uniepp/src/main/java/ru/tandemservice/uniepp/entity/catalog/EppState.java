package ru.tandemservice.uniepp.entity.catalog;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.apache.log4j.Logger;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.uniepp.entity.catalog.gen.EppStateGen;

import java.util.List;
import java.util.Set;

/**
 * Состояние
 */
public class EppState extends EppStateGen
{
    public static final String STATE_FORMATIVE = "1";  // Формируется
    public static final String STATE_ACCEPTABLE = "2"; // На согласовании
    public static final String STATE_REJECTED = "3";   // Отклонено
    public static final String STATE_ACCEPTED = "4";   // Согласовано
    public static final String STATE_ARCHIVED = "5";   // Архив

    public boolean isFormative() { return EppState.STATE_FORMATIVE.equals(this.getCode()); }
    public boolean isAcceptable() { return EppState.STATE_ACCEPTABLE.equals(this.getCode()); }
    public boolean isRejected() { return EppState.STATE_REJECTED.equals(this.getCode()); }
    public boolean isAccepted() { return EppState.STATE_ACCEPTED.equals(this.getCode()); }
    public boolean isArchived() { return EppState.STATE_ARCHIVED.equals(this.getCode()); }

    public static final Set<String> READ_ONLY_CODES = ImmutableSet.of(
            EppState.STATE_ACCEPTABLE,
            EppState.STATE_ACCEPTED,
            EppState.STATE_REJECTED,
            EppState.STATE_ARCHIVED
    );

    public static final Set<String> INACTIVE_CODES = ImmutableSet.of(EppState.STATE_REJECTED, EppState.STATE_ARCHIVED);

    public static final List<String> UNACTUAL_STATES = ImmutableList.copyOf(EppState.INACTIVE_CODES);

    /**
     * @return true, если в состоянии редактировать объект запрещено
     */
    public boolean isReadOnlyState() {
        return EppState.READ_ONLY_CODES.contains(this.getCode());
    }

    public static class EppStateException extends ApplicationException {
        private static final Logger logger = Logger.getLogger(EppStateException.class);
        private static final long serialVersionUID = 1L;
        private EppStateException(final String message) { super(message); }
    }

    public void check_editable(final ITitled object) throws EppStateException {
        if (this.isReadOnlyState()) {
            final IEntityMeta meta = EntityRuntime.getMeta(ClassUtils.getUserClass(object));
            final String objectTitle = (null == meta ? object.getClass().getSimpleName() : meta.getTitle()) + " «"+object.getTitle()+"»";
            final String message = objectTitle+" находится состоянии «"+this.getTitle()+"», изменение объектов в этом состоянии не допускается.";
            EppState.throw_check_exception(message);
        }
    }

    public static void throw_check_exception(final String message) throws EppStateException {
        if (EppStateException.logger.isTraceEnabled()) {
            // если надо делать trace - логируем и кидаем исключение
            try {
                throw new EppStateException(message);
            } catch (final EppStateException e) {
                EppStateException.logger.trace(e.getMessage(), e);
                throw e;
            }
        } else {
            // если не надо - то просто кидаем исключение
            throw new EppStateException(message);
        }
    }
}