package ru.tandemservice.uniepp.component.registry.RegElementWorkPlanList;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{


    @Override
    public void prepare(final Model model)
    {

    }

    @Override
    public void prepareDataSource(final Model model)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EppWorkPlanRegistryElementRow.class, "e").column(property("e"))
        .where(eq(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias("e")), value(model.getId())))
        .order(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias("e")));

        UniBaseUtils.createPage(model.getDataSource(), dql, this.getSession());
    }

}
