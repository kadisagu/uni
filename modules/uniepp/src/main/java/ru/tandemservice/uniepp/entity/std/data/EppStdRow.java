package ru.tandemservice.uniepp.entity.std.data;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.std.data.gen.EppStdRowGen;

/**
 * Запись ГОС (базовая)
 */
public abstract class EppStdRow extends EppStdRowGen implements IEppStdRow
{
    @Override
    public String getUuid() {
        return (StringUtils.isNotBlank(super.getUuid())) ? super.getUuid() : String.valueOf(this.getId());
    }

    @Override
    public abstract String getTitle();

    @Override
    @EntityDSLSupport
    public String getDisplayableTitle() {
        return this.getTitle();
    }

    @Override
    public abstract EppStdRow getHierarhyParent();

    public abstract void setHierarhyParent(EppStdRow parent);

    public abstract int getComparatorClassIndex();

    public abstract String getComparatorString();

    @Override
    public abstract String getSelfIndexPart();

    @Override
    public String toString() {
        return (this.getId() + "@" + this.getClass().getSimpleName() + ": " + this.getTitle());
    }

    @Override
    public String getQualificationCode() {
        return null;
    }

    @Override
    public EppGeneration getGeneration() {
        return this.getOwner().getStateEduStandard().getGeneration();
    }

    protected String formatLoad(final Long load) {
        if (null == load) { return "0"; }
        return UniEppUtils.formatLoad(UniEppUtils.wrap(load.longValue()), false);
    }

    @Override
    @EntityDSLSupport
    public String getTotalSize()
    {
        if ((null != this.getTotalSizeMin()) && (null != this.getTotalSizeMax())) {
            return this.formatLoad(this.getTotalSizeMin()) + " - " + this.formatLoad(this.getTotalSizeMax());
        }

        if (null != this.getTotalSizeMin()) {
            return "⩾ " + this.formatLoad(this.getTotalSizeMin());
        }
        if (null != this.getTotalSizeMax()) {
            return "⩽ " + this.formatLoad(this.getTotalSizeMax());
        }
        return "";
    }

    @Override
    @EntityDSLSupport
    public String getTotalLabor()
    {
        if ((null != this.getTotalLaborMin()) && (null != this.getTotalLaborMax())) {
            return this.formatLoad(this.getTotalLaborMin()) + " - " + this.formatLoad(this.getTotalLaborMax());
        }

        if (null != this.getTotalLaborMin()) {
            return "⩾ " + this.formatLoad(this.getTotalLaborMin());
        }
        if (null != this.getTotalLaborMax()) {
            return "⩽ " + this.formatLoad(this.getTotalLaborMax());
        }
        return "";
    }

    @Override
    public Boolean getUsedInLoad()
    {
        if ((null != this.getHierarhyParent()) && Boolean.FALSE.equals(this.getHierarhyParent().getUsedInLoad())) {
            return Boolean.FALSE;
        }
        return !Boolean.TRUE.equals(this.isExcludedFromLoad());
    }

    public void setUsedInLoad(final Boolean used)
    {
        this.setExcludedFromLoad(!Boolean.TRUE.equals(used));
    }

    @Override
    public Boolean getUsedInActions()
    {
        if ((null != this.getHierarhyParent()) && Boolean.FALSE.equals(this.getHierarhyParent().getUsedInActions())) {
            return Boolean.FALSE;
        }
        return !Boolean.TRUE.equals(this.isExcludedFromActions());
    }

    public void setUsedInActions(final Boolean used)
    {
        this.setExcludedFromActions(!Boolean.TRUE.equals(used));
    }

    public Map<String, Double> getLoadMap()
    {
        final Map<String, Double> loadMap = new HashMap<String, Double>();
        loadMap.put(EppLoadType.FULL_CODE_TOTAL_HOURS, UniEppUtils.wrap(this.getTotalSizeMin()));
        loadMap.put(EppLoadType.FULL_CODE_LABOR, UniEppUtils.wrap(this.getTotalLaborMin()));
        return loadMap;
    }

}