package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionDiffTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.component.workplan.WorkPlanData.WorkPlanDataBaseModel;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;


@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id", required = true) })
public class Model extends WorkPlanDataBaseModel
{
    private final EntityHolder<EppWorkPlanBase> holder = new EntityHolder<>();
    public EntityHolder<EppWorkPlanBase> getHolder() { return this.holder; }
    public Long getId() { return this.getHolder().getId(); }

    public boolean isNotCustom() {
        return getHolder().getValue().getWorkPlan().getCustomEduPlan() == null;
    }

    @Override protected Long getWorkPlanId() { return this.getId(); }
    @Override protected boolean isEditable() { return false; }
    @Override protected String getPermissionKeyEdit() { return null; }

}
