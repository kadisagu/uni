/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.SecondaryProfAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.EppEduPlanManager;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 9/5/14
 */
@Configuration
public class EppEduPlanSecondaryProfAddEdit extends BusinessComponentManager
{
    public static final String BIND_PROGRAM_SUBJECT = "programSubject";
    public static final String BIND_PROGRAM_KIND_ID = "programKindId";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("programSubjectDS", programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
            .addDataSource(selectDS("programQualificationDS", programQualificationDSHandler()))
            .addDataSource(EducationCatalogsManager.instance().programFormDSConfig())
            .addDataSource(selectDS("eduLevelDS", eduLevelDSHandler()))
            .addDataSource(selectDS("stateEduStandardDS", stateEduStandardDSHandler()))
            .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
            .addDataSource(EducationCatalogsManager.instance().programTraitDSConfig())
            .addDataSource(EducationCatalogsManager.instance().developGridDSConfig())
            .addDataSource(EducationCatalogsManager.instance().developCombinationDSConfig())
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
            .where(EduProgramSubject.subjectIndex().programKind().id(), BIND_PROGRAM_KIND_ID)
            .where(EduProgramSubject.subjectIndex().programKind().programSecondaryProf(), Boolean.TRUE)
            .filter(EduProgramSubject.code())
            .filter(EduProgramSubject.title())
            .order(EduProgramSubject.code())
            .order(EduProgramSubject.title())
            ;
    }

    @Bean
    public IDefaultComboDataSourceHandler programQualificationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramQualification.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.joinEntity(alias, DQLJoinType.inner, EduProgramSubjectQualification.class, "sq", eq(property("sq", EduProgramSubjectQualification.programQualification()), property(alias)));
                dql.where(eq(property("sq", EduProgramSubjectQualification.programSubject()), commonValue(context.get(BIND_PROGRAM_SUBJECT))));
            }
        }
            .filter(EduProgramSubject.code())
            .filter(EduProgramQualification.title())
            .order(EduProgramQualification.title())
            ;
    }

    @Bean
    public IDefaultComboDataSourceHandler eduLevelDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduLevel.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                EduProgramSubject subject = context.get(BIND_PROGRAM_SUBJECT);
                if (null == subject) {
                    dql.where(isNull(alias + ".id"));
                } else {
                    dql.where(in(property(alias, EduLevel.code()), EppEduPlanManager.getAllowedBaseLevelCodes(subject.getSubjectIndex().getProgramKind())));
                }
            }
        }
            .filter(EduLevel.title())
            .order(EduLevel.code())
            ;
    }

    @Bean
    public IDefaultComboDataSourceHandler stateEduStandardDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppStateEduStandard.class)
            .where(EppStateEduStandard.programSubject(), BIND_PROGRAM_SUBJECT, true)
            .filter(EppStateEduStandard.number())
            .filter(EppStateEduStandard.programSubject().code())
            .filter(EppStateEduStandard.programSubject().title())
            .order(EppStateEduStandard.number())
            ;
    }

}