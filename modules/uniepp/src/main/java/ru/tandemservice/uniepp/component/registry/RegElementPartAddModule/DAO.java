package ru.tandemservice.uniepp.component.registry.RegElementPartAddModule;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DAO<T extends EppRegistryElement> extends UniBaseDao implements IDAO {

    @Override
    public void prepare(final Model model) {
        final EppRegistryElementPart part = model.getHolder().refresh(EppRegistryElementPart.class);
        part.getState().check_editable(part);

        model.getModule().setSource(new DQLFullCheckSelectModel(
                EppRegistryModule.title(),
                EppRegistryModule.parent().shortTitle(),
                EppRegistryModule.owner().shortTitle()
        ) {
            @Override protected DQLSelectBuilder query(final String alias, final String filter) {
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppRegistryModule.class, alias)
                .order(property(EppRegistryModule.title().fromAlias(alias)))
                .order(property(EppRegistryModule.number().fromAlias(alias)))
                // еще не добавлены
                .where(notIn(
                        property(alias),
                        new DQLSelectBuilder()
                        .fromEntity(EppRegistryElementPartModule.class, alias+"_x")
                        .column(property(EppRegistryElementPartModule.module().fromAlias(alias+"_x")))
                        .where(eq(property(EppRegistryElementPartModule.part().registryElement().fromAlias(alias+"_x")), value(part.getRegistryElement())))
                        .buildQuery()
                ))
                // либо общий и согласованный, либо с той же кафедры
                .where(or(
                        and(
                                eq(property(EppRegistryModule.shared().fromAlias(alias)), value(Boolean.TRUE)),
                                eq(property(EppRegistryModule.state().code().fromAlias(alias)), value(EppState.STATE_ACCEPTED))
                        ),
                        eq(property(EppRegistryModule.owner().fromAlias(alias)), value(part.getTutorOu()))
                ));


                FilterUtils.applyLikeFilter(
                    dql,
                    filter,
                    EppRegistryModule.number().fromAlias(alias),
                    EppRegistryModule.title().fromAlias(alias),
                    EppRegistryModule.shortTitle().fromAlias(alias),
                    EppRegistryModule.fullTitle().fromAlias(alias),
                    EppRegistryModule.owner().title().fromAlias(alias),
                    EppRegistryModule.owner().fullTitle().fromAlias(alias)
                );

                return dql;
            }
        });
    }

    @Override
    public void save(final Model model, final IBusinessComponent moduleComponent) {
        final Long id = ru.tandemservice.uniepp.component.registry.ModuleRegistry.AddEdit.Controller.executeSave(moduleComponent);
        if (null == id) { throw new IllegalStateException(); }

        model.getModule().setValue(this.get(EppRegistryModule.class, id));
        IEppRegistryDAO.instance.get().doAddRegistryElementPartModule(model.getHolder().getValue(), model.getModule().getValue());

    }
}
