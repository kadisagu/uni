package ru.tandemservice.uniepp.component.base;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id")
})
public abstract class SimpleOrgUnitBasedAddEditModel<T extends IEntity> extends SimpleOrgUnitBasedModel {

    private T element;
    public T getElement() { return this.element; }
    public void setElement(final T element) { this.element = element; }

    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }
}
