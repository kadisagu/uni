/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphWeeksTab;

import java.util.Set;

import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public interface IDAO extends IUpdateable<Model>
{
    // загружает из базы все данные в таблицу гуп
    void prepareGraphDataMap(Model model);

    // подготавливаем данные для редактирования строки гуп
    void prepareEditRow(Model model, Long rowId);

    // заполнение значениями dataSource для гуп
    void prepareGraphDataSource(Model model);

    // сохраняет изменения в таблице для строки
    void updateGraphRow(Long rowId, Model model);

    // объединяет строки ГУП на базе templateRow
    void updateCombineRows(Model model, Long templateRow, Set<Long> selected);

    // выделяет версию УП в отдельную группу
    void updateExcludeRow(Model model, Long excludeRowId);
}
