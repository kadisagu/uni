package ru.tandemservice.uniepp.dao.eduplan.export.highSchool;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;

import java.util.*;

/**
 * Общие методы, необходимые для импорта направлений подготовки (специальностей) ВПО
 * @author nkokorina
 *
 */

public class EppEPVExportHSUtils
{
    public static Comparator<EppEduPlanVersionWeekType> weekTypeComparator = new Comparator<EppEduPlanVersionWeekType>()
    {
        @Override
        public int compare(final EppEduPlanVersionWeekType o1, final EppEduPlanVersionWeekType o2)
        {
            final Integer num1 = o1.getWeek().getNumber();
            final Integer num2 = o2.getWeek().getNumber();

            return num1.compareTo(num2);
        }
    };

    /**
     * 
     * @param versionId
     * @param version
     * @param course
     * @return - имя файла для экспорта для ВПО
     */
    public static String buildFileTitleToHighWithId(final Long versionId, final EppEduPlanVersion version, final Set<Integer> course)
    {
        final StringBuilder sb = new StringBuilder();

        // todo DEV-6000
//        final EducationLevelsHighSchool eduLevelsHS = version.getEduPlan().getEducationLevelHighSchool();
//        final String qualificationCode = eduLevelsHS.getEducationLevel().getQualification().getCode();
//        final String formCode = version.getEduPlan().getDevelopForm().getCode();
//
//        sb.append(EppEPVExportHSUtils.buildFileTitleToHigh(version, course));
//
//        sb.append("_");
//        sb.append(Long.toHexString(versionId.longValue() >> IdGen.CODE_BITS)); // идентификатор уп(в)
//
//        sb.append(".");
//        sb.append(EppEPVExportHSUtils.getCodeFileExport(qualificationCode, formCode));// Уровень форма освоения
//
//        sb.append(".xml");

        return sb.toString();
    }

    @SuppressWarnings("deprecation")
    public static String buildFileTitleToHigh(final EppEduPlanVersion version, final Set<Integer> course)
    {
        final StringBuilder sb = new StringBuilder();

        // todo DEV-6000
//        final EducationLevelsHighSchool eduLevelsHS = version.getEduPlan().getEducationLevelHighSchool();
//
//        // здесь должно быть именно ОКСО - это ИМЦА, а она работает только с ОКСО
//        String inheritedOkso = StringUtils.trimToNull(eduLevelsHS.getEducationLevel().getInheritedOkso());
//        if (null == inheritedOkso) {
//            throw new ApplicationException("Направление подготовки (специальность) «"+eduLevelsHS.getFullTitleExtended()+"» не содержит ОКСО.");
//        }
//
//        Date confirmDate = version.getEduPlan().getConfirmDate();
//        if (null == confirmDate) {
//            confirmDate = version.getEduPlan().getParent().getConfirmDate();
//        }
//        if (null == confirmDate) {
//            throw new ApplicationException("Для учебного плана «"+version.getEduPlan().getTitleWithFCTG()+"» не указана дата утверждения.");
//        }
//
//        sb.append(inheritedOkso); // ОКСО
//        if (null != eduLevelsHS.getEducationLevel().getSpecializationNumber()) // номер специализации
//        {
//            sb.append(eduLevelsHS.getEducationLevel().getSpecializationNumber());
//        }
//        final String qualificationCode = eduLevelsHS.getEducationLevel().getQualification().getCode();
//        sb.append("_" + qualificationCode); // Код Квалификации
//        sb.append("-" + RussianDateFormatUtils.getYearString(confirmDate, true)); // Последние две цифры года
//
//        sb.append("-"); // Список курсов
//        sb.append(EppEPVExportHSUtils.getCourseListString(course));
//        sb.append("-");
//        sb.append("0000"); // Рег. номер

        return sb.toString();
    }

    /**
     * 
     * @param qualificationCode
     * @param formCode
     * @return - шифр файла для экспорта
     */
    public static String getCodeFileExport(final String qualificationCode, final String formCode)
    {
        if (DevelopFormCodes.CORESP_FORM.equals(formCode)) {
            throw new ApplicationException("Экспортировать в формат ИМЦА можно только учебные планы очной формы обучения.");
        }

        if (QualificationsCodes.SPETSIALIST.equals(qualificationCode) && !DevelopFormCodes.CORESP_FORM.equals(formCode))
        {
            return "pli";
        }
        if ((QualificationsCodes.BAKALAVR.equals(qualificationCode) || QualificationsCodes.MAGISTR.equals(qualificationCode)) && !DevelopFormCodes.CORESP_FORM.equals(formCode))
        {
            return "plm";
        }
        return "plz";
    }

    /**
     * 
     * @param course - список курсов в виде строки "1245"
     * @return
     */
    public static String getCourseListString(final Set<Integer> course)
    {
        final List<Integer> courselist = new ArrayList<Integer>();
        courselist.addAll(course);
        Collections.sort(courselist);
        if (courselist.isEmpty())
        {
            return "0";
        }


        final StringBuilder sb = new StringBuilder();
        for (final Integer num : courselist)
        {
            sb.append(num.toString());
        }
        return sb.toString();
    }

    public static String getSubTypeString(final String qualificationCode, final String formCode)
    {
        if (QualificationsCodes.SPETSIALIST.equals(qualificationCode) && !DevelopFormCodes.CORESP_FORM.equals(formCode))
        {
            return "рабочий учебный план специалистов";
        }
        if ((QualificationsCodes.BAKALAVR.equals(qualificationCode) || QualificationsCodes.MAGISTR.equals(qualificationCode)) && !DevelopFormCodes.CORESP_FORM.equals(formCode))
        {
            return "рабочий учебный план бакалавров и магистров";
        }
        return "рабочий учебный план специалистов или бакалавров и магистров (заочники)";
    }

    public static String getEducationProgramString(final String qualificationCode, final String formCode)
    {
        if (QualificationsCodes.SPETSIALIST.equals(qualificationCode) && !DevelopFormCodes.CORESP_FORM.equals(formCode))
        {
            return "подготовка специалистов";
        }
        if ((QualificationsCodes.BAKALAVR.equals(qualificationCode) || QualificationsCodes.MAGISTR.equals(qualificationCode)) && !DevelopFormCodes.CORESP_FORM.equals(formCode))
        {
            return "подготовка бакалавров и магистров";
        }
        return "подготовка специалистов или бакалавров и магистров";

    }

    public static String getdevelopFormString(final String formCode)
    {
        if (DevelopFormCodes.CORESP_FORM.equals(formCode))
        {
            return "заочная";
        }
        if (DevelopFormCodes.FULL_TIME_FORM.equals(formCode) || DevelopFormCodes.PART_TIME_FORM.equals(formCode))
        {
            return "очная или вечерняя";
        }
        return null;
    }

    public static String getCathedraCode(final EppEduPlanVersion version)
    {
        final OrgUnit owner = version.getEduPlan().getOwner();

        if(null == owner) return null;
        if (owner.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.CATHEDRA))
        {
            return owner.getDivisionCode();
        }
        return null;
    }

    public static String getFacultyTitle(final EppEduPlanVersion version)
    {
        final OrgUnit owner = version.getEduPlan().getOwner();

        if(null == owner) return null;
        if (owner.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.CATHEDRA))
        {
            if (owner.getParent().getOrgUnitType().getCode().equals(OrgUnitTypeCodes.FACULTY))
            {
                owner.getParent().getShortTitle();
            }
        }
        return null;
    }

    public static String getPlanLineIdType(final String qualificationCode, final String formCode)
    {
        if (QualificationsCodes.SPETSIALIST.equals(qualificationCode) && !DevelopFormCodes.CORESP_FORM.equals(formCode))
        {
            return "1";
        }
        if ((QualificationsCodes.BAKALAVR.equals(qualificationCode) || QualificationsCodes.MAGISTR.equals(qualificationCode)) && !DevelopFormCodes.CORESP_FORM.equals(formCode))
        {
            return "2";
        }
        return "3";
    }

    private static String getIndex4PlanLineCycle(final IEppEpvRowWrapper row)
    {
        if (null == row) { return null; }

        final String parentIndex = EppEPVExportHSUtils.getIndex4PlanLineCycle(row.getHierarhyParent());
        if (null == parentIndex) { return row.getSelfIndexPart(); }

        if (row.getRow() instanceof EppEpvGroupImRow) {
            return (parentIndex + row.getSelfIndexPart()); // без точки (опять таки требование ИМЦА)
        }

        return parentIndex + "." + row.getSelfIndexPart();
    }

    public static String getPlanLineCycle(final IEppEpvRowWrapper row)
    {
        final IEppEpvRowWrapper p = row.getHierarhyParent();
        if (null == p) { return null; }
        return EppEPVExportHSUtils.getIndex4PlanLineCycle(p);
    }


    public static String formatAction(final List<Integer> terms)
    {
        if (terms.isEmpty())
        {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        for (final Integer term : terms)
        {
            if (term < 13)
            {
                sb.append(Long.toHexString((long) term).toString().toUpperCase());
            }
        }
        return sb.toString();
    }
}
