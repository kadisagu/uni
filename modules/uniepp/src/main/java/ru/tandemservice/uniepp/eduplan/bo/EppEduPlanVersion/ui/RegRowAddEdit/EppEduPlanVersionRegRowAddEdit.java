/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RegRowAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.logic.regRowAddEdit.EppProfessionalTaskComboDSHandler;

/**
 * @author oleyba
 * @since 11/12/14
 */
@Configuration
public class EppEduPlanVersionRegRowAddEdit extends BusinessComponentManager
{
    public final static String EPP_PROFESSIONAL_TASK_DS = "eppProfessionalTaskDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(EPP_PROFESSIONAL_TASK_DS, eppProfessionalTaskDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eppProfessionalTaskDSHandler()
    {
        return new EppProfessionalTaskComboDSHandler(getName());
    }
}