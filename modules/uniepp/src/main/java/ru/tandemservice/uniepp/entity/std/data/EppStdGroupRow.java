package ru.tandemservice.uniepp.entity.std.data;

import ru.tandemservice.uniepp.entity.IEppNumberRow;
import ru.tandemservice.uniepp.entity.std.data.gen.EppStdGroupRowGen;

/**
 * Запись ГОС (группа дисциплин)
 */
public abstract class EppStdGroupRow extends EppStdGroupRowGen implements IEppNumberRow
{
    @Override public EppStdHierarchyRow getHierarhyParent() { return this.getParent(); }
    @Override public void setHierarhyParent(final EppStdRow parent) { this.setParent((EppStdHierarchyRow) parent); }

    @Override public int getComparatorClassIndex() { return 3; }
    @Override public String getComparatorString() { return this.getSelfIndexPart(); }
    @Override public String getSelfIndexPart() { return IEppNumberRow.NUMBER_FORMATTER.format(this); }

}