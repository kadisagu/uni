/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.AttachRegElements;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;

/**
 * @author Nikolay Fedorovskih
 * @since 20.05.2015
 */
@Configuration
public class EppEduPlanVersionAttachRegElements extends BusinessComponentManager
{
    public static final Long ATTACH_RULE_ALWAYS_NEW = 1L; // Всегда создавать новые элементы реестра
    public static final Long ATTACH_RULE_NEW_IF_MISSING = 2L; // Искать среди существующих, создавать только если не найдено
    public static final Long ATTACH_RULE_ONLY_EXISTS = 3L; // Не создавать новые. Прикреплять только существующие

    public static final String ATTACHE_RULE_DS = "attachRuleDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EppStateManager.instance().eppStateDSConfig())
                .addDataSource(selectDS(ATTACHE_RULE_DS, attachRuleDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler attachRuleDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addRecord(ATTACH_RULE_ALWAYS_NEW, "Всегда создавать новые элементы реестра")
                .addRecord(ATTACH_RULE_NEW_IF_MISSING, "Создавать недостающие элементы реестра")
                .addRecord(ATTACH_RULE_ONLY_EXISTS, "Не создавать");
    }
}