package ru.tandemservice.uniepp.entity.plan.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка УП (с нагрузкой)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvTermDistributedRowGen extends EppEpvRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow";
    public static final String ENTITY_NAME = "eppEpvTermDistributedRow";
    public static final int VERSION_HASH = -1087686360;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";
    public static final String P_NUMBER = "number";
    public static final String P_TITLE = "title";
    public static final String P_TOTAL_LABOR = "totalLabor";
    public static final String P_HOURS_TOTAL = "hoursTotal";
    public static final String P_HOURS_AUDIT = "hoursAudit";
    public static final String P_HOURS_SELFWORK = "hoursSelfwork";
    public static final String P_HOURS_CONTROL = "hoursControl";
    public static final String P_HOURS_CONTROL_E = "hoursControlE";

    private EppEpvRow _parent;     // Строка УП
    private String _number;     // Номер строки
    private String _title;     // Название
    private long _totalLabor;     // Всего трудоемкость
    private long _hoursTotal;     // Всего часов
    private long _hoursAudit;     // Аудиторных часов
    private long _hoursSelfwork;     // Часов самостоятельной работы
    private long _hoursControl;     // Часов на контроль (экзамен)
    private long _hoursControlE;     // Часов на контроль в эл. форме

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка УП.
     */
    public EppEpvRow getParent()
    {
        return _parent;
    }

    /**
     * @param parent Строка УП.
     */
    public void setParent(EppEpvRow parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Номер строки. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер строки. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Всего трудоемкость. Свойство не может быть null.
     */
    @NotNull
    public long getTotalLabor()
    {
        return _totalLabor;
    }

    /**
     * @param totalLabor Всего трудоемкость. Свойство не может быть null.
     */
    public void setTotalLabor(long totalLabor)
    {
        dirty(_totalLabor, totalLabor);
        _totalLabor = totalLabor;
    }

    /**
     * @return Всего часов. Свойство не может быть null.
     */
    @NotNull
    public long getHoursTotal()
    {
        return _hoursTotal;
    }

    /**
     * @param hoursTotal Всего часов. Свойство не может быть null.
     */
    public void setHoursTotal(long hoursTotal)
    {
        dirty(_hoursTotal, hoursTotal);
        _hoursTotal = hoursTotal;
    }

    /**
     * @return Аудиторных часов. Свойство не может быть null.
     */
    @NotNull
    public long getHoursAudit()
    {
        return _hoursAudit;
    }

    /**
     * @param hoursAudit Аудиторных часов. Свойство не может быть null.
     */
    public void setHoursAudit(long hoursAudit)
    {
        dirty(_hoursAudit, hoursAudit);
        _hoursAudit = hoursAudit;
    }

    /**
     * @return Часов самостоятельной работы. Свойство не может быть null.
     */
    @NotNull
    public long getHoursSelfwork()
    {
        return _hoursSelfwork;
    }

    /**
     * @param hoursSelfwork Часов самостоятельной работы. Свойство не может быть null.
     */
    public void setHoursSelfwork(long hoursSelfwork)
    {
        dirty(_hoursSelfwork, hoursSelfwork);
        _hoursSelfwork = hoursSelfwork;
    }

    /**
     * @return Часов на контроль (экзамен). Свойство не может быть null.
     */
    @NotNull
    public long getHoursControl()
    {
        return _hoursControl;
    }

    /**
     * @param hoursControl Часов на контроль (экзамен). Свойство не может быть null.
     */
    public void setHoursControl(long hoursControl)
    {
        dirty(_hoursControl, hoursControl);
        _hoursControl = hoursControl;
    }

    /**
     * @return Часов на контроль в эл. форме. Свойство не может быть null.
     */
    @NotNull
    public long getHoursControlE()
    {
        return _hoursControlE;
    }

    /**
     * @param hoursControlE Часов на контроль в эл. форме. Свойство не может быть null.
     */
    public void setHoursControlE(long hoursControlE)
    {
        dirty(_hoursControlE, hoursControlE);
        _hoursControlE = hoursControlE;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEpvTermDistributedRowGen)
        {
            setParent(((EppEpvTermDistributedRow)another).getParent());
            setNumber(((EppEpvTermDistributedRow)another).getNumber());
            setTitle(((EppEpvTermDistributedRow)another).getTitle());
            setTotalLabor(((EppEpvTermDistributedRow)another).getTotalLabor());
            setHoursTotal(((EppEpvTermDistributedRow)another).getHoursTotal());
            setHoursAudit(((EppEpvTermDistributedRow)another).getHoursAudit());
            setHoursSelfwork(((EppEpvTermDistributedRow)another).getHoursSelfwork());
            setHoursControl(((EppEpvTermDistributedRow)another).getHoursControl());
            setHoursControlE(((EppEpvTermDistributedRow)another).getHoursControlE());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvTermDistributedRowGen> extends EppEpvRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvTermDistributedRow.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppEpvTermDistributedRow is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return obj.getParent();
                case "number":
                    return obj.getNumber();
                case "title":
                    return obj.getTitle();
                case "totalLabor":
                    return obj.getTotalLabor();
                case "hoursTotal":
                    return obj.getHoursTotal();
                case "hoursAudit":
                    return obj.getHoursAudit();
                case "hoursSelfwork":
                    return obj.getHoursSelfwork();
                case "hoursControl":
                    return obj.getHoursControl();
                case "hoursControlE":
                    return obj.getHoursControlE();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "parent":
                    obj.setParent((EppEpvRow) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "totalLabor":
                    obj.setTotalLabor((Long) value);
                    return;
                case "hoursTotal":
                    obj.setHoursTotal((Long) value);
                    return;
                case "hoursAudit":
                    obj.setHoursAudit((Long) value);
                    return;
                case "hoursSelfwork":
                    obj.setHoursSelfwork((Long) value);
                    return;
                case "hoursControl":
                    obj.setHoursControl((Long) value);
                    return;
                case "hoursControlE":
                    obj.setHoursControlE((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                        return true;
                case "number":
                        return true;
                case "title":
                        return true;
                case "totalLabor":
                        return true;
                case "hoursTotal":
                        return true;
                case "hoursAudit":
                        return true;
                case "hoursSelfwork":
                        return true;
                case "hoursControl":
                        return true;
                case "hoursControlE":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return true;
                case "number":
                    return true;
                case "title":
                    return true;
                case "totalLabor":
                    return true;
                case "hoursTotal":
                    return true;
                case "hoursAudit":
                    return true;
                case "hoursSelfwork":
                    return true;
                case "hoursControl":
                    return true;
                case "hoursControlE":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return EppEpvRow.class;
                case "number":
                    return String.class;
                case "title":
                    return String.class;
                case "totalLabor":
                    return Long.class;
                case "hoursTotal":
                    return Long.class;
                case "hoursAudit":
                    return Long.class;
                case "hoursSelfwork":
                    return Long.class;
                case "hoursControl":
                    return Long.class;
                case "hoursControlE":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvTermDistributedRow> _dslPath = new Path<EppEpvTermDistributedRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvTermDistributedRow");
    }
            

    /**
     * @return Строка УП.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getParent()
     */
    public static EppEpvRow.Path<EppEpvRow> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Номер строки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Всего трудоемкость. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getTotalLabor()
     */
    public static PropertyPath<Long> totalLabor()
    {
        return _dslPath.totalLabor();
    }

    /**
     * @return Всего часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getHoursTotal()
     */
    public static PropertyPath<Long> hoursTotal()
    {
        return _dslPath.hoursTotal();
    }

    /**
     * @return Аудиторных часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getHoursAudit()
     */
    public static PropertyPath<Long> hoursAudit()
    {
        return _dslPath.hoursAudit();
    }

    /**
     * @return Часов самостоятельной работы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getHoursSelfwork()
     */
    public static PropertyPath<Long> hoursSelfwork()
    {
        return _dslPath.hoursSelfwork();
    }

    /**
     * @return Часов на контроль (экзамен). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getHoursControl()
     */
    public static PropertyPath<Long> hoursControl()
    {
        return _dslPath.hoursControl();
    }

    /**
     * @return Часов на контроль в эл. форме. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getHoursControlE()
     */
    public static PropertyPath<Long> hoursControlE()
    {
        return _dslPath.hoursControlE();
    }

    public static class Path<E extends EppEpvTermDistributedRow> extends EppEpvRow.Path<E>
    {
        private EppEpvRow.Path<EppEpvRow> _parent;
        private PropertyPath<String> _number;
        private PropertyPath<String> _title;
        private PropertyPath<Long> _totalLabor;
        private PropertyPath<Long> _hoursTotal;
        private PropertyPath<Long> _hoursAudit;
        private PropertyPath<Long> _hoursSelfwork;
        private PropertyPath<Long> _hoursControl;
        private PropertyPath<Long> _hoursControlE;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка УП.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getParent()
     */
        public EppEpvRow.Path<EppEpvRow> parent()
        {
            if(_parent == null )
                _parent = new EppEpvRow.Path<EppEpvRow>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Номер строки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppEpvTermDistributedRowGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppEpvTermDistributedRowGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Всего трудоемкость. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getTotalLabor()
     */
        public PropertyPath<Long> totalLabor()
        {
            if(_totalLabor == null )
                _totalLabor = new PropertyPath<Long>(EppEpvTermDistributedRowGen.P_TOTAL_LABOR, this);
            return _totalLabor;
        }

    /**
     * @return Всего часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getHoursTotal()
     */
        public PropertyPath<Long> hoursTotal()
        {
            if(_hoursTotal == null )
                _hoursTotal = new PropertyPath<Long>(EppEpvTermDistributedRowGen.P_HOURS_TOTAL, this);
            return _hoursTotal;
        }

    /**
     * @return Аудиторных часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getHoursAudit()
     */
        public PropertyPath<Long> hoursAudit()
        {
            if(_hoursAudit == null )
                _hoursAudit = new PropertyPath<Long>(EppEpvTermDistributedRowGen.P_HOURS_AUDIT, this);
            return _hoursAudit;
        }

    /**
     * @return Часов самостоятельной работы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getHoursSelfwork()
     */
        public PropertyPath<Long> hoursSelfwork()
        {
            if(_hoursSelfwork == null )
                _hoursSelfwork = new PropertyPath<Long>(EppEpvTermDistributedRowGen.P_HOURS_SELFWORK, this);
            return _hoursSelfwork;
        }

    /**
     * @return Часов на контроль (экзамен). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getHoursControl()
     */
        public PropertyPath<Long> hoursControl()
        {
            if(_hoursControl == null )
                _hoursControl = new PropertyPath<Long>(EppEpvTermDistributedRowGen.P_HOURS_CONTROL, this);
            return _hoursControl;
        }

    /**
     * @return Часов на контроль в эл. форме. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow#getHoursControlE()
     */
        public PropertyPath<Long> hoursControlE()
        {
            if(_hoursControlE == null )
                _hoursControlE = new PropertyPath<Long>(EppEpvTermDistributedRowGen.P_HOURS_CONTROL_E, this);
            return _hoursControlE;
        }

        public Class getEntityClass()
        {
            return EppEpvTermDistributedRow.class;
        }

        public String getEntityName()
        {
            return "eppEpvTermDistributedRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
