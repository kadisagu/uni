package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Базовый учебный план (УП)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppEduPlan";
    public static final String ENTITY_NAME = "eppEduPlan";
    public static final int VERSION_HASH = 682430454;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_KIND = "programKind";
    public static final String L_PROGRAM_FORM = "programForm";
    public static final String L_PROGRAM_TRAIT = "programTrait";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_DEVELOP_GRID = "developGrid";
    public static final String P_NUMBER = "number";
    public static final String P_REGISTRATION_NUMBER = "registrationNumber";
    public static final String P_TITLE_POSTFIX = "titlePostfix";
    public static final String P_EDU_START_YEAR = "eduStartYear";
    public static final String P_EDU_END_YEAR = "eduEndYear";
    public static final String L_OWNER = "owner";
    public static final String L_STATE = "state";
    public static final String P_CONFIRM_DATE = "confirmDate";
    public static final String P_COMMENT = "comment";
    public static final String L_PARENT = "parent";
    public static final String P_DEVELOP_COMBINATION_TITLE = "developCombinationTitle";
    public static final String P_DEVELOP_FORM_CONDITION = "developFormCondition";
    public static final String P_EDUCATION_ELEMENT_SIMPLE_TITLE = "educationElementSimpleTitle";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_WITH_ARCHIVE = "titleWithArchive";
    public static final String P_YEARS_STRING = "yearsString";

    private EduProgramKind _programKind;     // Вид образовательной программы
    private EduProgramForm _programForm;     // Форма обучения
    private EduProgramTrait _programTrait;     // Особенность реализации ОП
    private DevelopCondition _developCondition;     // Условие освоения
    private DevelopGrid _developGrid;     // Учебная сетка
    private String _number;     // Номер
    private String _registrationNumber;     // Номер регистрации
    private String _titlePostfix;     // Постфикс
    private int _eduStartYear;     // Год начала обучения
    private Integer _eduEndYear;     // Год окончания обучения
    private OrgUnit _owner;     // Подразделение, на котором создан УП
    private EppState _state;     // Состояние
    private Date _confirmDate;     // Дата утверждения
    private String _comment;     // Комментарий
    private EppStateEduStandard _parent;     // ГОС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид образовательной программы. Свойство не может быть null.
     */
    @NotNull
    public EduProgramKind getProgramKind()
    {
        return _programKind;
    }

    /**
     * @param programKind Вид образовательной программы. Свойство не может быть null.
     */
    public void setProgramKind(EduProgramKind programKind)
    {
        dirty(_programKind, programKind);
        _programKind = programKind;
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     */
    @NotNull
    public EduProgramForm getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения. Свойство не может быть null.
     */
    public void setProgramForm(EduProgramForm programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    /**
     * @return Особенность реализации ОП.
     */
    public EduProgramTrait getProgramTrait()
    {
        return _programTrait;
    }

    /**
     * @param programTrait Особенность реализации ОП.
     */
    public void setProgramTrait(EduProgramTrait programTrait)
    {
        dirty(_programTrait, programTrait);
        _programTrait = programTrait;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения. Свойство не может быть null.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Учебная сетка. Свойство не может быть null.
     */
    @NotNull
    public DevelopGrid getDevelopGrid()
    {
        return _developGrid;
    }

    /**
     * @param developGrid Учебная сетка. Свойство не может быть null.
     */
    public void setDevelopGrid(DevelopGrid developGrid)
    {
        dirty(_developGrid, developGrid);
        _developGrid = developGrid;
    }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null и должно быть уникальным.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Номер регистрации. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRegistrationNumber()
    {
        return _registrationNumber;
    }

    /**
     * @param registrationNumber Номер регистрации. Свойство не может быть null.
     */
    public void setRegistrationNumber(String registrationNumber)
    {
        dirty(_registrationNumber, registrationNumber);
        _registrationNumber = registrationNumber;
    }

    /**
     * @return Постфикс.
     */
    @Length(max=255)
    public String getTitlePostfix()
    {
        return _titlePostfix;
    }

    /**
     * @param titlePostfix Постфикс.
     */
    public void setTitlePostfix(String titlePostfix)
    {
        dirty(_titlePostfix, titlePostfix);
        _titlePostfix = titlePostfix;
    }

    /**
     * @return Год начала обучения. Свойство не может быть null.
     */
    @NotNull
    public int getEduStartYear()
    {
        return _eduStartYear;
    }

    /**
     * @param eduStartYear Год начала обучения. Свойство не может быть null.
     */
    public void setEduStartYear(int eduStartYear)
    {
        dirty(_eduStartYear, eduStartYear);
        _eduStartYear = eduStartYear;
    }

    /**
     * @return Год окончания обучения.
     */
    public Integer getEduEndYear()
    {
        return _eduEndYear;
    }

    /**
     * @param eduEndYear Год окончания обучения.
     */
    public void setEduEndYear(Integer eduEndYear)
    {
        dirty(_eduEndYear, eduEndYear);
        _eduEndYear = eduEndYear;
    }

    /**
     * @return Подразделение, на котором создан УП.
     */
    public OrgUnit getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Подразделение, на котором создан УП.
     */
    public void setOwner(OrgUnit owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public EppState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(EppState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Дата утверждения.
     */
    public Date getConfirmDate()
    {
        return _confirmDate;
    }

    /**
     * @param confirmDate Дата утверждения.
     */
    public void setConfirmDate(Date confirmDate)
    {
        dirty(_confirmDate, confirmDate);
        _confirmDate = confirmDate;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return ГОС.
     */
    public EppStateEduStandard getParent()
    {
        return _parent;
    }

    /**
     * @param parent ГОС.
     */
    public void setParent(EppStateEduStandard parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEduPlanGen)
        {
            setProgramKind(((EppEduPlan)another).getProgramKind());
            setProgramForm(((EppEduPlan)another).getProgramForm());
            setProgramTrait(((EppEduPlan)another).getProgramTrait());
            setDevelopCondition(((EppEduPlan)another).getDevelopCondition());
            setDevelopGrid(((EppEduPlan)another).getDevelopGrid());
            setNumber(((EppEduPlan)another).getNumber());
            setRegistrationNumber(((EppEduPlan)another).getRegistrationNumber());
            setTitlePostfix(((EppEduPlan)another).getTitlePostfix());
            setEduStartYear(((EppEduPlan)another).getEduStartYear());
            setEduEndYear(((EppEduPlan)another).getEduEndYear());
            setOwner(((EppEduPlan)another).getOwner());
            setState(((EppEduPlan)another).getState());
            setConfirmDate(((EppEduPlan)another).getConfirmDate());
            setComment(((EppEduPlan)another).getComment());
            setParent(((EppEduPlan)another).getParent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlan.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppEduPlan is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "programKind":
                    return obj.getProgramKind();
                case "programForm":
                    return obj.getProgramForm();
                case "programTrait":
                    return obj.getProgramTrait();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developGrid":
                    return obj.getDevelopGrid();
                case "number":
                    return obj.getNumber();
                case "registrationNumber":
                    return obj.getRegistrationNumber();
                case "titlePostfix":
                    return obj.getTitlePostfix();
                case "eduStartYear":
                    return obj.getEduStartYear();
                case "eduEndYear":
                    return obj.getEduEndYear();
                case "owner":
                    return obj.getOwner();
                case "state":
                    return obj.getState();
                case "confirmDate":
                    return obj.getConfirmDate();
                case "comment":
                    return obj.getComment();
                case "parent":
                    return obj.getParent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "programKind":
                    obj.setProgramKind((EduProgramKind) value);
                    return;
                case "programForm":
                    obj.setProgramForm((EduProgramForm) value);
                    return;
                case "programTrait":
                    obj.setProgramTrait((EduProgramTrait) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "developGrid":
                    obj.setDevelopGrid((DevelopGrid) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "registrationNumber":
                    obj.setRegistrationNumber((String) value);
                    return;
                case "titlePostfix":
                    obj.setTitlePostfix((String) value);
                    return;
                case "eduStartYear":
                    obj.setEduStartYear((Integer) value);
                    return;
                case "eduEndYear":
                    obj.setEduEndYear((Integer) value);
                    return;
                case "owner":
                    obj.setOwner((OrgUnit) value);
                    return;
                case "state":
                    obj.setState((EppState) value);
                    return;
                case "confirmDate":
                    obj.setConfirmDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "parent":
                    obj.setParent((EppStateEduStandard) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "programKind":
                        return true;
                case "programForm":
                        return true;
                case "programTrait":
                        return true;
                case "developCondition":
                        return true;
                case "developGrid":
                        return true;
                case "number":
                        return true;
                case "registrationNumber":
                        return true;
                case "titlePostfix":
                        return true;
                case "eduStartYear":
                        return true;
                case "eduEndYear":
                        return true;
                case "owner":
                        return true;
                case "state":
                        return true;
                case "confirmDate":
                        return true;
                case "comment":
                        return true;
                case "parent":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "programKind":
                    return true;
                case "programForm":
                    return true;
                case "programTrait":
                    return true;
                case "developCondition":
                    return true;
                case "developGrid":
                    return true;
                case "number":
                    return true;
                case "registrationNumber":
                    return true;
                case "titlePostfix":
                    return true;
                case "eduStartYear":
                    return true;
                case "eduEndYear":
                    return true;
                case "owner":
                    return true;
                case "state":
                    return true;
                case "confirmDate":
                    return true;
                case "comment":
                    return true;
                case "parent":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "programKind":
                    return EduProgramKind.class;
                case "programForm":
                    return EduProgramForm.class;
                case "programTrait":
                    return EduProgramTrait.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "developGrid":
                    return DevelopGrid.class;
                case "number":
                    return String.class;
                case "registrationNumber":
                    return String.class;
                case "titlePostfix":
                    return String.class;
                case "eduStartYear":
                    return Integer.class;
                case "eduEndYear":
                    return Integer.class;
                case "owner":
                    return OrgUnit.class;
                case "state":
                    return EppState.class;
                case "confirmDate":
                    return Date.class;
                case "comment":
                    return String.class;
                case "parent":
                    return EppStateEduStandard.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlan> _dslPath = new Path<EppEduPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlan");
    }
            

    /**
     * @return Вид образовательной программы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getProgramKind()
     */
    public static EduProgramKind.Path<EduProgramKind> programKind()
    {
        return _dslPath.programKind();
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getProgramForm()
     */
    public static EduProgramForm.Path<EduProgramForm> programForm()
    {
        return _dslPath.programForm();
    }

    /**
     * @return Особенность реализации ОП.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getProgramTrait()
     */
    public static EduProgramTrait.Path<EduProgramTrait> programTrait()
    {
        return _dslPath.programTrait();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Учебная сетка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getDevelopGrid()
     */
    public static DevelopGrid.Path<DevelopGrid> developGrid()
    {
        return _dslPath.developGrid();
    }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Номер регистрации. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getRegistrationNumber()
     */
    public static PropertyPath<String> registrationNumber()
    {
        return _dslPath.registrationNumber();
    }

    /**
     * @return Постфикс.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getTitlePostfix()
     */
    public static PropertyPath<String> titlePostfix()
    {
        return _dslPath.titlePostfix();
    }

    /**
     * @return Год начала обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getEduStartYear()
     */
    public static PropertyPath<Integer> eduStartYear()
    {
        return _dslPath.eduStartYear();
    }

    /**
     * @return Год окончания обучения.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getEduEndYear()
     */
    public static PropertyPath<Integer> eduEndYear()
    {
        return _dslPath.eduEndYear();
    }

    /**
     * @return Подразделение, на котором создан УП.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getOwner()
     */
    public static OrgUnit.Path<OrgUnit> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getState()
     */
    public static EppState.Path<EppState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getConfirmDate()
     */
    public static PropertyPath<Date> confirmDate()
    {
        return _dslPath.confirmDate();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return ГОС.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getParent()
     */
    public static EppStateEduStandard.Path<EppStateEduStandard> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getDevelopCombinationTitle()
     */
    public static SupportedPropertyPath<String> developCombinationTitle()
    {
        return _dslPath.developCombinationTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getDevelopFormCondition()
     */
    public static SupportedPropertyPath<String> developFormCondition()
    {
        return _dslPath.developFormCondition();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getEducationElementSimpleTitle()
     */
    public static SupportedPropertyPath<String> educationElementSimpleTitle()
    {
        return _dslPath.educationElementSimpleTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getTitleWithArchive()
     */
    public static SupportedPropertyPath<String> titleWithArchive()
    {
        return _dslPath.titleWithArchive();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getYearsString()
     */
    public static SupportedPropertyPath<String> yearsString()
    {
        return _dslPath.yearsString();
    }

    public static class Path<E extends EppEduPlan> extends EntityPath<E>
    {
        private EduProgramKind.Path<EduProgramKind> _programKind;
        private EduProgramForm.Path<EduProgramForm> _programForm;
        private EduProgramTrait.Path<EduProgramTrait> _programTrait;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private DevelopGrid.Path<DevelopGrid> _developGrid;
        private PropertyPath<String> _number;
        private PropertyPath<String> _registrationNumber;
        private PropertyPath<String> _titlePostfix;
        private PropertyPath<Integer> _eduStartYear;
        private PropertyPath<Integer> _eduEndYear;
        private OrgUnit.Path<OrgUnit> _owner;
        private EppState.Path<EppState> _state;
        private PropertyPath<Date> _confirmDate;
        private PropertyPath<String> _comment;
        private EppStateEduStandard.Path<EppStateEduStandard> _parent;
        private SupportedPropertyPath<String> _developCombinationTitle;
        private SupportedPropertyPath<String> _developFormCondition;
        private SupportedPropertyPath<String> _educationElementSimpleTitle;
        private SupportedPropertyPath<String> _title;
        private SupportedPropertyPath<String> _titleWithArchive;
        private SupportedPropertyPath<String> _yearsString;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид образовательной программы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getProgramKind()
     */
        public EduProgramKind.Path<EduProgramKind> programKind()
        {
            if(_programKind == null )
                _programKind = new EduProgramKind.Path<EduProgramKind>(L_PROGRAM_KIND, this);
            return _programKind;
        }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getProgramForm()
     */
        public EduProgramForm.Path<EduProgramForm> programForm()
        {
            if(_programForm == null )
                _programForm = new EduProgramForm.Path<EduProgramForm>(L_PROGRAM_FORM, this);
            return _programForm;
        }

    /**
     * @return Особенность реализации ОП.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getProgramTrait()
     */
        public EduProgramTrait.Path<EduProgramTrait> programTrait()
        {
            if(_programTrait == null )
                _programTrait = new EduProgramTrait.Path<EduProgramTrait>(L_PROGRAM_TRAIT, this);
            return _programTrait;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Учебная сетка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getDevelopGrid()
     */
        public DevelopGrid.Path<DevelopGrid> developGrid()
        {
            if(_developGrid == null )
                _developGrid = new DevelopGrid.Path<DevelopGrid>(L_DEVELOP_GRID, this);
            return _developGrid;
        }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppEduPlanGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Номер регистрации. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getRegistrationNumber()
     */
        public PropertyPath<String> registrationNumber()
        {
            if(_registrationNumber == null )
                _registrationNumber = new PropertyPath<String>(EppEduPlanGen.P_REGISTRATION_NUMBER, this);
            return _registrationNumber;
        }

    /**
     * @return Постфикс.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getTitlePostfix()
     */
        public PropertyPath<String> titlePostfix()
        {
            if(_titlePostfix == null )
                _titlePostfix = new PropertyPath<String>(EppEduPlanGen.P_TITLE_POSTFIX, this);
            return _titlePostfix;
        }

    /**
     * @return Год начала обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getEduStartYear()
     */
        public PropertyPath<Integer> eduStartYear()
        {
            if(_eduStartYear == null )
                _eduStartYear = new PropertyPath<Integer>(EppEduPlanGen.P_EDU_START_YEAR, this);
            return _eduStartYear;
        }

    /**
     * @return Год окончания обучения.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getEduEndYear()
     */
        public PropertyPath<Integer> eduEndYear()
        {
            if(_eduEndYear == null )
                _eduEndYear = new PropertyPath<Integer>(EppEduPlanGen.P_EDU_END_YEAR, this);
            return _eduEndYear;
        }

    /**
     * @return Подразделение, на котором создан УП.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getOwner()
     */
        public OrgUnit.Path<OrgUnit> owner()
        {
            if(_owner == null )
                _owner = new OrgUnit.Path<OrgUnit>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getState()
     */
        public EppState.Path<EppState> state()
        {
            if(_state == null )
                _state = new EppState.Path<EppState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getConfirmDate()
     */
        public PropertyPath<Date> confirmDate()
        {
            if(_confirmDate == null )
                _confirmDate = new PropertyPath<Date>(EppEduPlanGen.P_CONFIRM_DATE, this);
            return _confirmDate;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EppEduPlanGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return ГОС.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getParent()
     */
        public EppStateEduStandard.Path<EppStateEduStandard> parent()
        {
            if(_parent == null )
                _parent = new EppStateEduStandard.Path<EppStateEduStandard>(L_PARENT, this);
            return _parent;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getDevelopCombinationTitle()
     */
        public SupportedPropertyPath<String> developCombinationTitle()
        {
            if(_developCombinationTitle == null )
                _developCombinationTitle = new SupportedPropertyPath<String>(EppEduPlanGen.P_DEVELOP_COMBINATION_TITLE, this);
            return _developCombinationTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getDevelopFormCondition()
     */
        public SupportedPropertyPath<String> developFormCondition()
        {
            if(_developFormCondition == null )
                _developFormCondition = new SupportedPropertyPath<String>(EppEduPlanGen.P_DEVELOP_FORM_CONDITION, this);
            return _developFormCondition;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getEducationElementSimpleTitle()
     */
        public SupportedPropertyPath<String> educationElementSimpleTitle()
        {
            if(_educationElementSimpleTitle == null )
                _educationElementSimpleTitle = new SupportedPropertyPath<String>(EppEduPlanGen.P_EDUCATION_ELEMENT_SIMPLE_TITLE, this);
            return _educationElementSimpleTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EppEduPlanGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getTitleWithArchive()
     */
        public SupportedPropertyPath<String> titleWithArchive()
        {
            if(_titleWithArchive == null )
                _titleWithArchive = new SupportedPropertyPath<String>(EppEduPlanGen.P_TITLE_WITH_ARCHIVE, this);
            return _titleWithArchive;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlan#getYearsString()
     */
        public SupportedPropertyPath<String> yearsString()
        {
            if(_yearsString == null )
                _yearsString = new SupportedPropertyPath<String>(EppEduPlanGen.P_YEARS_STRING, this);
            return _yearsString;
        }

        public Class getEntityClass()
        {
            return EppEduPlan.class;
        }

        public String getEntityName()
        {
            return "eppEduPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDevelopCombinationTitle();

    public abstract String getDevelopFormCondition();

    public abstract String getEducationElementSimpleTitle();

    public abstract String getTitle();

    public abstract String getTitleWithArchive();

    public abstract String getYearsString();
}
