/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppSettings;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Nikolay Fedorovskih
 * @since 16.01.2015
 */
@Configuration
public class EppSettingsManager extends BusinessObjectManager
{
}