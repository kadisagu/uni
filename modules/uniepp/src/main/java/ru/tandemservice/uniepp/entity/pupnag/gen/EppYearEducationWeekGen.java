package ru.tandemservice.uniepp.entity.pupnag.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Неделя (в учебном году)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppYearEducationWeekGen extends EntityBase
 implements INaturalIdentifiable<EppYearEducationWeekGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek";
    public static final String ENTITY_NAME = "eppYearEducationWeek";
    public static final int VERSION_HASH = -7078546;
    private static IEntityMeta ENTITY_META;

    public static final String L_YEAR = "year";
    public static final String P_NUMBER = "number";
    public static final String P_DATE = "date";
    public static final String P_TITLE = "title";
    public static final String P_CALCULATED_TITLE = "calculatedTitle";
    public static final String P_END_DATE = "endDate";

    private EppYearEducationProcess _year;     // ПУПнаГ
    private int _number;     // Номер недели
    private Date _date;     // Дата начала недели
    private String _title;     // Заголовок (кэш)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getYear()
    {
        return _year;
    }

    /**
     * @param year ПУПнаГ. Свойство не может быть null.
     */
    public void setYear(EppYearEducationProcess year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Номер недели. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер недели. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата начала недели. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата начала недели. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Заголовок (кэш). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Заголовок (кэш). Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppYearEducationWeekGen)
        {
            if (withNaturalIdProperties)
            {
                setYear(((EppYearEducationWeek)another).getYear());
                setNumber(((EppYearEducationWeek)another).getNumber());
            }
            setDate(((EppYearEducationWeek)another).getDate());
            setTitle(((EppYearEducationWeek)another).getTitle());
        }
    }

    public INaturalId<EppYearEducationWeekGen> getNaturalId()
    {
        return new NaturalId(getYear(), getNumber());
    }

    public static class NaturalId extends NaturalIdBase<EppYearEducationWeekGen>
    {
        private static final String PROXY_NAME = "EppYearEducationWeekNaturalProxy";

        private Long _year;
        private int _number;

        public NaturalId()
        {}

        public NaturalId(EppYearEducationProcess year, int number)
        {
            _year = ((IEntity) year).getId();
            _number = number;
        }

        public Long getYear()
        {
            return _year;
        }

        public void setYear(Long year)
        {
            _year = year;
        }

        public int getNumber()
        {
            return _number;
        }

        public void setNumber(int number)
        {
            _number = number;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppYearEducationWeekGen.NaturalId) ) return false;

            EppYearEducationWeekGen.NaturalId that = (NaturalId) o;

            if( !equals(getYear(), that.getYear()) ) return false;
            if( !equals(getNumber(), that.getNumber()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getYear());
            result = hashCode(result, getNumber());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getYear());
            sb.append("/");
            sb.append(getNumber());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppYearEducationWeekGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppYearEducationWeek.class;
        }

        public T newInstance()
        {
            return (T) new EppYearEducationWeek();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "year":
                    return obj.getYear();
                case "number":
                    return obj.getNumber();
                case "date":
                    return obj.getDate();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "year":
                    obj.setYear((EppYearEducationProcess) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "year":
                        return true;
                case "number":
                        return true;
                case "date":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "year":
                    return true;
                case "number":
                    return true;
                case "date":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "year":
                    return EppYearEducationProcess.class;
                case "number":
                    return Integer.class;
                case "date":
                    return Date.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppYearEducationWeek> _dslPath = new Path<EppYearEducationWeek>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppYearEducationWeek");
    }
            

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek#getYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Номер недели. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата начала недели. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Заголовок (кэш). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek#getCalculatedTitle()
     */
    public static SupportedPropertyPath<String> calculatedTitle()
    {
        return _dslPath.calculatedTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek#getEndDate()
     */
    public static SupportedPropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    public static class Path<E extends EppYearEducationWeek> extends EntityPath<E>
    {
        private EppYearEducationProcess.Path<EppYearEducationProcess> _year;
        private PropertyPath<Integer> _number;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<String> _calculatedTitle;
        private SupportedPropertyPath<Date> _endDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek#getYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> year()
        {
            if(_year == null )
                _year = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_YEAR, this);
            return _year;
        }

    /**
     * @return Номер недели. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(EppYearEducationWeekGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата начала недели. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(EppYearEducationWeekGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Заголовок (кэш). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppYearEducationWeekGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek#getCalculatedTitle()
     */
        public SupportedPropertyPath<String> calculatedTitle()
        {
            if(_calculatedTitle == null )
                _calculatedTitle = new SupportedPropertyPath<String>(EppYearEducationWeekGen.P_CALCULATED_TITLE, this);
            return _calculatedTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek#getEndDate()
     */
        public SupportedPropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new SupportedPropertyPath<Date>(EppYearEducationWeekGen.P_END_DATE, this);
            return _endDate;
        }

        public Class getEntityClass()
        {
            return EppYearEducationWeek.class;
        }

        public String getEntityName()
        {
            return "eppYearEducationWeek";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getCalculatedTitle();

    public abstract Date getEndDate();
}
