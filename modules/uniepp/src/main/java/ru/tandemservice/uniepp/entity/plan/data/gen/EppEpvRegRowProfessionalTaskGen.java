package ru.tandemservice.uniepp.entity.plan.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppProfessionalTask;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowProfessionalTask;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Профессиональная задача строки УП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvRegRowProfessionalTaskGen extends EntityBase
 implements INaturalIdentifiable<EppEpvRegRowProfessionalTaskGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowProfessionalTask";
    public static final String ENTITY_NAME = "eppEpvRegRowProfessionalTask";
    public static final int VERSION_HASH = 32072835;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPV_REGISTRY_ROW = "epvRegistryRow";
    public static final String L_PROFESSIONAL_TASK = "professionalTask";

    private EppEpvRegistryRow _epvRegistryRow;     // Строка УП (элемент реестра)
    private EppProfessionalTask _professionalTask;     // Профессиональная задача

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка УП (элемент реестра). Свойство не может быть null.
     */
    @NotNull
    public EppEpvRegistryRow getEpvRegistryRow()
    {
        return _epvRegistryRow;
    }

    /**
     * @param epvRegistryRow Строка УП (элемент реестра). Свойство не может быть null.
     */
    public void setEpvRegistryRow(EppEpvRegistryRow epvRegistryRow)
    {
        dirty(_epvRegistryRow, epvRegistryRow);
        _epvRegistryRow = epvRegistryRow;
    }

    /**
     * @return Профессиональная задача. Свойство не может быть null.
     */
    @NotNull
    public EppProfessionalTask getProfessionalTask()
    {
        return _professionalTask;
    }

    /**
     * @param professionalTask Профессиональная задача. Свойство не может быть null.
     */
    public void setProfessionalTask(EppProfessionalTask professionalTask)
    {
        dirty(_professionalTask, professionalTask);
        _professionalTask = professionalTask;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEpvRegRowProfessionalTaskGen)
        {
            if (withNaturalIdProperties)
            {
                setEpvRegistryRow(((EppEpvRegRowProfessionalTask)another).getEpvRegistryRow());
                setProfessionalTask(((EppEpvRegRowProfessionalTask)another).getProfessionalTask());
            }
        }
    }

    public INaturalId<EppEpvRegRowProfessionalTaskGen> getNaturalId()
    {
        return new NaturalId(getEpvRegistryRow(), getProfessionalTask());
    }

    public static class NaturalId extends NaturalIdBase<EppEpvRegRowProfessionalTaskGen>
    {
        private static final String PROXY_NAME = "EppEpvRegRowProfessionalTaskNaturalProxy";

        private Long _epvRegistryRow;
        private Long _professionalTask;

        public NaturalId()
        {}

        public NaturalId(EppEpvRegistryRow epvRegistryRow, EppProfessionalTask professionalTask)
        {
            _epvRegistryRow = ((IEntity) epvRegistryRow).getId();
            _professionalTask = ((IEntity) professionalTask).getId();
        }

        public Long getEpvRegistryRow()
        {
            return _epvRegistryRow;
        }

        public void setEpvRegistryRow(Long epvRegistryRow)
        {
            _epvRegistryRow = epvRegistryRow;
        }

        public Long getProfessionalTask()
        {
            return _professionalTask;
        }

        public void setProfessionalTask(Long professionalTask)
        {
            _professionalTask = professionalTask;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppEpvRegRowProfessionalTaskGen.NaturalId) ) return false;

            EppEpvRegRowProfessionalTaskGen.NaturalId that = (NaturalId) o;

            if( !equals(getEpvRegistryRow(), that.getEpvRegistryRow()) ) return false;
            if( !equals(getProfessionalTask(), that.getProfessionalTask()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEpvRegistryRow());
            result = hashCode(result, getProfessionalTask());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEpvRegistryRow());
            sb.append("/");
            sb.append(getProfessionalTask());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvRegRowProfessionalTaskGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvRegRowProfessionalTask.class;
        }

        public T newInstance()
        {
            return (T) new EppEpvRegRowProfessionalTask();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "epvRegistryRow":
                    return obj.getEpvRegistryRow();
                case "professionalTask":
                    return obj.getProfessionalTask();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "epvRegistryRow":
                    obj.setEpvRegistryRow((EppEpvRegistryRow) value);
                    return;
                case "professionalTask":
                    obj.setProfessionalTask((EppProfessionalTask) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "epvRegistryRow":
                        return true;
                case "professionalTask":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "epvRegistryRow":
                    return true;
                case "professionalTask":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "epvRegistryRow":
                    return EppEpvRegistryRow.class;
                case "professionalTask":
                    return EppProfessionalTask.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvRegRowProfessionalTask> _dslPath = new Path<EppEpvRegRowProfessionalTask>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvRegRowProfessionalTask");
    }
            

    /**
     * @return Строка УП (элемент реестра). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowProfessionalTask#getEpvRegistryRow()
     */
    public static EppEpvRegistryRow.Path<EppEpvRegistryRow> epvRegistryRow()
    {
        return _dslPath.epvRegistryRow();
    }

    /**
     * @return Профессиональная задача. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowProfessionalTask#getProfessionalTask()
     */
    public static EppProfessionalTask.Path<EppProfessionalTask> professionalTask()
    {
        return _dslPath.professionalTask();
    }

    public static class Path<E extends EppEpvRegRowProfessionalTask> extends EntityPath<E>
    {
        private EppEpvRegistryRow.Path<EppEpvRegistryRow> _epvRegistryRow;
        private EppProfessionalTask.Path<EppProfessionalTask> _professionalTask;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка УП (элемент реестра). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowProfessionalTask#getEpvRegistryRow()
     */
        public EppEpvRegistryRow.Path<EppEpvRegistryRow> epvRegistryRow()
        {
            if(_epvRegistryRow == null )
                _epvRegistryRow = new EppEpvRegistryRow.Path<EppEpvRegistryRow>(L_EPV_REGISTRY_ROW, this);
            return _epvRegistryRow;
        }

    /**
     * @return Профессиональная задача. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowProfessionalTask#getProfessionalTask()
     */
        public EppProfessionalTask.Path<EppProfessionalTask> professionalTask()
        {
            if(_professionalTask == null )
                _professionalTask = new EppProfessionalTask.Path<EppProfessionalTask>(L_PROFESSIONAL_TASK, this);
            return _professionalTask;
        }

        public Class getEntityClass()
        {
            return EppEpvRegRowProfessionalTask.class;
        }

        public String getEntityName()
        {
            return "eppEpvRegRowProfessionalTask";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
