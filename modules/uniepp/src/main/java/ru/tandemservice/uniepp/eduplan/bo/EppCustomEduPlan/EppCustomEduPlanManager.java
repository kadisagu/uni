/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppCustomPlanRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil.applySelectFilter;

/**
 * @author Nikolay Fedorovskih
 * @since 14.08.2015
 */
@Configuration
public class EppCustomEduPlanManager extends BusinessObjectManager
{
    public static EppCustomEduPlanManager instance()
    {
        return instance(EppCustomEduPlanManager.class);
    }

    public static final Long REATTESTATION_ITEM_ID = 1L;
    public static final Long REEXAMINATION_ITEM_ID = 2L;

    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
    public static final String EDU_PLAN_VERSION_DS = "eduPlanVersionDS";
    public static final String EPV_BLOCK_DS = "epvBlockDS";

    public static final String PARAM_PROGRAM_KIND = "programKind";
    public static final String PARAM_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String PARAM_PROGRAM_SUBJECT = "programSubject";

    public static ISelectModel createRetakeSelectModel()
    {
        return new LazySimpleSelectModel<IIdentifiable>(ImmutableList.of(
                new IdentifiableWrapper<>(REATTESTATION_ITEM_ID, IEppCustomPlanRowWrapper.REATTESTATION_DONE_TITLE),
                new IdentifiableWrapper<>(REEXAMINATION_ITEM_ID, IEppCustomPlanRowWrapper.REEXAMINATION_DONE_TITLE)
        ));
    }

    @Bean
    public UIDataSourceConfig programSubjectDSConfig()
    {
        return SelectDSConfig.with(PROGRAM_SUBJECT_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(programSubjectDSHandler())
                .addColumn(EduProgramSubject.titleWithCode().s())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {

                super.applyWhereConditions(alias, dql, context);

                final EduProgramKind programKind = context.get(PARAM_PROGRAM_KIND);
                final DQLSelectBuilder planDQL = new DQLSelectBuilder()
                        .fromEntity(EppEduPlanProf.class, "p")
                        .where(eq(property("p", EppEduPlanProf.programKind()), value(programKind)))
                        .where(eq(property("p", EppEduPlanProf.programSubject()), property(alias)));

                applySelectFilter(dql, alias, EduProgramSubject.subjectIndex().programKind(), programKind);

                dql.where(exists(planDQL.buildQuery()));
            }
        }
                .filter(EduProgramSubject.code())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.code())
                .order(EduProgramKind.title())
                .pageable(true);
    }

    @Bean
    public UIDataSourceConfig eduPlanVersionDSConfig()
    {
        return SelectDSConfig.with(EDU_PLAN_VERSION_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(eduPlanVersionDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduPlanVersionDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppEduPlanVersion.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {

                super.applyWhereConditions(alias, dql, context);

                final Object programSubject = context.get(PARAM_PROGRAM_SUBJECT);
                if (programSubject == null) {
                    dql.where(nothing());
                    return;
                }

                dql.joinEntity(alias, DQLJoinType.inner, EppEduPlanProf.class, "plan", eq(property(alias, EppEduPlanVersion.eduPlan()), property("plan")));

                applySelectFilter(dql, "plan", EppEduPlanProf.programSubject(), programSubject);
            }

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                final DQLSelectBuilder dql = super.query(alias, filter);
                if (StringUtils.isNotBlank(filter)) {
                    final IDQLExpression fullNumberExpr = DQLFunctions.concat(
                            property(alias, EppEduPlanVersion.number()), value("."),
                            property(alias, EppEduPlanVersion.eduPlan().number())
                    );
                    dql.where(likeUpper(fullNumberExpr, value(CoreStringUtils.escapeLike(filter))));
                }
                return dql;
            }
        }
                .order(EppEduPlanVersion.eduPlan().number())
                .order(EppEduPlanVersion.number())
                .pageable(true);
    }

    @Bean
    public UIDataSourceConfig epvBlockDSConfig()
    {
        return SelectDSConfig.with(EPV_BLOCK_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(epvBlockDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler epvBlockDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppEduPlanVersionSpecializationBlock.class) {
            @Override protected boolean isExternalSortRequired() { return true; }

            @SuppressWarnings("unchecked")
            @Override protected List<IEntity> sortOptions(List list) {
                Collections.sort((List<EppEduPlanVersionSpecializationBlock>) list);
                return list;
            }
        }
                .where(EppEduPlanVersionSpecializationBlock.eduPlanVersion(), PARAM_EDU_PLAN_VERSION, false)
                .pageable(false);
    }

}