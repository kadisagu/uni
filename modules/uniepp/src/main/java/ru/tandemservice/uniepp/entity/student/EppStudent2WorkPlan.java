package ru.tandemservice.uniepp.entity.student;

import org.tandemframework.core.common.IEntityDebugTitled;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.gen.EppStudent2WorkPlanGen;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

/**
 * Рабочий план студента
 */
public class EppStudent2WorkPlan extends EppStudent2WorkPlanGen implements IEntityDebugTitled
{
    public EppStudent2WorkPlan() {}
    public EppStudent2WorkPlan(final EppStudent2EduPlanVersion studentEduPlanVersion, final EppWorkPlanBase workPlan) {
        this.setStudentEduPlanVersion(studentEduPlanVersion);
        this.setWorkPlan(workPlan);
    }

    @SuppressWarnings("deprecation")
    public EppYearEducationProcess getEppYear() {
        return this.getCachedEppYear();
    }

    /** @deprecated метод сделан исключительно для поиска в базе */
    @Deprecated
    @Override
    public EppYearEducationProcess getCachedEppYear() {
        final EppYearEducationProcess v = super.getCachedEppYear();
        if (null != v) { return v; }

        // этот код по идее должен выполняться только в момент сохранения объекта в базу
        try {
            // в сущность значение сохранять нельзя
            // здесь используется КЭШ сессии
            return UniDaoFacade.getCoreDao().getCalculatedValue(session -> EppStudent2WorkPlan.this.getWorkPlan().getYear());
        } catch (final Exception t) {
            // неосили в силу ряда причин
            return null;
        }
    }

    /** @deprecated метод сделан исключительно для поиска в базе */
    @Deprecated
    @Override
    public void setCachedEppYear(final EppYearEducationProcess cachedEppYear) {
        super.setCachedEppYear(cachedEppYear);
    }


    @SuppressWarnings("deprecation")
    public DevelopGridTerm getGridTerm() {
        return this.getCachedGridTerm();
    }

    /** @deprecated метод сделан исключительно для поиска в базе */
    @Deprecated
    @Override
    public DevelopGridTerm getCachedGridTerm() {
        final DevelopGridTerm v = super.getCachedGridTerm();
        if (null != v) { return v; }

        // этот код по идее должен выполняться только в момент сохранения объекта в базу
        try {
            // в сущность значение сохранять нельзя
            // здесь используется КЭШ сессии
            return UniDaoFacade.getCoreDao().getCalculatedValue(session -> EppStudent2WorkPlan.this.getWorkPlan().getGridTerm());
        } catch (final Exception t) {
            // неосили в силу ряда причин
            return null;
        }
    }

    /** @deprecated метод сделан исключительно для поиска в базе */
    @Deprecated
    @Override
    public void setCachedGridTerm(final DevelopGridTerm cachedGridTerm) {
        super.setCachedGridTerm(cachedGridTerm);
    }

    @Override
    public String getEntityDebugTitle()
    {
        return getStudentEduPlanVersion().getStudent().getFio() + ", РУП " + getWorkPlan().getShortTitle();
    }
}