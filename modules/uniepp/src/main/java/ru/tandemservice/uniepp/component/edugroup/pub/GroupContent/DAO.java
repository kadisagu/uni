package ru.tandemservice.uniepp.component.edugroup.pub.GroupContent;

import org.hibernate.Session;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.gen.IdentityCardGen.Path;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    @Override
    public void prepare(final Model model) {
        final EppRealEduGroup group = model.getGroup();
        if (group == null)
            return;

        final List<OrgUnit> list = new DQLOrderDescriptionRegistry(EppRealEduGroupRow.class, "rel").buildDQLSelectBuilder()
        .column(property(EppRealEduGroupRow.studentEducationOrgUnit().groupOrgUnit().fromAlias("rel")))
        .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))))
        .where(eq(property(EppRealEduGroupRow.group().fromAlias("rel")), value(group)))
        .createStatement(getSession()).list();
        model.setGroupOUList(new HashSet<>(list));
    }

    @Override
    public void prepareDataSource(final Model model) {
        final DynamicListDataSource<EppRealEduGroupRow> dataSource = model.getDataSource();
        if (null == dataSource) {
            return;
        }

        final EppRealEduGroup group = model.getGroup();
        if (null == group) {
            UniBaseUtils.createPage(dataSource, Collections.<EppRealEduGroupRow>emptyList());
            return;
        }

        final Session session = this.getSession();
        final DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(EppRealEduGroupRow.class, "rel");
        final DQLSelectBuilder dql = registry.buildDQLSelectBuilder();
        dql.where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))));
        dql.where(eq(property(EppRealEduGroupRow.group().fromAlias("rel")), value(group)));

        dql.fetchPath(DQLJoinType.inner, EppRealEduGroupRow.studentWpePart().fromAlias("rel"), "rel_s");
        dql.fetchPath(DQLJoinType.inner, EppStudentWpePart.studentWpe().fromAlias("rel_s"), "slt");
        dql.fetchPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("slt"), "st");

        EntityOrder entityOrder = dataSource.getEntityOrder();
        if (null != entityOrder)
        {
            // сначала сортируем по колонке
            final Set<String> orderSet = new HashSet<>(registry.applyOrder(dql, entityOrder));

            // затем, добавляем сортировку по ФИО (если она еще не добавленна)
            final Path<IdentityCard> identityCardPath = EppRealEduGroupRow.studentWpePart().studentWpe().student().person().identityCard();
            if (DQLOrderDescriptionRegistry.order(dql, identityCardPath.lastName().fromAlias("rel").s(), OrderDirection.asc, orderSet)) {
                DQLOrderDescriptionRegistry.order(dql, identityCardPath.firstName().fromAlias("rel").s(), OrderDirection.asc, orderSet);
                DQLOrderDescriptionRegistry.order(dql, identityCardPath.middleName().fromAlias("rel").s(), OrderDirection.asc, orderSet);
            }
        }
        UniBaseUtils.createFullPage(dataSource, dql.column(property("rel")).createStatement(session).<EppRealEduGroupRow>list());
    }

    @Override
    public Map<Object, Collection<Long>> getRowDistributionMap(final Model model, final Collection<Long> ids, final PropertyPath[] key) {

        final EppRealEduGroup group = model.getGroup();
        if (null == group) { return Collections.emptyMap(); }

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, "rel");
        dql.where(eq(property(EppRealEduGroupRow.group().fromAlias("rel")), value(group)));

        dql.column(property(EppRealEduGroupRow.id().fromAlias("rel")));
        for (final PropertyPath path: key) {
            dql.column(property(path.fromAlias("rel")));
        }

        final int sz = key.length;
        final List<Object[]> rows = dql.createStatement(this.getSession()).list();
        final Map<Object, Collection<Long>> map = new HashMap<>();
        for (final Object[] row: rows) {
            SafeMap.safeGet(map, this.key(row, sz), ArrayList.class).add((Long)row[0]);
        }
        return map;
    }

    private Object key(final Object[] row, final int sz) {
        final StringBuilder sb = new StringBuilder();
        for (int i=0;i<sz;i++) { sb.append(row[1+i]).append(" "); }
        return sb.toString();
    }

}
