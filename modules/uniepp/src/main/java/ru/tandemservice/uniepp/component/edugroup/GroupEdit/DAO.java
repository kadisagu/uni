// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.edugroup.GroupEdit;

import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.component.edugroup.EppEduGroupRegElPartSelectModel;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;

import java.util.Collection;
import java.util.Collections;

/**
 * @author iolshvang
 * @since 12.04.2011
 */
public class DAO extends UniBaseDao implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        if (null == model.getGroupHolder().refresh()) {
            throw new IllegalArgumentException("model.groupHolder.id is null");
        }
        // Проверяем _только_ уровень согласования. Т.е. редактирование разрешаем, даже если есть ведомости, чтобы можно было мержить УГС.
        IEppRealGroupRowDAO.instance.get().checkGroupLockedLevel(Collections.singleton(model.getGroupHolder().getId()), model.getLevel());

        @SuppressWarnings("unchecked")
        final Class<? extends EppRealEduGroup> groupClass = EntityRuntime.getMeta(model.getGroupHolder().getId()).getEntityClass();
        model.setRegElementSelectModel(new EppEduGroupRegElPartSelectModel() {
            @Override protected Class<? extends EppRealEduGroup> getGroupClass() { return groupClass; }
            @Override protected Collection<Long> getGroupIds() { return Collections.singleton(model.getGroupHolder().getId()); }
        });
    }

    @Override
    public void update(final Model model)
    {
        // Проверяем _только_ уровень согласования. Т.е. редактирование разрешаем, даже если есть ведомости, чтобы можно было мержить УГС.
        IEppRealGroupRowDAO.instance.get().checkGroupLockedLevel(Collections.singleton(model.getGroupHolder().getId()), model.getLevel());
        EppRealEduGroup group = model.getGroup();

        // если в УГС есть стоока РУП, но на другую часть элемента реестра - обнуляем
        if (null != group.getWorkPlanRow()) {
            if (!group.getActivityPart().equals(group.getWorkPlanRow().getRegistryElementPart())) {
                group.setProperty(EppRealEduGroup.L_WORK_PLAN_ROW, null);
            }
        }

        this.update(group);
    }

}

