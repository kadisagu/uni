/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniepp.base.bo.EppSystemAction.logic.EppSystemActionDao;
import ru.tandemservice.uniepp.base.bo.EppSystemAction.logic.IEppSystemActionDao;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
@Configuration
public class EppSystemActionManager extends BusinessObjectManager
{
    public static EppSystemActionManager instance()
    {
        return instance(EppSystemActionManager.class);
    }

    @Bean
    public IEppSystemActionDao dao()
    {
        return new EppSystemActionDao();
    }
}
