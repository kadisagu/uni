/* $Id$ */
package ru.tandemservice.uniepp.component.workplan.WorkPlanTrajectoryPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;

/**
 * @author oleyba
 * @since 5/6/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));

        final Model model = this.getModel(component);
        component.createChildRegion("trajectoryRegion", new ComponentActivator(
                ru.tandemservice.uniepp.component.workplan.WorkPlanTrajectory.Model.COMPONENT,
                new ParametersMap().add("ids", model.getIds())
        ));
        if ( model.getEduPlanVersion()!= null) {
            component.createChildRegion("eduPlanVersionInfoRegion", new ComponentActivator(
                    ru.tandemservice.uniepp.component.eduplan.EduPlanVersionInfo.Model.COMPONENT,
                    new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getEduPlanVersion().getId())
            ));
        }
    }
}
