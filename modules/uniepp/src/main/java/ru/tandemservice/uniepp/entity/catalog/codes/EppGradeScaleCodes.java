package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Шкала оценок"
 * Имя сущности : eppGradeScale
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppGradeScaleCodes
{
    /** Константа кода (code) элемента : Пятибалльная (title) */
    String SCALE_5 = "scale5";
    /** Константа кода (code) элемента : Зачет-незачет (title) */
    String SCALE_2 = "scale2";

    Set<String> CODES = ImmutableSet.of(SCALE_5, SCALE_2);
}
