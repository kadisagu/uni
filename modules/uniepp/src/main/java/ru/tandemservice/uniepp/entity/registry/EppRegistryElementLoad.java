package ru.tandemservice.uniepp.entity.registry;

import java.util.Comparator;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementLoadGen;

/**
 * Нагрузка элемента реестра (всего)
 */
public class EppRegistryElementLoad extends EppRegistryElementLoadGen
{

    public static final Comparator<EppRegistryElementLoad> BY_TYPE_COMPARATOR = new Comparator<EppRegistryElementLoad>() {
        @Override public int compare(final EppRegistryElementLoad o1, final EppRegistryElementLoad o2) {
            final EppLoadType t1 = (null == o1 ? null : o1.getLoadType());
            final EppLoadType t2 = (null == o2 ? null : o2.getLoadType());
            return EppLoadType.COMPARATOR.compare(t1, t2);
        }
    };


    public EppRegistryElementLoad() {}
    public EppRegistryElementLoad(Double loadAsDouble) {
        this.setLoadAsDouble(loadAsDouble);
    }

    public Double getLoadAsDouble() {
        final long load = this.getLoad();
        return UniEppUtils.wrap(load < 0 ? null : load);
    }

    public void setLoadAsDouble(final Double value) {
        final Long load = UniEppUtils.unwrap(value);
        this.setLoad(null == load ? -1 : load.longValue());
    }



}
