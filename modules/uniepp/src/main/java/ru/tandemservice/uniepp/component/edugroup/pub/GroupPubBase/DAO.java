/* $Id$ */
package ru.tandemservice.uniepp.component.edugroup.pub.GroupPubBase;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;

/**
 * @author oleyba
 * @since 9/13/11
 */
public abstract class DAO extends UniBaseDao implements IDAO
{
    @Override
    public boolean prepare(final Model model)
    {
        final EppRealEduGroup eduGroup = model.getGroupHolder().refresh();
        if (null == eduGroup) { return false; }

        final OrgUnit orgUnit = model.getOrgUnitHolder().refresh();
        if (!this.checkAvailability(model, eduGroup, orgUnit)) {
            throw new ApplicationException("У Вас недостаточно прав для работы с учебной группой студентов.");
        }

        // все ок
        return true;
    }

    public abstract boolean checkAvailability(Model model, EppRealEduGroup eduGroup, OrgUnit orgUnit);

}
