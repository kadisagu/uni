/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.dto;

/**
 * @author azhebko
 * @since 28.10.2014
 */
public class ImEpvRegistryRowDTO extends ImEpvTermDistributedRowDTO
{
    private String _registryElementType;     // Тип элемента
    private String _registryElementOwner;     // Читающее подразделение

    public String getRegistryElementOwner() { return _registryElementOwner; }
    public void setRegistryElementOwner(String registryElementOwner) { _registryElementOwner = registryElementOwner; }

    public String getRegistryElementType() { return _registryElementType; }
    public void setRegistryElementType(String registryElementType) { _registryElementType = registryElementType; }

    @Override
    public String toString()
    {
        return "Строка элемент реестра: " + getRegistryElementType() + ", " + getRegistryElementOwner() + '\n' + super.toString();
    }
}