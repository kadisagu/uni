package ru.tandemservice.uniepp.component.settings.EducationalPlanning;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * 
 * @author nkokorina
 * @since 26.02.2010
 */

public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
}
