package ru.tandemservice.uniepp.entity.student.slot.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * МСРП по нагрузке
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStudentWpePartGen extends EntityBase
 implements INaturalIdentifiable<EppStudentWpePartGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart";
    public static final String ENTITY_NAME = "eppStudentWpePart";
    public static final int VERSION_HASH = 1033402784;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_WPE = "studentWpe";
    public static final String L_TYPE = "type";
    public static final String P_MODIFICATION_DATE = "modificationDate";
    public static final String P_REMOVAL_DATE = "removalDate";

    private EppStudentWorkPlanElement _studentWpe;     // МСРП
    private EppGroupType _type;     // Вид учебной группы
    private Date _modificationDate;     // Дата изменения
    private Date _removalDate;     // Дата утраты актуальности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return МСРП. Свойство не может быть null.
     */
    @NotNull
    public EppStudentWorkPlanElement getStudentWpe()
    {
        return _studentWpe;
    }

    /**
     * @param studentWpe МСРП. Свойство не может быть null.
     */
    public void setStudentWpe(EppStudentWorkPlanElement studentWpe)
    {
        dirty(_studentWpe, studentWpe);
        _studentWpe = studentWpe;
    }

    /**
     * @return Вид учебной группы. Свойство не может быть null.
     */
    @NotNull
    public EppGroupType getType()
    {
        return _type;
    }

    /**
     * @param type Вид учебной группы. Свойство не может быть null.
     */
    public void setType(EppGroupType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getModificationDate()
    {
        return _modificationDate;
    }

    /**
     * @param modificationDate Дата изменения. Свойство не может быть null.
     */
    public void setModificationDate(Date modificationDate)
    {
        dirty(_modificationDate, modificationDate);
        _modificationDate = modificationDate;
    }

    /**
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppStudentWpePartGen)
        {
            if (withNaturalIdProperties)
            {
                setStudentWpe(((EppStudentWpePart)another).getStudentWpe());
                setType(((EppStudentWpePart)another).getType());
            }
            setModificationDate(((EppStudentWpePart)another).getModificationDate());
            setRemovalDate(((EppStudentWpePart)another).getRemovalDate());
        }
    }

    public INaturalId<EppStudentWpePartGen> getNaturalId()
    {
        return new NaturalId(getStudentWpe(), getType());
    }

    public static class NaturalId extends NaturalIdBase<EppStudentWpePartGen>
    {
        private static final String PROXY_NAME = "EppStudentWpePartNaturalProxy";

        private Long _studentWpe;
        private Long _type;

        public NaturalId()
        {}

        public NaturalId(EppStudentWorkPlanElement studentWpe, EppGroupType type)
        {
            _studentWpe = ((IEntity) studentWpe).getId();
            _type = ((IEntity) type).getId();
        }

        public Long getStudentWpe()
        {
            return _studentWpe;
        }

        public void setStudentWpe(Long studentWpe)
        {
            _studentWpe = studentWpe;
        }

        public Long getType()
        {
            return _type;
        }

        public void setType(Long type)
        {
            _type = type;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppStudentWpePartGen.NaturalId) ) return false;

            EppStudentWpePartGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudentWpe(), that.getStudentWpe()) ) return false;
            if( !equals(getType(), that.getType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudentWpe());
            result = hashCode(result, getType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudentWpe());
            sb.append("/");
            sb.append(getType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStudentWpePartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStudentWpePart.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppStudentWpePart is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentWpe":
                    return obj.getStudentWpe();
                case "type":
                    return obj.getType();
                case "modificationDate":
                    return obj.getModificationDate();
                case "removalDate":
                    return obj.getRemovalDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentWpe":
                    obj.setStudentWpe((EppStudentWorkPlanElement) value);
                    return;
                case "type":
                    obj.setType((EppGroupType) value);
                    return;
                case "modificationDate":
                    obj.setModificationDate((Date) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentWpe":
                        return true;
                case "type":
                        return true;
                case "modificationDate":
                        return true;
                case "removalDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentWpe":
                    return true;
                case "type":
                    return true;
                case "modificationDate":
                    return true;
                case "removalDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentWpe":
                    return EppStudentWorkPlanElement.class;
                case "type":
                    return EppGroupType.class;
                case "modificationDate":
                    return Date.class;
                case "removalDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStudentWpePart> _dslPath = new Path<EppStudentWpePart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStudentWpePart");
    }
            

    /**
     * @return МСРП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart#getStudentWpe()
     */
    public static EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> studentWpe()
    {
        return _dslPath.studentWpe();
    }

    /**
     * @return Вид учебной группы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart#getType()
     */
    public static EppGroupType.Path<EppGroupType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart#getModificationDate()
     */
    public static PropertyPath<Date> modificationDate()
    {
        return _dslPath.modificationDate();
    }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    public static class Path<E extends EppStudentWpePart> extends EntityPath<E>
    {
        private EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> _studentWpe;
        private EppGroupType.Path<EppGroupType> _type;
        private PropertyPath<Date> _modificationDate;
        private PropertyPath<Date> _removalDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return МСРП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart#getStudentWpe()
     */
        public EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> studentWpe()
        {
            if(_studentWpe == null )
                _studentWpe = new EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement>(L_STUDENT_WPE, this);
            return _studentWpe;
        }

    /**
     * @return Вид учебной группы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart#getType()
     */
        public EppGroupType.Path<EppGroupType> type()
        {
            if(_type == null )
                _type = new EppGroupType.Path<EppGroupType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart#getModificationDate()
     */
        public PropertyPath<Date> modificationDate()
        {
            if(_modificationDate == null )
                _modificationDate = new PropertyPath<Date>(EppStudentWpePartGen.P_MODIFICATION_DATE, this);
            return _modificationDate;
        }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(EppStudentWpePartGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

        public Class getEntityClass()
        {
            return EppStudentWpePart.class;
        }

        public String getEntityName()
        {
            return "eppStudentWpePart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
