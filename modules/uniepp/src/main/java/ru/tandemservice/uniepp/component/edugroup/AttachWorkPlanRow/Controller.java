package ru.tandemservice.uniepp.component.edugroup.AttachWorkPlanRow;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;

/**
 * @author vdanilov
 */

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onClickApply(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().save(model);
        if (!UserContext.getInstance().getErrorCollector().hasErrors()) {
            this.deactivate(component);
        }
    }



}
