package ru.tandemservice.uniepp.entity.workplan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часть РУП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkPlanPartGen extends EntityBase
 implements INaturalIdentifiable<EppWorkPlanPartGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart";
    public static final String ENTITY_NAME = "eppWorkPlanPart";
    public static final int VERSION_HASH = -2039457822;
    private static IEntityMeta ENTITY_META;

    public static final String L_WORK_PLAN = "workPlan";
    public static final String P_NUMBER = "number";
    public static final String P_TOTAL_WEEKS = "totalWeeks";
    public static final String P_AUDIT_WEEKS = "auditWeeks";
    public static final String P_SELFWORK_WEEKS = "selfworkWeeks";

    private EppWorkPlan _workPlan;     // РУП
    private int _number;     // Номер части
    private int _totalWeeks;     // Общее количество недель (в части семестре)
    private int _auditWeeks;     // Количество недель аудиторного обучения (в части семестре)
    private int _selfworkWeeks;     // Количество недель самостоятельного обучения (в части семестре)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return РУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkPlan getWorkPlan()
    {
        return _workPlan;
    }

    /**
     * @param workPlan РУП. Свойство не может быть null.
     */
    public void setWorkPlan(EppWorkPlan workPlan)
    {
        dirty(_workPlan, workPlan);
        _workPlan = workPlan;
    }

    /**
     * @return Номер части. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер части. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Общее количество недель (в части семестре). Свойство не может быть null.
     */
    @NotNull
    public int getTotalWeeks()
    {
        return _totalWeeks;
    }

    /**
     * @param totalWeeks Общее количество недель (в части семестре). Свойство не может быть null.
     */
    public void setTotalWeeks(int totalWeeks)
    {
        dirty(_totalWeeks, totalWeeks);
        _totalWeeks = totalWeeks;
    }

    /**
     * @return Количество недель аудиторного обучения (в части семестре). Свойство не может быть null.
     */
    @NotNull
    public int getAuditWeeks()
    {
        return _auditWeeks;
    }

    /**
     * @param auditWeeks Количество недель аудиторного обучения (в части семестре). Свойство не может быть null.
     */
    public void setAuditWeeks(int auditWeeks)
    {
        dirty(_auditWeeks, auditWeeks);
        _auditWeeks = auditWeeks;
    }

    /**
     * @return Количество недель самостоятельного обучения (в части семестре). Свойство не может быть null.
     */
    @NotNull
    public int getSelfworkWeeks()
    {
        return _selfworkWeeks;
    }

    /**
     * @param selfworkWeeks Количество недель самостоятельного обучения (в части семестре). Свойство не может быть null.
     */
    public void setSelfworkWeeks(int selfworkWeeks)
    {
        dirty(_selfworkWeeks, selfworkWeeks);
        _selfworkWeeks = selfworkWeeks;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppWorkPlanPartGen)
        {
            if (withNaturalIdProperties)
            {
                setWorkPlan(((EppWorkPlanPart)another).getWorkPlan());
                setNumber(((EppWorkPlanPart)another).getNumber());
            }
            setTotalWeeks(((EppWorkPlanPart)another).getTotalWeeks());
            setAuditWeeks(((EppWorkPlanPart)another).getAuditWeeks());
            setSelfworkWeeks(((EppWorkPlanPart)another).getSelfworkWeeks());
        }
    }

    public INaturalId<EppWorkPlanPartGen> getNaturalId()
    {
        return new NaturalId(getWorkPlan(), getNumber());
    }

    public static class NaturalId extends NaturalIdBase<EppWorkPlanPartGen>
    {
        private static final String PROXY_NAME = "EppWorkPlanPartNaturalProxy";

        private Long _workPlan;
        private int _number;

        public NaturalId()
        {}

        public NaturalId(EppWorkPlan workPlan, int number)
        {
            _workPlan = ((IEntity) workPlan).getId();
            _number = number;
        }

        public Long getWorkPlan()
        {
            return _workPlan;
        }

        public void setWorkPlan(Long workPlan)
        {
            _workPlan = workPlan;
        }

        public int getNumber()
        {
            return _number;
        }

        public void setNumber(int number)
        {
            _number = number;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppWorkPlanPartGen.NaturalId) ) return false;

            EppWorkPlanPartGen.NaturalId that = (NaturalId) o;

            if( !equals(getWorkPlan(), that.getWorkPlan()) ) return false;
            if( !equals(getNumber(), that.getNumber()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getWorkPlan());
            result = hashCode(result, getNumber());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getWorkPlan());
            sb.append("/");
            sb.append(getNumber());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkPlanPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkPlanPart.class;
        }

        public T newInstance()
        {
            return (T) new EppWorkPlanPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "workPlan":
                    return obj.getWorkPlan();
                case "number":
                    return obj.getNumber();
                case "totalWeeks":
                    return obj.getTotalWeeks();
                case "auditWeeks":
                    return obj.getAuditWeeks();
                case "selfworkWeeks":
                    return obj.getSelfworkWeeks();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "workPlan":
                    obj.setWorkPlan((EppWorkPlan) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "totalWeeks":
                    obj.setTotalWeeks((Integer) value);
                    return;
                case "auditWeeks":
                    obj.setAuditWeeks((Integer) value);
                    return;
                case "selfworkWeeks":
                    obj.setSelfworkWeeks((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "workPlan":
                        return true;
                case "number":
                        return true;
                case "totalWeeks":
                        return true;
                case "auditWeeks":
                        return true;
                case "selfworkWeeks":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "workPlan":
                    return true;
                case "number":
                    return true;
                case "totalWeeks":
                    return true;
                case "auditWeeks":
                    return true;
                case "selfworkWeeks":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "workPlan":
                    return EppWorkPlan.class;
                case "number":
                    return Integer.class;
                case "totalWeeks":
                    return Integer.class;
                case "auditWeeks":
                    return Integer.class;
                case "selfworkWeeks":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkPlanPart> _dslPath = new Path<EppWorkPlanPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkPlanPart");
    }
            

    /**
     * @return РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart#getWorkPlan()
     */
    public static EppWorkPlan.Path<EppWorkPlan> workPlan()
    {
        return _dslPath.workPlan();
    }

    /**
     * @return Номер части. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Общее количество недель (в части семестре). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart#getTotalWeeks()
     */
    public static PropertyPath<Integer> totalWeeks()
    {
        return _dslPath.totalWeeks();
    }

    /**
     * @return Количество недель аудиторного обучения (в части семестре). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart#getAuditWeeks()
     */
    public static PropertyPath<Integer> auditWeeks()
    {
        return _dslPath.auditWeeks();
    }

    /**
     * @return Количество недель самостоятельного обучения (в части семестре). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart#getSelfworkWeeks()
     */
    public static PropertyPath<Integer> selfworkWeeks()
    {
        return _dslPath.selfworkWeeks();
    }

    public static class Path<E extends EppWorkPlanPart> extends EntityPath<E>
    {
        private EppWorkPlan.Path<EppWorkPlan> _workPlan;
        private PropertyPath<Integer> _number;
        private PropertyPath<Integer> _totalWeeks;
        private PropertyPath<Integer> _auditWeeks;
        private PropertyPath<Integer> _selfworkWeeks;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart#getWorkPlan()
     */
        public EppWorkPlan.Path<EppWorkPlan> workPlan()
        {
            if(_workPlan == null )
                _workPlan = new EppWorkPlan.Path<EppWorkPlan>(L_WORK_PLAN, this);
            return _workPlan;
        }

    /**
     * @return Номер части. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(EppWorkPlanPartGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Общее количество недель (в части семестре). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart#getTotalWeeks()
     */
        public PropertyPath<Integer> totalWeeks()
        {
            if(_totalWeeks == null )
                _totalWeeks = new PropertyPath<Integer>(EppWorkPlanPartGen.P_TOTAL_WEEKS, this);
            return _totalWeeks;
        }

    /**
     * @return Количество недель аудиторного обучения (в части семестре). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart#getAuditWeeks()
     */
        public PropertyPath<Integer> auditWeeks()
        {
            if(_auditWeeks == null )
                _auditWeeks = new PropertyPath<Integer>(EppWorkPlanPartGen.P_AUDIT_WEEKS, this);
            return _auditWeeks;
        }

    /**
     * @return Количество недель самостоятельного обучения (в части семестре). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart#getSelfworkWeeks()
     */
        public PropertyPath<Integer> selfworkWeeks()
        {
            if(_selfworkWeeks == null )
                _selfworkWeeks = new PropertyPath<Integer>(EppWorkPlanPartGen.P_SELFWORK_WEEKS, this);
            return _selfworkWeeks;
        }

        public Class getEntityClass()
        {
            return EppWorkPlanPart.class;
        }

        public String getEntityName()
        {
            return "eppWorkPlanPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
