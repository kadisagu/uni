package ru.tandemservice.uniepp.component.eduplan.row.AddEdit;

import java.util.Collection;
import java.util.Map;

import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

/**
 * @author vdanilov
 */
public abstract class LoadAddEditModel<T extends EppEpvTermDistributedRow> extends BaseAddEditModel<T>
{

    private Map<String, EppLoadType> loadTypeMap;
    public Map<String, EppLoadType> getLoadTypeMap() { return this.loadTypeMap; }
    public void setLoadTypeMap(Map<String, EppLoadType> loadTypeMap) { this.loadTypeMap = loadTypeMap; }

    public Collection<EppALoadType> getEppALoadTypes() { return EppLoadTypeUtils.getALoadTypeList(getLoadTypeMap()); }
    public Collection<EppELoadType> getEppELoadTypes() { return EppLoadTypeUtils.getELoadTypeList(getLoadTypeMap()); }

    private EppLoadType currentLoadType;
    public EppLoadType getCurrentLoadType() { return this.currentLoadType; }
    public void setCurrentLoadType(final EppLoadType currentLoadType) { this.currentLoadType = currentLoadType; }

    private Map<String, Double> loadMap;
    public Map<String, Double> getLoadMap() { return this.loadMap; }
    public void setLoadMap(final Map<String, Double> loadMap) { this.loadMap = loadMap; }

    public Double getCurrentLoad() { return loadMap.get(this.getCurrentLoadType().getFullCode()); }
    public void setCurrentLoad(Double load) { loadMap.put(this.getCurrentLoadType().getFullCode(), load); }
}
