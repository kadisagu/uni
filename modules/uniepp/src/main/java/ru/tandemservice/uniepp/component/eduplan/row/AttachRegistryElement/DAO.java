package ru.tandemservice.uniepp.component.eduplan.row.AttachRegistryElement;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.shared.person.base.bo.Person.util.FastStringFinder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.Range;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.EppEpvRegElWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.ui.EppRegistryElementSelectModel;

import java.util.*;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author zhuj
 */
public class DAO extends UniBaseDao implements IDAO
{

    public static final double EPS = 0.3;
    public static final int LEV = 3;

    @Override
    public void prepare(final Model model)
    {
        model.setControlActionMap(EppEduPlanVersionDataDAO.getActionTypeMap());
        model.setLoadTypeMap(EppEduPlanVersionDataDAO.getLoadTypeMap());

        Debug.begin("prepare.list");
        try {

            final FastStringFinder.Node titleSearchTree = new FastStringFinder.Node(0);
            final Range<Integer> partRange = new Range<>();
            final Range<Long> sizeRange = new Range<>();

            // заполняем данные на основе выбранных строк
            {
                final List<Object[]> rows = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "rr")
                .column(property(EppEpvRegistryRow.owner().id().fromAlias("rr")))
                .column(property("rr.id"))
                .where(in(property("rr.id"), model.getIds()))
                .order(property(EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().number().fromAlias("rr")))
                .order(property(EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().programForm().code().fromAlias("rr")))
                .order(property(EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().developCondition().code().fromAlias("rr")))
                .order(property(EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().developGrid().code().fromAlias("rr")))
                .createStatement(this.getSession()).list();

                final Map<Long, IEppEpvBlockWrapper> blockWrapperMap = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(
                    new HashSet<>(CollectionUtils.collect(rows, input -> (Long) input[0])),
                    true
                );

                final List<IEppEpvRowWrapper> wrappers = new ArrayList<>(rows.size());

                for (final Object[] row: rows) {
                    final IEppEpvRowWrapper w = blockWrapperMap.get(row[0]).getRowMap().get(row[1]);
                    wrappers.add(w);

                    final Set<Integer> activeTermSet = w.getActiveTermSet();
                    final EppEpvRegistryRow regRlRow = (EppEpvRegistryRow)w.getRow();

                    /* поиск */ {
                        partRange.add(activeTermSet.size());
                        sizeRange.add(regRlRow.getHoursTotal());
                        titleSearchTree.add(key(regRlRow.getTitle()), 0, 0);
                    }
                }

                model.getData().setup(wrappers);
                {
                    final List<Integer> partNumbers = new ArrayList<>(partRange.getMax());
                    for (int i=1;i<=Math.max(1, partRange.getMax());i++) { partNumbers.add(i); }
                    model.setDataPartNumbers(partNumbers);
                }

                if (wrappers.isEmpty()) {
                    throw new IllegalStateException();
                }
            }

            // находим подходящие строки (по названию близкие хотябы одной из строк)
            {
                final Set<Long> ids = new HashSet<>();
                final List<Object[]> rows = new DQLSelectBuilder()
                .fromEntity(EppRegistryElement.class, "rel")
                .column(property("rel.id"))
                .column(property("rel", EppRegistryElement.P_TITLE))
                .column(property("rel", EppRegistryElement.P_SHORT_TITLE))
                .joinEntity("rel", DQLJoinType.left, EppEpvRegistryRow.class, "rr", eq(property("rel"), property("rr", EppEpvRegistryRow.L_REGISTRY_ELEMENT)))
                .column(property("rr", EppEpvRegistryRow.P_TITLE))
                .where(eq(property(EppRegistryElement.owner().id().fromAlias("rel")), value(model.getOrgUnitId())))

                /* совпадение числа часов (с окрестоностью) */
                .where(and(
                    (null == sizeRange.getMin() ? null : ge(property(EppRegistryElement.size().fromAlias("rel")), value((long)(sizeRange.getMin()*(1-EPS))))),
                    (null == sizeRange.getMax() ? null : le(property(EppRegistryElement.size().fromAlias("rel")), value((long)(sizeRange.getMax()*(1+EPS)))))
                ))

                /* точное совпадение числа частей */
                .where(and(
                    (null == partRange.getMin() ? null : ge(property(EppRegistryElement.parts().fromAlias("rel")), value(partRange.getMin()))),
                    (null == partRange.getMax() ? null : le(property(EppRegistryElement.parts().fromAlias("rel")), value(partRange.getMax())))
                ))

                .createStatement(this.getSession()).list();

                for (final Object[] row: rows) {
                    if (!ids.contains(row[0])) {
                        for (int i=1;i<row.length;i++) {
                            final String trimToNull = StringUtils.trimToNull((String)row[i]);
                            if ((null != trimToNull) && titleSearchTree.contains(key(trimToNull), 0, LEV, -1 /* не он сам */)) {
                                ids.add((Long)row[0]);
                                continue;
                            }
                        }
                    }
                }

                // если когда-нибудь придется выводить строки по убыванию реливантности - используем этот код
                {
                    // для каждой строки считаем невязку
                    final Collection<IEppEpvRowWrapper> wpvWrappers = model.getData().getRows();
                    final Map<IEppRegElWrapper, Double> distanceMap = SafeMap.get(new SafeMap.Callback<IEppRegElWrapper, Double>() {

                        private double sq(final double ...X) {
                            double result = 0;
                            for (final double x: X) { result += x*x; }

                            int size = wpvWrappers.size();
                            return result / (size * size);
                        }

                        @Override public Double resolve(final IEppRegElWrapper key) {
                            final String title = key.getItem().getTitle();
                            final int parts = key.getItem().getParts();

                            double title_distance = 0;
                            double part_distance = 0;
                            double type_distance = 0;
                            for (final IEppEpvRowWrapper row: wpvWrappers) {
                                title_distance += StringUtils.getLevenshteinDistance(title, row.getRow().getTitle()) / (double)title.length();
                                part_distance += Math.abs(parts - row.getActiveTermSet().size());
                                type_distance += 0; /* TODO */
                            }

                            return this.sq(title_distance, part_distance*10, type_distance);
                        }
                    });

                    final List<ViewWrapper<IEppRegElWrapper>> elements = ViewWrapper.getPatchedList(
                        IEppRegistryDAO.instance.get().getRegistryElementDataMap(ids).values()
                    );

                    Collections.sort(elements, (o1, o2) -> Double.compare(distanceMap.get(o1.getEntity()), distanceMap.get(o2.getEntity())));
                    model.getResult().setup(elements.subList(0, Math.min(10, elements.size())));

                    final Map<Long, Double> modelDistanceMap = new HashMap<>(distanceMap.size());
                    for (final ViewWrapper<IEppRegElWrapper> e: model.getResult().getRows()) { modelDistanceMap.put(e.getId(), distanceMap.get(e.getEntity())); }
                    model.setDistanceMap(modelDistanceMap);
                }

                // строк должно быть не так много - поэтому in(ids)
                model.setRegistryElementSource(new EppRegistryElementSelectModel<EppRegistryElement>(EppRegistryElement.class, "state.title") {
                    @Override protected DQLSelectBuilder query(final String alias, final String filter) {

                        if (Model.SOURCE_ALL.equals(model.getRegistryElementSourceFilter())) {
                            return super.query(alias, filter)
                            .where(ne(property(EppRegistryElement.state().code().fromAlias(alias)), value(EppState.STATE_ARCHIVED)))
                            .where(eq(property(EppRegistryElement.owner().id().fromAlias(alias)), value(model.getOrgUnitId())))
                            ;
                        }

                        if (Model.SOURCE_SIM.equals(model.getRegistryElementSourceFilter())) {
                            return super.query(alias, filter)
                            .where(in(property(alias, "id"), ids))
                            .where(ne(property(EppRegistryElement.state().code().fromAlias(alias)), value(EppState.STATE_ARCHIVED)))
                            .where(eq(property(EppRegistryElement.owner().id().fromAlias(alias)), value(model.getOrgUnitId())))
                            ;
                        }

                        return null;
                    }
                });

            }
        } finally {
            Debug.end();
        }
    }

    private static Pattern PATTERN_NON_CHARS = Pattern.compile("[._/()<>\"«»'\\[\\]]");
    private static Pattern PATTERN_SPACES = Pattern.compile("[ ]+");

    public static int[] key(String title) {
        title = StringUtils.trimToEmpty(title).toLowerCase();
        title = PATTERN_NON_CHARS.matcher(title).replaceAll(" ");
        title = PATTERN_SPACES.matcher(title).replaceAll(" ");
        title = StringUtils.trimToEmpty(title);
        return FastStringFinder.direct_parse(title);
    }

    @Override
    public EppRegistryElement doSelect(final Model model) {
        final EppRegistryElement regElement = model.getRegistryElement();
        if (null == regElement) { throw new ApplicationException("Не выбран элемент реестра"); }
        attach(model, regElement);
        return regElement;
    }

    @Override
    public EppRegistryElement doCreate(final Model model) {
        final IEppEpvRowWrapper epvRow = model.getSelectedRow();
        if (null == epvRow) { throw new ApplicationException("Не выбрана строка УП(в)"); }
        if (!EppEpvRegistryRow.class.isInstance(epvRow.getRow())) { throw new IllegalStateException(); }
        final EppRegistryElement regElement = IEppRegistryDAO.instance.get().doCreateRegistryElement(new EppEpvRegElWrapper(epvRow), null, null, null);
        attach(model, regElement);
        return regElement;
    }

    protected void attach(final Model model, final EppRegistryElement value) {
        try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler())) {
            final List<EppEpvRegistryRow> rows = this.getList(EppEpvRegistryRow.class, EppEpvRegistryRow.id(), model.getIds());
            for (final EppEpvRegistryRow row: rows) {
                row.setRegistryElement(value);
                this.saveOrUpdate(row);
            }
        }
    }

}