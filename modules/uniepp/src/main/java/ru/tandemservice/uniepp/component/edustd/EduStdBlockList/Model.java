package ru.tandemservice.uniepp.component.edustd.EduStdBlockList;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

@State( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "element.id") })
public class Model
{
    private EppStateEduStandard element = new EppStateEduStandard();
    private List<BlockWrapper> blockList = Collections.emptyList();
    private BlockWrapper block;
    private LinkedHashMap<String, String> code2shortTitle = new LinkedHashMap<String, String>();

    public EppStateEduStandard getElement()
    {
        return this.element;
    }

    public void setElement(final EppStateEduStandard element)
    {
        this.element = element;
    }

    public List<BlockWrapper> getBlockList()
    {
        return this.blockList;
    }

    public void setBlockList(final List<BlockWrapper> blockList)
    {
        this.blockList = blockList;
    }

    public BlockWrapper getBlock()
    {
        return this.block;
    }

    public void setBlock(final BlockWrapper block)
    {
        this.block = block;
    }

    public LinkedHashMap<String, String> getCode2shortTitle()
    {
        return this.code2shortTitle;
    }

    public void setCode2shortTitle(final LinkedHashMap<String, String> code2shortTitle)
    {
        this.code2shortTitle = code2shortTitle;
    }
}
