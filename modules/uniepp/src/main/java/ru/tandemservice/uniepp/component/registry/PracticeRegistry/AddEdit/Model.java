package ru.tandemservice.uniepp.component.registry.PracticeRegistry.AddEdit;

import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;

/**
 * @author vdanilov
 */
public class Model extends ru.tandemservice.uniepp.component.registry.base.AddEdit.Model<EppRegistryPractice> {
    @Override public Class<EppRegistryPractice> getElementClass() {
        return EppRegistryPractice.class;
    }

    @Override
    public void setupElementDefaultLoad() {
        super.setupElementDefaultLoad();
        this.getElement().setWeeksAsDouble(this.getDefaultLoadMap().get(EppLoadType.FULL_CODE_WEEKS));
    }
}
