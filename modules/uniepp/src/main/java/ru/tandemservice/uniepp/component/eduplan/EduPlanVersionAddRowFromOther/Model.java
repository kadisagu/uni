/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddRowFromOther;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.tandemframework.core.CoreUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author nkokorina
 * Created on: 20.09.2010
 */

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "blockId", required = true) })
@SuppressWarnings("unchecked")
public class Model
{
    private Long blockId;
    public Long getBlockId() { return this.blockId; }
    public void setBlockId(final Long blockId) { this.blockId = blockId; }


    public static final Long BLOCK_SOURCE_ALL = 1L;
    public static final Long BLOCK_SOURCE_SAME_PROGRAM_KIND = 2L;
    public static final Long BLOCK_SOURCE_SAME_SUBJECT = 3L;
    public static final Long BLOCK_SOURCE_SAME_SPEC = 4L;

    private final SelectModel<IdentifiableWrapper> blockSourceModel = new SelectModel<IdentifiableWrapper>(Arrays.asList(
            new IdentifiableWrapper(Model.BLOCK_SOURCE_ALL, "Любые УП(в) и блоки"),
            new IdentifiableWrapper(Model.BLOCK_SOURCE_SAME_PROGRAM_KIND, "Текущий вид образовательной программы"),
            new IdentifiableWrapper(Model.BLOCK_SOURCE_SAME_SUBJECT, "Текущее направление, специальность, профессия"),
            new IdentifiableWrapper(Model.BLOCK_SOURCE_SAME_SPEC, "Текущая направленность")
    ));
    public SelectModel<IdentifiableWrapper> getBlockSourceModel() { return this.blockSourceModel; }


    private final SelectModel<EppEduPlanVersionBlock> blockModel = new SelectModel<EppEduPlanVersionBlock>();
    public SelectModel<EppEduPlanVersionBlock> getBlockModel() { return this.blockModel; }

    private StaticListDataSource<IEppEpvRowWrapper> rowDataSource = new StaticListDataSource<IEppEpvRowWrapper>();
    public StaticListDataSource<IEppEpvRowWrapper> getRowDataSource() { return this.rowDataSource; }
    public void setRowDataSource(final StaticListDataSource<IEppEpvRowWrapper> rowDataSource) { this.rowDataSource = rowDataSource; }



    public static final Long OVERRIDE_NO = 0L;
    public static final Long OVERRIDE_YES = 1L;

    private final SelectModel<IdentifiableWrapper> overrideModel = new SelectModel<IdentifiableWrapper>(Arrays.asList(
            new IdentifiableWrapper(Model.OVERRIDE_NO, "Без замещения"),
            new IdentifiableWrapper(Model.OVERRIDE_YES, "С замещением")
    ));

    public SelectModel<IdentifiableWrapper> getOverrideModel() { return this.overrideModel; }


    public boolean isSameGrid() {
        // интерфейсная штука, если есть ошибка (ее быть не должно) - то считаем, что различаются
        try { return this.isSameGrid(this.getSourceEduPlanVersion(), this.getTargetEduPlanVersion()); }
        catch (final Throwable t) { return false; }
    }

    public String getSourcePlanOverloadTerms() {
        final DevelopGrid sourceDevelopGrid = this.getSourceEduPlanVersion().getEduPlan().getDevelopGrid();
        final DevelopGrid targetDevelopGrid = this.getTargetEduPlanVersion().getEduPlan().getDevelopGrid();

        final int sourceTerms = DataAccessServices
                .dao().getCount(DevelopGridTerm.class, DevelopGridTerm.developGrid().s(), sourceDevelopGrid);
        final int targetTerms = DataAccessServices.
                dao().getCount(DevelopGridTerm.class, DevelopGridTerm.developGrid().s(), targetDevelopGrid);
        if (sourceTerms <= targetTerms)
            return null;

        List<String> overloadTerms = new ArrayList<>();
        for (int i = targetTerms + 1; i <= sourceTerms; i++) {
            overloadTerms.add(String.valueOf(i));
        }

        return "Распределение нагрузки и форм контроля, реализуемых в " + (overloadTerms.size() > 1 ? "семестрах " : "семестре ")
                + overloadTerms.stream().collect(Collectors.joining(", "))
                + ", будет скопировано со смещением семестра.";
    }

    public EppEduPlanVersion getTargetEduPlanVersion() {
        // интерфейсная штука, если есть ошибка (ее быть не должно) - то считаем, что null
        try { return IUniBaseDao.instance.get().getNotNull(EppEduPlanVersionBlock.class, this.getBlockId()).getEduPlanVersion(); }
        catch (final Throwable t) { return null; }
    }

    public EppEduPlanVersion getSourceEduPlanVersion() {
        // интерфейсная штука, если есть ошибка (ее быть не должно) - то считаем, что null
        try { return IUniBaseDao.instance.get().getNotNull(EppEduPlanVersionBlock.class, this.getBlockModel().getValue().getId()).getEduPlanVersion(); }
        catch (final Throwable t) { return null; }
    }

    protected boolean isSameGrid(final EppEduPlanVersion a, final EppEduPlanVersion b) {
        return CoreUtils.safeEqual(a.getEduPlan().getDevelopGrid(), b.getEduPlan().getDevelopGrid());
    }

}
