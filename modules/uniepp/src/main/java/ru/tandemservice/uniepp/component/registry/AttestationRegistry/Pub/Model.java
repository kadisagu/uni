package ru.tandemservice.uniepp.component.registry.AttestationRegistry.Pub;

import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;

/**
 * 
 * @author nkokorina
 *
 */

public class Model extends ru.tandemservice.uniepp.component.registry.base.Pub.Model<EppRegistryAttestation>
{
    @Override public Class<EppRegistryAttestation> getElementClass() { return EppRegistryAttestation.class; }
    @Override public String getPermissionPrefix() { return "eppAttestation"; }
}
