package ru.tandemservice.uniepp.entity.workplan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка РУП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkPlanRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow";
    public static final String ENTITY_NAME = "eppWorkPlanRow";
    public static final int VERSION_HASH = -78472382;
    private static IEntityMeta ENTITY_META;

    public static final String L_WORK_PLAN = "workPlan";
    public static final String L_KIND = "kind";
    public static final String P_NUMBER = "number";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_APPENDIX = "titleAppendix";
    public static final String P_COMMENT = "comment";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_NEED_RETAKE = "needRetake";
    public static final String P_DISPLAYABLE_TITLE = "displayableTitle";
    public static final String P_TITLE_WITH_INDEX = "titleWithIndex";

    private EppWorkPlanBase _workPlan;     // РУП
    private EppWorkPlanRowKind _kind;     // Вид дисциплины
    private String _number;     // Номер (индекс)
    private String _title;     // Название в РУП
    private String _titleAppendix;     // Дополнение к названию
    private String _comment;     // Комментарий
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private Boolean _needRetake;     // Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return РУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkPlanBase getWorkPlan()
    {
        return _workPlan;
    }

    /**
     * @param workPlan РУП. Свойство не может быть null.
     */
    public void setWorkPlan(EppWorkPlanBase workPlan)
    {
        dirty(_workPlan, workPlan);
        _workPlan = workPlan;
    }

    /**
     * @return Вид дисциплины. Свойство не может быть null.
     */
    @NotNull
    public EppWorkPlanRowKind getKind()
    {
        return _kind;
    }

    /**
     * @param kind Вид дисциплины. Свойство не может быть null.
     */
    public void setKind(EppWorkPlanRowKind kind)
    {
        dirty(_kind, kind);
        _kind = kind;
    }

    /**
     * @return Номер (индекс).
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер (индекс).
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Название в РУП. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название в РУП. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Дополнение к названию.
     */
    @Length(max=255)
    public String getTitleAppendix()
    {
        return _titleAppendix;
    }

    /**
     * @param titleAppendix Дополнение к названию.
     */
    public void setTitleAppendix(String titleAppendix)
    {
        dirty(_titleAppendix, titleAppendix);
        _titleAppendix = titleAppendix;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Дата начала.
     */
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     * null  - обычное освоение: необходимо изучать дисциплину и сдавать контрольные мероприятия, как обычно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение).
     */
    public Boolean getNeedRetake()
    {
        return _needRetake;
    }

    /**
     * @param needRetake Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение).
     */
    public void setNeedRetake(Boolean needRetake)
    {
        dirty(_needRetake, needRetake);
        _needRetake = needRetake;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppWorkPlanRowGen)
        {
            setWorkPlan(((EppWorkPlanRow)another).getWorkPlan());
            setKind(((EppWorkPlanRow)another).getKind());
            setNumber(((EppWorkPlanRow)another).getNumber());
            setTitle(((EppWorkPlanRow)another).getTitle());
            setTitleAppendix(((EppWorkPlanRow)another).getTitleAppendix());
            setComment(((EppWorkPlanRow)another).getComment());
            setBeginDate(((EppWorkPlanRow)another).getBeginDate());
            setEndDate(((EppWorkPlanRow)another).getEndDate());
            setNeedRetake(((EppWorkPlanRow)another).getNeedRetake());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkPlanRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkPlanRow.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppWorkPlanRow is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "workPlan":
                    return obj.getWorkPlan();
                case "kind":
                    return obj.getKind();
                case "number":
                    return obj.getNumber();
                case "title":
                    return obj.getTitle();
                case "titleAppendix":
                    return obj.getTitleAppendix();
                case "comment":
                    return obj.getComment();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "needRetake":
                    return obj.getNeedRetake();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "workPlan":
                    obj.setWorkPlan((EppWorkPlanBase) value);
                    return;
                case "kind":
                    obj.setKind((EppWorkPlanRowKind) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "titleAppendix":
                    obj.setTitleAppendix((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "needRetake":
                    obj.setNeedRetake((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "workPlan":
                        return true;
                case "kind":
                        return true;
                case "number":
                        return true;
                case "title":
                        return true;
                case "titleAppendix":
                        return true;
                case "comment":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "needRetake":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "workPlan":
                    return true;
                case "kind":
                    return true;
                case "number":
                    return true;
                case "title":
                    return true;
                case "titleAppendix":
                    return true;
                case "comment":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "needRetake":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "workPlan":
                    return EppWorkPlanBase.class;
                case "kind":
                    return EppWorkPlanRowKind.class;
                case "number":
                    return String.class;
                case "title":
                    return String.class;
                case "titleAppendix":
                    return String.class;
                case "comment":
                    return String.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "needRetake":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkPlanRow> _dslPath = new Path<EppWorkPlanRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkPlanRow");
    }
            

    /**
     * @return РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getWorkPlan()
     */
    public static EppWorkPlanBase.Path<EppWorkPlanBase> workPlan()
    {
        return _dslPath.workPlan();
    }

    /**
     * @return Вид дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getKind()
     */
    public static EppWorkPlanRowKind.Path<EppWorkPlanRowKind> kind()
    {
        return _dslPath.kind();
    }

    /**
     * @return Номер (индекс).
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Название в РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Дополнение к названию.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getTitleAppendix()
     */
    public static PropertyPath<String> titleAppendix()
    {
        return _dslPath.titleAppendix();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Дата начала.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     * null  - обычное освоение: необходимо изучать дисциплину и сдавать контрольные мероприятия, как обычно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение).
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getNeedRetake()
     */
    public static PropertyPath<Boolean> needRetake()
    {
        return _dslPath.needRetake();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getDisplayableTitle()
     */
    public static SupportedPropertyPath<String> displayableTitle()
    {
        return _dslPath.displayableTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getTitleWithIndex()
     */
    public static SupportedPropertyPath<String> titleWithIndex()
    {
        return _dslPath.titleWithIndex();
    }

    public static class Path<E extends EppWorkPlanRow> extends EntityPath<E>
    {
        private EppWorkPlanBase.Path<EppWorkPlanBase> _workPlan;
        private EppWorkPlanRowKind.Path<EppWorkPlanRowKind> _kind;
        private PropertyPath<String> _number;
        private PropertyPath<String> _title;
        private PropertyPath<String> _titleAppendix;
        private PropertyPath<String> _comment;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Boolean> _needRetake;
        private SupportedPropertyPath<String> _displayableTitle;
        private SupportedPropertyPath<String> _titleWithIndex;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getWorkPlan()
     */
        public EppWorkPlanBase.Path<EppWorkPlanBase> workPlan()
        {
            if(_workPlan == null )
                _workPlan = new EppWorkPlanBase.Path<EppWorkPlanBase>(L_WORK_PLAN, this);
            return _workPlan;
        }

    /**
     * @return Вид дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getKind()
     */
        public EppWorkPlanRowKind.Path<EppWorkPlanRowKind> kind()
        {
            if(_kind == null )
                _kind = new EppWorkPlanRowKind.Path<EppWorkPlanRowKind>(L_KIND, this);
            return _kind;
        }

    /**
     * @return Номер (индекс).
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppWorkPlanRowGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Название в РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppWorkPlanRowGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Дополнение к названию.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getTitleAppendix()
     */
        public PropertyPath<String> titleAppendix()
        {
            if(_titleAppendix == null )
                _titleAppendix = new PropertyPath<String>(EppWorkPlanRowGen.P_TITLE_APPENDIX, this);
            return _titleAppendix;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EppWorkPlanRowGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Дата начала.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EppWorkPlanRowGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EppWorkPlanRowGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     * null  - обычное освоение: необходимо изучать дисциплину и сдавать контрольные мероприятия, как обычно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение).
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getNeedRetake()
     */
        public PropertyPath<Boolean> needRetake()
        {
            if(_needRetake == null )
                _needRetake = new PropertyPath<Boolean>(EppWorkPlanRowGen.P_NEED_RETAKE, this);
            return _needRetake;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getDisplayableTitle()
     */
        public SupportedPropertyPath<String> displayableTitle()
        {
            if(_displayableTitle == null )
                _displayableTitle = new SupportedPropertyPath<String>(EppWorkPlanRowGen.P_DISPLAYABLE_TITLE, this);
            return _displayableTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow#getTitleWithIndex()
     */
        public SupportedPropertyPath<String> titleWithIndex()
        {
            if(_titleWithIndex == null )
                _titleWithIndex = new SupportedPropertyPath<String>(EppWorkPlanRowGen.P_TITLE_WITH_INDEX, this);
            return _titleWithIndex;
        }

        public Class getEntityClass()
        {
            return EppWorkPlanRow.class;
        }

        public String getEntityName()
        {
            return "eppWorkPlanRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDisplayableTitle();

    public abstract String getTitleWithIndex();
}
