package ru.tandemservice.uniepp.entity.plan.data;

import org.apache.commons.collections15.Predicate;

import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.IEppNumberRow;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvGroupReRowGen;

/**
 * Запись версии УП (группа дисциплин)
 */
public class EppEpvGroupReRow extends EppEpvGroupReRowGen implements IEppNumberRow
{
    public static final Predicate<IEppEpvRow> PARENT_PREDICATE = IEppEduPlanVersionDataDAO.PREDICATE_HIERARCHY_ROW;

    public EppEpvGroupReRow() {
        super();
    }
    public EppEpvGroupReRow(final EppEduPlanVersionBlock block) {
        this();
        this.setOwner(block);
    }

    @Override public int getComparatorClassIndex() { return 2; }
    @Override public String getComparatorString() { return this.getSelfIndexPart(); }
    @Override public String getSelfIndexPart() { return IEppNumberRow.NUMBER_FORMATTER.format(this); }

    @Override public String getRowType() { return "Гр"; }
    @Override public String getDisplayableTitle() { return this.getTitle() /*+" ("+getRowType()+")"*/; }

    @Override public EppEpvHierarchyRow getHierarhyParent() { return this.getParent(); }
    @Override public void setHierarhyParent(final EppEpvRow parent) { this.setParent((EppEpvHierarchyRow) parent); }
}