package ru.tandemservice.uniepp.component.edugroup.list.GroupList;

import org.apache.commons.collections15.Factory;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow.IEppRealEduGroupRowDescription;

/**
 * @author vdanilov
 */
public interface IGroupListParameters {

    /** @return подразделение, на котором отображается список */
    OrgUnitHolder getOrgUnitHolder();

    /** @return степень согласования, на которой запущен компонент */
    EppRealEduGroupCompleteLevel getLevel();

    /** @return какие именно УГС надо показывать */
    IEppRealEduGroupRowDescription getRelationDescription();

    /** @return factory-of-dqlBuilder (EppRealGroup as "g" left join EppRealGroupRow as "rel") */
    Factory<DQLSelectBuilder> getDqlFactory();

    /** @return название компонента, отображающего содержимое конкретной УГС (используется при открытии УГС из списка) */
    String getGroupContentComponentName();

}
