package ru.tandemservice.uniepp.component.workplan.WorkPlanPartSetEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.dao.workplan.EppWorkPlanDAO;
import ru.tandemservice.uniepp.tapestry.richTableList.RichRangeSelection;

import java.util.Map;
import java.util.Set;


/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
        this.onChangePartsInTerm(component);
    }

    public void onChangePartsInTerm(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        final boolean firstTime = (null == model.getPartsInTerm());
        if (firstTime) {
            // если ничего не выбранно - то части должно быть две (#5228)
            model.setPartsInTerm(Model.getPartsInTerm(2));
        }

        this.getDao().prepareWorkGraph(model);
        this.resetLastVisitedEntityId(model);

        if (firstTime) {
            // если первый раз - то считаем недели
            this.onClickDistribute(component);
        }
    }

    public void onClickDistribute(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        final Map<Integer, String> graphWeekCodeMap = model.getGraphWeekCodeMap();
        DevelopForm developForm = DataAccessServices.dao().get(DevelopForm.class, DevelopForm.programForm(), model.getElement().getEduPlan().getProgramForm());
        if (null == developForm) return;
        final Map<String, Set<String>> eLoadWeekTypeMap = IEppSettingsDAO.instance.get().getELoadWeekTypeMap().get(developForm.getCode());
        EppWorkPlanDAO.recalculateWorkPlanPartWeeks(eLoadWeekTypeMap, graphWeekCodeMap, model.getPartMap().values());
    }

    public void onClickPoint(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        final RichRangeSelection selection = model.getScheduleDataSource().getSelection();
        selection.doPointClick((Integer) component.getListenerParameter());
        this.resetLastVisitedEntityId(model);
    }

    private void resetLastVisitedEntityId(final Model model) {
        model.getScheduleDataSource().getDataSource().setLastVisitedEntityId(null);
    }

    public void onClickApply(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().save(model);
        this.deactivate(component, new ParametersMap().add(Model.RETURN_SUCCESS_PARAM, model.getPartsInTerm().getId()));
    }

}
