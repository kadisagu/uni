/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppSkill;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;

/**
 * @author Igor Belanov
 * @since 03.04.2017
 */
@Configuration
public class EppSkillManager extends BusinessObjectManager
{
    public static final String PROP_PROGRAM_KIND = "programKind";
    public static final String PROP_PROGRAM_SUBJECT_INDEX = "programSubjectIndex";

    // status wrappers
    public static final IdentifiableWrapper ONLY_ACTUAL = new IdentifiableWrapper(0L, "только актуальные");
    public static final IdentifiableWrapper ONLY_NOT_ACTUAL = new IdentifiableWrapper(1L, "только не актуальные");
    // from IMCA wrappers
    public static final IdentifiableWrapper FROM_IMCA = new IdentifiableWrapper(0L, "да");
    public static final IdentifiableWrapper NOT_FROM_IMCA = new IdentifiableWrapper(1L, "нет");

    public static EppSkillManager instance()
    {
        return instance(EppSkillManager.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler programKindDSHandler()
    {
        return EduProgramKind.defaultSelectDSHandler(getName())
                .where(EduProgramKind.programAdditionalProf(), Boolean.FALSE) // ДПО не нужон
                .filter(EduProgramKind.title())
                .order(EduProgramKind.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectIndexDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
                .where(EduProgramSubjectIndex.programKind(), PROP_PROGRAM_KIND)
                .filter(EduProgramSubjectIndex.title())
                .order(EduProgramSubjectIndex.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
                .where(EduProgramSubject.subjectIndex().programKind(), PROP_PROGRAM_KIND)
                .where(EduProgramSubject.subjectIndex(), PROP_PROGRAM_SUBJECT_INDEX)
                .filter(EduProgramSubject.code())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.code())
                .order(EduProgramSubject.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler statusDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                context.put(UIDefines.COMBO_OBJECT_LIST, Lists.newArrayList(ONLY_ACTUAL, ONLY_NOT_ACTUAL));
                return super.execute(input, context);
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler fromIMCADSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                context.put(UIDefines.COMBO_OBJECT_LIST, Lists.newArrayList(FROM_IMCA, NOT_FROM_IMCA));
                return super.execute(input, context);
            }
        };
    }
}
