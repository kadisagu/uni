/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AttestationList;

import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractListUI;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;

/**
 * @author Irina Ugfeld
 * @since 10.03.2016
 */
public class EppRegistryAttestationListUI extends EppRegistryAbstractListUI {

    @Override public String getMenuPermission() { return "menuEppAttestationRegistry"; }
    @Override public String getPermissionPrefix() { return "eppAttestation_list"; }
    @Override public Class getElementClass() { return EppRegistryAttestation.class; }

}