package ru.tandemservice.uniepp.component.group.GroupEduPlanTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.IStudentListModel;

import java.util.List;

@Input( { @Bind(key = "groupId", binding = "group.id") })
public class Model implements IStudentListModel
{
    private Group group = new Group();
    private DynamicListDataSource<Student> dataSource;
    private IMultiSelectModel studentCustomStateCIModel;
    private IDataSettings settings;

    public void setGroup(final Group group)
    {
        this.group = group;
    }

    public Group getGroup()
    {
        return this.group;
    }

    public void setDataSource(final DynamicListDataSource<Student> dataSource)
    {
        this.dataSource = dataSource;
    }

    public DynamicListDataSource<Student> getDataSource()
    {
        return this.dataSource;
    }

    @Override
    public List<IdentifiableWrapper> getStudentStatusOptionList()
    {
        return STUDENT_STATUS_OPTION_LIST;
    }

    public IMultiSelectModel getStudentCustomStateCIModel()
    {
        return studentCustomStateCIModel;
    }

    public void setStudentCustomStateCIModel(IMultiSelectModel studentCustomStateCIModel)
    {
        this.studentCustomStateCIModel = studentCustomStateCIModel;
    }

    public IDataSettings getSettings()
    {
        return settings;
    }

    public void setSettings(IDataSettings settings)
    {
        this.settings = settings;
    }
}
