/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithoutEduPlans;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListPresenter;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
@State({
    @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
public class EppIndicatorStudentsWithoutEduPlansUI extends AbstractEppIndicatorStudentListPresenter
{
    @Override
    public String getSettingsKey()
    {
        return "epp.StudentsWithoutEduPlans.filter";
    }
}
