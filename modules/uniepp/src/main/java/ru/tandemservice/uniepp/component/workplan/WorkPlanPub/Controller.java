package ru.tandemservice.uniepp.component.workplan.WorkPlanPub;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.ChangeEpvBlock.EppWorkPlanChangeEpvBlock;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.ChangeEpvBlock.EppWorkPlanChangeEpvBlockUI;
import ru.tandemservice.uniepp.catalog.entity.EppScriptItem;
import ru.tandemservice.uniepp.catalog.entity.codes.EppScriptItemCodes;

import java.util.Collections;
import java.util.Map;

/**
 * @author nkokorina
 * @since 18.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        IScriptItem scriptItem = DataAccessServices.dao().getByCode(EppScriptItem.class, EppScriptItemCodes.UNIEPP_WORK_PLAN);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(
                scriptItem,
                IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                "wpIds", Collections.singletonList(this.getModel(component).getEppWorkPlan().getId())
        );

        byte[] document = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
        String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

        if (null == document) {
            throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");
        }

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(filename).document(document), false);
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getEppWorkPlan().getId())
        ));
    }

    public void onClickEditPartSet(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.workplan.WorkPlanPartSetEdit.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getEppWorkPlan().getId())
        ));
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        UniDaoFacade.getCoreDao().delete(this.getModel(component).getEppWorkPlan().getId());
        this.deactivate(component);
    }

    public void onClickFillDisciplinesOfEduPlan(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.workplan.WorkPlanFillDiscipline.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getEppWorkPlan().getId())
        ));
    }

    public void onClickChangeEpvBlock(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                EppWorkPlanChangeEpvBlock.class.getSimpleName(),
                ParametersMap.createWith(EppWorkPlanChangeEpvBlockUI.EPP_WP_ID_BIND_PARAM, this.getModel(component).getEppWorkPlan().getId())
        ));
    }
}
