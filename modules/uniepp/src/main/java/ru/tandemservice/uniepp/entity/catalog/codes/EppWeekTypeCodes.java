package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Типы деятельности в учебном графике"
 * Имя сущности : eppWeekType
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppWeekTypeCodes
{
    /** Константа кода (code) элемента : Теоретическое обучение (title) */
    String THEORY = "1";
    /** Константа кода (code) элемента : Экзаменационная сессия (title) */
    String EXAMINATION_SESSION = "2";
    /** Константа кода (code) элемента : Каникулы (title) */
    String HOLIDAYS = "3";
    /** Константа кода (code) элемента : Квалификационная выпускная работа (title) */
    String RESULT_QUALIFICATION_WORK = "4";
    /** Константа кода (code) элемента : Итоговая аттестация (title) */
    String RESULT_ATTESTATION = "5";
    /** Константа кода (code) элемента : Учебная практика (title) */
    String TEACH_PRACTICE = "6";
    /** Константа кода (code) элемента : Учебная практика совместно с аудиторными занятиями (title) */
    String TEACH_PRACTICE_WITH_AUDIT_LOAD = "7";
    /** Константа кода (code) элемента : Производственная практика (title) */
    String WORK_PRACTICE = "8";
    /** Константа кода (code) элемента : Производственная практика совместно с аудиторными занятиями (title) */
    String WORK_PRACTICE_WITH_AUDIT_LOAD = "9";
    /** Константа кода (code) элемента : Неиспользуемые недели (title) */
    String UNUSED = "10";
    /** Константа кода (code) элемента : Дипломное проектирование (title) */
    String DIPLOMA = "11";
    /** Константа кода (code) элемента : Государственные экзамены (title) */
    String STATE_EXAMINATION = "12";
    /** Константа кода (code) элемента : Преддипломная практика (title) */
    String PREDIPLOMA_PRACTICE = "13";
    /** Константа кода (code) элемента : Военные сборы (title) */
    String MILITARY = "14";
    /** Константа кода (code) элемента : Подготовка диссертации (title) */
    String PREPARE_QUALIFICATION_WORK = "15";
    /** Константа кода (code) элемента : Научная практика (title) */
    String SCIENTIFIC_PRACTICE = "16";
    /** Константа кода (code) элемента : Научно-исследовательская работа (title) */
    String RESEARCH_WORK = "17";
    /** Константа кода (code) элемента : Научно-исследовательская работа совместно с аудиторными занятиями (title) */
    String RESEARCH_WORK_WITH_AUDIT_LOAD = "18";

    Set<String> CODES = ImmutableSet.of(THEORY, EXAMINATION_SESSION, HOLIDAYS, RESULT_QUALIFICATION_WORK, RESULT_ATTESTATION, TEACH_PRACTICE, TEACH_PRACTICE_WITH_AUDIT_LOAD, WORK_PRACTICE, WORK_PRACTICE_WITH_AUDIT_LOAD, UNUSED, DIPLOMA, STATE_EXAMINATION, PREDIPLOMA_PRACTICE, MILITARY, PREPARE_QUALIFICATION_WORK, SCIENTIFIC_PRACTICE, RESEARCH_WORK, RESEARCH_WORK_WITH_AUDIT_LOAD);
}
