package ru.tandemservice.uniepp.entity.registry;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;

/**
 * @author vdanilov
 */
public interface IEppRegistryElementItem extends IEntity, ITitled {

}
