package ru.tandemservice.uniepp.base.bo.Student.ui.LifecycleTab.logic;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

import java.util.List;
import java.util.function.Function;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 07.12.2016
 */
public class StudentWpeDAO extends SharedBaseDao implements IStudentWpeDAO
{
	@Override
	public List<EppGroupType> getStudentWpeGroupTypes(Student student)
	{
		final String wpePartAlias = "wpePart";
		return new DQLSelectBuilder().fromEntity(EppStudentWpePart.class, wpePartAlias)
				.where(eq(property(wpePartAlias, EppStudentWpePart.studentWpe().student()), value(student)))
				.column(property(wpePartAlias, EppStudentWpePart.type()))
				.order(property(wpePartAlias, EppStudentWpePart.type().priority()))
				.distinct()
				.createStatement(getSession()).list();
	}

	@Override
	public List<EppStudentWorkPlanElement> getStudentWpe(Student student, Boolean activeFilter, EppYearPart yearPart)
	{
		final String wpeAlias = "studentWpe";
		final String wpRowAlias = "wpRow";
		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, wpeAlias)
				.column(property(wpeAlias))
				.where(eq(property(wpeAlias, EppStudentWorkPlanElement.student()), value(student)))
				.joinPath(DQLJoinType.left, EppStudentWorkPlanElement.sourceRow().fromAlias(wpeAlias), wpRowAlias)
				.order(property(wpeAlias, EppStudentWorkPlanElement.year().educationYear().intValue()))
				.order(property(wpeAlias, EppStudentWorkPlanElement.part().yearDistribution().amount()))
				.order(property(wpeAlias, EppStudentWorkPlanElement.part().number()))
				.order(property(wpeAlias, EppStudentWorkPlanElement.removalDate()), OrderDirection.desc)
				.order(property(wpRowAlias, EppWorkPlanRow.number()))
				.order(property(wpRowAlias, EppWorkPlanRow.title()));
		applyActiveWpeFilter(dql, wpeAlias, activeFilter);
		if (yearPart != null)
		{
			dql.where(eq(property(wpeAlias, EppStudentWorkPlanElement.year()), value(yearPart.getYear())))
					.where(eq(property(wpeAlias, EppStudentWorkPlanElement.part()), value(yearPart.getPart())));
		}
		return dql.createStatement(getSession()).list();
	}

	@Override
	public void applyActiveWpeFilter(DQLSelectBuilder studentWpeDql, String studentWpeAlias, Boolean activeFilter)
	{
		if (activeFilter == null)
			return;
		Function<IDQLExpression, IDQLExpression> nullPredicate = activeFilter ? (DQLExpressions::isNull) : (DQLExpressions::isNotNull);
		studentWpeDql.where(nullPredicate.apply(property(studentWpeAlias, EppStudentWorkPlanElement.removalDate())));
	}

	@Override
	public Table<Long, String, Boolean> getWpeIdAndGroupTypeCode2LoadExist(Student student)
	{
		final String wpeAlias = "wpe";
		final String wpePartAlias = "wpePart";
		DQLSelectBuilder studentWpeDql = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, wpeAlias)
				.where(eq(property(wpeAlias, EppStudentWorkPlanElement.student()), value(student)));
		List<Object[]> rows = new DQLSelectBuilder().fromEntity(EppStudentWpePart.class, wpePartAlias)
				.where(in(property(wpePartAlias, EppStudentWpePart.studentWpe()), studentWpeDql.buildQuery()))
				.column(property(wpePartAlias, EppStudentWpePart.studentWpe().id()))
				.column(property(wpePartAlias, EppStudentWpePart.type().code()))
				.column(property(wpePartAlias, EppStudentWpePart.removalDate()))
				.createStatement(getSession()).list();

		Table<Long, String, Boolean> result = HashBasedTable.create();
		rows.forEach((Object[] row) -> result.put((Long)row[0], (String)row[1], (row[2] == null)));
		return result;
	}
}
