package ru.tandemservice.uniepp.component.registry.RegElementWorkPlanList;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

/**
 * @author vdanilov
 */
@Input( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id")
} )
public class Model {

    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    private DynamicListDataSource<EppWorkPlanRegistryElementRow> dataSource;
    public DynamicListDataSource<EppWorkPlanRegistryElementRow> getDataSource() { return this.dataSource; }
    public void setDataSource(final DynamicListDataSource<EppWorkPlanRegistryElementRow> dataSource) { this.dataSource = dataSource; }

}
