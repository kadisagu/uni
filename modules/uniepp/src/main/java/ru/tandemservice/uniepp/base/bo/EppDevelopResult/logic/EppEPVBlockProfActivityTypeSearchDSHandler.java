/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlockProfActivityType;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 28.02.2017
 */
public class EppEPVBlockProfActivityTypeSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String PROP_EDU_PLAN_VERSION_ID = "eduPlanVersionId";

    public EppEPVBlockProfActivityTypeSearchDSHandler(String ownerId)
    {
        super(ownerId, EppEduPlanVersionBlockProfActivityType.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
//        String filter = input.getComboFilterByValue();
        Long eduPlanVersionId = context.get(PROP_EDU_PLAN_VERSION_ID);
        // версия УП должна быть в любом случае
        assert eduPlanVersionId != null;

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(EppEduPlanVersionBlockProfActivityType.class, "epvb_pat");
        builder.where(eq(
                property("epvb_pat", EppEduPlanVersionBlockProfActivityType.eduPlanVersionBlock().eduPlanVersion().id()),
                value(eduPlanVersionId)));

        // фильтр по введённому названию
//        if (!StringUtils.isEmpty(filter))
//            builder.where(like(upper(property("epvb_pat", EppProfActivityType.title())), value(CoreStringUtils.escapeLike(filter, true))));

        // общий блок в начало
        builder.order(caseExpr(
                exists(new DQLSelectBuilder().fromEntity(EppEduPlanVersionRootBlock.class, "epvb_r")
                        .where(eq(
                                property("epvb_pat", EppEduPlanVersionBlockProfActivityType.eduPlanVersionBlock().id()),
                                property("epvb_r", EppEduPlanVersionRootBlock.id())))
                        .column("epvb_r.id").buildQuery()),
                value(0), value(1)));
        builder.order(property("epvb_pat", EppEduPlanVersionBlockProfActivityType.profActivityType().priority()));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().pageable(true).build();
    }
}
