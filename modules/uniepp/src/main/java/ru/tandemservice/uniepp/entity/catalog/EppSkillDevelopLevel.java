package ru.tandemservice.uniepp.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uniepp.entity.catalog.gen.EppSkillDevelopLevelGen;

/** @see ru.tandemservice.uniepp.entity.catalog.gen.EppSkillDevelopLevelGen */
public class EppSkillDevelopLevel extends EppSkillDevelopLevelGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EppSkillDevelopLevel.class)
                .titleProperty(EppSkillDevelopLevel.title().s())
                .filter(EppSkillDevelopLevel.title())
                .order(EppSkillDevelopLevel.priority());
    }
}