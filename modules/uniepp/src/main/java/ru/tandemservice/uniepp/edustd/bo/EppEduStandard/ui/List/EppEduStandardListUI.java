/* $Id:$ */
package ru.tandemservice.uniepp.edustd.bo.EppEduStandard.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduStd.io.IEppEduStdIODAO;
import ru.tandemservice.uniepp.edustd.bo.EppEduStandard.EppEduStandardManager;
import ru.tandemservice.uniepp.edustd.bo.EppEduStandard.ui.AddEdit.EppEduStandardAddEdit;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

import java.util.*;

/**
 * @author oleyba
 * @since 9/10/14
 */
public class EppEduStandardListUI extends UIPresenter
{
    private final CommonPostfixPermissionModelBase sec = new CommonPostfixPermissionModel("eppEdustd_list").addException("view", "menuEppEduStdList");

    private List<EppGeneration> generationList = Collections.emptyList();
    private ISelectModel _stateListModel;
    
    
    @Override
    public void onComponentRefresh()
    {
        setGenerationList(Arrays.asList(EppGeneration.values()));
        setStateListModel(UniEppUtils.getStateSelectModel(null));
    }
    
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("settings", getSettings());
        dataSource.put(EppEduStandardManager.BIND_GENERATION, getSettings().get("generation"));
        dataSource.put(EppEduStandardManager.BIND_PROGRAM_KIND, getSettings().get("eduProgramKind"));
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((CheckboxColumn) (((BaseSearchListDataSource) _uiConfig.getDataSource("eduStdDS")).getLegacyDataSource()).getColumn("select")).getSelectedObjects();
            selected.clear();
        }
    }
    
    public void onClickAdd() {
        _uiActivation
            .asRegionDialog(EppEduStandardAddEdit.class)
            .activate();
    }

    public void onClickEdit() {
        _uiActivation
            .asRegionDialog(EppEduStandardAddEdit.class)
            .parameter(PUBLISHER_ID, getListenerParameterAsLong())
            .activate();
    }

    public void onClickDelete() {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickExport() {
        final Collection<IEntity> selectedObject = ((CheckboxColumn) (((BaseSearchListDataSource) _uiConfig.getDataSource("eduStdDS")).getLegacyDataSource()).getColumn("select")).getSelectedObjects();

        final Set<Long> selectedIds = new HashSet<Long>(selectedObject.size());
        for (final IEntity entity : selectedObject)
        {
            final IEntityMeta meta = EntityRuntime.getMeta(entity.getId());
            if (null != meta)
            {
                if (EppStateEduStandard.class.isAssignableFrom(meta.getEntityClass()))
                {
                    selectedIds.add(entity.getId());
                }
            }
        }

        if (selectedIds.isEmpty())
        {
            ContextLocal.getErrorCollector().add("Выберите ГОСы для экспорта.");
            return;
        }

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().zip().fileName("eduStandarts.zip").document(IEppEduStdIODAO.instance.get().export(selectedIds)), false);
    }

    // getters and setters
    
    public CommonPostfixPermissionModelBase getSec()
    {
        return this.sec;
    }

    public ISelectModel getStateListModel()
    {
        return _stateListModel;
    }

    public void setStateListModel(ISelectModel stateListModel)
    {
        _stateListModel = stateListModel;
    }

    public List<EppGeneration> getGenerationList()
    {
        return generationList;
    }

    public void setGenerationList(List<EppGeneration> generationList)
    {
        this.generationList = generationList;
    }
}