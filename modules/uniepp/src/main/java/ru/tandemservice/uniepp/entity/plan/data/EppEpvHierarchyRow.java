package ru.tandemservice.uniepp.entity.plan.data;

import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvHierarchyRowGen;

/**
 * Запись версии УП (иерархическая структура)
 */
public abstract class EppEpvHierarchyRow extends EppEpvHierarchyRowGen
{
    @Override public EppRegistryStructure getType() { return null; }

}