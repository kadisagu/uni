// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.edugroup.GroupEdit;

import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;

/**
 * @author iolshvang
 * @since 12.04.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().update(model);
        this.deactivate(component);
    }

    public void onClickGenerateTitle(final IBusinessComponent component) {
        Model model = this.getModel(component);
        FastBeanUtils.setValue(model.getGroup(), EppRealEduGroup.P_TITLE, IEppRealGroupRowDAO.instance.get().calculateTitle(model.getGroup()));
    }
}
