package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Форма итогового контроля
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppFControlActionTypeGen extends EppControlActionType
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppFControlActionType";
    public static final String ENTITY_NAME = "eppFControlActionType";
    public static final int VERSION_HASH = 872217260;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPP_GROUP_TYPE = "eppGroupType";
    public static final String L_DEFAULT_GRADE_SCALE = "defaultGradeScale";
    public static final String L_GROUP = "group";
    public static final String P_PRIORITY = "priority";
    public static final String P_TOTAL_MARK_PRIORITY = "totalMarkPriority";

    private EppGroupTypeFCA _eppGroupType;     // Вид учебной группы (ФИК)
    private EppGradeScale _defaultGradeScale;     // Шкала оценки по умолчанию
    private EppFControlActionGroup _group;     // Группа контрольных мероприятий
    private int _priority;     // Приоритет
    private int _totalMarkPriority;     // Приоритет при выставлении оценки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид учебной группы (ФИК). Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppGroupTypeFCA getEppGroupType()
    {
        return _eppGroupType;
    }

    /**
     * @param eppGroupType Вид учебной группы (ФИК). Свойство не может быть null и должно быть уникальным.
     */
    public void setEppGroupType(EppGroupTypeFCA eppGroupType)
    {
        dirty(_eppGroupType, eppGroupType);
        _eppGroupType = eppGroupType;
    }

    /**
     * Используется для заполнения по умолчанию значения в eppRegistryElementPartFControlAction.gradeScale
     *
     * @return Шкала оценки по умолчанию. Свойство не может быть null.
     */
    @NotNull
    public EppGradeScale getDefaultGradeScale()
    {
        return _defaultGradeScale;
    }

    /**
     * @param defaultGradeScale Шкала оценки по умолчанию. Свойство не может быть null.
     */
    public void setDefaultGradeScale(EppGradeScale defaultGradeScale)
    {
        dirty(_defaultGradeScale, defaultGradeScale);
        _defaultGradeScale = defaultGradeScale;
    }

    /**
     * Используется для группировки типов мероприятий, например в журналах
     *
     * @return Группа контрольных мероприятий. Свойство не может быть null.
     */
    @NotNull
    public EppFControlActionGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа контрольных мероприятий. Свойство не может быть null.
     */
    public void setGroup(EppFControlActionGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * Приоритет для отображения в отчетах и интерфейсе
     *
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * Приоритет формы оценки при вычислении итога по части дисциплины (при наличии более чем одного мероприятия в части)
     *
     * @return Приоритет при выставлении оценки. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getTotalMarkPriority()
    {
        return _totalMarkPriority;
    }

    /**
     * @param totalMarkPriority Приоритет при выставлении оценки. Свойство не может быть null и должно быть уникальным.
     */
    public void setTotalMarkPriority(int totalMarkPriority)
    {
        dirty(_totalMarkPriority, totalMarkPriority);
        _totalMarkPriority = totalMarkPriority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppFControlActionTypeGen)
        {
            setEppGroupType(((EppFControlActionType)another).getEppGroupType());
            setDefaultGradeScale(((EppFControlActionType)another).getDefaultGradeScale());
            setGroup(((EppFControlActionType)another).getGroup());
            setPriority(((EppFControlActionType)another).getPriority());
            setTotalMarkPriority(((EppFControlActionType)another).getTotalMarkPriority());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppFControlActionTypeGen> extends EppControlActionType.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppFControlActionType.class;
        }

        public T newInstance()
        {
            return (T) new EppFControlActionType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "eppGroupType":
                    return obj.getEppGroupType();
                case "defaultGradeScale":
                    return obj.getDefaultGradeScale();
                case "group":
                    return obj.getGroup();
                case "priority":
                    return obj.getPriority();
                case "totalMarkPriority":
                    return obj.getTotalMarkPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "eppGroupType":
                    obj.setEppGroupType((EppGroupTypeFCA) value);
                    return;
                case "defaultGradeScale":
                    obj.setDefaultGradeScale((EppGradeScale) value);
                    return;
                case "group":
                    obj.setGroup((EppFControlActionGroup) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "totalMarkPriority":
                    obj.setTotalMarkPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eppGroupType":
                        return true;
                case "defaultGradeScale":
                        return true;
                case "group":
                        return true;
                case "priority":
                        return true;
                case "totalMarkPriority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eppGroupType":
                    return true;
                case "defaultGradeScale":
                    return true;
                case "group":
                    return true;
                case "priority":
                    return true;
                case "totalMarkPriority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "eppGroupType":
                    return EppGroupTypeFCA.class;
                case "defaultGradeScale":
                    return EppGradeScale.class;
                case "group":
                    return EppFControlActionGroup.class;
                case "priority":
                    return Integer.class;
                case "totalMarkPriority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppFControlActionType> _dslPath = new Path<EppFControlActionType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppFControlActionType");
    }
            

    /**
     * @return Вид учебной группы (ФИК). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionType#getEppGroupType()
     */
    public static EppGroupTypeFCA.Path<EppGroupTypeFCA> eppGroupType()
    {
        return _dslPath.eppGroupType();
    }

    /**
     * Используется для заполнения по умолчанию значения в eppRegistryElementPartFControlAction.gradeScale
     *
     * @return Шкала оценки по умолчанию. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionType#getDefaultGradeScale()
     */
    public static EppGradeScale.Path<EppGradeScale> defaultGradeScale()
    {
        return _dslPath.defaultGradeScale();
    }

    /**
     * Используется для группировки типов мероприятий, например в журналах
     *
     * @return Группа контрольных мероприятий. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionType#getGroup()
     */
    public static EppFControlActionGroup.Path<EppFControlActionGroup> group()
    {
        return _dslPath.group();
    }

    /**
     * Приоритет для отображения в отчетах и интерфейсе
     *
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionType#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * Приоритет формы оценки при вычислении итога по части дисциплины (при наличии более чем одного мероприятия в части)
     *
     * @return Приоритет при выставлении оценки. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionType#getTotalMarkPriority()
     */
    public static PropertyPath<Integer> totalMarkPriority()
    {
        return _dslPath.totalMarkPriority();
    }

    public static class Path<E extends EppFControlActionType> extends EppControlActionType.Path<E>
    {
        private EppGroupTypeFCA.Path<EppGroupTypeFCA> _eppGroupType;
        private EppGradeScale.Path<EppGradeScale> _defaultGradeScale;
        private EppFControlActionGroup.Path<EppFControlActionGroup> _group;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Integer> _totalMarkPriority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид учебной группы (ФИК). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionType#getEppGroupType()
     */
        public EppGroupTypeFCA.Path<EppGroupTypeFCA> eppGroupType()
        {
            if(_eppGroupType == null )
                _eppGroupType = new EppGroupTypeFCA.Path<EppGroupTypeFCA>(L_EPP_GROUP_TYPE, this);
            return _eppGroupType;
        }

    /**
     * Используется для заполнения по умолчанию значения в eppRegistryElementPartFControlAction.gradeScale
     *
     * @return Шкала оценки по умолчанию. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionType#getDefaultGradeScale()
     */
        public EppGradeScale.Path<EppGradeScale> defaultGradeScale()
        {
            if(_defaultGradeScale == null )
                _defaultGradeScale = new EppGradeScale.Path<EppGradeScale>(L_DEFAULT_GRADE_SCALE, this);
            return _defaultGradeScale;
        }

    /**
     * Используется для группировки типов мероприятий, например в журналах
     *
     * @return Группа контрольных мероприятий. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionType#getGroup()
     */
        public EppFControlActionGroup.Path<EppFControlActionGroup> group()
        {
            if(_group == null )
                _group = new EppFControlActionGroup.Path<EppFControlActionGroup>(L_GROUP, this);
            return _group;
        }

    /**
     * Приоритет для отображения в отчетах и интерфейсе
     *
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionType#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EppFControlActionTypeGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * Приоритет формы оценки при вычислении итога по части дисциплины (при наличии более чем одного мероприятия в части)
     *
     * @return Приоритет при выставлении оценки. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionType#getTotalMarkPriority()
     */
        public PropertyPath<Integer> totalMarkPriority()
        {
            if(_totalMarkPriority == null )
                _totalMarkPriority = new PropertyPath<Integer>(EppFControlActionTypeGen.P_TOTAL_MARK_PRIORITY, this);
            return _totalMarkPriority;
        }

        public Class getEntityClass()
        {
            return EppFControlActionType.class;
        }

        public String getEntityName()
        {
            return "eppFControlActionType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
