package ru.tandemservice.uniepp.dao.eduplan;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.util.WeekTypeLegendRow;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Операции с УП, версией УП, ГУП (на уровне УП)
 *
 * @author vdanilov
 */
public interface IEppEduPlanDAO
{
    SpringBeanCache<IEppEduPlanDAO> instance = new SpringBeanCache<>(IEppEduPlanDAO.class.getName());

    /**
     * @param eppEduPlan УП
     * @return список объектов, на которых надо проверять локальные права для учебного плата
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Collection<IEntity> getSecLocalEntities(EppEduPlan eppEduPlan);

    /**
     * @return Список строк легенды для использования типов недель
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    List<WeekTypeLegendRow> getWeekTypeLegendRowList(Collection<EppWeekType> weekTypeCollection);


    /**
     * Обновляет блоки в версии УП
     *
     * @param eduPlanVersionId id версии учебного плана
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateBlockList(Long eduPlanVersionId, Map<EduProgramSpecialization, EduOwnerOrgUnit> specMap);

    /**
     * Возвращает список активных связей студентов с УП(в)
     * @return { student.id -> student2epv (active=true) }
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<Long, EppStudent2EduPlanVersion> getActiveStudentEduplanVersionRelationMap(Collection<Long> studentIds);

    /**
     * Возвращает активную связь студента с УП(в)
     * @return student2epv (active=true)
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    EppStudent2EduPlanVersion getActiveStudentEduPlanVersionRelation(Long studentId);

    /**
     * Возвращает активный блок УП(в), соответсвующий активной связи с УП(в) и текущему НПП студента
     * @return block | block.eduHs=student.eduOu.eduHS, block.epv=student2epv(student,active=true)
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    EppEduPlanVersionBlock getActiveStudentEduPlanVersionBlock(Long studentId);

    /**
     * Обновляет связи студентов с версией УП
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateStudentEduPlanVersion(Collection<StudentEduPlanData> data);

    /** @return правило нумерации УП */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    INumberGenerationRule<EppEduPlan> getEduPlanNumberGenerationRule();

    /** @return правило нумерации УП (в) */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    INumberGenerationRule<EppEduPlanVersion> getEduPlanVersionNumberGenerationRule();

    interface StudentEduPlanData {
        Long getStudentId();
        Long getVersionId();
        Long getBlockId();
        Long getCustomEduPlanId();
    }

}
