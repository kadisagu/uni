package ru.tandemservice.uniepp.dao.group;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe.Reason;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow.IEppRealEduGroupRowDescription;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary;

import java.util.Collection;

/**
 * @author vdanilov
 */

public interface IEppRealGroupRowDAO {
    public static final SpringBeanCache<IEppRealGroupRowDAO> instance = new SpringBeanCache<IEppRealGroupRowDAO>(IEppRealGroupRowDAO.class.getName());


    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Collection<IEppRealEduGroupRowDescription> getRelations();

    /**
     * Создает необходимые сводки (только для учебный процессов текущего года)
     * ps. для учебных процессов предыдущих лет нельзя использовать названия групп из объекта "группа"
     * @return true, если были произведены какие-либо изменения
     * @throws RuntimeTimeoutException если протухла блокировка
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Используется демоном")
    boolean doCreateMissingSummary(boolean force);

    /**
     * удаляет невалидные и не нужные строки в группах, а также пустые группы (по текущим данным системы)
     * @return true, если были произведены какие-либо изменения
     * @throws RuntimeTimeoutException если протухла блокировка
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Используется демоном")
    boolean doDisableUnnecessaryRelations();

    /**
     * пробуждает к жизни убиенных студентов в УГС, если у студента воскресло МСРП (пробуем восстановить студента в прежнюю УГС):
     * * если у студента есть более одного трупа, то трупы утилизируются
     * * если у студента только один труп - то его и воскрешаем
     * @return true, если были произведены какие-либо изменения
     * @throws RuntimeTimeoutException если протухла блокировка
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Используется демоном")
    boolean doAwakeStudentRows();

    /**
     * обновляет список связей студентов с группами (по текущим данным системы)
     * @return true, если были произведены какие-либо изменения
     * @throws RuntimeTimeoutException если протухла блокировка
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Используется демоном")
    boolean doUpdateStudentRowList(IEppRealEduGroupRowDescription relation, EppRealEduGroupSummary summary, EppGroupType type);


    /**
     * обновляет список связей студентов с группами (по текущим данным системы)
     * @return true, если были произведены какие-либо изменения
     * @throws RuntimeTimeoutException если протухла блокировка
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Используется демоном")
    boolean doUpdateStudentRowList();


    /**
     * Условие отбора УГС
     * см. doUpdateStudentRowData
     */
    public static interface UpdateStudentRowDataCondition {
        /**
         * @param relation тип строки УГС
         * @return дополнительное суловие отбора строк УГС, в которых необходимо обновить значение (relation-alias="x")
         */
        IDQLExpression condition(EppRealEduGroupRow.IEppRealEduGroupRowDescription relation);
    }

    /**
     * обновляет данные студента для списка связей студентов с группами (по текущим данным системы)
     *  - всегда обновляет данные в строках, в которых нет данных
     *  - обновляет данные в активных строках текущего учебного года, соответствующих доп.условию
     * @param condition доп. условие отбора строу УГС
     * @return true, если были произведены какие-либо изменения
     * @throws RuntimeTimeoutException если протухла блокировка
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Используется демоном")
    boolean doUpdateStudentRowData(UpdateStudentRowDataCondition condition);

    /**
     * обновляет данные студента для списка связей студентов с группами (по текущим данным системы)
     *  - всегда обновляет данные в строках, в которых нет данных
     *  - обновляет данные в активных строках текущего учебного года для групп без этапов согласования
     * @return true, если были произведены какие-либо изменения
     * @throws RuntimeTimeoutException если протухла блокировка
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    @DoNotUseMe(reason=Reason.INTERNAL, comment="Используется демоном")
    boolean doUpdateStudentRowData();

    /**
     * проверяет, что группа доступна для изменения (не заблокирована)
     *
     * @param level nullable, если null, то проверки на соответсвие уровню не происходит
     * @throws RuntimeException если группа заблокирована
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void checkGroupLockedEdit(Collection<Long> groupIds, EppRealEduGroupCompleteLevel level) throws RuntimeException;

    /**
     * проверяет, что группа доступна для изменения уровня
     *
     * @param level nullable, если null, то проверки на соответсвие уровню не происходит
     * @throws RuntimeException если группа заблокирована
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void checkGroupLockedLevel(Collection<Long> groupIds, EppRealEduGroupCompleteLevel level) throws RuntimeException;

    /**
     * объединяет указанные группы в одну (всегда создает новую)
     * (в зависимости от состояния - старые группы либо удаляются, либо связи в них - архивируются)
     * 
     * @param groupIds (EppRealEduGroup)
     * @param level уровень, на котором осуществляется операция
     * @return id новой группы
     * @throws IllegalArgumentException если группы были из разных сводок или были разных типов
     * @throws java.util.concurrent.TimeoutException если протухла блокировка
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    Long doJoin(Collection<Long> groupIds, EppRealEduGroupCompleteLevel level);


    /**
     * Извлекает студентов из группы (создает новую группу и переносит туда всех студентов)
     * 
     * @param studentIds (EppRealEduGroupRow) - могут быть из разных УГС
     * @param level уровень, на котором осуществляется операция
     * @param targetLevel уровень, который будет присвоен новой группе (не может быть больше, чем level)
     * @return id новой группы
     * @throws IllegalArgumentException если студенты были из разных сводок или были разных типов
     * @throws java.util.concurrent.TimeoutException если протухла блокировка
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    Long doSplit(Collection<Long> studentIds, EppRealEduGroupCompleteLevel level, EppRealEduGroupCompleteLevel targetLevel);

    /**
     * Перемещает указанных студентов из своих групп в указанную группу
     * 
     * @param groupId (EppRealEduGroup) УГС, в которую надо переметить студентов
     * @param studentIds (EppRealEduGroupRow) студенты, которых надо переместить - должны быть соответствующего типа, с одной операторской и одной годочасти
     * @param level уровень, на котором осуществляется операция
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doMove(Long groupId, Collection<Long> studentIds, EppRealEduGroupCompleteLevel level);


    /**
     * формирует название для учебной группы исходя из состава
     * @param group учебная группа
     * @return
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    String calculateTitle(final EppRealEduGroup group);

    /**
     * переводит группы на заданный этап согласования
     * @param groupIds список id УГСов
     * @param targetLevel новый этап согласования
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSetLevel(Collection<Long> groupIds, EppRealEduGroupCompleteLevel targetLevel);

    /**
     * Вызывает проверки используемости групп, и, если все ок, переводит группы на нулевой этап согласования
     * @param groupIds список id УГСов
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doResetLevel(Collection<Long> groupIds);


    /**
     * исправляет сводки для УГС текущего года (в соответствии с настройками)
     * ! переносит группу в другую диспетчерскую, если у всех студентов группы диспетчерская стала одинаковая !
     * ! удаляет связи с УГС для студентов, чьи диспетчерские не соотвествуют диспетчерским группы, далее запускает демон, который создает все, что требуется !
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    boolean doFixCurrentYearEduGroupSummary();


    /**
     * блокирует действия с УГС до окончания транзакции
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void lock();


}
