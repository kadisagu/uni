package ru.tandemservice.uniepp.entity.catalog;

import ru.tandemservice.uniepp.entity.catalog.gen.EppLoadTypeGen;

import java.util.Comparator;

/**
 * Базовый класс для вида нагрузки
 */
public abstract class EppLoadType extends EppLoadTypeGen implements IEppFullCodeCatalogItem
{
    public static final Comparator<EppLoadType> COMPARATOR = (o1, o2) -> {
        final String c1 = (null == o1 ? "" : o1.getFullCode());
        final String c2 = (null == o2 ? "" : o2.getFullCode());
        return c1.compareTo(c2);
    };

    public static String getFullCode(final String catalogCode, final String code) {
        return ("load."+catalogCode+"."+code);
    }

    private String _full_code_cache;

    @Override
    public String getFullCode()
    {
        final String v = _full_code_cache;
        if (null != v) { return v; }

        return (_full_code_cache = EppLoadType.getFullCode(this.getCatalogCode(), this.getCode()));
    }

    /** всего часов */
    public static final String FULL_CODE_TOTAL_HOURS = "xload.size";
    /** трудоемкость */
    public static final String FULL_CODE_LABOR = "xload.labor";
    /** число недель */
    public static final String FULL_CODE_WEEKS = "xload.weeks";
    /** часов на контроль */
    public static final String FULL_CODE_CONTROL = "xload.control";
    /** из часов на контроль - в эл. форме */
    public static final String FULL_CODE_CONTROL_E = "xload.controlE";
}