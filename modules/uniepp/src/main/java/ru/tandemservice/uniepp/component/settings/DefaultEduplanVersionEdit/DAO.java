// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.settings.DefaultEduplanVersionEdit;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 04.12.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setEducationOrgUnit(this.getNotNull(EducationOrgUnit.class, model.getEducationOrgUnit().getId()));
        EppEducationOrgUnitEduPlanVersion settings = this.get(EppEducationOrgUnitEduPlanVersion.class, EppEducationOrgUnitEduPlanVersion.educationOrgUnit().s(), model.getEducationOrgUnit());
        if (null == settings)
        {
            settings = new EppEducationOrgUnitEduPlanVersion();
            settings.setEducationOrgUnit(model.getEducationOrgUnit());
        }
        model.setSettings(settings);
        model.setVersion(model.getSettings().getEduPlanVersion());
        if (null != model.getVersion()) {
            model.setEduPlan(model.getVersion().getEduPlan());
        }
        model.setEduPlanModel(new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder eduPlan = new DQLSelectBuilder()
                .fromEntity(EppEduPlanProf.class, alias)
                .where(ne(property(EppEduPlan.state().code().fromAlias(alias)), value(EppState.STATE_ARCHIVED)))
                .where(ne(property(EppEduPlan.state().code().fromAlias(alias)), value(EppState.STATE_REJECTED)));

                eduPlan.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(EducationOrgUnit.class, "ou").column(property("ou.id"))
                        .where(eq(property("ou.id"), value(model.getEducationOrgUnit().getId())))
                        .where(eq(property(EppEduPlanProf.programSubject().fromAlias(alias)), property(EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().fromAlias("ou"))))
                        .where(eq(property(EducationOrgUnit.developForm().programForm().fromAlias("ou")), property(EppEduPlan.programForm().fromAlias(alias))))
                        .where(eq(property(EducationOrgUnit.developCondition().fromAlias("ou")), property(EppEduPlan.developCondition().fromAlias(alias))))
                        .where(eqNullSafe(property(EducationOrgUnit.developTech().programTrait().fromAlias("ou")), property(EppEduPlan.programTrait().fromAlias(alias))))
                        .where(eq(property(EducationOrgUnit.developPeriod().fromAlias("ou")), property(EppEduPlan.developGrid().developPeriod().fromAlias(alias))))
                        .buildQuery()
                ));
                return eduPlan;
            }
        });
        model.setVersionModel(new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder version = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersion.class, alias);
                if (model.getEduPlan() != null) {
                    version.where(eq(property(EppEduPlanVersion.eduPlan().fromAlias(alias)), value(model.getEduPlan())));
                } else {
                    version.where(isNull(alias + ".id"));
                }
                return version;
            }
        });
    }

    @Override
    public void update(final Model model)
    {
        final EppEducationOrgUnitEduPlanVersion settings = model.getSettings();
        final EppEduPlanVersion version = model.getVersion();
        if ((null == version) && (settings.getId() != null))
        {
            this.delete(settings);
        }
        else if (null != version)
        {
            settings.setEduPlanVersion(version);
            settings.setModificationDate(new Date());
            this.getSession().saveOrUpdate(settings);
        }

    }
}
