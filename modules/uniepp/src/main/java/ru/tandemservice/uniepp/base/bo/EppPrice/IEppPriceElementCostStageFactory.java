/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppPrice;

import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.ICtrPriceElementCostStageFactory;

/**
 * @author oleyba
 * @since 7/20/12
 */
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=8950856")
public interface IEppPriceElementCostStageFactory extends ICtrPriceElementCostStageFactory
{
    /**
     * Ключ для фильтрации этапов по курсу обучения. Передавать как значение нужно int - порядковый номер курса.
     * @see ICtrPriceElementCostStageFactory#filterStageList(org.tandemframework.shared.ctr.base.entity.ICtrPriceElement, java.util.List, java.util.Map)
     */
    String FILTER_KEY_COURSE = "course";

    /**
     * Ключ для фильтрации этапов по семестру обучения. Передавать как значение нужно int - порядковый номер семестра.
     * @see ICtrPriceElementCostStageFactory#filterStageList(org.tandemframework.shared.ctr.base.entity.ICtrPriceElement, java.util.List, java.util.Map)
     */
    String FILTER_KEY_TERM = "term";
}
