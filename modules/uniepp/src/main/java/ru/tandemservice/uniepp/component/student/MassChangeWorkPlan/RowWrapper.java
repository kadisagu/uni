package ru.tandemservice.uniepp.component.student.MassChangeWorkPlan;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

public abstract class RowWrapper
{
    public RowWrapper(final EppWorkPlanBase workPlan, final boolean value) {
        if (null != workPlan)
        {
            this.setCurrentWorkPlan(workPlan);
            this.setCurrentYear(workPlan.getYear());
        }
        this.setDisabled(value);
    }

    public abstract ISelectModel getYearListModel();
    public abstract ISelectModel getWorkplanListModel();

    private EppYearEducationProcess currentYear;
    public EppYearEducationProcess getCurrentYear() { return this.currentYear; }
    public void setCurrentYear(final EppYearEducationProcess currentYear) { this.currentYear = currentYear; }

    private EppWorkPlanBase currentWorkPlan;
    public EppWorkPlanBase getCurrentWorkPlan() { return this.currentWorkPlan; }
    public void setCurrentWorkPlan(final EppWorkPlanBase currentWorkPlan) { this.currentWorkPlan = currentWorkPlan; }

    private boolean disabled = false;
    public boolean isDisabled() { return this.disabled; }
    public void setDisabled(final boolean disabled) { this.disabled = disabled; }
}
