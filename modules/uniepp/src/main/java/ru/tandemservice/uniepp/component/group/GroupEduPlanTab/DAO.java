package ru.tandemservice.uniepp.component.group.GroupEduPlanTab;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.uni.ui.IStudentListModel;
import ru.tandemservice.uni.util.CustomStateUtil;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.util.EppDevelopCombinationTitleUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(final Model model)
    {
        final Session session = this.getSession();

        final MQBuilder builder = new MQBuilder(Student.ENTITY_NAME, "s");
        builder.add(MQExpression.eq("s", StudentGen.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.eq("s", Student.group().id().s(), model.getGroup().getId()));
        builder.addOrder("s", Student.STUDENT_LAST_NAME);
        builder.addOrder("s", Student.STUDENT_FIRST_NAME);
        builder.addOrder("s", Student.STUDENT_MIDDLE_NAME);

        IdentifiableWrapper<IEntity> status = model.getSettings().get("status");
        if (status != null) {
            builder.addJoinFetch("s", Student.L_STATUS, "studentStatus");
            builder.add(MQExpression.eq("studentStatus", StudentStatus.P_ACTIVE, status.getId().equals(IStudentListModel.STUDENT_STATUS_ACTIVE)));
        }

        List<StudentCustomStateCI> studentCustomStateCIList = model.getSettings().get("studentCustomStateCIs");
        CustomStateUtil.addCustomStatesFilter(builder, "s", studentCustomStateCIList);

        final List<Student> studentList = builder.getResultList(session);

        final Map<Long, EppStudent2EduPlanVersion> relationMap = IEppEduPlanDAO.instance.get().getActiveStudentEduplanVersionRelationMap(UniBaseDao.ids(studentList));
        final Map<Long, EppStudent2EduPlanVersion> idStudent2rel = new HashMap<Long, EppStudent2EduPlanVersion>();
        for (final EppStudent2EduPlanVersion rel : relationMap.values()) {
            idStudent2rel.put(rel.getStudent().getId(), rel);
        }

        final DynamicListDataSource<Student> dataSource = model.getDataSource();

        dataSource.setTotalSize(studentList.size());
        dataSource.setCountRow(studentList.size());
        dataSource.createPage(studentList);

        for (final ViewWrapper<IEntity> wrapper : ViewWrapper.getPatchedList(dataSource))
        {
            final Long id = wrapper.getId();
            EppStudent2EduPlanVersion rel = idStudent2rel.get(id);
            String data = null;

            if (rel != null)
            {
                final EppEduPlan plan = rel.getEduPlanVersion().getEduPlan();
                data = plan.getEducationElementSimpleTitle() + " " + EppDevelopCombinationTitleUtil.getTitle(plan);
            }
            wrapper.setViewProperty("epvData", data);
            wrapper.setViewProperty("eduPlanVersion", rel == null ? null : rel.getEduPlanVersion());
            wrapper.setViewProperty("eduPlanBlock", rel == null ? null : rel.getBlock());
        }
    }

    @Override
    public void prepare(Model model)
    {
        model.setStudentCustomStateCIModel(new CommonMultiSelectModel(StudentCustomStateCI.title().s())
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateCI.class, "st").column(property("st"))
                        .order(property(StudentCustomStateCI.title().fromAlias("st")));

                if (!StringUtils.isEmpty(filter))
                    builder.where(like(upper(property(StudentCustomStateCI.title().fromAlias("st"))), value(CoreStringUtils.escapeLike(filter, true))));

                if (set != null)
                    builder.where(in(property(StudentCustomStateCI.id().fromAlias("st")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });
    }
}
