package ru.tandemservice.uniepp.dao.index;

import org.apache.commons.lang.StringUtils;

import ru.tandemservice.uniepp.entity.catalog.EppGeneration;

/**
 * @author vdanilov
 */
public interface IEppIndexRule {
    // TODO: DEV-3265 - описание


    public static IEppIndexRule RULE_SIMPLE_CONCAT = new IEppIndexRule() {
        @Override public String getIndex(final IEppIndexedRowWrapper eppRowWrapper) {
            final IEppIndexedRowWrapper hierarchyParent = (IEppIndexedRowWrapper)eppRowWrapper.getHierarhyParent();
            if (null == hierarchyParent) {
                return eppRowWrapper.getSelfIndexPart();
            }
            return (hierarchyParent.getIndex()+"."+eppRowWrapper.getSelfIndexPart());
        }
    };

    public static IEppIndexRule RULE_POSTFIX_CONCAT = new IEppIndexRule() {
        @Override public String getIndex(final IEppIndexedRowWrapper eppRowWrapper) {
            final String index = IEppIndexRule.RULE_SIMPLE_CONCAT.getIndex(eppRowWrapper);
            if (null == eppRowWrapper.getHierarhyParent()) {
                final String qualification = StringUtils.trimToEmpty(eppRowWrapper.getQualificationCode());
                return (index+qualification);
            }
            return index;
        }
    };

    public static IEppIndexRule RULE_TRIMMED_CONCAT = new IEppIndexRule() {
        @Override public String getIndex(final IEppIndexedRowWrapper eppRowWrapper) {
            final IEppIndexedRowWrapper hierarchyParent = (IEppIndexedRowWrapper)eppRowWrapper.getHierarhyParent();
            if (null == hierarchyParent) { return eppRowWrapper.getSelfIndexPart(); }
            if (null == hierarchyParent.getHierarhyParent()) { return eppRowWrapper.getSelfIndexPart(); }
            return (hierarchyParent.getIndex()+"."+eppRowWrapper.getSelfIndexPart());
        }
    };

    public static IEppIndexRule RULE_PREFIX_HIERARCHY = new IEppIndexRule() {
        @Override public String getIndex(final IEppIndexedRowWrapper eppRowWrapper) {
            final String postfix = getPostfix(eppRowWrapper);
            final IEppIndexedRowWrapper hierarchyParent = (IEppIndexedRowWrapper)eppRowWrapper.getHierarhyParent();
            if (null == hierarchyParent) {
                final String qualification = StringUtils.trimToNull(eppRowWrapper.getQualificationCode());
                if (null == qualification) { return postfix; }
                return (qualification + "." + postfix);
            }
            return (hierarchyParent.getIndex()+"."+ postfix);
        }

        private String getPostfix(IEppIndexedRowWrapper eppRowWrapper) {
            final IEppIndexedRow row = eppRowWrapper.getRow();
            if (row instanceof IEppGroupImIndexedRow) {
                final EppGeneration generation = eppRowWrapper.getGeneration();
                if (EppGeneration.GENERATION_3.equals(generation)) {
                    return "ДВ."+String.valueOf(eppRowWrapper.getSequenceNumber());
                }
            }
            return String.valueOf(eppRowWrapper.getSequenceNumber());
        }
    };

    public static IEppIndexRule RULE_PREFIX_COMPONENT_HIERARCHY = new IEppIndexRule() {
        @Override public String getIndex(final IEppIndexedRowWrapper eppRowWrapper) {
            final String postfix = getPostfix(eppRowWrapper);
            final IEppIndexedRowWrapper hierarchyParent = (IEppIndexedRowWrapper)eppRowWrapper.getHierarhyParent();
            if (null == hierarchyParent) {
                final String qualification = StringUtils.trimToNull(eppRowWrapper.getQualificationCode());
                if (null == qualification) { return String.valueOf(eppRowWrapper.getSequenceNumber()); }
                return (qualification + String.valueOf(eppRowWrapper.getSequenceNumber()));
            }
            return (hierarchyParent.getIndex()+"."+postfix);
        }

        private String getPostfix(IEppIndexedRowWrapper eppRowWrapper) {
            final IEppIndexedRow row = eppRowWrapper.getRow();
            if (row instanceof IEppStructureIndexedRow) {
                final String postfix = StringUtils.trimToNull(((IEppStructureIndexedRow) row).getValue().getShortTitle());
                if (null != postfix) { return postfix; }
            } else if (row instanceof IEppGroupImIndexedRow) {
                final EppGeneration generation = eppRowWrapper.getGeneration();
                if (EppGeneration.GENERATION_3.equals(generation)) {
                    return "ДВ."+String.valueOf(eppRowWrapper.getSequenceNumber());
                }
            }
            return String.valueOf(eppRowWrapper.getSequenceNumber());
        }
    };




    /**
     * @param eppRowWrapper - строка
     * @return индекс для строки
     */
    String getIndex(IEppIndexedRowWrapper eppRowWrapper);


}
