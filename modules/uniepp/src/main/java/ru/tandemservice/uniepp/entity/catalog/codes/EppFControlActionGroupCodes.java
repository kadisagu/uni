package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Группа форм итогового контроля"
 * Имя сущности : eppFControlActionGroup
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppFControlActionGroupCodes
{
    /** Константа кода (code) элемента : Экзамен (title) */
    String CONTROL_ACTION_GROUP_EXAM = "1";
    /** Константа кода (code) элемента : Зачет (title) */
    String CONTROL_ACTION_GROUP_SETOFF = "2";
    /** Константа кода (code) элемента : Проект (title) */
    String CONTROL_ACTION_GROUP_PROJECT = "3";

    Set<String> CODES = ImmutableSet.of(CONTROL_ACTION_GROUP_EXAM, CONTROL_ACTION_GROUP_SETOFF, CONTROL_ACTION_GROUP_PROJECT);
}
