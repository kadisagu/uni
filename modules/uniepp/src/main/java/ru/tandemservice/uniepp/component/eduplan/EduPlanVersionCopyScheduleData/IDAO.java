/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionCopyScheduleData;

import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author nkokorina
 * Created on: 24.09.2010
 */
public interface IDAO extends IUpdateable<Model>
{
    void prepareCourseList(Model model);
}
