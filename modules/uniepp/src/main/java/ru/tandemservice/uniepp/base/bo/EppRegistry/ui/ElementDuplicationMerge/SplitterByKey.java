/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.ElementDuplicationMerge;

/**
 * @author Nikolay Fedorovskih
 * @since 15.05.2015
 */
public class SplitterByKey extends RowWrapperBase
{
    @Override
    public boolean isSplitterByKey()
    {
        return true;
    }

    @Override
    public boolean isSplitterByTitle()
    {
        return false;
    }

    @Override
    public String getSplitterStyle()
    {
        return "height: 12px;background-color:#BFD2E4;";
    }
}