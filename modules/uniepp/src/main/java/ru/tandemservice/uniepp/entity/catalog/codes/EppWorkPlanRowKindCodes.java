package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид дисциплины РУП"
 * Имя сущности : eppWorkPlanRowKind
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppWorkPlanRowKindCodes
{
    /** Константа кода (code) элемента : Основная (title) */
    String MAIN = "1";
    /** Константа кода (code) элемента : Элективная (title) */
    String SELECTED = "2";
    /** Константа кода (code) элемента : Факультативная (title) */
    String OPTIONAL = "3";
    /** Константа кода (code) элемента : Адаптационная (title) */
    String ADAPTATION = "4";

    Set<String> CODES = ImmutableSet.of(MAIN, SELECTED, OPTIONAL, ADAPTATION);
}
