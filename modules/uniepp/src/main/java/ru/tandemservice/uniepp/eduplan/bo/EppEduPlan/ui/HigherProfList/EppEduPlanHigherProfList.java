/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/28/14
 */
@Configuration
public class EppEduPlanHigherProfList extends BusinessComponentManager
{
    public static final String PARAM_PROGRAM_SUBJECT = "programSubject";
    public static final String PARAM_PROGRAM_KIND = "programKind";

    public static final String PROGRAM_SPECIALIZATION_DS = "programSpecializationDS";

    public static final String EDUCATION_PERIOD = "educationPeriod";
    public static final String LABOR = "labor";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("subjectIndexDS", subjectIndexDSHandler()))
            .addDataSource(selectDS("programSubjectDS", programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
            .addDataSource(selectDS(PROGRAM_SPECIALIZATION_DS, programSpecializationDSHandler()).addColumn(EduProgramSpecialization.displayableTitle().s()))
            .addDataSource(selectDS("programFormDS", programFormDSHandler()))
            .addDataSource(selectDS("developConditionDS", developConditionDSHandler()))
            .addDataSource(selectDS("programTraitDS", programTraitDSHandler()))
            .addDataSource(selectDS("developGridDS", developGridDSHandler()))
            .addDataSource(EppStateManager.instance().eppStateDSConfig())
            .addDataSource(searchListDS("eduPlanDS", eduPlanDSColumns(), eduPlanDSHandler()))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler subjectIndexDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
                .where(EduProgramSubjectIndex.programKind(), PARAM_PROGRAM_KIND)
                .filter(EduProgramSubjectIndex.title())
                .order(EduProgramSubjectIndex.code());
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                dql.where(exists(new DQLSelectBuilder()
                    .fromEntity(EppEduPlanHigherProf.class, "p")
                    .where(eq(property("p", EppEduPlanHigherProf.programSubject()), property(alias)))
                    .where(eq(property("p", EppEduPlanHigherProf.programKind()), commonValue(context.get(PARAM_PROGRAM_KIND))))
                    .buildQuery()));
            }
        }
            .filter(EduProgramSubject.code())
            .filter(EduProgramSubject.title())
            .order(EduProgramSubject.code())
            .order(EduProgramKind.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler programSpecializationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSpecialization.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EduProgramSubject programSubject = context.get(PARAM_PROGRAM_SUBJECT);

                if (null == programSubject) dql.where(nothing());
                else
                    dql.where(eq(property(alias, EduProgramSpecialization.programSubject()), value(programSubject)));
            }
        }
            .filter(EduProgramSpecialization.title())
            .order(EduProgramSpecialization.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler programFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder planDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanHigherProf.class, "p")
                    .where(eq(property("p", EppEduPlanHigherProf.programForm()), property(alias)))
                    .where(eq(property("p", EppEduPlanHigherProf.programKind()), commonValue(context.get(PARAM_PROGRAM_KIND))));

                IUISettings settings = context.get("settings");
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programSubject(), settings.get("programSubject"));

                dql.where(exists(planDql.buildQuery()));
            }
        }
            .filter(EduProgramForm.title())
            .order(EduProgramForm.code());
    }

    @Bean
    public IDefaultComboDataSourceHandler developConditionDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopCondition.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder planDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanHigherProf.class, "p")
                    .where(eq(property("p", EppEduPlanHigherProf.developCondition()), property(alias)))
                    .where(eq(property("p", EppEduPlanHigherProf.programKind()), commonValue(context.get(PARAM_PROGRAM_KIND))));

                IUISettings settings = context.get("settings");
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programSubject(), settings.get("programSubject"));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programForm(), settings.get("programForm"));

                dql.where(exists(planDql.buildQuery()));
            }
        }
            .filter(DevelopCondition.title())
            .order(DevelopCondition.code());
    }


    @Bean
    public IDefaultComboDataSourceHandler programTraitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramTrait.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder planDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanHigherProf.class, "p")
                    .where(eq(property("p", EppEduPlanHigherProf.programTrait()), property(alias)))
                    .where(eq(property("p", EppEduPlanHigherProf.programKind()), commonValue(context.get(PARAM_PROGRAM_KIND))));

                IUISettings settings = context.get("settings");
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programSubject(), settings.get("programSubject"));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programForm(), settings.get("programForm"));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.developCondition(), settings.get("developCondition"));

                dql.where(exists(planDql.buildQuery()));
            }
        }
            .filter(EduProgramTrait.title())
            .order(EduProgramTrait.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler developGridDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopGrid.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder planDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanHigherProf.class, "p")
                    .where(eq(property("p", EppEduPlanHigherProf.developGrid()), property(alias)))
                    .where(eq(property("p", EppEduPlanHigherProf.programKind()), commonValue(context.get(PARAM_PROGRAM_KIND))));

                IUISettings settings = context.get("settings");
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programSubject(), settings.get("programSubject"));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programForm(), settings.get("programForm"));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.developCondition(), settings.get("developCondition"));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanHigherProf.programTrait(), settings.get("programTrait"));

                dql.where(exists(planDql.buildQuery()));
            }
        }
            .filter(DevelopGrid.title())
            .order(DevelopGrid.developPeriod().priority())
            .order(DevelopGrid.title());
    }

    @Bean
    public ColumnListExtPoint eduPlanDSColumns(){
        return columnListExtPointBuilder("eduPlanDS")
            .addColumn(publisherColumn("title", EppEduPlanHigherProf.title().s()).order())
            .addColumn(publisherColumn("version", "title")
                .entityListProperty(EppEduPlanHigherProfDSHandler.VIEW_PROP_VERSION)
                .formatter(CollectionFormatter.COLLECTION_FORMATTER))
            .addColumn(textColumn("subjectIndex", EppEduPlanHigherProf.programSubject().subjectIndex().generation().title()).clickable(Boolean.FALSE).width("80px"))
            .addColumn(textColumn("programSubject", EppEduPlanHigherProf.programSubject().titleWithCode()).order())
            .addColumn(textColumn("qualification", EppEduPlanHigherProf.programQualification().title()).order())
            .addColumn(textColumn("orientation", EppEduPlanHigherProf.programOrientation().abbreviation()).visible("ui:showOrientation"))
            .addColumn(textColumn("developCombination", EppEduPlanHigherProf.developCombinationTitle().s()))
            .addColumn(textColumn("orgUnit", EppEduPlan.owner().fullTitle().s()).order())
            .addColumn(textColumn("yearsString", EppEduPlan.yearsString().s()).order())
            .addColumn(textColumn(EDUCATION_PERIOD, EDUCATION_PERIOD).order())
            .addColumn(textColumn(LABOR, LABOR))
            .addColumn(textColumn("state", EppEduPlanHigherProf.state().title().s()).order())
            .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditEduPlan")
                .permissionKey("ui:sec.edit_eppEduPlan_list"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEduPlan")
                .alert(new FormattedMessage("eduPlanDS.delete.alert", EppEduPlan.title().s()))
                .permissionKey("ui:sec.delete_eppEduPlan_list"))
            .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduPlanDSHandler()
    {
        return new EppEduPlanHigherProfDSHandler(getName());
    }
}