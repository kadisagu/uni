package ru.tandemservice.uniepp.base.bo.EppRegistryStructure;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author avedernikov
 * @since 26.08.2015
 */

@Configuration
public class EppRegistryStructureManager extends BusinessObjectManager
{
	public static EppRegistryStructureManager instance()
	{
		return instance(EppRegistryStructureManager.class);
	}
}