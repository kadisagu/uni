package ru.tandemservice.uniepp.entity.std.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Государственный образовательный стандарт
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStateEduStandardGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.std.EppStateEduStandard";
    public static final String ENTITY_NAME = "eppStateEduStandard";
    public static final int VERSION_HASH = -1632534381;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_SUBJECT = "programSubject";
    public static final String P_NUMBER = "number";
    public static final String P_REGISTRATION_NUMBER = "registrationNumber";
    public static final String L_STATE = "state";
    public static final String P_CONFIRM_DATE = "confirmDate";
    public static final String P_TITLE_POSTFIX = "titlePostfix";
    public static final String P_COMMENT = "comment";
    public static final String L_CONTENT = "content";
    public static final String P_GENERATION_TITLE = "generationTitle";
    public static final String P_TITLE = "title";

    private EduProgramSubject _programSubject;     // Направление подготовки
    private String _number;     // Номер
    private String _registrationNumber;     // Номер регистрации
    private EppState _state;     // Состояние
    private Date _confirmDate;     // Дата утверждения
    private String _titlePostfix;     // Постфикс
    private String _comment;     // Комментарий
    private DatabaseFile _content;     // Файл ГОСа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление подготовки. Свойство не может быть null.
     */
    public void setProgramSubject(EduProgramSubject programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null и должно быть уникальным.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Номер регистрации. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRegistrationNumber()
    {
        return _registrationNumber;
    }

    /**
     * @param registrationNumber Номер регистрации. Свойство не может быть null.
     */
    public void setRegistrationNumber(String registrationNumber)
    {
        dirty(_registrationNumber, registrationNumber);
        _registrationNumber = registrationNumber;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public EppState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(EppState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Дата утверждения.
     */
    public Date getConfirmDate()
    {
        return _confirmDate;
    }

    /**
     * @param confirmDate Дата утверждения.
     */
    public void setConfirmDate(Date confirmDate)
    {
        dirty(_confirmDate, confirmDate);
        _confirmDate = confirmDate;
    }

    /**
     * @return Постфикс.
     */
    @Length(max=255)
    public String getTitlePostfix()
    {
        return _titlePostfix;
    }

    /**
     * @param titlePostfix Постфикс.
     */
    public void setTitlePostfix(String titlePostfix)
    {
        dirty(_titlePostfix, titlePostfix);
        _titlePostfix = titlePostfix;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Файл ГОСа.
     */
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Файл ГОСа.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppStateEduStandardGen)
        {
            setProgramSubject(((EppStateEduStandard)another).getProgramSubject());
            setNumber(((EppStateEduStandard)another).getNumber());
            setRegistrationNumber(((EppStateEduStandard)another).getRegistrationNumber());
            setState(((EppStateEduStandard)another).getState());
            setConfirmDate(((EppStateEduStandard)another).getConfirmDate());
            setTitlePostfix(((EppStateEduStandard)another).getTitlePostfix());
            setComment(((EppStateEduStandard)another).getComment());
            setContent(((EppStateEduStandard)another).getContent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStateEduStandardGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStateEduStandard.class;
        }

        public T newInstance()
        {
            return (T) new EppStateEduStandard();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "programSubject":
                    return obj.getProgramSubject();
                case "number":
                    return obj.getNumber();
                case "registrationNumber":
                    return obj.getRegistrationNumber();
                case "state":
                    return obj.getState();
                case "confirmDate":
                    return obj.getConfirmDate();
                case "titlePostfix":
                    return obj.getTitlePostfix();
                case "comment":
                    return obj.getComment();
                case "content":
                    return obj.getContent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((EduProgramSubject) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "registrationNumber":
                    obj.setRegistrationNumber((String) value);
                    return;
                case "state":
                    obj.setState((EppState) value);
                    return;
                case "confirmDate":
                    obj.setConfirmDate((Date) value);
                    return;
                case "titlePostfix":
                    obj.setTitlePostfix((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "programSubject":
                        return true;
                case "number":
                        return true;
                case "registrationNumber":
                        return true;
                case "state":
                        return true;
                case "confirmDate":
                        return true;
                case "titlePostfix":
                        return true;
                case "comment":
                        return true;
                case "content":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "programSubject":
                    return true;
                case "number":
                    return true;
                case "registrationNumber":
                    return true;
                case "state":
                    return true;
                case "confirmDate":
                    return true;
                case "titlePostfix":
                    return true;
                case "comment":
                    return true;
                case "content":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "programSubject":
                    return EduProgramSubject.class;
                case "number":
                    return String.class;
                case "registrationNumber":
                    return String.class;
                case "state":
                    return EppState.class;
                case "confirmDate":
                    return Date.class;
                case "titlePostfix":
                    return String.class;
                case "comment":
                    return String.class;
                case "content":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStateEduStandard> _dslPath = new Path<EppStateEduStandard>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStateEduStandard");
    }
            

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Номер регистрации. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getRegistrationNumber()
     */
    public static PropertyPath<String> registrationNumber()
    {
        return _dslPath.registrationNumber();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getState()
     */
    public static EppState.Path<EppState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getConfirmDate()
     */
    public static PropertyPath<Date> confirmDate()
    {
        return _dslPath.confirmDate();
    }

    /**
     * @return Постфикс.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getTitlePostfix()
     */
    public static PropertyPath<String> titlePostfix()
    {
        return _dslPath.titlePostfix();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Файл ГОСа.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getGenerationTitle()
     */
    public static SupportedPropertyPath<String> generationTitle()
    {
        return _dslPath.generationTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EppStateEduStandard> extends EntityPath<E>
    {
        private EduProgramSubject.Path<EduProgramSubject> _programSubject;
        private PropertyPath<String> _number;
        private PropertyPath<String> _registrationNumber;
        private EppState.Path<EppState> _state;
        private PropertyPath<Date> _confirmDate;
        private PropertyPath<String> _titlePostfix;
        private PropertyPath<String> _comment;
        private DatabaseFile.Path<DatabaseFile> _content;
        private SupportedPropertyPath<String> _generationTitle;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new EduProgramSubject.Path<EduProgramSubject>(L_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Номер. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppStateEduStandardGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Номер регистрации. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getRegistrationNumber()
     */
        public PropertyPath<String> registrationNumber()
        {
            if(_registrationNumber == null )
                _registrationNumber = new PropertyPath<String>(EppStateEduStandardGen.P_REGISTRATION_NUMBER, this);
            return _registrationNumber;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getState()
     */
        public EppState.Path<EppState> state()
        {
            if(_state == null )
                _state = new EppState.Path<EppState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getConfirmDate()
     */
        public PropertyPath<Date> confirmDate()
        {
            if(_confirmDate == null )
                _confirmDate = new PropertyPath<Date>(EppStateEduStandardGen.P_CONFIRM_DATE, this);
            return _confirmDate;
        }

    /**
     * @return Постфикс.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getTitlePostfix()
     */
        public PropertyPath<String> titlePostfix()
        {
            if(_titlePostfix == null )
                _titlePostfix = new PropertyPath<String>(EppStateEduStandardGen.P_TITLE_POSTFIX, this);
            return _titlePostfix;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EppStateEduStandardGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Файл ГОСа.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getGenerationTitle()
     */
        public SupportedPropertyPath<String> generationTitle()
        {
            if(_generationTitle == null )
                _generationTitle = new SupportedPropertyPath<String>(EppStateEduStandardGen.P_GENERATION_TITLE, this);
            return _generationTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandard#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EppStateEduStandardGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EppStateEduStandard.class;
        }

        public String getEntityName()
        {
            return "eppStateEduStandard";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getGenerationTitle();

    public abstract String getTitle();
}
