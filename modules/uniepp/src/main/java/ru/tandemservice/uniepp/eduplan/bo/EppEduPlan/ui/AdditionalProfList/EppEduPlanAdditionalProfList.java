/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.AdditionalProfList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanAdditionalProf;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/28/14
 */
@Configuration
public class EppEduPlanAdditionalProfList extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("programSubjectDS", programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
            .addDataSource(selectDS("programFormDS", programFormDSHandler()))
            .addDataSource(selectDS("developConditionDS", developConditionDSHandler()))
            .addDataSource(selectDS("programTraitDS", programTraitDSHandler()))
            .addDataSource(selectDS("developGridDS", developGridDSHandler()))
            .addDataSource(EppStateManager.instance().eppStateDSConfig())
            .addDataSource(searchListDS("eduPlanDS", eduPlanDSColumns(), eduPlanDSHandler()))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                dql.where(exists(new DQLSelectBuilder()
                    .fromEntity(EppEduPlanAdditionalProf.class, "p")
                    .buildQuery()));
            }
        }
            .filter(EduProgramSubject.code())
            .filter(EduProgramSubject.title())
            .order(EduProgramSubject.code())
            .order(EduProgramKind.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler programFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder planDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanAdditionalProf.class, "p")
                    .where(eq(property("p", EppEduPlanAdditionalProf.programForm()), property(alias)));

                dql.where(exists(planDql.buildQuery()));
            }
        }
            .filter(EduProgramForm.title())
            .order(EduProgramForm.code());
    }

    @Bean
    public IDefaultComboDataSourceHandler developConditionDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopCondition.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder planDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanAdditionalProf.class, "p")
                    .where(eq(property("p", EppEduPlanAdditionalProf.developCondition()), property(alias)));

                IUISettings settings = context.get("settings");
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanAdditionalProf.programForm(), settings.get("programForm"));

                dql.where(exists(planDql.buildQuery()));
            }
        }
            .filter(DevelopCondition.title())
            .order(DevelopCondition.code());
    }


    @Bean
    public IDefaultComboDataSourceHandler programTraitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramTrait.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder planDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanAdditionalProf.class, "p")
                    .where(eq(property("p", EppEduPlanAdditionalProf.programTrait()), property(alias)));

                IUISettings settings = context.get("settings");
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanAdditionalProf.programForm(), settings.get("programForm"));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanAdditionalProf.developCondition(), settings.get("developCondition"));

                dql.where(exists(planDql.buildQuery()));
            }
        }
            .filter(EduProgramTrait.title())
            .order(EduProgramTrait.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler developGridDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopGrid.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder planDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanAdditionalProf.class, "p")
                    .where(eq(property("p", EppEduPlanAdditionalProf.developGrid()), property(alias)));

                IUISettings settings = context.get("settings");
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanAdditionalProf.programForm(), settings.get("programForm"));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanAdditionalProf.developCondition(), settings.get("developCondition"));
                FilterUtils.applySelectFilter(planDql, "p", EppEduPlanAdditionalProf.programTrait(), settings.get("programTrait"));

                dql.where(exists(planDql.buildQuery()));
            }
        }
            .filter(DevelopGrid.title())
            .order(DevelopGrid.developPeriod().priority())
            .order(DevelopGrid.title());
    }

    @Bean
    public ColumnListExtPoint eduPlanDSColumns(){
        return columnListExtPointBuilder("eduPlanDS")
            .addColumn(publisherColumn("title", EppEduPlanAdditionalProf.title().s()).order())
            .addColumn(publisherColumn("version", "title")
                .entityListProperty(EppEduPlanAdditionalProfDSHandler.VIEW_PROP_VERSION)
                .formatter(CollectionFormatter.COLLECTION_FORMATTER))
            .addColumn(textColumn("program", EppEduPlanAdditionalProf.eduProgram().title()).order())
            .addColumn(textColumn("developCombination", EppEduPlan.developCombinationTitle().s()))
            .addColumn(textColumn("orgUnit", EppEduPlan.owner().fullTitle().s()).order())
            .addColumn(textColumn("yearsString", EppEduPlan.yearsString().s()).order())
            .addColumn(textColumn("state", EppEduPlan.state().title().s()).order())
            .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, "onClickEditEduPlan")
                .permissionKey("ui:sec.edit_eppEduPlan_list"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEduPlan")
                .alert(new FormattedMessage("eduPlanDS.delete.alert", EppEduPlan.title().s()))
                .permissionKey("ui:sec.delete_eppEduPlan_list"))
            .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduPlanDSHandler()
    {
        return new EppEduPlanAdditionalProfDSHandler(getName());
    }
}