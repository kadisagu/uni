/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppProfActivityType.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.catalog.bo.EppProfActivityType.EppProfActivityTypeManager;

/**
 * @author Igor Belanov
 * @since 16.02.2017
 */
@Configuration
public class EppProfActivityTypeList extends BusinessComponentManager
{
    public static final String PROGRAM_KIND_DS = "programKindDS";
    public static final String PROGRAM_SUBJECT_INDEX_DS = "programSubjectIndexDS";
    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
    public static final String STATUS_DS = "statusDS";
    public static final String FROM_IMCA_DS = "fromIMCADS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(PROGRAM_KIND_DS, EppProfActivityTypeManager.instance().programKindDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_INDEX_DS, EppProfActivityTypeManager.instance().programSubjectIndexDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_DS, EppProfActivityTypeManager.instance().programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(selectDS(STATUS_DS, EppProfActivityTypeManager.instance().statusDSHandler()))
                .addDataSource(selectDS(FROM_IMCA_DS, EppProfActivityTypeManager.instance().fromIMCADSHandler()))
                .create();
    }
}
