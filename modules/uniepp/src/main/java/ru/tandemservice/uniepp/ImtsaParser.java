package ru.tandemservice.uniepp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.collect.Sets;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;

/**
 * выделен в отдельный проект
 * 
 * @author iolshvang
 * @since 16.12.11 13:48
 */
@Deprecated
@SuppressWarnings("all")
public class ImtsaParser
{
    static boolean checkCorrectCycleNames(PrintWriter pw, String eduPlanId, HashMap<String, LinkedHashSet<String>> blockHierarchyMap, Set<String>titles, String gosType, HashMap<String, String> documentCycleMap)
    {
        boolean isCorrect = true;
        Set<String> gos2Cycles = Sets.newHashSet("В", "ДС", "СД", "Р", "Ф");
        Set<String> gos3Cycles = Sets.newHashSet("Б", "В");
        for (String eduHsId : blockHierarchyMap.keySet())
        {
            for (String cycleId : blockHierarchyMap.get(eduHsId))
            {
                boolean isCycle = cycleId.indexOf(".") == -1;
                String name = isCycle ? cycleId : cycleId.substring(cycleId.indexOf(".")+1);
                if (isCycle && !titles.contains(documentCycleMap.get(name)))
                {
                    pw.println("В справочнике нет записи о цикле \"" + documentCycleMap.get(name) + "\"");
                    isCorrect = false;
                }
                if (!isCycle)
                {
                    if ((gosType.equals("3") && !gos3Cycles.contains(name)) || (gosType.equals("2") && !gos2Cycles.contains(name)))
                    {
                        pw.println("Неизвестный цикл \"" + name + "\" в файле " + eduPlanId + "-" + eduHsId);
                        isCorrect = false;
                    }

                }
            }
        }
        return isCorrect;
    }

    static boolean checkHierarchyCorrect(PrintWriter pw, String eduPlanId, HashMap<String, LinkedHashSet<String>> blockHierarchyMap, Set<String>titles, String gosType, HashMap<String, String> documentCycleMap)
    {
        boolean correctNames = checkCorrectCycleNames(pw, eduPlanId, blockHierarchyMap, titles, gosType, documentCycleMap);
        Object[] eduHsIds = blockHierarchyMap.keySet().toArray();
        Object[] firstBlockArr = blockHierarchyMap.get(eduHsIds[0]).toArray();
        for (Object eduHsId : eduHsIds)
        {
            Object[] blockArr = blockHierarchyMap.get((String)eduHsId).toArray();
            if (firstBlockArr.length != blockArr.length)
            {
                pw.println("Структура блоков УПв " + eduPlanId + " не совпадает");
                return false;
            }
            for (int compTitleInd = 0; compTitleInd < blockArr.length; compTitleInd++)
            {
                String name = (String)blockArr[compTitleInd];
                if (!(name.equals((String)firstBlockArr[compTitleInd])))
                {
                    pw.println("Структура блоков УПв " + eduPlanId + " не совпадает");
                    return false;
                }
            }
        }
        return correctNames;
    }

    static int setHierarchyIndexes(LinkedHashMap<String, TreeNode> hierarchyMap)
    {
        Queue<String> nodeQueue = new LinkedList<String>();
        int order = 1;
        for (String index : hierarchyMap.keySet())
            if (null == hierarchyMap.get(index).getParent())
            {
                nodeQueue.add(index);
                hierarchyMap.get(index).setIndex((order < 10 ? "0" : "")+order);
                order++;
            }
        int nextCycleNumber = order;
        while (!nodeQueue.isEmpty())
        {
            order = 1;
            for (Object child : hierarchyMap.get(nodeQueue.peek()).getChilds())
            {
                hierarchyMap.get((String)child).setIndex( hierarchyMap.get(nodeQueue.peek()).getIndex() + "." + (order < 10 ? "0" : "") + order);
                nodeQueue.add((String)child);
                order++;
            }
            nodeQueue.poll();
        }
        return nextCycleNumber;
    }

    static void importPlan (PrintWriter pw, String orgUnitId, String eduPlanId, String eduPlanHsId,  HashMap<String, Document> imtsaPlans, Table eduplanversionRowTable, Table eduplanversionTermTable, Table catalogEppRegistryStructure)
    {


        //соответствие индекса цикла циклу справочника
        int nextCycleIndex = 1;
        Map<AbstractMap.SimpleEntry<String, String>, String> hierarchyIndexMap = new HashMap<AbstractMap.SimpleEntry<String, String>, String>();

        final Map<AbstractMap.SimpleEntry<String, String>, String> catalogCycleMap = new HashMap<AbstractMap.SimpleEntry<String, String>, String>();

        for (Map<String, Object> row : catalogEppRegistryStructure) {
            final String text = (String) row.get("text");
            if (null == text) {
                continue;
            }
            catalogCycleMap.put(new AbstractMap.SimpleEntry(text.substring(0, 1), text.substring(text.indexOf(" ") + 1)), text);
        }

        Map<String, HashSet<String>> groupMap = new HashMap<String, HashSet<String>>();
        HashMap<String, String> documentCycleMap = new HashMap<String, String>();
        String gosType = "";

        //        System.out.println("Циклы:");
        for (Document document : imtsaPlans.values())
        {
            Node titul = document.getElementsByTagName("Титул").item(0);
            String documentGosType = "2";
            if (null != titul.getAttributes().getNamedItem("ТипГОСа") && titul.getAttributes().getNamedItem("ТипГОСа").getNodeValue().equals("3"))
                documentGosType = "3";
            if (gosType.equals(""))
                gosType = documentGosType;
            else if (!gosType.equals(documentGosType))
            {
                pw.println("не совпадают поколения ГОСов файлов блоков для УПв " + eduPlanId);
                return;
            }

            NodeList nodeList = document.getElementsByTagName("Цикл");
            for (int s = 0; s < nodeList.getLength(); s++)
            {
                Node fstNode = nodeList.item(s);
                if (fstNode.getNodeType() == Node.ELEMENT_NODE)
                {
                    if (fstNode.getParentNode().getNodeName().equals("АтрибутыЦиклов"))
                    {
                        String key = fstNode.getAttributes().getNamedItem("Абревиатура").getNodeValue();
                        if (null == fstNode.getAttributes().getNamedItem("Название"))
                        {
                            pw.println("цикл " + key + " УПв " + eduPlanId + " не был импортирован.");
                            continue;
                        }
                        String value = fstNode.getAttributes().getNamedItem("Название").getNodeValue();
                        System.out.println(key + "\t" + value);
                        documentCycleMap.put(key, value);
                    }
                }
            }
        }
        System.out.println();

        HashMap<String, List<Node>> blockDisciplineMap = new HashMap<String, List<Node>>();
        HashMap<String, List<Node>> blockPracticeMap = new HashMap<String, List<Node>>();
        HashMap<String, LinkedHashSet<String>> blockHierarchyMap = new HashMap<String, LinkedHashSet<String>>();
        HashMap<String, LinkedHashSet<String>> blockVariativeMap = new HashMap<String, LinkedHashSet<String>>();
        HashMap<String, LinkedHashSet<String>> blockDisciplineHierarchyMap = new HashMap<String, LinkedHashSet<String>>();

        for (String eduHsId : imtsaPlans.keySet())
        {
            //заполняем данные строк - дисциплин
            //        System.out.println("Дисциплины");
            LinkedHashSet<String> components = new LinkedHashSet<String>(), noDiscipline = new LinkedHashSet<String>();
            Set<String> lines = new LinkedHashSet<String>();
            Document document = imtsaPlans.get(eduHsId);
            NodeList nodeList = document.getElementsByTagName("Строка");
            for (int s = 0; s < nodeList.getLength(); s++)
            {
                Node fstNode = nodeList.item(s);
                if (fstNode.getNodeType() == Node.ELEMENT_NODE)
                {
                    if (fstNode.getParentNode().getNodeName().equals("СтрокиПлана"))
                    {
                        if (null == fstNode.getAttributes().getNamedItem("ИдетификаторДисциплины"))
                            continue;
                        if (!blockDisciplineMap.containsKey(eduHsId))
                            blockDisciplineMap.put(eduHsId, new ArrayList<Node>());
                        blockDisciplineMap.get(eduHsId).add(fstNode);
                        //                        System.out.println(s);
                        String index = fstNode.getAttributes().getNamedItem("ИдетификаторДисциплины").getNodeValue();
                        if (gosType.equals("2"))
                        {
                            index = index.replaceAll("\\.Б\\.", "\\.Ф\\.");
                            index = index.replaceAll("\\.В\\.", "\\.Р\\.");
                            index = index.replaceAll("\\.ДВ(\\d+)\\.", "\\.В\\.$1\\.");
                        }
                        index = index.replaceAll("\\.В(\\d+)\\.", "\\.В\\.$1\\.");

                        String[] hierarchy = index.split("\\.");
                        String componentName = "";
                        for (int struct = 0; struct < hierarchy.length; struct++)
                        {
                            componentName = componentName.equals("")?"":componentName+".";
                            componentName+=hierarchy[struct];
                            components.add(componentName);
                            if (struct < hierarchy.length - 1)
                            {
                                if (!groupMap.containsKey(eduHsId)) groupMap.put(eduHsId, new HashSet<String>());
                                groupMap.get(eduHsId).add(componentName);
                                //System.out.println(componentName);
                            }
                        }
                        lines.add(index);

                    }
                }
            }

            //practice

            nodeList = document.getElementsByTagName("УчебПрактика");
            for (int s = 0; s < nodeList.getLength(); s++)
            {
                Node fstNode = nodeList.item(s);
                if (fstNode.getNodeType() == Node.ELEMENT_NODE)
                    if (fstNode.getParentNode().getNodeName().equals("УчебПрактики") && null != fstNode.getAttributes().getNamedItem("Вид"))
                    {
                        if (!blockPracticeMap.containsKey(eduHsId))
                            blockPracticeMap.put(eduHsId, new ArrayList<Node>());
                        blockPracticeMap.get(eduHsId).add(fstNode);
                    }
            }
            nodeList = document.getElementsByTagName("ПрочаяПрактика");
            for (int s = 0; s < nodeList.getLength(); s++)
            {
                Node fstNode = nodeList.item(s);
                if (fstNode.getNodeType() == Node.ELEMENT_NODE)
                    if (fstNode.getParentNode().getNodeName().equals("ПрочиеПрактики") && null != fstNode.getAttributes().getNamedItem("Вид"))
                    {
                        if (!blockPracticeMap.containsKey(eduHsId))
                            blockPracticeMap.put(eduHsId, new ArrayList<Node>());
                        blockPracticeMap.get(eduHsId).add(fstNode);
                    }
            }
            nodeList = document.getElementsByTagName("ПрочаяПрактика");
            for (int s = 0; s < nodeList.getLength(); s++)
            {
                Node fstNode = nodeList.item(s);
                if (fstNode.getNodeType() == Node.ELEMENT_NODE)
                    if (fstNode.getParentNode().getNodeName().equals("ПреддипломныеПрактики") && null != fstNode.getAttributes().getNamedItem("Вид"))
                    {
                        if (!blockPracticeMap.containsKey(eduHsId))
                            blockPracticeMap.put(eduHsId, new ArrayList<Node>());
                        blockPracticeMap.get(eduHsId).add(fstNode);
                    }
            }

            //hierarchy
            blockDisciplineHierarchyMap.put(eduHsId, (LinkedHashSet<String>)components.clone());
            for (String index : lines)
                if (components.contains(index))
                {
                    components.remove(index);
                }
            blockHierarchyMap.put(eduHsId, components);


            LinkedHashSet<String> variative = new LinkedHashSet<String>();
            for (String index : components)
            {
                if (index.contains(".В."))
                {
                    variative.add(index);
                }
            }
            components.removeAll(variative);
            blockVariativeMap.put(eduHsId, variative);

            //            for (String index : lines)
            //                System.out.print(index + "\t");
            //            System.out.println();
            //            for (String comp : components)
            //                System.out.print(comp + "\t");

            LinkedHashMap<String, TreeNode> hierarchyMap= new LinkedHashMap<String, TreeNode>();
            Set<TreeNode> hierarchySet = new HashSet<TreeNode>();
            for (String hierarchElem : blockDisciplineHierarchyMap.get(eduHsId))
            {
                TreeNode<String> node = new TreeNode<String>(hierarchElem);
                hierarchySet.add(node);
                hierarchyMap.put(hierarchElem, node);

            }

            for (String index : hierarchyMap.keySet())
            {
                if (index.indexOf(".") != -1)
                {
                    hierarchyMap.get(index).setParent(hierarchyMap.get(index.substring(0, index.lastIndexOf("."))));
                    hierarchyMap.get(index.substring(0, index.lastIndexOf("."))).getChilds().add(index);
                }
                //hierarchyMap.get(index).setValue(documentCycleMap.get(index.substring(index.lastIndexOf(".")+1)));
            }

            nextCycleIndex = setHierarchyIndexes(hierarchyMap);
            for (String lineIndex : hierarchyMap.keySet())
                hierarchyIndexMap.put(new AbstractMap.SimpleEntry<String, String>(eduHsId, lineIndex), hierarchyMap.get(lineIndex).getIndex());

        }

        Set<String> catalogCycles = new HashSet<String>();
        for (AbstractMap.SimpleEntry<String, String> cycle : catalogCycleMap.keySet())
            if (cycle.getKey().equals(gosType))
                catalogCycles.add(cycle.getValue());

        if (!checkHierarchyCorrect(pw, eduPlanId, blockHierarchyMap, catalogCycles, gosType, documentCycleMap))
            return;


        HashMap<String, AbstractMap.SimpleEntry<String, String>> catalogCodeMap = new HashMap<String, AbstractMap.SimpleEntry<String, String>>();
        for (String docCycle : documentCycleMap.keySet())
            catalogCodeMap.put(docCycle, new AbstractMap.SimpleEntry(catalogCycleMap.get(new AbstractMap.SimpleEntry(gosType, documentCycleMap.get(docCycle))), documentCycleMap.get(docCycle)));

        if (gosType.equals("3"))
        {
            catalogCodeMap.put("Б", new AbstractMap.SimpleEntry(catalogCycleMap.get(new AbstractMap.SimpleEntry("3", "Базовая часть")), "Базовая часть"));
            catalogCodeMap.put("В", new AbstractMap.SimpleEntry(catalogCycleMap.get(new AbstractMap.SimpleEntry("3", "Вариативная часть")), "Вариативная часть"));
        }
        else
        {
            catalogCodeMap.put("В", new AbstractMap.SimpleEntry(catalogCycleMap.get(new AbstractMap.SimpleEntry("2", "Дисциплины и курсы по выбору студента, устанавливаемые ОУ")), "Дисциплины и курсы по выбору студента, устанавливаемые ОУ"));
            catalogCodeMap.put("ДС", new AbstractMap.SimpleEntry(catalogCycleMap.get(new AbstractMap.SimpleEntry("2", "Дисциплины специализации")), "Дисциплины специализации"));
            catalogCodeMap.put("СД", new AbstractMap.SimpleEntry(catalogCycleMap.get(new AbstractMap.SimpleEntry("2", "Специальные дисциплины")), "Специальные дисциплины"));
            catalogCodeMap.put("Р", new AbstractMap.SimpleEntry(catalogCycleMap.get(new AbstractMap.SimpleEntry("2", "Национально-региональный (вузовский) компонент")), "Национально-региональный (вузовский) компонент"));
            catalogCodeMap.put("Ф", new AbstractMap.SimpleEntry(catalogCycleMap.get(new AbstractMap.SimpleEntry("2", "Федеральный компонент")), "Федеральный компонент"));
        }




        //        for (String index : hierarchyMap.keySet())
        //        {
        //            TreeNode<String> node = hierarchyMap.get(index);
        //            System.out.println(node.getValue() + "\t" + (null!=node.getParent()?node.getParent().getValue():""));
        //        }

        //все проверили, можно выгружать

        try
        {


            System.out.println();
            System.out.println("***********");

            //     выгрузка структуры
            for(String index : blockHierarchyMap.get(blockHierarchyMap.keySet().toArray()[0]))
            {
                System.out.println(index);
                eduplanversionRowTable.addRow(
                        eduPlanId,
                        hierarchyIndexMap.get(new AbstractMap.SimpleEntry<String, String>((String)blockDisciplineHierarchyMap.keySet().toArray()[0], index)),       //index,
                        eduPlanHsId,
                        orgUnitId,
                        "s",
                        null==catalogCodeMap.get(index.substring(index.lastIndexOf(".")+1)).getKey()?"":catalogCodeMap.get(index.substring(index.lastIndexOf(".")+1)).getKey(),                                                //catalogCode
                            index.substring(index.lastIndexOf(".")+1),
                            catalogCodeMap.get(index.substring(index.lastIndexOf(".")+1)).getValue(),                                               //catalogTitle
                            ""
                );
            }

            /*
epp_eduplanversion_row

             * row_version_id ID УПв в hex
             * row_path номера элементов в иерархии от корня до текущего, разделенные через '.' из СтрокиПлана/Строка[@ИдентификаторДисциплины]
      Для дисциплин по выбору вмето "В[номер]" ставим "В.[номер]", т.к. в UNI иерархия глубже (есть части).
             * row_eduhs_id берем направления подготовки, которому принадлежит строка (hex id направления)
             * row_owner_id hex id подразделения, соответствующего ОУ
             * row_dsc_type_p указываем "r" - элемент реестра, чтобы сделать фейк
             * row_dsc_rel пусто (делаем фейк)
             * row_index_p СтрокиПлана/Строка[@ИдентификаторДисциплины] часть после последней точки
             * row_title_p СтрокиПлана/Строка[@Дис]
             * row_terms_p все СтрокиПлана/Строка/Сем[@Ном] через запятую в порядке возрастания

             * */
            int eduHsNumber = 0;
            for (String eduHsId : blockDisciplineMap.keySet())
            {
                eduHsNumber++;
                if (blockDisciplineMap.containsKey(eduHsId))
                    for (Node discipline : blockDisciplineMap.get(eduHsId))
                    {
                        try
                        {
                            if (null == discipline.getAttributes().getNamedItem("ИдетификаторДисциплины"))
                                continue;

                            String index = discipline.getAttributes().getNamedItem("ИдетификаторДисциплины").getNodeValue();
                            if (gosType.equals("2"))
                            {
                                index = index.replaceAll("\\.Б\\.", "\\.Ф\\.");
                                index = index.replaceAll("\\.В\\.", "\\.Р\\.");
                                index = index.replaceAll("\\.ДВ(\\d+)\\.", "\\.В\\.$1\\.");
                            }
                            index = index.replaceAll("\\.В(\\d+)\\.", "\\.В\\.$1\\.");

                            Map<String, String> caMap = new HashMap<String, String>();

                            Node  exams = discipline.getAttributes().getNamedItem("СемЭкз"),
                            test = discipline.getAttributes().getNamedItem("СемЗач"),
                            cWork = discipline.getAttributes().getNamedItem("КР"),
                            pWork = discipline.getAttributes().getNamedItem("КП");

                            String totalDisc = "0", srDisc = "0", zetDisc = "0";
                            try {totalDisc = discipline.getAttributes().getNamedItem("ПодлежитИзучению").getNodeValue();} catch(Throwable e) {}
                            try {srDisc = discipline.getAttributes().getNamedItem("СР").getNodeValue();} catch(Throwable e) {}
                            try {zetDisc = discipline.getAttributes().getNamedItem("КредитовНаДисциплину").getNodeValue();} catch(Throwable e) {}
                            String caDisc = "";

                            double lekDisc = 0., prDisc = 0., labDisc = 0.;

                            if (null != exams)
                                for (char semester : exams.getNodeValue().toCharArray())
                                {
                                    if (caMap.containsKey(""+semester))
                                        caMap.put(""+semester, caMap.get(semester) + ",");
                                    caMap.put(""+semester, (null == caMap.get(semester) ? "" : caMap.get(semester))+"Э");
                                    caDisc += (caDisc.equals("")?"":", ") + "Э";
                                }
                            if (null != test)
                                for (char semester : test.getNodeValue().toCharArray())
                                {
                                    if (caMap.containsKey(""+semester))
                                        caMap.put(""+semester, caMap.get(semester) + ",");
                                    caMap.put(""+semester, (null == caMap.get(semester) ? "" : caMap.get(semester))+"З");
                                    caDisc += (caDisc.equals("")?"":", ") + "З";
                                }
                            if (null != cWork)
                                for (char semester : cWork.getNodeValue().toCharArray())
                                {
                                    if (caMap.containsKey(""+semester))
                                        caMap.put(""+semester, caMap.get(semester) + ",");
                                    caMap.put(""+semester, (null == caMap.get(semester) ? "" : caMap.get(semester))+"КР");
                                    caDisc += (caDisc.equals("")?"":", ") + "КР";
                                }
                            if (null != pWork)
                                for (char semester : pWork.getNodeValue().toCharArray())
                                {
                                    if (caMap.containsKey(""+semester))
                                        caMap.put(""+semester, caMap.get(semester) + ",");
                                    caMap.put(""+semester, (null == caMap.get(semester) ? "" : caMap.get(semester))+"КП");
                                    caDisc += (caDisc.equals("")?"":", ") + "КП";
                                }

                            String terms = "";
                            for (int attrInd = 0; attrInd < discipline.getChildNodes().getLength(); attrInd++)
                            {
                                Node termNode = discipline.getChildNodes().item(attrInd);
                                if (termNode.getNodeName().equals("Сем") && null!=termNode.getAttributes().getNamedItem("Ном").getNodeValue())
                                {
                                    terms += (terms.equals("")?"":", ") + termNode.getAttributes().getNamedItem("Ном").getNodeValue();

                                    /*
epp_eduplanversion_term
                                     * row_version_id ID УПв
                                     * row_path номера элементов в иерархии от корня до текущего
                                     * term номер семестра начиная с 1 СтрокиПлана/Строка/Сем[@Ном]
                                     * weeks_p
                                     * size_p Сумма аудиторной и самостоятельной (см. ниже) + СтрокиПлана/Строка/Сем[@КСР]
                                     * labor_p СтрокиПлана/Строка/Сем[@ЗЕТ]
                                     * e_audit_p Сумма СтрокиПлана/Строка/Сем[@Лек]+СтрокиПлана/Строка/Сем[@Пр]+СтрокиПлана/Строка/Сем[@Лаб]+СтрокиПлана/Строка/Сем[@СРС]
                                     * a_lect_p СтрокиПлана/Строка/Сем[@Лек]
                                     * a_pract_p СтрокиПлана/Строка/Сем[@Пр]
                                     * a_lab_p СтрокиПлана/Строка/Сем[@Лаб]
                                     * e_self_p СтрокиПлана/Строка/Сем[@СРС]
                                     * actions_p Контрольные мероприятия через зпт из СтрокиПлана/Строка[@СемЭкз] СтрокиПлана/Строка[@СемЗач] СтрокиПлана/Строка[@КР] СтрокиПлана/Строка[@КП]. Используется поле abbreviation элемента справочника
                                     * */
                                    Node    ksrNode = termNode.getAttributes().getNamedItem("КСР"),
                                    lekNode = termNode.getAttributes().getNamedItem("Лек"),
                                    prNode = termNode.getAttributes().getNamedItem("Пр"),
                                    labNode = termNode.getAttributes().getNamedItem("Лаб"),
                                    srsNode = termNode.getAttributes().getNamedItem("СРС"),
                                    nomNode = termNode.getAttributes().getNamedItem("Ном"),
                                    zetNode = termNode.getAttributes().getNamedItem("ЗЕТ");

                                    double  ksr = (null==ksrNode?0:Double.valueOf(ksrNode.getNodeValue())),
                                    lek = (null==lekNode?0:Double.valueOf(lekNode.getNodeValue())),
                                    pr = (null==prNode?0:Double.valueOf(prNode.getNodeValue())),
                                    lab = (null==labNode?0:Double.valueOf(labNode.getNodeValue())),
                                    srs = (null==srsNode?0:Double.valueOf(srsNode.getNodeValue()));
                                    String  nom = (null==nomNode?"":nomNode.getNodeValue()),
                                    zet = (null==zetNode?"0":zetNode.getNodeValue());

                                    lekDisc += lek;
                                    prDisc += pr;
                                    labDisc += lab;

                                    String rowPath = hierarchyIndexMap.get(new AbstractMap.SimpleEntry<String, String>(eduHsId, index));
                                    String[] rowPathInd = rowPath.split("\\.");
                                    rowPath = "";
                                    for (int ind = 0; ind < rowPathInd.length; ind++)
                                    {
                                        if (ind > 0) rowPath += ".";
                                        if (ind == 2)
                                            rowPath += Integer.valueOf(rowPathInd[ind]) + 100 * eduHsNumber;
                                        else
                                            rowPath += rowPathInd[ind];
                                    }


                                    eduplanversionTermTable.addRow(
                                            eduPlanId,
                                            rowPath,//index,
                                            nom,
                                            "0",
                                            lek+pr+lab+srs+ksr,
                                            zet,
                                            lek+pr+lab,
                                            lek,
                                            pr,
                                            lab,
                                            srs,
                                            (caMap.containsKey(
                                                    ""+(Integer.valueOf(nom)<10
                                                            ?nom:
                                                                (""+(char)('A'+Integer.valueOf(nom)-10)))))

                                                                ?caMap.get(""+(Integer.valueOf(nom)<10
                                                                        ?nom:
                                                                            (""+(char)(('A'+Integer.valueOf(nom)-10))))):""
                                    );


                                }

                            }

                            String rowPath = hierarchyIndexMap.get(new AbstractMap.SimpleEntry<String, String>(eduHsId, index));
                            String[] rowPathInd = rowPath.split("\\.");
                            rowPath = "";
                            for (int ind = 0; ind < rowPathInd.length; ind++)
                            {
                                if (ind > 0) rowPath += ".";
                                if (ind == 2)
                                    rowPath += Integer.valueOf(rowPathInd[ind]) + 100 * eduHsNumber;
                                else
                                    rowPath += rowPathInd[ind];
                            }

                            eduplanversionTermTable.addRow(
                                    eduPlanId,
                                    rowPath,//index,
                                    "0",
                                    "0",
                                    totalDisc,
                                    zetDisc,
                                    lekDisc + prDisc + labDisc,
                                    lekDisc,
                                    prDisc,
                                    labDisc,
                                    srDisc,
                                    caDisc
                            );

                            eduplanversionRowTable.addRow(
                                    eduPlanId,
                                    rowPath, //index,
                                    eduHsId,
                                    orgUnitId,
                                    groupMap.get(eduHsId).contains(index)?"g":"r",
                                        groupMap.get(eduHsId).contains(index)?"":"eppRegistryDiscipline Дисциплины",
                                            index.substring(index.lastIndexOf(".")+1),
                                            discipline.getAttributes().getNamedItem("Дис").getNodeValue(),
                                            terms
                            );
                            System.out.println(discipline.getAttributes().getNamedItem("Дис").getNodeValue());
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                    }

                // Практики

                int index = nextCycleIndex;
                if (blockPracticeMap.containsKey(eduHsId))
                    for (Node practice : blockPracticeMap.get(eduHsId))
                    {
                        int prIndex = eduHsNumber * 100 + index;
                        String practiceType;
                        if (practice.getNodeName().equals("УчебПрактика"))
                            practiceType = "eppRegistryPractice.1 Учебная практика";
                        else if (practice.getParentNode().getNodeName().equals("ПрочиеПрактики"))
                            practiceType = "eppRegistryPractice.4 Прочая практика";
                        else practiceType = "eppRegistryPractice.3 Преддипломная практика";

                        eduplanversionRowTable.addRow(
                                eduPlanId,
                                prIndex, //index,
                                eduHsId,
                                orgUnitId,
                                "r",
                                practiceType,
                                prIndex,
                                practice.getAttributes().getNamedItem("Вид").getNodeValue(),
                                practice.getAttributes().getNamedItem("Сем").getNodeValue()
                        );

                        eduplanversionTermTable.addRow(
                                eduPlanId,
                                prIndex,
                                practice.getAttributes().getNamedItem("Сем").getNodeValue(),
                                practice.getAttributes().getNamedItem("Нед").getNodeValue(),
                                "0",
                                "0",
                                "0",
                                "0",
                                "0",
                                "0",
                                "0",
                                ""
                        );

                        index++;
                        System.out.println(practice.getAttributes().getNamedItem("Вид").getNodeValue());
                    }


                //группы дисциплин по выбору
                for (String variative : blockVariativeMap.get(eduHsId))
                {
                    String varIndex = hierarchyIndexMap.get(new AbstractMap.SimpleEntry<String, String>(eduHsId, variative));
                    String[] rowPathInd = varIndex.split("\\.");
                    varIndex = "";
                    for (int ind = 0; ind < rowPathInd.length; ind++)
                    {
                        if (ind > 0) varIndex += ".";
                        if (ind == 2)
                            varIndex += Integer.valueOf(rowPathInd[ind]) + 100 * eduHsNumber;
                        else
                            varIndex += rowPathInd[ind];
                    }
                    eduplanversionRowTable.addRow(
                            eduPlanId,
                            varIndex,
                            eduHsId,
                            orgUnitId,
                            "v",
                            "1",
                            varIndex.substring(varIndex.lastIndexOf(".")+1),
                            "",
                            ""
                    );
                }
            }
        }
        catch (Exception e)
        {
            pw.println("Ошибка записи в базу данных " + eduPlanId + "-" + eduPlanHsId);
            e.printStackTrace(pw);
            e.printStackTrace();
            return;
        }

    }


    public static void main(String[] args)
    {
        File logFile = new File("log.txt");
        PrintWriter pw = null;
        try { pw = new PrintWriter(logFile); } catch (FileNotFoundException e) { e.printStackTrace(); }

        Database mdb = null;
        try
        {
            final File mdbFile = new File("eduPlans.mdb");
            mdb = Database.open(mdbFile, false);
        }
        catch(Throwable e)
        {
            pw.println("Ошибка доступа к файлу базы данных");
            pw.flush();
            e.printStackTrace();
            return;
        }

        Table eduplanversionTable = null;
        Table eduplanversionRowTable = null;
        Table eduplanversionTermTable = null;
        Table eppEduplanversionBlock = null;
        Table catalogEppPlanStrusture = null;
        Table eduLvlHs = null;
        try
        {
            eduplanversionTable = mdb.getTable("epp_eduplanversion_t");
            eduplanversionRowTable = mdb.getTable("epp_eduplanversion_row_t");
            eduplanversionTermTable = mdb.getTable("epp_eduplanversion_row_term_t");
            eppEduplanversionBlock = mdb.getTable("epp_eduplanversion_block_t");
            catalogEppPlanStrusture = mdb.getTable("catalog_eppPlanStructure");
            eduLvlHs = mdb.getTable("edu_lvlhs_t");
        }
        catch(Exception e)
        {
            pw.println("Неверная структура базы данных");
            pw.flush();
            return;
        }

        Map<String, String> eduPlanHsMap = new HashMap<String, String>();
        for (Map<String, Object> row : eduplanversionTable) {
            String version_id = (String) row.get("id");
            if (null == version_id) {
                continue;
            }
            String eduhs_id = (String) row.get("plan_eduhs_id");
            if (null == eduhs_id) {
                continue;
            }

            eduPlanHsMap.put(version_id, eduhs_id);
        }

        Set<String> eduHsMap = new HashSet<String>();
        for (Map<String, Object> row : eppEduplanversionBlock) {
            String version_id = (String) row.get("version_id");
            if (null == version_id) {
                continue;
            }
            String eduhs_id = (String) row.get("eduhs_id");
            if (null == eduhs_id) {
                continue;
            }

            eduHsMap.add(version_id + "-" + eduhs_id);
        }

        Map<String, String> orgUnitMap = new HashMap<String, String>();
        for (Map<String, Object> row : eduLvlHs) {
            String id = (String) row.get("id");
            if (null == id) {
                continue;
            }
            String orgunit_id = (String) row.get("orgunit_id");
            if (null == orgunit_id) {
                continue;
            }

            orgUnitMap.put(id, orgunit_id);
        }


        File dir = new File("imtsa");
        boolean isDir = dir.isDirectory();
        if(isDir)
        {
            File [] imtsaFiles = dir.listFiles();
            HashMap<String, HashMap<String, Document>> imtsaDocMap = new HashMap<String, HashMap<String, Document>>();   //Long = row_eduhs_id
            boolean hasFileErrors = false;
            for (File file :imtsaFiles)
            {
                String fileName = file.getName();

                if (fileName.lastIndexOf(".") < 1)
                {
                    pw.println("Файл " + fileName + " был проигнорирован");
                    continue;
                }

                if (fileName.lastIndexOf(".") != -1)
                    fileName = fileName.substring(0, fileName.lastIndexOf("."));

                if (fileName.indexOf("-") == -1)
                {
                    pw.println("Неправильное название файла " + fileName);
                    hasFileErrors = true;
                }
                else
                {
                    String versionId = fileName.substring(0, fileName.indexOf("-"));
                    String eduHsId = fileName.substring(fileName.indexOf("-") + 1);
                    if (eduHsMap.contains(fileName))
                        eduHsMap.remove(fileName);
                    else
                    {
                        pw.println("Неправильное название файла " + fileName);
                        hasFileErrors = true;
                    }

                    Document doc = null;
                    try
                    {
                        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                        DocumentBuilder db = dbf.newDocumentBuilder();
                        doc = db.parse(file);
                        doc.getDocumentElement().normalize();
                    }
                    catch (Exception e)
                    {
                        pw.println("Неверная структура файла " + fileName);
                        hasFileErrors = true;
                    }

                    if (!imtsaDocMap.containsKey(versionId))
                        imtsaDocMap.put(versionId, new HashMap<String, Document>());
                    imtsaDocMap.get(versionId).put(eduHsId, doc);
                }

            }
            if (hasFileErrors)
            {
                pw.flush();
                return;
            }
            for (String versionId : imtsaDocMap.keySet())
                importPlan(pw, orgUnitMap.get((String)(imtsaDocMap.get(versionId).keySet().toArray()[0])), versionId, eduPlanHsMap.get(versionId), imtsaDocMap.get(versionId), eduplanversionRowTable, eduplanversionTermTable, catalogEppPlanStrusture);
        }
        try { mdb.close(); }
        catch(Exception e) { pw.println("Ошибка доступа к файлу базы данных"); e.printStackTrace(pw); }

        //        }
        //        catch (Exception e)
        //        {
        //            e.printStackTrace();
        //        }
        pw.flush();
    }

    private static class TreeNode<T>
    {
        T key;
        TreeNode parent;
        //        String value;
        String index;
        List<String> childs = new ArrayList<String>();

        private TreeNode()
        {
        }

        private TreeNode(T key)
        {
            this.key = key;
            this.parent = null;
            //            this.value = null;
            this.index = null;
        }

        public List<String> getChilds()
        {
            return childs;
        }

        public void setChilds(List<String> childs)
        {
            this.childs = childs;
        }

        public String getIndex()
        {
            return index;
        }

        public void setIndex(String index)
        {
            this.index = index;
        }

        //        public String getValue()
        //        {
        //            return value;
        //        }
        //
        //        public void setValue(String value)
        //        {
        //            this.value = value;
        //        }

        public TreeNode getParent()
        {
            return parent;
        }

        public void setParent(TreeNode parent)
        {
            this.parent = parent;
        }

        public T getKey()
        {
            return key;
        }

        public void setKey(T key)
        {
            this.key = key;
        }
    }

}


