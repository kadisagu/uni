/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphEduPlansTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "workGraph.id")
})
public class Model
{
    private EppWorkGraph _workGraph = new EppWorkGraph();
    private DynamicListDataSource<WorkGraphEduPlansRow> _dataSource;

    // Getters & Setters

    public EppWorkGraph getWorkGraph()
    {
        return this._workGraph;
    }

    public void setWorkGraph(final EppWorkGraph workGraph)
    {
        this._workGraph = workGraph;
    }

    public DynamicListDataSource<WorkGraphEduPlansRow> getDataSource()
    {
        return this._dataSource;
    }

    public void setDataSource(final DynamicListDataSource<WorkGraphEduPlansRow> dataSource)
    {
        this._dataSource = dataSource;
    }
}

