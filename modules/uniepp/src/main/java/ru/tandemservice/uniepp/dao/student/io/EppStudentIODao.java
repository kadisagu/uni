package ru.tandemservice.uniepp.dao.student.io;

import com.healthmarketscience.jackcess.*;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.mdbio.BaseIODao;
import ru.tandemservice.uni.dao.mdbio.IPersonIODao;
import ru.tandemservice.uni.dao.mdbio.IStudentIODao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.io.IEppEduPlanVersionIODao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.gen.EppStudent2EduPlanVersionGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EppStudentIODao extends BaseIODao implements IEppStudentIODao {

    @Override
    public void doImport_Student2EduPlanVersionRelations(final Database mdb) throws Exception
    {

        final Map<String, Long> map = IEppEduPlanVersionIODao.instance.get().doImport_EppEduPlanVersionList(mdb);

        final Table epp_student_eduplanversion_t = mdb.getTable("epp_student_eduplanversion_t");

        final Session session = this.getSession();

        BatchUtils.execute(epp_student_eduplanversion_t, 512, elements -> {

            for (final Map<String, Object> e: elements) {
                final Student s = (Student) session.load(Student.class, EppStudentIODao.this.id(e, "student_id"));
                final EppEduPlanVersion epv = (EppEduPlanVersion) session.load(EppEduPlanVersion.class, map.get(e.get("version_id")));

                if (null == EppStudentIODao.this.getByNaturalId(new EppStudent2EduPlanVersionGen.NaturalId(s, epv))) {
                    session.save(new EppStudent2EduPlanVersion(s, epv));
                }
            }


            System.out.print("+");
            session.flush();
            session.clear();
        });



    }

    @Override
    public void export_Student2EduplanVersionRelations(final Database mdb, final Collection<Long> studentIds) throws Exception
    {
        if (null != mdb.getTable("epp_student_eduplanversion_t")) { return; }

        final Session session = this.getSession();

        // идентификаторы связей
        final List<Long> relationIds = new ArrayList<>(studentIds.size());
        BatchUtils.execute(studentIds, 128, studentIds1 -> relationIds.addAll(
            new DQLSelectBuilder()
            .fromEntity(EppStudent2EduPlanVersion.class, "s2epv")
            .column(property(EppStudent2EduPlanVersion.id().fromAlias("s2epv")))
            .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
            .where(in(property(EppStudent2EduPlanVersion.student().id().fromAlias("s2epv")), studentIds1))
            .createStatement(session).<Long>list()
        ));

        // выгружаем данные УП по указанным студентам
        {
            final Set<Long> epvIds = new HashSet<>();
            BatchUtils.execute(relationIds, 128, relationIds1 -> epvIds.addAll(
                new DQLSelectBuilder()
                .fromEntity(EppStudent2EduPlanVersion.class, "s2epv")
                .predicate(DQLPredicateType.distinct)
                .column(property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("s2epv")))
                .where(in(property(EppStudent2EduPlanVersion.id().fromAlias("s2epv")), relationIds1))
                .createStatement(session).<Long>list()
            ));

            // выгружаем список планов
            IEppEduPlanVersionIODao.instance.get().export_EppEduPlanVersionList(mdb, epvIds, false /* нагрузку по планам не выгружаем */);
        }

        // выгружаем персон и студентов
        IPersonIODao.instance.get().export_PersonList(mdb, false);
        IStudentIODao.instance.get().export_StudentListBase(mdb);

        // создаем таблицу связей
        final Table epp_student_eduplanversion_t = new TableBuilder("epp_student_eduplanversion_t")
        .addColumn(new ColumnBuilder("student_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder("version_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
        .toTable(mdb);

        // выгружаем связи
        BatchUtils.execute(relationIds, 128, ids -> {
            try {
                final List<EppStudent2EduPlanVersion> blocks =  new DQLSelectBuilder()
                .fromEntity(EppStudent2EduPlanVersion.class, "e").column(property("e"))
                .where(in(property(EppStudent2EduPlanVersion.id().fromAlias("e")), ids))
                .createStatement(session).list();

                for (final EppStudent2EduPlanVersion block: blocks) {
                    epp_student_eduplanversion_t.addRow(
                        EppStudentIODao.this.id(block.getStudent().getId()),
                        EppStudentIODao.this.id(block.getEduPlanVersion().getId())
                    );
                }
            } catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });

    }

}
