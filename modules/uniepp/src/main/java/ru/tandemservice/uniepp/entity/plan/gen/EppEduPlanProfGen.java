package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебный план проф. образования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanProfGen extends EppEduPlan
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppEduPlanProf";
    public static final String ENTITY_NAME = "eppEduPlanProf";
    public static final int VERSION_HASH = -580906061;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE_LEVEL = "baseLevel";
    public static final String L_PROGRAM_SUBJECT = "programSubject";
    public static final String L_PROGRAM_QUALIFICATION = "programQualification";
    public static final String L_SUBJECT_QUALIFICATION = "subjectQualification";
    public static final String P_STD_PROGRAM_DURATION_YEARS = "stdProgramDurationYears";
    public static final String P_STD_PROGRAM_DURATION_MONTHS = "stdProgramDurationMonths";
    public static final String P_LABOR_AS_LONG = "laborAsLong";
    public static final String P_WEEKS_AS_LONG = "weeksAsLong";

    private EduLevel _baseLevel;     // На базе
    private EduProgramSubject _programSubject;     // Направление подготовки
    private EduProgramQualification _programQualification;     // Квалификация
    private EduProgramSubjectQualification _subjectQualification;     // Квалификации направления подготовки (для констрейнта)
    private Integer _stdProgramDurationYears;     // Число полных лет норм. срока обучения в очной форме
    private Integer _stdProgramDurationMonths;     // Число месяцев сверх полных лет норм. срока обучения в очной форме
    private Long _laborAsLong;     // Общая трудоемкость образовательной программы (в сотых долях ЗЕ)
    private Long _weeksAsLong;     // Общая трудоемкость образовательной программы (в сотых долях недель)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return На базе. Свойство не может быть null.
     */
    @NotNull
    public EduLevel getBaseLevel()
    {
        return _baseLevel;
    }

    /**
     * @param baseLevel На базе. Свойство не может быть null.
     */
    public void setBaseLevel(EduLevel baseLevel)
    {
        dirty(_baseLevel, baseLevel);
        _baseLevel = baseLevel;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление подготовки. Свойство не может быть null.
     */
    public void setProgramSubject(EduProgramSubject programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Квалификация. Свойство не может быть null.
     */
    @NotNull
    public EduProgramQualification getProgramQualification()
    {
        return _programQualification;
    }

    /**
     * @param programQualification Квалификация. Свойство не может быть null.
     */
    public void setProgramQualification(EduProgramQualification programQualification)
    {
        dirty(_programQualification, programQualification);
        _programQualification = programQualification;
    }

    /**
     * Связь направления подготовки и квалификации. Ссылка нужна для констрейнта, чтобы нельзя было эту связь удалить.
     * Можно ею заменить направление и квалификацию, но это снижение производительности (всегда лишний джойн) и рефакторинг.
     *
     * @return Квалификации направления подготовки (для констрейнта). Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubjectQualification getSubjectQualification()
    {
        return _subjectQualification;
    }

    /**
     * @param subjectQualification Квалификации направления подготовки (для констрейнта). Свойство не может быть null.
     */
    public void setSubjectQualification(EduProgramSubjectQualification subjectQualification)
    {
        dirty(_subjectQualification, subjectQualification);
        _subjectQualification = subjectQualification;
    }

    /**
     * @return Число полных лет норм. срока обучения в очной форме.
     */
    public Integer getStdProgramDurationYears()
    {
        return _stdProgramDurationYears;
    }

    /**
     * @param stdProgramDurationYears Число полных лет норм. срока обучения в очной форме.
     */
    public void setStdProgramDurationYears(Integer stdProgramDurationYears)
    {
        dirty(_stdProgramDurationYears, stdProgramDurationYears);
        _stdProgramDurationYears = stdProgramDurationYears;
    }

    /**
     * @return Число месяцев сверх полных лет норм. срока обучения в очной форме.
     */
    public Integer getStdProgramDurationMonths()
    {
        return _stdProgramDurationMonths;
    }

    /**
     * @param stdProgramDurationMonths Число месяцев сверх полных лет норм. срока обучения в очной форме.
     */
    public void setStdProgramDurationMonths(Integer stdProgramDurationMonths)
    {
        dirty(_stdProgramDurationMonths, stdProgramDurationMonths);
        _stdProgramDurationMonths = stdProgramDurationMonths;
    }

    /**
     * @return Общая трудоемкость образовательной программы (в сотых долях ЗЕ).
     */
    public Long getLaborAsLong()
    {
        return _laborAsLong;
    }

    /**
     * @param laborAsLong Общая трудоемкость образовательной программы (в сотых долях ЗЕ).
     */
    public void setLaborAsLong(Long laborAsLong)
    {
        dirty(_laborAsLong, laborAsLong);
        _laborAsLong = laborAsLong;
    }

    /**
     * @return Общая трудоемкость образовательной программы (в сотых долях недель).
     */
    public Long getWeeksAsLong()
    {
        return _weeksAsLong;
    }

    /**
     * @param weeksAsLong Общая трудоемкость образовательной программы (в сотых долях недель).
     */
    public void setWeeksAsLong(Long weeksAsLong)
    {
        dirty(_weeksAsLong, weeksAsLong);
        _weeksAsLong = weeksAsLong;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEduPlanProfGen)
        {
            setBaseLevel(((EppEduPlanProf)another).getBaseLevel());
            setProgramSubject(((EppEduPlanProf)another).getProgramSubject());
            setProgramQualification(((EppEduPlanProf)another).getProgramQualification());
            setSubjectQualification(((EppEduPlanProf)another).getSubjectQualification());
            setStdProgramDurationYears(((EppEduPlanProf)another).getStdProgramDurationYears());
            setStdProgramDurationMonths(((EppEduPlanProf)another).getStdProgramDurationMonths());
            setLaborAsLong(((EppEduPlanProf)another).getLaborAsLong());
            setWeeksAsLong(((EppEduPlanProf)another).getWeeksAsLong());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanProfGen> extends EppEduPlan.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlanProf.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppEduPlanProf is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "baseLevel":
                    return obj.getBaseLevel();
                case "programSubject":
                    return obj.getProgramSubject();
                case "programQualification":
                    return obj.getProgramQualification();
                case "subjectQualification":
                    return obj.getSubjectQualification();
                case "stdProgramDurationYears":
                    return obj.getStdProgramDurationYears();
                case "stdProgramDurationMonths":
                    return obj.getStdProgramDurationMonths();
                case "laborAsLong":
                    return obj.getLaborAsLong();
                case "weeksAsLong":
                    return obj.getWeeksAsLong();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "baseLevel":
                    obj.setBaseLevel((EduLevel) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((EduProgramSubject) value);
                    return;
                case "programQualification":
                    obj.setProgramQualification((EduProgramQualification) value);
                    return;
                case "subjectQualification":
                    obj.setSubjectQualification((EduProgramSubjectQualification) value);
                    return;
                case "stdProgramDurationYears":
                    obj.setStdProgramDurationYears((Integer) value);
                    return;
                case "stdProgramDurationMonths":
                    obj.setStdProgramDurationMonths((Integer) value);
                    return;
                case "laborAsLong":
                    obj.setLaborAsLong((Long) value);
                    return;
                case "weeksAsLong":
                    obj.setWeeksAsLong((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "baseLevel":
                        return true;
                case "programSubject":
                        return true;
                case "programQualification":
                        return true;
                case "subjectQualification":
                        return true;
                case "stdProgramDurationYears":
                        return true;
                case "stdProgramDurationMonths":
                        return true;
                case "laborAsLong":
                        return true;
                case "weeksAsLong":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "baseLevel":
                    return true;
                case "programSubject":
                    return true;
                case "programQualification":
                    return true;
                case "subjectQualification":
                    return true;
                case "stdProgramDurationYears":
                    return true;
                case "stdProgramDurationMonths":
                    return true;
                case "laborAsLong":
                    return true;
                case "weeksAsLong":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "baseLevel":
                    return EduLevel.class;
                case "programSubject":
                    return EduProgramSubject.class;
                case "programQualification":
                    return EduProgramQualification.class;
                case "subjectQualification":
                    return EduProgramSubjectQualification.class;
                case "stdProgramDurationYears":
                    return Integer.class;
                case "stdProgramDurationMonths":
                    return Integer.class;
                case "laborAsLong":
                    return Long.class;
                case "weeksAsLong":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlanProf> _dslPath = new Path<EppEduPlanProf>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlanProf");
    }
            

    /**
     * @return На базе. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getBaseLevel()
     */
    public static EduLevel.Path<EduLevel> baseLevel()
    {
        return _dslPath.baseLevel();
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Квалификация. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getProgramQualification()
     */
    public static EduProgramQualification.Path<EduProgramQualification> programQualification()
    {
        return _dslPath.programQualification();
    }

    /**
     * Связь направления подготовки и квалификации. Ссылка нужна для констрейнта, чтобы нельзя было эту связь удалить.
     * Можно ею заменить направление и квалификацию, но это снижение производительности (всегда лишний джойн) и рефакторинг.
     *
     * @return Квалификации направления подготовки (для констрейнта). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getSubjectQualification()
     */
    public static EduProgramSubjectQualification.Path<EduProgramSubjectQualification> subjectQualification()
    {
        return _dslPath.subjectQualification();
    }

    /**
     * @return Число полных лет норм. срока обучения в очной форме.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getStdProgramDurationYears()
     */
    public static PropertyPath<Integer> stdProgramDurationYears()
    {
        return _dslPath.stdProgramDurationYears();
    }

    /**
     * @return Число месяцев сверх полных лет норм. срока обучения в очной форме.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getStdProgramDurationMonths()
     */
    public static PropertyPath<Integer> stdProgramDurationMonths()
    {
        return _dslPath.stdProgramDurationMonths();
    }

    /**
     * @return Общая трудоемкость образовательной программы (в сотых долях ЗЕ).
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getLaborAsLong()
     */
    public static PropertyPath<Long> laborAsLong()
    {
        return _dslPath.laborAsLong();
    }

    /**
     * @return Общая трудоемкость образовательной программы (в сотых долях недель).
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getWeeksAsLong()
     */
    public static PropertyPath<Long> weeksAsLong()
    {
        return _dslPath.weeksAsLong();
    }

    public static class Path<E extends EppEduPlanProf> extends EppEduPlan.Path<E>
    {
        private EduLevel.Path<EduLevel> _baseLevel;
        private EduProgramSubject.Path<EduProgramSubject> _programSubject;
        private EduProgramQualification.Path<EduProgramQualification> _programQualification;
        private EduProgramSubjectQualification.Path<EduProgramSubjectQualification> _subjectQualification;
        private PropertyPath<Integer> _stdProgramDurationYears;
        private PropertyPath<Integer> _stdProgramDurationMonths;
        private PropertyPath<Long> _laborAsLong;
        private PropertyPath<Long> _weeksAsLong;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return На базе. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getBaseLevel()
     */
        public EduLevel.Path<EduLevel> baseLevel()
        {
            if(_baseLevel == null )
                _baseLevel = new EduLevel.Path<EduLevel>(L_BASE_LEVEL, this);
            return _baseLevel;
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new EduProgramSubject.Path<EduProgramSubject>(L_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Квалификация. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getProgramQualification()
     */
        public EduProgramQualification.Path<EduProgramQualification> programQualification()
        {
            if(_programQualification == null )
                _programQualification = new EduProgramQualification.Path<EduProgramQualification>(L_PROGRAM_QUALIFICATION, this);
            return _programQualification;
        }

    /**
     * Связь направления подготовки и квалификации. Ссылка нужна для констрейнта, чтобы нельзя было эту связь удалить.
     * Можно ею заменить направление и квалификацию, но это снижение производительности (всегда лишний джойн) и рефакторинг.
     *
     * @return Квалификации направления подготовки (для констрейнта). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getSubjectQualification()
     */
        public EduProgramSubjectQualification.Path<EduProgramSubjectQualification> subjectQualification()
        {
            if(_subjectQualification == null )
                _subjectQualification = new EduProgramSubjectQualification.Path<EduProgramSubjectQualification>(L_SUBJECT_QUALIFICATION, this);
            return _subjectQualification;
        }

    /**
     * @return Число полных лет норм. срока обучения в очной форме.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getStdProgramDurationYears()
     */
        public PropertyPath<Integer> stdProgramDurationYears()
        {
            if(_stdProgramDurationYears == null )
                _stdProgramDurationYears = new PropertyPath<Integer>(EppEduPlanProfGen.P_STD_PROGRAM_DURATION_YEARS, this);
            return _stdProgramDurationYears;
        }

    /**
     * @return Число месяцев сверх полных лет норм. срока обучения в очной форме.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getStdProgramDurationMonths()
     */
        public PropertyPath<Integer> stdProgramDurationMonths()
        {
            if(_stdProgramDurationMonths == null )
                _stdProgramDurationMonths = new PropertyPath<Integer>(EppEduPlanProfGen.P_STD_PROGRAM_DURATION_MONTHS, this);
            return _stdProgramDurationMonths;
        }

    /**
     * @return Общая трудоемкость образовательной программы (в сотых долях ЗЕ).
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getLaborAsLong()
     */
        public PropertyPath<Long> laborAsLong()
        {
            if(_laborAsLong == null )
                _laborAsLong = new PropertyPath<Long>(EppEduPlanProfGen.P_LABOR_AS_LONG, this);
            return _laborAsLong;
        }

    /**
     * @return Общая трудоемкость образовательной программы (в сотых долях недель).
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanProf#getWeeksAsLong()
     */
        public PropertyPath<Long> weeksAsLong()
        {
            if(_weeksAsLong == null )
                _weeksAsLong = new PropertyPath<Long>(EppEduPlanProfGen.P_WEEKS_AS_LONG, this);
            return _weeksAsLong;
        }

        public Class getEntityClass()
        {
            return EppEduPlanProf.class;
        }

        public String getEntityName()
        {
            return "eppEduPlanProf";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
