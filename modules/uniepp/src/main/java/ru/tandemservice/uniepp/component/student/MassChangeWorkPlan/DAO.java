/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.student.MassChangeWorkPlan;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

import java.util.*;
import java.util.Map.Entry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nkokorina
 * 
 */

public class DAO extends UniDao<Model> implements IDAO
{
    // доступные РУП и РУП(в)
    private List<EppWorkPlanBase> getPossibleWorkPlanList(final EppEduPlanVersion version, final Collection<EppWorkPlanBase> currentWorkPlans)
    {
        // берем то, что есть сейчас
        final Set<EppWorkPlanBase> workPlanSet = new HashSet<>(currentWorkPlans);

        // берем все РУП (по текущему УП)
        workPlanSet.addAll(this.getWorkPlanList(
            new DQLSelectBuilder().fromEntity(EppWorkPlan.class, "wp").column(property("wp")),
            version, "wp"
        ));

        // ... и их версии
        workPlanSet.addAll(this.getWorkPlanList(
            new DQLSelectBuilder().fromEntity(EppWorkPlanVersion.class, "wpv").column(property("wpv")).joinPath(DQLJoinType.inner, EppWorkPlanVersion.parent().fromAlias("wpv"), "wp"),
            version, "wpv"
        ));

        // сотритуем
        final List<EppWorkPlanBase> workPlanList = new ArrayList<>(workPlanSet);
        Collections.sort(workPlanList, EppWorkPlanBase.BY_YEAR_COMPARATOR);
        return workPlanList;
    }

    protected List<EppWorkPlan> getWorkPlanList(final DQLSelectBuilder dql, final EppEduPlanVersion version, final String alias)
    {
        dql.where(eq(property(EppWorkPlan.parent().eduPlanVersion().fromAlias("wp")), value(version)));
        if (IEppSettingsDAO.instance.get().getGlobalSettings().isCheckElementState()) {
            dql.where(eq(property(EppWorkPlanBase.state().code().fromAlias(alias)), value(EppState.STATE_ACCEPTED)));
        }
        dql.where(notIn(property(EppWorkPlanBase.state().code().fromAlias(alias)), EppState.INACTIVE_CODES));
        return dql.createStatement(this.getSession()).list();
    }

    // все актуальные связи студентов с РУП term.intValue -> { s2epv.id -> EppWorkPlanBase }
    private Map<Integer, Map<Long, EppWorkPlanBase>> getStudentTerm2WorkPlanMap(final Model model)
    {
        final Session session = this.getSession();

        final List<EppStudent2WorkPlan> student2WorkPlan = new DQLSelectBuilder()
        .fromEntity(EppStudent2WorkPlan.class, "s2wp")
        .column(property("s2wp")).fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("s2wp"), "s2epv")
        .where(isNull(property(EppStudent2WorkPlan.removalDate().fromAlias("s2wp"))))
        .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
        .where(in(property(EppStudent2EduPlanVersion.student().id().fromAlias("s2epv")), model.getStudentIds()))
        .createStatement(session).list();

        final Map<Integer, Map<Long, EppWorkPlanBase>> term2studentPlanMap = SafeMap.get(HashMap.class);
        for (final EppStudent2WorkPlan rel : student2WorkPlan) {
            term2studentPlanMap.get(rel.getGridTerm().getTerm().getIntValue()).put(rel.getStudentEduPlanVersion().getId(), rel.getWorkPlan());
        }

        return term2studentPlanMap;
    }

    @Override
    public void prepare(final Model model)
    {
        // связи студентов с уп
        final Map<Long, EppStudent2EduPlanVersion> relationMap = IEppEduPlanDAO.instance.get().getActiveStudentEduplanVersionRelationMap(model.getStudentIds());
        if (relationMap.isEmpty()) {
            throw new ApplicationException("К студенту не привязан учебный план.");
        }

        // проверяем, что к выбранным студентам привязан один и тот же УП(в)
        final Set<EppEduPlanVersion> versionSet = new HashSet<>();
        for (final EppStudent2EduPlanVersion rel : relationMap.values()) {
            versionSet.add(rel.getEduPlanVersion());
        }

        if (versionSet.size() != 1) {
            throw new ApplicationException("К студентам привязаны разные версии учебного плана.");
        }

        final EppEduPlanVersion currentVersion = versionSet.iterator().next();

        // семестры
        final List<DevelopGridTerm> termList = IDevelopGridDAO.instance.get().getDevelopGridTermList(currentVersion.getEduPlan().getDevelopGrid());
        model.setTermList(termList);

        // все актуальные связи студентов с РУП
        final Map<Integer, Map<Long, EppWorkPlanBase>> term2studentPlanMap = this.getStudentTerm2WorkPlanMap(model);

        // проверяем какие поля необходимо задизаблить
        // если у студентов есть отличия в привязанных РУП/РУП(в) на данный семестр, то этот и все предыдущие семестры дизаблятся
        final Set<Integer> disabledTermSet = new HashSet<>();
        final Set<Integer> yearSet = new HashSet<>();

        // для тех семестров, которые не дизаблим, формируем мап с одним подходящим РУП, которое будет отображаться в селекте
        final Map<Integer, EppWorkPlanBase> planTermMap = new HashMap<>();

        final int studentCount = model.getStudentIds().size();

        for (final Entry<Integer, Map<Long, EppWorkPlanBase>> entry : term2studentPlanMap.entrySet())
        {
            final Collection<EppWorkPlanBase> workPlanValues = entry.getValue().values();

            final boolean isStudentCountDifferent = (entry.getValue().keySet().size() != studentCount);
            final boolean isWorkPlanNotUnique = (!org.springframework.util.CollectionUtils.hasUniqueObject(workPlanValues));

            if (isStudentCountDifferent || isWorkPlanNotUnique)
            {
                disabledTermSet.add(entry.getKey());
                for (final EppWorkPlanBase wp : workPlanValues) {
                    yearSet.add(wp.getYear().getEducationYear().getIntValue());
                }
            }
            else
            {
                planTermMap.put(entry.getKey(), workPlanValues.iterator().next());
            }
        }

        // ищем семестр после которого значения РУП/РУП(в) для всех выбранных студентов одинаковы
        final Integer disabledTerm = disabledTermSet.isEmpty() ? -1 : Collections.max(disabledTermSet);

        // ищем максимальный год на который создан РУП в последнем задизабленном поле
        final Integer disabledYear =  yearSet.isEmpty() ? Integer.MIN_VALUE : Collections.max(yearSet);

        // доступные РУП и РУП(в)
        final List<EppWorkPlanBase> workPlanList = this.getPossibleWorkPlanList(currentVersion, planTermMap.values());

        final HashMap<Integer, RowWrapper> rowTermMap = new HashMap<>();
        for (final DevelopGridTerm term : termList)
        {
            final int currentTerm = term.getTermNumber();

            rowTermMap.put(
                currentTerm,
                new RowWrapper(planTermMap.get(currentTerm), (currentTerm <= disabledTerm))
                {
                    private final ISelectModel yearListModel = new FullCheckSelectModel()
                    {
                        @Override public ListResult findValues(final String filter) {
                            int maxYear = disabledYear;

                            for (final DevelopGridTerm term : termList) {
                                if (term.getTermNumber() < currentTerm) {
                                    final RowWrapper row = rowTermMap.get(term.getTermNumber());
                                    final EppYearEducationProcess year = (null == row ? null : row.getCurrentYear());
                                    if (null != year) {
                                        maxYear = Math.max(year.getEducationYear().getIntValue(), maxYear);
                                    }
                                }
                            }

                            final Set<EppYearEducationProcess> yearSet = new LinkedHashSet<>();
                            for (final EppWorkPlanBase wp : workPlanList) {
                                if ((wp.getTerm().getIntValue() == currentTerm) && (wp.getYear().getEducationYear().getIntValue() >= maxYear)) {
                                    yearSet.add(wp.getYear());
                                }
                            }

                            return new ListResult<>(yearSet);
                        }
                    };

                    private final ISelectModel workplanListModel = new FullCheckSelectModel(EppWorkPlanBase.P_SHORT_TITLE)
                    {
                        @Override public ListResult findValues(final String filter) {
                            final RowWrapper row = rowTermMap.get(currentTerm);
                            final EppYearEducationProcess year = (null == row ? null : row.getCurrentYear());
                            if (null == year) {
                                return ListResult.getEmpty();
                            }

                            final List<EppWorkPlanBase> planList = new ArrayList<>();
                            for (final EppWorkPlanBase wp : workPlanList) {
                                if ((wp.getTerm().getIntValue() == currentTerm) && (wp.getYear().equals(year))) {
                                    planList.add(wp);
                                }
                            }
                            return new ListResult<>(planList);
                        }

                        @Override
                        public String getLabelFor(Object value, int columnIndex)
                        {
                            String workPlanLabel =  super.getLabelFor(value, columnIndex);
                            return workPlanLabel + " | " +((EppWorkPlanBase)value).getBlock().getTitle();
                        }
                    };

                    @Override public ISelectModel getYearListModel() { return this.yearListModel; }
                    @Override public ISelectModel getWorkplanListModel() { return this.workplanListModel; }
                }
            );
        }

        model.setRowTermMap(rowTermMap);
    }

    @Override
    public void update(final Model model)
    {
        // все актуальные связи студентов с РУП
        final Map<Integer, Map<Long, EppWorkPlanBase>> term2studentPlanMap = this.getStudentTerm2WorkPlanMap(model);

        // то что изменил пользователь
        final Map<Integer, RowWrapper> rowTermMap = model.getRowTermMap();

        final Set<Integer> disabledSet = new HashSet<>();
        final Set<MultiKey> yearPartsSet = new HashSet<>();

        for (final Entry<Integer, RowWrapper> entry : rowTermMap.entrySet()) {
            if (entry.getValue().isDisabled()) {
                disabledSet.add(entry.getKey());
                final Collection<EppWorkPlanBase> workPlanList = term2studentPlanMap.get(entry.getKey()).values();
                for (final EppWorkPlanBase wp: workPlanList) {
                    final MultiKey key = new MultiKey(wp.getYear().getEducationYear().getId(), wp.getGridTerm().getPart().getId());
                    yearPartsSet.add(key); // добавляем все использованные годочасти для элементов, которые нельзя менять руками - они считаются занятыми
                }
            } else {
                // проверяем уникальность года и части года (для введенных значений)
                final EppWorkPlanBase wp = entry.getValue().getCurrentWorkPlan();
                if (null != wp) {
                    final MultiKey key = new MultiKey(wp.getYear().getEducationYear().getId(), wp.getGridTerm().getPart().getId());
                    if (!yearPartsSet.add(key)) {
                        ContextLocal.getErrorCollector().add(
                            "В "+wp.getYear().getEducationYear().getTitle()+" учебном году для части года «"+wp.getGridTerm().getPart().getTitle()+"» уже задан РУП.",
                            "year_"+entry.getKey(), "wp_"+entry.getKey()
                        );
                    }
                }
            }
        }

        if (ContextLocal.getErrorCollector().hasErrors()) {
            return;
        }

        // ищем семестр после которого значения РУП/РУП(в) изменялись пользователем
        final Integer disabledTerm = disabledSet.isEmpty() ? -1 : Collections.max(disabledSet);

        // активные связи студента с УП(в)
        final Map<Long, EppStudent2EduPlanVersion> s2epvMap = IEppEduPlanDAO.instance.get().getActiveStudentEduplanVersionRelationMap(model.getStudentIds());

        // итоговый мап с тем что должно быть у каждого студента
        final Map<Long, Set<EppWorkPlanBase>> student2workPlanSet = SafeMap.get(HashSet.class);

        for (final EppStudent2EduPlanVersion s2epv : s2epvMap.values())
        {
            for (final Entry<Integer, RowWrapper> entry : rowTermMap.entrySet())
            {
                final Integer currentTerm = entry.getKey();
                if (currentTerm <= disabledTerm)
                {
                    final Map<Long, EppWorkPlanBase> map = term2studentPlanMap.get(currentTerm);
                    final EppWorkPlanBase workPlan = (null == map) ? null : map.get(s2epv.getId());
                    student2workPlanSet.get(s2epv.getId()).add(workPlan);
                }
                else
                {
                    final EppWorkPlanBase workPlan = entry.getValue().getCurrentWorkPlan();
                    student2workPlanSet.get(s2epv.getId()).add(workPlan);
                }
            }
        }

        IEppWorkPlanDAO.instance.get().doUpdateStudentEpvWorkPlan(student2workPlanSet);
    }
}
