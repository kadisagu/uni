package ru.tandemservice.uniepp.base.bo.EppState.logic;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IPropertyMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateMutable;
import ru.tandemservice.uniepp.base.bo.EppState.EppStatePath;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;


/**
 * @author vdanilov
 */
public class EppDSetUpdateCheckEventListener
{
    private static final Logger log = Logger.getLogger(EppDSetUpdateCheckEventListener.class);

    public static final EventListenerLocker<IDSetEventListener> LOCKER = new EventListenerLocker<>();


    // собственно listener
    private static class Listener implements IDSetEventListener {

        private final String path;
        private final List<String> mutableFields;

        public Listener(final Collection<String> mutableFields, final String path) {
            this.mutableFields = new ArrayList<>(mutableFields);
            this.path = (null == path ? "" : (path +".")) + IEppStateObject.FIELD_STATE+".code";
        }

        private Listener(final Collection<String> mutableFields) {
            this(mutableFields, null);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onEvent(final DSetEvent event)
        {
            final IEntityMeta entityMeta = event.getMultitude().getEntityMeta();

            final Number count = new DQLSelectBuilder().fromEntity(entityMeta.getEntityClass(), "x")
            .where(in(property("x.id"), event.getMultitude().getInExpression()))
            .where(in(property("x", this.path), EppState.READ_ONLY_CODES))
            .column(DQLFunctions.count(property("x.id")))
            .createStatement(event.getContext())
            .uniqueResult();

            if ((null != count) && (count.intValue() > 0)) {
                final Set<String> properties = new HashSet<>(event.getMultitude().getAffectedProperties());
                properties.removeAll(this.mutableFields);
                properties.remove(IEntity.P_ID);
                properties.remove(IEntity.P_CLASS);
                if (properties.size() > 0)
                {
                    final EventListenerLocker.Handler handler = LOCKER.handler();
                    if (!handler.handle(this, entityMeta, properties, count, event))
                    {
                        // если ошибка не была обработкна - кидаем исключение
                        EppDSetUpdateCheckEventListener.log.info("event-error (" + entityMeta.getEntityClass().getSimpleName() + "): " + properties.toString());
                        EppState.throw_check_exception(
                                EppStateManager.instance().getProperty("error.state-check-listener", entityMeta.getTitle())
                        );
                    }
                }
            }

            // Проверка возможности смены состояния объекта
            if (event.getMultitude().isSingular() && IEppStateObject.class.isAssignableFrom(entityMeta.getEntityClass()) && event.getMultitude().getAffectedProperties().contains(IEppStateObject.FIELD_STATE))
            {
                IEppStateObject singularEntity = (IEppStateObject) event.getMultitude().getSingularEntity();
                EppState oldState = (EppState) event.getMultitude().getSingularEntityOldProperty(EppWorkPlanBase.L_STATE);
                EppState newState = (EppState) event.getMultitude().getSingularEntityNewProperty(EppWorkPlanBase.L_STATE);

                String err;
                if (null == oldState) {
                    err = "Невозможно изменить состояние объекта «" + singularEntity.getTitle() + "»: не задано текущее значение.";
                } else if (null == newState) {
                    err = "Невозможно изменить состояние объекта «" + singularEntity.getTitle() + "»: не задано новое значение.";
                } else {
                    err = EppStateManager.instance().eppStateDao().canChangeStateEppObject(oldState.getCode(), newState.getCode(), singularEntity);
                }

                if (StringUtils.isNotEmpty(err)) {
                    final EventListenerLocker.Handler handler = LOCKER.handler();
                    if (!handler.handle(this, entityMeta, ImmutableSet.of(IEppStateObject.FIELD_STATE), 1, event)) {
                        // если ошибка не была обработкна - кидаем исключение
                        EppDSetUpdateCheckEventListener.log.info("event-error (" + entityMeta.getEntityClass().getSimpleName() + "): try change state with error.");
                        EppState.throw_check_exception(err);
                    }
                }
            }
        }
    }

    // заполняет listenerы для объектов типа IEppState
    public void init()
    {
        try {
            for (final IEntityMeta meta: EntityRuntime.getInstance().getEntities())
            {
                if (meta.isAbstract()) { continue; }
                if (meta.isInterface()) { continue; }
                if (meta.getChildren().size() > 0) { continue; }

                final Class<?> entityClass = meta.getEntityClass();
                if (!IEppStateObject.class.isAssignableFrom(entityClass)) { continue; }

                final Set<String> mutableFields = getMutableFields(entityClass);
                if (null == mutableFields) { continue; }

                final IPropertyMeta p = meta.getProperty(IEppStateObject.FIELD_STATE);
                if (null != p)
                {
                    // это прямо объект в базе с таким свойством
                    final Listener listener = new Listener(mutableFields);
                    DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, entityClass, listener);
                    DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, entityClass, listener);
                }
                else
                {
                    // это некоторый объект, у которого есть ссылка
                    {
                        Class<?> k = entityClass;
                        while (!Object.class.equals(k)) {
                            final EppStatePath annotation = k.getAnnotation(EppStatePath.class);
                            if (null != annotation) {
                                final Listener listener = new Listener(mutableFields, annotation.value());
                                DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, entityClass, listener);
                                DSetEventManager.getInstance().registerListener(DSetEventType.beforeUpdate, entityClass, listener);
                                DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, entityClass, listener);
                                break;
                            }
                            k = k.getSuperclass();
                        }
                        if (Object.class.equals(k)) {
                            EppDSetUpdateCheckEventListener.log.warn("No @EppStatePath found for "+entityClass.getSimpleName()+".state");
                        }
                    }
                }
            }
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    protected Set<String> getMutableFields(final Class<?> entityClass)
    {
        final Set<String> mutableFields = new HashSet<>(Arrays.asList(IEppStateObject.FIELD_STATE, IEppStateObject.FIELD_COMMENT));

        Class<?> k = entityClass;
        while (!Object.class.equals(k)) {
            final EppStateMutable annotation = k.getAnnotation(EppStateMutable.class);
            if (null != annotation) {
                if (annotation.disable()) {
                    // отказ от обработки событий (для подклассов)
                    return null;
                }
                mutableFields.addAll(Arrays.asList(annotation.value()));
            }
            k = k.getSuperclass();
        }

        return mutableFields;
    }


}
