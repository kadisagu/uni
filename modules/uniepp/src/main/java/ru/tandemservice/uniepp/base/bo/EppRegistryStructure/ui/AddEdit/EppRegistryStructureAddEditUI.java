package ru.tandemservice.uniepp.base.bo.EppRegistryStructure.ui.AddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;

/**
 * @author avedernikov
 * @since 26.08.2015
 */

@Input({
	@Bind(key = "catalogItemId", binding = "holder.id"),
	@Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EppRegistryStructureAddEditUI extends BaseCatalogItemAddEditUI<EppRegistryStructure>
{
	@Override
	public String getAdditionalPropertiesPageName()
	{
		return "ru.tandemservice.uniepp.base.bo.EppRegistryStructure.ui.AddEdit.AdditProps";
	}
}
