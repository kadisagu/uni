/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppProfActivityType.ui.AddEdit;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicItemAddEdit.CatalogDynamicItemAddEditUI;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.catalog.bo.EppProfActivityType.EppProfActivityTypeManager;
import ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask.ui.AddEdit.EppProfessionalTaskAddEdit;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;

/**
 * @author Igor Belanov
 * @since 16.02.2017
 */
@State({
        @Bind(key = EppProfActivityTypeAddEditUI.EDU_PROGRAM_SUBJECT_ID, binding = "programSubjectId")
})
public class EppProfActivityTypeAddEditUI extends CatalogDynamicItemAddEditUI<EppProfActivityType>
{
    // как и в случае списка, к нам может прийти конкретное направление
    public static final String EDU_PROGRAM_SUBJECT_ID = "programSubjectId";

    private Long _programSubjectId;

    // поля, которые не будут записаны в сущность
    private EduProgramKind _programKind;
    private EduProgramSubjectIndex _programSubjectIndex;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        EduProgramSubject programSubject = getCatalogItem().getProgramSubject();
        if (programSubject != null || _programSubjectId != null)
        {
            if (programSubject == null)
                programSubject = DataAccessServices.dao().getNotNull(_programSubjectId);
            getCatalogItem().setProgramSubject(programSubject);
            setProgramSubjectIndex(programSubject.getSubjectIndex());
            setProgramKind(programSubject.getSubjectIndex().getProgramKind());
        }
        onChangeProgramKind();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppProfActivityTypeAddEdit.PROGRAM_SUBJECT_INDEX_DS.equals(dataSource.getName()) ||
                EppProfessionalTaskAddEdit.PROGRAM_SUBJECT_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppProfActivityTypeManager.PROP_PROGRAM_KIND, _programKind);
            dataSource.put(EppProfActivityTypeManager.PROP_PROGRAM_SUBJECT_INDEX, _programSubjectIndex);
        }
    }

    public boolean isDisableProgramSubjectFields()
    {
        // поля выключены либо если это форма редактирования
        // либо если мы работаем в пределах конкретного направления подготовки
        return !isAddForm() || (_programSubjectId != null);
    }

    public boolean isDisableInDepthStudy()
    {
        // чекбокс выключен если это форма редактирования
        return !isAddForm();
    }

    public boolean isVisibleInDepthStudy()
    {
        return getCatalogItem().getInDepthStudy() != null;
    }

    // listeners
    public void onChangeProgramKind()
    {
        // если вид ОП не установлен либо установлен не СПО, то чекбокс будет задизаблен и в базу запишется null
        if (_programKind == null || !_programKind.isProgramSecondaryProf())
        {
            getCatalogItem().setInDepthStudy(null);
        }
        // в противном случае (если мы установили вид ОП) этот чекбокс будет доступен, поэтому поставим дефолтный false
        else if (_programKind != null && getCatalogItem().getInDepthStudy() == null)
        {
            getCatalogItem().setInDepthStudy(false);
        }
    }

    // getters & setters
    public Long getProgramSubjectId()
    {
        return _programSubjectId;
    }

    public void setProgramSubjectId(Long programSubjectId)
    {
        _programSubjectId = programSubjectId;
    }

    public EduProgramKind getProgramKind()
    {
        return _programKind;
    }

    public void setProgramKind(EduProgramKind programKind)
    {
        _programKind = programKind;
    }

    public EduProgramSubjectIndex getProgramSubjectIndex()
    {
        return _programSubjectIndex;
    }

    public void setProgramSubjectIndex(EduProgramSubjectIndex programSubjectIndex)
    {
        _programSubjectIndex = programSubjectIndex;
    }
}
