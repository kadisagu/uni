/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.MassSplitEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.base.bo.EppRegistry.EppRegistryManager;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.List;

/**
 * @author Denis Katkov
 * @since 27.04.2016
 */
@Input({
        @Bind(key = "selectedRegistryElements", binding = "selectedRegistryElements")
})
public class EppRegistryMassSplitEditUI extends UIPresenter
{
    public static final String EDU_GROUP_SPLIT_VARIANT = "eduGroupSplitVariant";
    public static final String EDU_GROUP_SPLIT_BY_TYPE = "eduGroupSplitByType";
    public static final String EPP_GROUP_TYPES = "eppGroupTypes";
    private List<Long> selectedRegistryElements;

    public void cleanSettings()
    {
        if (getSettings().get(EDU_GROUP_SPLIT_VARIANT) == null) {
            clearSettings();
        } else if (getSettings().get(EDU_GROUP_SPLIT_BY_TYPE) == null || !((boolean) getSettings().get(EDU_GROUP_SPLIT_BY_TYPE))) {
            getSettings().set(EPP_GROUP_TYPES, null);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        cleanSettings();
    }

    public boolean isEduGroupSplitByTypeVisible()
    {
        return getSettings().get(EDU_GROUP_SPLIT_VARIANT) != null;
    }

    public boolean isGroupTypeVisible()
    {
        return (getSettings().get(EDU_GROUP_SPLIT_BY_TYPE) != null && ((boolean) getSettings().get(EDU_GROUP_SPLIT_BY_TYPE))) && isEduGroupSplitByTypeVisible();
    }

    public void onClickApply()
    {
        for (EppRegistryElement eppRegistryElement : getSelectedRegistryElements()) {
            eppRegistryElement.setEduGroupSplitVariant(getSettings().getDefault(EDU_GROUP_SPLIT_VARIANT, null));
            eppRegistryElement.setEduGroupSplitByType(getSettings().getDefault(EDU_GROUP_SPLIT_BY_TYPE, false));
        }
        // актуализируем связи мероприятия и ограничений делений по виду потока
        EppRegistryManager.instance().massSplitDAO().saveRegistryElementsSplitByTypeEduGroupRel(getSelectedRegistryElements(), getSettings().get(EPP_GROUP_TYPES));
        // актуализируем элементы реестра
        EppRegistryManager.instance().massSplitDAO().saveRegistryElements(getSelectedRegistryElements());
        deactivate();
    }

    public List<EppRegistryElement> getSelectedRegistryElements()
    {
        return DataAccessServices.dao().getList(EppRegistryElement.class, selectedRegistryElements);
    }

    public void setSelectedRegistryElements(List<Long> selectedRegistryElements)
    {
        this.selectedRegistryElements = selectedRegistryElements;
    }
}