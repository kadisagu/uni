/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.EppEPVBlockProfActivityTypeList.EppDevelopResultEppEPVBlockProfActivityTypeList;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.EppEpvRegRowProfessionalTaskList.EppDevelopResultEppEpvRegRowProfessionalTaskList;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.EppEpvRegRowSkillList.EppDevelopResultEppEpvRegRowSkillList;

/**
 * @author Igor Belanov
 * @since 28.02.2017
 */
@Configuration
public class EppDevelopResultTab extends BusinessComponentManager
{
    // tab panel
    public static final String TAB_PANEL = "eppDevelopResultTabPanel";

    public static final String TAB_PANEL_REGION_NAME = "eduPlanVersionDevelopResultRegion";

    // tab names
    public static final String EPP_EPV_BLOCK_PROF_ACTIVITY_TYPE_TAB = "eppEPVBlockProfActivityTypeTab";
    public static final String EPP_EPV_REG_ROW_PROFESSIONAL_TASK_TAB = "eppEpvRegRowProfessionalTaskTab";
    public static final String EPP_EPV_REG_ROW_SKILL_TAB = "eppEpvRegRowSkillTab";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .create();
    }

    @Bean
    public TabPanelExtPoint eppDevelopResultTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(EPP_EPV_BLOCK_PROF_ACTIVITY_TYPE_TAB, EppDevelopResultEppEPVBlockProfActivityTypeList.class)
                        .permissionKey("viewProfActivityType_eppEduPlanVersion")
                        .create())
                .addTab(componentTab(EPP_EPV_REG_ROW_PROFESSIONAL_TASK_TAB, EppDevelopResultEppEpvRegRowProfessionalTaskList.class)
                        .permissionKey("viewProfessionalTask_eppEduPlanVersion")
                        .create())
                .addTab(componentTab(EPP_EPV_REG_ROW_SKILL_TAB, EppDevelopResultEppEpvRegRowSkillList.class)
                        .permissionKey("viewSkill_eppEduPlanVersion")
                        .create())
                .create();
    }
}
