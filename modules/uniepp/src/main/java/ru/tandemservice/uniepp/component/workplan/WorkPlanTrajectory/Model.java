package ru.tandemservice.uniepp.component.workplan.WorkPlanTrajectory;

import java.util.Collection;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;


@Input({
    @Bind(key="ids", binding="ids", required=true)
})
public class Model
{
    public static final String COMPONENT = Model.class.getPackage().getName();

    private Collection<Long> ids;
    public Collection<Long> getIds() { return this.ids; }
    public void setIds(final Collection<Long> ids) { this.ids = ids; }

    private final StaticListDataSource<ViewWrapper<IEntity>> dataSource = new StaticListDataSource<ViewWrapper<IEntity>>();
    public StaticListDataSource<ViewWrapper<IEntity>> getDataSource() { return this.dataSource; }
}
