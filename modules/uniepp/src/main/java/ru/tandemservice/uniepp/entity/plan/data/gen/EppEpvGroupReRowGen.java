package ru.tandemservice.uniepp.entity.plan.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvHierarchyRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка УП (группа)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvGroupReRowGen extends EppEpvHierarchyRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow";
    public static final String ENTITY_NAME = "eppEpvGroupReRow";
    public static final int VERSION_HASH = 1637099702;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";
    public static final String P_NUMBER = "number";
    public static final String P_TITLE = "title";

    private EppEpvHierarchyRow _parent;     // Строка УП (группирующая)
    private String _number;     // Номер строки
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка УП (группирующая).
     */
    public EppEpvHierarchyRow getParent()
    {
        return _parent;
    }

    /**
     * @param parent Строка УП (группирующая).
     */
    public void setParent(EppEpvHierarchyRow parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Номер строки. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер строки. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEpvGroupReRowGen)
        {
            setParent(((EppEpvGroupReRow)another).getParent());
            setNumber(((EppEpvGroupReRow)another).getNumber());
            setTitle(((EppEpvGroupReRow)another).getTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvGroupReRowGen> extends EppEpvHierarchyRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvGroupReRow.class;
        }

        public T newInstance()
        {
            return (T) new EppEpvGroupReRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return obj.getParent();
                case "number":
                    return obj.getNumber();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "parent":
                    obj.setParent((EppEpvHierarchyRow) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                        return true;
                case "number":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return true;
                case "number":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return EppEpvHierarchyRow.class;
                case "number":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvGroupReRow> _dslPath = new Path<EppEpvGroupReRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvGroupReRow");
    }
            

    /**
     * @return Строка УП (группирующая).
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow#getParent()
     */
    public static EppEpvHierarchyRow.Path<EppEpvHierarchyRow> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Номер строки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EppEpvGroupReRow> extends EppEpvHierarchyRow.Path<E>
    {
        private EppEpvHierarchyRow.Path<EppEpvHierarchyRow> _parent;
        private PropertyPath<String> _number;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка УП (группирующая).
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow#getParent()
     */
        public EppEpvHierarchyRow.Path<EppEpvHierarchyRow> parent()
        {
            if(_parent == null )
                _parent = new EppEpvHierarchyRow.Path<EppEpvHierarchyRow>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Номер строки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppEpvGroupReRowGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppEpvGroupReRowGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EppEpvGroupReRow.class;
        }

        public String getEntityName()
        {
            return "eppEpvGroupReRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
