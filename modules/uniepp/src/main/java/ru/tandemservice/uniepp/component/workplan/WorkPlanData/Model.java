package ru.tandemservice.uniepp.component.workplan.WorkPlanData;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id", required = true) })
public class Model extends WorkPlanDataBaseModel
{
    private final EntityHolder<EppWorkPlanBase> holder = new EntityHolder<>();
    public EntityHolder<EppWorkPlanBase> getHolder() { return this.holder; }
    public Long getId() { return this.getHolder().getId(); }

    private CommonPostfixPermissionModel sec;
    public CommonPostfixPermissionModel getSec() { return this.sec; }
    public void setSec(final CommonPostfixPermissionModel sec) { this.sec = sec; }

    public boolean isNotCustom() {
        return getHolder().getValue().getWorkPlan().getCustomEduPlan() == null;
    }

    @Override protected Long getWorkPlanId() { return this.getId(); }
    @Override public boolean isEditable() { return !this.getHolder().getValue().getState().isReadOnlyState(); }
    @Override protected String getPermissionKeyEdit() { return (this.isEditable() ? this.getSec().getPermission("editContent") : null); }

}
