/* $Id:$ */
package ru.tandemservice.uniepp.dao.registry;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.*;
import jxl.write.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.registry.data.EppControlFormatter;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author oleyba
 * @since 5/30/12
 */
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=8945678")
public class EppRegistryCheckPrintDAO extends UniBaseDao implements IEppRegistryCheckPrintDAO
{
    public static final String WARN =
    "Внимание: на основе реестра дисциплин работают модули «Учебный процесс», «Сессия», «Журналы», «Нагрузка ППС».\n" +
    "Для корректной работы этих модулей необходимо, чтобы число читающих подразделений и число читаемых дисциплин на каждом совпадало с реальными значениями.\n" +
    "Красным подсвечены строки, для подразделений которых созданы не все дисциплины для строк УП(в).";
    public static final String OU_WARN =
    "Внимание: на основе реестра дисциплин работают модули «Учебный процесс», «Сессия», «Журналы», «Нагрузка ППС».\n" +
    "Для корректной работы этих модулей необходимо, чтобы данные по дисциплинам реестра совпадали с реально используемыми в учебном процессе.\n\n" +
    "Сравните список дисциплин и нагрузки по ним ниже с данными, используемыми на подразделении при расчете учебной нагрузки для ППС:\n" +
    "Проверьте, что в списке ровно столько различных дисциплин, сколько используется при расчете учебной нагрузки, и нагрузка по каждой дисциплине совпадает с используемой при расчете.\n\n" +
    "Красным подсвечены строки, в которых нагрузка по дисциплине предположительно внесена в систему неверно (нет общих часов, часов ауд. нагрузки, или сумма по видам ауд. нагрузки не совпадает с общей ауд. нагрузкой).\n" +
    "Жирным шрифтом выведены предполагаемые дубли дисциплин.";

    private static final EppControlFormatter<IEppRegElPartWrapper> eppControlFormatter = new EppControlFormatter<IEppRegElPartWrapper>(EppEduPlanVersionDataDAO.getActionTypeMap())
    {
        @Override
        protected boolean allow(IEppRegElPartWrapper element, EppControlActionType actionType)
        {
            return (actionType instanceof EppFControlActionType);
        }

        @Override
        protected int size(IEppRegElPartWrapper element, String actionFullCode)
        {
            return element.getActionSize(actionFullCode);
        }
    };

    @Override
    public byte[] printReport(Set<Long> orgUnits) throws WriteException, IOException
    {
        boolean printSummary = CollectionUtils.isEmpty(orgUnits);
        if (printSummary) {
            orgUnits = new LinkedHashSet<>(
                    getOrgUnitList().stream()
                            .map(w -> w.getOrgUnit().getId())
                            .collect(Collectors.toList())
            );
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        CellFormats formats = new CellFormats();

        if (printSummary)
            printSummary(workbook, formats);
        for (Long orgUnit: orgUnits) {
            printOrgUnit(orgUnit, workbook, formats);
        }

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }

    private void printOrgUnit(Long orgUnitId, WritableWorkbook workbook, CellFormats formats) throws WriteException
    {
        OrgUnit orgUnit = getNotNull(OrgUnit.class, orgUnitId);

        WritableSheet sheet = workbook.createSheet(orgUnit.getShortTitle(), workbook.getSheets().length);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);

        int excelColumn = 0;
        int excelRow = 0;

        sheet.addCell(new Label(excelColumn, excelRow, OU_WARN, formats.important));
        sheet.mergeCells(excelColumn, excelRow, excelColumn + 20, excelRow + 7);
        excelRow = excelRow + 10;

        sheet.setColumnView(0, 50);
        for (int part = 1; part <= 12 ; part ++) {
            for (int loadColumn = 0; loadColumn <= 5 ; loadColumn ++)
                sheet.setColumnView(4 + (part - 1) * 7 + loadColumn, 5);
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EppEpvRegistryRow.class, "fakeDisc")
        .where(eq(property(EppEpvRegistryRow.registryElementOwner().fromAlias("fakeDisc")), value(orgUnit)))
        .where(isNull(property(EppEpvRegistryRow.registryElement().fromAlias("fakeDisc"))))
        .where(eq(property(EppEpvRegistryRow.owner().eduPlanVersion().state().code().fromAlias("fakeDisc")), value(EppState.STATE_ACCEPTABLE)))
        .column("fakeDisc.id")
        ;

        Long fakeCount = dql.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();

        sheet.addCell(new Label(excelColumn++, excelRow, "Необработанных строк УП(в)", fakeCount > 0 ? formats.error : formats.def));
        sheet.addCell(new Label(excelColumn, excelRow, String.valueOf(fakeCount), fakeCount > 0 ? formats.error : formats.def));
        excelRow = excelRow + 3;
        excelColumn = 0;

        sheet.addCell(new Label(excelColumn, excelRow, "Дисциплины", formats.header_horizontal));
        sheet.mergeCells(excelColumn, excelRow, excelColumn + 12 * 7 + 3, excelRow);
        excelRow++;
        sheet.addCell(new Label(excelColumn, excelRow, "Название дисциплины", formats.header_horizontal));
        sheet.mergeCells(excelColumn, excelRow, excelColumn++, excelRow + 2);
        sheet.addCell(new Label(excelColumn, excelRow, "№ в реестре", formats.header_horizontal));
        sheet.mergeCells(excelColumn, excelRow, excelColumn++, excelRow + 2);
        sheet.addCell(new Label(excelColumn, excelRow, "Частей", formats.header_horizontal));
        sheet.mergeCells(excelColumn, excelRow, excelColumn++, excelRow + 2);
        sheet.addCell(new Label(excelColumn, excelRow, "Часов всего", formats.header_horizontal));
        sheet.mergeCells(excelColumn, excelRow, excelColumn++, excelRow + 2);
        sheet.addCell(new Label(excelColumn, excelRow, "Нагрузка по частям", formats.header_horizontal));
        sheet.mergeCells(excelColumn, excelRow, excelColumn + 12 * 7 - 1, excelRow++);
        List<String> loadTitles = Arrays.asList("Ауд.", "Лек.", "Пр.", "Лаб.", "Сам.", "Тр.", "ФК");
        for (int part = 1; part <= 12 ; part ++) {
            sheet.addCell(new Label(excelColumn, excelRow, String.valueOf(part) + " часть", formats.header_horizontal));
            sheet.mergeCells(excelColumn, excelRow, excelColumn + 6, excelRow);
            for (String loadTitle : loadTitles) {
                sheet.addCell(new Label(excelColumn++, excelRow + 1, loadTitle, formats.header_horizontal));
            }
        }
        excelRow++;

        dql = new DQLSelectBuilder()
        .fromEntity(EppRegistryElement.class, "disc")
        .where(ne(property(EppRegistryElement.state().code().fromAlias("disc")), value(EppState.STATE_ARCHIVED)))
        .where(eq(property(EppRegistryElement.owner().fromAlias("disc")), value(orgUnit)))
        .column("disc.id")
        ;
        Map<Long, IEppRegElWrapper> registryElementDataMap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(dql.createStatement(getSession()).<Long>list());
        List<IEppRegElWrapper> discList = new ArrayList<>(registryElementDataMap.values());
        Collections.sort(discList, (o1, o2) -> {
            int res = o1.getTitle().compareTo(o2.getTitle());
            if (res == 0)
                res = (int) (o2.getItem().getSize() - o1.getItem().getSize());
            return res;
        });

        IEppRegElWrapper previous = null;
        for (IEppRegElWrapper wrapper : discList) {
            WritableCellFormat rowFormat = getFormat(wrapper, previous, formats);
            previous = wrapper;
            excelColumn = 0;
            excelRow++;

            sheet.addCell(new Label(excelColumn++, excelRow, wrapper.getTitle(), rowFormat));
            sheet.addCell(new Label(excelColumn++, excelRow, wrapper.getItem().getNumber(), rowFormat));
            sheet.addCell(new jxl.write.Number(excelColumn++, excelRow, wrapper.getPartMap().size(), rowFormat));
            sheet.addCell(new jxl.write.Number(excelColumn++, excelRow, wrapper.getItem().getSizeAsDouble(), rowFormat));

            for (int part = 1; part <= 12 ; part ++) {
                IEppRegElPartWrapper partWrapper = wrapper.getPartMap().get(part);
                if (partWrapper == null) {
                    for (@SuppressWarnings("unused") String loadTitle : loadTitles) {
                        sheet.addCell(new Label(excelColumn++, excelRow, "-", rowFormat));
                    }
                    continue;
                }
                // List<String> loadTitles = Arrays.asList("Ауд.", "Лек.", "Пр.", "Лаб.", "Сам.", "Тр.", "Формы контроля");

                sheet.addCell(new jxl.write.Number(excelColumn++, excelRow, partWrapper.getLoadAsDouble(EppELoadType.FULL_CODE_AUDIT),  rowFormat));
                sheet.addCell(new jxl.write.Number(excelColumn++, excelRow, partWrapper.getLoadAsDouble(EppALoadType.FULL_CODE_TOTAL_LECTURES),  rowFormat));
                sheet.addCell(new jxl.write.Number(excelColumn++, excelRow, partWrapper.getLoadAsDouble(EppALoadType.FULL_CODE_TOTAL_PRACTICE),  rowFormat));
                sheet.addCell(new jxl.write.Number(excelColumn++, excelRow, partWrapper.getLoadAsDouble(EppALoadType.FULL_CODE_TOTAL_LABS),  rowFormat));
                sheet.addCell(new jxl.write.Number(excelColumn++, excelRow, partWrapper.getLoadAsDouble(EppELoadType.FULL_CODE_SELFWORK),  rowFormat));
                sheet.addCell(new jxl.write.Number(excelColumn++, excelRow, partWrapper.getLoadAsDouble(EppLoadType.FULL_CODE_LABOR),  rowFormat));
                sheet.addCell(new Label(excelColumn++, excelRow, eppControlFormatter.format(partWrapper), rowFormat));
            }
        }
    }

    public static final String[] all_load_codes = new String[] {EppALoadType.FULL_CODE_TOTAL_LABS, EppALoadType.FULL_CODE_TOTAL_PRACTICE, EppALoadType.FULL_CODE_TOTAL_LECTURES, EppELoadType.FULL_CODE_AUDIT, EppELoadType.FULL_CODE_SELFWORK};
    public static final String[] all_action_codes = new String[] {EppFControlActionType.FULL_CODE_CONTROL_ACTION_EXAM, EppFControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT, EppFControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK, EppFControlActionType.FULL_CODE_CONTROL_ACTION_EXAM_ACCUM, EppFControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF, EppFControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF, EppFControlActionType.FULL_CODE_CONTROL_ACTION_CONTROL_WORK};

    private WritableCellFormat getFormat(IEppRegElWrapper wrapper, IEppRegElWrapper previous, CellFormats formats)
    {
        boolean doubleDisc = false;
        if (null != previous) {
            doubleDisc = true;
            doubleDisc = doubleDisc && wrapper.getItem().getSize() == previous.getItem().getSize();
            doubleDisc = doubleDisc && wrapper.getPartMap().size() == previous.getPartMap().size();
            if (doubleDisc)
                for (Map.Entry<Integer, IEppRegElPartWrapper> entry : wrapper.getPartMap().entrySet()) {
                    IEppRegElPartWrapper partWrapper = entry.getValue();
                    IEppRegElPartWrapper prevPartWrapper = previous.getPartMap().get(entry.getKey());
                    if (null == prevPartWrapper) {
                        doubleDisc = false;
                        continue;
                    }
                    for (String code : all_load_codes)
                        doubleDisc = doubleDisc && UniEppUtils.eq(partWrapper.getLoadAsDouble(code), prevPartWrapper.getLoadAsDouble(code));
                    for (String code : all_action_codes)
                        doubleDisc = doubleDisc && partWrapper.getActionSize(code) == prevPartWrapper.getActionSize(code);
                }
        }

        boolean error = wrapper.getItem().getSize() == 0;
        for (IEppRegElPartWrapper partWrapper : wrapper.getPartMap().values()) {
            error = error || UniEppUtils.eq(partWrapper.getLoadAsDouble(EppELoadType.FULL_CODE_AUDIT), 0);
            error = error || UniEppUtils.eq(partWrapper.getLoadAsDouble(EppALoadType.FULL_CODE_TOTAL_LABS) + partWrapper.getLoadAsDouble(EppALoadType.FULL_CODE_TOTAL_LECTURES) + partWrapper.getLoadAsDouble(EppALoadType.FULL_CODE_TOTAL_PRACTICE), 0);
            error = error || !UniEppUtils.eq(partWrapper.getLoadAsDouble(EppALoadType.FULL_CODE_TOTAL_LABS) + partWrapper.getLoadAsDouble(EppALoadType.FULL_CODE_TOTAL_LECTURES) + partWrapper.getLoadAsDouble(EppALoadType.FULL_CODE_TOTAL_PRACTICE), partWrapper.getLoadAsDouble(EppELoadType.FULL_CODE_AUDIT));
            error = error || StringUtils.isEmpty(eppControlFormatter.format(partWrapper));
        }

        return formats.getFormat(doubleDisc, error);
    }

    private void printSummary(WritableWorkbook workbook, CellFormats formats) throws WriteException
    {
        WritableSheet sheet = workbook.createSheet("По вузу", workbook.getSheets().length);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);

        int excelColumn = 0;
        int excelRow = 0;

        sheet.addCell(new Label(excelColumn, excelRow, WARN, formats.important));
        sheet.mergeCells(excelColumn, excelRow, excelColumn + 3, excelRow + 3);
        excelRow = excelRow + 5;

        sheet.setColumnView(0, 50);
        sheet.setColumnView(1, 10);
        sheet.setColumnView(2, 10);
        sheet.setColumnView(3, 10);

        sheet.setRowView(5, 510);

        sheet.addCell(new Label(excelColumn++, excelRow, "Подразделение", formats.header_horizontal));
        sheet.addCell(new Label(excelColumn++, excelRow, "Дисциплин", formats.header_horizontal));
        sheet.addCell(new Label(excelColumn++, excelRow, "Необработанных\nстрок УП(в)", formats.header_horizontal));
        sheet.addCell(new Label(excelColumn, excelRow++, "Сумма", formats.header_horizontal));

        for (OrgUnitWrapper orgUnit: getOrgUnitList()) {
            excelColumn = 0;
            sheet.addCell(new Label(excelColumn++, excelRow, orgUnit.getOrgUnit().getFullTitle(), formats.def));
            sheet.addCell(new jxl.write.Number(excelColumn++, excelRow, orgUnit.getDiscCount(), formats.def));
            sheet.addCell(new jxl.write.Number(excelColumn++, excelRow, orgUnit.getFakeCount(), formats.def));
            sheet.addCell(new jxl.write.Number(excelColumn, excelRow++, orgUnit.getTotalCount(), formats.def));
        }
    }

    private List<OrgUnitWrapper> getOrgUnitList()
    {
        Map<Long, OrgUnitWrapper> wrapperMap = new HashMap<>();
        for (OrgUnit orgUnit : getList(OrgUnit.class)) {
            wrapperMap.put(orgUnit.getId(), new OrgUnitWrapper(orgUnit, 0, 0));
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(OrgUnit.class, "ou")
        .joinEntity("ou", DQLJoinType.left, EppRegistryElement.class, "disc", eq(property(EppRegistryElement.owner().fromAlias("disc")), property("ou")))
        .where(ne(property(EppRegistryElement.state().code().fromAlias("disc")), value(EppState.STATE_ARCHIVED)))
        .group(property("ou.id"))
        .column("ou.id")
        .column(DQLFunctions.count("disc.id"))
        ;

        for (Object[] row : dql.createStatement(getSession()).<Object[]>list()) {
            wrapperMap.get((Long) row[0]).discCount = ((Long) row[1]).intValue();
        }

        dql = new DQLSelectBuilder()
        .fromEntity(OrgUnit.class, "ou")
        .joinEntity("ou", DQLJoinType.left, EppEpvRegistryRow.class, "fakeDisc", eq(property(EppEpvRegistryRow.registryElementOwner().fromAlias("fakeDisc")), property("ou")))
        .where(isNull(property(EppEpvRegistryRow.registryElement().fromAlias("fakeDisc"))))
        .where(eq(property(EppEpvRegistryRow.owner().eduPlanVersion().state().code().fromAlias("fakeDisc")), value(EppState.STATE_ACCEPTABLE)))
        .group(property("ou.id"))
        .column("ou.id")
        .column(DQLFunctions.count("fakeDisc.id"))
        ;

        for (Object[] row : dql.createStatement(getSession()).<Object[]>list()) {
            wrapperMap.get((Long) row[0]).fakeCount = ((Long) row[1]).intValue();
        }

        return wrapperMap.values().stream()
                .filter(wrapper -> wrapper.getTotalCount() > 0)
                .sorted(OrgUnitWrapper.totalComparator)
                .collect(Collectors.toList());
    }

    private static class CellFormats
    {
        final WritableCellFormat def;
        final WritableCellFormat header_horizontal;
        final WritableCellFormat important;
        final WritableCellFormat error;
        final WritableCellFormat doubleDisc;
        final WritableCellFormat doubleDiscError;

        private CellFormats() throws WriteException
        {
            def = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));
            def.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
            def.setAlignment(jxl.format.Alignment.LEFT);

            header_horizontal = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));
            header_horizontal.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
            header_horizontal.setAlignment(jxl.format.Alignment.LEFT);
            header_horizontal.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);

            important = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10));
            important.setBorder(jxl.format.Border.ALL, BorderLineStyle.DOUBLE, Colour.DARK_RED);
            important.setAlignment(Alignment.LEFT);
            important.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
            important.setBackground(Colour.VERY_LIGHT_YELLOW);
            important.setWrap(true);

            error = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));
            error.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
            error.setAlignment(jxl.format.Alignment.LEFT);
            error.setBackground(Colour.ROSE);

            doubleDisc = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
                Colour.DARK_RED));
            doubleDisc.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
            doubleDisc.setAlignment(jxl.format.Alignment.LEFT);

            doubleDiscError = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
                Colour.DARK_RED));
            doubleDiscError.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
            doubleDiscError.setAlignment(jxl.format.Alignment.LEFT);
            doubleDiscError.setBackground(Colour.ROSE);
        }

        public WritableCellFormat getFormat(boolean isDoubleDisc, boolean isError) {
            if (isDoubleDisc && isError)
                return doubleDiscError;
            if (isDoubleDisc)
                return doubleDisc;
            if (isError)
                return error;
            return def;
        }
    }

    private static class OrgUnitWrapper
    {
        private OrgUnit orgUnit;
        private int discCount;
        private int fakeCount;

        private OrgUnitWrapper(OrgUnit orgUnit, int discCount, int fakeCount)
        {
            this.orgUnit = orgUnit;
            this.discCount = discCount;
            this.fakeCount = fakeCount;
        }

        public OrgUnit getOrgUnit() { return orgUnit; }
        public int getDiscCount() { return discCount; }
        public int getFakeCount() { return fakeCount; }
        public int getTotalCount() { return getDiscCount() + getFakeCount(); }

        // private static final Comparator<OrgUnitWrapper> discComparator = new Comparator<OrgUnitWrapper>() { @Override public int compare(OrgUnitWrapper o1, OrgUnitWrapper o2) { return o2.getDiscCount() - o1.getDiscCount(); } };
        // private static final Comparator<OrgUnitWrapper> fakeComparator = new Comparator<OrgUnitWrapper>() { @Override public int compare(OrgUnitWrapper o1, OrgUnitWrapper o2) { return o2.getFakeCount() - o1.getFakeCount(); } };
        private static final Comparator<OrgUnitWrapper> totalComparator = (o1, o2) -> o2.getTotalCount() - o1.getTotalCount();
    }
}

