package ru.tandemservice.uniepp.entity.workplan;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanRowGen;

/**
 * Запись РУП (базовая)
 */
public abstract class EppWorkPlanRow extends EppWorkPlanRowGen implements ITitled, IEppWorkPlanRow
{
    public abstract String getFormattedLoad();


    @Override
    @EntityDSLSupport(parts={EppWorkPlanRowGen.P_TITLE, EppWorkPlanRowGen.P_TITLE_APPENDIX})
    public String getDisplayableTitle()
    {
        final StringBuilder sb = new StringBuilder(this.getTitle()).append(" (")
        .append(this.getFormattedLoad()).append(", ")
        .append(this.getRegistryElementPartNumber());

        final int parts = this.getRegistryElementPartAmount();
        if (parts > 0) { sb.append("/").append(parts); }

        final String titleAppendix = StringUtils.trimToNull(this.getTitleAppendix());
        if (null != titleAppendix) { sb.append(", ").append(titleAppendix); }

        return sb.append(")").toString();
    }

    @Override
    @EntityDSLSupport
    public String getTitleWithIndex() {
        return StringUtils.trimToEmpty(this.getNumber()) + " " + this.getDisplayableTitle();
    }

    @Override
    public String getDescriptionString() {
        return this.getNumber() + '\n' + this.getKind().getCode() + (this.isReattestation() ? "t" : (isReexamination() ? "f" : ""));
    }

    /**
     * @return переаттестация
     */
    public boolean isReattestation() {
        return Boolean.TRUE.equals(getNeedRetake());
    }

    /**
     * @return перезачтение
     */
    public boolean isReexamination() {
        return Boolean.FALSE.equals(getNeedRetake());
    }
}