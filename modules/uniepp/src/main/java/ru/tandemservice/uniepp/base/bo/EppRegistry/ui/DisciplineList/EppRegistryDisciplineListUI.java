/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.DisciplineList;

import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractListUI;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;

/**
 * @author Irina Ugfeld
 * @since 04.03.2016
 */
public class EppRegistryDisciplineListUI extends EppRegistryAbstractListUI {

    @Override public String getMenuPermission() { return "menuEppDisciplineRegistry"; }
    @Override public String getPermissionPrefix() { return "eppDiscipline_list"; }
    @Override public Class getElementClass() { return EppRegistryDiscipline.class; }

}