package ru.tandemservice.uniepp.entity.contract.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Обязательство обучения
 *
 * Обязательство обучить человека (dst - обучаемый)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppCtrEducationPromiceGen extends CtrContractPromice
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice";
    public static final String ENTITY_NAME = "eppCtrEducationPromice";
    public static final int VERSION_HASH = 1650393253;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_CATEGORY = "studentCategory";
    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";

    private StudentCategory _studentCategory;     // Категория
    private EppEduPlanVersion _eduPlanVersion;     // Версия учебного плана
    private EducationOrgUnit _educationOrgUnit;     // Направление подготовки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Категория. Свойство не может быть null.
     */
    @NotNull
    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    /**
     * @param studentCategory Категория. Свойство не может быть null.
     */
    public void setStudentCategory(StudentCategory studentCategory)
    {
        dirty(_studentCategory, studentCategory);
        _studentCategory = studentCategory;
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion Версия учебного плана. Свойство не может быть null.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Направление подготовки. Свойство не может быть null.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppCtrEducationPromiceGen)
        {
            setStudentCategory(((EppCtrEducationPromice)another).getStudentCategory());
            setEduPlanVersion(((EppCtrEducationPromice)another).getEduPlanVersion());
            setEducationOrgUnit(((EppCtrEducationPromice)another).getEducationOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppCtrEducationPromiceGen> extends CtrContractPromice.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppCtrEducationPromice.class;
        }

        public T newInstance()
        {
            return (T) new EppCtrEducationPromice();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "studentCategory":
                    return obj.getStudentCategory();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "studentCategory":
                    obj.setStudentCategory((StudentCategory) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentCategory":
                        return true;
                case "eduPlanVersion":
                        return true;
                case "educationOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentCategory":
                    return true;
                case "eduPlanVersion":
                    return true;
                case "educationOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "studentCategory":
                    return StudentCategory.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppCtrEducationPromice> _dslPath = new Path<EppCtrEducationPromice>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppCtrEducationPromice");
    }
            

    /**
     * @return Категория. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice#getStudentCategory()
     */
    public static StudentCategory.Path<StudentCategory> studentCategory()
    {
        return _dslPath.studentCategory();
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    public static class Path<E extends EppCtrEducationPromice> extends CtrContractPromice.Path<E>
    {
        private StudentCategory.Path<StudentCategory> _studentCategory;
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Категория. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice#getStudentCategory()
     */
        public StudentCategory.Path<StudentCategory> studentCategory()
        {
            if(_studentCategory == null )
                _studentCategory = new StudentCategory.Path<StudentCategory>(L_STUDENT_CATEGORY, this);
            return _studentCategory;
        }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

        public Class getEntityClass()
        {
            return EppCtrEducationPromice.class;
        }

        public String getEntityName()
        {
            return "eppCtrEducationPromice";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
