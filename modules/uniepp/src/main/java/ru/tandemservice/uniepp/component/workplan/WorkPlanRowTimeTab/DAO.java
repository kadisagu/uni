package ru.tandemservice.uniepp.component.workplan.WorkPlanRowTimeTab;

import org.apache.commons.lang.mutable.MutableLong;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.dao.year.IEppYearDAO;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;

import java.util.*;

/**
 * 
 * @author nkokorina
 * @since 18.03.2010
 */

public class DAO extends UniBaseDao implements IDAO
{

    private long index(final Date date) {
        return (date.getTime()/1000/60/60/24);
    }

    private long time(final Date date, final MutableLong wp) {
        return (null == date ? wp.longValue() : this.index(date));
    }

    @Override
    public void prepare(final Model model)
    {
        final EppWorkPlanBase wp = model.getHolder().refresh(EppWorkPlanBase.class);

        final IEppWorkPlanWrapper wpWrapper = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(Collections.singleton(wp.getId())).get(wp.getId());
        final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> rowPartLoadDataMap = IEppWorkPlanDataDAO.instance.get().getRowPartLoadDataMap(wpWrapper.getRowMap().keySet());
        final Map<String, EppLoadType> loadTypeMap = EppEduPlanVersionDataDAO.getLoadTypeMap();

        {
            final MutableLong wp_min = new MutableLong(Long.MAX_VALUE);
            final MutableLong wp_max = new MutableLong(Long.MIN_VALUE);

            final StaticListDataSource<IEntity> dataSource = model.getDataSource();
            dataSource.clearColumns();
            dataSource.addColumn(new SimpleColumn("Индекс", "number").setWidth(1));
            dataSource.addColumn(new SimpleColumn("Название", "displayableTitle").setWidth(20));

            {
                final List<ViewWrapper<IEppWorkPlanRowWrapper>> wrappers = ViewWrapper.getPatchedList(wpWrapper.getRowMap().values());
                dataSource.setupRows(wrappers);

                for (final ViewWrapper<IEppWorkPlanRowWrapper> wrapper: wrappers)
                {
                    final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> partLoadMap = rowPartLoadDataMap.get(wrapper.getId());
                    if (null != partLoadMap) {
                        for (final Map.Entry<Integer, Map<String, EppWorkPlanRowPartLoad>> partEntry: partLoadMap.entrySet()) {
                            for (final EppWorkPlanRowPartLoad load: partEntry.getValue().values()) {
                                wp_min.setValue(Math.min(wp_min.longValue(), this.time(load.getStudyPeriodStartDate(), wp_min)));
                                wp_max.setValue(Math.max(wp_max.longValue(), this.time(load.getStudyPeriodFinishDate(), wp_max)));
                            }

                        }
                    }
                }

                dataSource.addColumn(new SimpleColumn("График", "graph") {
                    @Override public boolean isRawContent() { return true; }
                    @Override public String getContent(final IEntity entity)
                    {
                        final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> partLoadMap = rowPartLoadDataMap.get(entity.getId());
                        if (null == partLoadMap) { return ""; }

                        final StringBuilder sb = new StringBuilder();

                        for (final Map.Entry<Integer, Map<String, EppWorkPlanRowPartLoad>> ePart: partLoadMap.entrySet()) {
                            for (final Map.Entry<String, EppWorkPlanRowPartLoad> eLoad: ePart.getValue().entrySet()) {
                                final EppWorkPlanRowPartLoad value = eLoad.getValue();
                                if ((null == value.getStudyPeriodStartDate()) && (null == value.getStudyPeriodFinishDate())) { continue; }

                                final EppLoadType eppLoadType = loadTypeMap.get(eLoad.getKey());

                                final long t0 = wp_min.longValue();
                                final long t1 = Math.max(t0, DAO.this.time(value.getStudyPeriodStartDate(), wp_min));
                                final long t2 = Math.max(t1, DAO.this.time(value.getStudyPeriodFinishDate(), wp_max));
                                final long t3 = Math.max(t2, wp_max.longValue());

                                if (t0 == t3) { continue; }

                                final int SZ = 98;
                                final int p1 = Math.max(1, (int)((SZ*(t1-t0))/(t3-t0)));
                                final int p2 = Math.max(1, (int)((SZ*(t2-t1))/(t3-t0)));
                                final int p3 = (2+SZ) -(p2+p1);

                                if (sb.length()>0) { sb.append("<br/>\n"); }
                                sb.append("<nobr>");
                                sb.append("<div style=\"float:right;width:80%;\">");
                                sb.append("<div style=\"float:left;width:").append(p1).append("%; height:12px; background-color: lightgray;\"></div>");
                                sb.append("<div style=\"float:left;width:").append(p2).append("%; height:12px; background-color: #82a0bf;\"></div>");
                                sb.append("<div style=\"float:left;width:").append(p3).append("%; height:12px; background-color: lightgray;\"></div>");
                                sb.append("</div>");
                                sb.append("<div style=\"float:left;width:1%\">");
                                sb.append(" [").append(ePart.getKey()).append("]");
                                sb.append(" ").append(eppLoadType.getShortTitle());
                                sb.append(" (").append(UniEppUtils.formatLoad(value.getLoadAsDouble(), false)).append(")");
                                sb.append("</div>");
                                sb.append("</nobr>");
                            }
                        }

                        return sb.toString();
                    }
                });

                final IEntityHandler editDisabler = entity -> (null == rowPartLoadDataMap.get(entity.getId()));
                model.getDataSource().addColumn(UniEppUtils.getColumn_action(editDisabler , null, "edit", "Редактировать", "onClickEditTime"));
            }
        }
    }

    @Override
    public void doFillFromWorkGraph(final Model model) {
        final EppWorkPlanBase wp = model.getEppWorkPlan();
        final Map<Integer, List<EppWorkGraphRowWeek>> partWeeksMap = IEppWorkPlanDAO.instance.get().getWorkGraphWeeks4PlanParts(wp.getWorkPlan().getId());
        if (partWeeksMap.isEmpty()) { return; }

        final EppYearEducationWeek[] weeks = IEppYearDAO.instance.get().getYearEducationWeeks(wp.getYear().getId());
        final Map<Integer, Date[]> partDateMap = new HashMap<>(partWeeksMap.size());
        for (final Map.Entry<Integer, List<EppWorkGraphRowWeek>> e: partWeeksMap.entrySet()) {

            int start = Integer.MAX_VALUE, finish = Integer.MIN_VALUE;
            for (final EppWorkGraphRowWeek gweek: e.getValue()) {
                final int i = gweek.getWeek();
                start = Math.min(i, start);
                finish = Math.max(i, finish);
            }

            if ((1 <= start) && (start <= finish) && (finish <= weeks.length)) {
                partDateMap.put(e.getKey(), new Date[] { weeks[start-1].getDate(), weeks[finish-1].getEndDate() });
            }
        }
        if (partDateMap.isEmpty()) { return;}


        final IEppWorkPlanWrapper wpWrapper = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(Collections.singleton(wp.getId())).get(wp.getId());
        final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> rowPartLoadDataMap = IEppWorkPlanDataDAO.instance.get().getRowPartLoadDataMap(wpWrapper.getRowMap().keySet());
        for (final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> m0 : rowPartLoadDataMap.values()) {
            for (final Map<String, EppWorkPlanRowPartLoad> m1: m0.values()) {
                for (final EppWorkPlanRowPartLoad load: m1.values()) {
                    final Date[] dates = partDateMap.get(load.getPart());
                    if (null == dates) {
                        load.setStudyPeriodStartDate(null);
                        load.setStudyPeriodFinishDate(null);
                    } else {
                        load.intersectStudyPeriod(dates[0], dates[1]);
                    }
                }
            }
        }

        IEppWorkPlanDataDAO.instance.get().doUpdateRowPartLoadPeriodsMap(rowPartLoadDataMap);
    }

}

