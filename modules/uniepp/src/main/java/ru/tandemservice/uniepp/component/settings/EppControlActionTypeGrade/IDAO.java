package ru.tandemservice.uniepp.component.settings.EppControlActionTypeGrade;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author iolshvang
 * @since 02.03.2011
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
}