/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphEduPlansTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));

        this.prepareListDataSource(component);
    }

    private void prepareListDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDataSource() != null) {
            return;
        }

        final DynamicListDataSource<WorkGraphEduPlansRow> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareListDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("УП", "eduPlanVersion.eduPlan.title").setOrderable(false).setClickable(false));
        dataSource.addColumn(new PublisherColumnBuilder("Версия УП", "eduPlanVersion.title")
            .id("eduPlanVersion.id")
            .build().setOrderable(false));

//        dataSource.addColumn(new SimpleColumn("Версия УП", "eduPlanVersion.title").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Курсы", "courses").setOrderable(false).setClickable(false));

        model.setDataSource(dataSource);
    }

    public void onClickAdd(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ru.tandemservice.uniepp.component.workgraph.WorkGraphEduPlanAdd.Model.class.getPackage().getName(), new ParametersMap()
                .add("workGraphId", this.getModel(component).getWorkGraph().getId())
        ));
    }
}