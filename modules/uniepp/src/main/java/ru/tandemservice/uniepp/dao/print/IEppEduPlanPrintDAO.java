package ru.tandemservice.uniepp.dao.print;

import java.util.Collection;
import java.util.Map;

import jxl.write.WritableWorkbook;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface IEppEduPlanPrintDAO {
    public static final SpringBeanCache<IEppEduPlanPrintDAO> instance = new SpringBeanCache<IEppEduPlanPrintDAO>(IEppEduPlanPrintDAO.class.getName());

    /**
     * печатаем версию УП из реестра
     * @param book - куда печатаем
     * @param epvIds - какие версии УП надо напечатать (будут в своих вкладках)
     * @param context - контекст печати (передача доп. информации)
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void printRegistryEduPlanVersion(WritableWorkbook book, Collection<Long> epvIds, Map<String, Object> context);

}
