/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphWeeksTab;

import org.hibernate.Session;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.gen.CourseGen;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.year.IEppYearDAO;
import ru.tandemservice.uniepp.dao.year.IEppYearWorkGraphDAO;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRow2EduPlanGen;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRowGen;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRowWeekGen;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniepp.tapestry.richTableList.RichRangeSelection;
import ru.tandemservice.uniepp.ui.WorkGraphEduProgramSubjectListModel;
import ru.tandemservice.uniepp.util.EppEduPlanVersionScheduleUtils;

import java.util.*;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setWorkGraph(this.getNotNull(EppWorkGraph.class, model.getWorkGraph().getId()));
        model.setProgramSubjectModel(new WorkGraphEduProgramSubjectListModel(model.getWorkGraph(), model));

        // массив недель
        model.setWeekData(IEppYearDAO.instance.get().getYearEducationWeeks(model.getWorkGraph().getYear().getId()));

        // используемые курсы в гуп
        final MQBuilder builder = new MQBuilder(EppWorkGraphRowGen.ENTITY_CLASS, "row", new String[]{EppWorkGraphRowGen.L_COURSE});
        builder.add(MQExpression.eq("row", EppWorkGraphRowGen.L_GRAPH, model.getWorkGraph()));
        builder.setNeedDistinct(true);
        final List<Course> courseList = builder.getResultList(this.getSession());
        Collections.sort(courseList, new EntityComparator<Course>(new EntityOrder(CourseGen.P_INT_VALUE)));

        // список курсов обучения согласно используемым версиям уп в гуп
        model.setCourseList(courseList);

        // обновляем фильтр курса актуальным значением
        model.getCourse();

        // заполнение легенды
        model.setWeekTypeLegendList(IEppEduPlanDAO.instance.get().getWeekTypeLegendRowList(null));
    }

    @Override
    public void prepareGraphDataMap(final Model model)
    {
        final Session session = this.getSession();
        final Course course = model.getCourse();
        if (course == null)
        {
            return;
        }

        // значения в ячейках таблицы
        final Map<PairKey<Long, Long>, EppWeekType> dataMap = new HashMap<PairKey<Long, Long>, EppWeekType>();
        model.getGraphDataSource().setDataMap(dataMap);

        // бежим по всем ячейкам из базы и сохраняем в dataMap значения
        for (final EppWorkGraphRowWeek rowWeek : DAO.getRelationList(session, model.getWorkGraph(), course, null))
        {
            // получаем данные
            final Long rowId = rowWeek.getRow().getId();
            final Long weekId = model.getWeekData()[rowWeek.getWeek() - 1].getId();

            // записываем тип недели в ячейку таблицы
            dataMap.put(PairKey.create(rowId, weekId), rowWeek.getType());
        }

        // rowId -> массив точек
        // rowId -> логическая карта строки
        // заполняем эти два важных массива согласно данным из базы
        final Map<Long, int[]> row2points = new HashMap<Long, int[]>();
        final Map<Long, int[]> row2data = new HashMap<Long, int[]>();
        model.getGraphDataSource().setRow2points(row2points);
        model.getGraphDataSource().setRow2data(row2data);
        DAO.prepareRowDataPoints(session, model.getWorkGraph(), course, row2points, row2data);

        // обновляем список отфильтрованных версий уп в строках ГУП
        final EduProgramSubject programSubject = model.getProgramSubject();
        final Collection<EduProgramSubject> subjectSet = programSubject == null ? null : Collections.singletonList(programSubject);
        model.setFiltedIds(IEppYearWorkGraphDAO.instance.get().getFilteredEduPlanRowIds(model.getWorkGraph(), subjectSet));
    }

    @Override
    public void prepareEditRow(final Model model, final Long rowId)
    {
        final Session session = this.getSession();
        final Course course = model.getCourse();

        final RangeSelectionWeekTypeListDataSource<EppWorkGraphRow> graphDataSource = model.getGraphDataSource();
        if (DAO.getRelationList(session, model.getWorkGraph(), course, rowId).isEmpty())
        {
            final DevelopGrid developGrid = model.getWorkGraph().getDevelopGrid();
            graphDataSource.setSelection(new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, EppEduPlanVersionScheduleUtils.getPoints4EmptyGridRow(developGrid, course)));
        }
        else
        {
            graphDataSource.setSelection(new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, graphDataSource.getRow2points().get(rowId)));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareGraphDataSource(final Model model)
    {
        final Session session = this.getSession();
        final Course course = model.getCourse();

        if (course == null)
        {
            model.getGraphDataSource().getDataSource().setCountRow(0);
            UniBaseUtils.createPage((DynamicListDataSource<EppWorkGraphRow>) model.getGraphDataSource().getDataSource(), Collections.<EppWorkGraphRow>emptyList());
        }
        else
        {
            final MQBuilder builder = new MQBuilder(EppWorkGraphRow2EduPlanGen.ENTITY_CLASS, "rel");
            builder.addJoin("rel", EppWorkGraphRow2EduPlanGen.L_ROW, "row");
            builder.add(MQExpression.eq("row", EppWorkGraphRowGen.L_GRAPH, model.getWorkGraph()));
            builder.add(MQExpression.eq("row", EppWorkGraphRowGen.L_COURSE, course));
            final List<EppWorkGraphRow2EduPlan> relList = builder.getResultList(session);

            final Map<EppWorkGraphRow, Object[]> row2epvMap = new HashMap<EppWorkGraphRow, Object[]>();


            final int MAP_INDEX = 0;
            final int DISABLED_COUNT_INDEX = 1;

            //List<EppWorkGraphRow> list = new ArrayList<EppWorkGraphRow>();
            for (final EppWorkGraphRow2EduPlan rel : relList)
            {
                final EppWorkGraphRow key = rel.getRow();
                Object[] value = row2epvMap.get(key);
                if (value == null)
                {
                    row2epvMap.put(key, value = new Object[]{new TreeMap<String, EppWorkGraphRow2EduPlan>(), 0});
                }

                final Map<String, EppWorkGraphRow2EduPlan> epvMap = (Map<String, EppWorkGraphRow2EduPlan>) value[MAP_INDEX];
                final int disabledCount = (Integer) value[DISABLED_COUNT_INDEX];

                epvMap.put(rel.getEduPlanVersion().getTitle() + rel.getEduPlanVersion().getId(), rel);

                // учитываем ненужные версии УП в строке ГУП
                if ((model.getFiltedIds() != null) && !model.getFiltedIds().contains(rel.getId()))
                {
                    value[1] = disabledCount + 1;
                }
            }


            // удаляем строки ГУП, которые состоят только из ненужных версий УП
            final Set<EppWorkGraphRow> forDelete = new HashSet<EppWorkGraphRow>();
            for (final Map.Entry<EppWorkGraphRow, Object[]> row : row2epvMap.entrySet())
            {
                final Object[] value = row.getValue();
                final Map<String, EppWorkGraphRow2EduPlan> epvMap = (Map<String, EppWorkGraphRow2EduPlan>) value[MAP_INDEX];
                final int disabledCount = (Integer) value[DISABLED_COUNT_INDEX];

                if (epvMap.size() == disabledCount)
                {
                    forDelete.add(row.getKey());
                }
            }
            for (final EppWorkGraphRow key : forDelete)
            {
                row2epvMap.remove(key);
            }

            final List<EppWorkGraphRow> list = new ArrayList<EppWorkGraphRow>(row2epvMap.keySet());

            Collections.sort(list, new EntityComparator<IEntity>(new EntityOrder(IEntity.P_ID)));
            model.getGraphDataSource().getDataSource().setCountRow(list.size());
            UniBaseUtils.createPage((DynamicListDataSource<EppWorkGraphRow>) model.getGraphDataSource().getDataSource(), list);

            final Map<EppWorkGraphRow, Collection<EppWorkGraphRow2EduPlan>> row2epvs = new HashMap<EppWorkGraphRow, Collection<EppWorkGraphRow2EduPlan>>();
            for (final Map.Entry<EppWorkGraphRow, Object[]> entry : row2epvMap.entrySet())
            {
                row2epvs.put(entry.getKey(), ((Map<String, EppWorkGraphRow2EduPlan>) row2epvMap.get(entry.getKey())[MAP_INDEX]).values());
            }
            model.setRow2epvs(row2epvs);
        }
    }

    @Override
    public void updateGraphRow(final Long rowId, final Model model)
    {
        IEppYearWorkGraphDAO.instance.get().doUpdateWorkGraphRow(rowId, model.getGraphDataSource().getDataMap(), model.getGraphDataSource().getSelection().getRanges());
        this.prepareGraphDataMap(model);
    }

    @Override
    public void updateCombineRows(final Model model, final Long templateRow, final Set<Long> selected)
    {
        IEppYearWorkGraphDAO.instance.get().doUpdateCombineRows(templateRow, selected);
        this.prepareGraphDataMap(model);
    }

    @Override
    public void updateExcludeRow(final Model model, final Long excludeRowId)
    {
        IEppYearWorkGraphDAO.instance.get().doUpdateExcludeRow(excludeRowId);
        this.prepareGraphDataMap(model);
    }

    // Component specific API function

    private static List<EppWorkGraphRowWeek> getRelationList(final Session session, final EppWorkGraph workGraph, final Course course, final Long rowId)
    {
        final MQBuilder builder = new MQBuilder(EppWorkGraphRowWeekGen.ENTITY_CLASS, "rw");
        builder.addJoin("rw", EppWorkGraphRowWeekGen.L_ROW, "r");
        builder.add(MQExpression.eq("r", EppWorkGraphRowGen.L_GRAPH, workGraph));
        builder.add(MQExpression.eq("r", EppWorkGraphRowGen.L_COURSE, course));
        if (rowId != null)
        {
            builder.add(MQExpression.eq("r", IEntity.P_ID, rowId));
        }
        return builder.getResultList(session);
    }

    private static void prepareRowDataPoints(final Session session, final EppWorkGraph workGraph, final Course course, final Map<Long, int[]> row2points, final Map<Long, int[]> row2data)
    {
        // row -> номер части в году -> [минимальный номер недели,максимальный номер недели]
        final Map<Long, Map<Integer, int[]>> map = new HashMap<Long, Map<Integer, int[]>>();
        final Integer[] gridDetail = IDevelopGridDAO.instance.get().getDevelopGridDetail(workGraph.getDevelopGrid()).get(course);

        for (final EppWorkGraphRowWeek item : DAO.getRelationList(session, workGraph, course, null))
        {
            // получаем данные
            final Long rowId = item.getRow().getId();
            final int term = item.getTerm().getIntValue();
            final int weekNumber = item.getWeek();
            int partNumber = 0;
            while ((partNumber < gridDetail.length) && ((null == gridDetail[partNumber]) || (term != gridDetail[partNumber]))) {
                partNumber++;
            }
            partNumber++;

            // сохраняем в мапе
            Map<Integer, int[]> partMap = map.get(rowId);
            if (partMap == null)
            {
                map.put(rowId, partMap = new TreeMap<Integer, int[]>());
            }

            final int[] points = partMap.get(partNumber);
            if (points == null)
            {
                partMap.put(partNumber, new int[]{weekNumber, weekNumber});
            }
            else if (weekNumber < points[0])
            {
                points[0] = weekNumber;
            }
            else if (weekNumber > points[1])
            {
                points[1] = weekNumber;
            }
        }

        for (final Map.Entry<Long, Map<Integer, int[]>> entry : map.entrySet())
        {
            final Map<Integer, int[]> partMap = entry.getValue();
            final int[] points = new int[partMap.size() * 2];
            int i = 0;
            for (final int[] pair : partMap.values())
            {
                points[i++] = pair[0] - 1;
                points[i++] = pair[1] - 1;
            }

            row2points.put(entry.getKey(), points);
            row2data.put(entry.getKey(), new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, points).getData());
        }
    }
}
