package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionMoveBlockRow;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;

@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "blockId"),
    @Bind(key = "rowIds", binding = "rowIds")
})
public class Model
{
    private Long blockId;
    public Long getBlockId() { return this.blockId; }
    public void setBlockId(final Long blockId) { this.blockId = blockId; }

    private Collection<Long> rowIds;
    public Collection<Long> getRowIds() { return this.rowIds; }
    public void setRowIds(final Collection<Long> rowIds) { this.rowIds = rowIds; }

    private List<HSelectOption> parentList = Collections.emptyList();
    public List<HSelectOption> getParentList() { return this.parentList; }
    public void setParentList(final List<HSelectOption> parentList) { this.parentList = parentList; }

    private IEppEpvRowWrapper parent;
    public IEppEpvRowWrapper getParent() { return this.parent; }
    public void setParent(final IEppEpvRowWrapper parent) { this.parent = parent; }

    private String title;
    public String getTitle() { return this.title; }
    public void setTitle(final String title) { this.title = title; }


}
