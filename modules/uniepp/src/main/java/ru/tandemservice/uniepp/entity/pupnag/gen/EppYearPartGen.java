package ru.tandemservice.uniepp.entity.pupnag.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часть года (в учебном году)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppYearPartGen extends EntityBase
 implements INaturalIdentifiable<EppYearPartGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.pupnag.EppYearPart";
    public static final String ENTITY_NAME = "eppYearPart";
    public static final int VERSION_HASH = -1399925015;
    private static IEntityMeta ENTITY_META;

    public static final String L_YEAR = "year";
    public static final String L_PART = "part";
    public static final String P_TITLE = "title";

    private EppYearEducationProcess _year;     // ПУПнаГ
    private YearDistributionPart _part;     // Часть года

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getYear()
    {
        return _year;
    }

    /**
     * @param year ПУПнаГ. Свойство не может быть null.
     */
    public void setYear(EppYearEducationProcess year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Часть года. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getPart()
    {
        return _part;
    }

    /**
     * @param part Часть года. Свойство не может быть null.
     */
    public void setPart(YearDistributionPart part)
    {
        dirty(_part, part);
        _part = part;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppYearPartGen)
        {
            if (withNaturalIdProperties)
            {
                setYear(((EppYearPart)another).getYear());
                setPart(((EppYearPart)another).getPart());
            }
        }
    }

    public INaturalId<EppYearPartGen> getNaturalId()
    {
        return new NaturalId(getYear(), getPart());
    }

    public static class NaturalId extends NaturalIdBase<EppYearPartGen>
    {
        private static final String PROXY_NAME = "EppYearPartNaturalProxy";

        private Long _year;
        private Long _part;

        public NaturalId()
        {}

        public NaturalId(EppYearEducationProcess year, YearDistributionPart part)
        {
            _year = ((IEntity) year).getId();
            _part = ((IEntity) part).getId();
        }

        public Long getYear()
        {
            return _year;
        }

        public void setYear(Long year)
        {
            _year = year;
        }

        public Long getPart()
        {
            return _part;
        }

        public void setPart(Long part)
        {
            _part = part;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppYearPartGen.NaturalId) ) return false;

            EppYearPartGen.NaturalId that = (NaturalId) o;

            if( !equals(getYear(), that.getYear()) ) return false;
            if( !equals(getPart(), that.getPart()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getYear());
            result = hashCode(result, getPart());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getYear());
            sb.append("/");
            sb.append(getPart());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppYearPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppYearPart.class;
        }

        public T newInstance()
        {
            return (T) new EppYearPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "year":
                    return obj.getYear();
                case "part":
                    return obj.getPart();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "year":
                    obj.setYear((EppYearEducationProcess) value);
                    return;
                case "part":
                    obj.setPart((YearDistributionPart) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "year":
                        return true;
                case "part":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "year":
                    return true;
                case "part":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "year":
                    return EppYearEducationProcess.class;
                case "part":
                    return YearDistributionPart.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppYearPart> _dslPath = new Path<EppYearPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppYearPart");
    }
            

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearPart#getYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearPart#getPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> part()
    {
        return _dslPath.part();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearPart#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EppYearPart> extends EntityPath<E>
    {
        private EppYearEducationProcess.Path<EppYearEducationProcess> _year;
        private YearDistributionPart.Path<YearDistributionPart> _part;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearPart#getYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> year()
        {
            if(_year == null )
                _year = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_YEAR, this);
            return _year;
        }

    /**
     * @return Часть года. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearPart#getPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> part()
        {
            if(_part == null )
                _part = new YearDistributionPart.Path<YearDistributionPart>(L_PART, this);
            return _part;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.pupnag.EppYearPart#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EppYearPartGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EppYearPart.class;
        }

        public String getEntityName()
        {
            return "eppYearPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
