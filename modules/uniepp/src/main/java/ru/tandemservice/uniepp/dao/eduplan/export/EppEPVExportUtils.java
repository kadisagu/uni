package ru.tandemservice.uniepp.dao.eduplan.export;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import java.util.Set;

/**
 * Общие методы, необходимые для импорта независимо от уровня образования
 * @author nkokorina
 *
 */

public class EppEPVExportUtils
{
    /**
     * 
     * @param jc - JAXBContext
     * @return - настроенный Marshaller
     * @throws JAXBException
     * @throws PropertyException
     */
    public static Marshaller getMarshaller(final JAXBContext jc) throws JAXBException, PropertyException
    {
        final Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_ENCODING, "windows-1251");
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        return m;
    }

    /**
     * @return - имя файла для экспорта в завивимости от уровня образования
     */
    public static String getFileName(final EppEduPlanVersion version, final Set<Integer> course)
    {
        // todo DEV-6000
//        if (version.getEduPlan().getEducationLevelHighSchool().getEducationLevel() instanceof EducationLevelHigh)
//        {
//            return EppEPVExportHSUtils.buildFileTitleToHighWithId(version.getId(), version, course);
//        }
//
        return null;
    }

    /**
     * 
     * @param version
     * @return - контент файла для экспорта в завивимости от уровня образования
     */
    public static byte[] getFileContent(final EppEduPlanVersion version)
    {
        // todo DEV-6000
//        if (version.getEduPlan().getEducationLevelHighSchool().getEducationLevel() instanceof EducationLevelHigh)
//        {
//            try
//            {
//                return new EppEPVExportHSGenerator(version).generateContent();
//            }
//            catch (final Throwable e)
//            {
//                e.printStackTrace();
//            }
//        }

        return null;
    }

    public static String getCheckSum(final byte[] bytes)
    {
        String str = StringUtils.substringAfter(new String(bytes), "\">");
        str = StringUtils.substringBeforeLast(str, "</");

        int checkSum = 0;
        for (final byte b : str.getBytes())
        {
            checkSum += (b < 0 ? 256 + b : b);
        }

        return String.valueOf(checkSum);
    }
}
