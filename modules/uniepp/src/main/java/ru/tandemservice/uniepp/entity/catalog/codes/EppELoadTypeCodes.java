package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид учебной нагрузки (типы учебной нагрузки)"
 * Имя сущности : eppELoadType
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppELoadTypeCodes
{
    /** Константа кода (code) элемента : Аудиторная (title) */
    String TYPE_TOTAL_AUDIT = "1";
    /** Константа кода (code) элемента : Самостоятельная (title) */
    String TYPE_TOTAL_SELFWORK = "2";

    Set<String> CODES = ImmutableSet.of(TYPE_TOTAL_AUDIT, TYPE_TOTAL_SELFWORK);
}
