package ru.tandemservice.uniepp.component.settings.EducationalPlanningAddEdit;

import java.util.Calendar;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

/**
 * 
 * @author nkokorina
 * @since 26.02.2010
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent component)
    {
        this.getDao().update(this.getModel(component));
        this.deactivate(component);
    }

    public void onChangeEducationYear(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final EducationYear educationYear = model.getYearEducationProcess().getEducationYear();
        if (educationYear != null)
        {
            final Calendar calendar = Calendar.getInstance();
            calendar.clear();
            calendar.set(educationYear.getIntValue(), Calendar.SEPTEMBER, 1);
            model.getYearEducationProcess().setTitle(educationYear.getTitle());
            model.getYearEducationProcess().setStartEducationDate(calendar.getTime());
        }
    }
}
