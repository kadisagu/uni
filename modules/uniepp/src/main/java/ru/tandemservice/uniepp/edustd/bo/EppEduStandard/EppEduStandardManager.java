/* $Id:$ */
package ru.tandemservice.uniepp.edustd.bo.EppEduStandard;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/10/14
 */
@Configuration
public class EppEduStandardManager extends BusinessObjectManager
{
    public static final String BIND_GENERATION = "generation";
    public static final String BIND_PROGRAM_KIND = "eduProgramKind";

    public static EppEduStandardManager instance()
    {
        return instance(EppEduStandardManager.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectFromEppStateEduStandartDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder eduStdDql = new DQLSelectBuilder()
                    .fromEntity(EppStateEduStandard.class, "std")
                    .where(eq(property("std", EppStateEduStandard.programSubject()), property(alias)));

                EppGeneration generation = context.get(BIND_GENERATION);
                if (EppGeneration.GENERATION_2.equals(generation)) {
                    dql.where(likeUpper(property(alias, EduProgramSubject.subjectIndex().code()), value(CoreStringUtils.escapeLike("2005"))));
                } else if (EppGeneration.GENERATION_3.equals(generation)){
                    dql.where(not(likeUpper(property(alias, EduProgramSubject.subjectIndex().code()), value(CoreStringUtils.escapeLike("2005")))));
                }

                dql.where(exists(eduStdDql.buildQuery()));
            }
        }
            .where(EduProgramSubject.subjectIndex().programKind(), BIND_PROGRAM_KIND, true)
            .filter(EduProgramSubject.code())
            .filter(EduProgramSubject.title())
            .order(EduProgramSubject.code())
            .order(EduProgramKind.title());
    }


    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EppGeneration generation = context.get(BIND_GENERATION);
                if (EppGeneration.GENERATION_2.equals(generation)) {
                    dql.where(likeUpper(property(alias, EduProgramSubject.subjectIndex().code()), value(CoreStringUtils.escapeLike("2005"))));
                } else if (EppGeneration.GENERATION_3.equals(generation)){
                    dql.where(not(likeUpper(property(alias, EduProgramSubject.subjectIndex().code()), value(CoreStringUtils.escapeLike("2005")))));
                }

            }
        }
                .where(EduProgramSubject.subjectIndex().programKind(), BIND_PROGRAM_KIND, true)
                .filter(EduProgramSubject.code())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.code())
                .order(EduProgramKind.title());
    }
}
