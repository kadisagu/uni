package ru.tandemservice.uniepp.component.registry.RegElementPartEdit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniepp.component.base.SimpleAddEditModel;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="holder.id", required = true)
})
public class Model {

    private final EntityHolder<EppRegistryElementPart> holder = new EntityHolder<EppRegistryElementPart>();
    public EntityHolder<EppRegistryElementPart> getHolder() { return this.holder; }
    public EppRegistryElementPart getElement() { return this.getHolder().getValue(); }

    public boolean isReadOnly() { return SimpleAddEditModel.isReadOnly(this.getElement()); }
    public boolean isShowWeeks() {return getElement().getRegistryElement() instanceof EppRegistryPractice; }

    private ISelectModel controlActionModel;
    public ISelectModel getControlActionModel() { return this.controlActionModel; }
    protected void setControlActionModel(final ISelectModel controlActionModel) { this.controlActionModel = controlActionModel; }

    private List<EppGradeScale> gradeScaleList = Collections.emptyList();
    public List<EppGradeScale> getGradeScaleList() { return this.gradeScaleList; }
    protected void setGradeScaleList(final List<EppGradeScale> gradeScaleList) { this.gradeScaleList = gradeScaleList; }


    private Map<EppFControlActionType, EppRegistryElementPartFControlAction> selection = Collections.emptyMap();
    protected Map<EppFControlActionType, EppRegistryElementPartFControlAction> getSelection() { return this.selection; }

    protected void setSelection(final Map<EppFControlActionType, EppRegistryElementPartFControlAction> selection) {
        final Map<EppFControlActionType, EppRegistryElementPartFControlAction> s = new LinkedHashMap<EppFControlActionType, EppRegistryElementPartFControlAction>(selection.size());
        final List<EppFControlActionType> keys = new ArrayList<EppFControlActionType>(selection.keySet());
        Collections.sort(keys, ITitled.TITLED_COMPARATOR);
        for (final EppFControlActionType key: keys) {
            s.put(key, selection.get(key));
        }
        this.selection = s;
    }

    public List<EppFControlActionType> getSelectedActions() {
        return new ArrayList<EppFControlActionType>(this.getSelection().keySet());
    }

    public List<EppRegistryElementPartFControlAction> getSelectedRelations() {
        return new ArrayList<EppRegistryElementPartFControlAction>(this.getSelection().values());
    }

    public void setSelectedActions(final List<EppFControlActionType> selectedActions) {
        final Map<EppFControlActionType, EppRegistryElementPartFControlAction> result = new HashMap<EppFControlActionType, EppRegistryElementPartFControlAction>(selectedActions.size());
        final Map<EppFControlActionType, EppRegistryElementPartFControlAction> map = this.getSelection();
        for (final EppFControlActionType a: selectedActions) {
            EppRegistryElementPartFControlAction r = map.get(a);
            if (null == r) { r = new EppRegistryElementPartFControlAction(this.getElement(), a); }
            result.put(r.getControlAction(), r);
        }
        this.setSelection(result);
    }

    public EppRegistryElementPartFControlAction current;
    public EppRegistryElementPartFControlAction getCurrent() { return this.current; }
    public void setCurrent(final EppRegistryElementPartFControlAction current) { this.current = current; }

}
