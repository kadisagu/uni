/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.EduPlansWithoutWorkPlans;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.util.EppFilterUtil;
import ru.tandemservice.uniepp.util.IYearEducationProcessSelectModel;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * todo: пока эта штука не отображается на подразделениях. нужно подумать, как вообще будет организована работа с УП и РУП с подразделений.
 * @author oleyba
 * @since 9/17/14
 */
@State ({
    @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
public class EppIndicatorEduPlansWithoutWorkPlansUI extends UIPresenter implements IYearEducationProcessSelectModel
{
    public static final String PROP_ORG_UNIT = "orgUnit";

    private final OrgUnitHolder _orgUnitHolder = new OrgUnitHolder();

    private List<EppYearEducationProcess> _yearEducationProcessList;
    private List<HSelectOption> _partsList;

    private ISelectModel _programSubjectModel;
    private ISelectModel _programFormListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _programTraitListModel;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        getOrgUnitHolder().refresh();
        setPartsList(HierarchyUtil.listHierarchyNodesWithParents(DataAccessServices.dao().getList(YearDistributionPart.class), false));
        EppFilterUtil.prepareYearEducationProcessFilter(this, _uiSupport.getSession());
        setProgramSubjectModel(new DQLFullCheckSelectModel(EduProgramSubject.titleWithCode().s())
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EduProgramSubject.class, alias)
                    .order(property(alias, EduProgramSubject.subjectCode()))
                    .order(property(alias, EduProgramSubject.title()))
                    ;

                FilterUtils.applyLikeFilter(dql, filter, EduProgramSubject.subjectCode().fromAlias(alias), EduProgramSubject.title().fromAlias(alias));
                FilterUtils.applySelectFilter(dql, alias, EduProgramSubject.subjectIndex().programKind(), getSettings().get("programKind"));

                DQLSelectBuilder epDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanProf.class, "p")
                    .where(eq(property("p", EppEduPlanProf.programSubject()), property(alias)));
                dql.where(exists(epDql.buildQuery()));
                return dql;
            }
        });
        setProgramFormListModel(new LazySimpleSelectModel<>(EduProgramForm.class));
        setDevelopConditionListModel(new LazySimpleSelectModel<>(IUniBaseDao.instance.get().getCatalogItemListOrderByCode(DevelopCondition.class)));
        setProgramTraitListModel(new LazySimpleSelectModel<>(EduProgramTrait.class));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("orgUnit", getOrgUnitHolder().getValue());
        dataSource.put("settings", getSettings());
    }

    public boolean isOrgUnitPage() {
        return getOrgUnitHolder().getValue() != null;
    }

    public String getViewPermissionKey() {
        if (null != getOrgUnitHolder().getValue()) {
            return getOrgUnitHolder().getSecModel().getPermission("eppGroupOuIndicatorBlockView");
        }
        return "";
    }

    @Override
    public EppYearEducationProcess getYearEducationProcess()
    {
        final Object object = this.getSettings().get(IYearEducationProcessSelectModel.YEAR_EDUCATION_PROCESS_FILTER_NAME);
        return ((object instanceof EppYearEducationProcess) ? IUniBaseDao.instance.get().get(EppYearEducationProcess.class, ((EppYearEducationProcess)object).getId()) : null);
    }
    @Override
    public void setYearEducationProcess(final EppYearEducationProcess yearEducationProcess)
    {
        this.getSettings().set(IYearEducationProcessSelectModel.YEAR_EDUCATION_PROCESS_FILTER_NAME, yearEducationProcess);
    }

    public void onClickSearch()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        saveSettings();
    }

    // Getters & Setters

    public OrgUnitHolder getOrgUnitHolder()
    {
        return _orgUnitHolder;
    }

    public List<HSelectOption> getPartsList()
    {
        return _partsList;
    }

    public void setPartsList(List<HSelectOption> partsList)
    {
        _partsList = partsList;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public ISelectModel getProgramFormListModel()
    {
        return _programFormListModel;
    }

    public void setProgramFormListModel(ISelectModel programFormListModel)
    {
        _programFormListModel = programFormListModel;
    }

    public ISelectModel getProgramSubjectModel()
    {
        return _programSubjectModel;
    }

    public void setProgramSubjectModel(ISelectModel programSubjectModel)
    {
        _programSubjectModel = programSubjectModel;
    }

    public ISelectModel getProgramTraitListModel()
    {
        return _programTraitListModel;
    }

    public void setProgramTraitListModel(ISelectModel programTraitListModel)
    {
        _programTraitListModel = programTraitListModel;
    }

    public List<EppYearEducationProcess> getYearEducationProcessList()
    {
        return _yearEducationProcessList;
    }

    public void setYearEducationProcessList(List<EppYearEducationProcess> yearEducationProcessList)
    {
        _yearEducationProcessList = yearEducationProcessList;
    }
}