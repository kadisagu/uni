package ru.tandemservice.uniepp.dao.registry.data;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;

import java.util.Map;

/**
 * @author vdanilov
 */
@Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
public abstract class EppLoadFormatter<T> {

    private final Map<String, EppLoadType> loadTypeMap;
    public EppLoadType getLoad(String loadFullCode) { return loadTypeMap.get(loadFullCode); }

    public EppLoadFormatter(Map<String, EppLoadType> loadTypeMap) {
        this.loadTypeMap = loadTypeMap;
    }

    public EppLoadFormatter() {
        this(EppEduPlanVersionDataDAO.getLoadTypeMap());
    }

    protected abstract Double load(T element, String loadFullCode);

    /**
     * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421
     * @return [Часов (всего)] ч. / [число зач. единиц] ЗЕТ ( [Аудиторные часы] = [Лекционные часы]/[Лабораторные]/[Практические] + [Самостоятельная работа] сам.)
     */
    public String format(T element)
    {
        final String s = formatLoadByType(element);
        return (
                UniEppUtils.formatLoad(load(element, EppLoadType.FULL_CODE_TOTAL_HOURS), false) + " ч. / " +
                UniEppUtils.formatLoad(load(element, EppLoadType.FULL_CODE_LABOR), false) + " ЗЕТ " +
                (s.length() > 0 ? (" (" + s +")") : "")
        );
    }

    /**
     * @return [Аудиторные часы] = [Лекционные часы]/[Лабораторные]/[Практические] + [Самостоятельная работа] сам.
     */
    public String formatLoadByType(T element)
    {
        final StringBuilder sb = new StringBuilder();

        {
            final Double totalAuditLoad = load(element, EppELoadType.FULL_CODE_AUDIT);
            if (null != totalAuditLoad) {
                sb.append(UniEppUtils.formatLoad(totalAuditLoad.doubleValue(), false)).append(" = ");
            }
        }

        {
            final StringBuilder loadDetails = new StringBuilder();
            for (final String fullCode : EppALoadType.FULL_CODES) {
                final Double load = load(element, fullCode);
                if (loadDetails.length() > 0) { loadDetails.append("/"); }
                loadDetails.append(UniEppUtils.formatLoad((null == load ? 0d : load), false));
            }
            sb.append(loadDetails);
        }

        for (final String fullCode: EppELoadType.FULL_CODES) {
            if (!EppELoadType.FULL_CODE_AUDIT.equals(fullCode)) {
                final Double load = load(element, fullCode);
                if ((null != load) && (load > 0)) {
                    sb.append(" + ");
                    sb.append(UniEppUtils.formatLoad(load, false));
                    sb.append(" ").append(StringUtils.removeEnd(loadTypeMap.get(fullCode).getShortTitle(), ".")).append(".");
                }
            }
        }
        return sb.toString();
    }

}
