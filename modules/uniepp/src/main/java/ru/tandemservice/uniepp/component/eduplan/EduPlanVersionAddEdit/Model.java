package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit;

import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.component.base.SimpleAddEditModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import java.util.List;

/**
 * @author vdanilov
 */
public class Model extends SimpleAddEditModel<EppEduPlanVersion>
{
    private List<DevelopGridTerm> _developGridTermList;
    public List<DevelopGridTerm> getDevelopGridTermList() { return _developGridTermList; }
    public void setDevelopGridTermList(List<DevelopGridTerm> developGridTermList) { _developGridTermList = developGridTermList; }

    public boolean isShowTerm () {
        return getElement().getEduPlan() instanceof EppEduPlanHigherProf;
    }
}
