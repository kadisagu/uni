package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionMoveBlockRow;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.collections15.functors.TruePredicate;
import org.hibernate.Session;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.BaseAddEditDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvRowGen;

public class DAO extends UniDao<Model> implements IDAO
{

    @SuppressWarnings("unchecked")
    @Override
    public void prepare(final Model model)
    {
        model.setTitle(null);
        final Collection<Long> rowIds = model.getRowIds();

        // определяем, что за элементы перемещаются
        final List<IEppEpvRow> selectRowList = (List)getList(EppEpvRow.class, "id", rowIds);

        // вернет из выделенных только те, которые реально надо переместить
        final Collection<IEppEpvRow> movingRowList = IEppEduPlanVersionDataDAO.instance.get().getTopLevelRows(selectRowList);

        final int structCount = CollectionUtils.countMatches(movingRowList, IEppEduPlanVersionDataDAO.PREDICATE_HIERARCHY_ROW);
        final Predicate<IEppEpvRow> predicate = ((structCount > 0) ? IEppEduPlanVersionDataDAO.PREDICATE_STRUCTURE_ROW : TruePredicate.<IEppEpvRow>getInstance());
        final List<HSelectOption> parentList;

        final IEntity entity = this.get(model.getBlockId());
        if (entity instanceof EppEduPlanVersionBlock)
        {
            final EppEduPlanVersionBlock block = (EppEduPlanVersionBlock) entity;
            block.getEduPlanVersion().getState().check_editable(block.getEduPlanVersion());

            // если это блок - то внутри блока
            parentList = IEppEduPlanVersionDataDAO.instance.get().getEpvRowHierarchyListFromBlock(block, predicate);
            model.setTitle(UniEppUtils.getEppEpvRowFormTitle(entity.getId()));
        }
        else
        {
            throw new IllegalArgumentException(String.valueOf(entity));
        }

        BaseAddEditDao.disablePossibleLoop(parentList, rowIds);
        model.setParentList(parentList);
    }



    @Override
    public void update(final Model model)
    {
        final Session session = this.getSession();

        final Collection<Long> rowIds = model.getRowIds();
        final MQBuilder rowBuilder = new MQBuilder(EppEpvRowGen.ENTITY_NAME, "r");
        rowBuilder.add(MQExpression.in("r", EntityBase.id().s(), rowIds));
        final Collection<IEppEpvRow> movingRowList = IEppEduPlanVersionDataDAO.instance.get().getTopLevelRows(rowBuilder.<IEppEpvRow> getResultList(session));

        final EppEpvRow parent = (null == model.getParent() ? null : (EppEpvRow)model.getParent().getRow());
        for (final IEppEpvRow row : movingRowList) {
            if (row instanceof EppEpvRow) {
                ((EppEpvRow)row).setHierarhyParent(parent);
                session.saveOrUpdate(row);
            }
        }

        // пересчитываем индекс
        EppEduPlanVersionBlock block = (EppEduPlanVersionBlock)session.get(EppEduPlanVersionBlock.class, model.getBlockId());
        IEppEduPlanVersionDataDAO.instance.get().doUpdateEpvRowStoredIndex(block.getEduPlanVersion().getId());
    }
}
