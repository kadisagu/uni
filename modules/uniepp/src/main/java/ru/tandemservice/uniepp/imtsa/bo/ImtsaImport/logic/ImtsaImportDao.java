/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.logic;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.data.*;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.ImtsaImportManager;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.ImParseResult;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.dto.*;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.node.ImElement;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.parser.IImParser;

import javax.validation.constraints.NotNull;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes.*;

/**
 * @author azhebko
 * @since 28.10.2014
 */
public class ImtsaImportDao extends UniBaseDao implements IImtsaImportDao
{
    public String doImportImtsaEduPlan(final EppEduPlanVersionBlock block, IUploadFile file)
    {
        StringBuilder messageBuilder = new StringBuilder();
        IImParser parser = IImParser.instance.get();
        InputStream input = file.getStream();

        Document document;
        try
        {
            document = parser.buildXmlDocument(input);

        } catch (ParserConfigurationException | IOException | SAXException e)
        {
            throw new ApplicationException(ImtsaImportManager.instance().getProperty("parse.error.incorrect-file-structure"));
        }


        ImElement root = parser.buildImElement(document);
        final ImParseResult parseResult = parser.parse(root);
        EppEduPlan eduPlan = block.getEduPlanVersion().getEduPlan();

        String programFormCode = eduPlan.getProgramForm().getCode();
        if (!programFormCode.equals(parseResult.getProgramFormCode()))
            throw new ApplicationException("В файле указана другая форма обучения.");

        String planStructureYear;
        String cycleCode;
        String partCode;
        switch (parseResult.getPlanStructureCode())
        {
            case FGOS:
                planStructureYear = "2009";
                cycleCode = FGOS_CYCLES;
                partCode = FGOS_PARTS;
                break;
            case FGOS_2013:
                planStructureYear = "2013";
                cycleCode = FGOS_2013_BLOCKS;
                partCode = FGOS_2013_PARTS;
                break;
            default:
                throw new ApplicationException("В файле не верно указан тип ГОСа.");
        }

        if (eduPlan instanceof EppEduPlanProf)
        {
            String subjectIndexCode = ((EppEduPlanProf) eduPlan).getProgramSubject().getSubjectIndex().getCode();
            if (!subjectIndexCode.startsWith(planStructureYear))
                throw new ApplicationException("Направление подготовки учебного плана относится к другому перечню, отличному от указанного в файле.");
        }
        else
            throw new IllegalStateException(); // импорт доступен лишь для учебных планов ВО

        Map<Integer, MutableInt> courseNumberMap = SafeMap.get(MutableInt.class);
        DevelopGrid developGrid = eduPlan.getDevelopGrid();
        List<DevelopGridTerm> developGridTerms = getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), developGrid);
        for (DevelopGridTerm dgt: developGridTerms)
            courseNumberMap.get(dgt.getCourse().getIntValue()).increment();

        DevelopPeriod developPeriod = developGrid.getDevelopPeriod();
        EduProgramDuration eduProgramDuration = developPeriod.getEduProgramDuration();
        if (null != eduProgramDuration)
        {
            PairKey<Integer, Integer> parseDuration = parseResult.getEduProgramDuration();
            int parseDurationYear = parseDuration.getFirst();
            int parseDurationMonth = parseDuration.getSecond();

            if (eduProgramDuration.getNumberOfYears() != parseDurationYear || eduProgramDuration.getNumberOfMonths() != parseDurationMonth)
                throw new ApplicationException("В файле указан срок обучения отличный от срока обучения учебного плана.");
        }

        Integer lastCourse = parseResult.getLastCourse();
        if (null != lastCourse && !lastCourse.equals(developPeriod.getLastCourse()))
            throw new ApplicationException("В файле указан последний курс обучения отличный от последнего курса учебного плана.");

        Integer sessionInCourse = parseResult.getCoursePartsNumber();
        for (int course = 1; course <= courseNumberMap.size() - 1; course++)
        {
            MutableInt termCount = courseNumberMap.get(course);
            if (null != sessionInCourse && sessionInCourse != termCount.intValue())
                throw new ApplicationException("В файле количество семестров на " + course + " курсе не совпадает со значением указанным в учебном плане.");
        }

        if (programFormCode.equals(EduProgramFormCodes.ZAOCHNAYA))
        {
            List<Integer> sessionNumbers = parseResult.getLastCourseSessionNumbers();
            List<Integer> lastCourseTerms = Lists.newArrayList();

            for (DevelopGridTerm dgt : developGridTerms)
            {
                if (dgt.getCourseNumber() == courseNumberMap.size())
                    lastCourseTerms.add(dgt.getTermNumber());
            }
            for (Integer sessionNumber : sessionNumbers)
            {
                if (sessionNumbers.size() > lastCourseTerms.size() || !lastCourseTerms.contains(sessionNumber))
                    throw new ApplicationException("В файле количество семестров на последнем курсе не совпадает со значением указанным в учебном плане.");
            }
        }

        Collector<EppPlanStructure, ?, Map<String, EppPlanStructure>> structureMapCollector = Collectors.toMap(
                std -> std.getShortTitle() + " - " + StringUtils.trimToEmpty(std.getTitle()).toLowerCase(),
                std -> std,
                (std1, std2) -> Integer.compare(std1.getPriority(), std2.getPriority()) < 0 ? std2 : std1 //на случай невероятного совпадения пары [индекс, название] у разных элементов
        );

        Map<String, EppPlanStructure> cyclesMap = CatalogManager.instance().dao()
                .getCatalogItemList(EppPlanStructure.class, EppPlanStructure.parent().code().s(), cycleCode)
                .stream().collect(structureMapCollector);

        Map<String, EppPlanStructure> partsMap = CatalogManager.instance().dao()
                .getCatalogItemList(EppPlanStructure.class, EppPlanStructure.parent().code().s(), partCode)
                .stream().collect(structureMapCollector);

        List<String> rowsWithError = new ArrayList<>();
        IPlanRowsImportContext context = new IPlanRowsImportContext()
        {
            @Override public EppEduPlanVersionBlock getBlock() { return block; }

            @Override
            public EppPlanStructure getPlanStructure(ImEpvStructureRowDTO row)
            {
                String index = row.getStoredIndex();
                String title = row.getTitle();

                if (index == null) return null;

                int lastPartIndex = index.lastIndexOf(".");
                String lastPart;
                boolean cycle = false;
                if (lastPartIndex > 0)
                    lastPart = index.substring(lastPartIndex + 1);
                else {
                    cycle = true;
                    lastPart = index;
                }

                String key = lastPart + " - " + StringUtils.trimToEmpty(title).toLowerCase();
                EppPlanStructure result = cycle ? cyclesMap.get(key) : partsMap.get(key);

                if (result == null && title != null)
                    rowsWithError.add(lastPart + " - " + title);

                return result;
            }
        };
        context.updateEpvRows(parseResult.getRootRows());

        if (!rowsWithError.isEmpty())
        {
            if (messageBuilder.length() > 0)
                messageBuilder.append("; ");
            messageBuilder.append("В справочнике «Структурные элементы ГОС и учебных планов» ");
            if (rowsWithError.size() > 1) messageBuilder.append("отсутствуют элементы:");
            else messageBuilder.append("отсутствует элемент");
            for (int i = 0; i < rowsWithError.size(); i++)
            {
                if (i > 0) messageBuilder.append(", ");
                else messageBuilder.append(" ");
                messageBuilder.append(rowsWithError.get(i));
            }

            messageBuilder.append(".");
        }

        return messageBuilder.toString();
    }

    /** Контекст импорта строк учебного плана. */
    private abstract class IPlanRowsImportContext
    {
        /** Блок УПв. */
        public abstract EppEduPlanVersionBlock getBlock();

        /** Структура ГОС для заданного узла (аббревиатура цикла, сокр. название части). */
        public abstract EppPlanStructure getPlanStructure(ImEpvStructureRowDTO row);

        private final OrgUnit _topOrgUnit = TopOrgUnit.getInstance();
        private final Map<String, OrgUnit> _tutorOUMap = buildTutorOrgUnitMap();
        private final Map<String, EppRegistryStructure> _registryStructureMap = buildRegistryStructureMap();
        private final Map<Long, IEppEduPlanVersionDataDAO.IRowLoadWrapper> _rowLoadMap = new HashMap<>();
        private final Map<String, EppEpvRow> _rowMap = buildEpvRowMap();

        /** Структура реестра: дисциплина, практика, ГИА. */
        private EppRegistryStructure getRegistryStructure(String typeCode) { return _registryStructureMap.get(typeCode); }

        /** Читающее подразделение: ОУ, если кафедра не найдена. */
        private OrgUnit getTutorOrgUnit(String tutorOUCode)
        {
            OrgUnit orgUnit = _tutorOUMap.get(tutorOUCode);
            return orgUnit == null ? _topOrgUnit : orgUnit;
        }

        /** Сохранение, обновление строк УП согласно данным плана ИМЦА. */
        public void updateEpvRows(Collection<ImEpvRowDTO> rootRows)
        {
            for (ImEpvRowDTO child: rootRows)
                updateEpvRow(child, null, "");

            updateRowLoad();
        }

        /** Сохранение, обновление строки УП согласно данным плана ИМЦА. */
        private void updateEpvRow(ImEpvRowDTO source, EppEpvRow parent, String path)
        {
            EppEpvRow result;
            if (source instanceof ImEpvRegistryRowDTO)
                result = updateEpvRegistryRow(((ImEpvRegistryRowDTO) source), parent, path);

            else if (source instanceof ImEpvStructureRowDTO)
                result = updateEpvStructureRow((ImEpvStructureRowDTO) source, ((EppEpvStructureRow) parent), path);

            else if (source instanceof ImEpvGroupImRowDTO)
                result = updateEpvGroupImRow((ImEpvGroupImRowDTO) source, parent, path);

            else if (source instanceof ImEpvGroupReRowDTO)
                result = updateEpvGroupReRow((ImEpvGroupReRowDTO) source, ((EppEpvHierarchyRow) parent), path);

            else
                throw new IllegalStateException("Unknown epv row type: " + source.getClass());

            for (ImEpvRowDTO child: source.getChildList())
                updateEpvRow(child, result == null ? parent : result, result == null ? "" : path + result.getTitle() + '\n');
        }

        private EppEpvStructureRow updateEpvStructureRow(ImEpvStructureRowDTO source, EppEpvStructureRow parent, String path)
        {
            EppPlanStructure planStructure = getPlanStructure(source);
            if (ImtsaImportManager.instance().dao().advancedModeEnabled())
                if (planStructure == null)
                    return null;

            EppEpvRow targetRow = _rowMap.get(path + planStructure.getTitle() + '\n');
            EppEpvStructureRow target = targetRow == null ? new EppEpvStructureRow() : (EppEpvStructureRow) checkRowClass(targetRow, EppEpvStructureRow.class);
            target.setParent(parent);
            target.setValue(planStructure);

            return updateEpvRow(target, source);
        }

        private EppEpvGroupReRow updateEpvGroupReRow(ImEpvGroupReRowDTO source, EppEpvHierarchyRow parent, String path)
        {
            EppEpvRow targetRow = _rowMap.get(path + source.getTitle() + '\n');
            EppEpvGroupReRow target = targetRow == null ? new EppEpvGroupReRow() : (EppEpvGroupReRow) checkRowClass(targetRow, EppEpvGroupReRow.class);
            target.setParent(parent);
            target.setTitle(source.getTitle());
            target.setNumber(source.getNumber());

            return updateEpvRow(target, source);
        }

        private EppEpvGroupImRow updateEpvGroupImRow(ImEpvGroupImRowDTO source, EppEpvRow parent, String path)
        {
            EppEpvRow targetRow = _rowMap.get(path + source.getTitle() + '\n');
            EppEpvGroupImRow target = targetRow == null ? new EppEpvGroupImRow() : (EppEpvGroupImRow) checkRowClass(targetRow, EppEpvGroupImRow.class);
            target.setSize(source.getSize());

            return updateEpvTermDistributedRow(target, source, parent);
        }

        private EppEpvRegistryRow updateEpvRegistryRow(ImEpvRegistryRowDTO source, EppEpvRow parent, String path)
        {
            EppEpvRow targetRow = _rowMap.get(path + source.getTitle() + '\n');
            EppEpvRegistryRow target = targetRow == null ? new EppEpvRegistryRow() : (EppEpvRegistryRow) checkRowClass(targetRow, EppEpvRegistryRow.class);
            target.setRegistryElementOwner(getTutorOrgUnit(source.getRegistryElementOwner()));
            target.setRegistryElementType(getRegistryStructure(source.getRegistryElementType()));

            return updateEpvTermDistributedRow(target, source, parent);
        }

        private <T extends EppEpvTermDistributedRow> T updateEpvTermDistributedRow(T target, ImEpvTermDistributedRowDTO source, EppEpvRow parent)
        {
            if (null == target.getOwner() || getBlock().equals(target.getOwner()))
            {
                target.setParent(parent);
                target.setNumber(source.getNumber());
                target.setTitle(source.getTitle());

                updateEpvRow(target, source);
                source.setRow(target);

                _rowLoadMap.put(target.getId(), source);
            }
            return target;
        }

        private <T extends EppEpvRow> T updateEpvRow(T target, ImEpvRowDTO source)
        {
            if (null == target.getOwner() || getBlock().equals(target.getOwner()))
            {
                target.setOwner(getBlock());
                target.setStoredIndex(source.getStoredIndex());
                target.setUserIndex(source.getUserIndex());

                ImtsaImportDao.this.saveOrUpdate(target);
            }
            return target;
        }

        @NotNull private EppEpvRow checkRowClass(@NotNull EppEpvRow row, @NotNull Class<? extends EppEpvRow> clazz)
        {
            if (!clazz.isAssignableFrom(row.getClass()))
                throw new ApplicationException("Невозможно импортировать учебный план. Неверный тип строки: «" + row.getTitle() + "».");

            return row;
        }
        
        private void updateRowLoad()
        {
            IEppEduPlanVersionDataDAO.instance.get().doUpdateRowLoad(_rowLoadMap, false);
        }

        // Инициализация
        private Map<String, OrgUnit> buildTutorOrgUnitMap()
        {
            Map<String, OrgUnit> resultMap = new HashMap<>();
            for (EppTutorOrgUnit tutorOrgUnit: ImtsaImportDao.this.getList(EppTutorOrgUnit.class, EppTutorOrgUnit.orgUnit().archival(), Boolean.FALSE))
            {
                String code = tutorOrgUnit.getCodeImtsa();
                if (null != code)
                {
                    if (resultMap.containsKey(code))
                        throw new ApplicationException("Один и тот же код ИМЦА (" + code+ ") указан одновременно для разных читающих подразделений.");
                    resultMap.put(code, tutorOrgUnit.getOrgUnit());
                }
            }
            return resultMap;
        }

        private Map<String, EppRegistryStructure> buildRegistryStructureMap()
        {
            ImmutableMap.Builder<String, EppRegistryStructure> builder = ImmutableMap.builder();
            for (EppRegistryStructure registryStructure: ImtsaImportDao.this.getList(EppRegistryStructure.class))
                builder.put(registryStructure.getCode(), registryStructure);
            return builder.build();
        }

        @NotNull private Map<String, EppEpvRow> buildEpvRowMap()
        {
            Map<String, EppEpvRow> result = new HashMap<>();
            EppEduPlanVersionBlock block = this.getBlock();
            EppEduPlanVersionRootBlock rootBlock = block.isRootBlock() ? ((EppEduPlanVersionRootBlock) block) :
                ImtsaImportDao.this.get(EppEduPlanVersionRootBlock.class, EppEduPlanVersionRootBlock.eduPlanVersion(), block.getEduPlanVersion());

            for (EppEpvRow row: ImtsaImportDao.this.getList(EppEpvRow.class, EppEpvRow.owner(), Arrays.asList(block, rootBlock)))
            {
                if (result.put(buildHierarchyPath(row), row) != null)
                    throw new ApplicationException("Невозможно импортировать учебный план. В блоке есть дубли дисциплин.");
            }

            return result;
        }

        @NotNull private String buildHierarchyPath(@NotNull EppEpvRow row)
        {
            Stack<String> paths = new Stack<>();
            while (row != null)
            {
                paths.push(row.getTitle());
                row = row.getHierarhyParent();
            }

            StringBuilder builder = new StringBuilder();
            while (!paths.isEmpty())
                builder.append(paths.pop()).append('\n');

            return builder.toString() ;
        }
    }

    @Override public boolean advancedModeEnabled() { return true; } // todo dev-6331
}