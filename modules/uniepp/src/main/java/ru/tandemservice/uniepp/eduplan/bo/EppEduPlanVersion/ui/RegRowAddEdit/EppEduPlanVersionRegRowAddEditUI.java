/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RegRowAddEdit;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.IUniEppDefines;
import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.BaseAddEditDao;
import ru.tandemservice.uniepp.component.registry.TutorOrgUnitAsOwnerSelectModel;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.registry.EppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.logic.regRowAddEdit.EppProfessionalTaskComboDSHandler;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAddEditBase.EppEduPlanVersionRowAddEditBaseUI;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAddEditBase.EppEduPlanVersionTermDistrRowAddEditBaseUI;
import ru.tandemservice.uniepp.entity.catalog.EppProfessionalTask;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniepp.ui.EppRegistryElementSelectModel;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 11/12/14
 */
@Input({
    @Bind(key = EppEduPlanVersionRowAddEditBaseUI.BIND_ROW_ID, binding = "row.id"),
    @Bind(key = EppEduPlanVersionRowAddEditBaseUI.BIND_BLOCK_ID, binding = EppEduPlanVersionRowAddEditBaseUI.FIELD_BLOCK_ID),
    @Bind(key = "scrollPos", binding = "scrollPos")
})
@Return({
    @Bind(key = "scroll", binding = "scroll"),
    @Bind(key = "scrollPos", binding = "scrollPos"),
    @Bind(key = "highlightRowId", binding = "row.id")
})
public class EppEduPlanVersionRegRowAddEditUI extends EppEduPlanVersionTermDistrRowAddEditBaseUI<EppEpvRegistryRow>
{
    private EppEpvRegistryRow row = new EppEpvRegistryRow();

    private List<EppProfessionalTask> _professionalTaskList;
    private List<HSelectOption> typeList = Collections.emptyList();
    private ISelectModel ownerSelectModel;
    private ISelectModel registrySelectModel;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        // поставим в мультиселект соответствующие проф. задачи (если у нас AddEdit)
        if (getRow() != null && getRow().getId() != null)
        {
            setProfessionalTaskList(IEppEduPlanVersionDataDAO.instance.get()
                    .getProfessionalTaskMap(Collections.singletonList(getRow())).get(getRow()));
        }

        final List<HSelectOption> parentList = IEppEduPlanVersionDataDAO.instance.get().getEpvRowHierarchyListFromBlock(getBlock(), EppEpvRegistryRow.PARENT_PREDICATE);
        BaseAddEditDao.disablePossibleLoop(parentList, Collections.singleton(getRow().getId()));
        setParentList(parentList);

        setTypeList(HierarchyUtil.listHierarchyNodesWithParents(IEppRegistryDAO.instance.get().getParentItems4Class(null), true));
        setOwnerSelectModel(new TutorOrgUnitAsOwnerSelectModel()
        {
            @Override
            protected OrgUnit getCurrentValue() { return getRow().getRegistryElementOwner(); }
        });

        setRegistrySelectModel(new EppRegistryElementSelectModel<EppRegistryElement>(EppRegistryElement.class)
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final EppRegistryStructure type = getRow().getType();
                if (null == type) { return null; }
                final Collection<Long> childTypeIds = getChilds(type);

                final EppRegistryElement registryElement = getRow().getRegistryElement();
                final IDQLExpression selectedElement = (null == registryElement ? null : eq(property(alias, "id"), value(registryElement.getId())));

                return super.query(alias, filter).where(or(
                    selectedElement,
                    and(
                        notIn(property(EppRegistryElement.state().code().fromAlias(alias)), EppState.UNACTUAL_STATES),
                        eq(property(EppRegistryElement.owner().fromAlias(alias)), value(getRow().getRegistryElementOwner())),
                        in(property(EppRegistryElement.parent().id().fromAlias(alias)), childTypeIds)
                    )
                ));
            }

            private Collection<Long> getChilds(EppRegistryStructure type)
            {
                final Set<Long> ids = new HashSet<>();
                ids.add(type.getId());
                collect(ids);
                return ids;
            }

            private void collect(Set<Long> ids)
            {
                while (true) {
                    boolean exit = true;
                    for (HSelectOption option : getTypeList()) {
                        EppRegistryStructure s = (EppRegistryStructure) option.getObject();
                        if (null != s.getParent() && ids.contains(s.getParent().getId())) {
                            if (ids.add(s.getId())) {
                                exit = false;
                            }
                        }
                    }
                    if (exit) { return; }
                }
            }

            @Override
            protected IDQLExpression getFilterCondition(final String alias, final String filter)
            {
                // проверяем еще и по всем названиям дисциплины (в других УП(в))
                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, "r")
                    .column(property(EppEpvRegistryRow.registryElement().id().fromAlias("r")))
                    .where(this.like(EppEpvRegistryRow.title().fromAlias("r"), filter));

                return or(
                    in(property(alias, "id"), dql.buildQuery()),
                    super.getFilterCondition(alias, filter)
                );
            }
        });
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppEduPlanVersionRegRowAddEdit.EPP_PROFESSIONAL_TASK_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppProfessionalTaskComboDSHandler.PROP_EDU_PLAN_VERSION_BLOCK_ID, getRow().getOwner().getId());
        }
        super.onBeforeDataSourceFetch(dataSource);
    }

    @Override
    protected void validate()
    {
        if (getRow().getRegistryElementType().getCode().equals(EppRegistryStructureCodes.REGISTRY_ATTESTATION))
        {
            throw new ApplicationException(getConfig().getProperty("ui.attestationInfo"));
        }

        super.validate();
    }

    @Override
    protected void saveData()
    {
        super.saveData();
        IEppEduPlanVersionDataDAO.instance.get().doSaveOrUpdateRegRowProfessionalTask(getProfessionalTaskList(), getRow());
    }

    public void onClickFillLoadFromRegistryElement() {
        final EppRegistryElement regEl = getRow().getRegistryElement();
        if (null == regEl) { return; }
        fillLoadFromRegistryElement(regEl, 1);
    }


    // presenter

    public boolean isFillLoadFromRegistryElementAllowed() {
        return (null != this.getRow().getRegistryElement());
    }

    public String getRegistryElementLoadAsString() {
        if (!this.isFillLoadFromRegistryElementAllowed()) { return ""; }

        final EppRegistryElement regEl = this.getRow().getRegistryElement();
        if (null == regEl) { return ""; }

        final ViewWrapper<IEntity> wrapper = EppRegistryDAO.fillRegistryElementWrappers(ViewWrapper.getPatchedList(Collections.<IEntity>singleton(regEl))).iterator().next();
        return (String) wrapper.getViewProperty("load");
    }

    public boolean isTermCountDoesNotMatch() {
        return isFillLoadFromRegistryElementAllowed() && getColumns().size() != getRow().getRegistryElement().getParts();
    }

    public String getRegElPubBcName() {
        if (getRow().getRegistryElement() instanceof EppRegistryAttestation) return IUniEppDefines.EPP_ATTESTATION_REGISTRY_PUB;
        if (getRow().getRegistryElement() instanceof EppRegistryDiscipline) return IUniEppDefines.EPP_DISCIPLINE_REGISTRY_PUB;
        if (getRow().getRegistryElement() instanceof EppRegistryPractice) return IUniEppDefines.EPP_PRACTICE_REGISTRY_PUB;
        return "";
    }

    // getters and setters

    @Override
    public EppEpvRegistryRow getRow()
    {
        return row;
    }

    @Override
    public void setRow(EppEpvRegistryRow row)
    {
        this.row = row;
    }

    public List<EppProfessionalTask> getProfessionalTaskList()
    {
        return _professionalTaskList;
    }

    public void setProfessionalTaskList(List<EppProfessionalTask> professionalTaskList)
    {
        _professionalTaskList = professionalTaskList;
    }

    public List<HSelectOption> getTypeList()
    {
        return typeList;
    }

    public void setTypeList(List<HSelectOption> typeList)
    {
        this.typeList = typeList;
    }

    public ISelectModel getOwnerSelectModel()
    {
        return ownerSelectModel;
    }

    public void setOwnerSelectModel(ISelectModel ownerSelectModel)
    {
        this.ownerSelectModel = ownerSelectModel;
    }

    public ISelectModel getRegistrySelectModel()
    {
        return registrySelectModel;
    }

    public void setRegistrySelectModel(ISelectModel registrySelectModel)
    {
        this.registrySelectModel = registrySelectModel;
    }

    // inner classes


}