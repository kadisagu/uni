/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.student.EppStudentPub;

import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;

import java.util.Map;

/**
 * @author vip_delete
 * @since 24.03.2010
 */
// @Input(@Bind(key="studentId", binding="studentHolder.id"))
public class Model
{
    private final EntityHolder<Student> studentHolder = new EntityHolder<Student>();

    public EntityHolder<Student> getStudentHolder()
    {
        return this.studentHolder;
    }

    public Long getStudentId()
    {
        return this.getStudentHolder().getId();
    }

    public Student getStudent()
    {
        return this.getStudentHolder().getValue();
    }

    public Map<String, Object> getParameters()
    {
        return new ParametersMap().add("studentId", this.getStudentId());
    }

    public boolean isWorkplanTabVisible()
    {
        return (null != IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(this.getStudentId()));
    }

}
