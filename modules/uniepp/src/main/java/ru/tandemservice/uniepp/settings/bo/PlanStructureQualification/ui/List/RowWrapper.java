package ru.tandemservice.uniepp.settings.bo.PlanStructureQualification.ui.List;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author avedernikov
 * @since 14.09.2015
 */

public class RowWrapper extends ViewWrapper<EppPlanStructure>
{
	private RowWrapper parent = null;

	RowWrapper(EppPlanStructure planStructure)
	{
		super(planStructure);
	}

	@Override
	public IHierarchyItem getHierarhyParent()
	{
		return parent;
	}

	public void setHierarchyParent(RowWrapper parent)
	{
		this.parent = parent;
	}

	public static void assignWrapperParents(List<RowWrapper> wrappers)
	{
		Map<IHierarchyItem, RowWrapper> wrapped2wrapper = wrappers.stream().collect(Collectors.toMap(RowWrapper::getEntity, wr -> wr));
		wrappers.forEach(wrapper -> wrapper.setHierarchyParent(wrapped2wrapper.get(wrapper.getEntity().getParent())));
	}
}
