package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Индивидуальный учебный план
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppCustomEduPlanGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan";
    public static final String ENTITY_NAME = "eppCustomEduPlan";
    public static final int VERSION_HASH = -1990394827;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPV_BLOCK = "epvBlock";
    public static final String L_PLAN = "plan";
    public static final String L_DEVELOP_GRID = "developGrid";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_STATE = "state";
    public static final String P_NUMBER = "number";
    public static final String P_COMMENT = "comment";
    public static final String P_DEVELOP_COMBINATION_TITLE = "developCombinationTitle";
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_WITH_GRID = "titleWithGrid";

    private EppEduPlanVersionSpecializationBlock _epvBlock;     // Блок версии УП
    private EppEduPlanProf _plan;     // Учебный план
    private DevelopGrid _developGrid;     // Учебная сетка
    private DevelopCondition _developCondition;     // Условие освоения
    private EppState _state;     // Состояние
    private String _number;     // Номер
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок версии УП. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionSpecializationBlock getEpvBlock()
    {
        return _epvBlock;
    }

    /**
     * @param epvBlock Блок версии УП. Свойство не может быть null.
     */
    public void setEpvBlock(EppEduPlanVersionSpecializationBlock epvBlock)
    {
        dirty(_epvBlock, epvBlock);
        _epvBlock = epvBlock;
    }

    /**
     * Поле для кэширования ссылки на УП из версии блока, чтобы не делать многие джойны в запросах.
     *
     * @return Учебный план. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanProf getPlan()
    {
        return _plan;
    }

    /**
     * @param plan Учебный план. Свойство не может быть null.
     */
    public void setPlan(EppEduPlanProf plan)
    {
        dirty(_plan, plan);
        _plan = plan;
    }

    /**
     * @return Учебная сетка. Свойство не может быть null.
     */
    @NotNull
    public DevelopGrid getDevelopGrid()
    {
        return _developGrid;
    }

    /**
     * @param developGrid Учебная сетка. Свойство не может быть null.
     */
    public void setDevelopGrid(DevelopGrid developGrid)
    {
        dirty(_developGrid, developGrid);
        _developGrid = developGrid;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения. Свойство не может быть null.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public EppState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(EppState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * Номер в рамках версии УПв
     *
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        initLazyForGet("comment");
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        initLazyForSet("comment");
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppCustomEduPlanGen)
        {
            setEpvBlock(((EppCustomEduPlan)another).getEpvBlock());
            setPlan(((EppCustomEduPlan)another).getPlan());
            setDevelopGrid(((EppCustomEduPlan)another).getDevelopGrid());
            setDevelopCondition(((EppCustomEduPlan)another).getDevelopCondition());
            setState(((EppCustomEduPlan)another).getState());
            setNumber(((EppCustomEduPlan)another).getNumber());
            setComment(((EppCustomEduPlan)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppCustomEduPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppCustomEduPlan.class;
        }

        public T newInstance()
        {
            return (T) new EppCustomEduPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "epvBlock":
                    return obj.getEpvBlock();
                case "plan":
                    return obj.getPlan();
                case "developGrid":
                    return obj.getDevelopGrid();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "state":
                    return obj.getState();
                case "number":
                    return obj.getNumber();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "epvBlock":
                    obj.setEpvBlock((EppEduPlanVersionSpecializationBlock) value);
                    return;
                case "plan":
                    obj.setPlan((EppEduPlanProf) value);
                    return;
                case "developGrid":
                    obj.setDevelopGrid((DevelopGrid) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "state":
                    obj.setState((EppState) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "epvBlock":
                        return true;
                case "plan":
                        return true;
                case "developGrid":
                        return true;
                case "developCondition":
                        return true;
                case "state":
                        return true;
                case "number":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "epvBlock":
                    return true;
                case "plan":
                    return true;
                case "developGrid":
                    return true;
                case "developCondition":
                    return true;
                case "state":
                    return true;
                case "number":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "epvBlock":
                    return EppEduPlanVersionSpecializationBlock.class;
                case "plan":
                    return EppEduPlanProf.class;
                case "developGrid":
                    return DevelopGrid.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "state":
                    return EppState.class;
                case "number":
                    return String.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppCustomEduPlan> _dslPath = new Path<EppCustomEduPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppCustomEduPlan");
    }
            

    /**
     * @return Блок версии УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getEpvBlock()
     */
    public static EppEduPlanVersionSpecializationBlock.Path<EppEduPlanVersionSpecializationBlock> epvBlock()
    {
        return _dslPath.epvBlock();
    }

    /**
     * Поле для кэширования ссылки на УП из версии блока, чтобы не делать многие джойны в запросах.
     *
     * @return Учебный план. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getPlan()
     */
    public static EppEduPlanProf.Path<EppEduPlanProf> plan()
    {
        return _dslPath.plan();
    }

    /**
     * @return Учебная сетка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getDevelopGrid()
     */
    public static DevelopGrid.Path<DevelopGrid> developGrid()
    {
        return _dslPath.developGrid();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getState()
     */
    public static EppState.Path<EppState> state()
    {
        return _dslPath.state();
    }

    /**
     * Номер в рамках версии УПв
     *
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getDevelopCombinationTitle()
     */
    public static SupportedPropertyPath<String> developCombinationTitle()
    {
        return _dslPath.developCombinationTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getFullTitle()
     */
    public static SupportedPropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getTitleWithGrid()
     */
    public static SupportedPropertyPath<String> titleWithGrid()
    {
        return _dslPath.titleWithGrid();
    }

    public static class Path<E extends EppCustomEduPlan> extends EntityPath<E>
    {
        private EppEduPlanVersionSpecializationBlock.Path<EppEduPlanVersionSpecializationBlock> _epvBlock;
        private EppEduPlanProf.Path<EppEduPlanProf> _plan;
        private DevelopGrid.Path<DevelopGrid> _developGrid;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private EppState.Path<EppState> _state;
        private PropertyPath<String> _number;
        private PropertyPath<String> _comment;
        private SupportedPropertyPath<String> _developCombinationTitle;
        private SupportedPropertyPath<String> _fullTitle;
        private SupportedPropertyPath<String> _title;
        private SupportedPropertyPath<String> _titleWithGrid;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок версии УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getEpvBlock()
     */
        public EppEduPlanVersionSpecializationBlock.Path<EppEduPlanVersionSpecializationBlock> epvBlock()
        {
            if(_epvBlock == null )
                _epvBlock = new EppEduPlanVersionSpecializationBlock.Path<EppEduPlanVersionSpecializationBlock>(L_EPV_BLOCK, this);
            return _epvBlock;
        }

    /**
     * Поле для кэширования ссылки на УП из версии блока, чтобы не делать многие джойны в запросах.
     *
     * @return Учебный план. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getPlan()
     */
        public EppEduPlanProf.Path<EppEduPlanProf> plan()
        {
            if(_plan == null )
                _plan = new EppEduPlanProf.Path<EppEduPlanProf>(L_PLAN, this);
            return _plan;
        }

    /**
     * @return Учебная сетка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getDevelopGrid()
     */
        public DevelopGrid.Path<DevelopGrid> developGrid()
        {
            if(_developGrid == null )
                _developGrid = new DevelopGrid.Path<DevelopGrid>(L_DEVELOP_GRID, this);
            return _developGrid;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getState()
     */
        public EppState.Path<EppState> state()
        {
            if(_state == null )
                _state = new EppState.Path<EppState>(L_STATE, this);
            return _state;
        }

    /**
     * Номер в рамках версии УПв
     *
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppCustomEduPlanGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EppCustomEduPlanGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getDevelopCombinationTitle()
     */
        public SupportedPropertyPath<String> developCombinationTitle()
        {
            if(_developCombinationTitle == null )
                _developCombinationTitle = new SupportedPropertyPath<String>(EppCustomEduPlanGen.P_DEVELOP_COMBINATION_TITLE, this);
            return _developCombinationTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getFullTitle()
     */
        public SupportedPropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new SupportedPropertyPath<String>(EppCustomEduPlanGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EppCustomEduPlanGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan#getTitleWithGrid()
     */
        public SupportedPropertyPath<String> titleWithGrid()
        {
            if(_titleWithGrid == null )
                _titleWithGrid = new SupportedPropertyPath<String>(EppCustomEduPlanGen.P_TITLE_WITH_GRID, this);
            return _titleWithGrid;
        }

        public Class getEntityClass()
        {
            return EppCustomEduPlan.class;
        }

        public String getEntityName()
        {
            return "eppCustomEduPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDevelopCombinationTitle();

    public abstract String getFullTitle();

    public abstract String getTitle();

    public abstract String getTitleWithGrid();
}
