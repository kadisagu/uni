/* $Id$ */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionTrajectoryList;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

/**
 * @author oleyba
 * @since 5/5/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final Session session = this.getSession();
        model.setEduPlanVersion(this.get(EppEduPlanVersion.class, model.getVersionId()));

        final DevelopGrid developGrid = this.get(EppEduPlanVersion.class, model.getVersionId()).getEduPlan().getDevelopGrid();

        final int termCount = new DQLSelectBuilder()
        .fromEntity(DevelopGridTerm.class, "t").column("t")
        .where(eq(property(DevelopGridTerm.developGrid().fromAlias("t")), value(developGrid)))
        .createCountStatement(new DQLExecutionContext(session))
        .<Number>uniqueResult().intValue();

        final Map<Long, Map<Integer, EppWorkPlanBase>> trajectoryByStudent = SafeMap.get(HashMap.class);

        final DQLSelectBuilder workplanRelaionsDQL = new DQLSelectBuilder()
        .fromEntity(EppStudent2WorkPlan.class, "s2wp")
        .column(property("s2wp"))
        .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias("s2wp"), "wp")
        .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("s2wp"), "s2epv")
        .fetchPath(DQLJoinType.inner, EppStudent2EduPlanVersion.student().fromAlias("s2epv"), "st")
        .where(isNull(property(EppStudent2WorkPlan.removalDate().fromAlias("s2wp"))))
        .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
        .where(eq(property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("s2epv")), value(model.getVersionId())))
        .where(eq(property(Student.status().active().fromAlias("st")), value(Boolean.TRUE)))
        .where(eq(property(Student.archival().fromAlias("st")), value(Boolean.FALSE)));

        for (final EppStudent2WorkPlan rel : workplanRelaionsDQL.createStatement(session).<EppStudent2WorkPlan>list()) {
            trajectoryByStudent.get(rel.getStudentEduPlanVersion().getStudent().getId()).put(rel.getGridTerm().getTerm().getIntValue(), rel.getWorkPlan());
        }

        final Map<MultiKey, EppTrajectoryWrapper> trajectoryWrapperByTrajectoryKey = new HashMap<MultiKey, EppTrajectoryWrapper>();
        for (final Map.Entry<Long, Map<Integer, EppWorkPlanBase>> entry : trajectoryByStudent.entrySet())
        {
            final Long student = entry.getKey();
            final Map<Integer, EppWorkPlanBase> studentTrajectoryMap = entry.getValue();
            final Object[] trajectoryKeyContent = new Object[termCount];
            for (int i = 0; i < termCount; i++) {
                trajectoryKeyContent[i] = studentTrajectoryMap.get(i+1);
            }

            final MultiKey trajectoryKey = new MultiKey(trajectoryKeyContent);
            final EppTrajectoryWrapper trajectoryWrapper = trajectoryWrapperByTrajectoryKey.get(trajectoryKey);
            if (null == trajectoryWrapper)
            {
                final List<EppWorkPlanBase> workplanList = new ArrayList<EppWorkPlanBase>(studentTrajectoryMap.values());
                Collections.sort(workplanList, EppWorkPlanBase.BY_TERM_COMPARATOR);
                trajectoryWrapperByTrajectoryKey.put(trajectoryKey, new EppTrajectoryWrapper(workplanList, student));
            }
            else
            {
                trajectoryWrapper.getStudentIds().add(student);
            }
        }

        final ArrayList<EppTrajectoryWrapper> wrappers = new ArrayList<EppTrajectoryWrapper>(trajectoryWrapperByTrajectoryKey.values());
        this.checkErrors(wrappers);

        final SimpleListDataSource<EppTrajectoryWrapper> dataSource = new SimpleListDataSource<EppTrajectoryWrapper>();
        dataSource.setEntityCollection(wrappers);
        model.setDataSource(dataSource);
    }

    private void checkErrors(final List<EppTrajectoryWrapper> wrappers)
    {
        final Set<Long> allWorkplanIds = new HashSet<Long>();
        for (final EppTrajectoryWrapper wrapper : wrappers) {
            allWorkplanIds.addAll(wrapper.getWorkplanIds());
        }
        final Map<Long, IEppWorkPlanWrapper> map = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(allWorkplanIds);
        for (final EppTrajectoryWrapper wrapper : wrappers)
        {
            final Map<Integer, IEppWorkPlanWrapper> workplanByTerm = new TreeMap<Integer, IEppWorkPlanWrapper>();
            for (final IEppWorkPlanWrapper w: map.values()) {
                if (wrapper.getWorkplanIds().contains(w.getWorkPlan().getId()) && (null != workplanByTerm.put(w.getWorkPlan().getTerm().getIntValue(), w))) {
                    throw new IllegalStateException();
                }
            }

            final Map<Long, List<Integer>> discTrajectoryByDiscId = new HashMap<Long, List<Integer>>();

            for (final Map.Entry<Integer, IEppWorkPlanWrapper> termEntry: workplanByTerm.entrySet()) {
                for (final IEppWorkPlanRowWrapper row : termEntry.getValue().getRowMap().values())
                {
                    Long registryElementId = row.getId();
                    if (row.getRow() instanceof EppWorkPlanRegistryElementRow) {
                        registryElementId = ((EppWorkPlanRegistryElementRow)row.getRow()).getRegistryElement().getId();
                    }
                    // добавляем номер части в траекторию дисциплины
                    SafeMap.safeGet(discTrajectoryByDiscId, registryElementId, ArrayList.class).add(row.getRegistryElementPartNumber());
                }
            }

            for (final Map.Entry<Long, List<Integer>> discTrajectory : discTrajectoryByDiscId.entrySet())
            {
                final List<Integer> t = discTrajectory.getValue();
                if (t.isEmpty() || (t.size() == 1)) { }

                final Set<Integer> s = new TreeSet<Integer>(t);

                if (s.size() < t.size())
                {
                    wrapper.setHasErrors(true); // что-то повторяется
                    wrapper.setErrorColor("#FAB5B0");
                }
                else if (t.size() < ((1 + Collections.max(t)) - Collections.min(t)))
                {
                    wrapper.setHasErrors(true); // что-то пропустили
                    wrapper.setErrorColor("#FAEEB0");
                }
                else if (!CollectionUtils.isEqualCollection(t, s))
                {
                    wrapper.setHasErrors(true); // не в той последовательности
                    wrapper.setErrorColor("#FAEEB0");
                }
            }
        }
    }
}
