package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x7x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppEducationOrgUnitEduPlanVersion

		// создано обязательное свойство applyOnlyForFirstCourse
		{
			// создать колонку
			tool.createColumn("epp_eduou_epv_rel_t", new DBColumn("applyonlyforfirstcourse_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultApplyOnlyForFirstCourse = true;
			tool.executeUpdate("update epp_eduou_epv_rel_t set applyonlyforfirstcourse_p=? where applyonlyforfirstcourse_p is null", defaultApplyOnlyForFirstCourse);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_eduou_epv_rel_t", "applyonlyforfirstcourse_p", false);

		}


    }
}