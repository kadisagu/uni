package ru.tandemservice.uniepp.component.registry.base.AddEdit;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import org.tandemframework.shared.commonbase.remoteDocument.service.ICommonRemoteDocumentService;
import org.tandemframework.shared.commonbase.remoteDocument.service.IRemoteDocumentProvider;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.component.registry.TutorOrgUnitAsOwnerSelectModel;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.gen.EppStateGen;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad;
import ru.tandemservice.uniepp.remoteDocument.ext.RemoteDocument.RemoteDocumentExtManager;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vdanilov
 */
public class DAO<T extends EppRegistryElement> extends UniBaseDao implements IDAO<T> {

    @Override
    public void prepare(final Model<T> model) {
        final IEntity entity = (null == model.getId() ? null : this.get(model.getId()));
        this.prepareElement(model, entity);
        this.prepareOwnerSelectModel(model, entity);
        this.prepareEduGroupSplitVariantSelectModel(model);
        this.prepareEduGroupType(model);
        model.setEduGroupTypes(IEppRegistryDAO.instance.get().getRegElementSplitGroupTypes(model.getElement()));

        model.setParentList(HierarchyUtil.listHierarchyNodesWithParents(IEppRegistryDAO.instance.get().getParentItems4Class(model.getElementClass()), false));

        // TODO: сделать настройку (пока - сразу состояние "утвержден")
        if (null == model.getElement().getState()) {
            model.getElement().setState(this.get(EppState.class, EppStateGen.P_CODE, EppState.STATE_FORMATIVE));
        }

        model.setLoadTypeMap(EppLoadTypeUtils.getLoadTypeMap());

        final Long elementId = model.getElement().getId();
        if (null == elementId) {
            model.setLoadMap(new HashMap<>());
        } else {
            final Map<Long, Map<String, EppRegistryElementLoad>> disciplineLoadMap = IEppRegistryDAO.instance.get().getRegistryElementTotalLoadMap(Collections.singleton(elementId));
            model.setLoadMap(disciplineLoadMap.get(elementId));
        }
    }

    private void prepareEduGroupSplitVariantSelectModel(final Model<T> model){
        model.setEduGroupSplitVariantModel(new LazySimpleSelectModel<>(EppEduGroupSplitVariant.class));
    }

    private void prepareEduGroupType(final Model<T> model){
        model.setEduGroupTypeModel(new LazySimpleSelectModel<>(getList(EppGroupType.class, EppGroupType.priority().s())));
    }

    private void prepareOwnerSelectModel(final Model<T> model, final IEntity entity) {
        if (entity instanceof OrgUnit) {
            // если указано подразделение, то ничего не сделать (оно и есть)
            model.setOwnerSelectModel(new StaticSelectModel("id", OrgUnitGen.fullTitle().s(), Collections.singleton(entity)));

        } else {
            // если подразделение не указано - то показываем все из настройки
            model.setOwnerSelectModel(new TutorOrgUnitAsOwnerSelectModel() {
                @Override protected OrgUnit getCurrentValue() {
                    return (null == model.getElement() ? null : model.getElement().getOwner());
                }
            });
        }
    }

    @SuppressWarnings("unchecked")
    protected void prepareElement(final Model<T> model, final IEntity entity) {
        if (entity instanceof EppRegistryElement) {
            model.setElement((T)entity);
        }

        if (null == model.getElement()) {
            try {
                model.setElement(model.getElementClass().newInstance());
                model.getElement().setNumber(INumberQueueDAO.instance.get().getNextNumber(model.getElement()));
                model.setupElementDefaultLoad();
            }
            catch (final Throwable t) {
                throw new RuntimeException(t);
            }
        }
        if (null == model.getElement().getOwner()) {
            if (entity instanceof OrgUnit) {
                model.getElement().setOwner((OrgUnit) entity);
            }
        }
        if (null == model.getElement().getState()) {
            model.getElement().setState(this.get(EppState.class, EppStateGen.P_CODE, EppState.STATE_FORMATIVE));
        }
    }


    @Override
    public void save(final Model<T> model)
    {
        EppRegistryElement element = model.getElement();
        IEppRegistryDAO.instance.get().doSaveRegistryElement(element, model.getEduGroupTypes());

        Long wpAttachId = getAttachmentId(element, element.getWorkProgramFile(), model.getWorkProgramFile(), RemoteDocumentExtManager.EPP_REGISTRY_WORK_PROGRAM_ANNOTATION_SERVICE_NAME, "Рабочая программа");
        if (null != wpAttachId)
        {
            // Явно вызываем update для срабатывания listener'а
            new DQLUpdateBuilder(EppRegistryElement.class)
                    .set(EppRegistryElement.workProgramFile().s(), DQLExpressions.value(wpAttachId))
                    .where(DQLExpressions.eq(DQLExpressions.property(EppRegistryElement.id()), DQLExpressions.value(element.getId())))
                    .createStatement(getSession())
                    .execute();
        }

        Long wpaAttachId = getAttachmentId(element, element.getWorkProgramAnnotation(), model.getWorkProgramAnnotation(), RemoteDocumentExtManager.EPP_REGISTRY_WORK_PROGRAM_ANNOTATION_SERVICE_NAME, "Аннотация рабочей программы");
        if (null != wpaAttachId)
        {
            // Явно вызываем update для срабатывания listener'а
            new DQLUpdateBuilder(EppRegistryElement.class)
                    .set(EppRegistryElement.workProgramAnnotation().s(), DQLExpressions.value(wpaAttachId))
                    .where(DQLExpressions.eq(DQLExpressions.property(EppRegistryElement.id()), DQLExpressions.value(element.getId())))
                    .createStatement(getSession())
                    .execute();
        }
        if (wpaAttachId != null || wpAttachId != null)
            refresh(element);

        final Long elementId = model.getElement().getId();
        for (final EppLoadType loadType: this.getList(EppLoadType.class)) {
            final EppRegistryElementLoad load = model.getLoadMap().get(loadType.getFullCode());
            if (null != load) {
                load.setLoadType(loadType);
                load.setRegistryElement(model.getElement());
            }
        }

        final Map<Long, Map<String, EppRegistryElementLoad>> disciplineLoadMap = IEppRegistryDAO.instance.get().getRegistryElementTotalLoadMap(Collections.singleton(elementId));
        disciplineLoadMap.put(elementId, model.getLoadMap());
        IEppRegistryDAO.instance.get().saveRegistryElementTotalLoadMap(disciplineLoadMap);
    }

    private Long getAttachmentId(EppRegistryElement element, Long loadFileId, IUploadFile file, String taskName, String preamble)
    {
        return loadFileId != null ? updateRemoteDocument(element, loadFileId, file, taskName,preamble) : file != null ? createRemoteDocument(element, file, taskName, preamble) : null;
    }

    private Long updateRemoteDocument(EppRegistryElement element, Long loadFileId, IUploadFile file, String taskName, String preamble)
    {
        try (ICommonRemoteDocumentService documentService = IRemoteDocumentProvider.instance.get().taskService(taskName))
        {
            RemoteDocumentDTO documentDTO = documentService.get(loadFileId);
            if(null == documentDTO)
            {
                return createRemoteDocument(element, file, taskName, preamble);
            }

            if(null != file)
                documentDTO.setFileContent(file.getFileName(), IOUtils.toByteArray(file.getStream()));

            documentService.update(documentDTO);
            return documentDTO.getId();
        }
        catch (IOException e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }

    private Long createRemoteDocument(EppRegistryElement element, IUploadFile file, String taskName, String preamble)
    {
        try (ICommonRemoteDocumentService documentService = IRemoteDocumentProvider.instance.get().taskService(taskName))
        {
            RemoteDocumentDTO documentDTO = new RemoteDocumentDTO(preamble + " " + element.getTitle(), RemoteDocumentExtManager.REMOTE_DOCUMENT_MODULE, taskName);
            documentDTO.setOwnerId(element.getId());

            if(null != file)
                documentDTO.setFileContent(file.getFileName(), IOUtils.toByteArray(file.getStream()));

            Long attachmentId = documentService.insert(documentDTO);

            return attachmentId;
        }
        catch (IOException e) {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }
}
