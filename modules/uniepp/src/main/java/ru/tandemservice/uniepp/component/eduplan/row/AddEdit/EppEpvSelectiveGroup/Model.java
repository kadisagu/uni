package ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvSelectiveGroup;

import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.LoadAddEditModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;

/**
 * @author vdanilov
 */
public class Model extends LoadAddEditModel<EppEpvGroupImRow> {

    @Override protected EppEpvGroupImRow newInstance(final EppEduPlanVersionBlock block) {
        return new EppEpvGroupImRow(block);
    }

    @Override protected EppEpvGroupImRow check(final EppEpvGroupImRow entity) {
        return entity;
    }

}

