// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.settings.DefaultSelfworkForSizeAddEdit;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uniepp.entity.settings.EppDefaultSelfworkForSize;

import java.util.Arrays;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 08.10.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null != model.getRule().getId()) {
            model.setRule(this.get(EppDefaultSelfworkForSize.class, model.getRule().getId()));
        }
        model.setDevelopFormList(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionList(new LazySimpleSelectModel<DevelopCondition>(DevelopCondition.class));
        model.setDevelopTechList(new LazySimpleSelectModel<DevelopTech>(DevelopTech.class));
        model.setDevelopGridList(new LazySimpleSelectModel<DevelopGrid>(DevelopGrid.class));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(final Model model)
    {
        NamedSyncInTransactionCheckLocker.register(this.getSession(), "EppDefaultSelfworkForSize");
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppDefaultSelfworkForSize.class, "rule").column(property("rule"));
        for (final PairKey<String, Object> pair : Arrays.asList(
                new PairKey(EppDefaultSelfworkForSize.developForm().s(), model.getRule().getDevelopForm()),
                new PairKey(EppDefaultSelfworkForSize.developCondition().s(), model.getRule().getDevelopCondition()),
                new PairKey(EppDefaultSelfworkForSize.developTech().s(), model.getRule().getDevelopTech()),
                new PairKey(EppDefaultSelfworkForSize.developGrid().s(), model.getRule().getDevelopGrid()))) {
            if (null == pair.getSecond()) {
                dql.where(isNull(property("rule", pair.getFirst())));
            } else {
                dql.where(eq(property("rule", pair.getFirst()), commonValue(pair.getSecond())));
            }
        }
        if (model.getRule().getId() != null) {
            dql.where(ne(property("rule", "id"), value(model.getRule().getId())));
        }
        if ( dql.createStatement(this.getSession()).uniqueResult() != null) {
            throw new ApplicationException("Невозможно сохранить правило, так как правило для такой комбинации уже существует.");
        }
        this.getSession().saveOrUpdate(model.getRule());
    }
}
