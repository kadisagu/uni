package ru.tandemservice.uniepp.entity.plan;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanGen;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanHigherProfGen;

/**
 * Учебный план ВО
 */
public class EppEduPlanHigherProf extends EppEduPlanHigherProfGen
{
    private String getTitle(boolean withArchive)
    {
        final StringBuilder sb = new StringBuilder(24);
        sb.append("№").append(this.getNumber());
        if (withArchive && EppState.STATE_ARCHIVED.equals(getState().getCode())) {
            sb.append(" архив");
        }
        sb.append(" ").append(getProgramSubject().getShortTitleWithCode());

        final String titlePostfix = StringUtils.trimToNull(this.getTitlePostfix());
        if (null != titlePostfix) { sb.append(" ").append(titlePostfix); }

        if (getProgramOrientation() != null) {
            sb.append(" (").append(getProgramOrientation().getShortTitle()).append(")");
        }

        return sb.toString();
    }

    @EntityDSLSupport(parts = { EppEduPlanGen.P_NUMBER })
    @Override
    public String getTitle()
    {
        if (getProgramSubject() == null) {
            return this.getClass().getSimpleName();
        }
        return getTitle(false);
    }

    @Override
    public String getTitleWithArchive()
    {
        return getTitle(true);
    }

    @Override
    public String getInfoBlockPageName()
    {
        return "ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.Pub.EppEduPlanHigherProfInfoBlock";
    }

    @Override
    public EduProgramOrientation getOrientation() {
        return this.getProgramOrientation();
    }

    @Override
    public Class<? extends EduProgram> getEduProgramClass()
    {
        return EduProgramHigherProf.class;
    }
}