package ru.tandemservice.uniepp.entity.plan;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionWeekTypeGen;

/**
 * Связь типа недели в версии учебного плана с курсом
 */
public class EppEduPlanVersionWeekType extends EppEduPlanVersionWeekTypeGen
{

    public EppEduPlanVersionWeekType() {}
    public EppEduPlanVersionWeekType(final EppEduPlanVersion eduPlanVersion, final Course course, final EppWeek week) {
        this.setEduPlanVersion(eduPlanVersion);
        this.setCourse(course);
        this.setWeek(week);
    }
}