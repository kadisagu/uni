package ru.tandemservice.uniepp.base.bo.EppContract.ui.Wizard;

import java.util.Collection;
import java.util.Collections;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.InWizardDataStep.CtrContractVersionInWizardDataStepUI;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.Wizard.CtrContractWizardUIPrsenter;

import ru.tandemservice.uniepp.IUniEppDefines;

/**
 * @author vdanilov
 */
@Output({
    @Bind(key=CtrContractVersionInWizardDataStepUI.CONTRACT_TYPE_CODES, binding="contractTypeCodes")
})
public class EppContractWizardUI extends CtrContractWizardUIPrsenter
{

    /* @Output */
    public Collection<String> getContractTypeCodes() {
        return Collections.singleton(IUniEppDefines.CTR_CONTRACT_TYPE_EPP);
    }

}
