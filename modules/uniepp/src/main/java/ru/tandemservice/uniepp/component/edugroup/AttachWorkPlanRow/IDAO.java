package ru.tandemservice.uniepp.component.edugroup.AttachWorkPlanRow;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {
    void save(Model model);
}
