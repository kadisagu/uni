package ru.tandemservice.uniepp.component.settings.WeekType2LoadType;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * 
 * @author nkokorina
 *
 */

public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{

}
