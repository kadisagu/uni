package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionWorkPlanList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

/**
 * @author vdanilov
 */

public class Model extends ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub.EduPlanVersionPubModel {

    private IDataSettings settings;
    public IDataSettings getSettings() { return this.settings; }
    public void setSettings(final IDataSettings settings) { this.settings = settings; }

    private DynamicListDataSource<EppWorkPlan> dataSource;
    public DynamicListDataSource<EppWorkPlan> getDataSource() { return this.dataSource; }
    public void setDataSource(final DynamicListDataSource<EppWorkPlan> dataSource) { this.dataSource = dataSource; }

}
