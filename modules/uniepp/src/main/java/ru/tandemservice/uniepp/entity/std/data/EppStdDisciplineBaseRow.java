package ru.tandemservice.uniepp.entity.std.data;

import ru.tandemservice.uniepp.entity.std.data.gen.EppStdDisciplineBaseRowGen;

/**
 * Запись ГОС (базовая запись для дисциплины)
 */
public abstract class EppStdDisciplineBaseRow extends EppStdDisciplineBaseRowGen
{
    @Override public boolean isStructureElement() { return false; }

}