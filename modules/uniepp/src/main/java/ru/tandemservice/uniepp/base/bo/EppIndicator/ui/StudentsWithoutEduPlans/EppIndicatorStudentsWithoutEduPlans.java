/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithoutEduPlans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.IPublisherColumnBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListManager;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
@Configuration
public class EppIndicatorStudentsWithoutEduPlans extends AbstractEppIndicatorStudentListManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return  super.presenterExtPoint(
            presenterExtPointBuilder()
            .addDataSource(searchListDS(STUDENT_SEARCH_LIST_DS, studentSearchListDSColumnExtPoint(), studentSearchListDSHandler())));

    }

    @Bean
    public ColumnListExtPoint studentSearchListDSColumnExtPoint()
    {
        return createStudentSearchListColumnsBuilder()
        .create();
    }

    @Override
    protected IPublisherColumnBuilder newFioColumnBuilder(IMergeRowIdResolver merger) {
        final IPublisherLinkResolver resolver = new DefaultPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) {
                return new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId())
                .add("selectedStudentTab","studentTab")
                .add("selectedDataTab","studentEppEduplanTab");
            }
        };
        return publisherColumn(FIO_COLUMN, Student.person().fullFio()).formatter(RawFormatter.INSTANCE).publisherLinkResolver(resolver).order().required(true);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchListDSHandler()
    {
        return new EppIndicatorStudentsWithoutEduPlansSearchListDSHandler(getName());
    }
}
