package ru.tandemservice.uniepp.component.settings.WeekType2LoadType;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.catalog.DevelopForm;

/**
 * 
 * @author nkokorina
 *
 */

public class Model
{
    private DynamicListDataSource<DevelopForm> dataSource;

    public DynamicListDataSource<DevelopForm> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<DevelopForm> dataSource)
    {
        this.dataSource = dataSource;
    }
}
