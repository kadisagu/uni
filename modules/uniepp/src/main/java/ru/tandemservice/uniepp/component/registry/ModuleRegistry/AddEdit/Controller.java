package ru.tandemservice.uniepp.component.registry.ModuleRegistry.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import ru.tandemservice.uni.util.AddEditResult;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public class Controller extends AbstractBusinessController<IDAO, Model> {

    public static Long executeSave(final IBusinessComponent component) {
        final Model model = (Model)component.getModel();
        if (Boolean.TRUE.equals(model.isReadOnly())) { return model.getElement().getId(); }

        ((Controller)component.getController()).save(component);
        return model.getElement().getId();
    }


    @Override public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }


    public void onClickApply(final IBusinessComponent component) {
        AddEditResult.saveAndDeactivate(component, this.getModel(component).getElement(), () -> Controller.this.save(component));
    }


    protected void save(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().save(model);
    }

}
