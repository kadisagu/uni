/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.AdditionalProfList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanAdditionalProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/28/14
 */
public class EppEduPlanAdditionalProfDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String  VIEW_PROP_VERSION = "version";

    public EppEduPlanAdditionalProfDSHandler(String ownerID)
    {
        super(ownerID);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {

        DQLOrderDescriptionRegistry orderDescription = new DQLOrderDescriptionRegistry(EppEduPlanAdditionalProf.class, "p");
        DQLSelectBuilder builder = orderDescription.buildDQLSelectBuilder().column("p");

        IDataSettings settings = context.get("settings");

        builder.where(eq(property("p", EppEduPlan.programKind()), commonValue(settings.get("programKind"))));
        FilterUtils.applySimpleLikeFilter(builder, "p", EppEduPlanAdditionalProf.number(), settings.<String>get("number"));
        FilterUtils.applySimpleLikeFilter(builder, "p", EppEduPlanAdditionalProf.eduProgram().title(), settings.<String>get("program"));
        FilterUtils.applySelectFilter(builder, "p", EppEduPlanAdditionalProf.programForm(), settings.get("programForm"));
        FilterUtils.applySelectFilter(builder, "p", EppEduPlanAdditionalProf.developCondition(), settings.get("developCondition"));
        FilterUtils.applySelectFilter(builder, "p", EppEduPlanAdditionalProf.programTrait(), settings.get("programTrait"));
        FilterUtils.applySelectFilter(builder, "p", EppEduPlanAdditionalProf.developGrid(), settings.get("developGrid"));
        FilterUtils.applySelectFilter(builder, "p", EppEduPlanAdditionalProf.state(), settings.get("state"));

        FilterUtils.applySelectFilter(builder, "p", EppEduPlan.owner(), context.get("orgUnit"));

        orderDescription.applyOrderWithLeftJoins(builder, input.getEntityOrder());

        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();

        Map<Long, List<EppEduPlanVersion>> versionMap = SafeMap.get(ArrayList.class);
        for (EppEduPlanVersion version : DataAccessServices.dao().getList(EppEduPlanVersion.class, EppEduPlanVersion.eduPlan().s(), output.getRecordList(), EppEduPlanVersion.number().s())) {
            versionMap.get(version.getEduPlan().getId()).add(version);
        }

        for (DataWrapper wrapper : DataWrapper.wrap(output)) {
            wrapper.setProperty(VIEW_PROP_VERSION, versionMap.get(wrapper.getId()));
        }

        return output;

    }

}

