package ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.ui.SectionPromice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.sec.bo.Sec.logic.IContextSecuredComponentManager;

/**
 * @author vdanilov
 */
@Configuration
public class EppCtrEducationPromiceSectionPromice extends BusinessComponentManager implements IContextSecuredComponentManager {

    @Override
    public void fillPermissionGroup(final PermissionGroupMeta parent, final String securityPostfix) {
        PermissionMetaUtil.createPermission(parent, "add_educationPromice_" + securityPostfix, getMeta().getProperty("bc.sec.add.title"));
        PermissionMetaUtil.createPermission(parent, "edit_educationPromice_" + securityPostfix, getMeta().getProperty("bc.sec.edit.title"));
        PermissionMetaUtil.createPermission(parent, "delete_educationPromice_" + securityPostfix, getMeta().getProperty("bc.sec.delete.title"));
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

}