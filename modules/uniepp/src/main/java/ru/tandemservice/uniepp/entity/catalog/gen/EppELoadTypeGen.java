package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид учебной нагрузки (типы учебной нагрузки)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppELoadTypeGen extends EppLoadType
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppELoadType";
    public static final String ENTITY_NAME = "eppELoadType";
    public static final int VERSION_HASH = -1669990065;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppELoadTypeGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppELoadTypeGen> extends EppLoadType.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppELoadType.class;
        }

        public T newInstance()
        {
            return (T) new EppELoadType();
        }
    }
    private static final Path<EppELoadType> _dslPath = new Path<EppELoadType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppELoadType");
    }
            

    public static class Path<E extends EppELoadType> extends EppLoadType.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EppELoadType.class;
        }

        public String getEntityName()
        {
            return "eppELoadType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
