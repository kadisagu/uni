package ru.tandemservice.uniepp.component.eduplan.row.TermDistribution;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {

    void save(Model model);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void move(Model model, Integer termNumber, int direction);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void fill(Model model);
}
