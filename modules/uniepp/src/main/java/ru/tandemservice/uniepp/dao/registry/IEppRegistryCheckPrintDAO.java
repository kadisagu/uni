/* $Id:$ */
package ru.tandemservice.uniepp.dao.registry;

import jxl.write.WriteException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.Wiki;

import java.io.IOException;
import java.util.Set;

/**
 * @author oleyba
 * @since 5/30/12
 */
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=8945678")
public interface IEppRegistryCheckPrintDAO
{
    SpringBeanCache<IEppRegistryCheckPrintDAO> instance = new SpringBeanCache<>(IEppRegistryCheckPrintDAO.class.getName());

    /**
     * Печатает отчет о читаемых дисциплинах
     * @param orgUnits подразделения, если пусто - то сделает выбор по числу дисциплин, самых репрезентативных
     * @return отчет, .xls
     */
    @Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
    byte[] printReport(Set<Long> orgUnits) throws WriteException, IOException;
}
