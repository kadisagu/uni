package ru.tandemservice.uniepp.entity.student.group;

import org.springframework.util.ClassUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroupRowGen;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;

import java.util.Collections;
import java.util.Date;
import java.util.Map;

/** @see ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroupRowGen */
public abstract class EppRealEduGroupRow extends EppRealEduGroupRowGen
{
    /** @deprecated use getStudentWpePart */
    public EppStudentWpePart getStudent()
    {
        return getStudentWpePart();
    }

    /** @return аналогичная связь, но в другой группе */
    public abstract EppRealEduGroupRow clone(EppRealEduGroup group);

    /** описание классов, реализующих логику УГС */
    public interface IEppRealEduGroupRowDescription
    {
        Class<? extends EppRealEduGroupRow> relationClass();
        Class<? extends EppStudentWpePart> studentClass();
        Class<? extends EppRealEduGroup> groupClass();
        Class<? extends EppGroupType> typeKlass();

        EppRealEduGroupRow buildRelation(EppRealEduGroup group, EppStudentWpePart student);
        EppRealEduGroup buildGroup(EppRealEduGroupSummary summary, EppRegistryElementPart activityPart, EppGroupType type, EppRealEduGroupCompleteLevel level, String title, Date creationDate);
    }

    public static final IEppRealEduGroupRowDescription SIMPLE = new IEppRealEduGroupRowDescription() {
        @Override public Class<? extends EppRealEduGroupRow> relationClass() { return EppRealEduGroupRow.class; }
        @Override public Class<? extends EppStudentWpePart> studentClass() { return EppStudentWpePart.class; }
        @Override public Class<? extends EppRealEduGroup> groupClass() { return EppRealEduGroup.class; }
        @Override public Class<? extends EppGroupType> typeKlass() { return EppGroupType.class; }
        @Override public EppRealEduGroupRow buildRelation(final EppRealEduGroup group, final EppStudentWpePart student) { throw new UnsupportedOperationException(); }
        @Override public EppRealEduGroup buildGroup(final EppRealEduGroupSummary summary, final EppRegistryElementPart activityPart, final EppGroupType type, final EppRealEduGroupCompleteLevel level, final String title, final Date creationDate) { throw new UnsupportedOperationException(); }
    };

    /** { group-type.class -> group.class } */
    @SuppressWarnings("unchecked")
    public static final Map<Class<? extends EppRealEduGroup>, IEppRealEduGroupRowDescription> group2descriptionMap = Collections.unmodifiableMap(SafeMap.get(
            groupKlass -> {
                if (EppRealEduGroup.class.equals(groupKlass)) { return EppRealEduGroupRow.SIMPLE; }
                groupKlass = (Class) ClassUtils.getUserClass(groupKlass);

                try
                {
                    final Class clazz;
                    if (EppRealEduGroup4LoadType.class.equals(groupKlass))
                        clazz = EppRealEduGroup4LoadTypeRow.class;
                    else if (EppRealEduGroup4ActionType.class.equals(groupKlass))
                        clazz = EppRealEduGroup4ActionTypeRow.class;
                    else
                        throw new IllegalStateException("No EppRealEduGroupRow with `group` instanceof `" + groupKlass.getSimpleName() + "`");

                    return (IEppRealEduGroupRowDescription) clazz.getField("RELATION").get(null);
                }
                catch (final Exception t) {
                    throw CoreExceptionUtils.getRuntimeException(t);
                }
            }
    ));
}