package ru.tandemservice.uniepp.entity.plan.data;

import org.apache.commons.collections15.Predicate;
import org.apache.commons.collections15.functors.TruePredicate;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.IEppNumberRow;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvTermDistributedRowGen;

import java.util.Map;

/**
 * Запись версии УП (строка с нагрузкой)
 */
public abstract class EppEpvTermDistributedRow extends EppEpvTermDistributedRowGen implements IEppNumberRow, IEppEpvTermDistributedRow
{
    public static final Predicate<IEppEpvRow> PARENT_PREDICATE = TruePredicate.getInstance();

    @Override public int getComparatorClassIndex() { return 3; }
    @Override public String getComparatorString() { return this.getSelfIndexPart(); }
    @Override public String getSelfIndexPart() { return IEppNumberRow.NUMBER_FORMATTER.format(this); }

    @Override public EppEpvRow getHierarhyParent() { return this.getParent(); }
    @Override public void setHierarhyParent(final EppEpvRow parent) { this.setParent(parent); }

    public static double wrap(final long size) {
        return UniEppUtils.wrap(size < 0 ? 0 : size);
    }

    public static long unwrap(final Double value) {
        final Long size = UniEppUtils.unwrap(value);
        return (null == size ? 0 : size);
    }

    @Deprecated
    public Double getTotalSizeAsDouble() { return getHoursTotalAsDouble(); }
    @Deprecated
    public void setTotalSizeAsDouble(final Double value) { setHoursTotalAsDouble(value); }

    public Double getTotalLaborAsDouble() { return wrap(this.getTotalLabor()); }
    public void setTotalLaborAsDouble(final Double value) { this.setTotalLabor(unwrap(value)); }

    public Double getHoursTotalAsDouble() {
        return wrap(this.getHoursTotal());
    }
    public void setHoursTotalAsDouble(final Double value) {
        this.setHoursTotal(unwrap(value));
    }

    public Double getHoursSelfworkAsDouble() {
        return wrap(this.getHoursSelfwork());
    }
    public void setHoursSelfworkAsDouble(final Double value) {
        this.setHoursSelfwork(unwrap(value));
    }

    public Double getHoursAuditAsDouble() {
        return wrap(this.getHoursAudit());
    }
    public void setHoursAuditAsDouble(final Double value) {
        this.setHoursAudit(unwrap(value));
    }



    @SuppressWarnings("unchecked")
    public void addLoad(Map detailMap)
    {
        detailMap.put(EppLoadType.FULL_CODE_TOTAL_HOURS, getHoursTotalAsDouble());
        detailMap.put(EppLoadType.FULL_CODE_LABOR, getTotalLaborAsDouble());
        detailMap.put(EppELoadType.FULL_CODE_AUDIT, getHoursAuditAsDouble());

        // В базе для самостоятельной работы хранится самостоятельная + контроль, а отображается самостоятельная в интерфейсе без контроля (DEV-6531)
        detailMap.put(EppELoadType.FULL_CODE_SELFWORK, getHoursSelfworkAsDouble());
        detailMap.put(EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL, wrap(getHoursSelfwork() - getHoursControl()));

        detailMap.put(EppLoadType.FULL_CODE_CONTROL, wrap(getHoursControl()));
        detailMap.put(EppLoadType.FULL_CODE_CONTROL_E, wrap(getHoursControlE()));
    }

    public void updateLoad(Map<String, Double> newDetailMap, boolean withSelfworkWoControl)
    {
        setTotalLaborAsDouble(newDetailMap == null ? null : newDetailMap.get(EppLoadType.FULL_CODE_LABOR));
        setHoursTotalAsDouble(newDetailMap == null ? null : newDetailMap.get(EppLoadType.FULL_CODE_TOTAL_HOURS));
        setHoursAudit(unwrap(newDetailMap == null ? null : newDetailMap.get(EppELoadType.FULL_CODE_AUDIT)));

        // В базе для самостоятельной работы хранится самостоятельная + контроль, а отображается самостоятельная в интерфейсе без контроля (DEV-6531)
        // FIXME при переходе на произвольные нагрузки надо избавиться от этого ужаса. Не должно быть никаких нагрузок в нагрузках - все в своих клеточках.
        Double selfworkWithoutControl = newDetailMap == null ? null : newDetailMap.get(EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);
        if (withSelfworkWoControl && selfworkWithoutControl == null && newDetailMap != null) {
            selfworkWithoutControl = 0d; // Хак. Надо как-то отличать, в каком виде к нам приходит самостоятельная работа
        }

        final Double control = newDetailMap == null ? null : newDetailMap.get(EppLoadType.FULL_CODE_CONTROL);
        if (selfworkWithoutControl != null)
            setHoursSelfwork(unwrap(selfworkWithoutControl + (control != null ? control : 0d)));
        else
            setHoursSelfwork(unwrap(newDetailMap == null ? null : newDetailMap.get(EppELoadType.FULL_CODE_SELFWORK)));

        setHoursControl(unwrap(control));
        setHoursControlE(unwrap(newDetailMap == null ? null : newDetailMap.get(EppLoadType.FULL_CODE_CONTROL_E)));
    }
}