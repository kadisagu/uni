/* $Id$ */
package ru.tandemservice.uniepp.reports.extViews;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * student_disciplines_ext_view
 * @author Ekaterina Zvereva
 * @since 06.09.2016
 */
public class EppExtViewProviderStuDisciplines extends SimpleDQLExternalViewConfig
{

    @Override
    protected DQLSelectBuilder buildDqlQuery()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppStudentWorkPlanElement.class, "msrp")
                .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("msrp"), "student")
                .where(eq(property(Student.archival().fromAlias("student")), value(Boolean.FALSE))); // только актуальные

        // данные МСРП
        column(dql, property(EppStudentWorkPlanElement.course().intValue().fromAlias("msrp")), "course_number").comment("номер курса");
        column(dql, property(EppStudentWorkPlanElement.part().code().fromAlias("msrp")), "year_part_code").comment("код части учебного года");
        column(dql, property(EppStudentWorkPlanElement.part().title().fromAlias("msrp")), "year_part_title").comment("название части учебного года");
        column(dql, property(EppStudentWorkPlanElement.part().yearDistribution().code().fromAlias("msrp")), "year_distrib_code").comment("код разбиения учебного года");
        column(dql, property(EppStudentWorkPlanElement.part().yearDistribution().title().fromAlias("msrp")), "year_distrib_title").comment("название разбиения учебного года");
        column(dql, property(EppStudentWorkPlanElement.term().intValue().fromAlias("msrp")), "term_number").comment("номер семестра");
        column(dql, property(EppStudentWorkPlanElement.year().educationYear().title().fromAlias("msrp")), "year_title").comment("название учебного года");
        column(dql, property(EppStudentWorkPlanElement.year().educationYear().code().fromAlias("msrp")), "year_code").comment("код учебного года");
        column(dql, property(EppStudentWorkPlanElement.registryElementPart().registryElement().id().fromAlias("msrp")), "regel_id").comment("id элемента реестра");
        column(dql, property(EppStudentWorkPlanElement.registryElementPart().registryElement().number().fromAlias("msrp")), "regel_number").comment("номер элемента реестра");
        column(dql, property(EppStudentWorkPlanElement.registryElementPart().registryElement().title().fromAlias("msrp")), "regel_title").comment("название элемента реестра");
        column(dql, property(EppStudentWorkPlanElement.registryElementPart().id().fromAlias("msrp")), "regel_part_id").comment("id части элемента реестра");
        column(dql, property(EppStudentWorkPlanElement.registryElementPart().number().fromAlias("msrp")), "regel_part_numer").comment("номер части части элемента реестра");
        column(dql, property(EppStudentWorkPlanElement.removalDate().fromAlias("msrp")), "removalDate").comment("дата утраты актуальности");

        // данные студента
        column(dql, property(Student.id().fromAlias("student")), "student_id").comment("id студента, на которого ссылается МРСП");
        column(dql, property(Student.personalNumber().fromAlias("student")), "student_personal_number").comment("личный номер студента");
        column(dql, property(Student.person().identityCard().firstName().fromAlias("student")), "student_first_name").comment("имя студента");
        column(dql, property(Student.person().identityCard().middleName().fromAlias("student")), "student_middle_name").comment("отчество студента");
        column(dql, property(Student.person().identityCard().lastName().fromAlias("student")), "student_last_name").comment("фамилия студента");
        column(dql, property(Student.person().contactData().email().fromAlias("student")), "student_email").comment("электронный адрес студента");

        // логин
        column(dql, property(Student.principal().login().fromAlias("student")), "student_login").comment("логин принциала студента");
         // группа
         dql.joinPath(DQLJoinType.left, Student.group().fromAlias("student"), "group");
         column(dql, property(Group.id().fromAlias("group")), "student_current_group_id").comment("id группы студента");
         column(dql, property(Group.title().fromAlias("group")), "student_current_group_title").comment("название группы студента");

         // НПП
         dql.joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("student"), "eduOu");
        column(dql, property(EducationOrgUnit.id().fromAlias("eduOu")), "eduOu_id").comment("uid направления подготовки подразделения");

        column(dql, property(EducationOrgUnit.educationLevelHighSchool().title().fromAlias("eduOu")), "eduOu_title").comment("название направления подготовки");
        column(dql, property(EducationOrgUnit.educationLevelHighSchool().fullTitle().fromAlias("eduOu")), "eduOu_fullTitle").comment("полное название направления подготовки");
        column(dql, property(EducationOrgUnit.educationLevelHighSchool().shortTitle().fromAlias("eduOu")), "eduOu_shortTitle").comment("сокращенное название направления подготовки");

        column(dql, property(EducationOrgUnit.formativeOrgUnit().id().fromAlias("eduOu")), "formOu_id").comment("uid формирующего подразделения");
        column(dql, property(EducationOrgUnit.formativeOrgUnit().title().fromAlias("eduOu")), "formOu_title").comment("название формирующего подразделения");
        column(dql, property(EducationOrgUnit.formativeOrgUnit().shortTitle().fromAlias("eduOu")), "formOu_shortTitle").comment("сокр. название формирующего подразделения");
        column(dql, property(EducationOrgUnit.territorialOrgUnit().id().fromAlias("eduOu")), "terrOu_id").comment("uid территориального подразделения");
        column(dql, property(EducationOrgUnit.territorialOrgUnit().title().fromAlias("eduOu")), "terrOu_title").comment("название территориального подразделения");
        column(dql, property(EducationOrgUnit.territorialOrgUnit().shortTitle().fromAlias("eduOu")), "terrOu_shortTitle").comment("сокр. название территориального подразделения");

        column(dql, property(EducationOrgUnit.developForm().title().fromAlias("eduOu")), "developForm").comment("форма обучения");
        column(dql, property(EducationOrgUnit.developCondition().title().fromAlias("eduOu")), "developCondition").comment("условия обучения");
        column(dql, property(EducationOrgUnit.developTech().title().fromAlias("eduOu")), "developTech").comment("технология освоения");
        column(dql, property(EducationOrgUnit.developPeriod().title().fromAlias("eduOu")), "developPeriodPlan").comment("срок освоения");
        column(dql, property(Student.developPeriodAuto().title().fromAlias("student")), "developPeriodStudent").comment("срок освоения (студент)");

        return dql;
    }
}