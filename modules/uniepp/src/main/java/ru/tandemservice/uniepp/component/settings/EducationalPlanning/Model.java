package ru.tandemservice.uniepp.component.settings.EducationalPlanning;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

/**
 * 
 * @author nkokorina
 * @since 26.02.2010
 */

public class Model
{
    private DynamicListDataSource<IEntity> _dataSource;

    public void setDataSource(final DynamicListDataSource<IEntity> _dataSource)
    {
        this._dataSource = _dataSource;
    }

    public DynamicListDataSource<IEntity> getDataSource()
    {
        return this._dataSource;
    }


}
