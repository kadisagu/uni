package ru.tandemservice.uniepp.component.edugroup.AttachTutors;

import org.hibernate.Session;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow.IEppRealEduGroupRowDescription;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        if (model.getIds().isEmpty()) { throw new IllegalArgumentException("model.ids is empty"); }

        IEppRealGroupRowDAO.instance.get().checkGroupLockedEdit(model.getIds(), model.getLevel());

        final IEppRealEduGroupRowDescription relation = this.getDescription(model);

        List<Long> orgUnitIds = new ArrayList<>();
        final DQLSelectBuilder ownerByGroups = new DQLSelectBuilder();
        ownerByGroups.fromEntity(relation.groupClass(), "x").column(property(EppRealEduGroup.activityPart().registryElement().owner().id().fromAlias("x")));
        ownerByGroups.where(in(property(EppRealEduGroup.id().fromAlias("x")), model.getIds()));
        List<Long> ownerByGroupsResult = ownerByGroups.createStatement(getSession()).list();

        final DQLSelectBuilder ownerByGroupStudents = new DQLSelectBuilder();
        ownerByGroupStudents.fromEntity(relation.relationClass(), "x").column(property(EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart().registryElement().owner().id().fromAlias("x")));
        ownerByGroupStudents.where(in(property(EppRealEduGroupRow.group().id().fromAlias("x")), model.getIds()));
        List<Long> ownerByGroupStudentsResult = ownerByGroups.createStatement(getSession()).list();

        orgUnitIds.addAll(ownerByGroupsResult);
        orgUnitIds.addAll(ownerByGroupStudentsResult);

        PpsEntrySelectBlockData ppsBlockData = new PpsEntrySelectBlockData(orgUnitIds).setMultiSelect(true).setInitiallySelectedPps(CommonBaseUtil.<PpsEntry>getPropertiesList(getList(EppPpsCollectionItem.class, EppPpsCollectionItem.list().id(), model.getIds()), EppPpsCollectionItem.pps()));
        model.setPpsData(ppsBlockData);
    }

    @SuppressWarnings("unchecked")
    protected IEppRealEduGroupRowDescription getDescription(final Model model)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppRealEduGroup.class, "grp").where(in(property("grp", "id"), model.getIds()));
        dql.column(property("grp", EppRealEduGroup.clazz()));
        dql.predicate(DQLPredicateType.distinct);
        final List<Number> rows = dql.createStatement(this.getSession()).list();
        if (rows.isEmpty()) { throw new IllegalArgumentException("No such groups"); }
        if (rows.size() > 1) {  throw new IllegalArgumentException("Multiple types"); }

        return EppRealEduGroupRow.group2descriptionMap.get(EntityRuntime.getMeta(rows.iterator().next().shortValue()).getEntityClass());
    }

    @Override
    public void save(final Model model)
    {
        IEppRealGroupRowDAO.instance.get().checkGroupLockedEdit(model.getIds(), model.getLevel());

        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppPpsCollectionItem.class, "rel").column(property("rel"));
        dql.where(in(property(EppPpsCollectionItem.list().id().fromAlias("rel")), model.getIds()));

        final Session session = this.getSession();
        final List<EppPpsCollectionItem> dbItems = dql.createStatement(session).list();
        final Map<Long, Map<Long, EppPpsCollectionItem>> map = new HashMap<>(model.getIds().size());
        for (final EppPpsCollectionItem item: dbItems)
        {
            final EppPpsCollectionItem dup = SafeMap.safeGet(map, item.getList().getId(), HashMap.class).put(item.getPps().getId(), item);
            if (null != dup) { session.delete(dup); }
        }


        final List<PpsEntry> value = model.getPpsData().getSelectedPpsList();
        for (final Long id: model.getIds())
        {
            final EppRealEduGroup owner = this.get(id);
            final Map<Long, EppPpsCollectionItem> m = SafeMap.safeGet(map, id, HashMap.class);

            for (final PpsEntry pps: (null == value ? Collections.<PpsEntry>emptyList() : value)) {
                if (null == m.remove(pps.getId())) {
                    session.save(new EppPpsCollectionItem(owner, pps));
                }
            }

            for (final EppPpsCollectionItem i: m.values()) {
                session.delete(i);
            }
        }

    }

}
