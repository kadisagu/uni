package ru.tandemservice.uniepp.entity.pupnag;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uni.entity.education.IDevelopCombination;
import ru.tandemservice.uni.util.DevelopUtil;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphGen;

/**
 * ГУП
 */
public class EppWorkGraph extends EppWorkGraphGen implements IEppStateObject, ITitled, IDevelopCombination
{
    public EppWorkGraph() {

    }

    public EppWorkGraph(final EppYearEducationProcess year, final IDevelopCombination developCombination) {
        this.setYear(year);
        this.setDevelopForm(developCombination.getDevelopForm());
        this.setDevelopCondition(developCombination.getDevelopCondition());
        this.setDevelopTech(developCombination.getDevelopTech());
        this.setDevelopGrid(developCombination.getDevelopGrid());
    }

    private String _title_cache;

    @EntityDSLSupport
    @Override
    public String getTitle()
    {
        if (null != this._title_cache) { return this._title_cache; }
        if (getDevelopForm() == null) {
            return this.getClass().getSimpleName();
        }
        return (this._title_cache = DevelopUtil.getTitle(this));
    }
}