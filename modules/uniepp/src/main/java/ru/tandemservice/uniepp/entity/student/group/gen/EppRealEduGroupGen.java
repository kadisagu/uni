package ru.tandemservice.uniepp.entity.student.group.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.pps.IEppPPSCollectionOwner;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебная группа студентов (УГС)
 *
 * Объединяет группу студентов по виду аудиторной нагрузки или форме итогового контроля
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRealEduGroupGen extends EntityBase
 implements IEppPPSCollectionOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup";
    public static final String ENTITY_NAME = "eppRealEduGroup";
    public static final int VERSION_HASH = -929418974;
    private static IEntityMeta ENTITY_META;

    public static final String L_SUMMARY = "summary";
    public static final String L_ACTIVITY_PART = "activityPart";
    public static final String L_TYPE = "type";
    public static final String P_TITLE = "title";
    public static final String P_CREATION_DATE = "creationDate";
    public static final String P_MODIFICATION_DATE = "modificationDate";
    public static final String L_LEVEL = "level";
    public static final String L_WORK_PLAN_ROW = "workPlanRow";
    public static final String P_ACTIVITY_TITLE = "activityTitle";
    public static final String P_FULL_TITLE = "fullTitle";

    private EppRealEduGroupSummary _summary;     // Сводка
    private EppRegistryElementPart _activityPart;     // Запись реестра (с указанием части)
    private EppGroupType _type;     // Вид УГС
    private String _title;     // Название
    private Date _creationDate;     // Дата создания объекта
    private Date _modificationDate;     // Дата изменения
    private EppRealEduGroupCompleteLevel _level;     // Степень готовности
    private EppWorkPlanRegistryElementRow _workPlanRow;     // Запись РУП, на основании которой будет проводиться обучение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сводка. Свойство не может быть null.
     */
    @NotNull
    public EppRealEduGroupSummary getSummary()
    {
        return _summary;
    }

    /**
     * @param summary Сводка. Свойство не может быть null.
     */
    public void setSummary(EppRealEduGroupSummary summary)
    {
        dirty(_summary, summary);
        _summary = summary;
    }

    /**
     * @return Запись реестра (с указанием части). Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getActivityPart()
    {
        return _activityPart;
    }

    /**
     * @param activityPart Запись реестра (с указанием части). Свойство не может быть null.
     */
    public void setActivityPart(EppRegistryElementPart activityPart)
    {
        dirty(_activityPart, activityPart);
        _activityPart = activityPart;
    }

    /**
     * @return Вид УГС. Свойство не может быть null.
     */
    @NotNull
    public EppGroupType getType()
    {
        return _type;
    }

    /**
     * @param type Вид УГС. Свойство не может быть null.
     */
    public void setType(EppGroupType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата создания объекта. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getModificationDate()
    {
        return _modificationDate;
    }

    /**
     * @param modificationDate Дата изменения. Свойство не может быть null.
     */
    public void setModificationDate(Date modificationDate)
    {
        dirty(_modificationDate, modificationDate);
        _modificationDate = modificationDate;
    }

    /**
     * @return Степень готовности.
     */
    public EppRealEduGroupCompleteLevel getLevel()
    {
        return _level;
    }

    /**
     * @param level Степень готовности.
     */
    public void setLevel(EppRealEduGroupCompleteLevel level)
    {
        dirty(_level, level);
        _level = level;
    }

    /**
     * @return Запись РУП, на основании которой будет проводиться обучение.
     */
    public EppWorkPlanRegistryElementRow getWorkPlanRow()
    {
        return _workPlanRow;
    }

    /**
     * @param workPlanRow Запись РУП, на основании которой будет проводиться обучение.
     */
    public void setWorkPlanRow(EppWorkPlanRegistryElementRow workPlanRow)
    {
        dirty(_workPlanRow, workPlanRow);
        _workPlanRow = workPlanRow;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRealEduGroupGen)
        {
            setSummary(((EppRealEduGroup)another).getSummary());
            setActivityPart(((EppRealEduGroup)another).getActivityPart());
            setType(((EppRealEduGroup)another).getType());
            setTitle(((EppRealEduGroup)another).getTitle());
            setCreationDate(((EppRealEduGroup)another).getCreationDate());
            setModificationDate(((EppRealEduGroup)another).getModificationDate());
            setLevel(((EppRealEduGroup)another).getLevel());
            setWorkPlanRow(((EppRealEduGroup)another).getWorkPlanRow());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRealEduGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRealEduGroup.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppRealEduGroup is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "summary":
                    return obj.getSummary();
                case "activityPart":
                    return obj.getActivityPart();
                case "type":
                    return obj.getType();
                case "title":
                    return obj.getTitle();
                case "creationDate":
                    return obj.getCreationDate();
                case "modificationDate":
                    return obj.getModificationDate();
                case "level":
                    return obj.getLevel();
                case "workPlanRow":
                    return obj.getWorkPlanRow();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "summary":
                    obj.setSummary((EppRealEduGroupSummary) value);
                    return;
                case "activityPart":
                    obj.setActivityPart((EppRegistryElementPart) value);
                    return;
                case "type":
                    obj.setType((EppGroupType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "modificationDate":
                    obj.setModificationDate((Date) value);
                    return;
                case "level":
                    obj.setLevel((EppRealEduGroupCompleteLevel) value);
                    return;
                case "workPlanRow":
                    obj.setWorkPlanRow((EppWorkPlanRegistryElementRow) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "summary":
                        return true;
                case "activityPart":
                        return true;
                case "type":
                        return true;
                case "title":
                        return true;
                case "creationDate":
                        return true;
                case "modificationDate":
                        return true;
                case "level":
                        return true;
                case "workPlanRow":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "summary":
                    return true;
                case "activityPart":
                    return true;
                case "type":
                    return true;
                case "title":
                    return true;
                case "creationDate":
                    return true;
                case "modificationDate":
                    return true;
                case "level":
                    return true;
                case "workPlanRow":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "summary":
                    return EppRealEduGroupSummary.class;
                case "activityPart":
                    return EppRegistryElementPart.class;
                case "type":
                    return EppGroupType.class;
                case "title":
                    return String.class;
                case "creationDate":
                    return Date.class;
                case "modificationDate":
                    return Date.class;
                case "level":
                    return EppRealEduGroupCompleteLevel.class;
                case "workPlanRow":
                    return EppWorkPlanRegistryElementRow.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRealEduGroup> _dslPath = new Path<EppRealEduGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRealEduGroup");
    }
            

    /**
     * @return Сводка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getSummary()
     */
    public static EppRealEduGroupSummary.Path<EppRealEduGroupSummary> summary()
    {
        return _dslPath.summary();
    }

    /**
     * @return Запись реестра (с указанием части). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getActivityPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> activityPart()
    {
        return _dslPath.activityPart();
    }

    /**
     * @return Вид УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getType()
     */
    public static EppGroupType.Path<EppGroupType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getModificationDate()
     */
    public static PropertyPath<Date> modificationDate()
    {
        return _dslPath.modificationDate();
    }

    /**
     * @return Степень готовности.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getLevel()
     */
    public static EppRealEduGroupCompleteLevel.Path<EppRealEduGroupCompleteLevel> level()
    {
        return _dslPath.level();
    }

    /**
     * @return Запись РУП, на основании которой будет проводиться обучение.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getWorkPlanRow()
     */
    public static EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow> workPlanRow()
    {
        return _dslPath.workPlanRow();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getActivityTitle()
     */
    public static SupportedPropertyPath<String> activityTitle()
    {
        return _dslPath.activityTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getFullTitle()
     */
    public static SupportedPropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    public static class Path<E extends EppRealEduGroup> extends EntityPath<E>
    {
        private EppRealEduGroupSummary.Path<EppRealEduGroupSummary> _summary;
        private EppRegistryElementPart.Path<EppRegistryElementPart> _activityPart;
        private EppGroupType.Path<EppGroupType> _type;
        private PropertyPath<String> _title;
        private PropertyPath<Date> _creationDate;
        private PropertyPath<Date> _modificationDate;
        private EppRealEduGroupCompleteLevel.Path<EppRealEduGroupCompleteLevel> _level;
        private EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow> _workPlanRow;
        private SupportedPropertyPath<String> _activityTitle;
        private SupportedPropertyPath<String> _fullTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сводка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getSummary()
     */
        public EppRealEduGroupSummary.Path<EppRealEduGroupSummary> summary()
        {
            if(_summary == null )
                _summary = new EppRealEduGroupSummary.Path<EppRealEduGroupSummary>(L_SUMMARY, this);
            return _summary;
        }

    /**
     * @return Запись реестра (с указанием части). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getActivityPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> activityPart()
        {
            if(_activityPart == null )
                _activityPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_ACTIVITY_PART, this);
            return _activityPart;
        }

    /**
     * @return Вид УГС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getType()
     */
        public EppGroupType.Path<EppGroupType> type()
        {
            if(_type == null )
                _type = new EppGroupType.Path<EppGroupType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppRealEduGroupGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Дата создания объекта. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(EppRealEduGroupGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getModificationDate()
     */
        public PropertyPath<Date> modificationDate()
        {
            if(_modificationDate == null )
                _modificationDate = new PropertyPath<Date>(EppRealEduGroupGen.P_MODIFICATION_DATE, this);
            return _modificationDate;
        }

    /**
     * @return Степень готовности.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getLevel()
     */
        public EppRealEduGroupCompleteLevel.Path<EppRealEduGroupCompleteLevel> level()
        {
            if(_level == null )
                _level = new EppRealEduGroupCompleteLevel.Path<EppRealEduGroupCompleteLevel>(L_LEVEL, this);
            return _level;
        }

    /**
     * @return Запись РУП, на основании которой будет проводиться обучение.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getWorkPlanRow()
     */
        public EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow> workPlanRow()
        {
            if(_workPlanRow == null )
                _workPlanRow = new EppWorkPlanRegistryElementRow.Path<EppWorkPlanRegistryElementRow>(L_WORK_PLAN_ROW, this);
            return _workPlanRow;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getActivityTitle()
     */
        public SupportedPropertyPath<String> activityTitle()
        {
            if(_activityTitle == null )
                _activityTitle = new SupportedPropertyPath<String>(EppRealEduGroupGen.P_ACTIVITY_TITLE, this);
            return _activityTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup#getFullTitle()
     */
        public SupportedPropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new SupportedPropertyPath<String>(EppRealEduGroupGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

        public Class getEntityClass()
        {
            return EppRealEduGroup.class;
        }

        public String getEntityName()
        {
            return "eppRealEduGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getActivityTitle();

    public abstract String getFullTitle();
}
