/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphWeeksTab;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniepp.ui.WorkGraphEduProgramSubjectListModel;
import ru.tandemservice.uniepp.util.WeekTypeLegendRow;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "workGraph.id")
})
public class Model implements WorkGraphEduProgramSubjectListModel.CoursesProvider
{
    private EppWorkGraph _workGraph = new EppWorkGraph(); // гуп
    private IDataSettings _settings;                      // сеттинги
    private ISelectModel _programSubjectModel;
    private EppYearEducationWeek[] _weekData;             // массив из 52-х учебных недель в гуп
    private List<Course> _courseList;                     // список курсов обучения в гуп, согласно учебной сетке
    private RangeSelectionWeekTypeListDataSource<EppWorkGraphRow> _graphDataSource;  // таблица гуп
    private Set<Long> _filtedIds;                                            // мн-во версий УП в строке ГУП, которые удовлетворяют выбранным значениями фильтра НП(с)
    private List<WeekTypeLegendRow> _weekTypeLegendList;                     // строки легенды
    private WeekTypeLegendRow _weekTypeLegendItem;                           // текущее значение цикла
    private Map<EppWorkGraphRow, Collection<EppWorkGraphRow2EduPlan>> _row2epvs; // row -> упорядоченный список версий уп в группе

    // Filter Methods

    public Course getCourse()
    {
        Course course = (Course) this._settings.get("course" + this._workGraph.getId());
        if ((course == null) && !this._courseList.isEmpty()) {
            this.setCourse(course = this._courseList.get(0));
        }
        return course;
    }

    @Override
    public Collection<Course> getCourses()
    {
        final Course course = this.getCourse();
        return course == null ? null : Collections.singletonList(course);
    }

    public void setCourse(final Course course)
    {
        if ((this._graphDataSource != null) && (this._graphDataSource.getEditId() != null)) {
            return;
        }
        this._settings.set("course" + this._workGraph.getId(), course);
    }

    public EduProgramSubject getProgramSubject()
    {
        return (EduProgramSubject) this._settings.get("programSubject" + this._workGraph.getId());
    }

    public void setProgramSubject(final EduProgramSubject programSubject)
    {
        this._settings.set("programSubject" + this._workGraph.getId(), programSubject);
    }

    // Getters & Setters

    public EppWorkGraph getWorkGraph()
    {
        return this._workGraph;
    }

    public void setWorkGraph(final EppWorkGraph workGraph)
    {
        this._workGraph = workGraph;
    }

    public IDataSettings getSettings()
    {
        return this._settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this._settings = settings;
    }

    public ISelectModel getProgramSubjectModel()
    {
        return _programSubjectModel;
    }

    public void setProgramSubjectModel(ISelectModel programSubjectModel)
    {
        _programSubjectModel = programSubjectModel;
    }

    public EppYearEducationWeek[] getWeekData()
    {
        return this._weekData;
    }

    public void setWeekData(final EppYearEducationWeek[] weekData)
    {
        this._weekData = weekData;
    }


    public List<Course> getCourseList()
    {
        return this._courseList;
    }

    public void setCourseList(final List<Course> courseList)
    {
        this._courseList = courseList;
    }

    public RangeSelectionWeekTypeListDataSource<EppWorkGraphRow> getGraphDataSource()
    {
        return this._graphDataSource;
    }

    public void setGraphDataSource(final RangeSelectionWeekTypeListDataSource<EppWorkGraphRow> graphDataSource)
    {
        this._graphDataSource = graphDataSource;
    }

    public Set<Long> getFiltedIds()
    {
        return this._filtedIds;
    }

    public void setFiltedIds(final Set<Long> filtedIds)
    {
        this._filtedIds = filtedIds;
    }

    public List<WeekTypeLegendRow> getWeekTypeLegendList()
    {
        return this._weekTypeLegendList;
    }

    public void setWeekTypeLegendList(final List<WeekTypeLegendRow> weekTypeLegendList)
    {
        this._weekTypeLegendList = weekTypeLegendList;
    }

    public WeekTypeLegendRow getWeekTypeLegendItem()
    {
        return this._weekTypeLegendItem;
    }

    public void setWeekTypeLegendItem(final WeekTypeLegendRow weekTypeLegendItem)
    {
        this._weekTypeLegendItem = weekTypeLegendItem;
    }

    public Map<EppWorkGraphRow, Collection<EppWorkGraphRow2EduPlan>> getRow2epvs()
    {
        return this._row2epvs;
    }

    public void setRow2epvs(final Map<EppWorkGraphRow, Collection<EppWorkGraphRow2EduPlan>> row2epvs)
    {
        this._row2epvs = row2epvs;
    }
}

