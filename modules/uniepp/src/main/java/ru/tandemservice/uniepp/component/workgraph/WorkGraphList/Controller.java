/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniepp.IUnieppComponents;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphGen;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        model.setSettings(component.getSettings());

        this.getDao().prepare(this.getModel(component));

        this.prepareListDataSource(component);
    }

    private void prepareListDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        final DynamicListDataSource<EppWorkGraph> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareListDataSource(model);
        });

        dataSource.addColumn(IndicatorColumn.createIconColumn("eduprocsched", "График учебного процесса"));

        dataSource.addColumn(new SimpleColumn("Название", EppWorkGraphGen.P_TITLE).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", EppWorkGraphGen.state().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить график учебного процесса «{0}»?", EppWorkGraphGen.P_TITLE).setPermissionKey("delete_eppWorkGraph_list"));

        model.setDataSource(dataSource);
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        component.saveSettings();
        this.getModel(component).getDataSource().refresh();
    }

    public void onClickClear(final IBusinessComponent component)
    {
        Model model = getModel(component);
        IDataSettings settings = model.getSettings();
        EppYearEducationProcess yearEducationProcess = settings.get("yearEducationProcess");
        settings.clear();
        settings.set("yearEducationProcess", yearEducationProcess);
        this.onClickSearch(component);
    }

    public void onClickAdd(final IBusinessComponent component)
    {
        final EppYearEducationProcess yearEducationProcess = this.getModel(component).getYearEducationProcess();
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(IUnieppComponents.WORK_GRAPH_ADD, new ParametersMap().add("selectedYear", (null == yearEducationProcess ? null : yearEducationProcess.getId()))));
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        this.getDao().deleteRow(component);
    }
}