/* $Id$ */
package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.SQLException;
import java.util.Map;

/**
 * @author Andrey Andreev
 * @since 20.02.2016
 */
public class MS_uniepp_2x9x2_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.2")
        };
    }

    @Override
    public void run(final DBTool tool) throws Exception {

        final Map<String, Long> map = MigrationUtils.getCatalogCode2IdMap(tool, "epp_c_skill_group_t");

        if (!map.keySet().contains("22")) {
            return;
        }

        if (map.keySet().contains("22") && !map.keySet().contains("24")) {
            tool.executeUpdate("update epp_c_skill_group_t set code_p=? where code_p=?", "24", "22");
            return;
        }

        for (String[] x : new String[][] {{"22", "04"}, {"23", "09"}}) {
            Long fromId = map.get(x[0]);
            Long toId = map.get(x[1]);

            moveLinks(tool, "epp_stateedustd_skill_t", "skillgroup_id", fromId, toId);
            moveLinks(tool, "epp_reg_skill_t", "skillgroup_id", fromId, toId);
            moveLinks(tool, "fefucompetence_t", "eppskillgroup_id", fromId, toId);
            moveLinks(tool, "usmacompetence_t", "eppskillgroup_id", fromId, toId);
        }

        tool.executeUpdate("delete from epp_c_skill_group_t where code_p in (?, ?)", "22", "23");
    }

    private void moveLinks(DBTool tool, String linkedTable, String linkedColumn, Long oldId, Long newId) throws SQLException {
        if (tool.columnExists(linkedTable, linkedColumn)) {
            tool.executeUpdate("update " + linkedTable + " set " + linkedColumn + "=? where " + linkedColumn + "=?", newId, oldId);
        }
    }
}