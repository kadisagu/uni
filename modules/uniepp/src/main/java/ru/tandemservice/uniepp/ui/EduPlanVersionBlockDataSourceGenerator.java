package ru.tandemservice.uniepp.ui;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.IRowCustomizer;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.CheckboxColumn;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.OptionBaseColumn;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.TreeColumn;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uni.ui.formatters.TermsFormatter;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvTotalRow;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public abstract class EduPlanVersionBlockDataSourceGenerator extends LoadDataSourceGeneratorBase
{

//    public static final String COLOR_GREEN = "#c5f4bd"; // # 7435 #
//    public static final String COLOR_CYAN = "#aadce1";  // # 7435 #
//    public static final String COLOR_BLUE = "#d8dee8";  // # 7435 #
//    public static final String COLOR_RED = "#fab5b0";   // # 7435 #

    public static final String COLUMN_NAME__HEAD_MAIN_FIELDS = "mainFields";
    public static final String COLUMN_NAME__HEAD_COURSES = "courses";

    public static final String TITLE__TITLE = "Название";
    public static final String TITLE__TYPE = "Тип";
    public static final String TITLE__REGISTRY_NUMBER = "№ (реестр)";

    private static final String TITLE__INDEX = "Индекс";
    private static final String TITLE__PARTS = "Частей";

    public static final String TITLE__HEAD__CONTROL_ACTIONS = "Формы контроля";

    public static final String TITLE__HEAD__LOAD = "Нагрузка";
    public static final String TITLE__HEAD__A_LOAD = "Аудиторных";

    private static final String TITLE__HEAD__TERM_LOAD = "Нагрузка по семестрам";

    public static final String TITLE__LOAD_TOTAL_SIZE = "Часов (всего)";
    public static final String TITLE__LOAD_TOTAL_LABOR = "Труд-сть (всего)";
    public static final String TITLE__LOAD_TOTAL_WEEKS = "Недель (всего)";

    private static final String TITLE__LOAD_LABOR = "Труд-сть";

    private static final String TITLE__ACTION__EDIT_ROW = "Редактировать запись";
    private static final String TITLE__ACTION__EDIT_LOAD = "Редактировать нагрузку";

    public static final String TITLE__OWNER = "Читающее\nподразделение";

    protected static final String LISTENER__EDIT_ROW = "onClickEditRow";

    protected static final String LISTENER__EDIT_ROW_ACTIONS = "onClickEditRowActions";

    protected static final String LISTENER__EDIT_ROW_LOADS = "onClickEditRowLoads";

    protected static final String LISTENER__EDIT_ROW_TOTAL_TERM_LOADS = "onClickEditRowTotalTermLoads";

    @SuppressWarnings("deprecation")
    protected static final TermsFormatter TERMS_FORMATTER = new TermsFormatter();


    public interface IBlockRenderContext {
        Collection<IEppEpvRowWrapper> getBlockRows(IEppEpvBlockWrapper versionWrapper);
        IEntityHandler getAdditionalEditDisabler();
    }

    @Override protected List<EppControlActionType> getPossibleControlActions() {
        return IEppEduPlanVersionDataDAO.instance.get().getActiveControlActionTypes(null).collect(Collectors.toList());
    }

    abstract protected boolean isCheckMode();
    final boolean isCheckMode = this.isCheckMode();

    abstract protected boolean isExtendedView();
    final boolean isExtendedView = this.isExtendedView() || this.isCheckMode;

    // load version
    protected abstract Long getVersionBlockId();
    private final EppEduPlanVersionBlock block = UniDaoFacade.getCoreDao().getNotNull(EppEduPlanVersionBlock.class, this.getVersionBlockId());
    private final EppGeneration generation = this.block.getEduPlanVersion().getEduPlan().getGeneration();
    private final boolean showTotalLabor = this.generation.showTotalLabor();

    // load grid
    private final Long gridId = this.block.getEduPlanVersion().getEduPlan().getDevelopGrid().getId();
    private final Map<Integer, DevelopGridTerm> gridMap = IDevelopGridDAO.instance.get().getDevelopGridMap(this.gridId);

    // then, load version wrapper
    private final IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(this.block.getId(), true);
    public IEppEpvBlockWrapper getVersionBlockWrapper() { return blockWrapper; }

    // проверяет, существует ли самостоятельная работа в указаных строках
    protected boolean isSelfWorkPresent(final Collection<IEppEpvRowWrapper> blockRows) {
        final String fullCode = this.eLoadSelfWorkType.getFullCode();
        for (final IEppEpvRowWrapper row: blockRows) {
            if (row.getTotalLoad(null, fullCode) > 0) {
                // если есть хоть в одном семестре (вычисленная > 0)
                return true;
            }
        }
        return false;
    }

    // возвращяет набор оболочек элементов реестра для проверки
    @SuppressWarnings("unused")
    protected Map<Long, IEppRegElWrapper> getRegistryElementsWrappers(final Collection<IEppEpvRowWrapper> blockRows) {

        // пока не делаем такую проверку (она взорвет им мозг)
        if (true) { return null; }

        if (!this.isCheckMode) { return null; }

        final Set<Long> regElIds = new HashSet<>(blockRows.size());
        for (final IEppEpvRowWrapper wrapper: blockRows) {
            if (wrapper.getRow() instanceof EppEpvRegistryRow) {
                final EppRegistryElement registryElement = ((EppEpvRegistryRow)wrapper.getRow()).getRegistryElement();
                if (null != registryElement) { regElIds.add(registryElement.getId()); }
            }
        }

        return IEppRegistryDAO.instance.get().getRegistryElementDataMap(regElIds);
    }

    /** ВЫБОР, НОМЕР И ОСНОВНОЙ БЛОК */
    protected void doPrepare_mainBlockColumns(final StaticListDataSource<IEppEpvRowWrapper> dataSource, final IEntityHandler editDisabler, final String mianBlockTitle) {

        // выбор
        if (this.editable) { dataSource.addColumn(this.getColumn_chechbox(null)); }

        // номер
        final SimpleColumn column_number = this.getColumn_number();
        if (null != column_number) { dataSource.addColumn(wrap(column_number).setWidth(1)); }

        // состояние
        if (this.editable) { dataSource.addColumn(UniEppUtils.getStateColumn("row."+EppEpvRegistryRow.L_REGISTRY_ELEMENT)); }

        // название, редактирование
        final HeadColumn mHeadColumn = new HeadColumn(EduPlanVersionBlockDataSourceGenerator.COLUMN_NAME__HEAD_MAIN_FIELDS, mianBlockTitle);
        mHeadColumn.addColumn(wrap(new SimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__INDEX, "index") {
            @Override public String getContent(final IEntity entity) {
                return ((IEppEpvRowWrapper)entity).getIndex();
            }
        }).setWidth(1));
        mHeadColumn.addColumn(wrapTitleColumn(wrap(new TreeColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__TITLE, "title") {
            @Override public String getContent(final IEntity entity) {
                return ((IEppEpvRowWrapper)entity).getDisplayableTitle();
            }
        })));
        mHeadColumn.addColumn(vwrap(new SimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__TYPE, "rowType") {
            @Override public String getContent(final IEntity entity) {
                return ((IEppEpvRowWrapper)entity).getRowType();
            }
        }).setWidth(1));

        mHeadColumn.addColumn(wrapRegistryNumberColumn(vwrap(getColumn_registryNumber()).setWidth(1)));
        mHeadColumn.addColumn(vwrap(getColumn_parts()).setWidth(1));

        if (this.editable) {
            mHeadColumn.addColumn(this.getColumn_action(editDisabler, EduPlanVersionBlockDataSourceGenerator.TITLE__ACTION__EDIT_ROW, ActionColumn.EDIT, EduPlanVersionBlockDataSourceGenerator.LISTENER__EDIT_ROW));
        }
        dataSource.addColumn(wrap(mHeadColumn).setWidth(1));
    }

    /** Еще одна колонка с названием дисциплины */
    protected void doPrepare_rowTitleColumn(final StaticListDataSource<IEppEpvRowWrapper> dataSource) {
        dataSource.addColumn(new SimpleColumn("Строка", "rowTitleEnd") {
            @Override public String getContent(final IEntity entity) {
                IEppEpvRowWrapper rowWrapper = (IEppEpvRowWrapper) entity;
                return rowWrapper.getIndex() + " " + rowWrapper.getDisplayableTitle();
            }
        });
    }

    /** ФОРМЫ КОНТРОЛЯ */
    protected void doPrepare_controlActionsColumns(final StaticListDataSource<IEppEpvRowWrapper> dataSource, final List<EppControlActionType> controlActionTypes, final IEntityHandler editDisabler) {
        final HeadColumn caHeadColumn = new HeadColumn("controlActions", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__CONTROL_ACTIONS);
        for (final EppControlActionType tp : controlActionTypes) {
            caHeadColumn.addColumn(vwrap(this.getColumn_totalRowUsage_byActionType(tp)));
        }
        //if (this.editable) {
        //    caHeadColumn.addColumn(this.getColumn_action(editDisabler, EduPlanVersionBlockDataSourceGenerator.TITLE__ACTION__EDIT_ACTIONS, ActionColumn.EDIT, EduPlanVersionBlockDataSourceGenerator.LISTENER__EDIT_ROW_ACTIONS));
        //}
        dataSource.addColumn(wrap(caHeadColumn).setWidth(1));
    }


    /** формирует dataSource для отображения списка дисциплин */
    public void doPrepareBlockDisciplineDataSource(final StaticListDataSource<IEppEpvRowWrapper> dataSource, final IBlockRenderContext context) {

        final IEntityHandler additionalEditDisabler = (null == context.getAdditionalEditDisabler() ? LoadDataSourceGeneratorBase.NO_EDIT_DISABLER : context.getAdditionalEditDisabler());

        final IEntityHandler globalEditDisabler = entity -> {
            final IEppEpvRowWrapper row = (IEppEpvRowWrapper) entity;
            return row.getRow() instanceof EppEpvStructureRow || additionalEditDisabler.handle(entity);
        };

        final IEntityHandler detailEditDisabler = entity -> {
            final IEppEpvRowWrapper row = (IEppEpvRowWrapper) entity;

            // редактировать можно только для detail-row
            return !(row.getRow() instanceof EppEpvTermDistributedRow) || globalEditDisabler.handle(entity);
        };

        final IRowCustomizer<IEppEpvRowWrapper> rowCustomizer = wrapRowCustomizer(buildRowCustomizer4Discipline(additionalEditDisabler));

        // сначала убиваем все колонки
        dataSource.getColumns().clear();
        dataSource.setRowCustomizer(rowCustomizer);

        final Collection<IEppEpvRowWrapper> blockRows = context.getBlockRows(this.blockWrapper);
        dataSource.setupRows(blockRows);

        final boolean isSelfWorkPresent = this.isSelfWorkPresent(blockRows);
        final Map<Long, IEppRegElWrapper> registryElementMap = this.getRegistryElementsWrappers(blockRows);

        this.doPrepare_mainBlockColumns(dataSource, globalEditDisabler, "Блоки, модули, дисциплины");
        this.doPrepare_controlActionsColumns(dataSource, this.disciplineCATypes, detailEditDisabler);

        /** ИТОГОВАЯ НАГРУЗКА ИЗ РЕЕСТРА */ {
            final HeadColumn loadXHeadColumn = new HeadColumn("xload", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__LOAD);
            if (this.showTotalLabor) { loadXHeadColumn.addColumn(vwrap(this.getColumn_totalRowLoad_totalLabor())); }
            loadXHeadColumn.addColumn(vwrap(this.getColumn_totalRowLoad_totalSize()));

            final HeadColumn loadAHeadColumn = new HeadColumn("aload", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__A_LOAD);
            loadAHeadColumn.addColumn(vwrap(this.getColumn_totalRowLoad_byEppLoadType(this.eLoadTotalAuditType)));
            for (final EppALoadType aLoadType : this.aLoadTypes) {
                loadAHeadColumn.addColumn(vwrap(this.getColumn_totalRowLoad_byEppLoadType(aLoadType)));
            }
            loadXHeadColumn.addColumn(wrap(loadAHeadColumn).setWidth(1));

            for (final EppELoadType eLoadType : this.eLoadTypes) {
                if (this.eLoadTotalAuditType != eLoadType /* same object */) {
                    loadXHeadColumn.addColumn(vwrap(this.getColumn_totalRowLoad_byEppLoadType(eLoadType)));
                }
            }

            dataSource.addColumn(wrap(loadXHeadColumn).setWidth(1));
        }


        /** ТЕОРЕТИЧЕСКАЯ НАГРУЗКА ПО КУРСАМ И СЕМЕСТРАМ */ {
            final boolean multipleLoadColumns = isSelfWorkPresent || this.isExtendedView || this.showTotalLabor;
            final HeadColumn coursesHeadColumn = new HeadColumn(
                EduPlanVersionBlockDataSourceGenerator.COLUMN_NAME__HEAD_COURSES,
                EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__TERM_LOAD
            );

            HeadColumn courseHeadColumn = null;
            for (final Entry<Integer, DevelopGridTerm> entry : this.gridMap.entrySet()) {
                final String courseKey = "course." + entry.getValue().getCourseNumber();
                if ((null == courseHeadColumn) || !courseKey.equals(courseHeadColumn.getName())) {
                    coursesHeadColumn.addColumn(wrap(courseHeadColumn = new HeadColumn(courseKey, String.valueOf(entry.getValue().getCourseNumber()) + " курс")).setWidth(1));
                }

                final int term = entry.getKey();
                final String termKey = "term."+term;

                //                if (multipleLoadColumns) {
                //                    // отделяем текущйи семестр от предыдущих
                //                    courseHeadColumn.addColumn(wrap(this.getColumn_separator(termKey)).setWidth(1));
                //                }

                // если колонок несколько (расш. форма или есть самост. нагрузка) то выводим еще и число недель
                final int termSize = this.blockWrapper.getTermSize(IEppEpvBlockWrapper.ELOAD_FULL_CODE_ALL, term);
                final HeadColumn termHeadColumn = new HeadColumn(termKey, String.valueOf(term) + (multipleLoadColumns ? ("\n("+termSize+")") : ""));
                courseHeadColumn.addColumn(wrap(termHeadColumn).setWidth(1));

                if (this.showTotalLabor) {
                    // сначала всегда выводим трудоемкость
                    termHeadColumn.addColumn(vwrap(wrapTermLoadColumn(entry.getValue(), this.getColumn_termRowLoad_totalTermLabor(term))).setWidth(1));
                }

                if (this.isExtendedView) {
                    // если расширенная форма - то разбиваем аудиторную нагрузку
                    for (final EppALoadType aLoadType : this.aLoadTypes) {
                        termHeadColumn.addColumn(vwrap(wrapTermLoadColumn(entry.getValue(), this.getColumn_termRowLoad_byEppALoadType(term, aLoadType, registryElementMap))).setWidth(1));
                    }
                } else {
                    // если форма простая - то выводим общуюаудиторную нагрузку
                    if (multipleLoadColumns) {
                        // если самостоятельная (или иная нагрузка) нагрузка есть - то выводим название (вертикально), а число недель не выводим (оно уже вверху)
                        termHeadColumn.addColumn(vwrap(wrapTermLoadColumn(entry.getValue(), this.getColumn_termRowLoad_totalAuditLoad(this.eLoadTotalAuditType.getShortTitle(), term))).setWidth(1));
                    } else {
                        // если самостоятельной нагрузки нет - то это одна колонка (отображение простое) - выводим только число недель (вверху оно не выводилось)
                        termHeadColumn.addColumn(wrap(wrapTermLoadColumn(entry.getValue(), this.getColumn_termRowLoad_totalAuditLoad("("+termSize+")", term))).setWidth(1));
                    }
                }

                if (isSelfWorkPresent) {
                    termHeadColumn.addColumn(wrapTermLoadColumn(entry.getValue(), vwrap(this.getColumn_termRowLoad_selfWorkLoad(term))).setWidth(1));
                }
            }

            if (this.editable) {
                coursesHeadColumn.addColumn(this.getColumn_action(detailEditDisabler, EduPlanVersionBlockDataSourceGenerator.TITLE__ACTION__EDIT_LOAD, ActionColumn.EDIT, EduPlanVersionBlockDataSourceGenerator.LISTENER__EDIT_ROW_LOADS));
            }
            dataSource.addColumn(wrap(coursesHeadColumn).setWidth(1));
        }

        this.doPrepare_rowTitleColumn(dataSource);
    }

    // кастомайзер для дисциплин
    protected IRowCustomizer<IEppEpvRowWrapper> buildRowCustomizer4Discipline(final IEntityHandler additionalEditDisabler) {
        return new SimpleRowCustomizer<IEppEpvRowWrapper>() {
            @Override public String getRowStyle(final IEppEpvRowWrapper entity) {
                final StringBuilder sb = new StringBuilder();
                if (additionalEditDisabler.handle(entity)) { sb.append("color: gray;"); }
                if (entity.getRow() instanceof EppEpvHierarchyRow) { sb.append("font-weight:bold;"); }

                final boolean skipLoad = !Boolean.TRUE.equals(entity.getUsedInLoad());
                final boolean skipActions = !Boolean.TRUE.equals(entity.getUsedInActions());
                if (skipActions || skipLoad) {
                    sb.append("background-color: #e8e8e8");
                }
                return sb.toString();
            }
        };
    }


    /** формирует dataSource для отображения списка мероприятий */
    public void doPrepareBlockActionsDataSource(final StaticListDataSource<IEppEpvRowWrapper> dataSource, final IBlockRenderContext context) {

        final IEntityHandler additionalEditDisabler = (null == context.getAdditionalEditDisabler() ? LoadDataSourceGeneratorBase.NO_EDIT_DISABLER : context.getAdditionalEditDisabler());

        final IEntityHandler globalEditDisabler = entity -> {
            final IEppEpvRowWrapper row = (IEppEpvRowWrapper) entity;
            return row.getRow() instanceof EppEpvStructureRow || additionalEditDisabler.handle(entity);
        };

        final IEntityHandler detailEditDisabler = globalEditDisabler::handle;

        final IRowCustomizer<IEppEpvRowWrapper> rowCustomizer = wrapRowCustomizer(buildRowCustomizer4Actions(additionalEditDisabler));

        // сначала убиваем все колонки
        dataSource.getColumns().clear();
        dataSource.setRowCustomizer(rowCustomizer);
        dataSource.setupRows(context.getBlockRows(this.blockWrapper));

        this.doPrepare_mainBlockColumns(dataSource, globalEditDisabler, "Блоки, модули, мероприятия");
        this.doPrepare_controlActionsColumns(dataSource, this.actionsCATypes, detailEditDisabler);

        /** ИТОГОВАЯ НАГРУЗКА ИЗ РЕЕСТРА */ {
            final HeadColumn loadXHeadColumn = new HeadColumn("xload", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__LOAD);
            if (this.showTotalLabor) { loadXHeadColumn.addColumn(vwrap(this.getColumn_totalRowLoad_totalLabor())); }
            loadXHeadColumn.addColumn(vwrap(this.getColumn_totalRowLoad_totalSize()));
            // loadXHeadColumn.addColumn(vwrap(this.getColumn_totalRowLoad_totalWeeks()));
            dataSource.addColumn(wrap(loadXHeadColumn).setWidth(1));
        }

        /** ИТОГОВАЯ НАГРУЗКА ПО КУРСАМ И СЕМЕСТРАМ */ {
            //            final boolean multipleLoadColumns = this.showTotalSize || this.showTotalLabor;
            final HeadColumn coursesHeadColumn = new HeadColumn(
                EduPlanVersionBlockDataSourceGenerator.COLUMN_NAME__HEAD_COURSES,
                "Нагрузка по семестрам"
            );
            HeadColumn courseHeadColumn = null;
            for (final Entry<Integer, DevelopGridTerm> entry : this.gridMap.entrySet()) {
                final String courseKey = "course." + entry.getValue().getCourseNumber();
                if ((null == courseHeadColumn) || !courseKey.equals(courseHeadColumn.getName())) {
                    coursesHeadColumn.addColumn(wrap(courseHeadColumn = new HeadColumn(courseKey, String.valueOf(entry.getValue().getCourseNumber()) + " курс")).setWidth(1));
                }

                final int term = entry.getKey();
                final String termKey = "term."+term;

                //                if (multipleLoadColumns) {
                //                    // отделяем текущйи семестр от предыдущих
                //                    courseHeadColumn.addColumn(wrap(this.getColumn_separator(termKey)).setWidth(1));
                //                }

                final HeadColumn termHeadColumn = new HeadColumn(termKey, String.valueOf(term));
                courseHeadColumn.addColumn(wrap(termHeadColumn).setWidth(1));

                if (this.showTotalLabor) { termHeadColumn.addColumn(vwrap(wrapTermLoadColumn(entry.getValue(), this.getColumn_termRowLoad_totalTermLabor(term))).setWidth(1)); }
                termHeadColumn.addColumn(vwrap(wrapTermLoadColumn(entry.getValue(), this.getColumn_termRowLoad_totalTermSize(term))).setWidth(1));
                termHeadColumn.addColumn(vwrap(wrapTermLoadColumn(entry.getValue(), this.getColumn_termRowLoad_totalTermWeeks(term))).setWidth(1));

            }

            if (this.editable) {
                coursesHeadColumn.addColumn(this.getColumn_action(detailEditDisabler, EduPlanVersionBlockDataSourceGenerator.TITLE__ACTION__EDIT_LOAD, ActionColumn.EDIT, EduPlanVersionBlockDataSourceGenerator.LISTENER__EDIT_ROW_TOTAL_TERM_LOADS));
            }
            dataSource.addColumn(wrap(coursesHeadColumn).setWidth(1));
        }

        this.doPrepare_rowTitleColumn(dataSource);
    }

    // кастомайзер для практик
    protected IRowCustomizer<IEppEpvRowWrapper> buildRowCustomizer4Actions(final IEntityHandler additionalEditDisabler) {
        return new SimpleRowCustomizer<IEppEpvRowWrapper>() {
            @Override public String getRowStyle(final IEppEpvRowWrapper entity) {
                final StringBuilder sb = new StringBuilder();
                if (additionalEditDisabler.handle(entity)) { sb.append("color: gray;"); }
                if (entity.getRow() instanceof EppEpvHierarchyRow) { sb.append("font-weight:bold;"); }
                return sb.toString();
            }
        };
    }


    /** формирует dataSource для отображения списка дисциплин */
    public void doPrepareTotalDataSource(final StaticListDataSource<EppEpvTotalRow> dataSource, final IBlockRenderContext context) {

        // сначала убиваем все колонки
        dataSource.getColumns().clear();
        dataSource.setRowCustomizer(buildRowCustomizer4TotalRows());

        final Collection<IEppEpvRowWrapper> sourceRows = context.getBlockRows(this.blockWrapper);
        final boolean isSelfWorkPresent = this.isSelfWorkPresent(sourceRows);

        dataSource.setupRows(IEppEduPlanVersionDataDAO.instance.get().getTotalRows(this.blockWrapper, sourceRows));

        /** ОСНОВНОЙ БЛОК */ {
            dataSource.addColumn(wrap(new SimpleColumn("Название характеристики", "title")));
        }

        /** ФОРМЫ КОНТРОЛЯ */ {
            final HeadColumn caHeadColumn = new HeadColumn("controlActions", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__CONTROL_ACTIONS);
            for (final EppControlActionType tp : this.disciplineCATypes) {
                final String actionFullCode = tp.getFullCode();
                caHeadColumn.addColumn(vwrap(new SimpleColumn(tp.getShortTitle(), actionFullCode) {
                    @Override public String getContent(final IEntity entity) {
                        return ((EppEpvTotalRow)entity).getControlActionValue(actionFullCode);
                    }
                }));
            }
            dataSource.addColumn(wrap(caHeadColumn).setWidth(1));
        }

        /** ИТОГОВАЯ НАГРУЗКА ИЗ РЕЕСТРА */ {
            final HeadColumn loadXHeadColumn = new HeadColumn("xload", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__LOAD);
            if (this.showTotalLabor) { loadXHeadColumn.addColumn(vwrap(this.getColumn_totalRows_load(EduPlanVersionBlockDataSourceGenerator.TITLE__LOAD_TOTAL_LABOR, EppLoadType.FULL_CODE_LABOR, 0))); }
            loadXHeadColumn.addColumn(vwrap(this.getColumn_totalRows_load(EduPlanVersionBlockDataSourceGenerator.TITLE__LOAD_TOTAL_SIZE, EppLoadType.FULL_CODE_TOTAL_HOURS, 0)));

            final HeadColumn loadAHeadColumn = new HeadColumn("aload", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__A_LOAD);
            loadAHeadColumn.addColumn(vwrap(this.getColumn_totalRows_load(this.eLoadTotalAuditType, 0)));
            for (final EppALoadType aLoadType : this.aLoadTypes) {
                loadAHeadColumn.addColumn(vwrap(this.getColumn_totalRows_load(aLoadType, 0)));
            }
            loadXHeadColumn.addColumn(wrap(loadAHeadColumn).setWidth(1));

            for (final EppELoadType eLoadType : this.eLoadTypes) {
                if (this.eLoadTotalAuditType != eLoadType /* same object */) {
                    loadAHeadColumn.addColumn(vwrap(this.getColumn_totalRows_load(eLoadType, 0)));
                }
            }
            dataSource.addColumn(wrap(loadXHeadColumn).setWidth(1));
        }


        /** ТЕОРЕТИЧЕСКАЯ НАГРУЗКА ПО КУРСАМ И СЕМЕСТРАМ */ {
            //            final boolean multipleLoadColumns = isSelfWorkPresent || this.isExtendedView || this.showTotalLabor;
            final HeadColumn coursesHeadColumn = new HeadColumn(
                EduPlanVersionBlockDataSourceGenerator.COLUMN_NAME__HEAD_COURSES,
                EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__TERM_LOAD + " (в семестр)"
            );

            HeadColumn courseHeadColumn = null;
            for (final Entry<Integer, DevelopGridTerm> entry : this.gridMap.entrySet()) {
                final String courseKey = "course." + entry.getValue().getCourseNumber();
                if ((null == courseHeadColumn) || !courseKey.equals(courseHeadColumn.getName())) {
                    coursesHeadColumn.addColumn(wrap(courseHeadColumn = new HeadColumn(courseKey, String.valueOf(entry.getValue().getCourseNumber()) + " курс")).setWidth(1));
                }

                final int term = entry.getKey();
                final String termKey = "term."+term;

                //                if (multipleLoadColumns) {
                //                    // отделяем текущйи семестр от предыдущих
                //                    courseHeadColumn.addColumn(wrap(this.getColumn_separator(termKey)).setWidth(1));
                //                }

                final int termSize = this.blockWrapper.getTermSize(IEppEpvBlockWrapper.ELOAD_FULL_CODE_ALL, term);
                final HeadColumn termHeadColumn = new HeadColumn(termKey, String.valueOf(term) + "\n("+termSize+")");
                courseHeadColumn.addColumn(wrap(termHeadColumn).setWidth(1));

                if (this.showTotalLabor) {
                    // выводим трудоемкость
                    termHeadColumn.addColumn(vwrap(this.getColumn_totalRows_load(EduPlanVersionBlockDataSourceGenerator.TITLE__LOAD_LABOR, EppLoadType.FULL_CODE_LABOR, term)));
                }

                if (this.isExtendedView) {
                    // если расширенная форма - то разбиваем аудиторную нагрузку
                    for (final EppALoadType aLoadType : this.aLoadTypes) {
                        termHeadColumn.addColumn(vwrap(this.getColumn_totalRows_load(aLoadType, term)).setWidth(1));
                    }
                } else {
                    // если форма простая - то выводим общуюаудиторную нагрузку
                    termHeadColumn.addColumn(vwrap(this.getColumn_totalRows_load(this.eLoadTotalAuditType, term)).setWidth(1));
                }

                if (isSelfWorkPresent) {
                    termHeadColumn.addColumn(vwrap(this.getColumn_totalRows_load(this.eLoadSelfWorkType, term)).setWidth(1));
                }

            }
            dataSource.addColumn(wrap(coursesHeadColumn).setWidth(1));
        }
    }

    // кастомайзер для итоговых строк
    protected IRowCustomizer<EppEpvTotalRow> buildRowCustomizer4TotalRows() {
        return new SimpleRowCustomizer<EppEpvTotalRow>() {
            @Override public String getRowStyle(final EppEpvTotalRow row) { return "font-weight:bold;"; }
        };
    }


    // обертка для колонки с названием
    protected AbstractColumn wrapTitleColumn(final AbstractColumn column) {
        return column;
    }

    // обертка для колонки с номером элемента реестра
    protected AbstractColumn wrapRegistryNumberColumn(final AbstractColumn column) {
        return column;
    }

    // обертка для колонок по семестрам (для дисциплин, практик и ГИА), но не для итоговой таблицы
    protected AbstractColumn wrapTermLoadColumn(final DevelopGridTerm term, final AbstractColumn column) {
        return column;
    }

    // номер в реестре (для переопределения, если потребуется)
    protected PublisherLinkColumn getColumn_registryNumber() {
        return getStaticColumn_registryNumber();
    }

    // номер в реестре (статика)
    public static PublisherLinkColumn getStaticColumn_registryNumber() {
        final PublisherLinkColumn column = new PublisherLinkColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__REGISTRY_NUMBER, "registryNumber") {
            @Override public List<IEntity> getEntityList(final IEntity entity) {
                final IEppEpvRowWrapper wrapper = (IEppEpvRowWrapper)entity;
                if (wrapper.getRow() instanceof EppEpvRegistryRow) {
                    final EppRegistryElement registryElement = ((EppEpvRegistryRow)wrapper.getRow()).getRegistryElement();
                    if (null != registryElement) {
                        return Collections.<IEntity>singletonList(registryElement);
                    }
                }
                return Collections.emptyList();
            }
            @Override public String getContent(final IEntity entity) {
                return this.getFormatter().format(((EppRegistryElement)entity).getNumberWithAbbreviation());
            }
        };
        column.setFormatter(UniEppUtils.NOBR_COMMA_FORMATTER);
        return column;
    }

    // число частей
    protected SimpleColumn getColumn_parts() {
        return new SimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__PARTS, "parts") {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper wrapper = (IEppEpvRowWrapper)entity;
                if (wrapper.getRow() instanceof EppEpvRegistryRow) {
                    final EppEpvRegistryRow row = (EppEpvRegistryRow)wrapper.getRow();
                    final int rowSize = Math.max(0, wrapper.getActiveTermSet().size());

                    final EppRegistryElement registryElement = row.getRegistryElement();
                    if (null != registryElement) {
                        final int regSize = Math.max(1, registryElement.getParts());
                        if (rowSize != regSize) {
                            return EduPlanVersionBlockDataSourceGenerator.error(String.valueOf(regSize) /*что выбрано*/, String.valueOf(rowSize) /*как должно быть*/);
                        }
                    }
                    return String.valueOf(rowSize);
                }
                return "";
            }
            @Override public boolean isRawContent() {
                return true;
            }
        };
    }

    // семестры по типу мероприятий
    protected SimpleColumn getColumn_totalRowUsage_byActionType(final EppControlActionType tp) {
        final String actionFullCode = tp.getFullCode();
        return new SimpleColumn(tp.getShortTitle(), actionFullCode) {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;
                if (row.isTermDataOwner()) {
                    return EduPlanVersionBlockDataSourceGenerator.TERMS_FORMATTER.format(row.getActionTermSet(actionFullCode));
                }
                return "";
            }
        };
    }

    // всего часов
    protected SimpleColumn getColumn_totalRowLoad_totalSize() {
        return new SimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__LOAD_TOTAL_SIZE, EppLoadType.FULL_CODE_TOTAL_HOURS) {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;
                return UniEppUtils.formatLoad(row.getTotalLoad(0, EppLoadType.FULL_CODE_TOTAL_HOURS), false);
            }
        };
    }

    // всего трудоемкость
    protected SimpleColumn getColumn_totalRowLoad_totalLabor() {
        return new SimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__LOAD_TOTAL_LABOR, EppLoadType.FULL_CODE_LABOR) {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;
                return UniEppUtils.formatLoad(row.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR), false);
            }
        };
    }

    //    // всего (недель из реестра)
    //    protected SimpleColumn getColumn_totalRowLoad_totalWeeks() {
    //        return new SimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__LOAD_TOTAL_WEEKS, EppLoadType.TYPE_TOTAL_WEEKS_FULL_CODE) {
    //            @Override public String getContent(final IEntity entity) {
    //                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;
    //                return UniEppUtils.formatLoad(row.getTotalLoad(0, EppLoadType.TYPE_TOTAL_WEEKS_FULL_CODE), false);
    //            }
    //        };
    //    }

    // всего часов по типу теоретической нагрузки
    protected SimpleColumn getColumn_totalRowLoad_byEppLoadType(final EppLoadType loadType) {
        final String loadFullCode = loadType.getFullCode();
        return new SimpleColumn(loadType.getShortTitle(), loadFullCode) {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;

                final double registryValue = row.getTotalLoad(0, loadFullCode);
                final String registryValueString = UniEppUtils.formatLoad(registryValue, false);

                if (row.isTermDataOwner()) {
                    /* если элемент содержит данные по нагрузке. придется считать соответствие */
                    final double calculatedValue = row.getTotalLoad(null, loadFullCode);
                    if ((calculatedValue <= 0) || UniEppUtils.eq(registryValue, calculatedValue)) {
                        // TODO: FIXME: писать ошибку, если не распределено
                        // если не распределено или совпало - то выводим то, что есть
                        return registryValueString;
                    }
                    final String calculatedValueString = UniEppUtils.formatLoad(calculatedValue, false);
                    return EduPlanVersionBlockDataSourceGenerator.error(calculatedValueString /*по УП-как сумма по строке*/, registryValueString /*из реестра*/);
                }

                return registryValueString;
            }
            @Override  public boolean isRawContent() {
                return true;
            }
        };
    }

    // всего (часов в семестр)
    protected SimpleColumn getColumn_termRowLoad_totalTermSize(final int term) {
        return new SimpleColumn("Часов", "term."+term+"."+ EppLoadType.FULL_CODE_TOTAL_HOURS) {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;
                return UniEppUtils.formatLoad(row.getTotalLoad(term, EppLoadType.FULL_CODE_TOTAL_HOURS), true);
            }
        };
    }

    // всего (трудоемкость в семестр)
    protected SimpleColumn getColumn_termRowLoad_totalTermLabor(final int term) {
        return new SimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__LOAD_LABOR, "term."+term+"."+ EppLoadType.FULL_CODE_LABOR) {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;
                return UniEppUtils.formatLoad(row.getTotalLoad(term, EppLoadType.FULL_CODE_LABOR), true);
            }
        };
    }

    // всего (недель в семестр)
    protected SimpleColumn getColumn_termRowLoad_totalTermWeeks(final int term) {
        return new SimpleColumn("Недель", "term."+term+"."+ EppLoadType.FULL_CODE_WEEKS) {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;
                return UniEppUtils.formatLoad(row.getTotalLoad(term, EppLoadType.FULL_CODE_WEEKS), true);
            }
        };
    }


    // распределение теоретической нагрузки по семестрам - по видам аудиторной нагрузки (расширенный режим)
    protected SimpleColumn getColumn_termRowLoad_byEppALoadType(final int term, final EppALoadType loadType, final Map<Long, IEppRegElWrapper> registryElementMap) {
        final String loadFullCode = loadType.getFullCode();
        //final int termSize = this.blockWrapper.getTermSize(EppLoadTypeUtils.getELoadTypeFullCode(loadFullCode), term);

        return new SimpleColumn(loadType.getShortTitle(), "term."+term+"."+loadFullCode) {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;
                if (row.isTermDataOwner()) {
                    /* если элемент содержит данные по нагрузке. придется считать */

                    final double databaseValue = row.getTotalLoad(term, loadFullCode);
                    String databaseValueString = UniEppUtils.formatLoad(databaseValue, true);

                    if (EduPlanVersionBlockDataSourceGenerator.this.isCheckMode)
                    {
                        if ((null != registryElementMap) && (row.getRow() instanceof EppEpvRegistryRow))
                        {
                            final EppRegistryElement regel = ((EppEpvRegistryRow)row.getRow()).getRegistryElement();
                            if (null != regel)
                            {
                                final IEppRegElWrapper w = registryElementMap.get(regel.getId());
                                if ((null != w) && row.hasInTermActivity(term))
                                {
                                    final int activeTermNumber = row.getActiveTermNumber(term);
                                    if (activeTermNumber > regel.getSize())
                                    {
                                        // если части нет... это ахтунг
                                        if (EppALoadType.FULL_CODE_TOTAL_LECTURES.equals(loadFullCode)) {
                                            // ошибку пишем только для лекций
                                            databaseValueString += " (нет части "+activeTermNumber+")";
                                        }
                                    }
                                    else
                                    {
                                        final IEppRegElPartWrapper pw = w.getPartMap().get(activeTermNumber);
                                        if (null != pw) {
                                            // если часть есть
                                            final double registryInTermValue = w.getLoadAsDouble(loadFullCode);
                                            final double databaseInTermValue = row.getTotalInTermLoad(term, loadFullCode);
                                            final double diff = registryInTermValue - databaseInTermValue;
                                            if (!UniEppUtils.eq(diff, 0d))
                                            {
                                                final String registryValueString = UniEppUtils.formatLoad(registryInTermValue, true);

                                                if (UniEppUtils.eq(databaseValue, databaseInTermValue)) {
                                                    // если в часах в семестр показываем - то так и показываем
                                                    databaseValueString = EduPlanVersionBlockDataSourceGenerator.error(databaseValueString, registryValueString);
                                                } else {
                                                    // придется показывать два значения - то, что на странице, и то, что в часах в семестр
                                                    final String databaseInTermValueString = UniEppUtils.formatLoad(databaseInTermValue, true);
                                                    databaseValueString = EduPlanVersionBlockDataSourceGenerator.error(databaseInTermValueString+" ["+databaseValueString+"]", registryValueString);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (EppALoadType.FULL_CODE_TOTAL_LECTURES.equals(loadFullCode))
                        {
                            // делаем проверку только для лекций
                            final double calculatedTotalAuditValue = row.getTotalLoad(term, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
                            final double diff = (row.getTotalLoad(term, EppELoadType.FULL_CODE_AUDIT) - calculatedTotalAuditValue);
                            if (!UniEppUtils.eq(diff, 0d)) {
                                final String calculatedValueString = UniEppUtils.formatLoad(databaseValue + diff, true);
                                return EduPlanVersionBlockDataSourceGenerator.error(databaseValueString, calculatedValueString);
                            }
                        }
                    }

                    return databaseValueString;
                }
                return "";
            }
            @Override  public boolean isRawContent() {
                return EduPlanVersionBlockDataSourceGenerator.this.isCheckMode;
            }
        };
    }

    // распределение нагрузки по семестрам (простой режим отображения - отображается аудиторная назгузка)
    protected SimpleColumn getColumn_termRowLoad_totalAuditLoad(final String title, final int term) {
        //final int termSize = this.blockWrapper.getTermSize(EppELoadType.FULL_CODE_AUDIT, term);

        return new SimpleColumn(title, "term."+term+"."+ EppELoadType.FULL_CODE_AUDIT) {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;
                if (row.isTermDataOwner()) {
                    /* если элемент содержит данные по нагрузке. придется считать  */

                    final double cellValue = row.getTotalLoad(term, EppELoadType.FULL_CODE_AUDIT);
                    if (!EduPlanVersionBlockDataSourceGenerator.this.isCheckMode) {
                        // если нет проверок - выводим то, что в ячейке
                        return UniEppUtils.formatLoad(cellValue, true);
                    }

                    final String cellValueString = UniEppUtils.formatLoad(cellValue, true);
                    final double calculatedValue = row.getTotalLoad(term, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
                    if ((calculatedValue <= 0) || UniEppUtils.eq(cellValue, calculatedValue)) {
                        // если совпало или не распределено - то выводим то, что есть
                        return cellValueString;
                    }

                    final String calculatedValueString = UniEppUtils.formatLoad(calculatedValue, true);
                    return EduPlanVersionBlockDataSourceGenerator.error(cellValueString /* то, что в УП */, calculatedValueString /* это тоже в УП, но вычисленное значение */);
                }
                return "";
            }
            @Override  public boolean isRawContent() {
                return EduPlanVersionBlockDataSourceGenerator.this.isCheckMode;
            }
        };
    }

    // распределение самостоятельной нагрузки по семестрам
    protected SimpleColumn getColumn_termRowLoad_selfWorkLoad(final int term) {
        //final int termSize = this.blockWrapper.getTermSize(EppELoadType.FULL_CODE_SELFWORK, term);

        return new SimpleColumn(this.eLoadSelfWorkType.getShortTitle(), "term."+term+"."+ EppELoadType.FULL_CODE_SELFWORK) {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;
                if (row.isTermDataOwner()) {
                    /* если элемент содержит данные по нагрузке. придется считать  */

                    final double databaseValue = row.getTotalLoad(term, EppELoadType.FULL_CODE_SELFWORK);
                    return UniEppUtils.formatLoad(databaseValue, true);
                }
                return "";
            }
        };
    }

    // выбор
    protected AbstractColumn getColumn_chechbox(final IEntityHandler selectDisabler) {
        final OptionBaseColumn checkboxColumn = new CheckboxColumn("select", "");
        if (null != selectDisabler) { checkboxColumn.setDisableHandler(selectDisabler); }
        checkboxColumn.setScriptResolver((tableName, columnNumber, rowEntity, entity, rowIndex) -> "eduPlanVersionCheckChildren(event, this, '" + tableName + "', " + columnNumber + ", '" + rowEntity.getId() + "');");
        return checkboxColumn.setRequired(true).setWidth(1);
    }

    // номер
    protected SimpleColumn getColumn_number() {
        return new SimpleColumn("№", "num") {
            private final Map<Long, Integer> index = new HashMap<>();
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper)entity;
                if (row.getRow() instanceof EppEpvHierarchyRow) { return ""; }

                Integer idx = this.index.get(entity.getId());
                if (null == idx) { this.index.put(entity.getId(), (idx = 1+this.index.size())); }
                return idx.toString();
            }
        };
    }

    // итоговые строки - нагрузка
    protected SimpleColumn getColumn_totalRows_load(final String title, final String fullCode, final int term) {
        return new SimpleColumn(title, fullCode) {
            @Override public String getContent(final IEntity entity) {
                return ((EppEpvTotalRow)entity).getLoadValue(term, fullCode);
            }
        };
    }

    // итоговые строки - нагрузка
    protected SimpleColumn getColumn_totalRows_load(final EppLoadType loadType, final int term) {
        return this.getColumn_totalRows_load(loadType.getShortTitle(), loadType.getFullCode(), term);
    }

    // читающее подразделение
    protected SimpleColumn getColumn_owner(final boolean fullTitle) {
        return new SimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__OWNER, "owner") {
            @Override public String getContent(final IEntity entity) {
                final IEppEpvRow row = ((IEppEpvRowWrapper)entity).getRow();
                if (row instanceof EppEpvRegistryRow) {
                    final OrgUnit owner = ((EppEpvRegistryRow)row).getRegistryElementOwner();
                    if (owner instanceof TopOrgUnit) { return ""; } // не показывать title для академии - это все равно фэйк
                    if (fullTitle) {
                        return EduPlanVersionBlockDataSourceGenerator.this.getOrgUnitFullTitle(owner);
                    } else {
                        return EduPlanVersionBlockDataSourceGenerator.this.getOrgUnitShortTitle(owner);
                    }
                }
                return "";
            }
        };
    }
}
