package ru.tandemservice.uniepp.component.edustd.EduStdImport;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (!UserContext.getInstance().getErrorCollector().hasErrors())
        {
            this.getDao().update(model);
            this.deactivate(component);
        }
    }
}
