// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.settings.DefaultEduplanVersionList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.IUnieppComponents;

import java.util.List;

/**
 * @author oleyba
 * @since 04.12.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.setSettings(component.getSettings());
        this.getDao().prepare(model);
        this.prepareListDateSource(component);
    }

    private void prepareListDateSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDataSource() != null) {
            return;
        }

        final DynamicListDataSource<EducationOrgUnit> dataSource = UniBaseUtils.createDataSource(component, this.getDao());
        dataSource.addColumn(new CheckboxColumn().setSelectCaption("Выбор").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("ФП", EducationOrgUnit.formativeOrgUnit().shortTitle()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ТП", EducationOrgUnit.territorialOrgUnit().territorialShortTitle()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ВП", EducationOrgUnit.educationLevelHighSchool().orgUnit().shortTitle()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", EducationOrgUnit.educationLevelHighSchool().displayableTitle()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Квалификация", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", EducationOrgUnit.developForm().title()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", EducationOrgUnit.developCondition().title()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Технология освоения", EducationOrgUnit.developTech().title()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", EducationOrgUnit.developPeriod().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(((PublisherLinkColumn) new PublisherLinkColumn("Версия УП", "version.title").setClickable(true).setOrderable(false)).setResolver(new SimplePublisherLinkResolver("version.id")));
        dataSource.addColumn(new SimpleColumn("Число сессий", "developGridTitle").setOrderable(false).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Только для 1 курса", "applyOnlyForFirstCourse").setListener("onClickToggle"));
        dataSource.addColumn(new SimpleColumn("Дата изменения", "changeDate", DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditRow"));

        model.setDataSource(dataSource);
    }

    public void onClickToggle(final IBusinessComponent component)
    {
        this.getDao().doToggleApplyOnlyForFirstCourse(component.getListenerParameter());
    }

    public void onClickActualizeSelected(final IBusinessComponent component)
    {
        final CheckboxColumn checkboxColumn = (CheckboxColumn) this.getModel(component).getDataSource().getColumn("checkbox");
        final List<Long> ids = UniBaseUtils.getIdList(checkboxColumn.getSelectedObjects());
        if (ids.isEmpty()) {
            throw new ApplicationException("Необходимо выбрать хотя бы одну строку.");
        }
        this.getDao().doActualize(ids);
        this.getModel(component).getDataSource().refresh();
        checkboxColumn.reset();
    }

    public void onClickActualizeAll(final IBusinessComponent component)
    {
        final CheckboxColumn checkboxColumn = (CheckboxColumn) this.getModel(component).getDataSource().getColumn("checkbox");
        this.getDao().doActualize(null);
        this.getModel(component).getDataSource().refresh();
        checkboxColumn.reset();
    }

    public void onClickEditRow(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUnieppComponents.DEFAULT_EDUPLAN_VERSION_EDIT, new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())));
    }

    public void onClickSearch(final IBusinessComponent context)
    {
        context.saveSettings();
        this.getModel(context).getDataSource().refresh();
    }

    public void onClickClear(final IBusinessComponent context)
    {
        final Model model = this.getModel(context);
        model.getSettings().clear();
        this.getDao().prepare(model);
        this.onClickSearch(context);
    }
}