package ru.tandemservice.uniepp.component.edugroup.pub.GroupContent;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.component.base.SimpleOrgUnitBasedModel;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;

import java.util.List;
import java.util.Set;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key="parameters", binding="parameters", required=true)
})
public class Model extends SimpleOrgUnitBasedModel {

    private IGroupContentParameters parameters;
    public IGroupContentParameters getParameters() { return this.parameters; }
    public void setParameters(final IGroupContentParameters parameters) { this.parameters = parameters; }

    private Set<OrgUnit> groupOUList;
    public Set<OrgUnit> getGroupOUList() { return groupOUList; }
    public void setGroupOUList(Set<OrgUnit> groupOUList) { this.groupOUList = groupOUList; }

    private OrgUnit currentGroupOU;
    public OrgUnit getCurrentGroupOU() { return currentGroupOU; }
    public void setCurrentGroupOU(OrgUnit currentGroupOU) { this.currentGroupOU = currentGroupOU; }

    private Integer currentGroupOUIndex;
    public Integer getCurrentGroupOUIndex() { return this.currentGroupOUIndex; }
    public void setCurrentGroupOUIndex(Integer currentGroupOUIndex) { this.currentGroupOUIndex = currentGroupOUIndex; }

    public EppRealEduGroup getGroup() { return this.getParameters().getGroup(); }
    public EppRealEduGroupCompleteLevel getLevel() { return this.getParameters().getLevel(); }
    public OrgUnitHolder getOrgUnitHolder() { return this.getParameters().getOrgUnitHolder(); }
    public CommonPostfixPermissionModelBase getSec() { return this.getOrgUnitHolder().getSecModel(); }

    public Long getOrgUnitId() { return this.getOrgUnitHolder().getId(); }
    @Override public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }

    public String getPpsTitle() {
        return IUniBaseDao.instance.get().getCalculatedValue(session -> {
            final List<EppPpsCollectionItem> items = IUniBaseDao.instance.get().getList(
                EppPpsCollectionItem.class,
                EppPpsCollectionItem.L_LIST, Model.this.getGroup().getId(),
                EppPpsCollectionItem.pps().person().identityCard().lastName().s(),
                EppPpsCollectionItem.pps().person().identityCard().firstName().s()
            );
            return StringUtils.join(CollectionUtils.collect(items, input -> {
                return ((EppPpsCollectionItem)input).getPps().getTitle();
            }), ", ");
        });
    }

    private DynamicListDataSource<EppRealEduGroupRow> dataSource;
    public DynamicListDataSource<EppRealEduGroupRow> getDataSource() { return this.dataSource; }
    public void setDataSource(final DynamicListDataSource<EppRealEduGroupRow> dataSource) { this.dataSource = dataSource; }

    private String selectedTab;
    public String getSelectedTab() { return this.selectedTab; }
    public void setSelectedTab(final String selectedTab) { this.selectedTab = selectedTab; }
}
