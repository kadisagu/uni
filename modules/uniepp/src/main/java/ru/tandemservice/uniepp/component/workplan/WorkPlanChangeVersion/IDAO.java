/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.workplan.WorkPlanChangeVersion;

import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author nkokorina
 * Created on: 12.08.2010
 */
public interface IDAO extends IUpdateable<Model>
{
    void prepareRowsData(Model model);
}
