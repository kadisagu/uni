package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.base.entity.IReorganizationObject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Блок направленности в версии УП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanVersionSpecializationBlockGen extends EppEduPlanVersionBlock
 implements IReorganizationObject{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock";
    public static final String ENTITY_NAME = "eppEduPlanVersionSpecializationBlock";
    public static final int VERSION_HASH = -1400418362;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_SPECIALIZATION = "programSpecialization";
    public static final String L_OWNER_ORG_UNIT = "ownerOrgUnit";

    private EduProgramSpecialization _programSpecialization;     // Направленность
    private EduOwnerOrgUnit _ownerOrgUnit;     // Выпускающее подразделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направленность. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSpecialization getProgramSpecialization()
    {
        return _programSpecialization;
    }

    /**
     * @param programSpecialization Направленность. Свойство не может быть null.
     */
    public void setProgramSpecialization(EduProgramSpecialization programSpecialization)
    {
        dirty(_programSpecialization, programSpecialization);
        _programSpecialization = programSpecialization;
    }

    /**
     * @return Выпускающее подразделение. Свойство не может быть null.
     */
    @NotNull
    public EduOwnerOrgUnit getOwnerOrgUnit()
    {
        return _ownerOrgUnit;
    }

    /**
     * @param ownerOrgUnit Выпускающее подразделение. Свойство не может быть null.
     */
    public void setOwnerOrgUnit(EduOwnerOrgUnit ownerOrgUnit)
    {
        dirty(_ownerOrgUnit, ownerOrgUnit);
        _ownerOrgUnit = ownerOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEduPlanVersionSpecializationBlockGen)
        {
            setProgramSpecialization(((EppEduPlanVersionSpecializationBlock)another).getProgramSpecialization());
            setOwnerOrgUnit(((EppEduPlanVersionSpecializationBlock)another).getOwnerOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanVersionSpecializationBlockGen> extends EppEduPlanVersionBlock.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlanVersionSpecializationBlock.class;
        }

        public T newInstance()
        {
            return (T) new EppEduPlanVersionSpecializationBlock();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "programSpecialization":
                    return obj.getProgramSpecialization();
                case "ownerOrgUnit":
                    return obj.getOwnerOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "programSpecialization":
                    obj.setProgramSpecialization((EduProgramSpecialization) value);
                    return;
                case "ownerOrgUnit":
                    obj.setOwnerOrgUnit((EduOwnerOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "programSpecialization":
                        return true;
                case "ownerOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "programSpecialization":
                    return true;
                case "ownerOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "programSpecialization":
                    return EduProgramSpecialization.class;
                case "ownerOrgUnit":
                    return EduOwnerOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlanVersionSpecializationBlock> _dslPath = new Path<EppEduPlanVersionSpecializationBlock>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlanVersionSpecializationBlock");
    }
            

    /**
     * @return Направленность. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock#getProgramSpecialization()
     */
    public static EduProgramSpecialization.Path<EduProgramSpecialization> programSpecialization()
    {
        return _dslPath.programSpecialization();
    }

    /**
     * @return Выпускающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock#getOwnerOrgUnit()
     */
    public static EduOwnerOrgUnit.Path<EduOwnerOrgUnit> ownerOrgUnit()
    {
        return _dslPath.ownerOrgUnit();
    }

    public static class Path<E extends EppEduPlanVersionSpecializationBlock> extends EppEduPlanVersionBlock.Path<E>
    {
        private EduProgramSpecialization.Path<EduProgramSpecialization> _programSpecialization;
        private EduOwnerOrgUnit.Path<EduOwnerOrgUnit> _ownerOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направленность. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock#getProgramSpecialization()
     */
        public EduProgramSpecialization.Path<EduProgramSpecialization> programSpecialization()
        {
            if(_programSpecialization == null )
                _programSpecialization = new EduProgramSpecialization.Path<EduProgramSpecialization>(L_PROGRAM_SPECIALIZATION, this);
            return _programSpecialization;
        }

    /**
     * @return Выпускающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock#getOwnerOrgUnit()
     */
        public EduOwnerOrgUnit.Path<EduOwnerOrgUnit> ownerOrgUnit()
        {
            if(_ownerOrgUnit == null )
                _ownerOrgUnit = new EduOwnerOrgUnit.Path<EduOwnerOrgUnit>(L_OWNER_ORG_UNIT, this);
            return _ownerOrgUnit;
        }

        public Class getEntityClass()
        {
            return EppEduPlanVersionSpecializationBlock.class;
        }

        public String getEntityName()
        {
            return "eppEduPlanVersionSpecializationBlock";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
