/* $Id$ */
package ru.tandemservice.uniepp.dao.registry.data;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.data.EppRegElementKeyGenerator.KeyGenRule;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Реализация враппера элемента реестра для строки УПв (точнее, враппера строки УПв).
 * Можно использовать только для создания элементов реестра на основе строк УПв.
 * Большая часть методов пока не поддерживается за ненадобностью.
 *
 * Условно не мутабельный.
 *
 * @author Nikolay Fedorovskih
 * @since 12.05.2015
 */
public class EppEpvRegElWrapper implements IEppRegElWrapper
{
    private final IEppEpvRowWrapper epvRowWrapper;
    private final Map<Integer, IEppRegElPartWrapper> partMap;

    public EppEpvRegElWrapper(IEppEpvRowWrapper epvRowWrapper)
    {
        Preconditions.checkArgument(epvRowWrapper.getRow() instanceof EppEpvRegistryRow);

        this.epvRowWrapper = epvRowWrapper;

        // Сразу создадим врапперы для частей элемента реестра
        ImmutableMap.Builder<Integer, IEppRegElPartWrapper> partMapBuilder = ImmutableMap.builder();
        int partNumber = 1;
        for (Integer term : epvRowWrapper.getActiveTermSet())
        {
            IEppRegElPartWrapper partWrapper = new EppEpvRegElPartWrapper(partNumber, term, this);
            partMapBuilder.put(partNumber, partWrapper);
            partNumber++;
        }
        this.partMap = partMapBuilder.build();
    }

    public IEppEpvRowWrapper getEpvRowWrapper()
    {
        return this.epvRowWrapper;
    }

    @Override
    public String generateKey(KeyGenRule rule)
    {
        return EppRegElementKeyGenerator.generate(this, rule);
    }

    @Override
    public EppEduGroupSplitVariant getEduGroupSplitVariant() {
        return null;
    }

    @Override
    public boolean isEduGroupSplitByType() {
        return false;
    }

    @Override
    public List<EppGroupType> getSplitGroupTypes() {
        return Collections.emptyList();
    }

    @Override
    public Map<Integer, IEppRegElPartWrapper> getPartMap()
    {
        return this.partMap;
    }

    @Override
    public EppRegistryStructure getRegistryElementType()
    {
        return this.epvRowWrapper.getType();
    }

    @Override
    public OrgUnit getOwner()
    {
        return ((EppEpvRegistryRow) this.epvRowWrapper.getRow()).getRegistryElementOwner();
    }

    @Override
    public long getLoadSize()
    {
        return UniEppUtils.unwrap(this.getLoadAsDouble(EppLoadType.FULL_CODE_TOTAL_HOURS));
    }

    @Override
    public long getLabor()
    {
        return UniEppUtils.unwrap(this.getLoadAsDouble(EppLoadType.FULL_CODE_LABOR));
    }

    @Override
    public double getLoadAsDouble(String loadFullCode)
    {
        return Math.max(0d, this.epvRowWrapper.getTotalLoad(0, loadFullCode));
    }

    @Override
    public double getChildrenLoadAsDouble(String loadFullCode)
    {
        double result = 0d;
        for (final IEppEpvRowWrapper child : this.getEpvRowWrapper().getChilds()) {
            result += child.getTotalLoad(null, loadFullCode);
        }
        return result;
    }

    @Override
    public int getActionSize(String actionFullCode)
    {
        return this.epvRowWrapper.getActionSize(null, actionFullCode);
    }

    @Override
    public long getWeeks()
    {
        if (getRegistryElementType().isDisciplineElement()) {
            return 0;
        }
        return UniEppUtils.unwrap(Math.max(0d, this.epvRowWrapper.getTotalLoad(null, EppLoadType.FULL_CODE_WEEKS)));
    }

    @Override
    public Long getId()
    {
        return this.epvRowWrapper.getId();
    }

    @Override
    public String getTitle()
    {
        return this.epvRowWrapper.getTitle();
    }

    // а нельзя. Этот враппер для создания элеемнта реестра на основе строки УПв, а не чего-то еще.
    @Override public EppRegistryElement getItem() { throw new UnsupportedOperationException(); }
    @Override public EppState getState() { throw new UnsupportedOperationException(); }
    @Override public void setId(Long id) { throw new UnsupportedOperationException(); }
    @Override public Object getProperty(Object propertyPath) { throw new UnsupportedOperationException(); }
    @Override public void setProperty(String propertyPath, Object propertyValue) { throw new UnsupportedOperationException(); }
}