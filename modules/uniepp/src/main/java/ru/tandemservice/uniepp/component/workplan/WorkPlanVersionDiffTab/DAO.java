package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionDiffTab;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.IRawFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.component.workplan.WorkPlanData.WorkPlanDataBaseDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanVersionGen;

/**
 * @author vdanilov
 */
public class DAO extends WorkPlanDataBaseDAO<Model> implements IDAO {

    protected static IRawFormatter<Collection<EppWorkPlanBase>> FORMATTER = (IRawFormatter<Collection<EppWorkPlanBase>>) source -> {
        final StringBuilder sb = new StringBuilder();
        for (final EppWorkPlanBase plan: source) {
            if (sb.length() > 0) { sb.append("\n"); }
            sb.append(plan.getShortTitle());
        }
        return UniEppUtils.NEW_LINE_FORMATTER.format(sb.toString());
    };


    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model) {
        super.prepare(model);

        class Operation {
            final Long id = model.getId();
            final EppWorkPlan workPlan = DAO.this.getNotNull(EppWorkPlan.class, this.id);

            final List<EppWorkPlanVersion> workPlanVersionList = new MQBuilder(EppWorkPlanVersionGen.ENTITY_CLASS, "e")
            .add(MQExpression.eq("e", EppWorkPlanVersionGen.parent().id().s(), this.id))
            .getResultList(DAO.this.getSession());

            final Map<Long, IEppWorkPlanWrapper> versionDataMap = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(UniBaseDao.ids(this.workPlanVersionList));

            final Map<String, Collection<EppWorkPlanBase>> rowMap = SafeMap.get(LinkedHashSet.class);
            final int total = 1 + this.versionDataMap.size();

            public void processDataSource(final StaticListDataSource<IEppWorkPlanRowWrapper> dataSource, final Predicate predicate) {

                // грузим то, что уже есть в датасорсе
                for (final IEppWorkPlanRowWrapper wrapper: dataSource.getRowList()) {
                    this.rowMap.get(wrapper.getDescriptionString()).add(this.workPlan);
                }

                // теперь бежим по версиям
                for (final EppWorkPlanVersion workPlanVersion: this.workPlanVersionList) {
                    final IEppWorkPlanWrapper versionWrapper = this.versionDataMap.get(workPlanVersion.getId());
                    final Collection<IEppWorkPlanRowWrapper> rowWrappers = CollectionUtils.select(versionWrapper.getRowMap().values(), predicate);
                    for (final IEppWorkPlanRowWrapper wrapper: rowWrappers) {
                        final Collection<EppWorkPlanBase> collection = this.rowMap.get(wrapper.getDescriptionString());
                        if (collection.isEmpty()) {
                            // если такой строки еще не было - то добавляем ее в список
                            dataSource.getRowList().add(wrapper);
                        }
                        collection.add(workPlanVersion);
                    }
                }

                // после всех извращений - сортируем в первую очередь по названию
                // по индексу сразу сортировать нельзя - если он изменился, то записи будут далеко друг от друга
                Collections.sort(dataSource.getRowList(), ITitled.TITLED_COMPARATOR);

                // собственно наша важная колонка
                dataSource.addColumn(new SimpleColumn("Версии", "versionList") {
                    @Override public boolean isRawContent() { return true; }
                    @Override public String getContent(final IEntity entity) {
                        final Collection<EppWorkPlanBase> source = Operation.this.rowMap.get(((IEppWorkPlanRowWrapper)entity).getDescriptionString());
                        if (source.size() == Operation.this.total) { return ""; }
                        return DAO.FORMATTER.format(source);
                    }
                }.setWidth(20));

                // ну и нужно подсвечивать разными цветами, как в diff
                dataSource.setRowCustomizer(new SimpleRowCustomizer<IEppWorkPlanRowWrapper>() {
                    @Override public String getRowStyle(final IEppWorkPlanRowWrapper entity) {
                        final Collection<EppWorkPlanBase> source = Operation.this.rowMap.get((entity).getDescriptionString());
                        if (source.size() == Operation.this.total) { return "font-weight:bold;"; }
                        final int color = (96 - ((96 * source.size()) / Operation.this.total));
                        return "color: RGB("+color+","+color+","+color+");";
                    }
                });
            }
        }

        final Operation op = new Operation();
        op.processDataSource(model.getDisciplineDataSource(), IEppWorkPlanRowWrapper.DISCIPLINE_ROWS);
        op.processDataSource(model.getActionDataSource(), IEppWorkPlanRowWrapper.NON_DISCIPLINE_ROWS);
    }
}
