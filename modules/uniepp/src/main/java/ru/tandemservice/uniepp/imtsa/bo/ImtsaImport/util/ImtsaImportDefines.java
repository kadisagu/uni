/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util;

import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;

/**
 * @author azhebko
 * @since 12.11.2014
 */
public interface ImtsaImportDefines
{
    // Виды учебной нагрузки
    public static final String L_LECTURES = EppALoadType.FULL_CODE_TOTAL_LECTURES;
    public static final String L_PRACTICE = EppALoadType.FULL_CODE_TOTAL_PRACTICE;
    public static final String L_LABS = EppALoadType.FULL_CODE_TOTAL_LABS;

    public static final String L_LECTURES_I = EppALoadType.FULL_CODE_TOTAL_LECTURES_I;  // EppALoadType.iFullCode(EppALoadType.FULL_CODE_TOTAL_LECTURES)
    public static final String L_PRACTICE_I = EppALoadType.FULL_CODE_TOTAL_PRACTICE_I;  // EppALoadType.iFullCode(EppALoadType.FULL_CODE_TOTAL_PRACTICE)
    public static final String L_LABS_I = EppALoadType.FULL_CODE_TOTAL_LABS_I;          // EppALoadType.iFullCode(EppALoadType.FULL_CODE_TOTAL_LABS)

    public static final String L_LECTURES_E = EppALoadType.FULL_CODE_TOTAL_LECTURES_E;  // EppALoadType.eFullCode(EppALoadType.FULL_CODE_TOTAL_LECTURES)
    public static final String L_PRACTICE_E = EppALoadType.FULL_CODE_TOTAL_PRACTICE_E;  // EppALoadType.eFullCode(EppALoadType.FULL_CODE_TOTAL_PRACTICE)
    public static final String L_LABS_E = EppALoadType.FULL_CODE_TOTAL_LABS_E;          // EppALoadType.eFullCode(EppALoadType.FULL_CODE_TOTAL_LABS)

    public static final String L_TOTAL_AUDIT = EppELoadType.FULL_CODE_AUDIT;
    public static final String L_TOTAL_SELF_WORK = EppELoadType.FULL_CODE_SELFWORK;
    public static final String L_TOTAL_SIZE = EppLoadType.FULL_CODE_TOTAL_HOURS;
    public static final String L_TOTAL_LABOR = EppLoadType.FULL_CODE_LABOR;
    public static final String L_TOTAL_WEEKS = EppLoadType.FULL_CODE_WEEKS;
    public static final String L_TOTAL_CONTROL = EppLoadType.FULL_CODE_CONTROL;
    public static final String L_TOTAL_CONTROL_E = EppLoadType.FULL_CODE_CONTROL_E;

    // Формы итогового контроля
    public static final String CA_EXAM = EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM;
    public static final String CA_SETOFF = EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF;
    public static final String CA_SETOFF_DIFF = EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF;
    public static final String CA_COURSE_WORK = EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK;
    public static final String CA_COURSE_PROJECT = EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT;
}