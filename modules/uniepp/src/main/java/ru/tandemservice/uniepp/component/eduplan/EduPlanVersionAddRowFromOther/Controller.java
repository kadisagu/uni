/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddRowFromOther;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author nkokorina
 * Created on: 20.09.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent component)
    {
        this.getDao().update(this.getModel(component));
        this.deactivate(component);
    }

    public void onChangeSourceBlock(final IBusinessComponent component)
    {
        this.getDao().prepareListDataSource(this.getModel(component));
    }

}
