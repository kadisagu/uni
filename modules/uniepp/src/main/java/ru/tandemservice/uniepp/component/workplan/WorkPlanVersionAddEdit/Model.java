/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionAddEdit;

import ru.tandemservice.uniepp.component.base.SimpleAddEditModel;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

/**
 * @author nkokorina
 * Created on: 10.08.2010
 */
public class Model extends SimpleAddEditModel<EppWorkPlanVersion>
{
}
