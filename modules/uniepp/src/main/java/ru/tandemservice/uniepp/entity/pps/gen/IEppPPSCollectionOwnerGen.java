package ru.tandemservice.uniepp.entity.pps.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.pps.IEppPPSCollectionOwner;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.uniepp.entity.pps.IEppPPSCollectionOwner;

/**
 * Объект, в котором могут жить ППС
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEppPPSCollectionOwnerGen extends InterfaceStubBase
 implements IEppPPSCollectionOwner{
    public static final int VERSION_HASH = -1908078561;



    private static final Path<IEppPPSCollectionOwner> _dslPath = new Path<IEppPPSCollectionOwner>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.uniepp.entity.pps.IEppPPSCollectionOwner");
    }
            

    public static class Path<E extends IEppPPSCollectionOwner> extends EntityPath<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return IEppPPSCollectionOwner.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.uniepp.entity.pps.IEppPPSCollectionOwner";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
