/* $Id:$ */
package ru.tandemservice.uniepp.entity.plan;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.common.ITitled;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;

import java.util.Comparator;

/**
 * @author oleyba
 * @since 8/19/14
 */
public interface IEppEduPlanDevelopCombination extends ITitled
{
    static int safeCompareCatalogItem(ICatalogItem o1, ICatalogItem o2) {
        if (o1 == null && o2 == null) return 0;
        if (o1 == null) return -1;
        if (o2 == null) return 1;
        return o1.getCode().compareTo(o2.getCode());
    }

    Comparator<IEppEduPlanDevelopCombination> COMPARATOR = (o1, o2) -> {
        int i;
        if (0 != (i = safeCompareCatalogItem(o1.getProgramForm(), o2.getProgramForm()))) { return i; }
        if (0 != (i = safeCompareCatalogItem(o1.getDevelopCondition(), o2.getDevelopCondition()))) { return i; }
        if (0 != (i = safeCompareCatalogItem(o1.getProgramTrait(), o2.getProgramTrait()))) { return i; }
        if (0 != (i = safeCompareCatalogItem(o1.getDevelopGrid(), o2.getDevelopGrid()))) { return i; }
        if (0 != (i = safeCompareCatalogItem(o1.getOrientation(), o2.getOrientation()))) { return i; }
        return 0;
    };


    EduProgramForm getProgramForm();
    EduProgramTrait getProgramTrait();
    DevelopCondition getDevelopCondition();
    DevelopGrid getDevelopGrid();
    EduProgramOrientation getOrientation();
}

