/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionScheduleTab;

import java.util.List;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniepp.util.WeekTypeLegendRow;

/**
 * @author vip_delete
 * @since 03.03.2010
 */

public class Model extends ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub.EduPlanVersionPubModel
{

    private RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> _scheduleDataSource; // таблица учебного графика
    private StaticListDataSource<ViewWrapper<Course>> _timesDataSource;       // сводные данные по бюджету времени

    private List<WeekTypeLegendRow> _weekTypeLegendList;          // строки легенды
    private WeekTypeLegendRow _weekTypeLegendItem;                // текущее значение цикла


    public RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> getScheduleDataSource()
    {
        return this._scheduleDataSource;
    }

    public void setScheduleDataSource(final RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> scheduleDataSource)
    {
        this._scheduleDataSource = scheduleDataSource;
    }

    public StaticListDataSource<ViewWrapper<Course>> getTimesDataSource()
    {
        return this._timesDataSource;
    }

    public void setTimesDataSource(final StaticListDataSource<ViewWrapper<Course>> timesDataSource)
    {
        this._timesDataSource = timesDataSource;
    }

    public List<WeekTypeLegendRow> getWeekTypeLegendList()
    {
        return this._weekTypeLegendList;
    }

    public void setWeekTypeLegendList(final List<WeekTypeLegendRow> weekTypeLegendList)
    {
        this._weekTypeLegendList = weekTypeLegendList;
    }

    public WeekTypeLegendRow getWeekTypeLegendItem()
    {
        return this._weekTypeLegendItem;
    }

    public void setWeekTypeLegendItem(final WeekTypeLegendRow weekTypeLegendItem)
    {
        this._weekTypeLegendItem = weekTypeLegendItem;
    }



}
