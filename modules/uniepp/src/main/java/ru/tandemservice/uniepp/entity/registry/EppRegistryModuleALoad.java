package ru.tandemservice.uniepp.entity.registry;

import java.util.Comparator;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryModuleALoadGen;

/**
 * Учебный модуль: аудиторная нагрузка
 *
 * Аудиторная нагрузка по модулю
 */
public class EppRegistryModuleALoad extends EppRegistryModuleALoadGen
{
    public static final Comparator<EppRegistryModuleALoad> BY_TYPE_COMPARATOR = new Comparator<EppRegistryModuleALoad>() {
        @Override public int compare(final EppRegistryModuleALoad o1, final EppRegistryModuleALoad o2) {
            final EppLoadType t1 = (null == o1 ? null : o1.getLoadType());
            final EppLoadType t2 = (null == o2 ? null : o2.getLoadType());
            return EppLoadType.COMPARATOR.compare(t1, t2);
        }
    };

    public EppRegistryModuleALoad() {}
    public EppRegistryModuleALoad(final EppRegistryModule module, final EppALoadType loadType) {
        this.setModule(module);
        this.setLoadType(loadType);
    }

    public EppRegistryModuleALoad(final EppRegistryModule module, final EppALoadType loadType, double loadAsDouble) {
        this(module, loadType);
        this.setLoadAsDouble(loadAsDouble);
    }


    public Double getLoadAsDouble() {
        final long load = this.getLoad();
        return UniEppUtils.wrap(load < 0 ? null : load);
    }

    public void setLoadAsDouble(final Double value) {
        final Long load = UniEppUtils.unwrap(value);
        this.setLoad(null == load ? -1 : load.longValue());
    }

}