package ru.tandemservice.uniepp.dao.year;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.pupnag.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EppYearWorkGraphDAO extends UniBaseDao implements IEppYearWorkGraphDAO
{

    @Override
    public Set<Long> getFilteredEduPlanRowIds(final EppWorkGraph workGraph, final Collection<EduProgramSubject> subjectSet)
    {
        if ((subjectSet == null) || subjectSet.isEmpty()) {
            return null; // фильтровать не надо
        }

        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppWorkGraphRow2EduPlan.class, "rel");
        dql.fromEntity(EppEduPlanProf.class, "prof");
        dql.where(eq(property("prof"), property(EppWorkGraphRow2EduPlan.eduPlanVersion().eduPlan().fromAlias("rel"))));
        dql.where(in(property(EppEduPlanProf.programSubject().fromAlias("prof")), subjectSet));
        dql.column(property("rel.id")).predicate(DQLPredicateType.distinct);
        return new HashSet<>(dql.createStatement(this.getSession()).<Long>list());
    }

    @Override
    public Map<EduProgramSubject, Map<EppWorkGraphRow, Map<EppYearEducationWeek, EppWeekType>>> getWorkGraphMapGroupByProgramSubject(final EppWorkGraph workGraph, final Collection<Course> courseSet, final Collection<EduProgramSubject> highSchoolSet)
    {
        final Set<Long> ids = this.getFilteredEduPlanRowIds(workGraph, highSchoolSet);

        // EducationLevelsHighSchool ->  List<EppWorkGraphRow2EduPlan>
        final MQBuilder rBuilder = new MQBuilder(EppWorkGraphRow2EduPlan.ENTITY_CLASS, "rel");
        rBuilder.add(MQExpression.eq("rel", EppWorkGraphRow2EduPlan.row().graph().s(), workGraph));
        if ((courseSet != null) && !courseSet.isEmpty())
        {
            rBuilder.add(MQExpression.in("rel", EppWorkGraphRow2EduPlan.row().course().s(), courseSet));
        }
        final List<EppWorkGraphRow2EduPlan> eduPlanRelListNonFiltered = rBuilder.getResultList(this.getSession());

        // фильтруем по направлениям подготовки
        List<EppWorkGraphRow2EduPlan> eduPlanRelList;
        if (ids == null)
        {
            eduPlanRelList = eduPlanRelListNonFiltered;
        }
        else
        {
            eduPlanRelList = new ArrayList<>();
            for (final EppWorkGraphRow2EduPlan rel : eduPlanRelListNonFiltered)
            {
                if (ids.contains(rel.getId()))
                {
                    eduPlanRelList.add(rel);
                }
            }
        }

        final Map<EduProgramSubject, List<EppWorkGraphRow2EduPlan>> edu2rows = new HashMap<>();
        for (final EppWorkGraphRow2EduPlan rel : eduPlanRelList) {
            EppEduPlan eduPlan = rel.getEduPlanVersion().getEduPlan();
            if (!(eduPlan instanceof EppEduPlanProf)) continue;

            final EduProgramSubject subject = ((EppEduPlanProf) eduPlan).getProgramSubject();
            SafeMap.safeGet(edu2rows, subject, ArrayList.class).add(rel);
        }

        // EppWorkGraphRow -> List<EppWorkGraphRowWeek>
        final MQBuilder wBuilder = new MQBuilder(EppWorkGraphRowWeek.ENTITY_CLASS, "rw");
        wBuilder.add(MQExpression.eq("rw", EppWorkGraphRowWeek.row().graph().s(), workGraph));
        if ((courseSet != null) && !courseSet.isEmpty())
        {
            wBuilder.add(MQExpression.in("rw", EppWorkGraphRowWeek.row().course().s(), courseSet));
        }
        final List<EppWorkGraphRowWeek> rowWeekList = wBuilder.getResultList(this.getSession());

        final Map<EppWorkGraphRow, List<EppWorkGraphRowWeek>> row2weeks = new HashMap<>();
        for (final EppWorkGraphRowWeek rowWeek : rowWeekList)
        {
            List<EppWorkGraphRowWeek> list = row2weeks.get(rowWeek.getRow());
            if (list == null)
            {
                row2weeks.put(rowWeek.getRow(), list = new ArrayList<>());
            }
            list.add(rowWeek);
        }

        // массив недель
        final EppYearEducationWeek[] weekData = IEppYearDAO.instance.get().getYearEducationWeeks(workGraph.getYear().getId());

        final Comparator<EppWorkGraphRow> WORK_GRAPH_ROW_COMPARATOR = new Comparator<EppWorkGraphRow>()
        {
            @Override
            public int compare(final EppWorkGraphRow o1, final EppWorkGraphRow o2)
            {
                final int result = o1.getCourse().getIntValue() - o2.getCourse().getIntValue();
                if (result != 0)
                {
                    return result;
                }
                return o1.getId().compareTo(o2.getId());
            }
        };

        // все данные готовы. заполняем результирующий мап
        final Map<EduProgramSubject, Map<EppWorkGraphRow, Map<EppYearEducationWeek, EppWeekType>>> dataMap = new TreeMap<>();

        for (final Map.Entry<EduProgramSubject, List<EppWorkGraphRow2EduPlan>> entry : edu2rows.entrySet())
        {
            Map<EppWorkGraphRow, Map<EppYearEducationWeek, EppWeekType>> dataValue = dataMap.get(entry.getKey());
            if (dataValue == null)
            {
                dataValue = new TreeMap<>(WORK_GRAPH_ROW_COMPARATOR);
            }

            for (final EppWorkGraphRow2EduPlan rel : entry.getValue())
            {
                final List<EppWorkGraphRowWeek> rowList = row2weeks.get(rel.getRow());

                Map<EppYearEducationWeek, EppWeekType> week2type = dataValue.get(rel.getRow());
                if (week2type == null)
                {
                    dataValue.put(rel.getRow(), week2type = new HashMap<>());
                }

                if (rowList != null)
                {
                    for (final EppWorkGraphRowWeek rowWeek : rowList)
                    {
                        week2type.put(weekData[rowWeek.getWeek() - 1], rowWeek.getType());
                    }
                }
            }

            if (!dataValue.isEmpty())
            {
                dataMap.put(entry.getKey(), dataValue);
            }
        }

        return dataMap;
    }

    @Override
    public Map<EduProgramSubject, Map<EppWorkGraphRow, Set<EppWorkGraphRowWeek>>> getWorkGraphRowMapGroupByProgramSubject(final EppWorkGraph workGraph, final Collection<Course> courseSet, final Collection<EduProgramSubject> highSchoolSet)
    {
        final Set<Long> ids = this.getFilteredEduPlanRowIds(workGraph, highSchoolSet);

        // EducationLevelsHighSchool ->  List<EppWorkGraphRow2EduPlan>
        final MQBuilder rBuilder = new MQBuilder(EppWorkGraphRow2EduPlan.ENTITY_CLASS, "rel");
        rBuilder.add(MQExpression.eq("rel", EppWorkGraphRow2EduPlan.row().graph().s(), workGraph));
        if ((courseSet != null) && !courseSet.isEmpty())
        {
            rBuilder.add(MQExpression.in("rel", EppWorkGraphRow2EduPlan.row().course().s(), courseSet));
        }
        final List<EppWorkGraphRow2EduPlan> eduPlanRelListNonFiltered = rBuilder.getResultList(this.getSession());

        // фильтруем по направлениям подготовки
        List<EppWorkGraphRow2EduPlan> eduPlanRelList;
        if (ids == null)
        {
            eduPlanRelList = eduPlanRelListNonFiltered;
        }
        else
        {
            eduPlanRelList = new ArrayList<>();
            for (final EppWorkGraphRow2EduPlan rel : eduPlanRelListNonFiltered)
            {
                if (ids.contains(rel.getId()))
                {
                    eduPlanRelList.add(rel);
                }
            }
        }

        final Map<EduProgramSubject, Set<EppWorkGraphRow2EduPlan>> edu2rows = new HashMap<>();
        for (final EppWorkGraphRow2EduPlan rel : eduPlanRelList)
        {
            EppEduPlan eduPlan = rel.getEduPlanVersion().getEduPlan();
            if (!(eduPlan instanceof EppEduPlanProf)) continue;

            final EduProgramSubject subject = ((EppEduPlanProf) eduPlan).getProgramSubject();
            SafeMap.safeGet(edu2rows, subject, HashSet.class).add(rel);
        }

        // EppWorkGraphRow -> List<EppWorkGraphRowWeek>
        final MQBuilder wBuilder = new MQBuilder(EppWorkGraphRowWeek.ENTITY_CLASS, "rw");
        wBuilder.add(MQExpression.eq("rw", EppWorkGraphRowWeek.row().graph().s(), workGraph));
        if ((courseSet != null) && !courseSet.isEmpty())
        {
            wBuilder.add(MQExpression.in("rw", EppWorkGraphRowWeek.row().course().s(), courseSet));
        }
        final List<EppWorkGraphRowWeek> rowWeekList = wBuilder.getResultList(this.getSession());

        final Map<EppWorkGraphRow, List<EppWorkGraphRowWeek>> row2weeks = new HashMap<>();
        for (final EppWorkGraphRowWeek rowWeek : rowWeekList)
        {
            List<EppWorkGraphRowWeek> list = row2weeks.get(rowWeek.getRow());
            if (list == null)
            {
                row2weeks.put(rowWeek.getRow(), list = new ArrayList<>());
            }
            list.add(rowWeek);
        }

        final Comparator<EppWorkGraphRow> WORK_GRAPH_ROW_COMPARATOR = new Comparator<EppWorkGraphRow>()
        {
            @Override
            public int compare(final EppWorkGraphRow o1, final EppWorkGraphRow o2)
            {
                final int result = o1.getCourse().getIntValue() - o2.getCourse().getIntValue();
                if (result != 0)
                {
                    return result;
                }
                return o1.getId().compareTo(o2.getId());
            }
        };

        // все данные готовы. заполняем результирующий мап
        final Map<EduProgramSubject, Map<EppWorkGraphRow, Set<EppWorkGraphRowWeek>>> dataMap = new TreeMap<>();

        for (final Map.Entry<EduProgramSubject, Set<EppWorkGraphRow2EduPlan>> entry : edu2rows.entrySet())
        {
            Map<EppWorkGraphRow, Set<EppWorkGraphRowWeek>> dataValue = dataMap.get(entry.getKey());
            if (dataValue == null)
            {
                dataValue = new TreeMap<>(WORK_GRAPH_ROW_COMPARATOR);
            }

            for (final EppWorkGraphRow2EduPlan rel : entry.getValue())
            {
                final List<EppWorkGraphRowWeek> rowList = row2weeks.get(rel.getRow());

                Set<EppWorkGraphRowWeek> rowSet = dataValue.get(rel.getRow());
                if (rowSet == null)
                {
                    dataValue.put(rel.getRow(), new HashSet<EppWorkGraphRowWeek>());
                }

                if (rowList != null)
                {
                    dataValue.get(rel.getRow()).addAll(rowList);
                }
            }

            if (!dataValue.isEmpty())
            {
                dataMap.put(entry.getKey(), dataValue);
            }
        }

        return dataMap;
    }


    @Override
    public void doUpdateWorkGraphRow(final Long editRowId, final Map<PairKey<Long, Long>, EppWeekType> dataMap, final int[] ranges)
    {
        final Session session = this.getSession();
        final EppWorkGraphRow row = this.getNotNull(EppWorkGraphRow.class, editRowId);
        final Course course = row.getCourse();
        final EppWorkGraph workGraph = row.getGraph();

        // V A L I D A T E

        // детализация учебной сетки

        final MQBuilder termBuilder = new MQBuilder(DevelopGridTerm.ENTITY_CLASS, "t");
        termBuilder.add(MQExpression.eq("t", DevelopGridTerm.L_DEVELOP_GRID, workGraph.getDevelopGrid()));
        termBuilder.add(MQExpression.eq("t", DevelopGridTerm.L_COURSE, course));
        final List<DevelopGridTerm> termList = termBuilder.getResultList(this.getSession());
        if (termList.isEmpty())
        {
            return; // Нет ни одной части из учебной сетки. нечего сохранять.
        }

        final Map<Integer, Term> part2term = new TreeMap<>();
        for (final DevelopGridTerm term : termList) {
            part2term.put(term.getPartNumber(), term.getTerm());
        }
        final List<Term> range2term = new ArrayList<>(part2term.values());

        // все недели в ГУП в виде массива
        final EppYearEducationWeek[] weekArray = IEppYearDAO.instance.get().getYearEducationWeeks(workGraph.getYear().getId());
        final EppWeekType theory = this.getCatalogItem(EppWeekType.class, EppWeekType.CODE_THEORY);

        // существующие в базе связи для данной строки
        final Map<PairKey<Long, Long>, EppWorkGraphRowWeek> map = new HashMap<>();
        for (final EppWorkGraphRowWeek item : getList(EppWorkGraphRowWeek.class, EppWorkGraphRowWeek.row(), row)) {
            map.put(PairKey.create(item.getRow().getId(), weekArray[item.getWeek() - 1].getId()), item);
        }

        for (final EppYearEducationWeek week : weekArray)
        {
            final PairKey<Long, Long> key = PairKey.create(editRowId, week.getId());
            EppWorkGraphRowWeek item = map.get(key);
            final int rangeNumber = ranges[week.getNumber() - 1];
            if (rangeNumber == -1)
            {
                // неделя не попала ни в один диапазон. надо удалить связь
                if (item != null)
                {
                    session.delete(item);
                }
            } else
            {
                final Term term = range2term.get(rangeNumber);
                final EppWeekType weekType = dataMap.get(key);
                if (item == null)
                {
                    // надо создать
                    item = new EppWorkGraphRowWeek();
                    item.setRow(row);
                    item.setWeek(week.getNumber());
                    item.setType(null == weekType ? theory : weekType);
                    item.setTerm(term);
                    session.save(item);
                }
                else
                {
                    // надо обновить
                    item.setType(null == weekType ? theory : weekType);
                    item.setTerm(term);
                    session.update(item);
                }
            }
        }
    }

    @Override
    public void doUpdateCombineRows(final Long templateRowId, final Set<Long> selectedIds)
    {
        if (selectedIds.size() <= 1)
        {
            throw new ApplicationException("Объединяемых строк должно быть больше одной.");
        }
        if (templateRowId == null)
        {
            throw new ApplicationException("Не выбран шаблон объединения.");
        }
        if (!selectedIds.contains(templateRowId))
        {
            throw new ApplicationException("Шаблон строки не входит в список объединяемых строк.");
        }

        final Session session = this.getSession();
        final List<IEntity> forSave = new ArrayList<>();

        final EppWorkGraphRow templateRow = this.getNotNull(EppWorkGraphRow.class, templateRowId);

        // получаем ячейки шаблонной строки
        final List<EppWorkGraphRowWeek> templateWeekList = this.getList(EppWorkGraphRowWeek.class, EppWorkGraphRowWeek.row(), templateRow);

        // создаем новую строку ГУП.
        final EppWorkGraphRow row = new EppWorkGraphRow();
        row.setGraph(templateRow.getGraph());
        row.setCourse(templateRow.getCourse());
        forSave.add(row);

        // получаем все EduPlanVersion у объединяемых строк
        final List<EppEduPlanVersion> eduPlanVersionList = new DQLSelectBuilder()
        .fromEntity(EppEduPlanVersion.class, "epv").column(property("epv"))
        .where(in(
            property("epv.id"),
            new DQLSelectBuilder()
            .fromEntity(EppWorkGraphRow2EduPlan.class, "r").column(property(EppWorkGraphRow2EduPlan.eduPlanVersion().id().fromAlias("r")))
            .where(in(property(EppWorkGraphRow2EduPlan.row().id().fromAlias("r")), selectedIds))
            .buildQuery()
        ))
        .createStatement(session).list();

        // сохраняем такие связи
        for (final EppEduPlanVersion eduPlanVersion : eduPlanVersionList)
        {
            final EppWorkGraphRow2EduPlan row2EduPlan = new EppWorkGraphRow2EduPlan();
            row2EduPlan.setRow(row);
            row2EduPlan.setEduPlanVersion(eduPlanVersion);
            forSave.add(row2EduPlan);
        }

        // создаем ячейки в новой строке ГУП
        for (final EppWorkGraphRowWeek item : templateWeekList)
        {
            final EppWorkGraphRowWeek rel = new EppWorkGraphRowWeek();
            rel.setRow(row);
            rel.setWeek(item.getWeek());
            rel.setTerm(item.getTerm());
            rel.setType(item.getType());
            forSave.add(rel);
        }

        // удаляем старые строки
        for (final Long rowId : selectedIds)
        {
            session.delete(this.getNotNull(EppWorkGraphRow.class, rowId));
        }

        // запускаем удаление в базе + выполнение всех каскадов
        session.flush();

        // сохраняем все созданные объекты
        for (final IEntity entity : forSave)
        {
            session.save(entity);
        }
    }

    @Override
    public void doUpdateExcludeRow(final Long excludeRowId)
    {
        final Session session = this.getSession();
        final List<IEntity> forSave = new ArrayList<>();

        final EppWorkGraphRow2EduPlan row2EduPlan = this.getNotNull(EppWorkGraphRow2EduPlan.class, excludeRowId);

        // получаем ячейки шаблонной строки
        final MQBuilder builder = new MQBuilder(EppWorkGraphRowWeek.ENTITY_CLASS, "rw");
        builder.add(MQExpression.eq("rw", EppWorkGraphRowWeek.L_ROW, row2EduPlan.getRow()));
        final List<EppWorkGraphRowWeek> templateWeekList = builder.getResultList(session);

        // создаем новую строку ГУП.
        final EppWorkGraphRow row = new EppWorkGraphRow();
        row.setGraph(row2EduPlan.getRow().getGraph());
        row.setCourse(row2EduPlan.getRow().getCourse());
        forSave.add(row);

        final EppWorkGraphRow2EduPlan row2EduPlanRel = new EppWorkGraphRow2EduPlan();
        row2EduPlanRel.setRow(row);
        row2EduPlanRel.setEduPlanVersion(row2EduPlan.getEduPlanVersion());
        forSave.add(row2EduPlanRel);

        // создаем ячейки в новой строке ГУП
        for (final EppWorkGraphRowWeek item : templateWeekList)
        {
            final EppWorkGraphRowWeek rel = new EppWorkGraphRowWeek();
            rel.setRow(row);
            rel.setWeek(item.getWeek());
            rel.setTerm(item.getTerm());
            rel.setType(item.getType());
            forSave.add(rel);
        }

        // удаляем старые связи
        session.delete(row2EduPlan);

        // запускаем удаление в базе + выполнение всех каскадов
        session.flush();

        // сохраняем все созданные объекты
        for (final IEntity entity : forSave)
        {
            session.save(entity);
        }
    }
}
