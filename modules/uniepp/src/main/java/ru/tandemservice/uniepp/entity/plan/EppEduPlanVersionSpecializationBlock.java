package ru.tandemservice.uniepp.entity.plan;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.IPropertyPath;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.commonbase.base.entity.IReorganizationElement;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.gen.EduOwnerOrgUnitGen;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionSpecializationBlockGen;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Блок направленности в версии УП
 */
public class EppEduPlanVersionSpecializationBlock extends EppEduPlanVersionSpecializationBlockGen
{
    public EppEduPlanVersionSpecializationBlock()
    {
    }

    public EppEduPlanVersionSpecializationBlock(final EppEduPlanVersion eduPlanVersion, final EduProgramSpecialization specialization) {
        this.setEduPlanVersion(eduPlanVersion);
        this.setProgramSpecialization(specialization);
    }

    @Override
    public boolean isRootBlock()
    {
        return false;
    }

    @Override
    public String getEducationElementSimpleTitle()
    {
        return getProgramSpecialization().getDisplayableTitle();
    }

    @Override
    public String getTitle() {
        if (getProgramSpecialization() == null) {
            return this.getClass().getSimpleName();
        }
        return getProgramSpecialization().getTitle() + (getProgramSpecialization().isRootSpecialization() ? EduProgramSpecializationRoot.HARD_POSTFIX_TITLE : "");
    }

    @Override
    public String getEducationElementTitle() {
        return getProgramSpecialization().getDisplayableTitle();
    }

    @Override
    public String getSpecializationExtendedTitle()
    {
        final EppEduPlan plan = getEduPlanVersion().getEduPlan();
        final List<String> parts = Arrays.asList(
                plan.getProgramForm().getShortTitle(),
                plan.getDevelopGrid().getTitle(),
                plan.getProgramTrait() != null ? plan.getProgramTrait().getShortTitle() : "",
                plan.getOrientation() != null ? plan.getOrientation().getShortTitle() : null,
                getOwnerOrgUnit().getOrgUnit().getShortTitle(),
                String.valueOf(plan.getEduStartYear()) + (plan.getEduEndYear() != null ? ("-" + plan.getEduEndYear()) : "") + " г."
        );
        return getProgramSpecialization().getDisplayableTitle() + " (" + CommonBaseStringUtil.joinNotEmpty(parts, ", ") + ")";
    }

    @Override
    public IPropertyPath getReorgranizationTitlePath() {
        return titleForListWithVersionAndSpecialization();
    }

    @Override
    public void changeReorganizationElement(IReorganizationElement element) {
        final EduOwnerOrgUnit ownerOrgUnit = DataAccessServices.dao().getByNaturalId(new EduOwnerOrgUnit.NaturalId(((OrgUnit) element)));
        if (ownerOrgUnit == null) throw new RuntimeException("Подразделение " + element.getTitle() + " (id:" + element.getId() + ") не является выпускающим подразделением. Невозможно установить его в поле «Выпускающее подразделение» сущности «Блок направленности в версии УП»");
        setOwnerOrgUnit(ownerOrgUnit);
    }

    @Override
    public EventListenerLocker<IDSetEventListener> getLocker() {
        return EppDSetUpdateCheckEventListener.LOCKER;
    }

    @Override
    public List<IEntity> getLinkedEntitiesToUpdate(IReorganizationElement newOwner) {
        return Collections.emptyList();
    }
}