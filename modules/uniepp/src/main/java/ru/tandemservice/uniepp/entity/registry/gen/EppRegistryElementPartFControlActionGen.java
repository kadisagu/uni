package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часть элемента реестра: итоговая форма контроля
 *
 * Указывает набор форм итогового контроля по части элемента реестра
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegistryElementPartFControlActionGen extends EntityBase
 implements INaturalIdentifiable<EppRegistryElementPartFControlActionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction";
    public static final String ENTITY_NAME = "eppRegistryElementPartFControlAction";
    public static final int VERSION_HASH = 1731587187;
    private static IEntityMeta ENTITY_META;

    public static final String L_PART = "part";
    public static final String L_CONTROL_ACTION = "controlAction";
    public static final String L_GRADE_SCALE = "gradeScale";

    private EppRegistryElementPart _part;     // Часть элемента реестра
    private EppFControlActionType _controlAction;     // Форма итогового контроля
    private EppGradeScale _gradeScale;     // Шкала оценки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getPart()
    {
        return _part;
    }

    /**
     * @param part Часть элемента реестра. Свойство не может быть null.
     */
    public void setPart(EppRegistryElementPart part)
    {
        dirty(_part, part);
        _part = part;
    }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     */
    @NotNull
    public EppFControlActionType getControlAction()
    {
        return _controlAction;
    }

    /**
     * @param controlAction Форма итогового контроля. Свойство не может быть null.
     */
    public void setControlAction(EppFControlActionType controlAction)
    {
        dirty(_controlAction, controlAction);
        _controlAction = controlAction;
    }

    /**
     * Используется для выставления оценки по части дисциплины по указанному контрольному мероприятию
     *
     * @return Шкала оценки. Свойство не может быть null.
     */
    @NotNull
    public EppGradeScale getGradeScale()
    {
        return _gradeScale;
    }

    /**
     * @param gradeScale Шкала оценки. Свойство не может быть null.
     */
    public void setGradeScale(EppGradeScale gradeScale)
    {
        dirty(_gradeScale, gradeScale);
        _gradeScale = gradeScale;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRegistryElementPartFControlActionGen)
        {
            if (withNaturalIdProperties)
            {
                setPart(((EppRegistryElementPartFControlAction)another).getPart());
                setControlAction(((EppRegistryElementPartFControlAction)another).getControlAction());
            }
            setGradeScale(((EppRegistryElementPartFControlAction)another).getGradeScale());
        }
    }

    public INaturalId<EppRegistryElementPartFControlActionGen> getNaturalId()
    {
        return new NaturalId(getPart(), getControlAction());
    }

    public static class NaturalId extends NaturalIdBase<EppRegistryElementPartFControlActionGen>
    {
        private static final String PROXY_NAME = "EppRegistryElementPartFControlActionNaturalProxy";

        private Long _part;
        private Long _controlAction;

        public NaturalId()
        {}

        public NaturalId(EppRegistryElementPart part, EppFControlActionType controlAction)
        {
            _part = ((IEntity) part).getId();
            _controlAction = ((IEntity) controlAction).getId();
        }

        public Long getPart()
        {
            return _part;
        }

        public void setPart(Long part)
        {
            _part = part;
        }

        public Long getControlAction()
        {
            return _controlAction;
        }

        public void setControlAction(Long controlAction)
        {
            _controlAction = controlAction;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppRegistryElementPartFControlActionGen.NaturalId) ) return false;

            EppRegistryElementPartFControlActionGen.NaturalId that = (NaturalId) o;

            if( !equals(getPart(), that.getPart()) ) return false;
            if( !equals(getControlAction(), that.getControlAction()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPart());
            result = hashCode(result, getControlAction());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPart());
            sb.append("/");
            sb.append(getControlAction());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegistryElementPartFControlActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegistryElementPartFControlAction.class;
        }

        public T newInstance()
        {
            return (T) new EppRegistryElementPartFControlAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "part":
                    return obj.getPart();
                case "controlAction":
                    return obj.getControlAction();
                case "gradeScale":
                    return obj.getGradeScale();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "part":
                    obj.setPart((EppRegistryElementPart) value);
                    return;
                case "controlAction":
                    obj.setControlAction((EppFControlActionType) value);
                    return;
                case "gradeScale":
                    obj.setGradeScale((EppGradeScale) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "part":
                        return true;
                case "controlAction":
                        return true;
                case "gradeScale":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "part":
                    return true;
                case "controlAction":
                    return true;
                case "gradeScale":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "part":
                    return EppRegistryElementPart.class;
                case "controlAction":
                    return EppFControlActionType.class;
                case "gradeScale":
                    return EppGradeScale.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRegistryElementPartFControlAction> _dslPath = new Path<EppRegistryElementPartFControlAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegistryElementPartFControlAction");
    }
            

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction#getPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> part()
    {
        return _dslPath.part();
    }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction#getControlAction()
     */
    public static EppFControlActionType.Path<EppFControlActionType> controlAction()
    {
        return _dslPath.controlAction();
    }

    /**
     * Используется для выставления оценки по части дисциплины по указанному контрольному мероприятию
     *
     * @return Шкала оценки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction#getGradeScale()
     */
    public static EppGradeScale.Path<EppGradeScale> gradeScale()
    {
        return _dslPath.gradeScale();
    }

    public static class Path<E extends EppRegistryElementPartFControlAction> extends EntityPath<E>
    {
        private EppRegistryElementPart.Path<EppRegistryElementPart> _part;
        private EppFControlActionType.Path<EppFControlActionType> _controlAction;
        private EppGradeScale.Path<EppGradeScale> _gradeScale;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction#getPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> part()
        {
            if(_part == null )
                _part = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_PART, this);
            return _part;
        }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction#getControlAction()
     */
        public EppFControlActionType.Path<EppFControlActionType> controlAction()
        {
            if(_controlAction == null )
                _controlAction = new EppFControlActionType.Path<EppFControlActionType>(L_CONTROL_ACTION, this);
            return _controlAction;
        }

    /**
     * Используется для выставления оценки по части дисциплины по указанному контрольному мероприятию
     *
     * @return Шкала оценки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction#getGradeScale()
     */
        public EppGradeScale.Path<EppGradeScale> gradeScale()
        {
            if(_gradeScale == null )
                _gradeScale = new EppGradeScale.Path<EppGradeScale>(L_GRADE_SCALE, this);
            return _gradeScale;
        }

        public Class getEntityClass()
        {
            return EppRegistryElementPartFControlAction.class;
        }

        public String getEntityName()
        {
            return "eppRegistryElementPartFControlAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
