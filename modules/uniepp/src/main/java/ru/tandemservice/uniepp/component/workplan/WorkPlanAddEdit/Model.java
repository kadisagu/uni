package ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.component.base.SimpleAddEditModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.util.EppFilterUtil;

/**
 * 
 * @author nkokorina
 * @since 16.03.2010
 */


@Input( { @Bind(key = "selectedYear", binding = "selectedYear") })
@Return( { @Bind(key = Model.RETURN_WORKPLAN_ID, binding = "element.id")})
public class Model extends SimpleAddEditModel<EppWorkPlan>
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    public static final String RETURN_WORKPLAN_ID = "workPlanId";

    private ISelectModel yearEducationProcessListModel;
    private ISelectModel programSubjectModel;
    private ISelectModel developCombinationModel;
    private ISelectModel eduPlanVersionModel;
    private ISelectModel eduPlanVersionBlockModel;
    private ISelectModel termListModel;
    private ISelectModel programKindModel;
    private ISelectModel customEduPlanModel;

    private EduProgramKind programKind;
    private EduProgramSubject programSubject;
    private DevelopCombination developCombination;
    private EppEduPlanVersion eduPlanVersion;
    private EppEduPlanVersionBlock eduPlanVersionBlock;


    private Integer selectedYearNumber = null;

    public void setSelectedYear(final Object selectedYear) {
        this.selectedYearNumber = EppFilterUtil.getSelectedYear(selectedYear);
    }

    public DevelopCombination getDevelopCombination()
    {
        return developCombination;
    }

    public void setDevelopCombination(DevelopCombination developCombination)
    {
        this.developCombination = developCombination;
    }

    public ISelectModel getDevelopCombinationModel()
    {
        return developCombinationModel;
    }

    public void setDevelopCombinationModel(ISelectModel developCombinationModel)
    {
        this.developCombinationModel = developCombinationModel;
    }

    public EppEduPlanVersion getEduPlanVersion()
    {
        return eduPlanVersion;
    }

    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        this.eduPlanVersion = eduPlanVersion;
    }

    public EppEduPlanVersionBlock getEduPlanVersionBlock()
    {
        return eduPlanVersionBlock;
    }

    public void setEduPlanVersionBlock(EppEduPlanVersionBlock eduPlanVersionBlock)
    {
        this.eduPlanVersionBlock = eduPlanVersionBlock;
    }

    public ISelectModel getEduPlanVersionBlockModel()
    {
        return eduPlanVersionBlockModel;
    }

    public void setEduPlanVersionBlockModel(ISelectModel eduPlanVersionBlockModel)
    {
        this.eduPlanVersionBlockModel = eduPlanVersionBlockModel;
    }

    public ISelectModel getEduPlanVersionModel()
    {
        return eduPlanVersionModel;
    }

    public void setEduPlanVersionModel(ISelectModel eduPlanVersionModel)
    {
        this.eduPlanVersionModel = eduPlanVersionModel;
    }

    public EduProgramSubject getProgramSubject()
    {
        return programSubject;
    }

    public void setProgramSubject(EduProgramSubject programSubject)
    {
        this.programSubject = programSubject;
    }

    public ISelectModel getProgramSubjectModel()
    {
        return programSubjectModel;
    }

    public void setProgramSubjectModel(ISelectModel programSubjectModel)
    {
        this.programSubjectModel = programSubjectModel;
    }

    public ISelectModel getTermListModel()
    {
        return termListModel;
    }

    public void setTermListModel(ISelectModel termListModel)
    {
        this.termListModel = termListModel;
    }

    public ISelectModel getYearEducationProcessListModel()
    {
        return yearEducationProcessListModel;
    }

    public void setYearEducationProcessListModel(ISelectModel yearEducationProcessListModel)
    {
        this.yearEducationProcessListModel = yearEducationProcessListModel;
    }

    public Integer getSelectedYearNumber()
    {
        return selectedYearNumber;
    }

    public void setSelectedYearNumber(Integer selectedYearNumber)
    {
        this.selectedYearNumber = selectedYearNumber;
    }

    public EduProgramKind getProgramKind()
    {
        return programKind;
    }

    public void setProgramKind(EduProgramKind programKind)
    {
        this.programKind = programKind;
    }

    public ISelectModel getProgramKindModel()
    {
        return programKindModel;
    }

    public void setProgramKindModel(ISelectModel programKindModel)
    {
        this.programKindModel = programKindModel;
    }

    public boolean isDpp()
    {
        return getProgramKind() != null && getProgramKind().getCode().equals(EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA);
    }

    public ISelectModel getCustomEduPlanModel()
    {
        return customEduPlanModel;
    }

    public void setCustomEduPlanModel(ISelectModel customEduPlanModel)
    {
        this.customEduPlanModel = customEduPlanModel;
    }
}
