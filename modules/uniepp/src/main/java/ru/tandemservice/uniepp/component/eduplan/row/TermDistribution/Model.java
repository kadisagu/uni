package ru.tandemservice.uniepp.component.eduplan.row.TermDistribution;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.IEppFullCodeCatalogItem;
import ru.tandemservice.uniepp.entity.plan.data.*;

import java.util.*;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="rowHolder.id", required=true)
})

public class Model {

    private final EntityHolder<EppEpvTermDistributedRow> rowHolder = new EntityHolder<EppEpvTermDistributedRow>();
    public EntityHolder<EppEpvTermDistributedRow> getRowHolder() { return this.rowHolder; }
    public EppEpvTermDistributedRow getRow() { return this.getRowHolder().getValue(); }
    public Long getId() { return this.getRowHolder().getId(); }

    public String getTitle() {
        return UniEppUtils.getEppEpvRowFormTitle(this.getId());
    }

    public boolean isFillSupportRow() {
        return (
        (this.getRow() instanceof EppEpvRegistryRow) &&
        (null != ((EppEpvRegistryRow)this.getRow()).getRegistryElement())
        );
    }

    private List<EppLoadType> loadTypeList = Collections.emptyList();
    public List<EppLoadType> getLoadTypeList() { return this.loadTypeList; }
    public void setLoadTypeList(final List<EppLoadType> loadTypeList) { this.loadTypeList = loadTypeList; }

    private List<EppControlActionType> controlActionTypeList = Collections.emptyList();
    public List<EppControlActionType> getControlActionTypeList() { return this.controlActionTypeList; }
    public void setControlActionTypeList(final List<EppControlActionType> controlActionTypeList) { this.controlActionTypeList = controlActionTypeList; }

    private IEppFullCodeCatalogItem current;
    public IEppFullCodeCatalogItem getCurrent() { return this.current; }
    public void setCurrent(final IEppFullCodeCatalogItem current) { this.current = current; }

    private final Map<EppEpvRowTerm, Map<String, Number>> termMap = new TreeMap<EppEpvRowTerm, Map<String, Number>>(new Comparator<EppEpvRowTerm>() {
        @Override public int compare(final EppEpvRowTerm o1, final EppEpvRowTerm o2) {
            return Integer.valueOf(o1.getTerm().getIntValue()).compareTo(o2.getTerm().getIntValue());
        }
    });
    public Map<EppEpvRowTerm, Map<String, Number>> getTermMap() { return this.termMap; }
    public Collection<Map.Entry<EppEpvRowTerm, Map<String, Number>>> getEntries() { return this.getTermMap().entrySet(); }

    private Map.Entry<EppEpvRowTerm, Map<String, Number>> entry;
    public Map.Entry<EppEpvRowTerm, Map<String, Number>> getEntry() { return this.entry; }
    public void setEntry(final Map.Entry<EppEpvRowTerm, Map<String, Number>> entry) { this.entry = entry; }

    public Term getTerm() { return this.getEntry().getKey().getTerm(); }

    public Double getSizeValue() {
        return this.getEntry().getKey().getHoursTotalAsDouble();
    }
    public void setSizeValue(final Double value) {
        this.getEntry().getKey().setHoursTotalAsDouble(value);
    }

    public Double getWeeksValue() {
        return this.getEntry().getKey().getWeeksAsDouble();
    }
    public void setWeeksValue(final Double value) {
        this.getEntry().getKey().setWeeksAsDouble(value);
    }

    public Double getLaborValue() {
        return this.getEntry().getKey().getLaborAsDouble();
    }
    public void setLaborValue(final Double value) {
        this.getEntry().getKey().setLaborAsDouble(value);
    }



    public Double getLoadValue() {
        return (Double)this.getEntry().getValue().get(this.getCurrent().getFullCode());
    }
    public void setLoadValue(final Double value) {
        this.getEntry().getValue().put(this.getCurrent().getFullCode(), value);
    }

    public Integer getValueAsInteger() {
        return (Integer)this.getEntry().getValue().get(this.getCurrent().getFullCode());
    }
    public void setValueAsInteger(final Integer value) {
        this.getEntry().getValue().put(this.getCurrent().getFullCode(), value);
    }

    public Boolean getValueAsBoolean() {
        final Integer value = this.getValueAsInteger();
        return (null == value ? null : value.intValue() > 0);
    }
    public void setValueAsBoolean(final Boolean value) {
        this.setValueAsInteger(null == value ? null : value.booleanValue() ? 1 : 0);
    }

    public boolean isFlagValue() {
        final EppEpvRow row = this.getRow();
        if ((row instanceof EppEpvGroupImRow) && (((EppEpvGroupImRow) row).getSize()>1)) {
            return false;
        }
        return ((EppControlActionType)this.getCurrent()).isFlagValue();
    }

    public static String getLoadInputId(final Integer term, final String fullCode) {
        return "load_"+term+"_"+fullCode.replace('.', '_');
    }
    public String getLoadInputId() {
        return Model.getLoadInputId(this.getEntry().getKey().getTerm().getIntValue(), this.getCurrent().getFullCode());
    }


    public boolean isShowTotalSize() {
        return this.getRow().isShowTotalSize();
    }

    public boolean isShowTotalWeeks() {
        return this.getRow().isShowTotalWeeks();
    }

    public boolean isShowTotalLabor() {
        return this.getRow().isShowTotalLabor();
    }
}
