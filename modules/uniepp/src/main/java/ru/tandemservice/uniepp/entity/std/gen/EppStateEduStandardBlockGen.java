package ru.tandemservice.uniepp.entity.std.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Государственный образовательный стандарт (блок направления подготовки)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStateEduStandardBlockGen extends EntityBase
 implements INaturalIdentifiable<EppStateEduStandardBlockGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock";
    public static final String ENTITY_NAME = "eppStateEduStandardBlock";
    public static final int VERSION_HASH = -372547719;
    private static IEntityMeta ENTITY_META;

    public static final String L_STATE_EDU_STANDARD = "stateEduStandard";
    public static final String L_PROGRAM_SPECIALIZATION = "programSpecialization";

    private EppStateEduStandard _stateEduStandard;     // Государственный образовательный стандарт
    private EduProgramSpecialization _programSpecialization;     // Направленность высшего профессионального образования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null.
     */
    @NotNull
    public EppStateEduStandard getStateEduStandard()
    {
        return _stateEduStandard;
    }

    /**
     * @param stateEduStandard Государственный образовательный стандарт. Свойство не может быть null.
     */
    public void setStateEduStandard(EppStateEduStandard stateEduStandard)
    {
        dirty(_stateEduStandard, stateEduStandard);
        _stateEduStandard = stateEduStandard;
    }

    /**
     * null - общий блок.
     *
     * @return Направленность высшего профессионального образования.
     */
    public EduProgramSpecialization getProgramSpecialization()
    {
        return _programSpecialization;
    }

    /**
     * @param programSpecialization Направленность высшего профессионального образования.
     */
    public void setProgramSpecialization(EduProgramSpecialization programSpecialization)
    {
        dirty(_programSpecialization, programSpecialization);
        _programSpecialization = programSpecialization;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppStateEduStandardBlockGen)
        {
            if (withNaturalIdProperties)
            {
                setStateEduStandard(((EppStateEduStandardBlock)another).getStateEduStandard());
                setProgramSpecialization(((EppStateEduStandardBlock)another).getProgramSpecialization());
            }
        }
    }

    public INaturalId<EppStateEduStandardBlockGen> getNaturalId()
    {
        return new NaturalId(getStateEduStandard(), getProgramSpecialization());
    }

    public static class NaturalId extends NaturalIdBase<EppStateEduStandardBlockGen>
    {
        private static final String PROXY_NAME = "EppStateEduStandardBlockNaturalProxy";

        private Long _stateEduStandard;
        private Long _programSpecialization;

        public NaturalId()
        {}

        public NaturalId(EppStateEduStandard stateEduStandard, EduProgramSpecialization programSpecialization)
        {
            _stateEduStandard = ((IEntity) stateEduStandard).getId();
            _programSpecialization = programSpecialization==null ? null : ((IEntity) programSpecialization).getId();
        }

        public Long getStateEduStandard()
        {
            return _stateEduStandard;
        }

        public void setStateEduStandard(Long stateEduStandard)
        {
            _stateEduStandard = stateEduStandard;
        }

        public Long getProgramSpecialization()
        {
            return _programSpecialization;
        }

        public void setProgramSpecialization(Long programSpecialization)
        {
            _programSpecialization = programSpecialization;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppStateEduStandardBlockGen.NaturalId) ) return false;

            EppStateEduStandardBlockGen.NaturalId that = (NaturalId) o;

            if( !equals(getStateEduStandard(), that.getStateEduStandard()) ) return false;
            if( !equals(getProgramSpecialization(), that.getProgramSpecialization()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStateEduStandard());
            result = hashCode(result, getProgramSpecialization());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStateEduStandard());
            sb.append("/");
            sb.append(getProgramSpecialization());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStateEduStandardBlockGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStateEduStandardBlock.class;
        }

        public T newInstance()
        {
            return (T) new EppStateEduStandardBlock();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "stateEduStandard":
                    return obj.getStateEduStandard();
                case "programSpecialization":
                    return obj.getProgramSpecialization();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "stateEduStandard":
                    obj.setStateEduStandard((EppStateEduStandard) value);
                    return;
                case "programSpecialization":
                    obj.setProgramSpecialization((EduProgramSpecialization) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "stateEduStandard":
                        return true;
                case "programSpecialization":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "stateEduStandard":
                    return true;
                case "programSpecialization":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "stateEduStandard":
                    return EppStateEduStandard.class;
                case "programSpecialization":
                    return EduProgramSpecialization.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStateEduStandardBlock> _dslPath = new Path<EppStateEduStandardBlock>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStateEduStandardBlock");
    }
            

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock#getStateEduStandard()
     */
    public static EppStateEduStandard.Path<EppStateEduStandard> stateEduStandard()
    {
        return _dslPath.stateEduStandard();
    }

    /**
     * null - общий блок.
     *
     * @return Направленность высшего профессионального образования.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock#getProgramSpecialization()
     */
    public static EduProgramSpecialization.Path<EduProgramSpecialization> programSpecialization()
    {
        return _dslPath.programSpecialization();
    }

    public static class Path<E extends EppStateEduStandardBlock> extends EntityPath<E>
    {
        private EppStateEduStandard.Path<EppStateEduStandard> _stateEduStandard;
        private EduProgramSpecialization.Path<EduProgramSpecialization> _programSpecialization;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock#getStateEduStandard()
     */
        public EppStateEduStandard.Path<EppStateEduStandard> stateEduStandard()
        {
            if(_stateEduStandard == null )
                _stateEduStandard = new EppStateEduStandard.Path<EppStateEduStandard>(L_STATE_EDU_STANDARD, this);
            return _stateEduStandard;
        }

    /**
     * null - общий блок.
     *
     * @return Направленность высшего профессионального образования.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock#getProgramSpecialization()
     */
        public EduProgramSpecialization.Path<EduProgramSpecialization> programSpecialization()
        {
            if(_programSpecialization == null )
                _programSpecialization = new EduProgramSpecialization.Path<EduProgramSpecialization>(L_PROGRAM_SPECIALIZATION, this);
            return _programSpecialization;
        }

        public Class getEntityClass()
        {
            return EppStateEduStandardBlock.class;
        }

        public String getEntityName()
        {
            return "eppStateEduStandardBlock";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
