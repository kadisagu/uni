package ru.tandemservice.uniepp.dao.student.io;

import com.healthmarketscience.jackcess.Database;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.util.Collection;

/**
 * @author vdanilov
 */
public interface IEppStudentIODao {

    SpringBeanCache<IEppStudentIODao> instance = new SpringBeanCache<IEppStudentIODao>(IEppStudentIODao.class.getName());

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void export_Student2EduplanVersionRelations(Database mdb, Collection<Long> studentIds) throws Exception;

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doImport_Student2EduPlanVersionRelations(Database mdb) throws Exception;

}
