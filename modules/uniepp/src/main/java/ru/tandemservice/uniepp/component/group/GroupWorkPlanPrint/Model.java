package ru.tandemservice.uniepp.component.group.GroupWorkPlanPrint;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

/**
 * @author vdanilov
 */
@Input( { @Bind(key = "ids", binding = "ids") })
public class Model {

    private Collection<Long> ids = Collections.emptyList();
    public Collection<Long> getIds() { return this.ids; }
    public void setIds(final Collection<Long> ids) { this.ids = ids; }

    /* { group.id -> { student.id -> { term.id -> relation }}} */
    private final Map<Long, Map<Long, Map<Integer, EppStudent2WorkPlan>>> dataMap = new HashMap<Long, Map<Long, Map<Integer, EppStudent2WorkPlan>>>();
    public Map<Long, Map<Long, Map<Integer, EppStudent2WorkPlan>>> getDataMap() { return this.dataMap; }

    private final SelectModel<EducationYear> yearModel = new SelectModel<EducationYear>();
    public SelectModel<EducationYear> getYearModel() { return this.yearModel; }

    private final SelectModel<YearDistributionPart> partModel = new SelectModel<YearDistributionPart>();
    public SelectModel<YearDistributionPart> getPartModel() { return this.partModel; }

}
