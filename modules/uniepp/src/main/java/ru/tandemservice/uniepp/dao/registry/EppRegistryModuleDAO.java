package ru.tandemservice.uniepp.dao.registry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.functors.NotNullPredicate;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction;

/**
 * @author vdanilov
 */
public class EppRegistryModuleDAO extends UniBaseDao implements IEppRegistryModuleDAO {

    @Override
    @SuppressWarnings("unchecked")
    public INumberGenerationRule<EppRegistryModule> getNumberGenerationRule() {
        return new SimpleNumberGenerationRule<EppRegistryModule>() {
            @Override public Set<String> getUsedNumbers(final EppRegistryModule object) {
                return new HashSet<String>(
                new DQLSelectBuilder().fromEntity(EppRegistryModule.class, "x")
                .column(property(EppRegistryModule.number().fromAlias("x")))
                .createStatement(EppRegistryModuleDAO.this.getSession()).<String>list()
                );
            }
            @Override public String getNumberQueueName(final EppRegistryModule object) {
                return EppRegistryModule.class.getSimpleName();
            }
        };
    }

    @Override
    public void doSaveRegistryModule(final EppRegistryModule element) {
        this.lock(element).saveOrUpdate(element);
    }

    @Override
    public Collection<EppRegistryModuleALoad> doSaveRegistryModuleLoad(final EppRegistryModule element, final Collection<EppRegistryModuleALoad> loadList) {
        this.lock(element);
        return new MergeAction.SessionMergeAction<EppLoadType, EppRegistryModuleALoad>() {
            @Override protected boolean isPossible(final EppRegistryModuleALoad source) { return source.getLoadAsDouble() > 0; }
            @Override protected EppLoadType key(final EppRegistryModuleALoad source) { return source.getLoadType(); }
            @Override protected void fill(final EppRegistryModuleALoad target, final EppRegistryModuleALoad source) { target.update(source, false); }
            @Override protected EppRegistryModuleALoad buildRow(final EppRegistryModuleALoad source) { return new EppRegistryModuleALoad(element, source.getLoadType()); }
        }.merge(
            this.getList(EppRegistryModuleALoad.class, EppRegistryModuleALoad.module(), element),
            CollectionUtils.select(loadList, NotNullPredicate.getInstance())
        );
    }

    @Override
    public Collection<EppRegistryModuleIControlAction> doSaveRegistryModuleControl(final EppRegistryModule element, final Collection<EppRegistryModuleIControlAction> controlActionList) {
        this.lock(element);
        return new MergeAction.SessionMergeAction<EppIControlActionType, EppRegistryModuleIControlAction>() {
            @Override protected boolean isPossible(final EppRegistryModuleIControlAction source) { return source.getAmount() > 0; }
            @Override protected EppIControlActionType key(final EppRegistryModuleIControlAction source) { return source.getControlAction(); }
            @Override protected void fill(final EppRegistryModuleIControlAction target, final EppRegistryModuleIControlAction source) { target.update(source, false); }
            @Override protected EppRegistryModuleIControlAction buildRow(final EppRegistryModuleIControlAction source) { return new EppRegistryModuleIControlAction(element, source.getControlAction()); }
        }.merge(
            this.getList(EppRegistryModuleIControlAction.class, EppRegistryModuleIControlAction.module(), element),
            CollectionUtils.select(controlActionList, NotNullPredicate.getInstance())
        );
    }


    public static String getDefaultModuleNumber(EppRegistryElementPart relp) {
        return relp.getRegistryElement().getNumber()+"-"+relp.getNumber();
    }

    public static String getDefaultModuleTitle(EppRegistryElementPart relp) {
        return relp.getRegistryElement().getTitle() + " (№ "+relp.getRegistryElement().getNumber()+", "+relp.getNumber()+"/"+relp.getRegistryElement().getParts()+")";
    }


}
