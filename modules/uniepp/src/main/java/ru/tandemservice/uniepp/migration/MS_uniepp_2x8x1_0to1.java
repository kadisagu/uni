package ru.tandemservice.uniepp.migration;

import com.google.common.collect.Maps;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x8x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Сущность eppDebitorsPayReport переименована в eduCtrDebitorsPayReport
        {
            tool.renameTable("epp_debitors_pay_report_t", "edu_ctr_debitors_pay_report_t");
            tool.entityCodes().delete("eduCtrDebitorsPayReport"); // TF-985
            tool.entityCodes().rename("eppDebitorsPayReport", "eduCtrDebitorsPayReport");
        }

        Map<String, String> orgUnitPermissionKeyMap = Maps.newHashMap();
        orgUnitPermissionKeyMap.put("orgUnit_viewEppDebitorsPayReport_", "orgUnit_viewEduCtrDebitorsPayReport_");

        Map<String, String> permissionKeyMap = Maps.newHashMap();
        permissionKeyMap.put("eppDebitorsPayReport", "eduCtrDebitorsPayReport");

        PreparedStatement update = tool.prepareStatement("update accessmatrix_t set permissionkey_p = ? where permissionkey_p = ?");

        Statement stmt = tool.getConnection().createStatement();
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();
        stmt.execute(translator.toSql(new SQLSelectQuery().from(SQLFrom.table("orgunittype_t", "outype")).column("outype.code_p", "code").distinct()));
        ResultSet src = stmt.getResultSet();

        // заменить необходимые вхождения
        while (src.next())
        {
            String code = src.getString("code");
            for (Map.Entry<String, String> entry: orgUnitPermissionKeyMap.entrySet())
            {
                String newCode = entry.getValue() + code;
                String oldCode = entry.getKey() + code;

                deleteKey(tool, newCode, oldCode);
                update.setString(1, newCode);
                update.setString(2, oldCode);
                update.execute();
            }
        }

        for (Map.Entry<String, String> entry: permissionKeyMap.entrySet())
        {
            String newCode = entry.getValue();
            String oldCode = entry.getKey();

            deleteKey(tool, newCode, oldCode);
            update.setString(1, newCode);
            update.setString(2, oldCode);
            update.execute();
        }
    }

    private void deleteKey(DBTool tool, String newCode, String oldCode) throws Exception
    {
        PreparedStatement stmt = tool.prepareStatement("select role_id from accessmatrix_t where permissionkey_p = ?", oldCode);
        stmt.execute();
        ResultSet src = stmt.getResultSet();

        while (src.next())
        {
            String roleId = src.getString("role_id");
            Long newId = (Long) tool.getUniqueResult("select id from accessmatrix_t where role_id = " + roleId + " and permissionkey_p = ?", newCode);
            if (null != newId)
                tool.executeUpdate("delete from accessmatrix_t where id = ?", newId);
        }
    }
}