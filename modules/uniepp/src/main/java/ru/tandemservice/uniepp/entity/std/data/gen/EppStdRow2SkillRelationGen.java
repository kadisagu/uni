package ru.tandemservice.uniepp.entity.std.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь строки ГОС и компетенции
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStdRow2SkillRelationGen extends EntityBase
 implements INaturalIdentifiable<EppStdRow2SkillRelationGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation";
    public static final String ENTITY_NAME = "eppStdRow2SkillRelation";
    public static final int VERSION_HASH = -1361743832;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW = "row";
    public static final String L_SKILL = "skill";

    private EppStdRow _row;     // Запись ГОС (базовая)
    private EppStateEduStandardSkill _skill;     // Государственный образовательный стандарт (компетенция)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись ГОС (базовая). Свойство не может быть null.
     */
    @NotNull
    public EppStdRow getRow()
    {
        return _row;
    }

    /**
     * @param row Запись ГОС (базовая). Свойство не может быть null.
     */
    public void setRow(EppStdRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Государственный образовательный стандарт (компетенция). Свойство не может быть null.
     */
    @NotNull
    public EppStateEduStandardSkill getSkill()
    {
        return _skill;
    }

    /**
     * @param skill Государственный образовательный стандарт (компетенция). Свойство не может быть null.
     */
    public void setSkill(EppStateEduStandardSkill skill)
    {
        dirty(_skill, skill);
        _skill = skill;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppStdRow2SkillRelationGen)
        {
            if (withNaturalIdProperties)
            {
                setRow(((EppStdRow2SkillRelation)another).getRow());
                setSkill(((EppStdRow2SkillRelation)another).getSkill());
            }
        }
    }

    public INaturalId<EppStdRow2SkillRelationGen> getNaturalId()
    {
        return new NaturalId(getRow(), getSkill());
    }

    public static class NaturalId extends NaturalIdBase<EppStdRow2SkillRelationGen>
    {
        private static final String PROXY_NAME = "EppStdRow2SkillRelationNaturalProxy";

        private Long _row;
        private Long _skill;

        public NaturalId()
        {}

        public NaturalId(EppStdRow row, EppStateEduStandardSkill skill)
        {
            _row = ((IEntity) row).getId();
            _skill = ((IEntity) skill).getId();
        }

        public Long getRow()
        {
            return _row;
        }

        public void setRow(Long row)
        {
            _row = row;
        }

        public Long getSkill()
        {
            return _skill;
        }

        public void setSkill(Long skill)
        {
            _skill = skill;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppStdRow2SkillRelationGen.NaturalId) ) return false;

            EppStdRow2SkillRelationGen.NaturalId that = (NaturalId) o;

            if( !equals(getRow(), that.getRow()) ) return false;
            if( !equals(getSkill(), that.getSkill()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRow());
            result = hashCode(result, getSkill());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRow());
            sb.append("/");
            sb.append(getSkill());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStdRow2SkillRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStdRow2SkillRelation.class;
        }

        public T newInstance()
        {
            return (T) new EppStdRow2SkillRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "row":
                    return obj.getRow();
                case "skill":
                    return obj.getSkill();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "row":
                    obj.setRow((EppStdRow) value);
                    return;
                case "skill":
                    obj.setSkill((EppStateEduStandardSkill) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "row":
                        return true;
                case "skill":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "row":
                    return true;
                case "skill":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "row":
                    return EppStdRow.class;
                case "skill":
                    return EppStateEduStandardSkill.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStdRow2SkillRelation> _dslPath = new Path<EppStdRow2SkillRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStdRow2SkillRelation");
    }
            

    /**
     * @return Запись ГОС (базовая). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation#getRow()
     */
    public static EppStdRow.Path<EppStdRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Государственный образовательный стандарт (компетенция). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation#getSkill()
     */
    public static EppStateEduStandardSkill.Path<EppStateEduStandardSkill> skill()
    {
        return _dslPath.skill();
    }

    public static class Path<E extends EppStdRow2SkillRelation> extends EntityPath<E>
    {
        private EppStdRow.Path<EppStdRow> _row;
        private EppStateEduStandardSkill.Path<EppStateEduStandardSkill> _skill;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись ГОС (базовая). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation#getRow()
     */
        public EppStdRow.Path<EppStdRow> row()
        {
            if(_row == null )
                _row = new EppStdRow.Path<EppStdRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Государственный образовательный стандарт (компетенция). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation#getSkill()
     */
        public EppStateEduStandardSkill.Path<EppStateEduStandardSkill> skill()
        {
            if(_skill == null )
                _skill = new EppStateEduStandardSkill.Path<EppStateEduStandardSkill>(L_SKILL, this);
            return _skill;
        }

        public Class getEntityClass()
        {
            return EppStdRow2SkillRelation.class;
        }

        public String getEntityName()
        {
            return "eppStdRow2SkillRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
