package ru.tandemservice.uniepp.entity.plan.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Семестр строки УП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvRowTermGen extends EntityBase
 implements INaturalIdentifiable<EppEpvRowTermGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm";
    public static final String ENTITY_NAME = "eppEpvRowTerm";
    public static final int VERSION_HASH = -251184699;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW = "row";
    public static final String L_TERM = "term";
    public static final String P_HOURS_TOTAL = "hoursTotal";
    public static final String P_LABOR = "labor";
    public static final String P_WEEKS = "weeks";
    public static final String P_HOURS_AUDIT = "hoursAudit";
    public static final String P_HOURS_SELFWORK = "hoursSelfwork";
    public static final String P_HOURS_CONTROL = "hoursControl";
    public static final String P_HOURS_CONTROL_E = "hoursControlE";

    private EppEpvTermDistributedRow _row;     // Строка УП (с нагрузкой)
    private Term _term;     // Семестр
    private long _hoursTotal = -1l;     // Всего часов (в семестр)
    private long _labor = -1l;     // Всего трудоемкость (в семестр)
    private long _weeks = -1l;     // Всего недель (в семестр)
    private long _hoursAudit = -1l;     // Аудиторных часов
    private long _hoursSelfwork = -1l;     // Часов самостоятельной работы
    private long _hoursControl = -1l;     // Часов на контроль (экзамен)
    private long _hoursControlE = -1l;     // Часов на контроль в эл. форме

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка УП (с нагрузкой). Свойство не может быть null.
     */
    @NotNull
    public EppEpvTermDistributedRow getRow()
    {
        return _row;
    }

    /**
     * @param row Строка УП (с нагрузкой). Свойство не может быть null.
     */
    public void setRow(EppEpvTermDistributedRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Семестр. Свойство не может быть null.
     */
    @NotNull
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр. Свойство не может быть null.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Всего часов (в семестр). Свойство не может быть null.
     */
    @NotNull
    public long getHoursTotal()
    {
        return _hoursTotal;
    }

    /**
     * @param hoursTotal Всего часов (в семестр). Свойство не может быть null.
     */
    public void setHoursTotal(long hoursTotal)
    {
        dirty(_hoursTotal, hoursTotal);
        _hoursTotal = hoursTotal;
    }

    /**
     * @return Всего трудоемкость (в семестр). Свойство не может быть null.
     */
    @NotNull
    public long getLabor()
    {
        return _labor;
    }

    /**
     * @param labor Всего трудоемкость (в семестр). Свойство не может быть null.
     */
    public void setLabor(long labor)
    {
        dirty(_labor, labor);
        _labor = labor;
    }

    /**
     * @return Всего недель (в семестр). Свойство не может быть null.
     */
    @NotNull
    public long getWeeks()
    {
        return _weeks;
    }

    /**
     * @param weeks Всего недель (в семестр). Свойство не может быть null.
     */
    public void setWeeks(long weeks)
    {
        dirty(_weeks, weeks);
        _weeks = weeks;
    }

    /**
     * @return Аудиторных часов. Свойство не может быть null.
     */
    @NotNull
    public long getHoursAudit()
    {
        return _hoursAudit;
    }

    /**
     * @param hoursAudit Аудиторных часов. Свойство не может быть null.
     */
    public void setHoursAudit(long hoursAudit)
    {
        dirty(_hoursAudit, hoursAudit);
        _hoursAudit = hoursAudit;
    }

    /**
     * @return Часов самостоятельной работы. Свойство не может быть null.
     */
    @NotNull
    public long getHoursSelfwork()
    {
        return _hoursSelfwork;
    }

    /**
     * @param hoursSelfwork Часов самостоятельной работы. Свойство не может быть null.
     */
    public void setHoursSelfwork(long hoursSelfwork)
    {
        dirty(_hoursSelfwork, hoursSelfwork);
        _hoursSelfwork = hoursSelfwork;
    }

    /**
     * @return Часов на контроль (экзамен). Свойство не может быть null.
     */
    @NotNull
    public long getHoursControl()
    {
        return _hoursControl;
    }

    /**
     * @param hoursControl Часов на контроль (экзамен). Свойство не может быть null.
     */
    public void setHoursControl(long hoursControl)
    {
        dirty(_hoursControl, hoursControl);
        _hoursControl = hoursControl;
    }

    /**
     * @return Часов на контроль в эл. форме. Свойство не может быть null.
     */
    @NotNull
    public long getHoursControlE()
    {
        return _hoursControlE;
    }

    /**
     * @param hoursControlE Часов на контроль в эл. форме. Свойство не может быть null.
     */
    public void setHoursControlE(long hoursControlE)
    {
        dirty(_hoursControlE, hoursControlE);
        _hoursControlE = hoursControlE;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEpvRowTermGen)
        {
            if (withNaturalIdProperties)
            {
                setRow(((EppEpvRowTerm)another).getRow());
                setTerm(((EppEpvRowTerm)another).getTerm());
            }
            setHoursTotal(((EppEpvRowTerm)another).getHoursTotal());
            setLabor(((EppEpvRowTerm)another).getLabor());
            setWeeks(((EppEpvRowTerm)another).getWeeks());
            setHoursAudit(((EppEpvRowTerm)another).getHoursAudit());
            setHoursSelfwork(((EppEpvRowTerm)another).getHoursSelfwork());
            setHoursControl(((EppEpvRowTerm)another).getHoursControl());
            setHoursControlE(((EppEpvRowTerm)another).getHoursControlE());
        }
    }

    public INaturalId<EppEpvRowTermGen> getNaturalId()
    {
        return new NaturalId(getRow(), getTerm());
    }

    public static class NaturalId extends NaturalIdBase<EppEpvRowTermGen>
    {
        private static final String PROXY_NAME = "EppEpvRowTermNaturalProxy";

        private Long _row;
        private Long _term;

        public NaturalId()
        {}

        public NaturalId(EppEpvTermDistributedRow row, Term term)
        {
            _row = ((IEntity) row).getId();
            _term = ((IEntity) term).getId();
        }

        public Long getRow()
        {
            return _row;
        }

        public void setRow(Long row)
        {
            _row = row;
        }

        public Long getTerm()
        {
            return _term;
        }

        public void setTerm(Long term)
        {
            _term = term;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppEpvRowTermGen.NaturalId) ) return false;

            EppEpvRowTermGen.NaturalId that = (NaturalId) o;

            if( !equals(getRow(), that.getRow()) ) return false;
            if( !equals(getTerm(), that.getTerm()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRow());
            result = hashCode(result, getTerm());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRow());
            sb.append("/");
            sb.append(getTerm());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvRowTermGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvRowTerm.class;
        }

        public T newInstance()
        {
            return (T) new EppEpvRowTerm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "row":
                    return obj.getRow();
                case "term":
                    return obj.getTerm();
                case "hoursTotal":
                    return obj.getHoursTotal();
                case "labor":
                    return obj.getLabor();
                case "weeks":
                    return obj.getWeeks();
                case "hoursAudit":
                    return obj.getHoursAudit();
                case "hoursSelfwork":
                    return obj.getHoursSelfwork();
                case "hoursControl":
                    return obj.getHoursControl();
                case "hoursControlE":
                    return obj.getHoursControlE();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "row":
                    obj.setRow((EppEpvTermDistributedRow) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "hoursTotal":
                    obj.setHoursTotal((Long) value);
                    return;
                case "labor":
                    obj.setLabor((Long) value);
                    return;
                case "weeks":
                    obj.setWeeks((Long) value);
                    return;
                case "hoursAudit":
                    obj.setHoursAudit((Long) value);
                    return;
                case "hoursSelfwork":
                    obj.setHoursSelfwork((Long) value);
                    return;
                case "hoursControl":
                    obj.setHoursControl((Long) value);
                    return;
                case "hoursControlE":
                    obj.setHoursControlE((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "row":
                        return true;
                case "term":
                        return true;
                case "hoursTotal":
                        return true;
                case "labor":
                        return true;
                case "weeks":
                        return true;
                case "hoursAudit":
                        return true;
                case "hoursSelfwork":
                        return true;
                case "hoursControl":
                        return true;
                case "hoursControlE":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "row":
                    return true;
                case "term":
                    return true;
                case "hoursTotal":
                    return true;
                case "labor":
                    return true;
                case "weeks":
                    return true;
                case "hoursAudit":
                    return true;
                case "hoursSelfwork":
                    return true;
                case "hoursControl":
                    return true;
                case "hoursControlE":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "row":
                    return EppEpvTermDistributedRow.class;
                case "term":
                    return Term.class;
                case "hoursTotal":
                    return Long.class;
                case "labor":
                    return Long.class;
                case "weeks":
                    return Long.class;
                case "hoursAudit":
                    return Long.class;
                case "hoursSelfwork":
                    return Long.class;
                case "hoursControl":
                    return Long.class;
                case "hoursControlE":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvRowTerm> _dslPath = new Path<EppEpvRowTerm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvRowTerm");
    }
            

    /**
     * @return Строка УП (с нагрузкой). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getRow()
     */
    public static EppEpvTermDistributedRow.Path<EppEpvTermDistributedRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Всего часов (в семестр). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getHoursTotal()
     */
    public static PropertyPath<Long> hoursTotal()
    {
        return _dslPath.hoursTotal();
    }

    /**
     * @return Всего трудоемкость (в семестр). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getLabor()
     */
    public static PropertyPath<Long> labor()
    {
        return _dslPath.labor();
    }

    /**
     * @return Всего недель (в семестр). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getWeeks()
     */
    public static PropertyPath<Long> weeks()
    {
        return _dslPath.weeks();
    }

    /**
     * @return Аудиторных часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getHoursAudit()
     */
    public static PropertyPath<Long> hoursAudit()
    {
        return _dslPath.hoursAudit();
    }

    /**
     * @return Часов самостоятельной работы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getHoursSelfwork()
     */
    public static PropertyPath<Long> hoursSelfwork()
    {
        return _dslPath.hoursSelfwork();
    }

    /**
     * @return Часов на контроль (экзамен). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getHoursControl()
     */
    public static PropertyPath<Long> hoursControl()
    {
        return _dslPath.hoursControl();
    }

    /**
     * @return Часов на контроль в эл. форме. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getHoursControlE()
     */
    public static PropertyPath<Long> hoursControlE()
    {
        return _dslPath.hoursControlE();
    }

    public static class Path<E extends EppEpvRowTerm> extends EntityPath<E>
    {
        private EppEpvTermDistributedRow.Path<EppEpvTermDistributedRow> _row;
        private Term.Path<Term> _term;
        private PropertyPath<Long> _hoursTotal;
        private PropertyPath<Long> _labor;
        private PropertyPath<Long> _weeks;
        private PropertyPath<Long> _hoursAudit;
        private PropertyPath<Long> _hoursSelfwork;
        private PropertyPath<Long> _hoursControl;
        private PropertyPath<Long> _hoursControlE;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка УП (с нагрузкой). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getRow()
     */
        public EppEpvTermDistributedRow.Path<EppEpvTermDistributedRow> row()
        {
            if(_row == null )
                _row = new EppEpvTermDistributedRow.Path<EppEpvTermDistributedRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * @return Всего часов (в семестр). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getHoursTotal()
     */
        public PropertyPath<Long> hoursTotal()
        {
            if(_hoursTotal == null )
                _hoursTotal = new PropertyPath<Long>(EppEpvRowTermGen.P_HOURS_TOTAL, this);
            return _hoursTotal;
        }

    /**
     * @return Всего трудоемкость (в семестр). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getLabor()
     */
        public PropertyPath<Long> labor()
        {
            if(_labor == null )
                _labor = new PropertyPath<Long>(EppEpvRowTermGen.P_LABOR, this);
            return _labor;
        }

    /**
     * @return Всего недель (в семестр). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getWeeks()
     */
        public PropertyPath<Long> weeks()
        {
            if(_weeks == null )
                _weeks = new PropertyPath<Long>(EppEpvRowTermGen.P_WEEKS, this);
            return _weeks;
        }

    /**
     * @return Аудиторных часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getHoursAudit()
     */
        public PropertyPath<Long> hoursAudit()
        {
            if(_hoursAudit == null )
                _hoursAudit = new PropertyPath<Long>(EppEpvRowTermGen.P_HOURS_AUDIT, this);
            return _hoursAudit;
        }

    /**
     * @return Часов самостоятельной работы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getHoursSelfwork()
     */
        public PropertyPath<Long> hoursSelfwork()
        {
            if(_hoursSelfwork == null )
                _hoursSelfwork = new PropertyPath<Long>(EppEpvRowTermGen.P_HOURS_SELFWORK, this);
            return _hoursSelfwork;
        }

    /**
     * @return Часов на контроль (экзамен). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getHoursControl()
     */
        public PropertyPath<Long> hoursControl()
        {
            if(_hoursControl == null )
                _hoursControl = new PropertyPath<Long>(EppEpvRowTermGen.P_HOURS_CONTROL, this);
            return _hoursControl;
        }

    /**
     * @return Часов на контроль в эл. форме. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm#getHoursControlE()
     */
        public PropertyPath<Long> hoursControlE()
        {
            if(_hoursControlE == null )
                _hoursControlE = new PropertyPath<Long>(EppEpvRowTermGen.P_HOURS_CONTROL_E, this);
            return _hoursControlE;
        }

        public Class getEntityClass()
        {
            return EppEpvRowTerm.class;
        }

        public String getEntityName()
        {
            return "eppEpvRowTerm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
