package ru.tandemservice.uniepp.component.registry.PracticeRegistry.Pub;

import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;

/**
 * 
 * @author nkokorina
 *
 */

public interface IDAO extends ru.tandemservice.uniepp.component.registry.base.Pub.IDAO<EppRegistryPractice>
{

}
