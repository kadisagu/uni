/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppSettings.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uniepp.base.bo.EppSettings.logic.EppSettingsWrapper;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;

/**
 * @author Nikolay Fedorovskih
 * @since 16.01.2015
 */
public class EppSettingsEditUI extends UIPresenter
{
    private EppSettingsWrapper _wrapper;

    @Override
    public void onComponentRefresh()
    {
        _wrapper = new EppSettingsWrapper(false);
    }

    @Override
    public void saveSettings()
    {
        IEppSettingsDAO.instance.get().saveGlobalSettings(getWrapper());
    }

    public EppSettingsWrapper getWrapper()
    {
        return _wrapper;
    }
}