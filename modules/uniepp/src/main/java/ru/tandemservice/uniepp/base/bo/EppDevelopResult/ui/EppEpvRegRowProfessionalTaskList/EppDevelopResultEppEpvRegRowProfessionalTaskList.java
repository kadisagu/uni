/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.EppEpvRegRowProfessionalTaskList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.EppDevelopResultManager;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowProfessionalTask;

/**
 * @author Igor Belanov
 * @since 27.03.2017
 */
@Configuration
public class EppDevelopResultEppEpvRegRowProfessionalTaskList extends BusinessComponentManager
{
    public static final String EPP_EPV_REG_ROW_PROFESSIONAL_TASK_DS = "eppEpvRegRowProfessionalTaskDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(EPP_EPV_REG_ROW_PROFESSIONAL_TASK_DS, eppEpvRegRowProfessionalTaskCL(), EppDevelopResultManager.instance().eppEpvRegRowProfessionalTaskSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eppEpvRegRowProfessionalTaskCL()
    {
        final IMergeRowIdResolver profTaskMerge = entity -> {
            EppEpvRegRowProfessionalTask eppEpvRegRowProfessionalTask = ((EppEpvRegRowProfessionalTask) entity);
            return String.valueOf(eppEpvRegRowProfessionalTask.getProfessionalTask().getId());
        };
        final IMergeRowIdResolver EPVBlock = entity -> {
            EppEpvRegRowProfessionalTask eppEpvRegRowProfessionalTask = ((EppEpvRegRowProfessionalTask) entity);
            return String.valueOf(eppEpvRegRowProfessionalTask.getEpvRegistryRow().getOwner().getId()) +
                    String.valueOf(eppEpvRegRowProfessionalTask.getProfessionalTask().getId());
        };
        return columnListExtPointBuilder(EPP_EPV_REG_ROW_PROFESSIONAL_TASK_DS)
                .addColumn(textColumn("EppProfessionalTask", EppEpvRegRowProfessionalTask.professionalTask().title()).merger(profTaskMerge))
                .addColumn(textColumn("EppEPVBlock", EppEpvRegRowProfessionalTask.epvRegistryRow().owner().title()).merger(EPVBlock))
                .addColumn(textColumn("EppEpvRegRow", EppEpvRegRowProfessionalTask.epvRegistryRow().titleWithIndex()))
                .create();
    }
}
