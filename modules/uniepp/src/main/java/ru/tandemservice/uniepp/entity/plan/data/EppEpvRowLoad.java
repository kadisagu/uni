package ru.tandemservice.uniepp.entity.plan.data;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvRowLoadGen;

/**
 * итоговая нагрузка строки УП (по видам)
 */
public class EppEpvRowLoad extends EppEpvRowLoadGen
{
    public EppEpvRowLoad() {}

    public EppEpvRowLoad(EppEpvTermDistributedRow row, EppALoadType loadType) {
        this.setRow(row);
        this.setLoadType(loadType);
    }

    public EppEpvRowLoad(EppEpvTermDistributedRow row, EppALoadType loadType, Double value) {
        this(row, loadType);
        this.setHoursAsDouble(value);
    }


    public static Double wrap(final long load) {
        return UniEppUtils.wrap(load < 0 ? null : load);
    }
    public static long unwrap(final Double value) {
        final Long load = UniEppUtils.unwrap(value);
        return null == load ? -1 : load.longValue();
    }

    @Deprecated
    public Double getLoadAsDouble() {
        return getHoursAsDouble();
    }

    @Deprecated
    public void setLoadAsDouble(final Double value) {
        this.setHoursAsDouble(value);
    }

    @Override
    @EntityDSLSupport(parts={EppEpvRowLoad.P_HOURS})
    public Double getHoursAsDouble() {
        return wrap(this.getHours());
    }

    public void setHoursAsDouble(final Double value) {
        this.setHours(unwrap(value));
    }
}