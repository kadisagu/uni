package ru.tandemservice.uniepp.entity.registry;

import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.registry.gen.EppEduGroupSplitByLangElementGen;

/** @see ru.tandemservice.uniepp.entity.registry.gen.EppEduGroupSplitByLangElementGen */
public class EppEduGroupSplitByLangElement extends EppEduGroupSplitByLangElementGen
{
    public EppEduGroupSplitByLangElement()
    {
    }

    public EppEduGroupSplitByLangElement(EppEduGroupSplitVariant splitVariant, ForeignLanguage foreignLanguage)
    {
        setSplitVariant(splitVariant);
        setForeignLanguage(foreignLanguage);
    }

    @Override
    public Long getSplitElementId()
    {
        return getForeignLanguage().getId();
    }

    @Override
    public String getSplitElementShortTitle()
    {
        return getForeignLanguage().getShortTitle();
    }

    @Override
    public String getSplitElementTitle()
    {
        return getForeignLanguage().getTitle();
    }
}