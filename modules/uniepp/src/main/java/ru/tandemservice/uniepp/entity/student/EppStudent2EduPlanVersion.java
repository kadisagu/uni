package ru.tandemservice.uniepp.entity.student;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.gen.EppStudent2EduPlanVersionGen;

/**
 * Учебный план студента
 */
public class EppStudent2EduPlanVersion extends EppStudent2EduPlanVersionGen implements ITitled
{
    public EppStudent2EduPlanVersion() {}
    public EppStudent2EduPlanVersion(final Student student, final EppEduPlanVersion eduPlanVersion) {
        this.setStudent(student);
        this.setEduPlanVersion(eduPlanVersion);
    }

    @Override
    public String getTitle()
    {
        if (getEduPlanVersion() == null) {
            return this.getClass().getSimpleName();
        }
        return "Учебный план " + getEduPlanVersion().getTitle() + " студента " + getStudent().getFio();
    }

    public OrgUnit getStudentCurrentGroupOu()
    {
        return getStudent().getEducationOrgUnit().getGroupOrgUnit();
    }
}