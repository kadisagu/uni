package ru.tandemservice.uniepp.component.group.GroupWorkPlanTab;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.BaseRawFormatter;
import org.tandemframework.core.view.formatter.PropertyFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.person.base.entity.gen.PersonGen;
import org.tandemframework.shared.person.base.entity.gen.PersonRoleGen;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.IStudentListModel;
import ru.tandemservice.uni.util.CustomStateUtil;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.gen.EppStudent2WorkPlanGen;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanGen;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanVersionGen;

import java.util.*;
import java.util.Map.Entry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * 
 * @author nkokorina
 */

public class DAO extends UniDao<Model> implements IDAO
{

    private static final SafeMap.Callback<Long, Map<Long, Map<Integer, EppWorkPlanBase>>> DATA_MAP_CALLBACK = key -> SafeMap.get(HashMap.class);

    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        model.getGroupHolder().refresh(Group.class);

        final Session session = this.getSession();

        // список студентов группы
        final MQBuilder builder = new MQBuilder(StudentGen.ENTITY_NAME, "s");
        builder.add(MQExpression.eq("s", StudentGen.P_ARCHIVAL, Boolean.FALSE));
        builder.add(MQExpression.eq("s", StudentGen.group().id().s(), model.getGroup().getId()));
        builder.addJoinFetch("s", PersonRoleGen.person().s(), "p");
        builder.addJoinFetch("p", PersonGen.identityCard().s(), "idc");

        IdentifiableWrapper status = model.getSettings().get("status");
        if (status != null) {
            builder.addJoinFetch("s", Student.L_STATUS, "studentStatus");
            builder.add(MQExpression.eq("studentStatus", StudentStatus.P_ACTIVE, status.getId().equals(IStudentListModel.STUDENT_STATUS_ACTIVE)));
        }
        List<StudentCustomStateCI> studentCustomStateCIList = model.getSettings().get("studentCustomStateCIs");
        CustomStateUtil.addCustomStatesFilter(builder, "s", studentCustomStateCIList);

        final List<Student> studentList = builder.getResultList(session);

        final Map<Long, EppStudent2EduPlanVersion> relationMap = IEppEduPlanDAO.instance.get().getActiveStudentEduplanVersionRelationMap(UniBaseDao.ids(studentList));
        final Map<Long, Set<Student>> gridId2StudentList = SafeMap.get(HashSet.class);
        for (final EppStudent2EduPlanVersion eduPlanrel : relationMap.values()) {
            final Long gridId = eduPlanrel.getEduPlanVersion().getEduPlan().getDevelopGrid().getId();
            gridId2StudentList.get(gridId).add(eduPlanrel.getStudent());
        }

        final List<EppStudent2WorkPlan> relWorkPlanList = new DQLSelectBuilder()
        .fromEntity(EppStudent2WorkPlan.class, "s2wp").column(property("s2wp"))
        .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias("s2wp"), "wp")
        .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("s2wp"), "s2epv")
        .fetchPath(DQLJoinType.inner, EppStudent2EduPlanVersion.student().fromAlias("s2epv"), "s")
        .where(isNull(property(EppStudent2WorkPlanGen.removalDate().fromAlias("s2wp"))))
        .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
        .where(in(property(Student.id().fromAlias("s")), CommonDAO.ids(studentList)))
        .createStatement(DAO.this.getSession()).list();

        {
            // подгружаем РУП и ссылки на блоки - массово, чтобы все за раз загрузилось
            {
                final Collection<EppStudent2WorkPlan> selectedRelations = CollectionUtils.select(relWorkPlanList, object -> ((EppStudent2WorkPlan)object).getWorkPlan() instanceof EppWorkPlan);
                final Collection<Long> ids = CollectionUtils.collect(selectedRelations, input -> {
                    return ((EppStudent2WorkPlan)input).getWorkPlan().getId();
                });

                if (!ids.isEmpty()) {
                    final MQBuilder b = new MQBuilder(EppWorkPlanGen.ENTITY_CLASS, "wpReal");
                    b.add(MQExpression.in("wpReal", "id", ids));
                    b.addJoinFetch("wpReal", EppWorkPlanGen.parent().s(), "wpBlock");
                    b.getResultList(this.getSession());
                }
            }

            {
                final Collection<EppStudent2WorkPlan> selectedRelations = CollectionUtils.select(relWorkPlanList, object -> ((EppStudent2WorkPlan)object).getWorkPlan() instanceof EppWorkPlanVersion);
                final Collection<Long> ids = CollectionUtils.collect(selectedRelations, input -> {
                    return ((EppStudent2WorkPlan)input).getWorkPlan().getId();
                });

                if (!ids.isEmpty()) {
                    final MQBuilder b = new MQBuilder(EppWorkPlanVersionGen.ENTITY_CLASS, "wpVersion");
                    b.add(MQExpression.in("wpVersion", "id", ids));
                    b.addJoinFetch("wpVersion", EppWorkPlanVersionGen.parent().s(), "wpReal");
                    b.addJoinFetch("wpReal", EppWorkPlanGen.parent().s(), "wpBlock");
                    b.getResultList(this.getSession());
                }
            }
        }



        // student.id -> { grid.id -> {term -> wp} }
        final Map<Long, Map<Long, Map<Integer, EppWorkPlanBase>>> dataMap = SafeMap.get(DAO.DATA_MAP_CALLBACK);
        for (final EppStudent2WorkPlan rel : relWorkPlanList)
        {
            final Long gridId = rel.getWorkPlan().getBlock().getEduPlanVersion().getEduPlan().getDevelopGrid().getId();
            dataMap.get(rel.getStudentEduPlanVersion().getStudent().getId()).get(gridId).put(rel.getWorkPlan().getTerm().getIntValue(), rel.getWorkPlan());
        }

        // итоговые сетки с РУП
        final ArrayList<BlockGridWrapper> result = new ArrayList<>(gridId2StudentList.keySet().size());

        // берем распределения курсов по семестрам
        final Map<Long, Map<Integer, DevelopGridTerm>> gridMap = IDevelopGridDAO.instance.get().getDevelopGridDistributionMap(gridId2StudentList.keySet());
        for (final Entry<Long, Map<Integer, DevelopGridTerm>> gridMapEntry : gridMap.entrySet())
        {
            final BlockGridWrapper block = new BlockGridWrapper();
            final StaticListDataSource<IEntity> dataSource = block.getDataSource();

            // формируем шапку таблицы
            this.doPrepareTableColumn(dataSource, gridMapEntry.getValue().entrySet());

            // берем студентов соответствующих данной сетке
            final Collection<Student> currentStudentList = gridId2StudentList.get(gridMapEntry.getKey());

            final List<ViewWrapper<Student>> patchedList = ViewWrapper.getPatchedList(currentStudentList);
            for (final ViewWrapper<Student> wrapper : patchedList)
            {
                final Long studentId = wrapper.getId();
                EppWorkPlanBase planVersion = null;
                for (final Entry<Integer, DevelopGridTerm> entry : gridMapEntry.getValue().entrySet())
                {
                    final int term = entry.getKey();
                    final String termKey = "term." + term;

                    if (dataMap.containsKey(studentId))
                    {
                        if (dataMap.get(studentId).containsKey(gridMapEntry.getKey()))
                        {
                            planVersion = dataMap.get(studentId).get(gridMapEntry.getKey()).get(term);
                        }
                    }
                    wrapper.setViewProperty(termKey, planVersion);
                }
            }
            Collections.sort((List)patchedList, new EntityComparator<Student>(new EntityOrder("title")));
            dataSource.setupRows(patchedList);

            result.add(block);
        }

        model.setBlockList(result);

        model.setStudentCustomStateCIModel(new CommonMultiSelectModel(StudentCustomStateCI.title().s())
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateCI.class, "st").column(property("st"))
                        .order(property(StudentCustomStateCI.title().fromAlias("st")));

                if (!StringUtils.isEmpty(filter))
                    builder.where(like(upper(property(StudentCustomStateCI.title().fromAlias("st"))), value(CoreStringUtils.escapeLike(filter, true))));

                if (set != null)
                    builder.where(in(property(StudentCustomStateCI.id().fromAlias("st")), set));

                return new DQLListResultBuilder(builder, 50);
            }
        });
    }

    @SuppressWarnings("unused")
    private static final BaseRawFormatter<IEntity> FUNNY_FORMATTER = new BaseRawFormatter<IEntity>() {
        @Override public String format(final IEntity source) {
            if (null == source) { return ""; }
            final int year = ((Number)source.getProperty("year.educationYear.intValue")).intValue() % 100;
            return (
                    /*"№"+*/BaseRawFormatter.encode(String.valueOf(source.getProperty("number")))+
                    "<div style=\"font-size:9px;float:right\">"+StringUtils.leftPad(String.valueOf(year), 2, '0')+"/"+StringUtils.leftPad(String.valueOf(1+year), 2, '0')+"</div>"
            );
        }
    };

    private void doPrepareTableColumn(final StaticListDataSource<IEntity> dataSource, final Collection<Entry<Integer, DevelopGridTerm>> sortedTermList)
    {
        dataSource.addColumn(new CheckboxColumn("select", "").setRequired(true).setWidth(1));
        dataSource.addColumn(this.wrap(new SimpleColumn("ФИО студента", Student.FIO_KEY).setClickable(false).setOrderable(false)));
        final Map<String, HeadColumn> columnMap = new LinkedHashMap<>();

        for (final Entry<Integer, DevelopGridTerm> entry : sortedTermList)
        {
            final String courseKey = "course." + entry.getValue().getCourseNumber();
            HeadColumn courseHeadColumn = columnMap.get(courseKey);

            if (null == courseHeadColumn) {
                final String courseColumnCaption = String.valueOf(entry.getValue().getCourseNumber()) + " курс";
                courseHeadColumn = new HeadColumn(courseKey, courseColumnCaption);
            }

            final int term = entry.getKey();
            final String termKey = "term." + term;

            final PublisherLinkColumn termColumn = new PublisherLinkColumn(String.valueOf(term), termKey).setResolver(new DefaultPublisherLinkResolver() {
                @Override public Object getParameters(final IEntity entity) {
                    return ((EppWorkPlanBase)entity.getProperty(termKey)).getId();
                }
            });

            termColumn.setFormatter(/*FUNNY_FORMATTER*/ new PropertyFormatter("shortTitle")).setWidth(5);

            courseHeadColumn.addColumn(this.wrap(termColumn));
            this.wrap(courseHeadColumn).setWidth(1);

            if (!columnMap.containsKey(courseKey)) {
                columnMap.put(courseKey, courseHeadColumn);
            }
        }
        for (final HeadColumn column : columnMap.values())
        {
            dataSource.addColumn(column);
        }
    }

    @SuppressWarnings("unchecked")
    private AbstractColumn wrap(final AbstractColumn column)
    {
        return column.setHeaderAlign("center").setOrderable(false);
    }
}
