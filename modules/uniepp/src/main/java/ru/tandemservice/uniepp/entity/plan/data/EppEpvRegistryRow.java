package ru.tandemservice.uniepp.entity.plan.data;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateMutable;
import ru.tandemservice.uniepp.entity.IEppNumberRow;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvRegistryRowGen;

/**
 * Запись версии УП (элемент реестра)
 */
@EppStateMutable({EppEpvRegistryRowGen.L_REGISTRY_ELEMENT})
public class EppEpvRegistryRow extends EppEpvRegistryRowGen implements IEppNumberRow
{

    public EppEpvRegistryRow() {
    }

    public EppEpvRegistryRow(EppEduPlanVersionBlock block) {
        this();
        this.setOwner(block);
    }

    @Override public EppRegistryStructure getType() { return this.getRegistryElementType(); }

    @Override public String getRowType() { return getType().getAbbreviation(); }
    @Override public String getDisplayableTitle() { return super.getDisplayableTitle() /*+" ("+getRowType()+")"*/; }

    /**
     * Как во враппере (понадобилось выводить титл с индексом без создания враппера)
     * @return стринг ([Индекс]. )[Название]
     */
    @Override
    @EntityDSLSupport
    public String getTitleWithIndex()
    {
        String storedIndex = getStoredIndex();
        return (storedIndex != null ? storedIndex + ". " : "") + getDisplayableTitle();
    }
}