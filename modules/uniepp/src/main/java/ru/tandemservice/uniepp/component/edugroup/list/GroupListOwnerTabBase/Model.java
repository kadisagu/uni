package ru.tandemservice.uniepp.component.edugroup.list.GroupListOwnerTabBase;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.component.edugroup.EduGroupOwnerModel;
import ru.tandemservice.uniepp.dao.group.EppRealGroupRowDAO;

import java.util.Date;

/**
 * @author vdanilov
 */
@State({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="orgUnitHolder.id")
})
public abstract class Model extends EduGroupOwnerModel {

    public abstract String getGroupContentComponentName();
    public String getDaemonStatus(final String message) {
        final Long date = EppRealGroupRowDAO.DAEMON.getCompleteStatus();
        if (null != date) {
            return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));
        }
        return message;
    }

    private boolean empty;
    public boolean isEmpty() { return this.empty; }
    public void setEmpty(final boolean empty) { this.empty = empty; }

    private IDataSettings settings;
    public IDataSettings getSettings() { return this.settings; }
    public void setSettings(final IDataSettings settings) { this.settings = settings; }

    public Object getOwner() { return this.getSettings().get("owner"); }
    public Object getYearPart() { return this.getSettings().get("yearPart"); }
    public Object getGroupType() { return this.getSettings().get("groupType"); }

    private ISelectModel ownerSelectModel;
    public ISelectModel getOwnerSelectModel() { return this.ownerSelectModel; }
    public void setOwnerSelectModel(final ISelectModel ownerSelectModel) { this.ownerSelectModel = ownerSelectModel; }

    private ISelectModel yearPartSelectModel;
    public ISelectModel getYearPartSelectModel() { return this.yearPartSelectModel; }
    public void setYearPartSelectModel(final ISelectModel yearSelectModel) { this.yearPartSelectModel = yearSelectModel; }

    private ISelectModel groupTypeSelectModel;
    public ISelectModel getGroupTypeSelectModel() { return this.groupTypeSelectModel; }
    public void setGroupTypeSelectModel(final ISelectModel groupTypeSelectModel) { this.groupTypeSelectModel = groupTypeSelectModel; }


}
