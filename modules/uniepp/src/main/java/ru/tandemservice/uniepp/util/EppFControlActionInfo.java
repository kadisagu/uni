/* $Id$ */
package ru.tandemservice.uniepp.util;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Nikolay Fedorovskih
 * @since 31.05.2015
 */
public class EppFControlActionInfo
{
    public static final Supplier<EppFControlActionInfo> SUPPLIER = Suppliers.memoize(() ->
        new EppFControlActionInfo(IUniBaseDao.instance.get().getList(EppFControlActionType.class, EppFControlActionType.priority().s()))
    );

    private final BiMap<Long, String> idToFullCodeMap;
    private final Map<Long, String> groupTypeId2fcaFullCodeMap;
    private final Set<String> examFullCodeSet;
    private final Set<String> courseFullCodeSet;

    private EppFControlActionInfo(List<EppFControlActionType> actionTypeList)
    {
        final ImmutableBiMap.Builder<Long, String> idToCodeBuilder = ImmutableBiMap.builder();
        final ImmutableSet.Builder<String> examsBuilder = ImmutableSet.builder();
        final ImmutableSet.Builder<String> coursesBuilder = ImmutableSet.builder();
        final ImmutableMap.Builder<Long, String> groupTypeId2fcaFullCodeBuilder = new ImmutableMap.Builder<>();
        for (EppFControlActionType actionType : actionTypeList) {
            final String fullCode = actionType.getFullCode();
            idToCodeBuilder.put(actionType.getId(), fullCode);
            groupTypeId2fcaFullCodeBuilder.put(actionType.getEppGroupType().getId(), fullCode);
            final String groupCode = actionType.getGroup().getCode();
            if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(groupCode)) {
                examsBuilder.add(fullCode);
            } else if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_PROJECT.equals(groupCode)) {
                coursesBuilder.add(fullCode);
            }
        }
        idToFullCodeMap = idToCodeBuilder.build();
        examFullCodeSet = examsBuilder.build();
        courseFullCodeSet = coursesBuilder.build();
        groupTypeId2fcaFullCodeMap = groupTypeId2fcaFullCodeBuilder.build();
    }

    public boolean isExam(String fullCode)
    {
        return examFullCodeSet.contains(fullCode);
    }

    public boolean isCourseWork(String fullCode)
    {
        return courseFullCodeSet.contains(fullCode);
    }

    public boolean isCourseWork(Long id)
    {
        return isCourseWork(idToFullCodeMap.get(id));
    }

    public String getFullCode(Long id)
    {
        return idToFullCodeMap.get(id);
    }

    public String getFullCodeByGroupTypeId(Long eppGroupTypeId)
    {
        return groupTypeId2fcaFullCodeMap.get(eppGroupTypeId);
    }

    public Long getFcaIdByGroupTypeId(Long eppGroupTypeId)
    {
        return getId(getFullCodeByGroupTypeId(eppGroupTypeId));
    }

    public Long getId(String fullCode)
    {
        return idToFullCodeMap.inverse().get(fullCode);
    }

    public Set<String> getExamFullCodes()
    {
        return examFullCodeSet;
    }

    public Set<String> getCourseFullCodes()
    {
        return courseFullCodeSet;
    }

    public Map<Long, String> getIdToFullCodeMap()
    {
        return idToFullCodeMap;
    }
}