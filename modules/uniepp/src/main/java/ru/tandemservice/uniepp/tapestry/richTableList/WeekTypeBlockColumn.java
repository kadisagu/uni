/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.tapestry.richTableList;

import org.tandemframework.core.view.list.column.SimpleColumn;

/**
 * @author vip_delete
 * @since 16.03.2010
 */
public final class WeekTypeBlockColumn extends SimpleColumn
{
    private final int _columnIdx;      // порядковый номер колонки (счет с нуля)
    private final long _columnId;      // идентификатор колонки

    public WeekTypeBlockColumn(final long columnId, final int columnIdx, final String caption)
    {
        super(caption, null);
        this.setOrderable(false);
        this.setClickable(false);
        this._columnIdx = columnIdx;
        this._columnId = columnId;
    }

    public int getColumnIdx()
    {
        return this._columnIdx;
    }

    public Long getColumnId()
    {
        return this._columnId;
    }
}
