/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.Content;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Nikolay Fedorovskih
 * @since 14.08.2015
 */
@Configuration
public class EppCustomEduPlanContent extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .create();
    }
}