package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.EppViewTableDisciplines;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Версия учебного плана
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanVersionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion";
    public static final String ENTITY_NAME = "eppEduPlanVersion";
    public static final int VERSION_HASH = 1079568821;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN = "eduPlan";
    public static final String P_NUMBER = "number";
    public static final String P_REGISTRATION_NUMBER = "registrationNumber";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";
    public static final String P_TITLE_POSTFIX = "titlePostfix";
    public static final String P_LOAD_PRESENTATION_IN_WEEKS = "loadPresentationInWeeks";
    public static final String L_VIEW_TABLE_DISCIPLINE = "viewTableDiscipline";
    public static final String L_STATE = "state";
    public static final String P_HOURS_I = "hoursI";
    public static final String P_HOURS_E = "hoursE";
    public static final String P_CONFIRM_DATE = "confirmDate";
    public static final String P_COMMENT = "comment";
    public static final String L_DEVELOP_GRID_TERM = "developGridTerm";
    public static final String P_EDUCATION_ELEMENT_TITLE = "educationElementTitle";
    public static final String P_FULL_NUMBER = "fullNumber";
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_FULL_TITLE_WITH_EDUCATIONS_CHARACTERISTICS = "fullTitleWithEducationsCharacteristics";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_WITH_ARCHIVE = "titleWithArchive";

    private EppEduPlan _eduPlan;     // Базовый учебный план (УП)
    private String _number;     // Номер
    private String _registrationNumber;     // Номер регистрации
    private String _protocolNumber;     // Номер протокола
    private String _titlePostfix;     // Постфикс
    private boolean _loadPresentationInWeeks; 
    private EppViewTableDisciplines _viewTableDiscipline;     // Вид таблицы дисциплин
    private EppState _state;     // Состояние
    private boolean _hoursI;     // Отображать часы в интерактивной форме
    private boolean _hoursE;     // Отображать часы в электронной форме
    private Date _confirmDate;     // Дата утверждения
    private String _comment;     // Комментарий
    private DevelopGridTerm _developGridTerm;     // Семестр выбора профиля

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Базовый учебный план (УП). Свойство не может быть null.
     */
    @NotNull
    public EppEduPlan getEduPlan()
    {
        return _eduPlan;
    }

    /**
     * @param eduPlan Базовый учебный план (УП). Свойство не может быть null.
     */
    public void setEduPlan(EppEduPlan eduPlan)
    {
        dirty(_eduPlan, eduPlan);
        _eduPlan = eduPlan;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Номер регистрации. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRegistrationNumber()
    {
        return _registrationNumber;
    }

    /**
     * @param registrationNumber Номер регистрации. Свойство не может быть null.
     */
    public void setRegistrationNumber(String registrationNumber)
    {
        dirty(_registrationNumber, registrationNumber);
        _registrationNumber = registrationNumber;
    }

    /**
     * @return Номер протокола.
     */
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    /**
     * @return Постфикс.
     */
    @Length(max=255)
    public String getTitlePostfix()
    {
        return _titlePostfix;
    }

    /**
     * @param titlePostfix Постфикс.
     */
    public void setTitlePostfix(String titlePostfix)
    {
        dirty(_titlePostfix, titlePostfix);
        _titlePostfix = titlePostfix;
    }

    /**
     * DEV-2552, DEV-5818
     *
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isLoadPresentationInWeeks()
    {
        return _loadPresentationInWeeks;
    }

    /**
     * @param loadPresentationInWeeks  Свойство не может быть null.
     */
    public void setLoadPresentationInWeeks(boolean loadPresentationInWeeks)
    {
        dirty(_loadPresentationInWeeks, loadPresentationInWeeks);
        _loadPresentationInWeeks = loadPresentationInWeeks;
    }

    /**
     * @return Вид таблицы дисциплин. Свойство не может быть null.
     */
    @NotNull
    public EppViewTableDisciplines getViewTableDiscipline()
    {
        return _viewTableDiscipline;
    }

    /**
     * @param viewTableDiscipline Вид таблицы дисциплин. Свойство не может быть null.
     */
    public void setViewTableDiscipline(EppViewTableDisciplines viewTableDiscipline)
    {
        dirty(_viewTableDiscipline, viewTableDiscipline);
        _viewTableDiscipline = viewTableDiscipline;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public EppState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(EppState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Отображать часы в интерактивной форме. Свойство не может быть null.
     */
    @NotNull
    public boolean isHoursI()
    {
        return _hoursI;
    }

    /**
     * @param hoursI Отображать часы в интерактивной форме. Свойство не может быть null.
     */
    public void setHoursI(boolean hoursI)
    {
        dirty(_hoursI, hoursI);
        _hoursI = hoursI;
    }

    /**
     * @return Отображать часы в электронной форме. Свойство не может быть null.
     */
    @NotNull
    public boolean isHoursE()
    {
        return _hoursE;
    }

    /**
     * @param hoursE Отображать часы в электронной форме. Свойство не может быть null.
     */
    public void setHoursE(boolean hoursE)
    {
        dirty(_hoursE, hoursE);
        _hoursE = hoursE;
    }

    /**
     * @return Дата утверждения.
     */
    public Date getConfirmDate()
    {
        return _confirmDate;
    }

    /**
     * @param confirmDate Дата утверждения.
     */
    public void setConfirmDate(Date confirmDate)
    {
        dirty(_confirmDate, confirmDate);
        _confirmDate = confirmDate;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Семестр выбора профиля. Свойство не может быть null.
     */
    @NotNull
    public DevelopGridTerm getDevelopGridTerm()
    {
        return _developGridTerm;
    }

    /**
     * @param developGridTerm Семестр выбора профиля. Свойство не может быть null.
     */
    public void setDevelopGridTerm(DevelopGridTerm developGridTerm)
    {
        dirty(_developGridTerm, developGridTerm);
        _developGridTerm = developGridTerm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEduPlanVersionGen)
        {
            setEduPlan(((EppEduPlanVersion)another).getEduPlan());
            setNumber(((EppEduPlanVersion)another).getNumber());
            setRegistrationNumber(((EppEduPlanVersion)another).getRegistrationNumber());
            setProtocolNumber(((EppEduPlanVersion)another).getProtocolNumber());
            setTitlePostfix(((EppEduPlanVersion)another).getTitlePostfix());
            setLoadPresentationInWeeks(((EppEduPlanVersion)another).isLoadPresentationInWeeks());
            setViewTableDiscipline(((EppEduPlanVersion)another).getViewTableDiscipline());
            setState(((EppEduPlanVersion)another).getState());
            setHoursI(((EppEduPlanVersion)another).isHoursI());
            setHoursE(((EppEduPlanVersion)another).isHoursE());
            setConfirmDate(((EppEduPlanVersion)another).getConfirmDate());
            setComment(((EppEduPlanVersion)another).getComment());
            setDevelopGridTerm(((EppEduPlanVersion)another).getDevelopGridTerm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanVersionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlanVersion.class;
        }

        public T newInstance()
        {
            return (T) new EppEduPlanVersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlan":
                    return obj.getEduPlan();
                case "number":
                    return obj.getNumber();
                case "registrationNumber":
                    return obj.getRegistrationNumber();
                case "protocolNumber":
                    return obj.getProtocolNumber();
                case "titlePostfix":
                    return obj.getTitlePostfix();
                case "loadPresentationInWeeks":
                    return obj.isLoadPresentationInWeeks();
                case "viewTableDiscipline":
                    return obj.getViewTableDiscipline();
                case "state":
                    return obj.getState();
                case "hoursI":
                    return obj.isHoursI();
                case "hoursE":
                    return obj.isHoursE();
                case "confirmDate":
                    return obj.getConfirmDate();
                case "comment":
                    return obj.getComment();
                case "developGridTerm":
                    return obj.getDevelopGridTerm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlan":
                    obj.setEduPlan((EppEduPlan) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "registrationNumber":
                    obj.setRegistrationNumber((String) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
                case "titlePostfix":
                    obj.setTitlePostfix((String) value);
                    return;
                case "loadPresentationInWeeks":
                    obj.setLoadPresentationInWeeks((Boolean) value);
                    return;
                case "viewTableDiscipline":
                    obj.setViewTableDiscipline((EppViewTableDisciplines) value);
                    return;
                case "state":
                    obj.setState((EppState) value);
                    return;
                case "hoursI":
                    obj.setHoursI((Boolean) value);
                    return;
                case "hoursE":
                    obj.setHoursE((Boolean) value);
                    return;
                case "confirmDate":
                    obj.setConfirmDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "developGridTerm":
                    obj.setDevelopGridTerm((DevelopGridTerm) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlan":
                        return true;
                case "number":
                        return true;
                case "registrationNumber":
                        return true;
                case "protocolNumber":
                        return true;
                case "titlePostfix":
                        return true;
                case "loadPresentationInWeeks":
                        return true;
                case "viewTableDiscipline":
                        return true;
                case "state":
                        return true;
                case "hoursI":
                        return true;
                case "hoursE":
                        return true;
                case "confirmDate":
                        return true;
                case "comment":
                        return true;
                case "developGridTerm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlan":
                    return true;
                case "number":
                    return true;
                case "registrationNumber":
                    return true;
                case "protocolNumber":
                    return true;
                case "titlePostfix":
                    return true;
                case "loadPresentationInWeeks":
                    return true;
                case "viewTableDiscipline":
                    return true;
                case "state":
                    return true;
                case "hoursI":
                    return true;
                case "hoursE":
                    return true;
                case "confirmDate":
                    return true;
                case "comment":
                    return true;
                case "developGridTerm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlan":
                    return EppEduPlan.class;
                case "number":
                    return String.class;
                case "registrationNumber":
                    return String.class;
                case "protocolNumber":
                    return String.class;
                case "titlePostfix":
                    return String.class;
                case "loadPresentationInWeeks":
                    return Boolean.class;
                case "viewTableDiscipline":
                    return EppViewTableDisciplines.class;
                case "state":
                    return EppState.class;
                case "hoursI":
                    return Boolean.class;
                case "hoursE":
                    return Boolean.class;
                case "confirmDate":
                    return Date.class;
                case "comment":
                    return String.class;
                case "developGridTerm":
                    return DevelopGridTerm.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlanVersion> _dslPath = new Path<EppEduPlanVersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlanVersion");
    }
            

    /**
     * @return Базовый учебный план (УП). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getEduPlan()
     */
    public static EppEduPlan.Path<EppEduPlan> eduPlan()
    {
        return _dslPath.eduPlan();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Номер регистрации. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getRegistrationNumber()
     */
    public static PropertyPath<String> registrationNumber()
    {
        return _dslPath.registrationNumber();
    }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    /**
     * @return Постфикс.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getTitlePostfix()
     */
    public static PropertyPath<String> titlePostfix()
    {
        return _dslPath.titlePostfix();
    }

    /**
     * DEV-2552, DEV-5818
     *
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#isLoadPresentationInWeeks()
     */
    public static PropertyPath<Boolean> loadPresentationInWeeks()
    {
        return _dslPath.loadPresentationInWeeks();
    }

    /**
     * @return Вид таблицы дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getViewTableDiscipline()
     */
    public static EppViewTableDisciplines.Path<EppViewTableDisciplines> viewTableDiscipline()
    {
        return _dslPath.viewTableDiscipline();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getState()
     */
    public static EppState.Path<EppState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Отображать часы в интерактивной форме. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#isHoursI()
     */
    public static PropertyPath<Boolean> hoursI()
    {
        return _dslPath.hoursI();
    }

    /**
     * @return Отображать часы в электронной форме. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#isHoursE()
     */
    public static PropertyPath<Boolean> hoursE()
    {
        return _dslPath.hoursE();
    }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getConfirmDate()
     */
    public static PropertyPath<Date> confirmDate()
    {
        return _dslPath.confirmDate();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Семестр выбора профиля. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getDevelopGridTerm()
     */
    public static DevelopGridTerm.Path<DevelopGridTerm> developGridTerm()
    {
        return _dslPath.developGridTerm();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getEducationElementTitle()
     */
    public static SupportedPropertyPath<String> educationElementTitle()
    {
        return _dslPath.educationElementTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getFullNumber()
     */
    public static SupportedPropertyPath<String> fullNumber()
    {
        return _dslPath.fullNumber();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getFullTitle()
     */
    public static SupportedPropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getFullTitleWithEducationsCharacteristics()
     */
    public static SupportedPropertyPath<String> fullTitleWithEducationsCharacteristics()
    {
        return _dslPath.fullTitleWithEducationsCharacteristics();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getTitleWithArchive()
     */
    public static SupportedPropertyPath<String> titleWithArchive()
    {
        return _dslPath.titleWithArchive();
    }

    public static class Path<E extends EppEduPlanVersion> extends EntityPath<E>
    {
        private EppEduPlan.Path<EppEduPlan> _eduPlan;
        private PropertyPath<String> _number;
        private PropertyPath<String> _registrationNumber;
        private PropertyPath<String> _protocolNumber;
        private PropertyPath<String> _titlePostfix;
        private PropertyPath<Boolean> _loadPresentationInWeeks;
        private EppViewTableDisciplines.Path<EppViewTableDisciplines> _viewTableDiscipline;
        private EppState.Path<EppState> _state;
        private PropertyPath<Boolean> _hoursI;
        private PropertyPath<Boolean> _hoursE;
        private PropertyPath<Date> _confirmDate;
        private PropertyPath<String> _comment;
        private DevelopGridTerm.Path<DevelopGridTerm> _developGridTerm;
        private SupportedPropertyPath<String> _educationElementTitle;
        private SupportedPropertyPath<String> _fullNumber;
        private SupportedPropertyPath<String> _fullTitle;
        private SupportedPropertyPath<String> _fullTitleWithEducationsCharacteristics;
        private SupportedPropertyPath<String> _title;
        private SupportedPropertyPath<String> _titleWithArchive;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Базовый учебный план (УП). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getEduPlan()
     */
        public EppEduPlan.Path<EppEduPlan> eduPlan()
        {
            if(_eduPlan == null )
                _eduPlan = new EppEduPlan.Path<EppEduPlan>(L_EDU_PLAN, this);
            return _eduPlan;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppEduPlanVersionGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Номер регистрации. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getRegistrationNumber()
     */
        public PropertyPath<String> registrationNumber()
        {
            if(_registrationNumber == null )
                _registrationNumber = new PropertyPath<String>(EppEduPlanVersionGen.P_REGISTRATION_NUMBER, this);
            return _registrationNumber;
        }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(EppEduPlanVersionGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

    /**
     * @return Постфикс.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getTitlePostfix()
     */
        public PropertyPath<String> titlePostfix()
        {
            if(_titlePostfix == null )
                _titlePostfix = new PropertyPath<String>(EppEduPlanVersionGen.P_TITLE_POSTFIX, this);
            return _titlePostfix;
        }

    /**
     * DEV-2552, DEV-5818
     *
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#isLoadPresentationInWeeks()
     */
        public PropertyPath<Boolean> loadPresentationInWeeks()
        {
            if(_loadPresentationInWeeks == null )
                _loadPresentationInWeeks = new PropertyPath<Boolean>(EppEduPlanVersionGen.P_LOAD_PRESENTATION_IN_WEEKS, this);
            return _loadPresentationInWeeks;
        }

    /**
     * @return Вид таблицы дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getViewTableDiscipline()
     */
        public EppViewTableDisciplines.Path<EppViewTableDisciplines> viewTableDiscipline()
        {
            if(_viewTableDiscipline == null )
                _viewTableDiscipline = new EppViewTableDisciplines.Path<EppViewTableDisciplines>(L_VIEW_TABLE_DISCIPLINE, this);
            return _viewTableDiscipline;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getState()
     */
        public EppState.Path<EppState> state()
        {
            if(_state == null )
                _state = new EppState.Path<EppState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Отображать часы в интерактивной форме. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#isHoursI()
     */
        public PropertyPath<Boolean> hoursI()
        {
            if(_hoursI == null )
                _hoursI = new PropertyPath<Boolean>(EppEduPlanVersionGen.P_HOURS_I, this);
            return _hoursI;
        }

    /**
     * @return Отображать часы в электронной форме. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#isHoursE()
     */
        public PropertyPath<Boolean> hoursE()
        {
            if(_hoursE == null )
                _hoursE = new PropertyPath<Boolean>(EppEduPlanVersionGen.P_HOURS_E, this);
            return _hoursE;
        }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getConfirmDate()
     */
        public PropertyPath<Date> confirmDate()
        {
            if(_confirmDate == null )
                _confirmDate = new PropertyPath<Date>(EppEduPlanVersionGen.P_CONFIRM_DATE, this);
            return _confirmDate;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EppEduPlanVersionGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Семестр выбора профиля. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getDevelopGridTerm()
     */
        public DevelopGridTerm.Path<DevelopGridTerm> developGridTerm()
        {
            if(_developGridTerm == null )
                _developGridTerm = new DevelopGridTerm.Path<DevelopGridTerm>(L_DEVELOP_GRID_TERM, this);
            return _developGridTerm;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getEducationElementTitle()
     */
        public SupportedPropertyPath<String> educationElementTitle()
        {
            if(_educationElementTitle == null )
                _educationElementTitle = new SupportedPropertyPath<String>(EppEduPlanVersionGen.P_EDUCATION_ELEMENT_TITLE, this);
            return _educationElementTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getFullNumber()
     */
        public SupportedPropertyPath<String> fullNumber()
        {
            if(_fullNumber == null )
                _fullNumber = new SupportedPropertyPath<String>(EppEduPlanVersionGen.P_FULL_NUMBER, this);
            return _fullNumber;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getFullTitle()
     */
        public SupportedPropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new SupportedPropertyPath<String>(EppEduPlanVersionGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getFullTitleWithEducationsCharacteristics()
     */
        public SupportedPropertyPath<String> fullTitleWithEducationsCharacteristics()
        {
            if(_fullTitleWithEducationsCharacteristics == null )
                _fullTitleWithEducationsCharacteristics = new SupportedPropertyPath<String>(EppEduPlanVersionGen.P_FULL_TITLE_WITH_EDUCATIONS_CHARACTERISTICS, this);
            return _fullTitleWithEducationsCharacteristics;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EppEduPlanVersionGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion#getTitleWithArchive()
     */
        public SupportedPropertyPath<String> titleWithArchive()
        {
            if(_titleWithArchive == null )
                _titleWithArchive = new SupportedPropertyPath<String>(EppEduPlanVersionGen.P_TITLE_WITH_ARCHIVE, this);
            return _titleWithArchive;
        }

        public Class getEntityClass()
        {
            return EppEduPlanVersion.class;
        }

        public String getEntityName()
        {
            return "eppEduPlanVersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getEducationElementTitle();

    public abstract String getFullNumber();

    public abstract String getFullTitle();

    public abstract String getFullTitleWithEducationsCharacteristics();

    public abstract String getTitle();

    public abstract String getTitleWithArchive();
}
