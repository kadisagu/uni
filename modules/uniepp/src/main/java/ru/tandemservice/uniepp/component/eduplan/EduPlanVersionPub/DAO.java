/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.*;

import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.plan.*;

/**
 * @author vip_delete
 * @since 27.02.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        EppEduPlanVersion epv = model.getEduplanVersionHolder().refresh(EppEduPlanVersion.class);
        EppEduPlan ep = epv.getEduPlan();
        Session session = this.getSession();

        model.setChangeDirectionBlocksDisabled(!(ep instanceof EppEduPlanHigherProf));
        // в ДПО направленностей нет, а в СПО пока считаем, что нет

        {
            final List<EppEduPlanVersionBlock> databaseBlockList = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion(), epv);
            Collections.sort(databaseBlockList);

            List<ViewWrapper<EppEduPlanVersionBlock>> wrappers = ViewWrapper.getPatchedList(databaseBlockList);

            Map<EduProgramSpecialization, StringBuilder> eduOuTitleMap = new HashMap<EduProgramSpecialization, StringBuilder>(wrappers.size());
            if (ep instanceof EppEduPlanProf) {

                EduProgramSubject programSubject = ((EppEduPlanProf) ep).getProgramSubject();

                DQLSelectBuilder eduOuDql = new DQLSelectBuilder()
                    .fromEntity(EducationOrgUnit.class, "eduOu")
                    .column(property("eduOu"))
                    .where(eq(property(EducationOrgUnit.developForm().programForm().fromAlias("eduOu")), value(ep.getProgramForm())))
                    .where(eq(property(EducationOrgUnit.developCondition().fromAlias("eduOu")), value(ep.getDevelopCondition())))
                    .where(eqNullSafe(property(EducationOrgUnit.developTech().programTrait().fromAlias("eduOu")), value(ep.getProgramTrait())))
                    .where(eq(property(EducationOrgUnit.developPeriod().fromAlias("eduOu")), value(ep.getDevelopGrid().getDevelopPeriod())))
                    .where(eq(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().fromAlias("eduOu")), value(programSubject)))
                    .order(property(EducationOrgUnit.territorialOrgUnit().territorialShortTitle().fromAlias("eduOu")))
                    .order(property(EducationOrgUnit.formativeOrgUnit().title().fromAlias("eduOu")));
                List<EducationOrgUnit> eduOuList = eduOuDql.createStatement(session).list();

                Map<EduProgramSpecialization, Set<CoreCollectionUtils.Pair<Long, Long>>> doublingPreventionMap = SafeMap.get(HashSet.class);
                for (EducationOrgUnit eduOu: eduOuList) {
                    EduProgramSpecialization spec = eduOu.getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization();
                    CoreCollectionUtils.Pair<Long, Long> ouPairKey = new CoreCollectionUtils.Pair<>(eduOu.getFormativeOrgUnit().getId(), eduOu.getTerritorialOrgUnit().getId());
                    if (doublingPreventionMap.get(spec).contains(ouPairKey)) continue;
                    doublingPreventionMap.get(spec).add(ouPairKey);
                    StringBuilder sb = SafeMap.safeGet(eduOuTitleMap, spec, StringBuilder.class);
                    if (sb.length() > 0) { sb.append("\n"); }
                    sb.append(eduOu.getFormativeOrgUnit().getTypeTitle()).append(" (").append(eduOu.getTerritorialOrgUnit().getTerritorialTitle()).append(")");
                }
            }

            for (ViewWrapper<EppEduPlanVersionBlock> vw: wrappers) {
                EppEduPlanVersionBlock block = vw.getEntity();
                EduProgramSpecialization spec = block instanceof EppEduPlanVersionSpecializationBlock ? ((EppEduPlanVersionSpecializationBlock) block).getProgramSpecialization() : null;
                StringBuilder sb = eduOuTitleMap.get(spec);
                vw.setViewProperty("eduOuTitleList", null == sb ? "" : sb.toString());

                EduOwnerOrgUnit ownerOrgUnit = null;
                if (ep instanceof EppEduPlanSecondaryProf) {
                    ownerOrgUnit = ((EppEduPlanSecondaryProf) ep).getOwnerOrgUnit();
                } else if (block instanceof EppEduPlanVersionSpecializationBlock) {
                    ownerOrgUnit = ((EppEduPlanVersionSpecializationBlock) block).getOwnerOrgUnit();
                }
                vw.setViewProperty("ownerOrgUnitTitle", ownerOrgUnit != null ? ownerOrgUnit.getTitle() : "-");
            }

            StaticListDataSource<ViewWrapper<EppEduPlanVersionBlock>> ds = model.getBlockDataSource();
            ds.clearColumns();
            ds.addColumn(new SimpleColumn("Блок", EppEduPlanVersionBlock.title()).setOrderable(false).setClickable(true));
            ds.addColumn(new SimpleColumn("Выпускающее подр.", "ownerOrgUnitTitle").setOrderable(false).setClickable(false));
            ds.addColumn(new SimpleColumn("Формирующее (территориальное) подразделение", "eduOuTitleList").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setOrderable(false).setClickable(false));
            ds.setupRows(wrappers);
        }
    }
}
