package ru.tandemservice.uniepp.dao.index;


/**
 * Оболочка с вычисленным индексом
 * @author vdanilov
 */
public interface IEppIndexedRowWrapper extends IEppIndexedRow {


    /** @return строка, оболочкой которой является текущий объект */
    IEppIndexedRow getRow();

    /** @return название элементов в иерархии от корня до текущего, разделенные через '\n' */
    String getHierarhyPath();

    /** @return номера элементов в иерархии от корня до текущего, разделенные через '.' */
    String getHierarhyIndex();

    /** @return порядковый номер внутри вышестоящего элемента */
    int getSequenceNumber();

    /** @return полный индекс, вычисленный согласно правилу формирования индексов */
    String getIndex();

    /** @return полное название (индекс + название элемента) */
    String getTitleWithIndex();



}
