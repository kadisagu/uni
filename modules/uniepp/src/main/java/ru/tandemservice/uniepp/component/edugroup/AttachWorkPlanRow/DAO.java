package ru.tandemservice.uniepp.component.edugroup.AttachWorkPlanRow;

import org.hibernate.Session;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.component.edugroup.EppEduGroupRegElPartSelectModel;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.dao.registry.data.EppControlFormatter;
import ru.tandemservice.uniepp.dao.registry.data.EppLoadFormatter;
import ru.tandemservice.uniepp.dao.workplan.EppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        if (model.getIds().isEmpty()) { throw new IllegalArgumentException("model.ids is empty"); }
        IEppRealGroupRowDAO.instance.get().checkGroupLockedEdit(model.getIds(), model.getLevel());

        final Object[] dscRow = this.getTypeDescriptionRow(model);
        final EppRealEduGroupSummary summary = this.get(EppRealEduGroupSummary.class, (Long)dscRow[0]);

        // выбор элемента реестра
        {
            @SuppressWarnings("unchecked")
            final Class<? extends EppRealEduGroup> groupClass = EntityRuntime.getMeta(((Number)dscRow[1]).shortValue()).getEntityClass();
            model.getRegElementSelectModel().setSource(new EppEduGroupRegElPartSelectModel() {
                @Override protected Class<? extends EppRealEduGroup> getGroupClass() { return groupClass; }
                @Override protected Collection<Long> getGroupIds() { return model.getIds(); }
            });

            // выбираем наиболее часто встречающуюся дисциплину (среди записей УГС), если значения еще нет
            if (null == model.getRegElementSelectModel().getValue()) {
                model.getRegElementSelectModel().setupFirstValue();
            }
        }

        // выбор строки РУП
        {
            final EppLoadFormatter<IEppWorkPlanRowWrapper> ldFormatter = new EppLoadFormatter<IEppWorkPlanRowWrapper>(EppEduPlanVersionDataDAO.getLoadTypeMap()) {
                @Override protected Double load(final IEppWorkPlanRowWrapper element, final String loadFullCode) { return element.getTotalPartLoad(null, loadFullCode); }
            };

            final EppControlFormatter<IEppWorkPlanRowWrapper> caFormatter = new EppControlFormatter<IEppWorkPlanRowWrapper>(EppEduPlanVersionDataDAO.getActionTypeMap()) {
                @Override protected int size(final IEppWorkPlanRowWrapper element, final String actionFullCode) { return element.getActionSize(actionFullCode); }
                @Override protected boolean allow(final IEppWorkPlanRowWrapper element, final EppControlActionType actionType) { return true; }
            };

            model.getWpRowSelectModel().setSource(new DQLFullCheckSelectModel("wpTitle", "number", "title", "load", "control") {

                @Override protected List wrap(List resultList)
                {
                    @SuppressWarnings("unchecked")
                    final List<ViewWrapper<EppWorkPlanRegistryElementRow>> wrappers = ViewWrapper.getPatchedList(resultList);

                    final Map<Long, Long> row2wpMap = new HashMap<>(resultList.size());
                    for (final Object o: resultList) {
                        EppWorkPlanRegistryElementRow row = (EppWorkPlanRegistryElementRow)o;
                        row2wpMap.put(row.getId(), row.getWorkPlan().getId());
                    }

                    final Map<Long, IEppWorkPlanWrapper> dataMap = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(new HashSet<>(row2wpMap.values()));
                    for (final ViewWrapper<EppWorkPlanRegistryElementRow> row: wrappers) {
                        final IEppWorkPlanWrapper wpWrapper = dataMap.get(row2wpMap.get(row.getId()));
                        final IEppWorkPlanRowWrapper rowWrapper = (null == wpWrapper ? null : wpWrapper.getRowMap().get(row.getId()));

                        row.setViewProperty("wpTitle", row.getEntity().getWorkPlan().getTitle());
                        row.setViewProperty("load", null == rowWrapper ? "" : ldFormatter.formatLoadByType(rowWrapper));
                        row.setViewProperty("control", null == rowWrapper ? "" : caFormatter.format(rowWrapper));
                    }

                    if (wrappers.size() > 1) {
                        // сортируем по названию РУП
                        Collections.sort(wrappers, new EntityComparator<>(new EntityOrder("wpTitle")));
                    }
                    return wrappers;
                }

                @Override protected DQLSelectBuilder query(String alias, String filter)
                {
                    final EppRegistryElementPart value = model.getRegElementSelectModel().getValue();
                    if (null == value) { return null; }

                    final DQLSelectBuilder dql = new DQLSelectBuilder();
                    dql.fromEntity(EppWorkPlanRegistryElementRow.class, alias);
                    EppWorkPlanDAO.joinWorkPlan(dql, null, "wp", alias, EppWorkPlanRegistryElementRow.workPlan().id().s());

                    dql.where(eq(property(EppWorkPlanRegistryElementRow.registryElementPart().fromAlias(alias)), value(value)));
                    dql.where(eq(property(EppWorkPlan.year().fromAlias("wp")), value(summary.getYearPart().getYear())));
                    dql.where(eq(property(EppWorkPlan.cachedGridTerm().part().fromAlias("wp")), value(summary.getYearPart().getPart())));

                    if (null != filter) {
                        dql.where(or(
                            this.like(EppWorkPlanRegistryElementRow.title().fromAlias(alias), filter),
                            this.like(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().title().fromAlias(alias), filter)
                        ));
                    }

                    return dql;
                }

                @Override public String getLabelFor(final Object value, final int columnIndex) {
                    final ViewWrapper wrapper = this.getWrapper(value);
                    return super.getLabelFor(wrapper, columnIndex);
                }
                @Override public String getFullLabel(final Object value) {
                    final ViewWrapper wrapper = this.getWrapper(value);
                    return super.getFullLabel(wrapper);
                }
            });
        }
    }

    protected Object[] getTypeDescriptionRow(final Model model)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppRealEduGroup.class, "grp").where(in(property("grp", "id"), model.getIds()));
        dql.column(property("grp", EppRealEduGroup.summary().id()));
        dql.column(property("grp", EppRealEduGroup.clazz()));
        dql.column(property(EppRealEduGroup.type().id().fromAlias("grp")));
        dql.predicate(DQLPredicateType.distinct);
        final List<Object[]> rows = dql.createStatement(this.getSession()).list();
        if (rows.isEmpty()) { throw new IllegalArgumentException("No such groups"); }
        if (rows.size() > 1) {  throw new IllegalArgumentException("Multiple types"); }

        return rows.iterator().next();
    }

    @Override
    public void save(final Model model)
    {
        IEppRealGroupRowDAO.instance.get().checkGroupLockedEdit(model.getIds(), model.getLevel());

        final EppRegistryElementPart regElementPart = model.getRegElementSelectModel().getValue();
        if (null == regElementPart) { throw new ApplicationException("Не выбрана дисциплина реестра."); }

        final EppWorkPlanRegistryElementRow wpRow = model.getWpRowSelectModel().getValue();
        if (null == wpRow) { throw new ApplicationException("Не выбрана дисциплина РУП."); }

        final Session session = this.getSession();
        for (final Long id: model.getIds())
        {
            final EppRealEduGroup group = this.get(id);
            group.setProperty(EppRealEduGroup.L_ACTIVITY_PART, regElementPart);
            group.setProperty(EppRealEduGroup.L_WORK_PLAN_ROW, wpRow);
            session.saveOrUpdate(group);
        }
    }

}
