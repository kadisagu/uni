package ru.tandemservice.uniepp.component.registry.DisciplineRegistry.Pub;

import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;

/**
 * 
 * @author nkokorina
 *
 */

public interface IDAO extends ru.tandemservice.uniepp.component.registry.base.Pub.IDAO<EppRegistryDiscipline>
{

}
