package ru.tandemservice.uniepp.entity.catalog;

import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniepp.entity.catalog.gen.EppEduGroupSplitVariantGen;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @see ru.tandemservice.uniepp.entity.catalog.gen.EppEduGroupSplitVariantGen
 */
public class EppEduGroupSplitVariant extends EppEduGroupSplitVariantGen implements IDynamicCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EppEduGroupSplitVariant.class)
                .titleProperty(EppEduGroupSplitVariant.title().s())
                .filter(EppEduGroupSplitVariant.title())
                .order(EppEduGroupSplitVariant.title());
    }

    public static EntityComboDataSourceHandler selectForEppRegistryElementDSHandler(String ownerId)
    {
        return defaultSelectDSHandler(ownerId).customize((alias, dql, context, filter) -> {
            DQLSelectBuilder selectIdsFromRegElBuilder = new DQLSelectBuilder()
                    .fromEntity(EppRegistryElement.class, "el")
                    .column(property("el", EppRegistryElement.eduGroupSplitVariant().id()));
            return dql.where(in(property(alias, EppEduGroupSplitVariant.id()), selectIdsFromRegElBuilder.buildQuery()));
        });
    }

    public static SimpleTitledComboDataSourceHandler selectWithNoSplitVariantDSHandler(String ownerId){
        return new EppEduGroupSplitVariantWithNoSplitVariantHandler(ownerId);
    }
}