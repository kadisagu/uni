package ru.tandemservice.uniepp.entity.plan.data;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvRowTermLoadGen;

/**
 * Нагрузка строки версии УП (по видам нагрузки)
 */
public class EppEpvRowTermLoad extends EppEpvRowTermLoadGen implements IEppEpvRowTermDetail
{
    public EppEpvRowTermLoad()
    {
    }

    public EppEpvRowTermLoad(EppEpvRowTerm rowTerm, EppALoadType loadType)
    {
        setRowTerm(rowTerm);
        setLoadType(loadType);
    }

    public EppEpvRowTermLoad(EppEpvRowTerm rowTerm, EppALoadType loadType, Double value)
    {
        setRowTerm(rowTerm);
        setLoadType(loadType);
        setHoursAsDouble(value);
    }

    @Override public String getRelationTargetCode() {
        return this.getLoadType().getFullCode();
    }

    public static Double wrap(final long load) {
        return UniEppUtils.wrap(load < 0 ? null : load);
    }
    public static long unwrap(final Double value) {
        final Long load = UniEppUtils.unwrap(value);
        return null == load ? -1 : load.longValue();
    }

    @EntityDSLSupport(parts={EppEpvRowTermLoadGen.P_HOURS})
    @Override
    @Deprecated
    public Double getLoadAsDouble() {
        return getHoursAsDouble();
    }

    @Deprecated
    public void setLoadAsDouble(final Double value) {
        this.setHoursAsDouble(value);
    }

    @Override
    @EntityDSLSupport(parts={EppEpvRowLoad.P_HOURS})
    public Double getHoursAsDouble() {
        return wrap(this.getHours());
    }

    public void setHoursAsDouble(final Double value) {
        this.setHours(unwrap(value));
    }
}