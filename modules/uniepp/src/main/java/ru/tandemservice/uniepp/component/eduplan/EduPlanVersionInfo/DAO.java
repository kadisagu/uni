/* $Id$ */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionInfo;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author oleyba
 * @since 5/13/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setEduPlanVersion(this.get(EppEduPlanVersion.class, model.getEduPlanVersion().getId()));
    }
}
