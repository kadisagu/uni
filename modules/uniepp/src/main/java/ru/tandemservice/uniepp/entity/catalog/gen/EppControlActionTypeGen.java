package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Форма контроля
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppControlActionTypeGen extends EntityBase
 implements INaturalIdentifiable<EppControlActionTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppControlActionType";
    public static final String ENTITY_NAME = "eppControlActionType";
    public static final int VERSION_HASH = 164542540;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_CATALOG_CODE = "catalogCode";
    public static final String P_DISABLED = "disabled";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_ABBREVIATION = "abbreviation";
    public static final String P_ACTIVE_IN_EDU_PLAN = "activeInEduPlan";
    public static final String P_USED_WITH_DISCIPLINES = "usedWithDisciplines";
    public static final String P_USED_WITH_PRACTICE = "usedWithPractice";
    public static final String P_USED_WITH_ATTESTATION = "usedWithAttestation";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _catalogCode;     // Код справочника
    private boolean _disabled = false;     // Отключен
    private String _shortTitle;     // Сокращенное название
    private String _abbreviation;     // Условное сокращение
    private boolean _activeInEduPlan;     // Использовать в УП
    private boolean _usedWithDisciplines;     // Использование для дисциплин
    private boolean _usedWithPractice;     // Использование для практик
    private boolean _usedWithAttestation;     // Использование для ГИА
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Код справочника. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCatalogCode()
    {
        return _catalogCode;
    }

    /**
     * @param catalogCode Код справочника. Свойство не может быть null.
     */
    public void setCatalogCode(String catalogCode)
    {
        dirty(_catalogCode, catalogCode);
        _catalogCode = catalogCode;
    }

    /**
     * Показывает, что элемент справочника более не должен использоваться в системе, т.е. не доступен на полях ввода для вновь-создаваемых объектов
     *
     * @return Отключен. Свойство не может быть null.
     */
    @NotNull
    public boolean isDisabled()
    {
        return _disabled;
    }

    /**
     * @param disabled Отключен. Свойство не может быть null.
     */
    public void setDisabled(boolean disabled)
    {
        dirty(_disabled, disabled);
        _disabled = disabled;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Условное сокращение.
     */
    @Length(max=255)
    public String getAbbreviation()
    {
        return _abbreviation;
    }

    /**
     * @param abbreviation Условное сокращение.
     */
    public void setAbbreviation(String abbreviation)
    {
        dirty(_abbreviation, abbreviation);
        _abbreviation = abbreviation;
    }

    /**
     * @return Использовать в УП. Свойство не может быть null.
     */
    @NotNull
    public boolean isActiveInEduPlan()
    {
        return _activeInEduPlan;
    }

    /**
     * @param activeInEduPlan Использовать в УП. Свойство не может быть null.
     */
    public void setActiveInEduPlan(boolean activeInEduPlan)
    {
        dirty(_activeInEduPlan, activeInEduPlan);
        _activeInEduPlan = activeInEduPlan;
    }

    /**
     * @return Использование для дисциплин. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsedWithDisciplines()
    {
        return _usedWithDisciplines;
    }

    /**
     * @param usedWithDisciplines Использование для дисциплин. Свойство не может быть null.
     */
    public void setUsedWithDisciplines(boolean usedWithDisciplines)
    {
        dirty(_usedWithDisciplines, usedWithDisciplines);
        _usedWithDisciplines = usedWithDisciplines;
    }

    /**
     * @return Использование для практик. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsedWithPractice()
    {
        return _usedWithPractice;
    }

    /**
     * @param usedWithPractice Использование для практик. Свойство не может быть null.
     */
    public void setUsedWithPractice(boolean usedWithPractice)
    {
        dirty(_usedWithPractice, usedWithPractice);
        _usedWithPractice = usedWithPractice;
    }

    /**
     * @return Использование для ГИА. Свойство не может быть null.
     */
    @NotNull
    public boolean isUsedWithAttestation()
    {
        return _usedWithAttestation;
    }

    /**
     * @param usedWithAttestation Использование для ГИА. Свойство не может быть null.
     */
    public void setUsedWithAttestation(boolean usedWithAttestation)
    {
        dirty(_usedWithAttestation, usedWithAttestation);
        _usedWithAttestation = usedWithAttestation;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppControlActionTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EppControlActionType)another).getCode());
                setCatalogCode(((EppControlActionType)another).getCatalogCode());
            }
            setDisabled(((EppControlActionType)another).isDisabled());
            setShortTitle(((EppControlActionType)another).getShortTitle());
            setAbbreviation(((EppControlActionType)another).getAbbreviation());
            setActiveInEduPlan(((EppControlActionType)another).isActiveInEduPlan());
            setUsedWithDisciplines(((EppControlActionType)another).isUsedWithDisciplines());
            setUsedWithPractice(((EppControlActionType)another).isUsedWithPractice());
            setUsedWithAttestation(((EppControlActionType)another).isUsedWithAttestation());
            setTitle(((EppControlActionType)another).getTitle());
        }
    }

    public INaturalId<EppControlActionTypeGen> getNaturalId()
    {
        return new NaturalId(getCode(), getCatalogCode());
    }

    public static class NaturalId extends NaturalIdBase<EppControlActionTypeGen>
    {
        private static final String PROXY_NAME = "EppControlActionTypeNaturalProxy";

        private String _code;
        private String _catalogCode;

        public NaturalId()
        {}

        public NaturalId(String code, String catalogCode)
        {
            _code = code;
            _catalogCode = catalogCode;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getCatalogCode()
        {
            return _catalogCode;
        }

        public void setCatalogCode(String catalogCode)
        {
            _catalogCode = catalogCode;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppControlActionTypeGen.NaturalId) ) return false;

            EppControlActionTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            if( !equals(getCatalogCode(), that.getCatalogCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            result = hashCode(result, getCatalogCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            sb.append("/");
            sb.append(getCatalogCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppControlActionTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppControlActionType.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppControlActionType is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "catalogCode":
                    return obj.getCatalogCode();
                case "disabled":
                    return obj.isDisabled();
                case "shortTitle":
                    return obj.getShortTitle();
                case "abbreviation":
                    return obj.getAbbreviation();
                case "activeInEduPlan":
                    return obj.isActiveInEduPlan();
                case "usedWithDisciplines":
                    return obj.isUsedWithDisciplines();
                case "usedWithPractice":
                    return obj.isUsedWithPractice();
                case "usedWithAttestation":
                    return obj.isUsedWithAttestation();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "catalogCode":
                    obj.setCatalogCode((String) value);
                    return;
                case "disabled":
                    obj.setDisabled((Boolean) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "abbreviation":
                    obj.setAbbreviation((String) value);
                    return;
                case "activeInEduPlan":
                    obj.setActiveInEduPlan((Boolean) value);
                    return;
                case "usedWithDisciplines":
                    obj.setUsedWithDisciplines((Boolean) value);
                    return;
                case "usedWithPractice":
                    obj.setUsedWithPractice((Boolean) value);
                    return;
                case "usedWithAttestation":
                    obj.setUsedWithAttestation((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "catalogCode":
                        return true;
                case "disabled":
                        return true;
                case "shortTitle":
                        return true;
                case "abbreviation":
                        return true;
                case "activeInEduPlan":
                        return true;
                case "usedWithDisciplines":
                        return true;
                case "usedWithPractice":
                        return true;
                case "usedWithAttestation":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "catalogCode":
                    return true;
                case "disabled":
                    return true;
                case "shortTitle":
                    return true;
                case "abbreviation":
                    return true;
                case "activeInEduPlan":
                    return true;
                case "usedWithDisciplines":
                    return true;
                case "usedWithPractice":
                    return true;
                case "usedWithAttestation":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "catalogCode":
                    return String.class;
                case "disabled":
                    return Boolean.class;
                case "shortTitle":
                    return String.class;
                case "abbreviation":
                    return String.class;
                case "activeInEduPlan":
                    return Boolean.class;
                case "usedWithDisciplines":
                    return Boolean.class;
                case "usedWithPractice":
                    return Boolean.class;
                case "usedWithAttestation":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppControlActionType> _dslPath = new Path<EppControlActionType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppControlActionType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Код справочника. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#getCatalogCode()
     */
    public static PropertyPath<String> catalogCode()
    {
        return _dslPath.catalogCode();
    }

    /**
     * Показывает, что элемент справочника более не должен использоваться в системе, т.е. не доступен на полях ввода для вновь-создаваемых объектов
     *
     * @return Отключен. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#isDisabled()
     */
    public static PropertyPath<Boolean> disabled()
    {
        return _dslPath.disabled();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Условное сокращение.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#getAbbreviation()
     */
    public static PropertyPath<String> abbreviation()
    {
        return _dslPath.abbreviation();
    }

    /**
     * @return Использовать в УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#isActiveInEduPlan()
     */
    public static PropertyPath<Boolean> activeInEduPlan()
    {
        return _dslPath.activeInEduPlan();
    }

    /**
     * @return Использование для дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#isUsedWithDisciplines()
     */
    public static PropertyPath<Boolean> usedWithDisciplines()
    {
        return _dslPath.usedWithDisciplines();
    }

    /**
     * @return Использование для практик. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#isUsedWithPractice()
     */
    public static PropertyPath<Boolean> usedWithPractice()
    {
        return _dslPath.usedWithPractice();
    }

    /**
     * @return Использование для ГИА. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#isUsedWithAttestation()
     */
    public static PropertyPath<Boolean> usedWithAttestation()
    {
        return _dslPath.usedWithAttestation();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EppControlActionType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _catalogCode;
        private PropertyPath<Boolean> _disabled;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _abbreviation;
        private PropertyPath<Boolean> _activeInEduPlan;
        private PropertyPath<Boolean> _usedWithDisciplines;
        private PropertyPath<Boolean> _usedWithPractice;
        private PropertyPath<Boolean> _usedWithAttestation;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EppControlActionTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Код справочника. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#getCatalogCode()
     */
        public PropertyPath<String> catalogCode()
        {
            if(_catalogCode == null )
                _catalogCode = new PropertyPath<String>(EppControlActionTypeGen.P_CATALOG_CODE, this);
            return _catalogCode;
        }

    /**
     * Показывает, что элемент справочника более не должен использоваться в системе, т.е. не доступен на полях ввода для вновь-создаваемых объектов
     *
     * @return Отключен. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#isDisabled()
     */
        public PropertyPath<Boolean> disabled()
        {
            if(_disabled == null )
                _disabled = new PropertyPath<Boolean>(EppControlActionTypeGen.P_DISABLED, this);
            return _disabled;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EppControlActionTypeGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Условное сокращение.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#getAbbreviation()
     */
        public PropertyPath<String> abbreviation()
        {
            if(_abbreviation == null )
                _abbreviation = new PropertyPath<String>(EppControlActionTypeGen.P_ABBREVIATION, this);
            return _abbreviation;
        }

    /**
     * @return Использовать в УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#isActiveInEduPlan()
     */
        public PropertyPath<Boolean> activeInEduPlan()
        {
            if(_activeInEduPlan == null )
                _activeInEduPlan = new PropertyPath<Boolean>(EppControlActionTypeGen.P_ACTIVE_IN_EDU_PLAN, this);
            return _activeInEduPlan;
        }

    /**
     * @return Использование для дисциплин. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#isUsedWithDisciplines()
     */
        public PropertyPath<Boolean> usedWithDisciplines()
        {
            if(_usedWithDisciplines == null )
                _usedWithDisciplines = new PropertyPath<Boolean>(EppControlActionTypeGen.P_USED_WITH_DISCIPLINES, this);
            return _usedWithDisciplines;
        }

    /**
     * @return Использование для практик. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#isUsedWithPractice()
     */
        public PropertyPath<Boolean> usedWithPractice()
        {
            if(_usedWithPractice == null )
                _usedWithPractice = new PropertyPath<Boolean>(EppControlActionTypeGen.P_USED_WITH_PRACTICE, this);
            return _usedWithPractice;
        }

    /**
     * @return Использование для ГИА. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#isUsedWithAttestation()
     */
        public PropertyPath<Boolean> usedWithAttestation()
        {
            if(_usedWithAttestation == null )
                _usedWithAttestation = new PropertyPath<Boolean>(EppControlActionTypeGen.P_USED_WITH_ATTESTATION, this);
            return _usedWithAttestation;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppControlActionType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppControlActionTypeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EppControlActionType.class;
        }

        public String getEntityName()
        {
            return "eppControlActionType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
