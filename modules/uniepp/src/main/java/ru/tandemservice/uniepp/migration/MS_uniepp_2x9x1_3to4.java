/* $Id$ */
package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;

/**
 * @author Alexey Lopatin
 * @since 05.10.2015
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x9x1_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Long groupTypeControlWorkId = (Long) tool.getUniqueResult("select id from epp_group_type_t where code_p = ?", "fca.controlWork");

        if (null == groupTypeControlWorkId)
        {
            Number priority113 = (Number) tool.getUniqueResult("select id from epp_group_type_t where priority_p = ?", 113);

            int priority = 113;
            if (null != priority113)
                priority = ((Integer) tool.getUniqueResult("select max(priority_p) from epp_group_type_t")) + 1;

            PreparedStatement insert = tool.prepareStatement("insert into epp_group_type_t (id, discriminator, code_p, priority_p, title_p, shorttitle_p, abbreviation_p) values (?, ? ,?, ?, ?, ?, ?)");

            short entityCode = tool.entityCodes().ensure("eppGroupTypeFCA");
            groupTypeControlWorkId = EntityIDGenerator.generateNewId(entityCode);

            insert.setLong(1, groupTypeControlWorkId);
            insert.setShort(2, entityCode);
            insert.setString(3, "fca.controlWork");
            insert.setInt(4, priority);
            insert.setString(5, "Контрольная работа");
            insert.setString(6, "КтР");
            insert.setString(7, "КтР");
            insert.execute();
        }

        Long actionTypeControlWorkId = (Long) tool.getUniqueResult("select id from epp_c_cactiontype_t where code_p = ? and catalogcode_p = ?", "07", "eppFControlActionType");
        if (null == actionTypeControlWorkId)
        {
            Number priority7 = (Number) tool.getUniqueResult("select id from epp_c_cactiontype_f_t where priority_p = ? or totalmarkpriority_p = ?", 7, 7);

            int priority = 7;
            if (null != priority7)
                priority = ((Integer) tool.getUniqueResult("select max(priority_p) from epp_c_cactiontype_f_t")) + 1;

            Long groupId = (Long) tool.getUniqueResult("select id from epp_c_fcactiongroup_t where code_p = ?", "2");  // Зачет
            Long gradeScaleId = (Long) tool.getUniqueResult("select id from epp_c_gradescale_t where code_p = ?", "scale5"); // Пятибалльная

            PreparedStatement insert = tool.prepareStatement("insert into epp_c_cactiontype_t (id, discriminator, code_p, catalogcode_p, title_p, shorttitle_p, abbreviation_p, " +
                                                                     "activeineduplan_p, usedwithdisciplines_p, usedwithpractice_p, usedwithattestation_p, disabled_p) " +
                                                                     "values (?, ? ,?, ?, ?, ?, ?, ? ,?, ?, ?, ?)");

            PreparedStatement insert2 = tool.prepareStatement("insert into epp_c_cactiontype_f_t (id, priority_p, totalmarkpriority_p, defaultgradescale_id, group_id, eppgrouptype_id) values (?, ? ,?, ?, ?, ?)");

            short entityCode = tool.entityCodes().ensure("eppFControlActionType");
            actionTypeControlWorkId = EntityIDGenerator.generateNewId(entityCode);

            insert.setLong(1, actionTypeControlWorkId);
            insert.setShort(2, entityCode);
            insert.setString(3, "07");
            insert.setString(4, "eppFControlActionType");
            insert.setString(5, "Контрольная работа");
            insert.setString(6, "КтР");
            insert.setString(7, "КтР");
            insert.setBoolean(8, true);
            insert.setBoolean(9, true);
            insert.setBoolean(10, false);
            insert.setBoolean(11, false);
            insert.setBoolean(12, false);
            insert.execute();

            insert2.setLong(1, actionTypeControlWorkId);
            insert2.setInt(2, priority);
            insert2.setInt(3, priority);
            insert2.setLong(4, gradeScaleId);
            insert2.setLong(5, groupId);
            insert2.setLong(6, groupTypeControlWorkId);
            insert2.execute();
        }
    }
}
