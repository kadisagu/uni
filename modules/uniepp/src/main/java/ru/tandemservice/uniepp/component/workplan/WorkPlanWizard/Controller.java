package ru.tandemservice.uniepp.component.workplan.WorkPlanWizard;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.wizard.SimpleWizardBase.SimpleWizardControllerBase;
import ru.tandemservice.uni.util.AddEditResult;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

import java.util.Map;

/**
 * @author vdanilov
 */
public class Controller extends SimpleWizardControllerBase<IDAO, Model> {

    private static final String STEP_0 = ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit.Model.COMPONENT_NAME;
    private static final String STEP_0_RETURN_WORKPLAN_ID = ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit.Model.RETURN_WORKPLAN_ID;

    private static final String STEP_1 = ru.tandemservice.uniepp.component.workplan.WorkPlanPartSetEdit.Model.COMPONENT_NAME;
    private static final String STEP_1_RETURN_SUCCESS_PARAM = ru.tandemservice.uniepp.component.workplan.WorkPlanPartSetEdit.Model.RETURN_SUCCESS_PARAM;

    private static final String STEP_2 = ru.tandemservice.uniepp.component.workplan.WorkPlanFillDiscipline.Model.COMPONENT_NAME;
    private static final String STEP_2_RETURN_SUCCESS_PARAM = ru.tandemservice.uniepp.component.workplan.WorkPlanFillDiscipline.Model.RETURN_SUCCESS_PARAM;

    @Override
    public void onActivateComponent(final IBusinessComponent component) {
        super.onActivateComponent(component);
        this.registerStep(component, Controller.STEP_0, "Данные рабочего учебного плана");
        this.registerStep(component, Controller.STEP_1, "Распределение недель в частях");
        this.registerStep(component, Controller.STEP_2, "Формирование содержимого рабочего учебного плана");
    }

    @Override
    protected Map<String, Object> fillStepReturnValueMap(final IBusinessComponent component, final String stepName, Map<String, Object> value, final Map<String, Object> returnedData) {
        value = super.fillStepReturnValueMap(component, stepName, value, returnedData);

        if (Controller.STEP_0.equals(stepName))
        {
            final Object id = returnedData.get(Controller.STEP_0_RETURN_WORKPLAN_ID);
            value.put(PublisherActivator.PUBLISHER_ID_KEY, id);

            // если форма добавления РУП ничего не вернула - то закрываем весь визард
            final IEntityMeta meta = (id instanceof Long ? EntityRuntime.getMeta((Long)id) : null);
            final boolean die = ((null == meta) || !EppWorkPlanBase.class.isAssignableFrom(meta.getEntityClass()));
            value.put(SimpleWizardControllerBase.DIE, die);
        }

        else if (Controller.STEP_1.equals(stepName))
        {
            // если форма закрыта нештатным методом - убиваем визард
            final Object success = returnedData.get(Controller.STEP_1_RETURN_SUCCESS_PARAM);
            value.put(SimpleWizardControllerBase.DIE, null == success);
        }

        else if (Controller.STEP_2.equals(stepName))
        {
            // если форма закрыта нештатным методом - убиваем визард
            final Object success = returnedData.get(Controller.STEP_2_RETURN_SUCCESS_PARAM);
            value.put(SimpleWizardControllerBase.DIE, null == success);
        }

        return value;
    }

    @Override
    public void deactivate(final IBusinessComponent component, final Map<String, Object> parameters)
    {
        final Long id = this.getModel(component).getId();
        if (null == id) {
            // ничего не создали
            super.deactivate(
                    component,
                    parameters
            );
        } else {
            // создали новый РУП
            super.deactivate(
                    component,
                    new ParametersMap().addAll(parameters).add(AddEditResult.PARAM, new AddEditResult(true, id))
            );
        }
    }

}
