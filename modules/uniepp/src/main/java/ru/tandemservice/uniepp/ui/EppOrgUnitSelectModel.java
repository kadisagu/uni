package ru.tandemservice.uniepp.ui;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.tandemframework.core.entity.dsl.IPropertyPath;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;

/**
 * @author vdanilov
 */
public class EppOrgUnitSelectModel extends DQLFullCheckSelectModel
{

    public EppOrgUnitSelectModel() {
        this(OrgUnit.title(), OrgUnit.orgUnitType().title());
    }

    public EppOrgUnitSelectModel(final IPropertyPath... labelProperties) {
        super(labelProperties);
    }

    @Override
    protected DQLSelectBuilder query(final String alias, final String filter) {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnit.class, alias);
        if (null != filter) {
            dql.where(or(
                    this.like(OrgUnitGen.title().fromAlias(alias), filter),
                    this.like(OrgUnitGen.shortTitle().fromAlias(alias), filter),
                    this.like(OrgUnitGen.fullTitle().fromAlias(alias), filter)
            ));
        }
        dql.order(property(OrgUnitGen.title().fromAlias(alias)));
        return dql;
    }

}
