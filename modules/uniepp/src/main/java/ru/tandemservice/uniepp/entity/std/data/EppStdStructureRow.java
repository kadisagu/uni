package ru.tandemservice.uniepp.entity.std.data;

import org.apache.commons.lang.StringUtils;

import ru.tandemservice.uniepp.dao.index.IEppStructureIndexedRow;
import ru.tandemservice.uniepp.entity.std.data.gen.EppStdStructureRowGen;

/**
 * Запись ГОС (индекс, цикл, компонент, ...)
 */
public class EppStdStructureRow extends EppStdStructureRowGen implements IEppStructureIndexedRow
{
    @Override public int getComparatorClassIndex() { return 1; }

    private String _comparator_string_cache;
    @Override public String getComparatorString() {
        if (null != this._comparator_string_cache) { return this._comparator_string_cache; }

        final StringBuilder sb = new StringBuilder();
        for(final Integer i : this.getValue().getPriorityTrace()) {
            if (sb.length() > 0) { sb.append(':'); }
            sb.append(StringUtils.leftPad(String.valueOf(i), 2, '0'));
        }

        return (this._comparator_string_cache = sb.toString());
    }


    @Override public EppStdStructureRow getHierarhyParent() { return this.getParent(); }
    @Override public void setHierarhyParent(final EppStdRow parent) { this.setParent((EppStdStructureRow) parent); }

    @Override public String getTitle() {
        if (getValue() == null) {
            return this.getClass().getSimpleName();
        }
        return this.getValue().getTitle();
    }
    @Override public String getSelfIndexPart() { return this.getValue().getShortTitle(); }

    @Override public boolean isStructureElement() { return true; }

}