package ru.tandemservice.uniepp.component.base.ChangeState;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickChangeState(final IBusinessComponent component) {
        try {
            this.getDao().changeState(this.getModel(component), component.<String>getListenerParameter());
        } finally {
            ContextLocal.getDesktop().setRefreshScheduled(true);
        }
    }

}
