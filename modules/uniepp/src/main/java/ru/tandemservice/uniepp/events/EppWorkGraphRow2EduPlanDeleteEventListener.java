/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.events;

import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import org.tandemframework.hibsupport.event.single.listener.IHibernateEventListener;
import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;

import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRow2EduPlanGen;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRowGen;

/**
 * @author vip_delete
 * @since 10.03.2010
 */
public class EppWorkGraphRow2EduPlanDeleteEventListener implements IHibernateEventListener<ISingleEntityEvent>
{

    private static final TransactionCompleteAction<Void> action = new TransactionCompleteAction<Void>() {
        @Override public Void beforeCompletion(final Session session) {
            final MQBuilder builder = new MQBuilder(EppWorkGraphRowGen.ENTITY_CLASS, "r");
            builder.add(MQExpression.notIn("r", IEntity.P_ID, new MQBuilder(EppWorkGraphRow2EduPlanGen.ENTITY_CLASS, "rel", new String[]{EppWorkGraphRow2EduPlanGen.L_ROW})));
            for (final EppWorkGraphRow row : builder.<EppWorkGraphRow>getResultList(session)) {
                session.delete(row);
            }

            // нужно, потому что инече данные в базу не попадут
            session.flush();

            return null;
        }
    };

    @Override
    public void onEvent(final ISingleEntityEvent event) {
        if (event.getEntity() instanceof EppWorkGraphRow2EduPlan) {
            EppWorkGraphRow2EduPlanDeleteEventListener.action.register(event.getSession());
        }
    }
}
