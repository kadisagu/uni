/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.logic.List;

import org.hibernate.Session;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLSubselectType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Katkov
 * @since 01.03.2016
 */
public class EppWorkPlanListDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String PARAM_NUMBER = "number";
    public static final String PARAM_YEAR_EDUCATION_PROCESS = "yearEducationProcess";
    public static final String PARAM_PROGRAM_SUBJECT = "programSubject";
    public static final String PARAM_PROGRAM_SPECIALIZATION_LIST = "programSpecializationList";
    public static final String PARAM_PROGRAM_FORM_LIST = "programFormList";
    public static final String PARAM_DEVELOP_CONDITION_LIST = "developConditionList";
    public static final String PARAM_PROGRAM_TRAIT_LIST = "programTraitList";
    public static final String PARAM_TERM = "term";
    public static final String PARAM_STATE = "state";
    public static final String PARAM_PROGRAM_KIND = "programKind";

    public EppWorkPlanListDSHandler(final String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Session session = context.getSession();
        String alias = "workplan";
        DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(EppWorkPlan.class, alias);
        DQLSelectBuilder dql = registry.buildDQLSelectBuilder();
        applyFilters(dql, alias, context);
        registry.applyOrder(dql, input.getEntityOrder());
        dql.column(alias);

        DSOutput output = DQLSelectOutputBuilder.get(input, dql, session).build();
        return output;
    }

    protected void applyFilters(DQLSelectBuilder dql, String alias, ExecutionContext context)
    {
        FilterUtils.applySelectFilter(dql, EppWorkPlan.number().fromAlias(alias), context.get(PARAM_NUMBER));
        FilterUtils.applySelectFilter(dql, EppWorkPlan.year().fromAlias(alias), context.get(PARAM_YEAR_EDUCATION_PROCESS));
        dql.fetchPath(DQLJoinType.inner, EppWorkPlan.parent().fromAlias(alias), "epvBlock");
        dql.fetchPath(DQLJoinType.inner, EppEduPlanVersionBlock.eduPlanVersion().fromAlias("epvBlock"), "epv");
        dql.fetchPath(DQLJoinType.inner, EppEduPlanVersion.eduPlan().fromAlias("epv"), "ep");
        dql.where(eq(property("ep", EppEduPlan.programKind()), commonValue(context.get(PARAM_PROGRAM_KIND))));

        EduProgramSubject programSubject = context.get(PARAM_PROGRAM_SUBJECT);
        if (programSubject != null) {
            dql.fromEntity(EppEduPlanProf.class, "prof");
            dql.where(eq(property("ep"), property("prof")));
            dql.where(eq(property("prof", EppEduPlanProf.programSubject()), value(programSubject)));
        }

        Collection<EduProgramSpecialization> programSpecializations = context.get(PARAM_PROGRAM_SPECIALIZATION_LIST);
        if (programSpecializations != null && !programSpecializations.isEmpty()) {
            dql.where(eqSubquery(property("epvBlock.id"), DQLSubselectType.any,
                    new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, "bs")
                            .column("bs.id")
                            .where(in(property("bs", EppEduPlanVersionSpecializationBlock.programSpecialization()), programSpecializations))
                            .buildQuery()
            ));
        }

        FilterUtils.applySelectFilter(dql, EppEduPlan.programForm().fromAlias("ep"), context.get(PARAM_PROGRAM_FORM_LIST));
        FilterUtils.applySelectFilter(dql, EppEduPlan.developCondition().fromAlias("ep"), context.get(PARAM_DEVELOP_CONDITION_LIST));
        FilterUtils.applySelectFilter(dql, EppEduPlan.programTrait().fromAlias("ep"), context.get(PARAM_PROGRAM_TRAIT_LIST));
        FilterUtils.applySelectFilter(dql, EppWorkPlan.term().fromAlias(alias), context.get(PARAM_TERM));
        FilterUtils.applySelectFilter(dql, EppWorkPlan.state().fromAlias(alias), context.get(PARAM_STATE));
        OrgUnit orgUnit = context.get("orgUnit");
        applyOrgUnitFilter(dql, orgUnit, alias);
    }

    protected void applyOrgUnitFilter(DQLSelectBuilder dql, OrgUnit orgUnit, String alias)
    {
        if (null != orgUnit) {
            dql.where(or(
                    eq(property(EppWorkPlan.parent().eduPlanVersion().eduPlan().owner().fromAlias(alias)), value(orgUnit)),
                    exists(new DQLSelectBuilder()
                            .fromEntity(EppEduPlanVersionSpecializationBlock.class, "sb")
                            .column(property("sb.id"))
                            .where(eq(property(EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit().fromAlias("sb")), value(orgUnit)))
                            .where(eq(property("sb"), property("epvBlock")))
                            .buildQuery()),
                    exists(new DQLSelectBuilder()
                            .fromEntity(EppStudent2EduPlanVersion.class, "s2epv")
                            .joinPath(DQLJoinType.inner, EppStudent2EduPlanVersion.student().educationOrgUnit().fromAlias("s2epv"), "eduOU")
                            .column(property("eduOU.id"))
                            .where(or(
                                    eq(property(EducationOrgUnit.groupOrgUnit().fromAlias("eduOU")), value(orgUnit)),
                                    eq(property(EducationOrgUnit.operationOrgUnit().fromAlias("eduOU")), value(orgUnit)),
                                    eq(property(EducationOrgUnit.formativeOrgUnit().fromAlias("eduOU")), value(orgUnit)),
                                    eq(property(EducationOrgUnit.territorialOrgUnit().fromAlias("eduOU")), value(orgUnit)),
                                    eq(property(EducationOrgUnit.educationLevelHighSchool().orgUnit().fromAlias("eduOU")), value(orgUnit))
                            ))
                            .where(eq(property("s2epv", EppStudent2EduPlanVersion.eduPlanVersion()), property("epv")))
                            .where(isNull(property("s2epv", EppStudent2EduPlanVersion.removalDate())))
                            .buildQuery())
            ));
        }
    }
}