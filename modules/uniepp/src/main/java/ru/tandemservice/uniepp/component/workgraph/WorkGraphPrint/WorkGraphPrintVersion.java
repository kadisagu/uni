package ru.tandemservice.uniepp.component.workgraph.WorkGraphPrint;

import java.util.List;
import java.util.Map;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;

/**
 * @author nkokorina
 */

public class WorkGraphPrintVersion
{
    private EppWorkGraph workGraph;

    private EppYearEducationWeek[] weeks;
    // id таблицы(специаль) -> название
    private Map<Long, String> id2tableTitle;
    // id таблицы(специаль) -> { id курса -> упорядоченный по номеру список недель}
    private Map<Long, Map<Long, List<String>>> id2idRow2weeksList;
    // { id курса -> название}
    private Map<Long, String> idRow2title;

    // данные о названии и руководителе ОУ
    private String academyTitle;
    private EmployeePost academyHead;

    // сокращение для типа недели - цвет
    Map<String, Long> weekType2color;
    // сокращение для типа недели - полное название
    Map<String, String> weekType2fullTitle;

    public EppWorkGraph getWorkGraph()
    {
        return this.workGraph;
    }

    public void setWorkGraph(final EppWorkGraph workGraph)
    {
        this.workGraph = workGraph;
    }

    public EppYearEducationWeek[] getWeeks()
    {
        return this.weeks;
    }

    public void setWeeks(final EppYearEducationWeek[] weeks)
    {
        this.weeks = weeks;
    }

    public Map<Long, String> getId2tableTitle()
    {
        return this.id2tableTitle;
    }

    public void setId2tableTitle(final Map<Long, String> id2tableTitle)
    {
        this.id2tableTitle = id2tableTitle;
    }

    public String getAcademyTitle()
    {
        return this.academyTitle;
    }

    public void setAcademyTitle(final String academyTitle)
    {
        this.academyTitle = academyTitle;
    }

    public EmployeePost getAcademyHead()
    {
        return this.academyHead;
    }

    public void setAcademyHead(final EmployeePost academyHead)
    {
        this.academyHead = academyHead;
    }

    public Map<String, Long> getWeekType2color()
    {
        return this.weekType2color;
    }

    public void setWeekType2color(final Map<String, Long> weekType2color)
    {
        this.weekType2color = weekType2color;
    }

    public Map<String, String> getWeekType2fullTitle()
    {
        return this.weekType2fullTitle;
    }

    public void setWeekType2fullTitle(final Map<String, String> weekType2fullTitle)
    {
        this.weekType2fullTitle = weekType2fullTitle;
    }

    public Map<Long, Map<Long, List<String>>> getId2idRow2weeksList()
    {
        return this.id2idRow2weeksList;
    }

    public void setId2idRow2weeksList(final Map<Long, Map<Long, List<String>>> id2idRow2weeksList)
    {
        this.id2idRow2weeksList = id2idRow2weeksList;
    }

    public Map<Long, String> getIdRow2title()
    {
        return this.idRow2title;
    }

    public void setIdRow2title(final Map<Long, String> idRow2title)
    {
        this.idRow2title = idRow2title;
    }

}
