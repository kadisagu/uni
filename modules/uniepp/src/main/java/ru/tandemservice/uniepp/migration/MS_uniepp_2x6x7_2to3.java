/* $Id:$ */
package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author oleyba
 * @since 9/15/14
 */
public class MS_uniepp_2x6x7_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.7")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (!tool.tableExists("epp_c_indicator_component_t")) return;

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppIndicatorComponent

		// сущность была удалена
		{
			// удалить таблицу
			tool.dropTable("epp_c_indicator_component_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("eppIndicatorComponent");
		}
    }
}
