package ru.tandemservice.uniepp.component.workplan.WorkPlanData;

import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;

/**
 * @author nkokorina
 * Created on: 05.08.2010
 */
public abstract class WorkPlanDataBaseModel
{

    protected abstract Long getWorkPlanId();
    protected abstract boolean isEditable();
    protected abstract String getPermissionKeyEdit();

    public abstract boolean isNotCustom();

    private StaticListDataSource<IEppWorkPlanRowWrapper> disciplineDataSource = new StaticListDataSource<>();
    public StaticListDataSource<IEppWorkPlanRowWrapper> getDisciplineDataSource() { return this.disciplineDataSource; }
    public void setDisciplineDataSource(final StaticListDataSource<IEppWorkPlanRowWrapper> disciplineDataSource) { this.disciplineDataSource = disciplineDataSource; }

    private StaticListDataSource<IEppWorkPlanRowWrapper> actionDataSource = new StaticListDataSource<>();
    public StaticListDataSource<IEppWorkPlanRowWrapper> getActionDataSource() { return this.actionDataSource; }
    public void setActionDataSource(final StaticListDataSource<IEppWorkPlanRowWrapper> actionDataSource) { this.actionDataSource = actionDataSource; }

}
