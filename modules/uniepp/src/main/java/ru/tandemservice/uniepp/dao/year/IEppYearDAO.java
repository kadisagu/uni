package ru.tandemservice.uniepp.dao.year;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;

/**
 * Операции с ПУПнаГ
 *
 * @author vdanilov
 */
public interface IEppYearDAO
{
    public static final SpringBeanCache<IEppYearDAO> instance = new SpringBeanCache<IEppYearDAO>(IEppYearDAO.class.getName());

    /**
     * Создает или обновляет список учебных недель укзанного учебного года (ПУПнаГ)
     *
     * @param pupnagId идентификатор EppYearEducationProcess
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doRefreshYearEducationWeeks(Long pupnagId);

    /**
     * @param pupnagId идентификатор EppYearEducationProcess
     * @return массив недель в ПУПнаГ (всегда 52 не null элемента)
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    EppYearEducationWeek[] getYearEducationWeeks(Long pupnagId);
}
