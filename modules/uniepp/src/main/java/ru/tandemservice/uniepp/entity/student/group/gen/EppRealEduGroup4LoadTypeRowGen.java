package ru.tandemservice.uniepp.entity.student.group.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись студента в УГС (по виду аудиторной нагрузки)
 *
 * Связь записи студента с УГС-по-ВАН
 * @warn ссылки на этот объект должны быть автоудаляемыми (т.к. неактуальная запись может быть удалена в любое время демоном)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRealEduGroup4LoadTypeRowGen extends EppRealEduGroupRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow";
    public static final String ENTITY_NAME = "eppRealEduGroup4LoadTypeRow";
    public static final int VERSION_HASH = -566530334;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppRealEduGroup4LoadTypeRowGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRealEduGroup4LoadTypeRowGen> extends EppRealEduGroupRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRealEduGroup4LoadTypeRow.class;
        }

        public T newInstance()
        {
            return (T) new EppRealEduGroup4LoadTypeRow();
        }
    }
    private static final Path<EppRealEduGroup4LoadTypeRow> _dslPath = new Path<EppRealEduGroup4LoadTypeRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRealEduGroup4LoadTypeRow");
    }
            

    public static class Path<E extends EppRealEduGroup4LoadTypeRow> extends EppRealEduGroupRow.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EppRealEduGroup4LoadTypeRow.class;
        }

        public String getEntityName()
        {
            return "eppRealEduGroup4LoadTypeRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
