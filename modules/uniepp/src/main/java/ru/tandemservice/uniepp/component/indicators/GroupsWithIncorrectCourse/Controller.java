/* $Id: Controller.java 11311 2010-02-03 11:27:19Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniepp.component.indicators.GroupsWithIncorrectCourse;

import java.util.Map;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.entity.orgstruct.Group;

/**
 * @author vip_delete
 */
public class Controller extends ru.tandemservice.uni.component.group.AbstractGroupList.Controller
{
    @Override
    protected String getSettingsKey()
    {
        return "epp.GroupsWithIncorrectCourse.filter";
    }

    @Override protected boolean isShowActiveStudentColumn() {
        return true;
    }
    @Override protected boolean isListAccessable(final IBusinessComponent component) {
        return false;
    }

    @Override
    protected DynamicListDataSource<Group> createNewDataSource(final IBusinessComponent component) {
        final DynamicListDataSource<Group> dataSource = super.createNewDataSource(component);

        // TODO: сделать индикатор
        dataSource.addColumn(new SimpleColumn("Распределение студентов по курсам", "courseDistributionMap") {
            @SuppressWarnings("unchecked")
            @Override public String getContent(final IEntity entity) {
                final Map<Integer, Integer> map = (Map<Integer, Integer>) entity.getProperty(this.getName());
                final int course = (Integer)entity.getProperty("course.intValue");
                final StringBuilder sb = new StringBuilder();
                for (final Map.Entry<Integer, Integer> e: map.entrySet()) {
                    sb.append("<div><nobr");
                    if (e.getKey() < (course-1)) {
                        sb.append(" style=\"color:#ff0000;font-weight:bold\"");
                    } else if (e.getKey() < course) {
                        sb.append(" style=\"color:#af0000\"");
                    } else if (e.getKey() > (course+1)) {
                        sb.append(" style=\"color:#ffff00;font-weight:bold\"");
                    } else if (e.getKey() > course) {
                        sb.append(" style=\"color:#5f5f00\"");
                    }
                    sb.append(">");
                    sb.append("курс ").append(e.getKey()).append(": ").append(e.getValue()).append(" ст.");
                    sb.append("</nobr></div>");
                }
                return sb.toString();
            }
            @Override public boolean isRawContent() {
                return true;
            };
        }.setOrderable(false).setClickable(false).setRequired(true));

        return dataSource;
    }

}
