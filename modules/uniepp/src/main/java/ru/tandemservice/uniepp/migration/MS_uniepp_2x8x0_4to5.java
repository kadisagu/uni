/* $Id$ */
package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.commonbase.utils.MigrationUtils.BatchUpdater;
import ru.tandemservice.uniepp.sec.UnieppStateChangePermissionModifier;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 28.04.2015
 */
public class MS_uniepp_2x8x0_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.17"),
                new ScriptDependency("org.tandemframework.shared", "1.8.0")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Переименовать права надобно
        final List<Object[]> statePermissionNames = tool.executeQuery(MigrationUtils.processor(String.class), "select permissionsuffix_p from epp_c_state_t");
        final BatchUpdater updater = new BatchUpdater("update accessmatrix_t set permissionkey_p=? where permissionkey_p=?", DBType.EMPTY_STRING, DBType.EMPTY_STRING);
        for (Object[] item : statePermissionNames)
        {
            final String suffix = (String) item[0];
            updater.addBatch("changeStateTo_" + item[0] + "_" + UnieppStateChangePermissionModifier.POSTFIX_WORK_PLAN_LIST, "changeStateTo_" + suffix + "_eppWorkPlanLPG");
        }
        updater.executeUpdate(tool);
    }
}