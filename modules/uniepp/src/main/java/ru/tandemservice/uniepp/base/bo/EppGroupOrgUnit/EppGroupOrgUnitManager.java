/**
 *$Id: EppEduPlanVersionManager.java 26089 2013-02-11 08:34:37Z vdanilov $
 */
package ru.tandemservice.uniepp.base.bo.EppGroupOrgUnit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

@Configuration
public class EppGroupOrgUnitManager extends BusinessObjectManager
{
    public static EppGroupOrgUnitManager instance()
    {
        return instance(EppGroupOrgUnitManager.class);
    }
}
