/* $Id:$ */
package ru.tandemservice.uniepp.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.uni.component.reports.FormativeAndTerritorialOrgUnitReportVisibleResolver;
import ru.tandemservice.uniepp.util.GroupOrgUnitReportVisibleResolver;
import ru.tandemservice.uniepp.util.TutorOrgUnitVisibleResolver;

/**
 * @author rsizonenko
 * @since 05.11.2015
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager {

    /** Код блока отчетов на подразделении "Отчеты модуля «Договоры на обучение»" **/
    public static final String UNIEPP_ORG_UNIT_STUDENT_REPORT_BLOCK = "uniEppOrgUnitStudentReportBlock";
    /** Код блока отчетов на подразделении "Показатели читающего подразделения" **/
    public static final String UNIEPP_TUTOR_ORG_UNIT_INDICATOR_BLOCK = "uniEppTutorOrgUnitIndicatorBlock";
    /** Код блока отчетов на подразделении "Показатели деканата" **/
    public static final String UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK = "uniEppGroupOrgUnitIndicatorBlock";

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;


    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {

        final IItemListExtensionBuilder<OrgUnitReportBlockDefinition> builder = itemListExtension(_orgUnitReportManager.blockListExtPoint());

        return builder
                .add(UNIEPP_ORG_UNIT_STUDENT_REPORT_BLOCK, new OrgUnitReportBlockDefinition("Отчеты модуля «Договоры на обучение»", UNIEPP_ORG_UNIT_STUDENT_REPORT_BLOCK, new FormativeAndTerritorialOrgUnitReportVisibleResolver()))
                .add(UNIEPP_TUTOR_ORG_UNIT_INDICATOR_BLOCK, new OrgUnitReportBlockDefinition("Показатели читающего подразделения", UNIEPP_TUTOR_ORG_UNIT_INDICATOR_BLOCK, "eppTutorOuIndicatorBlockView", new TutorOrgUnitVisibleResolver()))
                .add(UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK, new OrgUnitReportBlockDefinition("Показатели деканата", UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK, "eppGroupOuIndicatorBlockView", new GroupOrgUnitReportVisibleResolver()))
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {

        final IItemListExtensionBuilder<OrgUnitReportDefinition> itemList = itemListExtension(_orgUnitReportManager.reportListExtPoint());

        return itemList
                .add("eppRegistryCheckOUIndicator", new OrgUnitReportDefinition("Проверка реестра мероприятий", "eppRegistryCheckOUIndicator", UNIEPP_TUTOR_ORG_UNIT_INDICATOR_BLOCK, "ru.tandemservice.uniepp.component.indicators.EppRegistryCheck"))
                .add("eppStudentsWithoutWPDSlotGroupOUIndicator", new OrgUnitReportDefinition("Студенты без РУП на текущий учебный год", "eppStudentsWithoutWPDSlotGroupOUIndicator", UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK, "EppIndicatorStudentsWithoutWPDSlots"))
                .add("eppStudentsWithNoLevelREGGroupOUIndicator", new OrgUnitReportDefinition("Студенты в УГС без согласования", "eppStudentsWithNoLevelREGGroupOUIndicator", UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK, "EppGroupOrgUnitStudentsInNewGrpsList"))
                .add("eppStudentWithoutEduPlansIndicator", new OrgUnitReportDefinition("Студенты без УП", "eppStudentWithoutEduPlansIndicator", UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK, "EppIndicatorStudentsWithoutEduPlans"))
                .add("eppStudentsWithoutEduPlanBlockIndicator", new OrgUnitReportDefinition("Студенты, для которых не указан блок УП", "eppStudentsWithoutEduPlanBlockIndicator", UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK, "EppIndicatorStudentsWithoutEduPlanBlock"))
                .add("eppStudentsWithDisciplineGaps", new OrgUnitReportDefinition("Пробелы в прохождении дисциплин", "eppStudentsWithDisciplineGaps", UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK, "EppIndicatorStudentsWithDisciplineGaps"))
                .add("eppStudentsWithWrongEduPlan", new OrgUnitReportDefinition("Студенты с УП, не соответствующими параметрам обучения", "eppStudentsWithWrongEduPlan", UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK, "EppIndicatorStudentsWithWrongEduPlan"))
                .add("eppStudentWithIncorrectWorkPlan", new OrgUnitReportDefinition("Студенты с некорректными РУП на текущий учебный год", "eppStudentWithIncorrectWorkPlan", UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK, "EppIndicatorStudentWithIncorrectWorkPlan"))
                .add("eppWorkPlanCheckPrint", new OrgUnitReportDefinition("Отчет о мероприятиях РУП", "eppWorkPlanCheckPrint", UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK, "EppWorkPlanCheckPrint"))
                .create();
    }

}
