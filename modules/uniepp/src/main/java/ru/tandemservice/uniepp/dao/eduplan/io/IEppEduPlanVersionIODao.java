package ru.tandemservice.uniepp.dao.eduplan.io;

import com.healthmarketscience.jackcess.Database;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.util.Collection;
import java.util.Map;

/**
 * @author vdanilov
 */
public interface IEppEduPlanVersionIODao {

    SpringBeanCache<IEppEduPlanVersionIODao> instance = new SpringBeanCache<>(IEppEduPlanVersionIODao.class.getName());

    /**
     * Выгружает в mdb данные по УП(в), их блокам и данные строк, если требуется
     * @param epvIds список УП(в), для которых требуется осуществить выгрузку
     * @param exportRowData true, если требуется выгружать данные строк
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void export_EppEduPlanVersionList(Database mdb, Collection<Long> epvIds, boolean exportRowData);

    /**
     * Загружает данные по УП(в) и их строкам из mdb
     * @return { mdb.epv.id -> epv.id }
     * @throws Exception
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    Map<String, Long> doImport_EppEduPlanVersionList(Database mdb) throws Exception;

}
