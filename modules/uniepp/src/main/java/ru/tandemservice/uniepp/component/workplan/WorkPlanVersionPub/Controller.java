package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionPub;

import jxl.Workbook;
import jxl.write.WritableWorkbook;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.dao.print.IEppWorkPlanPrintDAO;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.HashMap;

/**
 * 
 * @author nkokorina
 *
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        try {
            final ByteArrayOutputStream os = new ByteArrayOutputStream();
            final WritableWorkbook book = Workbook.createWorkbook(os);
            final EppWorkPlanVersion workPlanVersion = this.getModel(component).getWorkPlanVersion();
            IEppWorkPlanPrintDAO.instance.get().printRegistryWorkPlan(book, Collections.singleton(workPlanVersion.getId()), new HashMap<String, Object>());
            book.write();
            book.close();
            os.close();
            final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(os.toByteArray(), "ВерсияРП-"+workPlanVersion.getFullNumber()+".xls");
            this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("extension", "xls")));
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onClickEditVersion(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.workplan.WorkPlanVersionAddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getWorkPlanVersion().getId())
        ));
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        UniDaoFacade.getCoreDao().delete(this.getModel(component).getWorkPlanVersion().getId());
        this.deactivate(component);
    }
}
