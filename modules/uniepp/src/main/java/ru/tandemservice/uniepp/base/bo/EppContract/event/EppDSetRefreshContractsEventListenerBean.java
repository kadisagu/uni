package ru.tandemservice.uniepp.base.bo.EppContract.event;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import static org.tandemframework.hibsupport.dql.DQLFunctions.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;

import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.uniepp.IUniEppDefines;
import ru.tandemservice.uniepp.base.bo.EppContract.EppContractManager;
import ru.tandemservice.uniepp.base.bo.EppContract.daemon.EppContractDaemonBean;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;


/**
 * @author vdanilov
 * @deprecated
 */
@Deprecated
public class EppDSetRefreshContractsEventListenerBean
{
    // проверяет условия активации версии
    public static class ActivationVersionTest extends ParamTransactionCompleteListener<Boolean> {

        public static final ActivationVersionTest INSTANCE = new ActivationVersionTest();
        private ActivationVersionTest() {}

        @Override public void onEvent(final DSetEvent event) {
            if (event.getMultitude().getAffectedProperties().contains(CtrContractVersion.P_ACTIVATION_DATE)) {
                register(event, getIds(event));
            }
        }

        @Override
        public Boolean beforeCompletion(final Session session, final Collection<Long> ids) {

            final Map<Long, Long> ids2type = new HashMap<Long, Long>(ids.size() / 2);

            // грузим версии (только те, которые были активированы) и их типы
            BatchUtils.execute(ids, 512, new BatchUtils.Action<Long>() {
                @Override public void execute(final Collection<Long> ids) {
                    final List<Object[]> rows = new DQLSelectBuilder()
                    .fromEntity(CtrContractVersion.class, "v")
                    .column(property("v.id"))
                    .column(property(CtrContractVersion.contract().type().id().fromAlias("v")))
                    .where(in(property("v.id"), ids))
                    .where(isNotNull(property(CtrContractVersion.activationDate().fromAlias("v"))))
                    .createStatement(session).list();

                    for (Object[] row: rows) {
                        ids2type.put((Long)row[0], (Long)row[1]);
                    }
                }
            });

            if (ids2type.isEmpty()) {
                // нет версий, которые стали активированными (остальные нас не интересуют)
                return Boolean.FALSE;
            }

            // формируем уникальный набор типов (единственный способ, который я вижу - поднять все типы)
            // грузим эту абракадабру только если есть активированные в данным момент версии
            final Map<Long, Boolean> is2eppStatusMap = new HashMap<Long, Boolean>() {
                private static final long serialVersionUID = 1L;

                private final Map<Long, Object[]> id2rowMap = new HashMap<Long, Object[]>();
                {
                    final List<Object[]> rows = new DQLSelectBuilder()
                    .fromEntity(CtrContractType.class, "tp")
                    .column(property(CtrContractType.id().fromAlias("tp")))
                    .column(property(CtrContractType.parent().id().fromAlias("tp")))
                    .column(property(CtrContractType.code().fromAlias("tp")))
                    .createStatement(session).list();

                    for (final Object[] row: rows) {
                        id2rowMap.put((Long)row[0], row);
                    }
                }

                @Override public Boolean get(Object key) {
                    Boolean value = super.get(key);
                    if (null != value) { return value; }

                    Object[] row = id2rowMap.get(key);
                    if (null != row) {
                        if (IUniEppDefines.CTR_CONTRACT_TYPE_EPP.equals(row[2])) {
                            this.put((Long)key, Boolean.TRUE);
                            return Boolean.TRUE;
                        }

                        Long parentId = (Long)row[1];
                        if (null != parentId) {
                            value = this.get(parentId);
                            this.put((Long)key, value);
                            return value;
                        }
                    }

                    this.put((Long)key, Boolean.FALSE);
                    return Boolean.FALSE;

                }
            };

            // среди всех активированных оставляем только те, которые являются договорами на обучение
            for (Iterator<Map.Entry<Long, Long>> it=ids2type.entrySet().iterator(); it.hasNext(); ) {
                if (Boolean.FALSE.equals(is2eppStatusMap.get(it.next().getValue()))) {
                    it.remove();
                }
            }

            if (ids2type.isEmpty()) {
                // нет версий, которые стали активированными и являются договрами на обучение
                return Boolean.FALSE;
            }

            // для всех этих версий проверяем, что у них есть обязательства по обучению
            BatchUtils.execute(ids2type.keySet(), 512, new BatchUtils.Action<Long>() {
                @Override public void execute(final Collection<Long> ids) {

                    final Number count = new DQLSelectBuilder()
                    .fromEntity(CtrContractVersion.class, "v")
                    .column(count(property("v.id")))
                    .where(in(property("v.id"), ids))
                    .where(notExists(
                        new DQLSelectBuilder()
                        .fromEntity(EppCtrEducationPromice.class, "p").column(property("p.id"))
                        .where(eq(property(EppCtrEducationPromice.src().owner().fromAlias("p")), property("v")))
                        .buildQuery()
                    ))
                    .where(notExists(
                        new DQLSelectBuilder()
                        .fromEntity(EduCtrEducationPromise.class, "p").column(property("p.id"))
                        .where(eq(property(EduCtrEducationPromise.src().owner().fromAlias("p")), property("v")))
                        .buildQuery()
                    ))
                    .createStatement(session)
                    .uniqueResult();

                    if ((null != count) && (count.intValue() > 0)) {
                        throw new ApplicationException(
                            EppContractManager.instance().getProperty("error.activation-check.no-education-promice")
                        );
                    }
                }
            });

            return Boolean.TRUE;
        }

    }


    public void init()
    {
        DSetEventManager manager = DSetEventManager.getInstance();

        // проверка условий активации
        manager.registerListener(DSetEventType.afterUpdate, CtrContractVersion.class, ActivationVersionTest.INSTANCE);

        // по обязательствам
        manager.registerListener(DSetEventType.afterInsert, EppCtrEducationPromice.class, EppContractDaemonBean.DAEMON.getAfterCompleteWakeUpListener());
        manager.registerListener(DSetEventType.afterUpdate, EppCtrEducationPromice.class, EppContractDaemonBean.DAEMON.getAfterCompleteWakeUpListener());
        manager.registerListener(DSetEventType.afterDelete, EppCtrEducationPromice.class, EppContractDaemonBean.DAEMON.getAfterCompleteWakeUpListener());

        // по связям студента с УП(в)
        manager.registerListener(DSetEventType.afterInsert, EppStudent2EduPlanVersion.class, EppContractDaemonBean.DAEMON.getAfterCompleteWakeUpListener());
        manager.registerListener(DSetEventType.afterUpdate, EppStudent2EduPlanVersion.class, EppContractDaemonBean.DAEMON.getAfterCompleteWakeUpListener());
        manager.registerListener(DSetEventType.afterDelete, EppStudent2EduPlanVersion.class, EppContractDaemonBean.DAEMON.getAfterCompleteWakeUpListener());
    }


}
