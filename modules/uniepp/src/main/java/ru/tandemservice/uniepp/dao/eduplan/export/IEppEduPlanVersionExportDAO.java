package ru.tandemservice.uniepp.dao.eduplan.export;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;

/**
 * 
 * @author nkokorina
 *
 */

public interface IEppEduPlanVersionExportDAO
{
    public static final SpringBeanCache<IEppEduPlanVersionExportDAO> instance = new SpringBeanCache<IEppEduPlanVersionExportDAO>(IEppEduPlanVersionExportDAO.class.getName());

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public byte[] export(Collection<Long> versionIds);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public HashSet<Integer> getVersionCourseSet(Long versionId);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EppEduPlanVersionBlock> getVersionBlockList(Long versionId);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EppEduPlanVersionWeekType> getEduPlanVersionWeeks(Long versionId);

    /**
     * @param versionId
     * @return { course.intValue -> GroupCount}
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<Integer, Integer> getCourse2GroupCount(Long versionId);

    /**
     * @param versionId
     * @return { course.intValue -> StudentCount}
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<Integer, Integer> getCourse2StudentCount(Long versionId);
}
