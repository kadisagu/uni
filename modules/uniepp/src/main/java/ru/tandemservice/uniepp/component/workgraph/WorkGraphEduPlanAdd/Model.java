/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphEduPlanAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;

import java.util.List;
import java.util.Set;

/**
 * @author vip_delete
 * @since 09.03.2010
 */
@Input({
    @Bind(key = "workGraphId", binding = "workGraph.id")
})
public class Model
{
    private EppWorkGraph _workGraph = new EppWorkGraph();
    private ISelectModel programSubjectListModel;
    private EduProgramSubject programSubject;
    private List<Course> _courseList;
    private DynamicListDataSource<EppEduPlanVersion> _dataSource;
    private Set<PairKey<Long, Long>> _checkSet;

    // Methods

    public boolean isChecked()
    {
        return this._checkSet.contains(this.getKey());
    }

    public void setChecked(final boolean checked)
    {
        if (checked) {
            this._checkSet.add(this.getKey());
        } else {
            this._checkSet.remove(this.getKey());
        }
    }

    private PairKey<Long, Long> getKey()
    {
        return PairKey.create(this._dataSource.getCurrentEntity().getId(), ((Controller.CourseBlockColumn) this._dataSource.getCurrentColumn()).getColumnId());
    }

    // Getters & Setters

    public EppWorkGraph getWorkGraph()
    {
        return this._workGraph;
    }

    public void setWorkGraph(final EppWorkGraph workGraph)
    {
        this._workGraph = workGraph;
    }

    public EduProgramSubject getProgramSubject()
    {
        return programSubject;
    }

    public void setProgramSubject(EduProgramSubject programSubject)
    {
        this.programSubject = programSubject;
    }

    public ISelectModel getProgramSubjectListModel()
    {
        return programSubjectListModel;
    }

    public void setProgramSubjectListModel(ISelectModel programSubjectListModel)
    {
        this.programSubjectListModel = programSubjectListModel;
    }

    public List<Course> getCourseList()
    {
        return this._courseList;
    }

    public void setCourseList(final List<Course> courseList)
    {
        this._courseList = courseList;
    }

    public DynamicListDataSource<EppEduPlanVersion> getDataSource()
    {
        return this._dataSource;
    }

    public void setDataSource(final DynamicListDataSource<EppEduPlanVersion> dataSource)
    {
        this._dataSource = dataSource;
    }

    public Set<PairKey<Long, Long>> getCheckSet()
    {
        return this._checkSet;
    }

    public void setCheckSet(final Set<PairKey<Long, Long>> checkSet)
    {
        this._checkSet = checkSet;
    }
}
