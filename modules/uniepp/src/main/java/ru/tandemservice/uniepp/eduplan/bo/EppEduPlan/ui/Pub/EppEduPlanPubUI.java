/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.AddEditResult;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.AdditionalProfAddEdit.EppEduPlanAdditionalProfAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfAddEdit.EppEduPlanHigherProfAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.ParametersEdit.EppEduPlanParametersEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.ParametersEdit.EppEduPlanParametersEditUI;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.SecondaryProfAddEdit.EppEduPlanSecondaryProfAddEdit;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.std.gen.EppStateEduStandardGen;

import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 9/1/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id")
})
public class EppEduPlanPubUI extends UIPresenter
{
    private EntityHolder<EppEduPlan> holder = new EntityHolder<>();
    public EntityHolder<EppEduPlan> getHolder() { return holder; }
    public EppEduPlan getEduPlan() { return getHolder().getValue(); }

    private DynamicListDataSource<EppEduPlanVersion> dataSource;
    public DynamicListDataSource<EppEduPlanVersion> getDataSource() { return this.dataSource; }
    public void setDataSource(final DynamicListDataSource<EppEduPlanVersion> dataSource) { this.dataSource = dataSource; }


    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();

        if (null == getDataSource()) {
            final DynamicListDataSource<EppEduPlanVersion> dataSource = new DynamicListDataSource<>(_uiConfig.getBusinessComponent(), component -> {
                final List<EppEduPlanVersion> result = DataAccessServices.dao().getList(EppEduPlanVersion.class, EppEduPlanVersion.eduPlan(), getEduPlan(), EppEduPlanVersion.number().s());
                final DynamicListDataSource<EppEduPlanVersion> dataSource1 = getDataSource();
                dataSource1.setTotalSize(result.size());
                dataSource1.setCountRow(Math.max(4, 1 + result.size()));
                dataSource1.createPage(result);
            });

            dataSource.addColumn(UniEppUtils.getStateColumn());
            dataSource.addColumn(new SimpleColumn("Название", EppEduPlanVersion.title().s()).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Утверждена", EppEduPlanVersion.confirmDate(), DateFormatter.DATE_FORMATTER_JUST_YEAR).setOrderable(false).setClickable(false));
            dataSource.addColumn(new SimpleColumn("Состояние", EppEduPlanVersion.state().title()).setOrderable(false).setClickable(false));
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditVersion").setPermissionKey("editVersion_eppEduPlan"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteVersion", "Удалить версию № {0}?", EppStateEduStandardGen.P_NUMBER).setPermissionKey("deleteVersion_eppEduPlan"));

            setDataSource(dataSource);
        }
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        super.onComponentBindReturnParameters(childRegionName, returnedData);
        AddEditResult.processReturned(returnedData);
    }

    public void onClickEdit() {
        if (getEduPlan() instanceof EppEduPlanHigherProf) {
            _uiActivation
            .asRegionDialog(EppEduPlanHigherProfAddEdit.class)
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, getEduPlan().getId())
            .activate();
        } else if (getEduPlan() instanceof EppEduPlanSecondaryProf) {
            _uiActivation
            .asRegionDialog(EppEduPlanSecondaryProfAddEdit.class)
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, getEduPlan().getId())
            .activate();
        } else if (getEduPlan() instanceof EppEduPlanAdditionalProf) {
            _uiActivation
            .asRegionDialog(EppEduPlanAdditionalProfAddEdit.class)
            .parameter(PublisherActivator.PUBLISHER_ID_KEY, getEduPlan().getId())
            .activate();
        }
    }

	public boolean isDataCorrectionAvailable()
	{
		return ! getEduPlan().getProgramKind().getCode().equals(EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA);
	}

	public void onClickDataCorrection()
	{
		_uiActivation.asDesktopRoot(EppEduPlanParametersEdit.class)
                .parameter(EppEduPlanParametersEditUI.PARAM_EDU_PLAN_ID, getEduPlan().getId())
                .activate();
	}

    public void onClickAddVersion() {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Model.class.getPackage().getName(),
            new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, getEduPlan().getId())
        ));
    }

    public void onClickEditVersion() {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Model.class.getPackage().getName(),
            new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong())
        ));
    }

    public void onClickDeleteVersion() {
        UniDaoFacade.getCoreDao().delete(getListenerParameterAsLong());
    }

    // getters and setters
}