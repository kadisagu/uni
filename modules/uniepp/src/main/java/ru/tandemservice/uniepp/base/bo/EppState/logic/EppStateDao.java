/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppState.logic;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.base.bo.EppState.util.BaseEppStateConfig;
import ru.tandemservice.uniepp.base.bo.EppState.util.IEppStateConfig;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Nikolay Fedorovskih
 * @since 13.10.2014
 */
public class EppStateDao extends UniBaseDao implements IEppStateDao
{
    @Override
    public IEppStateConfig getStateConfig(Class<? extends IEppStateObject> clazz)
    {
        IEppStateConfig stateConfig = EppStateManager.instance().stateConfigExtPoint().getItem(clazz.getSimpleName());
        return stateConfig == null ? new BaseEppStateConfig() : stateConfig;
    }

    @Override
    public String canChangeStateEppObject(String oldStateCode, String newStateCode, IEppStateObject eppStateObject)
    {
        IEppStateConfig stateConfig = getStateConfig(eppStateObject.getClass());

        if (!stateConfig.canChangeState(oldStateCode, newStateCode))
        {
            final EppState newState = IUniBaseDao.instance.get().getCatalogItem(EppState.class, newStateCode);
            final List<String> fromStateCode = stateConfig.getFromPossibleTransitions().get(newStateCode);
            final List<String> fromStateTitles = IUniBaseDao.instance.get().getPropertiesList(EppState.class, EppState.code(), fromStateCode, true, EppState.title());
            return EppStateManager.instance().getProperty("error.cant-change-state-from-state", newState.getTitle(), fromStateTitles.stream().map(s -> "«" + s +  "»").collect(Collectors.joining(" или ")));
        }

        if (eppStateObject instanceof EppWorkPlanBase) {

            EppWorkPlanBase workPlan = (EppWorkPlanBase) eppStateObject;
            if (EppState.STATE_FORMATIVE.equals(oldStateCode) && EppState.STATE_ACCEPTABLE.equals(newStateCode) &&
                    !IEppSettingsDAO.instance.get().getGlobalSettings().isAllowEmptyWP() &&
                    !IUniBaseDao.instance.get().existsEntity(EppWorkPlanRegistryElementRow.class, EppWorkPlanRow.L_WORK_PLAN, workPlan))
            {
                // Если нет ни одной строки РУП и согласовывать пустые РУП нельзя, то кидаем исключение
                return EppStateManager.instance().getProperty("error.cant-accept-empty-wp");
            }
        }
        return null;
    }

    @Override
    public int massChangeState(String newStateCode, Collection<Long> eppStateObjectIds)
    {
        final EppState newState = getCatalogItem(EppState.class, newStateCode);
        final Session session = getSession();
        int counter = 0;
        for (final Long id : eppStateObjectIds)
        {
            final IEntity object = this.refresh(id);
            if (!(object instanceof IEppStateObject)) {
                throw new IllegalArgumentException();
            }
            final IEppStateObject stateObject = (IEppStateObject) object;
            if (this.canChangeStateEppObject(stateObject.getState().getCode(), newStateCode, stateObject) == null) {
                stateObject.setProperty("state", newState);
                session.update(stateObject);
                counter++;
            }
        }
        return counter;
    }
}