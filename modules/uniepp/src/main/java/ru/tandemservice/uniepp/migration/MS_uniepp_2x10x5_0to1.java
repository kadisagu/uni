package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_2x10x5_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppRegistryStructure

		// создано обязательное свойство themeRequired
		{
			// создать колонку
			tool.createColumn("epp_c_reg_struct_t", new DBColumn("themerequired_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update epp_c_reg_struct_t set themerequired_p = ? where themerequired_p is null", false);
			tool.executeUpdate("update epp_c_reg_struct_t set themerequired_p = ? where code_p = ?", true, EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_c_reg_struct_t", "themerequired_p", false);
		}
    }
}