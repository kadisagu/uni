/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.ui.EditEduPlanVersionSpecializationBlockOwner;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.base.bo.EppReorganization.EppReorganizationManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author rsizonenko
 * @since 05.04.2016
 */
@Input({
        @Bind(key = "singleValue", binding = "entity"),
        @Bind(key = "multiValue", binding = "entities")
})
public class EppReorganizationEditEduPlanVersionSpecializationBlockOwnerUI extends UIPresenter {
    private List<EppEduPlanVersionSpecializationBlock> entities;
    private Long entity;
    private OrgUnit owner;

    @Override
    public void onComponentActivate() {
        super.onComponentActivate();
    }

    public void onClickApply()
    {
        List<EppEduPlanVersionSpecializationBlock> output = new ArrayList<>();
        if (null != getEntities())
            output = getEntities();
        else if (null != getEntity())
            output.add(DataAccessServices.dao().get(getEntity()));
        EppReorganizationManager.instance().dao().doChangeEduPlanVersionSpecializationBlockOwner(output, owner);
        deactivate();
    }

    public List<EppEduPlanVersionSpecializationBlock> getEntities() {
        return entities;
    }

    public void setEntities(List<EppEduPlanVersionSpecializationBlock> entities) {
        this.entities = entities;
    }

    public Long getEntity() {
        return entity;
    }

    public void setEntity(Long entity) {
        this.entity = entity;
    }

    public OrgUnit getOwner() {
        return owner;
    }

    public void setOwner(OrgUnit owner) {
        this.owner = owner;
    }
}