package ru.tandemservice.uniepp.dao.eduStd.io.export;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

/**
 * Методы используемы для экспорта ГОС в xml
 * @author nkokorina
 *
 */
public class EppEduStdExportUtils
{

    /**
     * 
     * @param std
     * @return Название приможенного к ГОСу файла
     */
    @SuppressWarnings("deprecation")
    public static String getContentFileName(final EppStateEduStandard std)
    {
        return getFileName(std);
    }

    /**
     * 
     * @param std
     * @return - имя файла для экспорта
     */
    @SuppressWarnings("deprecation")
    public static String getFileName(final EppStateEduStandard std)
    {
        final StringBuilder sb = new StringBuilder();

        sb.append(std.getId().toString());
        sb.append("_");
        sb.append(std.getProgramSubject().getSubjectCode().replaceAll(".", "_"));
        sb.append("_");
        sb.append(RussianDateFormatUtils.getYearString(std.getConfirmDate(), false));
        sb.append(".xml");

        return sb.toString();
    }

    /**
     * 
     * @param std
     * @return Сформированный контент госа в виде byte[]
     */
    public static byte[] getFileContent(final EppStateEduStandard std)
    {
        try
        {
            return new EppEduStdExportGenerator(std).generateContent();
        }
        catch (final Throwable e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static String formatRage(final Long value1, final Long value2)
    {
        if ((null == value1) && (null == value2))
        {
            return null;
        }
        if (null == value1)
        {
            return "?-" + UniEppUtils.formatLoad(UniEppUtils.wrap(value2), false);
        }
        if (null == value2)
        {
            return UniEppUtils.formatLoad(UniEppUtils.wrap(value1), false) + "-?";
        }
        if (value1.equals(value2))
        {
            return UniEppUtils.formatLoad(UniEppUtils.wrap(value1), false);
        }
        return UniEppUtils.formatLoad(UniEppUtils.wrap(value1), false) + "-" + UniEppUtils.formatLoad(UniEppUtils.wrap(value2), false);
    }
}
