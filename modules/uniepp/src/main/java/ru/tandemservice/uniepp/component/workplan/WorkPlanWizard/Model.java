package ru.tandemservice.uniepp.component.workplan.WorkPlanWizard;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.commonbase.base.util.wizard.SimpleWizardBase.SimpleWizardModelBase;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id")
})
public class Model extends SimpleWizardModelBase {



}
