package ru.tandemservice.uniepp.base.bo.EppEduGroup.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.BaseRegistryElementDSHandler;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * Список читающих подразделений для указанных студентов
 * @author vdanilov
 */
public class EppEduGroupRegistryElement4StudentIdsDSHandler extends BaseRegistryElementDSHandler
{
    public static final String PARAM_STUDENT_IDS = "studentIds";
    public static final String PARAM_TUTOR_ORGUNIT_LIST = "tutorOrgUnitList";

    public EppEduGroupRegistryElement4StudentIdsDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
        super.applyWhereConditions(alias, dql, context);

        // по студентам
        {
            final Collection<Long> studentIds = context.get(PARAM_STUDENT_IDS);
            final DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(EppRealEduGroup.class, "g");
            final DQLSelectBuilder idsDql = EppEduGroupList4StudentIdsDSHandler.getEduGroupBuilder(registry, studentIds).column(property(EppRealEduGroup.activityPart().registryElement().id().fromAlias("g")));
            dql.where(in(property(alias, "id"), idsDql.buildQuery()));
        }

        // по читающему подразделению
        {
            final Collection<OrgUnit> ownerList = context.get(PARAM_TUTOR_ORGUNIT_LIST);
            if (null != ownerList && ownerList.size() > 0) {
                dql.where(in(property(EppRegistryElement.owner().fromAlias(alias)), ownerList));
            }
        }

    }

}
