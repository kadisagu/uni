package ru.tandemservice.uniepp.base.bo.EppContract.logic;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.tool.IdGen;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityCodes;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrPromiceResultResolver;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
public class EppContractEduPromiceResultPair extends UniBaseDao implements ICtrPromiceResultResolver {

    public static final String NAME = EppContractEduPromiceResultPair.class.getSimpleName();

    private static final EntityCodes<EppCtrEducationPromice> PROMICE_CODES = new EntityCodes<>(EppCtrEducationPromice.class);
    private static final EntityCodes<EppCtrEducationResult> RESULT_CODES = new EntityCodes<>(EppCtrEducationResult.class);

    @Override public String getName() { return EppContractEduPromiceResultPair.NAME; }
    @Override public int getAverageKeyNumberPerContract() { return 1; }

    protected String getKey(final Long dst, final Long studentCategory, final Long educationOrgUnit, final Long eduPlanVersion) {
        return new StringBuilder("epp-student")
        .append("\n").append(dst)
        .append("\n").append(studentCategory)
        .append("\n").append(educationOrgUnit)
        .append("\n").append(eduPlanVersion)
        .toString();
    }

    protected void putAll(final Map<Long, ICompareWrapper> result, final boolean promice, final DQLSelectBuilder dql) {
        for (final Object[] row: CommonDAO.scrollRows(dql.createStatement(this.getSession())))
        {
            final Long id = (Long)row[0];
            final long timestamp = ((Date)row[1]).getTime();
            final String key = this.getKey((Long)row[2], (Long)row[3], (Long)row[4], (Long)row[5]);

            final ICompareWrapper wrapper = new ICompareWrapper() {
                @Override public Long getId() { return id; }
                @Override public boolean isPromice() { return promice; }
                @Override public long getTimestamp() { return timestamp; }
                @Override public String getKey() { return key; }
                @Override public long getAmount() { return 1; }
                @Override public String toString() {
                    return this.getKey()+"("+this.isPromice()+")"+"@"+this.getTimestamp()+":"+this.getAmount();
                }
            };

            if (null != result.put(id, wrapper)) {
                throw new IllegalStateException();
            }
        }
    }


    @SuppressWarnings("unchecked")
    @Override public Map<Long, ICompareWrapper> getPromiceKeys(Collection<Long> promiceIds) {
        final Collection<Short> codes = EppContractEduPromiceResultPair.PROMICE_CODES.get();
        promiceIds = CollectionUtils.select(promiceIds, promiceId -> ((null != promiceId) && codes.contains(IdGen.getCode((Long)promiceId))));

        final Map<Long, ICompareWrapper> result = new HashMap<>(promiceIds.size());
        BatchUtils.execute(promiceIds, DQL.MAX_VALUES_ROW_NUMBER, promiceIds1 -> EppContractEduPromiceResultPair.this.putAll(
                result,
                true,
                new DQLSelectBuilder()
                .fromEntity(EppCtrEducationPromice.class, "p").where(in(property("p.id"), promiceIds1))
                .column(property(EppCtrEducationPromice.id().fromAlias("p")))
                .column(property(EppCtrEducationPromice.deadlineDate().fromAlias("p")))
                .column(property(EppCtrEducationPromice.dst().contactor().person().id().fromAlias("p")))
                .column(property(EppCtrEducationPromice.studentCategory().id().fromAlias("p")))
                .column(property(EppCtrEducationPromice.educationOrgUnit().id().fromAlias("p")))
                .column(property(EppCtrEducationPromice.eduPlanVersion().id().fromAlias("p")))
        ));
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override public Map<Long, ICompareWrapper> getResultKeys(Collection<Long> resultIds) {
        final Collection<Short> codes = EppContractEduPromiceResultPair.RESULT_CODES.get();
        resultIds = CollectionUtils.select(resultIds, resultId -> ((null != resultId) && codes.contains(IdGen.getCode((Long)resultId))));

        final Map<Long, ICompareWrapper> result = new HashMap<>(resultIds.size());
        BatchUtils.execute(resultIds, DQL.MAX_VALUES_ROW_NUMBER, resultIds1 -> EppContractEduPromiceResultPair.this.putAll(
                result,
                false,
                new DQLSelectBuilder()
                .fromEntity(EppCtrEducationResult.class, "r").where(in(property("r.id"), resultIds1))
                .column(property(EppCtrEducationResult.id().fromAlias("r")))
                .column(property(EppCtrEducationResult.timestamp().fromAlias("r")))
                .column(property(EppCtrEducationResult.target().student().person().id().fromAlias("r")))
                .column(property(EppCtrEducationResult.target().student().studentCategory().id().fromAlias("r")))
                .column(property(EppCtrEducationResult.target().student().educationOrgUnit().id().fromAlias("r")))
                .column(property(EppCtrEducationResult.target().eduPlanVersion().id().fromAlias("r")))
        ));
        return result;
    }

}
