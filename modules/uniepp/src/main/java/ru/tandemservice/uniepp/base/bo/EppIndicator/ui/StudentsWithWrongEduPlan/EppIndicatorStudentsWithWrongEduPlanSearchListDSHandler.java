/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithWrongEduPlan;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListPresenter;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/15/14
 */
public class EppIndicatorStudentsWithWrongEduPlanSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public EppIndicatorStudentsWithWrongEduPlanSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);

        // только неархивные студенты
        builder.where(eq(property(Student.archival().fromAlias(alias)), value(false)));

        // этого деканата
        final OrgUnit orgUnit = context.get(AbstractEppIndicatorStudentListPresenter.PROP_ORG_UNIT);
        if (null != orgUnit && null != orgUnit.getId()) {
            builder.where(eq(property(Student.educationOrgUnit().groupOrgUnit().id().fromAlias(alias)), value(orgUnit.getId())));
        }

        // актуальная связь не соответствует
        builder.where(exists(
            new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "s2epv").column(property("s2epv.id"))
                .joinPath(DQLJoinType.inner, EppStudent2EduPlanVersion.student().educationOrgUnit().fromAlias("s2epv"), "eduOu")
                .where(eq(property(EppStudent2EduPlanVersion.student().fromAlias("s2epv")), property(alias)))
                .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
                .where(isNotNull(property(EppStudent2EduPlanVersion.block().fromAlias("s2epv"))))
                .fromEntity(EppEduPlanProf.class, "p")
                .where(eq(property("s2epv", EppStudent2EduPlanVersion.eduPlanVersion().eduPlan()), property("p")))
                .where(or(
                    // не совпадает направление
                    ne(property("p", EppEduPlanProf.programSubject()), property("eduOu", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject())),
                    // не совпадает направленность - указан корневой блок и специализация
                    // пока убираем, потому что у них сейчас по факту куча планов с единственным общим блоком, и непонятно, стоит ли их заставлять создавать в них конкретные блоки
                    // and(isNotNull(property("eduOu", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization())),
                    //     instanceOf(EppStudent2EduPlanVersion.block().fromAlias("s2epv").s(), EppEduPlanVersionRootBlock.class)
                    // ),
                    // не совпадает направленность - указан дочерний блок и в нем другая специализация
                    exists(new DQLSelectBuilder()
                        .fromEntity(EppEduPlanVersionSpecializationBlock.class, "sb")
                        .where(eq(property("sb"), property(EppStudent2EduPlanVersion.block().fromAlias("s2epv"))))
                        .where(ne(property("sb", EppEduPlanVersionSpecializationBlock.programSpecialization()), property("eduOu", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSpecialization())))
                        .buildQuery()
                    ),
                    // не совпадает ФУТС
                    ne(property("p", EppEduPlanProf.programForm()), property("eduOu", EducationOrgUnit.developForm().programForm())),
                    ne(property("p", EppEduPlanProf.developCondition()), property("eduOu", EducationOrgUnit.developCondition())),
                    neNullSafe(property("p", EppEduPlanProf.programTrait()), property("eduOu", EducationOrgUnit.developTech().programTrait())),
                    ne(property("p", EppEduPlanProf.developGrid().developPeriod()), property("eduOu", EducationOrgUnit.developPeriod()))
                ))
                .buildQuery()
        ));
    }
}

