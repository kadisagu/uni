package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебный модуль: текущий контроль
 *
 * Текущий контроль по модулю
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegistryModuleIControlActionGen extends EntityBase
 implements INaturalIdentifiable<EppRegistryModuleIControlActionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction";
    public static final String ENTITY_NAME = "eppRegistryModuleIControlAction";
    public static final int VERSION_HASH = 324564184;
    private static IEntityMeta ENTITY_META;

    public static final String L_MODULE = "module";
    public static final String L_CONTROL_ACTION = "controlAction";
    public static final String P_AMOUNT = "amount";

    private EppRegistryModule _module;     // Учебный модуль
    private EppIControlActionType _controlAction;     // Форма текущего контроля
    private int _amount;     // Количество

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный модуль. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryModule getModule()
    {
        return _module;
    }

    /**
     * @param module Учебный модуль. Свойство не может быть null.
     */
    public void setModule(EppRegistryModule module)
    {
        dirty(_module, module);
        _module = module;
    }

    /**
     * @return Форма текущего контроля. Свойство не может быть null.
     */
    @NotNull
    public EppIControlActionType getControlAction()
    {
        return _controlAction;
    }

    /**
     * @param controlAction Форма текущего контроля. Свойство не может быть null.
     */
    public void setControlAction(EppIControlActionType controlAction)
    {
        dirty(_controlAction, controlAction);
        _controlAction = controlAction;
    }

    /**
     * @return Количество. Свойство не может быть null.
     */
    @NotNull
    public int getAmount()
    {
        return _amount;
    }

    /**
     * @param amount Количество. Свойство не может быть null.
     */
    public void setAmount(int amount)
    {
        dirty(_amount, amount);
        _amount = amount;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRegistryModuleIControlActionGen)
        {
            if (withNaturalIdProperties)
            {
                setModule(((EppRegistryModuleIControlAction)another).getModule());
                setControlAction(((EppRegistryModuleIControlAction)another).getControlAction());
            }
            setAmount(((EppRegistryModuleIControlAction)another).getAmount());
        }
    }

    public INaturalId<EppRegistryModuleIControlActionGen> getNaturalId()
    {
        return new NaturalId(getModule(), getControlAction());
    }

    public static class NaturalId extends NaturalIdBase<EppRegistryModuleIControlActionGen>
    {
        private static final String PROXY_NAME = "EppRegistryModuleIControlActionNaturalProxy";

        private Long _module;
        private Long _controlAction;

        public NaturalId()
        {}

        public NaturalId(EppRegistryModule module, EppIControlActionType controlAction)
        {
            _module = ((IEntity) module).getId();
            _controlAction = ((IEntity) controlAction).getId();
        }

        public Long getModule()
        {
            return _module;
        }

        public void setModule(Long module)
        {
            _module = module;
        }

        public Long getControlAction()
        {
            return _controlAction;
        }

        public void setControlAction(Long controlAction)
        {
            _controlAction = controlAction;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppRegistryModuleIControlActionGen.NaturalId) ) return false;

            EppRegistryModuleIControlActionGen.NaturalId that = (NaturalId) o;

            if( !equals(getModule(), that.getModule()) ) return false;
            if( !equals(getControlAction(), that.getControlAction()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getModule());
            result = hashCode(result, getControlAction());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getModule());
            sb.append("/");
            sb.append(getControlAction());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegistryModuleIControlActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegistryModuleIControlAction.class;
        }

        public T newInstance()
        {
            return (T) new EppRegistryModuleIControlAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "module":
                    return obj.getModule();
                case "controlAction":
                    return obj.getControlAction();
                case "amount":
                    return obj.getAmount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "module":
                    obj.setModule((EppRegistryModule) value);
                    return;
                case "controlAction":
                    obj.setControlAction((EppIControlActionType) value);
                    return;
                case "amount":
                    obj.setAmount((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "module":
                        return true;
                case "controlAction":
                        return true;
                case "amount":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "module":
                    return true;
                case "controlAction":
                    return true;
                case "amount":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "module":
                    return EppRegistryModule.class;
                case "controlAction":
                    return EppIControlActionType.class;
                case "amount":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRegistryModuleIControlAction> _dslPath = new Path<EppRegistryModuleIControlAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegistryModuleIControlAction");
    }
            

    /**
     * @return Учебный модуль. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction#getModule()
     */
    public static EppRegistryModule.Path<EppRegistryModule> module()
    {
        return _dslPath.module();
    }

    /**
     * @return Форма текущего контроля. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction#getControlAction()
     */
    public static EppIControlActionType.Path<EppIControlActionType> controlAction()
    {
        return _dslPath.controlAction();
    }

    /**
     * @return Количество. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction#getAmount()
     */
    public static PropertyPath<Integer> amount()
    {
        return _dslPath.amount();
    }

    public static class Path<E extends EppRegistryModuleIControlAction> extends EntityPath<E>
    {
        private EppRegistryModule.Path<EppRegistryModule> _module;
        private EppIControlActionType.Path<EppIControlActionType> _controlAction;
        private PropertyPath<Integer> _amount;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный модуль. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction#getModule()
     */
        public EppRegistryModule.Path<EppRegistryModule> module()
        {
            if(_module == null )
                _module = new EppRegistryModule.Path<EppRegistryModule>(L_MODULE, this);
            return _module;
        }

    /**
     * @return Форма текущего контроля. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction#getControlAction()
     */
        public EppIControlActionType.Path<EppIControlActionType> controlAction()
        {
            if(_controlAction == null )
                _controlAction = new EppIControlActionType.Path<EppIControlActionType>(L_CONTROL_ACTION, this);
            return _controlAction;
        }

    /**
     * @return Количество. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction#getAmount()
     */
        public PropertyPath<Integer> amount()
        {
            if(_amount == null )
                _amount = new PropertyPath<Integer>(EppRegistryModuleIControlActionGen.P_AMOUNT, this);
            return _amount;
        }

        public Class getEntityClass()
        {
            return EppRegistryModuleIControlAction.class;
        }

        public String getEntityName()
        {
            return "eppRegistryModuleIControlAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
