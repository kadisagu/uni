package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionSetupRowsOwners;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.uniepp.ui.EduPlanVersionBlockDataSourceGenerator;
import ru.tandemservice.uniepp.ui.EppOrgUnitSelectModel;

import java.util.Collection;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

public class DAO extends UniDao<Model> implements IDAO
{
    private static final String TERM_SELECT_COLUMN = "termSelectColumn";

    @Override
    public void prepare(final Model model)
    {
        final EppEduPlanVersionBlock block = model.getHolder().refresh(EppEduPlanVersionBlock.class);
        block.getState().check_editable(block);

        model.setSelectModel(new EppOrgUnitSelectModel() {
            @Override protected DQLSelectBuilder query(final String alias, final String filter) {
                final String a = alias+"_t";
                return super.query(alias, filter)
                .joinEntity(alias, DQLJoinType.inner, EppTutorOrgUnit.class, a, eq(property(EppTutorOrgUnit.orgUnit().fromAlias(alias+"_t")), property(alias)));
            }
        });


        Debug.begin("DAO.prepareBlockListDataSource");
        try {

            final Map<Long, OrgUnit> valueMap = model.getValueMap();
            valueMap.clear();

            final EduPlanVersionBlockDataSourceGenerator dataSourceGenerator = new EduPlanVersionBlockDataSourceGenerator() {
                @Override public boolean isCheckMode() { return false; }
                @Override public boolean isExtendedView() { return false; }
                @Override protected boolean isEditable() { return false; }
                @Override protected String getPermissionKeyEdit() { return null; }
                @Override protected Long getVersionBlockId() { return model.getHolder().getId(); }

                @Override
                public void doPrepareBlockDisciplineDataSource(final StaticListDataSource<IEppEpvRowWrapper> dataSource, final IBlockRenderContext context) {
                    super.doPrepareBlockDisciplineDataSource(dataSource, context);
                    dataSource.addColumn(new BlockColumn<Void>(DAO.TERM_SELECT_COLUMN, "Подразделение", "disciplineOrgUnitBlock").setWidth(30));
                }

                @Override
                public void doPrepareBlockActionsDataSource(final StaticListDataSource<IEppEpvRowWrapper> dataSource, final IBlockRenderContext context) {
                    super.doPrepareBlockActionsDataSource(dataSource, context);
                    dataSource.addColumn(new BlockColumn<Void>(DAO.TERM_SELECT_COLUMN, "Подразделение", "actionOrgUnitBlock").setWidth(30));
                }
            };

            ///////////////////////////
            // данные по дисциплинам //
            ///////////////////////////
            {

                dataSourceGenerator.doPrepareBlockDisciplineDataSource(model.getEppDisciplinesDataSource(), new EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext() {
                    @Override public IEntityHandler getAdditionalEditDisabler() { return null; }
                    @Override public Collection<IEppEpvRowWrapper> getBlockRows(final IEppEpvBlockWrapper versionWrapper) {
                        return EppEduPlanVersionDataDAO.filterEduPlanBlockRows(versionWrapper.getRowMap(), object -> {
                            // редактируем только строки текущего блока
                            return object.getOwner().equals(model.getHolder().getValue()) && IEppEpvRowWrapper.DISCIPLINES_ROWS.evaluate(object);
                        }, true);
                    }
                });

                for (final IEppEpvRowWrapper w: model.getEppDisciplinesDataSource().getRowList()) {
                    if (model.isEditable(w)) {
                        valueMap.put(w.getId(), ((EppEpvRegistryRow)w.getRow()).getRegistryElementOwner());
                    }
                }
            }

            ////////////////////////////
            // данные по мероприятиям //
            ////////////////////////////
            {
                dataSourceGenerator.doPrepareBlockActionsDataSource(model.getEppActionsDataSource(), new EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext() {
                    @Override public IEntityHandler getAdditionalEditDisabler() { return null; }
                    @Override public Collection<IEppEpvRowWrapper> getBlockRows(final IEppEpvBlockWrapper versionWrapper) {
                        return EppEduPlanVersionDataDAO.filterEduPlanBlockRows(versionWrapper.getRowMap(), object -> {
                            // редактируем только строки текущего блока
                            return object.getOwner().equals(model.getHolder().getValue()) && IEppEpvRowWrapper.ACTIONS_ROWS.evaluate(object);
                        }, true);
                    }
                });

                for (final IEppEpvRowWrapper w: model.getEppActionsDataSource().getRowList()) {
                    if (model.isEditable(w)) {
                        valueMap.put(w.getId(), ((EppEpvRegistryRow)w.getRow()).getRegistryElementOwner());
                    }
                }
            }

        } finally {
            Debug.end();
        }
    }


    @Override
    @SuppressWarnings("unchecked")
    public void update(final Model model)
    {
        for (final Map.Entry<Long, OrgUnit> e: model.getValueMap().entrySet()) {
            if (null == e.getValue()) { continue; }

            final EppEpvRow row = this.getNotNull(EppEpvRow.class, e.getKey());
            if ((row.getOwner().getId().equals(model.getId())) && (row instanceof EppEpvRegistryRow)) {
                ((EppEpvRegistryRow)row).setRegistryElementOwner(e.getValue());
                this.update(row);
            }
        }
    }
}
