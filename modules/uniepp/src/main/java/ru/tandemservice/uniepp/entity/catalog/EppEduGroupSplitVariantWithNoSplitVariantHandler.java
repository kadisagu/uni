/* $Id$ */
package ru.tandemservice.uniepp.entity.catalog;

import com.beust.jcommander.internal.Lists;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import java.util.List;

/**
 * @author Denis Katkov
 * @since 28.04.2016
 */
public class EppEduGroupSplitVariantWithNoSplitVariantHandler extends SimpleTitledComboDataSourceHandler
{
    public EppEduGroupSplitVariantWithNoSplitVariantHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduGroupSplitVariant.class, "o");
        List<EppEduGroupSplitVariant> recordList = builder.createStatement(getSession()).list();

        List<DataWrapper> recordListWrapped = Lists.newArrayList();
        recordListWrapped.add(new DataWrapper(0L, "Не задан"));
        recordList.stream().forEach(o -> recordListWrapped.add(new DataWrapper(o.getId(), o.getTitle())));

        context.put(UIDefines.COMBO_OBJECT_LIST, recordListWrapped);

        return super.execute(input, context);
    }
}