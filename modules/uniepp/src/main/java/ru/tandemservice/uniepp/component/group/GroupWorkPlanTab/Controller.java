package ru.tandemservice.uniepp.component.group.GroupWorkPlanTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author nkokorina
 *
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.setSettings(component.getSettings());
        this.getDao().prepare(model);
    }

    public void onClickConnectWorkPlan(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        final Set<Long> selectedIds = new HashSet<Long>();
        for (final BlockGridWrapper block : model.getBlockList()) {
            final Collection<IEntity> objectList = ((CheckboxColumn)block.getDataSource().getColumn("select")).getSelectedObjects();
            for (final IEntity entity : objectList) {
                selectedIds.add(entity.getId());
            }
        }

        if (selectedIds.isEmpty()) {
            ContextLocal.getErrorCollector().add("Не выбрано ни одного элемента.");
            return;
        }

        component.createDefaultChildRegion(new ComponentActivator(
                ru.tandemservice.uniepp.component.student.MassChangeWorkPlan.Model.class.getPackage().getName(),
                new ParametersMap().add("ids", new ArrayList<Long>(selectedIds))
        ));
    }

    public void onClickPrintWorkPlan(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(
                ru.tandemservice.uniepp.component.group.GroupWorkPlanPrint.Model.class.getPackage().getName(),
                new ParametersMap().add("ids", UniBaseDao.ids(UniDaoFacade.getCoreDao().getList(Student.class, StudentGen.L_GROUP, model.getGroup())))
        ));
    }

    public void onClickSearch(IBusinessComponent component){
        component.saveSettings();
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onClickClear(IBusinessComponent component){
        component.getSettings().set("status", null);
        component.getSettings().set("studentCustomStateCIs", null);
        onClickSearch(component);
    }
}
