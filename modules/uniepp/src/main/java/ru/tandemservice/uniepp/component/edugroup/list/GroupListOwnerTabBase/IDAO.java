package ru.tandemservice.uniepp.component.edugroup.list.GroupListOwnerTabBase;

import org.apache.commons.collections15.Factory;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {

    Factory<DQLSelectBuilder> getRelationDqlFactory(Model model);
}
