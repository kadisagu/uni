package ru.tandemservice.uniepp.component.registry.RegElementPartEdit;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;

/**
 * @author vdanilov
 */
public class DAO<T extends EppRegistryElement> extends UniBaseDao implements IDAO {

    @Override
    public void prepare(final Model model) {
        final EppRegistryElementPart part = model.getHolder().refresh(EppRegistryElementPart.class);
        part.getState().check_editable(part);

        model.setGradeScaleList(this.getCatalogItemListOrderByCode(EppGradeScale.class));

        model.setControlActionModel(
                new LazySimpleSelectModel<EppFControlActionType>(EppFControlActionType.class)
        );
        model.setSelection(
                IEppRegistryDAO.instance.get().getRegistryElementPartControlActions(model.getElement())
        );
    }

    @Override
    public void save(final Model model) {
        IEppRegistryDAO.instance.get().doSaveRegistryElementPart(model.getElement());
        IEppRegistryDAO.instance.get().doSaveRegistryElementPartControlActions(model.getElement(), model.getSelection().values());
    }


}
