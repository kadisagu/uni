package ru.tandemservice.uniepp.component.settings.TypesControlActionsTotalMarkPriority;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

/**
 * 
 * @author nkokorina
 * @since 16.03.2010
 */

@State( { @Bind(key = "selectedTab", binding = "selectedTab") })
public class Model
{
    private DynamicListDataSource<IEntity> _dataSource;
    public void setDataSource(final DynamicListDataSource<IEntity> _dataSource) { this._dataSource = _dataSource; }
    public DynamicListDataSource<IEntity> getDataSource() { return this._dataSource; }

    private String _selectedTab;
    public void setSelectedTab(final String selectedTab) { this._selectedTab = selectedTab; }
    public String getSelectedTab() { return this._selectedTab; }

}
