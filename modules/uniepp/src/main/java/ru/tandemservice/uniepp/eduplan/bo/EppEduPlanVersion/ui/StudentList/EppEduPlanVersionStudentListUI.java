/**
 *$Id$
 */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.StudentList;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;

/**
 * @author Alexander Shaburov
 * @since 01.02.13
 */
@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id") })
public class EppEduPlanVersionStudentListUI extends AbstractUniStudentListUI
{
    public static final String PARAM_ID = "id";

    private Long _id;

    @Override
    public String getSettingsKey()
    {
        return "epp.EduPlanVersionStudentList.filter";
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);

        if (EppEduPlanVersionStudentList.STUDENT_SEARCH_LIST_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_ID, _id);
        }
    }

    // Getters & Setters

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }
}
