// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.IOrgUnitLPGVisibilityResolver;
import org.tandemframework.shared.organization.base.util.IOrgUnitPermissionCustomizer;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.settings.EppPlanOrgUnit;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 01.12.2010
 */
public class UnieppOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier, IOrgUnitPermissionCustomizer, IOrgUnitLPGVisibilityResolver
{
    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uniepp");
        config.setName("uniepp-orgunit-sec-config");
        config.setTitle("");

        final ModuleGlobalGroupMeta mggOrgstruct = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        final ModuleLocalGroupMeta mlgOrgstruct = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            final String code = description.getCode();
            final ClassGroupMeta gcg = PermissionMetaUtil.createClassGroup(mggOrgstruct, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            final ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(mlgOrgstruct, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            // "Вкладка «Учебный процесс»

            final PermissionGroupMeta pgEppTab = PermissionMetaUtil.createPermissionGroup(config, code + "EppPG", "Вкладка «Учебный процесс»");
            PermissionMetaUtil.createGroupRelation(config, pgEppTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, pgEppTab.getName(), lcg.getName());
            PermissionMetaUtil.createPermission(pgEppTab, "orgUnitEppTab_" + code, "Просмотр");

            final PermissionGroupMeta pgModuleTab = PermissionMetaUtil.createPermissionGroup(pgEppTab, code + "EppModulePG", "Вкладка «Модули»");
            PermissionMetaUtil.createPermission(pgModuleTab, "view_eppModule_list_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgModuleTab, "add_eppModule_list_" + code, "Добавление модуля");
            PermissionMetaUtil.createPermission(pgModuleTab, "edit_eppModule_list_" + code, "Редактирование модуля");
            PermissionMetaUtil.createPermission(pgModuleTab, "delete_eppModule_list_" + code, "Удаление модуля");

            final PermissionGroupMeta pgDisciplineTab = PermissionMetaUtil.createPermissionGroup(pgEppTab, code + "EppDisciplinePG", "Вкладка «Дисциплины»");
            PermissionMetaUtil.createPermission(pgDisciplineTab, "view_eppDiscipline_list_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgDisciplineTab, "add_eppDiscipline_list_" + code, "Добавление дисциплины");
            PermissionMetaUtil.createPermission(pgDisciplineTab, "edit_eppDiscipline_list_" + code, "Редактирование дисциплины");
            PermissionMetaUtil.createPermission(pgDisciplineTab, "massSplitEdit_eppDiscipline_list_" + code, "Редактирование способа деления потоков");
            PermissionMetaUtil.createPermission(pgDisciplineTab, "delete_eppDiscipline_list_" + code, "Удаление дисциплины");


            final PermissionGroupMeta pgAttestationTab = PermissionMetaUtil.createPermissionGroup(pgEppTab, code + "EppAttestationPG", "Вкладка «Мероприятия ГИА»");
            PermissionMetaUtil.createPermission(pgAttestationTab, "view_eppAttestation_list_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgAttestationTab, "add_eppAttestation_list_" + code, "Добавление мероприятия");
            PermissionMetaUtil.createPermission(pgAttestationTab, "edit_eppAttestation_list_" + code, "Редактирование мероприятия");
            PermissionMetaUtil.createPermission(pgAttestationTab, "massSplitEdit_eppAttestation_list_" + code, "Редактирование способа деления потоков");
            PermissionMetaUtil.createPermission(pgAttestationTab, "delete_eppAttestation_list_" + code, "Удаление мероприятия");

            final PermissionGroupMeta pgEpvAcceptionTab = PermissionMetaUtil.createPermissionGroup(pgEppTab, code + "EppEpvAttestationPG", "Вкладка «Согласование УП»");
            PermissionMetaUtil.createPermission(pgEpvAcceptionTab, "view_eppEpvAcception_list_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgEpvAcceptionTab, "attach_eppEpvAcception_list_" + code, "Указание мероприятия реестра");

            final PermissionGroupMeta pgPracticeTab = PermissionMetaUtil.createPermissionGroup(pgEppTab, code + "EppPracticePG", "Вкладка «Практики»");
            PermissionMetaUtil.createPermission(pgPracticeTab, "view_eppPractice_list_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgPracticeTab, "add_eppPractice_list_" + code, "Добавление практики");
            PermissionMetaUtil.createPermission(pgPracticeTab, "edit_eppPractice_list_" + code, "Редактирование практики");
            PermissionMetaUtil.createPermission(pgPracticeTab, "massSplitEdit_eppPractice_list_" + code, "Редактирование способа деления потоков");
            PermissionMetaUtil.createPermission(pgPracticeTab, "delete_eppPractice_list_" + code, "Удаление практики");

            final PermissionGroupMeta pgEduPlanTab = PermissionMetaUtil.createPermissionGroup(pgEppTab, code + "EppEduPlanPG", "Вкладка «Учебные планы»");
            PermissionMetaUtil.createPermission(pgEduPlanTab, "view_eppEduPlan_list_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgEduPlanTab, "add_eppEduPlan_list_" + code, "Добавление учебного плана");
            PermissionMetaUtil.createPermission(pgEduPlanTab, "edit_eppEduPlan_list_" + code, "Редактирование учебного плана");
            PermissionMetaUtil.createPermission(pgEduPlanTab, "exportToFormatIMCA_eppEduPlan_list_" + code, "Экспорт учебных планов в формат ИМЦА");
            PermissionMetaUtil.createPermission(pgEduPlanTab, "delete_eppEduPlan_list_" + code, "Удаление учебного плана");

            final PermissionGroupMeta pgWorkPlanTab = PermissionMetaUtil.createPermissionGroup(pgEppTab, code + "EppWorkPlanPG", "Вкладка «Рабочие учебные планы»");
            final String wp_list_postfix = UnieppStateChangePermissionModifier.POSTFIX_WORK_PLAN_LIST + "_" + code;
            PermissionMetaUtil.createPermission(pgWorkPlanTab, "view_" + wp_list_postfix, "Просмотр");
            PermissionMetaUtil.createPermission(pgWorkPlanTab, "add_" + wp_list_postfix, "Добавление рабочего учебного плана");
            PermissionMetaUtil.createPermission(pgWorkPlanTab, "edit_" + wp_list_postfix, "Редактирование рабочего учебного плана");
            PermissionMetaUtil.createPermission(pgWorkPlanTab, "addByGUP_" + wp_list_postfix, "Формирование РУП на основе ГУП");
            PermissionMetaUtil.createPermission(pgWorkPlanTab, "delete_" + wp_list_postfix, "Удаление рабочего учебного плана");
            for (final EppState state : UniDaoFacade.getCoreDao().getList(EppState.class))
            {
                final String p = UnieppStateChangePermissionHolder.getMassPermission(state, wp_list_postfix);
                PermissionMetaUtil.createPermission(pgWorkPlanTab, p, UnieppStateChangePermissionModifier.getMassPermissionTitle(state.getTitle()));
            }

            // Вкладка «Учебные группы»

            final PermissionGroupMeta realGroupTab = PermissionMetaUtil.createPermissionGroup(config, code + "EppRealGroupPG", "Вкладка «Учебные группы»");
            PermissionMetaUtil.createGroupRelation(config, realGroupTab.getName(), gcg.getName());
            PermissionMetaUtil.createGroupRelation(config, realGroupTab.getName(), lcg.getName());
            PermissionMetaUtil.createPermission(realGroupTab, "orgUnitRealGroupTab_" + code, "Просмотр");

            final PermissionGroupMeta realGroupOwnerTab = PermissionMetaUtil.createPermissionGroup(realGroupTab, code + "EppRealGroupOwnerTabPG", "Вкладка «Диспетчерская»");
            PermissionMetaUtil.createPermission(realGroupOwnerTab, "viewOwnerTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(realGroupOwnerTab, "actualize_eppEduGroup_owner_" + code, "Актуализация списка");
            PermissionMetaUtil.createPermission(realGroupOwnerTab, "edit_eppEduGroup_owner_" + code, "Редактирование групп в списке и действия с ними");
            PermissionMetaUtil.createPermission(realGroupOwnerTab, "delete_eppEduGroup_owner_" + code, "Удаление группы");
            PermissionMetaUtil.createPermission(realGroupOwnerTab, "sendFormingFromPrevious_eppEduGroup_owner_" + code, "Перевод УГС, утвержденных на предыдущем этапе, на формирование");
            PermissionMetaUtil.createPermission(realGroupOwnerTab, "sendForming_eppEduGroup_owner_" + code, "Перевод УГС на формирование");
            PermissionMetaUtil.createPermission(realGroupOwnerTab, "confirm_eppEduGroup_owner_" + code, "Утверждение группы");
            PermissionMetaUtil.createPermission(realGroupOwnerTab, "sendTo_eppEduGroup_owner_" + code, "Возврат группы на предыдущие этапы согласования");

            final PermissionGroupMeta realGroupOwnerPub = PermissionMetaUtil.createPermissionGroup(realGroupOwnerTab, code + "EppRealGroupOwnerPubPG", "Карточка учебной группы для диспетчерской");
            PermissionMetaUtil.createPermission(realGroupOwnerPub, "pubActions_eppEduGroup_owner_" + code, "Редактирование данных группы и состава студентов");
            PermissionMetaUtil.createPermission(realGroupOwnerPub, "pubResetLevel_eppEduGroup_owner_" + code, "Сброс согласования");
            PermissionMetaUtil.createPermission(realGroupOwnerPub, "pubView_eppEduGroup_owner_" + code, "Просмотр");

            final PermissionGroupMeta realGroupGroupTab = PermissionMetaUtil.createPermissionGroup(realGroupTab, code + "EppRealGroupGroupTabPG", "Вкладка «Деканат»");
            PermissionMetaUtil.createPermission(realGroupGroupTab, "viewGroupTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(realGroupGroupTab, "actualize_eppEduGroup_group_" + code, "Актуализация списка");
            PermissionMetaUtil.createPermission(realGroupGroupTab, "edit_eppEduGroup_group_" + code, "Редактирование групп в списке и действия с ними");
            PermissionMetaUtil.createPermission(realGroupGroupTab, "delete_eppEduGroup_group_" + code, "Удаление группы");
            PermissionMetaUtil.createPermission(realGroupGroupTab, "sendFormingFromPrevious_eppEduGroup_group_" + code, "Перевод УГС, утвержденных на предыдущем этапе, на формирование");
            PermissionMetaUtil.createPermission(realGroupGroupTab, "sendForming_eppEduGroup_group_" + code, "Перевод УГС на формирование");
            PermissionMetaUtil.createPermission(realGroupGroupTab, "confirm_eppEduGroup_group_" + code, "Утверждение группы");
            PermissionMetaUtil.createPermission(realGroupGroupTab, "sendTo_eppEduGroup_group_" + code, "Возврат группы на предыдущие этапы согласования");

            final PermissionGroupMeta realGroupGroupPub = PermissionMetaUtil.createPermissionGroup(realGroupGroupTab, code + "EppRealGroupGroupPubPG", "Карточка учебной группы для деканата");
            PermissionMetaUtil.createPermission(realGroupGroupPub, "pubActions_eppEduGroup_group_" + code, "Редактирование данных группы и состава студентов");
            PermissionMetaUtil.createPermission(realGroupGroupPub, "pubResetLevel_eppEduGroup_group_" + code, "Сброс согласования");
            PermissionMetaUtil.createPermission(realGroupGroupPub, "pubView_eppEduGroup_group_" + code, "Просмотр");

            final PermissionGroupMeta realGroupTutorTab = PermissionMetaUtil.createPermissionGroup(realGroupTab, code + "EppRealGroupTutorTabPG", "Вкладка «Читающее подр.»");
            PermissionMetaUtil.createPermission(realGroupTutorTab, "viewTutorTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(realGroupTutorTab, "actualize_eppEduGroup_tutor_" + code, "Актуализация списка");
            PermissionMetaUtil.createPermission(realGroupTutorTab, "edit_eppEduGroup_tutor_" + code, "Редактирование групп в списке и действия с ними");
            PermissionMetaUtil.createPermission(realGroupTutorTab, "delete_eppEduGroup_tutor_" + code, "Удаление группы");
            PermissionMetaUtil.createPermission(realGroupTutorTab, "sendFormingFromPrevious_eppEduGroup_tutor_" + code, "Перевод УГС, утвержденных на предыдущем этапе, на формирование");
            PermissionMetaUtil.createPermission(realGroupTutorTab, "sendForming_eppEduGroup_tutor_" + code, "Перевод УГС на формирование");
            PermissionMetaUtil.createPermission(realGroupTutorTab, "confirm_eppEduGroup_tutor_" + code, "Утверждение группы");
            PermissionMetaUtil.createPermission(realGroupTutorTab, "sendTo_eppEduGroup_tutor_" + code, "Возврат группы на предыдущие этапы согласования");

            final PermissionGroupMeta realGroupTutorPub = PermissionMetaUtil.createPermissionGroup(realGroupTutorTab, code + "EppRealGroupTutorPubPG", "Карточка учебной группы для читащего подр.");
            PermissionMetaUtil.createPermission(realGroupTutorPub, "pubActions_eppEduGroup_tutor_" + code, "Редактирование данных группы и состава студентов");
            PermissionMetaUtil.createPermission(realGroupTutorPub, "pubResetLevel_eppEduGroup_tutor_" + code, "Сброс согласования");
            PermissionMetaUtil.createPermission(realGroupTutorPub, "pubView_eppEduGroup_tutor_" + code, "Просмотр");

            // Вкладка «Отчеты»

            final PermissionGroupMeta permissionGroupReportsAll = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            final PermissionGroupMeta pgReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReportsAll, code + "EppIndicatorsPG", "Показатели корректности данных в учебном процессе");
            PermissionMetaUtil.createPermission(pgReports, "eppGroupOuIndicatorBlockView_" + code, "Просмотр показателей деканата");
            PermissionMetaUtil.createPermission(pgReports, "eppTutorOuIndicatorBlockView_" + code, "Просмотр показателей читающего подразделения");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }

    @Override
    public void customize(final OrgUnitType orgUnitType, final Map<String, PermissionGroupMeta> permissionGroupMap)
    {
        final boolean hidden = this.isHidden(orgUnitType);
        final String code = orgUnitType.getCode();

        permissionGroupMap.get(code + "EppPG").setHidden(hidden);
    }

    @Override
    public List<IOrgUnitLPGVisibilityResolver.ILPGVisibilityResolver> getResolverList()
    {
        return Arrays.<IOrgUnitLPGVisibilityResolver.ILPGVisibilityResolver>asList(new IOrgUnitLPGVisibilityResolver.ILPGVisibilityResolver()
        {
            @Override
            public String getGroupName()
            {
                return "unieppLocal";
            }

            @Override
            public boolean isHidden(final OrgUnitType orgUnitType)
            {
                return UnieppOrgstructPermissionModifier.this.isHidden(orgUnitType);
            }
        });
    }

    @SuppressWarnings("unused")
    private boolean isHidden(final OrgUnitType orgUnitType)
    {
        if (true)
            return false; // Толик обойдется, все равно это не работало

        final Set<OrgUnitType> types = OrgUnitManager.instance().dao().getOrgUnitTypeChildrenFullList(orgUnitType);
        types.add(orgUnitType);

        final DQLSelectBuilder sub = new DQLSelectBuilder()
        .fromEntity(EppPlanOrgUnit.class, "ou")
        .column(DQLFunctions.count(property("ou", EppPlanOrgUnit.orgUnit().id().s())))
        .where(in(property(EppPlanOrgUnit.orgUnit().orgUnitType().fromAlias("ou")), types));

        final Number number = IUniBaseDao.instance.get().getCalculatedValue(session -> (Number) sub.createStatement(session).uniqueResult());

        return (number == null) || (number.intValue() == 0);
    }
}
