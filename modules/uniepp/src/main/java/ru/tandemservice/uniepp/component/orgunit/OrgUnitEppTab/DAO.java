package ru.tandemservice.uniepp.component.orgunit.OrgUnitEppTab;

import ru.tandemservice.uni.dao.UniBaseDao;

/**
 * @author nkokorina
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override public void prepare(final Model model) {
        model.getOrgUnitHolder().refresh();
    }
}