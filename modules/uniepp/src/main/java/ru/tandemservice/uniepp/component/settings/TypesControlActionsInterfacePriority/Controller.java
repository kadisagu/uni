package ru.tandemservice.uniepp.component.settings.TypesControlActionsInterfacePriority;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;

/**
 * 
 * @author nkokorina
 * @since 16.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.prepareListDateSource(component);
    }

    private void prepareListDateSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        final DynamicListDataSource<IEntity> dataSource = UniBaseUtils.createDataSource(component, this.getDao());
        dataSource.addColumn(new SimpleColumn("Название", EppFControlActionType.P_TITLE).setOrderable(false));

        final ActionColumn veryUp = new ActionColumn("Установить максимальный приоритет", CommonBaseDefine.ICO_VERY_UP, "onClickVeryUp");
        veryUp.setIconResolver(entity -> CommonBaseDefine.ICO_VERY_UP);
        dataSource.addColumn(veryUp.setOrderable(false).setDisableHandler(this.getDao().getUpDisabledEntityHandler()));

        final ActionColumn up = new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp");
        up.setIconResolver(entity -> CommonBaseDefine.ICO_UP);
        dataSource.addColumn(up.setOrderable(false).setDisableHandler(this.getDao().getUpDisabledEntityHandler()));

        final ActionColumn down = new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown");
        down.setIconResolver(entity -> CommonBaseDefine.ICO_DOWN);
        dataSource.addColumn(down.setOrderable(false).setDisableHandler(this.getDao().getDownDisabledEntityHandler()));

        final ActionColumn veryDown = new ActionColumn("Установить минимальный приоритет", CommonBaseDefine.ICO_VERY_DOWN, "onClickVeryDown");
        veryDown.setIconResolver(entity -> CommonBaseDefine.ICO_VERY_DOWN);
        dataSource.addColumn(veryDown.setOrderable(false).setDisableHandler(this.getDao().getDownDisabledEntityHandler()));

        model.setDataSource(dataSource);
    }

    public void onClickUp(final IBusinessComponent context)
    {
        this.getDao().updatePriorityUp((Long) context.getListenerParameter());
    }

    public void onClickVeryUp(final IBusinessComponent context)
    {
        this.getDao().updatePriorityVeryUp((Long) context.getListenerParameter());
    }

    public void onClickDown(final IBusinessComponent context)
    {
        this.getDao().updatePriorityDown((Long) context.getListenerParameter());
    }

    public void onClickVeryDown(final IBusinessComponent context)
    {
        this.getDao().updatePriorityVeryDown((Long) context.getListenerParameter());
    }
}
