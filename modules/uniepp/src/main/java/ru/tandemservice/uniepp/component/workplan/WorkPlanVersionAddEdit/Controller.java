/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;

/**
 * @author nkokorina
 * Created on: 10.08.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().update(model);
        this.deactivate(component);
        this.activateInRoot(component, new ComponentActivator(
                ru.tandemservice.uniepp.component.workplan.WorkPlanVersionPub.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getElement().getId())
        ));
    }

    @Override public void onRenderComponent(final IBusinessComponent component) {
        ContextLocal.beginPageTitlePart(this.getModel(component).getElement().getId() == null ? "Добавление версии РУП" : "Редактирование версии РУП");
    }
}
