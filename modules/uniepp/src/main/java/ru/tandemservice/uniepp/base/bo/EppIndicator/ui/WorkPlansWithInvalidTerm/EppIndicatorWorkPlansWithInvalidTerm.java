/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.WorkPlansWithInvalidTerm;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanGen;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Nikolay Fedorovskih
 * @since 06.11.2014
 */
@Configuration
public class EppIndicatorWorkPlansWithInvalidTerm extends BusinessComponentManager
{
    public static final String WORK_PLAN_DS = "workPlanDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(WORK_PLAN_DS, workPlanDSColumns(), workPlanDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint workPlanDSColumns()
    {
        return columnListExtPointBuilder(WORK_PLAN_DS)
                .addColumn(publisherColumn("title", EppWorkPlan.title()).order())
                .addColumn(textColumn("direction", EppWorkPlanGen.parent().eduPlanVersion().eduPlan().educationElementSimpleTitle()))
                .addColumn(textColumn("block", EppWorkPlanGen.parent().title()))
                .addColumn(publisherColumn("epv", EppWorkPlan.parent().eduPlanVersion().title()).publisherLinkResolver(
                        new SimplePublisherLinkResolver(EppWorkPlan.parent().eduPlanVersion().id())
                ))
                .addColumn(textColumn("wpTerm", EppWorkPlanGen.term().title()))
                .addColumn(textColumn("epvTerm", EppWorkPlanGen.parent().eduPlanVersion().developGridTerm().term().title()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler workPlanDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final DQLOrderDescriptionRegistry orderSettings = new DQLOrderDescriptionRegistry(EppWorkPlan.class, "wp");
                final DQLSelectBuilder dql = orderSettings.buildDQLSelectBuilder().column("wp");

                // Условие невалидности РУП
                {
                    // ссылающихся на блок специализации и имеющих семестр строго раньше семестра выбора профиля
                    IDQLExpression specExpr = and(
                            instanceOf("epvBlock", EppEduPlanVersionSpecializationBlock.class),
                            lt(property("wp", EppWorkPlan.term().intValue()), property("epv", EppEduPlanVersion.developGridTerm().term().intValue()))
                    );

                    // ссылающихся на основной блок и имеющих семестр не раньше семестра выбора профиля
                    IDQLExpression rootExpr = and(
                            instanceOf("epvBlock", EppEduPlanVersionRootBlock.class),
                            ge(property("wp", EppWorkPlan.term().intValue()), property("epv", EppEduPlanVersion.developGridTerm().term().intValue()))
                    );

                    dql.where(or(specExpr, rootExpr));
                }

                // Дальше копипаста из реестра РУП для фильтров

                dql.fetchPath(DQLJoinType.inner, EppWorkPlan.parent().fromAlias("wp"), "epvBlock");
                dql.fetchPath(DQLJoinType.inner, EppEduPlanVersionBlock.eduPlanVersion().fromAlias("epvBlock"), "epv");
                dql.fetchPath(DQLJoinType.inner, EppEduPlanVersion.eduPlan().fromAlias("epv"), "ep");

                FilterUtils.applySelectFilter(dql, EppEduPlan.programKind().fromAlias("ep"), context.get("programKind"));
                FilterUtils.applySelectFilter(dql, EppWorkPlan.term().fromAlias("wp"), context.get("term"));
                FilterUtils.applySelectFilter(dql, EppWorkPlan.state().fromAlias("wp"), context.get("state"));
                FilterUtils.applySelectFilter(dql, EppWorkPlan.number().fromAlias("wp"), context.get("number"));
                FilterUtils.applySelectFilter(dql, EppWorkPlan.year().fromAlias("wp"), context.get("yearEducationProcess"));

                EduProgramSubject programSubject = context.get("programSubject");
                if (programSubject != null) {
                    dql.fromEntity(EppEduPlanProf.class, "prof");
                    dql.where(eq(property("ep"), property("prof")));
                    dql.where(eq(property("prof", EppEduPlanProf.programSubject()), value(programSubject)));
                }
                Collection<EduProgramSpecialization> programSpecializations = context.get("programSpecializations");
                if (programSpecializations != null && !programSpecializations.isEmpty())
                {
                    dql.where(eqSubquery(property("epvBlock.id"), DQLSubselectType.any,
                                         new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, "bs")
                                                 .column("bs.id")
                                                 .where(in(property("bs", EppEduPlanVersionSpecializationBlock.programSpecialization()), programSpecializations))
                                                 .buildQuery()
                    ));
                }
                FilterUtils.applySelectFilter(dql, EppEduPlan.programForm().fromAlias("ep"), context.get("programFormList"));
                FilterUtils.applySelectFilter(dql, EppEduPlan.developCondition().fromAlias("ep"), context.get("developConditionList"));
                FilterUtils.applySelectFilter(dql, EppEduPlan.programTrait().fromAlias("ep"), context.get("programTraitList"));


                orderSettings.applyOrder(dql, input.getEntityOrder());

                return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();
            }
        };
    }
}