/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.workplan.WorkPlanFillDiscipline;

import org.apache.commons.collections15.Predicate;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.EppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.ui.EduPlanVersionBlockDataSourceGenerator;

import java.util.Collection;

/**
 * @author nkokorina
 * Created on: 06.08.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final EppWorkPlanBase eppWorkPlan = (EppWorkPlanBase)this.getSession().get(EppWorkPlanBase.class, model.getId());
        eppWorkPlan.getState().check_editable(eppWorkPlan);

        final Long blockId = eppWorkPlan.getBlock().getId();
        final IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(blockId, true);
        final Predicate<IEppEpvRowWrapper> workPlanRowPredicate = EppWorkPlanDAO.getPossibleEpvRowWrapperPredicate(eppWorkPlan);
        final Collection<IEppEpvRowWrapper> workPlanBlockRows = EppEduPlanVersionDataDAO.filterEduPlanBlockRows(blockWrapper.getRowMap(), workPlanRowPredicate, false);

        final StaticListDataSource<IEppEpvRowWrapper> rowDataSource = model.getRowDataSource();
        rowDataSource.setupRows(workPlanBlockRows);

        rowDataSource.getColumns().clear();
        rowDataSource.addColumn(this.wrap(new SimpleColumn("Индекс", "index").setWidth(1)));
        rowDataSource.addColumn(this.wrap(new SimpleColumn("Элемент УП", "title")));

    }

    @SuppressWarnings("unchecked")
    protected AbstractColumn wrap(final AbstractColumn column) {
        return EduPlanVersionBlockDataSourceGenerator.wrap(column);
    }

    @Override
    public void update(final Model model)
    {
        IEppWorkPlanDAO.instance.get().doGenerateWorkPlanRows(model.getId(), false);
    }
}
