/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentWithIncorrectWorkPlan;

import com.google.common.collect.Iterables;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListPresenter;

/**
 * @author Alexey Lopatin
 * @since 04.03.2015
 */
@State({
        @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
public class EppIndicatorStudentWithIncorrectWorkPlanUI extends AbstractEppIndicatorStudentListPresenter
{
    public static final String PROP_TERM = "term";
    public static final String PROP_DEVELOP_TECH = "developTech";

    private Term _firstTerm;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        if (null == _firstTerm)
            _firstTerm = Iterables.getFirst(DevelopGridDAO.getTermMap().values(), null);
        if (null == getSettings().get(PROP_TERM))
            getSettings().set(PROP_TERM, _firstTerm);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(PROP_TERM, getSettings().get(PROP_TERM));
        dataSource.put(PROP_DEVELOP_TECH, getSettings().get(PROP_DEVELOP_TECH));
    }

    @Override
    public void onClickClear()
    {
        super.onClickClear();
        getSettings().set(PROP_TERM, _firstTerm);
    }

    @Override
    public String getSettingsKey()
    {
        return "epp.StudentWithIncorrectWorkPlan.filter";
    }
}
