package ru.tandemservice.uniepp.dao.student;

import com.google.common.collect.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeALoad;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

import java.lang.reflect.Constructor;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
@SuppressWarnings("RedundantCast")
public class EppStudentSlotDAO extends UniBaseDao implements IEppStudentSlotDAO {

    public static final String GLOBAL_DAEMON_LOCK = "eppStudentSlotDAO.lock";

    public static final String LOCK_NAME_UPDATE_STUDENT_SLOT = EppStudentSlotDAO.class.getName()+".doUpdateStudentSlotList";

    public static final SyncDaemon DAEMON = new SyncDaemon(EppStudentSlotDAO.class.getName(), 60, EppStudentSlotDAO.GLOBAL_DAEMON_LOCK)
    {
        @Override protected void main() {
            try
            {
                // сначала валидируем данные по связям студента с УП и РУП
                try {
                    IEppStudentSlotDAO.instance.get().doUpdateStudentRelations(true);
                } catch (final Exception t) {
                    // поскольку это не основная часть работы демона - то забиваем на ошибку и идем дальше
                    this.logger.error(t.getMessage(), t);
                    Debug.exception(t.getMessage(), t);
                }

                // затем обновляем данные по слотам
                IEppStudentSlotDAO.instance.get().doUpdateStudentSlotList(true);
            }
            finally
            {
                // сначала выводим отладочную информацию
                if (this.logger.isInfoEnabled())
                {
                    UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
                        {
                            final int enabledSlots = ((Number)session.createCriteria(EppStudent2EduPlanVersion.class).add(Restrictions.isNull(EppStudent2EduPlanVersion.removalDate().s())).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            final int totalSlots = ((Number)session.createCriteria(EppStudent2EduPlanVersion.class).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            logger.info("s2epv="+enabledSlots+"/"+totalSlots);
                        }
                        {
                            final int enabledSlots = ((Number)session.createCriteria(EppStudent2WorkPlan.class).add(Restrictions.isNull(EppStudent2WorkPlan.removalDate().s())).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            final int totalSlots = ((Number)session.createCriteria(EppStudent2WorkPlan.class).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            logger.info("s2wp="+enabledSlots+"/"+totalSlots);
                        }
                        {
                            final int enabledSlots = ((Number)session.createCriteria(EppStudentWorkPlanElement.class).add(Restrictions.isNull(EppStudentWorkPlanElement.removalDate().s())).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            final int totalSlots = ((Number)session.createCriteria(EppStudentWorkPlanElement.class).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            logger.info("slot="+enabledSlots+"/"+totalSlots);
                        }
                        {
                            final int enabledSlots = ((Number)session.createCriteria(EppStudentWpeALoad.class).add(Restrictions.isNull(EppStudentWpeALoad.removalDate().s())).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            final int totalSlots = ((Number)session.createCriteria(EppStudentWpeALoad.class).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            logger.info("slot(a-load)="+enabledSlots+"/"+totalSlots);
                        }
                        {
                            final int enabledSlots = ((Number)session.createCriteria(EppStudentWpeCAction.class).add(Restrictions.isNull(EppStudentWpeCAction.removalDate().s())).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            final int totalSlots = ((Number)session.createCriteria(EppStudentWpeCAction.class).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            logger.info("slot(c-action)="+enabledSlots+"/"+totalSlots);
                        }

                        return null;
                    });
                }
            }
        }
    };

    // пробуждает демон если изменились связи студента и УП(в) и РУП(в)
    public static final TransactionCompleteAction<Boolean> TRANSACTION_AFTER_COMPLETE_REFRESH_RELATIONS_AND_WAKEUP = new TransactionCompleteAction<Boolean>() {
        @Override public Boolean beforeCompletion(final Session session) {
            if (EppStudentSlotDAO.DAEMON.isCurrentThread()) {
                return Boolean.FALSE; /* уже в демоне */
            }

            // делаем в той же транзакции, что и изменение связей c УП и РУП
            if(IEppStudentSlotDAO.instance.get().doUpdateStudentRelations(true)) {
                // нужно, потому что инече данные в базу не попадут
                session.flush();
                return Boolean.TRUE;
            }
            return Boolean.FALSE;
        }

        @Override public void afterCompletion(final Session session, final int status, final Boolean beforeCompleteResult) {
            // если что-то поменялось - пинаем демон
            if (Boolean.TRUE.equals(beforeCompleteResult)) { EppStudentSlotDAO.DAEMON.wakeUpDaemon(); }
        }
    };

    // пробуждает демон если изменился студент (хотим отследить изменение НПП студента)
    public static final TransactionCompleteAction<Boolean> TRANSACTION_AFTER_COMPLETE_REFRESH_STUDENT_AND_WAKEUP = new TransactionCompleteAction<Boolean>() {
        @Override public Boolean beforeCompletion(final Session session) {
            if (EppStudentSlotDAO.DAEMON.isCurrentThread()) {
                return Boolean.FALSE; /* уже в демоне */
            }

            // делаем в той же транзакции, что и изменение студента
            if (IEppStudentSlotDAO.instance.get().doUpdateStudentRelations(false)) {
                // нужно, потому что инече данные в базу не попадут
                session.flush();
                return Boolean.TRUE;
            }
            return Boolean.FALSE;
        }

        @Override public void afterCompletion(final Session session, final int status, final Boolean beforeCompleteResult) {
            // если что-то поменялось - пинаем демон
            if (Boolean.TRUE.equals(beforeCompleteResult)) { EppStudentSlotDAO.DAEMON.wakeUpDaemon(); }
        }
    };


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////   Обновление списков МСРП, МСРП-АН, МСРП-ФК   ///////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    /** актуализирует список дисциплин студентов (когда какая часть какой дисциплины должна проходится) */
    @Override
    public boolean doUpdateStudentSlotList(final boolean activeStudentOnly)
    {
        final MutableBoolean result = new MutableBoolean(false);
        final Date now = new Date();
        final Session session = getSession();
        NamedSyncInTransactionCheckLocker.register(session, EppStudentSlotDAO.LOCK_NAME_UPDATE_STUDENT_SLOT);

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {

            // поехали...
            Debug.begin("EppStudentSlotDAO.doUpdateStudentSlotList");
            try
            {
                Debug.begin("EppStudentSlotDAO.doUpdateStudentSlotList.deactivate-slot (slot-is-null)");
                try
                {
                    final DQLUpdateBuilder update = new DQLUpdateBuilder(EppStudentWorkPlanElement.class)
							.set(EppStudentWorkPlanElement.P_REMOVAL_DATE, value(now, PropertyType.DATE))
							.where(isNull(property(EppStudentWorkPlanElement.removalDate())))
							.where(isNull(property(EppStudentWorkPlanElement.sourceRow())));
                    final int updateCount = executeAndClear(update);
                    if (updateCount > 0)
                    {
                        Debug.message("updated-rows="+updateCount);
                        result.setValue(true);
                    }
                }
                finally
                {
                    Debug.end();
                }

                Debug.begin("EppStudentSlotDAO.doUpdateStudentSlotList.deactivate-slot (not-in-list)");
                try
                {
	                DQLSelectBuilder wpRow4Wpe = getStudentSlotBaseDQL(activeStudentOnly)
			                .column(property("wpRow.id"))
			                .where(and(
					                eq(property("wpe", EppStudentWorkPlanElement.student()), property("s")),
					                eq(property("wpe", EppStudentWorkPlanElement.registryElementPart()), property("wpRow", EppWorkPlanRegistryElementRow.registryElementPart())),
					                eq(property("wpe", EppStudentWorkPlanElement.year()), property("s2wp", EppStudent2WorkPlan.cachedEppYear())),
					                eq(property("wpe", EppStudentWorkPlanElement.course()), property("gt", DevelopGridTerm.course())),
					                eq(property("wpe", EppStudentWorkPlanElement.term()), property("gt", DevelopGridTerm.term())),
					                eq(property("wpe", EppStudentWorkPlanElement.part()), property("gt", DevelopGridTerm.part()))
			                ));
	                DQLSelectBuilder wpeWithoutWpRow = new DQLSelectBuilder()
			                .fromEntity(EppStudentWorkPlanElement.class, "wpe")
			                .column(property("wpe.id"), "slot_id")
			                .where(notExists(wpRow4Wpe.buildQuery()));
                    final DQLUpdateBuilder update = new DQLUpdateBuilder(EppStudentWorkPlanElement.class)
							.set(EppStudentWorkPlanElement.P_REMOVAL_DATE, valueDate(now))
							.where(isNull(property(EppStudentWorkPlanElement.removalDate())))
							.fromDataSource(wpeWithoutWpRow.buildQuery(), "xxx")
							.where(eq(property("id"), property("xxx.slot_id")));

                    final int updateCount = executeAndClear(update);
                    if (updateCount > 0)
                    {
                        Debug.message("updated-rows="+updateCount);
                        result.setValue(true);
                    }
                }
                finally
                {
                    Debug.end();
                }

                Debug.begin("EppStudentSlotDAO.doUpdateStudentSlotList.create-or-update");
                try {
                    // FIXME: как победить людей, которые в один РУП добавляют одни и те же части несколько раз???
                    // FIXME: если в этом месте падает UniqueConstraint то ошибка скорее всего в том, что в РУПах есть одинаковые дисциплино-части (что есть ошибка системы)
                    // FIXME: использование distinct на больших объемах приводит к !существенному! замедлению  даже на корректных состояниях системы

	                DQLSelectBuilder wpRowWithoutActiveWpe = getStudentSlotBaseDQL(activeStudentOnly)
			                .joinEntity("s2wp", DQLJoinType.left, EppStudentWorkPlanElement.class, "wpe", and(
									eq(property("wpe", EppStudentWorkPlanElement.student()), property("s")),
									eq(property("wpe", EppStudentWorkPlanElement.registryElementPart()), property("wpRow", EppWorkPlanRegistryElementRow.registryElementPart())),
									eq(property("wpe", EppStudentWorkPlanElement.year()), property("s2wp", EppStudent2WorkPlan.cachedEppYear())),
									eq(property("wpe", EppStudentWorkPlanElement.course()), property("gt", DevelopGridTerm.course())),
									eq(property("wpe", EppStudentWorkPlanElement.term()), property("gt", DevelopGridTerm.term())),
									eq(property("wpe", EppStudentWorkPlanElement.part()), property("gt", DevelopGridTerm.part()))
			                ))
			                .where(or(
									// либо слота нет
									isNull(property("wpe", EppStudentWorkPlanElement.id())),

									// либо он не актуален
									isNotNull(property("wpe", EppStudentWorkPlanElement.removalDate())),

									// либо соответсвует другой стоке РУП
									ne(property("wpe", EppStudentWorkPlanElement.sourceRow().id()), property("wpRow", EppWorkPlanRegistryElementRow.id()))
			                ))
			                .order(property("s.id"))
	                        .order(property("wpRow", EppWorkPlanRegistryElementRow.registryElementPart().id()));


                    final DQLSelectColumnNumerator dqlColumns = new DQLSelectColumnNumerator(wpRowWithoutActiveWpe);

                    final int studentIdCol =    dqlColumns.column(property("s.id"));
                    final int regElementIdCol = dqlColumns.column(property("wpRow", EppWorkPlanRegistryElementRow.registryElementPart().id()));
                    final int wpRowIdCol =      dqlColumns.column(property("wpRow.id"));
                    final int eppYearIdCol =    dqlColumns.column(property("s2wp", EppStudent2WorkPlan.cachedEppYear().id()));
                    final int courseIdCol =     dqlColumns.column(property("gt", DevelopGridTerm.course().id()));
                    final int termIdCol =       dqlColumns.column(property("gt", DevelopGridTerm.term().id()));
                    final int yearPartIdCol =   dqlColumns.column(property("gt", DevelopGridTerm.part().id()));
                    final int wpeIdCol =        dqlColumns.column(property("wpe", "id"));

                    final Map<Long, EppYearEducationProcess> eppYearCache = EppStudentSlotDAO.this.getLoadCacheMap(EppYearEducationProcess.class);
                    final Map<Long, Course> courseCache = EppStudentSlotDAO.this.getLoadCacheMap(Course.class);
                    final Map<Long, Term> termCache = EppStudentSlotDAO.this.getLoadCacheMap(Term.class);
                    final Map<Long, YearDistributionPart> yearPartCache = EppStudentSlotDAO.this.getLoadCacheMap(YearDistributionPart.class);

                    for (List<Object[]> rows : Lists.partition(dqlColumns.getDql().createStatement(session).<Object[]>list(), DQL.MAX_VALUES_ROW_NUMBER)) {

                        final Map<Long, Student> studentCache = EppStudentSlotDAO.this.getLoadCacheMap(Student.class);
                        final Map<Long, EppRegistryElementPart> registryElementPartCache = EppStudentSlotDAO.this.getLoadCacheMap(EppRegistryElementPart.class);

                        final Map<Long, EppStudentWorkPlanElement> id2Wpe = new HashMap<>(rows.size());

                        // грузим слоты из базы отдельным запросом (через in)
                        {
                            for (final Object[] row: rows) {
                                final Long slotId = (Long)row[wpeIdCol];
                                if (null != slotId) { id2Wpe.put(slotId, null); }
                            }
                            for (final EppStudentWorkPlanElement slot: EppStudentSlotDAO.this.getList(EppStudentWorkPlanElement.class, "id", id2Wpe.keySet())) {
                                id2Wpe.put(slot.getId(), slot);
                            }
                        }

                        // обрабатываем результат
                        for (final Object[] row: rows)
                        {
                            EppStudentWorkPlanElement wpe = id2Wpe.get((Long) row[wpeIdCol]);
                            if (null == wpe)
                            {
                                wpe = new EppStudentWorkPlanElement(
                                        studentCache.get((Long) row[studentIdCol]),
                                        registryElementPartCache.get((Long) row[regElementIdCol]),
                                        eppYearCache.get((Long) row[eppYearIdCol]),
                                        courseCache.get((Long) row[courseIdCol]),
                                        termCache.get((Long) row[termIdCol]),
                                        yearPartCache.get((Long) row[yearPartIdCol])
                                );
                            }
                            // всегда обновляем данные об источнике и даты (в списке только те объекты, которые действительно требуется реактуализировать)
                            wpe.setSourceRow((EppWorkPlanRow) session.load(EppWorkPlanRow.class, (Long) row[wpRowIdCol])); // для этой штуки КЭШ не нужен - она почти всегда уникальная
                            wpe.setRemovalDate(null);
                            wpe.setModificationDate(now);
                            session.saveOrUpdate(wpe);
                        }

                        session.flush();
                        session.clear();
                        EppStudentSlotDAO.this.infoActivity('.');
                        result.setValue(true);
                    }

                } finally {
                    Debug.end();
                }

            } finally {
                Debug.end();
            }

            if (doUpdateEppStudentWpALoadSlotList(now)) { result.setValue(true); }
            if (doUpdateEppStudentWpCActionSlotList(now)) { result.setValue(true); }
	        if (deleteOrphanInactiveWpes())
		        result.setValue(true);

        } finally
        {
            eventLock.release();
        }

        return result.booleanValue();
    }

	/**
	 * Построить запрос к строкам РУП с частями элементов дисциплин ({@link EppWorkPlanRegistryElementRow}), у которых связи студентов с РУП и УП(в) актуальны.
	 * Содержит следующие колонки:
	 * <ul>
	 *     <li/> строка РУП;
	 *     <li/> связь студента с РУП ({@link EppStudent2WorkPlan}), соответствующая тому же РУП, что и строка;
	 *     <li/> семестр учебной сетки ({@link DevelopGridTerm}) из строки;
	 *     <li/> студент из связи студента с УП(в).
	 * </ul>
	 * Если задана настройка "Учитывать согласование объектов", то учитываются только РУП в состоянии "Согласовано".
	 * @param activeStudentOnly Учитывать только неархивных студентов в активном состоянии.
	 */
    private DQLSelectBuilder getStudentSlotBaseDQL(boolean activeStudentOnly)
    {
	    final Long eppStateAcceptedId = getId(EppState.class, EppState.P_CODE, EppState.STATE_ACCEPTED);
        final DQLSelectBuilder dql = new DQLSelectBuilder()
				.fromEntity(EppWorkPlanRegistryElementRow.class, "wpRow")
				.joinEntity("wpRow", DQLJoinType.inner, EppStudent2WorkPlan.class, "s2wp",
						eq(property("s2wp", EppStudent2WorkPlan.workPlan()), property("wpRow", EppWorkPlanRegistryElementRow.workPlan())))
				.joinPath(DQLJoinType.inner, EppStudent2WorkPlan.cachedGridTerm().fromAlias("s2wp"), "gt")
				.joinPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("s2wp"), "s2epv")
				.joinPath(DQLJoinType.inner, EppStudent2EduPlanVersion.student().fromAlias("s2epv"), "s")

				// связи должны быть актуальными
				.where(isNull(property("s2wp", EppStudent2WorkPlan.removalDate())))
				.where(isNull(property("s2epv", EppStudent2EduPlanVersion.removalDate())));

        // студент должен быть живой  и состояние у него тоже должно быть живее всех живых
        // (null == activeStudentStatusIds означает, что проверять нчего не надо)
        if (activeStudentOnly)
        {
            dql.where(eq(property("s", Student.archival()), value(Boolean.FALSE)));
            dql.where(in(property("s", Student.status().id()), getActiveStudentStatusIds()));
        }

        // учет состояния с учетом настройки
        if (IEppSettingsDAO.instance.get().getGlobalSettings().isCheckElementState())
            dql.where(eq(property("s2wp", EppStudent2WorkPlan.workPlan().state().id()), value(eppStateAcceptedId)));
        return dql;
    }

	/** Активные состояния студентов (пока там мало элементов - это лучше, чем делать join на таблицу условием) */
	private List<Long> getActiveStudentStatusIds()
	{
		return new DQLSelectBuilder()
				.fromEntity(StudentStatus.class, "s").column(property("s", StudentStatus.id()))
				.where(eq(property("s", StudentStatus.active()), value(Boolean.TRUE)))
				.createStatement(getSession()).list();
	}


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected DQLSelectBuilder getActiveWpeDql(final String alias)
    {
        return new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, alias)
				.column(property(alias, EppStudentWorkPlanElement.id()), "slot_id")
				.where(isNull(property(alias, EppStudentWorkPlanElement.removalDate())));
    }

    protected boolean doUpdateEppStudentWpALoadSlotList(final Date now)
    {
	    final String wpeAlias = "sl";
	    final String regPartModuleAlias = "rm";
	    final String moduleLoadAlias = "ml";
	    DQLSelectBuilder dql = getActiveWpeDql(wpeAlias)
			    .joinEntity(wpeAlias, DQLJoinType.inner, EppRegistryElementPartModule.class, regPartModuleAlias,
					    eq(property(regPartModuleAlias, EppRegistryElementPartModule.part()), property(wpeAlias, EppStudentWorkPlanElement.registryElementPart())))
			    .joinEntity(regPartModuleAlias, DQLJoinType.inner, EppRegistryModuleALoad.class, moduleLoadAlias, and(
					    eq(property(moduleLoadAlias, EppRegistryModuleALoad.module()), property(regPartModuleAlias, EppRegistryElementPartModule.module())),
					    gt(property(moduleLoadAlias, EppRegistryModuleALoad.load()), value(0L))
			    ))
			    .distinct()
			    .column(property(moduleLoadAlias, EppRegistryModuleALoad.loadType().eppGroupType().id()), "type_id");
        return doUpdateStudentSlotRowList(now, EppStudentWpeALoad.class, dql);
    }

    protected boolean doUpdateEppStudentWpCActionSlotList(final Date now)
    {
	    final String wpeAlias = "sl";
	    final String fcaAlias = "ca";
	    DQLSelectBuilder dql = getActiveWpeDql(wpeAlias)
			    .joinEntity(wpeAlias, DQLJoinType.inner, EppRegistryElementPartFControlAction.class, fcaAlias, and(
					    eq(property(fcaAlias, EppRegistryElementPartFControlAction.part()), property(wpeAlias, EppStudentWorkPlanElement.registryElementPart()))
			    ))
			    .distinct()
			    .column(property(fcaAlias, EppRegistryElementPartFControlAction.controlAction().eppGroupType().id()), "type_id");
        return doUpdateStudentSlotRowList(now, EppStudentWpeCAction.class, dql);
    }



    protected boolean doUpdateStudentSlotRowList(final Date now, final Class<? extends EppStudentWpePart> slotKlass, final DQLSelectBuilder sourceDql)
    {
        final MutableBoolean result = new MutableBoolean(false);

        final String message = "EppStudentSlotDAO.doUpdateEppStudentSlotRowList("+slotKlass.getSimpleName()+")";
        Debug.begin(message);
        try
        {
            final Session session = getSession();
            Debug.begin(message+".deactivate-slot");
            try
            {
                // список актуальных МСРП по видам нагрузки, для которых нет актуальных МСРП (из запроса)
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(slotKlass, "xxx")
                .where(isNull(property("xxx", EppStudentWpePart.removalDate())))
                .joinDataSource("xxx", DQLJoinType.left, sourceDql.buildQuery(), "tmp", and(
                        eq(property("tmp.slot_id"), property("xxx", EppStudentWpePart.studentWpe().id())),
                        eq(property("tmp.type_id"), property("xxx", EppStudentWpePart.type().id()))
                )).where(isNull(property("tmp.slot_id")));

                final DQLUpdateBuilder upd = new DQLUpdateBuilder(slotKlass);
                upd.set(EppStudentWpePart.P_REMOVAL_DATE, value(now, PropertyType.DATE));
                upd.where(in(property("id"), dql.column(property("xxx.id")).buildQuery()));

                final int updateCount = executeAndClear(upd);
                if (updateCount > 0)
                {
                    Debug.message("updated-rows="+updateCount);
                    result.setValue(true);
                }

            } finally {
                Debug.end();
            }

            Debug.begin(message+".create-or-update-slot");
            try
            {
                // список МСРП свидами нагрузки (из запроса), по которым нет или не актуален слот
                final DQLSelectBuilder dql = new DQLSelectBuilder()
						.fromDataSource(sourceDql.buildQuery(), "tmp")
						.joinEntity("tmp", DQLJoinType.left, slotKlass, "xxx", and(
						        eq(property("tmp.slot_id"), property("xxx", EppStudentWpePart.studentWpe().id())),
						        eq(property("tmp.type_id"), property("xxx", EppStudentWpePart.type().id()))
						))
						.where(or(
						        isNull(property("xxx", EppStudentWpePart.id())) /* либо нет слота */,
						        isNotNull(property("xxx", EppStudentWpePart.removalDate())) /* либо слот неактуальный */
						))

						.column(property("tmp.slot_id"), "slot_id")
						.column(property("tmp.type_id"), "type_id")
						.column(property("xxx.id"), "xxx_id")
						.distinct();

                final List<Object[]> rows = dql.createStatement(session).list();
                if (rows.size() > 0)
                {
                    try
                    {
                        final Constructor<? extends EppStudentWpePart> constructor = slotKlass.getConstructor(EppStudentWorkPlanElement.class, EppGroupType.class);
                        final Map<Long, EppGroupType> typeCache = getLoadCacheMap(null);

                        for (List<Object[]> rowsPart : Lists.partition(rows, 256))
                        {
                            // TODO: XXX: подумать, как поюзать массовое обновление в базе (вместо session.saveOrUpdate)

                            final Map<Long, EppStudentWpePart> map = new HashMap<>(rowsPart.size());
                            for (final Object[] row: rowsPart)
                            {
                                final Long id = (Long)row[2];
                                if (null != id) { map.put(id, null); }
                            }

                            for (final EppStudentWpePart slotRow: EppStudentSlotDAO.this.getList(slotKlass, "id", map.keySet()))
                                map.put(slotRow.getId(), slotRow);

                            for (final Object[] row: rowsPart)
                            {
                                final Long slotId = (Long)row[0];
                                final Long typeId = (Long)row[1];
                                EppStudentWpePart slotRow = map.get((Long) row[2]);

                                if (null == slotRow)
                                {
                                    final EppStudentWorkPlanElement slot = (EppStudentWorkPlanElement)session.load(EppStudentWorkPlanElement.class, slotId);
                                    final EppGroupType type = typeCache.get(typeId);
                                    slotRow = constructor.newInstance(slot, type);
                                }

                                slotRow.wakeup(now);
                                session.saveOrUpdate(slotRow);
                            }

                            session.flush();
                            session.clear();
                            EppStudentSlotDAO.this.infoActivity('~');
                            result.setValue(true);
                        }

                    } catch (final Exception t) {
                        throw CoreExceptionUtils.getRuntimeException(t);
                    }
                    session.flush();
                }

            } finally {
                Debug.end();
            }
        } finally {
            Debug.end();
        }

        return result.booleanValue();
    }

	/** Удалить неактуальные МСРП, на которые не ссылается ни одна из сущностей из заданного списка (см. {@link EppStudentSlotDAO#modulesToBlockingWpeDeleteEntities}). */
	private boolean deleteOrphanInactiveWpes()
	{
		boolean wpeDeleted = false;
		Collection<ModuleMeta> presentModules = ApplicationRuntime.getModules();
		Collection<BlockingDeleteWpeEntity> presentEntityClasses = getEntitiesReferredToWpe(presentModules);
		Collection<Long> orphanInactiveWpes = getOrphanInactiveWpe(presentEntityClasses);

		// Сущности, которые могут ссылаться на МСРП, не блокируя удаление. Экземпляры таких сущностей нужно удалить перед МСРП.
		// Сущности других типов удалять не надо - среди них никто не ссылается на соответствующие МСРП.
		Collection<BlockingDeleteWpeEntity> nonBlockingEntities = presentEntityClasses.stream().filter(BlockingDeleteWpeEntity::maybeNonBlockingInstances).collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(orphanInactiveWpes))
		{
			Session session = getSession();
			Iterables.partition(orphanInactiveWpes, DQL.MAX_VALUES_ROW_NUMBER).forEach(wpeIds -> {
				// Поскольку есть только такие сущности, которые не блокируют удаление МСРП, то можно удалять все без разбора (для нужных МСРП)
				nonBlockingEntities.forEach(e -> e.deleteDqlReferredToWpe(wpeIds).createStatement(session).execute());
				deleteWpes(wpeIds);
			});
			wpeDeleted = true;
		}
		return wpeDeleted;
	}

	/**
	 * Описание сущности, которая может ссылаться на МСРП. Если ссылка из такой сущности есть, то неактивное МСРП удалять нельзя.
	 * Класс нужно получать в рантайме, т.к. сущности находятся в модулях, зависящих от Учебного процесса (а сам Уч. процесс про них ничего не знает).
	 */
	private static class BlockingDeleteWpeEntity
	{
		public final String name;
		public final String wpeProperty;
		public Class entityClass = null;

		private DQLModifier dqlModifier = null;

		public BlockingDeleteWpeEntity(String name, String wpeProperty)
		{
			this.name = name;
			this.wpeProperty = wpeProperty;
		}

		public BlockingDeleteWpeEntity(String name, String wpeProperty, DQLModifier dqlModifier)
		{
			this(name, wpeProperty);
			this.dqlModifier = dqlModifier;
		}

		/**
		 * DQL-запрос к сущности, ссылающейся на МСРП. Используется в качестве подзапроса в exist-запросе.
		 * @param wpeAlias Алиас МСРП.
		 * @return "SELECT x FROM name WHERE (x.wpeProperty = wpeAlias)"
		 */
		public DQLSelectBuilder dqlReferredToWpe(final String wpeAlias)
		{
			final String alias = name + "ReferredToWpe";
			DQLSelectBuilder dql =  new DQLSelectBuilder().fromEntity(entityClass, alias)
					.where(eq(property(alias, wpeProperty), property(wpeAlias)));
			return (dqlModifier == null) ? dql : dqlModifier.modifyDql(dql, alias);
		}

		/** Могут быть особые экземпляры этой сущности, ссылающиеся на МСРП, но не блокирующие его удаление. */
		public boolean maybeNonBlockingInstances()
		{
			return dqlModifier != null;
		}

		/**
		 * Запрос на удаление сущности, ссылающейся на МСРП. Имеет смысл только тогда, когда допускается наличие сущности, ссылающейся на МСРП, но не блокирующей удаление.
		 * @param wpeIds id МСРП.
		 * @return {@code null}, если нет доп. условий к сущности ({@link BlockingDeleteWpeEntity#maybeNonBlockingInstances} = {@code false}),
		 *          иначе - {@code DELETE x FROM name WHERE (x.wpeProperty.id in wpeIds)}.
		 */
		public DQLDeleteBuilder deleteDqlReferredToWpe(Collection<Long> wpeIds)
		{
			if (!maybeNonBlockingInstances())
				return null;
			return new DQLDeleteBuilder(name).where(in(property(wpeProperty + ".id"), wpeIds));
		}

		/** Лямбда, добавляющася доп. условия к сущности, ссылающейся на МСРП (т.е. блокируют удаление только некоторые экземпляры сущности, без доп. условий). */
		public interface DQLModifier
		{
			DQLSelectBuilder modifyDql(DQLSelectBuilder dql, String entityAlias);
		}
	}

	/** Помодульный список сущностей, которые могут ссылаться на МСРП. */
	private static final Multimap<String, BlockingDeleteWpeEntity> modulesToBlockingWpeDeleteEntities = HashMultimap.create();

	static
	{
		// Модуль "Учебный процесс": "Запись студента в УГС".
		modulesToBlockingWpeDeleteEntities.putAll("uniepp", ImmutableList.of(new BlockingDeleteWpeEntity("eppRealEduGroupRow", "studentWpePart.studentWpe")));

		/** Запись в документе сессии, кроме записей из электронной зачетки (т.к. такие записи создаются автоматически в демоне
		 * {@link ru.tandemservice.unisession.base.bo.SessionMark.daemon.SessionMarkDaemonBean}) и блокируют удаление*/
		final BlockingDeleteWpeEntity.DQLModifier documentSlotWhereExpression = (dql, entityAlias) -> {
			final Class gradeBookClass = EntityRuntime.getMeta("sessionStudentGradeBookDocument").getEntityClass();
			final String docAlias = "slotDocument";
			dql.joinPath(DQLJoinType.left, entityAlias + ".document", docAlias)
						.where(notInstanceOf(docAlias, gradeBookClass));
			return dql;
		};
		// Модуль "Сессия": "Запись в документе сессии", "Запись в атт. ведомости", "Элемент портфолио студента".
		modulesToBlockingWpeDeleteEntities.putAll("unisession", ImmutableList.of(
				new BlockingDeleteWpeEntity("sessionDocumentSlot", "studentWpeCAction.studentWpe", documentSlotWhereExpression),
				new BlockingDeleteWpeEntity("sessionAttestationSlot", "studentWpe"),
				new BlockingDeleteWpeEntity("studentPortfolioElement", "wpeCAction.studentWpe")
		));

		// Модуль "БРС и журналы": "Запись в журнале", "Запись по событию в журнале".
		modulesToBlockingWpeDeleteEntities.putAll("unitraining", ImmutableList.of(
				new BlockingDeleteWpeEntity("trJournalGroupStudent", "studentWpe"),
				new BlockingDeleteWpeEntity("trEduGroupEventStudent", "studentWpe")
		));

		// Модуль"Практики студентов": "Напрвление на практику".
		modulesToBlockingWpeDeleteEntities.putAll("unipractice", ImmutableList.of(new BlockingDeleteWpeEntity("prPracticeAssignment", "practice")));
	}

	/**
	 * По списку модулей получить те описания классов сущностей (из заданного списка), ссылающихся на МСРП, которые находятся в этих модулях.
	 * @param entityModules Список модулей.
	 */
	private static Collection<BlockingDeleteWpeEntity> getEntitiesReferredToWpe(Collection<ModuleMeta> entityModules)
	{
		Collection<BlockingDeleteWpeEntity> presentEntities = entityModules.stream()
				.map(ModuleMeta::getName)
				.filter(modulesToBlockingWpeDeleteEntities::containsKey)
				.flatMap(module -> modulesToBlockingWpeDeleteEntities.get(module).stream())
				.collect(Collectors.toList());
		presentEntities.forEach(entity -> entity.entityClass = EntityRuntime.getMeta(entity.name).getEntityClass());
		return presentEntities;
	}

	/**
	 * Получить неактуальные МСРП, на которые не ссылается ни одна из сущностей.
	 * @param entitiesReferredToWpe Сущности, которые могут ссылаться на МСРП.
	 * @return id неактуальных МСРП.
	 */
	private Collection<Long> getOrphanInactiveWpe(Collection<BlockingDeleteWpeEntity> entitiesReferredToWpe)
	{
		final String wpeAlias = "studentWpe";
		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, wpeAlias)
				.column(property(wpeAlias, EppStudentWorkPlanElement.id()))
				.where(isNotNull(property(wpeAlias, EppStudentWorkPlanElement.removalDate())));
		entitiesReferredToWpe.forEach(entity -> dql.where(notExists(entity.dqlReferredToWpe(wpeAlias).buildQuery())));
		return dql.createStatement(getSession()).list();
	}

	/**
	 * Удалить МСРП и ссылающиеся на них МСРП по нагрузке (МСРП-ФК и МСРП-АН).
	 * @param wpeIds id удаляемых МСРП.
	 */
	private void deleteWpes(Collection<Long> wpeIds)
	{
		DQLDeleteBuilder wpePartDql = new DQLDeleteBuilder(EppStudentWpePart.class).where(in(property(EppStudentWpePart.studentWpe().id()), wpeIds));
		executeAndClear(wpePartDql);
		DQLDeleteBuilder wpeDql = new DQLDeleteBuilder(EppStudentWorkPlanElement.class).where(in(property("id"), wpeIds));
		executeAndClear(wpeDql);
	}


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////   Обновление связей студентов с УП(в) и РУПами   /////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////


    protected boolean doUpdateStudent2WorkPlanRelations()
    {
        boolean changed = false;

        final Date now = new Date();
        final Session session = getSession();
        NamedSyncInTransactionCheckLocker.register(session, EppStudentSlotDAO.class.getName()+".doUpdateStudent2WorkPlanRelations");

        Debug.begin("EppStudentSlotDAO.doUpdateStudent2WorkPlanRelations");
        try {

            Debug.begin("EppStudentSlotDAO.doUpdateStudent2WorkPlanRelations.s2wp-by-s2epv-disable");
            try {
                /*
                 * для тех связей студента-с-РУП, по которым ссылки на УП уже неактуальные,
                 * выставляем дату неактуальности - текущую (т.е. делаем их также неактуальными)
                 */

                final DQLSelectBuilder nql_g = new DQLSelectBuilder()
						.fromEntity(EppStudent2WorkPlan.class, "s2wp")
						.column(property("s2wp.id"))
						.where(isNull(property("s2wp", EppStudent2WorkPlan.removalDate())))
						.where(isNotNull(property("s2wp", EppStudent2WorkPlan.studentEduPlanVersion().removalDate())));

                // обновляем список связей
                final DQLUpdateBuilder upd = new DQLUpdateBuilder(EppStudent2WorkPlan.class)
						.set(EppStudent2WorkPlan.P_REMOVAL_DATE, value(now, PropertyType.DATE))
						.where(in(property("id"), nql_g.buildQuery())); // но уже не валидна

                final int update = executeAndClear(upd);
                if (update > 0) {
                    changed = true;

                    final String message = update+" relations marked to be disabled";
                    Debug.message(message);
                    this.logger.info(message);
                }

            } finally {
                Debug.end();
            }

            Debug.begin("EppStudentSlotDAO.doUpdateStudent2WorkPlanRelations.s2wp-uniqure-constraint (term)");
            try {
                // необходимо проверить, есть ли у нас активные связи студента-с-РУП, в которых к студенту привязаны несколько РУП на один и тот же семестр
                changed = doCheckEppStudent2WorkPlanConstraint(now, EppStudent2WorkPlan.cachedGridTerm().term().id()) | changed;
            } finally {
                Debug.end();
            }

            Debug.begin("EppStudentSlotDAO.doUpdateStudent2WorkPlanRelations.s2wp-uniqure-constraint (year+part)");
            try {
                // необходимо проверить, есть ли у нас активные связи студента-с-РУП, в которых к студенту привязаны несколько РУП на одну и ту же комбинацию года и части года
                changed = doCheckEppStudent2WorkPlanConstraint(now, EppStudent2WorkPlan.cachedEppYear().educationYear().id(), EppStudent2WorkPlan.cachedGridTerm().part().id()) | changed;
            } finally {
                Debug.end();
            }

        } finally {
            Debug.end();
        }

        return changed;
    }

	/**
	 * Проверить наличие для одного студента ({@link EppStudent2EduPlanVersion}) более одной актуальной связи студента и РУП ({@link EppStudent2WorkPlan}) с одинаковыми параметрами.
	 * Если для студента нашлось более одной такой связи, то все, кроме самой последней, помечаются как неактуальные ({@link EppStudent2WorkPlan#removalDate} задается текущая дата).
	 * @param now Текущая дата, которая будет задана в качестве даты утраты актуальности дублирующим связям.
	 * @param constraint Параметры, которые должны совпадать у связи, чтобы они считались дублями.
	 * @return Нашлись ли связи, которые потом были помечены как неактуальные.
	 */
    protected boolean doCheckEppStudent2WorkPlanConstraint(final Date now, final PropertyPath ...constraint)
    {
        if ((null == constraint) || (constraint.length <= 0))
	        return false;

	    final Set<Long> sepvIds = getDuplicateStudent2WP(constraint);
	    if (sepvIds.isEmpty())
		    return false;

	    return updateDuplicateStudent2WP(now, sepvIds, constraint);
    }

	/**
	 * Получить список {@link EppStudent2EduPlanVersion}, для которых есть более одной актуальной связи студента и РУП ({@link EppStudent2WorkPlan}) с одинаковыми параметрами.
	 * @param constraint Параметры, которые должны совпадать у связи, чтобы они считались дублями.
	 * @return id связей студента с УП(в).
	 */
	private Set<Long> getDuplicateStudent2WP(PropertyPath[] constraint)
	{
		final Set<Long> duplicateEpvIds = new HashSet<>();
		final DQLSelectBuilder dql_g = new DQLSelectBuilder()
				.fromEntity(EppStudent2WorkPlan.class, "s2wp")
				.where(isNull(property("s2wp", EppStudent2WorkPlan.removalDate())))
				.column(property("s2wp", EppStudent2WorkPlan.studentEduPlanVersion().id()))
				.column(DQLFunctions.count(property("s2wp.id")))
				.group(property("s2wp", EppStudent2WorkPlan.studentEduPlanVersion().id()))
				.having(gt(DQLFunctions.count(property("s2wp.id")), value(1)));

		for (final MetaDSLPath dsl: constraint)
		{
			final MetaDSLPath fromAlias = dsl.fromAlias("s2wp");
			dql_g.column(property(fromAlias));
			dql_g.group(property(fromAlias));
		}

		final List<Object[]> rows_g = dql_g.createStatement(getSession()).list();
		if (rows_g.isEmpty())
			return new HashSet<>();

		for (final Object[] row: rows_g)
		{
			// делаем для каждой пары, т.к. их не много должно быть - поидее вообще не должно быть,
			// но все же проверял методом кривых рук в базе

			final Long epvId = (Long)row[0];
			duplicateEpvIds.add(epvId);

			final int count = ((Number)row[1]).intValue();
			final StringBuilder error = new StringBuilder("Student s2epv=`"+epvId+"' has `"+count+"' active workplan relation, connected to");
			for (int i=0;i<constraint.length;i++) {
				error.append(" ").append(constraint[i].toString()).append("=").append(row[2+i]);
			}

			final String message = error.toString();
			Debug.message(message);
			this.logger.warn(message);
		}
		return duplicateEpvIds;
	}

	/**
	 * Среди всех связей студента и РУП ({@link EppStudent2WorkPlan}), имеющих одинаковые параметры для одного и того же студента ({@link EppStudent2EduPlanVersion}),
	 * оставить только последнюю, а остальные сделать неактуальными.
	 * @param now Дата утраты актуальности, которая будет выставлена дублирующим связям студента и РУП (текущая дата).
	 * @param sepvIds id связей студента и УП(в).
	 * @param constraint Параметры, которые должны совпадать у связи, чтобы они считались дублями.
	 * @return Были ли выставлены каким-либо связям даты утраты актуальности.
	 */
	private boolean updateDuplicateStudent2WP(Date now, Set<Long> sepvIds, PropertyPath[] constraint)
	{
		boolean changed = false;
		for (final Long sepvId: sepvIds)
		{
		    // для каждой связи студента с УПВ делаем перепривязку отдельно
		    // здесь - только те связи, для которых есть ошибки
		    // (так что можно себе позволить делать по запросу на связь)

		    final DQLSelectBuilder dql = new DQLSelectBuilder()
							.fromEntity(EppStudent2WorkPlan.class, "s2wp")
							.where(isNull(property("s2wp", EppStudent2WorkPlan.removalDate())))
							.where(eq(property("s2wp", EppStudent2WorkPlan.studentEduPlanVersion().id()), value(sepvId)))
							.column(property("s2wp.id"))
							.order(property("s2wp", EppStudent2WorkPlan.cachedGridTerm().term().intValue()))
							.order(property("s2wp.id"));

		    for (final MetaDSLPath dsl: constraint)
		        dql.column(property("s2wp", dsl));

		    final Map<MultiKey, Long> key2lastRelation = new HashMap<>();
		    for (final Object[] row: CommonDAO.scrollRows(dql.createStatement(getSession())))
		    {
		        // задача - для каждого ограничения сохранить последнюю связь
		        final Long relationId = (Long)row[0];
		        key2lastRelation.put(new MultiKey(Arrays.copyOfRange(row, 1, row.length), false), relationId);
		    }

		    // обновляем список связей (ставим дату утраты актуальности тем, которых нет в итоговом списке)
		    final DQLUpdateBuilder upd = new DQLUpdateBuilder(EppStudent2WorkPlan.class)
							.set(EppStudent2WorkPlan.P_REMOVAL_DATE, value(now, PropertyType.DATE))
							.where(isNull(property(EppStudent2WorkPlan.removalDate())))
							.where(eq(property(EppStudent2WorkPlan.studentEduPlanVersion().id()), value(sepvId)))
							.where(notIn(property("id"), key2lastRelation.values()));

		    final int update = executeAndClear(upd);
		    if (update > 0)
		    {
		        changed = true;

		        final String message = "student s2epv=`"+sepvId+"' fixed: "+update+" relations marked to be disabled";
		        Debug.message(message);
		        this.logger.info(message);
		    }
		}

		return changed;
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Поиск подходящего блока УПв для студента. Если у НПв студента указана направленность, ищется блок с той же направленностью.
     * Если направленность не указана, или блок с такой направленностью не найден, возвращается основной блок версии УП.
     *
     * @param studentId id студента
     * @param epvId id УПв
     * @return id блока УПв
     */
    private Long getEppEduPlanVersionBlockId(Long studentId, Long epvId)
    {
        final Long eduProgramSpecializationId = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .column(property("s", Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSpecialization().id()))
                .where(eq(property("s.id"), value(studentId)))
                .createStatement(getSession()).uniqueResult();

        Long blockId = null;
        if (eduProgramSpecializationId != null)
        {
            blockId = new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, "block")
                    .where(eq(property("block", EppEduPlanVersionSpecializationBlock.L_EDU_PLAN_VERSION), value(epvId)))
                    .where(eq(property("block", EppEduPlanVersionSpecializationBlock.L_PROGRAM_SPECIALIZATION), value(eduProgramSpecializationId)))
                    .column("block.id")
                    .createStatement(getSession()).uniqueResult();
        }

        if (blockId == null)
        {
            EppEduPlanVersionRootBlock rootBlock = get(EppEduPlanVersionRootBlock.class, EppEduPlanVersionRootBlock.L_EDU_PLAN_VERSION, epvId);
            if (rootBlock != null)
                blockId = rootBlock.getId();
        }

        return blockId;
    }

    protected boolean doUpdateStudent2EduPlanVersionRelations()
    {
        boolean changed = false;

        final Session session = getSession();

        NamedSyncInTransactionCheckLocker.register(session, EppStudentSlotDAO.class.getName() + ".doUpdateStudent2EduPlanVersionRelations");

        Debug.begin("EppStudentSlotDAO.doUpdateStudent2EduPlanVersionRelations");
        try {

            Debug.begin("EppStudentSlotDAO.doUpdateStudent2EduPlanVersionRelations.s2epv-fill-defaults-epv");
            try {
                // Создание связи студента с УПв, если у него нет такой связи,
                // а для НПП студента указана УПв в настройке "Версии УП для параметров обучения студентов (НПП) по умолчанию"

                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(Student.class, "s");
                dql.column("s.id");
                dql.column(property("eou2epv", EppEducationOrgUnitEduPlanVersion.eduPlanVersion().id()));
                dql.joinEntity("s", DQLJoinType.inner, EppEducationOrgUnitEduPlanVersion.class, "eou2epv",
                               eq(property("s", Student.educationOrgUnit()), property("eou2epv", EppEducationOrgUnitEduPlanVersion.educationOrgUnit())));
                dql.where(or(
                        eq(property("eou2epv", EppEducationOrgUnitEduPlanVersion.applyOnlyForFirstCourse()), value(false)),
                        eq(property("s", Student.course().intValue()), value(1))
                ));
                dql.where(notExists(EppStudent2EduPlanVersion.class, EppStudent2EduPlanVersion.L_STUDENT, property("s")));

                final List<Object[]> rows = dql.createStatement(session).list();
                if (!rows.isEmpty())
                {
                    for (List<Object[]> elements : Lists.partition(rows, DQL.MAX_VALUES_ROW_NUMBER)) {
                        final DQLInsertValuesBuilder insertDql = new DQLInsertValuesBuilder(EppStudent2EduPlanVersion.class);
                        for (Object[] row : elements)
                        {
                            Long studentId = (Long) row[0];
                            Long epvId = (Long) row[1];
                            insertDql.value(EppStudent2EduPlanVersion.L_STUDENT, studentId);
                            insertDql.value(EppStudent2EduPlanVersion.L_EDU_PLAN_VERSION, epvId);
                            insertDql.value(EppStudent2EduPlanVersion.L_BLOCK, getEppEduPlanVersionBlockId(studentId, epvId));
                            insertDql.addBatch();
                        }
                        insertDql.createStatement(session).execute();
                    }
                    changed = true;
                    session.clear();
                }
            } finally {
                Debug.end();
            }

            Debug.begin("EppStudentSlotDAO.doUpdateStudent2EduPlanVersionRelations.s2epv-set-removal-dates");
            try {
                // в случае, если у студента более одной актуальной связи, упорядочивает все актуальные связи по дате утверждения по убыванию,
                // затем по id по убыванию, и выставляет дату утраты актуальности всем, кроме первой

	            // В этой таблице есть другие актуальные связи для данного студента
	            DQLSelectBuilder anotherStudent2Epv = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "s2epv2")
			            .where(eq(property("s2epv", EppStudent2EduPlanVersion.L_STUDENT), property("s2epv2", EppStudent2EduPlanVersion.L_STUDENT)))
			            .where(ne("s2epv.id", "s2epv2.id"))
			            .where(isNull(property("s2epv2", EppStudent2EduPlanVersion.P_REMOVAL_DATE)));

                final DQLSelectBuilder dql = new DQLSelectBuilder()
						.fromEntity(EppStudent2EduPlanVersion.class, "s2epv")
		                .column("s2epv.id")
		                .column(property("s2epv", EppStudent2EduPlanVersion.student().id()))
		                .where(isNull(property("s2epv", EppStudent2EduPlanVersion.P_REMOVAL_DATE)))
		                .where(exists(anotherStudent2Epv.buildQuery()))
						// Сортируем так, чтобы строки без даты утверждения всегда оказывались ниже строк с датой
		                .order(DQLFunctions.coalesce(property("s2epv", EppStudent2EduPlanVersion.P_CONFIRM_DATE), valueDate(new Date(0L))), OrderDirection.desc)
						.order("s2epv.id", OrderDirection.desc);

                final List<Object[]> rows = dql.createStatement(session).list();
                if (!rows.isEmpty())
                {
                    final Set<Long> studentSet = new HashSet<>();
                    final List<Long> updateList = new ArrayList<>(rows.size());
                    for (Object[] row : rows)
                    {
                        // Собираем все вторые и последующие связи для каждого студента
                        if (!studentSet.add((Long) row[1]))
                            updateList.add((Long) row[0]);
                    }

                    final Date now = new Date();
                    for (List<Long> elements : Lists.partition(updateList, DQL.MAX_VALUES_ROW_NUMBER))
                    {
                        // Выставляем дату утраты актуальности
                        new DQLUpdateBuilder(EppStudent2EduPlanVersion.class)
                                .set(EppStudent2EduPlanVersion.P_REMOVAL_DATE, valueDate(now))
                                .where(in(property("id"), elements))
                                .createStatement(session).execute();
                    }
                    changed = true;
                }
            } finally {
                Debug.end();
            }

            /* Деактуализация убрана согласно DEV-6008
            Debug.begin("EppStudentSlotDAO.doUpdateStudent2EduPlanVersionRelations.s2epv-unique-constraint");
            try {
                // убиваем (деактуализируем) дублирование связей

                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EppStudent2EduPlanVersion.class, "s2epv");
                dql.where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))));
                dql.column(property(EppStudent2EduPlanVersionGen.student().id().fromAlias("s2epv")));
                dql.column(DQLFunctions.count(property(EppStudent2EduPlanVersionGen.id().fromAlias("s2epv"))));
                dql.group(property(EppStudent2EduPlanVersionGen.student().id().fromAlias("s2epv")));
                dql.having(gt(
                    DQLFunctions.count(property(EppStudent2EduPlanVersionGen.id().fromAlias("s2epv"))),
                    value(1, PropertyType.INTEGER)
                ));

                final List<Object[]> rows = dql.createStatement(session).list();
                if (rows.size() > 0)
                {
                    BatchUtils.execute(rows, 128, new BatchUtils.Action<Object[]>() {
                        @SuppressWarnings("unchecked")
                        @Override public void execute(final Collection<Object[]> elements) {
                            // идея следующая - загружаем все активные связи (загрузятся только самые последние - дубринование разрулится, причем так же как и в интерфейсе) по студентам (у которых более одной активной связи)
                            // и сохраняем их методом DAO - он сам оставит только актуальную связь - все остальные сделает неактуальными

                            final Collection studentIds = CollectionUtils.collect(elements, new Transformer() {
                                @Override public Object transform(final Object input) { return Array.get(input, 0); }
                            });
                            final Map<Long, Object> map = (Map)IEppEduPlanDAO.instance.get().getActiveStudentEduplanVersionRelationMap(studentIds);
                            for (final Map.Entry<Long, Object> e: map.entrySet()) {
                                e.setValue(((EppStudent2EduPlanVersion)e.getValue()).getEduPlanVersion().getId());
                            }
//                            IEppEduPlanDAO.instance.get().doUpdateStudentEduPlanVersion((Map)map);
                        }
                    });

                    // надо делать flush, иначе в последующих запросам может быть путаница (порядки удаления и добавления никак не синхранизированы между собой)
                    session.flush();
                    changed |= true;
                }

            } finally {
                Debug.end();
            }
            */

        } finally {
            Debug.end();
        }

        return changed;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public boolean doUpdateStudentRelations(final boolean forceRefreshWorkPlanRelations) {
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            final boolean epvChanged = doUpdateStudent2EduPlanVersionRelations();
            if (forceRefreshWorkPlanRelations || epvChanged)
            {
                final boolean wpChanged = doUpdateStudent2WorkPlanRelations();
                return (epvChanged || wpChanged);
            }
            return false;
        }
        finally
        {
            eventLock.release();
        }
    }

}
