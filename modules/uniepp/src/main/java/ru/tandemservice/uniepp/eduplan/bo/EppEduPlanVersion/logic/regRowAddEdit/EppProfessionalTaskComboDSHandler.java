/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.logic.regRowAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.catalog.EppProfessionalTask;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlockProfActivityType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * @author Igor Belanov
 * @since 27.03.2017
 */
public class EppProfessionalTaskComboDSHandler extends SimpleTitledComboDataSourceHandler
{
    public static final String PROP_EDU_PLAN_VERSION_BLOCK_ID = "eduPlanVersionBlockId";

    public EppProfessionalTaskComboDSHandler(String ownderId)
    {
        super(ownderId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String filter = input.getComboFilterByValue();
        Long eduPlanVersionBlockId = context.get(PROP_EDU_PLAN_VERSION_BLOCK_ID);
        // блок версии УП должен быть в любом случае
        assert eduPlanVersionBlockId != null;

        // подзапрос для вытаскивания общего блока версии УП
        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersionBlock.class, "epvb").column("epvb_r.id")
                .joinPath(DQLJoinType.inner, EppEduPlanVersionBlock.eduPlanVersion().fromAlias("epvb"), "epv")
                .joinEntity("epv", DQLJoinType.inner, EppEduPlanVersionBlock.class, "epvb1",
                        eq(property("epvb1", EppEduPlanVersionBlock.eduPlanVersion().id()), property("epv.id")))
                .joinEntity("epvb1", DQLJoinType.inner, EppEduPlanVersionRootBlock.class, "epvb_r",
                        eq(property("epvb1.id"), property("epvb_r.id")))
                .where(eq(property("epvb.id"), value(eduPlanVersionBlockId)));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionBlockProfActivityType.class, "epvb_pat").column("pt")
                .joinPath(DQLJoinType.inner, EppEduPlanVersionBlockProfActivityType.profActivityType().fromAlias("epvb_pat"), "pat")
                .joinEntity("pat", DQLJoinType.inner, EppProfessionalTask.class, "pt", eq(
                        property("pat", EppProfActivityType.id()),
                        property("pt", EppProfessionalTask.profActivityType().id())))
                .where(or(
                        // берём либо соответствующие конкретному блоку либо соответствующие общему блоку
                        eq(property("epvb_pat", EppEduPlanVersionBlockProfActivityType.eduPlanVersionBlock().id()), value(eduPlanVersionBlockId)),
                        in(property("epvb_pat", EppEduPlanVersionBlockProfActivityType.eduPlanVersionBlock().id()), subBuilder.buildQuery())));

        // фильтр по введённому названию
        if (!StringUtils.isEmpty(filter))
            builder.where(like(upper(property("pt", EppProfessionalTask.title())), value(CoreStringUtils.escapeLike(filter, true))));

        builder.order(property("pt", EppProfessionalTask.priority()));

        context.put(UIDefines.COMBO_OBJECT_LIST, createStatement(builder).list());
        return super.execute(input, context);
    }
}
