package ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvSimpleGroup;

import java.util.Collections;
import java.util.List;

import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.BaseAddEditDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow;

public class DAO extends BaseAddEditDao<EppEpvGroupReRow, Model> implements IDAO {

    @Override public void prepare(final Model model)
    {
        super.prepare(model);

        final List<HSelectOption> parentList = IEppEduPlanVersionDataDAO.instance.get().getEpvRowHierarchyListFromBlock(model.getBlock(), EppEpvGroupReRow.PARENT_PREDICATE);
        BaseAddEditDao.disablePossibleLoop(parentList, Collections.singleton(model.getRow().getId()));
        model.setParentList(parentList);
    }

    @Override public void save(final Model model) {
        this.doSaveRow(model);
    }
}
