package ru.tandemservice.uniepp.entity.registry;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.entity.registry.gen.*;

/** @see ru.tandemservice.uniepp.entity.registry.gen.EppEduGroupSplitBaseElementGen */
public abstract class EppEduGroupSplitBaseElement extends EppEduGroupSplitBaseElementGen implements ITitled
{
    @Override
    public String getTitle()
    {
        return getSplitElementShortTitle() + " (" + getSplitVariant().getShortTitle() + ")";
    }

    @Override
    @EntityDSLSupport()
    public abstract Long getSplitElementId();

    @Override
    @EntityDSLSupport()
    public abstract String getSplitElementShortTitle();

    @Override
    @EntityDSLSupport()
    public abstract String getSplitElementTitle();
}