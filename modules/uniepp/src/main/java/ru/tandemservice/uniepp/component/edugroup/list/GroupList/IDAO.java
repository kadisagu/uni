package ru.tandemservice.uniepp.component.edugroup.list.GroupList;

import org.apache.commons.collections15.Predicate;
import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;

import java.util.Collection;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model>
{
    void prepareDataSource(Model model);
    void doChangeLevel(Model model, Collection<Long> ids, Predicate<EppRealEduGroup> denyPredicate, String message,  String code);
}
