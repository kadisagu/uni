/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.logic;

import com.google.common.base.Strings;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/28/14
 */
public class EppEduPlanDao extends UniBaseDao implements IEppEduPlanDao
{
    @Override
    public void saveOrUpdateEduPlanProf(EppEduPlanProf plan)
    {
        plan.setSubjectQualification(this.getByNaturalId(new EduProgramSubjectQualification.NaturalId(plan.getProgramSubject(), plan.getProgramQualification())));
        saveOrUpdate(plan);
    }

	/**
	 * Сопоставить направленности ВПО из учебного плана (по названиям) направленностям из направления подготовки ВО.
	 * Направленности соответствуют друг другу, если их названия совпадают. Если для какой-то направленности из УП нет соответствия, значение будет null.
	 * @param oldSpecsOwner учебный план высшего образования, для всех направленностей которого нужно найти соответствие
	 * @param newSubject новое направление подготовки, которое должно содержать все необходимые направленности
	 * @return Отображение (название направленности из УП) -> (направленность нового направления подготовки).
	 * Ключи здесь - названия всех направленностей из УП, значения - соответствующие направленности из направления подготовки или null, если соответствие не найдено
	 */
	private Map<String, EduProgramSpecialization> oldSpecsNameToNewSubjectSpecs(EppEduPlanHigherProf oldSpecsOwner, EduProgramSubject newSubject)
	{
		final String blockAlias = "block";
		final String blockSpecAlias = "blockSpecialization";
		final String subjectSpecAlias = "subjectSpecialization";
		List<Object[]> rows = new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, blockAlias)
				.where(eq(property(blockAlias, EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan()), value(oldSpecsOwner)))
				.column(property(blockSpecAlias))
				.column(property(subjectSpecAlias))
				.joinPath(DQLJoinType.inner, EppEduPlanVersionSpecializationBlock.programSpecialization().fromAlias(blockAlias), blockSpecAlias)
				.joinEntity(blockAlias, DQLJoinType.left, EduProgramSpecialization.class, subjectSpecAlias, and(
									like(property(blockSpecAlias, EduProgramSpecialization.title()), property(subjectSpecAlias, EduProgramSpecialization.title())),
									eq(property(subjectSpecAlias, EduProgramSpecialization.programSubject()), value(newSubject))))
				.createStatement(getSession()).<Object[]>list();
		Map<String, EduProgramSpecialization> result = new HashMap<>(rows.size());
		for (Object[] row : rows)
			result.put(((EduProgramSpecialization)row[0]).getTitle(), (EduProgramSpecialization)row[1]);
		return result;
	}

	/**
	 * Обновить направленности ВПО для учебного плана - заменить имеющиеся (в его блоках направленности) на аналогичные из нового направления подготовки
	 * (новая направленность должна иметь то же название, что и старая). Если для каких-то направленностей аналогичных не найдено - выбросить исключение с сообщением.
	 * @param eduPlan - учебный план высшего образования, для которого нужно заменить направленности
	 * @param newSubject - новое направление подготовки
	 */
	private void updateSpecializations(EppEduPlanHigherProf eduPlan, EduProgramSubject newSubject)
	{
		Map<String, EduProgramSpecialization> replacementSpecsMap = oldSpecsNameToNewSubjectSpecs(eduPlan, newSubject);

		String missingSpecsString = replacementSpecsMap.entrySet().stream()
				.filter(entry -> entry.getValue() == null)
				.map(Map.Entry::getKey)
				.collect(Collectors.joining(", "));

		if (!Strings.isNullOrEmpty(missingSpecsString))
		{
			String errorMessage = "Для направления подготовки «" + newSubject.getTitleWithCode() + "» отсутствуют направленности: " + missingSpecsString;
			throw new ApplicationException(errorMessage);
		}

		List<EppEduPlanVersionSpecializationBlock> blocks = getList(EppEduPlanVersionSpecializationBlock.class, EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan(), eduPlan);
		blocks.forEach(block -> {
			final String specName = block.getProgramSpecialization().getTitle();
			block.setProgramSpecialization(replacementSpecsMap.get(specName));
			update(block);
		});
		getSession().flush();
	}

	@Override
	public void updateEduPlan(EppEduPlanProf eduPlan, EduProgramSubject newSubject, EduProgramQualification newQualification, EduProgramOrientation newOrientation)
	{
		// Корректировка данных должна быть доступна в любом состоянии учебного плана
		try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler()))
		{
			if (eduPlan instanceof EppEduPlanHigherProf)
			{
				EppEduPlanHigherProf higherProfEduPlan = (EppEduPlanHigherProf) eduPlan;
				higherProfEduPlan.setProgramOrientation(newOrientation);
				updateSpecializations(higherProfEduPlan, newSubject);
			}
			eduPlan.setProgramSubject(newSubject);
			eduPlan.setProgramQualification(newQualification);

			// Обновить УП с учетом согласованности связи квалификации и направления подготовки (констрейнт qualification_check)
			saveOrUpdateEduPlanProf(eduPlan);
			getSession().flush();
		}
	}
}