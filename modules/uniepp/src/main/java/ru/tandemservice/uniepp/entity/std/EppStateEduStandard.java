package ru.tandemservice.uniepp.entity.std;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;

import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniepp.dao.eduStd.IEppEduStdDAO;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.std.gen.EppStateEduStandardGen;

/**
 * Государственый образовательный стандарт
 */
public class EppStateEduStandard extends EppStateEduStandardGen implements ITitled, IEppStateObject, INumberObject
{
    @Override public INumberGenerationRule<EppStateEduStandard> getNumberGenerationRule() {
        return IEppEduStdDAO.instance.get().getNumberGenerationRule();
    }

    @EntityDSLSupport(parts = P_NUMBER)
    @Override
    public String getTitle()
    {
        if (getProgramSubject() == null) {
            return this.getClass().getSimpleName();
        }
        final StringBuilder sb = new StringBuilder()
        .append("№").append(this.getNumber())
        .append(" ").append(StringUtils.trimToEmpty(getProgramSubject().getSubjectCode()));

        final String shortTitle = StringUtils.trimToNull(getProgramSubject().getShortTitle());
        if (null != shortTitle) { sb.append(" ").append(shortTitle); }

        final Date confirmDate = this.getConfirmDate();
        if (null != confirmDate) { sb.append(" ").append(CoreDateUtils.getYear(confirmDate)); }

        final String titlePostfix = StringUtils.trimToNull(this.getTitlePostfix());
        if (null != titlePostfix) { sb.append(" ").append(titlePostfix); }

        return sb.toString();
    }

    public EppGeneration getGeneration() {
        if (getProgramSubject() == null) return null;
        return IEppEduStdDAO.instance.get().getGeneration(getProgramSubject());
    }

    @EntityDSLSupport
    public String getGenerationTitle() {
        return getGeneration() == null ? "" : getGeneration().getShortTitle();
    }

}