package ru.tandemservice.uniepp.component.base;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.uni.dao.UniBaseDao;

/**
 * @author vdanilov
 */
public class SimpleOrgUnitBasedListDAO<T extends IEntity, Model extends SimpleOrgUnitBasedListModel<T>> extends UniBaseDao implements IPrepareable<Model> {

    @Override
    public void prepare(final Model model) {
        final Long id = model.getOrgUnitId();
        model.setOrgUnit((null == id) ? null : this.get(OrgUnit.class, id));
    }

}
