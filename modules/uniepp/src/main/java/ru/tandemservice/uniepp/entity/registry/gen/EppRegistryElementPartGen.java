package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часть элемента реестра
 *
 * Часть дисциплины (семестр)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegistryElementPartGen extends EntityBase
 implements INaturalIdentifiable<EppRegistryElementPartGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart";
    public static final String ENTITY_NAME = "eppRegistryElementPart";
    public static final int VERSION_HASH = 122228482;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_ELEMENT = "registryElement";
    public static final String P_NUMBER = "number";
    public static final String P_LABOR = "labor";
    public static final String P_SIZE = "size";
    public static final String P_WEEKS = "weeks";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_TITLE = "title";
    public static final String P_TITLE_WITH_LABOR = "titleWithLabor";
    public static final String P_TITLE_WITH_NUMBER = "titleWithNumber";
    public static final String P_TITLE_WITHOUT_BRACES = "titleWithoutBraces";

    private EppRegistryElement _registryElement;     // Элемент реестра
    private int _number;     // Номер части
    private long _labor;     // Трудоемкость (в единицах трудоемкости)
    private long _size;     // Нагрузка (в часах)
    private long _weeks;     // Продолжительность (в неделях)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getRegistryElement()
    {
        return _registryElement;
    }

    /**
     * @param registryElement Элемент реестра. Свойство не может быть null.
     */
    public void setRegistryElement(EppRegistryElement registryElement)
    {
        dirty(_registryElement, registryElement);
        _registryElement = registryElement;
    }

    /**
     * Определяет последовательность чтения частей
     *
     * @return Номер части. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер части. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * Со смещением на два знака для хранения дробной части.
     *
     * @return Трудоемкость (в единицах трудоемкости). Свойство не может быть null.
     */
    @NotNull
    public long getLabor()
    {
        return _labor;
    }

    /**
     * @param labor Трудоемкость (в единицах трудоемкости). Свойство не может быть null.
     */
    public void setLabor(long labor)
    {
        dirty(_labor, labor);
        _labor = labor;
    }

    /**
     * Со смещением на два знака для хранения дробной части.
     *
     * @return Нагрузка (в часах). Свойство не может быть null.
     */
    @NotNull
    public long getSize()
    {
        return _size;
    }

    /**
     * @param size Нагрузка (в часах). Свойство не может быть null.
     */
    public void setSize(long size)
    {
        dirty(_size, size);
        _size = size;
    }

    /**
     * Со смещением на два знака для хранения дробной части. Заполняется только для практик.
     *
     * @return Продолжительность (в неделях). Свойство не может быть null.
     */
    @NotNull
    public long getWeeks()
    {
        return _weeks;
    }

    /**
     * @param weeks Продолжительность (в неделях). Свойство не может быть null.
     */
    public void setWeeks(long weeks)
    {
        dirty(_weeks, weeks);
        _weeks = weeks;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRegistryElementPartGen)
        {
            if (withNaturalIdProperties)
            {
                setRegistryElement(((EppRegistryElementPart)another).getRegistryElement());
                setNumber(((EppRegistryElementPart)another).getNumber());
            }
            setLabor(((EppRegistryElementPart)another).getLabor());
            setSize(((EppRegistryElementPart)another).getSize());
            setWeeks(((EppRegistryElementPart)another).getWeeks());
        }
    }

    public INaturalId<EppRegistryElementPartGen> getNaturalId()
    {
        return new NaturalId(getRegistryElement(), getNumber());
    }

    public static class NaturalId extends NaturalIdBase<EppRegistryElementPartGen>
    {
        private static final String PROXY_NAME = "EppRegistryElementPartNaturalProxy";

        private Long _registryElement;
        private int _number;

        public NaturalId()
        {}

        public NaturalId(EppRegistryElement registryElement, int number)
        {
            _registryElement = ((IEntity) registryElement).getId();
            _number = number;
        }

        public Long getRegistryElement()
        {
            return _registryElement;
        }

        public void setRegistryElement(Long registryElement)
        {
            _registryElement = registryElement;
        }

        public int getNumber()
        {
            return _number;
        }

        public void setNumber(int number)
        {
            _number = number;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppRegistryElementPartGen.NaturalId) ) return false;

            EppRegistryElementPartGen.NaturalId that = (NaturalId) o;

            if( !equals(getRegistryElement(), that.getRegistryElement()) ) return false;
            if( !equals(getNumber(), that.getNumber()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRegistryElement());
            result = hashCode(result, getNumber());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRegistryElement());
            sb.append("/");
            sb.append(getNumber());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegistryElementPartGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegistryElementPart.class;
        }

        public T newInstance()
        {
            return (T) new EppRegistryElementPart();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "registryElement":
                    return obj.getRegistryElement();
                case "number":
                    return obj.getNumber();
                case "labor":
                    return obj.getLabor();
                case "size":
                    return obj.getSize();
                case "weeks":
                    return obj.getWeeks();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "registryElement":
                    obj.setRegistryElement((EppRegistryElement) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "labor":
                    obj.setLabor((Long) value);
                    return;
                case "size":
                    obj.setSize((Long) value);
                    return;
                case "weeks":
                    obj.setWeeks((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "registryElement":
                        return true;
                case "number":
                        return true;
                case "labor":
                        return true;
                case "size":
                        return true;
                case "weeks":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "registryElement":
                    return true;
                case "number":
                    return true;
                case "labor":
                    return true;
                case "size":
                    return true;
                case "weeks":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "registryElement":
                    return EppRegistryElement.class;
                case "number":
                    return Integer.class;
                case "labor":
                    return Long.class;
                case "size":
                    return Long.class;
                case "weeks":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRegistryElementPart> _dslPath = new Path<EppRegistryElementPart>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegistryElementPart");
    }
            

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> registryElement()
    {
        return _dslPath.registryElement();
    }

    /**
     * Определяет последовательность чтения частей
     *
     * @return Номер части. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * Со смещением на два знака для хранения дробной части.
     *
     * @return Трудоемкость (в единицах трудоемкости). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getLabor()
     */
    public static PropertyPath<Long> labor()
    {
        return _dslPath.labor();
    }

    /**
     * Со смещением на два знака для хранения дробной части.
     *
     * @return Нагрузка (в часах). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getSize()
     */
    public static PropertyPath<Long> size()
    {
        return _dslPath.size();
    }

    /**
     * Со смещением на два знака для хранения дробной части. Заполняется только для практик.
     *
     * @return Продолжительность (в неделях). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getWeeks()
     */
    public static PropertyPath<Long> weeks()
    {
        return _dslPath.weeks();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getShortTitle()
     */
    public static SupportedPropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getTitleWithLabor()
     */
    public static SupportedPropertyPath<String> titleWithLabor()
    {
        return _dslPath.titleWithLabor();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getTitleWithNumber()
     */
    public static SupportedPropertyPath<String> titleWithNumber()
    {
        return _dslPath.titleWithNumber();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getTitleWithoutBraces()
     */
    public static SupportedPropertyPath<String> titleWithoutBraces()
    {
        return _dslPath.titleWithoutBraces();
    }

    public static class Path<E extends EppRegistryElementPart> extends EntityPath<E>
    {
        private EppRegistryElement.Path<EppRegistryElement> _registryElement;
        private PropertyPath<Integer> _number;
        private PropertyPath<Long> _labor;
        private PropertyPath<Long> _size;
        private PropertyPath<Long> _weeks;
        private SupportedPropertyPath<String> _shortTitle;
        private SupportedPropertyPath<String> _title;
        private SupportedPropertyPath<String> _titleWithLabor;
        private SupportedPropertyPath<String> _titleWithNumber;
        private SupportedPropertyPath<String> _titleWithoutBraces;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> registryElement()
        {
            if(_registryElement == null )
                _registryElement = new EppRegistryElement.Path<EppRegistryElement>(L_REGISTRY_ELEMENT, this);
            return _registryElement;
        }

    /**
     * Определяет последовательность чтения частей
     *
     * @return Номер части. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(EppRegistryElementPartGen.P_NUMBER, this);
            return _number;
        }

    /**
     * Со смещением на два знака для хранения дробной части.
     *
     * @return Трудоемкость (в единицах трудоемкости). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getLabor()
     */
        public PropertyPath<Long> labor()
        {
            if(_labor == null )
                _labor = new PropertyPath<Long>(EppRegistryElementPartGen.P_LABOR, this);
            return _labor;
        }

    /**
     * Со смещением на два знака для хранения дробной части.
     *
     * @return Нагрузка (в часах). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getSize()
     */
        public PropertyPath<Long> size()
        {
            if(_size == null )
                _size = new PropertyPath<Long>(EppRegistryElementPartGen.P_SIZE, this);
            return _size;
        }

    /**
     * Со смещением на два знака для хранения дробной части. Заполняется только для практик.
     *
     * @return Продолжительность (в неделях). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getWeeks()
     */
        public PropertyPath<Long> weeks()
        {
            if(_weeks == null )
                _weeks = new PropertyPath<Long>(EppRegistryElementPartGen.P_WEEKS, this);
            return _weeks;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getShortTitle()
     */
        public SupportedPropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new SupportedPropertyPath<String>(EppRegistryElementPartGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EppRegistryElementPartGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getTitleWithLabor()
     */
        public SupportedPropertyPath<String> titleWithLabor()
        {
            if(_titleWithLabor == null )
                _titleWithLabor = new SupportedPropertyPath<String>(EppRegistryElementPartGen.P_TITLE_WITH_LABOR, this);
            return _titleWithLabor;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getTitleWithNumber()
     */
        public SupportedPropertyPath<String> titleWithNumber()
        {
            if(_titleWithNumber == null )
                _titleWithNumber = new SupportedPropertyPath<String>(EppRegistryElementPartGen.P_TITLE_WITH_NUMBER, this);
            return _titleWithNumber;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart#getTitleWithoutBraces()
     */
        public SupportedPropertyPath<String> titleWithoutBraces()
        {
            if(_titleWithoutBraces == null )
                _titleWithoutBraces = new SupportedPropertyPath<String>(EppRegistryElementPartGen.P_TITLE_WITHOUT_BRACES, this);
            return _titleWithoutBraces;
        }

        public Class getEntityClass()
        {
            return EppRegistryElementPart.class;
        }

        public String getEntityName()
        {
            return "eppRegistryElementPart";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getShortTitle();

    public abstract String getTitle();

    public abstract String getTitleWithLabor();

    public abstract String getTitleWithNumber();

    public abstract String getTitleWithoutBraces();
}
