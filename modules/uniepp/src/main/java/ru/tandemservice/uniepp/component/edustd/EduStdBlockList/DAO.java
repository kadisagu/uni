package ru.tandemservice.uniepp.component.edustd.EduStdBlockList;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.core.view.list.source.IRowCustomizer;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.TreeColumn;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uniepp.dao.eduStd.EppEduStdDataDAO;
import ru.tandemservice.uniepp.dao.eduStd.IEppEduStdDataDAO;
import ru.tandemservice.uniepp.dao.eduStd.data.EppStdRowWrapper;
import ru.tandemservice.uniepp.dao.eduStd.data.IEppStdRowWrapper;
import ru.tandemservice.uniepp.dao.eduStd.data.IEppStdWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import ru.tandemservice.uniepp.entity.std.data.EppStdActionRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineBaseRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow;
import ru.tandemservice.uniepp.ui.ObligatoryMapFormatter;

import java.util.*;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setElement(this.getNotNull(EppStateEduStandard.class, model.getElement().getId()));

        final List<DevelopForm> formList = this.getCatalogItemList(DevelopForm.class);
        final LinkedHashMap<String, String> code2shortTitle = new LinkedHashMap<>(formList.size());
        for (final DevelopForm form : formList)
        {
            code2shortTitle.put(form.getCode(), form.getShortTitle());
        }
        model.setCode2shortTitle(code2shortTitle);

        this.prepareBlockList(model);
    }

    public void prepareBlockList(final Model model)
    {
        final MQBuilder builder = new MQBuilder(EppStateEduStandardBlock.ENTITY_CLASS, "block");
        builder.add(MQExpression.eq("block", EppStateEduStandardBlock.stateEduStandard().id().s(), model.getElement().getId()));
        final List<EppStateEduStandardBlock> list = builder.getResultList(this.getSession());

        Collections.sort(list, EppStateEduStandardBlock.COMPARATOR);

        final List<BlockWrapper> result = new ArrayList<>();
        for (final EppStateEduStandardBlock block : list)
        {
            result.add(new BlockWrapper(block));
        }

        this.prepareBlockListDataSource(model, result);
        model.setBlockList(result);
    }

    private void prepareBlockListDataSource(final Model model, final List<BlockWrapper> result)
    {
        final Long stdId = model.getElement().getId();
        final EppGeneration generation = model.getElement().getGeneration();
        // загрузка данных
        final IEppStdWrapper stdWrapper = IEppEduStdDataDAO.instance.get().getEduStdDataMap(Collections.singleton(stdId), true).get(stdId);
        final Map<Long, IEppStdRowWrapper> stdDataMap = stdWrapper.getRowMap();

        for (final BlockWrapper block : result)
        {
            final Long blockId = block.getId();
            final Collection<IEppStdRowWrapper> eduStdBlockRows = EppEduStdDataDAO.getEduStandardBlockRows(stdDataMap, object -> blockId.equals(object.getOwner().getId()));
            CollectionUtils.filter(eduStdBlockRows, object -> {
                // все, кроме мероприятий - они в отдельной таблице
                return !(object.getRow() instanceof EppStdActionRow);
            });
            block.setupRows(eduStdBlockRows);

            final IRowCustomizer<IEppStdRowWrapper> rowCustomizer = new SimpleRowCustomizer<IEppStdRowWrapper>()
            {
                @Override
                public String getRowStyle(final IEppStdRowWrapper row)
                {
                    final StringBuilder sb = new StringBuilder();
                    if (row.isStructureElement()) { sb.append("font-weight:bold;"); }
                    if (!block.getBlock().equals(row.getOwner())) { sb.append("color: gray;"); }
                    return sb.toString();
                }

                @Override
                public boolean isClickable(final AbstractColumn<?> column, final IEppStdRowWrapper row) {
                    return row instanceof EppStdDisciplineBaseRow;
                }
            };

            final AbstractListDataSource<IEppStdRowWrapper> dataSource = block.getDataSource();
            dataSource.setRowCustomizer(rowCustomizer);

            // ОСНОВНОЙ БЛОК И ФОРМЫ КОНТРОЛЯ
            this.prepareDataSourceMainBlock(model, dataSource, "Блоки, модули, дисциплины", generation, model.getCode2shortTitle());
        }
    }

    private void prepareDataSourceMainBlock(final Model model, final AbstractListDataSource<IEppStdRowWrapper> dataSource, final String title, final EppGeneration generation, final LinkedHashMap<String, String> code2shortTitle)
    {
        // НАЗВАНИЕ
        dataSource.addColumn(this.wrap(this.getColumn_number()).setWidth(1));

        // ОСНОВНОЙ БЛОК
        dataSource.addColumn(this.wrap(new SimpleColumn("Индекс", "index")).setWidth(1));
        dataSource.addColumn(new TreeColumn("Название", "title").setHeaderAlign("center").setOrderable(false).setRequired(true));
        dataSource.addColumn(this.wrap(new SimpleColumn("Всего часов", "totalSize")));
        if (generation.showTotalLabor())
        {
            dataSource.addColumn(this.wrap(new SimpleColumn("Всего трудоемкости", "totalLabor")));
        }
        if (generation.showCompetencies())
        {
            dataSource.addColumn(this.wrap(new SimpleColumn("Коды компетенций", "competenciesCodes")));
        }
        dataSource.addColumn(this.wrap(new SimpleColumn("Обязат.", "obligatoryMap").setFormatter(new ObligatoryMapFormatter(code2shortTitle))));

        final IEntityHandler usedInLoadDisabler = entity -> {
            final EppStdRowWrapper hierarhyParent = ((EppStdRowWrapper)entity).getHierarhyParent();
            return (null != hierarhyParent) && Boolean.FALSE.equals(hierarhyParent.getUsedInLoad());
        };
        dataSource.addColumn(this.wrap(new ToggleColumn("Учитывать в нагрузке", "usedInLoad")
        .setListener("onClickSwitchUsedInLoad")
        .setDisableHandler(usedInLoadDisabler)
        .setPermissionKey("switchUsedInLoad_eppEdustd")));

        final IEntityHandler usedInActionsDisabler = entity -> {
            final EppStdRowWrapper hierarhyParent = ((EppStdRowWrapper)entity).getHierarhyParent();
            return (null != hierarhyParent) && Boolean.FALSE.equals(hierarhyParent.getUsedInActions());
        };
        dataSource.addColumn(this.wrap(new ToggleColumn("Учитывать в контроле", "usedInActions")
        .setListener("onClickSwitchUsedInActions")
        .setDisableHandler(usedInActionsDisabler)
        .setPermissionKey("switchUsedInActions_eppEdustd")));
    }

    // номер
    private SimpleColumn getColumn_number()
    {
        return new SimpleColumn("№", "num")
        {
            private final Map<Long, Integer> index = new HashMap<>();

            @Override
            public String getContent(final IEntity entity)
            {
                final IEppStdRowWrapper row = (IEppStdRowWrapper)entity;
                if (row.isStructureElement())
                {
                    return ""; /* для структурных элементов ничего не показываем */
                }

                Integer idx = this.index.get(entity.getId());
                if (null == idx)
                {
                    this.index.put(entity.getId(), (idx = 1 + this.index.size()));
                }
                return idx.toString();
            }
        };
    }

    @SuppressWarnings("unchecked")
    private AbstractColumn wrap(final AbstractColumn column)
    {
        return column.setHeaderAlign("center").setOrderable(false).setClickable(false).setRequired(true);
    }

    @Override
    public void doSwitchUsedInLoad(final Long rowWrapperId)
    {
        final EppStdRow wrapper = this.get(EppStdRow.class, rowWrapperId);
        wrapper.setUsedInLoad(!wrapper.getUsedInLoad());
        this.update(wrapper);

    }

    @Override
    public void doSwitchUsedInActions(final Long rowWrapperId)
    {
        final EppStdRow wrapper = this.get(EppStdRow.class, rowWrapperId);
        wrapper.setUsedInActions(!wrapper.getUsedInActions());
        this.update(wrapper);
    }
}
