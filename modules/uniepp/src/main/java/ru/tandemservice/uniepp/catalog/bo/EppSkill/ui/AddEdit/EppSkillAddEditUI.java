/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppSkill.ui.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicItemAddEdit.CatalogDynamicItemAddEditUI;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.catalog.bo.EppSkill.EppSkillManager;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.catalog.EppSkill;

/**
 * @author Igor Belanov
 * @since 03.04.2017
 */
@State({
        @Bind(key = EppSkillAddEditUI.EDU_PROGRAM_SUBJECT_ID, binding = "programSubjectId")
})
public class EppSkillAddEditUI extends CatalogDynamicItemAddEditUI<EppSkill>
{
    // как и в случае списка, к нам может прийти конкретное направление
    public static final String EDU_PROGRAM_SUBJECT_ID = "programSubjectId";

    private Long _programSubjectId;

    // поля, которые не будут записаны в сущность
    private EduProgramKind _programKind;
    private EduProgramSubjectIndex _programSubjectIndex;

    private boolean _isEnableSpecialization;

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        // если это форма добавления или если не проставлена направленность (текст есть, а привязки нет)
        _isEnableSpecialization = isAddForm() ||
                (StringUtils.isNotEmpty(getCatalogItem().getProgramSpecialization()) && getCatalogItem().getProgramSpecializationVal() == null);

        EduProgramSubject programSubject = getCatalogItem().getProgramSubject();
        if (programSubject != null || _programSubjectId != null)
        {
            if (programSubject == null)
                programSubject = DataAccessServices.dao().getNotNull(_programSubjectId);
            getCatalogItem().setProgramSubject(programSubject);
            setProgramSubjectIndex(programSubject.getSubjectIndex());
            setProgramKind(programSubject.getSubjectIndex().getProgramKind());
        }
        onChangeProgramKind();

        if (isDisableTitleAndCode())
            getDisabledProperties().add(ICatalogItem.CATALOG_ITEM_TITLE);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppSkillAddEdit.PROGRAM_SUBJECT_INDEX_DS.equals(dataSource.getName()) ||
                EppSkillAddEdit.PROGRAM_SUBJECT_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppSkillManager.PROP_PROGRAM_KIND, _programKind);
            dataSource.put(EppSkillManager.PROP_PROGRAM_SUBJECT_INDEX, _programSubjectIndex);
        }
        else if (EppSkillAddEdit.PROF_ACTIVITY_TYPE_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppProfActivityType.PROP_PROGRAM_KIND, _programKind);
            dataSource.put(EppProfActivityType.PROP_PROGRAM_SUBJECT_INDEX, _programSubjectIndex);
            dataSource.put(EppProfActivityType.PROP_PROGRAM_SUBJECT, getCatalogItem().getProgramSubject());
            dataSource.put(EppProfActivityType.PROP_IN_DEPTH_STUDY, getCatalogItem().getInDepthStudy());
        }
        else if (EppSkillAddEdit.PROGRAM_SPECIALIZATION_DS.equals(dataSource.getName()))
        {
            dataSource.put(EduProgramSpecialization.PROP_PROGRAM_SUBJECT, getCatalogItem().getProgramSubject());
        }
    }

    public boolean isDisableTitleAndCode()
    {
        return !isAddForm() && getCatalogItem().isFromIMCA();
    }

    public boolean isDisableProgramSubjectFields()
    {
        // поля выключены либо если это форма редактирования
        // либо если мы работаем в пределах конкретного направления подготовки
        return !isAddForm() || (_programSubjectId != null) || getCatalogItem().isFromIMCA();
    }

    public boolean isDisableSpecializationField()
    {
        return !_isEnableSpecialization;
    }

    public boolean isDisableInDepthStudy()
    {
        // чекбокс выключен если это форма редактирования
        return !isAddForm() || getCatalogItem().isFromIMCA();
    }

    public boolean isVisibleInDepthStudy()
    {
        return getCatalogItem().getInDepthStudy() != null;
    }

    // listeners
    public void onChangeProgramKind()
    {
        // если вид ОП не установлен либо установлен не СПО, то чекбокс будет задизаблен и в базу запишется null
        if (_programKind == null || !_programKind.isProgramSecondaryProf())
        {
            getCatalogItem().setInDepthStudy(null);
        }
        // в противном случае (если мы установили вид ОП) этот чекбокс будет доступен, поэтому поставим дефолтный false
        else if (_programKind != null && getCatalogItem().getInDepthStudy() == null)
        {
            getCatalogItem().setInDepthStudy(false);
        }
    }

    @Override
    public void onClickApply()
    {
        // при сохранении текст направленности сохраняется автоматически
        if (getCatalogItem().getProgramSpecializationVal() != null)
            getCatalogItem().setProgramSpecialization(getCatalogItem().getProgramSpecializationVal().getDisplayableTitle());

        super.onClickApply();
    }

    // getters & setters
    public Long getProgramSubjectId()
    {
        return _programSubjectId;
    }

    public void setProgramSubjectId(Long programSubjectId)
    {
        _programSubjectId = programSubjectId;
    }

    public EduProgramKind getProgramKind()
    {
        return _programKind;
    }

    public void setProgramKind(EduProgramKind programKind)
    {
        _programKind = programKind;
    }

    public EduProgramSubjectIndex getProgramSubjectIndex()
    {
        return _programSubjectIndex;
    }

    public void setProgramSubjectIndex(EduProgramSubjectIndex programSubjectIndex)
    {
        _programSubjectIndex = programSubjectIndex;
    }
}
