package ru.tandemservice.uniepp.component.settings.WeekType2LoadTypeAddEdit;

import org.hibernate.Session;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.settings.EppELoadWeekType;
import ru.tandemservice.uniepp.entity.settings.gen.EppELoadWeekTypeGen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author nkokorina
 *
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setDevelopForm(this.getNotNull(DevelopForm.class, model.getDevelopFormId()));
        model.setDevelopFormSelectModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setWeekTypeSelectModel(new LazySimpleSelectModel<>(this.getCatalogItemListOrderByCode(EppWeekType.class)));

        final MQBuilder builder = new MQBuilder(EppELoadWeekType.ENTITY_NAME, "lwt");
        builder.add(MQExpression.eq("lwt", EppELoadWeekType.developForm().s(), model.getDevelopForm()));
        final List<EppELoadWeekType> relList = builder.getResultList(this.getSession());

        final ArrayList<EppWeekType> weekTypeAuditList = new ArrayList<>();
        final ArrayList<EppWeekType> weekTypeSelfWorkList = new ArrayList<>();

        for (final EppELoadWeekType rel : relList)
        {
            if (rel.getLoadType().isAuditTotal())
            {
                weekTypeAuditList.add(rel.getWeekType());
            }
            if (rel.getLoadType().isSelfWork())
            {
                weekTypeSelfWorkList.add(rel.getWeekType());
            }
        }

        model.setWeekTypeAuditList(weekTypeAuditList);
        model.setWeekTypeSelfWorkList(weekTypeSelfWorkList);
    }

    @Override
    public void update(final Model model)
    {
        final Session session = this.getSession();

        final DevelopForm developForm = model.getDevelopForm();

        final List<EppELoadType> eLoadTypeList = this.getCatalogItemList(EppELoadType.class);
        final Map<String, EppELoadType> code2ELoadType = new HashMap<>();
        for (final EppELoadType eload : eLoadTypeList)
        {
            code2ELoadType.put(eload.getCode(), eload);
        }

        final MQBuilder builder = new MQBuilder(EppELoadWeekType.ENTITY_NAME, "lwt");
        builder.add(MQExpression.eq("lwt", EppELoadWeekType.developForm().s(), developForm));
        final List<EppELoadWeekType> oldElementsList = builder.getResultList(session);

        final Map<INaturalId<EppELoadWeekTypeGen>, EppELoadWeekType> oldElementMap = new HashMap<>();
        for (final EppELoadWeekType element : oldElementsList)
        {
            oldElementMap.put(element.getNaturalId(), element);
        }

        this.doUpdateElement(session, developForm, code2ELoadType.get(EppELoadType.TYPE_TOTAL_AUDIT), oldElementMap, model.getWeekTypeAuditList());
        this.doUpdateElement(session, developForm, code2ELoadType.get(EppELoadType.TYPE_TOTAL_SELFWORK), oldElementMap, model.getWeekTypeSelfWorkList());

        for (final EppELoadWeekType element : oldElementMap.values())
        {
            session.delete(element);
        }
    }

    private void doUpdateElement(final Session session, final DevelopForm developForm, final EppELoadType type, final Map<INaturalId<EppELoadWeekTypeGen>, EppELoadWeekType> oldElementMap, final List<EppWeekType> weekTypeList)
    {
        for (final EppWeekType element : weekTypeList)
        {
            final EppELoadWeekType rel = new EppELoadWeekType();
            rel.setDevelopForm(developForm);
            rel.setLoadType(type);
            rel.setWeekType(element);

            EppELoadWeekType lastRel = oldElementMap.remove(rel.getNaturalId());
            if (null == lastRel)
            {
                lastRel = rel;
            }
            session.saveOrUpdate(lastRel);
        }
    }
}
