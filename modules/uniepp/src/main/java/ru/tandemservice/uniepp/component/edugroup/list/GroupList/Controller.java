package ru.tandemservice.uniepp.component.edugroup.list.GroupList;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.component.edugroup.EppEduGroupPublisherLinkResolver;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    private static final String COMPONENT_ATTACH_WORK_PLAN_ROW = ru.tandemservice.uniepp.component.edugroup.AttachWorkPlanRow.Model.class.getPackage().getName();
    private static final String COMPONENT_ATTACH_PPS = ru.tandemservice.uniepp.component.edugroup.AttachTutors.Model.class.getPackage().getName();

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (null == model.getDqlFactory()) { throw new IllegalStateException("no-dql-error"); }

        this.getDao().prepare(model);

        final DynamicListDataSource<EppRealEduGroup> dataSource = new DynamicListDataSource<EppRealEduGroup>(component, component1 -> Controller.this.getDao().prepareDataSource(Controller.this.getModel(component1))) {
            @Override public List<Long> getCountRowValueList() { return Arrays.asList(10L, 20L, 50L, 100L); }
        };

        final IPublisherLinkResolver eduGroupLinkResolver = new EppEduGroupPublisherLinkResolver() {
            @Override protected Long getOrgUnitId(IEntity entity) {
                return model.getOrgUnitId();
            }
            @Override public String getComponentName(final IEntity entity) {
                return model.getParameters().getGroupContentComponentName();
            }
        };


        dataSource.addColumn(new CheckboxColumn("select"));

        dataSource.addColumn(new PublisherLinkColumn("Название УГС", "title").setResolver(eduGroupLinkResolver).setFormatter(UniEppUtils.NOBR_COMMA_FORMATTER));

        dataSource.addColumn(new SimpleColumn("Курс", "course", UniEppUtils.NEW_LINE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Академическая\nгруппа", "group", UniEppUtils.NEW_LINE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки\n(специальность)", "educationOrgUnit", UniEppUtils.NEW_LINE_FORMATTER).setClickable(false));

        dataSource.addColumn(new PublisherLinkColumn("Дисциплина", EppRegistryElementPart.titleWithNumber(), EppRealEduGroup.activityPart()).setResolver(new SimplePublisherLinkResolver(EppRegistryElementPart.registryElement().id())));
        // OLD: dataSource.addColumn(new PublisherLinkColumn("Дисциплина", EppRegistryElement.titleWithNumber(), EppRealEduGroup.activityPart().registryElement()));
        dataSource.addColumn(new SimpleColumn("Читающее\nподразделение", EppRealEduGroup.activityPart().registryElement().owner().shortTitle()).setClickable(false));

        dataSource.addColumn(new PublisherLinkColumn("РУП", EppWorkPlanBase.shortTitle(), EppRealEduGroup.workPlanRow().workPlan()));
        dataSource.addColumn(new SimpleColumn("ППС", "tutor", UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Согласование", EppRealEduGroup.level().displayableTitle()).setClickable(false));

        // dataSource.addColumn(new SimpleColumn("Время\n создания", EppRealEduGroup.creationDate(), DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Время\n изменения", EppRealEduGroup.modificationDate(), DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false));


        final HeadColumn load = new HeadColumn("load", "Состав");
        // load.addColumn(new SimpleColumn("ПТ", "load_t", Controller.THREAD_FORMATTER).setClickable(false).setOrderable(false));
        load.addColumn(new SimpleColumn("ГР", "load_g").setClickable(false).setOrderable(false).setRequired(true));
        load.addColumn(new SimpleColumn("СТ", "load_s").setClickable(false).setOrderable(false).setRequired(true));

        dataSource.addColumn(load.setRequired(true));

        // dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));

        final IEntityHandler deleteDisabler = entity -> !Boolean.TRUE.equals(entity.getProperty("empty"));

        dataSource.addColumn(new ActionColumn("Редактировать данные УГС", ActionColumn.EDIT, "onClickEdit").setPermissionKey(model.getSec().getPermission("edit_eppEduGroup")));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить УГС № {0}?", EppRealEduGroup.P_TITLE).setDisableHandler(deleteDisabler).setPermissionKey(model.getSec().getPermission("delete_eppEduGroup")));

        model.setDataSource(dataSource);

    }


    public void onClickJoinGroups(final IBusinessComponent component)
    {
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try  {
            final Model model = this.getModel(component);
            final Collection<Long> ids = getSelectedIds(model);
            if (ids.isEmpty()) { throw new ApplicationException("Не выбрана ни одна группа."); }

            final Long joinId = IEppRealGroupRowDAO.instance.get().doJoin(ids, model.getLevel());
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.edugroup.GroupEdit.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, joinId).add("levelId", model.getLevel().getId())
            ));
        } finally {
            eventLock.release();
        }
    }

    public void onClickAttachWorkPlanRow(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final Collection<Long> ids = getSelectedIds(model);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            Controller.COMPONENT_ATTACH_WORK_PLAN_ROW,
            new ParametersMap().add("ids", ids).add("levelId", model.getLevel().getId())
        ));
    }

    public void onClickAttachPPS(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final Collection<Long> ids = getSelectedIds(model);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            Controller.COMPONENT_ATTACH_PPS,
            new ParametersMap().add("ids", ids).add("levelId", model.getLevel().getId())
        ));
    }

    protected Collection<Long> getSelectedIds(final Model model) {
        final CheckboxColumn checkboxColumn = (CheckboxColumn)model.getDataSource().getColumn("select");
        final Collection<Long> ids = UniBaseDao.ids(checkboxColumn.getSelectedObjects());
        if (ids.isEmpty()) { throw new ApplicationException("Не выбрана ни одна группа."); }
        checkboxColumn.getSelectedObjects().clear();
        return ids;
    }

    public void onClickSetFormingGroupCompleteLevel(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final boolean check_allPermissions = CoreServices.securityService().check(model.getOrgUnit(), ContextLocal.getUserContext().getPrincipalContext(), model.getSec().getPermission("sendForming_eppEduGroup"));
        final boolean check_onlyFromPrevious = CoreServices.securityService().check(model.getOrgUnit(), ContextLocal.getUserContext().getPrincipalContext(), model.getSec().getPermission("sendFormingFromPrevious_eppEduGroup"));

        final EppRealEduGroupCompleteLevel root = model.getLevel().getRoot();
        final String prevApproved = (Integer.parseInt(root.getCode()) - 1) + ".2";
        this.getDao().doChangeLevel(
            model,
            this.getSelectedIds(model),
            group -> {

                if (group.getLevel() == null) {
                    // если уровня не указано, то изменять его можно
                    return false;
                }

                if (EppRealEduGroupCompleteLevel.gt(group.getLevel(), model.getLevel())) {
                    // если уровень группы, у которой меняем этап, более высокий, чем текущий, ругаемся
                    return true;
                }

                if (check_allPermissions) {
                    // если правами разрешено отправлять на формирование любые с учетом проверок выше, то все ОК
                    return false;
                }

                if (check_onlyFromPrevious) {
                    // если разрешено отправлять на формирование только группы из текущего уровня и состояния
                    // "Утверждено" предыдущего уровня, то проверяем, в таком ли состоянии находится группа
                    final boolean currentLevel = group.getLevel().getRoot().equals(root);
                    final boolean previousLevel = group.getLevel().getCode().equals(prevApproved);
                    return !currentLevel &&!previousLevel;
                }

                // если нет прав вообще - ругаемся
                return true;
            },
            "Нельзя отправить на формирование выбранные УГС" + (
            check_allPermissions ? ", так как не все находятся на предыдущих этапах согласования." :
                check_onlyFromPrevious ?", так как не все находятся в состоянии «Утверждено» предыдущего этапа согласования." : "."
            ),
            root.getTakedCode()
        );
    }

    public void onClickConfirmGroupCompleteLevel(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final EppRealEduGroupCompleteLevel rootLevel = model.getLevel().getRoot();
        final String takedCode = rootLevel.getTakedCode();
        this.getDao().doChangeLevel(
            model,
            this.getSelectedIds(model),
            group -> (group.getLevel() != null) && !group.getLevel().getCode().equals(takedCode),
            "Нельзя утвердить выбранные УГС, так как не все находятся в состоянии «Формируется» текущего подразделения.",
            rootLevel.getApprovedCode()
        );

    }

    public void onClickSendToGroupCompleteLevel(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final EppRealEduGroupCompleteLevel rootLevel = model.getLevel().getRoot();
        this.getDao().doChangeLevel(
            model,
            this.getSelectedIds(model),
            group -> (group.getLevel() != null) && !group.getLevel().isChildOf(rootLevel),
            "Нельзя взять выбранные УГС, так как не все находятся на текущем этапе согласования.",
            component.getListenerParameter() + ".1");
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            ru.tandemservice.uniepp.component.edugroup.GroupEdit.Model.class.getPackage().getName(),
            new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.<Long> getListenerParameter()).add("levelId", model.getLevel().getId())
        ));
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final Long groupId = component.<Long> getListenerParameter();
        IEppRealGroupRowDAO.instance.get().checkGroupLockedEdit(Collections.singleton(groupId), model.getLevel());
        UniDaoFacade.getCoreDao().delete(groupId);
    }


}
