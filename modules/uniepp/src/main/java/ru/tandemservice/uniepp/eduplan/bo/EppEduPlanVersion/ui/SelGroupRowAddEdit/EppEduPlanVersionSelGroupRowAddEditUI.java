/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.SelGroupRowAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.BaseAddEditDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAddEditBase.EppEduPlanVersionRowAddEditBaseUI;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAddEditBase.EppEduPlanVersionTermDistrRowAddEditBaseUI;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.SelGroupFillForm.EppEduPlanVersionSelGroupFillForm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;

import java.util.Collections;
import java.util.List;

/**
 * @author oleyba
 * @since 11/12/14
 */
@Input({
    @Bind(key = EppEduPlanVersionRowAddEditBaseUI.BIND_ROW_ID, binding = "row.id"),
    @Bind(key = EppEduPlanVersionRowAddEditBaseUI.BIND_BLOCK_ID, binding = EppEduPlanVersionRowAddEditBaseUI.FIELD_BLOCK_ID),
    @Bind(key = "scrollPos", binding = "scrollPos"),
    @Bind(key = EppEduPlanVersionSelGroupRowAddEditUI.BIND_FILL_FROM_REGISTRY_ELEMENT_ID, binding = "regElementToFillLoadId")
})
@Return({
    @Bind(key = "scroll", binding = "scroll"),
    @Bind(key = "scrollPos", binding = "scrollPos"),
    @Bind(key = "highlightRowId", binding = "row.id")
})
public class EppEduPlanVersionSelGroupRowAddEditUI extends EppEduPlanVersionTermDistrRowAddEditBaseUI<EppEpvGroupImRow>
{
    public static final String BIND_FILL_FROM_REGISTRY_ELEMENT_ID = "fillGroupFromRegElementId";

    private EppEpvGroupImRow row = new EppEpvGroupImRow();
    private Long regElementToFillLoadId;

    @Override
    public void onComponentRefresh()
    {
        EppEpvGroupImRow hack_row = null;
        if (getRegElementToFillLoadId() != null)
        {
            hack_row = new EppEpvGroupImRow();
            hack_row.update(getRow(), false);
        }

        super.onComponentRefresh();

        final List<HSelectOption> parentList = IEppEduPlanVersionDataDAO.instance.get().getEpvRowHierarchyListFromBlock(getBlock(), EppEpvGroupImRow.PARENT_PREDICATE);
        BaseAddEditDao.disablePossibleLoop(parentList, Collections.singleton(getRow().getId()));
        setParentList(parentList);

        if (getRegElementToFillLoadId() != null)
        {
            getRow().update(hack_row);
            EppRegistryDiscipline discipline = IUniBaseDao.instance.get().getNotNull(EppRegistryDiscipline.class, getRegElementToFillLoadId());
            fillLoadFromRegistryElement(discipline, getRow().getSize());
            setRegElementToFillLoadId(null);
        }
    }

    @Override
    public boolean isCaFieldBoolean() {
        return getRow().getSize() <= 1 && getCaRow().getCa().isFlagValue();
    }

    // liesteners

    public void onClickFillLoadFromRegistryElement()
    {
        getActivationBuilder().asRegionDialog(EppEduPlanVersionSelGroupFillForm.class)
                .parameter(UIPresenter.PUBLISHER_ID, getRow().getId())
                .activate();
    }

    // getters and setters

    @Override
    public EppEpvGroupImRow getRow()
    {
        return row;
    }

    @Override
    public void setRow(EppEpvGroupImRow row)
    {
        this.row = row;
    }

    public Long getRegElementToFillLoadId()
    {
        return regElementToFillLoadId;
    }

    public void setRegElementToFillLoadId(Long regElementToFillLoadId)
    {
        this.regElementToFillLoadId = regElementToFillLoadId;
    }
}