package ru.tandemservice.uniepp.component.registry.RegElementEduPlanList;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

/**
 * @author vdanilov
 */
@Input( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id")
} )
public class Model {

    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    private final StaticListDataSource<ViewWrapper<IEntity>> dataSource = new StaticListDataSource<ViewWrapper<IEntity>>();
    public StaticListDataSource<ViewWrapper<IEntity>> getDataSource() { return this.dataSource; }


}
