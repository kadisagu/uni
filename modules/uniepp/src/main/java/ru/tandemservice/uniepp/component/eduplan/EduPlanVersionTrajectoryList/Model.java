/* $Id$ */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionTrajectoryList;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.list.source.SimpleListDataSource;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author oleyba
 * @since 5/5/11
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "versionId")
public class Model
{
    private Long versionId;
    private EppEduPlanVersion eduPlanVersion;

    private SimpleListDataSource<EppTrajectoryWrapper> dataSource = new SimpleListDataSource<EppTrajectoryWrapper>();

    public Long getVersionId()
    {
        return this.versionId;
    }

    public void setVersionId(final Long versionId)
    {
        this.versionId = versionId;
    }

    public SimpleListDataSource<EppTrajectoryWrapper> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final SimpleListDataSource<EppTrajectoryWrapper> dataSource)
    {
        this.dataSource = dataSource;
    }

    public EppEduPlanVersion getEduPlanVersion()
    {
        return this.eduPlanVersion;
    }

    public void setEduPlanVersion(final EppEduPlanVersion eduPlanVersion)
    {
        this.eduPlanVersion = eduPlanVersion;
    }
}
