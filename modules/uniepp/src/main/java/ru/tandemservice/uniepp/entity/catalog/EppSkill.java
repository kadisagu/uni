package ru.tandemservice.uniepp.entity.catalog;

import com.google.common.collect.ImmutableSet;
import org.apache.axis.utils.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.catalog.bo.EppSkill.ui.AddEdit.EppSkillAddEdit;
import ru.tandemservice.uniepp.entity.catalog.gen.EppSkillGen;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/** @see ru.tandemservice.uniepp.entity.catalog.gen.EppSkillGen */
public class EppSkill extends EppSkillGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EppSkill.class)
                .titleProperty(EppSkill.title().s())
                .filter(EppSkill.title())
                .customize((alias, dql, context, filter) -> {
                    dql.order(property(EppSkill.programSubject().subjectCode().fromAlias(alias)));
                    dql.order(property(EppSkill.programSubject().title().fromAlias(alias)));
                    dql.order(property(EppSkill.programSubject().id().fromAlias(alias)));
                    dql.order(property(EppSkill.skillGroup().priority().fromAlias(alias)));
                    dql.order(property(EppSkill.skillGroup().id().fromAlias(alias)));
                    dql.fetchPath(DQLJoinType.left, EppSkill.profActivityType().fromAlias(alias), "pat");
                    dql.order(caseExpr(isNull(property("pat", EppProfActivityType.priority())), value(0), value(1)));
                    dql.order(property(EppProfActivityType.priority().fromAlias("pat")));
                    dql.order(property(EppProfActivityType.id().fromAlias("pat")));
                    dql.order(property(EppSkill.priority().fromAlias(alias)));
                    dql.order(property(EppSkill.id().fromAlias(alias)));
                    return dql;
                });
    }

    /**
     * @return титл направленности
     */
    @Override
    @EntityDSLSupport
    public String getProgramSpecializationTitle()
    {
        return getProgramSpecializationVal() != null ? getProgramSpecializationVal().getTitle() : getProgramSpecialization();
    }

    public static IDynamicCatalogDesc getUiDesc()
    {
        return new BaseDynamicCatalogDesc()
        {
            @Override
            public String getAddEditComponentName()
            {
                return EppSkillAddEdit.class.getSimpleName();
            }

            @Override
            public String getAdditionalPropertiesAddEditPageName()
            {
                return EppSkillAddEdit.class.getPackage().getName() + ".AdditionalProps";
            }

            @Override
            public Collection<String> getHiddenFields()
            {
                // эти поля будут добавлены в интерфейс справочника руками (т.к. нужна их кастомизация)
                return ImmutableSet.of(L_PROGRAM_SUBJECT, L_SKILL_GROUP, L_PROF_ACTIVITY_TYPE, L_PROGRAM_SPECIALIZATION_VAL, P_PROGRAM_SPECIALIZATION, P_IN_DEPTH_STUDY, P_FROM_I_M_C_A, P_SKILL_CODE);
            }

            @Override
            public Collection<String> getHiddenFieldsAddEditForm()
            {
                return ImmutableSet.of(P_SKILL_CODE, P_PROGRAM_SPECIALIZATION);
            }

            @Override
            public Collection<String> getHiddenFieldsItemPub()
            {
                return ImmutableSet.of();
            }

            @Override
            public Set<Long> getEditDisabledItems(Set<Long> itemIds)
            {
                return new HashSet<>(IUniBaseDao.instance.get().<Long>getList(
                        new DQLSelectBuilder().fromEntity(EppSkill.class, "e")
                                .column(property("e.id"))
                                .where(in(property("e", EppSkill.id()), itemIds))
                                .where(or(
                                        eq(property("e", EppSkill.fromIMCA()), value(Boolean.TRUE)),
                                        and(
                                                isNotNull(property("e", EppSkill.programSpecialization())),
                                                isNull(property("e", EppSkill.programSpecializationVal()))))))); // =))))))
            }

            @Override
            public Set<Long> getDeleteDisabledItems(Set<Long> itemIds)
            {
                return new HashSet<>(IUniBaseDao.instance.get().<Long>getList(
                        new DQLSelectBuilder().fromEntity(EppSkill.class, "e")
                                .column(property("e.id"))
                                .where(in(property("e", EppSkill.id()), itemIds))
                                .where(eq(property("e", EppSkill.fromIMCA()), value(Boolean.TRUE)))
                ));
            }

            @Override
            public MetaDSLPath[] getPriorityGroupFields()
            {
                return new MetaDSLPath[]{EppSkill.programSubject().id(), EppSkill.skillGroup().id(), EppSkill.profActivityType().id()};
            }
        };
    }

    /**
     * [Название]
     *
     * @return (1) Формат вывода (default)
     */
    @Override
    public String getTitle()
    {
        // метод перекрыт только чтобы показать что такой формат вывода есть =)
        return super.getTitle();
    }

    /**
     * ([Код компетенции])
     *
     * @return (2) Краткий формат вывода
     */
    @Override
    @EntityDSLSupport
    public String getShortTitle()
    {
        return !StringUtils.isEmpty(getSkillCode()) ? getSkillCode() : "";
    }

    /**
     * ([Код компетенции] - )[Название]
     *
     * @return (3) Полный формат вывода
     */
    @Override
    @EntityDSLSupport
    public String getFullTitle()
    {
        StringBuilder sb = new StringBuilder();
        if (!StringUtils.isEmpty(getSkillCode())) sb.append(getSkillCode()).append(" - ");
        sb.append(getTitle());
        return sb.toString();
    }
}