package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings("unused")
public class MS_uniepp_2x8x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppRegistryModule

		//  свойство shared стало обязательным
		{
			// задать значение по умолчанию
			java.lang.Boolean defaultShared = false;
			tool.executeUpdate("update epp_reg_module_t set shared_p=? where shared_p is null", defaultShared);

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_reg_module_t", "shared_p", false);
		}
    }
}