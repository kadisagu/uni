package ru.tandemservice.uniepp.ui;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uniepp.dao.registry.EppRegistryDAO;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.or;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vdanilov
 */
@Wiki(url="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
public class EppRegistryElementPartSelectModel<T extends EppRegistryElement> extends DQLFullCheckSelectModel
{
    private static final String[] BASE_PROPERTIES = { EppRegistryElementPart.titleWithNumber().s(), EppRegistryElementPart.registryElement().owner().shortTitle().s() };

    public EppRegistryElementPartSelectModel(String ...labelProperties) {
        super(getLabelProperties(labelProperties));
    }

    private static String[] getLabelProperties(String[] labelProperties) {
        if (null == labelProperties || 0 == labelProperties.length) {
            return BASE_PROPERTIES;
        }

        List<String> properties = new ArrayList<>(Arrays.asList(BASE_PROPERTIES));
        properties.addAll(Arrays.asList(labelProperties));
        return properties.toArray(new String[properties.size()]);
    }

    @Override protected DQLSelectBuilder query(final String alias, final String filter) {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElementPart.class, alias);

        if (null != filter) {
            dql.where(this.getFilterCondition(alias, filter));
        }

        dql.order(property(EppRegistryElementPart.registryElement().title().fromAlias(alias)));
        dql.order(property(EppRegistryElementPart.registryElement().owner().title().fromAlias(alias)));
        dql.order(property(EppRegistryElementPart.number().fromAlias(alias)));
        return dql;
    }

    protected IDQLExpression getFilterCondition(final String alias, final String filter) {
        return or(
            this.like(EppRegistryElementPart.registryElement().title().fromAlias(alias), filter),
            this.like(EppRegistryElementPart.registryElement().shortTitle().fromAlias(alias), filter),
            this.like(EppRegistryElementPart.registryElement().fullTitle().fromAlias(alias), filter),
            this.like(EppRegistryElementPart.registryElement().number().fromAlias(alias), filter)
        );
    }

    @SuppressWarnings("unchecked")
    @Override protected List wrap(final List resultList) {
        return EppRegistryDAO.fillRegistryElementWrappers(ViewWrapper.getPatchedList(resultList));
    }

    @Override public String getLabelFor(final Object value, final int columnIndex) {
        final ViewWrapper wrapper = this.getWrapper(value);
        return super.getLabelFor(wrapper, columnIndex);
    }

    @Override public String getFullLabel(final Object value) {
        final ViewWrapper wrapper = this.getWrapper(value);
        final EppRegistryElementPart row = (EppRegistryElementPart)wrapper.getEntity();
        return row.getTitleWithNumber();
    }


}
