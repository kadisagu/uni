package ru.tandemservice.uniepp.component.edugroup;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;

/**
 * @author vdanilov
 */
public abstract class EppEduGroupPublisherLinkResolver implements IPublisherLinkResolver {

    @Override public Object getParameters(final IEntity entity) {
        return new ParametersMap()
        .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId())
        .add("orgUnitId", getOrgUnitId(entity));
    }

    protected abstract Long getOrgUnitId(IEntity entity);
}
