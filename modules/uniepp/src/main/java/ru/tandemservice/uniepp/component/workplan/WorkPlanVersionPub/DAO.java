package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionPub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

/**
 * 
 * @author nkokorina
 *
 */

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(final Model model) {
        model.setWorkPlanVersion(this.getNotNull(EppWorkPlanVersion.class, model.getWorkPlanVersion().getId()));
    }

}
