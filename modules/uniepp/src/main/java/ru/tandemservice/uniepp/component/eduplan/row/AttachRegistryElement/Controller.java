package ru.tandemservice.uniepp.component.eduplan.row.AttachRegistryElement;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author zhuj
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (null == model.getIds() || model.getIds().isEmpty()) {
            throw new IllegalStateException();
        }

        this.getDao().prepare(model);
    }

    public void onClickSelect(final IBusinessComponent component)
    {
        EppRegistryElement selected = this.getDao().doSelect(getModel(component));
        deactivate(component);
        ContextLocal.getInfoCollector().add("Указан элемент реестра «"+selected.getTitleWithNumberAndLoad()+"».");
    }

    public void onClickCreate(final IBusinessComponent component)
    {
        EppRegistryElement created = this.getDao().doCreate(getModel(component));
        deactivate(component);
        ContextLocal.getInfoCollector().add("Добавлен элемент реестра «"+created.getTitleWithNumberAndLoad()+"».");
    }

}
