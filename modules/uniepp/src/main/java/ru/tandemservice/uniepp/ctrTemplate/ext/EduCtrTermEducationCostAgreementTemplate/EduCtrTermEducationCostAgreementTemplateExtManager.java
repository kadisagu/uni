/* $Id$ */
package ru.tandemservice.uniepp.ctrTemplate.ext.EduCtrTermEducationCostAgreementTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.EduCtrTermEducationCostAgreementTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.logic.IEduCtrTermEducationCostAgreementTemplateDao;
import ru.tandemservice.uniepp.ctrTemplate.ext.EduCtrTermEducationCostAgreementTemplate.logic.EppEduCtrTermEducationCostAgreementTemplateDao;

/**
 * @author azhebko
 * @since 04.12.2014
 */
@Configuration
public class EduCtrTermEducationCostAgreementTemplateExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private EduCtrTermEducationCostAgreementTemplateManager _templateManager;

    @Bean
    @BeanOverride
    public IEduCtrTermEducationCostAgreementTemplateDao dao()
    {
        return new EppEduCtrTermEducationCostAgreementTemplateDao();
    }
}