/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.PracticeList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.RegistryBaseDSHandler;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractList;

/**
 * @author Irina Ugfeld
 * @since 10.03.2016
 */
@Configuration
public class EppRegistryPracticeList extends EppRegistryAbstractList {

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {
        return super.createBasePresenterExtPointBuilder()
                .addDataSource(searchListDS(ELEMENT_DS, getColumns(), getPracticeListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getColumns() {
        return getPracticeColumns().create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> getPracticeListDSHandler() {
        return new RegistryBaseDSHandler(this.getName());
    }

    @Bean
    public ButtonListExtPoint blockActionButtonListExtPoint(){
        return super.blockActionButtonListExtPoint();
    }

    @Override
    public String getMVCBasePackage() {
        return "ru.tandemservice.uniepp.component.registry.PracticeRegistry";
    }
}