package ru.tandemservice.uniepp.component.eduplan.row.TermDistribution;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    @Override
    public void prepare(final Model model)
    {
        IEntity rowE = DataAccessServices.dao().getNotNull(model.getRowHolder().getId());
        if (!(rowE instanceof EppEpvTermDistributedRow)) {
            throw new ApplicationException("Редактирование нагрузок по данной строке не поддерживается системой.");
        }

        final EppEpvTermDistributedRow row = model.getRowHolder().refresh(EppEpvTermDistributedRow.class);
        final EppEduPlanVersion eduPlanVersion = row.getOwner().getEduPlanVersion();
        eduPlanVersion.getState().check_editable(eduPlanVersion);


        final Map<EppEpvRowTerm, Map<String, Number>> termMap = model.getTermMap();
        termMap.clear();
        {
            final Map<Integer, EppEpvRowTerm> currentEpvRowTermMap = new HashMap<>();
            for (final EppEpvRowTerm rowTerm: this.getList(EppEpvRowTerm.class, EppEpvRowTerm.row().id(), model.getId())) {
                currentEpvRowTermMap.put(rowTerm.getTerm().getIntValue(), rowTerm);
            }

            final DevelopGrid grid = row.getOwner().getEduPlanVersion().getEduPlan().getDevelopGrid();
            final Map<Integer, DevelopGridTerm> gridMap = IDevelopGridDAO.instance.get().getDevelopGridMap(grid.getId());

            IEppEduPlanVersionDataDAO.IRowLoadWrapper rowLoadWrapper = IEppEduPlanVersionDataDAO.instance.get().getRowTermLoad(row.getId());

            final Map<Integer, Map<String, Double>> loadMap = rowLoadWrapper.rowTermLoadMap();
            final Map<Integer, Map<String, Integer>> actionMap = rowLoadWrapper.rowTermActionMap();

            for(final Entry<Integer, DevelopGridTerm> e: gridMap.entrySet()) {
                EppEpvRowTerm rowTerm = currentEpvRowTermMap.get(e.getKey());
                if (null == rowTerm) { rowTerm = new EppEpvRowTerm(row, e.getValue().getTerm()); }

                final Map<String, Number> map = new HashMap<>();
                termMap.put(rowTerm, map);

                final Map<String, Double> loads = loadMap.get(e.getKey());
                if (null != loads) { map.putAll(loads); }

                final Map<String, Integer> actions = actionMap.get(e.getKey());
                if (null != actions) { map.putAll(actions); }
            }
        }

        final Set<EppLoadType> loadTypes = new LinkedHashSet<>();
        loadTypes.add(this.getCatalogItem(EppELoadType.class, EppELoadType.TYPE_TOTAL_AUDIT));
        loadTypes.addAll(this.getCatalogItemListOrderByCode(EppALoadType.class));
        loadTypes.addAll(this.getCatalogItemListOrderByCode(EppELoadType.class));
        model.setLoadTypeList(new ArrayList<>(loadTypes));

        final List<EppControlActionType> controlActionTypes = IEppEduPlanVersionDataDAO.instance.get().getActiveControlActionTypes(row.getType())
                .sorted((o1, o2) -> {
                    final boolean m1 = o1.isFlagValue();
                    final boolean m2 = o2.isFlagValue();
                    return ((m1 == m2) ? 0 : (m1 ? 1 : -1));
                })
                .collect(Collectors.toList());

        model.setControlActionTypeList(controlActionTypes);
    }

    @Override
    public void move(final Model model, final Integer termNumber, final int direction)
    {
        final Map<EppEpvRowTerm, Map<String, Number>> termMap = model.getTermMap();

        final Map<Integer, Map.Entry<EppEpvRowTerm, Map<String, Number>>> map = new LinkedHashMap<>(termMap.size());
        for (final Map.Entry<EppEpvRowTerm, Map<String, Number>> e: model.getTermMap().entrySet()) {
            map.put(e.getKey().getTerm().getIntValue(), e);
        }

        final Entry<EppEpvRowTerm, Map<String, Number>> source = map.get(termNumber);
        if (null == source) { return; }

        final Entry<EppEpvRowTerm, Map<String, Number>> target = map.get(termNumber + direction);
        if (null == target) { return; }

        // переносим итоговые значения в семестре
        {
//            final EppEpvRowTerm tmp = new EppEpvRowTerm();
//            tmp.updateLoads(source.getKey());
//            source.getKey().updateLoads(target.getKey());
//            target.getKey().updateLoads(tmp);
        }

        // переносим значения по видам нагрузки
        {
            final Map<String, Number> tmp = source.getValue();
            source.setValue(target.getValue());
            target.setValue(tmp);
        }
    }

    @Override
    public void fill(final Model model)
    {
        if (!(model.getRow() instanceof EppEpvRegistryRow)) { return; }

        final EppRegistryElement regEl = ((EppEpvRegistryRow)model.getRow()).getRegistryElement();
        if (null == regEl) { return; }

        final IEppRegElWrapper regElWrap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(Collections.singleton(regEl.getId())).get(regEl.getId());
        if (null == regElWrap) { return; }

        for (final Map.Entry<EppEpvRowTerm, Map<String, Number>> e: model.getTermMap().entrySet()) {
            //final IEppRegElPartWrapper partWrap = regElWrap.getPartMap().get(e.getKey().getTerm().getIntValue());
            e.setValue(new HashMap<>());
//            if (null == partWrap) {
//                e.getKey().clearTotalTermLoad();
//            } else {
//                e.getKey().setupTotalTermLoad(partWrap.getItem());
//                final Map<String, Number> map = e.getValue();
//
//                // по аудитоной нагрузке
//                for (final EppLoadType loadType: model.getLoadTypeList()) {
//                    final String fullCode = loadType.getFullCode();
//
//                    // TODO: переводить в темпы, если надо
//                    map.put(fullCode, partWrap.getLoadAsDouble(fullCode));
//                }
//
//                // по формам контроля
//                for (final EppControlActionType actionType: model.getControlActionTypeList()) {
//                    final String fullCode = actionType.getFullCode();
//                    map.put(fullCode, partWrap.getActionSize(fullCode));
//                }
//           }
        }
    }


    @SuppressWarnings("unchecked")
    @Override
    public void save(final Model model)
    {
//        final Map<Integer, Map<String, Number>> map = new HashMap<>();
//        for (final Map.Entry<EppEpvRowTerm, Map<String, Number>> e: model.getTermMap().entrySet()) {
//            map.put(e.getKey().getTerm().getIntValue(), e.getValue());
//        }

        // обновляем данными по учебной нагрузке и формам контроля
//        IEppEduPlanVersionDataDAO.instance.get().doUpdateRowTermLoadMap((Map)Collections.singletonMap(model.getId(), map));
//        IEppEduPlanVersionDataDAO.instance.get().doUpdateRowTermActionMap((Map)Collections.singletonMap(model.getId(), map));

        // обновляем данные по трудоемкости и итоговой нагрузке
//        final Map<Integer, EppEpvRowTerm> dbMap = IEppEduPlanVersionDataDAO.instance.get().doUpdateTermRows(Collections.singletonMap(model.getId(), map.keySet()), false).get(model.getId());
//        for (final EppEpvRowTerm rowTerm: model.getTermMap().keySet()) {
//            if (rowTerm.hasTotalLoad()) {
//                final EppEpvRowTerm dbRowTerm = dbMap.get(rowTerm.getTerm().getIntValue());
//                dbRowTerm.updateLoads(rowTerm);
//                this.update(dbRowTerm);
//            }
//        }
    }
}
