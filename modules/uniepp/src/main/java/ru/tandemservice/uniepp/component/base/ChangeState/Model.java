package ru.tandemservice.uniepp.component.base.ChangeState;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.proxy.CGLIBLazyInitializer;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.base.bo.EppState.util.IEppStateConfig;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.sec.UnieppStateChangePermissionHolder;

import java.util.*;

/**
 * @author vdanilov
 */
@Input({@Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id")})
public class Model {

    private IEppStateObject stateObject;
    public IEppStateObject getStateObject() { return this.stateObject; }
    public void setStateObject(final IEppStateObject stateObject) { this.stateObject = stateObject; }

    public Long getId() {
        final IEppStateObject stateObject = this.getStateObject();
        return (null == stateObject ? null : stateObject.getId());
    }
    public void setId(final Long id) {
        final IEntity stateObject = (IEntity) (null == id ? null : CGLIBLazyInitializer.getProxy(id));
        this.setStateObject(stateObject instanceof IEppStateObject ? (IEppStateObject)stateObject : null);
    }

    private EppState transition;
    public EppState getTransition() { return this.transition; }
    public void setTransition(final EppState transition) { this.transition = transition; }

    public Collection<EppState> getPossibleTransitions() {
        if (null == this.getStateObject()) { return Collections.emptyList(); }

        final List<EppState> stateList = UniDaoFacade.getCoreDao().getList(EppState.class);
        final Map<String, EppState> stateMap = new HashMap<>(stateList.size());
        for (final EppState state: stateList) { stateMap.put(state.getCode(), state); }

        IEppStateConfig stateConfig = EppStateManager.instance().eppStateDao().getStateConfig(getStateObject().getClass());
        List<String> possibleTransitions = stateConfig.getPossibleTransitions(getStateObject());
        return CollectionUtils.collect(possibleTransitions, stateMap::get);
    }

    public String getTransitionTitle() {
        final EppState transition = this.getTransition();
        if (null == transition) { return ""; }

        final String title = StringUtils.trimToNull(transition.getTransitionTitle());
        if (null != title) { return title; }

        return ("Перевести в состояние «"+transition.getTitle()+"»");
    }

    public String getTransitionPermissionKey() {
        return UnieppStateChangePermissionHolder.getPermission(this.transition, this.stateObject);
    }

}
