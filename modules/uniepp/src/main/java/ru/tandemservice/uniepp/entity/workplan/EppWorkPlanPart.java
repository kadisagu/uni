package ru.tandemservice.uniepp.entity.workplan;

import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanPartGen;

/**
 * Часть РУП
 */
public class EppWorkPlanPart extends EppWorkPlanPartGen implements IEppWorkPlanPart
{
    public EppWorkPlanPart() {
    }

    public EppWorkPlanPart(final EppWorkPlan eppWorkPlan, final int number) {
        this();
        this.setNumber(number);
        this.setWorkPlan(eppWorkPlan);
    }

    public EppWorkPlanPart(final EppWorkPlan eppWorkPlan, final int number, final int totalWeeks) {
        this(eppWorkPlan, number);
        this.setTotalWeeks(totalWeeks);
    }

    @Override
    public boolean isEmpty() {
        return (this.getAuditWeeks() <= 0) && (this.getSelfworkWeeks() <= 0) && (this.getTotalWeeks() <= 0);
    }

    @Override
    public int getTotalWeeks() {
        return super.getTotalWeeks();
        // так нельзя делать // return Math.max(super.getTotalWeeks(), Math.max(getAuditWeeks(), getSelfworkWeeks()));
    }

    public int calculateTotalWeeks() {
        return Math.max(this.getTotalWeeks(), Math.max(this.getAuditWeeks(), this.getSelfworkWeeks()));
    }

    public EppWorkPlanPart updateSize(final IEppWorkPlanPart param) {
        this.setTotalWeeks(param.getTotalWeeks());
        this.setAuditWeeks(param.getAuditWeeks());
        this.setSelfworkWeeks(param.getSelfworkWeeks());
        return this;
    }


}