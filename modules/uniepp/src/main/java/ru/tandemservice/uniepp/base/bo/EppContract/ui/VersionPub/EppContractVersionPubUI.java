package ru.tandemservice.uniepp.base.bo.EppContract.ui.VersionPub;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.CtrContractVersionInlinePubUIPresenter;

import ru.tandemservice.uniepp.base.bo.EppContract.EppContractManager;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;

/**
 * @author vdanilov
 */
public class EppContractVersionPubUI extends CtrContractVersionInlinePubUIPresenter
{
    /**
     * удаление договора на обучение с карточки связи договора со студентом
     * (если у студента много связей - то удалить такой договор с карточки этой связи нельзя)
     */
    public void onDeleteEntity()
    {
        try {
            // удаляем договор со связью
            EppContractManager.instance().dao().doDeleteContract(
                (EppCtrEducationResult) EppContractVersionPubUI.this.getParent().getHolder().getValue()
            );
        } finally {
            // обновляем компонент (если ошибка - тем более)
            this.getSupport().setRefreshScheduled(true);
        }

        // деактивируем презентер
        // TODO: this.getRootPresenter().deactivate();
    }

    // удалять договор можно, если версия первая (она же последняя)
    public boolean isDeleteAllowed() {
        return this.getContext().isEditable() && (this.getContractVersion().equals(CtrContractVersionManager.instance().dao().getFirstVersion(this.getContract())));
    }

}