package ru.tandemservice.uniepp.entity.contract.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractResult;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.student.entity.IEduStudentContract;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Зачисление в контингент студентов
 *
 * Факт того, что есть студент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppCtrEducationResultGen extends CtrContractResult
 implements IEduContractRelation, IEduStudentContract{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult";
    public static final String ENTITY_NAME = "eppCtrEducationResult";
    public static final int VERSION_HASH = -580451866;
    private static IEntityMeta ENTITY_META;

    public static final String L_TARGET = "target";
    public static final String L_STUDENT = "student";
    public static final String L_CONTRACT_OBJECT = "contractObject";

    private EppStudent2EduPlanVersion _target;     // Связь студента с УП(в)
    private Student _student;     // Студент
    private CtrContractObject _contractObject;     // Договор на обучение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Связь студента с УП(в). Свойство не может быть null.
     */
    @NotNull
    public EppStudent2EduPlanVersion getTarget()
    {
        return _target;
    }

    /**
     * @param target Связь студента с УП(в). Свойство не может быть null.
     */
    public void setTarget(EppStudent2EduPlanVersion target)
    {
        dirty(_target, target);
        _target = target;
    }

    /**
     * @return Студент. Свойство не может быть null.
     *
     * Это формула "target.student".
     */
    // @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Договор на обучение. Свойство не может быть null.
     *
     * Это формула "contract".
     */
    // @NotNull
    public CtrContractObject getContractObject()
    {
        return _contractObject;
    }

    /**
     * @param contractObject Договор на обучение. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setContractObject(CtrContractObject contractObject)
    {
        dirty(_contractObject, contractObject);
        _contractObject = contractObject;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppCtrEducationResultGen)
        {
            setTarget(((EppCtrEducationResult)another).getTarget());
            setStudent(((EppCtrEducationResult)another).getStudent());
            setContractObject(((EppCtrEducationResult)another).getContractObject());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppCtrEducationResultGen> extends CtrContractResult.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppCtrEducationResult.class;
        }

        public T newInstance()
        {
            return (T) new EppCtrEducationResult();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "target":
                    return obj.getTarget();
                case "student":
                    return obj.getStudent();
                case "contractObject":
                    return obj.getContractObject();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "target":
                    obj.setTarget((EppStudent2EduPlanVersion) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "contractObject":
                    obj.setContractObject((CtrContractObject) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "target":
                        return true;
                case "student":
                        return true;
                case "contractObject":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "target":
                    return true;
                case "student":
                    return true;
                case "contractObject":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "target":
                    return EppStudent2EduPlanVersion.class;
                case "student":
                    return Student.class;
                case "contractObject":
                    return CtrContractObject.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppCtrEducationResult> _dslPath = new Path<EppCtrEducationResult>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppCtrEducationResult");
    }
            

    /**
     * @return Связь студента с УП(в). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult#getTarget()
     */
    public static EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion> target()
    {
        return _dslPath.target();
    }

    /**
     * @return Студент. Свойство не может быть null.
     *
     * Это формула "target.student".
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Договор на обучение. Свойство не может быть null.
     *
     * Это формула "contract".
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult#getContractObject()
     */
    public static CtrContractObject.Path<CtrContractObject> contractObject()
    {
        return _dslPath.contractObject();
    }

    public static class Path<E extends EppCtrEducationResult> extends CtrContractResult.Path<E>
    {
        private EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion> _target;
        private Student.Path<Student> _student;
        private CtrContractObject.Path<CtrContractObject> _contractObject;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Связь студента с УП(в). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult#getTarget()
     */
        public EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion> target()
        {
            if(_target == null )
                _target = new EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion>(L_TARGET, this);
            return _target;
        }

    /**
     * @return Студент. Свойство не может быть null.
     *
     * Это формула "target.student".
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Договор на обучение. Свойство не может быть null.
     *
     * Это формула "contract".
     * @see ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult#getContractObject()
     */
        public CtrContractObject.Path<CtrContractObject> contractObject()
        {
            if(_contractObject == null )
                _contractObject = new CtrContractObject.Path<CtrContractObject>(L_CONTRACT_OBJECT, this);
            return _contractObject;
        }

        public Class getEntityClass()
        {
            return EppCtrEducationResult.class;
        }

        public String getEntityName()
        {
            return "eppCtrEducationResult";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
