package ru.tandemservice.uniepp.component.group.GroupEduPlanTab;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{

}
