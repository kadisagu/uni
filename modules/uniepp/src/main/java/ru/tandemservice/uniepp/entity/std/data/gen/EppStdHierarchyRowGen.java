package ru.tandemservice.uniepp.entity.std.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.std.data.EppStdHierarchyRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись ГОС (иерархия элементов)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStdHierarchyRowGen extends EppStdRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.std.data.EppStdHierarchyRow";
    public static final String ENTITY_NAME = "eppStdHierarchyRow";
    public static final int VERSION_HASH = -724368285;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppStdHierarchyRowGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStdHierarchyRowGen> extends EppStdRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStdHierarchyRow.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppStdHierarchyRow is abstract");
        }
    }
    private static final Path<EppStdHierarchyRow> _dslPath = new Path<EppStdHierarchyRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStdHierarchyRow");
    }
            

    public static class Path<E extends EppStdHierarchyRow> extends EppStdRow.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EppStdHierarchyRow.class;
        }

        public String getEntityName()
        {
            return "eppStdHierarchyRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
