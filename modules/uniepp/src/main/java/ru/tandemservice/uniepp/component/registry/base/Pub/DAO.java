package ru.tandemservice.uniepp.component.registry.base.Pub;

import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.List;

/**
 * @author nkokorina
 */
public class DAO<T extends EppRegistryElement> extends UniBaseDao implements IDAO<T>
{
    @Override
    public void prepare(final Model<T> model)
    {
        model.getHolder().refresh(model.getElementClass());
        model.setupPermissionModel();
        setGroupTypeNames(model);
    }

    private void setGroupTypeNames(Model<T> model) {
        EppRegistryElement element =  model.getElement();
        if (model.getElement().getEduGroupSplitVariant() == null) {
            model.setGroupTypesNames(null);
            return;
        }

        if(element.isEduGroupSplitByType()) {
            List<EppGroupType> groupTypes = IEppRegistryDAO.instance.get().getRegElementSplitGroupTypes(element);
            model.setGroupTypesNames(CommonBaseStringUtil.join(groupTypes, EppGroupType.P_TITLE, ", "));
        } else {
            model.setGroupTypesNames("Нет");
        }
    }
}
