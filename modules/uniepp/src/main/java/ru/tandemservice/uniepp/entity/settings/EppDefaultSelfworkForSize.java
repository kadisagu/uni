package ru.tandemservice.uniepp.entity.settings;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.uni.entity.education.IDevelopCombination;
import ru.tandemservice.uni.util.DevelopUtil;
import ru.tandemservice.uniepp.entity.settings.gen.EppDefaultSelfworkForSizeGen;

/**
 * Доля самостоятельной работы (при формировании УП на основе ГОСа)
 */
public class EppDefaultSelfworkForSize extends EppDefaultSelfworkForSizeGen implements IDevelopCombination
{
    public static final String P_DOUBLE_VALUE = "doubleValue";
    public static final String P_TITLE = "doubleValue";

    @EntityDSLSupport
    @Override
    public String getTitle()
    {
        if (getDevelopForm() == null) {
            return this.getClass().getSimpleName();
        }
        final String combination = StringUtils.trimToNull(DevelopUtil.getTitle(this));
        if (null == combination) {
            return "Доля самостоятельной работы для произвольной комбинации";
        }
        return "Доля самостоятельной работы для комбинации - " + combination;
    }


    @EntityDSLSupport(parts = EppDefaultSizeForLabor.P_VALUE)
    @Override
    public Double getDoubleValue()
    {
        final Long area = this.getValue();
        return (area != null) ? 0.01d * area : null;
    }

    @EntityDSLSupport(parts = EppDefaultSizeForLabor.P_VALUE)
    public void setDoubleValue(final Double area)
    {
        this.setValue((area != null) ? Math.round(area * 100.0d) : null);
    }
}