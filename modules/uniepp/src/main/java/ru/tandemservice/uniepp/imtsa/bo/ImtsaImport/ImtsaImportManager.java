/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.logic.IImtsaImportDao;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.logic.ImtsaImportDao;

/**
 * @author azhebko
 * @since 27.10.2014
 */
@Configuration
public class ImtsaImportManager extends BusinessObjectManager
{
    public static ImtsaImportManager instance() { return instance(ImtsaImportManager.class); }

    @Bean
    public IImtsaImportDao dao() { return new ImtsaImportDao(); }
}