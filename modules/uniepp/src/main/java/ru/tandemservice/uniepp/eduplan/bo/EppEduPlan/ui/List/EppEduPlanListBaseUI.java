/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;

/**
 * @author oleyba
 * @since 9/12/14
 */
public class EppEduPlanListBaseUI extends UIPresenter
{
    private EntityHolder<EduProgramKind> programKindHolder = new EntityHolder<>();
    private EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<>();
    private CommonPostfixPermissionModelBase sec;

    @Override
    public void onComponentRefresh()
    {
        getProgramKindHolder().refresh();
        getOrgUnitHolder().refresh();
        setSec(getSecModel(getOrgUnit()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        getSettings().set("programKind", getProgramKindHolder().getValue());
        dataSource.put("settings", getSettings());
        dataSource.put("programKind", getProgramKindHolder().getValue());
        dataSource.put("orgUnit", getOrgUnitHolder().getValue());
    }

    public static CommonPostfixPermissionModelBase getSecModel(final OrgUnit orgUnit)
    {
        String postfix = orgUnit == null ? null : OrgUnitSecModel.getPostfix(orgUnit);
        return new CommonPostfixPermissionModel(postfix).addException("view", new CommonPostfixPermissionModel.IResolver() {
            @Override public String getPermissionKey(final String propertyName) {
                if (orgUnit != null) { return null; }
                return "menuEppEduPlanList";
            }
        });
    }

    public OrgUnit getOrgUnit() {
        return getOrgUnitHolder().getValue();
    }

    public void onClickSearch()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        saveSettings();
    }

    public void onClickDeleteEduPlan() {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    // getters and setters

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return orgUnitHolder;
    }

    public void setOrgUnitHolder(EntityHolder<OrgUnit> orgUnitHolder)
    {
        this.orgUnitHolder = orgUnitHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return sec;
    }

    public void setSec(CommonPostfixPermissionModelBase sec)
    {
        this.sec = sec;
    }

    public EntityHolder<EduProgramKind> getProgramKindHolder()
    {
        return programKindHolder;
    }

    public void setProgramKindHolder(EntityHolder<EduProgramKind> programKindHolder)
    {
        this.programKindHolder = programKindHolder;
    }
}
