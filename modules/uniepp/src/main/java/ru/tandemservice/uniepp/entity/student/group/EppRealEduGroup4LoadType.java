package ru.tandemservice.uniepp.entity.student.group;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroup4LoadTypeGen;

/**
 * Фактическая учебная группа (по виду аудиторной нагрузки)
 *
 * Объединяет группу студентов по виду нагрузки в рамках дисциплины реестра
 */
public class EppRealEduGroup4LoadType extends EppRealEduGroup4LoadTypeGen
{
    public EppRealEduGroup4LoadType() {}

    public EppRealEduGroup4LoadType(final EppRealEduGroupSummary summary, final EppRegistryElementPart activityPart, final EppGroupType groupType) {
        this.setSummary(summary);
        this.setActivityPart(activityPart);
        this.setType(groupType);
    }

    @Override
    public EppGroupTypeALT getType()
    {
        return (EppGroupTypeALT) super.getType();
    }

    /**
     * Внимание!
     * Метод выполняет запрос к базе. Использовать осознанно и аккуратно.
     */
    public EppALoadType getLoadType()
    {
        return IUniBaseDao.instance.get().getNotNull(EppALoadType.class, EppALoadType.eppGroupType(), this.getType());
    }

    public OrgUnit getTutorOu() {
        return getActivityPart().getTutorOu();
    }

    @Override
    @EntityDSLSupport
    public String getActivityTitle() {
        return EppRealEduGroupTitleUtil.getActivityTitle(this);
    }

}