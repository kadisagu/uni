// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.sec;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Гененирует ключи для кнопок перевода объектов по состояниям для компонента ru.tandemservice.uniepp.component.base.ChangeState
 *
 * Краткая инструкция по использованию:
 * 1. Зарегистрировать class-group с классом, к которому принадлежит ваш stateObject, в sec.xml или другом PermissionModifier'е
 * 2. Зарегистрировать permission-group с name = "someKeyPG", связанную с class-group из п1, аналогично п1
 * 3. Внести этот класс в classPermissionSuffixMap со значением <someKey, название permission-group из п.2>
 * И ключи для кнопок будут зарегистрированы.
 *
 * Можно также зарегистрировать группу прав, в которую добавятся массовые действия над состояниями.
 * Например, в списке рабочих планов нужны массовые переводы во все состояния.
 * Как делать - см. код.
 *
 * @author oleyba
 * @since 30.11.2010
 */
public class UnieppStateChangePermissionModifier implements ISecurityConfigMetaMapModifier, UnieppStateChangePermissionHolder.IUnieppStateChangePermissionResolver
{
    private static final Map<Class, Pair<String, String>> classPermissionMap = ImmutableMap.<Class, Pair<String, String>>builder()
    .put(EppEduPlan.class, new Pair<>("eppEduPlan", "Объект «Учебный план»"))
    .put(EppCustomEduPlan.class, new Pair<>("eppCustomEduPlan", "Объект «Индивидуальный учебный план»"))
    .put(EppEduPlanVersion.class, new Pair<>("eppEduPlanVersion", "Объект «Версия учебного плана»"))
    .put(EppWorkPlan.class, new Pair<>("eppWorkPlan", "Объект «Рабочий учебный план»"))
    .put(EppWorkPlanVersion.class, new Pair<>("eppWorkPlanVersion", "Объект «Версия рабочего учебного плана»"))
    .put(EppWorkGraph.class, new Pair<>("eppWorkGraph", "Объект «График учебного процесса»"))
    .put(EppRegistryDiscipline.class, new Pair<>("eppDiscipline", "Объект «Дисциплина»"))
    .put(EppRegistryPractice.class, new Pair<>("eppPractice", "Объект «Практика»"))
    .put(EppRegistryModule.class, new Pair<>("eppModule", "Объект «Учебный модуль»"))
    .put(EppRegistryAttestation.class, new Pair<>("eppAttestation", "Объект «Мероприятие ГИА»"))
    .put(EppStateEduStandard.class, new Pair<>("eppEdustd", "Объект «ГОС»"))
    .build();

    public static final String PG_WORK_PLAN_LIST = "eppWorkPlanLPG";
    public static final String POSTFIX_WORK_PLAN_LIST = "eppWorkPlan_list";

    public static class MassChangeStatePermConfig
    {
        private final String globalPermissionGroupName;
        private final String globalPermissionGroupTitle;
        private final String permissionKeyPostfix;

        public MassChangeStatePermConfig(String globalPermissionGroupName, String globalPermissionGroupTitle, String permissionKeyPostfix)
        {
            this.globalPermissionGroupName = globalPermissionGroupName;
            this.globalPermissionGroupTitle = globalPermissionGroupTitle;
            this.permissionKeyPostfix = permissionKeyPostfix;
        }

        public String getGlobalPermissionGroupName()
        {
            return globalPermissionGroupName;
        }

        public String getGlobalPermissionGroupTitle()
        {
            return globalPermissionGroupTitle;
        }

        public String getPermissionKeyPostfix()
        {
            return permissionKeyPostfix;
        }
    }

    /**
     * Группы прав для массовых переводов между состояниями
     * module -> [global permission group name, global permission group title, permission key postfix]
     */
    private static final Map<String, Collection<MassChangeStatePermConfig>> massGroupPermissionMap = ImmutableMap.<String, Collection<MassChangeStatePermConfig>>of(
            "uniepp", ImmutableList.of(new MassChangeStatePermConfig(PG_WORK_PLAN_LIST, "Реестр рабочих учебных планов", POSTFIX_WORK_PLAN_LIST))
    );

    private String getPermissionStr(String suffix1, String suffix2)
    {
        return "changeStateTo_" + suffix1 + "_" + suffix2;
    }

    public static String getMassPermissionTitle(String stateTitle)
    {
        return "Массовый перевод в состояние «" + stateTitle + "»";
    }

    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName(this.getModuleName());
        config.setName(this.getConfigName());
        config.setTitle("");

        final List<EppState> stateList = UniDaoFacade.getCoreDao().getList(EppState.class);

        for (final Pair<String, String> entry : this.getClassPermissionMap().values())
        {
            final String pgTitle = entry.getY();
            if (pgTitle == null) {
                continue;
            }
            final String classPermissionSuffix = entry.getX();
            final PermissionGroupMeta pg = PermissionMetaUtil.createPermissionGroup(config, classPermissionSuffix + "PG", pgTitle);
            for (final EppState state : stateList) {
                PermissionMetaUtil.createPermission(pg, getPermissionStr(state.getPermissionSuffix(), classPermissionSuffix), "Перевод в состояние «" + state.getTitle() + "»");
            }
        }

        final Collection<MassChangeStatePermConfig> massGroupPermissions = this.getMassGroupPermissionMap().get(this.getModuleName());
        if (massGroupPermissions != null)
        {
            for (final MassChangeStatePermConfig entry : massGroupPermissions)
            {
                final PermissionGroupMeta pg = PermissionMetaUtil.createPermissionGroup(config, entry.getGlobalPermissionGroupName(), entry.getGlobalPermissionGroupTitle());
                for (final EppState state : stateList)
                {
                    PermissionMetaUtil.createPermission(pg, getPermissionStr(state.getPermissionSuffix(), entry.getPermissionKeyPostfix()), getMassPermissionTitle(state.getTitle()));
                }
            }
        }

        securityConfigMetaMap.put(config.getName(), config);
    }

    protected String getConfigName() {return "uniepp-stateChange-config";}

    protected String getModuleName() {return "uniepp";}

    @Override
    public String getPermission(final EppState transition, final IEppStateObject stateObject)
    {
        Class<? extends IEppStateObject> stateObjectClass = stateObject.getClass();
        if (EppEduPlan.class.isAssignableFrom(stateObjectClass)) {
            stateObjectClass = EppEduPlan.class;
        }
        final Pair<String, String> classPermissionEntry = this.getClassPermissionMap().get(stateObjectClass);
        // Если где-то используется компонент изменения состояний, который вызывает этот метод для получения permissionKey,
        // то класс объекта, у которого таким образом меняется состояние, нужно зарегистрировать для генерации ключей, см. инструкцию в комментарии выше
        // если класс не зарегистрирован здесь, будем надеяться, что его зарегистрировали в других бинах листа
        if (null == classPermissionEntry) {
            return null;
        }
        final String classPermissionSuffix = classPermissionEntry.getX();
        return getPermissionStr(transition.getPermissionSuffix(), classPermissionSuffix);
    }

    @Override
    public String getMassPermission(EppState transition, String permissionPostfix)
    {
        if (!getMassGroupPermissionMap().containsKey(this.getModuleName())) {
            return null;
        }
        return getPermissionStr(transition.getPermissionSuffix(), permissionPostfix);
    }

    protected Map<Class, Pair<String, String>> getClassPermissionMap()
    {
        return UnieppStateChangePermissionModifier.classPermissionMap;
    }

    protected Map<String, Collection<MassChangeStatePermConfig>> getMassGroupPermissionMap()
    {
        return UnieppStateChangePermissionModifier.massGroupPermissionMap;
    }
}
