package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionDiffTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;


/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
    }
}
