package ru.tandemservice.uniepp.entity.registry;

import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryModuleIControlActionGen;

/**
 * Учебный модуль: текущий контроль
 *
 * Текущий контроль по модулю
 */
public class EppRegistryModuleIControlAction extends EppRegistryModuleIControlActionGen
{

    public EppRegistryModuleIControlAction() {}
    public EppRegistryModuleIControlAction(final EppRegistryModule module, final EppIControlActionType controlAction) {
        this.setModule(module);
        this.setControlAction(controlAction);
    }

    public EppRegistryModuleIControlAction(EppRegistryModule module, EppIControlActionType controlAction, int amount) {
        this(module, controlAction);
        this.setAmount(amount);
    }
}