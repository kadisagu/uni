/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppSkill.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.catalog.bo.EppSkill.EppSkillManager;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;

/**
 * @author Igor Belanov
 * @since 03.04.2017
 */
@Configuration
public class EppSkillAddEdit extends BusinessComponentManager
{
    public static final String PROGRAM_KIND_DS = "programKindDS";
    public static final String PROGRAM_SUBJECT_INDEX_DS = "programSubjectIndexDS";
    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
    public static final String PROGRAM_SPECIALIZATION_DS = "programSpecializationDS";
    public static final String PROF_ACTIVITY_TYPE_DS = "profActivityTypeDS";
    public static final String SKILL_GROUP_DS = "skillGroupDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(PROGRAM_KIND_DS, EppSkillManager.instance().programKindDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_INDEX_DS, EppSkillManager.instance().programSubjectIndexDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_DS, EppSkillManager.instance().programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(PROGRAM_SPECIALIZATION_DS, getName(), EduProgramSpecialization.defaultSelectDSHandler(getName())))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(PROF_ACTIVITY_TYPE_DS, getName(), EppProfActivityType.defaultSelectDSHandler(getName())))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(SKILL_GROUP_DS, getName(), EppSkillGroup.defaultSelectDSHandler(getName())))
                .create();
    }
}
