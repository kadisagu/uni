package ru.tandemservice.uniepp.migration;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.catalog.CTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.shared.commonbase.utils.MergeBuilder;
import org.tandemframework.shared.commonbase.utils.MergeBuilder.*;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.shared.commonbase.utils.MergeBuilder.*;

/**
 * Руками написанная миграция
 *
 * @author Nikolay Fedorovskih
 */
@SuppressWarnings({"unused", "SpellCheckingInspection"})
public class MS_uniepp_2x9x1_6to7 extends IndependentMigrationScript {

    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.1")
        };
    }

    @Override
    public void run(final DBTool tool) throws Exception {

        // сущность eppStudentWorkPlanElement - Мероприятие студента из РУП (МСРП)
        final CTable wpe_t = tool.table("epp_student_wpe_t");
        {
            // В МСРП поле "Семестр сетки" заменяем на три поля: курс, семестр и часть года.
            // Они войдут в состав натурального идентификатора.

            wpe_t.createColumn(new DBColumn("course_id", DBType.LONG));
            wpe_t.createColumn(new DBColumn("term_id", DBType.LONG));
            wpe_t.createColumn(new DBColumn("part_id", DBType.LONG));

            SQLUpdateQuery upd = new SQLUpdateQuery(wpe_t.name(), "wpe");
            upd.set("course_id", "gt.course_id");
            upd.set("term_id", "gt.term_id");
            upd.set("part_id", "gt.part_id");
            upd.getUpdatedTableFrom().innerJoin(SQLFrom.table("developgridterm_t", "gt"), "wpe.gridterm_id=gt.id");

            final int ret = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(upd));
            if (Debug.isEnabled()) {
                System.out.println(this.getClass().getSimpleName() + ": " + ret + " items migrated in " + wpe_t.name());
            }

            wpe_t.column("course_id").changeNullable(false);
            wpe_t.column("term_id").changeNullable(false);
            wpe_t.column("part_id").changeNullable(false);

            wpe_t.column("gridterm_id").drop();
        }

        // Надо смержить МСРП, у которых совпадают новые натуральные идентификаторы: студент, дисциплиночасть, год, курс, семестр, част года.
        {
            // Сами МСРП мержим на основе актуального.
            // Если нет актуального, берем самый свежий по дате утраты актуальности.
            final MergeRule mainRule = new MergeRuleBuilder(wpe_t.name())
                    .key("student_id", "registryelementpart_id", "year_id", "course_id", "term_id", "part_id")
                    .orderNullFirst("removaldate_p", OrderDirection.desc)
                    .build();

            MigrationUtils.mergeBuilder(tool, mainRule)

                    /* Учебный процесс */
                    .addRule(byLinkRule("epp_student_wpe_part_t", 0, "studentwpe_id", "type_id")) // МСРП по нагрузке

                    /* Сессия */
                    .addRule(createSessionMarkMergeRule()) // Оценка студента в сессии
                    .addRule(createAttestationSlotMergeRule()) // Запись студента в атт. ведомости
                    .addRule(createSessionSlotRegularMarkMergeRule()) // Оценка студента в сессию (регулярная)
                    .addRule(byLinkRule("session_doc_slot_t", 0, "studentwpecaction_id", "document_id", "insession_p")) // Запись студента по мероприятию в документе сессии
                    .addRule(singleRule("session_slot_rating_data_t", "slot_id")) // Данные рейтинга для записи студента в документе сессии
                    .addRule(singleRule("session_project_theme_t", "slot_id")) // Тема работы студента в сессии
                    .addRule(singleRule("session_att_slot_adddata_t", "slot_id")) // Доп. данные для записи студента в атт. ведомости
                    .addRule(singleRule("session_mark_rating_data_t", "mark_id")) // Данные рейтинга для оценки студента в сессии
                    .addRule(singleRule("session_transfer_op_t", "targetmark_id")) // Перезачтенное мероприятие

                    /* Журналы */
                    .addRule(byLinkRule("epp_rgrp_row_t", 0, "studentwpepart_id", "group_id")) // Запись студента в УГС
                    .addRule(byLinkRule("tr_journal_group_student", 0, "studentwpe_id", "group_id")) // Запись студента в журнале
                    .addRule(byLinkRule("tr_edugrp_event_student", 0, "studentwpe_id", "event_id")) // Запись студента по событию в журнале

                    /* Практики */
                    .addRule(stopRule("pr_practice_assignment_t", "practice_id")) // Направление на практику
                    .addRule(stopRule("pr_practice_result_t", "practiceassignment_id")) // Результат прохождения практики
                    .addRule(stopRule("pr_order_extract_pract_t", "entity_id")) // Выписка о направлении на практику
                    .addRule(stopRule("pr_order_extract_change_t", "entity_id")) // Выписка об изменении приказа на практику

                    /* Визирование */
                    .addRule(stopRule("visa_t", "document_id", "possiblevisa_id", "groupmembervising_id")) // Виза
                    .addRule(stopRule("visa_t", "document_id", "index_p"))
                    .addRule(stopRule("visahistoryitem_t", "document_id", "creationdate_p")) // Элемент истории согласования

                    /* Движение кадров */
                    .addRule(stopRule("mplstextrcttbscrltn_t", "extract_id", "basic_id")) // Связь выписки с основанием

                    /* Рамек */
                    .addRule(singleRule("session_transfer_outop_ext_t", "sssntrnsfrotsdoprtn_id")) // Расширение перезачтенного мероприятия из другого ОУ

                    /* ДВФУ */
                    .addRule(singleRule("session_op_outside_tran_fefu_t", "sssntrnsfrotsdoprtn_id")) // Внешнее перезачтение (расширение ДВФУ)

                    /* ПГУПС */
                    .addRule(singleRule("pgpsmssntextrctrltn_t", "extract_id")) // Связь выписки с командировкой сотрудника

                    /* Do it! */
                    .merge("eppStudentWorkPlanElement");
        }
    }

    // Правило мержа оценок, выставленных человеком (SessionSlotRegularMark).
    private MergeRule createSessionSlotRegularMarkMergeRule() throws Exception {

        return new MergeRule("session_mark_reg_t", ImmutableSet.of(IEntity.P_ID), null) {

            @Override
            protected Long findMainRow(Map<Long, Object[]> groupRows, Long newLinkId, DBTool tool) throws SQLException {
                throw new IllegalStateException(); // мы сюда не должны попадать
            }

            @Override
            protected void afterMergeAction(Map<Long, Set<Long>> outMergeMap, DBTool tool) throws SQLException {

                // Нужно, чтобы для таких оценок обновились итоговые оценки, поэтому создаем правило,
                // которое для всех ссылок на мержащиеся оценки ставит старую дату изменения, чтобы демон их пересчитал.
                final String sql = "update session_mark_t set modificationdate_p=? where id in (select id from session_mark_link_t where target_id in (:p))";
                final int ret = MigrationUtils.massUpdateByIds(tool, sql, ":p", MergeBuilder.getAllIds(outMergeMap), new java.sql.Date(17L));
                tool.debug(ret + " marks from session_mark_link_t updated modification date (FOR DAEMON)");
            }

            @Override
            public String toString() {
                return super.toString() + ", WITH UPDATE modificationDate in sessionMarkLink (FOR DAEMON)";
            }
        };
    }

    // Правило мержа записей в аттестационной ведомости
    private MergeRule createAttestationSlotMergeRule() throws Exception {

        return new MergeRule("session_att_slot_t", ImmutableSet.of("bulletin_id", "studentwpe_id"), ImmutableSet.of("mark_id")) {

            private Map<Long, Integer> priorityMap;

            @Override
            protected Long findMainRow(Map<Long, Object[]> groupRows, Long newLinkId, DBTool tool) throws SQLException {

                if (priorityMap == null) {
                    priorityMap = MigrationUtils.getId2PriorityMap(tool, "session_c_att_mark_t");
                }
                // Сортируем оценки по приоритету и берем первую
                final Map.Entry<Long, Object[]> mainEntry = Collections.min(
                        groupRows.entrySet(),
                        Comparator.comparingInt(entry -> priorityMap.get((Long) entry.getValue()[0]))
                );
                return mainEntry.getKey();
            }

            @Override
            public String toString() {
                return super.toString() + ", WITH SELECT BY mark priority";
            }
        };
    }

    // Правило мержа для оценок в сессию
    private MergeRule createSessionMarkMergeRule() throws Exception {

        return new MergeRule("session_mark_t", ImmutableSet.of("slot_id")) {

            private Long getMainMarkId(Collection<Long> markIds, DBTool tool) throws SQLException {

                final String idsStr = MigrationUtils.idsToStr(markIds);
                final ISQLTranslator translator = tool.getDialect().getSQLTranslator();

                // Сначала ищем оценку, сортируем по приоритету
                {
                    final ISQLQuery query = new SQLSelectQuery()
                            .top(1).column("m.id")
                            .from(SQLFrom.table("session_mark_value_t", "m")
                                          .innerJoin(SQLFrom.table("session_c_mark_grade_t", "v"), "v.id = m.value_id")
                                          .innerJoin(SQLFrom.table("epp_c_gradescale_t", "s"), "s.id = v.scale_id"))
                            .where("m.id in (" + idsStr + ")")
                            .order("s.priority_p")
                            .order("v.priority_p", false); // Внезапно оказалось, что лучшие оценки внизу списка при обычной сортировке по приоритету. См. DEV-8762

                    final Long id = (Long) tool.getUniqueResult(translator.toSql(query));
                    if (id != null) {
                        return id;
                    }
                }

                // Затем ищем отметку, если оценок нет. Сортируем по приоритету
                {
                    final ISQLQuery query = new SQLSelectQuery()
                            .top(1).column("m.id")
                            .from(SQLFrom.table("session_mark_state_t", "m")
                                          .innerJoin(SQLFrom.table("session_c_mark_state_t", "v"), "v.id = m.value_id")
                            )
                            .where("m.id in (" + idsStr + ")")
                            .order("v.priority_p");

                    final Long id = (Long) tool.getUniqueResult(translator.toSql(query));
                    if (id != null) {
                        return id;
                    }
                }

                return null;
            }

            @Override
            public Long findMainRow(Map<Long, Object[]> groupRows, Long newLinkId, DBTool tool) throws SQLException {

                Long id = getMainMarkId(groupRows.keySet(), tool);

                if (id == null) {
                    // Если не нашли, значит это итоговые оценки (sessionSlotLinkMark).
                    // Получаем оценки на которые они ссылаются и сравниваем эти оценки

                    // { linkMark.id -> regularMark.id }
                    final Map<Long, Long> idMap = tool.executeQuery(
                            MigrationUtils.processor(2),
                            "select id, target_id from session_mark_link_t where id in (" + MigrationUtils.idsToStr(groupRows.keySet()) + ")"
                    ).stream().collect(Collectors.toMap(row -> (Long) row[0], row -> (Long) row[1]));

                    final Long regularMarkId = getMainMarkId(idMap.values(), tool);
                    if (regularMarkId == null) { throw new IllegalStateException("Regular mark not found"); }

                    id = idMap.entrySet().stream()
                            .filter(entry -> entry.getValue().equals(regularMarkId))
                            .findFirst().orElseThrow(() -> new IllegalStateException("Wrong regular mark")).getKey();
                }
                return id;
            }

            @Override
            public String toString() {
                return super.toString() + ", WITH SELECT by mark priority";
            }
        };
    }

//    @Override
//    public void run(DBTool tool) throws Exception
//    {
//        // сущность eppStudentWorkPlanElement - Мероприятие студента из РУП (МСРП)
//        final CTable wpe_t = tool.table("epp_student_wpe_t");
//
//        // Готовим пациента к операции.
//        wpe_t.constraints().clear();
//
//        {
//            // В МСРП поле "Семестр сетки" заменяем на три поля: курс, семестр и часть года.
//            // Они войдут в состав натурального идентификатора.
//
//            wpe_t.createColumn(new DBColumn("course_id", DBType.LONG));
//            wpe_t.createColumn(new DBColumn("term_id", DBType.LONG));
//            wpe_t.createColumn(new DBColumn("part_id", DBType.LONG));
//
//            SQLUpdateQuery upd = new SQLUpdateQuery(wpe_t.name(), "wpe");
//            upd.set("course_id", "gt.course_id");
//            upd.set("term_id", "gt.term_id");
//            upd.set("part_id", "gt.part_id");
//            upd.getUpdatedTableFrom().innerJoin(SQLFrom.table("developgridterm_t", "gt"), "wpe.gridterm_id=gt.id");
//
//            int ret = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(upd));
//            if (Debug.isEnabled())
//            {
//                System.out.println(this.getClass().getSimpleName() + ": " + ret + " items migrated in " + wpe_t.name());
//            }
//
//            wpe_t.column("course_id").changeNullable(false);
//            wpe_t.column("term_id").changeNullable(false);
//            wpe_t.column("part_id").changeNullable(false);
//
//            wpe_t.column("gridterm_id").drop();
//        }
//
//        {
//            // Теперь самое страшное.
//            // Надо смержить МСРП, у которых совпадают новые натуральные идентификаторы: студент, дисциплиночасть, год, курс, семестр, част года.
//            // Мержить на основе актуального МСРП. Если нет актуального, берем самый свежий по дате утраты актуальности.
//
//            final Multimap<MultiKey<Long>, PairKey<Long, Date>> keyMap = ArrayListMultimap.create(340, 2);
//            {
//                // Получаем все дубли МСРП
//                final String sql = "select              " +
//                    /*0*/"    t1.id,                    " +
//                    /*1*/"    t1.student_id,            " +
//                    /*2*/"    t1.registryelementpart_id," +
//                    /*3*/"    t1.year_id,               " +
//                    /*4*/"    t1.course_id,             " +
//                    /*5*/"    t1.term_id,               " +
//                    /*6*/"    t1.part_id,               " +
//                    /*7*/"    t1.removaldate_p          " +
//                        "                              " +
//                        " from epp_student_wpe_t t1,   " +
//                        "      epp_student_wpe_t t2    " +
//                        "                              " +
//                        " where                        " +
//                        "    t1.id <> t2.id and                                        " +
//                        "    t1.student_id             = t2.student_id and             " +
//                        "    t1.registryelementpart_id = t2.registryelementpart_id and " +
//                        "    t1.year_id                = t2.year_id and                " +
//                        "    t1.course_id              = t2.course_id and              " +
//                        "    t1.term_id                = t2.term_id and                " +
//                        "    t1.part_id                = t2.part_id                    ";
//
//
//                tool.executeQuery(
//                        processor(Long.class, Long.class, Long.class, Long.class, Long.class, Long.class, Long.class, Date.class),
//                        sql
//                ).forEach(row -> {
//
//                    final Long slotId = (Long) row[0];
//                    final Long studentId = (Long) row[1];
//                    final Long regElementPartId = (Long) row[2];
//                    final Long yearId = (Long) row[3];
//                    final Long courseId = (Long) row[4];
//                    final Long termId = (Long) row[5];
//                    final Long partId = (Long) row[6];
//                    final Date removalDate = (Date) row[7];
//
//                    final MultiKey<Long> key = new MultiKey<>(studentId, regElementPartId, yearId, courseId, termId, partId);
//                    final PairKey<Long, Date> slot = new PairKey<>(slotId, removalDate);
//
//                    keyMap.put(key, slot);
//
//                });
//            }
//
//            // Теперь разбираемся с каждым набором дублей по-отдельности
//            for (Collection<PairKey<Long, Date>> allDuplicates : keyMap.asMap().values()) {
//
//                Preconditions.checkState(allDuplicates.size() > 1);
//
//                // Находим актуальный МСРП. Если все неактуальны - берем самый свежий по дате утраты актуальности.
//                PairKey<Long, Date> mainSlot = null;
//                for (PairKey<Long, Date> slot : allDuplicates) {
//                    if (null == mainSlot) { mainSlot = slot; continue; }
//
//                    final Date dSlot = slot.getSecond();
//                    final Date dMain = mainSlot.getSecond();
//
//                    Preconditions.checkState(!slot.getFirst().equals(mainSlot.getFirst()), "Wrong sql request [1]");
//                    Preconditions.checkState(dSlot != null || dMain != null, "Two or many actual slots");
//
//                    if (dSlot == null || (dMain != null && dSlot.after(dMain))) {
//                        mainSlot = slot;
//                    }
//                }
//
//                assert mainSlot != null;
//                final Long mainSlotId = mainSlot.getFirst();
//                final Set<Long> duplicateForRemoveSlotIds = allDuplicates.stream()
//                        .map(PairKey::getFirst)
//                        .filter(s -> !s.equals(mainSlotId))
//                        .collect(Collectors.toSet());
//
//                Preconditions.checkState(duplicateForRemoveSlotIds.size() + 1 == allDuplicates.size(), "Wrong sql request [2]");
//
//                // Поехали
//                mergeSlots(tool, mainSlot.getFirst(), duplicateForRemoveSlotIds);
//            }
//        }
//    }
//
//    private void mergeSlots(DBTool tool, Long mainSlotId, Set<Long> removedSlotIds) throws SQLException
//    {
//        final Set<Long> allSlotIds = new HashSet<>(removedSlotIds);
//        allSlotIds.add(mainSlotId);
//
//        // Для начала надо разобраться с МСРП по нагрузке (это МСРП + вид УГС).
//        // Если вид УГС совпадает, то мержим. Иначе - переносим на основной МСРП.
//        {
//            final List<Object[]> rows = tool.executeQuery(
//                    processor(Long.class, Long.class, Long.class),
//                    "select id, studentwpe_id, type_id from epp_student_wpe_part_t where studentwpe_id in (" + idsToStr(allSlotIds) + ")"
//            );
//
//            // { [id МСРП] -> [[id МСРП-(АН/ФК), id вида УГС]] }
//            final Multimap<Long, PairKey<Long, Long>> partsMap = ArrayListMultimap.create(rows.size(), 10);
//            for (final Object[] row : rows) {
//                final Long partId = (Long) row[0];
//                final Long slotId = (Long) row[1];
//                final Long typeId = (Long) row[2];
//                partsMap.put(slotId, new PairKey<>(partId, typeId));
//            }
//
//            // МСРП по нагрузке, которые есть у основного МСРП. Одновременно удаляем основной МСРП из мапы.
//            final Set<PairKey<Long, Long>> mainParts = new HashSet<>(partsMap.removeAll(mainSlotId));
//
//            // Теперь мержим или переносим МСРП по нагрузке, ссылающиеся на дубликаты МСРП
//            for (final Collection<PairKey<Long, Long>> otherPartsEntry : partsMap.asMap().values()) {
//
//                // Переносим те МСПР по нагрузке, для которых нет соответствующих МСРП по нагрузке у основного МСРП
//                final List<Long> partsToMove = otherPartsEntry.stream()
//                        .filter(p -> mainParts.stream().noneMatch(m -> p.getSecond().equals(m.getSecond())))
//                        .map(PairKey::getFirst)
//                        .collect(Collectors.toList());
//
//                if (!partsToMove.isEmpty()) {
//                    if (partsToMove.size() != tool.executeUpdate("update epp_student_wpe_part_t set studentwpe_id=? where id in (" + idsToStr(partsToMove) + ")", mainSlotId)) {
//                        throw new IllegalStateException();
//                    }
//                    if (Debug.isEnabled()) {
//                        System.out.println(partsToMove.size() + " rows moved in epp_student_wpe_part_t from slots [" + Arrays.toString(partsToMove.toArray()) + "] to main slot " + mainSlotId);
//                    }
//                    otherPartsEntry.stream()
//                            .filter(p -> partsToMove.contains(p.getFirst()))
//                            .forEach(p -> { if (!mainParts.add(p)) { throw new IllegalStateException(); } });
//                }
//
//                // Остальное надо мержить
//                for (PairKey<Long, Long> otherPart : otherPartsEntry) {
//                    if (partsToMove.contains(otherPart.getFirst())) { continue; }
//
//                    // Ищем МСРП по нагрузке из основного МСРП по соответствию типа УГС
//                    final Long mainPartId = mainParts.stream()
//                            .filter(pMain -> otherPart.getSecond().equals(pMain.getSecond()))
//                            .findAny().orElseThrow(IllegalStateException::new).getFirst();
//
//                    commonMerge(tool, mainPartId, ImmutableList.of(otherPart.getFirst()));
//                }
//            }
//        }
//
//        // Теперь мержим сами МСРП
//        commonMerge(tool, mainSlotId, removedSlotIds);
//    }
//
//    private void commonMerge(DBTool tool, Long mainId, Collection<Long> duplicateIds) throws SQLException
//    {
//        final IEntityMeta entityMeta = EntityRuntime.getMeta(mainId);
//
//        Preconditions.checkState(duplicateIds != null && duplicateIds.size() > 0);
//        Preconditions.checkArgument(EntityRuntime.getMeta(duplicateIds.iterator().next()).equals(entityMeta));
//
//        // Собираем входящие ссылки на сущность: она сама, родители, интерфейсы - на всех может кто-то ссылаться, и надо эти ссылки помёржить.
//        // Детей не берем. (?)
//
//        final Set<IEntityMeta> metaPlainTree = new LinkedHashSet<>();
//        metaPlainTree.add(entityMeta);
//        metaPlainTree.addAll(entityMeta.getInterfaces());
//        for (IEntityMeta parent = entityMeta.getParent(); parent != null; parent = parent.getParent()) {
//            metaPlainTree.add(parent);
//            metaPlainTree.addAll(parent.getInterfaces());
//        }
//
//        final Set<IRelationMeta> incomingRelations = new LinkedHashSet<>();
//        for (IEntityMeta meta : metaPlainTree) {
//            incomingRelations.addAll(meta.getIncomingRelations());
//        }
//
//        // Мигрируем входящие ссылки на новый идентификатор
//        for (IRelationMeta relMeta : incomingRelations) {
//
//            final IEntityMeta relEntity = relMeta.getForeignEntity();
//            if (relEntity.hasTable()) {
//
//                final String relTableName = relEntity.getTableName();
//                final String relColumnName = relMeta.getForeignProperty().getColumnName();
//
//                if (0L == tool.getNumericResult("select count(*) from " + relTableName + " where " + relColumnName + " in (" + idsToStr(duplicateIds) + ")" )) {
//                    continue; // В таблице нет ссылок на удаляемые строки - ничего делать не нужно (ни мержить ни апдейтить).
//                }
//
//                // Убираем констрейнты
//                tool.table(relTableName).constraints().clear();
//
//                final int ret = tool.executeUpdate("update " + relTableName + " set " + relColumnName + "=? where " + relColumnName + " in (" + idsToStr(duplicateIds) + ")",  mainId);
//
//                if (Debug.isEnabled()) {
//                    System.out.println(entityMeta.getName() + " move links: " + ret + " for " + relEntity.getName() + "." + relMeta.getForeignProperty().getName());
//                }
//            }
//        }
//
//        // Удаляем строки из всех дочерних и базовой таблиц
//        for (IEntityMeta meta : metaPlainTree) {
//            if (!meta.isInterface() && meta.hasTable()) {
//                final int ret = tool.executeUpdate("delete from " + meta.getTableName() + " where id in (" +  idsToStr(duplicateIds) + ")");
//                if (Debug.isEnabled()) {
//                    System.out.println("" + ret + " rows deleted (merged) from " + meta.getName());
//                }
//            }
//        }
//    }

}