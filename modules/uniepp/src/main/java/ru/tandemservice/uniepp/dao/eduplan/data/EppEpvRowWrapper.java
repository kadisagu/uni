package ru.tandemservice.uniepp.dao.eduplan.data;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.index.EppIndexedRowWrapper;
import ru.tandemservice.uniepp.dao.index.IEppIndexedRowWrapper;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;

import java.util.*;
import java.util.Map.Entry;

public class EppEpvRowWrapper extends EppIndexedRowWrapper<EppEpvRow> implements IEppEpvRowWrapper, IEppIndexedRowWrapper {
    protected static final Logger logger = Logger.getLogger(EppEpvRowWrapper.class);

    public EppEpvRowWrapper(final EppEpvBlockWrapper version, final EppEpvRowWrapper parent, final EppEpvRow row) {
        super(row);
        this.version = version;
        this.parent = parent;
    }

    private Integer _epvRegistryRowNumber;

    @Override
    public Integer getEpvRegistryRowNumber() {
        return _epvRegistryRowNumber;
    }

    public void setEpvRegistryRowNumber(Integer epvRegistryRowNumber){
        _epvRegistryRowNumber = epvRegistryRowNumber;
    }

    @Override
    public EppRegistryStructure getType() {
        return this.row.getType();
    }

    @Override
    public String getRowType() {
        return this.row.getRowType();
    }

    @Override
    public EppEduPlanVersionBlock getOwner() {
        return this.row.getOwner();
    }

    @Override
    public boolean isTermDataOwner() {
        return (this.getRow() instanceof EppEpvTermDistributedRow);
    }

    @Override
    public String getDisplayableControlActionUsage(String controlActionFullCode)
    {
        if (this.isTermDataOwner()) {

            final List<Integer> termList = this.getActionTermSet(controlActionFullCode);
            if (termList == null || termList.isEmpty()) { return ""; }

            final StringBuilder result = new StringBuilder();
            int i = 0;
            while (i < termList.size())
            {
                final int first = termList.get(i);
                int last = first;
                i++;
                while (i < termList.size() && termList.get(i) - last <= 1)
                {
                    last = termList.get(i);
                    i++;
                }
                if (result.length() > 0) { result.append(", "); }
                result.append(first);
                if (first < last) { result.append("-").append(last);}
            }
            return result.toString();
        }
        return "";
    }

    @Override
    public String getStoredIndex() {
        return this.getRow().getStoredIndex();
    }

    @Override
    public String getUserIndex() {
        return this.getRow().getUserIndex();
    }

    @Override
    public Class getRowEntityClass()
    {
        return this.getRow().getRowEntityClass();
    }

    @Override
    protected String getIndexInternal() {
        return this.getStoredIndex();
    }

    @Override
    public String getIndex() {
        return this.getIndexInternal();
    }


    @Override
    protected int getSequenceNumberInternal() {
        return this.version.getRecordHierarchyIndex(this);
    }

    private Boolean _cache_getUsedInLoad;

    @Override
    public Boolean getUsedInLoad() {
        return (null != this._cache_getUsedInLoad ? this._cache_getUsedInLoad : (this._cache_getUsedInLoad=this.row.getUsedInLoad()));
    }

    private Boolean _cache_getUsedInActions;

    @Override
    public Boolean getUsedInActions() {
        return (null != this._cache_getUsedInActions ? this._cache_getUsedInActions : (this._cache_getUsedInActions = this.row.getUsedInActions()));
    }


    private final EppEpvRowWrapper parent;

    @Override
    public EppEpvRowWrapper getHierarhyParent() {
        return this.parent;
    }

    private final EppEpvBlockWrapper version;

    @Override
    public EppEpvBlockWrapper getVersion() {
        return this.version;
    }

    /* список дочерних элементов */
    private final List<EppEpvRowWrapper> childRows = new ArrayList<>(4);
    public List<EppEpvRowWrapper> getChildRows() { return this.childRows; }

    @SuppressWarnings("unchecked")
    @Override
    public List<IEppEpvRowWrapper> getChilds() {
        return (List) this.childRows;
    }

    /* одие большой мап, который хранит данные по семестрам (нагрузка, мероприятия, ... ) */
    private final Map<Integer, Map<String, Object>> localTermDataMap = new HashMap<>(16);

    public Map<Integer, Map<String, Object>> getLocalTermDataMap() {
        return this.localTermDataMap;
    }



    private Set<Integer> _activeTermSet_cache;

    @Override
    public Set<Integer> getActiveTermSet() {
        Set<Integer> result = this._activeTermSet_cache;
        if (null != result) { return result; }

        result = new LinkedHashSet<>();
        for (final Map.Entry<Integer, Map<String, Object>> termEntry : this.localTermDataMap.entrySet()) {
            if ((null != termEntry.getKey()) && (termEntry.getKey() > 0)) {
                for (final Object value : termEntry.getValue().values()) {
                    if (((value instanceof Number) && (((Number) value).doubleValue() > 0))) {
                        result.add(termEntry.getKey());
                        break;
                    }
                }
            }
        }
        return (this._activeTermSet_cache = result);
    }

    @Override
    public boolean hasInTermActivity(final Integer term) {
        if ((null != term) && (term > 0)) {
            // если семестр реальный, проверяем его в наборе активных семестров
            return this.getActiveTermSet().contains(term);
        }

        // проверяем только нагрузку
        final Map<String, Double> termMap = this.getLocalLoadMap4Term(term);
        for (final Double d : termMap.values()) {
            if ((null != d) && (d > 0)) { return true; }
        }
        return false;
    }

    @Override
    public int getActiveTermNumber(final int term) {
        return CollectionUtils.countMatches(this.getActiveTermSet(), object -> ((null != object) && ((Integer) object <= term)));
    }

    @Override
    public int getTermNumber(int activeTermNumber) {
        if (activeTermNumber <= 0) { return 0; }

        final Iterator<Integer> it = getActiveTermSet().iterator();
        for (int t=1;t<activeTermNumber;t++) {
            if (!it.hasNext()) { return 0; }
            it.next();
        }

        if (!it.hasNext()) { return 0; }
        return it.next();
    }

    private List<EppProfessionalTask> _professionalTaskList;

    @Override
    public List<EppProfessionalTask> getProfessionalTaskList()
    {
        return _professionalTaskList;
    }

    @Override
    public void setProfessionalTaskList(List<EppProfessionalTask> professionalTaskList)
    {
        _professionalTaskList = professionalTaskList;
    }

    // ////////////////////////////////////////////////////////
    // ////////////// контрольные мероприятия /////////////////
    // ////////////////////////////////////////////////////////

    @Override
    public int getActionSize(final Integer term, final String actionFullCode) {
        if ((null != term) && (term > 0)) {
            final Map<String, Object> dataMap = this.localTermDataMap.get(term);
            if (null == dataMap) { return 0; }

            final Object value = dataMap.get(actionFullCode);
            return Math.max(((value instanceof Number) ? ((Number) value).intValue() : 0), 0);
        }

        if (null == term) {
            int result = 0;
            for (final Integer termInteger : this.localTermDataMap.keySet()) {
                if ((null != termInteger) && (termInteger > 0)) {
                    result += this.getActionSize(termInteger.intValue(), actionFullCode);
                }
            }
            return result;
        }

        return 0;
    }

    @Override
    public List<Integer> getActionTermSet(final String actionFullCode) {
        final List<Integer> result = new ArrayList<>();
        for (final Map.Entry<Integer, Map<String, Object>> e : this.localTermDataMap.entrySet()) {
            if ((null != e.getValue()) && (null != e.getKey()) && (e.getKey() > 0)) {
                final Object value = e.getValue().get(actionFullCode);
                if ((value instanceof Number) && (((Number) value).intValue() > 0)) {
                    result.add(e.getKey());
                }
            }
        }
        Collections.sort(result);
        return result;
    }

    // ////////////////////////////////////////////////////////
    // ///////////////////// нагрузка /////////////////////////
    // ////////////////////////////////////////////////////////

    private static final Double ZERO = (double) 0;
    private static final SafeMap.Callback<Integer, Map<String, Double>> TERM_LOAD_MAP_CALLBACK = key -> new HashMap<>(8);

    /** @return справочник нагрузки для указанного семестра */
    @SuppressWarnings("unchecked")
    private Map<String, Double> getLocalLoadMap4Term(final Integer term) {
        final Map<Integer, Map<String, Double>> map = (Map) this.localTermDataMap;
        if (null != term) { return SafeMap.safeGet(map, term, EppEpvRowWrapper.TERM_LOAD_MAP_CALLBACK); }

        // если не указан семестр, то суммируем по семестрам данные из семестровой нагрузки (по каждому виду нагрузки)
        // при этом это значение всегда указывается в итоговых часах (часах в семестр)
        return SafeMap.safeGet(map, Integer.MIN_VALUE, key -> {
            final Map<String, Double> resultMap = EppLoadTypeUtils.newLoadMap();
            for (final Integer termItem : map.keySet()) {
                if (termItem > 0) {
                    for (final Entry<String, Double> e : resultMap.entrySet()) {
                        final double value = EppEpvRowWrapper.this.getTotalInTermLoad(termItem, e.getKey());
                        if (value > 0) {
                            e.setValue(e.getValue() + value);
                        }
                    }
                }
            }
            return resultMap;
        });
    }

    private interface ILoadResolver {
        Double getLoad(final Map<String, Double> loadMap, final String loadFullCode);
    }

    /** см. @see IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE */
    private static class TotalLoadCalcResolver implements ILoadResolver {
        private final String[] _totalAuditFullCodes;
        public TotalLoadCalcResolver(String... totalAuditFullCodes)
        {
            _totalAuditFullCodes = totalAuditFullCodes;
        }

        @Override
        public Double getLoad(final Map<String, Double> loadMap, final String loadFullCode) {
            double result = 0.0f;
            for (final String fullCode : _totalAuditFullCodes) {
                final Double value = loadMap.get(fullCode);
                if ((null == value) || (value < 0)) {
                    continue; /* отбрасываем то, что не удалось посчитать */
                }
                result += value;
            }
            return result;
        }
    }

    private static final String[] TOTAL_AUDIT_LOAD_FULL_CODES = new String[] {
            EppALoadType.FULL_CODE_TOTAL_LECTURES,
            EppALoadType.FULL_CODE_TOTAL_PRACTICE,
            EppALoadType.FULL_CODE_TOTAL_LABS
    };
    private static final String[] TOTAL_AUDIT_LOAD_FULL_CODES_I = new String[] {
            EppALoadType.FULL_CODE_TOTAL_LECTURES_I,
            EppALoadType.FULL_CODE_TOTAL_PRACTICE_I,
            EppALoadType.FULL_CODE_TOTAL_LABS_I
    };
    private static final String[] TOTAL_AUDIT_LOAD_FULL_CODES_E = new String[] {
            EppALoadType.FULL_CODE_TOTAL_LECTURES_E,
            EppALoadType.FULL_CODE_TOTAL_PRACTICE_E,
            EppALoadType.FULL_CODE_TOTAL_LABS_E
    };

    private static class SelfWorkWithoutControlCalcResolver implements ILoadResolver {
        @Override
        public Double getLoad(final Map<String, Double> loadMap, final String loadFullCode) {
            final Double selfwork = loadMap.get(EppELoadType.FULL_CODE_SELFWORK);
            if (selfwork != null)
            {
                final Double control = loadMap.get(EppLoadType.FULL_CODE_CONTROL);
                if (control != null)
                    return selfwork - control;
                return selfwork;
            }
            return 0.0d;
        }
    }


    private static final Map<String, ILoadResolver> RESOLVERS = ImmutableMap.<String, ILoadResolver>builder()
            .put(IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE, new TotalLoadCalcResolver(TOTAL_AUDIT_LOAD_FULL_CODES))
            .put(EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL, new SelfWorkWithoutControlCalcResolver())
            .put(EppALoadType.iFullCode(EppELoadType.FULL_CODE_AUDIT), new TotalLoadCalcResolver(TOTAL_AUDIT_LOAD_FULL_CODES_I))
            .put(EppALoadType.eFullCode(EppELoadType.FULL_CODE_AUDIT), new TotalLoadCalcResolver(TOTAL_AUDIT_LOAD_FULL_CODES_E))
            .put(EppALoadType.iFullCode(IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE), new TotalLoadCalcResolver(TOTAL_AUDIT_LOAD_FULL_CODES_I))
            .put(EppALoadType.eFullCode(IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE), new TotalLoadCalcResolver(TOTAL_AUDIT_LOAD_FULL_CODES_E))
            .put(EppALoadType.eFullCode(EppLoadType.FULL_CODE_TOTAL_HOURS), new TotalLoadCalcResolver(
                    EppALoadType.FULL_CODE_TOTAL_LECTURES_E,
                    EppALoadType.FULL_CODE_TOTAL_PRACTICE_E,
                    EppALoadType.FULL_CODE_TOTAL_LABS_E,
                    EppLoadType.FULL_CODE_CONTROL_E
            ))
            .build();

    /** @return нагрузка (хранящаяся или расчетная) для указанного семестра и кода нагрузки */
    private static Double getLoad(final Map<String, Double> loadMap, final String loadFullCode) {
        if (null == loadFullCode) {
            // просто показываем в логе ошибку (чтобы никто не юзал)
            try {
                throw new NullPointerException("loadFullCode is not-null parameter");
            } catch (final Throwable t) {
                EppEpvRowWrapper.logger.error(t);
            }
            return null;
        }

        Double value = loadMap.get(loadFullCode);
        if (null == value) {
            final ILoadResolver resolver = EppEpvRowWrapper.RESOLVERS.get(loadFullCode);
            if (null != resolver) {
                loadMap.put(loadFullCode, value = resolver.getLoad(loadMap, loadFullCode));
            } else {
                loadMap.put(loadFullCode, value = EppEpvRowWrapper.ZERO);
            }
        }
        return value;
    }

    /** объединяет два справочника нагрузки (покомпонентно для каждого вида нагруки) */
    private static void mergeLoadMap(final Map<String, Double> resultMap, final Map<String, Double> childLoadMap) {
        for (final Map.Entry<String, Double> e : resultMap.entrySet()) {
            final Double value = EppEpvRowWrapper.getLoad(childLoadMap, e.getKey());
            if ((null != value) && (value > 0)) {
                e.setValue(e.getValue() + value);
            }
        }
    }

    /** возвращает дочерние элементы текущего элемента для расчета суммарной нагрузки по элементу */
    private List<EppEpvRowWrapper> getChildRows4LoadCalculation()
    {
        final List<EppEpvRowWrapper> childRows = this.getChildRows();
        final List<EppEpvRowWrapper> result = new ArrayList<>(childRows.size());

        final EppEduPlanVersionBlock selectedBlock = getVersion().getBlock();
        final EppEduPlanVersionBlock myBlock = this.getOwner();
        for (final EppEpvRowWrapper child : childRows) {
            final EppEduPlanVersionBlock childBlock = child.getOwner();
            if (childBlock.isRootBlock()) {
                result.add(child); // строки корневого боока попадают всегда
            } else if (childBlock.equals(myBlock)) {
                result.add(child); // если данная строка уже добавлена (с блоком), то и дочерние строки того же блока попадают
            } else if (null != selectedBlock && childBlock.equals(selectedBlock)) {
                result.add(child); // если все это совпадает с выбранным для расчета нагрузки блоком
            }
        }
        return result;
    }

    private final Map<Integer, Map<String, Double>> hierarchicalTotalTermLoadMap = new HashMap<>(8);

    /** @return данные по строке (с учетом нагрузки дочерних строк) */
    private Map<String, Double> getTotalLoadMap4Term(final Integer term) {
        if ((null != term) && (term > 0)) {
            // не суммирем по реальным семестрам (берем локально)
            // здесь может быть нагрузка как в неделю, так и в семестр
            return this.getLocalLoadMap4Term(term);
        }

        final IEppEpvRow row = this.row;

        if (row instanceof EppEpvGroupImRow) {
            // если это дисциплина по выбору, то НЕ нужно счатить нагрузку по дочерним элементам
            return this.getLocalLoadMap4Term(term);
        }

        // иначе - это просто сумма элементов
        // важно понимать, что здесь нагрузка всегда в часах в семестр (что в итоге, что в элементах)
        return SafeMap.safeGet(this.hierarchicalTotalTermLoadMap, term, term1 -> {
            final Map<String, Double> resultMap = EppLoadTypeUtils.newLoadMap();
            EppEpvRowWrapper.mergeLoadMap(resultMap, EppEpvRowWrapper.this.getLocalLoadMap4Term(term1));
            final List<EppEpvRowWrapper> childRows1 = EppEpvRowWrapper.this.getChildRows4LoadCalculation();
            for (final EppEpvRowWrapper child : childRows1) {
                EppEpvRowWrapper.mergeLoadMap(resultMap, child.getTotalLoadMap4Term(term1));
            }
            return resultMap;
        });
    }

    @Override
    public String getTotalDisplayableLoadString(Integer term, String loadFullCode, boolean hideZero, int divider)
    {
        Preconditions.checkArgument(divider > 0);

        return UniEppUtils.formatLoad(this.getTotalLoad(term, loadFullCode) / divider, hideZero);
    }

    @Override
    public String getRowWrapperTotalByLoadTypeRaw(String loadFullCode, int divider)
    {
        Preconditions.checkArgument(divider > 0);

        final double registryValue = this.getTotalLoad(0, loadFullCode) / divider;
        final String registryValueString = UniEppUtils.formatLoad(registryValue, false);

        if (this.isTermDataOwner()) {
            /* если элемент содержит данные по нагрузке. придется считать соответствие */
            final double calculatedValue = this.getTotalLoad(null, loadFullCode) / divider;
            if ((calculatedValue <= 0) || UniEppUtils.eq(registryValue, calculatedValue)) {
                // TODO: FIXME: писать ошибку, если не распределено
                // если не распределено или совпало - то выводим то, что есть
                return registryValueString;
            }
            final String calculatedValueString = UniEppUtils.formatLoad(calculatedValue, false);
            return UniEppUtils.error(calculatedValueString /*по УП-как сумма по строке*/, registryValueString /*из реестра*/);
        }

        return registryValueString;
    }

    @Override
    public double getTotalLoad(final Integer term, final String loadFullCode) {
        // если у элемента есть часы - то отображаем именно их, если нет - то берем сумму

        // TODO когда в строке УПв появится поле для суммы по неделям - найти все места, которые вызывают данный метод с term==null и loadFullCode==EppLoadType.FULL_CODE_WEEKS
        // и заменить null на 0, если того требует логика

        final Map<String, Double> map = (this.isTermDataOwner() ? this.getLocalLoadMap4Term(term) : this.getTotalLoadMap4Term(term));
        final Double value = EppEpvRowWrapper.getLoad(map, loadFullCode);
        return (null == value ? 0d : value);
    }

    @Override
    public double getTotalDisplayableLoad(final Integer term, final String loadFullCode) {
        final double totalLoad = this.getTotalLoad(term, loadFullCode);
        if (totalLoad <= 0) { return 0; }

/*
        if ((null != term) && (term > 0)) {
            // всегда в часах в семестр
            if (EppLoadTypeUtils.isXLoad(loadFullCode)) { return totalLoad; }

            return totalLoad;
        }
*/

        // null - сумма (всегда выводится в часах в семестр), 0 - реестр (всегда выводится в часах в семестр)
        return totalLoad;
    }

    @Override
    public double getTotalInTermLoad(final Integer term, final String loadFullCode) {
        final double totalLoad = this.getTotalLoad(term, loadFullCode);
        if (totalLoad <= 0) { return 0; }

/*
        if ((null != term) && (term > 0)) {
            // всегда в часах в семестр
            if (EppLoadTypeUtils.isXLoad(loadFullCode)) { return totalLoad; }

            return totalLoad;
        }
*/

        // null - сумма (всегда выводится в часах в семестр), 0 - реестр (всегда выводится в часах в семестр)
        return totalLoad;
    }

    // распределение нагрузки для конкретного семестра
    public static Map<String, Double> getTotalInTermLoadMap(final IEppEpvRowWrapper wrapper, final int term) {
        final Map<String, Double> result = EppLoadTypeUtils.newLoadMap();
        for (final Map.Entry<String, Double> e : result.entrySet()) {
            e.setValue(wrapper.getTotalInTermLoad(term, e.getKey()));
        }
        return result;
    }

    /** закрывает враппер, убивает все данные (в помощь GC) */
    public void close() {
        this.localTermDataMap.values().forEach(Map::clear);
        this.localTermDataMap.clear();

        this.hierarchicalTotalTermLoadMap.values().forEach(Map::clear);
        this.hierarchicalTotalTermLoadMap.clear();

        this.childRows.clear();
    }

    // возврящает мап всей нагрузки для строки УП
    @SuppressWarnings("unchecked")
    @Override
    public Map<Integer, Map<String, Number>> getWrapperDataMap() {
        final Map<Integer, Map<String, Number>> dataMap = new HashMap<>(this.getLocalTermDataMap().keySet().size());
        for (final Entry<Integer, Map<String, Object>> entry : this.getLocalTermDataMap().entrySet()) {
            final Integer key = entry.getKey();
            if ((null != key) && (key > 0)) {
                dataMap.put(key, new HashMap<>((Map) entry.getValue()));
            }
        }
        return dataMap;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String getTermKey(final Integer term)
    {
        if (null == term) { return "empty"; }
        if (!this.getActiveTermSet().contains(term)) { return "empty"; }

        final StringBuilder result = new StringBuilder();
        final List<Map.Entry<String, Number>> entries = new ArrayList<>((Set)this.getLocalTermDataMap().get(term).entrySet());
        Collections.sort(entries, (o1, o2) -> o1.getKey().compareTo(o2.getKey()));
        for (final Map.Entry<String, Number> entry : entries) {
            result.append(entry.getKey()).append("=").append(entry.getValue()).append(",");
        }
        return result.toString();
    }
}
