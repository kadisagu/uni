// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.settings.DefaultSizeForLaborAddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniepp.entity.settings.EppDefaultSizeForLabor;

/**
 * @author oleyba
 * @since 07.10.2010
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "rule.id")
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    private EppDefaultSizeForLabor rule = new EppDefaultSizeForLabor();

    private ISelectModel developFormList;
    private ISelectModel developConditionList;
    private ISelectModel developTechList;
    private ISelectModel developGridList;

    public boolean isEditForm()
    {
        return this.rule.getId() != null;
    }

    public EppDefaultSizeForLabor getRule()
    {
        return this.rule;
    }

    public void setRule(final EppDefaultSizeForLabor rule)
    {
        this.rule = rule;
    }

    public ISelectModel getDevelopFormList()
    {
        return this.developFormList;
    }

    public void setDevelopFormList(final ISelectModel developFormList)
    {
        this.developFormList = developFormList;
    }

    public ISelectModel getDevelopConditionList()
    {
        return this.developConditionList;
    }

    public void setDevelopConditionList(final ISelectModel developConditionList)
    {
        this.developConditionList = developConditionList;
    }

    public ISelectModel getDevelopTechList()
    {
        return this.developTechList;
    }

    public void setDevelopTechList(final ISelectModel developTechList)
    {
        this.developTechList = developTechList;
    }

    public ISelectModel getDevelopGridList()
    {
        return this.developGridList;
    }

    public void setDevelopGridList(final ISelectModel developGridList)
    {
        this.developGridList = developGridList;
    }
}
