/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.logic.*;

/**
 * @author Igor Belanov
 * @since 28.02.2017
 */
@Configuration
public class EppDevelopResultManager extends BusinessObjectManager
{
    public static EppDevelopResultManager instance()
    {
        return instance(EppDevelopResultManager.class);
    }

    @Bean
    public IEppDevelopResultDAO dao()
    {
        return new EppDevelopResultDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eppEPVBlockProfActivityTypeSearchDSHandler()
    {
        return new EppEPVBlockProfActivityTypeSearchDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler eppProfActivityTypeComboDSHandler()
    {
        return new EppProfActivityTypeComboDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler eppEpvRegRowProfessionalTaskSearchDSHandler()
    {
        return new EppEpvRegRowProfessionalTaskSearchDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler eppEpvRegRowSkillSearchDSHandler()
    {
        return new EppEpvRegRowSkillSearchDSHandler(getName());
    }
}
