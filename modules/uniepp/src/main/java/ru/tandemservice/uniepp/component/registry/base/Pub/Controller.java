package ru.tandemservice.uniepp.component.registry.base.Pub;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.file.FileNotFoundApplicationException;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import org.tandemframework.shared.commonbase.remoteDocument.service.ICommonRemoteDocumentService;
import org.tandemframework.shared.commonbase.remoteDocument.service.IRemoteDocumentProvider;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.CloneElement.EppRegistryCloneElement;
import ru.tandemservice.uniepp.base.bo.EppReorganization.ui.EditRegistryElementOwner.EppReorganizationEditRegistryElementOwner;
import ru.tandemservice.uniepp.remoteDocument.ext.RemoteDocument.RemoteDocumentExtManager;

/**
 * 
 * @author nkokorina
 *
 */

@SuppressWarnings("unchecked")
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickEditElement(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final String name = StringUtils.removeEnd(model.getClass().getPackage().getName(), ".Pub");
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(name + ".AddEdit", new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getElement().getId())));
    }

    protected void downloadExtFile(Long fileId, String taskName) {
        Preconditions.checkNotNull(fileId);

        try (final ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(taskName)) {
            final RemoteDocumentDTO dto = service.get(fileId);
            Preconditions.checkNotNull(dto, "File with id " + fileId + " not found in service " + service.getClass().getSimpleName());

            final CommonBaseRenderer renderer = new CommonBaseRenderer().contentType(dto.getFileType()).fileName(dto.getFileName()).document(dto.getContent());
            BusinessComponentUtils.downloadDocument(renderer, true);
        }
    }

    protected void deleteExtFile(Long fileId, String taskName) {
        Preconditions.checkNotNull(fileId);

        try (final ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(taskName)) {
            RemoteDocumentDTO dto = null;
            try {
                dto = service.get(fileId);
            } catch (FileNotFoundApplicationException ignored) {
            }
            if (dto != null)
                service.delete(dto);
        }
    }

    public void onClickDownloadWorkProgram(IBusinessComponent component)
    {
        downloadExtFile(this.getModel(component).getElement().getWorkProgramFile(), RemoteDocumentExtManager.EPP_REGISTRY_WORK_PROGRAM_ANNOTATION_SERVICE_NAME);
    }

    public void onClickDownloadWorkProgramAnnotation(IBusinessComponent component)
    {
        downloadExtFile(this.getModel(component).getElement().getWorkProgramAnnotation(), RemoteDocumentExtManager.EPP_REGISTRY_WORK_PROGRAM_ANNOTATION_SERVICE_NAME);
    }

    public void onClickDeleteElement(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        Long workProgramFile = model.getElement().getWorkProgramFile();
        Long workProgramAnnotation = model.getElement().getWorkProgramAnnotation();
        UniDaoFacade.getCoreDao().delete(model.getElement().getId());
        if (workProgramFile != null)
            deleteExtFile(workProgramFile, RemoteDocumentExtManager.EPP_REGISTRY_WORK_PROGRAM_ANNOTATION_SERVICE_NAME);
        if (workProgramAnnotation != null)
            deleteExtFile(workProgramAnnotation, RemoteDocumentExtManager.EPP_REGISTRY_WORK_PROGRAM_ANNOTATION_SERVICE_NAME);
        this.deactivate(component);
    }

    public void onClickClone(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(EppRegistryCloneElement.class.getSimpleName(), new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getElement().getId())));
    }

    public void onClickChangeOwner(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(EppReorganizationEditRegistryElementOwner.class.getSimpleName(), new ParametersMap().add("singleValue", model.getElement().getId())));
    }
}
