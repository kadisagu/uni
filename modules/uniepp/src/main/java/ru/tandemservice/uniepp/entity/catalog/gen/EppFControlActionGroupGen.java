package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Группа форм итогового контроля
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppFControlActionGroupGen extends EntityBase
 implements INaturalIdentifiable<EppFControlActionGroupGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup";
    public static final String ENTITY_NAME = "eppFControlActionGroup";
    public static final int VERSION_HASH = 1678379776;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_THEME_REQUIRED = "themeRequired";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private boolean _themeRequired;     // Требует темы
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Требует темы. Свойство не может быть null.
     */
    @NotNull
    public boolean isThemeRequired()
    {
        return _themeRequired;
    }

    /**
     * @param themeRequired Требует темы. Свойство не может быть null.
     */
    public void setThemeRequired(boolean themeRequired)
    {
        dirty(_themeRequired, themeRequired);
        _themeRequired = themeRequired;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppFControlActionGroupGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EppFControlActionGroup)another).getCode());
            }
            setThemeRequired(((EppFControlActionGroup)another).isThemeRequired());
            setTitle(((EppFControlActionGroup)another).getTitle());
        }
    }

    public INaturalId<EppFControlActionGroupGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EppFControlActionGroupGen>
    {
        private static final String PROXY_NAME = "EppFControlActionGroupNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppFControlActionGroupGen.NaturalId) ) return false;

            EppFControlActionGroupGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppFControlActionGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppFControlActionGroup.class;
        }

        public T newInstance()
        {
            return (T) new EppFControlActionGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "themeRequired":
                    return obj.isThemeRequired();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "themeRequired":
                    obj.setThemeRequired((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "themeRequired":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "themeRequired":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "themeRequired":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppFControlActionGroup> _dslPath = new Path<EppFControlActionGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppFControlActionGroup");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Требует темы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup#isThemeRequired()
     */
    public static PropertyPath<Boolean> themeRequired()
    {
        return _dslPath.themeRequired();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EppFControlActionGroup> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _themeRequired;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EppFControlActionGroupGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Требует темы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup#isThemeRequired()
     */
        public PropertyPath<Boolean> themeRequired()
        {
            if(_themeRequired == null )
                _themeRequired = new PropertyPath<Boolean>(EppFControlActionGroupGen.P_THEME_REQUIRED, this);
            return _themeRequired;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppFControlActionGroupGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EppFControlActionGroup.class;
        }

        public String getEntityName()
        {
            return "eppFControlActionGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
