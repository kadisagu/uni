package ru.tandemservice.uniepp.component.registry.RegElementPartList;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {

    void deleteRow(Model model, Long id);

    void addPart(final Model model, final Long regElemId);

    void doMove(Model model, Long id, int direction);
}