package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Нагрузка элемента реестра (всего)
 *
 * Целевая (предполагаемая) итоговая нагрузка по элементу реестра
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegistryElementLoadGen extends EntityBase
 implements INaturalIdentifiable<EppRegistryElementLoadGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad";
    public static final String ENTITY_NAME = "eppRegistryElementLoad";
    public static final int VERSION_HASH = -963373274;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_ELEMENT = "registryElement";
    public static final String L_LOAD_TYPE = "loadType";
    public static final String P_LOAD = "load";

    private EppRegistryElement _registryElement;     // Элемент реестра
    private EppLoadType _loadType;     // Базовый класс для вида теоретической нагрузки
    private long _load;     // Число часов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getRegistryElement()
    {
        return _registryElement;
    }

    /**
     * @param registryElement Элемент реестра. Свойство не может быть null.
     */
    public void setRegistryElement(EppRegistryElement registryElement)
    {
        dirty(_registryElement, registryElement);
        _registryElement = registryElement;
    }

    /**
     * @return Базовый класс для вида теоретической нагрузки. Свойство не может быть null.
     */
    @NotNull
    public EppLoadType getLoadType()
    {
        return _loadType;
    }

    /**
     * @param loadType Базовый класс для вида теоретической нагрузки. Свойство не может быть null.
     */
    public void setLoadType(EppLoadType loadType)
    {
        dirty(_loadType, loadType);
        _loadType = loadType;
    }

    /**
     * @return Число часов. Свойство не может быть null.
     */
    @NotNull
    public long getLoad()
    {
        return _load;
    }

    /**
     * @param load Число часов. Свойство не может быть null.
     */
    public void setLoad(long load)
    {
        dirty(_load, load);
        _load = load;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRegistryElementLoadGen)
        {
            if (withNaturalIdProperties)
            {
                setRegistryElement(((EppRegistryElementLoad)another).getRegistryElement());
                setLoadType(((EppRegistryElementLoad)another).getLoadType());
            }
            setLoad(((EppRegistryElementLoad)another).getLoad());
        }
    }

    public INaturalId<EppRegistryElementLoadGen> getNaturalId()
    {
        return new NaturalId(getRegistryElement(), getLoadType());
    }

    public static class NaturalId extends NaturalIdBase<EppRegistryElementLoadGen>
    {
        private static final String PROXY_NAME = "EppRegistryElementLoadNaturalProxy";

        private Long _registryElement;
        private Long _loadType;

        public NaturalId()
        {}

        public NaturalId(EppRegistryElement registryElement, EppLoadType loadType)
        {
            _registryElement = ((IEntity) registryElement).getId();
            _loadType = ((IEntity) loadType).getId();
        }

        public Long getRegistryElement()
        {
            return _registryElement;
        }

        public void setRegistryElement(Long registryElement)
        {
            _registryElement = registryElement;
        }

        public Long getLoadType()
        {
            return _loadType;
        }

        public void setLoadType(Long loadType)
        {
            _loadType = loadType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppRegistryElementLoadGen.NaturalId) ) return false;

            EppRegistryElementLoadGen.NaturalId that = (NaturalId) o;

            if( !equals(getRegistryElement(), that.getRegistryElement()) ) return false;
            if( !equals(getLoadType(), that.getLoadType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRegistryElement());
            result = hashCode(result, getLoadType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRegistryElement());
            sb.append("/");
            sb.append(getLoadType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegistryElementLoadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegistryElementLoad.class;
        }

        public T newInstance()
        {
            return (T) new EppRegistryElementLoad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "registryElement":
                    return obj.getRegistryElement();
                case "loadType":
                    return obj.getLoadType();
                case "load":
                    return obj.getLoad();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "registryElement":
                    obj.setRegistryElement((EppRegistryElement) value);
                    return;
                case "loadType":
                    obj.setLoadType((EppLoadType) value);
                    return;
                case "load":
                    obj.setLoad((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "registryElement":
                        return true;
                case "loadType":
                        return true;
                case "load":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "registryElement":
                    return true;
                case "loadType":
                    return true;
                case "load":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "registryElement":
                    return EppRegistryElement.class;
                case "loadType":
                    return EppLoadType.class;
                case "load":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRegistryElementLoad> _dslPath = new Path<EppRegistryElementLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegistryElementLoad");
    }
            

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad#getRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> registryElement()
    {
        return _dslPath.registryElement();
    }

    /**
     * @return Базовый класс для вида теоретической нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad#getLoadType()
     */
    public static EppLoadType.Path<EppLoadType> loadType()
    {
        return _dslPath.loadType();
    }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad#getLoad()
     */
    public static PropertyPath<Long> load()
    {
        return _dslPath.load();
    }

    public static class Path<E extends EppRegistryElementLoad> extends EntityPath<E>
    {
        private EppRegistryElement.Path<EppRegistryElement> _registryElement;
        private EppLoadType.Path<EppLoadType> _loadType;
        private PropertyPath<Long> _load;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad#getRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> registryElement()
        {
            if(_registryElement == null )
                _registryElement = new EppRegistryElement.Path<EppRegistryElement>(L_REGISTRY_ELEMENT, this);
            return _registryElement;
        }

    /**
     * @return Базовый класс для вида теоретической нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad#getLoadType()
     */
        public EppLoadType.Path<EppLoadType> loadType()
        {
            if(_loadType == null )
                _loadType = new EppLoadType.Path<EppLoadType>(L_LOAD_TYPE, this);
            return _loadType;
        }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad#getLoad()
     */
        public PropertyPath<Long> load()
        {
            if(_load == null )
                _load = new PropertyPath<Long>(EppRegistryElementLoadGen.P_LOAD, this);
            return _load;
        }

        public Class getEntityClass()
        {
            return EppRegistryElementLoad.class;
        }

        public String getEntityName()
        {
            return "eppRegistryElementLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
