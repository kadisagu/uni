package ru.tandemservice.uniepp.dao.eduStd;

import java.util.Collection;
import java.util.Map;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import ru.tandemservice.uniepp.dao.eduStd.data.IEppStdWrapper;

/**
 * @author vdanilov
 */
public interface IEppEduStdDataDAO
{
    public static final SpringBeanCache<IEppEduStdDataDAO> instance = new SpringBeanCache<IEppEduStdDataDAO>(IEppEduStdDataDAO.class.getName());

    /**
     * @param eppEduStdIds - идентификаторы ГОСов (not-null)
     * @param loadDetails - загружать ограничения, компетенции, ...
     * @return { std.id -> wrapper(std) }
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<Long, IEppStdWrapper> getEduStdDataMap(Collection<Long> eppEduStdIds, boolean loadDetails);

}
