package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид аудиторной нагрузки (типы аудиторных занятий)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppALoadTypeGen extends EppLoadType
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppALoadType";
    public static final String ENTITY_NAME = "eppALoadType";
    public static final int VERSION_HASH = -902352705;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPP_GROUP_TYPE = "eppGroupType";

    private EppGroupTypeALT _eppGroupType;     // Вид учебной группы (ВАН)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид учебной группы (ВАН). Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppGroupTypeALT getEppGroupType()
    {
        return _eppGroupType;
    }

    /**
     * @param eppGroupType Вид учебной группы (ВАН). Свойство не может быть null и должно быть уникальным.
     */
    public void setEppGroupType(EppGroupTypeALT eppGroupType)
    {
        dirty(_eppGroupType, eppGroupType);
        _eppGroupType = eppGroupType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppALoadTypeGen)
        {
            setEppGroupType(((EppALoadType)another).getEppGroupType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppALoadTypeGen> extends EppLoadType.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppALoadType.class;
        }

        public T newInstance()
        {
            return (T) new EppALoadType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "eppGroupType":
                    return obj.getEppGroupType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "eppGroupType":
                    obj.setEppGroupType((EppGroupTypeALT) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eppGroupType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eppGroupType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "eppGroupType":
                    return EppGroupTypeALT.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppALoadType> _dslPath = new Path<EppALoadType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppALoadType");
    }
            

    /**
     * @return Вид учебной группы (ВАН). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppALoadType#getEppGroupType()
     */
    public static EppGroupTypeALT.Path<EppGroupTypeALT> eppGroupType()
    {
        return _dslPath.eppGroupType();
    }

    public static class Path<E extends EppALoadType> extends EppLoadType.Path<E>
    {
        private EppGroupTypeALT.Path<EppGroupTypeALT> _eppGroupType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид учебной группы (ВАН). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppALoadType#getEppGroupType()
     */
        public EppGroupTypeALT.Path<EppGroupTypeALT> eppGroupType()
        {
            if(_eppGroupType == null )
                _eppGroupType = new EppGroupTypeALT.Path<EppGroupTypeALT>(L_EPP_GROUP_TYPE, this);
            return _eppGroupType;
        }

        public Class getEntityClass()
        {
            return EppALoadType.class;
        }

        public String getEntityName()
        {
            return "eppALoadType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
