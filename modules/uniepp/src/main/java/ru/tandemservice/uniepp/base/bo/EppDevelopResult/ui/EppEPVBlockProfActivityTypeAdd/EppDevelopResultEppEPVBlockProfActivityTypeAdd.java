/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.EppEPVBlockProfActivityTypeAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.EppDevelopResultManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author Igor Belanov
 * @since 28.02.2017
 */
@Configuration
public class EppDevelopResultEppEPVBlockProfActivityTypeAdd extends BusinessComponentManager
{
    public static String EPV_BLOCK_DS = "EPVBlockDS";
    public static String PROF_ACTIVITY_TYPE_DS = "profActivityTypeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(EPV_BLOCK_DS, getName(), EppEduPlanVersionBlock.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(PROF_ACTIVITY_TYPE_DS, EppDevelopResultManager.instance().eppProfActivityTypeComboDSHandler()))
                .create();
    }
}
