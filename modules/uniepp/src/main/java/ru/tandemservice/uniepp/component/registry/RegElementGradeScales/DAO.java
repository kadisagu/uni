/* $Id$ */
package ru.tandemservice.uniepp.component.registry.RegElementGradeScales;

import java.util.List;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;

/**
 * @author oleyba
 * @since 11/15/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final List<EppRegistryElementPartFControlAction> caList = this.getList(
                EppRegistryElementPartFControlAction.class,
                EppRegistryElementPartFControlAction.part().registryElement().s(),
                model.getElement(),
                EppRegistryElementPartFControlAction.part().number().s(),
                EppRegistryElementPartFControlAction.controlAction().code().s()
        );
        model.getRowList().clear();
        for (final EppRegistryElementPartFControlAction ca : caList) {
            if (!model.getRowList().contains(ca.getPart())) {
                model.getRowList().add(ca.getPart());
            }
            model.getRowList().add(ca);
        }
        model.setScaleModel(new LazySimpleSelectModel<EppGradeScale>(EppGradeScale.class));
    }

    @Override
    public void update(final Model model)
    {
        for (final IEntity entity : model.getRowList()) {
            this.update(entity);
        }
    }
}
