/**
 *$Id: EppWorkPlanStudentList.java 26167 2013-02-14 14:19:39Z ashaburov $
 */
package ru.tandemservice.uniepp.base.bo.EppGroupOrgUnit.ui.StudentsInNewGrpsList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow.IEppRealEduGroupRowDescription;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Configuration
public class EppGroupOrgUnitStudentsInNewGrpsList extends AbstractUniStudentList
{
    public static final String GROUP_LIST_DS = "groupListDS";

    public static final String EDU_GROUP_LIST = "eduGroupList";
    public static final String CHECKBOX_COLUMN = "checkbox";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(
            this.presenterExtPointBuilder()
            .addDataSource(this.searchListDS(STUDENT_SEARCH_LIST_DS, this.studentSearchListDSColumnExtPoint(), this.studentSearchListDSHandler()))
            .addDataSource(this.selectDS(GROUP_LIST_DS, this.groupListDSHandler()))
        );
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> groupListDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Group.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                final Long id = context.get(EppGroupOrgUnitStudentsInNewGrpsListUI.PARAM_ID);
                dql.where(in(
                    property(alias, "id"),
                    new DQLSelectBuilder()
                    .fromEntity(Student.class, "s").column(property(Student.group().id().fromAlias("s")))
                    .where(eq(property(Student.archival().fromAlias("s")), value(Boolean.FALSE)))
                    .where(eq(property(Student.status().active().fromAlias("s")), value(Boolean.TRUE)))
                    .where(eq(property(Student.educationOrgUnit().groupOrgUnit().id().fromAlias("s")), value(id)))
                    .buildQuery()
                ));
            }
        }.order(Group.title()).filter(Group.title());
    }

    @Bean
    public ColumnListExtPoint studentSearchListDSColumnExtPoint()
    {
        return this.createStudentSearchListColumnsBuilder()
        .create();
    }

    @Override
    protected List<ColumnBase> prepareColumnList()
    {
        final List<ColumnBase> columnList = new ArrayList<ColumnBase>();
        columnList.add(checkboxColumn(CHECKBOX_COLUMN).create());
        columnList.add(this.newFioColumnBuilder(null).create());
        columnList.add(this.newBookNumberColumnBuilder().create());
        columnList.add(this.newCourseColumnBuilder().create());
        columnList.add(this.newGroupColumnBuilder().create());
        columnList.add(this.newCategoryColumnBuilder().create());

        //columnList.add(newFormativeOrgUnitColumnBuilder().create());
        //columnList.add(newTerritorialOrgUnitColumnBuilder().create());
        //columnList.add(newProductiveOrgUnitColumnBuilder().create());
        //columnList.add(newEducationLevelHSColumnBuilder().create());
        columnList.add(this.newDevelopCombinationColumnBuilder().create());


        final IPublisherLinkResolver resolver = new IPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) {
                return new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, ((EppRealEduGroupRow)entity).getGroup().getId())
                .add("orgUnitId", ((EppGroupOrgUnitStudentsInNewGrpsListUI)ContextLocal.getComponent().getPresenter()).getOrgUnitHolder().getId());
            }
            @Override public String getComponentName(final IEntity entity) {
                return ru.tandemservice.uniepp.component.edugroup.pub.GroupOrgUnitGroupPub.Model.class.getPackage().getName();
            }
        };
        columnList.add(publisherColumn(EDU_GROUP_LIST).entityListProperty(EDU_GROUP_LIST).path("group.activityTitle").publisherLinkResolver(resolver).formatter(NewLineFormatter.NOBR_IN_LINES).create());

        return columnList;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchListDSHandler()
    {
        return new AbstractStudentSearchListDSHandler(this.getName()) {

            @Override
            protected void addAdditionalRestrictions(final DQLSelectBuilder builder, final String alias, final DSInput input, final ExecutionContext context)
            {
                final EppYearEducationProcess currentEppYear = DataAccessServices.dao().get(EppYearEducationProcess.class, EppYearEducationProcess.educationYear().current(), Boolean.TRUE);
                final Long id = context.get(EppGroupOrgUnitStudentsInNewGrpsListUI.PARAM_ID);
                final List<Group> groupList = context.get(EppGroupOrgUnitStudentsInNewGrpsListUI.PARAM_GROUP_LIST);

                builder.where(eq(property(Student.archival().fromAlias(alias)), value(Boolean.FALSE)));
                builder.where(eq(property(Student.status().active().fromAlias(alias)), value(Boolean.TRUE)));
                builder.where(eq(property(Student.educationOrgUnit().groupOrgUnit().id().fromAlias(alias)), value(id)));

                if (null != groupList && groupList.size() > 0) {
                    builder.where(in(property(Student.group().fromAlias(alias)), groupList));
                }

                IDQLExpression e = null;
                final Collection<IEppRealEduGroupRowDescription> relations = IEppRealGroupRowDAO.instance.get().getRelations();
                for (final IEppRealEduGroupRowDescription dsc: relations) {
                    e = or(e, exists(this.baseDql(dsc, currentEppYear)
                        .column(property("r.id"))
                        .where(eq(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().fromAlias("r")), property(alias)))
                        .buildQuery()));
                }
                builder.where(e);
            }

            @SuppressWarnings("unchecked")
            @Override
            protected DSOutput execute(final DSInput input, final ExecutionContext context)
            {
                final DSOutput output = super.execute(input, context);

                if (output.getCountRecord() > 0) {
                    final EppYearEducationProcess currentEppYear = DataAccessServices.dao().get(EppYearEducationProcess.class, EppYearEducationProcess.educationYear().current(), Boolean.TRUE);
                    final List<DataWrapper> wrappers = DataWrapper.wrap(output);
                    for (final DataWrapper wrapper: wrappers) {
                        wrapper.setProperty(EDU_GROUP_LIST, new ArrayList(4));
                    }

                    final Map<Long, DataWrapper> wrapperMap = CommonDAO.map(wrappers);
                    final Collection<IEppRealEduGroupRowDescription> relations = IEppRealGroupRowDAO.instance.get().getRelations();
                    for (final IEppRealEduGroupRowDescription dsc: relations) {

                        final List<Object[]> rows = this.baseDql(dsc, currentEppYear)
                        .where(in(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().id().fromAlias("r")), wrapperMap.keySet()))
                        .column(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().id().fromAlias("r")))
                        .column(property(EppRealEduGroupRow.id().fromAlias("r")))
                        .createStatement(context.getSession()).list();

                        for (final Object[] row: rows) {
                            final DataWrapper wrapper = wrapperMap.get(row[0]);
                            final List<EppRealEduGroupRow> groups = (List<EppRealEduGroupRow>)wrapper.getProperty(EDU_GROUP_LIST);
                            groups.add((EppRealEduGroupRow)context.getSession().get(dsc.relationClass(), (Long)row[1]));
                        }
                    }


                    for (final DataWrapper wrapper: wrappers) {
                        Collections.sort((List<EppRealEduGroupRow>)wrapper.getProperty(EDU_GROUP_LIST), (o1, o2) -> EppRealEduGroup.FULL_COMPARATOR.compare(o1.getGroup(), o2.getGroup()));
                    }

                }

                return output;
            }

            protected DQLSelectBuilder baseDql(final IEppRealEduGroupRowDescription dsc, final EppYearEducationProcess currentEppYear) {
                return new DQLSelectBuilder()
                .fromEntity(dsc.relationClass(), "r")
                .where(eq(property(EppRealEduGroupRow.group().summary().yearPart().year().fromAlias("r")), value(currentEppYear)))
                .where(isNull(property(EppRealEduGroupRow.group().level().fromAlias("r"))))
                .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("r"))));
            }

        };
    }
}
