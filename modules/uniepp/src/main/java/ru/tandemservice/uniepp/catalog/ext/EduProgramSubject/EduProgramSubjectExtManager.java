/* $Id$ */
package ru.tandemservice.uniepp.catalog.ext.EduProgramSubject;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Igor Belanov
 * @since 22.02.2017
 */
@Configuration
public class EduProgramSubjectExtManager extends BusinessObjectExtensionManager
{
}
