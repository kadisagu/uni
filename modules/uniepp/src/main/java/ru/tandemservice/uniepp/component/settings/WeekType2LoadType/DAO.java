package ru.tandemservice.uniepp.component.settings.WeekType2LoadType;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.settings.EppELoadWeekType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author nkokorina
 *
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(final Model model)
    {
        final MQBuilder builder = new MQBuilder(EppELoadWeekType.ENTITY_NAME, "lwt");
        final List<EppELoadWeekType> relList = builder.getResultList(this.getSession());

        final List<DevelopForm> developFormList = this.getCatalogItemListOrderByCode(DevelopForm.class);

        // аудиторная нагрузка
        final Map<Long, List<EppWeekType>> idForm2aload = SafeMap.get(ArrayList.class);
        // самостоятельная нагрузка
        final Map<Long, List<EppWeekType>> idForm2sload = SafeMap.get(ArrayList.class);

        for (final EppELoadWeekType rel : relList)
        {

            if (rel.getLoadType().isAuditTotal()) {
                idForm2aload.get(rel.getDevelopForm().getId()).add(rel.getWeekType());
            }
            if (rel.getLoadType().isSelfWork()) {
                idForm2sload.get(rel.getDevelopForm().getId()).add(rel.getWeekType());
            }
        }

        final DynamicListDataSource<DevelopForm> dataSource = model.getDataSource();

        dataSource.setTotalSize(developFormList.size());
        dataSource.setCountRow(developFormList.size());
        dataSource.createPage(developFormList);

        for (final ViewWrapper<IEntity> wrapper : ViewWrapper.getPatchedList(dataSource))
        {
            final Long formId = wrapper.getEntity().getId();

            final String auditWeeks = idForm2aload.containsKey(formId) ? this.getWeekTypeString(idForm2aload.get(formId)) : "";
            final String selfWorkWeeks = idForm2sload.containsKey(formId) ? this.getWeekTypeString(idForm2sload.get(formId)) : "";

            wrapper.setViewProperty("audit", auditWeeks);
            wrapper.setViewProperty("selfwork", selfWorkWeeks);
        }
    }

    private String getWeekTypeString(final List<EppWeekType> weekTypeList)
    {
        Collections.sort(weekTypeList, new EntityComparator<>(new EntityOrder(EppWeekType.code().s())));
        return CollectionFormatter.COLLECTION_FORMATTER.format(CollectionUtils.collect(weekTypeList, input ->
            (ITitled) ((EppWeekType) input)::getAbbreviationView
        ));

    }

}
