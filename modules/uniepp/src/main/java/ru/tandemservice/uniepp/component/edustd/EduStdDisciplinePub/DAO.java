/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.edustd.EduStdDisciplinePub;

import java.util.Collections;
import java.util.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.dao.eduStd.IEppEduStdDataDAO;
import ru.tandemservice.uniepp.dao.eduStd.data.IEppStdWrapper;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation;

/**
 * @author nkokorina
 * Created on: 08.09.2010
 */
public class DAO extends UniDao<Model>
{
    @SuppressWarnings("unchecked")
    @Override
    public void prepare(final Model model)
    {
        final Long stdId = this.getNotNull(EppStdRow.class, model.getId()).getOwner().getStateEduStandard().getId();
        final IEppStdWrapper stdWrapper = IEppEduStdDataDAO.instance.get().getEduStdDataMap(Collections.singleton(stdId), true).get(stdId);
        model.setElement(stdWrapper.getRowMap().get(model.getId()));

    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        final MQBuilder builder = new MQBuilder(EppStdRow2SkillRelation.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", EppStdRow2SkillRelation.row().id().s(), model.getId()));
        builder.addOrder("r", EppStdRow2SkillRelation.skill().code().s());

        final List<EppStdRow2SkillRelation> resultList = builder.getResultList(this.getSession());
        final DynamicListDataSource<EppStdRow2SkillRelation> dataSource = model.getDataSource();
        dataSource.setTotalSize(Math.min(3, resultList.size()));
        dataSource.setCountRow(resultList.size());
        dataSource.createPage(resultList);
    }
}
