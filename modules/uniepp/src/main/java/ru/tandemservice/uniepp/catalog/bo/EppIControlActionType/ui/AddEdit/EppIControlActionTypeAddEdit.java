/* $Id:$ */
package ru.tandemservice.uniepp.catalog.bo.EppIControlActionType.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;

/**
 * @author rsizonenko
 * @since 10.11.2015
 */
@Configuration
public class EppIControlActionTypeAddEdit extends BusinessComponentManager {

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS("groupTypeDs", getName(), EppGroupType.defaultSelectDSHandler(getName())))
                .create();
    }

}
