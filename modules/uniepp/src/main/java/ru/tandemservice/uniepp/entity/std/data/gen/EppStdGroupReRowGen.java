package ru.tandemservice.uniepp.entity.std.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.std.data.EppStdGroupReRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdGroupRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись ГОС (группа дисциплин)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStdGroupReRowGen extends EppStdGroupRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.std.data.EppStdGroupReRow";
    public static final String ENTITY_NAME = "eppStdGroupReRow";
    public static final int VERSION_HASH = 1291116806;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppStdGroupReRowGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStdGroupReRowGen> extends EppStdGroupRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStdGroupReRow.class;
        }

        public T newInstance()
        {
            return (T) new EppStdGroupReRow();
        }
    }
    private static final Path<EppStdGroupReRow> _dslPath = new Path<EppStdGroupReRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStdGroupReRow");
    }
            

    public static class Path<E extends EppStdGroupReRow> extends EppStdGroupRow.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EppStdGroupReRow.class;
        }

        public String getEntityName()
        {
            return "eppStdGroupReRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
