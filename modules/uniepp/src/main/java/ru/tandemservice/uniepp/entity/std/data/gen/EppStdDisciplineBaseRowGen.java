package ru.tandemservice.uniepp.entity.std.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineBaseRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись ГОС (базовая запись для дисциплины)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStdDisciplineBaseRowGen extends EppStdRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineBaseRow";
    public static final String ENTITY_NAME = "eppStdDisciplineBaseRow";
    public static final int VERSION_HASH = 145915672;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";

    private EppRegistryStructure _type;     // Структура реестра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Структура реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryStructure getType()
    {
        return _type;
    }

    /**
     * @param type Структура реестра. Свойство не может быть null.
     */
    public void setType(EppRegistryStructure type)
    {
        dirty(_type, type);
        _type = type;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppStdDisciplineBaseRowGen)
        {
            setType(((EppStdDisciplineBaseRow)another).getType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStdDisciplineBaseRowGen> extends EppStdRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStdDisciplineBaseRow.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppStdDisciplineBaseRow is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                    return obj.getType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "type":
                    obj.setType((EppRegistryStructure) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                    return EppRegistryStructure.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStdDisciplineBaseRow> _dslPath = new Path<EppStdDisciplineBaseRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStdDisciplineBaseRow");
    }
            

    /**
     * @return Структура реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineBaseRow#getType()
     */
    public static EppRegistryStructure.Path<EppRegistryStructure> type()
    {
        return _dslPath.type();
    }

    public static class Path<E extends EppStdDisciplineBaseRow> extends EppStdRow.Path<E>
    {
        private EppRegistryStructure.Path<EppRegistryStructure> _type;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Структура реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineBaseRow#getType()
     */
        public EppRegistryStructure.Path<EppRegistryStructure> type()
        {
            if(_type == null )
                _type = new EppRegistryStructure.Path<EppRegistryStructure>(L_TYPE, this);
            return _type;
        }

        public Class getEntityClass()
        {
            return EppStdDisciplineBaseRow.class;
        }

        public String getEntityName()
        {
            return "eppStdDisciplineBaseRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
