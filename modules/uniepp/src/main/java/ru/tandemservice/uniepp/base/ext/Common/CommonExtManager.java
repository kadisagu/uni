/* $Id:$ */
package ru.tandemservice.uniepp.base.ext.Common;

import org.apache.commons.collections15.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.ModuleStatusReportBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.util.DuplicationMergeResolver;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IDuplicationMergeResolver;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.ElementDuplicationMerge.EppRegistryElementDuplicationMerge;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/11/14
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
            .add("uniepp", () -> {

                IUniBaseDao dao = IUniBaseDao.instance.get();
                List<String> result = new ArrayList<>();
                String alias = "a";

                List<Object[]> eduPlanByPrKindsList = dao.getList(new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, alias)
                        .where(isNull(property(alias, EppStudent2EduPlanVersion.removalDate())))
                        .column(property(alias, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().programKind().title()))
                        .column(DQLFunctions.count(property(alias, EppStudent2EduPlanVersion.id())))
                        .group(property(alias, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().programKind().title()))
                        .order(property(alias, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().programKind().title())));

                final Collection<String> eduPlan2PrKind = CollectionUtils.collect(eduPlanByPrKindsList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);

                final DQLSelectBuilder eduPlansWithRupsBuilder = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, alias)
                        .joinEntity(alias, DQLJoinType.inner, EppStudent2WorkPlan.class, "b", eq(property("b", EppStudent2WorkPlan.studentEduPlanVersion().id()), property(alias, EppStudent2EduPlanVersion.id())))
                        .joinEntity(alias, DQLJoinType.inner, EppStudent2WorkPlan.class, "c", eq(property("c", EppStudent2WorkPlan.studentEduPlanVersion().id()), property(alias, EppStudent2EduPlanVersion.id())))
                        .where(isNull(property(alias, EppStudent2EduPlanVersion.removalDate())))
                        .where(eq(property("b", EppStudent2WorkPlan.cachedGridTerm().id()), property("c", EppStudent2WorkPlan.cachedGridTerm().id())))
                        .where(not(eq(property("b", EppStudent2WorkPlan.cachedEppYear().id()), property("c", EppStudent2WorkPlan.cachedEppYear().id()))))
                        .where(not(eq(property("b", EppStudent2WorkPlan.id()), property("c", EppStudent2WorkPlan.id()))))
                        .distinct()
                        .column(property(alias, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().programKind().title()), "title")
                        .column(DQLFunctions.count(property(alias, EppStudent2EduPlanVersion.id())), "epv")
                        .group(property(alias, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().programKind().title()))
                        .order(property(alias, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().programKind().title()));

                final List<Object[]> eduPlanWith2PlusRupsList = dao.getList(eduPlansWithRupsBuilder);
                final Collection<String> eduPlanWith2PlusRups = CollectionUtils.collect(eduPlanWith2PlusRupsList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);


                final List<Object[]> eduPlanBasesWithMinYearList = dao.getList(new DQLSelectBuilder()
                    .fromDataSource(new DQLSelectBuilder()
                        .fromEntity(EppWorkPlan.class, alias)
                        .where(notExists(new DQLSelectBuilder()
                            .fromEntity(EppWorkPlan.class, "y")
                            .where(eq(property(alias, EppWorkPlan.parent().eduPlanVersion().eduPlan()), property("y", EppWorkPlan.parent().eduPlanVersion().eduPlan())))
                            .where(lt(property(alias, EppWorkPlan.year().educationYear().intValue()), property("y", EppWorkPlan.year().educationYear().intValue())))
                            .buildQuery()
                        ))
                        .where(eq(property(alias, EppWorkPlan.cachedGridTerm().term().intValue()), value(1)))
                        .column(property(alias, EppWorkPlan.parent().eduPlanVersion().eduPlan().programKind().title()), "title")
                        .column(property(alias, EppWorkPlan.year().educationYear().intValue()), "year")
                        .column(property(alias, EppWorkPlan.parent().eduPlanVersion().eduPlan().id()), "epbid")
                        .buildQuery(), "e"
                    )
                    .column(property("e.title"))
                    .column(property("e.year"))
                    .column(DQLFunctions.count(property("e.epbid")))
                    .group(property("e.title"))
                    .group(property("e.year")));

                final Collection<String> eduPlanBasesWithMinYear = CollectionUtils.collect(eduPlanBasesWithMinYearList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);

                final List<Object[]> groupOUCountByOwnersList = dao.getList(new DQLSelectBuilder()
                    .fromEntity(EppStudentWorkPlanElement.class, alias)
                    .where(isNull(property(alias, EppStudentWorkPlanElement.removalDate())))
                    .column(property(alias, EppStudentWorkPlanElement.registryElementPart().registryElement().owner().title()))
                    .group(property(alias, EppStudentWorkPlanElement.registryElementPart().registryElement().owner().title()))
                    .column(DQLFunctions.count(property(alias, EppStudent2EduPlanVersion.student().educationOrgUnit().groupOrgUnit())))
                    .order(DQLFunctions.count(property(alias, EppStudent2EduPlanVersion.student().educationOrgUnit().groupOrgUnit())), OrderDirection.desc)
                    .top(3));
                final Collection<String> groupOUCountByOwners = CollectionUtils.collect(groupOUCountByOwnersList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);


                final List<Object[]> actualMSRPList = dao.getList(new DQLSelectBuilder().fromDataSource(new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, alias)
                            .where(isNull(property(alias, EppStudentWorkPlanElement.removalDate())))
                            .column(property(alias, EppStudentWorkPlanElement.year().educationYear().intValue()), "year")
                            .column(property(alias, EppStudentWorkPlanElement.student().educationOrgUnit().groupOrgUnit().title()), "groupou")
                            .column(property(alias, EppStudentWorkPlanElement.studentEduPlanVersion().eduPlanVersion().eduPlan().programKind().title()), "epkind")
                            .column(property(alias, EppStudentWorkPlanElement.id()), "id").buildQuery(), "e")
                        .column(property("e.year"))
                        .column(property("e.groupou"))
                        .column(property("e.epkind"))
                        .distinct()
                        .column(DQLFunctions.count(property("e.id")))
                        .group(property("e.year"))
                        .group(property("e.groupou"))
                        .group(property("e.epkind")));
                final List<Object[]> nonActualMSRPList = dao.getList(new DQLSelectBuilder().fromDataSource(new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, alias)
                            .where(isNotNull(property(alias, EppStudentWorkPlanElement.removalDate())))
                            .column(property(alias, EppStudentWorkPlanElement.year().educationYear().intValue()), "year")
                            .column(property(alias, EppStudentWorkPlanElement.student().educationOrgUnit().groupOrgUnit().title()), "groupou")
                            .column(property(alias, EppStudentWorkPlanElement.studentEduPlanVersion().eduPlanVersion().eduPlan().programKind().title()), "epkind")
                            .column(property(alias, EppStudentWorkPlanElement.id()), "id").buildQuery(), "e")
                        .column(property("e.year"))
                        .column(property("e.groupou"))
                        .column(property("e.epkind"))
                        .distinct()
                        .column(DQLFunctions.count(property("e.id")))
                        .group(property("e.year"))
                        .group(property("e.groupou"))
                        .group(property("e.epkind")));

                final Collection<String> actualMSRP = CollectionUtils.collect(actualMSRPList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);
                final Collection<String> nonActualMSRP = CollectionUtils.collect(nonActualMSRPList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER);



                result.add("Число учебных планов по видам ОП (прикрепленных к студентам):");
                result.addAll(eduPlan2PrKind);
                result.add("из них УП, для которых есть РУП на один и тот же курс и разные года");
                result.addAll(eduPlanWith2PlusRups);
                result.add("Число учебных планов по видам ОП и наименьшему году РУП первого курса:");
                result.addAll(eduPlanBasesWithMinYear);
                result.add("Число читающих (в УП): " + dao.getCount(new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, alias)
                        .column(property(alias, EppEpvRegistryRow.registryElementOwner().id())).distinct()));
                result.add("Число читающих (в реестре): " + dao.getCount(new DQLSelectBuilder().fromEntity(EppRegistryElement.class, alias)
                        .column(property(alias, EppRegistryElement.owner().id())).distinct()));

                result.add("Число читающих (по актуальным МСРП): " + dao.getCount(new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, alias)
                        .where(isNull(property(alias, EppStudentWorkPlanElement.removalDate())))
                        .column(property(alias, EppStudentWorkPlanElement.registryElementPart().registryElement().owner().id())).distinct()));
                result.add("Число деканатов (по актуальным МСРП): " + dao.getCount(new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, alias)
                        .where(isNull(property(alias, EppStudentWorkPlanElement.removalDate())))
                        .column(property(alias, EppStudentWorkPlanElement.student().educationOrgUnit().groupOrgUnit().id())).distinct()));
                result.add("Число диспетчерских (по актуальным МСРП): " + dao.getCount(new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, alias)
                        .where(isNull(property(alias, EppStudentWorkPlanElement.removalDate())))
                        .column(property(alias, EppStudentWorkPlanElement.student().educationOrgUnit().operationOrgUnit().id())).distinct()));
                result.add("Число деканатов для читающего (по актуальным МСРП):");
                result.addAll(groupOUCountByOwners);

                // todo число индивидуальных руп (там ???)

                result.add("Число актуальных МСРП:");
                result.addAll(actualMSRP);
                result.add("Число неактуальных МСРП:");
                result.addAll(nonActualMSRP);


                return result;
            })
            .create();

    }

    @Bean
    public ItemListExtension<IDuplicationMergeResolver> duplicationMergeExtPoint()
    {
        return itemListExtension(_commonManager.duplicationMergeExtPoint())
                .add("eppRegistryElement", new DuplicationMergeResolver("Элементы реестра", EppRegistryElementDuplicationMerge.class.getSimpleName()))
                .create();
    }
}
