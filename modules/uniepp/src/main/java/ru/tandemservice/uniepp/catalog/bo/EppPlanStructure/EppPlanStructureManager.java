package ru.tandemservice.uniepp.catalog.bo.EppPlanStructure;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author avedernikov
 * @since 27.08.2015
 */

@Configuration
public class EppPlanStructureManager extends BusinessObjectManager
{
	public static EppPlanStructureManager instance()
	{
		return instance(EppPlanStructureManager.class);
	}
}