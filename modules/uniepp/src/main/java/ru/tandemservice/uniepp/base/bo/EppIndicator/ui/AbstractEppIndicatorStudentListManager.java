package ru.tandemservice.uniepp.base.bo.EppIndicator.ui;

import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.datasource.column.IPublisherColumnBuilder;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vdanilov
 */
public abstract class AbstractEppIndicatorStudentListManager extends AbstractUniStudentList
{

    @Override
    protected List<ColumnBase> prepareColumnList() {
        List<ColumnBase> columnList = new ArrayList<>();
        IMergeRowIdResolver merger = newMergeRowIdResolver();
        columnList.add(newFioColumnBuilder(merger).create());
        columnList.add(newBookNumberColumnBuilder().create());
        columnList.add(newEntranceYearColumnBuilder().create());
        columnList.add(newStatusColumnBuilder().create());
        columnList.add(newCourseColumnBuilder().create());
        columnList.add(newGroupColumnBuilder().create());
        columnList.add(newCategoryColumnBuilder().create());

        columnList.add(newFormativeOrgUnitColumnBuilder().create());
        columnList.add(newTerritorialOrgUnitColumnBuilder().create());
        columnList.add(newProductiveOrgUnitColumnBuilder().create());
        columnList.add(newSpecialityColumnBuilder().create());
        columnList.add(newSpecializationColumnBuilder().create());

        return columnList;
    }

    @Override
    protected IMergeRowIdResolver newMergeRowIdResolver() {
        return null;
    }

    @Override
    protected IPublisherColumnBuilder newFioColumnBuilder(IMergeRowIdResolver merger) {
        final IPublisherLinkResolver resolver = new DefaultPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) {
                return new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId())
                .add("selectedStudentTab","studentTab")
                .add("selectedDataTab","studentEppTrajectoryTab");
            }
        };
        return publisherColumn(FIO_COLUMN, Student.person().fullFio()).formatter(RawFormatter.INSTANCE).publisherLinkResolver(resolver).order().required(true);
    }

}
