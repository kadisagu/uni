/* $Id$ */
package ru.tandemservice.uniepp.dao.registry.data;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppModuleStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;

/**
 * Враппер для модуля части дисциплины реестра на основе строки УПв.
 * Большая часть методов не поддерживаетс за ненадобностью.
 *
 * Условно не мутабельный.
 *
 * @author Nikolay Fedorovskih
 * @since 12.05.2015
 */
public class EppEpvRegElPartModuleWrapper implements IEppRegElPartModuleWrapper
{
    private final EppEpvRegElPartWrapper parent;
    private final int number;

    public EppEpvRegElPartModuleWrapper(EppEpvRegElPartWrapper parent, int number)
    {
        this.parent = parent;
        this.number = number;
    }

    @Override
    public int getActionSize(String actionFullCode)
    {
        return this.parent.getActionSize(actionFullCode);
    }

    @Override
    public double getLoadAsDouble(String loadFullCode)
    {
        return this.parent.getLoadAsDouble(loadFullCode);
    }

    @Override
    public boolean shared()
    {
        return false;
    }

    @Override
    public EppModuleStructure getModuleParent()
    {
        return null; // null == EppModuleStructureCodes.BASE_MODULES
    }

    @Override
    public OrgUnit getModuleOwner()
    {
        return this.parent.getParentWrapper().getOwner();
    }

    @Override
    public int getNumber()
    {
        return this.number;
    }

    @Override public void setId(Long id) { throw new UnsupportedOperationException(); }
    @Override public Object getProperty(Object propertyPath) { throw new UnsupportedOperationException(); }
    @Override public void setProperty(String propertyPath, Object propertyValue) { throw new UnsupportedOperationException(); }
    @Override public EppRegistryElementPartModule getItem() { throw new UnsupportedOperationException(); }
    @Override public Long getId() { throw new UnsupportedOperationException(); }
    @Override public String getTitle() { throw new UnsupportedOperationException(); }
}