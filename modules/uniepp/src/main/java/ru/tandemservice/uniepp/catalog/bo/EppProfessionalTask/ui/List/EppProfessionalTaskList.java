/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask.EppProfessionalTaskManager;

/**
 * @author Igor Belanov
 * @since 23.03.2017
 */
@Configuration
public class EppProfessionalTaskList extends BusinessComponentManager
{
    public static final String PROGRAM_KIND_DS = "programKindDS";
    public static final String PROGRAM_SUBJECT_INDEX_DS = "programSubjectIndexDS";
    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(PROGRAM_KIND_DS, EppProfessionalTaskManager.instance().programKindDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_INDEX_DS, EppProfessionalTaskManager.instance().programSubjectIndexDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_DS, EppProfessionalTaskManager.instance().programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .create();
    }
}
