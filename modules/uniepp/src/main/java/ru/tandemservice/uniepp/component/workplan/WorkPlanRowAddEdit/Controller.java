package ru.tandemservice.uniepp.component.workplan.WorkPlanRowAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author amakarova
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = component.getModel();
        final ErrorCollector errors = component.getUserContext().getErrorCollector();

        this.getDao().validate(model, errors);
        if (!errors.hasErrors())
        {
            this.getDao().update(this.getModel(component));
            this.deactivate(component);
        }
    }
}
