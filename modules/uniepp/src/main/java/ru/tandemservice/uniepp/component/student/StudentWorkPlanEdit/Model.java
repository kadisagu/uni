package ru.tandemservice.uniepp.component.student.StudentWorkPlanEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.component.workplan.WorkPlanData.WorkPlanDataBaseModel;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;


@Input( { @Bind(key ="id", binding = "holder.id", required = true) })
public class Model extends WorkPlanDataBaseModel {

    private final EntityHolder<EppStudent2WorkPlan> holder = new EntityHolder<>();
    public EntityHolder<EppStudent2WorkPlan> getHolder() { return this.holder; }
    public Long getId() { return this.getHolder().getId(); }

    private CommonPostfixPermissionModel sec;
    public CommonPostfixPermissionModel getSec() { return this.sec; }
    public void setSec(final CommonPostfixPermissionModel sec) { this.sec = sec; }

    public Student getStudent() { return this.getHolder().getValue().getStudentEduPlanVersion().getStudent(); }
    public EppWorkPlanBase getWorkplan() { return this.getHolder().getValue().getWorkPlan(); }

    public boolean isNotCustom() {
        return getWorkplan().getWorkPlan().getCustomEduPlan() == null;
    }

    @Override protected Long getWorkPlanId() { return this.getWorkplan().getId(); }
    @Override public boolean isEditable() { return ! this.getWorkplan().getState().isReadOnlyState(); }
    @Override protected String getPermissionKeyEdit() { return (this.isEditable() ? this.getSec().getPermission("editContent") : null); }


}
