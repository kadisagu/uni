/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.util;


import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;

import java.util.List;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class EppFilterUtil
{
    public static void prepareYearEducationProcessFilter(final IYearEducationProcessSelectModel model, final Session session)
    {
        final MQBuilder builder = new MQBuilder(EppYearEducationProcess.ENTITY_CLASS, "e");
        builder.addJoinFetch("e", EppYearEducationProcess.L_EDUCATION_YEAR, "y");
        builder.addOrder("e", EppYearEducationProcess.educationYear().intValue().s());
        final List<EppYearEducationProcess> list = builder.getResultList(session);
        model.setYearEducationProcessList(list);
    }

    public static Integer getSelectedYear(final Object selectedYear) {
        if (selectedYear instanceof Integer) {
            return ((Integer)selectedYear);
        } else if (selectedYear instanceof String) {
            return Integer.valueOf((String)selectedYear);
        }
        return UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
            final EducationYear currentYear = UniDaoFacade.getCoreDao().get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE);
            return (null == currentYear ? null : currentYear.getIntValue());
        });
    }
}