package ru.tandemservice.uniepp.entity.catalog;

import com.google.common.collect.ImmutableList;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniepp.entity.catalog.gen.EppWeekGen;

import java.util.Collection;
import java.util.List;

/**
 * Учебная неделя
 */
public class EppWeek extends EppWeekGen implements IDynamicCatalogItem
{
    private static final List<String> HIDDEN_PROPERTIES = ImmutableList.of(P_MONTH);

    public static final int YEAR_WEEK_COUNT = 52;

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public Collection<String> getHiddenFields() {
                return HIDDEN_PROPERTIES;
            }
        };
    }
}