package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид профессиональной деятельности
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppProfActivityTypeGen extends EntityBase
 implements INaturalIdentifiable<EppProfActivityTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppProfActivityType";
    public static final String ENTITY_NAME = "eppProfActivityType";
    public static final int VERSION_HASH = 106650791;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String L_PROGRAM_SUBJECT = "programSubject";
    public static final String P_IN_DEPTH_STUDY = "inDepthStudy";
    public static final String P_FROM_I_M_C_A = "fromIMCA";
    public static final String P_REMOVAL_DATE = "removalDate";
    public static final String P_PRIORITY = "priority";

    private String _code;     // Системный код
    private String _title;     // Название
    private EduProgramSubject _programSubject;     // Направление подготовки
    private Boolean _inDepthStudy;     // Углубленное изучение
    private boolean _fromIMCA;     // Импортировано из ИМЦА
    private Date _removalDate;     // Дата утраты актуальности
    private int _priority;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * Вид. проф. деятельности существует в рамках ФГОС и его направления.
     *
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление подготовки. Свойство не может быть null.
     */
    public void setProgramSubject(EduProgramSubject programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * Для СПО виды проф. деятельности подразделяются на базовое и углубленное обучение.
     *
     * @return Углубленное изучение.
     */
    public Boolean getInDepthStudy()
    {
        return _inDepthStudy;
    }

    /**
     * @param inDepthStudy Углубленное изучение.
     */
    public void setInDepthStudy(Boolean inDepthStudy)
    {
        dirty(_inDepthStudy, inDepthStudy);
        _inDepthStudy = inDepthStudy;
    }

    /**
     * Если вид создан в результате системного действия, то устанавливается значение "true".
     * Если создан пользователем, то значение "false".
     *
     * @return Импортировано из ИМЦА. Свойство не может быть null.
     */
    @NotNull
    public boolean isFromIMCA()
    {
        return _fromIMCA;
    }

    /**
     * @param fromIMCA Импортировано из ИМЦА. Свойство не может быть null.
     */
    public void setFromIMCA(boolean fromIMCA)
    {
        dirty(_fromIMCA, fromIMCA);
        _fromIMCA = fromIMCA;
    }

    /**
     * Если в результате системного действия ранее импортированный вид не был найден, то сохраняется текущая дата.
     *
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    /**
     * Приоритет вывода видов проф. деятельности в списках, при печати.
     *
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppProfActivityTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EppProfActivityType)another).getCode());
            }
            setTitle(((EppProfActivityType)another).getTitle());
            setProgramSubject(((EppProfActivityType)another).getProgramSubject());
            setInDepthStudy(((EppProfActivityType)another).getInDepthStudy());
            setFromIMCA(((EppProfActivityType)another).isFromIMCA());
            setRemovalDate(((EppProfActivityType)another).getRemovalDate());
            setPriority(((EppProfActivityType)another).getPriority());
        }
    }

    public INaturalId<EppProfActivityTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EppProfActivityTypeGen>
    {
        private static final String PROXY_NAME = "EppProfActivityTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppProfActivityTypeGen.NaturalId) ) return false;

            EppProfActivityTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppProfActivityTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppProfActivityType.class;
        }

        public T newInstance()
        {
            return (T) new EppProfActivityType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "programSubject":
                    return obj.getProgramSubject();
                case "inDepthStudy":
                    return obj.getInDepthStudy();
                case "fromIMCA":
                    return obj.isFromIMCA();
                case "removalDate":
                    return obj.getRemovalDate();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((EduProgramSubject) value);
                    return;
                case "inDepthStudy":
                    obj.setInDepthStudy((Boolean) value);
                    return;
                case "fromIMCA":
                    obj.setFromIMCA((Boolean) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "programSubject":
                        return true;
                case "inDepthStudy":
                        return true;
                case "fromIMCA":
                        return true;
                case "removalDate":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "programSubject":
                    return true;
                case "inDepthStudy":
                    return true;
                case "fromIMCA":
                    return true;
                case "removalDate":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "programSubject":
                    return EduProgramSubject.class;
                case "inDepthStudy":
                    return Boolean.class;
                case "fromIMCA":
                    return Boolean.class;
                case "removalDate":
                    return Date.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppProfActivityType> _dslPath = new Path<EppProfActivityType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppProfActivityType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * Вид. проф. деятельности существует в рамках ФГОС и его направления.
     *
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#getProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * Для СПО виды проф. деятельности подразделяются на базовое и углубленное обучение.
     *
     * @return Углубленное изучение.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#getInDepthStudy()
     */
    public static PropertyPath<Boolean> inDepthStudy()
    {
        return _dslPath.inDepthStudy();
    }

    /**
     * Если вид создан в результате системного действия, то устанавливается значение "true".
     * Если создан пользователем, то значение "false".
     *
     * @return Импортировано из ИМЦА. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#isFromIMCA()
     */
    public static PropertyPath<Boolean> fromIMCA()
    {
        return _dslPath.fromIMCA();
    }

    /**
     * Если в результате системного действия ранее импортированный вид не был найден, то сохраняется текущая дата.
     *
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    /**
     * Приоритет вывода видов проф. деятельности в списках, при печати.
     *
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends EppProfActivityType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private EduProgramSubject.Path<EduProgramSubject> _programSubject;
        private PropertyPath<Boolean> _inDepthStudy;
        private PropertyPath<Boolean> _fromIMCA;
        private PropertyPath<Date> _removalDate;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EppProfActivityTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppProfActivityTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * Вид. проф. деятельности существует в рамках ФГОС и его направления.
     *
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#getProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new EduProgramSubject.Path<EduProgramSubject>(L_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * Для СПО виды проф. деятельности подразделяются на базовое и углубленное обучение.
     *
     * @return Углубленное изучение.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#getInDepthStudy()
     */
        public PropertyPath<Boolean> inDepthStudy()
        {
            if(_inDepthStudy == null )
                _inDepthStudy = new PropertyPath<Boolean>(EppProfActivityTypeGen.P_IN_DEPTH_STUDY, this);
            return _inDepthStudy;
        }

    /**
     * Если вид создан в результате системного действия, то устанавливается значение "true".
     * Если создан пользователем, то значение "false".
     *
     * @return Импортировано из ИМЦА. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#isFromIMCA()
     */
        public PropertyPath<Boolean> fromIMCA()
        {
            if(_fromIMCA == null )
                _fromIMCA = new PropertyPath<Boolean>(EppProfActivityTypeGen.P_FROM_I_M_C_A, this);
            return _fromIMCA;
        }

    /**
     * Если в результате системного действия ранее импортированный вид не был найден, то сохраняется текущая дата.
     *
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(EppProfActivityTypeGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

    /**
     * Приоритет вывода видов проф. деятельности в списках, при печати.
     *
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppProfActivityType#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EppProfActivityTypeGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return EppProfActivityType.class;
        }

        public String getEntityName()
        {
            return "eppProfActivityType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
