package ru.tandemservice.uniepp.entity.student.slot.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeALoad;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * МСРП по виду аудиторной нагрузки (МСРП-АН)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStudentWpeALoadGen extends EppStudentWpePart
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeALoad";
    public static final String ENTITY_NAME = "eppStudentWpeALoad";
    public static final int VERSION_HASH = -1584957054;
    private static IEntityMeta ENTITY_META;

    public static final String P_REGISTRY_ELEMENT_TITLE = "registryElementTitle";


    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppStudentWpeALoadGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStudentWpeALoadGen> extends EppStudentWpePart.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStudentWpeALoad.class;
        }

        public T newInstance()
        {
            return (T) new EppStudentWpeALoad();
        }
    }
    private static final Path<EppStudentWpeALoad> _dslPath = new Path<EppStudentWpeALoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStudentWpeALoad");
    }
            

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeALoad#getRegistryElementTitle()
     */
    public static SupportedPropertyPath<String> registryElementTitle()
    {
        return _dslPath.registryElementTitle();
    }

    public static class Path<E extends EppStudentWpeALoad> extends EppStudentWpePart.Path<E>
    {
        private SupportedPropertyPath<String> _registryElementTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeALoad#getRegistryElementTitle()
     */
        public SupportedPropertyPath<String> registryElementTitle()
        {
            if(_registryElementTitle == null )
                _registryElementTitle = new SupportedPropertyPath<String>(EppStudentWpeALoadGen.P_REGISTRY_ELEMENT_TITLE, this);
            return _registryElementTitle;
        }

        public Class getEntityClass()
        {
            return EppStudentWpeALoad.class;
        }

        public String getEntityName()
        {
            return "eppStudentWpeALoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getRegistryElementTitle();
}
