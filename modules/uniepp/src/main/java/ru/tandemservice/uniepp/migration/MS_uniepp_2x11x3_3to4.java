package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_2x11x3_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		ISQLTranslator translator = tool.getDialect().getSQLTranslator();

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppSkill

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_c_skill_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eppskill"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200)).setNullable(false), 
				new DBColumn("programsubject_id", DBType.LONG).setNullable(false), 
				new DBColumn("skillgroup_id", DBType.LONG).setNullable(false), 
				new DBColumn("profactivitytype_id", DBType.LONG), 
				new DBColumn("skillcode_p", DBType.createVarchar(255)), 
				new DBColumn("programspecializationval_id", DBType.LONG), 
				new DBColumn("programspecialization_p", DBType.createVarchar(255)), 
				new DBColumn("indepthstudy_p", DBType.BOOLEAN), 
				new DBColumn("fromimca_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("removaldate_p", DBType.DATE), 
				new DBColumn("priority_p", DBType.INTEGER).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppSkill");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppSkillDevelopLevel

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_c_skill_dev_level_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eppskilldeveloplevel"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200)).setNullable(false), 
				new DBColumn("shorttitle_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("description_p", DBType.createVarchar(255)), 
				new DBColumn("priority_p", DBType.INTEGER).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppSkillDevelopLevel");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppSkillGroup

		// создано обязательное свойство priority
		{
			// создать колонку
			tool.createColumn("epp_c_skill_group_t", new DBColumn("priority_p", DBType.INTEGER));

			// нужно проставить приоритет в соответствии с названием группы компетенций
			SQLSelectQuery selectSGPriority = new SQLSelectQuery();
			selectSGPriority.from(SQLFrom.table("epp_c_skill_group_t"));
			selectSGPriority.column("id");
			selectSGPriority.column("row_number() over (order by title_p)", "newPriority");

			SQLUpdateQuery updateSGPriority = new SQLUpdateQuery("epp_c_skill_group_t", "sg");
			updateSGPriority.set("priority_p", "t.newPriority");
			updateSGPriority.from(SQLFrom.select(translator.toSql(selectSGPriority), "t"));
			updateSGPriority.where("sg.id=t.id");
			tool.executeUpdate(translator.toSql(updateSGPriority));

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_c_skill_group_t", "priority_p", false);
		}
    }
}
