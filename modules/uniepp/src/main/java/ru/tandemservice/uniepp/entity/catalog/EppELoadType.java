package ru.tandemservice.uniepp.entity.catalog;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppELoadTypeGen;

import java.util.List;

/**
 * Вид учебной нагрузки
 */
public class EppELoadType extends EppELoadTypeGen
{
    @Override public void setCode(final String code) {
        Preconditions.checkState(EppELoadTypeCodes.CODES.contains(code));
        super.setCode(code);
    }

    public static final String CATALOG_CODE = EppELoadTypeGen.ENTITY_NAME;
    @Override public String getCatalogCode() { return EppELoadType.CATALOG_CODE; }

    public static final String TYPE_TOTAL_AUDIT = EppELoadTypeCodes.TYPE_TOTAL_AUDIT;
    public static final String TYPE_TOTAL_SELFWORK = EppELoadTypeCodes.TYPE_TOTAL_SELFWORK;
    public static final String TYPE_TOTAL_SELFWORK_WO_CONTROL = EppELoadTypeCodes.TYPE_TOTAL_SELFWORK + "_wo_control";

    public static final String FULL_CODE_AUDIT = EppLoadType.getFullCode(EppELoadType.CATALOG_CODE, EppELoadType.TYPE_TOTAL_AUDIT);
    public static final String FULL_CODE_SELFWORK = EppLoadType.getFullCode(EppELoadType.CATALOG_CODE, EppELoadType.TYPE_TOTAL_SELFWORK);
    public static final String FULL_CODE_SELFWORK_WO_CONTROL = EppLoadType.getFullCode(EppELoadType.CATALOG_CODE, EppELoadType.TYPE_TOTAL_SELFWORK_WO_CONTROL);

    public static final List<String> FULL_CODES;
    static {
        final ImmutableList.Builder<String> builder = ImmutableList.builder();
        EppELoadTypeCodes.CODES.forEach(code -> builder.add(EppLoadType.getFullCode(EppELoadType.CATALOG_CODE, code)));
        FULL_CODES = builder.build();
    }

    public boolean isAuditTotal() { return EppELoadType.TYPE_TOTAL_AUDIT.equals(this.getCode()); }
    public boolean isSelfWork() { return EppELoadType.TYPE_TOTAL_SELFWORK.equals(this.getCode()); }
}