package ru.tandemservice.uniepp.component.settings.TutorOrgUnit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.setSettings(component.getSettings());
        this.getDao().prepare(model);
        this.prepareListDataSource(component);
    }

    private void prepareListDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        final DynamicListDataSource<IEntity> dataSource = UniBaseUtils.createDataSource(component, this.getDao());
        dataSource.addColumn(new SimpleColumn("Название", OrgUnit.P_TYPE_TITLE).setTreeColumn(true).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ToggleColumn("Может быть читающим", "isTutor").setListener("onClickSetTutor").setOrderable(false).setDisabledProperty("isParent"));
        dataSource.addColumn(new ToggleColumn("Отображать УП и РУП", "isVisible").setListener("onClickSetVisible").setOrderable(false).setDisabledProperty("isParent"));
        model.setDataSource(dataSource);
    }

    public void updateListDataSource(final IBusinessComponent component)
    {
        this.getDao().prepareListDataSource(this.getModel(component));
    }

    public void onClickSetTutor(final IBusinessComponent context)
    {
        this.getDao().doSwitchTutorStatus((Long)context.getListenerParameter());
    }

    public void onClickSetVisible(final IBusinessComponent context)
    {
        this.getDao().doSwitchVisiblePlan((Long)context.getListenerParameter());
    }

    public void onClickSearch(final IBusinessComponent context)
    {
        context.saveSettings();
        this.getModel(context).getDataSource().refresh();
    }

    public void onClickClear(final IBusinessComponent context)
    {
        final Model model = this.getModel(context);
        model.getSettings().clear();
        this.getDao().prepare(model);
        this.onClickSearch(context);
    }
}
