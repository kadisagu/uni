package ru.tandemservice.uniepp.entity.workplan;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.base.bo.EntityEventLog.ui.View.IExtendedEntityEventLogOwner;

import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanBaseGen;

/**
 * Рабочий план (базовый)
 */
public abstract class EppWorkPlanBase extends EppWorkPlanBaseGen implements ITitled, IEppStateObject, IHierarchyItem, ISecLocalEntityOwner, IExtendedEntityEventLogOwner
{
    private Collection<IEntity> _cache_sec_local_entities = null;
    @Override public Collection<IEntity> getSecLocalEntities() {
        if (null != this._cache_sec_local_entities) { return this._cache_sec_local_entities; }
        return (this._cache_sec_local_entities = IEppWorkPlanDAO.instance.get().getSecLocalEntities(this));
    }

    // сортировка РУП по семестрам
    public static final Comparator<EppWorkPlanBase> BY_TERM_COMPARATOR = (o1, o2) -> {
        final int i1 = o1.getTerm().getIntValue();
        final int i2 = o2.getTerm().getIntValue();
        return (i1-i2);
    };

    // сортировка РУП по годам (и по семестрам в рамках года)
    public static final Comparator<EppWorkPlanBase> BY_YEAR_COMPARATOR = (o1, o2) -> {
        final int i1 = o1.getYear().getEducationYear().getIntValue();
        final int i2 = o2.getYear().getEducationYear().getIntValue();
        final int i0 = (i1-i2);
        if (0 != i0) { return i0; }

        return EppWorkPlanBase.BY_TERM_COMPARATOR.compare(o1, o2);
    };


    public static final String P_FULL_NUMBER = "fullNumber";

    /** @return work-plan */
    public abstract EppWorkPlan getWorkPlan();

    /** @return номер полный */
    public abstract String getFullNumber();

    /** @return год */
    public abstract EppYearEducationProcess getYear();

    /** @return семестр */
    public abstract Term getTerm();

    /** @return семестр (сетки) */
    public abstract DevelopGridTerm getGridTerm();

    /** @return блок версии УП на основании которого создан РУП */
    public abstract EppEduPlanVersionBlock getBlock();

    public EppEduPlanVersion getEduPlanVersion() { return this.getBlock().getEduPlanVersion(); }
    public EppEduPlan getEduPlan() { return this.getEduPlanVersion().getEduPlan(); }

    public String getTitleBase() {
        final String termYearString = " Семестр " + this.getTerm().getTitle() + " " + this.getYear().getEducationYear().getTitle();
        final String titlePostfix = StringUtils.trimToNull(this.getTitlePostfix());
        if (null == titlePostfix) {
            return "№ " + this.getFullNumber() + termYearString;
        }
        return "№ " + this.getFullNumber() + termYearString  + " " + titlePostfix;
    }

    @Override
    @EntityDSLSupport
    public abstract String getTitle();

    @Override
    @EntityDSLSupport
    public String getShortTitle() {
        final int year = this.getYear().getEducationYear().getIntValue() % 100;
        final String yearString = StringUtils.leftPad(String.valueOf(year), 2, '0')+"/"+StringUtils.leftPad(String.valueOf(1+year), 2, '0');
        final String titlePostfix = StringUtils.trimToNull(this.getTitlePostfix());
        if (null == titlePostfix) {
            return "№ " + this.getFullNumber() + " " + yearString;
        }
        return "№ " + this.getFullNumber() + " " + yearString + " " + titlePostfix;
    }

    @Override
    @EntityDSLSupport
    public String getEducationElementTitle() {
        return this.getShortTitle();
    }

    @Override
    @EntityDSLSupport
    public String getPartFullTitle() {
        final DevelopGridTerm gt = this.getGridTerm();
        return this.getYear().getTitle() + " / " + gt.getTermNumber() + " ("+gt.getPart().getTitle()+", "+gt.getCourseNumber()+" курс)";
    }

    private static final List<String> loggingEntityNames = Arrays.asList(
        EppWorkPlanBase.ENTITY_CLASS,
        EppWorkPlanPart.ENTITY_CLASS,
        EppWorkPlanRow.ENTITY_CLASS,
        EppWorkPlanRowPartLoad.ENTITY_CLASS
    );

    @Override
    public List<String> getEntityClassNames()
    {
        return loggingEntityNames;
    }

    @Override
    public String getListSettingsKey()
    {
        return "logView.eppWorkPlan.";
    }
}