package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionWorkPlanList;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanGen;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{

    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("e");

    @Override
    public void prepare(final Model model)
    {
        model.getEduplanVersionHolder().refresh(EppEduPlanVersion.class);
    }

    @Override
    public void prepareDataSource(final Model model)
    {
        final MQBuilder builder = new MQBuilder(EppWorkPlanGen.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", EppWorkPlanGen.parent().eduPlanVersion().id(), model.getEduplanVersion().getId()));
        DAO._orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, this.getSession());
    }

}
