package ru.tandemservice.uniepp.component.registry.RegElementPartEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent component) {
        this.getDao().save(this.getModel(component));
        if (!UserContext.getInstance().getErrorCollector().hasErrors()) {
            this.deactivate(component);
        }
    }
}
