package ru.tandemservice.uniepp.component.edugroup.list.GroupListOwnerTabBase;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Factory;
import org.hibernate.Session;
import org.springframework.util.ClassUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.component.edugroup.EduGroupOwnerModel;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupSummary;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroupSummaryGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    // список доступных сводок УГС
    protected List<EppRealEduGroupSummary> summaryList(final EduGroupOwnerModel model) {
        return getList(EppRealEduGroupSummary.class);
    }

    protected DQLSelectBuilder groupWithRelationsDql(final EduGroupOwnerModel model, final EppRealEduGroupSummary summary, Class<? extends EppRealEduGroup> groupClass)
    {
        return new DQLSelectBuilder()
        .fromEntity(groupClass, "g")
        .joinEntity("g", DQLJoinType.left, EppRealEduGroupRow.class, "rel", eq(property("g"), property("rel", EppRealEduGroupRow.L_GROUP)))
        .where(eq(property(EppRealEduGroup.summary().id().fromAlias("g")), value(summary.getId())));
    }

    protected DQLSelectBuilder groupWithRelationsDql(final EppRealEduGroupSummary summary)
    {
        return new DQLSelectBuilder()
                .fromEntity(EppRealEduGroup.class, "g")
                .joinEntity("g", DQLJoinType.left, EppRealEduGroupRow.class, "rel", eq(property("g"), property("rel", EppRealEduGroupRow.L_GROUP)))
                .where(eq(property(EppRealEduGroup.summary().id().fromAlias("g")), value(summary.getId())));
    }

    protected String getSettingsName() { return "global"; }

    @Override
    public void prepare(final Model model) {
        {
            if (null == model.getOrgUnitHolder().getId()) {
                model.getOrgUnitHolder().setValue(TopOrgUnit.getInstance());
            }
            model.getOrgUnitHolder().refresh();
            model.setSettings(UniBaseUtils.getDataSettings("epp.groups."+this.getSettingsName()+"."+model.getOrgUnitId()));
        }

        final List<EppRealEduGroupSummary> summaryList = this.summaryList(model);
        model.setEmpty(summaryList.isEmpty());
        model.setOwnerSelectModel(buildOwnerSelectModel(model, summaryList));
        model.setYearPartSelectModel(buildYearPartSelectModel(model, summaryList));
        model.setGroupTypeSelectModel(buildGroupTypeSelectModel(model, summaryList));
    }

    protected DQLFullCheckSelectModel buildOwnerSelectModel(final Model model, final List<EppRealEduGroupSummary> summaryList) {
        return new DQLFullCheckSelectModel(OrgUnit.P_TITLE, OrgUnit.P_SHORT_TITLE) {
            @Override public String getName() { return "ownerSelectModel"; }

            // кэшируем
            private final List<Long> ownerIds = new ArrayList<>(
            new HashSet<>(CollectionUtils.collect(summaryList, input -> input.getOwner().getId())));

            @Override protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(OrgUnit.class, alias)
                .order(property(OrgUnit.title().fromAlias(alias)));

                if (null != filter) {
                    dql.where(this.like(OrgUnit.title().fromAlias(alias), filter));
                }

                return dql.where(in(property(alias, "id"), ownerIds));
            }
        };
    }

    protected DQLFullCheckSelectModel buildYearPartSelectModel(final Model model, final List<EppRealEduGroupSummary> summaryList) {
        return new DQLFullCheckSelectModel("title") {
            @Override public String getName() { return "yearPartSelectModel"; }

            // кэшируем запросы
            private final Map<Long, List<Long>> owner2yearPartIds = SafeMap.get(ownerId -> {
                final Set<Long> yearPartIds = new HashSet<>();
                for (EppRealEduGroupSummary summary: summaryList) {
                    if (ownerId.equals(summary.getOwner().getId())) {
                        yearPartIds.add(summary.getYearPart().getId());
                    }
                }
                return new ArrayList<>(yearPartIds);
            });

            @Override protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final Object owner = model.getOwner();
                if (!(owner instanceof OrgUnit)) { return null; }

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppYearPart.class, alias)
                .order(property(EppYearPart.year().educationYear().intValue().fromAlias(alias)))
                .order(property(EppYearPart.part().yearDistribution().amount().fromAlias(alias)))
                .order(property(EppYearPart.part().number().fromAlias(alias)));

                FilterUtils.applyLikeFilter(dql, filter,
                    EppYearPart.year().title().fromAlias(alias),
                    EppYearPart.year().educationYear().title().fromAlias(alias),
                    EppYearPart.part().title().fromAlias(alias),
                    EppYearPart.part().yearDistribution().title().fromAlias(alias)
                );

                return dql.where(in(property(alias, "id"), owner2yearPartIds.get(((OrgUnit)owner).getId())));
            }
        }.setPageSize(-1);
    }

    protected DQLFullCheckSelectModel buildGroupTypeSelectModel(final Model model, final List<EppRealEduGroupSummary> summaryList) {
        return new DQLFullCheckSelectModel(ICatalogItem.CATALOG_ITEM_TITLE) {
            @Override public String getName() { return "groupTypeSelectModel"; }

            // кэшируем запросы
            private final Map<Long, List<Long>> summary2typeIds = SafeMap.get(key -> {
                final Session session = getSession();
                final EppRealEduGroupSummary summary = (EppRealEduGroupSummary)session.load(EppRealEduGroupSummary.class, key);
                return DAO.this.groupWithRelationsDql(summary)
                        .column(property(EppRealEduGroup.type().id().fromAlias("g")), "id")
                        .predicate(DQLPredicateType.distinct)
                        .createStatement(session).list();
            });

            @Override protected DQLSelectBuilder query(final String alias, final String filter) {
                final EppRealEduGroupSummary summary = DAO.this.getSummary(model);
                if (null == summary) { return null; }

                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppGroupType.class, alias)
                .order(property(EppGroupType.priority().fromAlias(alias)));

                if (null != filter) {
                    dql.where(this.like(EppGroupType.title().fromAlias(alias), filter));
                }

                return dql.where(in(property(alias, "id"), summary2typeIds.get(summary.getId())));
            }
        }.setPageSize(-1);
    }

    protected EppRealEduGroupSummary getSummary(final Model model)
    {
        final Object owner = model.getOwner();
        if (!(owner instanceof OrgUnit)) { return null; }

        final Object yearpart = model.getYearPart();
        if (!(yearpart instanceof EppYearPart)) { return null; }

        final EppRealEduGroupSummary summary = this.getByNaturalId(new EppRealEduGroupSummaryGen.NaturalId((OrgUnit)owner, (EppYearPart)yearpart));
        if (null == summary) { return null; }

        return summary;
    }

    @SuppressWarnings("unchecked")
    @Override public Factory<DQLSelectBuilder> getRelationDqlFactory(final Model model)
    {
        final Object groupType = model.getGroupType();
        if (!(groupType instanceof EppGroupType)) { return null; }

        final Class<? extends EppRealEduGroup> groupClass = EppRealEduGroup.getGroupClass((Class) ClassUtils.getUserClass(groupType));

        final EppRealEduGroupSummary summary = this.getSummary(model);
        if (null == summary) { return null; }

        return () -> DAO.this.groupWithRelationsDql(model, summary, groupClass)
        .where(eq(property(EppRealEduGroup.type().fromAlias("g")), commonValue(groupType)));
    }


}
