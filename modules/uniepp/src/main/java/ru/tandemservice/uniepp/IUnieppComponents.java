package ru.tandemservice.uniepp;

public interface IUnieppComponents
{
    String EDUCATIONAL_PLANNING_ADD_EDIT = ru.tandemservice.uniepp.component.settings.EducationalPlanningAddEdit.Model.COMPONENT_NAME;
    String WORK_GRAPH_ADD = ru.tandemservice.uniepp.component.workgraph.WorkGraphAdd.Model.COMPONENT_NAME;
    String DEFAULT_SIZE_FOR_LABOR_ADD_EDIT = ru.tandemservice.uniepp.component.settings.DefaultSizeForLaborAddEdit.Model.COMPONENT_NAME;
    String DEFAULT_SELFWORK_FOR_SIZE_ADD_EDIT = ru.tandemservice.uniepp.component.settings.DefaultSelfworkForSizeAddEdit.Model.COMPONENT_NAME;
    String DEFAULT_EDUPLAN_VERSION_EDIT = ru.tandemservice.uniepp.component.settings.DefaultEduplanVersionEdit.Model.COMPONENT_NAME;
    String EPP_CTR_ACADEMY_PRESENTER_EDIT = ru.tandemservice.unieductr.component.settings.EduCtrAcademyPresenterEdit.Model.COMPONENT_NAME;
}
