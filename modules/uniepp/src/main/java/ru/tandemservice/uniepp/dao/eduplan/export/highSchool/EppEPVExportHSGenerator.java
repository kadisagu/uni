package ru.tandemservice.uniepp.dao.eduplan.export.highSchool;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.eduplan.export.EppEPVExportUtils;
import ru.tandemservice.uniepp.dao.eduplan.export.IEppEduPlanVersionExportDAO;
import ru.tandemservice.uniepp.dao.eduplan.export.highSchool.XmlDocumentData.*;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * 
 * @author nkokorina
 *
 */

public class EppEPVExportHSGenerator
{
    private final EppEduPlanVersion version;
    private final IEppEpvBlockWrapper rootBlockWrapper;
    private final Set<Integer> course;
    private final List<EppEduPlanVersionBlock> versionBlockList;
    private final List<EppEduPlanVersionWeekType> weeksList;
    private final Map<Integer, DevelopGridTerm> termNum2grid;
    private final Map<Integer, Integer> groupCount;
    private final Map<Integer, Integer> studentCount;

    private final DevelopForm form = null;
    private final Qualifications qualification = null;

    public EppEPVExportHSGenerator(final EppEduPlanVersion planVersion)
    {
        this.version = planVersion;

        EppEduPlanVersionBlock rootBlock = IEppEduPlanVersionDataDAO.instance.get().getRootBlock(this.version);
        this.rootBlockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(rootBlock.getId(), true);
        this.course = IEppEduPlanVersionExportDAO.instance.get().getVersionCourseSet(this.version.getId());
        this.versionBlockList = IEppEduPlanVersionExportDAO.instance.get().getVersionBlockList(this.version.getId());
        this.weeksList = IEppEduPlanVersionExportDAO.instance.get().getEduPlanVersionWeeks(this.version.getId());

        final Long gridId = this.version.getEduPlan().getDevelopGrid().getId();
        this.termNum2grid = IDevelopGridDAO.instance.get().getDevelopGridMap(gridId);
        this.groupCount = IEppEduPlanVersionExportDAO.instance.get().getCourse2GroupCount(this.version.getId());
        this.studentCount = IEppEduPlanVersionExportDAO.instance.get().getCourse2StudentCount(this.version.getId());

        // todo DEV-6000
        //        this.form = this.version.getEduPlan().getDevelopForm();
        //        this.qualification = this.version.getEduPlan().getEducationLevelHighSchool().getEducationLevel().getQualification();
    }

    public byte[] generateContent() throws IOException, JAXBException
    {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final ByteArrayOutputStream outTemp = new ByteArrayOutputStream();

        final JAXBContext jc = JAXBContext.newInstance(XmlDocumentData.class);
        final Marshaller m = EppEPVExportUtils.getMarshaller(jc);

        final XmlDocumentData root = new XmlDocumentData();
        this.convertVersionToXml(root);
        m.marshal(root, outTemp);

        outTemp.close();
        root.checkSum = EppEPVExportUtils.getCheckSum(outTemp.toByteArray());

        m.marshal(root, out);
        out.close();

        return out.toByteArray();
    }

    private void convertVersionToXml(final XmlDocumentData root)
    {
        final XmlEppEduPlan plan = new XmlEppEduPlan();
        plan.subType = EppEPVExportHSUtils.getSubTypeString(this.qualification.getCode(), this.form.getCode());
        plan.code = EppEPVExportHSUtils.getCodeFileExport(this.qualification.getCode(), this.form.getCode()).toUpperCase();
        plan.educationProgram = EppEPVExportHSUtils.getEducationProgramString(this.qualification.getCode(), this.form.getCode());
        plan.developForm = EppEPVExportHSUtils.getdevelopFormString(this.form.getCode());
        plan.educationLevel = "ВПО";

        final XmlPlanTitul title = new XmlPlanTitul();
        final String titulplanTitle = EppEPVExportHSUtils.buildFileTitleToHigh(this.version, this.course);
        title.titulplanTitle = titulplanTitle;
        title.titulplanFullTitle = titulplanTitle.length() >= 16 ? titulplanTitle.substring(0, 16) : titulplanTitle;
        title.titulHead = null;
        title.titullastCode = null;
        title.titulNoAutoWeeks = null;
        title.titulDateGos = this.version.getEduPlan().getParent().getConfirmDate() == null ? null : DateFormatter.DEFAULT_DATE_FORMATTER.format(this.version.getEduPlan().getParent().getConfirmDate());
        // todo DEV-6000
        //        title.titulDateCertificate = String.valueOf(this.version.getEduPlan().getEduStartYear());
        title.titulPlanType = null;
        title.titulYearStartTraining = null;
        title.titulCodeCathedra = EppEPVExportHSUtils.getCathedraCode(this.version);
        title.titulFaculty = EppEPVExportHSUtils.getFacultyTitle(this.version);
        title.titulApplication = null;
        title.titulDateApplication = null;
        title.titulVersionApplication = null;

        final XmlPlanCyclesAttributes cyclesAttributes = new XmlPlanCyclesAttributes();
        this.fillPlanCyclesList(cyclesAttributes);

        final XmlPlanApproval approval = new XmlPlanApproval();
        approval.approvalDate = this.version.getConfirmDate() == null ? null : DateFormatter.DEFAULT_DATE_FORMATTER.format(this.version.getConfirmDate());
        approval.approvalProtocolNumber = this.version.getProtocolNumber();

        final XmlPlanDevelopers developers = new XmlPlanDevelopers();
        this.fillPlanDevelopersList(developers);

        final XmlPlanQualifications qualifications = new XmlPlanQualifications();
        this.fillPlanQualificationsList(qualifications);

        final XmlPlanSpecialties specialties = new XmlPlanSpecialties();
        this.fillPlanSpecialtiesList(specialties);

        final XmlPlanScheduleProcess scheduleProcess = new XmlPlanScheduleProcess();
        this.fillPlansCheduleProcessList(scheduleProcess);

        title.cyclesAttributes = cyclesAttributes;
        title.approval = approval;
        title.developers = developers;
        title.qualifications = qualifications;
        title.specialties = specialties;
        title.scheduleProcess = scheduleProcess;

        final XmlPlanLines planLines = new XmlPlanLines();
        this.fillPlanLines(planLines);

        final XmlPlanTypeWorks typeWorks = new XmlPlanTypeWorks();
        // todo DEV-6000
        //        this.fillPlanTypeWorks(typeWorks);

        final XmlPlanStandards standards = new XmlPlanStandards();

        plan.planTitle = title;
        plan.planLines = planLines;
        plan.planTypeWorks = typeWorks;
        plan.planStandards = standards;

        root.element = plan;
        root.type = "рабочий учебный план";
        root.checkSum = "";
    }


    private void fillPlanCyclesList(XmlPlanCyclesAttributes cyclesAttributes)
    {
        final List<IEppEpvRowWrapper> eduPlanRows = new ArrayList<>(this.rootBlockWrapper.getRowMap().values());
        CollectionUtils.filter(eduPlanRows, object -> null == object.getHierarhyParent());

        final List<IEppEpvRowWrapper> eduPlanRowsList = new ArrayList<>(eduPlanRows);
        final List<XmlPlanCycle> cycleList = new ArrayList<>();

        if (!eduPlanRowsList.isEmpty())
        {
            for (int i = 0; i < eduPlanRowsList.size(); i++)
            {
                final IEppEpvRowWrapper row = eduPlanRowsList.get(i);

                final XmlPlanCycle cycle = new XmlPlanCycle();
                cycle.cycleNom = String.valueOf(i + 1);
                cycle.cycleTitle = row.getTitle();
                cycle.cycleAbbreviation = row.getIndex();
                cycle.cycleDeviation = null;

                cycleList.add(cycle);
            }

            cyclesAttributes.planCycles = cycleList;
        }
        //        else
        //        {
        //            cyclesAttributes = null;
        //        }
    }

    private void fillPlanDevelopersList(final XmlPlanDevelopers developers)
    {

    }

    private void fillPlanQualificationsList(XmlPlanQualifications qualifications)
    {
        // todo DEV-6000
        //        final DiplomaQualifications diplomaQualification = this.version.getEduPlan().getEducationLevelHighSchool().getEducationLevel().getDiplomaQualification();
        //
        //        final XmlPlanQualification qualification = new XmlPlanQualification();
        //        final List<XmlPlanQualification> qualificationList = new ArrayList<>();
        //        if (null != diplomaQualification)
        //        {
        //            qualification.qualificationNom = "1";
        //            final String qualificationTitle = diplomaQualification.getTitle();
        //            qualification.qualificationTitle = qualificationTitle.length() >= 30 ? qualificationTitle.substring(0, 30) : qualificationTitle;
        //            qualification.qualificationLearning = String.valueOf(this.version.getEduPlan().getDevelopGrid().getDevelopPeriod().getLastCourse());
        //
        //            qualificationList.add(qualification);
        //            qualifications.planQualifications = qualificationList;
        //        }
        //        else
        //        {
        //            qualifications = null;
        //        }
    }

    private void fillPlanSpecialtiesList(XmlPlanSpecialties specialties)
    {
        // todo DEV-6000
        //        if (!this.versionBlockList.isEmpty())
        //        {
        //            final List<XmlPlanSpecialitie> specialities = new ArrayList<>();
        //
        //            final int size = this.versionBlockList.size() > 7 ? 7 : this.versionBlockList.size();
        //            for (int i = 0; i < size; i++)
        //            {
        //                final EducationLevelsHighSchool educationLevelHighSchool = this.versionBlockList.get(i).getEducationLevelHighSchool();
        //                final EducationLevels educationLevel = educationLevelHighSchool.getEducationLevel();
        //                final StringBuilder sb = new StringBuilder();
        //                sb.append(StringUtils.capitalize(educationLevel.getLevelType().getTitle())).append(" ");
        //                sb.append(educationLevel.getTitleCodePrefix() == null ? "" : (educationLevel.getTitleCodePrefix() + " "));
        //                sb.append("\"").append(educationLevelHighSchool.getTitle()).append("\"");
        //
        //                final XmlPlanSpecialitie specialitie = new XmlPlanSpecialitie();
        //                specialitie.specialitieNom = String.valueOf(i + 1);
        //                specialitie.specialitieTitle = sb.toString();
        //
        //                specialities.add(specialitie);
        //            }
        //            specialties.planSpecialities = specialities;
        //        }
        //        else
        //        {
        //            specialties = null;
        //        }

    }

    private void fillPlansCheduleProcessList(XmlPlanScheduleProcess scheduleProcess)
    {
        if (!this.weeksList.isEmpty())
        {
            final Map<Integer, List<EppEduPlanVersionWeekType>> course2WeekType = SafeMap.get(ArrayList.class);
            for (final EppEduPlanVersionWeekType week : this.weeksList)
            {
                course2WeekType.get(week.getCourse().getIntValue()).add(week);
            }

            final List<XmlPlanScheduleCourse> scheduleCourseList = new ArrayList<>();
            for (final Integer course : course2WeekType.keySet())
            {
                final List<EppEduPlanVersionWeekType> list = course2WeekType.get(course);
                Collections.sort(list, EppEPVExportHSUtils.weekTypeComparator);

                int weekAut = 0;
                int weekSp = 0;
                // проверяем сколько честей в учебном году
                final boolean isPartsTwo = this.termNum2grid.get(list.get(0).getTerm().getIntValue()).getPartAmount() == 2;

                final StringBuilder sb = new StringBuilder();
                for (final EppEduPlanVersionWeekType week : list)
                {
                    // заполняем строчку с типами учебных недель
                    sb.append(week.getWeekType().getAbbreviationIMCA());

                    // считаем количество недель в частях, если их две
                    if (isPartsTwo)
                    {
                        final int partNumber = this.termNum2grid.get(week.getTerm().getIntValue()).getPartNumber();
                        if ((partNumber == 1) && EppWeekType.CODE_THEORY.equals(week.getWeekType().getCode()))
                        {
                            weekAut++;
                        }
                        if ((partNumber == 2) && EppWeekType.CODE_THEORY.equals(week.getWeekType().getCode()))
                        {
                            weekSp++;
                        }
                    }
                }

                final XmlPlanScheduleCourse courseStr = new XmlPlanScheduleCourse();
                courseStr.scheduleNom = String.valueOf(course);
                courseStr.scheduleGraph = sb.toString();
                courseStr.scheduleWeekAut = weekAut <= 0 ? null : String.valueOf(weekAut);
                courseStr.scheduleWeekSp = weekSp <= 0 ? null : String.valueOf(weekSp);

                courseStr.scheduleGroup = this.groupCount.containsKey(course) ? String.valueOf(this.groupCount.get(course)) : null;
                courseStr.scheduleStudent = this.studentCount.containsKey(course) ? String.valueOf(this.studentCount.get(course)) : null;

                scheduleCourseList.add(courseStr);
            }

            scheduleProcess.planScheduleCourses = scheduleCourseList;
        }
        //        else
        //        {
        //            scheduleProcess = null;
        //        }
    }

    private void fillPlanLines(final XmlPlanLines planLines)
    {
        // todo DEV-6000

        //        final Long levelSHId = this.version.getEduPlan().getEducationLevelHighSchool().getId();
        //
        //        // так как условиями задачи требуется взять первый из неосновного блока
        //        // берем любое направление подготовки, которое не совпадает с направлением версии
        //        final EppEduPlanVersionBlock block = (EppEduPlanVersionBlock)CollectionUtils.find(this.versionBlockList, new Predicate<EppEduPlanVersionBlock>() {
        //            @Override public boolean evaluate(final EppEduPlanVersionBlock object) {
        //                return !levelSHId.equals(object.getEducationLevelHighSchool().getId());
        //            }
        //        });
        //
        //        final Predicate<IEppEpvRowWrapper> predicate = new Predicate<IEppEpvRowWrapper>()
        //        {
        //            final Long additionalLevelId = (null != block) ? block.getEducationLevelHighSchool().getId() : null;
        //
        //            // TODO: use EppEduPlanVersionDataDAO.buildBlockRowPredicate
        //            @Deprecated
        //            final Set<Long> educationLevelIds = new HashSet<>(Arrays.asList(new Long[] { levelSHId, this.additionalLevelId }));
        //
        //            // в выгрузку попадают реальные группы или дисциплины вне реальных групп
        //            // идея тут в следующем - если у нас есть группа (реальная, а не по выбору)
        //            // то это означает что дисциплину ГОСа мы превратили в группу и напихали в нее
        //            // кучу внутренних ВУЗовских дисциплин (вот их выгружать не надо), а саму группу выгрузить придется
        //            // однако! если это дисциплина по выбору (т.е. тоже группа, но мнимая)
        //            // то такие дисциплины выгрудать надо (но без группы), поле `цикл` для них вычисляется хитровывернутым способом
        //            // (познанно аналитиками эмпирическим путем под очень тяжелыми грибами)
        //            // проблема в том. что эта программа  ИМЦА имеет внутри себя кучу недокументированных ограничений под которые приходится подстраивать систему
        //
        //            public boolean isInReGroup(IEppEpvRow row) {
        //                row = row.getHierarhyParent(); // сам элемент проверять не надо
        //
        //                while (null != row) {
        //                    if (row instanceof EppEpvGroupReRow) { return true; }
        //                    row = row.getHierarhyParent();
        //                }
        //                return false;
        //            }
        //
        //            @Override
        //            public boolean evaluate(final IEppEpvRowWrapper object)
        //            {
        //                final IEppEpvRow row = object.getRow();
        //
        //                if (row instanceof EppEpvRegistryRow) {
        //                    // если это дисциплина - то смотрим, что она из нужного блока и что она не в группе (считается, что в этом случае группа - это дисциплина)
        //                    return (this.educationLevelIds.contains(row.getOwner().getEducationLevelHighSchool().getId()) && (!this.isInReGroup(row)));
        //                }
        //
        //                if (row instanceof EppEpvGroupReRow) {
        //                    // если это группа - то ее выгружать надо (она считается дисциплиной)
        //                    return (this.educationLevelIds.contains(row.getOwner().getEducationLevelHighSchool().getId()));
        //                }
        //
        //                // если это что-то левое, то мы его не выгружаем
        //                // остается единственный вопрос к аналитикам: а что делать, если в ГОСе были группы???
        //                return false;
        //            }
        //        };
        //
        //        final Collection<IEppEpvRowWrapper> eduPlanBlockRows = EppEduPlanVersionDataDAO.filterEduPlanBlockRows(this.versionWrapper.getRowMap(), predicate, true);
        //        CollectionUtils.filter(eduPlanBlockRows, predicate);
        //
        //        final List<XmlPlanLine> planLineList = new ArrayList<>(eduPlanBlockRows.size());
        //        for (final IEppEpvRowWrapper row : eduPlanBlockRows)
        //        {
        //
        //
        //            final XmlPlanLine planLine = new XmlPlanLine();
        //            planLine.lineTitle = row.getTitle();
        //            planLine.lineCycle = EppEPVExportHSUtils.getPlanLineCycle(row);
        //            planLine.lineIdPlanType = EppEPVExportHSUtils.getPlanLineIdType(this.qualification.getCode(), this.form.getCode());
        //
        //            // если это группа дисциплин по выбору - берем нагрузку из нее
        //            // делим на количество выбираемых дисциплин в группе
        //            // (если в группе 200 часов и выбирается 2 дисциплины из 10, то на каждую выбранную приходится 100 часов)
        //            IEppEpvRowWrapper row4load = row;
        //            int loadSize = 1;
        //            if (null != row4load.getHierarhyParent() && row4load.getHierarhyParent().getRow() instanceof EppEpvGroupImRow) {
        //                row4load = row4load.getHierarhyParent();
        //                loadSize = Math.max(1, ((EppEpvGroupImRow)row4load.getRow()).getSize());
        //            }
        //
        //            planLine.lineGos = null; /// пока так, так как еще не ясны требования
        //            planLine.lineSR = UniEppUtils.formatLoad(row4load.getTotalInTermLoad(0, EppELoadType.FULL_CODE_TOTAL_SELFWORK) / loadSize, false);
        //            planLine.lineTermExam = EppEPVExportHSUtils.formatAction(row4load.getActionTermSet(EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM));
        //            planLine.lineTermTest = EppEPVExportHSUtils.formatAction(row4load.getActionTermSet(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF));
        //
        //            final List<Integer> courseProjectTerms = row4load.getActionTermSet(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT);
        //            final List<Integer> coursWorkTerms = row4load.getActionTermSet(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK);
        //            planLine.lineTermCP = EppEPVExportHSUtils.formatAction(courseProjectTerms);
        //            planLine.lineTermCW = EppEPVExportHSUtils.formatAction(coursWorkTerms);
        //            planLine.lineCathedra = null;
        //            planLine.lineBeStudied = UniEppUtils.formatLoad(row4load.getTotalInTermLoad(0, EppLoadType.TYPE_TOTAL_SIZE_FULL_CODE) / loadSize, false);
        //            planLine.lineIdDiscipline = row4load.getIndex();
        //            planLine.lineDisciplineToCategories = (row4load.getHierarhyParent() instanceof EppEpvGroupReRow) ? "1" : "0";
        //            planLine.lineCategorie = (row4load instanceof EppEpvGroupReRow) ? "1" : "0";
        //
        //            if (!courseProjectTerms.isEmpty())
        //            {
        //                final XmlCourseProject project = new XmlCourseProject();
        //
        //                final List<XmlCourseProjectTerm> planCourseProjectTerm = new ArrayList<>();
        //                for (final Integer term : courseProjectTerms)
        //                {
        //                    final XmlCourseProjectTerm xmlTerm = new XmlCourseProjectTerm();
        //                    xmlTerm.term = String.valueOf(term);
        //
        //                    final XmlCPDistributionStudents planCPDistributionStudent = new XmlCPDistributionStudents();
        //                    planCPDistributionStudent.planCPNom = "1";
        //                    planCPDistributionStudent.planCPStudents = null;
        //                    planCPDistributionStudent.planCPCathedra = null;
        //
        //                    xmlTerm.planCPDistributionStudent = planCPDistributionStudent;
        //
        //                    planCourseProjectTerm.add(xmlTerm);
        //                }
        //                project.planCourseProjectTerm = planCourseProjectTerm;
        //                planLine.planCourseProject = project;
        //
        //            }
        //            if (!coursWorkTerms.isEmpty())
        //            {
        //                final XmlCourseWork work = new XmlCourseWork();
        //
        //                final List<XmlCourseProjectTerm> planCourseProjectTerm = new ArrayList<>();
        //                for (final Integer term : coursWorkTerms)
        //                {
        //                    final XmlCourseProjectTerm xmlTerm = new XmlCourseProjectTerm();
        //                    xmlTerm.term = String.valueOf(term);
        //
        //                    final XmlCPDistributionStudents planCPDistributionStudent = new XmlCPDistributionStudents();
        //                    planCPDistributionStudent.planCPNom = "1";
        //                    planCPDistributionStudent.planCPStudents = null;
        //                    planCPDistributionStudent.planCPCathedra = null;
        //
        //                    xmlTerm.planCPDistributionStudent = planCPDistributionStudent;
        //
        //                    planCourseProjectTerm.add(xmlTerm);
        //                }
        //                work.planCourseWorkTerm = planCourseProjectTerm;
        //                planLine.planCourseWork = work;
        //            }
        //            final List<XmlPlanLineTerm> planLineTerms = new ArrayList<>();
        //            for (final Integer term : this.termNum2grid.keySet())
        //            {
        //                final double lectures = row4load.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_LECTURES) / loadSize;
        //                final double practice = row4load.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_PRACTICE) / loadSize;
        //                final double labs = row4load.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_LABS) / loadSize;
        //
        //                if ((lectures > 0) || (practice > 0) || (labs > 0))
        //                {
        //                    final XmlPlanLineTerm xmlTerm = new XmlPlanLineTerm();
        //                    xmlTerm.planLineTermNom = String.valueOf(term);
        //                    xmlTerm.planLineTermLec = (lectures > 0) ? UniEppUtils.formatLoad(lectures, false) : null;
        //                    xmlTerm.planLineTermPR = (practice > 0) ? UniEppUtils.formatLoad(practice, false) : null;
        //                    xmlTerm.planLineTermLab = (labs > 0) ? UniEppUtils.formatLoad(labs, false) : null;
        //                    xmlTerm.planLineTermCPC = null;
        //
        //                    planLineTerms.add(xmlTerm);
        //                }
        //            }
        //            planLine.planLineTerm = planLineTerms.isEmpty() ? null : planLineTerms;
        //
        //            planLineList.add(planLine);
        //        }
        //
        //        planLines.planLineList = planLineList;
        //    }
        //
        //    private void fillPlanTypeWorks(final XmlPlanTypeWorks typeWorks)
        //    {
        //        for (final IEppEpvRowWrapper w: this.versionWrapper.getRowMap().values())
        //        {
        //            final IEppEpvRow row = (w).getRow();
        //            if (!(row instanceof EppEpvRegistryRow)) { continue; }
        //
        //            final EppRegistryElement registryElement = ((EppEpvRegistryRow)row).getRegistryElement();
        //            if (!(registryElement instanceof EppRegistryPractice)) { continue; }
        //
        //            boolean isEduPractice = false;
        //            EppRegistryStructure p = registryElement.getParent();
        //            while (null != p) {
        //                if ("eppRegistryPractice.1".equals(p.getCode())) { isEduPractice = true; break; }
        //                p = p.getParent();
        //            }
        //
        //            final List<XmlPlanSpecWorkTypePractice> list = isEduPractice ? typeWorks.eduPracticeList.list : typeWorks.anotherPracticeList.list;
        //            for (final Integer term: w.getActiveTermSet()) {
        //                if (term > 14) { continue; }
        //                final XmlPlanSpecWorkTypePractice practice = new XmlPlanSpecWorkTypePractice();
        //                practice.title = StringUtils.trimToNull(row.getTitle());
        //                if (null == practice.title) { practice.title = StringUtils.trimToNull(registryElement.getTitle()); }
        //
        //                practice.term = String.valueOf(term);
        //                practice.weeks = UniEppUtils.formatLoad(w.getTotalInTermLoad(term, EppLoadType.TYPE_TOTAL_WEEKS_FULL_CODE), false);
        //                practice.size = UniEppUtils.formatLoad(w.getTotalInTermLoad(term, EppLoadType.TYPE_TOTAL_SIZE_FULL_CODE), false);
        //                list.add(practice);
        //            }
        //        }
        //
        //        if (typeWorks.eduPracticeList.list.isEmpty()) { typeWorks.eduPracticeList = null; }
        //        if (typeWorks.anotherPracticeList.list.isEmpty()) { typeWorks.anotherPracticeList = null; }

    }


}
