/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.student.StudentEduplanTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * @author vip_delete
 * @since 24.03.2010
 */
@Input({
    @Bind(key = "studentId", binding = "student.id")
})
public class Model
{
    private Student _student = new Student();
    private EppStudent2EduPlanVersion _relation;

    private String errors;

    public boolean isShowErrors() {
        return null != errors;
    }

    // Getters & Setters

    public Student getStudent()
    {
        return this._student;
    }

    public void setStudent(final Student student)
    {
        this._student = student;
    }

    public EppStudent2EduPlanVersion getRelation()
    {
        return this._relation;
    }

    public void setRelation(final EppStudent2EduPlanVersion relation)
    {
        this._relation = relation;
    }

    public String getErrors()
    {
        return errors;
    }

    public void setErrors(String errors)
    {
        this.errors = errors;
    }

    public EppCustomEduPlan getCustomPlan()
    {
        return _relation != null ? _relation.getCustomEduPlan() : null;
    }
}
