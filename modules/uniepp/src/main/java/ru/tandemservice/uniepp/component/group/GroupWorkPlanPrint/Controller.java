package ru.tandemservice.uniepp.component.group.GroupWorkPlanPrint;

import jxl.Workbook;
import jxl.write.WritableWorkbook;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.tool.IdGen;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.dao.print.IEppWorkPlanPrintDAO;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent component) {
        final Model model = this.getModel(component);

        try {


            final Map<Long, byte[]> groupDataMap = new HashMap<>(model.getDataMap().size());
            for (final Map.Entry<Long, Map<Long, Map<Integer, EppStudent2WorkPlan>>> groupEntry: model.getDataMap().entrySet()) {

                // { wp.id -> { group.id -> { student.id }}}
                final Map<Long, Map<Long, Collection<Long>>> wp2gsIdsMap = new LinkedHashMap<>();
                for (final Map<Integer, EppStudent2WorkPlan> byStudent: groupEntry.getValue().values()) {
                    for (final EppStudent2WorkPlan rel: byStudent.values()) {
                        if (!model.getYearModel().getValue().equals(rel.getWorkPlan().getYear().getEducationYear())) { continue; }
                        if (!model.getPartModel().getValue().equals(rel.getWorkPlan().getGridTerm().getPart())) { continue; }

                        final Student student = rel.getStudentEduPlanVersion().getStudent();
                        final Group group = student.getGroup();
                        final Map<Long, Collection<Long>> gsMap = SafeMap.safeGet(wp2gsIdsMap, rel.getWorkPlan().getId(), HashMap.class);
                        SafeMap.safeGet(gsMap, (null == group ? 0L : group.getId()), ArrayList.class).add(student.getId());
                    }
                }

                if (!wp2gsIdsMap.isEmpty()) {
                    try {

                        final ByteArrayOutputStream os = new ByteArrayOutputStream();
                        final WritableWorkbook book = Workbook.createWorkbook(os);
                        IEppWorkPlanPrintDAO.instance.get().printGroupWorkPlan(book, wp2gsIdsMap, new HashMap<>());
                        book.write();
                        book.close();
                        os.close();

                        groupDataMap.put(groupEntry.getKey(), os.toByteArray());
                    } catch (final Throwable t) {
                        throw CoreExceptionUtils.getRuntimeException(t);
                    }
                }
            }

            final String fileNamePrefix = ("РУП-"+model.getYearModel().getValue().getIntValue()+"-"+model.getPartModel().getValue().getTitle()).replace(' ', '_');
            if (groupDataMap.size() > 1)
            {
                final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                final ZipOutputStream zipOut = new ZipOutputStream(outputStream);
                for (final Map.Entry<Long, byte[]> groupEntry: groupDataMap.entrySet()) {
                    final String groupTitle = (
                            (null == groupEntry.getKey()) ? "ВнеГруппы" : UniDaoFacade.getCoreDao().getCalculatedValue(
                                    session -> ((Group) session.get(Group.class, groupEntry.getKey())).getTitle()
                            )
                    );

                    zipOut.putNextEntry(new ZipEntry(IdGen.getTime(groupEntry.getKey() != null ? groupEntry.getKey() : 0)+"-РУП-"+groupTitle+".xls"));
                    zipOut.write(groupEntry.getValue());
                    zipOut.closeEntry();
                }

                zipOut.close();
                outputStream.close();
                final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(outputStream.toByteArray(), fileNamePrefix + ".zip");
                this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("extension", "zip")));
            }
            else if (groupDataMap.size() > 0)
            {
                final Entry<Long, byte[]> groupEntry = groupDataMap.entrySet().iterator().next();
                final String groupTitle = (
                        (null == groupEntry.getKey()) ? "ВнеГруппы" : UniDaoFacade.getCoreDao().getCalculatedValue(
                                session -> ((Group)session.get(Group.class, groupEntry.getKey())).getTitle()
                        )
                );
                final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(groupEntry.getValue(), fileNamePrefix + "-" + groupTitle + ".xls");
                this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", id).add("extension", "xls")));
            }
            else
            {
                throw new IllegalStateException();
            }

        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

        this.deactivate(component);
    }


}
