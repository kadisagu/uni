package ru.tandemservice.uniepp.component.orgunit.EppEpvAcceptionTab;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.commonbase.tapestry.component.list.SimpleListDataHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author zhuj
 */
@Input({
    @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
public class Model
{
    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    private IDataSettings settings;

    private ISelectModel _stateListModel;
    private ISelectModel _programSubjectModel;
    private ISelectModel _programFormListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _programTraitListModel;
    private ISelectModel developGridListModel;
    private ISelectModel errorListModel;
    private List<HSelectOption> registryStructureList;

    private List<Integer> partNumbers;
    private Integer partNumber;
    private SimpleListDataHolder<RowWrapper> data = new SimpleListDataHolder<RowWrapper>();


    public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }
    public Long getOrgUnitId() { return this.getOrgUnitHolder().getId(); }
    public CommonPostfixPermissionModelBase getSec() { return this.getOrgUnitHolder().getSecModel(); }

    public EppState getState() { return (EppState) getSettings().get("state"); }
    public EppRegistryStructure getRegistryStructure() { return (EppRegistryStructure) getSettings().get("registryStructure"); }

    public String getCurrentRowClassName() {
        return ((0 == (getData().getCurrentIndex() & 1)) ? "list-row list-row-odd" : "list-row list-row-even");
    }

    public RowWrapper getCurrent() { return getData().getCurrent(); }
    public String getCurrentNumber() { return String.valueOf(getData().getCurrentNumber()); }

    public EppEpvRegistryRow getCurrentRow() { return getCurrent().getRegistryRow(); }
    public EppEduPlanVersionBlock getCurrentRowBlock() { return getCurrentRow().getOwner(); }
    public EppEduPlanVersion getCurrentRowVersion() { return getCurrentRowBlock().getEduPlanVersion(); }
    public String getCurrentRowVersionFullNumber() { return getCurrentRowVersion().getFullNumber(); }

    public String getCurrentRowStartGridTermTitle() {
        return getCurrent().getData()[0][1];
    }
    public String getCurrentRowSizeString() {
        return getCurrent().getData()[0][0];
    }

    public String getCurrentRowTermLoad4PartNumber() {
        int n = getPartNumber();
        String[][] data = getCurrent().getData();
        return ((0 < n && n < data.length) ? data[n][0] : "");
    }

    public String getCurrentRowTermActions4PartNumber() {
        int n = getPartNumber();
        String[][] data = getCurrent().getData();
        return ((0 < n && n < data.length) ? data[n][1] : "");
    }

    public RegistryElementWrapper getCurrentRegistryElementWrapper() { return getCurrent().getRegistryElementWrapper(); }
    public EppRegistryElement getCurrentRegistryElement() {
        final RegistryElementWrapper w = getCurrentRegistryElementWrapper();
        return null == w ? null : w.getRegistryElement();
    }

    public String getCurrentRegistryElementSizeString() {
        RegistryElementWrapper w = getCurrentRegistryElementWrapper();
        if (null == w) { return ""; }
        return w.getData()[0][0];
    }

    public String getCurrentRegistryElementLoad4PartNumber() {
        RegistryElementWrapper w = getCurrentRegistryElementWrapper();
        if (null == w) { return ""; }

        int n = getPartNumber();
        String[][] data = w.getData();
        return ((0 < n && n < data.length) ? data[n][0] : "");
    }

    public String getCurrentRegistryElementActions4PartNumber() {
        RegistryElementWrapper w = getCurrentRegistryElementWrapper();
        if (null == w) { return ""; }

        int n = getPartNumber();
        String[][] data = w.getData();
        return ((0 < n && n < data.length) ? data[n][1] : "");
    }

    public static class RowWrapper implements IIdentifiable
    {
        private static final Set<String> SELECTABLE_STATES = new HashSet<String>(Arrays.asList(EppState.STATE_FORMATIVE, EppState.STATE_ACCEPTABLE));

        private final EppEpvRegistryRow registryRow;
        public EppEpvRegistryRow getRegistryRow() { return this.registryRow; }
        @Override public Long getId() { return getRegistryRow().getId(); }

        public boolean isSelectionDisabled() { return !SELECTABLE_STATES.contains(getRegistryRow().getOwner().getEduPlanVersion().getState().getCode()); }

        private final RegistryElementWrapper registryElementWrapper;
        public RegistryElementWrapper getRegistryElementWrapper() { return this.registryElementWrapper; }


        private final String index;
        public String getIndex() { return this.index; }

        private final String[][] data;
        public String[][] getData() { return this.data; }

        private boolean selected = false;
        public boolean isSelected() { return this.selected; }
        public void setSelected(boolean selected) { this.selected = selected; }


        public RowWrapper(EppEpvRegistryRow registryRow, RegistryElementWrapper registryElementWrapper, String[][] data, String index) {
            this.registryRow = registryRow;
            this.registryElementWrapper = registryElementWrapper;
            this.data = data;
            this.index = index;
        }
    }

    public static class RegistryElementWrapper implements IIdentifiable
    {
        private final EppRegistryElement registryElement;
        public EppRegistryElement getRegistryElement() { return this.registryElement; }
        @Override public Long getId() { return getRegistryElement().getId(); }

        private final String[][] data;
        public String[][] getData() { return this.data; }

        public RegistryElementWrapper(EppRegistryElement registryElement, String[][] data) {
            this.registryElement = registryElement;
            this.data = data;

        }
    }

    // getters and setters

    public OrgUnitHolder getOrgUnitHolder() { return this.orgUnitHolder; }

    public IDataSettings getSettings() { return this.settings; }
    public void setSettings(final IDataSettings settings) { this.settings = settings; }

    public ISelectModel getStateListModel() { return this._stateListModel; }
    public void setStateListModel(final ISelectModel stateListModel) { this._stateListModel = stateListModel; }

    public ISelectModel getErrorListModel() { return this.errorListModel; }
    public void setErrorListModel(ISelectModel errorListModel) { this.errorListModel = errorListModel; }

    public List<Integer> getPartNumbers() { return this.partNumbers; }
    public void setPartNumbers(List<Integer> partNumbers) { this.partNumbers = partNumbers; }

    public Integer getPartNumber() { return this.partNumber; }
    public void setPartNumber(Integer partNumber) { this.partNumber = partNumber; }

    public SimpleListDataHolder<RowWrapper> getData() { return this.data; }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public ISelectModel getProgramFormListModel()
    {
        return _programFormListModel;
    }

    public void setProgramFormListModel(ISelectModel programFormListModel)
    {
        _programFormListModel = programFormListModel;
    }

    public ISelectModel getProgramSubjectModel()
    {
        return _programSubjectModel;
    }

    public void setProgramSubjectModel(ISelectModel programSubjectModel)
    {
        _programSubjectModel = programSubjectModel;
    }

    public ISelectModel getProgramTraitListModel()
    {
        return _programTraitListModel;
    }

    public void setProgramTraitListModel(ISelectModel programTraitListModel)
    {
        _programTraitListModel = programTraitListModel;
    }

    public void setData(SimpleListDataHolder<RowWrapper> data)
    {
        this.data = data;
    }

    public ISelectModel getDevelopGridListModel()
    {
        return developGridListModel;
    }

    public void setDevelopGridListModel(ISelectModel developGridListModel)
    {
        this.developGridListModel = developGridListModel;
    }

    public List<HSelectOption> getRegistryStructureList() { return this.registryStructureList; }
    public void setRegistryStructureList(List<HSelectOption> registryStructureList) { this.registryStructureList = registryStructureList; }
}
