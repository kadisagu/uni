package ru.tandemservice.uniepp.entity.settings;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniepp.entity.settings.gen.EppTutorOrgUnitGen;

/**
 * Читающая кафедра (подразделение)
 */
public class EppTutorOrgUnit extends EppTutorOrgUnitGen
{
    public EppTutorOrgUnit() {}
    public EppTutorOrgUnit(final OrgUnit orgUnit) { this.setOrgUnit(orgUnit); }
}