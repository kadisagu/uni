package ru.tandemservice.uniepp.dao.student;

import java.util.Set;

import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;

import ru.tandemservice.uni.entity.employee.Student;

/**
 * вызывает актуализацию связей (с РУП и УПв) при изменении данных студента
 * 
 * @author vdanilov
 */
public class EppDSetRefreshStudentEventListener implements IDSetEventListener
{
    public void init()
    {
        // студент
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, Student.class, this);
    }

    @Override
    public void onEvent(final DSetEvent event)
    {
        if (EppStudentSlotDAO.DAEMON.isCurrentThread()) {
            return; /* уже в демоне */
        }

        final Set affectedProperties = event.getMultitude().getAffectedProperties();
        if (affectedProperties.contains(Student.L_EDUCATION_ORG_UNIT) || affectedProperties.contains(Student.L_STATUS)) {
            // если изменился студент - то проверяем все связи (могли изменится НПП), если что-то поменялось - запускаем демон
            EppStudentSlotDAO.TRANSACTION_AFTER_COMPLETE_REFRESH_STUDENT_AND_WAKEUP.register(event);
        }
    }

}
