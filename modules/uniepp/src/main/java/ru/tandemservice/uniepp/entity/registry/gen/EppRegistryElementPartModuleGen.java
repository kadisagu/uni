package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Часть элемента реестра: модуль
 *
 * Определяет порядок прохождения модулей и аудиторную нагрузку для части элемента реестра
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegistryElementPartModuleGen extends EntityBase
 implements INaturalIdentifiable<EppRegistryElementPartModuleGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule";
    public static final String ENTITY_NAME = "eppRegistryElementPartModule";
    public static final int VERSION_HASH = -168569873;
    private static IEntityMeta ENTITY_META;

    public static final String L_PART = "part";
    public static final String L_MODULE = "module";
    public static final String P_NUMBER = "number";

    private EppRegistryElementPart _part;     // Часть элемента реестра
    private EppRegistryModule _module;     // Учебный модуль
    private int _number;     // Номер модуля в части

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getPart()
    {
        return _part;
    }

    /**
     * @param part Часть элемента реестра. Свойство не может быть null.
     */
    public void setPart(EppRegistryElementPart part)
    {
        dirty(_part, part);
        _part = part;
    }

    /**
     * @return Учебный модуль. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryModule getModule()
    {
        return _module;
    }

    /**
     * @param module Учебный модуль. Свойство не может быть null.
     */
    public void setModule(EppRegistryModule module)
    {
        dirty(_module, module);
        _module = module;
    }

    /**
     * Определяет последовательность чтения модулей в части дисциплины
     *
     * @return Номер модуля в части. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер модуля в части. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRegistryElementPartModuleGen)
        {
            if (withNaturalIdProperties)
            {
                setPart(((EppRegistryElementPartModule)another).getPart());
                setModule(((EppRegistryElementPartModule)another).getModule());
            }
            setNumber(((EppRegistryElementPartModule)another).getNumber());
        }
    }

    public INaturalId<EppRegistryElementPartModuleGen> getNaturalId()
    {
        return new NaturalId(getPart(), getModule());
    }

    public static class NaturalId extends NaturalIdBase<EppRegistryElementPartModuleGen>
    {
        private static final String PROXY_NAME = "EppRegistryElementPartModuleNaturalProxy";

        private Long _part;
        private Long _module;

        public NaturalId()
        {}

        public NaturalId(EppRegistryElementPart part, EppRegistryModule module)
        {
            _part = ((IEntity) part).getId();
            _module = ((IEntity) module).getId();
        }

        public Long getPart()
        {
            return _part;
        }

        public void setPart(Long part)
        {
            _part = part;
        }

        public Long getModule()
        {
            return _module;
        }

        public void setModule(Long module)
        {
            _module = module;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppRegistryElementPartModuleGen.NaturalId) ) return false;

            EppRegistryElementPartModuleGen.NaturalId that = (NaturalId) o;

            if( !equals(getPart(), that.getPart()) ) return false;
            if( !equals(getModule(), that.getModule()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPart());
            result = hashCode(result, getModule());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPart());
            sb.append("/");
            sb.append(getModule());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegistryElementPartModuleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegistryElementPartModule.class;
        }

        public T newInstance()
        {
            return (T) new EppRegistryElementPartModule();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "part":
                    return obj.getPart();
                case "module":
                    return obj.getModule();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "part":
                    obj.setPart((EppRegistryElementPart) value);
                    return;
                case "module":
                    obj.setModule((EppRegistryModule) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "part":
                        return true;
                case "module":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "part":
                    return true;
                case "module":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "part":
                    return EppRegistryElementPart.class;
                case "module":
                    return EppRegistryModule.class;
                case "number":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRegistryElementPartModule> _dslPath = new Path<EppRegistryElementPartModule>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegistryElementPartModule");
    }
            

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule#getPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> part()
    {
        return _dslPath.part();
    }

    /**
     * @return Учебный модуль. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule#getModule()
     */
    public static EppRegistryModule.Path<EppRegistryModule> module()
    {
        return _dslPath.module();
    }

    /**
     * Определяет последовательность чтения модулей в части дисциплины
     *
     * @return Номер модуля в части. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    public static class Path<E extends EppRegistryElementPartModule> extends EntityPath<E>
    {
        private EppRegistryElementPart.Path<EppRegistryElementPart> _part;
        private EppRegistryModule.Path<EppRegistryModule> _module;
        private PropertyPath<Integer> _number;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule#getPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> part()
        {
            if(_part == null )
                _part = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_PART, this);
            return _part;
        }

    /**
     * @return Учебный модуль. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule#getModule()
     */
        public EppRegistryModule.Path<EppRegistryModule> module()
        {
            if(_module == null )
                _module = new EppRegistryModule.Path<EppRegistryModule>(L_MODULE, this);
            return _module;
        }

    /**
     * Определяет последовательность чтения модулей в части дисциплины
     *
     * @return Номер модуля в части. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(EppRegistryElementPartModuleGen.P_NUMBER, this);
            return _number;
        }

        public Class getEntityClass()
        {
            return EppRegistryElementPartModule.class;
        }

        public String getEntityName()
        {
            return "eppRegistryElementPartModule";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
