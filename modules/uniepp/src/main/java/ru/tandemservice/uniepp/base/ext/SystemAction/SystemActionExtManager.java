/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;

import ru.tandemservice.uniepp.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
//        .add("uniio_exportTemplate4EduPlan", new SystemActionDefinition("uniepp", "exportTemplate4EduPlan", "onClickExportTemplate4EduPlan", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
//        .add("uniio_exportEduPlanRows", new SystemActionDefinition("uniepp", "exportEduPlanRows", "onClickExportEduPlanRows", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
//        .add("uniio_getFileNames4ImportToMdb", new SystemActionDefinition("uniepp", "getFileNames4ImportToMdb", "onClickGetFileNames4ImportToMdb", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
//        .add("uniio_importEduPlan", new SystemActionDefinition("uniepp", "importEduPlan", "onClickImportEduPlans", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        .add("uniepp_doSyncEppStudentSlots", new SystemActionDefinition("uniepp", "doSyncEppStudentSlots", "onClickSyncEppStudentSlots", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        .add("uniepp_doSyncEppRealGroupRows", new SystemActionDefinition("uniepp", "doSyncEppRealGroupRows", "onClickSyncEppRealGroupRows", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        .add("uniepp_doSyncEppRealGroupRowData", new SystemActionDefinition("uniepp", "doSyncEppRealGroupRowData", "onClickSyncEppRealGroupStudentRowData", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        //  Deprecated  .add("uniepp_fixEduPlanNumbers", new SystemActionDefinition("uniepp", "fixEduPlanNumbers", "onClickFixEduPlanNumbers", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        //  Deprecated  .add("uniepp_mergeRegistryElements", new SystemActionDefinition("uniepp", "mergeRegistryElements", "onClickMergeRegistryElements", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        .add("uniepp_showEppForTutorOrgUnits", new SystemActionDefinition("uniepp", "showEppForTutorOrgUnits", "onClickShowEppForTutorOrgUnits", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        .add("uniepp_correctEduGroupLevels", new SystemActionDefinition("uniepp", "correctEduGroupLevels", "onClickCorrectEduGroupLevels", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        /*<button name="uniepp_importEduStdBlockFromEduPlan" label="Создать блоки ГОС из УП" listener="{extension}:onClickImportEduStdBlockFromEduPlan" alert="Создать блоки ГОС из УП?"/>*/
        .add("uniepp_acceptDisciplinesActions", new SystemActionDefinition("uniepp", "acceptDisciplinesActions", "onClickAcceptDisciplinesActions", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        .add("uniepp_createSkills", new SystemActionDefinition("uniepp", "createSkills", "onClickCreateSkills", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        .add("uniepp_fixEduGroupOperationOrgUnit", new SystemActionDefinition("uniepp", "fixEduGroupOperationOrgUnit", "onClickFixEduGroupOperationOrgUnit", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        // .add("uniepp_dev5818CheckList", new SystemActionDefinition("uniepp", "dev5818CheckList", "onClickDev5818CheckList", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        .add("unieppctr_setContractRoles", new SystemActionDefinition("uniepp", "setContractRoles", "onClickSetContractRoles", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        .add("uniepp_moveWorkPlans2specializationBlock", new SystemActionDefinition("uniepp", "moveWorkPlans2specializationBlock", "onClickMoveWorkPlans2specializationBlock", SystemActionPubExt.EPP_SYSTEM_ACTION_PUB_ADDON_NAME))
        .create();
    }
}
