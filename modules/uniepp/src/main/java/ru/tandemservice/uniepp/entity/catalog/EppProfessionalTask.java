package ru.tandemservice.uniepp.entity.catalog;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask.ui.AddEdit.EppProfessionalTaskAddEdit;
import ru.tandemservice.uniepp.entity.catalog.gen.EppProfessionalTaskGen;

import java.util.Collection;

/** @see ru.tandemservice.uniepp.entity.catalog.gen.EppProfessionalTaskGen */
public class EppProfessionalTask extends EppProfessionalTaskGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    // параметры для дефолтного хэндлера
    public static final String PROP_PROGRAM_KIND = "programKind";
    public static final String PROP_PROGRAM_SUBJECT_INDEX = "programSubjectIndex";
    public static final String PROP_PROGRAM_SUBJECT = "programSubject";
    public static final String PROP_IN_DEPTH_STUDY = "inDepthStudy";
    public static final String PROP_PROF_ACTIVITY_TYPE = "profActivityType";

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EppProfessionalTask.class)
                .titleProperty(EppProfessionalTask.title().s())
                .where(EppProfessionalTask.profActivityType().programSubject().subjectIndex().programKind(), PROP_PROGRAM_KIND)
                .where(EppProfessionalTask.profActivityType().programSubject().subjectIndex(), PROP_PROGRAM_SUBJECT_INDEX)
                .where(EppProfessionalTask.profActivityType().programSubject(), PROP_PROGRAM_SUBJECT)
                .where(EppProfessionalTask.profActivityType().inDepthStudy(), PROP_IN_DEPTH_STUDY)
                .where(EppProfessionalTask.profActivityType(), PROP_PROF_ACTIVITY_TYPE)
                .order(EppProfessionalTask.priority())
                .filter(EppProfessionalTask.title());
    }

    public static IDynamicCatalogDesc getUiDesc()
    {
        return new BaseDynamicCatalogDesc()
        {
            @Override
            public String getAddEditComponentName()
            {
                return EppProfessionalTaskAddEdit.class.getSimpleName();
            }

            @Override
            public String getAdditionalPropertiesAddEditPageName()
            {
                return EppProfessionalTaskAddEdit.class.getPackage().getName() + ".AdditionalProps";
            }

            @Override
            public Collection<String> getHiddenFields()
            {
                // эти поля будут добавлены в интерфейс справочника руками (т.к. нужна их кастомизация)
                return ImmutableSet.of(L_PROF_ACTIVITY_TYPE);
            }

            @Override
            public Collection<String> getHiddenFieldsAddEditForm()
            {
                return ImmutableSet.of();
            }

            @Override
            public Collection<String> getHiddenFieldsItemPub()
            {
                return ImmutableSet.of();
            }

            @Override
            public MetaDSLPath[] getPriorityGroupFields()
            {
                return new MetaDSLPath[]{EppProfessionalTask.profActivityType().id()};
            }
        };
    }
}