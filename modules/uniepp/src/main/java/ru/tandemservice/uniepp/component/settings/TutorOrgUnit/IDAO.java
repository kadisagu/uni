package ru.tandemservice.uniepp.component.settings.TutorOrgUnit;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
    void doSwitchTutorStatus(Long listenerParameter);

    void doSwitchVisiblePlan(Long listenerParameter);

}
