// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.settings.DefaultEduplanVersionEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion;

/**
 * @author oleyba
 * @since 04.12.2010
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "educationOrgUnit.id")
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    private EducationOrgUnit educationOrgUnit = new EducationOrgUnit();
    private EppEducationOrgUnitEduPlanVersion settings;
    private EppEduPlan eduPlan;
    private EppEduPlanVersion version;

    private ISelectModel eduPlanModel;
    private ISelectModel versionModel;

    public EducationOrgUnit getEducationOrgUnit()
    {
        return this.educationOrgUnit;
    }

    public void setEducationOrgUnit(final EducationOrgUnit educationOrgUnit)
    {
        this.educationOrgUnit = educationOrgUnit;
    }

    public EppEducationOrgUnitEduPlanVersion getSettings()
    {
        return this.settings;
    }

    public void setSettings(final EppEducationOrgUnitEduPlanVersion settings)
    {
        this.settings = settings;
    }

    public EppEduPlan getEduPlan()
    {
        return this.eduPlan;
    }

    public void setEduPlan(final EppEduPlan eduPlan)
    {
        this.eduPlan = eduPlan;
    }

    public EppEduPlanVersion getVersion()
    {
        return this.version;
    }

    public void setVersion(final EppEduPlanVersion version)
    {
        this.version = version;
    }

    public ISelectModel getEduPlanModel()
    {
        return this.eduPlanModel;
    }

    public void setEduPlanModel(final ISelectModel eduPlanModel)
    {
        this.eduPlanModel = eduPlanModel;
    }

    public ISelectModel getVersionModel()
    {
        return this.versionModel;
    }

    public void setVersionModel(final ISelectModel versionModel)
    {
        this.versionModel = versionModel;
    }
}
