package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

@Configuration
public class EppEduPlanVersionBlockPub extends BusinessComponentManager
{
    public final static String BLOCK_ACTION_BUTTONS = "blockActionButtons";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

    @Bean
    public ButtonListExtPoint blockActionButtonListExtPoint()
    {
        return buttonListExtPointBuilder(BLOCK_ACTION_BUTTONS)
                .addButton(submitButton("importBlockRow").listener("onClickAddRowFromEduPlanVersion").permissionKey("editContent_eppEduPlanVersion"))
                .addButton(submitButton("setupRowOwners").listener("onClickSetupRowOwners").permissionKey("rowOwnersEdit_eppEduPlanVersion"))
                .addButton(submitButton("setupIndexes").listener("onClickSetupIndexes").permissionKey("editContent_eppEduPlanVersion"))
                .addButton(submitButton("importImtsa").listener("onClickImportImtsa").permissionKey("imtsaImport_eppEduPlanVersion").visible("ui:imtsaImportAvailable"))
                .addButton(submitButton("specifyControlHours").listener("onClickSpecifyControlHours").permissionKey("editContent_eppEduPlanVersion"))
                .addButton(submitButton("attachRegElements").listener("onClickAttachRegElements").permissionKey("attachRegElements_eppEduPlanVersion"))
                .create();
    }
}
