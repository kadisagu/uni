/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.logic;

import org.apache.tapestry.request.IUploadFile;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author azhebko
 * @since 28.10.2014
 */
public interface IImtsaImportDao extends IUniBaseDao
{

    /**
     * Импорт файла учебного плана ИМЦА.
     * @return сообщение об ошибках не мешающих импорту
     */
    String doImportImtsaEduPlan(EppEduPlanVersionBlock block, IUploadFile file);

    /** "Продвинутый" режим импорта: данные импортируются как есть, ошибки игнорируются. */
    public boolean advancedModeEnabled();
}