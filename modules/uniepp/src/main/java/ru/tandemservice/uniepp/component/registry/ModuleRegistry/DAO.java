package ru.tandemservice.uniepp.component.registry.ModuleRegistry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.util.SafeSimpleColumn;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.component.registry.GlobalOwnerSelectModel;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    private final DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(EppRegistryModule.class, "e");

    @Override
    public void prepare(final Model model) {
        model.getOrgUnitHolder().refresh();
        model.setStateListModel(UniEppUtils.getStateSelectModel(null));
        model.setOwnerSelectModel(new GlobalOwnerSelectModel(
            new DQLSelectBuilder()
            .fromEntity(EppRegistryModule.class, "r")
            .column(property(EppRegistryModule.owner().id().fromAlias("r")))
            .buildQuery()
        ));

        String postfix = "eppModule_list";
        if (model.getOrgUnit() != null) {
            postfix = postfix + "_" + StringUtils.uncapitalize(model.getOrgUnit().getOrgUnitType().getCode());
        }
        model.setSec(
            new CommonPostfixPermissionModel(postfix).addException(
                "view",
                new CommonPostfixPermissionModel.IResolver() {
                    @Override public String getPermissionKey(final String propertyName) {
                        if (model.getOrgUnit() != null) {
                            return "";
                        }
                        return "menuEppModuleRegistry";
                    }
                }
            )
        );
    }

    @Override
    public void prepareLoadColumns(final Model model, final DynamicListDataSource dataSource) {
        {
            final HeadColumn h = new HeadColumn("aload", "Нагрузка");
            for (final EppALoadType t: this.getCatalogItemListOrderByCode(EppALoadType.class)) {
                h.addColumn(new SafeSimpleColumn(t.getShortTitle(), t.getFullCode()).setWidth(1).setVerticalHeader(true).setRequired(true));
            }
            if (h.getColumns().size() > 0) {
                //dataSource.addColumn(new SeparatorColumn(h.getName()+".separator").setOrderable(false));
                dataSource.addColumn(h);
            }
        }
        {
            final HeadColumn h = new HeadColumn("caction", "Контроль");
            for (final EppIControlActionType t: this.getCatalogItemListOrderByCode(EppIControlActionType.class)) {
                h.addColumn(new SafeSimpleColumn(t.getShortTitle(), t.getFullCode()).setWidth(1).setVerticalHeader(true).setRequired(true));
            }
            if (h.getColumns().size() > 0) {
                //dataSource.addColumn(new SeparatorColumn(h.getName()+".separator").setOrderable(false));
                dataSource.addColumn(h);
            }
        }
    }

    @Override
    public void prepareDataSource(final Model model) {

        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryModule.class, "e");

        if (null != model.getOrgUnit()) {
            // на подразделении нет фильтра
            builder.where(eq(property(EppRegistryModule.owner().fromAlias("e")), value(model.getOrgUnit())));
        } else {
            // иначе - учитываем фильтр
            final Object owner = model.getSettings().get("owner");
            if (owner instanceof OrgUnit) {
                builder.where(eq(property(EppRegistryModule.owner().fromAlias("e")), commonValue(owner)));
            }
        }

        Object title = model.getSettings().get("title");
        if (title instanceof String) {
            title = CoreStringUtils.escapeLike((String)title, true);
            builder.where(or(
                like(DQLFunctions.upper(property(EppRegistryModule.title().fromAlias("e"))), commonValue(title)),
                like(DQLFunctions.upper(property(EppRegistryModule.fullTitle().fromAlias("e"))), commonValue(title)),
                like(DQLFunctions.upper(property(EppRegistryModule.shortTitle().fromAlias("e"))), commonValue(title)),
                like(DQLFunctions.upper(property(EppRegistryModule.number().fromAlias("e"))), commonValue(title))
            ));
        }

        final Object state = model.getSettings().get("state");
        if (state instanceof EppState) {
            builder.where(eq(property(EppRegistryModule.state().fromAlias("e")), commonValue(state)));
        }


        this.applyOrder(model, builder, "e");
        UniBaseUtils.createPage(model.getDataSource(), builder, this.getSession());

        final List<ViewWrapper<IEntity>> wrappers = ViewWrapper.getPatchedList(model.getDataSource());
        BatchUtils.execute(wrappers, 100, new BatchUtils.Action<ViewWrapper<IEntity>>() {
            @Override public void execute(final Collection<ViewWrapper<IEntity>> elements) {
                final Map<Long, ViewWrapper<IEntity>> map = CommonDAO.map(elements);

                for (final EppRegistryModuleALoad al: DAO.this.getList(EppRegistryModuleALoad.class, EppRegistryModuleALoad.module().id(), map.keySet())) {
                    map.get(al.getModule().getId()).setViewProperty(al.getLoadType().getFullCode(), UniEppUtils.formatLoad(al.getLoadAsDouble(), true));
                }

                for (final EppRegistryModuleIControlAction ca: DAO.this.getList(EppRegistryModuleIControlAction.class, EppRegistryModuleIControlAction.module().id(), map.keySet())) {
                    map.get(ca.getModule().getId()).setViewProperty(ca.getControlAction().getFullCode(), (ca.getAmount() > 0 ? String.valueOf(ca.getAmount()) : ""));
                }
            }
        });

    }

    protected void applyOrder(final Model model, final DQLSelectBuilder builder, final String alias) {
        final EntityOrder entityOrder = model.getDataSource().getEntityOrder();
        this.registry.applyOrder(builder, entityOrder);
        if (!EppRegistryModule.P_TITLE.equals(entityOrder.getColumnName())) {
            builder.order(property(alias, EppRegistryModule.P_TITLE));
        }
    }

}
