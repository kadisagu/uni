package ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.gen.TermGen;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uni.entity.education.gen.DevelopGridTermGen;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.gen.EppStateGen;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearEducationProcessGen;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.concat;

/**
 * 
 * @author nkokorina
 * @since 16.03.2010
 */

public class DAO extends UniBaseDao implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        final IEntity entity = (null == model.getContextId() ? null : this.get(model.getContextId()));
        model.setProgramKindModel(new LazySimpleSelectModel<>(getList(EduProgramKind.class, EduProgramKind.priority().s())));

        if (entity instanceof EppWorkPlan) {
            model.setElement((EppWorkPlan)entity);

            final EppWorkPlan wplan = model.getElement();
            model.setYearEducationProcessListModel(new LazySimpleSelectModel<>(ImmutableList.of(wplan.getYear())));
            model.setTermListModel(new LazySimpleSelectModel<>(ImmutableList.of(wplan.getTerm())));
            model.setEduPlanVersionModel(new LazySimpleSelectModel<>(ImmutableList.of(wplan.getEduPlanVersion())));
            model.setEduPlanVersionBlockModel(new LazySimpleSelectModel<>(ImmutableList.of(wplan.getBlock())));

            model.setProgramKind(wplan.getEduPlan().getProgramKind());
            model.setEduPlanVersion(wplan.getEduPlanVersion());
            model.setEduPlanVersionBlock(wplan.getBlock());

            if (wplan.getEduPlan() instanceof EppEduPlanProf) {
                EduProgramSubject subject = ((EppEduPlanProf)wplan.getEduPlan()).getProgramSubject();
                model.setProgramSubject(subject);
                model.setProgramSubjectModel(new LazySimpleSelectModel<>(ImmutableList.of(subject)));
            }
        }
        else {
            model.setElement(new EppWorkPlan());

            // устанавливаем состояние по умолчанию
            model.getElement().setState(this.get(EppState.class, EppStateGen.P_CODE, EppState.STATE_FORMATIVE));

            model.setYearEducationProcessListModel(new LazySimpleSelectModel<>(getList(EppYearEducationProcess.class, EppYearEducationProcessGen.educationYear().intValue().s())));

            List<DevelopCombination> developCombinationList = getList(DevelopCombination.class);
            Collections.sort(developCombinationList, DevelopCombination.COMPARATOR);
            model.setDevelopCombinationModel(new LazySimpleSelectModel<>(developCombinationList));

            model.setProgramSubjectModel(new DQLFullCheckSelectModel(EduProgramSubject.titleWithCode().s()) {
                @Override protected DQLSelectBuilder query(String alias, String filter) {
                    DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(EduProgramSubject.class, alias)
                        .order(property(alias, EduProgramSubject.subjectCode()))
                        .order(property(alias, EduProgramSubject.title()))
                        ;

                    FilterUtils.applySelectFilter(dql, alias, EduProgramSubject.subjectIndex().programKind(), model.getProgramKind());
                    FilterUtils.applyLikeFilter(dql, filter, EduProgramSubject.subjectCode().fromAlias(alias), EduProgramSubject.title().fromAlias(alias));

                    dql.where(exists(new DQLSelectBuilder()
                        .fromEntity(EppEduPlanProf.class, "p").column("p")
                        .where(eq(property("p", EppEduPlanProf.programSubject()), property(alias)))
                        .buildQuery()));

                    return dql;
                }
            });

            model.setEduPlanVersionModel(new DQLFullCheckSelectModel() {
                @Override protected DQLSelectBuilder query(String alias, String filter) {
                    DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(EppEduPlanVersion.class, alias)
                        .order(property(alias, EppEduPlanVersion.eduPlan().number()))
                        .order(property(alias, EppEduPlanVersion.number()))
                        ;

                    if (StringUtils.isNotEmpty(filter)) {
                        dql.where(likeUpper(concat(property(alias, EppEduPlanVersion.eduPlan().number()), value("."), property(alias, EppEduPlanVersion.number())),
                                            value(CoreStringUtils.escapeLike(filter))));
                    }

                    if (model.getProgramSubject() != null) {
                        dql.where(exists(new DQLSelectBuilder()
                            .fromEntity(EppEduPlanProf.class, "p").column("p")
                            .where(eq(property("p"), property(alias, EppEduPlanVersion.eduPlan())))
                            .where(eq(property("p", EppEduPlanProf.programSubject()), value(model.getProgramSubject())))
                            .buildQuery()));
                    }

                    if (model.getDevelopCombination() != null) {
                        dql.where(eq(property(alias, EppEduPlanVersion.eduPlan().programForm()), value(model.getDevelopCombination().getDevelopForm().getProgramForm())));
                        dql.where(eq(property(alias, EppEduPlanVersion.eduPlan().developCondition()), value(model.getDevelopCombination().getDevelopCondition())));
                        dql.where(eqNullSafe(property(alias, EppEduPlanVersion.eduPlan().programTrait()), value(model.getDevelopCombination().getDevelopTech().getProgramTrait())));
                        dql.where(eq(property(alias, EppEduPlanVersion.eduPlan().developGrid()), value(model.getDevelopCombination().getDevelopGrid())));
                    }

                    return dql;
                }
            });

            model.setTermListModel(new UniQueryFullCheckSelectModel() {
                @Override protected MQBuilder query(final String alias, final String filter)
                {
                    if (model.getEduPlanVersion() == null) return null;

                    final EppYearEducationProcess year = model.getElement().getYear();
                    if (null == year) { return null; }

                    final EppCustomEduPlan customEduPlan = model.getElement().getCustomEduPlan();
                    final DevelopGrid developGrid = customEduPlan != null ? customEduPlan.getDevelopGrid() : model.getEduPlanVersion().getEduPlan().getDevelopGrid();

                    final MQBuilder termBuilder = new MQBuilder(DevelopGridTermGen.ENTITY_NAME, "dgt", new String[] { DevelopGridTermGen.term().id().s() });

                    termBuilder.add(MQExpression.eq("dgt", DevelopGridTermGen.developGrid().s(), developGrid));

                    final MQBuilder builder = new MQBuilder(TermGen.ENTITY_CLASS, alias);
                    builder.add(MQExpression.in(alias, "id", termBuilder));
                    builder.addOrder(alias, TermGen.intValue().s());
                    return builder;
                }
            });

            model.setEduPlanVersionBlockModel(new DQLFullCheckSelectModel()
            {
                @Override
                protected DQLSelectBuilder query(String alias, String filter)
                {
                    return new DQLSelectBuilder()
                        .fromEntity(EppEduPlanVersionBlock.class, alias)
                        .where(eqNullSafe(property(alias, EppEduPlanVersionBlock.eduPlanVersion()), value(model.getEduPlanVersion())));
                }

                @Override protected void sortOptions(List list) {
                    Collections.sort(list, EppEduPlanVersionBlock.COMPARATOR);
                }
            });

            // устанавливаем год из настроек, если он есть
            final int number = (null == model.getSelectedYearNumber() ? 0 : model.getSelectedYearNumber());
            model.getElement().setYear((EppYearEducationProcess)CollectionUtils.find(this.getList(EppYearEducationProcess.class, EppYearEducationProcessGen.educationYear().intValue().s()), object -> ((EppYearEducationProcess)object).getEducationYear().getIntValue() >= number));
            this.onChangeEppYear(model);
        }

        model.setCustomEduPlanModel(new DQLFullCheckSelectModel(EppCustomEduPlan.titleWithGrid()) {
            @Override protected DQLSelectBuilder query(String alias, String filter) {
                return new DQLSelectBuilder()
                        .fromEntity(EppCustomEduPlan.class, alias)
                        .where(eq(property(alias, EppCustomEduPlan.epvBlock()), value(model.getEduPlanVersionBlock())));
            }
            @Override protected void sortOptions(List list) {
                Collections.sort(list, ITitled.TITLED_COMPARATOR);
            }
        });

    }

    @Override
    public void onChangeEppYear(final Model model) {
        model.getElement().setNumber(null);
        if (null != model.getElement().getYear()) {
            model.getElement().setNumber(INumberQueueDAO.instance.get().getNextNumber(model.getElement()));
        }
    }

    @Override
    public void validate(final Model model, final ErrorCollector errors)
    {
    }

    @Override
    public void save(final Model model)
    {
        final Session session = this.getSession();
        EppWorkPlan wp = model.getElement();
        if (null == wp.getParent()) {
            wp.setParent(model.getEduPlanVersionBlock());
        }
        session.saveOrUpdate(wp);

        // Создаем часть года
        IEppWorkPlanDAO.instance.get().doCreateEppYearPartToWP(wp);
    }

}
