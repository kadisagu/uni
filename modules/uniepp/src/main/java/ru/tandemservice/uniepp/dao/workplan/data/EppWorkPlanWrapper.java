package ru.tandemservice.uniepp.dao.workplan.data;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.tandemframework.core.entity.EntityBase;

import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

/**
 * @author vdanilov
 */
public class EppWorkPlanWrapper extends EntityBase implements IEppWorkPlanWrapper {
    protected static final Logger logger = Logger.getLogger(EppWorkPlanWrapper.class);

    private final EppWorkPlanBase workPlan;
    @Override public EppWorkPlanBase getWorkPlan() { return this.workPlan; }

    private final Map<Long, IEppWorkPlanRowWrapper> rowMap = new LinkedHashMap<Long, IEppWorkPlanRowWrapper>();
    @Override public Map<Long, IEppWorkPlanRowWrapper> getRowMap() { return this.rowMap ; }

    @Override public String getTitle() { return this.workPlan.getTitle(); }

    public EppWorkPlanWrapper(final EppWorkPlanBase workPlan) {
        this.workPlan = workPlan;
    }

    @Override public Long getId() { return this.workPlan.getId(); }

    @Override public void setId(final Long id) {
        throw new UnsupportedOperationException();
    }
    @Override public void setProperty(final String propertyName, final Object propertyValue) {
        throw new UnsupportedOperationException();
    }
    @Override public Object getProperty(final Object propertyPath) {
        try {
            return super.getProperty(propertyPath);
        } catch(final Throwable t) {
            return null;
        }
    }


    private Set<String> _description_set_cache = null;

    @Override
    public Set<String> getDescriptionSet() {
        if (null != this._description_set_cache) { return this._description_set_cache; }
        final Set<String> set = new HashSet<String>(this.rowMap.size());
        for (final IEppWorkPlanRowWrapper wrapper: this.rowMap.values()) {
            set.add(wrapper.getDescriptionString());
        }
        return (this._description_set_cache = set );
    }

    /** закрывает враппер, убивает все данные (в помощь GC) */
    public void close() {
        for (final IEppWorkPlanRowWrapper wrapper: this.rowMap.values()) {
            ((EppWorkPlanRowWrapper)wrapper).close();
        }

        this.rowMap.clear();

        if (null != this._description_set_cache) {
            this._description_set_cache.clear();
            this._description_set_cache = null;
        }
    }

}
