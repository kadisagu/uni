/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.tapestry.richTableList;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.IEditableRichDataSourceModel;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.RichDataSourceModel;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;

import java.util.Map;

/**
 * @author vip_delete
 * @since 17.03.2010
 */
@Zlo
public class RangeSelectionWeekTypeListDataSource<T extends IEntity> extends RichDataSourceModel<T> implements IEditableRichDataSourceModel
{
    private Long _editId;                                    // редактируемая строка
    private Map<PairKey<Long, Long>, EppWeekType> _dataMap;  // мап значений таблицы
    private RichRangeSelection _selection;                   // карты текущей строки
    private Map<Long, int[]> _row2points;
    private Map<Long, int[]> _row2data;

    @SuppressWarnings("unchecked")
    public RangeSelectionWeekTypeListDataSource(final AbstractListDataSource dataSource) {
        super(dataSource);
    }

    private final Map<String, EppWeekType> weekTypeMap = IEppSettingsDAO.instance.get().getWeekTypeEditAbbreviationMap();
    public Map<String, EppWeekType> getWeekTypeMap() {
        return this.weekTypeMap;
    }

    public EppWeekType getWeekType(final String abbreviationEdit) throws ValidatorException {
        final String value = StringUtils.trimToNull(abbreviationEdit);
        if (null == value) { return null; }
        final EppWeekType weekType = this.getWeekTypeMap().get(value.toUpperCase());
        if (null == weekType) {
            throw new ValidatorException("Неизвестный тип недели «" + value + "»");
        }
        return weekType;
    }

    // Getters & Setters

    @Override
    public Long getEditId() {
        return this._editId;
    }

    public void setEditId(final Long editId) {
        this._editId = editId;
    }

    public Map<PairKey<Long, Long>, EppWeekType> getDataMap() {
        return this._dataMap;
    }

    public void setDataMap(final Map<PairKey<Long, Long>, EppWeekType> dataMap) {
        this._dataMap = dataMap;
    }

    public RichRangeSelection getSelection() {
        return this._selection;
    }

    public void setSelection(final RichRangeSelection selection) {
        this._selection = selection;
    }

    public Map<Long, int[]> getRow2points() {
        return this._row2points;
    }

    public void setRow2points(final Map<Long, int[]> row2points) {
        this._row2points = row2points;
    }

    public Map<Long, int[]> getRow2data() {
        return this._row2data;
    }

    public void setRow2data(final Map<Long, int[]> row2data) {
        this._row2data = row2data;
    }
}
