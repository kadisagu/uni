package ru.tandemservice.uniepp.dao.eduStd;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

public interface IEppEduStdDAO
{
    public static final SpringBeanCache<IEppEduStdDAO> instance = new SpringBeanCache<IEppEduStdDAO>(IEppEduStdDAO.class.getName());

    /**
     * Обновляет блоки в версии УП
     *
     * @param eduStdId - id ГОС
     * @param eduLevList       - список уровней образования
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateBlockList(Long eduStdId, Collection<EduProgramSpecialization> specializations);

    /** @return правило нумерования ГОС */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public INumberGenerationRule<EppStateEduStandard> getNumberGenerationRule();

    /**
     * 
     * @param eduStdId - id ГОС
     * @return отображать ли таб с компетенциями
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isEduStdCompetenceTabVisible(final Long eduStdId);

    /**
     * @param educationLevel
     * @return поколение ГОС по умолчанию для уровня
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public EppGeneration getGeneration(final EduProgramSubject programSubject);
}
