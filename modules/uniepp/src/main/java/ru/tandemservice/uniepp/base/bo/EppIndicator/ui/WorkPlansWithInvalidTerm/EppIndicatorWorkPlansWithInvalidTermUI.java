/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.WorkPlansWithInvalidTerm;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.util.IYearEducationProcessSelectModel;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 06.11.2014
 */
public class EppIndicatorWorkPlansWithInvalidTermUI extends UIPresenter implements IYearEducationProcessSelectModel
{
    private List<EppYearEducationProcess> _yearEducationProcessList;

    private ISelectModel _programSubjectModel;
    private ISelectModel _programFormListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _programTraitListModel;

    private ISelectModel _termListModel;
    private ISelectModel _stateListModel;
    private ISelectModel _eduProgramKindModel;
    private ISelectModel _programSpecializationModel;

    @Override
    public void onComponentRefresh()
    {
        // Далее копипаста из реестра РУП

        _eduProgramKindModel = new LazySimpleSelectModel<>(EduProgramKind.class);

        _yearEducationProcessList = IUniBaseDao.instance.get().getList(EppYearEducationProcess.class, EppYearEducationProcess.educationYear().intValue().s());
/*
        IUniBaseDao.instance.get().getCalculatedValue(new HibernateCallback<Object>()
        {
            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException
            {
                EppFilterUtil.prepareYearEducationProcessFilter(EppIndicatorWorkPlansWithInvalidTermUI.this, session);
                return null;
            }
        });
*/

        _programSubjectModel = new DQLFullCheckSelectModel(EduProgramSubject.titleWithCode().s())
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(EduProgramSubject.class, alias)
                        .order(property(alias, EduProgramSubject.subjectCode()))
                        .order(property(alias, EduProgramSubject.title()));

                FilterUtils.applyLikeFilter(dql, filter, EduProgramSubject.subjectCode().fromAlias(alias), EduProgramSubject.title().fromAlias(alias));

                DQLSelectBuilder wpDql = new DQLSelectBuilder()
                        .fromEntity(EppEduPlanProf.class, "p")
                        .fromEntity(EppWorkPlan.class, "w")
                        .where(eq(property("w", EppWorkPlan.parent().eduPlanVersion().eduPlan()), property("p")))
                        .where(eq(property("p", EppEduPlanProf.programSubject()), property(alias)));
                FilterUtils.applySelectFilter(wpDql, "w", EppWorkPlan.year(), getSettings().get("yearEducationProcess"));
                dql.where(exists(wpDql.buildQuery()));

                return dql;
            }
        };

        _programSpecializationModel = new DQLFullCheckSelectModel(EduProgramSpecialization.title().s()){
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(EduProgramSpecialization.class, "e")
                        .order(property("e", EduProgramSpecialization.title()))
                        .where(eq(property("e", EduProgramSpecialization.programSubject()), commonValue(getSettings().get("programSubject"))))
                        ;

                FilterUtils.applySimpleLikeFilter(dql, "e", EduProgramSpecialization.title(), filter);

                return dql;
            }
        };

        _programFormListModel = new LazySimpleSelectModel<>(EduProgramForm.class);
        _developConditionListModel = new LazySimpleSelectModel<>(IUniBaseDao.instance.get().getCatalogItemListOrderByCode(DevelopCondition.class));
        _programTraitListModel = new LazySimpleSelectModel<>(EduProgramTrait.class);
        _termListModel = new LazySimpleSelectModel<>(DevelopGridDAO.getTermMap().values());
        _stateListModel = UniEppUtils.getStateSelectModel(null);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppIndicatorWorkPlansWithInvalidTerm.WORK_PLAN_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(getSettings().getAsMap(
                    "programKind", "term", "state", "number", "yearEducationProcess", "programSubject",
                    "programSpecializations", "programFormList", "developConditionList",  "programTraitList"
            ));
        }
    }

    @Override
    public void setYearEducationProcessList(List<EppYearEducationProcess> yearEducationProcessList)
    {
        _yearEducationProcessList = yearEducationProcessList;
    }

    @Override
    public EppYearEducationProcess getYearEducationProcess()
    {
        final Object object = this.getSettings().get(IYearEducationProcessSelectModel.YEAR_EDUCATION_PROCESS_FILTER_NAME);
        return ((object instanceof EppYearEducationProcess) ? IUniBaseDao.instance.get().get(EppYearEducationProcess.class, ((EppYearEducationProcess)object).getId()) : null);
    }

    @Override
    public void setYearEducationProcess(EppYearEducationProcess yearEducationProcess)
    {
        this.getSettings().set(IYearEducationProcessSelectModel.YEAR_EDUCATION_PROCESS_FILTER_NAME, yearEducationProcess);
    }

    public List<EppYearEducationProcess> getYearEducationProcessList()
    {
        return _yearEducationProcessList;
    }

    public ISelectModel getProgramSubjectModel()
    {
        return _programSubjectModel;
    }

    public ISelectModel getProgramFormListModel()
    {
        return _programFormListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public ISelectModel getProgramTraitListModel()
    {
        return _programTraitListModel;
    }

    public ISelectModel getTermListModel()
    {
        return _termListModel;
    }

    public ISelectModel getStateListModel()
    {
        return _stateListModel;
    }

    public ISelectModel getEduProgramKindModel()
    {
        return _eduProgramKindModel;
    }

    public ISelectModel getProgramSpecializationModel()
    {
        return _programSpecializationModel;
    }
}