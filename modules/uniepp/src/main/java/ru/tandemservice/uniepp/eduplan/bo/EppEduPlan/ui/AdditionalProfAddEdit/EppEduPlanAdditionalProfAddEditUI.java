/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.AdditionalProfAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.EppEduPlanManager;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.Pub.EppEduPlanPub;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanAdditionalProf;

/**
 * @author oleyba
 * @since 9/4/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "element.id"),
    @Bind(key = EppEduPlanManager.BIND_PROGRAM_KIND_ID, binding = "programKindId"),
    @Bind(key = EppEduPlanManager.BIND_ORG_UNIT_ID, binding = "orgUnitId")
})
public class EppEduPlanAdditionalProfAddEditUI extends UIPresenter
{
    private EppEduPlanAdditionalProf element = new EppEduPlanAdditionalProf();
    private DevelopCombination developCombination;

    private Long programKindId;
    private Long orgUnitId;

    @Override
    public void onComponentRefresh()
    {
        if (getElement().getId() != null) {
            setElement(DataAccessServices.dao().get(EppEduPlanAdditionalProf.class, getElement().getId()));
            setProgramKindId(getElement().getProgramKind().getId());
        } else {
            getElement().setNumber(INumberQueueDAO.instance.get().getNextNumber(getElement()));
            if (getOrgUnitId() != null && getElement().getOwner() == null) {
                getElement().setOwner(DataAccessServices.dao().get(OrgUnit.class, getOrgUnitId()));
            }
        }

        if (isEditForm() && (null != getElement().getState()) && (getElement().getState().isReadOnlyState())) {
            throw new ApplicationException("Учебный план находится в состоянии «" + getElement().getState().getTitle() + "», редактирование запрещено.");
        }
    }

    public boolean isEditForm() {
        return ((null != getElement()) && (null != getElement().getId()));
    }

    public boolean isDevelopCombinationPartsDisabled()
    {
        return this.isEditForm() || (null != this.getDevelopCombination());
    }

    public void onClickApply() {
        if (getElement().getProgramKind() == null) {
            getElement().setProgramKind(DataAccessServices.dao().get(EduProgramKind.class, getProgramKindId()));
        }
        if (getElement().getState() == null) {
            getElement().setState(DataAccessServices.dao().get(EppState.class, EppState.code(), EppState.STATE_FORMATIVE));
        }
        boolean justCreated = ! isEditForm();
        DataAccessServices.dao().saveOrUpdate(getElement());
        if (justCreated)
            _uiActivation.asDesktopRoot(EppEduPlanPub.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getElement().getId()).activate();
        deactivate();
    }

    public void onChangeCombination()
    {
        if (getDevelopCombination() != null) {
            getElement().setProgramForm(getDevelopCombination().getDevelopForm().getProgramForm());
            getElement().setDevelopCondition(getDevelopCombination().getDevelopCondition());
            getElement().setProgramTrait(getDevelopCombination().getDevelopTech().getProgramTrait());
            getElement().setDevelopGrid(getDevelopCombination().getDevelopGrid());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EppEduPlanAdditionalProfAddEdit.BIND_PROGRAM_KIND_ID, getProgramKindId());
    }

    // getters and setters

    public EppEduPlanAdditionalProf getElement()
    {
        return element;
    }

    public void setElement(EppEduPlanAdditionalProf element)
    {
        this.element = element;
    }

    public DevelopCombination getDevelopCombination()
    {
        return developCombination;
    }

    public void setDevelopCombination(DevelopCombination developCombination)
    {
        this.developCombination = developCombination;
    }

    public Long getProgramKindId()
    {
        return programKindId;
    }

    public void setProgramKindId(Long programKindId)
    {
        this.programKindId = programKindId;
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }
}