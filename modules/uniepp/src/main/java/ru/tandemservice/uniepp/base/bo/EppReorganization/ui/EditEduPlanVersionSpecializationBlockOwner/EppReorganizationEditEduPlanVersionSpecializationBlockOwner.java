/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.ui.EditEduPlanVersionSpecializationBlockOwner;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;

/**
 * @author rsizonenko
 * @since 05.04.2016
 */
@Configuration
public class EppReorganizationEditEduPlanVersionSpecializationBlockOwner extends BusinessComponentManager {

    public static final String OWNER_DS = "ownerDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(OWNER_DS, getName(), EduOwnerOrgUnit.defaultDSHandler(getName())))
                .create();
    }

}
