/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.StructRowAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.BaseAddEditDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAddEditBase.EppEduPlanVersionRowAddEditBaseUI;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 11/12/14
 */
@Input({
    @Bind(key = EppEduPlanVersionRowAddEditBaseUI.BIND_ROW_ID, binding = "row.id"),
    @Bind(key = EppEduPlanVersionRowAddEditBaseUI.BIND_BLOCK_ID, binding = EppEduPlanVersionRowAddEditBaseUI.FIELD_BLOCK_ID),
})
@Return({
    @Bind(key = "scroll", binding = "scroll"),
    @Bind(key = "scrollPos", binding = "scrollPos"),
    @Bind(key = "highlightRowId", binding = "row.id")
})
public class EppEduPlanVersionStructRowAddEditUI extends EppEduPlanVersionRowAddEditBaseUI<EppEpvStructureRow>
{
    private EppEpvStructureRow row = new EppEpvStructureRow();

    private List<HSelectOption> valueList = Collections.emptyList();

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        final List<HSelectOption> parentList = IEppEduPlanVersionDataDAO.instance.get().getEpvRowHierarchyListFromBlock(getBlock(), EppEpvStructureRow.PARENT_PREDICATE);
        BaseAddEditDao.disablePossibleLoop(parentList, Collections.singleton(getRow().getId()));
        setParentList(parentList);

        onChangeParent();
    }

    @Override
    public void onChangeParent() {
        final Set<Long> usedValueIds = this.getUsedValueIds();
        final List<HSelectOption> valueList = IEppEduPlanVersionDataDAO.instance.get().getPlanStructureHierarchyList4Add(getBlock());
        for (final HSelectOption option: valueList) {
            option.setCanBeSelected(option.isCanBeSelected() && (!usedValueIds.contains(((IEntity)option.getObject()).getId())));
        }
        setValueList(valueList);
    }

    // utils

    private Set<Long> getUsedValueIds() {
        final EppEpvStructureRow row = getRow();

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EppEpvStructureRow.class, "r").column(property("r", EppEpvStructureRow.value().id()))
            .where(eq(property("r", EppEpvStructureRow.owner().eduPlanVersion()), value(row.getOwner().getEduPlanVersion())))
            .where(or(
                eq(property("r", EppEpvStructureRow.owner()), value(row.getOwner())),
                instanceOf(EppEpvStructureRow.owner().fromAlias("r").s(), EppEduPlanVersionRootBlock.class)
            ));

        if (getRow().getParent() != null) {
            dql.where(eq(property("r", EppEpvStructureRow.parent()), value(getRow().getParent())));
        } else {
            dql.where(isNull(property("r", EppEpvStructureRow.parent())));
        }


        return new HashSet<Long>(DataAccessServices.dao().<Long>getList(dql));
    }

    // getters and setters

    @Override
    public EppEpvStructureRow getRow()
    {
        return row;
    }

    @Override
    public void setRow(EppEpvStructureRow row)
    {
        this.row = row;
    }

    public List<HSelectOption> getValueList()
    {
        return valueList;
    }

    public void setValueList(List<HSelectOption> valueList)
    {
        this.valueList = valueList;
    }
}