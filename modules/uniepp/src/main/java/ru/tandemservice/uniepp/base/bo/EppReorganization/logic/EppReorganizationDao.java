/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.logic;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.ReorganizationHistoryItem;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 31.03.2016
 */
public class EppReorganizationDao extends UniBaseDao implements IEppReorganizationDao {

    @Override
    public void doChangeEppRegistryElementOwner(Collection<EppRegistryElement> input, OrgUnit newOwner) {

        for (EppRegistryElement element : input) {

            if (element.getOwner().getId().equals(newOwner.getId())) continue;

            OrgUnit oldOwner = element.getOwner();
            ReorganizationHistoryItem historyItem = new ReorganizationHistoryItem();
            historyItem.setObject(element);
            historyItem.setOldElement(oldOwner);
            historyItem.setNewElement(newOwner);
            historyItem.setFieldName("owner");
            historyItem.setModifyDate(new Date());

            final DQLSelectBuilder modulesBuilder = new DQLSelectBuilder().fromEntity(EppRegistryModule.class, "regModule")
                    .where(exists(
                            new DQLSelectBuilder()
                                    .fromEntity(EppRegistryElementPartModule.class, "e")
                                    .joinPath(DQLJoinType.inner, EppRegistryElementPartModule.module().fromAlias("e"), "module")
                                    .joinPath(DQLJoinType.inner, EppRegistryElementPartModule.part().fromAlias("e"), "part")
                                    .joinPath(DQLJoinType.inner, EppRegistryModule.owner().fromAlias("module"), "moduleOwner")
                                    .joinPath(DQLJoinType.inner, EppRegistryElementPart.registryElement().fromAlias("part"), "element")
                                    .where(eq(property("element" ,EppRegistryElement.id()), value(element.getId())))
                                    .where(eq(property("moduleOwner", OrgUnit.id()), value(oldOwner.getId())))
                                    .where(eq(property("module", EppRegistryModule.id()), property("regModule", EppRegistryModule.id())))
                                    .buildQuery()
                    ));



            final List<EppRegistryModule> registryModules = getList(modulesBuilder);
            final List<EppEpvRegistryRow> registryRows = getList(EppEpvRegistryRow.class, EppEpvRegistryRow.registryElement(), element);

            try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler()))
            {
                element.setOwner(newOwner);
                save(historyItem);
                update(element);

                for (EppRegistryModule registryModule : registryModules) {
                    registryModule.setOwner(newOwner);
                    update(registryModule);
                }

                for (EppEpvRegistryRow registryRow : registryRows) {
                    registryRow.setRegistryElementOwner(newOwner);
                    update(registryRow);
                }

                getSession().flush();
            }
        }

    }


    @Override
    public void doChangeEduPlanVersionSpecializationBlockOwner(Collection<EppEduPlanVersionSpecializationBlock> input, OrgUnit owner) {

        EduOwnerOrgUnit eduOwnerOrgUnit = getByNaturalId(new EduOwnerOrgUnit.NaturalId(owner));

        try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler()))

        {

            for (EppEduPlanVersionSpecializationBlock block : input) {

                if (block.getOwnerOrgUnit().getId().equals(eduOwnerOrgUnit.getId())) continue;


                ReorganizationHistoryItem historyItem = new ReorganizationHistoryItem();
                historyItem.setObject(block);
                historyItem.setOldElement(block.getOwnerOrgUnit().getOrgUnit());
                historyItem.setNewElement(owner);
                historyItem.setFieldName("ownerOrgUnit");
                historyItem.setModifyDate(new Date());

                block.setOwnerOrgUnit(eduOwnerOrgUnit);


                update(block);
                save(historyItem);

            }

            getSession().flush();
        }

    }
}
