package ru.tandemservice.uniepp.component.workplan.WorkPlanDisciplineAddFromEduPlan;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

import java.util.Date;

@Input( {
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "workPlanHolder.id"),
        @Bind(key = Model.BIND_IS_ACTIONS, binding = "isActions")
})
@SuppressWarnings("unchecked")
public class Model
{
    public static final String BIND_IS_ACTIONS = "actions"; // Доп. мероприятия (ГИА / практики)

    private final EntityHolder<EppWorkPlanBase> workPlanHolder = new EntityHolder<EppWorkPlanBase>();
    public EntityHolder<EppWorkPlanBase> getWorkPlanHolder() { return this.workPlanHolder; }
    public Long getId() { return this.getWorkPlanHolder().getId(); }

    private StaticListDataSource<IEppEpvRowWrapper> rowDataSource = new StaticListDataSource<IEppEpvRowWrapper>();
    public StaticListDataSource<IEppEpvRowWrapper> getRowDataSource() { return this.rowDataSource; }
    public void setRowDataSource(final StaticListDataSource<IEppEpvRowWrapper> rowDataSource) { this.rowDataSource = rowDataSource; }

    public String getTitle() {
        return UniEppUtils.getEppWorkPlanRowFormTitle(this.getId());
    }

    public SelectModel<Integer> getTermSelectModel() {
        final BlockColumn<SelectModel<Integer>> column = (BlockColumn<SelectModel<Integer>>) this.getRowDataSource().getCurrentColumn();
        return column.getValueMap().get(this.getRowDataSource().getCurrentEntity().getId());
    }

    private boolean isActions;
    public boolean isActions() { return isActions;} // Доп. мероприятия (ГИА / практики)
    public void setIsActions(boolean isActions) { this.isActions = isActions; }

    public Date getDate() {
        final BlockColumn<Date> column = (BlockColumn<Date>) this.getRowDataSource().getCurrentColumn();
        return column.getValueMap().get(this.getRowDataSource().getCurrentEntity().getId());
    }
    public void setDate(Date date) {
        final BlockColumn<Date> column = (BlockColumn<Date>) this.getRowDataSource().getCurrentColumn();
        column.getValueMap().put(this.getRowDataSource().getCurrentEntity().getId(), date);
    }
}
