package ru.tandemservice.uniepp.events;

import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import org.tandemframework.hibsupport.event.single.listener.IHibernateEventListener;
import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;

import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import ru.tandemservice.uniepp.entity.std.gen.EppStateEduStandardBlockGen;
import ru.tandemservice.uniepp.entity.std.gen.EppStateEduStandardGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EppEduStdDefaultBlockListener implements IHibernateEventListener<ISingleEntityEvent>
{

    private static final TransactionCompleteAction<Void> action = new TransactionCompleteAction<Void>() {
        @Override public Void beforeCompletion(final Session session) {

            // создаем рутовый блок для версий УПв, если его там нет
            DQLSelectBuilder vDql = new DQLSelectBuilder().fromEntity(EppStateEduStandard.class, "v").column("v.id")
                .where(notExists(new DQLSelectBuilder()
                    .fromEntity(EppStateEduStandardBlock.class, "r")
                    .where(isNull(property("r", EppStateEduStandardBlock.programSpecialization())))
                    .buildQuery()));

            for (final Long versionId: vDql.createStatement(session).<Long>list()) {
                final EppStateEduStandardBlock block = new EppStateEduStandardBlock();
                block.setStateEduStandard((EppStateEduStandard)session.load(EppEduPlanVersion.class, versionId));
                session.save(block);
            }

            // нужно, потому что инече данные в базу не попадут
            session.flush();

            return null;
        }
    };

    @Override
    public void onEvent(final ISingleEntityEvent event) {
        if (event.getEntity() instanceof EppStateEduStandard) {
            EppEduStdDefaultBlockListener.action.register(event.getSession());
        }
    }

}
