/* $Id: $ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.CheckPrint;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.EppWorkPlanManager;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.CheckPrint.logic.EppWorkPlanCheckPrintData;

/**
 * @author Andrey Andreev
 * @since 16.03.2017
 */
@State({@Bind(key = EppWorkPlanCheckPrint.ORG_UNIT_ID, binding = "orgUnitHolder.id")})
public class EppWorkPlanCheckPrintUI extends UIPresenter
{

    private final OrgUnitHolder _orgUnitHolder = new OrgUnitHolder();

    @Override
    public void onComponentRefresh()
    {
        if (_orgUnitHolder.getId() != null) _orgUnitHolder.refresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EppWorkPlanCheckPrint.ORG_UNIT_ID, _orgUnitHolder.getId());
    }

    @Override
    public String getSettingsKey()
    {
        if (_orgUnitHolder.getId() != null) return super.getSettingsKey() + String.valueOf(_orgUnitHolder.getId());
        else return super.getSettingsKey();
    }

    public void onClickPrint()
    {
        EppWorkPlanCheckPrintData data = new EppWorkPlanCheckPrintData();
        data.setEducationYearList(_uiSettings.get(EppWorkPlanCheckPrint.EDUCATION_YEAR_PARAM));
        data.setWorkPlanStateList(_uiSettings.get(EppWorkPlanCheckPrint.WORK_PLAN_STATUS_PARAM));
        data.setOrgUnitList(_uiSettings.get(EppWorkPlanCheckPrint.ORG_UNIT_PARAM));
        if (CollectionUtils.isEmpty(data.getOrgUnitList()) && _orgUnitHolder.getId() != null)
            data.setOrgUnitList(getConfig().getDataSource(EppWorkPlanCheckPrint.ORG_UNIT_DS).getRecords());
        data.setRegStateList(_uiSettings.get(EppWorkPlanCheckPrint.REG_STATUS_PARAM));
        data.setRegTypeList(_uiSettings.get(EppWorkPlanCheckPrint.REG_TYPE_PARAM));


        byte[] bytes = EppWorkPlanManager.instance().workPlanCheckPrintDao().printReport(data);

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("Отчет о мероприятиях РУП.xls").document(bytes), true);

        saveSettings();

        deactivate();
    }


    @Override
    public ISecured getSecuredObject()
    {
        return getOrgUnit();
    }

    public String getPermissionKey()
    {
        if (getOrgUnitHolder().getValue() != null)
            return getOrgUnitHolder().getSecModel().getPermission("eppGroupOuIndicatorBlockView");
        else return "";
    }

    public OrgUnitHolder getOrgUnitHolder()
    {
        return _orgUnitHolder;
    }

    public OrgUnit getOrgUnit()
    {
        return getOrgUnitHolder().getValue();
    }
}
