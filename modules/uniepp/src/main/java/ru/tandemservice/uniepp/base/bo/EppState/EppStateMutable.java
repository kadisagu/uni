package ru.tandemservice.uniepp.base.bo.EppState;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author vdanilov
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EppStateMutable {
    boolean disable() default false;
    String[] value() default {};
}
