/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphEduPlansTab;

import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author vip_delete
 * @since 09.03.2010
 */
@SuppressWarnings("unchecked")
class WorkGraphEduPlansRow extends IdentifiableWrapper
{
    private static final long serialVersionUID = 1L;

    private final EppEduPlanVersion _eduPlanVersion;
    private final String _courses;

    WorkGraphEduPlansRow(final Long id, final EppEduPlanVersion eduPlanVersion, final String courses)
    {
        super(id, "");
        this._eduPlanVersion = eduPlanVersion;
        this._courses = courses;
    }

    // Getters

    public EppEduPlanVersion getEduPlanVersion()
    {
        return this._eduPlanVersion;
    }

    public String getCourses()
    {
        return this._courses;
    }
}
