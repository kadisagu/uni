/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractListUI;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;

import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Irina Ugfeld
 * @since 03.03.2016
 */
public class RegistryBaseDSHandler extends DefaultSearchDataSourceHandler {


    public RegistryBaseDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {

        Class elementClass = context.get(EppRegistryAbstractListUI.SETTING_ELEMENT_CLASS);

        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(elementClass, "e")
                .column(property("e"));

        Long orgUnitId = context.get(OrgUnitUIPresenter.INPUT_PARAM_ORG_UNIT_ID);
        if (orgUnitId!=null) {
            // на подразделении нет фильтра
            builder.where(eq(property(EppRegistryElement.owner().id().fromAlias("e")), value(orgUnitId)));
        } else {
            // иначе - учитываем фильтр
            CommonBaseFilterUtil.applySelectFilter(builder, "e", EppRegistryElement.owner(), context.get(EppRegistryAbstractListUI.SETTING_OWNER));
        }

        String title = context.get(EppRegistryAbstractListUI.SETTING_TITLE);
        CommonBaseFilterUtil.applyLikeFilter(
                builder, title,
                EppRegistryElement.title().fromAlias("e"),
                EppRegistryElement.fullTitle().fromAlias("e"),
                EppRegistryElement.shortTitle().fromAlias("e"),
                EppRegistryElement.number().fromAlias("e")
        );

        CommonBaseFilterUtil.applySelectFilter(builder, "e", EppRegistryElementGen.state(), context.get(EppRegistryAbstractListUI.SETTING_STATE));

        // учитывается фильтрация по фейковому элементу для типа деления "Не задан"
        DataWrapper splitVariant = context.get(EppRegistryAbstractListUI.SETTING_EDU_GROUP_SPLIT_VARIANT);
        if (splitVariant != null) {
            if (splitVariant.getId() == 0L) {
                builder.where(DQLExpressions.isNull(property(EppRegistryElement.eduGroupSplitVariant().fromAlias("e"))));
            } else {
                CommonBaseFilterUtil.applySelectFilter(builder, "e", EppRegistryElement.eduGroupSplitVariant(), DataAccessServices.dao().get(splitVariant.getId()));
            }
        }

        applyWhereConditions(builder, "e",input,elementClass,context);
        applyOrder(builder, "e",input,elementClass);

        DSOutput output =  DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
        //Если элемент реестра - дисциплина, нужно добавить часы для видов теоритической нагрузки
        if(elementClass.equals(EppRegistryDiscipline.class)){
            createWrapperWithLoad(output);
        }

        return output;
    }

    protected void createWrapperWithLoad(DSOutput output) {
        IEppRegistryDAO dao = IEppRegistryDAO.instance.get();

        List<Long> disciplineRegistryIds = output.getRecordIds();
        final Map<Long, Map<String, EppRegistryElementLoad>> map = dao.getRegistryElementTotalLoadMap(disciplineRegistryIds);
        final List<EppLoadType> loadTypes = dao.getList(EppLoadType.class);


        output.transform( (EppRegistryElement registryElement) ->{

            final DataWrapper wrapper = new DataWrapper(registryElement);
            final Map<String, EppRegistryElementLoad> loadMap = map.get(wrapper.getId());
            for (final EppLoadType loadType: loadTypes) {
                final EppRegistryElementLoad load = (null == loadMap ? null : loadMap.get(loadType.getFullCode()));
                wrapper.put(loadType.getFullCode(), (null == load ? null : load.getLoadAsDouble()));
            }
            return wrapper;
        });
    }

    /**
     * Здесь можно добавлять условия в запрос
     */
    protected void applyWhereConditions(final DQLSelectBuilder builder,final String alias, final DSInput input, final Class elementClass, final ExecutionContext context) {
        // Переопределяется в проектах
    }

    /**
     * Здесь можно добавлять правила сортировки
     */
    protected void applyOrder(final DQLSelectBuilder builder, final String alias, final DSInput input, final Class elementClass) {

        DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(elementClass, alias);
        orderDescriptionRegistry
                .setOrders("title",new OrderDescription(EppRegistryElement.title()))
                .setOrders("number",new OrderDescription(EppRegistryElement.number()))
                .setOrders("fullTitle",new OrderDescription(EppRegistryElement.fullTitle()))
                .setOrders("shortTitle",new OrderDescription(EppRegistryElement.shortTitle()))
                .setOrders("state",new OrderDescription(EppRegistryElement.state().code()))
                .setOrders("orgUnit",new OrderDescription(EppRegistryElement.owner().shortTitle()))
                .setOrders("structure",new OrderDescription(EppRegistryElement.parent().title()));
        orderDescriptionRegistry.applyOrder(builder, input.getEntityOrder());

    }
}