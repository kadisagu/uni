package ru.tandemservice.uniepp.entity.catalog;

import com.google.common.collect.ImmutableList;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uniepp.catalog.bo.EppWeekType.ui.AddEdit.EppWeekTypeAddEdit;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppWeekTypeGen;

import java.util.Collection;
import java.util.List;

/**
 * Тип учебной недели
 */
public class EppWeekType extends EppWeekTypeGen implements EppWeekTypeCodes, IDynamicCatalogItem, IPrioritizedCatalogItem
{
    private static final List<String> HIDDEN_PROPERTIES = ImmutableList.of(
            P_ABBREVIATION_VIEW,
            P_ABBREVIATION_EDIT,
            P_ABBREVIATION_I_M_C_A,
            P_COLOR
    );
    private static final List<String> HIDDEN_PROPERTIES_ITEM_PUB = ImmutableList.of(P_COLOR);

    // недели теоретического обучения
    public static final String CODE_THEORY = EppWeekTypeCodes.THEORY;
    public static final String CODE_NOT_USED = EppWeekTypeCodes.UNUSED;

    public void setColorHex(final String color)
    {
        this.setColor(Long.valueOf(color, 16));
    }

    public static IDynamicCatalogDesc getUiDesc()
    {
        return new BaseDynamicCatalogDesc()
        {
            @Override
            public Collection<String> getHiddenFields()
            {
                return HIDDEN_PROPERTIES;
            }

            @Override
            public Collection<String> getHiddenFieldsItemPub()
            {
                return HIDDEN_PROPERTIES_ITEM_PUB;
            }

            @Override
            public String getAddEditComponentName()
            {
                return EppWeekTypeAddEdit.class.getSimpleName();
            }

            @Override
            public void addAdditionalColumns(DynamicListDataSource<ICatalogItem> dataSource)
            {
                HeadColumn abbreviationColumn = new HeadColumn("abbreviation", "Сокращение");
                abbreviationColumn.setHeaderAlign("center");
                abbreviationColumn.addColumn(new SimpleColumn("Отображение", EppWeekType.abbreviationView()).setOrderable(false));
                abbreviationColumn.addColumn(new SimpleColumn("Редактирование", EppWeekType.abbreviationEdit()).setOrderable(false));
                abbreviationColumn.addColumn(new SimpleColumn("ИМЦА", EppWeekType.abbreviationIMCA()).setOrderable(false));
                dataSource.addColumn(abbreviationColumn);
            }
        };
    }
}