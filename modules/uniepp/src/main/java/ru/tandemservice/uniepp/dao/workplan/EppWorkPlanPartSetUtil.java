package ru.tandemservice.uniepp.dao.workplan;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;

/**
 * @author vdanilov
 */
public class EppWorkPlanPartSetUtil {

    public static Map<Integer, EppWorkPlanPart> fillPartMap(final EppWorkPlan workPlan, final Collection<EppWorkPlanPart> partList, final int parts, int targetWeeks) {

        if (targetWeeks <= 0) {
            // если не задано требуемое число недель, то считаем его из текущего распределения
            targetWeeks = 0;
            for (final EppWorkPlanPart part: partList) {
                targetWeeks += part.getTotalWeeks();
            }
        }

        final Map<Integer, EppWorkPlanPart> partMap = new LinkedHashMap<Integer, EppWorkPlanPart>();
        {
            int number = 1;
            int missing = 0;
            int maxSize = 1;
            for (final EppWorkPlanPart part: partList) {
                if ((parts < 0) || (part.getNumber() <= parts))
                {
                    // если часть подходит, то сначала добавляем все, которые были до нее (чтобы дырок не было)
                    while (number < part.getNumber()) {
                        partMap.put(number, new EppWorkPlanPart(workPlan, number, 1));
                        number++;
                    }

                    // потом добавляем саму часть
                    partMap.put(number, new EppWorkPlanPart(workPlan, number).updateSize(part));
                    number++;

                }
                else
                {
                    // если часть не подходит - то сохраняем число недель, о которых мы забыли
                    missing += part.getTotalWeeks();
                }

                // нужно сохранять максимальное число недель в части
                maxSize = Math.max(maxSize, part.getTotalWeeks());
            }

            // дополняем набор недостающими элементами
            // расширяем максимальным размером, чтобы части делились поровну при ужатии
            while (number <= parts) {
                partMap.put(number, new EppWorkPlanPart(workPlan, number, maxSize));
                number++;
            }

            if (missing > 0) {
                // если что-то удалилось - то добавляем это в последнюю часть
                final EppWorkPlanPart lastWeek = partMap.get(number-1);
                if (null != lastWeek) {
                    lastWeek.setTotalWeeks(lastWeek.getTotalWeeks()+missing);
                }
            }
        }


        if ((partMap.size() > 0) && (targetWeeks > 0)) {
            // теперь нужно впихнуть результат в недели

            if (targetWeeks < partMap.size()) {
                // нельзя впихнуть невпихуемое:
                // количество частей*1 неделю (минимум) должно быть не меньше чем количество недель в семестре
                throw new IllegalStateException("");
            }


            if (partMap.size() != partList.size()) {
                // делим равными частями ( #5228 )
                final int totalWeeks = Math.max(1, targetWeeks / partMap.size());
                for (final EppWorkPlanPart part: partMap.values()) {
                    part.setTotalWeeks(totalWeeks);
                }
            }


            while (true) {
                int balance = targetWeeks;
                for (final EppWorkPlanPart part: partMap.values()) {
                    balance -= part.getTotalWeeks();
                }

                if (0 == balance)
                {
                    // если мы совпали - то все, выходим, ничего больше распределять не надо
                    break;
                }
                else if (balance > 0)
                {
                    // не хватает, нужно добавлять в последнюю часть недели
                    final EppWorkPlanPart lastWeek = partMap.get(partMap.size());
                    lastWeek.setTotalWeeks(lastWeek.getTotalWeeks()+balance);
                }
                else if (balance < 0)
                {
                    // слишком много - будем уменьшать
                    boolean happened = false;
                    final Iterator<EppWorkPlanPart> it = partMap.values().iterator();
                    while ((balance < 0) && it.hasNext()) {
                        final EppWorkPlanPart part = it.next();
                        final int diff = Math.max(1, -balance/partMap.size());
                        final int totalWeeks = part.getTotalWeeks();
                        if (totalWeeks > 1) {
                            part.setTotalWeeks(Math.max(1, totalWeeks - diff));
                            happened = true;
                            balance += (totalWeeks - part.getTotalWeeks());
                        }
                    }

                    if (!happened) {
                        // это значит, что мы не смогли изменить ниодной части
                        // например, если частей больше, чем недель
                        throw new IllegalStateException("");
                    }
                }
            }
        }

        return partMap;
    }

}
