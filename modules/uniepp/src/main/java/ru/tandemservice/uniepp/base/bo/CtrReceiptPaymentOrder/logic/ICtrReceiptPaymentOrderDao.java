package ru.tandemservice.uniepp.base.bo.CtrReceiptPaymentOrder.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;

import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author vdanilov
 */
public interface ICtrReceiptPaymentOrderDao extends INeedPersistenceSupport {

    /**
     * Возвращает студента по договору. Если студентов несколько, то бросается исключение.
     * Данный метод подходит только для договоров, в которых студент один
     * (всего одно обязательство по обучению)
     */
    Student getSingleStudent(CtrContractObject contract);

}
