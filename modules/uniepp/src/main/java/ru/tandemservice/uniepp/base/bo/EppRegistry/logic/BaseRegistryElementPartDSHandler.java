/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.logic;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;

/**
 * Список частей элементов реестра
 * @author oleyba
 * @since 11/8/12
 */
public class BaseRegistryElementPartDSHandler extends EntityComboDataSourceHandler
{
    public BaseRegistryElementPartDSHandler(String ownerId)
    {
        super(ownerId, EppRegistryElementPart.class);
        this
        .order(EppRegistryElementPart.registryElement().title())
        .order(EppRegistryElementPart.registryElement().number())
        .order(EppRegistryElementPart.number())
        .filter(EppRegistryElementPart.registryElement().title())
        .filter(EppRegistryElementPart.registryElement().shortTitle())
        .filter(EppRegistryElementPart.registryElement().fullTitle())
        .filter(EppRegistryElementPart.registryElement().number())
        .pageable(true)
        ;

    }
}
