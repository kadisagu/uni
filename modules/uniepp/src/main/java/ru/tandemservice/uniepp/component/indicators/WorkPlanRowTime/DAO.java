package ru.tandemservice.uniepp.component.indicators.WorkPlanRowTime;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Date;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanVersionGen;

/**
 * 
 * @author nkokorina
 * @since 18.03.2010
 */

public class DAO extends UniBaseDao implements IDAO
{

    private long index(final Date date) {
        return (date.getTime()/1000/60/60/24);
    }

    private long time(final Date date, final long wp) {
        return (null == date ? wp : this.index(date));
    }


    private Long calculate(final DQLSelectBuilder dql, final PropertyPath<Date> property, final String param) {
        try {
            final Method method = DQLFunctions.class.getMethod(param, IDQLExpression.class);
            final DQLSelectBuilder tmp = new DQLSelectBuilder().fromDataSource(dql.buildQuery(), "xxx");
            final PropertyPath<Date> path = property.fromAlias("xxx.wpLoad");
            tmp.where(isNotNull(property(path)));
            tmp.column((IDQLExpression)method.invoke(null, property(path)));
            final Date uniqueResult = tmp.createStatement(this.getSession()).uniqueResult();
            return (null == uniqueResult ? null : this.index(uniqueResult));
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }



    @Override
    public void prepare(final Model model)
    {
        if (null == model.getYearModel().getValue()) {
            model.getYearModel().setValue(IUniBaseDao.instance.get().get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE));
        }

        Debug.begin("DAO.prepare");
        try {


            final StaticListDataSource<IEntity> dataSource = model.getDataSource();
            dataSource.setRowList(Collections.<IEntity>emptyList());

            dataSource.clearColumns();

            dataSource.addColumn(new PublisherLinkColumn("РУП", EppWorkPlan.title(), EppWorkPlanRowPartLoad.row().workPlan()).setClickable(true));
            dataSource.addColumn(new SimpleColumn("Часть\nгода", EppWorkPlanRowPartLoad.row().workPlan()+".gridTerm."+DevelopGridTerm.part().shortTitleWithParent()).setClickable(false).setWidth(1));
            dataSource.addColumn(new SimpleColumn("Часть\nРУП", EppWorkPlanRowPartLoad.part()).setClickable(false).setWidth(1));
            dataSource.addColumn(new SimpleColumn("Индекс", EppWorkPlanRowPartLoad.row().number()).setClickable(false).setWidth(1));
            dataSource.addColumn(new SimpleColumn("Дисциплина", EppWorkPlanRowPartLoad.row().displayableTitle()).setClickable(false));




            final DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(EppWorkPlanRowPartLoad.class, "wpLoad").column(property("wpLoad"), "wpLoad");
            dql.fetchPath(DQLJoinType.inner, EppWorkPlanRowPartLoad.row().fromAlias("wpLoad"), "wpRow");
            dql.where(or(
                    isNotNull(property(EppWorkPlanRowPartLoad.studyPeriodStartDate().fromAlias("wpLoad"))),
                    isNotNull(property(EppWorkPlanRowPartLoad.studyPeriodFinishDate().fromAlias("wpLoad")))
            ));

            {
                dql.joinEntity("wpRow", DQLJoinType.left, EppWorkPlanVersion.class, "wpv",
                        eq(
                                property("wpv", "id"),
                                property(EppWorkPlanRow.workPlan().id().fromAlias("wpRow"))
                        )
                );
                dql.joinEntity("wpv",  DQLJoinType.left, EppWorkPlan.class, "wp", or(
                        eq(
                                property("wp", "id"),
                                property(EppWorkPlanVersionGen.parent().id().fromAlias("wpv"))
                        ),
                        eq(
                                property("wp", "id"),
                                property(EppWorkPlanRow.workPlan().id().fromAlias("wpRow"))
                        )
                ));

                dql.where(eq(
                        property(EppWorkPlan.year().educationYear().fromAlias("wp")),
                        value(model.getYearModel().getValue())
                ));
            }


            final long wp_min, wp_max;
            {
                Long wpMin = this.calculate(dql, EppWorkPlanRowPartLoad.studyPeriodStartDate(), "min");
                Long wpMax = this.calculate(dql, EppWorkPlanRowPartLoad.studyPeriodFinishDate(), "max");

                if (null == wpMin) { wpMin = wpMax; }
                if (null == wpMax) { wpMax = wpMin; }
                if ((null == wpMin) || (null == wpMax)) { return; }

                wp_min = wpMin.longValue();
                wp_max = wpMax.longValue();
            }


            Debug.begin("preload");
            try {
                EppEduPlanVersionDataDAO.getLoadTypeMap();
            } finally {
                Debug.end();
            }

            dataSource.addColumn(new SimpleColumn("График", "graph") {
                @Override public boolean isRawContent() { return true; }
                @Override public String getContent(final IEntity entity)
                {
                    final EppWorkPlanRowPartLoad load = (EppWorkPlanRowPartLoad) entity;

                    final long t0 = wp_min;
                    final long t1 = Math.max(t0, DAO.this.time(load.getStudyPeriodStartDate(), wp_min));
                    final long t2 = Math.max(t1, DAO.this.time(load.getStudyPeriodFinishDate(), wp_max));
                    final long t3 = Math.max(t2, wp_max);

                    if (t0 == t3) { return ""; }

                    final int SZ = 98;
                    final int p1 = Math.max(1, (int)((SZ*(t1-t0))/(t3-t0)));
                    final int p2 = Math.max(1, (int)((SZ*(t2-t1))/(t3-t0)));
                    final int p3 = 100 - (p2+p1);

                    final StringBuilder sb = new StringBuilder();
                    sb.append("<div style=\"width:500px;\">");
                    sb.append("<div style=\"float:left;width:"+p1+"%; height:12px; background-color: lightgray;\"></div>");
                    sb.append("<div style=\"float:left;width:"+p2+"%; height:12px; background-color: blue;\"></div>");
                    sb.append("<div style=\"float:left;width:"+p3+"%; height:12px; background-color: lightgray;\"></div>");
                    sb.append("</div>");
                    return sb.toString();
                }
            }.setWidth(1));

            dataSource.setupRows(dql.createStatement(this.getSession()).<EppWorkPlanRowPartLoad>list());

        } finally {
            Debug.end();
        }

    }



}
