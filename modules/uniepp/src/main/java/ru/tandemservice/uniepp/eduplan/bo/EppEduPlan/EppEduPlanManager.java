/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.logic.EppEduPlanDao;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.logic.IEppEduPlanDao;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author oleyba
 * @since 8/28/14
 */
@Configuration
public class EppEduPlanManager extends BusinessObjectManager
{
    public static final String BIND_ORG_UNIT_ID = "orgUnitId";
    public static final String BIND_PROGRAM_KIND_ID = "programKind";

    public static EppEduPlanManager instance()
    {
        return instance(EppEduPlanManager.class);
    }

    @Bean
    public IEppEduPlanDao dao()
    {
        return new EppEduPlanDao();
    }

    public static List<String> getAllowedBaseLevelCodes(EduProgramKind programKind) {
        switch (programKind.getCode()) {
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_:
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA: return Arrays.asList(EduLevelCodes.OSNOVNOE_OBTSHEE_OBRAZOVANIE, EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE);
            case EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA:
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV: return Arrays.asList(EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE, EduLevelCodes.SREDNEE_PROFESSIONALNOE_OBRAZOVANIE, EduLevelCodes.VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA);
            case EduProgramKindCodes.PROGRAMMA_MAGISTRATURY: return Arrays.asList(EduLevelCodes.VYSSHEE_OBRAZOVANIE_BAKALAVRIAT, EduLevelCodes.VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA);
            case EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_:
            case EduProgramKindCodes.PROGRAMMA_ORDINATURY:
            case EduProgramKindCodes.PROGRAMMA_INTERNATURY: return Collections.singletonList(EduLevelCodes.VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA);
            case EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA: return Arrays.asList(EduLevelCodes.SREDNEE_PROFESSIONALNOE_OBRAZOVANIE, EduLevelCodes.VYSSHEE_OBRAZOVANIE_BAKALAVRIAT, EduLevelCodes.VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA, EduLevelCodes.VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII);
        }
        throw new IllegalStateException();
    }
}
