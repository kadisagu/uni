/**
 * $Id$
 */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.logic.List.EppWorkPlanListDSHandler;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.CheckPrint.logic.EppWorkPlanCheckPrintDao;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.CheckPrint.logic.IEppWorkPlanCheckPrintDao;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 01.02.13
 */
@Configuration
public class EppWorkPlanManager extends BusinessObjectManager
{
    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
    public static final String DEVELOP_CONDITION_LIST_DS = "developConditionListDS";
    public static final String PROGRAM_TRAIT_LIST_DS = "programTraitListDS";
    public static final String TERM_LIST_DS = "termListDS";
    public static final String STATE_LIST_DS = "stateListDS";

    public static EppWorkPlanManager instance()
    {
        return instance(EppWorkPlanManager.class);
    }

    @Bean
    public UIDataSourceConfig programSubjectDSConfig()
    {
        return SelectDSConfig.with(PROGRAM_SUBJECT_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn(EduProgramSubject.titleWithCode().s())
                .handler(programSubjectDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler programSpecializationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSpecialization.class)
                .where(EduProgramSpecialization.programSubject(), EppWorkPlanListDSHandler.PARAM_PROGRAM_SUBJECT)
                .filter(EduProgramSpecialization.title())
                .order(EduProgramSpecialization.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler yearEducationProcessDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppYearEducationProcess.class)
                .order(EppYearEducationProcess.educationYear().intValue());
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder wpDql = new DQLSelectBuilder()
                        .fromEntity(EppEduPlanProf.class, "p")
                        .fromEntity(EppWorkPlan.class, "w")
                        .where(eq(property("w", EppWorkPlan.parent().eduPlanVersion().eduPlan()), property("p")))
                        .where(eq(property("p", EppEduPlanProf.programSubject()), property(alias)));
                FilterUtils.applySelectFilter(wpDql, "w", EppWorkPlan.year(), context.get(EppWorkPlanListDSHandler.PARAM_YEAR_EDUCATION_PROCESS));
                dql.where(exists(wpDql.buildQuery()));
            }
        }
                .where(EduProgramSubject.subjectIndex().programKind(), EppWorkPlanListDSHandler.PARAM_PROGRAM_KIND)
                .order(EduProgramSubject.subjectCode())
                .order(EduProgramSubject.title())
                .filter(EduProgramSubject.subjectCode())
                .filter(EduProgramSubject.title());
    }

    @Bean
    public UIDataSourceConfig developConditionListDSConfig()
    {
        return SelectDSConfig.with(DEVELOP_CONDITION_LIST_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn(DevelopCondition.title().s())
                .handler(developConditionListDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler developConditionListDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopCondition.class)
                .filter(DevelopCondition.title())
                .order(DevelopCondition.code());
    }

    @Bean
    public UIDataSourceConfig programTraitListDSConfig()
    {
        return SelectDSConfig.with(PROGRAM_TRAIT_LIST_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn(EduProgramTrait.title().s())
                .handler(programTraitListDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler programTraitListDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramTrait.class)
                .filter(EduProgramTrait.title())
                .order(EduProgramTrait.title());
    }

    @Bean
    public UIDataSourceConfig termListDSConfig()
    {
        return SelectDSConfig.with(TERM_LIST_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(termListDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler termListDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Term.class)
                .order(Term.intValue())
                .filter(Term.title());
    }

    @Bean
    public UIDataSourceConfig stateListDSConfig()
    {
        return SelectDSConfig.with(STATE_LIST_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(stateListDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler stateListDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppState.class)
                .order(EppState.code())
                .filter(EppState.title());
    }

    @Bean
    public IEppWorkPlanCheckPrintDao workPlanCheckPrintDao()
    {
        return new EppWorkPlanCheckPrintDao();
    }
}
