package ru.tandemservice.uniepp.dao.eduStd.io;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "edu-std")
public class XmlEduStd
{

    @XmlAttribute(name = "level-type")
    public String levelType;
    @XmlAttribute(name = "level-name")
    public String levelName;
    @XmlAttribute(name = "generation")
    public String generation;
    @XmlAttribute(name = "reg-number")
    public String regNumber;
    @XmlAttribute(name = "reg-date")
    public String regDate;
    @XmlAttribute(name = "file")
    public String file;

    @XmlElements(@XmlElement(name = "edu-std-skill", type = XmlStdSkill.class))
    public List<XmlStdSkill> stdSkillList;

    @XmlElements(@XmlElement(name = "edu-std-block", type = XmlStdBlock.class))
    public List<XmlStdBlock> stdBlockList;

    @XmlType
    public static class XmlRowContainer
    {
        @XmlElements( {
            @XmlElement(name = "edu-std-row-structure", type = XmlStdStructureRow.class),
            @XmlElement(name = "edu-std-row-group-re", type = XmlStdGroupReRow.class),
            @XmlElement(name = "edu-std-row-group-im", type = XmlStdGroupImRow.class),
            @XmlElement(name = "edu-std-top-discipline", type = XmlStdDisciplineTopRow.class),
            @XmlElement(name = "edu-std-nested-discipline", type = XmlStdDisciplineNestedRow.class),
            @XmlElement(name = "edu-std-row-action", type = XmlStdActionRow.class)
        })
        public List<XmlRow> xmlRowList;
    }

    @XmlType
    public static class XmlStdBlock extends XmlRowContainer
    {
        @XmlAttribute(name = "level-type")
        public String levelType;
        @XmlAttribute(name = "level-name")
        public String levelName;
    }

    @XmlType
    public static class XmlStdSkill
    {
        @XmlAttribute(name = "code")
        public String code;
        @XmlAttribute(name = "comment")
        public String comment;
        @XmlAttribute(name = "group")
        public String group;
        @XmlAttribute(name = "type")
        public String type;
        @XmlElement(name = "edu-std-description", type = XmlStdComment.class)
        public XmlStdComment description;
    }

    @XmlType
    public static abstract class XmlRow extends XmlRowContainer
    {
        @XmlAttribute(name = "uuid")
        public String uuid;
        @XmlAttribute(name = "name")
        public String name;
        @XmlAttribute(name = "totalSize")
        public String totalSize;
        @XmlAttribute(name = "totalLabor")
        public String totalLabor;

        @XmlElements(@XmlElement(name = "skill", type = XmlStdRowSkill.class))
        public List<XmlStdRowSkill> skillList;
    }

    @XmlType
    public static class XmlStdStructureRow extends XmlRow
    {

    }

    @XmlType
    public static class XmlStdGroupImRow extends XmlRow
    {
        @XmlAttribute(name = "number")
        public String number;
        @XmlAttribute(name = "size")
        public String size;
    }

    @XmlType
    public static class XmlStdGroupReRow extends XmlRow
    {
        @XmlAttribute(name = "number")
        public String number;
    }

    @XmlType
    public static class XmlStdDisciplineTopRow extends XmlRow
    {
        @XmlAttribute(name = "number")
        public String number;
        @XmlAttribute(name = "type")
        public String type;
        @XmlElement(name = "edu-std-description", type = XmlStdComment.class)
        public XmlStdComment description;
    }

    @XmlType
    public static class XmlStdDisciplineNestedRow extends XmlRow
    {
        @XmlAttribute(name = "number")
        public String number;
        @XmlAttribute(name = "type")
        public String type;
        @XmlElement(name = "edu-std-description", type = XmlStdComment.class)
        public XmlStdComment description;
    }

    @XmlType
    public static class XmlStdActionRow extends XmlRow
    {
        @XmlAttribute(name = "number")
        public String number;
        @XmlAttribute(name = "type")
        public String type;
        @XmlAttribute(name = "totalWeeks")
        public String totalWeeks;
    }

    @XmlType
    public static class XmlStdComment
    {
        @XmlValue
        public String description;
    }

    @XmlType
    public static class XmlStdRowSkill
    {
        @XmlAttribute(name = "code")
        public String code;
    }
}
