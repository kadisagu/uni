package ru.tandemservice.uniepp.entity.catalog_ctr;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uniepp.entity.catalog_ctr.gen.*;

/**
 * Шаблон документа, используемый в договорах на обучение
 */
public class EppCtrDocTemplate extends EppCtrDocTemplateGen
{
    @Override public byte[] getContent() { return CommonBaseUtil.getTemplateContent(this); }
}