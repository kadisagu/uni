package ru.tandemservice.uniepp.events;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import org.tandemframework.hibsupport.event.single.listener.IHibernateEventListener;
import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;

import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion;
import ru.tandemservice.uniepp.entity.settings.gen.EppEducationOrgUnitEduPlanVersionGen;

/**
 * @author vdanilov
 */
public class EppEducationOrgUnitEduPlanVersionValidateListener implements IHibernateEventListener<ISingleEntityEvent> {

    private static final TransactionCompleteAction<Void> action = new TransactionCompleteAction<Void>() {
        @Override public Void beforeCompletion(final Session session) {

            // перед тем как выполнять запросы - убедимся, что все в базе
            session.flush();

            final DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(EppEducationOrgUnitEduPlanVersion.class, "rel").column(property("rel.id"));
            dql.where(in(property(EppEducationOrgUnitEduPlanVersionGen.eduPlanVersion().state().code().fromAlias("rel")), EppState.INACTIVE_CODES));

            final DQLDeleteBuilder del = new DQLDeleteBuilder(EppEducationOrgUnitEduPlanVersion.class);
            del.where(in(property("id"), dql.buildQuery()));
            final int deleted = del.createStatement(session).execute();

            if (deleted > 0) {
                this.logger.info("Delete `"+deleted+"` EppEducationOrgUnitEduPlanVersion with inactive EPV");
            }

            return null;
        }
    };

    @Override
    public void onEvent(final ISingleEntityEvent event) {
        final IEntity entity = event.getEntity();
        if ((entity instanceof EppEduPlanVersion) || (entity instanceof EppEducationOrgUnitEduPlanVersion)) {
            EppEducationOrgUnitEduPlanVersionValidateListener.action.register(event.getSession());
        }
    }

}
