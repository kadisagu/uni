/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.edustd.EduStdDisciplinePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation;

/**
 * @author nkokorina
 * Created on: 08.09.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        this.prepareListDateSource(component);
    }

    private void prepareListDateSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        final DynamicListDataSource<EppStdRow2SkillRelation> dataSource = UniBaseUtils.createDataSource(component, this.getDao());
        dataSource.addColumn(new SimpleColumn("Код", EppStdRow2SkillRelation.skill().code().s()).setWidth(5).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Группа", EppStdRow2SkillRelation.skill().skillGroup().title().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Описание", EppStdRow2SkillRelation.skill().comment().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Тип", EppStdRow2SkillRelation.skill().skillType().title().s()).setOrderable(false));

        model.setDataSource(dataSource);
    }
}
