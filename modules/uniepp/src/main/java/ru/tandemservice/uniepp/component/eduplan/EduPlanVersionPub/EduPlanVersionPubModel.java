package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author vdanilov
 */

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduplanVersionHolder.id") })
public abstract class EduPlanVersionPubModel
{

    private final EntityHolder<EppEduPlanVersion> eduplanVersionHolder = new EntityHolder<EppEduPlanVersion>();
    public EntityHolder<EppEduPlanVersion> getEduplanVersionHolder() { return this.eduplanVersionHolder; }
    public EppEduPlanVersion getEduplanVersion() { return this.getEduplanVersionHolder().getValue(); }

    public EppEduPlan getEduPlan() {
        return getEduplanVersion().getEduPlan();
    }
}
