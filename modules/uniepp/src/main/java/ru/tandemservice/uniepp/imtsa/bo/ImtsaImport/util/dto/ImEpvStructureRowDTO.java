/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.dto;

/**
 * @author azhebko
 * @since 28.10.2014
 */
public class ImEpvStructureRowDTO extends ImEpvRowDTO
{
    private String _value;     // Структура ГОС/УП

    public ImEpvStructureRowDTO() {}
    public ImEpvStructureRowDTO(String value) { _value = value; }

    public String getValue() { return _value; }
    public void setValue(String value) { _value = value; }

    @Override
    public String toString()
    {
        return "Структурная строка: " + getValue() + '\n' + super.toString();
    }
}