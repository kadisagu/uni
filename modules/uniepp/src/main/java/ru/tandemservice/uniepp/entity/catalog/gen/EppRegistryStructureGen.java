package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Структура реестра
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegistryStructureGen extends EntityBase
 implements INaturalIdentifiable<EppRegistryStructureGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure";
    public static final String ENTITY_NAME = "eppRegistryStructure";
    public static final int VERSION_HASH = 786965543;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_PARENT = "parent";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_ABBREVIATION = "abbreviation";
    public static final String P_THEME_REQUIRED = "themeRequired";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private EppRegistryStructure _parent;     // Структура реестра
    private String _shortTitle;     // Сокращенное название
    private String _abbreviation;     // Условное сокращение
    private boolean _themeRequired;     // Требует темы
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Структура реестра.
     */
    public EppRegistryStructure getParent()
    {
        return _parent;
    }

    /**
     * @param parent Структура реестра.
     */
    public void setParent(EppRegistryStructure parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Условное сокращение. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getAbbreviation()
    {
        return _abbreviation;
    }

    /**
     * @param abbreviation Условное сокращение. Свойство не может быть null.
     */
    public void setAbbreviation(String abbreviation)
    {
        dirty(_abbreviation, abbreviation);
        _abbreviation = abbreviation;
    }

    /**
     * @return Требует темы. Свойство не может быть null.
     */
    @NotNull
    public boolean isThemeRequired()
    {
        return _themeRequired;
    }

    /**
     * @param themeRequired Требует темы. Свойство не может быть null.
     */
    public void setThemeRequired(boolean themeRequired)
    {
        dirty(_themeRequired, themeRequired);
        _themeRequired = themeRequired;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRegistryStructureGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EppRegistryStructure)another).getCode());
            }
            setParent(((EppRegistryStructure)another).getParent());
            setShortTitle(((EppRegistryStructure)another).getShortTitle());
            setAbbreviation(((EppRegistryStructure)another).getAbbreviation());
            setThemeRequired(((EppRegistryStructure)another).isThemeRequired());
            setTitle(((EppRegistryStructure)another).getTitle());
        }
    }

    public INaturalId<EppRegistryStructureGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EppRegistryStructureGen>
    {
        private static final String PROXY_NAME = "EppRegistryStructureNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppRegistryStructureGen.NaturalId) ) return false;

            EppRegistryStructureGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegistryStructureGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegistryStructure.class;
        }

        public T newInstance()
        {
            return (T) new EppRegistryStructure();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "parent":
                    return obj.getParent();
                case "shortTitle":
                    return obj.getShortTitle();
                case "abbreviation":
                    return obj.getAbbreviation();
                case "themeRequired":
                    return obj.isThemeRequired();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "parent":
                    obj.setParent((EppRegistryStructure) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "abbreviation":
                    obj.setAbbreviation((String) value);
                    return;
                case "themeRequired":
                    obj.setThemeRequired((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "parent":
                        return true;
                case "shortTitle":
                        return true;
                case "abbreviation":
                        return true;
                case "themeRequired":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "parent":
                    return true;
                case "shortTitle":
                    return true;
                case "abbreviation":
                    return true;
                case "themeRequired":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "parent":
                    return EppRegistryStructure.class;
                case "shortTitle":
                    return String.class;
                case "abbreviation":
                    return String.class;
                case "themeRequired":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRegistryStructure> _dslPath = new Path<EppRegistryStructure>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegistryStructure");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Структура реестра.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure#getParent()
     */
    public static EppRegistryStructure.Path<EppRegistryStructure> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Условное сокращение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure#getAbbreviation()
     */
    public static PropertyPath<String> abbreviation()
    {
        return _dslPath.abbreviation();
    }

    /**
     * @return Требует темы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure#isThemeRequired()
     */
    public static PropertyPath<Boolean> themeRequired()
    {
        return _dslPath.themeRequired();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EppRegistryStructure> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EppRegistryStructure.Path<EppRegistryStructure> _parent;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _abbreviation;
        private PropertyPath<Boolean> _themeRequired;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EppRegistryStructureGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Структура реестра.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure#getParent()
     */
        public EppRegistryStructure.Path<EppRegistryStructure> parent()
        {
            if(_parent == null )
                _parent = new EppRegistryStructure.Path<EppRegistryStructure>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EppRegistryStructureGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Условное сокращение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure#getAbbreviation()
     */
        public PropertyPath<String> abbreviation()
        {
            if(_abbreviation == null )
                _abbreviation = new PropertyPath<String>(EppRegistryStructureGen.P_ABBREVIATION, this);
            return _abbreviation;
        }

    /**
     * @return Требует темы. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure#isThemeRequired()
     */
        public PropertyPath<Boolean> themeRequired()
        {
            if(_themeRequired == null )
                _themeRequired = new PropertyPath<Boolean>(EppRegistryStructureGen.P_THEME_REQUIRED, this);
            return _themeRequired;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppRegistryStructureGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EppRegistryStructure.class;
        }

        public String getEntityName()
        {
            return "eppRegistryStructure";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
