package ru.tandemservice.uniepp.component.workplan.AutocreateWorkPlanByGUP;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.gen.DevelopGridTermGen;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanGen;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionBlockGen;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphGen;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRow2EduPlanGen;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearEducationProcessGen;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    @Override
    public void prepare(final Model model) {
        {
            final List<EppYearEducationProcess> yearList = this.getList(EppYearEducationProcess.class, EppYearEducationProcessGen.educationYear().intValue().s());
            model.getYearSelectModel().setSource(new LazySimpleSelectModel<>(yearList));

            if (null == model.getYearSelectModel().getValue()) {
                final int number = (null == model.getSelectedYearNumber() ? 0 : model.getSelectedYearNumber());
                model.getYearSelectModel().setValue((EppYearEducationProcess)CollectionUtils.find(yearList, object -> ((EppYearEducationProcess) object).getEducationYear().getIntValue() >= number));
                if (null != model.getYearSelectModel().getValue()) {
                    this.onChangeEppYear(model);
                }
            }
        }

        {
            model.getWorkGraphSelectModel().setSource(new DQLFullCheckSelectModel() {
                @Override protected DQLSelectBuilder query(final String alias, final String filter) {
                    if (null == model.getYearSelectModel().getValue()) { return null; }

                    final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppWorkGraph.class, alias);

                    if (null != model.getContextId()) {
                        // если в контексте подразделения - то выбираем только подходящие
                        final DQLSelectBuilder t = new DQLSelectBuilder().fromEntity(EppWorkGraphRow2EduPlan.class, "rel")
                            .column(property("rel", EppWorkGraphRow2EduPlan.row().graph().id()));
                        t.where(in(property("rel", EppWorkGraphRow2EduPlan.eduPlanVersion().id()), DAO.this.getEpvBlockInContextBuilder(model.getContextId(), EppEduPlanVersionBlockGen.eduPlanVersion().id().s()).buildQuery()));
                        builder.where(in(property(alias, "id"), t.buildQuery()));
                    }

                    builder.where(eq(property(alias, EppWorkGraphGen.year()), value( model.getYearSelectModel().getValue())));
                    builder.order(property(alias, EppWorkGraphGen.developForm().code()));
                    builder.order(property(alias, EppWorkGraphGen.developCondition().code()));
                    builder.order(property(alias, EppWorkGraphGen.developTech().code()));
                    builder.order(property(alias, EppWorkGraphGen.developGrid().code()));
                    return builder;
                }
            });
        }
    }

    @Override
    public void onChangeEppYear(final Model model) {

    }

    @Override
    public void onChangeEppWorkGraph(final Model model) {

    }



    @Override
    public void save(final Model model) {
        final EppWorkGraph selectedWorkGraph = model.getWorkGraphSelectModel().getValue();
        if (null == selectedWorkGraph) { throw new ApplicationException("Необходимо указать график учебного процесса."); }

        final boolean lookup4WorkPlan = Model.LOOKUP_LOOKUP.equals(model.getLookupSelectModel().getValue());
        final boolean check4ExistWorkPlan = Model.OVERCOPY_NEW.equals(model.getOvercopySelectModel().getValue());

        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, String.valueOf(selectedWorkGraph.getId()));

        final String comment = DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()) + "\n" + model.getComment();

        final EppState state = this.getByNaturalId(new EppState.NaturalId(EppState.STATE_FORMATIVE));
        IEppSettingsDAO.instance.get().getELoadWeekTypeMap();

        final Map<Long, EppYearEducationProcess> yearMap = SafeMap.get(id -> (EppYearEducationProcess)session.get(EppYearEducationProcess.class, id));
        final Map<Long, DevelopGridTerm> termMap = SafeMap.get(id -> (DevelopGridTerm)session.get(DevelopGridTerm.class, id));


        final DQLSelectBuilder dqlCore = new DQLSelectBuilder();
        dqlCore.fromEntity(EppWorkGraphRow2EduPlan.class, "wgr2ep");
        dqlCore.where(eq(
            property(EppWorkGraphRow2EduPlanGen.row().graph().fromAlias("wgr2ep")),
            value(selectedWorkGraph)
        ));

        dqlCore.fromEntity(DevelopGridTerm.class, "gterm");
        dqlCore.where(eq(
            property(DevelopGridTermGen.developGrid().fromAlias("gterm")),
            property(EppWorkGraphRow2EduPlanGen.eduPlanVersion().eduPlan().developGrid().fromAlias("wgr2ep"))
        ));
        dqlCore.where(eq(
            property(DevelopGridTermGen.course().fromAlias("gterm")),
            property(EppWorkGraphRow2EduPlanGen.row().course().fromAlias("wgr2ep"))
        ));

        dqlCore.fromEntity(EppEduPlanVersionBlock.class, "epvBlock");
        dqlCore.where(eq(
            property(EppEduPlanVersionBlockGen.eduPlanVersion().fromAlias("epvBlock")),
            property(EppWorkGraphRow2EduPlanGen.eduPlanVersion().fromAlias("wgr2ep"))
        ));

        dqlCore.column(property(EppWorkGraphRow2EduPlanGen.row().graph().year().id().fromAlias("wgr2ep")), "eppYear_id");
        dqlCore.column(property("gterm.id"), "gridTerm_id");
        dqlCore.column(property("epvBlock.id"), "eppEpvBlock_id");

        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromDataSource(dqlCore.buildQuery(), "src");
        dql.column(property("src.eppYear_id"), "eppYear_id");
        dql.column(property("src.gridTerm_id"), "gridTerm_id");
        dql.column(property("src.eppEpvBlock_id"), "eppEpvBlock_id");
        //dql.order(property(EppYearEducationProcessGen.educationYear().intValue().fromAlias("src.eppYear")));
        //dql.order(property(DevelopGridTermGen.term().intValue().fromAlias("src.gridTerm")));
        //dql.order(property(EppEduPlanVersionBlockGen.educationLevelHighSchool().code().fromAlias("src.eppEpvBlock")));

        if (check4ExistWorkPlan)
        {
            // не должно быть РУП(основной РУП) на тот же блок в том же году и семестре
            dql.where(notExists(
                new DQLSelectBuilder()
                .fromEntity(EppWorkPlan.class, "wpx").column(property("wpx.id"))
                .where(eq(property(EppWorkPlanGen.parent().id().fromAlias("wpx")), property("src.eppEpvBlock_id")))
                .where(eq(property(EppWorkPlanGen.year().id().fromAlias("wpx")), property("src.eppYear_id")))
                .where(eq(property(EppWorkPlanGen.cachedGridTerm().id().fromAlias("wpx")), property("src.gridTerm_id")))
                .buildQuery()
            ));
        }

        int i = 0;
        final Set<Long> blockIds = this.getEvpBlockIds(model, session);
        for (final Object[] row: CommonDAO.scrollRows(dql.createStatement(this.getSession()))) {
            final EppYearEducationProcess year = yearMap.get(((Number)row[0]).longValue());
            final DevelopGridTerm term = termMap.get(((Number)row[1]).longValue());
            final EppEduPlanVersionBlock block = (EppEduPlanVersionBlock)session.load(EppEduPlanVersionBlock.class, ((Number)row[2]).longValue());
            final EppEduPlan eduPlan = block.getEduPlanVersion().getEduPlan();

            if ((null != blockIds) && !blockIds.contains(block.getId())) {
                continue;
            }


            //  для общего блока создавать РУП только на семестры, строго меньшие семестра выбора профиля, а для блоков направленностей создавать РУП только для семестров больше или равных семестру выбора профиля.
            final DevelopGridTerm epvDevelopGridTerm = block.getEduPlanVersion().getDevelopGridTerm();

            if (eduPlan instanceof EppEduPlanHigherProf)
            {
                if(block instanceof EppEduPlanVersionRootBlock && term.getTermNumber() >= epvDevelopGridTerm.getTermNumber())
                {
                    continue;
                }
                else if(block instanceof EppEduPlanVersionSpecializationBlock && term.getTermNumber() < epvDevelopGridTerm.getTermNumber())
                {
                    continue;
                }
            }

            // сохраняем РП
            final EppWorkPlan wp = new EppWorkPlan(year, block, term);
            wp.setState(state);
            wp.setNumber(INumberQueueDAO.instance.get().getNextNumber(wp));
            wp.setRegistrationNumber(wp.getNumber());
            wp.setComment(comment);
            session.save(wp);

            // Создаем часть года
            IEppWorkPlanDAO.instance.get().doCreateEppYearPartToWP(wp);

            // заполняем его данными (либо из УП, либо из РП прошлых лет)
            if (IEppWorkPlanDAO.instance.get().doGenerateWorkPlanRows(wp.getId(), lookup4WorkPlan)) {
                i++;
            }
        }

        ContextLocal.getInfoCollector().add("Скопировано "+i+" РУП");
    }


    protected Set<Long> getEvpBlockIds(final Model model, final Session session)
    {
        final Long contextId = model.getContextId();
        if (null == contextId) { return null; }

        // если в контексте подразделения - то выводим только его НПВ
        final DQLSelectBuilder b = this.getEpvBlockInContextBuilder(contextId, "id");
        return new HashSet<>(DataAccessServices.dao().<Long>getList(b));
    }

    @SuppressWarnings("unchecked")
    protected DQLSelectBuilder getEpvBlockInContextBuilder(final Long contextId, final String path)
    {
        if ((null != contextId) && OrgUnit.class.isAssignableFrom(EntityRuntime.getMeta(contextId).getEntityClass()))
        {
            DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(EppEduPlanVersionBlock.class, "epvBlock")
                .column("epvBlock." + path);
            
            b.joinPath(DQLJoinType.inner, EppEduPlanVersionBlockGen.eduPlanVersion().fromAlias("epvBlock"), "epv");
            b.fromEntity(EppEduPlanProf.class, "eppEduPlan");
            b.where(eq(property("eppEduPlan"), property("epvBlock", EppEduPlanVersionBlockGen.eduPlanVersion().eduPlan())));

            b.fromEntity(EducationOrgUnit.class, "eduOU");
            b.where(eq(property("eppEduPlan", EppEduPlanGen.programForm()), property("eduOU", EducationOrgUnitGen.developForm().programForm().s())));
            b.where(eq(property("eppEduPlan", EppEduPlanGen.developCondition()), property("eduOU", EducationOrgUnitGen.developCondition())));
            b.where(eqNullSafe(property("eppEduPlan", EppEduPlanGen.programTrait()), property("eduOU", EducationOrgUnitGen.developTech().programTrait())));
            b.where(eq(property("eppEduPlan", EppEduPlanGen.developGrid().developPeriod()), property("eduOU", EducationOrgUnitGen.developPeriod())));
            b.where(eq(property("eppEduPlan", EppEduPlanProf.programSubject()), property("eduOU", EducationOrgUnitGen.educationLevelHighSchool().educationLevel().eduProgramSubject())));

            b.where(or(
                eq(property("eduOU", EducationOrgUnitGen.groupOrgUnit().id()), value(contextId)),
                eq(property("eduOU", EducationOrgUnitGen.operationOrgUnit().id()), value(contextId)),
                eq(property("eduOU", EducationOrgUnitGen.formativeOrgUnit().id()), value(contextId)),
                eq(property("eduOU", EducationOrgUnitGen.territorialOrgUnit().id()), value(contextId)),
                eq(property("eduOU", EducationOrgUnitGen.educationLevelHighSchool().orgUnit().id()), value(contextId))
            ));
            return b;
        }
        return null;
    }

}
