package ru.tandemservice.uniepp.workgraph.bo.EppWorkGraph;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * Created by nsvetlov on 11.07.2016.
 */
@Configuration
public class EppWorkGraphManager extends BusinessObjectManager
{
    public static EppWorkGraphManager instance()
    {
        return instance(EppWorkGraphManager.class);
    }
}
