/**
 *$Id$
 */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.logic.studentList;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.StudentList.EppEduPlanVersionStudentListUI;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * @author Alexander Shaburov
 * @since 01.02.13
 */
public class EduPlanVersionStudentSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public EduPlanVersionStudentSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        final Long id = context.get(EppEduPlanVersionStudentListUI.PARAM_ID);

        // только неархивные студенты
        builder.where(eq(property(alias, Student.P_ARCHIVAL), value(Boolean.FALSE)));

        // сущестувет актуальная связь с текущим УП(в)
        builder.where(exists(
            new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "s2epv").column(property("s2epv.id"))
            .where(eq(property(EppStudent2EduPlanVersion.student().fromAlias("s2epv")), property(alias)))
            .where(eq(property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("s2epv")), value(id)))
            .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
            .buildQuery()
        ));
    }
}
