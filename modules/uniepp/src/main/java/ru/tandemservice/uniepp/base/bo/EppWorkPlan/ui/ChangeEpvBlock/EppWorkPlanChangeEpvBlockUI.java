/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.ChangeEpvBlock;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import java.util.Collections;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 20.10.2014
 */

@Input({
               @Bind(key = EppWorkPlanChangeEpvBlockUI.EPP_WP_ID_BIND_PARAM, binding = "eppWorkPlan.id", required = true),
       })
public class EppWorkPlanChangeEpvBlockUI extends UIPresenter
{
    public static final String EPP_WP_ID_BIND_PARAM = "eppWP";

    private ISelectModel _blockModel;
    private EppWorkPlan _eppWorkPlan = new EppWorkPlan();

    @Override
    public void onComponentRefresh()
    {
        setEppWorkPlan(DataAccessServices.dao().getNotNull(EppWorkPlan.class, getEppWorkPlan().getId()));
        List<EppEduPlanVersionBlock> blockList = DataAccessServices.dao().getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.L_EDU_PLAN_VERSION, getEppWorkPlan().getEduPlanVersion());
        Collections.sort(blockList);
        setBlockModel(new LazySimpleSelectModel<>(blockList));
    }

    @Override
    public ISecured getSecuredObject()
    {
        return getEppWorkPlan();
    }

    public ISelectModel getBlockModel()
    {
        return _blockModel;
    }

    public void setBlockModel(ISelectModel blockModel)
    {
        _blockModel = blockModel;
    }

    public EppWorkPlan getEppWorkPlan()
    {
        return _eppWorkPlan;
    }

    public void setEppWorkPlan(EppWorkPlan eppWorkPlan)
    {
        _eppWorkPlan = eppWorkPlan;
    }

    public void onClickApply()
    {
        // TODO убрать обёртку в noopHandler, когда закончится переходный период, чтобы редактирование было возможным только в состоянии на формировании
        try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler()))
        {
            DataAccessServices.dao().update(_eppWorkPlan);
        }
        deactivate();
    }
}