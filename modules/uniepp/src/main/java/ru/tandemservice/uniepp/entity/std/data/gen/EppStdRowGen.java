package ru.tandemservice.uniepp.entity.std.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись ГОС (базовая)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStdRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.std.data.EppStdRow";
    public static final String ENTITY_NAME = "eppStdRow";
    public static final int VERSION_HASH = 452310685;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String P_EXCLUDED_FROM_LOAD = "excludedFromLoad";
    public static final String P_EXCLUDED_FROM_ACTIONS = "excludedFromActions";
    public static final String P_TOTAL_SIZE_MIN = "totalSizeMin";
    public static final String P_TOTAL_SIZE_MAX = "totalSizeMax";
    public static final String P_TOTAL_LABOR_MIN = "totalLaborMin";
    public static final String P_TOTAL_LABOR_MAX = "totalLaborMax";
    public static final String P_UUID = "uuid";
    public static final String P_DISPLAYABLE_TITLE = "displayableTitle";
    public static final String P_TOTAL_LABOR = "totalLabor";
    public static final String P_TOTAL_SIZE = "totalSize";

    private EppStateEduStandardBlock _owner;     // Блок
    private boolean _excludedFromLoad = false;     // Исключить из нагрузки
    private boolean _excludedFromActions = false;     // Исключить из мероприятий
    private Long _totalSizeMin;     // Число часов (min)
    private Long _totalSizeMax;     // Число часов (max)
    private Long _totalLaborMin;     // Трудоемкость (min)
    private Long _totalLaborMax;     // Трудоемкость (max)
    private String _uuid;     // Идентификатор строки для импорта

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок. Свойство не может быть null.
     */
    @NotNull
    public EppStateEduStandardBlock getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Блок. Свойство не может быть null.
     */
    public void setOwner(EppStateEduStandardBlock owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Исключить из нагрузки. Свойство не может быть null.
     */
    @NotNull
    public boolean isExcludedFromLoad()
    {
        return _excludedFromLoad;
    }

    /**
     * @param excludedFromLoad Исключить из нагрузки. Свойство не может быть null.
     */
    public void setExcludedFromLoad(boolean excludedFromLoad)
    {
        dirty(_excludedFromLoad, excludedFromLoad);
        _excludedFromLoad = excludedFromLoad;
    }

    /**
     * @return Исключить из мероприятий. Свойство не может быть null.
     */
    @NotNull
    public boolean isExcludedFromActions()
    {
        return _excludedFromActions;
    }

    /**
     * @param excludedFromActions Исключить из мероприятий. Свойство не может быть null.
     */
    public void setExcludedFromActions(boolean excludedFromActions)
    {
        dirty(_excludedFromActions, excludedFromActions);
        _excludedFromActions = excludedFromActions;
    }

    /**
     * @return Число часов (min).
     */
    public Long getTotalSizeMin()
    {
        return _totalSizeMin;
    }

    /**
     * @param totalSizeMin Число часов (min).
     */
    public void setTotalSizeMin(Long totalSizeMin)
    {
        dirty(_totalSizeMin, totalSizeMin);
        _totalSizeMin = totalSizeMin;
    }

    /**
     * @return Число часов (max).
     */
    public Long getTotalSizeMax()
    {
        return _totalSizeMax;
    }

    /**
     * @param totalSizeMax Число часов (max).
     */
    public void setTotalSizeMax(Long totalSizeMax)
    {
        dirty(_totalSizeMax, totalSizeMax);
        _totalSizeMax = totalSizeMax;
    }

    /**
     * @return Трудоемкость (min).
     */
    public Long getTotalLaborMin()
    {
        return _totalLaborMin;
    }

    /**
     * @param totalLaborMin Трудоемкость (min).
     */
    public void setTotalLaborMin(Long totalLaborMin)
    {
        dirty(_totalLaborMin, totalLaborMin);
        _totalLaborMin = totalLaborMin;
    }

    /**
     * @return Трудоемкость (max).
     */
    public Long getTotalLaborMax()
    {
        return _totalLaborMax;
    }

    /**
     * @param totalLaborMax Трудоемкость (max).
     */
    public void setTotalLaborMax(Long totalLaborMax)
    {
        dirty(_totalLaborMax, totalLaborMax);
        _totalLaborMax = totalLaborMax;
    }

    /**
     * @return Идентификатор строки для импорта.
     */
    @Length(max=255)
    public String getUuid()
    {
        return _uuid;
    }

    /**
     * @param uuid Идентификатор строки для импорта.
     */
    public void setUuid(String uuid)
    {
        dirty(_uuid, uuid);
        _uuid = uuid;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppStdRowGen)
        {
            setOwner(((EppStdRow)another).getOwner());
            setExcludedFromLoad(((EppStdRow)another).isExcludedFromLoad());
            setExcludedFromActions(((EppStdRow)another).isExcludedFromActions());
            setTotalSizeMin(((EppStdRow)another).getTotalSizeMin());
            setTotalSizeMax(((EppStdRow)another).getTotalSizeMax());
            setTotalLaborMin(((EppStdRow)another).getTotalLaborMin());
            setTotalLaborMax(((EppStdRow)another).getTotalLaborMax());
            setUuid(((EppStdRow)another).getUuid());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStdRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStdRow.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppStdRow is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "excludedFromLoad":
                    return obj.isExcludedFromLoad();
                case "excludedFromActions":
                    return obj.isExcludedFromActions();
                case "totalSizeMin":
                    return obj.getTotalSizeMin();
                case "totalSizeMax":
                    return obj.getTotalSizeMax();
                case "totalLaborMin":
                    return obj.getTotalLaborMin();
                case "totalLaborMax":
                    return obj.getTotalLaborMax();
                case "uuid":
                    return obj.getUuid();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((EppStateEduStandardBlock) value);
                    return;
                case "excludedFromLoad":
                    obj.setExcludedFromLoad((Boolean) value);
                    return;
                case "excludedFromActions":
                    obj.setExcludedFromActions((Boolean) value);
                    return;
                case "totalSizeMin":
                    obj.setTotalSizeMin((Long) value);
                    return;
                case "totalSizeMax":
                    obj.setTotalSizeMax((Long) value);
                    return;
                case "totalLaborMin":
                    obj.setTotalLaborMin((Long) value);
                    return;
                case "totalLaborMax":
                    obj.setTotalLaborMax((Long) value);
                    return;
                case "uuid":
                    obj.setUuid((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "excludedFromLoad":
                        return true;
                case "excludedFromActions":
                        return true;
                case "totalSizeMin":
                        return true;
                case "totalSizeMax":
                        return true;
                case "totalLaborMin":
                        return true;
                case "totalLaborMax":
                        return true;
                case "uuid":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "excludedFromLoad":
                    return true;
                case "excludedFromActions":
                    return true;
                case "totalSizeMin":
                    return true;
                case "totalSizeMax":
                    return true;
                case "totalLaborMin":
                    return true;
                case "totalLaborMax":
                    return true;
                case "uuid":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return EppStateEduStandardBlock.class;
                case "excludedFromLoad":
                    return Boolean.class;
                case "excludedFromActions":
                    return Boolean.class;
                case "totalSizeMin":
                    return Long.class;
                case "totalSizeMax":
                    return Long.class;
                case "totalLaborMin":
                    return Long.class;
                case "totalLaborMax":
                    return Long.class;
                case "uuid":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStdRow> _dslPath = new Path<EppStdRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStdRow");
    }
            

    /**
     * @return Блок. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getOwner()
     */
    public static EppStateEduStandardBlock.Path<EppStateEduStandardBlock> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Исключить из нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#isExcludedFromLoad()
     */
    public static PropertyPath<Boolean> excludedFromLoad()
    {
        return _dslPath.excludedFromLoad();
    }

    /**
     * @return Исключить из мероприятий. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#isExcludedFromActions()
     */
    public static PropertyPath<Boolean> excludedFromActions()
    {
        return _dslPath.excludedFromActions();
    }

    /**
     * @return Число часов (min).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getTotalSizeMin()
     */
    public static PropertyPath<Long> totalSizeMin()
    {
        return _dslPath.totalSizeMin();
    }

    /**
     * @return Число часов (max).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getTotalSizeMax()
     */
    public static PropertyPath<Long> totalSizeMax()
    {
        return _dslPath.totalSizeMax();
    }

    /**
     * @return Трудоемкость (min).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getTotalLaborMin()
     */
    public static PropertyPath<Long> totalLaborMin()
    {
        return _dslPath.totalLaborMin();
    }

    /**
     * @return Трудоемкость (max).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getTotalLaborMax()
     */
    public static PropertyPath<Long> totalLaborMax()
    {
        return _dslPath.totalLaborMax();
    }

    /**
     * @return Идентификатор строки для импорта.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getUuid()
     */
    public static PropertyPath<String> uuid()
    {
        return _dslPath.uuid();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getDisplayableTitle()
     */
    public static SupportedPropertyPath<String> displayableTitle()
    {
        return _dslPath.displayableTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getTotalLabor()
     */
    public static SupportedPropertyPath<String> totalLabor()
    {
        return _dslPath.totalLabor();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getTotalSize()
     */
    public static SupportedPropertyPath<String> totalSize()
    {
        return _dslPath.totalSize();
    }

    public static class Path<E extends EppStdRow> extends EntityPath<E>
    {
        private EppStateEduStandardBlock.Path<EppStateEduStandardBlock> _owner;
        private PropertyPath<Boolean> _excludedFromLoad;
        private PropertyPath<Boolean> _excludedFromActions;
        private PropertyPath<Long> _totalSizeMin;
        private PropertyPath<Long> _totalSizeMax;
        private PropertyPath<Long> _totalLaborMin;
        private PropertyPath<Long> _totalLaborMax;
        private PropertyPath<String> _uuid;
        private SupportedPropertyPath<String> _displayableTitle;
        private SupportedPropertyPath<String> _totalLabor;
        private SupportedPropertyPath<String> _totalSize;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getOwner()
     */
        public EppStateEduStandardBlock.Path<EppStateEduStandardBlock> owner()
        {
            if(_owner == null )
                _owner = new EppStateEduStandardBlock.Path<EppStateEduStandardBlock>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Исключить из нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#isExcludedFromLoad()
     */
        public PropertyPath<Boolean> excludedFromLoad()
        {
            if(_excludedFromLoad == null )
                _excludedFromLoad = new PropertyPath<Boolean>(EppStdRowGen.P_EXCLUDED_FROM_LOAD, this);
            return _excludedFromLoad;
        }

    /**
     * @return Исключить из мероприятий. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#isExcludedFromActions()
     */
        public PropertyPath<Boolean> excludedFromActions()
        {
            if(_excludedFromActions == null )
                _excludedFromActions = new PropertyPath<Boolean>(EppStdRowGen.P_EXCLUDED_FROM_ACTIONS, this);
            return _excludedFromActions;
        }

    /**
     * @return Число часов (min).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getTotalSizeMin()
     */
        public PropertyPath<Long> totalSizeMin()
        {
            if(_totalSizeMin == null )
                _totalSizeMin = new PropertyPath<Long>(EppStdRowGen.P_TOTAL_SIZE_MIN, this);
            return _totalSizeMin;
        }

    /**
     * @return Число часов (max).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getTotalSizeMax()
     */
        public PropertyPath<Long> totalSizeMax()
        {
            if(_totalSizeMax == null )
                _totalSizeMax = new PropertyPath<Long>(EppStdRowGen.P_TOTAL_SIZE_MAX, this);
            return _totalSizeMax;
        }

    /**
     * @return Трудоемкость (min).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getTotalLaborMin()
     */
        public PropertyPath<Long> totalLaborMin()
        {
            if(_totalLaborMin == null )
                _totalLaborMin = new PropertyPath<Long>(EppStdRowGen.P_TOTAL_LABOR_MIN, this);
            return _totalLaborMin;
        }

    /**
     * @return Трудоемкость (max).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getTotalLaborMax()
     */
        public PropertyPath<Long> totalLaborMax()
        {
            if(_totalLaborMax == null )
                _totalLaborMax = new PropertyPath<Long>(EppStdRowGen.P_TOTAL_LABOR_MAX, this);
            return _totalLaborMax;
        }

    /**
     * @return Идентификатор строки для импорта.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getUuid()
     */
        public PropertyPath<String> uuid()
        {
            if(_uuid == null )
                _uuid = new PropertyPath<String>(EppStdRowGen.P_UUID, this);
            return _uuid;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getDisplayableTitle()
     */
        public SupportedPropertyPath<String> displayableTitle()
        {
            if(_displayableTitle == null )
                _displayableTitle = new SupportedPropertyPath<String>(EppStdRowGen.P_DISPLAYABLE_TITLE, this);
            return _displayableTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getTotalLabor()
     */
        public SupportedPropertyPath<String> totalLabor()
        {
            if(_totalLabor == null )
                _totalLabor = new SupportedPropertyPath<String>(EppStdRowGen.P_TOTAL_LABOR, this);
            return _totalLabor;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdRow#getTotalSize()
     */
        public SupportedPropertyPath<String> totalSize()
        {
            if(_totalSize == null )
                _totalSize = new SupportedPropertyPath<String>(EppStdRowGen.P_TOTAL_SIZE, this);
            return _totalSize;
        }

        public Class getEntityClass()
        {
            return EppStdRow.class;
        }

        public String getEntityName()
        {
            return "eppStdRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDisplayableTitle();

    public abstract String getTotalLabor();

    public abstract String getTotalSize();
}
