package ru.tandemservice.uniepp.entity.student.group;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroup4ActionTypeGen;

/**
 * Фактическая учебная группа (по форме итогового контроля)
 *
 * Объединяет группу студентов по форме контроля в рамках дисциплины реестра
 */
public class EppRealEduGroup4ActionType extends EppRealEduGroup4ActionTypeGen
{
    public EppRealEduGroup4ActionType() {}

    public EppRealEduGroup4ActionType(final EppRealEduGroupSummary summary, final EppRegistryElementPart activityPart, final EppGroupType groupType) {
        this.setSummary(summary);
        this.setActivityPart(activityPart);
        this.setType(groupType);
    }

    @Override
    public EppGroupTypeFCA getType()
    {
        return (EppGroupTypeFCA) super.getType();
    }

    /**
     * Внимание!
     * Метод выполняет запрос к базе. Использовать осознанно и аккуратно.
     */
    public EppFControlActionType getActionType()
    {
        return IUniBaseDao.instance.get().getNotNull(EppFControlActionType.class, EppFControlActionType.eppGroupType(), this.getType());
    }

    public OrgUnit getTutorOu() {
        return getActivityPart().getTutorOu();
    }
}