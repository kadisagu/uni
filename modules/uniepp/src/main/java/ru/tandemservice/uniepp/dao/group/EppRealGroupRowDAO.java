package ru.tandemservice.uniepp.dao.group;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.EntityPath;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.HibSupportUtils;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.util.ThreadCountProperty;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearPartGen;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.*;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow.IEppRealEduGroupRowDescription;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author vdanilov
 */
public class EppRealGroupRowDAO extends UniBaseDao implements IEppRealGroupRowDAO {

    protected boolean fixEduOrgUnit = true;
    protected boolean checkLevel = true;
    private final ThreadCountProperty UPDATE_THREAD_COUNT = new ThreadCountProperty(this.getClass().getName()+".threads", 4);

    public static final String LOCK_NAME_UPDATE_ROWS = EppRealGroupRowDAO.class.getName()+".doUpdateStudentRowList";
    public static final SyncDaemon DAEMON = new SyncDaemon(EppRealGroupRowDAO.class.getName(), 120, EppStudentSlotDAO.GLOBAL_DAEMON_LOCK)
    {
        @SuppressWarnings("ConstantConditions")
        @Override protected void main() {
            final IEppRealGroupRowDAO dao = IEppRealGroupRowDAO.instance.get();

            try
            {
                // для начала надо создать недостающие сводки и годочасти
                try {
                    dao.doCreateMissingSummary(false);
                } catch (final Exception t) {
                    Debug.exception(t.getMessage(), t);
                    this.logger.warn(t.getMessage(), t); /* если упало - то и хрен с ним (это не основаня задача демона) */
                }

                // далее и ниже = это все разные транзакции, это сделано намеренно, чтобы снизить нагрузку на сервер (изменяется и блокируетсяочень большое количество объектов)
                // на качетсве работы это не влияет никак - т.к. разные куски делают разные действия (в тех местах, где действительно важно наличие одной транзакции, операции вынесены в отдельный транзакционный метод)

                boolean updateStatus = false;
                updateStatus |= dao.doDisableUnnecessaryRelations();
                updateStatus |= dao.doAwakeStudentRows();
                updateStatus |= dao.doUpdateStudentRowList();

                // ждем в любом случае (чтобы не было проблем с тайм-аутами)
                // это должны быть разные транзакции (иначе сервак умирает)
                try { Thread.sleep(SyncDaemon.ONE_SECOND); }
                catch (final Exception ignored) {}

                if (updateStatus)
                {
                    // если что-то поменялось, то обновляем все данные в базе
                    // (здесь надо обновлять все, что можно обновить)
                    dao.doUpdateStudentRowData();
                }
                else
                {
                    // если мы ничего не меняли - то обновляем только пустые записи
                    // (их надо обновить потому что они есть и это плохо)
                    dao.doUpdateStudentRowData(null);
                }

            }
            finally
            {

                // сначала выводим отладочную информацию
                if (this.logger.isInfoEnabled())
                {
                    UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
                        for (final IEppRealEduGroupRowDescription rel: dao.getRelations()) {
                            final int enabledSlots = ((Number)session.createCriteria(rel.relationClass()).add(Restrictions.isNull(EppRealEduGroupRow.removalDate().s())).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            final int totalSlots = ((Number)session.createCriteria(rel.relationClass()).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            final int totalGroups = ((Number)session.createCriteria(rel.groupClass()).setProjection(Projections.count("id")).uniqueResult()).intValue();
                            logger.info("group-s-"+rel.groupClass().getSimpleName()+"="+enabledSlots+"/"+totalSlots+" ("+totalGroups+")");
                        }
                        return null;
                    });
                }
            }
        }
    };


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public Collection<IEppRealEduGroupRowDescription> getRelations()
    {
        return Arrays.asList(
            EppRealEduGroup4LoadTypeRow.RELATION,
            EppRealEduGroup4ActionTypeRow.RELATION
        );
    }


    @SuppressWarnings("unchecked")
    private final Map<Class<? extends EppRealEduGroup>, Collection<IEppRealGroupOperationHandler>> handlerMap = SafeMap.get(new SafeMap.Callback<Class<? extends EppRealEduGroup>, Collection<IEppRealGroupOperationHandler>>() {

        private final SingleValueCache<Collection<IEppRealGroupOperationHandler>> handlers = new SingleValueCache<Collection<IEppRealGroupOperationHandler>>() {
            @Override protected Collection<IEppRealGroupOperationHandler> resolve() {
                return new ArrayList<>(
                ApplicationRuntime.getApplicationContext().getBeansOfType(IEppRealGroupOperationHandler.class).values()
                );
            }
        };

        @Override public Collection<IEppRealGroupOperationHandler> resolve(final Class<? extends EppRealEduGroup> key) {
            return CollectionUtils.select(this.handlers.get(), object -> ((IEppRealGroupOperationHandler)object).canHandle(key));
        }
    });


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected boolean isAutoSummaryMode() { return true; }

    @Override
    public boolean doCreateMissingSummary(final boolean force)
    {
        boolean result = false;
        final Date now = new Date();

        final Session session = this.lock4update();
        NamedSyncInTransactionCheckLocker.register(session, EppRealEduGroupSummary.class.getSimpleName());

        if (force || this.isAutoSummaryMode())
        {
            Debug.begin("EppRealGroupRowDAO.doCreateMissingSummary");
            try {

                // обновляем НПП (выставляем диспетчерские по умолчанию, что, наверное, плохо)
                if (this.fixEduOrgUnit)
                {
                    Debug.begin("fix-edu-out-unit");
                    try {
                        final TopOrgUnit academy = (TopOrgUnit)session.load(
                            TopOrgUnit.class,
                            new DQLSelectBuilder()
                            .fromEntity(TopOrgUnit.class, "a").column(property("a.id"))
                            .where(isNull(property(TopOrgUnit.parent().fromAlias("a"))))
                            .order(property(TopOrgUnit.id().fromAlias("a")))
                            .createStatement(session).setMaxResults(1).<Long>uniqueResult()
                        );

                        new DQLUpdateBuilder(EducationOrgUnit.class)
                        .where(isNull(property(EducationOrgUnitGen.L_OPERATION_ORG_UNIT)))
                        .set(EducationOrgUnitGen.L_OPERATION_ORG_UNIT, value(academy))
                        .createStatement(session).execute();
                    } finally {
                        Debug.end();
                    }
                }

                // формируем годо-части по МСРП
                Debug.begin("create-missing-year-part");
                try {
                    final DQLSelectBuilder dql = new DQLSelectBuilder();
                    dql.fromEntity(EppStudentWorkPlanElement.class, "slot");
                    dql.predicate(DQLPredicateType.distinct);
                    dql.column(property(EppStudentWorkPlanElement.year().id().fromAlias("slot")));
                    dql.column(property(EppStudentWorkPlanElement.part().id().fromAlias("slot")));
                    dql.where(not(exists(
                            EppYearPart.class,
                            EppYearPart.year().s(), property("slot", EppStudentWorkPlanElement.year()),
                            EppYearPart.part().s(), property("slot", EppStudentWorkPlanElement.part())
                    )));

                    final List<Object[]> rows = dql.createStatement(session).list();
                    for (final Object[] row: rows) {
                        session.save(new EppYearPart(
                            (EppYearEducationProcess)session.load(EppYearEducationProcess.class, (Long)row[0]),
                            (YearDistributionPart)session.load(YearDistributionPart.class, (Long)row[1])
                        ));
                    }

                    if (rows.size() > 0) {
                        result = true;
                        session.flush();
                        session.clear();
                    }
                } finally {
                    Debug.end();
                }

                // формируем сводки по МСРП
                Debug.begin("create-missing-summary");
                try {
                    final DQLSelectBuilder dql = new DQLSelectBuilder();
                    dql.fromEntity(EppStudentWorkPlanElement.class, "slot");
                    dql.joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("slot"), "stud");
                    dql.joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias("stud"), "eduou");

                    dql.joinEntity("slot", DQLJoinType.inner, EppYearPart.class, "yearpart", and(
                            eq(property(EppYearPart.year().fromAlias("yearpart")), property(EppStudentWorkPlanElement.year().fromAlias("slot"))),
                            eq(property(EppYearPart.part().fromAlias("yearpart")), property(EppStudentWorkPlanElement.part().fromAlias("slot")))
                    ));

                    dql.where(not(exists(
                            EppRealEduGroupSummary.class,
                            EppRealEduGroupSummary.owner().s(), property(EducationOrgUnit.operationOrgUnit().fromAlias("eduou")),
                            EppRealEduGroupSummary.yearPart().s(), property("yearpart")
                    )));
                    // только по текущему году ( в прошлом году диспетчерские могут отличаться :( )
                    dql.where(eq(property(EppStudentWorkPlanElement.year().educationYear().current().fromAlias("slot")), value(Boolean.TRUE)));

                    dql.predicate(DQLPredicateType.distinct);
                    dql.column(property(EducationOrgUnit.operationOrgUnit().id().fromAlias("eduou")));
                    dql.column(property("yearpart", "id"));

                    final List<Object[]> rows = dql.createStatement(session).list();
                    for (final Object[] row: rows) {
                        final EppRealEduGroupSummary summary = new EppRealEduGroupSummary(
                                (OrgUnit)session.load(OrgUnit.class, (Long)row[0]),
                                (EppYearPart)session.load(EppYearPart.class, (Long)row[1])
                        );
                        summary.setModificationDate(now);
                        session.save(summary);
                    }

                    if (rows.size() > 0) {
                        result = true;
                        session.flush();
                        session.clear();
                    }
                } finally {
                    Debug.end();
                }
            } finally {
                Debug.end();
            }
        }

        return result;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected Session lock4update() {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, EppStudentSlotDAO.LOCK_NAME_UPDATE_STUDENT_SLOT);
        NamedSyncInTransactionCheckLocker.register(session, EppRealGroupRowDAO.LOCK_NAME_UPDATE_ROWS);
        return session;
    }

    @Override
    public void lock() {
        this.lock4update();
    }

    private static final Long L0 = 0L;

    @Override
    public boolean doDisableUnnecessaryRelations()
    {
        boolean result = false;
        final Date now = new Date();
        final Session session = this.lock4update();

        Debug.begin("EppRealGroupRowDAO.doDisableUnnecessaryRelations");
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            final Collection<EppRealEduGroupRow.IEppRealEduGroupRowDescription> relations = this.getRelations();
            for (final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation: relations) {
                final String message = relation.relationClass().getSimpleName();

                Debug.begin(message+".delete-incorrect-relations");
                try {
                    /* удаляем связи студентов с группами, для которых слоты студентов не совпадают по виду нагрузки с группой и по времени (год, часть) со сводкой. */
                    /* дисциплина в данном случае совпадать не должна (потому как я могу объединять разные дисциплины, а читать какую-то одну) */

                    final DQLDeleteBuilder del = new DQLDeleteBuilder(relation.relationClass());
                    del.fromEntity(relation.relationClass(), "rel").where(eq(property("id") , property("rel", "id")));
                    del.where(not(and(
                        eq(property(EppRealEduGroupRow.studentWpePart().type().fromAlias("rel")), property(EppRealEduGroupRow.group().type().fromAlias("rel"))),
                        eq(property(EppRealEduGroupRow.studentWpePart().studentWpe().year().fromAlias("rel")), property(EppRealEduGroupRow.group().summary().yearPart().year().fromAlias("rel"))),
                        eq(property(EppRealEduGroupRow.studentWpePart().studentWpe().part().fromAlias("rel")), property(EppRealEduGroupRow.group().summary().yearPart().part().fromAlias("rel")))
                    )));

                    final int updateCount = CommonDAO.executeAndClear(del, session);
                    if (updateCount > 0) {
                        result = true;

                        final String msg = "delete "+updateCount+" `"+relation.relationClass().getSimpleName()+"` broken relations";
                        Debug.message("WARN: "+msg);
                        this.logger.warn(msg);
                    }
                } finally {
                    Debug.end();
                }

                Debug.begin(message+".delete-wrong-operation-org-unit-relations");
                try {
                    /* удаляем связи для студентов с других диспетчерских в УГС текущего года без степени готовности. */
                    /* строки надо именно удалять, иначе актуальные воскреснут ниже,... а удалять можно, т.к. это УГС без степени готовности. */
                    /* -> если кто-то поменял настройку диспетчерской, то студенты групп текущего года без степени готовности должны переехать */
                    /* -> если группу зафиксировали - то она переехать штатным механизмом демона не может */

                    final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(relation.relationClass(), "rel").column(property("rel", "id"));
                    dql.where(eq(property(EppRealEduGroupRow.group().summary().yearPart().year().educationYear().current().fromAlias("rel")), value(Boolean.TRUE))); // текущий учебны год
                    dql.where(isNull(property(EppRealEduGroupRow.group().level().fromAlias("rel")))); // степень готовности пустая
                    dql.where(isNotNull(property(EppRealEduGroupRow.studentEducationOrgUnit().fromAlias("rel")))); // у строки должно быть НПП
                    dql.where(ne(
                        // и для этого НПП должна быть иная диспетчерская
                        property(EppRealEduGroupRow.studentEducationOrgUnit().operationOrgUnit().fromAlias("rel") /* здесь можно и нужно юзать кэш, т.к. учебный год текущий и УГС без согласования */),
                        property(EppRealEduGroupRow.group().summary().owner().fromAlias("rel"))
                    ));

                    final int deleteCount = executeAndClear(
                        new DQLDeleteBuilder(relation.relationClass())
                        .where(in(property("id"), dql.buildQuery())),
                        session
                    );
                    if (deleteCount > 0) {
                        result = true;
                        final String msg = "delete "+deleteCount+" `"+relation.relationClass().getSimpleName()+"` relations (wrong operation orgunit)";
                        Debug.message(msg);
                        this.logger.info(msg);
                    }

                } finally {
                    Debug.end();
                }

                Debug.begin(message+".disable-unactual-relations");
                try {
                    /* отцепляем (деактуализируем) связи студентов с группами, для которых слоты студентов потеряли актуальность */

                    final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(relation.relationClass(), "rel").column(property("rel", "id"));
                    dql.where(isNotNull(property(EppRealEduGroupRow.studentWpePart().removalDate().fromAlias("rel"))));

                    final DQLUpdateBuilder upd = new DQLUpdateBuilder(relation.relationClass());
                    upd.set(EppRealEduGroupRow.removalDate().s(), value(now, PropertyType.DATE));
                    upd.where(in(property("id"), dql.buildQuery()));
                    upd.where(isNull(property(EppRealEduGroupRow.removalDate().s())));

                    final int updateCount = executeAndClear(upd, session);
                    if (updateCount > 0) {
                        result = true;
                        final String msg = "deactivate "+updateCount+" `"+relation.relationClass().getSimpleName()+"` relations (unactual)";
                        Debug.message(msg);
                        this.logger.info(msg);
                    }
                } finally {
                    Debug.end();
                }

                Debug.begin(message+".delete-duplicate-unactual-relations");
                try {
                    /* удаляем неактуальные записи УГС, которые ссылаются на одни и те же МСРП(АН,ФК): если таких записей более одной - то удаляем все, кроме наиболее актуальной */
                    final int deleteCount = this.fixDuplicateUnactualRelations(relation);
                    if (deleteCount > 0) {
                        result = true;
                        final String msg = "delete "+deleteCount+" `"+relation.relationClass().getSimpleName()+"` duplicate unactual relations";
                        Debug.message(msg);
                        this.logger.info(msg);
                    }
                } finally {
                    Debug.end();
                }

                Debug.begin(message+".delete-duplicate-actual-relations");
                try {
                    /* удаляем актуальные записи УГС, которые ссылаются на одни и те же МСРП(АН,ФК): если записей более одной - то удаляем все, кроме наиболее актуальной */
                    final int deleteCount = this.fixDuplicateRelations(
                        new DQLSelectBuilder()
                        .fromEntity(relation.relationClass(), "r")
                        .column(property("r"), "r")
                        .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("r")))),
                        EppRealEduGroupRow.studentWpePart().id()
                    );

                    if (deleteCount > 0) {
                        result = true;
                        final String msg = "delete "+deleteCount+" `"+relation.relationClass().getSimpleName()+"` duplicate actual relations";
                        Debug.message(msg);
                        this.logger.info(msg);
                    }
                } finally {
                    Debug.end();
                }

                Debug.begin(message+".delete-duplicate-students-in-edugroup");
                try {
                    /* удаляем записи УГС, которые ссылаются на одну и ту же связь студента с УП(в) в рамках одной УГС (в УГС добавлен студент два раза) */
                    final int deleteCount = this.fixDuplicateRelations(
                        new DQLSelectBuilder()
                        .fromEntity(relation.relationClass(), "r")
                        .column(property("r"), "r"),
                        EppRealEduGroupRow.group().id(),
                        EppRealEduGroupRow.studentWpePart().studentWpe().student().id()
                    );

                    if (deleteCount > 0) {
                        result = true;
                        final String msg = "delete "+deleteCount+" `"+relation.relationClass().getSimpleName()+"` duplicate student-in-group relations";
                        Debug.message(msg);
                        this.logger.info(msg);
                    }
                } finally {
                    Debug.end();
                }

                Debug.begin(message+".delete-empty-groups");
                try {
                    // удаляем левые записи и пустые группы
                    if (this.doCleanUpEmptyGroups(relation, null)) {
                        result = true;
                    }
                } finally {
                    Debug.end();
                }

            }
        } finally {
            eventLock.release();
            Debug.end();
        }
        return result;
    }


    // см. fixDuplicateRelations: сортирует записи УГС в рамках некоторой группировки по убыванию актуальности (первый элемент будет сохранен, остальные - убиты)
    private static final Comparator<Object[]> fixDuplicateRelationsRowComparator = (o1, o2) -> {

        // сначала актуальные, затем по убыванию даты утраты актуальности
        {
            final long rdt1 = null == o1[1] ? Long.MAX_VALUE : ((Date)o1[1]).getTime();
            final long rdt2 = null == o2[1] ? Long.MAX_VALUE : ((Date)o2[1]).getTime();
            final int cmp = Long.compare(rdt1, rdt2);
            if (0 != cmp) { return -cmp; /* в обратном порядке */ }
        }

        // по совпадению дисциплины (в УГС и МСРП) - сначала совпадающие
        {
            boolean s1 = o1[3].equals(o1[4]);
            boolean s2 = o2[3].equals(o2[4]);
            final int cmp = Boolean.compare(s1, s2);
            if (0 != cmp) { return -cmp; /* в обратном порядке */ }
        }

        // по убыванию даты модификации УГС
        {
            final long rdt1 = null == o1[2] ? Long.MAX_VALUE : ((Date)o1[2]).getTime();
            final long rdt2 = null == o2[2] ? Long.MAX_VALUE : ((Date)o2[2]).getTime();
            final int cmp = Long.compare(rdt1, rdt2);
            if (0 != cmp) { return -cmp; /* в обратном порядке */ }
        }

        // если все совпало - то по id
        return -Long.compare((Long)o1[0], (Long)o2[0]);
    };

    /**
     * Убирает из базы дублирующие записи: осуществляет поиск дублей записей УГС (критерии дублировнаия),
     * в случае обнаружения дублирующих записей - оставляет последнюю, остальные - удаляются из базы.
     * @param ds dqlSelectBuilder(EppRealEduGroupRow as r).column(property("r") as r) - запрос с условиями
     * @param groupPath параметры уникальности
     */
    protected int fixDuplicateRelations(final DQLSelectBuilder ds, final PropertyPath... groupPath)
    {
        // идея: поскольку это баг (и он должен быть не часто), делаем очень быструю проверку,
        // затем, только если проверка что-то выдала, начинаем грузить данные по каждой группировке

        final Session session = this.getSession();

        // список приговоренных к смерти (будем заполнять его)
        final Map<Class, List<Long>> sentencedToDeathMap = SafeMap.get(SafeMap.<Class, List<Long>>createCallback(ArrayList.class), 2);

        // группировка запроса, оставляем только те группировки, в которых более одной записи
        final DQLSelectBuilder groupDql = new DQLSelectBuilder().fromDataSource(ds.buildQuery(), "x")
        .having(gt(count(property("x.r.id")), value(1))); /* более одного */
        for (final PropertyPath element : groupPath) {
            final PropertyPath path = element.fromAlias("x.r");
            groupDql.column(property(path)).group(property(path));
        }

        // пробегаемся по каждой группировке: надо удалить все строки, кроме одной (последней)
        for (final Object[] groupRow: scrollRows(groupDql.createStatement(session)))
        {
            final DQLSelectBuilder relDql = new DQLSelectBuilder().fromDataSource(ds.buildQuery(), "x");
            for (int i=0;i<groupPath.length;i++) {
                relDql.where(eq(property(groupPath[i].fromAlias("x.r")), commonValue(groupRow[i])));
            }

            relDql.column(property(EppRealEduGroupRow.id().fromAlias("x.r")));
            relDql.column(property(EppRealEduGroupRow.removalDate().fromAlias("x.r")));
            relDql.column(property(EppRealEduGroupRow.group().modificationDate().fromAlias("x.r")));

            // для проверки совпадения дисциплины
            relDql.column(property(EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart().id().fromAlias("x.r")));
            relDql.column(property(EppRealEduGroupRow.group().activityPart().id().fromAlias("x.r")));

            // извлекаем данные группировки (для отладки)
            for (final PropertyPath element : groupPath) {
                final PropertyPath path = element.fromAlias("x.r");
                relDql.column(property(path));
            }

            final List<Object[]> rows = relDql.createStatement(session).list();
            if (rows.size() <= 1) { continue; /* O_o */ }

            // встать, суд идет
            Collections.sort(rows, fixDuplicateRelationsRowComparator);
            final Iterator<Object[]> it = rows.iterator();
            it.next(); // первому сохраняем жизнь, остальных - казним

            // остальных будем расстреливать (умертвлять будем группами - по классам записей)
            while (it.hasNext()) {
                final Long id = (Long)it.next()[0];
                final Class k = EntityRuntime.getMeta(id).getRoot().getEntityClass();
                sentencedToDeathMap.get(k).add(id);
            }
        }

        // удаляем (если там что-то осталось) приговоренных
        final MutableInt count = new MutableInt(0);
        for (final Map.Entry<Class, List<Long>> e: sentencedToDeathMap.entrySet()) {
            for (List<Long> ids : Lists.partition(e.getValue(), DQL.MAX_VALUES_ROW_NUMBER)) {
                count.add(
                        EppRealGroupRowDAO.this.executeAndClear(
                                new DQLDeleteBuilder(e.getKey())
                                        .where(in(property("id"), ids))
                        )
                );
            }
        }
        return count.intValue();
    }

    /** удаляет ВСЕ неактуальные записи УГС для МСРП(АН,ФК), ести таковых более одной */
    protected int fixDuplicateUnactualRelations(final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation)
    {
        return this.fixDuplicateRelations(
            new DQLSelectBuilder()
            .fromEntity(relation.relationClass(), "r")
            .column(property("r"), "r")
            .where(isNotNull(property(EppRealEduGroupRow.removalDate().fromAlias("r")))),
            EppRealEduGroupRow.studentWpePart().id()
        );

        //        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(relation.relationClass(), "r")
        //        .where(isNotNull(property(EppRealEduGroupRow.removalDate().fromAlias("x"))))
        //        .column(property(EppRealEduGroupRow.student().id().fromAlias("x")))
        //        .group(property(EppRealEduGroupRow.student().id().fromAlias("x")))
        //        .having(gt(count(property("x.id")), value(1))); /* более одного */
        //
        //        final int deleteCount = executeAndClear(
        //            new DQLDeleteBuilder(relation.relationClass())
        //            .where(isNotNull(property(EppRealEduGroupRow.removalDate()))) // только неактуальные (здесь условие нужно, т.к. сам запрос - это только группировка)
        //            .where(in(
        //                property(EppRealEduGroupRow.student().id()),
        //                dql.buildQuery()
        //            ))
        //        );
        //        return deleteCount;
    }

    /**
     * Удаляет неактивные строки из УГС, затем удаляет пустые группы
     *  если groupIds указан явно, то операция будет проведена с указанными группами,
     *  если groupIds не указан, то будут обработаны ВСЕ УГС (но только без степени готовности)
     *
     * @param relation описание связи
     * @param groupIds набор групп (nullable)
     * @return true, если что-то поменялось
     */
    protected boolean doCleanUpEmptyGroups(final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation, final Collection<Long> groupIds) {
        boolean result = false;
        final Session session = this.getSession();

        {
            // удаляем неактивные записи в группах: если группы не указаны - то во всех УГС без степени готовности
            final DQLDeleteBuilder del = new DQLDeleteBuilder(relation.relationClass());
            del.where(isNotNull(property(EppRealEduGroupRow.removalDate().s())));
            // ROW // считаем, что все ссылки на строки - авто-удаляемые
            // ROW // del.where(new DQLCanDeleteExpressionBuilder(relation.relationClass(), "id").getExpression()); // только те строки, на которые нет ссылок из других частей системы

            if (null != groupIds) {
                // если группы указаны - удаляем все из данных групп (без ограничения по уровню)
                del.where(in(property(EppRealEduGroupRow.group().id()), groupIds));
            } else {
                // если группы не указаны - удаляем все (но с ограничением по уровню)
                del.fromEntity(relation.relationClass(), "rel").where(eq(property("id") , property("rel", "id")));
                del.where(isNull(property(EppRealEduGroupRow.group().level().fromAlias("rel"))));
            }

            final int updateCount = CommonDAO.executeAndClear(del, session);
            if (updateCount > 0) {
                result = true;

                final String msg = "delete "+updateCount+" `"+relation.relationClass().getSimpleName()+"` unnecessary relations in groups with no level";
                Debug.message(msg);
                this.logger.info(msg);
            }
        }

        {
            // удаляем совсем пустые группы
            final DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(relation.groupClass(), "g").column(property("g.id"));

            if (null != groupIds) {
                // если группы указаны - удаляем именно эти групп (без ограничения по уровню)
                dql.where(in(property("g.id"), groupIds));
            } //else {
                // если группы не указаны - удаяем все (пустые группы удаляются ВСЕГДА, даже если они такими согласованы)
            //}

            // не должно быть записей (не важно каких) - это основной критерий
            dql.joinEntity("g", DQLJoinType.left, relation.relationClass(), "rel", eq(property("g"), property("rel.group")));
            dql.where(isNull(property("rel.id")));

            // дополнительные условия на группу
            for (final IEppRealGroupOperationHandler handler: this.handlerMap.get(relation.groupClass())) {
                handler.checkAutoDelete(relation, dql, "g");
            }

            // удаляем все группы (из запроса выше), на которые нет ссылок из других частей системы
            final DQLDeleteBuilder del = new DQLDeleteBuilder(relation.groupClass());
            del.where(new DQLCanDeleteExpressionBuilder(relation.groupClass(), "id").getExpression()); // только те группы, на которые нет ссылок из других частей системы
            del.where(in(property("id"), dql.buildQuery()));

            final int updateCount = executeAndClear(del, session);
            if (updateCount > 0) {
                result = true;

                final String msg = "delete "+updateCount+" `"+relation.groupClass().getSimpleName()+"` empty groups";
                Debug.message(msg);
                this.logger.info(msg);
            }
        }

        return result;
    }


    @Override
    public boolean doAwakeStudentRows()
    {
        boolean result = false;
        final Session session = this.lock4update();
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            // воскрешаем трупы
            Debug.begin("EppRealGroupRowDAO.doAwakeStudentRows.miracle-of-the-resurrection");
            try {
                final Collection<EppRealEduGroupRow.IEppRealEduGroupRowDescription> relations = this.getRelations();
                for (final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation: relations)
                {
                    // важно - здесь отдельная транзакция:  перед выполнением следующего запроса надо убедиться, что в базе нет дублей записей, утративших актуальность
                    final int deleteCount = this.fixDuplicateUnactualRelations(relation);
                    if (deleteCount > 0) {
                        result = true;
                        final String msg = "delete "+deleteCount+" `"+relation.relationClass().getSimpleName()+"` duplicate unactual relations";
                        Debug.message(msg);
                        this.logger.info(msg);
                    }

                    // затем, воскрешаем трупы (здесь уже неактуальные записи гарантированно уникальны - можно воскрешать через update, чтобы не грузить)
                    final int updateCount = executeAndClear(
                        new DQLUpdateBuilder(relation.relationClass())
                        .set(EppRealEduGroupRow.P_REMOVAL_DATE, nul(PropertyType.TIMESTAMP))
                        .where(isNotNull(property(EppRealEduGroupRow.removalDate())))
                        .where(in(
                            property(EppRealEduGroupRow.studentWpePart().id()),
                            this.buildProcessedEppStudentWpSlotRowsDqlBase(relation)
                            .column(property("s_x.id"))
                            .buildQuery()
                        )),
                        session
                    );
                    if (updateCount > 0) {
                        result = true;
                        final String msg = "wake up "+updateCount+" `"+relation.relationClass().getSimpleName()+"` removal relations";
                        Debug.message(msg);
                        this.logger.info(msg);
                    }
                }
            } finally {
                Debug.end();
            }

        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            eventLock.release();
        }

        return result;
    }


    @Override
    public boolean doUpdateStudentRowList()
    {
        final boolean info = this.logger.isInfoEnabled();
        final MutableBoolean result = new MutableBoolean(false);
        final Session session = this.lock4update();

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {

            // делаем в несколько потоков - иначе юзатеся один проц, при этом вся система курит
            try (SyncDaemon.ExecutorService executor = EppRealGroupRowDAO.DAEMON.getExecutorService(false, this.UPDATE_THREAD_COUNT.get()))
            {

                int runnable_number = 0;
                final Collection<IEppRealEduGroupRowDescription> relations = this.getRelations();
                for (final IEppRealEduGroupRowDescription relation : relations)
                {
                    final List<Object[]> summaryTypePairList = this.getSummaryTypePairList(relation);
                    for (final Object[] e : summaryTypePairList)
                    {

                        Debug.begin("runnable[" + (runnable_number++) + "]: " + Arrays.asList(e).toString());

                        ////////////////////////////
                        // начало дейсвий в debug //

                        final OrgUnit owner = (OrgUnit) session.load(OrgUnit.class, (Long) e[0]);

                        final EppYearEducationProcess year = (EppYearEducationProcess) session.load(EppYearEducationProcess.class, (Long) e[1]);
                        final YearDistributionPart part = (YearDistributionPart) session.load(YearDistributionPart.class, (Long) e[2]);
                        final EppYearPart yearpart = HibSupportUtils.getByNaturalId(session, new EppYearPartGen.NaturalId(year, part));
                        if (null == yearpart)
                        {
                            // это означает, что не создались у нас все необходимые годо-части
                            // это не ошибка, всеголишь есть данные с датой назад
                            // FIXME:XXX: ввести флаг, создающий нужные годо-части
                            if (info)
                            {
                                final String message = "no-YearPart(year=" + year.getId() + ", part=" + part.getId() + ")";
                                this.logger.info(message);
                                Debug.message("WARN: " + message);
                            }
                            Debug.end();
                            continue;
                        }

                        final EppRealEduGroupSummary summary = HibSupportUtils.getByNaturalId(session, new EppRealEduGroupSummary.NaturalId(owner, yearpart));
                        if (null == summary)
                        {
                            // это означает, что не создались у нас все необходимые сводки
                            // это не ошибка, всеголишь есть данные с датой назад
                            // FIXME:XXX: ввести флаг, создающий нужные сводки
                            if (info)
                            {
                                final String message = "no-Summary(owner=" + owner.getId() + ", yearpart=" + yearpart.getId() + ")";
                                this.logger.info(message);
                                Debug.message("WARN: " + message);
                            }
                            Debug.end();
                            continue;
                        }

                        final EppGroupType type = HibSupportUtils.getEntityById(session, (Long) e[3]);

                        // окончание действий в debug //
                        ////////////////////////////////

                        Debug.end();

                        executor.submit(new Runnable()
                        {
                            @Override
                            public String toString()
                            {
                                return "doUpdateStudentRowList(summary=" + summary.getId() + ", type=" + type.getId() + ")." + relation.relationClass().getSimpleName();
                            }

                            @Override
                            public void run()
                            {
                                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                                try
                                {
                                    // нужно выполнять в отдельных транзакциях
                                    final boolean r = IEppRealGroupRowDAO.instance.get().doUpdateStudentRowList(relation, summary, type);
                                    if (r)
                                    {
                                        synchronized (result)
                                        {
                                            result.setValue(true);
                                        }
                                    }
                                }
                                finally
                                {
                                    eventLock.release();
                                }
                            }
                        });
                    }
                }

            }
            // ждем, пока все закончится


        } catch (final Exception t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        } finally {
            eventLock.release();
        }

        return result.booleanValue();
    }


    @Override
    public boolean doUpdateStudentRowList(final IEppRealEduGroupRowDescription relation, final EppRealEduGroupSummary summary, final EppGroupType type)
    {
        final Date now = new Date();
        final Session session = this.getSession();
        final MutableBoolean result = new MutableBoolean(false);

        final List<Long> unactualRelationIdsList = new ArrayList<>();
        final List<Long> studentWithoutGroupIdsList = new ArrayList<>();
        final Map<Long, Long> studentWithoutRelationsMap = new HashMap<>();

        final Map<Long, Collection<Long>> uncoveredSlotMap = this.getUncoveredSlotsMap(relation, summary, type);
        Debug.begin("prepare");
        try {
            for (final Map.Entry<Long, Collection<Long>> slEntry: uncoveredSlotMap.entrySet()) {
                final EppRegistryElementPart activityPart = (EppRegistryElementPart)session.load(EppRegistryElementPart.class, slEntry.getKey());
                for (List<Long> elements : Iterables.partition(slEntry.getValue(), DQL.MAX_VALUES_ROW_NUMBER)) {

                    final DQLSelectBuilder dql_x = new DQLSelectBuilder();

                    dql_x.fromEntity(relation.studentClass(), "s_x");
                    dql_x.where(in(property("s_x.id"), elements));

                    dql_x.joinPath(DQLJoinType.inner, EppStudentWpePart.studentWpe().fromAlias("s_x"), "slot_x");
                    dql_x.joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("slot_x"), "stud_x");

                    // занимаемся поиском группы-кандидата для включения
                    final DQLSelectBuilder dql_y = new DQLSelectBuilder();

                    // аутуальные записи
                    dql_y.fromEntity(relation.relationClass(), "rel");
                    dql_y.where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))));

                    // группы которых без согласования, с нужной сводки, нужного типа, того же элемента реестра
                    dql_y.joinPath(DQLJoinType.inner, EppRealEduGroupRow.group().fromAlias("rel"), "grp");
                    dql_y.where(eq(property(EppRealEduGroup.summary().fromAlias("grp")), value(summary)));
                    dql_y.where(eq(property(EppRealEduGroup.type().fromAlias("grp")), value(type)));
                    dql_y.where(eq(property(EppRealEduGroup.activityPart().fromAlias("grp")), value(activityPart)));
                    dql_y.where(isNull(property(EppRealEduGroup.level().fromAlias("grp"))));

                    dql_y.joinPath(DQLJoinType.inner, EppRealEduGroupRow.studentWpePart().fromAlias("rel"), "s");
                    dql_y.joinPath(DQLJoinType.inner, EppStudentWpePart.studentWpe().fromAlias("s"), "slot");
                    dql_y.joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("slot"), "stud");

                    dql_y.column(property("rel", "id"), "rel_id");
                    dql_y.column(property("grp", "id"), "grp_id");
                    dql_y.column(property("stud", StudentGen.L_GROUP), "stud_"+StudentGen.L_GROUP);

                    // цепляпем: у записей должна совпадать текущая академическая группа студента (либо одинаковые, либо пустые) - все остальное (сводка, тип, часть элемента реестра) - уже совпадают
                    dql_x.joinDataSource("stud_x", DQLJoinType.left, dql_y.buildQuery(), "y", or(
                            eq(property("stud_x", StudentGen.L_GROUP), property("y.stud_"+StudentGen.L_GROUP)),  // либо совпадают
                            and(isNull(property("stud_x", StudentGen.L_GROUP)), isNull(property("y.stud_"+StudentGen.L_GROUP)))  // лабо оба null
                    ));

                    // нам нужна еще и связь, если вдруг она была раньше с указанной группой
                    dql_x.joinEntity("y", DQLJoinType.left, relation.relationClass(), "rel_x", and(
                            eq(property(EppRealEduGroupRow.studentWpePart().id().fromAlias("rel_x")), property("s_x", "id")),
                            eq(property(EppRealEduGroupRow.group().id().fromAlias("rel_x")), property("y", "grp_id"))
                    ));

                    dql_x.group(property("s_x", "id"));
                    dql_x.group(property("y", "grp_id"));
                    dql_x.group(property("rel_x", "id"));

                    dql_x.column(property("s_x", "id"));
                    dql_x.column(property("y", "grp_id"));
                    dql_x.column(property("rel_x", "id"));

                    dql_x.column(DQLFunctions.count(DQLPredicateType.distinct, property("y", "rel_id")));

                    final List<Object[]> rows = dql_x.createStatement(session).list();
                    if (rows.size() > 0) {

                        final Map<Long, TreeMap<Integer, Long>> s2groupMap = new HashMap<>(rows.size());
                        for (final Object[] row: rows) {
                            final Long studentWpLoadSlotId = (Long)row[0];
                            final Long realGroupId = (Long)row[1];
                            final Long existRelationId = (Long)row[2];
                            final int count = ((Number)row[3]).intValue();
                            final TreeMap<Integer, Long> gMap = SafeMap.safeGet(s2groupMap, studentWpLoadSlotId, TreeMap.class);

                            if (null != existRelationId) {
                                // если связь существует - всегда сохраняем ее (даже если там была другая связь или группа)
                                gMap.put(count, existRelationId);
                            } else {
                                // если связи нет, то мы сохраняем групп только если там пусто (это сделано, чтобы группы не перетирали существующие связи)
                                if (null == gMap.get(count)) { gMap.put(count, realGroupId); }
                            }
                        }

                        final IEntityMeta RELATION_META = EntityRuntime.getMeta(relation.relationClass());
                        final IEntityMeta GROUP_META = EntityRuntime.getMeta(relation.groupClass());

                        for (final Map.Entry<Long, TreeMap<Integer, Long>> e: s2groupMap.entrySet()) {
                            final Long studentWpLoadSlotId = e.getKey();
                            final Map.Entry<Integer, Long> last = e.getValue().lastEntry();
                            if ((null == last) || (null == last.getValue())) {
                                // подходящей группы нет, ровно как и нет уже существующей связи с ней
                                // надо будет делать группу и создавать с ней связь
                                studentWithoutGroupIdsList.add(studentWpLoadSlotId);

                            } else {
                                final Long relatedId = last.getValue();
                                final IEntityMeta meta = EntityRuntime.getMeta(relatedId);
                                if (Objects.equals(RELATION_META, meta)) {
                                    // уже есть связь с группой (ее надо актуализировать)
                                    // набо актуализировать связь
                                    unactualRelationIdsList.add(relatedId);

                                } else if (Objects.equals(GROUP_META, meta)) {
                                    // связи, увы, нет (но есть группа)
                                    // надо создавать связь
                                    studentWithoutRelationsMap.put(studentWpLoadSlotId, relatedId);

                                } else {
                                    throw new IllegalStateException(meta.getName());
                                }
                            }
                        }
                    }

                    EppRealGroupRowDAO.this.infoActivity('=');
                }
            }
        } finally {
            Debug.end();
        }

        // актуализируем все неактуальные связи
        Debug.begin("wakeup-rows");
        try {
            for (List<Long> relationIds : Lists.partition(unactualRelationIdsList, DQL.MAX_VALUES_ROW_NUMBER)) {

                for (final EppRealEduGroupRow row: EppRealGroupRowDAO.this.getList(relation.relationClass(), "id", relationIds)) {
                    FastBeanUtils.setValue(row, EppRealEduGroupRow.P_REMOVAL_DATE, null);
                    session.saveOrUpdate(row);
                }

                session.flush();
                session.clear();
                EppRealGroupRowDAO.this.infoActivity('.');
                result.setValue(true);
            }
        } finally {
            Debug.end();
        }

        // создаем связи с уже существующими группами
        Debug.begin("insert-rows-in-groups");
        try {
            for (List<Long> studentIds : Iterables.partition(studentWithoutRelationsMap.keySet(), 256)) {
                for (final Long studentId: studentIds) {
                    final Long groupId = studentWithoutRelationsMap.get(studentId);
                    final EppRealEduGroup group = (EppRealEduGroup)session.load(relation.groupClass(), groupId);
                    final EppStudentWpePart student = (EppStudentWpePart)session.load(relation.studentClass(), studentId);
                    session.save(relation.buildRelation(group, student));
                }
                session.flush();
                session.clear();
                EppRealGroupRowDAO.this.infoActivity('.');
                result.setValue(true);
            }
        } finally {
            Debug.end();
        }

        // создаем группы для студентов и цепляем к ним студентов
        // для каждой дисциплиночасти и академ.группы будет создана одна УГС
        Debug.begin("create-rows-with-groups");
        try {
            BatchUtils.execute(studentWithoutGroupIdsList, 256, new BatchUtils.Action<Long>() {

                /* { regElPart.id -> regElPart } */
                private final Map<Long, EppRegistryElementPart> activityCacheMap = EppRealGroupRowDAO.this.getLoadCacheMap(EppRegistryElementPart.class);

                /* { regElPart.id -> { grpup.id -> edugroup.id } }*/
                private final Map<Long, Map<Long, EppRealEduGroup>> globalMap = new HashMap<>();

                @Override public void execute(final Collection<Long> studentIds) {

                    final DQLSelectBuilder dql = new DQLSelectBuilder();
                    dql.fromEntity(relation.studentClass(), "s_x").where(in(property("s_x", "id"), studentIds));
                    dql.column(property(EppStudentWpePart.id().fromAlias("s_x")));
                    dql.column(property(EppStudentWpePart.studentWpe().registryElementPart().id().fromAlias("s_x")));
                    dql.column(property(EppStudentWpePart.studentWpe().student().group().id().fromAlias("s_x")));

                    for (final Object[] row: dql.createStatement(session).<Object[]>list()) {
                        final Long s_id = (Long)row[0];
                        final Long actv_id = (Long)row[1];
                        final Long grup_id = (null == row[2] ? EppRealGroupRowDAO.L0 : (Long)row[2]);

                        final Map<Long, EppRealEduGroup> groupMap = SafeMap.safeGet(this.globalMap, actv_id, HashMap.class);

                        EppRealEduGroup group = groupMap.get(grup_id);
                        if (null == group) {

                            try {
                                final Group g = (EppRealGroupRowDAO.L0 == grup_id ? null : (Group) session.get(Group.class, grup_id));
                                final String title = ((null == g ? "" : (g.getTitle()+" "))+DateFormatter.DATE_FORMATTER_WITH_TIME.format(now));

                                final EppRegistryElementPart activityPart = this.activityCacheMap.get(actv_id);
                                group = relation.buildGroup(summary, activityPart, type, null, title, now);

                            } catch (final Throwable t) {
                                throw CoreExceptionUtils.getRuntimeException(t);
                            }
                            session.save(group);
                            groupMap.put(grup_id, group);
                        }

                        final EppStudentWpePart student = (EppStudentWpePart)session.load(relation.studentClass(), s_id);
                        session.save(relation.buildRelation(group, student));
                    }

                    session.flush();
                    session.clear();
                    EppRealGroupRowDAO.this.infoActivity('.');
                    result.setValue(true);
                }
            });
        } finally {
            Debug.end();
        }

        return false;
    }

    /* @return { orgUnit.id, eppYear.id, gridTerm.id, loadType.id } */
    protected List<Object[]> getSummaryTypePairList(final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation)
    {
        Debug.begin("getSummaryTypePairList."+relation.relationClass().getSimpleName());
        try {

            final DQLSelectBuilder dql = this.buildProcessedEppStudentWpSlotRowsDql(relation);
            dql.predicate(DQLPredicateType.distinct);
            dql.column(property(EducationOrgUnitGen.operationOrgUnit().id().fromAlias("eduou_x")));
            dql.column(property(EppStudentWorkPlanElement.year().id().fromAlias("slot_x")));
            dql.column(property(EppStudentWorkPlanElement.part().id().fromAlias("slot_x")));
            dql.column(property(EppStudentWpePart.type().id().fromAlias("s_x")));

            // только те, для которых есть сводка
            dql.where(exists(
                new DQLSelectBuilder()
                .fromEntity(EppRealEduGroupSummary.class, "s").column(property("s.id"))
                .where(eq(property(EppRealEduGroupSummary.owner().fromAlias("s")), property(EducationOrgUnitGen.operationOrgUnit().fromAlias("eduou_x"))))
                .where(eq(property(EppRealEduGroupSummary.yearPart().year().fromAlias("s")), property(EppStudentWorkPlanElement.year().fromAlias("slot_x"))))
                .where(eq(property(EppRealEduGroupSummary.yearPart().part().fromAlias("s")), property(EppStudentWorkPlanElement.part().fromAlias("slot_x"))))
                .buildQuery()
            ));

            return dql.createStatement(this.getSession()).list();
        } finally {
            Debug.end();
        }
    }

    /** @return { wpSlotRow.slot.registryElementPart.id -> { wpSlotRow.id } } */
    protected Map<Long, Collection<Long>> getUncoveredSlotsMap(final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation, final EppRealEduGroupSummary summary, final EppGroupType type)
    {
        Debug.begin("getUncoveredSlots."+relation.relationClass().getSimpleName());
        try {
            final DQLSelectBuilder dql = this.buildProcessedEppStudentWpSlotRowsDql(relation);
            dql.predicate(DQLPredicateType.distinct);
            dql.where(eq(property(EppStudentWpePart.type().id().fromAlias("s_x")), value(type.getId())));
            dql.where(eq(property(EducationOrgUnitGen.operationOrgUnit().fromAlias("eduou_x")), value(summary.getOwner())));
            dql.where(eq(property(EppStudentWorkPlanElement.year().fromAlias("slot_x")), value(summary.getYearPart().getYear())));
            dql.where(eq(property(EppStudentWorkPlanElement.part().fromAlias("slot_x")), value(summary.getYearPart().getPart())));

            dql.column(property(EppStudentWpePart.studentWpe().registryElementPart().id().fromAlias("s_x")));
            dql.column(property(EppStudentWpePart.id().fromAlias("s_x")));
            final Map<Long, Collection<Long>> result = SafeMap.get(ArrayList.class);
            final List<Object[]> rows = dql.createStatement(this.getSession()).list();
            for (final Object[] row: rows) { result.get((Long) row[0]).add((Long) row[1]); }
            return result;
        } finally {
            Debug.end();
        }
    }

    /* @return dql( studentWpSlotRow as s_x where removalDate is null and not exists realEduGroupRow ) */
    protected DQLSelectBuilder buildProcessedEppStudentWpSlotRowsDql(final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation) {
        return this.buildProcessedEppStudentWpSlotRowsDqlBase(relation)
        .joinPath(DQLJoinType.inner, EppStudentWpePart.studentWpe().fromAlias("s_x"), "slot_x")
        .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("slot_x"), "stud_x")
        .joinPath(DQLJoinType.inner, StudentGen.educationOrgUnit().fromAlias("stud_x"), "eduou_x");
    }

    /* @return dql( studentWpSlotRow as s_x where removalDate is null and not exists realEduGroupRow ) */
    protected DQLSelectBuilder buildProcessedEppStudentWpSlotRowsDqlBase(final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(relation.studentClass(), "s_x");

        /* условия работы с записью всего два */ {
            // запись должна быть актуальная
            dql.where(isNull(property(EppStudentWpePart.removalDate().fromAlias("s_x"))));

            // и по ней не должно быть активного слота
            // not-in использовать нельзя, т.к. объем данных слишком большой
            dql.joinEntity("s_x", DQLJoinType.left, relation.relationClass(), "s_x_relation", and(
                isNull(property(EppRealEduGroupRow.removalDate().fromAlias("s_x_relation"))),
                eq(property(EppRealEduGroupRow.studentWpePart().id().fromAlias("s_x_relation")), property("s_x", "id"))
            )).where(isNull(property("s_x_relation", "id")));
        }

        return dql;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean doUpdateStudentRowData(final UpdateStudentRowDataCondition condition)
    {
        boolean result = false;
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, EppRealGroupRowDAO.LOCK_NAME_UPDATE_ROWS);

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            final String message = "EppRealGroupRowDAO.doUpdateStudentRowData("+String.valueOf(condition)+")";
            Debug.begin(message);
            try {

                final Collection<EppRealEduGroupRow.IEppRealEduGroupRowDescription> relations = this.getRelations();
                for (final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation: relations) {
                    Debug.begin(message+": "+relation.relationClass().getSimpleName());
                    try {

                        final DQLSelectBuilder dql = new DQLSelectBuilder();
                        dql.fromEntity(relation.relationClass(), "x");
                        dql.joinPath(DQLJoinType.left, EppRealEduGroupRow.studentWpePart().studentWpe().student().fromAlias("x"), "s");
                        dql.joinPath(DQLJoinType.left, Student.group().fromAlias("s"), "g");
                        dql.column(property("x", "id"), "id");
                        dql.column(property(Student.educationOrgUnit().fromAlias("s")), "eduOu");
                        dql.column(property(Group.title().fromAlias("g")), "gTitle");

                        final IDQLExpression c = (null == condition ? null : condition.condition(relation));
                        if (null != c)
                        {
                            // если условие есть
                            dql.where(or(
                                // либо соответствует условию для активных строк текущего года
                                and(
                                    c,
                                    eq(property(EppRealEduGroupRow.studentWpePart().studentWpe().year().educationYear().current().fromAlias("x")), value(Boolean.TRUE)), /* текущий год */
                                    isNull(property(EppRealEduGroupRow.removalDate().fromAlias("x"))), /* строка еще активна */

                                    // и значения не совпадают
                                    not(and(
                                        eq(property(EppRealEduGroupRow.studentEducationOrgUnit().fromAlias("x")), property(Student.educationOrgUnit().fromAlias("s"))),
                                        or(
                                            and(isNull(property("g")), isNotNull(EppRealEduGroupRow.studentGroupTitle().fromAlias("x"))),
                                            eq(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("x")), property(Group.title().fromAlias("g")))
                                        )
                                    ))
                                ),

                                // либо ЗАПОЛНЯЕМ пустые данные из студента (и это всегда, т.к. заполнить надо хоть чем-нибудь)
                                // делаем это всегда, т.к. их так или иначе обновит потом демон
                                isNull(property(EppRealEduGroupRow.studentEducationOrgUnit().fromAlias("x")))
                            ));
                        }
                        else
                        {
                            // если доп. условия нет
                            dql.where(
                                // ЗАПОЛНЯЕМ пустые данные из студента (и это всегда, т.к. заполнить надо хоть чем-нибудь)
                                // делаем это всегда, т.к. их так или иначе обновит потом демон
                                isNull(property(EppRealEduGroupRow.studentEducationOrgUnit().fromAlias("x")))
                            );
                        }


                        final DQLUpdateBuilder upd = new DQLUpdateBuilder(relation.relationClass());
                        upd.fromDataSource(dql.buildQuery(), "xxx").where(eq(property("id"), property("xxx", "id")));
                        upd.set(EppRealEduGroupRow.L_STUDENT_EDUCATION_ORG_UNIT, property("xxx", "eduOu"));
                        upd.set(EppRealEduGroupRow.P_STUDENT_GROUP_TITLE, property("xxx", "gTitle"));

                        final int updateCount = executeAndClear(upd, session);
                        if (updateCount > 0) {
                            result = true;

                            final String msg = "update "+updateCount+" `"+relation.relationClass().getSimpleName()+"` student-data ";
                            Debug.message(msg);
                            this.logger.info(msg);
                        }

                    } finally {
                        Debug.end();
                    }
                }
            } finally {
                Debug.end();
            }
            return result;
        } finally {
            eventLock.release();
        }
    }

    @Override
    public boolean doUpdateStudentRowData()
    {
        // обновляем все, в текущем году в группах без уровня
        return this.doUpdateStudentRowData(new UpdateStudentRowDataCondition() {
            @Override public IDQLExpression condition(final IEppRealEduGroupRowDescription relation) {
                return eq(value(1), value(1));// не null что бы обновил все записи, а не только с не указанным НПП и группой.
            }
            @Override public String toString() {
                return "all-current-year-active-only";
            }
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean doFixCurrentYearEduGroupSummary()
    {
        boolean result = false;
        final Date now = new Date();
        final Session session = this.lock4update();

        //  создаем недостающие сводки (а куда перемещать то?) и сносим левые записи
        this.doCreateMissingSummary(true);
        this.doDisableUnnecessaryRelations(); // вызывает cleanUpEmptyGroups

        Debug.begin("EppRealGroupRowDAO.doFixCurrentYearEduGroupSummary");
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {

            final Collection<EppRealEduGroupRow.IEppRealEduGroupRowDescription> relations = this.getRelations();
            for (final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation: relations) {
                final String message = relation.relationClass().getSimpleName();

                Debug.begin(message+".try-to-move-groups");
                try {
                    // в текущем учебном году
                    // если в группе все активные студенты имеют на данный момент однй и ту же диспетчерскую. при этом она не совпадает с той, которая указана в УГС - пененосим всю УГС

                    final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(relation.relationClass(), "rel");
                    dql.where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel")))); // активная связь
                    dql.column(property(EppRealEduGroupRow.group().id().fromAlias("rel")), "gr_id");
                    dql.column(property(EppRealEduGroupRow.studentEducationOrgUnit().operationOrgUnit().id().fromAlias("rel")), "so_id");
                    dql.predicate(DQLPredicateType.distinct);

                    // только те группы, в которых в текущем учебном году есть различия между диспетчерскими студентов и групп
                    dql.where(in(
                        property(EppRealEduGroupRow.group().id().fromAlias("rel")),
                        new DQLSelectBuilder()
                        .fromEntity(relation.relationClass(), "x").column(property(EppRealEduGroupRow.group().id().fromAlias("x")))
                        .where(eq(property(EppRealEduGroupRow.group().summary().yearPart().year().educationYear().current().fromAlias("x")), value(Boolean.TRUE)))
                        .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("x"))))
                        .where(ne(
                            property(EppRealEduGroupRow.studentEducationOrgUnit().operationOrgUnit().fromAlias("x") /* здесь можно и нужно юзать кэш, т.к. проверяем не студента а запись */),
                            property(EppRealEduGroupRow.group().summary().owner().fromAlias("x"))
                        ))
                        .buildQuery()
                    ));

                    // group.id -> operationOrgUnit.id
                    final Map<Long, List<Long>> moveCondidateMap = SafeMap.get(ArrayList.class);
                    for (final Object[] row: scrollRows(dql.createStatement(session))) {
                        moveCondidateMap.get((Long) row[0]).add((Long) row[1]);
                    }

                    final Map<Long, OrgUnit> operationOrgUnitMap = this.getLoadCacheMap(OrgUnit.class);
                    for (final Map.Entry<Long, List<Long>> e: moveCondidateMap.entrySet()) {
                        if (e.getValue().size() > 1) {
                            final String msg = "group(id="+e.getKey()+"): too many operationOrgUnits("+e.getValue().toString()+")";
                            Debug.message(msg);
                            this.logger.warn(msg);
                            continue; /* увы, для этой группы несколько разных диспетчерских - переносить нельзя */
                        }

                        // переносим группу на другую диспетчерскую
                        final EppRealEduGroup group = this.get(e.getKey());
                        final OrgUnit operationOrgUnit = operationOrgUnitMap.get(e.getValue().get(0));
                        final EppRealEduGroupSummary newSummary = this.getByNaturalId(new EppRealEduGroupSummary.NaturalId(operationOrgUnit, group.getSummary().getYearPart()));
                        if (null == newSummary) {
                            final String msg = "group(id="+group.getId()+"): no summary for (ou="+operationOrgUnit.getId()+", yp="+group.getSummary().getYearPart().getId()+")";
                            Debug.message(msg);
                            this.logger.warn(msg);
                            continue; /* не нашли диспетчерскую */
                        }

                        final String msg = "group(id="+group.getId()+"): move-to-summary(id="+newSummary.getId()+")";
                        Debug.message(msg);
                        this.logger.info(msg);

                        FastBeanUtils.setValue(group, EppRealEduGroup.L_SUMMARY, newSummary);
                        session.saveOrUpdate(group);
                        result = true;
                    }

                    // явно сохраняем все, что нагородили
                    session.flush();

                } finally {
                    Debug.end();
                }


                Debug.begin(message+".disable-relations-in-non-movable-groups");
                try {
                    // в текущем учебном году
                    // все записи, для которых не совпадает диспетчерская - делаем неактуальными (не важно какая степень готовности - действие делается вручную)

                    final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(relation.relationClass(), "rel").column(property("rel", "id"));
                    dql.where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel")))); // активная связь (зачем убирать актуальность у неактуальной строки)
                    dql.where(eq(property(EppRealEduGroupRow.group().summary().yearPart().year().educationYear().current().fromAlias("rel")), value(Boolean.TRUE))); // текущий учебны год
                    dql.where(ne(
                        property(EppRealEduGroupRow.studentEducationOrgUnit().operationOrgUnit().fromAlias("rel") /* здесь можно и нужно юзать кэш, т.к. проверяем не студента а запись */),
                        property(EppRealEduGroupRow.group().summary().owner().fromAlias("rel"))
                    ));

                    final DQLUpdateBuilder upd = new DQLUpdateBuilder(relation.relationClass());
                    upd.set(EppRealEduGroupRow.removalDate().s(), value(now, PropertyType.DATE));
                    upd.where(in(property("id"), dql.buildQuery()));
                    upd.where(isNull(property(EppRealEduGroupRow.removalDate().s())));

                    final int updateCount = CommonDAO.executeAndClear(upd, session);
                    if (updateCount > 0) {
                        result = true;

                        final String msg = "force deactivate "+updateCount+" `"+relation.relationClass().getSimpleName()+"` relations (wrong operation orgunit)";
                        Debug.message(msg);
                        this.logger.info(msg);
                    }

                } finally {
                    Debug.end();
                }
            }

        } finally {
            eventLock.release();
            Debug.end();
        }

        // запускаем демон после действия (он все почистит)
        DAEMON.registerAfterCompleteWakeUp(session);

        return result;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////


    interface IGroupParameters {
        EppRealEduGroupSummary summary();
        EppGroupType type();
        IEppRealEduGroupRowDescription relation();
    }

    @SuppressWarnings("unchecked")
    protected IGroupParameters getGroupParameters(final DQLSelectBuilder relBuilder)
    {
        final Session session = this.getSession();

        final EppRealEduGroupSummary summary;
        final EppGroupType type;
        final Class<? extends EppRealEduGroup> groupClass;

        {
            final DQLSelectBuilder tmp = new DQLSelectBuilder();
            tmp.fromEntity(EppRealEduGroupRow.class, "tmp").column(property(EppRealEduGroupRow.group().id().fromAlias("tmp")));
            tmp.where(in(property("tmp", "id"), relBuilder.buildQuery()));

            final DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(EppRealEduGroup.class, "grp").where(in(property("grp", "id"), tmp.buildQuery()));
            dql.column(property("grp", EppRealEduGroup.summary().id()));
            dql.column(property("grp", EppRealEduGroup.type().id()));
            dql.column(property("grp", EppRealEduGroup.clazz()));
            dql.predicate(DQLPredicateType.distinct);
            final List<Object[]> rows = dql.createStatement(session).list();

            if (rows.isEmpty()) { throw new IllegalArgumentException("No such groups"); }
            if (rows.size() > 1) { throw new IllegalArgumentException("Multiple (summary+type) combination"); }

            final Object[] row = rows.iterator().next();
            summary = (EppRealEduGroupSummary)session.load(EntityRuntime.getMeta((Long)row[0]).getEntityClass(), (Long)row[0]);
            type = (EppGroupType)session.load(EntityRuntime.getMeta((Long)row[1]).getEntityClass(), (Long)row[1]);
            groupClass = EntityRuntime.getMeta(((Number)row[2]).shortValue()).getEntityClass();
        }

        final IEppRealEduGroupRowDescription relation = EppRealEduGroupRow.group2descriptionMap.get(groupClass);
        return new IGroupParameters() {
            @Override public EppRealEduGroupSummary summary() { return summary; }
            @Override public EppGroupType type() { return type; }
            @Override public IEppRealEduGroupRowDescription relation() { return relation; }
        };
    }

    @Override
    public void doSetLevel(final Collection<Long> groupIds, final EppRealEduGroupCompleteLevel targetLevel)
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, EppRealGroupRowDAO.LOCK_NAME_UPDATE_ROWS);

        for (List<Long> ids : Iterables.partition(groupIds, DQL.MAX_VALUES_ROW_NUMBER)) {
            final List<EppRealEduGroup> groups = EppRealGroupRowDAO.this.getList(EppRealEduGroup.class, "id", ids);
            for (final EppRealEduGroup group : groups) {
                group.setLevel(targetLevel);
                session.saveOrUpdate(group);
            }
        }

        session.flush();
        this.cleanUpGroupsAfterChange(groupIds);
    }

    @Override
    public void doResetLevel(Collection<Long> groupIds) {
        checkGroupLockedLevel(groupIds, null /* не проверять соответсвие уровню */);
        doSetLevel(groupIds, null /* сбросить уровень */);
    }

    @Override
    public void checkGroupLockedLevel(Collection<Long> groupIds, EppRealEduGroupCompleteLevel level) throws RuntimeException {
        checkGroupLocked(groupIds, level, true);
    }

    @Override
    public void checkGroupLockedEdit(final Collection<Long> groupIds, final EppRealEduGroupCompleteLevel level) throws RuntimeException {
        checkGroupLocked(groupIds, level, false);
    }

    protected void checkGroupLocked(final Collection<Long> groupIds, final EppRealEduGroupCompleteLevel level, boolean checkOnlyLevel) throws RuntimeException {
        final Map<Class, Collection<EppRealEduGroup>> map = new HashMap<>();
        for (final EppRealEduGroup group: this.getList(EppRealEduGroup.class, "id", groupIds))
        {
            if (null != level) {
                if (EppRealEduGroupCompleteLevel.gt(group.getLevel(), level)) {
                    final String message = "Учебная группа «"+group.getTitle()+" "+group.getActivityPart().getTitleWithNumber()+", "+group.getType().getTitle()+"» заблокирована";
                    if (null != group.getLevel()) {
                        throw new ApplicationException(message + " ("+group.getLevel().getDisplayableTitle()+").");
                    }
                    throw new ApplicationException(message + ".");
                }
            }

            SafeMap.safeGet(map, ClassUtils.getUserClass(group), ArrayList.class).add(group);
        }

        if (checkOnlyLevel) {
            // не будем блокировать смену согласования УГС, если на них созданы ведомости, журналы или циклы
            return;
        }

        for (final Map.Entry<Class, Collection<EppRealEduGroup>> e: map.entrySet())
        {
            final Collection<IEppRealGroupOperationHandler> handlers = this.handlerMap.get(e.getKey());
            for (final IEppRealGroupOperationHandler handler: handlers) {
                handler.checkLocked(e.getValue());
            }
        }
    }


    @SuppressWarnings("unchecked")
    protected Map<EppRealEduGroup, Collection<EppRealEduGroupRow>> getJoinCandidates(final Class<? extends EppRealEduGroup> groupClass, final Collection<EppRealEduGroupRow> rows) {
        final Map<Long, EppRealEduGroup> groupMap = SafeMap.get(EppRealGroupRowDAO.this::get);

        final Map<EppRealEduGroup, Collection<EppRealEduGroupRow>> candidates = new HashMap<>();
        for (final EppRealEduGroupRow row: rows) {
            final EppRealEduGroup group = groupMap.get(row.getGroup().getId()); // force get object (not proxy)
            SafeMap.safeGet(candidates, group, ArrayList.class).add(row);
        }

        final Collection<IEppRealGroupOperationHandler> handlers = this.handlerMap.get(groupClass);
        for (final IEppRealGroupOperationHandler handler: handlers) {
            final Collection<Long> joinCandidate = handler.getJoinCandidate(rows);
            if (null != joinCandidate /* если ему не пофиг */ ) {
                final Set<Long> tmp = new HashSet<>(joinCandidate);
                for (final Iterator<EppRealEduGroup> it = candidates.keySet().iterator(); it.hasNext(); ) {
                    if (!tmp.contains(it.next().getId())) { it.remove(); }
                }
            }
        }
        return candidates;
    }

    protected EppRealEduGroup getJoinCandidate(final Class<? extends EppRealEduGroup> groupClass, final Collection<EppRealEduGroupRow> rows)
    {
        final Map<EppRealEduGroup, Collection<EppRealEduGroupRow>> candidates = this.getJoinCandidates(groupClass, rows);
        if (candidates.isEmpty()) {
            /* все хотят разного */
            throw new ApplicationException("Нельзя объединить УГС, т.к. на разные УГС ссылаются разные объекты (журналы преподавателей, ведомости и т.п.).");
        }

        if (candidates.size() > 1) {
            // если значений много - то придется сортировать
            final List<Map.Entry<EppRealEduGroup, Collection<EppRealEduGroupRow>>> entries = new ArrayList<>(candidates.entrySet());
            Collections.sort(entries, this.getJoinCandidateComparator());
            return entries.iterator().next().getKey();
        }

        // если одно - его и отдаем
        return candidates.keySet().iterator().next();
    }

    /**
     * @return компаратор, сравнивающий УГС (с составом студентов)
     * УГС, оказавшаяся в начале списка будет взята за основу для объединения УГС
     */
    protected Comparator<Map.Entry<EppRealEduGroup, Collection<EppRealEduGroupRow>>> getJoinCandidateComparator()
    {
        return (o1, o2) -> {

            final EppRealEduGroup g1 = o1.getKey();
            final EppRealEduGroup g2 = o2.getKey();

            {
                // сотритуем группы по убыванию степени готовности
                final int p1 = (null != g1.getLevel() ? g1.getLevel().getPriority() : Integer.MIN_VALUE);
                final int p2 = (null != g2.getLevel() ? g2.getLevel().getPriority() : Integer.MIN_VALUE);
                final int p = Integer.compare(p1, p2);

                if (0 != p) { return -p; }
            }

            {
                // далее - по убыванию числа студентов
                final int s1 = o1.getValue().size();
                final int s2 = o2.getValue().size();
                final int s = Integer.compare(s1, s2);

                if (0 != s) { return -s; }
            }


            // затем - по убыванию даты изменения
            return -g1.getModificationDate().compareTo(g2.getModificationDate());
        };
    }

    protected interface IJoinCandidateResolver {
        EppRealEduGroup resolve(final Class<? extends EppRealEduGroup> groupClass, final Collection<EppRealEduGroupRow> rows);
    }

    protected Long doJoin(final Collection<Long> groupIds, EppRealEduGroupCompleteLevel level, final IJoinCandidateResolver joinCandidateResolver)
    {
        if (this.checkLevel && (null == level)) { throw new NullPointerException(); }
        level = (null == level ? null : level.getTaked()); /* выставляем состояние ФОРМИРУЕТСЯ для текущего уровня */

        // здесь может быть создана группа либо взята из списка уже существующих групп
        // (одна из указанных в параметре)

        if ((null == groupIds) || groupIds.isEmpty()) { return null; }
        if (1 == groupIds.size()) { return groupIds.iterator().next(); }

        final Date now = new Date();
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, EppRealGroupRowDAO.LOCK_NAME_UPDATE_ROWS);

        {
            // Проверяем только уровень согласования. Проверки на возможность объединения должны быть в методе поиска кандидата.
            this.checkGroupLockedLevel(groupIds, level);
        }

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {

            final DQLSelectBuilder relBuilder = new DQLSelectBuilder();
            relBuilder.fromEntity(EppRealEduGroupRow.class, "rel").column(property("rel", "id"));
            relBuilder.where(in(property(EppRealEduGroupRow.group().id().fromAlias("rel")), groupIds));
            relBuilder.where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))));

            // проверяем, что в записях нет одинаковых студентов (включение в УГС Иванова одновременно по математике и физике - плохая идея)
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EppRealEduGroupRow.class, "rel");
                dql.where(in(property(EppRealEduGroupRow.id().fromAlias("rel")), relBuilder.buildQuery()));
                dql.group(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().id().fromAlias("rel")));
                dql.column(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().id().fromAlias("rel")));
                dql.having(gt(count(property("rel.id")), value(1)));
                final List<Long> duplicateStudentIdsList = dql.createStatement(session).list();

                if (duplicateStudentIdsList.size() > 0) {
                    if (duplicateStudentIdsList.size() > 1) {
                        final List<String> studentTitleList = new ArrayList<>();
                        for (final Long id: duplicateStudentIdsList) {
                            final Student student = this.get(Student.class, id);
                            studentTitleList.add(student.getFio());
                        }
                        Collections.sort(studentTitleList);
                        throw new ApplicationException("Объединение УГС невозможно: в итоговой УГС записи студентов "+StringUtils.join(studentTitleList, ", ")+" встречаются более одного раза.");
                    }
                    final Student student = this.get(Student.class, duplicateStudentIdsList.iterator().next());
                    throw new ApplicationException("Объединение УГС невозможно: в итоговой УГС запись студента "+student.getFio()+" встречается более одного раза.");
                }
            }

            // поехали плясать
            try {

                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EppRealEduGroupRow.class, "rel").column(property("rel"));
                dql.where(in(property(EppRealEduGroupRow.id().fromAlias("rel")), relBuilder.buildQuery()));

                final List<EppRealEduGroupRow> rows = dql.createStatement(session).list();
                if (rows.isEmpty()) { return null; }

                final IGroupParameters params = this.getGroupParameters(relBuilder);

                EppRealEduGroup group = joinCandidateResolver.resolve(params.relation().groupClass(), rows);
                if (null == group)
                {
                    // ------------------------------------------ //
                    // если вдруг кто-то попросил создать группу //

                    final String title = this.calculateTitle2(relBuilder);
                    final EppRegistryElementPart activityPart = this.calculateTopLevelRelationElement(EppRegistryElementPart.class, relBuilder, EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart());
                    group = params.relation().buildGroup(params.summary(), activityPart, params.type(), level, title, now);
                }
                else if (null == group.getLevel())
                {
                    // ---------------------------------------- //
                    // если новая группа без степени готовности //

                    // меняем ей название
                    final String title = this.calculateTitle2(relBuilder);
                    group.setProperty(EppRealEduGroup.P_TITLE, title);

                    // обновляем дисциплину
                    final EppRegistryElementPart activityPart = this.calculateTopLevelRelationElement(EppRegistryElementPart.class, relBuilder, EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart());
                    group.setProperty(EppRealEduGroup.L_ACTIVITY_PART, activityPart);
                }

                // выставляем уровень готовности
                this.setGroupLevelAfterChange(group, level);

                // выставляем дату изменения и сохраняем группу
                group.setProperty(EppRealEduGroup.P_MODIFICATION_DATE, now);
                session.saveOrUpdate(group);


                {
                    final DQLSelectBuilder tutorBuilder = new DQLSelectBuilder().predicate(DQLPredicateType.distinct);
                    tutorBuilder.fromEntity(EppPpsCollectionItem.class, "ppsItem").column(property(EppPpsCollectionItem.pps().id().fromAlias("ppsItem")));
                    tutorBuilder.where(in(property(EppPpsCollectionItem.list().id().fromAlias("ppsItem")), groupIds));

                    final DQLSelectBuilder ydql = new DQLSelectBuilder();
                    ydql.fromEntity(EppPpsCollectionItem.class, "rrr").column(property(EppPpsCollectionItem.pps().id().fromAlias("rrr")));
                    ydql.where(eq(property(EppPpsCollectionItem.list().fromAlias("rrr")), value(group)));
                    tutorBuilder.where(notIn(property(EppPpsCollectionItem.pps().id().fromAlias("ppsItem")), ydql.buildQuery()));

                    this.fillGroupPps(group, tutorBuilder.createStatement(session).<Long>list());
                }

                this.moveRowsToGroup(group, rows, now, level);
                return group.getId();

            } catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
        } finally {
            eventLock.release();
        }
    }

    @Override
    public Long doJoin(final Collection<Long> groupIds, final EppRealEduGroupCompleteLevel level) {
        return this.doJoin(groupIds, level, EppRealGroupRowDAO.this::getJoinCandidate);
    }

    @Override
    public Long doSplit(final Collection<Long> studentIds, EppRealEduGroupCompleteLevel level, EppRealEduGroupCompleteLevel targetLevel)
    {
        if (this.checkLevel && (null == level)) { throw new NullPointerException(); }
        level = (null == level ? null : level.getTaked()); /* выставляем состояние ФОРМИРУЕТСЯ для текущего уровня */

        if (null != targetLevel) {
            targetLevel = targetLevel.getTaked(); /* выставляем состояние ФОРМИРУЕТСЯ */
            if (EppRealEduGroupCompleteLevel.gt(targetLevel, level)) { throw new IllegalArgumentException(); }
        }

        // здесь всегда создается новая группа

        if ((null == studentIds) || studentIds.isEmpty()) { return null; }

        final Date now = new Date();
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, EppRealGroupRowDAO.LOCK_NAME_UPDATE_ROWS);

        {
            // check locked status
            final DQLSelectBuilder tmp = new DQLSelectBuilder();
            tmp.fromEntity(EppRealEduGroupRow.class, "rel").column(property(EppRealEduGroupRow.group().id().fromAlias("rel")));
            tmp.where(in(property("rel.id"), studentIds));
            tmp.predicate(DQLPredicateType.distinct);

            this.checkGroupLockedEdit(tmp.createStatement(session).<Long>list(), level);
        }

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {

            final DQLSelectBuilder relBuilder = new DQLSelectBuilder();
            relBuilder.fromEntity(EppRealEduGroupRow.class, "rel").column(property("rel", "id"));
            relBuilder.where(in(property(EppRealEduGroupRow.id().fromAlias("rel")), studentIds));

            final IGroupParameters params = this.getGroupParameters(relBuilder);
            final EppRegistryElementPart activityPart = this.calculateTopLevelRelationElement(EppRegistryElementPart.class, relBuilder, EppRealEduGroupRow.studentWpePart().studentWpe().registryElementPart());

            try {

                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(params.relation().relationClass(), "rel").column(property("rel"));
                dql.where(in(property(EppRealEduGroupRow.id().fromAlias("rel")), relBuilder.buildQuery()));
                dql.where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))));

                final List<EppRealEduGroupRow> rows = dql.createStatement(session).list();
                if (rows.isEmpty()) { return null; }

                final String title = this.calculateTitle2(relBuilder);
                final EppRealEduGroup group = params.relation().buildGroup(params.summary(), activityPart, params.type(), targetLevel, title, now);
                session.save(group);

                {
                    final DQLSelectBuilder tutorBuilder = new DQLSelectBuilder().predicate(DQLPredicateType.distinct);
                    tutorBuilder.fromEntity(EppPpsCollectionItem.class, "ppsItem").column(property(EppPpsCollectionItem.pps().id().fromAlias("ppsItem")));

                    final DQLSelectBuilder xdql = new DQLSelectBuilder();
                    xdql.fromEntity(EppRealEduGroupRow.class, "rel").column(property(EppRealEduGroupRow.group().fromAlias("rel")));
                    xdql.where(in(property(EppRealEduGroupRow.id().fromAlias("rel")), relBuilder.buildQuery()));
                    tutorBuilder.where(in(property(EppPpsCollectionItem.list().id().fromAlias("ppsItem")), xdql.buildQuery()));

                    this.fillGroupPps(group, tutorBuilder.createStatement(session).<Long>list());
                }

                this.moveRowsToGroup(group, rows, now, level);
                return group.getId();

            } catch (final Throwable t) {
                throw CoreExceptionUtils.getRuntimeException(t);
            }

        } finally {
            eventLock.release();
        }
    }

    @Override
    public void doMove(final Long groupId, final Collection<Long> studentIds, EppRealEduGroupCompleteLevel level)
    {
        if (this.checkLevel && (null == level)) { throw new NullPointerException(); }
        level = (null == level ? null : level.getTaked()); /* выставляем состояние ФОРМИРУЕТСЯ для текущего уровня */

        // указанные студенты выделяются в отдельную группу - временную (без согласования)
        // затем, две группы объединяются - поскольку новая группа без согласования, она должна будет поглатиться группой с согласованием (на всякий случай используем кастомный ресолвер)
        // пустые группы должны быть уничтожены

        this.lock4update();

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            // вытаскиваем выбранные строки из группы
            final EppRealEduGroup tmpGroup = this.get(this.doSplit(studentIds, level, null /* создаем группу без согласования */));

            // объединяем две группы вместе
            final Collection<Long> groupIds = new ArrayList<>(2);
            groupIds.add(groupId);
            groupIds.add(tmpGroup.getId());
            final Long resultId = this.doJoin(groupIds, level, (groupClass, rows) -> {
                final EppRealEduGroup group = EppRealGroupRowDAO.this.get(groupClass, groupId);
                final Map<EppRealEduGroup, Collection<EppRealEduGroupRow>> candidates = EppRealGroupRowDAO.this.getJoinCandidates(groupClass, rows);
                if (null != candidates.get(group)) { return group; }
                throw new IllegalStateException("No-Join-Candidate");
            });

            if (!groupId.equals(resultId)) {
                // должна быть та же самая УГС - временная УГС должна умереть
                throw new IllegalStateException();
            }

        } finally {
            eventLock.release();
        }
    }


    protected void fillGroupPps(final EppRealEduGroup group, final List<Long> ppsIds)
    {
        if ((null == ppsIds) || ppsIds.isEmpty()) { return; }
        final Session session = this.getSession();
        final Map<Long, PpsEntry> ppsMap = this.getLoadCacheMap(PpsEntry.class);
        for (final Long id: ppsIds) { session.save(new EppPpsCollectionItem(group, ppsMap.get(id))); }
    }

    @SuppressWarnings("unchecked")
    protected void moveRowsToGroup( final EppRealEduGroup group, final Collection<EppRealEduGroupRow> rows, final Date now, final EppRealEduGroupCompleteLevel level)
    {
        final Session session = this.getSession();
        final Set<Long> groupIds = new HashSet<>();
        final Map<EppRealEduGroupRow, EppRealEduGroupRow> rowMoveMap = new HashMap<>();

        // в группе могут уже быть студенты
        final Map<Long, EppRealEduGroupRow> studentId2currentGroupRowMap = this.getGroupStudentRowMap(group, session);

        {
            for (final EppRealEduGroupRow row: rows)
            {
                if (null == row.getRemovalDate())
                {
                    final EppRealEduGroup g = row.getGroup();
                    if (!group.equals(g))
                    {
                        // если группы разные - то перемещаем
                        groupIds.add(g.getId());
                        FastBeanUtils.setValue(row, EppRealEduGroupRow.P_REMOVAL_DATE, now);
                        session.saveOrUpdate(row);

                        EppRealEduGroupRow targetRow = studentId2currentGroupRowMap.get(row.getStudentWpePart().getId());
                        if (null == targetRow) {
                            // если студента в группе нет - то создаем
                            targetRow = row.clone(group);
                            session.save(targetRow);
                        } else {
                            // если он там уже был - то воскрешаем
                            FastBeanUtils.setValue(targetRow, EppRealEduGroupRow.P_REMOVAL_DATE, null);
                            session.saveOrUpdate(targetRow);
                        }
                        rowMoveMap.put(row, targetRow);
                    }
                    else
                    {
                        rowMoveMap.put(row, row);
                    }
                }
            }

            group.setProperty(EppRealEduGroup.P_MODIFICATION_DATE, now);
            session.saveOrUpdate(group);
            session.flush();
        }

        // запускаем обработчики переноса строк
        {
            final Collection<IEppRealGroupOperationHandler> handlers = this.handlerMap.get((Class) ClassUtils.getUserClass(group));
            for (final IEppRealGroupOperationHandler handler: handlers) {
                handler.move(group, rowMoveMap);
            }
            session.flush();
        }

        this.setGroupLevelAfterChange(groupIds, level);
        this.cleanUpGroupsAfterChange(groupIds);
    }

    protected Map<Long, EppRealEduGroupRow> getGroupStudentRowMap(final EppRealEduGroup group, final Session session)
    {
        final List<EppRealEduGroupRow> currentGroupRows = new DQLSelectBuilder()
        .fromEntity(EppRealEduGroupRow.class, "rel").column(property("rel"))
        .where(eq(property(EppRealEduGroupRow.group().fromAlias("rel")), value(group)))
        .createStatement(session)
        .list();

        final Map<Long, EppRealEduGroupRow> studentId2currentGroupRowMap = new HashMap<>(currentGroupRows.size());
        for (final EppRealEduGroupRow row: currentGroupRows) {
            studentId2currentGroupRowMap.put(row.getStudentWpePart().getId(), row);
        }
        return studentId2currentGroupRowMap;
    }

    protected void setGroupLevelAfterChange(final Set<Long> groupIds, final EppRealEduGroupCompleteLevel level)
    {
        for (final Long groupId: groupIds) {
            this.setGroupLevelAfterChange((EppRealEduGroup)this.get(groupId), level);
        }
        this.getSession().flush();
    }

    protected void setGroupLevelAfterChange(final EppRealEduGroup g, final EppRealEduGroupCompleteLevel level)
    {
        if (null == g.getLevel()) {
            // если уровня не было - то оставляем пустой, чтобы демон мог работать дальше
            return;
        }

        // выставляем соответсвующий уровень согласования
        g.setProperty(EppRealEduGroup.L_LEVEL, level);
    }

    /**
     * DEV-1573
     * фиксирует состояние группы после действий с ней (т.е. удаляет из группы неактуальные строки, затем - пустые группы)
     * удяляться будут только те объекты, на которые нет ссылок (если по каким-то причинам в группе будет неактуальный студент, на которого есть ссылка - он останется неактуальным)
     */
    protected void cleanUpGroupsAfterChange(final Collection<Long> allGroupIds)
    {
        if (allGroupIds.isEmpty()) { return; }

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {

            // разбиваем группы по классам (как правило класс здесь один, но метод может использоваться и с несколькими кассами групп одновременно - этому ничего не мешает)
            final Map<EppRealEduGroupRow.IEppRealEduGroupRowDescription, List<Long>> dsc2idsList = new HashMap<>(4);
            for (final Long id: allGroupIds) {
                final IEntityMeta meta = EntityRuntime.getMeta(id);
                final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation = EppRealEduGroupRow.group2descriptionMap.get(meta.getEntityClass());
                SafeMap.safeGet(dsc2idsList, relation, ArrayList.class).add(id);
            }

            // далее - по каждой группе делаем запросы
            for (final Map.Entry<IEppRealEduGroupRowDescription, List<Long>> e: dsc2idsList.entrySet())
            {
                final EppRealEduGroupRow.IEppRealEduGroupRowDescription relation = e.getKey();
                final Collection<Long> groupIds = e.getValue();

                // INFO: важно понимать, что удалится не все, а только то, что может быть удалено (например, пустые группы, на которые есть ссылки из ЖУРНАЛОВ не удалятся, т.к. это приведет к ошибкам)
                // INFO: зато они будут удалены через какое-то время (когда отработает демон УГС), который удалит их через doCleanUpEmptyGroups, когда демон журналов освободит связи (это не критично)
                Debug.begin("do-cleanup-empty-groups-after-change-"+relation.relationClass().getSimpleName());
                try {
                    this.doCleanUpEmptyGroups(relation, groupIds);
                } finally {
                    Debug.end();
                }
            }

        } finally {
            eventLock.release();
        }
    }

    @SuppressWarnings("unchecked")
    protected <T extends IEntity> T calculateTopLevelRelationElement(final Class<T> klass, final DQLSelectBuilder relBuilder, final EntityPath<T> path)
    {
        final Session session = this.getSession();
        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppRealEduGroupRow.class, "rel");
        dql.group(property(path.id().fromAlias("rel")));
        dql.column(property(path.id().fromAlias("rel")));
        dql.column(DQLFunctions.count(property("rel.id")));
        dql.order(DQLFunctions.count(property("rel.id")), OrderDirection.desc);
        dql.where(in(property(EppRealEduGroupRow.id().fromAlias("rel")), relBuilder.buildQuery()));
        dql.where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))));
        final Object[] row = dql.createStatement(session).setMaxResults(1).uniqueResult();
        if (null == row) { throw new IllegalStateException("no-active-rows"); }
        return (T)session.load(klass, (Long)row[0]);
    }


    @Override
    public String calculateTitle(final EppRealEduGroup group)
    {
        final DQLSelectBuilder relBuilder = new DQLSelectBuilder();
        relBuilder.fromEntity(EppRealEduGroupRow.class, "rel").column(property("rel", "id"));
        relBuilder.where(eq(property(EppRealEduGroupRow.group().fromAlias("rel")), value(group)));
        relBuilder.where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))));
        return this.calculateTitle2(relBuilder);
    }

    /**
     * Формирует название УГС на основе набора записей
     * @return названия групп через запятую, ФИО студентов без групп через запятую
     */
    protected String calculateTitle2(final DQLSelectBuilder relBuilder)
    {
        final StringBuilder sb = new StringBuilder();

        {
            // по названию групп
            final DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(EppRealEduGroupRow.class, "rel");
            dql.group(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("rel")));
            dql.column(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("rel")));
            dql.column(DQLFunctions.count(property("rel", "id")));
            dql.order(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("rel")));
            dql.order(DQLFunctions.count(property("rel", "id")), OrderDirection.desc);
            dql.where(in(property(EppRealEduGroupRow.id().fromAlias("rel")), relBuilder.buildQuery()));
            dql.where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))));
            final String title = StringUtils.trimToNull(this.calculateTitle(dql));
            if (null != title) { sb.append(title); }
        }

        {
            // по ФИО студентов вне группе
            final DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(EppRealEduGroupRow.class, "rel");
            dql.column(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().person().identityCard().id().fromAlias("rel")));
            dql.order(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().person().identityCard().lastName().fromAlias("rel")), OrderDirection.asc);
            dql.order(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().person().identityCard().firstName().fromAlias("rel")), OrderDirection.asc);
            dql.order(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().person().identityCard().middleName().fromAlias("rel")), OrderDirection.asc);
            dql.where(in(property(EppRealEduGroupRow.id().fromAlias("rel")), relBuilder.buildQuery()));
            dql.where(isNull(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("rel"))));
            dql.where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("rel"))));

            final List<Long> rows = dql.createStatement(this.getSession()).setMaxResults(2).list();
            if (rows.size() > 0) {
                if (sb.length() > 0) { sb.append(", "); }
                sb.append(this.getNotNull(IdentityCard.class, rows.iterator().next()).getFio());

                if (rows.size() > 1) { sb.append(", ..."); }
            }
        }

        return sb.toString();
    }

    /**
     * Формирует название на основе запроса
     * @param titleBuilder DQLSelectBuilder( ... -> Object[])
     * @return значения первой колонки запроса через запятую (если длина более 60 символов, в конце ставится ...)
     */
    protected String calculateTitle(final DQLSelectBuilder titleBuilder)
    {
        final StringBuilder title = new StringBuilder();
        for (final Object[] row: UniBaseDao.scrollRows(titleBuilder.createStatement(this.getSession()))) {
            final String name = StringUtils.trimToNull((String)row[0]);
            if (null != name) {
                if ((title.length() + name.length()) > 60) {
                    return title.append(", ...").toString();
                }

                if (title.length() > 0) { title.append(", "); }
                title.append(name);
            }
        }
        return title.toString();
    }
}


