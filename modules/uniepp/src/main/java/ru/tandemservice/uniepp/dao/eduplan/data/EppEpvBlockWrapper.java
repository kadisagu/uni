package ru.tandemservice.uniepp.dao.eduplan.data;

import com.google.common.collect.Multiset;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import java.util.*;

/**
 * @author vdanilov
 */
public class EppEpvBlockWrapper extends EntityBase implements IEppEpvBlockWrapper
{
    protected static final Logger logger = Logger.getLogger(EppEpvBlockWrapper.class);

    private static final int[] EMPTY = new int[48];

    private static Map<String, int[]> getTermSizeMap(final Map<Integer, Multiset<String>> map)
    {
        final Map<String, int[]> result = new HashMap<>(4, 0.5f);
        if (null == map || map.isEmpty()) { return result; }

        final int size = 1 + Collections.max(map.keySet());
        final SafeMap.Callback<String, int[]> callback = key -> new int[size];

        for (final Map.Entry<Integer, Multiset<String>> termEntry : map.entrySet()) {
            final int term = termEntry.getKey();
            for (final Multiset.Entry<String> loadEntry: termEntry.getValue().entrySet()) {
                SafeMap.safeGet(result, loadEntry.getElement(), callback)[term] = loadEntry.getCount();
            }
        }

        return result;
    }

    public static int getTermSize(final Map<String, int[]> termSizeMap, final String loadFullCode, final int term) {
        if (term <= 0) { return 0; }

        int[] termSizeArray = termSizeMap.get(loadFullCode);
        if (null == termSizeArray) {
            final String eLoadFullCode = EppLoadTypeUtils.getELoadTypeFullCode(loadFullCode);
            if ((null != eLoadFullCode) && !eLoadFullCode.equals(loadFullCode)) {
                termSizeArray = termSizeMap.get(eLoadFullCode);
            }

            if (null == termSizeArray) {
                termSizeArray = EppEpvBlockWrapper.EMPTY;
            }
            termSizeMap.put(loadFullCode, termSizeArray);
        }

        if (term >= termSizeArray.length) { return 0; }
        return termSizeArray[term];
    }

    private final EppEduPlanVersionRootBlock rootBlock;
    @Override public EppEduPlanVersionBlock getRootBlock() { return this.rootBlock; }
    @Override public EppEduPlanVersion getVersion() { return this.getRootBlock().getEduPlanVersion(); }

    private final EppEduPlanVersionBlock block;
    @Override public EppEduPlanVersionBlock getBlock() { return this.block; }

    private final Map<Long, IEppEpvRowWrapper> rowMap = new LinkedHashMap<>();
    @Override public Map<Long, IEppEpvRowWrapper> getRowMap() { return this.rowMap ; }

    private final Map<String, int[]> termSizeMap;
    //public Map<String, int[]> getTermSizeMap() { return this.termSizeMap; }

    @Override public int getTermSize(final String loadFullCode, final int term) {
        return EppEpvBlockWrapper.getTermSize(this.termSizeMap, loadFullCode, term);
    }

    private static EppEduPlanVersionBlock checkBlock(final EppEduPlanVersionRootBlock rootBlock, final EppEduPlanVersionBlock block) {
        if (null == block) { return rootBlock; }
        if (!block.getEduPlanVersion().equals(rootBlock.getEduPlanVersion())) { throw new IllegalStateException(); }
        return block;
    }

    public EppEpvBlockWrapper(final EppEduPlanVersionRootBlock rootBlock, final EppEduPlanVersionBlock block, final Map<Integer, Multiset<String>> epvTermSizeMap) {
        this.rootBlock = rootBlock;
        this.block = checkBlock(rootBlock, block);
        this.termSizeMap = EppEpvBlockWrapper.getTermSizeMap(epvTermSizeMap);
    }

    @Override public Long getId() { return this.block.getEduPlanVersion().getId(); }

    @Override public void setId(final Long id) {
        throw new UnsupportedOperationException();
    }
    @Override public void setProperty(final String propertyName, final Object propertyValue) {
        throw new UnsupportedOperationException();
    }
    @Override public Object getProperty(final Object propertyPath) {
        try {
            return super.getProperty(propertyPath);
        } catch(final Exception t) {
            return null;
        }
    }

    public static <T extends IEppEpvRow> int getRecordHierarchyIndex(final Collection<T> sortedRowList, T row) {
        int seq = 0;
        for (T x: sortedRowList) {
            if (row.equals(x)) { break; }
            if (ObjectUtils.equals(row.getHierarhyParent(), x.getHierarhyParent())) { seq ++; }
        }
        return seq;
    }

    /**
     * порядковый номер записи (нумерация ведется в рамках родительской записи)
     * @param eppEpvRowWrapper - запись
     * @return порядковый номер
     */
    protected int getRecordHierarchyIndex(final IEppEpvRowWrapper eppEpvRowWrapper) {
        return getRecordHierarchyIndex(this.getRowMap().values(), eppEpvRowWrapper);
    }

    @Override
    public IEppEpvRowWrapper findSuitableRow(final EppWorkPlanRegistryElementRow wpRegRow) {
        final EppEduPlanVersionBlock wpBlock = wpRegRow.getWorkPlan().getBlock();
        return (IEppEpvRowWrapper) CollectionUtils.find(this.getRowMap().values(), object -> {
            final IEppEpvRowWrapper epvRow = (IEppEpvRowWrapper) object;
            if (epvRow.getOwner().isRootBlock() || epvRow.getOwner().equals(wpBlock)) {
                if (epvRow.getRow() instanceof EppEpvRegistryRow) {
                    final EppEpvRegistryRow epvRegRow = (EppEpvRegistryRow) epvRow.getRow();
                    final EppRegistryElement registryElement = epvRegRow.getRegistryElement();
                    if (null != registryElement) {
                        return registryElement.equals(wpRegRow.getRegistryElement());
                    }
                }
            }
            return false;
        });
    }

    /** закрывает враппер, убивает все данные (в помощь GC) */
    public void close() {
        for (final IEppEpvRowWrapper wrapper : this.rowMap.values()) {
            ((EppEpvRowWrapper)wrapper).close();
        }
        this.rowMap.clear();
        this.termSizeMap.clear();
    }

    @Override
    public int compareTo(IEppEpvBlockWrapper o)
    {
        return getBlock().compareTo(o.getBlock());
    }
}
