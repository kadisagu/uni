/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.workplan.WorkPlanChangeVersion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

/**
 * @author nkokorina
 * Created on: 12.08.2010
 */
@Input( {
    @Bind(key = "ids", binding = "studentIds"),
    @Bind(key = "workPlanId", binding = "workPlanId")
})
public class Model
{
    private Long workPlanId;
    public Long getWorkPlanId() { return this.workPlanId; }
    public void setWorkPlanId(final Long workPlanId) { this.workPlanId = workPlanId; }

    private List<Long> studentIds;
    public List<Long> getStudentIds() { return this.studentIds; }
    public void setStudentIds(final List<Long> studentIds) { this.studentIds = studentIds; }

    private ISelectModel versionSelectModel;
    public ISelectModel getVersionSelectModel() { return this.versionSelectModel; }
    public void setVersionSelectModel(final ISelectModel versionSelectModel) { this.versionSelectModel = versionSelectModel; }

    private EppWorkPlanBase currentWorkPlan;
    public EppWorkPlanBase getCurrentWorkPlan() { return this.currentWorkPlan; }
    public void setCurrentWorkPlan(final EppWorkPlanBase currentWorkPlan) { this.currentWorkPlan = currentWorkPlan; }

    private StaticListDataSource<IEppWorkPlanRowWrapper> rowDataSource = new StaticListDataSource<IEppWorkPlanRowWrapper>();
    public StaticListDataSource<IEppWorkPlanRowWrapper> getRowDataSource() { return this.rowDataSource; }
    public void setRowDataSource(final StaticListDataSource<IEppWorkPlanRowWrapper> rowDataSource) { this.rowDataSource = rowDataSource; }

    private Map<Long, IEppWorkPlanWrapper> workPlanWrapperMap = new HashMap<Long, IEppWorkPlanWrapper>();
    public Map<Long, IEppWorkPlanWrapper> getWorkPlanWrapperMap() { return this.workPlanWrapperMap; }
    public void setWorkPlanWrapperMap(final Map<Long, IEppWorkPlanWrapper> workPlanWrapperMap) {  this.workPlanWrapperMap = workPlanWrapperMap; }
}
