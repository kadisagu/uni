package ru.tandemservice.uniepp.entity.catalog;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.tool.tree.IHierarchyItem;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniepp.base.bo.EppRegistryStructure.ui.AddEdit.EppRegistryStructureAddEdit;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppRegistryStructureGen;

import java.util.Collection;
import java.util.List;

/**
 * Структура реестра
 */
public class EppRegistryStructure extends EppRegistryStructureGen implements IHierarchyItem, IDynamicCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
    {
        return new EntityComboDataSourceHandler(name, EppRegistryStructure.class)
                .titleProperty(EppRegistryStructure.title().s())
                .filter(EppRegistryStructure.title())
                .order(EppRegistryStructure.title());
    }

    private static final List<String> HIDDEN_PROPERTIES = ImmutableList.of(
        L_PARENT
    );

    public EppRegistryStructure()
    {
        setThemeRequired(false);
    }

    @Override public EppRegistryStructure getHierarhyParent() {
        return this.getParent();
    }

    public EppRegistryStructure getRoot() {
        final EppRegistryStructure p = this.getParent();
        if (null == p) { return this; }
        return p.getRoot();
    }

    private Boolean _discipline_element_cache = null;
    public boolean isDisciplineElement() {
        if (null != this._discipline_element_cache) {
            return this._discipline_element_cache;
        }

        if (EppRegistryStructureCodes.REGISTRY_DISCIPLINE.equals(this.getCode())) {
            return this._discipline_element_cache = Boolean.TRUE;
        }

        final EppRegistryStructure p = this.getParent();
        return this._discipline_element_cache = (null != p) && p.isDisciplineElement();
    }

    private Boolean _practice_element_cache = null;
    public boolean isPracticeElement() {
        if (null != this._practice_element_cache) {
            return this._practice_element_cache;
        }

        if (EppRegistryStructureCodes.REGISTRY_PRACTICE.equals(this.getCode())) {
            return this._practice_element_cache = Boolean.TRUE;
        }

        final EppRegistryStructure p = this.getParent();
        return this._practice_element_cache = (null != p) && p.isPracticeElement();
    }

    private Boolean _attestation_element_cache = null;
    public boolean isAttestationElement() {
        if (null != this._attestation_element_cache) {
            return this._attestation_element_cache;
        }

        if (EppRegistryStructureCodes.REGISTRY_ATTESTATION.equals(this.getCode())) {
            return this._attestation_element_cache = Boolean.TRUE;
        }

        final EppRegistryStructure p = this.getParent();
        return this._attestation_element_cache = (null != p) && p.isAttestationElement();
    }

    public static IDynamicCatalogDesc getUiDesc()
    {
        return new BaseDynamicCatalogDesc()
        {
            @Override
            public Collection<String> getHiddenFields()
            {
                return HIDDEN_PROPERTIES;
            }

            @Override
            public String getAddEditComponentName()
            {
                return EppRegistryStructureAddEdit.class.getSimpleName();
            }
        };
    }
}