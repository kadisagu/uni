package ru.tandemservice.uniepp.base.bo.EppEduGroup.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.transformer.IOutputTransformer;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow.IEppRealEduGroupRowDescription;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Список УГС для указанных студентоы
 * @author vdanilov
 */
public class EppEduGroupList4StudentIdsDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String PARAM_STUDENT_IDS = "studentIds";
    public static final String PARAM_YEAR_PART_LIST = "yearPartList";
    public static final String PARAM_GROUP_TYPE_LIST = "groupTypeList";
    public static final String PARAM_TUTOR_ORGUNIT_LIST = "tutorOrgUnitList";
    public static final String PARAM_REGISTRY_ELEMENT_LIST = "registryElementList";

    public static final String COLUMN_GROUPS = "groups";
    public static final String COLUMN_COURSES = "courses";

    public EppEduGroupList4StudentIdsDSHandler(final String ownerId) {
        super(ownerId, EppRealEduGroup.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected DSOutput execute(final DSInput input, final ExecutionContext context)
    {
        final Collection<Long> studentIds = context.get(PARAM_STUDENT_IDS);
        final Collection<EppYearPart> yearPartList = context.get(PARAM_YEAR_PART_LIST);
        final Collection<EppGroupType> groupTypeList = context.get(PARAM_GROUP_TYPE_LIST);
        final Collection<OrgUnit> tutorOrgUnitList = context.get(PARAM_TUTOR_ORGUNIT_LIST);
        final Collection<EppRegistryElement> registryElementList = context.get(PARAM_REGISTRY_ELEMENT_LIST);


        final DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(EppRealEduGroup.class, "g");
        final DQLSelectBuilder grpIdsDql = getEduGroupBuilder(registry, studentIds).column(property("g.id"));

        // !здесь не должно быть фильтров по строкам!

        if ((null != yearPartList) && (yearPartList.size() > 0)) {
            grpIdsDql.where(in(property(EppRealEduGroup.summary().yearPart().fromAlias("g")), yearPartList));
        }
        if ((null != groupTypeList) && (groupTypeList.size() > 0)) {
            grpIdsDql.where(in(property(EppRealEduGroup.type().fromAlias("g")), groupTypeList));
        }
        if ((null != tutorOrgUnitList) && (tutorOrgUnitList.size() > 0)) {
            grpIdsDql.where(in(property(EppRealEduGroup.activityPart().registryElement().owner().fromAlias("g")), tutorOrgUnitList));
        }
        if ((null != tutorOrgUnitList) && (tutorOrgUnitList.size() > 0)) {
            grpIdsDql.where(in(property(EppRealEduGroup.activityPart().registryElement().owner().fromAlias("g")), tutorOrgUnitList));
        }
        if ((null != registryElementList) && (registryElementList.size() > 0)) {
            grpIdsDql.where(in(property(EppRealEduGroup.activityPart().registryElement().fromAlias("g")), registryElementList));
        }

        grpIdsDql.order(property(EppRealEduGroup.summary().yearPart().year().educationYear().intValue().fromAlias("g")));
        grpIdsDql.order(property(EppRealEduGroup.summary().yearPart().part().yearDistribution().amount().fromAlias("g")));
        grpIdsDql.order(property(EppRealEduGroup.summary().yearPart().part().number().fromAlias("g")));
        grpIdsDql.order(property(EppRealEduGroup.activityPart().registryElement().owner().title().fromAlias("g")));
        grpIdsDql.order(property(EppRealEduGroup.activityPart().registryElement().owner().id().fromAlias("g")));
        grpIdsDql.order(property(EppRealEduGroup.activityPart().registryElement().title().fromAlias("g")));
        grpIdsDql.order(property(EppRealEduGroup.activityPart().registryElement().id().fromAlias("g")));
        grpIdsDql.order(property(EppRealEduGroup.activityPart().number().fromAlias("g")));
        grpIdsDql.order(property(EppRealEduGroup.type().priority().fromAlias("g")));

        //registry.applyOrder(grpIdsDql, input.getEntityOrder());

        // грузим id, затем превращаем это в объекты
        final DSOutput output = DQLSelectOutputBuilder.get(input, grpIdsDql, context.getSession()).pageable(this.isPageable()).build();
        output.setRecordList((List)CommonDAO.sort(DataAccessServices.dao().getListByIds(output.<Long>getRecordList()), output.<Long>getRecordList()));

        // вычисляем группы и курсы в УГС для указанных студентов
        final Map<Long, Set<String>> coursesMap = SafeMap.get(TreeSet.class);
        final Map<Long, Set<String>> groupsMap = SafeMap.get(TreeSet.class);
        final Collection<IEppRealEduGroupRowDescription> relations = IEppRealGroupRowDAO.instance.get().getRelations();
        for (final IEppRealEduGroupRowDescription dsc: relations)
        {
            final List<Object[]> rows = new DQLSelectBuilder()
            .fromEntity(dsc.relationClass(), "e")
            .column(property(EppRealEduGroupRow.group().id().fromAlias("e")))
            .column(property(EppRealEduGroupRow.studentWpePart().studentWpe().course().title().fromAlias("e")))
            .column(property(EppRealEduGroupRow.studentGroupTitle().fromAlias("e")))
            .where(in(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().id().fromAlias("e")), studentIds))
            .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("e"))))
            .createStatement(context.getSession()).list();

            for (final Object[] row: rows) {
                final String courseStr = StringUtils.trimToNull((String)row[1]);
                if (null != courseStr) { coursesMap.get(row[0]).add(courseStr); }
                final String groupStr = StringUtils.trimToNull((String)row[2]);
                if (null != groupStr) { groupsMap.get(row[0]).add(groupStr); }
            }
        }

        // оборачиваем данные
        final List<DataWrapper> wrappers = DataWrapper.wrap(output);
        for (final DataWrapper wrapper: wrappers) {
            wrapper.setProperty(COLUMN_COURSES, StringUtils.join(coursesMap.get(wrapper.getId()), ", "));
            wrapper.setProperty(COLUMN_GROUPS, StringUtils.join(groupsMap.get(wrapper.getId()), ", "));
        }

        // патчим список точками расширения
        for(final IOutputTransformer<DSInput, DSOutput> rt : this.getOutputTransformersInverse()) {
            rt.transformOutput(input, output, context);
        }
        return output;
    }


    public static DQLSelectBuilder getEduGroupBuilder(final DQLOrderDescriptionRegistry registry, final Collection<Long> studentIds)
    {
        final DQLSelectBuilder grpIdsDql = registry.buildDQLSelectBuilder();

        IDQLExpression e = null;
        final Collection<IEppRealEduGroupRowDescription> relations = IEppRealGroupRowDAO.instance.get().getRelations();
        for (final IEppRealEduGroupRowDescription dsc: relations)
        {
            final DQLSelectBuilder relDql = new DQLSelectBuilder()
            .fromEntity(dsc.relationClass(), "e").column(property("e.id"))
            .where(in(property(EppRealEduGroupRow.studentWpePart().studentWpe().student().id().fromAlias("e")), studentIds))
            .where(isNull(property(EppRealEduGroupRow.removalDate().fromAlias("e"))))
            .where(eq(property(EppRealEduGroupRow.group().id().fromAlias("e")), property("g.id")));

            e = or(e, exists(relDql.buildQuery()));
        }
        grpIdsDql.where(e);

        return grpIdsDql;
    }

}
