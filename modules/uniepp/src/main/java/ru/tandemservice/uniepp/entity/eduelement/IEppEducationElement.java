package ru.tandemservice.uniepp.entity.eduelement;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

/**
 * @author vdanilov
 */
public interface IEppEducationElement extends IEntity
{

    @EntityDSLSupport
    String getEducationElementTitle();

    @EntityDSLSupport
    String getEducationElementSimpleTitle();
}
