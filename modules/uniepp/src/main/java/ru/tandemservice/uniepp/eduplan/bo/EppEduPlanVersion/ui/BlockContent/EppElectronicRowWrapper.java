/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockContent;

import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;

/**
 * @author Nikolay Fedorovskih
 * @since 14.01.2015
 */
public class EppElectronicRowWrapper extends EppFakeRowWrapper
{
    public EppElectronicRowWrapper(IEppEpvRowWrapper parentRow)
    {
        super(parentRow);
    }

    @Override
    public String wrapFullCode(String loadFullCode)
    {
        if (EppLoadType.FULL_CODE_CONTROL.equals(loadFullCode))
            return EppLoadType.FULL_CODE_CONTROL_E;

        return EppALoadType.eFullCode(loadFullCode);
    }

    @Override
    public String getDisplayableTitle()
    {
        return "в электронной форме";
    }
}