package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppModuleStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебный модуль
 *
 * Учебный модуль (тема) - часть рабочей программы дисциплины (может использоваться в нескольких дисциплинах)
 * Показывает как модуль (тема) будет читаться: какие виды аудиторной нагрузки предусмотрены, какие формы текущего контроля необходимо провести
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegistryModuleGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppRegistryModule";
    public static final String ENTITY_NAME = "eppRegistryModule";
    public static final int VERSION_HASH = 143448574;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String P_TITLE = "title";
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_COMMENT = "comment";
    public static final String P_SHARED = "shared";
    public static final String L_OWNER = "owner";
    public static final String L_PARENT = "parent";
    public static final String L_STATE = "state";

    private String _number;     // Номер
    private String _title;     // Название
    private String _fullTitle;     // Полное название
    private String _shortTitle;     // Сокращенное название
    private String _comment;     // Комментарий
    private boolean _shared;     // Разделяемый
    private OrgUnit _owner;     // Читающее подразделение
    private EppModuleStructure _parent;     // Группа
    private EppState _state;     // Состояние

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Полное название.
     */
    @Length(max=255)
    public String getFullTitle()
    {
        return _fullTitle;
    }

    /**
     * @param fullTitle Полное название.
     */
    public void setFullTitle(String fullTitle)
    {
        dirty(_fullTitle, fullTitle);
        _fullTitle = fullTitle;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        initLazyForGet("comment");
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        initLazyForSet("comment");
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * Означает, что данный модуль можно использовать в разных дисциплинах
     *
     * @return Разделяемый. Свойство не может быть null.
     */
    @NotNull
    public boolean isShared()
    {
        return _shared;
    }

    /**
     * @param shared Разделяемый. Свойство не может быть null.
     */
    public void setShared(boolean shared)
    {
        dirty(_shared, shared);
        _shared = shared;
    }

    /**
     * @return Читающее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Читающее подразделение. Свойство не может быть null.
     */
    public void setOwner(OrgUnit owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public EppModuleStructure getParent()
    {
        return _parent;
    }

    /**
     * @param parent Группа. Свойство не может быть null.
     */
    public void setParent(EppModuleStructure parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public EppState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(EppState state)
    {
        dirty(_state, state);
        _state = state;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRegistryModuleGen)
        {
            setNumber(((EppRegistryModule)another).getNumber());
            setTitle(((EppRegistryModule)another).getTitle());
            setFullTitle(((EppRegistryModule)another).getFullTitle());
            setShortTitle(((EppRegistryModule)another).getShortTitle());
            setComment(((EppRegistryModule)another).getComment());
            setShared(((EppRegistryModule)another).isShared());
            setOwner(((EppRegistryModule)another).getOwner());
            setParent(((EppRegistryModule)another).getParent());
            setState(((EppRegistryModule)another).getState());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegistryModuleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegistryModule.class;
        }

        public T newInstance()
        {
            return (T) new EppRegistryModule();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "title":
                    return obj.getTitle();
                case "fullTitle":
                    return obj.getFullTitle();
                case "shortTitle":
                    return obj.getShortTitle();
                case "comment":
                    return obj.getComment();
                case "shared":
                    return obj.isShared();
                case "owner":
                    return obj.getOwner();
                case "parent":
                    return obj.getParent();
                case "state":
                    return obj.getState();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "fullTitle":
                    obj.setFullTitle((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "shared":
                    obj.setShared((Boolean) value);
                    return;
                case "owner":
                    obj.setOwner((OrgUnit) value);
                    return;
                case "parent":
                    obj.setParent((EppModuleStructure) value);
                    return;
                case "state":
                    obj.setState((EppState) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "title":
                        return true;
                case "fullTitle":
                        return true;
                case "shortTitle":
                        return true;
                case "comment":
                        return true;
                case "shared":
                        return true;
                case "owner":
                        return true;
                case "parent":
                        return true;
                case "state":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "title":
                    return true;
                case "fullTitle":
                    return true;
                case "shortTitle":
                    return true;
                case "comment":
                    return true;
                case "shared":
                    return true;
                case "owner":
                    return true;
                case "parent":
                    return true;
                case "state":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return String.class;
                case "title":
                    return String.class;
                case "fullTitle":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "comment":
                    return String.class;
                case "shared":
                    return Boolean.class;
                case "owner":
                    return OrgUnit.class;
                case "parent":
                    return EppModuleStructure.class;
                case "state":
                    return EppState.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRegistryModule> _dslPath = new Path<EppRegistryModule>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegistryModule");
    }
            

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Полное название.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getFullTitle()
     */
    public static PropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * Означает, что данный модуль можно использовать в разных дисциплинах
     *
     * @return Разделяемый. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#isShared()
     */
    public static PropertyPath<Boolean> shared()
    {
        return _dslPath.shared();
    }

    /**
     * @return Читающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getOwner()
     */
    public static OrgUnit.Path<OrgUnit> owner()
    {
        return _dslPath.owner();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getParent()
     */
    public static EppModuleStructure.Path<EppModuleStructure> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getState()
     */
    public static EppState.Path<EppState> state()
    {
        return _dslPath.state();
    }

    public static class Path<E extends EppRegistryModule> extends EntityPath<E>
    {
        private PropertyPath<String> _number;
        private PropertyPath<String> _title;
        private PropertyPath<String> _fullTitle;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _comment;
        private PropertyPath<Boolean> _shared;
        private OrgUnit.Path<OrgUnit> _owner;
        private EppModuleStructure.Path<EppModuleStructure> _parent;
        private EppState.Path<EppState> _state;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppRegistryModuleGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppRegistryModuleGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Полное название.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getFullTitle()
     */
        public PropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new PropertyPath<String>(EppRegistryModuleGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EppRegistryModuleGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EppRegistryModuleGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * Означает, что данный модуль можно использовать в разных дисциплинах
     *
     * @return Разделяемый. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#isShared()
     */
        public PropertyPath<Boolean> shared()
        {
            if(_shared == null )
                _shared = new PropertyPath<Boolean>(EppRegistryModuleGen.P_SHARED, this);
            return _shared;
        }

    /**
     * @return Читающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getOwner()
     */
        public OrgUnit.Path<OrgUnit> owner()
        {
            if(_owner == null )
                _owner = new OrgUnit.Path<OrgUnit>(L_OWNER, this);
            return _owner;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getParent()
     */
        public EppModuleStructure.Path<EppModuleStructure> parent()
        {
            if(_parent == null )
                _parent = new EppModuleStructure.Path<EppModuleStructure>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModule#getState()
     */
        public EppState.Path<EppState> state()
        {
            if(_state == null )
                _state = new EppState.Path<EppState>(L_STATE, this);
            return _state;
        }

        public Class getEntityClass()
        {
            return EppRegistryModule.class;
        }

        public String getEntityName()
        {
            return "eppRegistryModule";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
