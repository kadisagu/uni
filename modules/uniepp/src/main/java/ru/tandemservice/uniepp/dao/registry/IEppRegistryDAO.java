package ru.tandemservice.uniepp.dao.registry;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.data.EppRegElementKeyGenerator.KeyGenRule;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.*;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Операции с реестром Дисциплин, Практик и Доп.мероприятий
 * @author vdanilov
 */
public interface IEppRegistryDAO extends IUniBaseDao {
    SpringBeanCache<IEppRegistryDAO> instance = new SpringBeanCache<>(IEppRegistryDAO.class.getName());

    /**
     * Загружает данные нагрузки по дисциплинам
     * @return { discipline.id -> { loadType.fullCode -> discipline-load }}
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<Long, Map<String, EppRegistryElementLoad>> getRegistryElementTotalLoadMap(Collection<Long> disciplineIds);

    /**
     * Сохраняет данные в базе + вычисляет все вычислимые нагрузки
     * @param loadMap - результат getDisciplineLoadMap
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void saveRegistryElementTotalLoadMap(Map<Long, Map<String, EppRegistryElementLoad>> loadMap);

    /**
     * @param klass - класс записи реестра
     * @return Список элементов справочника EppRegistryStructure, доступных для указакнного класса записи реестра
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    List<EppRegistryStructure> getParentItems4Class(Class<? extends EppRegistryElement> klass);

    /** @return правило нумерации элементов реестра */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    INumberGenerationRule<EppRegistryElement> getNumberGenerationRule();

    /** @return часть дисциплины реестра (если таковой нет - то создает ее) */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    EppRegistryElementPart doGetRegistryElementPart(EppRegistryElement registryElement, int number);

    /**
     * сохраняет данные элемента реестра (проверяет использование частей)
     * @param element элемент реестра
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSaveRegistryElement(EppRegistryElement element);

    /**
     * сохраняет данные элемента реестра (проверяет использование частей)
     * сохраняет или удаляет cвязи с видами нагрузки, в зависимости от того, есть ли у элемента реестра
     * ограничение по виду потоков
     * @param element элемент реестра
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSaveRegistryElement(EppRegistryElement element, List<EppGroupType> eduGroupTypes);

    /**
     * сохраняет данные части элемента реестра
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSaveRegistryElementPart(EppRegistryElementPart element);

    /**
     * добавляет модуль к части элемента реестра
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doAddRegistryElementPartModule(EppRegistryElementPart element, EppRegistryModule module);


    /**
     * Получает перечень форм контроля по части дисциплины
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<EppFControlActionType, EppRegistryElementPartFControlAction> getRegistryElementPartControlActions(EppRegistryElementPart element);


    /**
     * сохраняет данные по итоговым формам контроля по части реестра
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    List<EppRegistryElementPartFControlAction> doSaveRegistryElementPartControlActions(EppRegistryElementPart element, Collection<EppRegistryElementPartFControlAction> actions);


    /**
     * возвращает обертки для указанных элементов реестра
     * @param registryElementIds список идентификаторов eppRegistryElement
     * @return { regEl.id -> wrapper(regEl) }
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<Long, IEppRegElWrapper> getRegistryElementDataMap(Collection<Long> registryElementIds);

    /**
     * возвращает обертки для указанных частей элементов реестра
     * @param registryElementPartIds список идентификаторов eppRegistryElementPart
     * @return { regElPart.id -> wrapper(regElPart) }
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<Long, IEppRegElPartWrapper> getRegistryElementPartDataMap(Collection<Long> registryElementPartIds);

    /**
     * перенумерует модули в частях элемента реестра
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doRecalculateRegistryElementNumbers(EppRegistryElement regElement);

    /**
     * Создает элемент реестра из враппера (либо на основе строки УПв, либо на основе другого элемента реестра)
     *
     * @param regElWrapper враппер для элемента реестра
     * @param newOwner читающее подразделение нового элемента реестра. Если не задано, берется из враппера.
     * @param newTitle название нового элемента реестра. Если не задано, берется из враппера.
     * @param newState состояние нового элемента реестра. Если не задано, элемент создается в состоянии на формировании.
     * @return вновь созданный элемент реестра
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    EppRegistryElement doCreateRegistryElement(@NotNull IEppRegElWrapper regElWrapper, @Nullable OrgUnit newOwner, @Nullable String newTitle, @Nullable EppState newState);


    /**
     * Обновляет элемент реестра по врапперу
     * TODO реализовать, когда понадобится
     *
     * @param registryElementId идентификатор обновляемого элемента реестра
     * @param regElWrapper враппер элемента реестра
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doUpdateRegistryElement(Long registryElementId, IEppRegElWrapper regElWrapper);

    /**
     * Производит объединение дублей элементов реестра.
     * Части и нагрузки, ссылающиеся на дубликаты, удаляются.
     * Все ссылки на дубли заменяются ссылками на шаблонную сущность.
     * Сами дубли удаляются.
     *
     * @param templateId идентификатор элеемнта, на базе которой производится объединение
     * @param duplicateIds идентификаторы дублей, которые следует удалить. Коллекция не должна содержать идентификатор шаблона и должна быть не пуста.
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doRegElementDuplicatesMerge(Long templateId, Collection<Long> duplicateIds);

    /**
     * Автоматическое создание и прикрепление элементов реестра к строкам УПв.
     * На каждую строку УПв создается новый элемент реестра и прикрепляется к строке УПв.
     *
     * @param wrappers список строк УПв
     * @param newState создавать элемент реестра в состоянии
     * @param alwaysCreateNewRegElement заменять существующий элемент в строке новым
     * @return map: epvRow.id -> newRegElement
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Map<Long, EppRegistryElement> createAndAttachRegElements(Collection<IEppEpvRowWrapper> wrappers, EppState newState, boolean alwaysCreateNewRegElement);

    /**
     * Автоматическое прикрепление элементов реестра к строкам УПв
     *
     * @param wrappers        список строк УПв
     * @param createIfMissing автоматически создавать элемент реестра, если подходящего не найдено
     * @param keyGenRule      правило генерации ключа для поиска элементов реестра
     * @param filterSates     искать элементы реестра в состояниях
     * @param newState        создавать элемент реестра в состоянии
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doAutoAttachRegElements(Collection<IEppEpvRowWrapper> wrappers, boolean createIfMissing, KeyGenRule keyGenRule, Collection<EppState> filterSates, EppState newState);

    /**
     * Если у элемента реестра есть ограничение по способу деления потоков,
     * то возвращает виды нагрузки, для которых действует ограничение
     * @param registryElement элемент реестра
     * @return виды нагрузок
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    List<EppGroupType> getRegElementSplitGroupTypes(EppRegistryElement registryElement);

    /**
     * Если у элемента реестра с переданным ид есть ограничение по способу деления потоков,
     * то возвращает список видов нагрузки для каждого элемента реестра
     *
     * @param registryElementIds идентификаторы элемента реестра
     * @return registryElementId -> [eppGroupType]
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    Map<Long, List<EppGroupType>> getRegElementSplitGroupTypes(List<Long> registryElementIds);

    /**
     * Возвращает номера частей для каждой формы контроля, элемента реестра.
     *
     * @param disciplineIds ids элементов реестра
     * @return { discipline.id -> { EppFControlActionType.id -> { EppRegistryElementPart.getNumber() }}}
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    Map<Long, Map<Long, List<Integer>>> getRegistryElement2FControlActionPartNumberMap(Collection<Long> disciplineIds);

    public  Map<String, Integer> getRegistryElementControlActionCountByAbbreviation(final EppRegistryElement element);
}
