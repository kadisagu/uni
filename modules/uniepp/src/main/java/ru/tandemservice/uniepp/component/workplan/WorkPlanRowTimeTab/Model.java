package ru.tandemservice.uniepp.component.workplan.WorkPlanRowTimeTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

/**
 * 
 * @author vdanilov
 */
@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id") })
public class Model
{
    private final EntityHolder<EppWorkPlanBase> holder = new EntityHolder<EppWorkPlanBase>();
    public EntityHolder<EppWorkPlanBase> getHolder() { return this.holder; }

    public EppWorkPlanBase getEppWorkPlan() { return this.getHolder().getValue(); }
    public Long getId() { return this.getHolder().getId(); }

    private final StaticListDataSource<IEntity> dataSource = new StaticListDataSource<IEntity>();
    public StaticListDataSource<IEntity> getDataSource() { return this.dataSource; }

}
