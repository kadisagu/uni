package ru.tandemservice.uniepp.component.registry.RegElementWorkPlanList;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {
    void prepareDataSource(Model model);
}
