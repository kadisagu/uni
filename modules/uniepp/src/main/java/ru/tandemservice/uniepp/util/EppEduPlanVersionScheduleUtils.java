package ru.tandemservice.uniepp.util;

import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.IRawFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.gen.CourseGen;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uni.ui.formatters.CourseRomanFormatter;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionWeekTypeGen;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniepp.tapestry.richTableList.RichRangeSelection;
import ru.tandemservice.uniepp.tapestry.richTableList.WeekTypeBlockColumn;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author vdanilov
 */
public abstract class EppEduPlanVersionScheduleUtils {


    // разбиение по умолчанию
    private static final int[][][] PREDEFINED = new int[][][]{
        {{0, 51}},
        {{0, 21}, {22, 51}},
        {{0, 17}, {18, 34}, {35, 51}},
        {{0, 10}, {11, 21}, {22, 32}, {33, 51}},
    };


    public static int[] getPoints4EmptyGridRow(final DevelopGrid developGrid, final Course course) {
        // еще ничего не создано. распределяем семестры согласно учебной сетке
        final Integer[] detail = IDevelopGridDAO.instance.get().getDevelopGridDetail(developGrid).get(course);
        final List<Integer> list = new ArrayList<>();

        if (detail.length <= EppEduPlanVersionScheduleUtils.PREDEFINED.length) {
            final int[][] pairs =  EppEduPlanVersionScheduleUtils.PREDEFINED[detail.length - 1];
            for (int i = 0; i < detail.length; i++)
            {
                if (detail[i] != null)
                {
                    final int[] pair = pairs[i];
                    list.add(pair[0]);
                    list.add(pair[1]);
                }
            }
        } else {
            final int[] point = EppEduPlanVersionScheduleUtils.PREDEFINED[0][0];
            final int step = Math.max(1, (point[1]-point[0])/detail.length);
            int j = point[0];
            list.add(j);
            for (int i=1;i<detail.length;i++) {
                j+=step;
                list.add(j++);
                list.add(j++);
            }
            list.add(point[1]);
        }

        final int[] points = new int[list.size()];
        {
            int i = 0;
            for (final Integer k : list) { points[i++] = k; }
        }
        return points;
    }


    public static final Long TOTAL_ID = 0L;

    public static final String ROW_COUNT = "rowCount";

    public static final IRawFormatter<MutableInt> BOLD_FORMATTER = value -> {
        if (null == value) { return ""; }
        return "<b>" + value + "</b>";
    };

    public static final CourseRomanFormatter COURSE_FORMATTER = CourseRomanFormatter.INSTANCE; // todo inline it

    public static final SimpleRowCustomizer<ViewWrapper<Course>> TOTAL_ROW_CUSTOMIZER = new SimpleRowCustomizer<ViewWrapper<Course>>() {
        @Override public String getRowStyle(final ViewWrapper<Course> entity) {
            return (EppEduPlanVersionScheduleUtils.TOTAL_ID.equals(entity.getId()) ? "font-weight: bold;" : "");
        }
    };



    protected abstract boolean isTotalCourseRowPresent();
    protected abstract EppEduPlanVersion getEduPlanVersion();
    final EppEduPlanVersion version = this.getEduPlanVersion();

    private final List<EppWeek> globalWeekList = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppWeek.class);
    public List<EppWeek> getGlobalWeekList() { return this.globalWeekList; }

    private final Course totalFakeCourse = this.buildTotalFakeCourse();
    public Course getTotalFakeCourse() { return this.totalFakeCourse; }

    private final Map<Course, Integer[]> developGridDetail = IDevelopGridDAO.instance.get().getDevelopGridDetail(this.version.getEduPlan().getDevelopGrid());
    public Map<Course, Integer[]> getDevelopGridDetail() { return this.developGridDetail; }

    private static List<Integer> getDevelopGridDetailTerms(final Integer[] developGridDetail4Course) {
        final List<Integer> terms = new ArrayList<>(4);
        for (final Integer termInRow: developGridDetail4Course) {
            if (null != termInRow) { terms.add(termInRow); }
        }
        return terms;
    }

    public static List<Integer> getDevelopGridDetailTerms(final DevelopGrid grid, final Course course) {
        return EppEduPlanVersionScheduleUtils.getDevelopGridDetailTerms(IDevelopGridDAO.instance.get().getDevelopGridDetail(grid).get(course));
    }

    public List<Integer> getDevelopGridDetailTerms(final Course course) {
        return EppEduPlanVersionScheduleUtils.getDevelopGridDetailTerms(this.getDevelopGridDetail().get(course));
    }

    private final List<ViewWrapper<Course>> courseList = ViewWrapper.getPatchedList(this.calculateCourseList(this.developGridDetail));
    public List<ViewWrapper<Course>> getCourseList() { return this.courseList; }

    protected final List<EppEduPlanVersionWeekType> epvWeekTypeList = UniDaoFacade.getCoreDao().getList(
        EppEduPlanVersionWeekType.class,
        EppEduPlanVersionWeekTypeGen.eduPlanVersion(), this.version,
        EppEduPlanVersionWeekType.course().intValue().s(),
        EppEduPlanVersionWeekType.week().number().s()
    );

    private final Set<EppWeekType> weekTypeSet = this.calculateWeekTypeSet(this.epvWeekTypeList);
    public Set<EppWeekType> getWeekTypeSet() { return this.weekTypeSet; }

    private final RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> rangeSelectionListDataSource = this.buildRangeSelectionListDataSource();
    public RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> getRangeSelectionListDataSource() { return this.rangeSelectionListDataSource; }

    private Map<String, Map<String, Number>> weekTypeAggregationMap = this.buildWeekTypeAggregationMap();
    public Map<String, Map<String, Number>> getWeekTypeAggregationMap() { return this.weekTypeAggregationMap; }


    protected RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> buildRangeSelectionListDataSource()
    {
        final StaticListDataSource<ViewWrapper<Course>> scheduleDataSource = this.buildScheduleDataSource();

        final RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> scheduleRangeModel = new RangeSelectionWeekTypeListDataSource<>(scheduleDataSource);
        final Map<PairKey<Long, Long>, EppWeekType> dataMap = new HashMap<>();
        scheduleRangeModel.setDataMap(dataMap);

        final Map<Long, int[]> row2points = new HashMap<>();
        final Map<Long, int[]> row2data = new HashMap<>();
        scheduleRangeModel.setRow2points(row2points);
        scheduleRangeModel.setRow2data(row2data);
        this.prepareRowDataPoints(row2points, row2data);

        final Map<Long, int[]> row2ranges = new HashMap<>();
        for (final Map.Entry<Long, int[]> entry : row2points.entrySet()) {
            row2ranges.put(entry.getKey(), new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, entry.getValue()).getRanges());
        }

        for (final EppEduPlanVersionWeekType item : this.epvWeekTypeList)
        {
            final Course course = item.getCourse();
            final EppWeek week = item.getWeek();
            final EppWeekType weekType = item.getWeekType();

            // записываем тип недели в ячейку таблицы
            dataMap.put(PairKey.create(course.getId(), week.getId()), weekType);
        }

        return scheduleRangeModel;
    }

    protected StaticListDataSource<ViewWrapper<Course>> buildScheduleDataSource()
    {
        final StaticListDataSource<ViewWrapper<Course>> scheduleDataSource = new StaticListDataSource<>(this.courseList);
        final AbstractColumn courseColumn = new SimpleColumn("Курс", CourseGen.P_INT_VALUE, EppEduPlanVersionScheduleUtils.COURSE_FORMATTER).setClickable(false).setOrderable(false);
        courseColumn.setHeaderStyle("text-align:center");
        scheduleDataSource.addColumn(courseColumn);

        HeadColumn head = null;

        for (final EppWeek week : this.globalWeekList) {
            final String headName = "month."+week.getMonth();
            if ((null == head) || (!head.getName().equals(headName))) {
                scheduleDataSource.addColumn(head = new HeadColumn(headName, RussianDateFormatUtils.getMonthName(week.getMonth(), true)));
                head.setHeaderStyle("text-align:center");
            }

            final HeadColumn weekColumn = new HeadColumn("week."+week.getCode(), week.getTitle());
            weekColumn.setVerticalHeader(true);

            final WeekTypeBlockColumn numColumn = new WeekTypeBlockColumn(week.getId(), week.getNumber() - 1, Integer.toString(week.getNumber()));
            numColumn.setHeaderStyle("padding-left:0;padding-right:0;text-align:center;min-width:17px;font-size:11px;");

            weekColumn.addColumn(numColumn);
            head.addColumn(weekColumn);
        }

        return scheduleDataSource;
    }

    private void prepareRowDataPoints(final Map<Long, int[]> row2points, final Map<Long, int[]> row2data)
    {
        // row -> номер части в году -> [минимальный номер недели,максимальный номер недели]
        final Map<Long, Map<Integer, int[]>> map = new HashMap<>();
        for (final EppEduPlanVersionWeekType item : this.epvWeekTypeList)
        {
            // получаем данные
            final Long rowId = item.getCourse().getId();
            final int term = item.getTerm().getIntValue();
            final int weekNumber = item.getWeek().getNumber();
            int partNumber = 0;
            final Integer[] gridDetail = this.developGridDetail.get(item.getCourse());
            while ((partNumber < gridDetail.length) && ((null == gridDetail[partNumber]) || (gridDetail[partNumber] != term)))
            {
                partNumber++;
            }
            partNumber++;

            // сохраняем в мапе
            Map<Integer, int[]> partMap = map.get(rowId);
            if (partMap == null)
            {
                map.put(rowId, partMap = new TreeMap<>());
            }

            final int[] points = partMap.get(partNumber);
            if (points == null)
            {
                partMap.put(partNumber, new int[]{weekNumber, weekNumber});
            }
            else if (weekNumber < points[0])
            {
                points[0] = weekNumber;
            }
            else if (weekNumber > points[1])
            {
                points[1] = weekNumber;
            }
        }

        for (final Map.Entry<Long, Map<Integer, int[]>> entry : map.entrySet())
        {
            final Map<Integer, int[]> partMap = entry.getValue();
            final int[] points = new int[partMap.size() * 2];
            int i = 0;
            for (final int[] pair : partMap.values())
            {
                points[i++] = pair[0] - 1;
                points[i++] = pair[1] - 1;
            }

            row2points.put(entry.getKey(), points);
            row2data.put(entry.getKey(), new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, points).getData());
        }
    }


    private List<Course> calculateCourseList(final Map<Course, Integer[]> developGridDetail)
    {
        // список курсов согласно сетке учебного графика
        final List<Course> courseList = new ArrayList<>(developGridDetail.keySet());

        // фэйковый курс
        if (this.isTotalCourseRowPresent()) {

            courseList.add(this.totalFakeCourse);
        }

        return courseList;
    }

    protected Course buildTotalFakeCourse()
    {
        final Course totalFakeCourse = new Course();
        totalFakeCourse.setId(EppEduPlanVersionScheduleUtils.TOTAL_ID);
        totalFakeCourse.setCode("columnCount");
        return totalFakeCourse;
    }

    private Set<EppWeekType> calculateWeekTypeSet(final List<EppEduPlanVersionWeekType> epvWeekTypeList)
    {
        // заполнение типов недель
        final Set<EppWeekType> weekTypeSet = new LinkedHashSet<>();
        for (final EppEduPlanVersionWeekType rel : epvWeekTypeList)  { weekTypeSet.add(rel.getWeekType()); }

        // фэйковый итоговый тип недели
        final EppWeekType total = new EppWeekType();
        total.setId(EppEduPlanVersionScheduleUtils.TOTAL_ID);
        total.setCode(EppEduPlanVersionScheduleUtils.ROW_COUNT);
        weekTypeSet.add(total);
        return weekTypeSet;
    }



    // номер_курса -> код_типа_недели -> количество_недель_типа
    @SuppressWarnings("unchecked")
    protected Map<String, Map<String, Number>> buildWeekTypeAggregationMap()
    {
        // подсчет сводных данных

        // номер_курса -> код_типа_недели  -> количество_недель_типа
        final Map<String, Map<String, MutableInt>> weekTypeSum = SafeMap.get(key -> new HashMap<>());

        // подсчет значений по курсам
        // подсчет колонки всего горизонтальной
        final Map<String, MutableInt> columnCount = new HashMap<>();
        for (final EppEduPlanVersionWeekType epvWeekType : this.epvWeekTypeList)
        {
            final String courseNumber = String.valueOf(epvWeekType.getCourse().getIntValue());
            final String weekTypeCode = epvWeekType.getWeekType().getCode();

            final Map<String, MutableInt> rowMap = weekTypeSum.get(courseNumber);
            if (rowMap.containsKey(weekTypeCode))
            {
                rowMap.get(weekTypeCode).increment();
            }
            else
            {
                rowMap.put(weekTypeCode, new MutableInt(1));
            }
            // подсчет колонки всего горизонтальной
            if (columnCount.containsKey(weekTypeCode))
            {
                columnCount.get(weekTypeCode).increment();
            }
            else
            {
                columnCount.put(weekTypeCode, new MutableInt(1));
            }
        }
        weekTypeSum.put("columnCount", columnCount);

        // подсчет колонки всего вертикальной
        for (final Entry<String, Map<String, MutableInt>> row : weekTypeSum.entrySet())
        {
            final MutableInt value = new MutableInt(0);
            for (final MutableInt intValue : row.getValue().values())
            {
                value.add(intValue);
            }
            row.getValue().put("rowCount", value);
        }

        return (Map)weekTypeSum;
    }

    public StaticListDataSource<ViewWrapper<Course>> getTimesDataSource() {
        final EppEduPlanVersionBlock rootBlock = IEppEduPlanVersionDataDAO.instance.get().getRootBlock(version);
        final IEppEpvBlockWrapper epvWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(rootBlock.getId(), true);
        final List<EppWeekType> weekTypeList = getEppWeekTypes();
        final StaticListDataSource<ViewWrapper<Course>> timesDataSource = new StaticListDataSource<ViewWrapper<Course>>();
        final RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> rangeModel = getRangeSelectionListDataSource();

        timesDataSource.addColumn(new SimpleColumn("Курс", CourseGen.P_INT_VALUE, EppEduPlanVersionScheduleUtils.COURSE_FORMATTER).setClickable(false).setOrderable(false).setWidth(10));

        timesDataSource.addColumn(new SimpleColumn("Всего учебных недель по частям курса", "graph") {
            @Override public boolean isRawContent() { return true; }
            @Override public String getContent(final IEntity entity)
            {
                final int[] points = rangeModel.getRow2points().get(entity.getId());
                if ((null == points) || (points.length < 1)) { return  ""; }

                final Course course = ((ViewWrapper<Course>)entity).getEntity();
                final List<Integer> terms = getDevelopGridDetailTerms(course);

                final StringBuilder sb = new StringBuilder();

                int i = 0, j = 0, p = 0;
                sb.append("<table border=\"0\" cellpadding=\"0\" class=\"epp-year-weeks-table\"><tr>");

                while (j<points.length)
                {
                    if (i<points[j]) {
                        sb.append("<td style=\"width:").append(2*(points[j]-i)).append("%\"></td>");
                    }
                    i = points[j++];


                    if (p < terms.size()) {

                        final Integer term = terms.get(p);
                        final MutableInt totalLoadWeeks = getTermSize(term, epvWrapper, IEppEpvBlockWrapper.ELOAD_FULL_CODE_ALL);
                        final MutableInt totalAuditWeeks = getTermSize(term, epvWrapper, EppELoadType.FULL_CODE_AUDIT);

                        sb.append("<td class=\"term\" style=\"width:").append(2*((1+points[j])-i)).append("%\">");

                        sb.append(totalLoadWeeks);
                        if (totalLoadWeeks != totalAuditWeeks) {
                            sb.append(getBracket());
                            sb.append(totalAuditWeeks);

                            final MutableInt totalSelfWorkWeeks = getTermSize(term, epvWrapper, EppELoadType.FULL_CODE_SELFWORK);
                            if (totalAuditWeeks != totalSelfWorkWeeks) {
                                sb.append(getDivisor());
                                sb.append(totalSelfWorkWeeks);
                            }
                            sb.append(")");
                        }

                        sb.append("</td>");

                    } else {
                        sb.append("<td class=\"skip\" style=\"width:").append(2*((1+points[j])-i)).append("%\"></td>");
                    }

                    i = (1+points[j++]);
                    p += 1;
                }

                if (i< EppWeek.YEAR_WEEK_COUNT) {
                    sb.append("<td style=\"width:").append(2*(EppWeek.YEAR_WEEK_COUNT-i)).append("%\"></td>");
                }

                sb.append("</tr></table>");
                return sb.toString();
            }
        }.setClickable(false).setOrderable(false).setHeaderAlign("center"));

        for (final EppWeekType weekType : weekTypeList) {
            timesDataSource.addColumn(new SimpleColumn(weekType.getAbbreviationView(), weekType.getCode()).setClickable(false).setOrderable(false).setVerticalHeader(true).setAlign("right").setHeaderAlign("center").setWidth(1));
        }

        timesDataSource.addColumn(new SimpleColumn("Всего", ROW_COUNT, BOLD_FORMATTER).setClickable(false).setOrderable(false).setVerticalHeader(true).setAlign("right").setHeaderAlign("center").setWidth(1));
        timesDataSource.setRowCustomizer(TOTAL_ROW_CUSTOMIZER);

        final List<ViewWrapper<Course>> entityList = new ArrayList(rangeModel.getDataSource().getEntityList());
        entityList.add(new ViewWrapper<Course>(getTotalFakeCourse()));
        timesDataSource.setupRows(entityList);

        // значения колонлок по типам недель
        final Map<String, Map<String, Number>> weekTypeAggregationMap = getWeekTypeAggregationMap();
        for (final ViewWrapper<Course> viewWrapper : timesDataSource.getEntityList())
        {
            final Map<String, Number> courseMap = weekTypeAggregationMap.get(viewWrapper.getEntity().getCode());
            for (final EppWeekType type : weekTypeList)  {
                viewWrapper.setViewProperty(type.getCode(), courseMap.get(type.getCode()));
            }
            viewWrapper.setViewProperty(EppEduPlanVersionScheduleUtils.ROW_COUNT, courseMap.get(EppEduPlanVersionScheduleUtils.ROW_COUNT));
        }
        return timesDataSource;
    }

    protected MutableInt getTermSize(Integer term, IEppEpvBlockWrapper epvWrapper, String eloadFullCodeAll)
    {
        return new MutableInt(epvWrapper.getTermSize(eloadFullCodeAll, term));
    }

    protected String getDivisor()
    {
        return " / ";
    }

    protected String getBracket()
    {
        return " (";
    }

    protected List<EppWeekType> getEppWeekTypes()
    {
        return UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppWeekType.class);
    }
}
