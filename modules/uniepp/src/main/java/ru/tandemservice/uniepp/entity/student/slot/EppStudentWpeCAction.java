package ru.tandemservice.uniepp.entity.student.slot;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.gen.EppStudentWpeCActionGen;

/**
 * Мероприятие студента из РУП по итоговой форме контроля (МСРП-ФК)
 */
public class EppStudentWpeCAction extends EppStudentWpeCActionGen implements Comparable<EppStudentWpeCAction>, ITitled
{

    public EppStudentWpeCAction() {

    }
    public EppStudentWpeCAction(final EppStudentWorkPlanElement slot, final EppGroupType type) {
        this();
        this.setStudentWpe(slot);
        this.setType(type);
    }
    public EppStudentWpeCAction(final EppStudentWorkPlanElement slot, final EppFControlActionType type) {
        this(slot, type.getEppGroupType());
    }

    @Override
    @EntityDSLSupport(parts = {EppStudentWpeCAction.L_STUDENT_WPE + "." + EppStudentWorkPlanElement.L_REGISTRY_ELEMENT_PART + "." + EppRegistryElementPart.P_TITLE, EppStudentWpeCAction.L_TYPE + "." + EppControlActionType.P_TITLE})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061307")
    public String getRegistryElementTitle()
    {
        return this.getStudentWpe().getRegistryElementPart().getTitleWithNumber() + ", " + StringUtils.uncapitalize(this.getType().getTitle());
    }

    @Override
    @EntityDSLSupport(parts = {
            EppStudentWpeCAction.L_STUDENT_WPE + "." + EppStudentWorkPlanElement.L_YEAR + "." + EppYearEducationProcess.L_EDUCATION_YEAR + "." + EducationYear.P_INT_VALUE,
            EppStudentWpeCAction.L_STUDENT_WPE + "." + EppStudentWorkPlanElement.L_PART + "." + YearDistributionPart.P_NUMBER
    })
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1060324")
    public String getTermTitle() {
        return this.getStudentWpe().getPart().getTitle() + " " + this.getStudentWpe().getYear().getTitle();
    }

    @Override
    public int compareTo(final EppStudentWpeCAction o)
    {
        int i;
        final EppStudentWorkPlanElement slot = this.getStudentWpe();
        final EppStudentWorkPlanElement otherSlot = o.getStudentWpe();
        if (0 != (i = Integer.compare(slot.getYear().getEducationYear().getIntValue(), otherSlot.getYear().getEducationYear().getIntValue()))) { return i; }
        if (0 != (i = Integer.compare(slot.getTerm().getIntValue(), otherSlot.getTerm().getIntValue()))) { return i; }
        if (0 != (i = CommonCollator.TITLED_COMPARATOR.compare(slot.getRegistryElementPart().getRegistryElement(), otherSlot.getRegistryElementPart().getRegistryElement()))) { return i; }
        if (0 != (i = Integer.compare(slot.getRegistryElementPart().getNumber(), otherSlot.getRegistryElementPart().getNumber()))) { return i; }
        if (0 != (i = Integer.compare(this.getType().getPriority(), o.getType().getPriority()))) { return i; }
        if (0 != (i = Long.compare(this.getId(), o.getId()))) { return i; }
        return 0;
    }

    @Override
    public String getTitle()
    {
        if (getType() == null) {
            return this.getClass().getSimpleName();
        }
        return "МСРП-ФК: " + getType().getTitle() + ", " + getStudentWpe().getTitle();
    }
    
    public OrgUnit getStudentCurrentGroupOu()
    {
        return getStudentWpe().getStudentCurrentGroupOu();
    }
    
    public OrgUnit getTutorOu()
    {
        return getStudentWpe().getTutorOu();
    }

    @Override
    public EppGroupTypeFCA getType()
    {
        return (EppGroupTypeFCA) super.getType();
    }

    /**
     * Внимание!
     * Метод выполняет запрос к базе. Использовать осознанно и аккуратно.
     */
    public EppFControlActionType getActionType()
    {
        return IUniBaseDao.instance.get().getNotNull(EppFControlActionType.class, EppFControlActionType.eppGroupType(), this.getType());
    }
}