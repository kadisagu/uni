/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.ui.RegistryElementCorrectionElementsTab;

import com.google.common.base.Function;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.fias.base.bo.util.NonActiveSelectValueStyle;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppOrgUnit.EppOrgUnitManager;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.RegistryBaseDSHandler;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractList;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 30.03.2016
 */
@Configuration
public class EppReorganizationRegistryElementCorrectionElementsTab extends EppRegistryAbstractList {

    public static final String REGISTRY_ELEMENT_OWNER_DS = "registryElementOwnerDS";
    public static final String OWNER_DS = "ownerDS";
    public static final String REGISTRY_ELEMENTS_DS = "registryElementsDS";
    public static final String REGISTRY_STRUCTURE_DS = "registryStructureDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(OWNER_DS, getName(), EppOrgUnitManager.instance().eppOrgUnitReorganizationDSHandler(getName()))
                        .valueStyleSource(value -> {
                            if (value instanceof OrgUnit && ((OrgUnit) value).isArchival()) return NonActiveSelectValueStyle.INSTANCE;
                            return null;
                        }))
                .addDataSource(searchListDS(ELEMENT_DS, registryElementsDSColumnListExtPoint(), registryElementsDSHandler()))
             //   .addDataSource(selectDS(REGISTRY_STRUCTURE_DS, registryStructureDSHandler()))
                .create();
    }


    @Bean
    public EntityComboDataSourceHandler registryStructureDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppRegistryStructure.class).order(EppRegistryStructure.title());
    }


    @Bean
    public ColumnListExtPoint registryElementsDSColumnListExtPoint()
    {
        return columnListExtPointBuilder(ELEMENT_DS)
                .addColumn(checkboxColumn("checkboxColumn"))
                .addColumn(getStateColumn())
                .addColumn(textColumn(COLUMN_NUMBER, EppRegistryElement.P_NUMBER).clickable(false).order())
                .addColumn(publisherColumn(COLUMN_TITLE, EppRegistryElement.P_TITLE).order())
                .addColumn(textColumn(COLUMN_STRUCTURE, EppRegistryElement.parent().title()).order())
                .addColumn(textColumn("partsColumn", EppRegistryElement.parts()))
                .addColumn(getLoadFormattedColumn("loadColumn", EppRegistryElement.labor().s()).required(false))
                .addColumn(getLoadFormattedColumn("hoursTotalColumn", EppRegistryElement.size().s()).required(false))
                .addColumn(textColumn("weeksColumn", "wrappedWeeksValue").formatter(UniEppUtils.LOAD_FORMATTER))
                .addColumn(publisherColumn("eduPlanVersionBlockColumn", "title")
                        .entityListProperty("wrappedRowsValue")
                        .formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .create();
    }

    @Override
    public String getMVCBasePackage() {
        return null;
    }


    @Bean
    public IBusinessHandler<DSInput, DSOutput> registryElementsDSHandler() {
        return new RegistryBaseDSHandler(this.getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context) {
                final DSOutput execute = super.execute(input, context);

                return execute.transform(new Function<IEntity, ViewWrapper>() {
                    @Nullable
                    @Override
                    public ViewWrapper apply(@Nullable IEntity input) {

                        ViewWrapper wrapper;
                        EppRegistryElement element;

                        if (input instanceof ViewWrapper) {
                            wrapper = ((ViewWrapper) input);
                            element = ((EppRegistryElement) wrapper.getEntity());
                        }
                        else {
                            element = ((EppRegistryElement) input);
                            wrapper = new ViewWrapper<>(element);
                        }

                        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, "rr")
                                .where(eq(property("rr", EppEpvRegistryRow.registryElement()), value(element)))
                                .column(property("rr", EppEpvRegistryRow.owner())).distinct()
                                ;

                        final List<EppEduPlanVersionBlock> list = builder.createStatement(context.getSession()).list();

                        wrapper.setViewProperty("wrappedRowsValue",
                                list.stream().sorted((o1, o2) -> {
                                    int result = 0;
                                    result = o1.getEduPlanVersion().getEduPlan().getNumber().compareTo(o2.getEduPlanVersion().getEduPlan().getNumber());
                                    if (result != 0) return result;
                                    result = o1.getEduPlanVersion().getNumber().compareTo(o2.getEduPlanVersion().getNumber());
                                    if (result != 0) return result;
                                    String o1specTitle = o1 instanceof EppEduPlanVersionSpecializationBlock ? ((EppEduPlanVersionSpecializationBlock) o1).getProgramSpecialization().getDisplayableTitle() : "Общий блок";
                                    String o2specTitle = o2 instanceof EppEduPlanVersionSpecializationBlock ? ((EppEduPlanVersionSpecializationBlock) o2).getProgramSpecialization().getDisplayableTitle() : "Общий блок";
                                    return o1specTitle.compareToIgnoreCase(o2specTitle);
                                    }
                                ).map(row -> new DataWrapper(row.getId(), row.getVersionFullTitleWithBlockTitle()))
                                        .collect(Collectors.toList()));

                        Double weeks = 0d;

                        if (element instanceof EppRegistryAction)
                            weeks = (((EppRegistryAction) element).getWeeksAsDouble());

                        wrapper.setViewProperty("wrappedWeeksValue", weeks);

                        return wrapper;
                    }
                });
            }

            @Override
            protected void applyWhereConditions(DQLSelectBuilder builder, String alias, DSInput input, Class elementClass, ExecutionContext context) {
                super.applyWhereConditions(builder, alias, input, elementClass, context);

                final EppRegistryStructure structure = context.get("structure");
                if (null != structure)
                    builder.where(or(eq(property(alias, EppRegistryElement.parent().id()), value(structure.getId())),
                        (eq(property(alias, EppRegistryElement.parent().parent().id()), value(structure.getId())))));
            }
        };
    }

}
