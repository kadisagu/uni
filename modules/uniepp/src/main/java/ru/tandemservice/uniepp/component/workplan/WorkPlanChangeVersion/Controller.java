/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.workplan.WorkPlanChangeVersion;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author nkokorina
 * Created on: 12.08.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().update(model);
        this.deactivate(component);
    }

    public void onClickUpdateRowDataList(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepareRowsData(model);
    }

}
