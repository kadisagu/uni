/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.EduProgramHigherProfManager;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.logic.CustomEduPlanDSHandler;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.AddEdit.EppCustomEduPlanAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.AddEdit.EppCustomEduPlanAddEditUI;

/**
 * @author Nikolay Fedorovskih
 * @since 14.08.2015
 */
public class EppCustomEduPlanListUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (!EduProgramHigherProfManager.EDU_PROGRAM_KIND_HIGHER_PROF_DS.equals(dataSource.getName())) {
            dataSource.putAll(getSettings().getAsMap(
                    CustomEduPlanDSHandler.PARAM_PROGRAM_KIND,
                    CustomEduPlanDSHandler.PARAM_NUMBER,
                    CustomEduPlanDSHandler.PARAM_SUBJECT_INDEX,
                    CustomEduPlanDSHandler.PARAM_PROGRAM_SUBJECT,
                    CustomEduPlanDSHandler.PARAM_PROGRAM_FORM,
                    CustomEduPlanDSHandler.PARAM_DEVELOP_CONDITION,
                    CustomEduPlanDSHandler.PARAM_PROGRAM_TRAIT,
                    CustomEduPlanDSHandler.PARAM_DEVELOP_GRID,
                    CustomEduPlanDSHandler.PARAM_EDU_PLAN_VERSION,
                    CustomEduPlanDSHandler.PARAM_STATE,
                    CustomEduPlanDSHandler.PARAM_STUDENT_LAST_NAME
            ));
        }
    }

    // listeners

    public void onClickAddCustomEduPlan()
    {
        getActivationBuilder().asRegionDialog(EppCustomEduPlanAddEdit.class)
                .parameter(EppCustomEduPlanAddEditUI.BIND_PROGRAM_KIND_ID, getProgramKind().getId())
                .activate();
    }

    public void onEditEntityFromList()
    {
        getActivationBuilder().asRegionDialog(EppCustomEduPlanAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, this.getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(this.getListenerParameterAsLong());
    }

    public void onChangeProgramKind()
    {
        this.clearSettings();
    }

    @Override
    public void clearSettings()
    {
        CommonBaseSettingsUtil.clearAndSaveSettingsExcept(getSettings(), CustomEduPlanDSHandler.PARAM_PROGRAM_KIND);
    }

    // Getters & Setters

    public EduProgramKind getProgramKind()
    {
        return this.getSettings().get(CustomEduPlanDSHandler.PARAM_PROGRAM_KIND);
    }

    public boolean isNothingSelected()
    {
        return getProgramKind() == null;
    }
}