package ru.tandemservice.uniepp.component.student.StudentEduplanWorkGraph;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.ViewWrapper;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;

/**
 * @author nkokorina
 */

@Input({ @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id") })
public class Model
{

    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    private RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> _scheduleDataSource;
    public RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> getScheduleDataSource() { return this._scheduleDataSource; }
    public void setScheduleDataSource(final RangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> scheduleDataSource) { this._scheduleDataSource = scheduleDataSource; }

}
