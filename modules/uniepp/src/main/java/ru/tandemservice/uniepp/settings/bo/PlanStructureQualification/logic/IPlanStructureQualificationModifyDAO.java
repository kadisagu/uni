package ru.tandemservice.uniepp.settings.bo.PlanStructureQualification.logic;

/**
 * @author avedernikov
 * @since 14.09.2015
 */

public interface IPlanStructureQualificationModifyDAO
{
	/**
	 * Добавляет/удаляет связь элементов ГОС/УП с перечнем направлений (EppPlanStructure4SubjectIndex)
	 * @param planStructureId - id элемента ГОС/УП
	 * @param indexId - id перечня направлений
	 */
	void doSwitchPlanStructureQualification(final Long planStructureId, final Long indexId);
}
