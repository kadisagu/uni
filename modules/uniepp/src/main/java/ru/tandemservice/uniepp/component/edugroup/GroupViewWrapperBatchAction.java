package ru.tandemservice.uniepp.component.edugroup;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroupRowGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public abstract class GroupViewWrapperBatchAction<X extends EppRealEduGroup> implements BatchUtils.Action<ViewWrapper<X>> {


    protected final Class<? extends EppRealEduGroupRow> relationKlass;
    protected final Session session;
    public GroupViewWrapperBatchAction(final Class<? extends EppRealEduGroupRow> relationKlass, final Session session) {
        this.relationKlass = relationKlass;
        this.session = session;
    }

    protected final IFormatter<String> groupFormatter = source -> (StringUtils.isEmpty(source) ? "без группы" : source);

    protected final IFormatter<Long> educationOrgUnitFormatter = new IFormatter<Long>() {
        @Override public String format(final Long id) {
            if (null == id) { return null; }
            final EducationOrgUnit eduOu = (EducationOrgUnit)GroupViewWrapperBatchAction.this.session.get(EducationOrgUnit.class, id);
            return (eduOu.getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix() + " " + eduOu.getShortTitle() + " " + eduOu.getDevelopCombinationTitle());
        }
    };

    protected final IFormatter<Long> ppsFormatter = new IFormatter<Long>() {
        @Override public String format(final Long id) {
            if (null == id) { return null; }
            final PpsEntry pps = (PpsEntry)GroupViewWrapperBatchAction.this.session.get(PpsEntry.class, id);
            return (pps.getTitle());
        }
    };

    @SuppressWarnings("unchecked")
    protected <T> Map<Long, Map<T, Integer>> getRelationMap(final Collection<Long> ids, final PropertyPath<T> relationPath) {
        Debug.begin(relationPath.s());
        try {
            final Map<Long, Map<T, Integer>> result = new HashMap<>(32);

            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(this.relationKlass, "row");
            dql.where(isNull(property(EppRealEduGroupRowGen.removalDate().fromAlias("row"))));
            dql.where(in(property(EppRealEduGroupRowGen.group().id().fromAlias("row")), ids));

            dql.order(property(EppRealEduGroupRowGen.group().id().fromAlias("row")));
            dql.order(property(relationPath.fromAlias("row")));

            dql.group(property(EppRealEduGroupRowGen.group().id().fromAlias("row")));
            dql.group(property(relationPath.fromAlias("row")));

            dql.column(property(EppRealEduGroupRowGen.group().id().fromAlias("row")));
            dql.column(property(relationPath.fromAlias("row")));

            dql.column(DQLFunctions.count(property(EntityBase.id().fromAlias("row"))));

            for (final Object[] row: UniBaseDao.scrollRows(dql.createStatement(this.session))) {
                SafeMap.safeGet(result, (Long)row[0], LinkedHashMap.class).put((T)row[1], ((Number)row[2]).intValue());
            }
            return result;
        } finally {
            Debug.end();
        }
    }

    protected Map<Long, Map<Long, Integer>> getTutorMap(final Collection<Long> ids) {
        final Map<Long, Map<Long, Integer>> tutorMap = new HashMap<>(ids.size());
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(EppPpsCollectionItem.class, "rel");
            dql.column(property(EppPpsCollectionItem.list().id().fromAlias("rel")));
            dql.column(property(EppPpsCollectionItem.pps().id().fromAlias("rel")));
            dql.where(in(property(EppPpsCollectionItem.list().id().fromAlias("rel")), ids));

            final Integer ONE = 1;
            for (final Object[] row: UniBaseDao.scrollRows(dql.createStatement(this.session))) {
                SafeMap.safeGet(tutorMap, (Long)row[0], LinkedHashMap.class).put((Long)row[1], ONE);
            }
        }
        return tutorMap;
    }


    protected <T> String collect(final Map<Long, Map<T, Integer>> map, final Long id, final IFormatter<T> formatter, final boolean showCount) {
        final Map<T, Integer> count = map.get(id);
        if (null == count) { return ""; }

        final Set<String> elements = new LinkedHashSet<>(count.size());
        for (final Map.Entry<T, Integer> i: count.entrySet()) {
            final Object obj = ((null == formatter) || (null == i.getKey())) ? i.getKey() : formatter.format(i.getKey());
            final String value = (null == obj ? "" : String.valueOf(obj));
            elements.add(value + (showCount ? (" [" + i.getValue() + "]") : ""));
        }
        return StringUtils.join(elements, "\n");
    }

}
