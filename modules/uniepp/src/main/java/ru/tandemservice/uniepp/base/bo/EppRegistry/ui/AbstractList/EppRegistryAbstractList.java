/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.datasource.column.IHeaderColumnBuilder;
import org.tandemframework.caf.ui.config.datasource.column.IIndicatorColumnBuilder;
import org.tandemframework.caf.ui.config.datasource.column.ITextColumnBuilder;
import org.tandemframework.caf.ui.config.datasource.column.TextDSColumn;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtPointBuilder;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppRegistry.EppRegistryManager;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.List;

/**
 * @author Irina Ugfeld
 * @since 03.03.2016
 */
public abstract class EppRegistryAbstractList extends BusinessComponentManager {

    //Кнопки
    public static final String BLOCK_ACTION_BUTTONS = "actionButtons";
    public static final String ADD_BUTTON = "regElements.add";
    public static final String MASS_EDIT_BUTTON = "regElements.massEdit";
    //Список
    public static final String ELEMENT_DS = "regElementsDS";
    //Фильтры
    public static final String OWNER_DS = "ownerDS";
    public static final String EDU_GROUP_SPLIT_VARIANT_DS = "eduGroupSplitVariantDS";

    //Колонки списка
    public static final String COLUMN_NUMBER = "columnTitle.number";
    public static final String COLUMN_TITLE = "columnTitle.title";
    public static final String COLUMN_STATE_ICON = "columnTitle.stateIcon";
    public static final String COLUMN_FULL_TITLE = "columnTitle.fullTitle";
    public static final String COLUMN_SHORT_TITLE = "columnTitle.shortTitle";
    public static final String COLUMN_EDIT = "columnTitle.edit";
    public static final String COLUMN_DELETE = "columnTitle.delete";
    public static final String COLUMN_STATE = "columnTitle.state";
    public static final String COLUMN_ORG_UNIT = "columnTitle.orgUnit";
    public static final String COLUMN_STRUCTURE = "columnTitle.structure";
    public static final String COLUMN_GROUP_SPLIT_VARIANT = "columnTitle.eduGroupSplitVariant";
    public static final String COLUMN_PARTS = "columnTitle.parts";
    public static final String COLUMN_CLASSROOM_HOURS = "columnTitle.classroomHours";
    public static final String COLUMN_LABOR = "columnTitle.labor";
    public static final String COLUMN_HOURS = "columnTitle.hours";
    public static final String COLUMN_TOTAL = "columnTitle.total";
    public static final String COLUMN_WEEKS = "columnTitle.weeks";
    public static final String COLUMN_CHECKBOX = "columnTitle.checkbox";

    //сообщение при удалении элемента реестра
    public static final String REG_ELEMENT_DELETE_ALERT = "regElementsDS.deleteRecord.alert";

    //базовый mvc пакет компонента (для вызова .Pub и .AddEdit)
    abstract public String getMVCBasePackage();

    protected IPresenterExtPointBuilder createBasePresenterExtPointBuilder()
    {
        return presenterExtPointBuilder()
                .addDataSource(EppRegistryManager.instance().splitDSConfig())
                .addDataSource(selectDS(OWNER_DS, EppRegistryManager.instance().ownerDSHandler()))
                .addDataSource(EppStateManager.instance().eppStateDSConfig());
    }

    /**
     * Блок кнопок
     *
     */
    protected ButtonListExtPoint blockActionButtonListExtPoint() {
        return buttonListExtPointBuilder(BLOCK_ACTION_BUTTONS)
                .addButton(submitButton(ADD_BUTTON).listener("onClickAddElement").permissionKey("ui:sec.add"))
                .addButton(submitButton(MASS_EDIT_BUTTON).listener("onClickMassEdit").permissionKey("ui:sec.massSplitEdit"))
                .create();
    }

    /**
     * Колонки реестра дисциплин
     *
     * @return билдер колонок
     */
    protected IColumnListExtPointBuilder getDisciplineColumns() {
        return getEditDeleteColumns(getDisciplineColumns(getBaseColumns()));
    }

    /**
     * Колонки реестра практик
     *
     * @return билдер колонок
     */
    protected IColumnListExtPointBuilder getPracticeColumns() {
        return getEditDeleteColumns(getPracticeColumns(getBaseColumns()));
    }

    /**
     * Колонки реестра мероприятий ГИА
     *
     * @return билдер колонок
     */
    protected IColumnListExtPointBuilder getAttestationColumns() {
        return getPracticeColumns();
    }

    /**
     * Добавляет базовые колонки для всех элементов реестра
     */
    protected IColumnListExtPointBuilder getBaseColumns() {

        return columnListExtPointBuilder(ELEMENT_DS)
                .addColumn(getStateColumn())
                .addColumn(checkboxColumn(COLUMN_CHECKBOX))
                .addColumn(textColumn(COLUMN_NUMBER, EppRegistryElement.P_NUMBER).clickable(false).required(true).order())
                .addColumn(publisherColumn(COLUMN_TITLE, EppRegistryElement.P_TITLE).required(true).order())
                .addColumn(textColumn(COLUMN_FULL_TITLE, EppRegistryElement.P_FULL_TITLE).order())
                .addColumn(textColumn(COLUMN_SHORT_TITLE, EppRegistryElement.P_SHORT_TITLE).order())
                .addColumn(textColumn(COLUMN_STATE, EppRegistryElement.state().title()).order())
                .addColumn(textColumn(COLUMN_ORG_UNIT, EppRegistryElement.owner().shortTitle()).order().visible("mvel:!presenter.fromOrgUnit"))
                .addColumn(textColumn(COLUMN_STRUCTURE, EppRegistryElement.parent().title()).order())
                .addColumn(textColumn(COLUMN_GROUP_SPLIT_VARIANT, EppRegistryElement.P_TITLE_WITH_SPLIT_VARIANT));

    }

    /**
     * Добавляет колонки, которые есть только у реестра дисциплин
     */
    protected IColumnListExtPointBuilder getDisciplineColumns(IColumnListExtPointBuilder extPointBuilder) {
        final List<EppALoadType> aLoadTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppALoadType.class);
        final List<EppELoadType> eLoadTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppELoadType.class);

        //Колонки "Частей" и "Трудоемкость"
        extPointBuilder.addColumn(textColumn(COLUMN_PARTS, EppRegistryElement.P_PARTS).required(true).verticalHeader("true"));
        extPointBuilder.addColumn(getLoadFormattedColumn(COLUMN_LABOR, EppRegistryElement.P_LABOR).verticalHeader("true"));

        // Колонка "Часов"
        IHeaderColumnBuilder loadXHeadColumn = headerColumn(COLUMN_HOURS);
        //Колонка "Всего"  в колонке "Часов"
        loadXHeadColumn.addSubColumn(getLoadFormattedColumn(COLUMN_TOTAL, EppRegistryElement.P_SIZE).verticalHeader("true").create());

        //Колонка "Аудиторных"  в колонке "Часов"
        IHeaderColumnBuilder loadAHeadColumn = headerColumn(COLUMN_CLASSROOM_HOURS);
        //Колонки аудиторных учебных нагрузок в составе колонки "Аудиторных"
        eLoadTypes.stream().filter(EppELoadType::isAuditTotal).forEach(e -> addLoadTypeColumn(loadAHeadColumn, e));
        aLoadTypes.forEach(a -> addLoadTypeColumn(loadAHeadColumn, a));
        loadXHeadColumn.addSubColumn(loadAHeadColumn.create());

        //Колонки внеаудиторных нагрузок в колонке "Часов"
        eLoadTypes.stream().filter(e -> !e.isAuditTotal()).forEach(e -> addLoadTypeColumn(loadXHeadColumn, e));
        extPointBuilder.addColumn(loadXHeadColumn);

        return extPointBuilder;
    }

    protected ITextColumnBuilder getLoadFormattedColumn(String name, String path) {
        return textColumn(name, path + "AsDouble")
                .formatter(UniEppUtils.LOAD_FORMATTER)
                .required(true);
    }

    /**
     * Добавляет колонки, которые есть только у реестра практик
     */
    protected IColumnListExtPointBuilder getPracticeColumns(IColumnListExtPointBuilder baseColumns) {

        IHeaderColumnBuilder headColumn = headerColumn(COLUMN_TOTAL);
        headColumn.addSubColumn(getLoadFormattedColumn(COLUMN_HOURS, EppRegistryElement.P_SIZE).create());
        headColumn.addSubColumn(getLoadFormattedColumn(COLUMN_LABOR, EppRegistryElement.P_LABOR).create());
        headColumn.addSubColumn(getLoadFormattedColumn(COLUMN_WEEKS, EppRegistryAction.P_WEEKS).create());

        return baseColumns.addColumn(headColumn);
    }

    /**
     * Добавляет колонки редактирования и удаления
     */
    protected IColumnListExtPointBuilder getEditDeleteColumns(IColumnListExtPointBuilder extPointBuilder) {
        return extPointBuilder
                .addColumn(actionColumn(COLUMN_EDIT, CommonDefines.ICON_EDIT, "onClickEditElement").permissionKey("ui:sec.edit").create())
                .addColumn(actionColumn(COLUMN_DELETE, CommonDefines.ICON_DELETE, "onClickDeleteElement").permissionKey("ui:sec.delete")
                        .alert(new FormattedMessage(REG_ELEMENT_DELETE_ALERT, EppRegistryElement.P_TITLE)));
    }

    /**
     * Возвращает indicatorColumn, значения в которой зависят от значения EppState
     */
    protected IIndicatorColumnBuilder getStateColumn() {
        return indicatorColumn(COLUMN_STATE_ICON, EppRegistryElement.state().code())
                .addIndicator(EppState.STATE_ACCEPTABLE, new IndicatorColumn.Item("epp-state/state-" + EppState.STATE_ACCEPTABLE))
                .addIndicator(EppState.STATE_ACCEPTED, new IndicatorColumn.Item("epp-state/state-" + EppState.STATE_ACCEPTED))
                .addIndicator(EppState.STATE_ARCHIVED, new IndicatorColumn.Item("epp-state/state-" + EppState.STATE_ARCHIVED))
                .addIndicator(EppState.STATE_FORMATIVE, new IndicatorColumn.Item("epp-state/state-" + EppState.STATE_FORMATIVE))
                .addIndicator(EppState.STATE_REJECTED, new IndicatorColumn.Item("epp-state/state-" + EppState.STATE_REJECTED));
    }

    /**
     * Добавляет в headColumn колонку, с названием loadType.getShortTitle()
     */
    private void addLoadTypeColumn(final IHeaderColumnBuilder headColumn, final EppLoadType loadType) {
        TextDSColumn column = textColumn(loadType.getFullCode(), loadType.getFullCode())
                .selectCaption(loadType.getShortTitle())
                .formatter(UniEppUtils.LOAD_FORMATTER)
                .verticalHeader("true")
                .required(true)
                .create();

        column.setLabel(loadType.getShortTitle());
        headColumn.addSubColumn(column);

    }
}