package ru.tandemservice.uniepp.component.student.MassChangeEduPlanVersions;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

@Input( {
        @Bind(key = "ids", binding = "studentIds", required = true),
        @Bind(key = "eduPlanVersion", binding = "eduPlanVersion"),
        @Bind(key = "eduPlan", binding = "eduPlan"),
        @Bind(key = "individual", binding = "individual")
})
public class Model
{
    private List<Long> studentIds = Collections.emptyList();

    private ISelectModel eduPlanListModel;
    private ISelectModel eduPlanVersionListModel;
    private ISelectModel blockListModel;
    private ISelectModel customPlanListModel;

    private EppEduPlan eduPlan;
    private EppEduPlanVersion eduPlanVersion;
    private EppEduPlanVersionBlock block;
    private EppCustomEduPlan customEduPlan;
    private boolean individual;

    public EppEduPlan getEduPlan()
    {
        return this.eduPlan;
    }

    public void setEduPlan(final EppEduPlan eduPlan)
    {
        this.eduPlan = eduPlan;
    }

    public EppEduPlanVersion getEduPlanVersion()
    {
        return this.eduPlanVersion;
    }

    public void setEduPlanVersion(final EppEduPlanVersion eduPlanVersion)
    {
        this.eduPlanVersion = eduPlanVersion;
    }

    public ISelectModel getEduPlanListModel()
    {
        return this.eduPlanListModel;
    }

    public void setEduPlanListModel(final ISelectModel eduPlanListModel)
    {
        this.eduPlanListModel = eduPlanListModel;
    }

    public ISelectModel getEduPlanVersionListModel()
    {
        return this.eduPlanVersionListModel;
    }

    public void setEduPlanVersionListModel(final ISelectModel eduPlanVersionListModel)
    {
        this.eduPlanVersionListModel = eduPlanVersionListModel;
    }

    public ISelectModel getCustomPlanListModel()
    {
        return customPlanListModel;
    }

    public void setCustomPlanListModel(ISelectModel customPlanListModel)
    {
        this.customPlanListModel = customPlanListModel;
    }

    public List<Long> getStudentIds()
    {
        return this.studentIds;
    }

    public void setStudentIds(final List<Long> studentIds)
    {
        this.studentIds = studentIds;
    }

    public EppEduPlanVersionBlock getBlock()
    {
        return block;
    }

    public void setBlock(EppEduPlanVersionBlock block)
    {
        this.block = block;
    }

    public ISelectModel getBlockListModel()
    {
        return blockListModel;
    }

    public void setBlockListModel(ISelectModel blockListModel)
    {
        this.blockListModel = blockListModel;
    }

    public boolean isShowMessage()
    {
        final Map<Long, EppStudent2EduPlanVersion> relationMap = IEppEduPlanDAO.instance.get().getActiveStudentEduplanVersionRelationMap(this.getStudentIds());
        if (relationMap.isEmpty()) { return false; }

        final Set<Long> versionIds = new HashSet<>();
        for (final Map.Entry<Long, EppStudent2EduPlanVersion> entry: relationMap.entrySet()) {
            final EppStudent2EduPlanVersion relation = entry.getValue();
            if (null == relation) { return false; }
            versionIds.add(relation.getEduPlanVersion().getId());
        }

        return versionIds.size() > 1;
    }

    public boolean isIndividual()
    {
        return individual;
    }

    public void setIndividual(boolean individual)
    {
        this.individual = individual;
    }

    public EppCustomEduPlan getCustomEduPlan()
    {
        return customEduPlan;
    }

    public void setCustomEduPlan(EppCustomEduPlan customEduPlan)
    {
        this.customEduPlan = customEduPlan;
    }
}
