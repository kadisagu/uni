package ru.tandemservice.uniepp.component.group.GroupWorkPlanTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.IStudentListModel;

import java.util.Collections;
import java.util.List;

/**
 * 
 * @author nkokorina
 *
 */

@Input( { @Bind(key = "groupId", binding = "groupHolder.id") })
public class Model implements IStudentListModel
{
    private final EntityHolder<Group> groupHolder = new EntityHolder<Group>();
    private IMultiSelectModel studentCustomStateCIModel;
    private IDataSettings settings;

    @Override
    public List<IdentifiableWrapper> getStudentStatusOptionList()
    {
        return STUDENT_STATUS_OPTION_LIST;
    }

    public EntityHolder<Group> getGroupHolder() { return this.groupHolder; }

    public Group getGroup() { return this.getGroupHolder().getValue(); }

    private List<BlockGridWrapper> blockList = Collections.emptyList();

    private BlockGridWrapper block;

    public BlockGridWrapper getBlock()
    {
        return this.block;
    }

    public void setBlock(final BlockGridWrapper block)
    {
        this.block = block;
    }

    public List<BlockGridWrapper> getBlockList()
    {
        return this.blockList;
    }

    public void setBlockList(final List<BlockGridWrapper> blockList)
    {
        this.blockList = blockList;
    }

    public String getPermissionKeyChangeWP()
    {
        return "";
    }

    public String getPermissionKeyPrintWP()
    {
        return "";
    }

    public IDataSettings getSettings()
    {
        return settings;
    }

    public void setSettings(IDataSettings settings)
    {
        this.settings = settings;
    }

    public IMultiSelectModel getStudentCustomStateCIModel()
    {
        return studentCustomStateCIModel;
    }

    public void setStudentCustomStateCIModel(IMultiSelectModel studentCustomStateCIModel)
    {
        this.studentCustomStateCIModel = studentCustomStateCIModel;
    }
}
