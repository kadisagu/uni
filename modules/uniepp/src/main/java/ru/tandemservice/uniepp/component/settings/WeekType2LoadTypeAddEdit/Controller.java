package ru.tandemservice.uniepp.component.settings.WeekType2LoadTypeAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * 
 * @author nkokorina
 *
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent component)
    {
        this.getDao().update(this.getModel(component));
        this.deactivate(component);
    }
}
