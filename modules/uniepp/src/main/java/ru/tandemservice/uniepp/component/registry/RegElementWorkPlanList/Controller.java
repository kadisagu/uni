package ru.tandemservice.uniepp.component.registry.RegElementWorkPlanList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        final DynamicListDataSource<EppWorkPlanRegistryElementRow> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareDataSource(model);
        });

        dataSource.addColumn(UniEppUtils.getStateColumn(EppWorkPlanRegistryElementRow.workPlan().s()));
        dataSource.addColumn(new PublisherLinkColumn("Название", "title", EppWorkPlanRegistryElementRow.workPlan()).setClickable(true).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("УП(в)", EppWorkPlanRegistryElementRow.workPlan()+".workPlan."+EppWorkPlan.parent().educationElementSimpleTitle()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Характеристики обучения", EppWorkPlanRegistryElementRow.workPlan()+".workPlan."+EppWorkPlan.parent().eduPlanVersion().eduPlan().developCombinationTitle()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Индекс", EppWorkPlanRow.number()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Часть из РУП", EppWorkPlanRegistryElementRow.registryElementPart().number()).setClickable(false).setOrderable(false));

        model.setDataSource(dataSource);
    }
}
