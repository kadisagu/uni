package ru.tandemservice.uniepp.component.workplan.WorkPlanPartSetEdit;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.component.base.SimpleOrgUnitBasedAddEditModel;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;

import java.util.*;

/**
 * @author vdanilov
 */

public class Model extends SimpleOrgUnitBasedAddEditModel<EppWorkPlan> {

    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    public static final String RETURN_SUCCESS_PARAM = "workPlanPartSetSize";

    public static final long FIRST_OPTION_ID = 1L;
    public static final long SECOND_OPTION_ID = 2L;
    public static final long THREED_OPTION_ID = 3L;

    @SuppressWarnings("unchecked")
    public static final List<IdentifiableWrapper<IEntity>> PARTS_IN_TERM_LIST = Arrays.asList(
            new IdentifiableWrapper<>(Model.FIRST_OPTION_ID, "1"),
            new IdentifiableWrapper<>(Model.SECOND_OPTION_ID, "2"),
            new IdentifiableWrapper<>(Model.THREED_OPTION_ID, "3")
    );

    @SuppressWarnings("unchecked")
    public static IdentifiableWrapper<IEntity> getPartsInTerm(final int size) {
        return (IdentifiableWrapper<IEntity>) CollectionUtils.find(Model.PARTS_IN_TERM_LIST, object -> (size == ((IdentifiableWrapper<IEntity>)object).getId().intValue()));
    }

    public List<IdentifiableWrapper<IEntity>> getPartsInTermList() {
        return Model.PARTS_IN_TERM_LIST;
    }

    private IdentifiableWrapper<IEntity> partsInTerm;
    public void setPartsInTerm(final IdentifiableWrapper<IEntity> partsInTerm) { this.partsInTerm = partsInTerm; }
    public IdentifiableWrapper<IEntity> getPartsInTerm() { return this.partsInTerm; }

    @Override public OrgUnit getOrgUnit() { return null; }

    private final Map<Integer, String> graphWeekCodeMap = new HashMap<>();
    public Map<Integer, String> getGraphWeekCodeMap() { return this.graphWeekCodeMap; }

    private Map<Integer, EppWorkPlanPart> partMap = Collections.emptyMap();
    public Map<Integer, EppWorkPlanPart> getPartMap() { return this.partMap; }
    public void setPartMap(final Map<Integer, EppWorkPlanPart> partMap) { this.partMap = partMap; }

    private EppWorkPlanPart currentPart;
    public EppWorkPlanPart getCurrentPart() { return this.currentPart; }
    public void setCurrentPart(final EppWorkPlanPart currentPart) { this.currentPart = currentPart; }

    private final RangeSelectionWeekTypeListDataSource<EppWorkPlan> scheduleDataSource = new RangeSelectionWeekTypeListDataSource<>(new AbstractListDataSource<EppWorkPlan>() {
        @Override public void onRefresh() {}
        @Override public void onChangeOrder() {}
        @Override public List<EppWorkPlan> getEntityList() { return Collections.singletonList(Model.this.getElement()); }
        @Override public long getTotalSize() { return 1; }
        @Override public long getCountRow() { return 1; }
        @Override public long getStartRow() { return 0; }
        @Override public AbstractListDataSource<EppWorkPlan> getCopy() { return this; }
    });

    public RangeSelectionWeekTypeListDataSource<EppWorkPlan> getScheduleDataSource() { return this.scheduleDataSource; }

}
