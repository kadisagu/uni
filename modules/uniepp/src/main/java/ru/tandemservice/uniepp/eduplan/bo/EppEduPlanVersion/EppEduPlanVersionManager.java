/**
 *$Id$
 */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Alexander Shaburov
 * @since 01.02.13
 */
@Configuration
public class EppEduPlanVersionManager extends BusinessObjectManager
{
    public static EppEduPlanVersionManager instance()
    {
        return instance(EppEduPlanVersionManager.class);
    }
}
