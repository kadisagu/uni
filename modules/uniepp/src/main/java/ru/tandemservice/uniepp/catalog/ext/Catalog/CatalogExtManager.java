/* $Id$ */
package ru.tandemservice.uniepp.catalog.ext.Catalog;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import ru.tandemservice.uniepp.entity.catalog.*;

/**
 * @author azhebko
 * @since 18.04.2014
 */
@Configuration
public class CatalogExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CatalogManager catalogManager;

    @Bean
    public ItemListExtension<IDynamicCatalogDesc> dynamicCatalogsExtension()
    {
        return itemListExtension(catalogManager.dynamicCatalogsExtPoint())
                .add(StringUtils.uncapitalize(EppFControlActionType.class.getSimpleName()), EppFControlActionType.getUiDesc())
                .add(StringUtils.uncapitalize(EppIControlActionType.class.getSimpleName()), EppIControlActionType.getUiDesc())
                .add(StringUtils.uncapitalize(EppPlanStructure.class.getSimpleName()), EppPlanStructure.getUiDesc())
                .add(StringUtils.uncapitalize(EppRegistryStructure.class.getSimpleName()), EppRegistryStructure.getUiDesc())
                .add(StringUtils.uncapitalize(EppWeek.class.getSimpleName()), EppWeek.getUiDesc())
                .add(StringUtils.uncapitalize(EppWeekType.class.getSimpleName()), EppWeekType.getUiDesc())
                .add(StringUtils.uncapitalize(EppProfActivityType.class.getSimpleName()), EppProfActivityType.getUiDesc())
                .add(StringUtils.uncapitalize(EppProfessionalTask.class.getSimpleName()), EppProfessionalTask.getUiDesc())
                .add(StringUtils.uncapitalize(EppSkill.class.getSimpleName()), EppSkill.getUiDesc())
                .create();
    }
}
