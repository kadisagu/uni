package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x6x7_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.7")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppEpvRow

        // создано обязательное свойство storedIndex
        {
            // создать колонку
            if (tool.columnExists("epp_epvrow_base_t", "storedindex_p")) {
                tool.dropColumn("epp_epvrow_base_t", "storedindex_p");
            }
            tool.createColumn("epp_epvrow_base_t", new DBColumn("storedindex_p", DBType.createVarchar(255)));


            // задать значение по умолчанию
            java.lang.String defaultStoredIndex = "--fillme--";		// TODO: задайте NOT NULL значение!
            tool.executeUpdate("update epp_epvrow_base_t set storedindex_p=? where storedindex_p is null", defaultStoredIndex);

            // сделать колонку NOT NULL
            tool.setColumnNullable("epp_epvrow_base_t", "storedindex_p", false);

        }

        // создано свойство userIndex
        {
            // создать колонку
            tool.createColumn("epp_epvrow_base_t", new DBColumn("userindex_p", DBType.createVarchar(255)));

        }


    }
}