package ru.tandemservice.uniepp.entity.plan;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;

import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanGen;
import ru.tandemservice.uniepp.util.EppDevelopCombinationTitleUtil;


/**
 * Учебный план
 */
public abstract class EppEduPlan extends EppEduPlanGen implements ITitled, IHierarchyItem, IEppStateObject, INumberObject, IEppEduPlanDevelopCombination, ISecLocalEntityOwner
{
    @EntityDSLSupport
    public abstract String getTitle();
    @EntityDSLSupport
    public abstract String getTitleWithArchive();
    @EntityDSLSupport
    public abstract String getEducationElementSimpleTitle();
    public abstract String getEducationElementShortTitle();
    public abstract String getEducationElementCode();
    public abstract Class<? extends EduProgram> getEduProgramClass();

    private Collection<IEntity> _cache_sec_local_entities = null;
    @Override public Collection<IEntity> getSecLocalEntities() {
        if (null != this._cache_sec_local_entities) { return this._cache_sec_local_entities; }
        return (this._cache_sec_local_entities = IEppEduPlanDAO.instance.get().getSecLocalEntities(this));
    }

    @Override
    public INumberGenerationRule<EppEduPlan> getNumberGenerationRule() {
        return IEppEduPlanDAO.instance.get().getEduPlanNumberGenerationRule();
    }

    public EppGeneration getGeneration() {
        if (getParent() == null) return EppGeneration.GENERATION_3;
        return this.getParent().getGeneration();
    }

    @Override
    public IHierarchyItem getHierarhyParent() {
        return null;
    }

    @EntityDSLSupport(parts = { EppEduPlanGen.L_PROGRAM_FORM + ".title", EppEduPlanGen.L_DEVELOP_CONDITION + ".title" })
    @Override
    public String getDevelopFormCondition() {
        return this.getProgramForm().getTitle() + " / " + this.getDevelopCondition().getTitle();
    }

    @EntityDSLSupport(parts = { EppEduPlanGen.L_PROGRAM_FORM + ".title", EppEduPlanGen.L_DEVELOP_CONDITION + ".title",  EppEduPlanGen.L_PROGRAM_TRAIT + ".title", EppEduPlanGen.L_DEVELOP_GRID + ".title" })
    @Override
    public String getDevelopCombinationTitle() {
        return EppDevelopCombinationTitleUtil.getTitle(this);
    }

    @EntityDSLSupport(parts = { EppEduPlanGen.P_EDU_START_YEAR, EppEduPlanGen.P_EDU_END_YEAR })
    @Override
    public String getYearsString() {
        final Integer startYear = this.getEduStartYear();
        final Integer endYear = this.getEduEndYear();
        return startYear + " - " + (endYear == null ? "н.вр." : endYear);
    }

    @Override
    public EduProgramOrientation getOrientation() {
        return null; // for override
    }

    public abstract String getInfoBlockPageName();

    public String getProgramTraitTitleNullSafe()
    {
        if (getProgramTrait() == null) {
            return "Без особенностей";
        }
        return getProgramTrait().getTitle();
    }
}
