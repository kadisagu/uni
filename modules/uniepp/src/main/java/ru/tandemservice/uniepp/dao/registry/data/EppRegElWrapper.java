package ru.tandemservice.uniepp.dao.registry.data;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.registry.data.EppRegElementKeyGenerator.KeyGenRule;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public class EppRegElWrapper extends EppBaseRegElWrapper<EppRegistryElement> implements IEppRegElWrapper
{
    private List<EppGroupType> splitGroupTypes;
    public EppRegElWrapper(final EppRegistryElement item, final Map<String, Long> loadMap, final List<EppGroupType> splitGroupTypes) {
        super(item);

        // Все виды нагрузки есть в самом элементе реестра.
        // Складываем эти значения сразу в кэш, иначе они начнут доставаться и суммировать по дочерним элементам (частям)
        final Map<String, Long> hierarchicalLoadMap = this.getHierarchicalLoadMap();
        for (Map.Entry<String, EppLoadType> loadType : EppEduPlanVersionDataDAO.getLoadTypeMap().entrySet())
        {
            final String fullCode = loadType.getKey();
            final Long value = loadMap.get(fullCode);
            hierarchicalLoadMap.put(fullCode, value != null ? value : 0L);
        }
        // Самостоятельная без контроля для элемента реестра равна самостоятельной, т.к. пока что часов на контроль нет в элементе реестра
        hierarchicalLoadMap.put(EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL, hierarchicalLoadMap.get(EppELoadType.FULL_CODE_SELFWORK));
        this.splitGroupTypes = splitGroupTypes;
    }

    private final Map<Integer, EppRegElPartWrapper> partMap = new TreeMap<>();

    @Override public Map<Integer, IEppRegElPartWrapper> getPartMap() { return (Map)this.partMap; }
    @Override protected Collection<? extends EppBaseRegElWrapper> getChildren() { return this.partMap.values(); }
    @Override public EppState getState() { return this.getItem().getState(); }

    @Override
    public EppRegistryStructure getRegistryElementType()
    {
        return getItem().getParent();
    }

    @Override
    public String generateKey(KeyGenRule rule)
    {
        return EppRegElementKeyGenerator.generate(this, rule);
    }

    @Override
    public OrgUnit getOwner()
    {
        return getItem().getOwner();
    }

    @Override
    public long getLoadSize()
    {
        return getItem().getSize();
    }

    @Override
    public double getChildrenLoadAsDouble(String loadFullCode)
    {
        long result = 0L;
        for (final EppBaseRegElWrapper child : this.getChildren()) {
            result += child.load(loadFullCode);
        }
        return UniEppUtils.wrap(result);
    }

    @Override
    public long getLabor()
    {
        return getItem().getLabor();
    }

    @Override
    public long getWeeks()
    {
        final EppRegistryElement item = getItem();
        if (item instanceof EppRegistryAction) {
            return ((EppRegistryAction) item).getWeeks();
        }
        return 0L;
    }

    @Override
    public EppEduGroupSplitVariant getEduGroupSplitVariant() {
        return getItem().getEduGroupSplitVariant();
    }

    @Override
    public boolean isEduGroupSplitByType() {
        return getItem().isEduGroupSplitByType();
    }

    @Override
    public List<EppGroupType> getSplitGroupTypes() {
        return splitGroupTypes;
    }
}