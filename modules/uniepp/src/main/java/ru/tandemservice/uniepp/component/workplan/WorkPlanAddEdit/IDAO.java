package ru.tandemservice.uniepp.component.workplan.WorkPlanAddEdit;

import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * 
 * @author nkokorina
 * @since 16.03.2010
 */

public interface IDAO extends IPrepareable<Model>
{
    void save(Model model);

    void validate(Model model, ErrorCollector errorCollector);

    void onChangeEppYear(Model model);
}
