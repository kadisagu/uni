package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;

import ru.tandemservice.uni.util.AddEditResult;


/**
 * @author vdanilov
 */

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    @Override public void onRenderComponent(final IBusinessComponent component) {
        ContextLocal.beginPageTitlePart(this.getModel(component).getElement().getId() == null ? "Добавление версии УП" : "Редактирование версии УП");
    }


    public void onClickApply(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        AddEditResult.saveAndDeactivate(component, model.getElement(), () -> Controller.this.getDao().save(model));
    }

}
