package ru.tandemservice.uniepp.dao.eduStd.data;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import ru.tandemservice.uniepp.dao.index.EppIndexedRowWrapper;
import ru.tandemservice.uniepp.dao.index.IEppIndexedRowWrapper;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;
import ru.tandemservice.uniepp.entity.std.data.IEppStdRow;

import java.util.*;

/**
 * @author vdanilov
 */
public class EppStdRowWrapper extends EppIndexedRowWrapper<IEppStdRow> implements IEppStdRowWrapper, IEppIndexedRowWrapper
{

    @Override
    public String getUuid()
    {
        return this.getRow().getUuid();
    }

    protected static final Logger logger = Logger.getLogger(EppStdRowWrapper.class);

    private final Set<EppStateEduStandardSkill> skillSet = new HashSet<>();

    public Set<EppStateEduStandardSkill> getSkillSet()
    {
        return this.skillSet;
    }

    @Override
    public EppStateEduStandardBlock getOwner()
    {
        return this.row.getOwner();
    }

    @Override
    public boolean isStructureElement()
    {
        return this.row.isStructureElement();
    }

    @Override
    protected String getIndexInternal()
    {
        return this.row.getOwner().getRule().getIndex(this);
    }

    @Override
    protected int getSequenceNumberInternal()
    {
        return this.std.getRecordHierarchyIndex(this);
    }

    private final EppStdRowWrapper parent;

    @Override
    public EppStdRowWrapper getHierarhyParent()
    {
        return this.parent;
    }

    private final EppStdWrapper std;

    @Override
    public IEppStdWrapper getEduStd()
    {
        return this.std;
    }

    /* список дочерних элементов */
    private final List<EppStdRowWrapper> childRows = new ArrayList<>(4);

    public List<EppStdRowWrapper> getChildRows()
    {
        return this.childRows;
    }

    public EppStdRowWrapper(final EppStdWrapper std, final EppStdRowWrapper parent, final IEppStdRow row)
    {
        super(row);
        this.std = std;
        this.parent = parent;
    }

    @Override
    public Map<String, Boolean> getObligatoryMap()
    {
        return new LinkedHashMap<>();
    }

    @Override
    public String getTotalSize()
    {
        return this.row.getTotalSize();
    }

    @Override
    public String getTotalLabor()
    {
        return this.row.getTotalLabor();
    }

    @Override
    public String getCompetenciesCodes()
    {
        return CollectionFormatter.COLLECTION_FORMATTER.format(CollectionUtils.collect(this.getSkillSet(), input ->
            (ITitled) ((EppStateEduStandardSkill) input)::getCode
        ));
    }

    @Override
    public Boolean getUsedInActions()
    {
        return this.row.getUsedInActions();
    }

    @Override
    public Boolean getUsedInLoad()
    {
        return this.row.getUsedInLoad();
    }

}
