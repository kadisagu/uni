/* $Id$ */
package ru.tandemservice.uniepp.dao.registry.data;

import com.google.common.collect.ImmutableMap;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;

import java.util.Map;

/**
 * Враппер для части элемента реестра, созданный на основе семестра строки УПв.
 * Большая часть методов пока не поддерживается за ненадобностью.
 *
 * Условно не мутабельный.
 *
 * @author Nikolay Fedorovskih
 * @since 12.05.2015
 */
public class EppEpvRegElPartWrapper implements IEppRegElPartWrapper
{
    private final int number;
    private final int term;
    private final EppEpvRegElWrapper parent;
    private final Map<Integer, IEppRegElPartModuleWrapper> moduleMap;

    public EppEpvRegElPartWrapper(int number, int term, EppEpvRegElWrapper parent)
    {
        this.number = number;
        this.term = term;
        this.parent = parent;
        this.moduleMap = ImmutableMap.<Integer, IEppRegElPartModuleWrapper>of(1, new EppEpvRegElPartModuleWrapper(this, 1));
    }

    public EppEpvRegElWrapper getParentWrapper()
    {
        return parent;
    }

    @Override
    public int getNumber()
    {
        return this.number;
    }

    @Override
    public Map<Integer, IEppRegElPartModuleWrapper> getModuleMap()
    {
        return this.moduleMap;
    }

    @Override
    public double getLoadAsDouble(String loadFullCode)
    {
        return Math.max(0d, this.parent.getEpvRowWrapper().getTotalLoad(this.term, loadFullCode));
    }

    @Override
    public int getActionSize(String actionFullCode)
    {
        return this.parent.getEpvRowWrapper().getActionSize(this.term, actionFullCode);
    }

    @Override
    public String getGradeScale(String fullCode)
    {
        return null; // null == default
    }

    @Override public boolean hasActions4Groups(String groupCode) { throw new UnsupportedOperationException(); }
    @Override public EppRegistryElementPart getItem() { throw new UnsupportedOperationException(); }
    @Override public String getGradeScale4Group(String groupCode) throws IllegalStateException { throw new UnsupportedOperationException(); }
    @Override public boolean isMultipleActionsForGroupError() { throw new UnsupportedOperationException(); }
    @Override public Long getId() { throw new UnsupportedOperationException(); }
    @Override public void setId(Long id) { throw new UnsupportedOperationException(); }
    @Override public Object getProperty(Object propertyPath) { throw new UnsupportedOperationException(); }
    @Override public void setProperty(String propertyPath, Object propertyValue) { throw new UnsupportedOperationException(); }
    @Override public String getTitle() { throw new UnsupportedOperationException(); }
    @Override public String getLoadAsString() { throw new UnsupportedOperationException(); }
    @Override public String getActionsAsString() { throw new UnsupportedOperationException(); }

    @Override
    public boolean hasActions4ControlActionType(EppFControlActionType actionType)
    {
        return false;
    }
}