package ru.tandemservice.uniepp.component.registry.ModuleRegistry.Pub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.SafeSimpleColumn;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction;
import ru.tandemservice.uniepp.ui.EduPlanVersionBlockDataSourceGenerator;

/**
 * @author iolshvang
 * @since 03.08.11 19:09
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        final DynamicListDataSource<EppRegistryModuleALoad> loadTypeDataSource = UniBaseUtils.createDataSource(component, this.getDao());
        loadTypeDataSource.addColumn(new SimpleColumn("Вид аудиторной нагрузки", EppRegistryModuleALoad.loadType().title()).setOrderable(false));
        loadTypeDataSource.addColumn(new SimpleColumn("Число часов", EppRegistryModuleALoad.P_LOAD + "AsDouble", UniEppUtils.LOAD_FORMATTER).setOrderable(false));
        model.setLoadTypeDataSource(loadTypeDataSource);

        final DynamicListDataSource<EppRegistryModuleIControlAction> controlActionDataSource = UniBaseUtils.createDataSource(component, this.getDao());
        controlActionDataSource.addColumn(new SimpleColumn("Тип контрольного мероприятия", EppRegistryModuleIControlAction.controlAction().title()).setOrderable(false));
        controlActionDataSource.addColumn(new SimpleColumn("Количество", EppRegistryModuleIControlAction.amount()).setOrderable(false));
        model.setControlActionDataSource(controlActionDataSource);


        final DynamicListDataSource<EppRegistryElementPartModule> disciplineDataSource = UniBaseUtils.createDataSource(component, this.getDao());
        disciplineDataSource.addColumn(UniEppUtils.getStateColumn());
        disciplineDataSource.addColumn(new PublisherLinkColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__TITLE, EppRegistryElement.educationElementTitle(), EppRegistryElementPartModule.part().registryElement()));
        disciplineDataSource.addColumn(new SimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__OWNER, EppRegistryElementPartModule.module().owner().fullTitle()).setClickable(false).setOrderable(false));

        final HeadColumn ha = new HeadColumn("aload", "Аудиторных");
        for (final EppALoadType t : this.getDao().getCatalogItemListOrderByCode(EppALoadType.class)) {
            ha.addColumn(new SimpleColumn(t.getShortTitle(), t.getFullCode(), UniEppUtils.LOAD_FORMATTER).setWidth(1).setVerticalHeader(true).setClickable(false).setOrderable(false));
        }

        final HeadColumn load = new HeadColumn("load", "Нагрузка");
        load.addColumn(new SimpleColumn("Часов (всего)", EppRegistryElementPartModule.part().registryElement().size().s()+ "AsDouble", UniEppUtils.LOAD_FORMATTER).setWidth(1).setVerticalHeader(true).setClickable(false).setOrderable(false));
        load.addColumn(new SimpleColumn("Трудоемкость", EppRegistryElementPartModule.part().registryElement().labor().s()+ "AsDouble", UniEppUtils.LOAD_FORMATTER).setWidth(1).setVerticalHeader(true).setClickable(false).setOrderable(false));


        load.addColumn(ha.setWidth(1).setHeaderAlign("center"));
        disciplineDataSource.addColumn(load.setWidth(1).setHeaderAlign("center"));

        final HeadColumn hc = new HeadColumn("caction", "Контроль");
        for (final EppFControlActionType t : this.getDao().getCatalogItemListOrderByCode(EppFControlActionType.class)) {
            hc.addColumn(new SafeSimpleColumn(t.getShortTitle(), t.getFullCode()).setWidth(1).setVerticalHeader(true).setClickable(false).setOrderable(false));
        }
        if (hc.getColumns().size() > 0) {
            disciplineDataSource.addColumn(hc.setWidth(1).setHeaderAlign("center"));
        }
        model.setDisciplineDataSource(disciplineDataSource);

        this.getDao().prepare(model);
    }


    public void onClickEditModule(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(ru.tandemservice.uniepp.component.registry.ModuleRegistry.AddEdit.Model.class.getPackage().getName(), new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getModule().getId())));
    }

    public void onClickDeleteModule(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        UniDaoFacade.getCoreDao().delete(model.getModule().getId());
    }

    public void onClickSetShared(final IBusinessComponent component)
    {
        this.getDao().setShared(this.getModel(component));
    }

}
