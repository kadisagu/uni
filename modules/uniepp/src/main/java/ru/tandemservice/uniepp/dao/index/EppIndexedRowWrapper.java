package ru.tandemservice.uniepp.dao.index;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;

/**
 * @author vdanilov
 */
public abstract class EppIndexedRowWrapper<T extends IEppIndexedRow> extends EntityBase implements IEppIndexedRowWrapper {
    protected final T row;
    public EppIndexedRowWrapper(final T row) {
        this.row = row;
    }

    @Override public T getRow() { return this.row; }
    @Override public Long getId() { return this.row.getId(); }

    @Override public String getDisplayableTitle() { return this.row.getDisplayableTitle(); }
    @Override public String getTitle() { return this.getDisplayableTitle(); }

    protected abstract int getSequenceNumberInternal();

    private Integer _cache_seq_number;
    @Override
    public int getSequenceNumber() {
        if (null == this._cache_seq_number) {
            return (this._cache_seq_number = 1 + this.getSequenceNumberInternal());
        }
        return this._cache_seq_number;
    }

    protected abstract String getIndexInternal();

    private String _cache_index = null;
    @Override
    public String getIndex() {
        if (null == this._cache_index) {
            return (this._cache_index = this.getIndexInternal());
        }
        return this._cache_index;
    }

    @Override public String getHierarhyPath() {
        final IEppIndexedRowWrapper hierarhyParent = (IEppIndexedRowWrapper)this.getHierarhyParent();
        if (null != hierarhyParent) {
            return (hierarhyParent.getHierarhyPath() + "\n" + this.getTitle());
        }
        return this.getTitle();
    }

    @Override public String getHierarhyIndex() {
        final IEppIndexedRowWrapper hierarhyParent = (IEppIndexedRowWrapper)this.getHierarhyParent();
        if (null != hierarhyParent) {
            return (hierarhyParent.getHierarhyIndex() + "." + StringUtils.leftPad(String.valueOf(this.getSequenceNumber()), 2, '0'));
        }
        return StringUtils.leftPad(String.valueOf(this.getSequenceNumber()), 2, '0');
    }

    @Override public String toString() {
        return ClassUtils.getUserClass(this.row) +"@"+Long.toHexString(this.getRow().getId()) + " " + this.getTitleWithIndex();
    }

    @Override public String getTitleWithIndex() {
        final String index = StringUtils.trimToNull(this.getIndex());
        if (null == index) { return this.getTitle(); }
        return index + ". " + this.getTitle();
    }

    @Override public String getQualificationCode() {
        return this.row.getQualificationCode();
    }

    @Override public EppGeneration getGeneration() {
        return this.row.getGeneration();
    }

    @Override public String getSelfIndexPart() {
        return StringUtils.trimToEmpty(this.row.getSelfIndexPart());
    }


}
