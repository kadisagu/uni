package ru.tandemservice.uniepp.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.IFormatter;

/**
 * @author vdanilov
 */
public interface IEppNumberRow {

    IFormatter<IEppNumberRow> NUMBER_FORMATTER = row -> {
        final int NUMBER_PADDING = 2;
        return StringUtils.leftPad(row.getNumber(), NUMBER_PADDING, '0');
    };

    String getNumber();
}
