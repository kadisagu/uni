package ru.tandemservice.uniepp.base.bo.EppContract.logic;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Session;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes;
import org.tandemframework.shared.commonbase.catalog.entity.gen.CurrencyGen;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.base.bo.EppContract.EppContractManager;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Фабрика создания договора на обучение
 * 
 * @author vdanilov
 * @deprecated use EduProgramContractObjectFactory (договоры на УП(в) будут переведены на договоры на ОП)
 */
@Deprecated
@SuppressWarnings("deprecation")
public abstract class EppContractObjectFactory<S> {

    protected String getLockName() {
        return CtrContractObject.ENTITY_CLASS;
    }

    protected Session prepareSession()
    {
        if (!TransactionSynchronizationManager.isActualTransactionActive()) { throw new IllegalStateException("no-transaction"); }
        if (TransactionSynchronizationManager.isCurrentTransactionReadOnly()) { throw new IllegalStateException("transaction-is-read-only"); }

        final Session session = DataAccessServices.dao().getComponentSession();
        NamedSyncInTransactionCheckLocker.register(session, this.getLockName());
        return session;
    }

    protected final Session session = this.prepareSession();
    public Session getSession() { return this.session; }

    protected Currency getDefaultCurrency(final CtrContractVersion contractVersion) {
        return DataAccessServices.dao().getByNaturalId(new CurrencyGen.NaturalId(CurrencyCodes.RUB));
    }

    protected abstract OrgUnit getCtrContractObjectOrgUnit();
    protected abstract String getCtrContractObjectNumber();
    protected abstract CtrContractType getCtrContractObjectType();
    protected abstract CtrContractVersionCreateData getVersionCreateData();

    protected abstract ContactorPerson getRequestor();
    protected abstract Collection<S> getStudentList();

    protected abstract String getStudentTitle(S student);
    protected abstract ContactorPerson getStudentContactPersion(S student);
    protected abstract Date getStudentEnrollmentDate(S student);
    protected abstract EppEduPlanVersionBlock getStudentEduPlanVersionBlock(S student);
    protected abstract EducationOrgUnit getStudentEducationOrgUnit(S student);
    protected abstract StudentCategory getStudentCategory(S student);

    protected abstract Collection<CtrPriceElementCost> getEppCtrEducationPromiceCostList(S student, EppCtrEducationPromice eduPromice, EppEduPlanVersionBlock epvBlock, CtrContractVersionContractor paymentSource, Currency defaultCurrency);

    public CtrContractVersion buildContractObject()
    {
        // формируем договор
        final CtrContractObject contractObject = new CtrContractObject();
        contractObject.setOrgUnit(this.getCtrContractObjectOrgUnit());
        contractObject.setNumber(this.getCtrContractObjectNumber());
        contractObject.setType(this.getCtrContractObjectType());
        this.session.save(contractObject);

        // его первую печатную версию (номер будет скопирован из договора)
        final CtrContractVersion contractVersion = CtrContractVersionManager.instance().dao().doCreateNewVersion(contractObject, getVersionCreateData(), null);
        this.session.saveOrUpdate(contractVersion);

        // справочник контрагентов (автоматически добавляет в базу)
        final Map<ContactorPerson, CtrContractVersionContractor> contractorMap = SafeMap.get(key -> {
            final CtrContractVersionContractor c = new CtrContractVersionContractor(contractVersion, key);
            EppContractObjectFactory.this.session.save(c);
            return c;
        });

        // представилети для заключения договоров (на указанном подразделении)
        final Collection<? extends ContactorPerson> academyPresenters = this.getAcademyPresenters(contractObject);

        // добавляем всех (если что - удалят из договора)
        for (final ContactorPerson presenter: academyPresenters) {
            final CtrContractVersionContractor p = contractorMap.get(presenter);
            p.setRequireSignature(false); // по умолчанию НЕ подписывают
            this.session.saveOrUpdate(p);
        }

        // заявитель
        final CtrContractVersionContractor requestor = contractorMap.get(this.getRequestor());
        requestor.setRequireSignature(true); // этот подписывает, если что - поправят либо у себя в коде, либо на форме
        this.session.update(requestor);

        // первый представитель ВУЗа
        final CtrContractVersionContractor presenter = contractorMap.get(this.getPresenter(academyPresenters));
        presenter.setRequireSignature(true); // этот подписывает, если что - поправят либо у себя в коде, либо на форме
        this.session.update(presenter);

        // список тех. кого необходимо обучить
        final Collection<S> students = this.getStudentList();
        if (students.isEmpty()) { throw new ApplicationException(this.error_noStudents()); }

        // список платежей за обучение { grid -> { payment.source -> { stage -> promice }}}
        final Map<Long, Map<CtrContractVersionContractor, Map<String, CtrPaymentPromice>>> paymentMap = new HashMap<>(2);

        // валюта договора по умолчанию
        final Currency defaultCurrency = this.getDefaultCurrency(contractVersion);

        // поехали
        for (final S student: students)
        {
            final CtrContractVersionContractor listener = contractorMap.get(this.getStudentContactPersion(student));
            if (null == listener) { throw new ApplicationException(this.error_student_noListener(student)); }

            final Date enrollmentDate = this.getStudentEnrollmentDate(student);
            if (null == enrollmentDate) { throw new ApplicationException(this.error_student_noEnrollmentDate(student)); }

            final StudentCategory category = this.getStudentCategory(student);
            if (null == category) { throw new ApplicationException(this.error_student_noStudentCategory(student)); }

            final EppEduPlanVersionBlock epvBlock = this.getStudentEduPlanVersionBlock(student);
            if (null == epvBlock) { throw new ApplicationException(this.error_student_noEppEduPlanVersionBlock(student)); }

            final EducationOrgUnit eduOu = this.getStudentEducationOrgUnit(student);
            if (null == eduOu) { throw new ApplicationException(this.error_student_noEducationOrgUnit(student)); }

            // ВУЗ обязается зачислить СЛУШАТЕЛЯ на НПП, указать УП(в)
            EppCtrEducationPromice eduPromice = new EppCtrEducationPromice();
            eduPromice.setDeadlineDate(enrollmentDate);
            eduPromice.setSrc(presenter);
            eduPromice.setDst(listener);
            eduPromice.setEduPlanVersion(epvBlock.getEduPlanVersion());
            eduPromice.setEducationOrgUnit(eduOu);
            eduPromice.setStudentCategory(category);
            this.session.save(eduPromice = this.processEduPromice(student, eduPromice, epvBlock));

            if (!isCreatePaymentPromices())
                continue;

            // цена, оплата (за обучение)
            final CtrContractVersionContractor paymentSource = this.getPaymentSource(student, eduPromice, epvBlock, requestor, contractorMap);
            final Collection<CtrPriceElementCost> costList = this.getEppCtrEducationPromiceCostList(student, eduPromice, epvBlock, paymentSource, defaultCurrency);
            if (costList.size() > 0)
            {
                // дата обязательства - будем увеличивать на день, начиная с даты зачисления для обязательств оплаты
                Date deadlineDate = eduPromice.getDeadlineDate();

                // если есть цены, формируем по каждой цене элемент
                for (final CtrPriceElementCost cost: costList)
                {
                    final Map<CtrContractVersionContractor, Map<String, CtrPaymentPromice>> gridMap = SafeMap.safeGet(paymentMap, cost.getPaymentGrid().getId(), HashMap.class);
                    final Map<String, CtrPaymentPromice> stageMap = SafeMap.safeGet(gridMap, paymentSource, HashMap.class);

                    // формируем список платежей (ЗАЯВИТЕЛЬ обязуется ВУЗу оплатить сумму за обучение)
                    final List<CtrPriceElementCostStage> stages = this.getEppCtrEducationPromiceCostStages(cost, student, eduPromice, epvBlock);
                    for (final CtrPriceElementCostStage stage: stages) {
                        final long costAsLong = this.getStudentPaymentStageCostAsLong(stage, student, eduPromice);
                        if (costAsLong > 0) {
                            final String stageName = this.getStudentPaymentStageName(stage, student, eduPromice);
                            CtrPaymentPromice payPromice = stageMap.get(stageName);
                            if (null == payPromice)
                            {
                                deadlineDate = DateUtils.addDays(deadlineDate, 1);
                                if ((null != stage.getDeadlineDate()) && deadlineDate.before(stage.getDeadlineDate())) {
                                    // если у цене дата указана и она позже, то выбираем ее
                                    // это соответсвует ситуации, что начиная с даты зачисления мы увеличиваем дату по каждому этапу
                                    // до тех пор пока она раньше, чем дата этапа (студент поступил в середине года - он должен заплатить за половину года позже)
                                    // потом берем даты из этапов (за оставшиеся этапы он платит вовремя)
                                    deadlineDate = stage.getDeadlineDate();
                                }

                                stageMap.put(stageName, payPromice = new CtrPaymentPromice());
                                payPromice.setCurrency(cost.getCurrency());
                                payPromice.setDeadlineDate(deadlineDate);
                                payPromice.setSrc(paymentSource);
                                payPromice.setDst(presenter);
                                payPromice.setStage(this.getStudentPaymentStageTitle(stage, student, eduPromice));
                            }
                            payPromice.setCostAsLong(payPromice.getCostAsLong() + costAsLong);
                        }
                    }
                }
            }
            else
            {
                // если цен нет - делаем запись с нулевой стоимостью (чтобы пользователь сам ввел цену)
                final Map<CtrContractVersionContractor, Map<String, CtrPaymentPromice>> gridMap = SafeMap.safeGet(paymentMap, 0L, HashMap.class);
                final Map<String, CtrPaymentPromice> stageMap = SafeMap.safeGet(gridMap, paymentSource, HashMap.class);
                final String stageName = this.getStudentPaymentNoStageName(student, eduPromice);

                CtrPaymentPromice payPromice = stageMap.get(stageName);
                if (null == payPromice) {
                    stageMap.put(stageName, payPromice = new CtrPaymentPromice());
                    payPromice.setCurrency(defaultCurrency);
                    payPromice.setDeadlineDate(eduPromice.getDeadlineDate());
                    payPromice.setSrc(paymentSource);
                    payPromice.setDst(presenter);
                    payPromice.setStage(this.getStudentPaymentNoStageTitle(student, eduPromice));
                }
            }
        }

        if (!isCreatePaymentPromices())
            return contractVersion;

        // сохраняем платежы
        for (final Map<CtrContractVersionContractor, Map<String, CtrPaymentPromice>> gridMap: paymentMap.values()) {
            for (final Map.Entry<CtrContractVersionContractor, Map<String, CtrPaymentPromice>> e: gridMap.entrySet()) {
                Date deadlineDate = null;
                for (final CtrPaymentPromice payPromice: e.getValue().values()) {
                    if (payPromice.getCostAsLong() >= 0) {
                        // нулевые платежи тоже надо добавлять, они для пользователей
                        deadlineDate = ((null == deadlineDate) || deadlineDate.after(payPromice.getDeadlineDate())) ? payPromice.getDeadlineDate() : deadlineDate;
                        this.session.save(this.processPayPromice(payPromice));
                    }
                }

                if ((null != deadlineDate) && this.isNeedGarantorPromice(contractVersion, e.getKey()) && !requestor.equals(e.getKey())) {
                    final CtrPaymentGarantorPromice garantor = new CtrPaymentGarantorPromice();
                    garantor.setDeadlineDate(deadlineDate);
                    garantor.setSrc(requestor);
                    garantor.setDst(e.getKey());
                    this.session.save(garantor);
                }
            }
        }

        return contractVersion;
    }

    protected boolean isNeedGarantorPromice(final CtrContractVersion contractVersion, final CtrContractVersionContractor paymentSource) {
        return false;
    }

    protected Collection<? extends ContactorPerson> getAcademyPresenters(final CtrContractObject contractObject) {
        return EppContractManager.instance().dao().getAcademyPresenters(contractObject.getOrgUnit());
    }

    protected long getStudentPaymentStageCostAsLong(final CtrPriceElementCostStage stage, final S student, final EppCtrEducationPromice eduPromice) {
        return stage.getStageCostAsLong();
    }

    protected String getStudentPaymentStageTitle(final CtrPriceElementCostStage stage, final S student, final EppCtrEducationPromice eduPromice) {
        return stage.getTitle();
    }

    protected String getStudentPaymentStageName(final CtrPriceElementCostStage stage, final S student, final EppCtrEducationPromice eduPromice) {
        return stage.getStageUniqueCode();
    }

    protected String getStudentPaymentNoStageTitle(final S student, final EppCtrEducationPromice eduPromice)
    {
        return
        StringUtils.trimToEmpty(this.getStudentTitle(student)) + " " +
        getPaymentNoStageTitle(eduPromice.getEduPlanVersion(), eduPromice.getEducationOrgUnit().getEducationLevelHighSchool());

    }

    public static String getPaymentNoStageTitle(EppEduPlanVersion epv, EducationLevelsHighSchool eduHs)
    {
        final String codePrefix = StringUtils.trimToNull(eduHs.getEducationLevel().getTitleCodePrefix());
        final StructureEducationLevels levelType = eduHs.getEducationLevel().getLevelType();
        //final String levelTypeShortTitle = StringUtils.trimToNull(levelType.getShortTitle());
        //final String levelTypeTitle = (null != levelTypeShortTitle ? levelTypeShortTitle : UniStringUtils.minimize(levelType.getTitle(), 20));

        return "№" + UniStringUtils.minimize(epv.getFullNumber(), 20) +
        (null == codePrefix ? "" : " " + codePrefix) +
        " " + UniStringUtils.minimize(eduHs.getTitle(), 40) +
        " (" + levelType.getRoot().getShortTitle() + ")" +
        " " + epv.getEduPlan().getDevelopCombinationTitle();
    }

    protected String getStudentPaymentNoStageName(final S student, final EppCtrEducationPromice eduPromice) {
        return String.valueOf(eduPromice.getId());
    }

    protected List<CtrPriceElementCostStage> getEppCtrEducationPromiceCostStages(final CtrPriceElementCost cost, final S student, final EppCtrEducationPromice eduPromice, final EppEduPlanVersionBlock epvBlock) {
        // TODO?: ICtrPriceElementCostStageFactory factory = CtrPriceManager.instance().getFactory(cost.getPaymentGrid());

        final List<CtrPriceElementCostStage> stages = DataAccessServices.dao().getCalculatedValue(session1 -> new DQLSelectBuilder()
        .fromEntity(CtrPriceElementCostStage.class, "s").column(property("s"))
        .where(eq(property("s", CtrPriceElementCostStage.L_COST), value(cost)))
        .order(property("s", CtrPriceElementCostStage.P_NUMBER))

        // выводим только листья
        .where(notExists(
            new DQLSelectBuilder()
            .fromEntity(CtrPriceElementCostStage.class, "x").column(property("x.id"))
            .where(eq(property("x", CtrPriceElementCostStage.L_PARENT), property("s")))
            .where(eq(property("x", CtrPriceElementCostStage.L_COST), property("s", CtrPriceElementCostStage.L_COST)))
            .buildQuery()
        ))
        .createStatement(session1).<CtrPriceElementCostStage>list());

        // сортируем иерархически
        Collections.sort(stages, CtrPriceElementCostStage.HIERARCHY_COMPARATOR);
        return stages;
    }

    protected ContactorPerson getPresenter(final Collection<? extends ContactorPerson> academyPresenters) {
        return academyPresenters.iterator().next();
    }

    protected CtrContractVersionContractor getPaymentSource(final S student, final EppCtrEducationPromice eduPromice, final EppEduPlanVersionBlock epvBlock, final CtrContractVersionContractor requestor, final Map<ContactorPerson, CtrContractVersionContractor> contractorMap) {
        return requestor;
    }

    protected CtrPaymentPromice processPayPromice(final CtrPaymentPromice payPromice) {
        return payPromice;
    }

    protected EppCtrEducationPromice processEduPromice(final S student, final EppCtrEducationPromice eduPromice, final EppEduPlanVersionBlock epvBlock) {
        return eduPromice;
    }

    protected boolean isCreatePaymentPromices()
    {
        return true;
    }



    protected String error_noStudents() {
        return EppContractManager.instance().getProperty("error.epp-no-students");
    }

    protected String error_student_noListener(final S student) {
        return EppContractManager.instance().getProperty("error.epp-student-no-listener", this.getStudentTitle(student));
    }

    protected String error_student_noEnrollmentDate(final S student) {
        return EppContractManager.instance().getProperty("error.epp-student-no-enrollment-date", this.getStudentTitle(student));
    }

    protected String error_student_noStudentCategory(final S student) {
        return EppContractManager.instance().getProperty("error.epp-student-no-student-category", this.getStudentTitle(student));
    }

    protected String error_student_noEppEduPlanVersionBlock(final S student) {
        return EppContractManager.instance().getProperty("error.epp-student-no-edu-plan-version-block", this.getStudentTitle(student));
    }

    protected String error_student_noEducationOrgUnit(final S student) {
        return EppContractManager.instance().getProperty("error.epp-student-no-edu-org-unit", this.getStudentTitle(student));
    }


}
