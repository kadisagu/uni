/* $Id$ */
package ru.tandemservice.uniepp.component.registry.RegElementEduPlanRowVariations;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.FormatterColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uni.dao.UniBaseDao;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author oleyba
 * @since 5/18/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final Long eppRegElementId = model.getEppRegElementId();
        model.setElement(this.get(EppRegistryElement.class, model.getEppRegElementId()));
        final List<EppControlActionType> controlActionTypes = this.getCatalogItemListOrderByCode(EppControlActionType.class);

        final Session session = this.getSession();

        // найдем все версии УП, где есть эта дисциплина
        final Set<Long> blockIds = new HashSet<Long>();

        blockIds.addAll(
            new DQLSelectBuilder()
            .fromEntity(EppEpvRegistryRow.class, "row")
            .column(property(EppEpvRegistryRow.owner().id().fromAlias("row")))
            .where(eq(property(EppEpvRegistryRow.registryElement().id().fromAlias("row")), value(eppRegElementId)))
            .createStatement(session).<Long>list()
        );

        // получим строки этих версий в готовом виде
        final Map<Long, IEppEpvBlockWrapper> blockWrapperMap = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(blockIds, true);

        // вычислим все семестры, в каких она читалась
        int termCount = 0;
        //Set<Integer> terms = new HashSet<Integer>();
        for (final IEppEpvBlockWrapper blockWrapper: blockWrapperMap.values())
        {
            for (final IEppEpvRowWrapper rowWrapper : blockWrapper.getRowMap().values())
            {
                if (!this.checkDisc(eppRegElementId, rowWrapper)) {
                    continue;
                }
                termCount = Math.max(termCount, rowWrapper.getActiveTermSet().size());
                //terms.addAll(rowWrapper.getActiveTermSet());
            }
        }

        // создадим врапперы для отображения
        final Map<String, Model.RowWrapper> wrapperMap = new HashMap<String, Model.RowWrapper>();
        for (final IEppEpvBlockWrapper blockWrapper: blockWrapperMap.values())
        {
            for (final IEppEpvRowWrapper rowWrapper : blockWrapper.getRowMap().values())
            {
                if (!this.checkDisc(eppRegElementId, rowWrapper)) {
                    continue;
                }
                final String complexKey = this.getComplexKey(termCount, rowWrapper);
                Model.RowWrapper wrapper = wrapperMap.get(complexKey);
                if (null == wrapper)
                {
                    wrapper = new Model.RowWrapper(rowWrapper.getId(), "");
                    wrapperMap.put(complexKey, wrapper);
                    // будем заполнять враппер по порядковым номерам семестров чтения дисциплины
                    // их настоящие номера не влияют на схему чтения, только порядок
                    final List<Integer> activeTermSet = new ArrayList<Integer>(rowWrapper.getActiveTermSet());
                    Collections.sort(activeTermSet);
                    int termNumber = 1;
                    for (final Integer term : activeTermSet)
                    {
                        wrapper.getKeyMap().put(termNumber, rowWrapper.getTermKey(term));
                        wrapper.getColumnContentMap().put(termNumber, this.getColumnContent(rowWrapper, term, controlActionTypes));
                        termNumber++;
                    }
                }
                if (!wrapper.getVersionList().contains(blockWrapper.getVersion())) {
                    wrapper.getVersionList().add(blockWrapper.getVersion());
                }
            }
        }

        // найдем битые строки
        final Map<Integer, String> keyByTerm = new HashMap<Integer, String>();
        model.getBrokenTerms().clear();
        final String emptyKey = "-";
        for (Integer term = 1; term <= termCount; term++)
        {
            for (final Model.RowWrapper wrapper : wrapperMap.values())
            {
                if (model.getBrokenTerms().contains(term))
                {
                    continue; // уже есть ошибка, дальше можно не проверять
                }
                final String termKey = keyByTerm.get(term);
                final String currentKey = wrapper.getKeyMap().get(term);
                if (null == termKey)
                {
                    keyByTerm.put(term, currentKey == null ? emptyKey : currentKey);
                    continue;
                }
                if (termKey.equals(emptyKey) && (currentKey == null)) {
                    continue;
                }
                if (termKey.equals(currentKey)) {
                    continue;
                }
                model.getBrokenTerms().add(term);
            }
        }

        final StaticListDataSource<Model.RowWrapper> dataSource = model.getDataSource();
        dataSource.clearColumns();
        dataSource.addColumn(new PublisherColumnBuilder("Версии", "educationElementTitle", "eduPlanVersionTab").entity("versions").build().setOrderable(false));
        for (Integer term = 1; term <= termCount; term++)
        {
            final FormatterColumn termColumn = new SimpleColumn(term + " часть", "term" + term).setFormatter(RawFormatter.INSTANCE);
            if (model.getBrokenTerms().contains(term)) {
                termColumn.setStyle("background-color: #FAB5B0");
            }
            dataSource.addColumn(termColumn.setWidth(1).setOrderable(false));
        }

        dataSource.setupRows(wrapperMap.values());

        for (final Model.RowWrapper wrapper : wrapperMap.values())
		{
            Collections.sort(wrapper.getVersionList(), Comparator.comparing(EppEduPlanVersion::getEducationElementSimpleTitle)
					.thenComparing(EppEduPlanVersion::getNumber)
					.thenComparing(EppEduPlanVersion::getId));
        }
    }

    private String getComplexKey(final int termCount, final IEppEpvRowWrapper rowWrapper)
    {
        final StringBuilder result = new StringBuilder();
        final Iterator<Integer> termIterator = rowWrapper.getActiveTermSet().iterator();
        for (Integer part = 1; part <= termCount; part++) {
            if (termIterator.hasNext()) {
                result.append(rowWrapper.getTermKey(termIterator.next())).append(".");
            } else {
                result.append(rowWrapper.getTermKey(null)).append(".");
            }
        }
        return result.toString();
    }

    private boolean checkDisc(final Long eppRegElementId, final IEppEpvRowWrapper rowWrapper)
    {
        final IEppEpvRow row = rowWrapper.getRow();
        if (row instanceof EppEpvRegistryRow) {
            final EppRegistryElement registryElement = ((EppEpvRegistryRow)row).getRegistryElement();
            return null != registryElement && eppRegElementId.equals(registryElement.getId());
        }
        return false;
    }

    private String getColumnContent(final IEppEpvRowWrapper wrapper, final Integer term, final List<EppControlActionType> controlActionTypes)
    {
        final StringBuilder result = new StringBuilder();
        // нагрузка
        result.append("<p style=\"margin:0; padding:0;\"><nobr>ауд. ");
        result.append(UniEppUtils.formatLoad(wrapper.getTotalInTermLoad(term, EppELoadType.FULL_CODE_AUDIT), false));
        result.append("ч. (");
        result.append(UniEppUtils.formatLoad(wrapper.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_LECTURES), false));
        result.append("/");
        result.append(UniEppUtils.formatLoad(wrapper.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_PRACTICE), false));
        result.append("/");
        result.append(UniEppUtils.formatLoad(wrapper.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_LABS), false));
        result.append("), сам. ");
        result.append(UniEppUtils.formatLoad(wrapper.getTotalInTermLoad(term, EppELoadType.FULL_CODE_SELFWORK), false));
        result.append("ч.");
        result.append("</nobr></p>");
        // КМ
        result.append("<p style=\"margin:0; padding:0;\"><nobr>КМ: ");
        boolean first = true;
        for (final EppControlActionType controlActionType : controlActionTypes)
        {
            final int actionSize = wrapper.getActionSize(term, controlActionType.getFullCode());
            if (actionSize == 0) {
                continue;
            }
            if (!first) {
                result.append("/");
            }
            first = false;
            result.append(controlActionType.getShortTitle());
            if (actionSize > 1) {
                result.append("-").append(actionSize);
            }
        }
        result.append("</nobr></p>");
        return result.toString();
    }
}
