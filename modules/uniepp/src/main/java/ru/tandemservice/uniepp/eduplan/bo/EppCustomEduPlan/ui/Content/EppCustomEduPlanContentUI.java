/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.Content;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableDouble;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppCustomEduPlanDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.EppCustomPlanWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppCustomPlanRowWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppCustomPlanWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.EppCustomEduPlanManager;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Nikolay Fedorovskih
 * @since 14.08.2015
 */
@State({
        @Bind(key=UIPresenter.PUBLISHER_ID, binding="holder.id", required=true),
        @Bind(key=EppCustomEduPlanContentUI.EDIT_MODE_BIND, binding="editMode"),
        @Bind(key=EppCustomEduPlanContentUI.PERMISSION_KEY_BIND, binding="permissionKey"),
        @Bind(key=EppCustomEduPlanContentUI.STUDENT_BIND, binding="student")
})
public class EppCustomEduPlanContentUI extends UIPresenter
{
    public static final String EDIT_MODE_BIND = "editMode";
    public static final String PERMISSION_KEY_BIND = "pKey";
    public static final String STUDENT_BIND = "student";

    private final EntityHolder<EppCustomEduPlan> holder = new EntityHolder<>();
    private boolean editMode;
    private EppCustomPlanWrapper planWrapper;
    private Map<Integer, DevelopGridTerm> gridTermMap;
    private Student _student;

    private Map<String, String> loadColumns;
    private Map.Entry<String, String> currentLoadColumn;
    private Collection<EppControlActionType> activeFcaList;
    private EppFControlActionType currentFCA;
    private Map.Entry<Integer, Set<IEppCustomPlanRowWrapper>> currentTermEntry;
    private IEppCustomPlanRowWrapper currentRow;
    private String permissionKey;
    private ISelectModel retakeSelectModel;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh(EppCustomEduPlan.class);

        this.planWrapper = new EppCustomPlanWrapper(getCustomPlan());
        this.retakeSelectModel = EppCustomEduPlanManager.createRetakeSelectModel();
        this.gridTermMap = IDevelopGridDAO.instance.get().getDevelopGridMap(getCustomPlan().getDevelopGrid().getId());

        this.loadColumns = ImmutableMap.<String, String>builder()
                .put(EppLoadType.FULL_CODE_LABOR, "Труд-сть")
                .put(EppLoadType.FULL_CODE_TOTAL_HOURS, "Часов (всего)")
                .put(EppALoadType.FULL_CODE_TOTAL_LECTURES, "Лекции")
                .put(EppALoadType.FULL_CODE_TOTAL_PRACTICE, "Практ. зан-я")
                .put(EppALoadType.FULL_CODE_TOTAL_LABS, "Лаб. зан-я")
                .put(EppELoadType.FULL_CODE_SELFWORK, "Самост.")
                .put(EppLoadType.FULL_CODE_CONTROL, "Контроль")
                .build();

        this.activeFcaList = IEppEduPlanVersionDataDAO.instance.get().getActiveControlActionTypes(null)
                .filter(ca -> ca instanceof EppFControlActionType)
                .map(ca -> (EppFControlActionType) ca)
                .collect(Collectors.toList());

        if (getPermissionKey() == null) {
            setPermissionKey(isEditMode() ? "edit_eppCustomEduPlan" : "view_eppCustomEduPlan");
        }
    }

    private boolean validate(boolean ... check)
    {
        final Map<Course, MutableInt> examsMap = SafeMap.get(MutableInt.class);
        final Map<Course, MutableDouble> laborMap = SafeMap.get(MutableDouble.class);

        final IEppCustomPlanWrapper planWrapper = getPlanWrapper();
        this.gridTermMap.forEach((term, developGridTerm) -> {
            final Course course = developGridTerm.getCourse();
            final MutableInt exams = examsMap.get(course);
            exams.add(planWrapper.calcFinalControlActionCountInTerm(term, EppFControlActionType.FULL_CODE_CONTROL_ACTION_EXAM));
            exams.add(planWrapper.calcFinalControlActionCountInTerm(term, EppFControlActionType.FULL_CODE_CONTROL_ACTION_EXAM_ACCUM));

            final MutableDouble laborSum = laborMap.get(course);
            laborSum.add(planWrapper.calcTotalLoadInTerm(term, EppLoadType.FULL_CODE_LABOR));
        });

        if (check[0])
        {
            // 1. В учебном году должно быть не более 20 экзаменов.
            final int maxExamCount = 20;
            final String courses = examsMap.entrySet().stream()
                    .filter(entry -> entry.getValue().longValue() > maxExamCount)
                    .map(entry -> entry.getKey().getTitle())
                    .collect(Collectors.joining(", "));

            if (StringUtils.isNotEmpty(courses)) {
                getSupport().info("На " + courses + " курс запланировано более " + maxExamCount + " экзаменов.");
            }
        }

        if (check[1])
        {
            // 2. В учебном году трудоемкость должна быть не более 75.
            final double maxLaborSize = 75d;
            final String courses = laborMap.entrySet().stream()
                    .filter(entry -> entry.getValue().doubleValue() > maxLaborSize)
                    .map(entry -> entry.getKey().getTitle())
                    .collect(Collectors.joining(", "));

            if (StringUtils.isNotEmpty(courses)) {
                getSupport().info("Нагрузка " + courses + " курса больше " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(maxLaborSize) + " зачетных единиц.");
            }
        }

        if (check[2])
        {
            // 3. Если для строки, подчиненныой ДВ, выбрано значение "Перезачет и переаттестация",
            // то проверять, что количество таких выбранных строк меньше либо равно количеству дисциплин из ДВ.
            for (Map.Entry<Integer, Collection<IEppCustomPlanRowWrapper>> entry : getPlanWrapper().getRowMap().entrySet()) {

                final Multimap<EppEpvGroupImRow, String> selectedRowMap = ArrayListMultimap.create();
                entry.getValue().stream()
                        .filter(row -> row.getNeedRetake() != null)
                        .forEach(row -> {
                            final EppEpvGroupImRow selectedParentGroup = row.getSourceRow().getParentSelectedGroupRow();
                            if (selectedParentGroup != null) {
                                selectedRowMap.put(selectedParentGroup, getSelectId(row));
                            }
                        });

                for (Map.Entry<EppEpvGroupImRow, Collection<String>> item : selectedRowMap.asMap().entrySet()) {
                    final int maxSize = item.getKey().getSize();
                    if (item.getValue().size() > maxSize) {
                        final String[] ids = item.getValue().stream().toArray(String[]::new);
                        getSupport().error("Можно выбрать только " + CommonBaseStringUtil.numberWithPostfixCase(maxSize, "строку.", "строки.", "строк."), ids);
                    }
                }
            }
        }
        return !getConfig().getUserContext().getErrorCollector().hasErrors();
    }

    private String getSelectId(IEppCustomPlanRowWrapper rowWrapper)
    {
        return "select_" + rowWrapper.getSourceRow().getId();
    }

    // Listeners

    public void onClickSave()
    {
        if (!isEditMode()) { return; }

        if (null != _student)
        {
            if (validate(false, false, true))
            {
                IEppCustomEduPlanDAO.instance.get().saveCustomEduPlanContent(getCustomPlan(), getPlanWrapper());

                final EppStudent2EduPlanVersion s2epv = DataAccessServices.dao().getByNaturalId(new EppStudent2EduPlanVersion.NaturalId(_student, getCustomPlan().getEpvBlock().getEduPlanVersion()));
                if (null == s2epv)
                    throw new ApplicationException("Связь с учебный планом студента отсутствует.");

                s2epv.setCustomEduPlan(getCustomPlan());
                DataAccessServices.dao().saveOrUpdate(s2epv);
                deactivate();

                _uiActivation.asDesktopRoot(IUniComponents.STUDENT_PUB)
                            .parameters(new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, _student.getId()).add("selectedStudentTab", "studentTab").add("selectedDataTab", "studentEppEduplanTab"))
                            .activate();
            }
        }
        else
        {
            if (validate(true, true, true))
            {
                IEppCustomEduPlanDAO.instance.get().saveCustomEduPlanContent(getCustomPlan(), getPlanWrapper());
                deactivate();
            }
        }
    }

    public void onClickApply()
    {
        if (!isEditMode()) { return; }
        if (validate(true, true, true))
            IEppCustomEduPlanDAO.instance.get().saveCustomEduPlanContent(getCustomPlan(), getPlanWrapper());
    }

    public void onClickMoveUp()
    {
        if (!isEditMode()) { return; }
        final IEppCustomPlanRowWrapper rowWrapper = getPlanWrapper().getKeyMap().get((String) getListenerParameter());
        getPlanWrapper().moveUp(rowWrapper);
    }

    public void onClickMoveDown()
    {
        if (!isEditMode()) { return; }
        final IEppCustomPlanRowWrapper rowWrapper = getPlanWrapper().getKeyMap().get((String) getListenerParameter());
        getPlanWrapper().moveDown(rowWrapper);
    }

    public void onClickExclude()
    {
        if (!isEditMode()) { return; }
        final IEppCustomPlanRowWrapper rowWrapper = getPlanWrapper().getKeyMap().get((String) getListenerParameter());
        rowWrapper.setExcluded(true);
    }

    public void onClickInclude()
    {
        if (!isEditMode()) { return; }
        final IEppCustomPlanRowWrapper rowWrapper = getPlanWrapper().getKeyMap().get((String) getListenerParameter());
        rowWrapper.setExcluded(false);
    }

    // Getters & Setters

    public IEppCustomPlanRowWrapper getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(IEppCustomPlanRowWrapper currentRow)
    {
        this.currentRow = currentRow;
    }

    public boolean isFirstRowInTerm()
    {
        return Iterables.getFirst(getCurrentTermEntry().getValue(), null) == getCurrentRow();
    }

    public Collection<Map.Entry<Integer, Collection<IEppCustomPlanRowWrapper>>> getTermSet()
    {
        return getPlanWrapper().getRowMap().entrySet();
    }

    public Map.Entry<Integer, Collection<IEppCustomPlanRowWrapper>> getCurrentTermEntry()
    {
        return new Map.Entry<Integer, Collection<IEppCustomPlanRowWrapper>>() {
            @Override public Integer getKey() { return EppCustomEduPlanContentUI.this.currentTermEntry.getKey(); }
            @Override public Collection<IEppCustomPlanRowWrapper> getValue() {
                // Если в семестре нет строк, надо добавить пустую строку для отображения
                return EppCustomEduPlanContentUI.this.currentTermEntry.getValue().isEmpty()
                        ? Collections.singletonList(null)
                        : EppCustomEduPlanContentUI.this.currentTermEntry.getValue();
            }
            @Override public Collection<IEppCustomPlanRowWrapper> setValue(Collection<IEppCustomPlanRowWrapper> value) { throw new UnsupportedOperationException(); }
        };
    }

    public void setCurrentTermEntry(Map.Entry<Integer, Set<IEppCustomPlanRowWrapper>> currentTermEntry)
    {
        this.currentTermEntry = currentTermEntry;
    }

    public Set<Map.Entry<String, String>> getLoadColumns()
    {
        return this.loadColumns.entrySet();
    }

    public Map.Entry<String, String> getCurrentLoadColumn()
    {
        return this.currentLoadColumn;
    }

    public void setCurrentLoadColumn(Map.Entry<String, String> currentLoadColumn)
    {
        this.currentLoadColumn = currentLoadColumn;
    }

    public Collection<EppControlActionType> getActiveFcaList()
    {
        return this.activeFcaList;
    }

    public EppFControlActionType getCurrentFCA()
    {
        return this.currentFCA;
    }

    public void setCurrentFCA(EppFControlActionType currentFCA)
    {
        this.currentFCA = currentFCA;
    }

    public EntityHolder<EppCustomEduPlan> getHolder()
    {
        return this.holder;
    }

    public EppCustomEduPlan getCustomPlan()
    {
        return getHolder().getValue();
    }

    public EppCustomPlanWrapper getPlanWrapper()
    {
        return planWrapper;
    }

    public boolean isEditMode()
    {
        return this.editMode;
    }

    public void setEditMode(boolean editMode)
    {
        this.editMode = editMode;
    }

    public String getPermissionKey()
    {
        return permissionKey;
    }

    public void setPermissionKey(String permissionKey)
    {
        this.permissionKey = permissionKey;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public String getCurrentRowStyle()
    {
        final IEppCustomPlanRowWrapper currentRow = getCurrentRow();
        return currentRow == null || !currentRow.isExcluded() ? "" : "background-color: #E3E3E3";
    }

    public String getCurrentRowLoadStyle()
    {
        final IEppCustomPlanRowWrapper currentRow = getCurrentRow();
        if (currentRow == null) {
            return "";
        }
        return !currentRow.getSourceRow().hasParentDistributedRow() ? "text-align: center" : "text-align: center; color: gray;";
    }

    public boolean isHasCurrentActionInCurrentRow()
    {
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        if (rowWrapper == null) {
            return false;
        }
        final String fcaFullCode = getCurrentFCA().getFullCode();
        return rowWrapper.hasControlAction(fcaFullCode);
    }

    public String getCurrentControlActionImage()
    {
        final IEppCustomPlanRowWrapper currentRow = getCurrentRow();
        return currentRow != null && currentRow.getSourceRow().hasParentDistributedRow() ? "img/general/yes_disabled.png" : "img/general/yes.png";
    }

    public Collection<IEppCustomPlanRowWrapper> getCurrentTermRows()
    {
        return getCurrentTermEntry().getValue();
    }

    public int getCurrentTermSize()
    {
        return getCurrentTermEntry().getValue().size();
    }

    public String getCurrentCustomTerm()
    {
        final int term = getCurrentTermEntry().getKey();
        return String.valueOf(term) + " " + this.gridTermMap.get(term).getPart().getTitle();
    }

    public String getCurrentRowIndex()
    {
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        return rowWrapper != null ? rowWrapper.getSourceRow().getIndex() : "";
    }

    public String getCurrentRowTitle()
    {
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        return rowWrapper != null ? getCurrentRow().getTitle() : "";
    }

    public EppRegistryElement getCurrentRowRegistryElement()
    {
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        if (rowWrapper == null) {
            return null;
        }
        return rowWrapper.getSourceRow().getRow() instanceof EppEpvRegistryRow ? ((EppEpvRegistryRow) rowWrapper.getSourceRow().getRow()).getRegistryElement() : null;
    }

    public String getCurrentLoadValue()
    {
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        if (rowWrapper == null) {
            return "";
        }
        final String loadFullCode = getCurrentLoadColumn().getKey();
        return UniEppUtils.formatLoad(rowWrapper.getTotalLoad(loadFullCode), true);
    }

    public String getCurrentTotalLoadValue()
    {
        final String loadFullCode = getCurrentLoadColumn().getKey();
        final int term = getCurrentTermEntry().getKey();
        return UniEppUtils.formatLoad(getPlanWrapper().calcTotalLoadInTerm(term, loadFullCode), false);
    }

    public String getCurrentTotalActionSize()
    {
        final String caFullCode = getCurrentFCA().getFullCode();
        final int term = getCurrentTermEntry().getKey();
        return String.valueOf(getPlanWrapper().calcFinalControlActionCountInTerm(term, caFullCode));
    }

    public String getCurrentTotalReattestationSize()
    {
        final int term = getCurrentTermEntry().getKey();
        return UniEppUtils.formatLoad(getPlanWrapper().calcTotalReattestationSizeInTerm(term), false);
    }

    public String getCurrentOwnerOrgUnitTitle()
    {
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        if (rowWrapper == null) {
            return "";
        }
        if (rowWrapper.getSourceRow().getRow() instanceof EppEpvRegistryRow) {
            final EppEpvRegistryRow row = (EppEpvRegistryRow) rowWrapper.getSourceRow().getRow();
            if (row.getRegistryElementOwner() != null) {
                return row.getRegistryElementOwner().getShortTitle();
            }
        }
        return "";
    }

    public String getCurrentRowSourceTerm()
    {
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        return rowWrapper != null ? String.valueOf(rowWrapper.getSourceTermNumber()) : "";
    }

    public boolean isCurrentMoveUpEnable()
    {
        if (!isEditMode()) { return false; }
        if (getCurrentTermEntry().getKey() == 1) { return false; } // Первый семестр

        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        return !rowWrapper.getSourceRow().hasParentDistributedRow(); // Все, кроме дисциплин из групп дисциплин по выбору
    }

    public boolean isCurrentMoveDownEnable()
    {
        if (!isEditMode()) { return false; }
        if (getCurrentTermEntry().getKey() == getPlanWrapper().getGridSize()) { return false; }// Последний семестр

        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        return !rowWrapper.getSourceRow().hasParentDistributedRow(); // Все, кроме дисциплин из групп дисциплин по выбору
    }

    public String getCurrentAttestationString()
    {
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        return rowWrapper != null ? rowWrapper.getCustomActionTitle() : "";
    }

    public String getCurrentStudied()
    {
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        if (rowWrapper != null && rowWrapper.getNeedRetake() != null && !rowWrapper.isExcluded())
        {
            return UniEppUtils.formatLoad(rowWrapper.getTotalLoad(EppLoadType.FULL_CODE_LABOR), true);
        }
        return "";
    }

    public String getCurrentShallBeStudied()
    {
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        final double load = getShallStudiedLoad(rowWrapper);
        return UniEppUtils.formatLoad(load, true);
    }

    private List<IEppCustomPlanRowWrapper> getChilds(IEppCustomPlanRowWrapper srcRow)
    {
        if (null == srcRow) return Collections.emptyList();

        return getPlanWrapper().getRowMap().get(srcRow.getNewTermNumber())
                .stream()
                .filter(row -> null != row && row.getSourceRow().isChildOf(srcRow.getSourceRow()))
                .collect(Collectors.toList());

    }

    private double getShallStudiedLoad(IEppCustomPlanRowWrapper rowWrapper)
    {

        if (rowWrapper != null && rowWrapper.getNeedRetake() == null && !rowWrapper.isExcluded())
        {
            final double loadToExclude = getChilds(rowWrapper)
                    .stream()
                    .filter(childRow -> childRow.getNeedRetake() != null && !childRow.isExcluded())
                    .collect(Collectors.summarizingDouble(childRow -> childRow.getTotalLoad(EppLoadType.FULL_CODE_LABOR))).getSum();

            double load = rowWrapper.getTotalLoad(EppLoadType.FULL_CODE_LABOR);
            load -= loadToExclude;
            if (load > 0d)
                return load;
        }
        return 0d;
    }


    public String getCurrentTotalStudied()
    {
        double result = 0d;

        for (IEppCustomPlanRowWrapper rowWrapper : getCurrentTermRows()) {
            if (rowWrapper != null && rowWrapper.getNeedRetake() != null && !rowWrapper.isExcluded())
            {
                result += (rowWrapper.getTotalLoad(EppLoadType.FULL_CODE_LABOR));
            }
        }

        return UniEppUtils.formatLoad(result, true);
    }


    public String getCurrentTotalShallBeStudied()
    {
        double result = 0d;
        for (IEppCustomPlanRowWrapper rowWrapper : getCurrentTermRows()) {
            if (null != rowWrapper && !rowWrapper.getSourceRow().hasParentDistributedRow())
            {
                result += getShallStudiedLoad(rowWrapper);
            }
        }

        return UniEppUtils.formatLoad(result, true);
    }


    public IIdentifiable getCurrentReattestationValue()
    {
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        if (rowWrapper == null) {
            return null;
        }
        final Long id;
        if (rowWrapper.isReattestation()) {
            id = EppCustomEduPlanManager.REATTESTATION_ITEM_ID;
        } else if (rowWrapper.isReexamination()) {
            id = EppCustomEduPlanManager.REEXAMINATION_ITEM_ID;
        } else {
            return null;
        }
        return new IdentifiableWrapper<>(id, "");
    }

    public String getCurrentReattestationSelectId()
    {
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        return getSelectId(rowWrapper);
    }

    public void setCurrentReattestationValue(IIdentifiable value)
    {
        if (!isEditMode()) { return; }
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        if (value == null) {
            rowWrapper.setNeedRetake(null);
        } else {
            rowWrapper.setNeedRetake(value.getId().equals(EppCustomEduPlanManager.REATTESTATION_ITEM_ID));
        }
    }

    public boolean isCurrentReattestationSelectVisible()
    {
        if (!isEditMode()) { return false; }
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        return rowWrapper != null && !(rowWrapper.getSourceRow().getRow() instanceof EppEpvGroupImRow) && !rowWrapper.isExcluded();
    }

    public boolean isCurrentRowOptional()
    {
        if (!isEditMode()) { return false; }
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        return rowWrapper != null && rowWrapper.getSourceRow().isOptionalItem();
    }

    public boolean isCurrentRowExcluded()
    {
        if (!isEditMode()) { return false; }
        final IEppCustomPlanRowWrapper rowWrapper = getCurrentRow();
        return rowWrapper != null && rowWrapper.isExcluded();
    }

    public ISelectModel getRetakeSelectModel()
    {
        return retakeSelectModel;
    }
}