package ru.tandemservice.uniepp.settings.bo.UsedTypesControlActions.logic;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;

/**
 * @author avedernikov
 * @since 04.09.2015
 */

public class UsedTypesModifyDAO extends UniBaseDao implements IUsedTypesModifyDAO
{
	@Override
	public void doEnable(Long typeCAId)
	{
		EppControlActionType ca = getNotNull(EppControlActionType.class, typeCAId);
		ca.setDisabled(false);
		update(ca);
	}

	@Override
	public void doDisable(Long typeCAId)
	{
		EppControlActionType ca = getNotNull(EppControlActionType.class, typeCAId);
		ca.setDisabled(true);
		ca.setActiveInEduPlan(false);
		ca.setUsedWithDisciplines(false);
		ca.setUsedWithPractice(false);
		ca.setUsedWithAttestation(false);
		update(ca);
	}

	@Override
	public void doToggleActiveInEduPlan(Long typeCAId)
	{
		EppControlActionType ca = getNotNull(EppControlActionType.class, typeCAId);
		ca.setActiveInEduPlan(!ca.isActiveInEduPlan());
		update(ca);
	}

	@Override
	public void doToggleUsedWithDisciplines(Long typeCAId)
	{
		EppControlActionType ca = getNotNull(EppControlActionType.class, typeCAId);
		ca.setUsedWithDisciplines(!ca.isUsedWithDisciplines());
		update(ca);
	}

	@Override
	public void doToggleUsedWithPractice(Long typeCAId)
	{
		EppControlActionType ca = getNotNull(EppControlActionType.class, typeCAId);
		ca.setUsedWithPractice(!ca.isUsedWithPractice());
		update(ca);
	}

	@Override
	public void doToggleUsedWithAttestation(Long typeCAId)
	{
		EppControlActionType ca = getNotNull(EppControlActionType.class, typeCAId);
		ca.setUsedWithAttestation(!ca.isUsedWithAttestation());
		update(ca);
	}
}
