package ru.tandemservice.uniepp.component.registry.ModuleRegistry;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.TitledFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.AddEditResult;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryModuleGen;

import java.util.Map;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void bindSetReturned(IBusinessComponent component, String childRegionName, Map<String, Object> returnedData) {
        super.bindSetReturned(component, childRegionName, returnedData);
        AddEditResult.processReturned(returnedData);         // если с формы пришла инфа, что создался объект - переходим на его карточку
    }

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        final String settingsKey = "epp.registry." + EppRegistryModule.class.getSimpleName() + model.getOrgUnitId();
        model.setSettings(UniBaseUtils.getDataSettings(component, settingsKey));

        if (null == model.getDataSource())
        {
            final DynamicListDataSource dataSource = new DynamicListDataSource<EppRegistryModule>(component, component1 -> {
                Controller.this.getDao().prepareDataSource(model);
            });
            dataSource.addColumn(UniEppUtils.getStateColumn());

            dataSource.addColumn(new SimpleColumn("Номер", EppRegistryModule.P_NUMBER).setClickable(false).setWidth(1).setRequired(true));
            dataSource.addColumn(new SimpleColumn("Название", EppRegistryModule.P_TITLE).setClickable(true).setRequired(true));
            dataSource.addColumn(new SimpleColumn("Полное название", EppRegistryModuleGen.P_FULL_TITLE).setClickable(false));
            dataSource.addColumn(new SimpleColumn("Сокращенное название", EppRegistryModule.P_SHORT_TITLE).setClickable(false));
            this.prepareDataSourceColumns(dataSource, model);

            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditElement").setPermissionKey(model.getSec().getPermission("edit")));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteElement", "Удалить запись «{0}» из реестра?", EppRegistryElementGen.P_TITLE).setPermissionKey(model.getSec().getPermission("delete")));

            dataSource.setOrder(EppRegistryElementGen.P_TITLE, OrderDirection.asc);

            model.setDataSource(dataSource);
        }
    }

    protected void prepareDataSourceColumns(final DynamicListDataSource dataSource, final Model model)
    {
        dataSource.addColumn(new BooleanColumn("Общий", EppRegistryModule.shared()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", EppRegistryModule.state(), TitledFormatter.INSTANCE).setClickable(false).setWidth(1));
        if (null == model.getOrgUnit())
        {
            dataSource.addColumn(new SimpleColumn("Подразделение", EppRegistryModule.owner().shortTitle()).setClickable(false));
        }
        dataSource.addColumn(new SimpleColumn("Тип (группа)", EppRegistryModule.parent(), TitledFormatter.INSTANCE).setClickable(false));

        this.getDao().prepareLoadColumns(model, dataSource);

    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent component)
    {
        this.getModel(component).getSettings().clear();
        this.onClickSearch(component);
    }

    public void onClickAddElement(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(model.getClass().getPackage().getName() + ".AddEdit", new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getOrgUnitId())));
    }

    public void onClickEditElement(final IBusinessComponent context)
    {
        final Model model = this.getModel(context);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(model.getClass().getPackage().getName() + ".AddEdit", new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, context.getListenerParameter())));
    }

    public void onClickDeleteElement(final IBusinessComponent component)
    {
        UniDaoFacade.getCoreDao().delete(component.<Long> getListenerParameter());
    }

}
