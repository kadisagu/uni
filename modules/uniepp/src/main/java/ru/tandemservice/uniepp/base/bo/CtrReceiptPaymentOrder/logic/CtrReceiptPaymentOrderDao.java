package ru.tandemservice.uniepp.base.bo.CtrReceiptPaymentOrder.logic;

import java.util.Collection;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.CtrReceiptPaymentOrder.CtrReceiptPaymentOrderManager;
import ru.tandemservice.uniepp.base.bo.EppContract.EppContractManager;

/**
 * @author vdanilov
 */
public class CtrReceiptPaymentOrderDao extends UniBaseDao implements ICtrReceiptPaymentOrderDao {

    @Override
    public Student getSingleStudent(final CtrContractObject contract)
    {
        final Collection<Student> students = EppContractManager.instance().dao().listStudents(contract);

        if (students.isEmpty()) {
            return null;
        }
        if (students.size() > 1) {
            throw new ApplicationException(CtrReceiptPaymentOrderManager.instance().getProperty("error.could-not-print-payment-order-for-few-students"));
        }

        return students.iterator().next();
    }

}
