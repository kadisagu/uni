package ru.tandemservice.uniepp.component.registry.DisciplineRegistry.Pub;

import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;

/**
 * 
 * @author nkokorina
 *
 */

public class Model extends ru.tandemservice.uniepp.component.registry.base.Pub.Model<EppRegistryDiscipline>
{
    @Override public Class<EppRegistryDiscipline> getElementClass() { return EppRegistryDiscipline.class; }
    @Override public String getPermissionPrefix() { return "eppDiscipline"; }
}
