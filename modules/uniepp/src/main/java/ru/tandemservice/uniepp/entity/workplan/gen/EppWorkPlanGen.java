package ru.tandemservice.uniepp.entity.workplan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Рабочий учебный план
 *
 * Рабочий учебный план, содержащий в том числе и распределение недель по частям семестра
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkPlanGen extends EppWorkPlanBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.workplan.EppWorkPlan";
    public static final String ENTITY_NAME = "eppWorkPlan";
    public static final int VERSION_HASH = -618164688;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";
    public static final String L_YEAR = "year";
    public static final String L_TERM = "term";
    public static final String P_NUMBER = "number";
    public static final String L_CUSTOM_EDU_PLAN = "customEduPlan";
    public static final String L_CACHED_GRID_TERM = "cachedGridTerm";

    private EppEduPlanVersionBlock _parent;     // УП (блок версии)
    private EppYearEducationProcess _year;     // ПУПнаГ
    private Term _term;     // Семестр
    private String _number;     // Номер
    private EppCustomEduPlan _customEduPlan;     // Индивидуальный учебный план
    private DevelopGridTerm _cachedGridTerm;     // Семестр сетки (кэш для запросов)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return УП (блок версии). Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlock getParent()
    {
        return _parent;
    }

    /**
     * @param parent УП (блок версии). Свойство не может быть null.
     */
    public void setParent(EppEduPlanVersionBlock parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getYear()
    {
        return _year;
    }

    /**
     * @param year ПУПнаГ. Свойство не может быть null.
     */
    public void setYear(EppYearEducationProcess year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Семестр. Свойство не может быть null.
     */
    @NotNull
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр. Свойство не может быть null.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Индивидуальный учебный план.
     */
    public EppCustomEduPlan getCustomEduPlan()
    {
        return _customEduPlan;
    }

    /**
     * @param customEduPlan Индивидуальный учебный план.
     */
    public void setCustomEduPlan(EppCustomEduPlan customEduPlan)
    {
        dirty(_customEduPlan, customEduPlan);
        _customEduPlan = customEduPlan;
    }

    /**
     * @return Семестр сетки (кэш для запросов). Свойство не может быть null.
     */
    @NotNull
    public DevelopGridTerm getCachedGridTerm()
    {
        return _cachedGridTerm;
    }

    /**
     * @param cachedGridTerm Семестр сетки (кэш для запросов). Свойство не может быть null.
     */
    public void setCachedGridTerm(DevelopGridTerm cachedGridTerm)
    {
        dirty(_cachedGridTerm, cachedGridTerm);
        _cachedGridTerm = cachedGridTerm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppWorkPlanGen)
        {
            setParent(((EppWorkPlan)another).getParent());
            setYear(((EppWorkPlan)another).getYear());
            setTerm(((EppWorkPlan)another).getTerm());
            setNumber(((EppWorkPlan)another).getNumber());
            setCustomEduPlan(((EppWorkPlan)another).getCustomEduPlan());
            setCachedGridTerm(((EppWorkPlan)another).getCachedGridTerm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkPlanGen> extends EppWorkPlanBase.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkPlan.class;
        }

        public T newInstance()
        {
            return (T) new EppWorkPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return obj.getParent();
                case "year":
                    return obj.getYear();
                case "term":
                    return obj.getTerm();
                case "number":
                    return obj.getNumber();
                case "customEduPlan":
                    return obj.getCustomEduPlan();
                case "cachedGridTerm":
                    return obj.getCachedGridTerm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "parent":
                    obj.setParent((EppEduPlanVersionBlock) value);
                    return;
                case "year":
                    obj.setYear((EppYearEducationProcess) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "customEduPlan":
                    obj.setCustomEduPlan((EppCustomEduPlan) value);
                    return;
                case "cachedGridTerm":
                    obj.setCachedGridTerm((DevelopGridTerm) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                        return true;
                case "year":
                        return true;
                case "term":
                        return true;
                case "number":
                        return true;
                case "customEduPlan":
                        return true;
                case "cachedGridTerm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return true;
                case "year":
                    return true;
                case "term":
                    return true;
                case "number":
                    return true;
                case "customEduPlan":
                    return true;
                case "cachedGridTerm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return EppEduPlanVersionBlock.class;
                case "year":
                    return EppYearEducationProcess.class;
                case "term":
                    return Term.class;
                case "number":
                    return String.class;
                case "customEduPlan":
                    return EppCustomEduPlan.class;
                case "cachedGridTerm":
                    return DevelopGridTerm.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkPlan> _dslPath = new Path<EppWorkPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkPlan");
    }
            

    /**
     * @return УП (блок версии). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlan#getParent()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlan#getYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlan#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlan#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Индивидуальный учебный план.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlan#getCustomEduPlan()
     */
    public static EppCustomEduPlan.Path<EppCustomEduPlan> customEduPlan()
    {
        return _dslPath.customEduPlan();
    }

    /**
     * @return Семестр сетки (кэш для запросов). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlan#getCachedGridTerm()
     */
    public static DevelopGridTerm.Path<DevelopGridTerm> cachedGridTerm()
    {
        return _dslPath.cachedGridTerm();
    }

    public static class Path<E extends EppWorkPlan> extends EppWorkPlanBase.Path<E>
    {
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _parent;
        private EppYearEducationProcess.Path<EppYearEducationProcess> _year;
        private Term.Path<Term> _term;
        private PropertyPath<String> _number;
        private EppCustomEduPlan.Path<EppCustomEduPlan> _customEduPlan;
        private DevelopGridTerm.Path<DevelopGridTerm> _cachedGridTerm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return УП (блок версии). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlan#getParent()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> parent()
        {
            if(_parent == null )
                _parent = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlan#getYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> year()
        {
            if(_year == null )
                _year = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_YEAR, this);
            return _year;
        }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlan#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlan#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppWorkPlanGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Индивидуальный учебный план.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlan#getCustomEduPlan()
     */
        public EppCustomEduPlan.Path<EppCustomEduPlan> customEduPlan()
        {
            if(_customEduPlan == null )
                _customEduPlan = new EppCustomEduPlan.Path<EppCustomEduPlan>(L_CUSTOM_EDU_PLAN, this);
            return _customEduPlan;
        }

    /**
     * @return Семестр сетки (кэш для запросов). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlan#getCachedGridTerm()
     */
        public DevelopGridTerm.Path<DevelopGridTerm> cachedGridTerm()
        {
            if(_cachedGridTerm == null )
                _cachedGridTerm = new DevelopGridTerm.Path<DevelopGridTerm>(L_CACHED_GRID_TERM, this);
            return _cachedGridTerm;
        }

        public Class getEntityClass()
        {
            return EppWorkPlan.class;
        }

        public String getEntityName()
        {
            return "eppWorkPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
