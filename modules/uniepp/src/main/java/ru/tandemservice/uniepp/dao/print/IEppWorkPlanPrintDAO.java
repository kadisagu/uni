package ru.tandemservice.uniepp.dao.print;

import java.util.Collection;
import java.util.Map;

import jxl.write.WritableWorkbook;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface IEppWorkPlanPrintDAO {
    public static final SpringBeanCache<IEppWorkPlanPrintDAO> instance = new SpringBeanCache<IEppWorkPlanPrintDAO>(IEppWorkPlanPrintDAO.class.getName());

    /**
     * печатаем РУП из реестра (просто как программу обучения)
     * @param book - куда печатаем
     * @param wpIds - какие РУП надо напечатать (будут в своих вкладках)
     * @param context - контекст печати (передача доп. информации)
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void printRegistryWorkPlan(WritableWorkbook book, Collection<Long> wpIds, Map<String, Object> context);


    /**
     * печатаем РУП из реестра (с различиями по версиям)
     * @param book - куда печатаем
     * @param wpIds - какие РУП надо напечатать (будут в своих вкладках - тут именно корневые РУП, версии будут игнорироваться)
     * @param context - контекст печати (передача доп. информации)
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void printRegistryWorkPlanWithVersions(WritableWorkbook book, Collection<Long> wpIds, Map<String, Object> context);


    /**
     * печатаем РУП группы (группированные по РУП)
     * @param book - куда печатаем
     * @param wp2gsIdsMap { wp.id -> { group.id -> { student.id }}} - связи РУП со студентами
     * @param context - контекст печати (передача доп. информации)
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void printGroupWorkPlan(WritableWorkbook book, Map<Long, Map<Long, Collection<Long>>> wp2gsIdsMap, Map<String, Object> context);


}
