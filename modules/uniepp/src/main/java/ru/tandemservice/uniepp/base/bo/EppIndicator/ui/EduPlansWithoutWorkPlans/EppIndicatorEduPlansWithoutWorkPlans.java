/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.EduPlansWithoutWorkPlans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniedu.program.bo.EduProgram.EduProgramManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author oleyba
 * @since 9/17/14
 */
@Configuration
public class EppIndicatorEduPlansWithoutWorkPlans extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EduProgramManager.instance().programKindDSConfig())
            .addDataSource(searchListDS("eduPlanDS", eduPlanDSColumns(), eduPlanDSHandler()))
            .create();
    }
    
    @Bean
    public ColumnListExtPoint eduPlanDSColumns(){
        return columnListExtPointBuilder("eduPlanDS")
            .addColumn(publisherColumn("title", EppEduPlanVersion.title().s()).order())
            .addColumn(textColumn("educationElement", EppEduPlanVersion.educationElementTitle()))
            .addColumn(textColumn("developCombination", EppEduPlanVersion.eduPlan().developCombinationTitle().s()))
            .addColumn(textColumn("orgUnit", EppEduPlanVersion.eduPlan().owner().fullTitle().s()).order())
            .addColumn(textColumn("yearsString", EppEduPlanVersion.eduPlan().yearsString().s()).order())
            .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduPlanDSHandler()
    {
        return new EppEduPlanWithoutWorkplanDSHandler(getName());
    }
}