/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.EppEPVBlockProfActivityTypeAdd;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.EppDevelopResultManager;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.logic.EppProfActivityTypeComboDSHandler;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.List;

/**
 * @author Igor Belanov
 * @since 28.02.2017
 */
@State({
        @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "eduPlanVersion.id", required = true)
})
public class EppDevelopResultEppEPVBlockProfActivityTypeAddUI extends UIPresenter
{
    // версия УП в рамках которой работаем
    private EppEduPlanVersion _eduPlanVersion = new EppEduPlanVersion();

    @Override
    public void onComponentRefresh()
    {
        if (getEduPlanVersion() != null && getEduPlanVersion().getId() != null)
        {
            setEduPlanVersion(DataAccessServices.dao().get(getEduPlanVersion().getId()));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppDevelopResultEppEPVBlockProfActivityTypeAdd.EPV_BLOCK_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppEduPlanVersionBlock.PROP_EDU_PLAN_VERSION, getEduPlanVersion());
        }
        else if (EppDevelopResultEppEPVBlockProfActivityTypeAdd.PROF_ACTIVITY_TYPE_DS.equals(dataSource.getName()))
        {
            if (_eduPlanVersion != null)
                dataSource.put(EppProfActivityTypeComboDSHandler.PROP_EDU_PLAN_VERSION_ID, _eduPlanVersion.getId());
            EppEduPlanVersionBlock EPVBLock = getSettings().get("eduPlanVersionBlock");
            if (EPVBLock != null)
                dataSource.put(EppProfActivityTypeComboDSHandler.PROP_EDU_PLAN_VERSION_BLOCK_ID, EPVBLock.getId());
        }
    }

    public void onClickApply()
    {
        EppEduPlanVersionBlock EPVBlock = getSettings().get("eduPlanVersionBlock");
        List<EppProfActivityType> profActivityTypeList = getSettings().get("profActivityTypeList");
        EppDevelopResultManager.instance().dao().saveEppEPVBlockProfActivityType(EPVBlock, profActivityTypeList);
        deactivate();
    }

    // getters & setters
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        _eduPlanVersion = eduPlanVersion;
    }
}
