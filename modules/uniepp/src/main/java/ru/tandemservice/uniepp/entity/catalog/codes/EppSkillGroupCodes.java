package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Группа компетенций"
 * Имя сущности : eppSkillGroup
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppSkillGroupCodes
{
    /** Константа кода (code) элемента : Знания, умения, навыки (title) */
    String ZNANIYA_UMENIYA_NAVYKI = "00";
    /** Константа кода (code) элемента : Общие компетенции (title) */
    String OBTSHIE_KOMPETENTSII = "01";
    /** Константа кода (code) элемента : Общекультурные компетенции (title) */
    String OBTSHEKULTURNYE_KOMPETENTSII = "02";
    /** Константа кода (code) элемента : Профессиональные компетенции (title) */
    String PROFESSIONALNYE_KOMPETENTSII = "03";
    /** Константа кода (code) элемента : Профессионально-специализированные компетенции (title) */
    String PROFESSIONALNO_SPETSIALIZIROVANNYE_KOMPETENTSII = "04";
    /** Константа кода (code) элемента : Общепрофессиональные компетенции (title) */
    String OBTSHEPROFESSIONALNYE_KOMPETENTSII = "05";
    /** Константа кода (code) элемента : Общенаучные компетенции (title) */
    String OBTSHENAUCHNYE_KOMPETENTSII = "06";
    /** Константа кода (code) элемента : Инструментальные компетенции (title) */
    String INSTRUMENTALNYE_KOMPETENTSII = "07";
    /** Константа кода (code) элемента : Системные компетенции (title) */
    String SISTEMNYE_KOMPETENTSII = "08";
    /** Константа кода (code) элемента : Социально-личностные и общекультурные компетенции (title) */
    String SOTSIALNO_LICHNOSTNYE_I_OBTSHEKULTURNYE_KOMPETENTSII = "09";
    /** Константа кода (code) элемента : Специализированные компетенции (title) */
    String SPETSIALIZIROVANNYE_KOMPETENTSII = "10";
    /** Константа кода (code) элемента : Универсальные компетенции (title) */
    String UNIVERSALNYE_KOMPETENTSII = "11";
    /** Константа кода (code) элемента : Профессиональные компетенции в области психолого-педагогического сопровождения дошкольного, общего, дополнительного и профессионального образования (title) */
    String PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_PSIHOLOGO_PEDAGOGICHESKOGO_SOPROVOJDENIYA_DOSHKOLNOGO_OBTSHEGO_DOPOLNITELNOGO_I_PROFESSIONALNOGO_OBRAZOVANIYA = "12";
    /** Константа кода (code) элемента : Профессиональные компетенции в области образовательной деятельности в дошкольном образовании (title) */
    String PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_OBRAZOVATELNOY_DEYATELNOSTI_V_DOSHKOLNOM_OBRAZOVANII = "13";
    /** Константа кода (code) элемента : Профессиональные компетенции в области учебной и воспитательной деятельности на начальной ступени общего образования (title) */
    String PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_UCHEBNOY_I_VOSPITATELNOY_DEYATELNOSTI_NA_NACHALNOY_STUPENI_OBTSHEGO_OBRAZOVANIYA = "14";
    /** Константа кода (code) элемента : Профессиональные компетенции в области социально-педагогической деятельности (title) */
    String PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_SOTSIALNO_PEDAGOGICHESKOY_DEYATELNOSTI = "15";
    /** Константа кода (code) элемента : Профессиональные компетенции в области психолого-педагогического сопровождения детей с ограниченными возможностями здоровья в коррекционном и инклюзивном образовании (title) */
    String PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_PSIHOLOGO_PEDAGOGICHESKOGO_SOPROVOJDENIYA_DETEY_S_OGRANICHENNYMI_VOZMOJNOSTYAMI_ZDOROVYA_V_KORREKTSIONNOM_I_INKLYUZIVNOM_OBRAZOVANII = "16";
    /** Константа кода (code) элемента : Ограниченные возможности здоровья (title) */
    String OGRANICHENNYE_VOZMOJNOSTI_ZDOROVYA = "17";
    /** Константа кода (code) элемента : Профессиональные компетенции в области образовательной деятельности (title) */
    String PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_OBRAZOVATELNOY_DEYATELNOSTI = "18";
    /** Константа кода (code) элемента : Профессиональные компетенции в области научно-методической деятельности (title) */
    String PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_NAUCHNO_METODICHESKOY_DEYATELNOSTI = "19";
    /** Константа кода (code) элемента : Профессиональные компетенции в научно-исследовательской деятельности (title) */
    String PROFESSIONALNYE_KOMPETENTSII_V_NAUCHNO_ISSLEDOVATELSKOY_DEYATELNOSTI = "20";
    /** Константа кода (code) элемента : Профессиональные компетенции в организационно-управленческой деятельности (title) */
    String PROFESSIONALNYE_KOMPETENTSII_V_ORGANIZATSIONNO_UPRAVLENCHESKOY_DEYATELNOSTI = "21";
    /** Константа кода (code) элемента : Профессионально-дисциплинарные компетенции (title) */
    String PROFESSIONALNO_DISTSIPLINARNYE_KOMPETENTSII = "24";

    Set<String> CODES = ImmutableSet.of(ZNANIYA_UMENIYA_NAVYKI, OBTSHIE_KOMPETENTSII, OBTSHEKULTURNYE_KOMPETENTSII, PROFESSIONALNYE_KOMPETENTSII, PROFESSIONALNO_SPETSIALIZIROVANNYE_KOMPETENTSII, OBTSHEPROFESSIONALNYE_KOMPETENTSII, OBTSHENAUCHNYE_KOMPETENTSII, INSTRUMENTALNYE_KOMPETENTSII, SISTEMNYE_KOMPETENTSII, SOTSIALNO_LICHNOSTNYE_I_OBTSHEKULTURNYE_KOMPETENTSII, SPETSIALIZIROVANNYE_KOMPETENTSII, UNIVERSALNYE_KOMPETENTSII, PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_PSIHOLOGO_PEDAGOGICHESKOGO_SOPROVOJDENIYA_DOSHKOLNOGO_OBTSHEGO_DOPOLNITELNOGO_I_PROFESSIONALNOGO_OBRAZOVANIYA, PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_OBRAZOVATELNOY_DEYATELNOSTI_V_DOSHKOLNOM_OBRAZOVANII, PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_UCHEBNOY_I_VOSPITATELNOY_DEYATELNOSTI_NA_NACHALNOY_STUPENI_OBTSHEGO_OBRAZOVANIYA, PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_SOTSIALNO_PEDAGOGICHESKOY_DEYATELNOSTI, PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_PSIHOLOGO_PEDAGOGICHESKOGO_SOPROVOJDENIYA_DETEY_S_OGRANICHENNYMI_VOZMOJNOSTYAMI_ZDOROVYA_V_KORREKTSIONNOM_I_INKLYUZIVNOM_OBRAZOVANII, OGRANICHENNYE_VOZMOJNOSTI_ZDOROVYA, PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_OBRAZOVATELNOY_DEYATELNOSTI, PROFESSIONALNYE_KOMPETENTSII_V_OBLASTI_NAUCHNO_METODICHESKOY_DEYATELNOSTI, PROFESSIONALNYE_KOMPETENTSII_V_NAUCHNO_ISSLEDOVATELSKOY_DEYATELNOSTI, PROFESSIONALNYE_KOMPETENTSII_V_ORGANIZATSIONNO_UPRAVLENCHESKOY_DEYATELNOSTI, PROFESSIONALNO_DISTSIPLINARNYE_KOMPETENTSII);
}
