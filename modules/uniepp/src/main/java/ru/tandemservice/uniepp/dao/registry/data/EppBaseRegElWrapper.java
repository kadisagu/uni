package ru.tandemservice.uniepp.dao.registry.data;

import org.tandemframework.core.bean.FastBeanUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.IEppRegistryElementItem;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vdanilov
 */
public abstract class EppBaseRegElWrapper<T extends IEppRegistryElementItem> implements IEppBaseRegElWrapper<T>
{
    private final T item;
    @Override public T getItem() { return this.item; }

    public EppBaseRegElWrapper(final T item) { this.item = item; }

    @Override public Long getId() { return this.getItem().getId(); }
    @Override public String getTitle() {  return this.getItem().getTitle(); }
    @Override public Object getProperty(final Object propertyPath) { return FastBeanUtils.getValue(this, propertyPath); }

    protected abstract Collection<? extends EppBaseRegElWrapper> getChildren();

    // первичный источник данных
    private final Map<String, Long> localLoadMap = new HashMap<>(8);
    public Map<String, Long> getLocalLoadMap() { return this.localLoadMap; }

    // кэш, хранящий итоговые значения
    private final Map<String, Long> hierarchicalLoadMap = new HashMap<>(16);
    public Map<String, Long> getHierarchicalLoadMap() { return this.hierarchicalLoadMap; }

    protected long load(final String fullCode)
    {
        // сначала ищем в иерархическом справочнике (там лежат уже готовые значения)
        Long value = this.hierarchicalLoadMap.get(fullCode);
        if (null != value) { return value; }

        // если там ничего нет - берем значение из локального справочника и добавляем к нему все дочерние
        long result = getLocalLoad(fullCode);
        for (final EppBaseRegElWrapper w: this.getChildren()) {
            result += w.load(fullCode);
        }

        // сохраняем значение
        this.hierarchicalLoadMap.put(fullCode, result);
        return result;
    }

    // извлекает локальную нагрузку, если не находит - вызывет resolver
    protected long getLocalLoad(final String fullCode) {
        Long value = this.localLoadMap.get(fullCode);
        if (null == value) {
            this.localLoadMap.put(fullCode, value = resolveLocalLoad(fullCode));
        }
        return value;
    }

    // resolver для поиска недостающих значений (переопределяемое)
    protected long resolveLocalLoad(final String fullCode) {
        return 0L;
    }

    @Override public double getLoadAsDouble(final String loadFullCode) { return UniEppUtils.wrap(this.load(loadFullCode)); }
    @Override public int getActionSize(final String actionFullCode) { return (int)this.load(actionFullCode); }

    @Override public void setId(final Long id) { throw new UnsupportedOperationException(); }
    @Override public void setProperty(final String propertyPath, final Object propertyValue) { throw new UnsupportedOperationException(); }
}
