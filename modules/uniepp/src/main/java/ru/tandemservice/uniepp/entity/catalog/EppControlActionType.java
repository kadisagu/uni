package ru.tandemservice.uniepp.entity.catalog;

import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppControlActionTypeGen;

import java.util.Comparator;
import java.util.function.Predicate;

/**
 * Тип контрольного мероприятия
 */
public abstract class EppControlActionType extends EppControlActionTypeGen implements IEppFullCodeCatalogItem
{
    public static final String CATALOG_CODE = EppControlActionType.ENTITY_NAME;

    public static final String FULL_CODE_CONTROL_ACTION_EXAM = EppControlActionType.getFullCode(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM);
    public static final String FULL_CODE_CONTROL_ACTION_SETOFF = EppControlActionType.getFullCode(EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF);
    public static final String FULL_CODE_CONTROL_ACTION_SETOFF_DIFF = EppControlActionType.getFullCode(EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF);
    public static final String FULL_CODE_CONTROL_ACTION_COURSE_WORK = EppControlActionType.getFullCode(EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK);
    public static final String FULL_CODE_CONTROL_ACTION_COURSE_PROJECT = EppControlActionType.getFullCode(EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT);
    public static final String FULL_CODE_CONTROL_ACTION_EXAM_ACCUM = EppControlActionType.getFullCode(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM);
    public static final String FULL_CODE_CONTROL_ACTION_CONTROL_WORK = EppControlActionType.getFullCode(EppFControlActionTypeCodes.CONTROL_ACTION_CONTROL_WORK);


    protected abstract int getClassComparatorPriority();
    protected abstract int getInClassComparatorPriority();

    public static Comparator<EppControlActionType> COMPARATOR = (o1, o2) -> {

        // сначала по классам (сначала по приоритетам)
        int diff;
        if (0 != (diff = (o1.getClassComparatorPriority() - o2.getClassComparatorPriority()))) { return diff; }

        // затем по названию классов, если они разные
        if (o1.getClass().equals(o2.getClass()))
        {
            // затем внутри класса, затем по коду справочника
            if (0 != (diff = (o1.getInClassComparatorPriority() - o2.getInClassComparatorPriority()))) { return diff; }
            return o1.getTitle().compareTo(o2.getTitle());

        }

        // если классы разные - то по имени класса
        return o1.getClass().getName().compareTo(o2.getClass().getName());
    };


    @Override
    public String getFullCode()
    {
        return EppControlActionType.getFullCode(this.getCode());
    }

    public static String getFullCode(final String code)
    {
        return "action." + code;
    }

    public boolean isFlagValue()
    {
        return false;
    }

    public static Predicate<EppControlActionType> getPredicate(final EppRegistryStructure eppRegistryStructure) {
        if (null != eppRegistryStructure)
        {
            if (eppRegistryStructure.isDisciplineElement()) {
                return EppControlActionTypeGen::isUsedWithDisciplines;
            } else if (eppRegistryStructure.isPracticeElement()) {
                return EppControlActionTypeGen::isUsedWithPractice;
            } else if (eppRegistryStructure.isAttestationElement()) {
                return EppControlActionTypeGen::isUsedWithAttestation;
            }
            return eppControlActionType -> false;
        }
        return eppControlActionType -> (
                eppControlActionType.isUsedWithDisciplines() ||
                eppControlActionType.isUsedWithPractice() ||
                eppControlActionType.isUsedWithAttestation()
        );
    }
}