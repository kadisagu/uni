package ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.ui.InWizardStep;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.InWizardPromicesStep.InWizardPromicesUI;

import ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.ui.SectionPromice.EppCtrEducationPromiceSectionPromice;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;

/**
 * @author vdanilov
 */
public class EppCtrEducationPromiceInWizardStepUI extends InWizardPromicesUI<EppCtrEducationPromice>
{
    @Override
    protected Class<? extends BusinessComponentManager> promiceComponent() {
        return EppCtrEducationPromiceSectionPromice.class;
    }
}