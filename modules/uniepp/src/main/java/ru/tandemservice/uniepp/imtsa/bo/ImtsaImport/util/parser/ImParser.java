/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.parser;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.ImParseResult;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.ImtsaImportDefines;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.ImtsaUtils;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.dto.*;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.node.ImElement;

import javax.validation.constraints.NotNull;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes.FGOS;
import static ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes.FGOS_2013;

/**
 * @author azhebko
 * @since 27.10.2014
 */
public class ImParser implements IImParser
{
    public Document buildXmlDocument(InputStream input) throws ParserConfigurationException, IOException, SAXException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        return db.parse(input);
    }

    public ImElement buildImElement(Node node)
    {
        ImElement result = new ImElement("root");
        elements(result, node.getChildNodes());

        return result;
    }

    private static void elements(ImElement parent, NodeList childNodeList)
    {
        for (int i = 0; i < childNodeList.getLength(); i++)
        {
            Node node = childNodeList.item(i);
            if (node instanceof Element)
            {
                ImElement element = new ImElement(((Element) node).getTagName());
                attributes(element, node.getAttributes());
                elements(element, node.getChildNodes());
                parent.addChildElement(element);
            }
        }
    }

    private static void attributes(ImElement parent, NamedNodeMap attributes)
    {
        for (int i = 0; i < attributes.getLength(); i++)
        {
            Attr attribute = (Attr) attributes.item(i);
            parent.setAttribute(attribute.getName(), attribute.getValue());
        }
    }


    public ImParseResult parse(ImElement root)
    {
        ImParseResult parseResult = new ImParseResult();

        ImElement plan = root.child("Документ").child("План");
        ImElement title = plan.child("Титул");

        String planStructureCode = getPlanStructureCode(title);
        parseResult.setPlanStructureCode(planStructureCode);
        parseResult.setProgramFormCode(getProgramFormCode(plan.getString("ФормаОбучения").toLowerCase()));
        parseResult.setEduProgramDuration(getEduProgramDuration(title));
        parseResult.setCycleAbbr2CycleTitleMap(getCycleAbbr2CycleTitleMap(title));


        Pattern emptyGraph = Pattern.compile("^=+$");
        Map<Integer, ImElement> courses = new HashMap<>();
        Collection<ImElement> graphCourses = title.child("ГрафикУчПроцесса").childList("Курс");
        int lastCourse = 0;
        for (ImElement course : graphCourses)
        {
            String graph = course.getString("График");
            Integer courseNumber = course.getInteger("Ном");

            if (courseNumber == null) continue;

            if (emptyGraph.matcher(graph).matches()) continue;

            courses.put(courseNumber, course);
            if (lastCourse < courseNumber) lastCourse = courseNumber;
        }
        parseResult.setCourses(new ArrayList<>(courses.keySet()));
        parseResult.setLastCourse(lastCourse);

        Integer sessionInCourse = title.getInteger("СессийНаКурсе");
        Integer termInCourse = title.getInteger("СеместровНаКурсе");
        parseResult.setCoursePartsNumber(EduProgramFormCodes.ZAOCHNAYA.equals(parseResult.getProgramFormCode()) ? sessionInCourse : termInCourse);

        Map<String, String> iControlActionCodeMap = DataAccessServices.dao().getList(EppIControlActionType.class)
                .stream().collect(Collectors.toMap(EppControlActionType::getTitle, EppControlActionType::getFullCode));

        ImParseRowsContext context = new ImParseRowsContext(parseResult, sessionInCourse, iControlActionCodeMap);
        context._termInCourse = termInCourse;
        context._detailGIA = title.getInteger("DetailGIA");

        ImEpvRowDTO rootRow = new ImEpvRowDTO() {}; // мнимый корень
        context._idRowMap.put(null, rootRow);

        Collection<ImElement> rows = plan.child("СтрокиПлана").childList("Строка");
        ImElement prevRow = null;

        context.fillCycles();
        for (ImElement row : rows)
        {
            context.planRowDTO(row, prevRow);
            prevRow = row;
        }

        Collection<ImEpvRowDTO> rootRows = rootRow.getChildList(); // корневые строки (без парента)
        parseResult.setRootRows(rootRows);

        Pattern structurePattern = Pattern.compile("^Б(\\d+)$");
        int cyclesLastNumber = 0;
        for (ImEpvRowDTO rowDTO : rootRows)
        {
            if (rowDTO instanceof ImEpvStructureRowDTO)
            {
                ImEpvStructureRowDTO structureRowDTO = (ImEpvStructureRowDTO) rowDTO;
                Matcher matcher = structurePattern.matcher(structureRowDTO.getValue());
                if (matcher.matches())
                {
                    int candidate = Integer.valueOf(matcher.group(1));
                    if (candidate > cyclesLastNumber)
                        cyclesLastNumber = candidate;
                }
            }
        }

        addPractices(context, plan);
        addNIR(context, plan);
        addFinalStateAtestations(context, plan, title, courses);

        completeData(rootRows);

        return parseResult;
    }


    private void addStateExamAndDiploma(ImEpvStructureRowDTO examStructureRow, ImElement title, Map<Integer, Integer> termCountMap, String number, boolean stateExam)
    {
        if (!termCountMap.isEmpty())
        {
            Integer partitionNumber = title.getInteger("ЭлементовВНеделе");
            Double hoursInZet = title.getDouble("ИГА_ЧасовВЗЕТ");
            Double zetInWeek = title.getDouble("ИГА_ЗЕТвНеделе");
            String ownerCode = title.getString("ИГА_Кафедра");
            ownerCode = ownerCode != null ? ownerCode : title.getString("КодКафедры");

            ImEpvRegistryRowDTO row = new ImEpvRegistryRowDTO();
            row.setRegistryElementType(stateExam ? EppRegistryStructureCodes.REGISTRY_ATTESTATION_EXAM : EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA);
            row.setTitle(stateExam ? "Государственный экзамен" : "Защита выпускной квалификационной работы");
            row.setNumber(number);
            row.setRegistryElementOwner(ownerCode);
            row.setStoredIndex(examStructureRow.getStoredIndex() + "." + row.getNumber());
            row.setUserIndex(examStructureRow.getStoredIndex() + "." + row.getNumber());

            ImParser.loadHours(row.getRowLoadMap(), ImtsaImportDefines.L_LECTURES, null, null, null);
            ImParser.loadHours(row.getRowLoadMap(), ImtsaImportDefines.L_PRACTICE, null, null, null);
            ImParser.loadHours(row.getRowLoadMap(), ImtsaImportDefines.L_LABS, null, null, null);
            ImParser.rowHours(row.getRowLoadMap(), null, null, null, null, null, null, null);

            for (Map.Entry<Integer, Integer> entry : termCountMap.entrySet())
            {
                Integer term = entry.getKey();
                Integer charCount = entry.getValue();

                Map<String, Double> loadMap = new TreeMap<>();

                double totalWeeks = Math.round(100d * charCount / partitionNumber) / 100d;
                double totalSize = Math.round(100d * charCount * hoursInZet * zetInWeek / partitionNumber) / 100d;
                double totalLabor = Math.round(100d * charCount * zetInWeek / partitionNumber) / 100d;
                double audit = 0d;
                double selfWork = totalSize;
                double control = 0d;
                double controlE = 0d;

                rowHours(loadMap, totalSize, totalLabor, totalWeeks, audit, selfWork, control, controlE);

                Map<String, Integer> actionMap = stateExam ? buildActionMap(1, null, null, null, null) : buildActionMapByNode(null, null);
                if (row.getRowTermLoadMap().put(term, loadMap) != null | row.getRowTermActionMap().put(term, actionMap) != null)
                    throw new IllegalArgumentException(); // неверная структура ИМЦА, один и тот же семестр встречается два раза

                ImtsaUtils.addValue(row.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_LABOR, totalLabor);
                ImtsaUtils.addValue(row.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_WEEKS, totalWeeks);
                ImtsaUtils.addValue(row.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_SIZE, totalSize);
                ImtsaUtils.addValue(row.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_AUDIT, audit);
                ImtsaUtils.addValue(row.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_SELF_WORK, selfWork);
                ImtsaUtils.addValue(row.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_CONTROL, control);
                ImtsaUtils.addValue(row.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_CONTROL_E, controlE);
            }
            examStructureRow.addChild(row);
        }
    }

    private static void completeData(Collection<ImEpvRowDTO> rows)
    {
        int number = 0;
        for (ImEpvRowDTO row: rows)
        {
            if (row instanceof ImEpvGroupReRowDTO)
            {
                ((ImEpvGroupReRowDTO) row).setNumber(StringUtils.leftPad(String.valueOf(++number), 2, '0'));

            } else if (row instanceof ImEpvTermDistributedRowDTO)
            {
                ((ImEpvTermDistributedRowDTO) row).setNumber(StringUtils.leftPad(String.valueOf(++number), 2, '0'));

                if (row instanceof ImEpvGroupImRowDTO)
                    fillEpvGroupImRow((ImEpvGroupImRowDTO) row);
            }

            completeData(row.getChildList());
        }
    }

    private static void fillEpvGroupImRow(ImEpvGroupImRowDTO groupImRow)
    {
        if (groupImRow.getChildList().isEmpty())
            return;

        ImEpvRegistryRowDTO firstborn = ((ImEpvRegistryRowDTO) groupImRow.getChildList().iterator().next());
        groupImRow.getRowLoadMap().putAll(firstborn.getRowLoadMap());

        for (Map.Entry<Integer, Map<String, Double>> le: firstborn.getRowTermLoadMap().entrySet())
            groupImRow.getRowTermLoadMap().put(le.getKey(), new TreeMap<>(le.getValue()));

        for (Map.Entry<Integer, Map<String, Integer>> ae: firstborn.getRowTermActionMap().entrySet())
            groupImRow.getRowTermActionMap().put(ae.getKey(), new TreeMap<>(ae.getValue()));
    }

    private static class ImParseRowsContext
    {
        private final static Map<Integer, Map<String, Integer>> sessionNumber2partNumberMap = ImmutableMap.<Integer, Map<String, Integer>>of(
            1, ImmutableMap.of("3", 1),
            2, ImmutableMap.of("2", 1, "3", 2),
            3, ImmutableMap.of("1", 1, "2", 2, "3", 3),
            4, ImmutableMap.of("1", 1, "2", 2, "3", 3, "4", 4));

        private ImParseResult _parseResult;
        private final Integer _sessionInCourse;
        private Integer _termInCourse;
        private Integer _detailGIA;
        private Map<String, String> _iControlActionCodeMap;
        private Map<String, ImEpvRowDTO> _idRowMap = new HashMap<>();
        private Map<String, String> _cyclesIndexByTitleMap = new HashMap<>();// для поиска блока практик, НИР и ГИА

        public ImParseRowsContext(ImParseResult parseResult, Integer sessionInCourse, Map<String, String> iControlActionCodeMap)
        {
            _parseResult = parseResult;
            _sessionInCourse = sessionInCourse;
            _iControlActionCodeMap = iControlActionCodeMap;
        }

        private Integer resolvePartNumber(String sessionNumberStr)
        {
            if (sessionNumberStr == null)
                throw new IllegalStateException();

            Map<String, Integer> termNumber = sessionNumber2partNumberMap.get(_sessionInCourse);
            if (null == termNumber || null == termNumber.get(sessionNumberStr))
                throw new ApplicationException("В файле неверно указано количество сессий на курсе.");

            return termNumber.get(sessionNumberStr);
        }

        protected void fillCycles(){
            _parseResult.getCycleAbbr2CycleTitleMap()
                    .forEach((index, name) ->
                             {
                                 structureRowDTO(index, name);
                                 _cyclesIndexByTitleMap.put(StringUtils.trimToEmpty(name).toLowerCase(), index);
                             });
        }

        // строка плана (запись в ИМЦА) - дисциплина/ группа дисциплин
        private void planRowDTO(ImElement rowNode, ImElement prevRow)
        {
            if (StringUtils.isNotEmpty(rowNode.getString("ТипПрактики")))
                return; // на данный момент практики не обрабатываются

            ImEpvRowDTO result;
            String index = rowNode.getString("НовИдДисциплины");
            String newCycle = rowNode.getString("НовЦикл");
            String idDiscipline = rowNode.getString("ИдетификаторДисциплины");
            String structureCode = _parseResult.getPlanStructureCode();
            switch (structureCode)
            {
                case FGOS:
                {
                    if (index == null || index.equals(newCycle)) index = idDiscipline;
                    break;
                }
                case FGOS_2013:
                {
                    String beStudied = rowNode.getString("ПодлежитИзучению");
                    if (null == index && null == newCycle && null == beStudied) return;
                    break;
                }
                default: throw new IllegalStateException();
            }

            if(index == null)
            {
                if (prevRow == null) return;

                String prevId = prevRow.getString("НовИдДисциплины");
                String prevIdNumber = prevId.substring(prevId.lastIndexOf(".") + 1, prevId.length());
                Integer number = Integer.parseInt(prevIdNumber) + 1;
                index = prevId.substring(0, prevId.lastIndexOf(prevIdNumber)) + number;
            }

            String name = rowNode.getString("Дис");
            boolean groupReRow = rowNode.getBooleanWithNullAsFalse("ДисциплинаДляРазделов");
            if (groupReRow) result = new ImEpvGroupReRowDTO(name);
            else result = registryRowDTO(rowNode, name);

            result.setStoredIndex(index);
            result.setUserIndex(index);

            if (result instanceof ImEpvRegistryRowDTO && null != _detailGIA && _detailGIA == 1 && _parseResult.getPlanStructureCode().equals(FGOS_2013))
            {
                Matcher matcher = STATE_EXAM_PATTERN.matcher(index);
                if (matcher.matches())
                {
                    ImEpvRegistryRowDTO regRow = (ImEpvRegistryRowDTO) result;
                    regRow.setRegistryElementType(EppRegistryStructureCodes.REGISTRY_ATTESTATION_EXAM);
                }
            }

            _idRowMap.put(index, result);

            String parentId = ids(index)[0];
            ImEpvRowDTO parent = groupReRow ? structureRowDTO(parentId, null) : parentRowDTO(parentId, rowNode.getString("BlockName"));
            parent.addChild(result);
        }

        // дисциплина
        private ImEpvRegistryRowDTO registryRowDTO(ImElement rowNode, String name)
        {
            ImEpvRegistryRowDTO result = new ImEpvRegistryRowDTO();
            result.setTitle(name);

            result.setRegistryElementOwner(rowNode.getString("Кафедра"));
            result.setRegistryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE);

            loadHours(result.getRowLoadMap(), ImtsaImportDefines.L_LECTURES, null, null, null);
            loadHours(result.getRowLoadMap(), ImtsaImportDefines.L_PRACTICE, null, null, null);
            loadHours(result.getRowLoadMap(), ImtsaImportDefines.L_LABS, null, null, null);
            rowHours(result.getRowLoadMap(), null, null, null, null, null, null, null);

            if (null == _sessionInCourse) // очная | очно-заочная
            {
                for (final ImElement termNode : rowNode.childList("Сем"))
                {
                    int termNumber = termNode.getInteger("Ном");

                    Collection<ImElement> vzs = termNode.childList("VZ");
                    if (vzs.isEmpty())
                    {
                        Double lec = termNode.getDouble("Лек");
                        Double pr = termNode.getDouble("Пр");
                        Double labs = termNode.getDouble("Лаб");

                        Map<String, Double> loadMap = new TreeMap<>();

                        loadHours(loadMap, ImtsaImportDefines.L_LECTURES, lec, null, null);
                        loadHours(loadMap, ImtsaImportDefines.L_PRACTICE, pr, null, null);
                        loadHours(loadMap, ImtsaImportDefines.L_LABS, labs, null, null);

                        Double audit = ImtsaUtils.safeSum(lec, pr, labs);
                        Double selfWork = termNode.getDouble("СРС");
                        Double totalSize = ImtsaUtils.safeSum(audit, selfWork);
                        Double totalLabor = termNode.getDouble("ЗЕТ");

                        rowHours(loadMap, totalSize, totalLabor, null, audit, selfWork, null, null);

                        if (result.getRowTermLoadMap().put(termNumber, loadMap) != null | result.getRowTermActionMap().put(termNumber, buildActionMapByNode(termNode, _iControlActionCodeMap)) != null)
                            throw new IllegalArgumentException(); // неверная структура ИМЦА, один и тот же семестр встречается два раза

                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_LECTURES, lec);
                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_PRACTICE, pr);
                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_LABS, labs);

                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_LECTURES_I, null);
                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_PRACTICE_I, null);
                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_LABS_I, null);

                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_LECTURES_E, null);
                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_PRACTICE_E, null);
                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_LABS_E, null);

                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_AUDIT, audit);
                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_SELF_WORK, selfWork);
                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_CONTROL, null);
                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_CONTROL_E, null);

                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_SIZE, totalSize);
                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_LABOR, totalLabor);
                        ImtsaUtils.addValue(result.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_WEEKS, null);


                    } else
                    {
                        if (result.getRowTermActionMap().put(termNumber, buildActionMapByNode(termNode, _iControlActionCodeMap)) != null)
                            throw new IllegalArgumentException(); // неверная структура ИМЦА, один и тот же семестр встречается два раза

                        updateVZHolderLoad(result.getRowLoadMap(), result.getRowTermLoadMap(), termNode.getDouble("ЗЕТ"), termNumber, vzs);
                    }
                }

            } else
            {
                for (ImElement courseNode: rowNode.childList("Курс"))
                {
                    Integer courseNumber = courseNode.getInteger("Ном");
                    for (ImElement sessionNode: courseNode.childList("Сессия"))
                    {
                        int termNumber = (courseNumber - 1) * _sessionInCourse + resolvePartNumber(sessionNode.getString("Ном"));
                        if (courseNumber.equals(_parseResult.getLastCourse()))
                        {
                            List<Integer> sessions = _parseResult.getLastCourseSessionNumbers();
                            if (!sessions.contains(termNumber)) sessions.add(termNumber);
                        }

                        if (result.getRowTermActionMap().put(termNumber, buildActionMapByNode(sessionNode, _iControlActionCodeMap)) != null)
                            throw new IllegalArgumentException(); // неверная структура ИМЦА, один и тот же семестр встречается два раза

                        updateVZHolderLoad(result.getRowLoadMap(), result.getRowTermLoadMap(), sessionNode.getDouble("ЗЕТ"), termNumber, sessionNode.childList("VZ"));
                    }
                }
            }

            {
                result.getRowLoadMap().put(ImtsaImportDefines.L_TOTAL_SIZE, rowNode.getDouble("ПодлежитИзучению"));
                result.getRowLoadMap().put(ImtsaImportDefines.L_TOTAL_LABOR, rowNode.getDouble("КредитовНаДисциплину"));
                result.getRowLoadMap().put(ImtsaImportDefines.L_TOTAL_CONTROL_E, rowNode.getDouble("ЧасовДист"));
            }

            return result;
        }

        private void updateVZHolderLoad(Map<String, Double> rowLoadMap, Map<Integer, Map<String, Double>> rowTermLoadMap, Double totalLabor, int termNumber, Collection<ImElement> vzs)
        {
            Map<String, Double> loadMap = new TreeMap<>();
            Map<String, Map<String, Double>> vzFullMap = new HashMap<>();
            for (ImElement vz : vzs)
            {
                Map<String, Double> vzMap = new HashMap<>();
                vzMap.put("H", vz.getDouble("H"));
                vzMap.put("IntH", vz.getDouble("IntH"));
                vzMap.put("Dist", vz.getDouble("Dist"));

                if (vzFullMap.put(vz.getString("ID"), vzMap) != null)
                    throw new IllegalArgumentException(); // неверная структура ИМЦА, один и тот же вид нагрузки встречается два раза
            }

            Map<String, Double> lectureMap = SafeMap.safeGet(vzFullMap, "101", HashMap.class);
            Map<String, Double> practiceMap = SafeMap.safeGet(vzFullMap, "103", HashMap.class);
            Map<String, Double> labsMap = SafeMap.safeGet(vzFullMap, "102", HashMap.class);

            Double lec = lectureMap.get("H");
            Double pr = practiceMap.get("H");
            Double labs = labsMap.get("H");

            Double lecI = lectureMap.get("IntH");
            Double prI = practiceMap.get("IntH");
            Double labsI = labsMap.get("IntH");

            Double lecE = lectureMap.get("Dist");
            Double prE = practiceMap.get("Dist");
            Double labsE = labsMap.get("Dist");

            loadHours(loadMap, ImtsaImportDefines.L_LECTURES, lec, lecI, lecE);
            loadHours(loadMap, ImtsaImportDefines.L_PRACTICE, pr, prI, prE);
            loadHours(loadMap, ImtsaImportDefines.L_LABS, labs, labsI, labsE);

            Double kcp = SafeMap.safeGet(vzFullMap, "106", HashMap.class).get("H");
            Double cpc = SafeMap.safeGet(vzFullMap, "107", HashMap.class).get("H");

            Map<String, Double> controlMap = SafeMap.safeGet(vzFullMap, "108", HashMap.class);
            Double control = controlMap.get("H");
            Double controlE = controlMap.get("Dist");

            Double audit = ImtsaUtils.safeSum(lec, pr, labs);
            Double selfWork = ImtsaUtils.safeSum(kcp, control, cpc);
            Double totalSize = ImtsaUtils.safeSum(audit, selfWork);
            Double totalWeeks = null;

            rowHours(loadMap, totalSize, totalLabor, totalWeeks, audit, selfWork, control, controlE);

            if (rowTermLoadMap.put(termNumber, loadMap) != null)
                throw new IllegalArgumentException(); // неверная структура ИМЦА, один и тот же семестр встречается два раза

            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_LECTURES, lec);
            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_PRACTICE, pr);
            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_LABS, labs);

            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_LECTURES_I, lecI);
            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_PRACTICE_I, prI);
            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_LABS_I, labsI);

            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_LECTURES_E, lecE);
            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_PRACTICE_E, prE);
            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_LABS_E, labsE);

            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_TOTAL_AUDIT, audit);
            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_TOTAL_SELF_WORK, selfWork);
            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_TOTAL_CONTROL, control);
            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_TOTAL_CONTROL_E, controlE);

            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_TOTAL_SIZE, totalSize);
            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_TOTAL_LABOR, totalLabor);
            ImtsaUtils.addValue(rowLoadMap, ImtsaImportDefines.L_TOTAL_WEEKS, totalWeeks);
        }

        // дисциплина по выбору, структурная строка
        private ImEpvRowDTO parentRowDTO(String index, String name)
        {
            ImEpvRowDTO result = _idRowMap.get(index);
            if (result != null)
                return result;

            Matcher m = GROUP_IM_LOCAL_ID_PATTERN.matcher(index);

            if (m.matches())
            {
                if (StringUtils.isEmpty(name))
                    name = "Выбор " + m.group(1);

                result = new ImEpvGroupImRowDTO(name);
                _idRowMap.put(index, result);

                String[] ids = ids(index); // { parent index, local index }
                structureRowDTO(ids[0], null).addChild(result);
            }
            else
            {
                result = structureRowDTO(index, name);
            }

            result.setStoredIndex(index);
            result.setUserIndex(index);

            return result;
        }

        // структурная строка
        private ImEpvStructureRowDTO structureRowDTO(String index, String name)
        {
            if (index == null) return null;

            ImEpvRowDTO result = _idRowMap.get(index);
            if (result != null && result instanceof ImEpvStructureRowDTO)
            {
                if (name != null) result.setTitle(name);
                return (ImEpvStructureRowDTO) result;
            }

            String[] ids = ids(index); // { parent index, local index }
            result = new ImEpvStructureRowDTO(ids[1]);
            result.setTitle(name);

            _idRowMap.put(index, result);

            ImEpvStructureRowDTO parent = structureRowDTO(ids[0], null);
            if (parent != null) parent.addChild(result);
            else _idRowMap.get(null).addChild(result);

            result.setStoredIndex(index);
            result.setUserIndex(index);

            if (StringUtils.isEmpty(name))
            {

                String partsBase = null;
                String partsVariation = null;
                switch (_parseResult.getPlanStructureCode())
                {
                    case FGOS:
                        partsBase = EppPlanStructureCodes.FGOS_PARTS_BASE;
                        partsVariation = EppPlanStructureCodes.FGOS_PARTS_VARIATIVE;
                        break;
                    case FGOS_2013:
                        partsBase = EppPlanStructureCodes.FGOS_2013_PARTS_BASE;
                        partsVariation = EppPlanStructureCodes.FGOS_2013_PARTS_VARIATIVE;
                        break;
                }
                String nextPart = null;
                int partIndex = index.lastIndexOf(".");
                if (partIndex >= 0 && partIndex < index.length())
                    nextPart = index.substring(partIndex + 1, index.length());
                if (StringUtils.isNotEmpty(nextPart))
                    switch (nextPart)
                    {
                        case "Б":
                            name = CatalogManager.instance().dao().getCatalogItem(EppPlanStructure.class, partsBase).getTitle();
                            break;
                        case "В":
                            name = CatalogManager.instance().dao().getCatalogItem(EppPlanStructure.class, partsVariation).getTitle();
                            break;
                        case "ОД":
                            name = "Обязательные дисциплины";
                            break;
                        case "ДВ":
                            name = "Дисциплины по выбору";
                            break;
                    }
            }

            result.setTitle(name);

            return (ImEpvStructureRowDTO)result;
        }

        private static final Pattern GROUP_IM_LOCAL_ID_PATTERN = Pattern.compile(".*ДВ\\.?(\\d+)$");
        private static final Pattern STATE_EXAM_PATTERN = Pattern.compile("^Б(\\d+)\\.Г.*");

        // id -> parent id, local id
        private String[] ids(String id)
        {
            String parentId = null;
            String localId = id;

            int ind = id.lastIndexOf(".");

            if (ind > 0)
            {
                parentId = id.substring(0, ind);
                localId = id.substring(ind + 1);
            }

            return new String[]{parentId, localId};
        }
    }

    public static void loadHours(@NotNull Map<String, Double> loadMap, @NotNull String fullCode, Double hours, Double iHours, Double eHours)
    {
        Preconditions.checkNotNull(loadMap);
        Preconditions.checkNotNull(fullCode);
        loadMap.put(fullCode, hours);
        loadMap.put(EppALoadType.iFullCode(fullCode), iHours);
        loadMap.put(EppALoadType.eFullCode(fullCode), eHours);
    }

    public static void rowHours(@NotNull Map<String, Double> loadMap, Double hours, Double labor, Double weeks, Double audit, Double selfwork, Double control, Double controlE)
    {
        Preconditions.checkNotNull(loadMap);
        loadMap.put(ImtsaImportDefines.L_TOTAL_SIZE, hours);
        loadMap.put(ImtsaImportDefines.L_TOTAL_LABOR, labor);
        loadMap.put(ImtsaImportDefines.L_TOTAL_WEEKS, weeks);
        loadMap.put(ImtsaImportDefines.L_TOTAL_AUDIT, audit);
        loadMap.put(ImtsaImportDefines.L_TOTAL_SELF_WORK, selfwork);
        loadMap.put(ImtsaImportDefines.L_TOTAL_CONTROL, control);
        loadMap.put(ImtsaImportDefines.L_TOTAL_CONTROL_E, controlE);
    }

    public static Map<String, Integer> buildActionMap(final Integer examValue, final Integer setoffValue, final Integer setoffDiffValue, final Integer cwValue, final Integer cpValue)
    {
        return Collections.unmodifiableMap(new TreeMap<String, Integer>()
        {
            {
                put(ImtsaImportDefines.CA_EXAM, examValue);
                put(ImtsaImportDefines.CA_SETOFF, setoffValue);
                put(ImtsaImportDefines.CA_SETOFF_DIFF, setoffDiffValue);
                put(ImtsaImportDefines.CA_COURSE_WORK, cwValue);
                put(ImtsaImportDefines.CA_COURSE_PROJECT, cpValue);
            }
        });
    }
    public static Map<String, Integer> buildActionMapByNode(ImElement actionNode, final Map<String, String> iControlActionCodeMap)
    {
        if (null == actionNode)
        {
            return Collections.unmodifiableMap(new TreeMap<String, Integer>()
            {
                {
                    put(ImtsaImportDefines.CA_EXAM, null);
                    put(ImtsaImportDefines.CA_SETOFF, null);
                    put(ImtsaImportDefines.CA_SETOFF_DIFF, null);
                    put(ImtsaImportDefines.CA_COURSE_WORK, null);
                    put(ImtsaImportDefines.CA_COURSE_PROJECT, null);
                }
            });
        }
        else
        {
            // Формы итогового контроля
            final Integer examValue = actionNode.getInteger("Экз");
            final Integer setoffValue = actionNode.getInteger("Зач");
            final Integer setoffDiffValue = actionNode.getInteger("ЗачО");
            final Integer cwValue = actionNode.getInteger("КР");
            final Integer cpValue = actionNode.getInteger("КП");

            // Формы текущего контроля
            final Integer referat = actionNode.getInteger("Реф");
            final Integer essay = actionNode.getInteger("Эссе");
            final Integer controlWork = actionNode.getInteger("КонтрРаб");
            final Integer graphWork = actionNode.getInteger("РГР");
            final Integer markRating = actionNode.getInteger("Оценка");

            return Collections.unmodifiableMap(new TreeMap<String, Integer>()
            {
                {
                    put(ImtsaImportDefines.CA_EXAM, examValue);
                    put(ImtsaImportDefines.CA_SETOFF, setoffValue);
                    put(ImtsaImportDefines.CA_SETOFF_DIFF, setoffDiffValue);
                    put(ImtsaImportDefines.CA_COURSE_WORK, cwValue);
                    put(ImtsaImportDefines.CA_COURSE_PROJECT, cpValue);

                    if (null != iControlActionCodeMap)
                    {
                        putByTitle("Реферат", referat);
                        putByTitle("Эссе", essay);
                        putByTitle("Контрольная работа", controlWork);
                        putByTitle("Расчетно-графическая работа", graphWork);
                        putByTitle("Оценки по рейтингу", markRating);
                    }
                }

                private void putByTitle(final String title, final Integer value)
                {
                    String code = iControlActionCodeMap.get(title);
                    if (null != code && null != value) put(code, value);
                }
            });
        }
    }


    protected String getPlanStructureCode(ImElement title)
    {
        String eduStandardType = title.getString("ТипГОСа");
        if (null == eduStandardType)
            throw new ApplicationException("В файле не указан «ТипГОСа».");

        String code;
        switch (eduStandardType)
        {
            case "3":
                code = FGOS;
                break;
            case "3.5":
                code = FGOS_2013;
                break;
            default:
                throw new ApplicationException("В файле для тега «ТипГОСа» задано неверное значение «" + eduStandardType + "».");
        }

        return code;
    }

    protected String getProgramFormCode(String value)
    {
        switch (value)
        {
            case "очная":
                return EduProgramFormCodes.OCHNAYA;
            case "заочная":
                return EduProgramFormCodes.ZAOCHNAYA;
            case "очно-заочная":
                return EduProgramFormCodes.OCHNO_ZAOCHNAYA;
            default:
                throw new ApplicationException("В файле неверно указан форма обучения.");
        }
    }

    protected Map<String, String> getCycleAbbr2CycleTitleMap(ImElement title)
    {
        Map<String, String> cycleAbbr2CycleTitleMap = new HashMap<>();
        ImElement cyclesElement = title.child("АтрибутыЦикловНов");
        if (null == cyclesElement)
            throw new ApplicationException("В файле не указаны Атрибуты Циклов.");

        Collection<ImElement> cycles = cyclesElement.childList("Цикл");
        for (ImElement cycle : cycles)
        {
            String cycleTitle = cycle.getString("Название");
            if (cycleTitle == null) continue;

            String cycleAbbr = cycle.getString("Аббревиатура");
            if (cycleAbbr == null) continue;

            int i1 = cycleTitle.indexOf("«");
            int i2 = cycleTitle.indexOf("»");
            if (i1 >= 0 && i2 >= 0 && i2 > i1)
                cycleTitle = cycleTitle.substring(i1 + 1, i2); //если есть кавычки то название берется из них

            cycleAbbr2CycleTitleMap.put(cycleAbbr, cycleTitle);
        }

        if (cycleAbbr2CycleTitleMap.isEmpty()) throw new ApplicationException("В файле не указаны Атрибуты Циклов.");

        return cycleAbbr2CycleTitleMap;
    }

    protected PairKey<Integer, Integer> getEduProgramDuration(ImElement title)
    {
        ImElement tagQualifications = title.child("Квалификации");
        if (null == tagQualifications) throw new ApplicationException("В файле не указана Квалификация.");

        Collection<ImElement> qualifications = tagQualifications.childList("Квалификация");
        if (CollectionUtils.isEmpty(qualifications)) throw new ApplicationException("В файле не указана Квалификация.");

        ImElement qualification = qualifications.iterator().next();
        String eduProgramDuration = qualification.getString("СрокОбучения");
        Integer yearIndex = null;
        int parseDurationYear = 0, parseDurationMonth = 0;

        try
        {
            if (eduProgramDuration.contains("г") || eduProgramDuration.contains("л"))
            {
                yearIndex = eduProgramDuration.contains("г") ? eduProgramDuration.indexOf("г") : eduProgramDuration.indexOf("л");
                parseDurationYear = Integer.parseInt(eduProgramDuration.substring(0, yearIndex));
            }
            if (eduProgramDuration.contains("м"))
                parseDurationMonth = Integer.parseInt(eduProgramDuration.substring(yearIndex == null ? 0 : yearIndex + 2, eduProgramDuration.indexOf("м")));
        }
        catch (Throwable t)
        {
            throw new ApplicationException("В файле неверно указан срок обучения.");
        }

        return PairKey.create(parseDurationYear, parseDurationMonth);
    }


    private void addPractices(ImParseRowsContext context, ImElement plan)
    {
        String[] indexAndName = getIndexAndName(context,
                                                Lists.newArrayList(EppPlanStructureCodes.FGOS_CYCLES_PRACTICE,
                                                                   EppPlanStructureCodes.FGOS_CYCLES_INSTRUCTIONAL_AND_WORKING_PRACTICE,
                                                                   EppPlanStructureCodes.FGOS_CYCLES_INSTRUCTIONAL_AND_WORKING_PRACTICE_RESEARCH_WORK,
                                                                   EppPlanStructureCodes.FGOS_CYCLES_WORKING_PRACTICE,
                                                                   EppPlanStructureCodes.FGOS_CYCLES_PRACTICE,
                                                                   EppPlanStructureCodes.FGOS_CYCLES_PRACTICE_AND_RESEARCH_WORK,
                                                                   EppPlanStructureCodes.FGOS_CYCLES_INSTRUCTIONAL_PRACTICE,
                                                                   EppPlanStructureCodes.FGOS_CYCLES_WORKING_PROFILE_PRACTICE,
                                                                   EppPlanStructureCodes.FGOS_CYCLES_INSTRUCTIONAL_PRACTICE_WORKING_STUDY,
                                                                   EppPlanStructureCodes.FGOS_CYCLES_INSTRUCTIONAL_AND_WORKING_PRACTICE_SUPERVISION_RESEARCH_WORK),
                                                Lists.newArrayList(EppPlanStructureCodes.FGOS_2013_PRACTICE, EppPlanStructureCodes.FGOS_2013_PRACTICE_2));
        String index = indexAndName[0];
        String name = indexAndName[1];
        if(index == null) return;

        ImEpvRowDTO structureRow = context._idRowMap.get(index);
        if(structureRow == null)
            structureRow = context.structureRowDTO(index, name);

        String[][] practiceKinds =
                {
                        {EppRegistryStructureCodes.REGISTRY_PRACTICE, "Практика", "Практика"},
                        {EppRegistryStructureCodes.REGISTRY_PRACTICE, "Практика", "ПрочаяПрактика"},
                        {EppRegistryStructureCodes.REGISTRY_PRACTICE_TUTORING, "УчебПрактики", "ПрочаяПрактика"},
                        {EppRegistryStructureCodes.REGISTRY_PRACTICE_PRE_OTHER, "НИР", "ПрочаяПрактика"},
                        {EppRegistryStructureCodes.REGISTRY_PRACTICE_PRODUCTION, "ПрочиеПрактики", "ПрочаяПрактика"}
                };

        for (String[] practiceKind : practiceKinds)
        {
            ImElement prKind = plan.child("СпецВидыРаботНов").child(practiceKind[1]);
            if (prKind == null)
                continue;

            parsePractices(structureRow, index, prKind.childList(practiceKind[2]), practiceKind[0], context._sessionInCourse);
        }
    }

    private void addNIR(ImParseRowsContext context, ImElement plan)
    {
        String[] indexAndName = getIndexAndName(context,
                                                Lists.newArrayList(EppPlanStructureCodes.FGOS_CYCLES_PRACTICE_AND_RESEARCH_WORK,
                                                                   EppPlanStructureCodes.FGOS_CYCLES_INSTRUCTIONAL_AND_WORKING_PRACTICE_RESEARCH_WORK,
                                                                   EppPlanStructureCodes.FGOS_CYCLES_INSTRUCTIONAL_AND_WORKING_PRACTICE_SUPERVISION_RESEARCH_WORK),
                                                Lists.newArrayList(EppPlanStructureCodes.FGOS_2013_RESEARCH_WORK, EppPlanStructureCodes.FGOS_2013_RESEARCH_WORK_2));
        String index = indexAndName[0];
        String name = indexAndName[1];
        if(index == null) return;

        ImEpvRowDTO structureRow = context._idRowMap.get(index);
        if(structureRow == null)
            structureRow = context.structureRowDTO(index, name);


        ImElement prKind = plan.child("СпецВидыРаботНов").child("НИРДиссер");
        if (prKind == null)
            return;

        parsePractices(structureRow, index, prKind.childList("НИРДиссер"), EppRegistryStructureCodes.REGISTRY_PRACTICE_PRE_OTHER, context._sessionInCourse);
    }

    private void addFinalStateAtestations(ImParseRowsContext context, ImElement plan, ImElement title, Map<Integer, ImElement> courses)
    {
        String[] indexAndName = getIndexAndName(context,
                                                Lists.newArrayList(EppPlanStructureCodes.FGOS_CYCLES_FINAL_STATE_ATTESTATION,
                                                                   EppPlanStructureCodes.FGOS_CYCLES_STATE_FINAL_ATTESTATION),
                                                Lists.newArrayList(EppPlanStructureCodes.FGOS_2013_FINAL_STATE_ATTESTATION,
                                                                   EppPlanStructureCodes.FGOS_2013_FINAL_STATE_ATTESTATION_2));
        String index = indexAndName[0];
        String name = indexAndName[1];
        if(index == null) return;

        ImEpvRowDTO structureRow = context._idRowMap.get(index);
        if(structureRow == null)
            structureRow = context.structureRowDTO(index, name);


        switch (context._detailGIA)
        {
            case 1:
                String subIndex = index + ".Д";
                ImEpvRowDTO subStructureRow = context._idRowMap.get(subIndex);
                if(subStructureRow == null )
                    subStructureRow = context.structureRowDTO(subIndex, name);


                ImElement rootPracticeKindNode = plan.child("СпецВидыРаботНов");
                if (null != rootPracticeKindNode)
                {
                    ImElement diploma = rootPracticeKindNode.child("ДиссерПодготовка");
                    if (null != diploma)
                    {
                        parsePractices(subStructureRow, subIndex, diploma.childList("ДиссерПодготовка"), EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA, context._sessionInCourse);
                        parsePractices(subStructureRow, subIndex, diploma.childList("ПрочаяПрактика"), EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA, context._sessionInCourse);// не должно быть но бывает
                    }
                }

                break;
            case 2:
                int partitionNumber = title.getInteger("ЭлементовВНеделе");
                Map<Integer, Integer> gTermCountMap = new HashMap<>();
                Map<Integer, Integer> dTermCountMap = new HashMap<>();

                courses.forEach((courseNumber, course) ->
                                {
                                    for (ImElement term : course.childList("Семестр"))
                                    {
                                        Integer termNumber = term.getInteger("Ном");
                                        Integer lastTermNumber = context._sessionInCourse != null ? courseNumber * context._sessionInCourse
                                                : ((courseNumber - 1) * context._termInCourse + termNumber); /*посл. семестр текущего курса*/

                                        int gCount = 0;
                                        int dCount = 0;

                                        for (char c : term.getString("График").toUpperCase().toCharArray())
                                        {
                                            if (c == 'Г') gCount++;
                                            if (c == 'Д') dCount++;
                                        }

                                        int totalGCount = gCount;
                                        int totalDCount = dCount;

                                        if (partitionNumber > 1)
                                        {
                                            for (int p = 2; p <= partitionNumber; p++)
                                            {
                                                String graphPart = term.getString("График" + p);
                                                if (graphPart == null)
                                                {
                                                    totalGCount += gCount;
                                                    totalDCount += dCount;

                                                }
                                                else
                                                {
                                                    for (char c : graphPart.toUpperCase().toCharArray())
                                                    {
                                                        if (c == 'Г') totalGCount++;
                                                        if (c == 'Д') totalDCount++;
                                                    }
                                                }
                                            }
                                        }
                                        if (totalGCount > 0) gTermCountMap.put(lastTermNumber, totalGCount);
                                        if (totalDCount > 0) dTermCountMap.put(lastTermNumber, totalDCount);
                                    }
                                });

                addStateExamAndDiploma((ImEpvStructureRowDTO)structureRow, title, gTermCountMap, "1", true);
                addStateExamAndDiploma((ImEpvStructureRowDTO)structureRow, title, dTermCountMap, gTermCountMap.isEmpty() ? "1" : "2", false);
                break;
        }
    }


    private void parsePractices(ImEpvRowDTO structureRow, String index, Collection<ImElement> practices, String elementTypeCode, Integer sessionInCourse)
    {
        for (ImElement practiceNode : practices)
        {
            Integer type = practiceNode.getInteger("Тип");
            ImEpvRegistryRowDTO practiceRow = new ImEpvRegistryRowDTO();
            practiceRow.setRegistryElementType(elementTypeCode);
            practiceRow.setTitle(practiceNode.getString("Наименование"));

            String rowNumberStr = String.valueOf(structureRow.getChildList().size() + 1);
            practiceRow.setNumber(rowNumberStr);
            practiceRow.setStoredIndex(index + "." + rowNumberStr);
            practiceRow.setUserIndex(index + "." + rowNumberStr);

            loadHours(practiceRow.getRowLoadMap(), ImtsaImportDefines.L_LECTURES, null, null, null);
            loadHours(practiceRow.getRowLoadMap(), ImtsaImportDefines.L_PRACTICE, null, null, null);
            loadHours(practiceRow.getRowLoadMap(), ImtsaImportDefines.L_LABS, null, null, null);
            rowHours(practiceRow.getRowLoadMap(), null, null, null, null, null, null, null);

            for (ImElement termNode : practiceNode.childList("Семестр"))
            {
                Map<String, Double> loadMap = new TreeMap<>();
                Integer term = termNode.getInteger("Ном");
                if (null != sessionInCourse)
                {
                    if (elementTypeCode.equals(EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA))
                        term *= sessionInCourse;
                    else
                        term = (term - 1) * sessionInCourse + 1;
                }

                Collection<ImElement> owners = termNode.childList("Кафедра");
                if (!owners.isEmpty())
                {
                    ImElement owner = owners.iterator().next();
                    practiceRow.setRegistryElementOwner(owner == null ? null : owner.getString("Код"));
                }
                if (null == practiceRow.getRegistryElementOwner())
                {
                    practiceRow.setRegistryElementOwner(practiceNode.getString("Кафедра"));
                }

                Double totalSize = termNode.getDouble("ПланЧасов");
                Double totalLabor = termNode.getDouble("ПланЗЕТ");

                Double termWeek = termNode.getDouble("ПланНед");
                Double termDay = termNode.getDouble("ПланДней");
                Double totalWeeks = (termWeek == null ? 0 : termWeek) + (termDay == null ? 0 : termDay) / 6;

                Double selfWork = type == 2 ? termNode.getDouble("ПланЧасовСРС") : totalSize;
                Double audit = type == 2 ? termNode.getDouble("ПланЧасовАуд") : Double.valueOf(0d);

                Double control = 0d;
                Double controlE = 0d;

                rowHours(loadMap, totalSize, totalLabor, totalWeeks, audit, selfWork, control, controlE);

                if (practiceRow.getRowTermLoadMap().put(term, loadMap) != null | practiceRow.getRowTermActionMap().put(term, buildActionMapByNode(termNode, null)) != null)
                    throw new IllegalArgumentException(); // неверная структура ИМЦА, один и тот же семестр встречается два раза

                ImtsaUtils.addValue(practiceRow.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_SIZE, totalSize);
                ImtsaUtils.addValue(practiceRow.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_LABOR, totalLabor);

                ImtsaUtils.addValue(practiceRow.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_SELF_WORK, selfWork);
                ImtsaUtils.addValue(practiceRow.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_AUDIT, audit);

                ImtsaUtils.addValue(practiceRow.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_CONTROL, control);
                ImtsaUtils.addValue(practiceRow.getRowLoadMap(), ImtsaImportDefines.L_TOTAL_CONTROL_E, controlE);
            }

            structureRow.addChild(practiceRow);
        }
    }

    /**
     * Ищет совпадения названий циклов из файла с названиями циклов из справочника без учета пробелов и регистра.
     * при совпадении возврашает это имя и индекс из файла
     * если совпадений нет возвращает первую попавшуюся пару из справочника
     * @return {index, name}
     */
    protected String[] getIndexAndName(ImParseRowsContext context, Collection<String> fgosCycleCodes, Collection<String> fgos13CycleCodes){
        List<EppPlanStructure> cycles;
        switch (context._parseResult.getPlanStructureCode())
        {
            case FGOS:
                cycles = CatalogManager.instance().dao().getCatalogItemList(EppPlanStructure.class, EppPlanStructure.code().s(), fgosCycleCodes);
                break;
            case FGOS_2013:
                cycles = CatalogManager.instance().dao().getCatalogItemList(EppPlanStructure.class, EppPlanStructure.code().s(), fgos13CycleCodes);
                break;
            default:
                throw new IllegalArgumentException(); //не может быть
        }

        String index = null;
        String name = null;
        for (EppPlanStructure cycle : cycles)
        {
            name = cycle.getTitle();
            index = context._cyclesIndexByTitleMap.get(StringUtils.trimToEmpty(name).toLowerCase());
            if (index != null)
            {
                name = context._parseResult.getCycleAbbr2CycleTitleMap().get(index);
                break;
            }
        }

        if(index == null && !cycles.isEmpty())
        {
            index = cycles.get(0).getShortTitle();
            name = cycles.get(0).getTitle();
        }

        return new String[]{index, name};
    }
}