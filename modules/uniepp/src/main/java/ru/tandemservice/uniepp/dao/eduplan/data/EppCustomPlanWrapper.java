/* $Id$ */
package ru.tandemservice.uniepp.dao.eduplan.data;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Not thread-safe
 *
 * @author Nikolay Fedorovskih
 * @since 24.08.2015
 */
public class EppCustomPlanWrapper implements IEppCustomPlanWrapper
{
    private final IEppEpvBlockWrapper blockWrapper;
    private final EppCustomEduPlan customPlan;
    private final Map<Integer, Collection<IEppCustomPlanRowWrapper>> rowMap;
    private final int gridSize;
    private final Map<String, IEppCustomPlanRowWrapper> keyMap;
    private final Map<PairKey<Long, Integer>, IEppCustomPlanRowWrapper> srcMap; // { [srcRow.id + source term number] -> custom row wrapper }

    public EppCustomPlanWrapper(EppCustomEduPlan customPlan)
    {
        Preconditions.checkNotNull(customPlan);

        this.blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(customPlan.getEpvBlock().getId(), true);
        this.customPlan = customPlan;

        // Заполняем список семестров и применяем кастомизации к ним, если такие есть
        // Число семестров берется из учебной сетки индивидуального плана.
        // Последний семестр ИУП соответствует последнему семестру исходного УП. Далее декрементируем номер семестра там и там.
        // Все семестры исходного УП, не вошедщие в ИУП попадают в первый семестр ИУП.

        // Количество семестров исходного УП
        final Integer sourceTermsSize = IDevelopGridDAO.instance.get().getDevelopGridSize(blockWrapper.getVersion().getEduPlan().getDevelopGrid());
        // Количество семестров ИУП
        this.gridSize = IDevelopGridDAO.instance.get().getDevelopGridSize(customPlan.getDevelopGrid());
        // Смещение семестров
        final int offset = sourceTermsSize - this.gridSize;

        // Строки исходного УП
        final Collection<IEppEpvRowWrapper> srcRows = blockWrapper.getRowMap().values();
        // Мапа [epv row id] -> [term numbers list]. Для опрпделения индекса семестра, если их несколько, - для вывода названий строк в ИУП.
        final Map<Long, List<Integer>> termIndexMap = new HashMap<>(srcRows.size());
        final ImmutableMap.Builder<String, IEppCustomPlanRowWrapper> keyMapBuilder = ImmutableMap.builder();

        // Строки ИУП на основе строк УП. [srcRow.id + source term] -> custom row wrapper
        this.srcMap = new HashMap<>(srcRows.size());
        for (IEppEpvRowWrapper srcRow : srcRows) {
            if (!(srcRow.getRow() instanceof EppEpvTermDistributedRow)) {
                continue; // нужны только строки с нагрузкой
            }

            final List<Integer> termList = new ArrayList<>(srcRow.getActiveTermSet().size());
            for (Integer term : srcRow.getActiveTermSet()) {
                Preconditions.checkState(term > 0);
                if (term > sourceTermsSize) {
                    continue; // кривой УП
                }

                final PairKey<Long, Integer> key = new PairKey<>(srcRow.getRow().getId(), term);
                final IEppCustomPlanRowWrapper customRowWrapper = new EppCustomPlanRowWrapper(srcRow, term);
                this.srcMap.put(key, customRowWrapper);
                customRowWrapper.setNewTermNumber(Math.max(term - offset, 1));
                keyMapBuilder.put(customRowWrapper.getKey(), customRowWrapper);
                termList.add(term);
            }
            termIndexMap.put(srcRow.getRow().getId(), termList);
        }
        this.keyMap = keyMapBuilder.build();

        // Расставляем названия (дописываем индекс семестра, если семестров несколько у строки УПв)
        for (Map.Entry<PairKey<Long, Integer>, IEppCustomPlanRowWrapper> entry : this.srcMap.entrySet()) {
            final Long epRowId = entry.getKey().getFirst();
            final Integer sourceTermNumber = entry.getKey().getSecond();
            final List<Integer> termList = termIndexMap.get(epRowId);
            final IEppCustomPlanRowWrapper customRowWrapper = entry.getValue();
            if (termList.size() != 1) {
                final int idx = termList.indexOf(sourceTermNumber);
                Preconditions.checkState(idx >= 0);

                final EppCustomPlanRowWrapper wrapper = (EppCustomPlanRowWrapper) customRowWrapper;
                wrapper.setTitle(customRowWrapper.getSourceRow().getTitle() + " (" + (idx + 1) + "/" + termList.size() + ")");
            } //else {
                // customRowWrapper.setTitle(customRowWrapper.getSourceRow().getTitle());
            //}
        }

        final Map<Long, Integer> id2termNumberMap = DevelopGridDAO.getIdToTermNumberMap();

        // Получаем список кастомизаций из базы
        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppCustomEpvRowTerm.class, "r")
                ./*0*/column(property("r", EppCustomEpvRowTerm.epvRowTerm().row().id()))
                ./*1*/column(property("r", EppCustomEpvRowTerm.epvRowTerm().term().id()))
                ./*2*/column(property("r", EppCustomEpvRowTerm.newTerm().id()))
                ./*3*/column(property("r", EppCustomEpvRowTerm.needRetake()))
                ./*4*/column(property("r", EppCustomEpvRowTerm.excluded()))
                .where(eq(property("r", EppCustomEpvRowTerm.customEduPlan()), value(customPlan)));

        for (Object[] item : IUniBaseDao.instance.get().<Object[]>getList(dql)) {

            final Long srcRowId = (Long) item[0];
            final Integer srcTermNumber = id2termNumberMap.get((Long) item[1]);
            final Integer newTermNumber = id2termNumberMap.get((Long) item[2]);
            final Boolean needRetake = (Boolean) item[3];
            final boolean excluded = (Boolean) item[4];

            if (srcTermNumber > sourceTermsSize) {
                continue; // Номера семестров не должны выходить за пределы сетки. Если выходят, то косяк в УП (и такие реально есть).
            }
            if (newTermNumber > this.gridSize) {
                throw new IllegalStateException("Out of custom edu plan term grid");
            }

            final IEppCustomPlanRowWrapper customRow = this.srcMap.get(new PairKey<>(srcRowId, srcTermNumber));

            // Применяем кастомизацию
            customRow.setNewTermNumber(newTermNumber);
            customRow.setNeedRetake(needRetake);
            customRow.setExcluded(excluded);
        }

        // Складываем всё в общую мапу.
        this.rowMap = new TreeMap<>();
        for (IEppCustomPlanRowWrapper wrapper : this.srcMap.values()) {
            Collection<IEppCustomPlanRowWrapper> set = this.rowMap.get(wrapper.getNewTermNumber());
            if (set == null) {
                this.rowMap.put(wrapper.getNewTermNumber(), set = new TreeSet<>());
            }
            set.add(wrapper);
        }

        // Если в каком-то семестре не оказалось строки, он всё равно должен быть в мапе, хоть и с пустым набором строк
        for (int i = 1; i <= this.gridSize;  i++) {
            if (!this.rowMap.containsKey(i)) {
                this.rowMap.put(i, new TreeSet<>());
            }
        }
    }

    public Map<String, IEppCustomPlanRowWrapper> getKeyMap() {
        return this.keyMap;
    }

    public void moveUp(IEppCustomPlanRowWrapper rowWrapper) {
        final int oldTerm = rowWrapper.getNewTermNumber();
        if (oldTerm == 1) {
            return; // Выше первого семестра перемещать некуда.
        } else if (oldTerm > this.gridSize || oldTerm < 1) {
            throw new IllegalStateException();
        }
        move(rowWrapper, oldTerm - 1);
    }

    public void moveDown(IEppCustomPlanRowWrapper rowWrapper) {
        final int oldTerm = rowWrapper.getNewTermNumber();
        if (oldTerm == this.gridSize) {
            return; // Ниже последнего семестра перемещать некуда.
        } else if (oldTerm > this.gridSize || oldTerm < 1) {
            throw new IllegalStateException();
        }
        move(rowWrapper, oldTerm + 1);
    }

    protected void move(IEppCustomPlanRowWrapper row, int newTerm) {
        Preconditions.checkArgument(this.keyMap.containsKey(row.getKey()));
        Preconditions.checkArgument(newTerm > 0 && newTerm <= this.gridSize);

        final int oldTerm = row.getNewTermNumber();
        final Collection<IEppCustomPlanRowWrapper> oldTermRows = this.rowMap.get(oldTerm);
        Preconditions.checkState(oldTermRows != null);
        Preconditions.checkState(oldTermRows.contains(row));

        if (row.getSourceRow().hasParentDistributedRow()) {
            return; // Дисциплины из группы дисциплин по выбору перемещать самостоятельно нельзя (они ходят только с группой)
        }

        final Collection<IEppCustomPlanRowWrapper> newTermRows = this.rowMap.get(newTerm);
        final List<IEppCustomPlanRowWrapper> movedItems = new ArrayList<>(3);
        movedItems.add(row);

        // Все вложенные дисциплины перемещаем вместе с ДВ
        oldTermRows.stream()
                .filter(item -> item.getSourceRow().isChildOf(row.getSourceRow()))
                .collect(Collectors.toCollection(() -> movedItems));

        movedItems.stream().forEach(item -> {
            oldTermRows.remove(item);
            item.setNewTermNumber(newTerm); // !!! Делаем до включения в newTermRows, т.к. поле участвует в компараторе
            newTermRows.add(item);
        });
    }

    @Override
    public IEppEpvBlockWrapper getBlockWrapper()
    {
        return this.blockWrapper;
    }

    @Override
    public DevelopGrid getDevelopGrid()
    {
        return this.customPlan.getDevelopGrid();
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return this.customPlan.getDevelopCondition();
    }

    @Override
    public int getGridSize()
    {
        return this.gridSize;
    }

    @Override
    public Map<Integer, Collection<IEppCustomPlanRowWrapper>> getRowMap()
    {
        return this.rowMap;
    }

    @Override
    public Map<PairKey<Long, Integer>, IEppCustomPlanRowWrapper> getSourceMap()
    {
        return this.srcMap;
    }
}
