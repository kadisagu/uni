package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_2x9x3_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppEduGroupSplitByLangElement

		// удалено свойство personForeignLanguage
		{
			// удалить колонку
			tool.dropColumn("epp_edu_group_split_el_lang_t", "personforeignlanguage_id");

		}

		// создано обязательное свойство foreignLanguage
		{
			// создать колонку
			tool.createColumn("epp_edu_group_split_el_lang_t", new DBColumn("foreignlanguage_id", DBType.LONG));

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_edu_group_split_el_lang_t", "foreignlanguage_id", false);

		}


    }
}