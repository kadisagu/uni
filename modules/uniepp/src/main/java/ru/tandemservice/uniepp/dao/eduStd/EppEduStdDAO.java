package ru.tandemservice.uniepp.dao.eduStd;

import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;

import java.util.*;

public class EppEduStdDAO extends UniBaseDao implements IEppEduStdDAO
{

    @Override
    @SuppressWarnings("unchecked")
    public SimpleNumberGenerationRule<EppStateEduStandard> getNumberGenerationRule() {
        return new SimpleNumberGenerationRule<EppStateEduStandard>() {
            @Override public Set<String> getUsedNumbers(final EppStateEduStandard object) {
                final List list = EppEduStdDAO.this.getSession().createCriteria(EppStateEduStandard.class).setProjection(Projections.property(EppStateEduStandard.number().toString())).list();
                return new HashSet<>(list);
            }
            @Override public String getNumberQueueName(final EppStateEduStandard object) {
                return EppStateEduStandard.class.getSimpleName();
            }
        };
    }

    @Override
    public EppGeneration getGeneration(final EduProgramSubject programSubject)
    {
        return programSubject.isOKSO() ? EppGeneration.GENERATION_2 : EppGeneration.GENERATION_3;
    }

    @Override
    public void doUpdateBlockList(final Long eduStdId, Collection<EduProgramSpecialization> specializations)
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, String.valueOf(eduStdId));

        final EppStateEduStandard eduStd = this.getNotNull(EppStateEduStandard.class, eduStdId);

        final Set<EduProgramSpecialization> eduLevelsToCreate = new HashSet<>();
        if (null != specializations) {
            eduLevelsToCreate.addAll(specializations);
        }
        eduLevelsToCreate.add(null);

        final Map<EduProgramSpecialization, EppStateEduStandardBlock> eduLevel2block = new HashMap<>();
        for (final EppStateEduStandardBlock block : this.getList(EppStateEduStandardBlock.class, EppStateEduStandardBlock.stateEduStandard().id().s(), eduStdId)) {
            eduLevel2block.put(block.getProgramSpecialization(), block);
        }

        // новые блоки
        for (final EduProgramSpecialization programSpecialization : eduLevelsToCreate) {
            EppStateEduStandardBlock block = eduLevel2block.remove(programSpecialization);
            if (block == null) {
                block = new EppStateEduStandardBlock();
                block.setStateEduStandard(eduStd);
                block.setProgramSpecialization(programSpecialization);
                session.save(block);
            }
            session.saveOrUpdate(block);
        }

        // удаляем оставшиеся
        for (final EppStateEduStandardBlock block : eduLevel2block.values()) {
            session.delete(block);
        }
    }

    @Override
    public boolean isEduStdCompetenceTabVisible(final Long eduStdId)
    {
        final EppStateEduStandard std = this.getNotNull(EppStateEduStandard.class, eduStdId);
        return EppGeneration.GENERATION_3.equals(std.getGeneration());
    }
}
