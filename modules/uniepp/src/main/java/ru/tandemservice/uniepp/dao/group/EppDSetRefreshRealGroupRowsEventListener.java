package ru.tandemservice.uniepp.dao.group;

import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;


/**
 * @author vdanilov
 */
public class EppDSetRefreshRealGroupRowsEventListener
{
    public void init()
    {
        // если создается или меняется год (т.к. используется понятие текущего года)
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EducationYear.class, EppRealGroupRowDAO.DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EducationYear.class, EppRealGroupRowDAO.DAEMON.getAfterCompleteWakeUpListener());

        // если создается или меняется ПУПнаГ (т.к. используется настройка)
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppYearEducationProcess.class, EppRealGroupRowDAO.DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppYearEducationProcess.class, EppRealGroupRowDAO.DAEMON.getAfterCompleteWakeUpListener());

        // если менялись МСРП
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppStudentWorkPlanElement.class, EppRealGroupRowDAO.DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppStudentWorkPlanElement.class, EppRealGroupRowDAO.DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, EppStudentWorkPlanElement.class, EppRealGroupRowDAO.DAEMON.getAfterCompleteWakeUpListener());

        // если менялись МСРП по формам кноторля и видам нагрузки
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppStudentWpePart.class, EppRealGroupRowDAO.DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppStudentWpePart.class, EppRealGroupRowDAO.DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, EppStudentWpePart.class, EppRealGroupRowDAO.DAEMON.getAfterCompleteWakeUpListener());
    }
}
