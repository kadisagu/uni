package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionsTab;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.tool.IdGen;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.CheckboxColumn;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.TreeColumn;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.gen.GroupGen;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.gen.EppStudent2WorkPlanGen;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanVersionGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    @Override
    public void prepare(final Model model) {
        final EppWorkPlan workPlan = model.getWorkPlanHolder().refresh(EppWorkPlan.class);

        this.fillFilters(model);

        final StaticListDataSource<Model.BaseWorkplanNode> dataSource = model.getVersionDistributionDataSource();
        dataSource.getColumns().clear();
        dataSource.addColumn(new CheckboxColumn("selected", "").setScriptResolver((tableName, columnNumber, rowEntity, entity, rowIndex) -> "workPlanVersionCheckChildren(event, this, '" + tableName + "', " + columnNumber + ", '" + rowEntity.getId() + "');")
                                     .setDisableHandler(entity -> (null == ((Model.BaseWorkplanNode) entity).getHierarhyParent())).setWidth(1));
        dataSource.addColumn(UniEppUtils.getStateColumn());
        dataSource.addColumn(new TreeColumn("Название", "title").setOrderable(false).setClickable(true));

        final Map<String, Model.BaseWorkplanNode> nodes = new LinkedHashMap<>();
        this.preparePlanNode(nodes, workPlan);

        for (final EppWorkPlanVersion workPlanVersion : this.getList(EppWorkPlanVersion.class, EppWorkPlanVersionGen.parent().toString(), workPlan)) {
            this.preparePlanNode(nodes, workPlanVersion);
        }

        final Session session = this.getSession();

        final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EppStudent2WorkPlan.class, "rel")
            .where(isNull(property("rel", EppStudent2WorkPlanGen.removalDate())))
            .where(or(
                eq(property("rel", EppStudent2WorkPlanGen.workPlan()), value(workPlan)),
                exists(new DQLSelectBuilder()
                    .fromEntity(EppWorkPlanVersion.class, "ver")
                    .where(eq(property("rel", EppStudent2WorkPlanGen.workPlan()), property("ver")))
                    .where(eq(property("ver", EppWorkPlanVersionGen.parent()), value(workPlan)))
                    .buildQuery())
            ))
            .fetchPath(DQLJoinType.left, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("rel"), "epv")
            .fetchPath(DQLJoinType.left, EppStudent2EduPlanVersion.student().fromAlias("epv"), "s")
            .fetchPath(DQLJoinType.left, Student.person().fromAlias("s"), "p")
            .fetchPath(DQLJoinType.left, Person.identityCard().fromAlias("p"), "idc")
            .fetchPath(DQLJoinType.left, Student.group().fromAlias("s"), "g")
            .order(property("g", Group.title()))
            .order(property("idc", IdentityCard.fullFio()))
            .column(property("rel"));

        // фильтруем их
        final IDataSettings settings = model.getSettings();

        FilterUtils.applySimpleLikeFilter(dql, "idc", IdentityCard.lastName(), settings.<String>get("surname"));
        FilterUtils.applySimpleLikeFilter(dql, "idc", IdentityCard.firstName(), settings.<String>get("name"));
        FilterUtils.applySimpleLikeFilter(dql, "s", Student.personalNumber(), settings.<String>get("personalNumber"));

        final List<EppStudent2WorkPlan> fullRelations = dql.createStatement(session).<EppStudent2WorkPlan>list();

        final List formativeOrgUnitList = (List) settings.get("formativeOrgUnitList");
        final List groupList = (List) settings.get("groupList");

        CollectionUtils.filter(fullRelations, object -> {
            final Student student = ((EppStudent2WorkPlan) object).getStudentEduPlanVersion().getStudent();

            final Long studentFormOrgUnit = student.getEducationOrgUnit().getFormativeOrgUnit().getId();
            final boolean orgUnitFilter = ((formativeOrgUnitList != null) && !formativeOrgUnitList.isEmpty()) ? UniBaseDao.ids(formativeOrgUnitList).contains(studentFormOrgUnit) : true;

            final long studentGroupId = (null == student.getGroup()) ? Model.EMPTY_GROUP_ID : student.getGroup().getId();
            final boolean groupFilter = ((groupList != null) && !groupList.isEmpty()) ? UniBaseDao.ids(groupList).contains(studentGroupId) : true;

            return orgUnitFilter && groupFilter;
        });

        this.processRelations(nodes, fullRelations);

        dataSource.setupRows(nodes.values());
    }

    private void processRelations(final Map<String, Model.BaseWorkplanNode> nodes, final List<EppStudent2WorkPlan> relations) {
        for (final EppStudent2WorkPlan relation : relations) {

            final EppWorkPlanBase plan = relation.getWorkPlan();
            final Model.BaseWorkplanNode planNode = this.preparePlanNode(nodes, plan);

            final Student student = relation.getStudentEduPlanVersion().getStudent();
            final Long groupId = (null == student.getGroup() ? 0L : student.getGroup().getId());
            final String groupStringId = (planNode.getId() + "." + groupId);

            Model.BaseWorkplanNode groupWorkplanNode = nodes.get(groupStringId);
            if (null == groupWorkplanNode) {
                final Long gid = (long) groupStringId.hashCode() << IdGen.CODE_BITS;
                final String groupTitle = (null == student.getGroup() ? "(без группы)" : student.getGroup().getTitle());
                nodes.put(groupStringId, groupWorkplanNode = new Model.BaseWorkplanNode(gid, planNode, groupTitle));
            }

            final Model.BaseWorkplanNode studentWorkplanNode = new Model.BaseWorkplanNode(student.getId(), groupWorkplanNode, student.getPerson().getFullFio() + " ( " + (student.getPersonalNumber()) + " )");
            nodes.put(String.valueOf(studentWorkplanNode.getId()), studentWorkplanNode);
        }
    }

    private Model.BaseWorkplanNode preparePlanNode(final Map<String, Model.BaseWorkplanNode> nodes, final EppWorkPlanBase plan) {
        final String planStringId = String.valueOf(plan.getId());
        Model.BaseWorkplanNode planNode = nodes.get(planStringId);
        if (null == planNode) {
            nodes.put(planStringId, planNode = new Model.WorkPlanNode(plan, null));
        }
        return planNode;
    }

    @SuppressWarnings("unchecked")
    private void fillFilters(final Model model) {

        final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EppStudent2WorkPlan.class, "rel")
            .column(property("rel", EppStudent2WorkPlan.studentEduPlanVersion().student()))
            .where(isNull(property("rel", EppStudent2WorkPlanGen.removalDate())))
            .where(or(
                eq(property("rel", EppStudent2WorkPlanGen.workPlan()), value(model.getWorkPlan())),
                exists(new DQLSelectBuilder()
                    .fromEntity(EppWorkPlanVersion.class, "ver")
                    .where(eq(property("rel", EppStudent2WorkPlanGen.workPlan()), property("ver")))
                    .where(eq(property("ver", EppWorkPlanVersionGen.parent()), value(model.getWorkPlan())))
                    .buildQuery())
            ));
        List<Student> students = dql.createStatement(getSession()).<Student>list();

        final HashSet<OrgUnit> orgUnitSet = new HashSet<>();

        final LinkedHashSet<IdentifiableWrapper<Group>> groupSet = new LinkedHashSet<>();
        groupSet.add(Model.emptyGroupOption);

        for (final Student student : students) {
            orgUnitSet.add(student.getEducationOrgUnit().getFormativeOrgUnit());
            if (null != student.getGroup()) {
                groupSet.add(new IdentifiableWrapper<>(student.getGroup().getId(), student.getGroup().getTitle()));
            }
        }

        model.setFormativeOrgUnitListModel(new LazySimpleSelectModel<>(new ArrayList(orgUnitSet), OrgUnitGen.title().s()).setSortProperty(OrgUnitGen.title().s()));
        model.setGroupSelectModel(new LazySimpleSelectModel<>(new ArrayList(groupSet), GroupGen.title().s()).setSortProperty(GroupGen.title().s()));
    }

}
