package ru.tandemservice.uniepp.component.student.StudentWorkPlanEdit;

import org.tandemframework.core.component.IBusinessComponent;

import ru.tandemservice.uniepp.component.workplan.WorkPlanData.WorkPlanDataEditableController;

public class Controller extends WorkPlanDataEditableController<IDAO, Model>
{

    public void onClickAccept(final IBusinessComponent component) {
        this.getDao().doAccept(this.getModel(component));
        this.deactivate(component);
    }

    public void onClickReject(final IBusinessComponent component) {
        this.getDao().doReject(this.getModel(component));
        this.deactivate(component);
    }

}
