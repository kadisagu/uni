/* $Id:$ */
package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Nikolay Fedorovskih
 * @since 18.01.2015
 */
@SuppressWarnings("unused")
public class MS_uniepp_2x7x1_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.16"),
                        new ScriptDependency("org.tandemframework.shared", "1.7.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // squeeze unsqueezed boolean values
        tool.executeUpdate("update settings_s set value_p=? where value_p like ?", "java.lang.Boolean-false", "false");
        tool.executeUpdate("update settings_s set value_p=? where value_p like ?", "java.lang.Boolean-true", "true");
    }
}