/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.ElementDuplicationMerge;

/**
 * @author Nikolay Fedorovskih
 * @since 15.05.2015
 */
public class SplitterByTitle extends RowWrapperBase
{
    @Override
    public boolean isSplitterByKey()
    {
        return false;
    }

    @Override
    public boolean isSplitterByTitle()
    {
        return true;
    }

    @Override
    public String getSplitterStyle()
    {
        return "height:3px";
    }
}