package ru.tandemservice.uniepp.component.registry.base.Pub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniepp.component.registry.base.ModelBase;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * 
 * @author nkokorina
 *
 */

@State( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id", required = true),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
@Output({
    @Bind(key = RegistryElementPubContext.REGISTRY_ELEMENT_PUB_CONTEXT_PARAM, binding = "context")
})
public abstract class Model<T extends EppRegistryElement> extends ModelBase<T> implements RegistryElementPubContext
{
    public RegistryElementPubContext getContext() { return this; }

    private final EntityHolder<EppRegistryElement> holder = new EntityHolder<>();
    public EntityHolder<EppRegistryElement> getHolder() { return this.holder; }

    @Override
    public EppRegistryElement getElement() { return this.getHolder().getValue(); }

    private String selectedTab;
    public String getSelectedTab() { return this.selectedTab; }
    public void setSelectedTab(final String selectedTab) { this.selectedTab = selectedTab; }


    @Override public OrgUnit getOrgUnit() {
        return (null == this.getElement() ? null : this.getElement().getOwner());
    }

    @Override public IEntity getSecuredObject() {
        return this.getElement();
    }

    @Override public CommonPostfixPermissionModelBase setupPermissionModel() {
        final String postfix = this.getPermissionPrefix();
        this.setSec(new CommonPostfixPermissionModel(postfix));
        return this.getSec();
    }

    public boolean isDownloadWorkProgramVisible() { return getElement().getWorkProgramFile() != null; }
    public boolean isDownloadWorkProgramAnnotationVisible() { return getElement().getWorkProgramAnnotation() != null; }

    private String groupTypesNames;
    public String getGroupTypesNames() {
        return groupTypesNames;
    }

    public void setGroupTypesNames(String groupTypesNames) {
        this.groupTypesNames = groupTypesNames;
    }
}
