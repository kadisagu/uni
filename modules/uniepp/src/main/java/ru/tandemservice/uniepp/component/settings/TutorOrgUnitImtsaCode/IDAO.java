package ru.tandemservice.uniepp.component.settings.TutorOrgUnitImtsaCode;

import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.uni.dao.IUpdateable;


public interface IDAO extends IPrepareable<Model>, IUpdateable<Model>
{
}
