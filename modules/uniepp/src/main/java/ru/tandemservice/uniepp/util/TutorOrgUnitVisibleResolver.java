/* $Id:$ */
package ru.tandemservice.uniepp.util;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.IOrgUnitReportVisibleResolver;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;

/**
 * @author oleyba
 * @since 5/31/12
 */
public class TutorOrgUnitVisibleResolver implements IOrgUnitReportVisibleResolver
{
    @Override
    public boolean isVisible(OrgUnit orgUnit)
    {
        return IEppSettingsDAO.instance.get().isTutorOrgUnit(orgUnit.getId());
    }
}

