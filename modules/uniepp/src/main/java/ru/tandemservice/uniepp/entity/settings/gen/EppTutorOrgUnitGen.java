package ru.tandemservice.uniepp.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Читающая кафедра (подразделение)
 *
 * Показывает, какие подразделения могут осуществлять чтение дисциплин (читающие кафедры)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppTutorOrgUnitGen extends EntityBase
 implements INaturalIdentifiable<EppTutorOrgUnitGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit";
    public static final String ENTITY_NAME = "eppTutorOrgUnit";
    public static final int VERSION_HASH = -1865318206;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_CODE_IMTSA = "codeImtsa";

    private OrgUnit _orgUnit;     // Подразделение
    private String _codeImtsa;     // Код подразделения в ИМЦА

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Код подразделения в ИМЦА.
     */
    @Length(max=255)
    public String getCodeImtsa()
    {
        return _codeImtsa;
    }

    /**
     * @param codeImtsa Код подразделения в ИМЦА.
     */
    public void setCodeImtsa(String codeImtsa)
    {
        dirty(_codeImtsa, codeImtsa);
        _codeImtsa = codeImtsa;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppTutorOrgUnitGen)
        {
            if (withNaturalIdProperties)
            {
                setOrgUnit(((EppTutorOrgUnit)another).getOrgUnit());
            }
            setCodeImtsa(((EppTutorOrgUnit)another).getCodeImtsa());
        }
    }

    public INaturalId<EppTutorOrgUnitGen> getNaturalId()
    {
        return new NaturalId(getOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EppTutorOrgUnitGen>
    {
        private static final String PROXY_NAME = "EppTutorOrgUnitNaturalProxy";

        private Long _orgUnit;

        public NaturalId()
        {}

        public NaturalId(OrgUnit orgUnit)
        {
            _orgUnit = ((IEntity) orgUnit).getId();
        }

        public Long getOrgUnit()
        {
            return _orgUnit;
        }

        public void setOrgUnit(Long orgUnit)
        {
            _orgUnit = orgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppTutorOrgUnitGen.NaturalId) ) return false;

            EppTutorOrgUnitGen.NaturalId that = (NaturalId) o;

            if( !equals(getOrgUnit(), that.getOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppTutorOrgUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppTutorOrgUnit.class;
        }

        public T newInstance()
        {
            return (T) new EppTutorOrgUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "codeImtsa":
                    return obj.getCodeImtsa();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "codeImtsa":
                    obj.setCodeImtsa((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "codeImtsa":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "codeImtsa":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "codeImtsa":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppTutorOrgUnit> _dslPath = new Path<EppTutorOrgUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppTutorOrgUnit");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Код подразделения в ИМЦА.
     * @see ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit#getCodeImtsa()
     */
    public static PropertyPath<String> codeImtsa()
    {
        return _dslPath.codeImtsa();
    }

    public static class Path<E extends EppTutorOrgUnit> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _codeImtsa;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Код подразделения в ИМЦА.
     * @see ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit#getCodeImtsa()
     */
        public PropertyPath<String> codeImtsa()
        {
            if(_codeImtsa == null )
                _codeImtsa = new PropertyPath<String>(EppTutorOrgUnitGen.P_CODE_IMTSA, this);
            return _codeImtsa;
        }

        public Class getEntityClass()
        {
            return EppTutorOrgUnit.class;
        }

        public String getEntityName()
        {
            return "eppTutorOrgUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
