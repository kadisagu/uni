/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.GroupRowAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.BaseAddEditDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAddEditBase.EppEduPlanVersionRowAddEditBaseUI;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow;

import java.util.Collections;
import java.util.List;

/**
 * @author oleyba
 * @since 11/12/14
 */
@Input({
    @Bind(key = EppEduPlanVersionRowAddEditBaseUI.BIND_ROW_ID, binding = "row.id"),
    @Bind(key = EppEduPlanVersionRowAddEditBaseUI.BIND_BLOCK_ID, binding = EppEduPlanVersionRowAddEditBaseUI.FIELD_BLOCK_ID),
})
@Return({
    @Bind(key = "scroll", binding = "scroll"),
    @Bind(key = "scrollPos", binding = "scrollPos"),
    @Bind(key = "highlightRowId", binding = "row.id")
})
public class EppEduPlanVersionGroupRowAddEditUI extends EppEduPlanVersionRowAddEditBaseUI<EppEpvGroupReRow>
{
    private EppEpvGroupReRow row = new EppEpvGroupReRow();

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        final List<HSelectOption> parentList = IEppEduPlanVersionDataDAO.instance.get().getEpvRowHierarchyListFromBlock(getBlock(), EppEpvGroupReRow.PARENT_PREDICATE);
        BaseAddEditDao.disablePossibleLoop(parentList, Collections.singleton(getRow().getId()));
        setParentList(parentList);
    }

    @Override
    public void onChangeParent()
    {
        if (!isEditForm()) getRow().setNumber(null);
        getRow().setNumber(IEppEduPlanVersionDataDAO.instance.get().getNextNumber(getRow(), null));
    }

    // getters and setters

    @Override
    public EppEpvGroupReRow getRow()
    {
        return row;
    }

    @Override
    public void setRow(EppEpvGroupReRow row)
    {
        this.row = row;
    }
}