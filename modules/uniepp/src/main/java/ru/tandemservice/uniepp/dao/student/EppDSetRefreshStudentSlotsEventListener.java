package ru.tandemservice.uniepp.dao.student;

import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;


/**
 * вызывает демон при изменении связей (через наборы объектов) студента и нагрузки из реестра
 * 
 * @author vdanilov
 */
public class EppDSetRefreshStudentSlotsEventListener
{
    private void event(final Class klass) {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, klass, EppStudentSlotDAO.DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, klass, EppStudentSlotDAO.DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, klass, EppStudentSlotDAO.DAEMON.getAfterCompleteWakeUpListener());
    }

    public void init()
    {
        // изменение структуры объектов, связывающих студента с видами нагрузки реестра
        this.event(EppStudent2EduPlanVersion.class);             // связи с УПВ
        this.event(EppStudent2WorkPlan.class);                   // связи с РУП
        this.event(EppWorkPlanBase.class);                       // РУП
        this.event(EppWorkPlanRegistryElementRow.class);         // строки РУП
        this.event(EppRegistryElementPartFControlAction.class);  // элементы реестра (контроль)
        this.event(EppRegistryElementPartModule.class);          // элементы реестра (модули)
        this.event(EppRegistryModuleALoad.class);                // элементы реестра (нагрузка)
    }
}
