package ru.tandemservice.uniepp.component.settings.EppControlActionTypeGrade;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;

/**
 * Created by IntelliJ IDEA.
 * User: Tandem
 * Date: 02.03.11
 * Time: 11:44
 * To change this template use File | Settings | File Templates.
 */
public class DAO extends UniDao<Model>
{

    @Override
    public void prepare(final Model model)
    {
        model.setGradeScaleList(this.getCatalogItemList(EppGradeScale.class));
        model.setControlActionTypeList(this.getList(EppFControlActionType.class, EppFControlActionType.P_PRIORITY));
    }

    @Override
    public void update(final Model model)
    {
        final ErrorCollector errors = UserContext.getInstance().getErrorCollector();
        this.validate(model, errors);
        if (errors.hasErrors()) { return; }

        for (final EppFControlActionType controlActionType : model.getControlActionTypeList()) {
            this.getSession().save(controlActionType);
        }
    }

}
