/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionScheduleTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onClickRichRowSave(final IBusinessComponent component)
    {
        try {
            final Model model = this.getModel(component);
            final Long rowId = component.getListenerParameter();
            this.getDao().updateScheduleRow(rowId, model);
            model.getScheduleDataSource().setEditId(null);
            this.resetLastVisitedEntityId(model);
        } finally {
            this.onRefreshComponent(component);
        }
    }

    public void onClickRichRowEdit(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final Long rowId = component.getListenerParameter();
        this.getDao().prepareEditRow(model, rowId);
        model.getScheduleDataSource().setEditId(rowId);
        this.resetLastVisitedEntityId(model);
    }

    public void onClickRichRowCancel(final IBusinessComponent component)
    {
        try {
            final Model model = this.getModel(component);
            model.getScheduleDataSource().setEditId(null);
            this.resetLastVisitedEntityId(model);
        } finally {
            this.onRefreshComponent(component);
        }
    }

    public void onClickPoint(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final Integer index = (Integer) component.getListenerParameter();
        model.getScheduleDataSource().getSelection().doPointClick(index);
        this.resetLastVisitedEntityId(model);
    }

    private void resetLastVisitedEntityId(final Model model) {
        model.getScheduleDataSource().getDataSource().setLastVisitedEntityId(null);
    }

    public void onClickCopyScheduleData(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            ru.tandemservice.uniepp.component.eduplan.EduPlanVersionCopyScheduleData.Model.class.getPackage().getName(),
            new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getEduplanVersion().getId())
        ));
    }
}