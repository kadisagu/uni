package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Степень сформированности учебной группы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRealEduGroupCompleteLevelGen extends EntityBase
 implements INaturalIdentifiable<EppRealEduGroupCompleteLevelGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel";
    public static final String ENTITY_NAME = "eppRealEduGroupCompleteLevel";
    public static final int VERSION_HASH = -1073130648;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_PARENT = "parent";
    public static final String P_PRIORITY = "priority";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_ACTION_TITLE = "actionTitle";
    public static final String P_READY_FOR_FINAL_CONTROL_ACTION = "readyForFinalControlAction";
    public static final String P_TITLE = "title";
    public static final String P_DISPLAYABLE_SHORT_TITLE = "displayableShortTitle";
    public static final String P_DISPLAYABLE_TITLE = "displayableTitle";

    private String _code;     // Системный код
    private EppRealEduGroupCompleteLevel _parent;     // Степень сформированности учебной группы
    private int _priority;     // Приоритет
    private String _shortTitle;     // Сокращенное название
    private String _actionTitle;     // Название перехода
    private boolean _readyForFinalControlAction = true;     // Готовность к использованию в ИФК
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Степень сформированности учебной группы.
     */
    public EppRealEduGroupCompleteLevel getParent()
    {
        return _parent;
    }

    /**
     * @param parent Степень сформированности учебной группы.
     */
    public void setParent(EppRealEduGroupCompleteLevel parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Название перехода.
     */
    @Length(max=255)
    public String getActionTitle()
    {
        return _actionTitle;
    }

    /**
     * @param actionTitle Название перехода.
     */
    public void setActionTitle(String actionTitle)
    {
        dirty(_actionTitle, actionTitle);
        _actionTitle = actionTitle;
    }

    /**
     * @return Готовность к использованию в ИФК. Свойство не может быть null.
     */
    @NotNull
    public boolean isReadyForFinalControlAction()
    {
        return _readyForFinalControlAction;
    }

    /**
     * @param readyForFinalControlAction Готовность к использованию в ИФК. Свойство не может быть null.
     */
    public void setReadyForFinalControlAction(boolean readyForFinalControlAction)
    {
        dirty(_readyForFinalControlAction, readyForFinalControlAction);
        _readyForFinalControlAction = readyForFinalControlAction;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRealEduGroupCompleteLevelGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EppRealEduGroupCompleteLevel)another).getCode());
            }
            setParent(((EppRealEduGroupCompleteLevel)another).getParent());
            setPriority(((EppRealEduGroupCompleteLevel)another).getPriority());
            setShortTitle(((EppRealEduGroupCompleteLevel)another).getShortTitle());
            setActionTitle(((EppRealEduGroupCompleteLevel)another).getActionTitle());
            setReadyForFinalControlAction(((EppRealEduGroupCompleteLevel)another).isReadyForFinalControlAction());
            setTitle(((EppRealEduGroupCompleteLevel)another).getTitle());
        }
    }

    public INaturalId<EppRealEduGroupCompleteLevelGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EppRealEduGroupCompleteLevelGen>
    {
        private static final String PROXY_NAME = "EppRealEduGroupCompleteLevelNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppRealEduGroupCompleteLevelGen.NaturalId) ) return false;

            EppRealEduGroupCompleteLevelGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRealEduGroupCompleteLevelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRealEduGroupCompleteLevel.class;
        }

        public T newInstance()
        {
            return (T) new EppRealEduGroupCompleteLevel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "parent":
                    return obj.getParent();
                case "priority":
                    return obj.getPriority();
                case "shortTitle":
                    return obj.getShortTitle();
                case "actionTitle":
                    return obj.getActionTitle();
                case "readyForFinalControlAction":
                    return obj.isReadyForFinalControlAction();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "parent":
                    obj.setParent((EppRealEduGroupCompleteLevel) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "actionTitle":
                    obj.setActionTitle((String) value);
                    return;
                case "readyForFinalControlAction":
                    obj.setReadyForFinalControlAction((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "parent":
                        return true;
                case "priority":
                        return true;
                case "shortTitle":
                        return true;
                case "actionTitle":
                        return true;
                case "readyForFinalControlAction":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "parent":
                    return true;
                case "priority":
                    return true;
                case "shortTitle":
                    return true;
                case "actionTitle":
                    return true;
                case "readyForFinalControlAction":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "parent":
                    return EppRealEduGroupCompleteLevel.class;
                case "priority":
                    return Integer.class;
                case "shortTitle":
                    return String.class;
                case "actionTitle":
                    return String.class;
                case "readyForFinalControlAction":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRealEduGroupCompleteLevel> _dslPath = new Path<EppRealEduGroupCompleteLevel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRealEduGroupCompleteLevel");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Степень сформированности учебной группы.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getParent()
     */
    public static EppRealEduGroupCompleteLevel.Path<EppRealEduGroupCompleteLevel> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Название перехода.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getActionTitle()
     */
    public static PropertyPath<String> actionTitle()
    {
        return _dslPath.actionTitle();
    }

    /**
     * @return Готовность к использованию в ИФК. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#isReadyForFinalControlAction()
     */
    public static PropertyPath<Boolean> readyForFinalControlAction()
    {
        return _dslPath.readyForFinalControlAction();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getDisplayableShortTitle()
     */
    public static SupportedPropertyPath<String> displayableShortTitle()
    {
        return _dslPath.displayableShortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getDisplayableTitle()
     */
    public static SupportedPropertyPath<String> displayableTitle()
    {
        return _dslPath.displayableTitle();
    }

    public static class Path<E extends EppRealEduGroupCompleteLevel> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EppRealEduGroupCompleteLevel.Path<EppRealEduGroupCompleteLevel> _parent;
        private PropertyPath<Integer> _priority;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _actionTitle;
        private PropertyPath<Boolean> _readyForFinalControlAction;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<String> _displayableShortTitle;
        private SupportedPropertyPath<String> _displayableTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EppRealEduGroupCompleteLevelGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Степень сформированности учебной группы.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getParent()
     */
        public EppRealEduGroupCompleteLevel.Path<EppRealEduGroupCompleteLevel> parent()
        {
            if(_parent == null )
                _parent = new EppRealEduGroupCompleteLevel.Path<EppRealEduGroupCompleteLevel>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EppRealEduGroupCompleteLevelGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EppRealEduGroupCompleteLevelGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Название перехода.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getActionTitle()
     */
        public PropertyPath<String> actionTitle()
        {
            if(_actionTitle == null )
                _actionTitle = new PropertyPath<String>(EppRealEduGroupCompleteLevelGen.P_ACTION_TITLE, this);
            return _actionTitle;
        }

    /**
     * @return Готовность к использованию в ИФК. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#isReadyForFinalControlAction()
     */
        public PropertyPath<Boolean> readyForFinalControlAction()
        {
            if(_readyForFinalControlAction == null )
                _readyForFinalControlAction = new PropertyPath<Boolean>(EppRealEduGroupCompleteLevelGen.P_READY_FOR_FINAL_CONTROL_ACTION, this);
            return _readyForFinalControlAction;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppRealEduGroupCompleteLevelGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getDisplayableShortTitle()
     */
        public SupportedPropertyPath<String> displayableShortTitle()
        {
            if(_displayableShortTitle == null )
                _displayableShortTitle = new SupportedPropertyPath<String>(EppRealEduGroupCompleteLevelGen.P_DISPLAYABLE_SHORT_TITLE, this);
            return _displayableShortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel#getDisplayableTitle()
     */
        public SupportedPropertyPath<String> displayableTitle()
        {
            if(_displayableTitle == null )
                _displayableTitle = new SupportedPropertyPath<String>(EppRealEduGroupCompleteLevelGen.P_DISPLAYABLE_TITLE, this);
            return _displayableTitle;
        }

        public Class getEntityClass()
        {
            return EppRealEduGroupCompleteLevel.class;
        }

        public String getEntityName()
        {
            return "eppRealEduGroupCompleteLevel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDisplayableShortTitle();

    public abstract String getDisplayableTitle();
}
