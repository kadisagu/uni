/* $Id$ */
package ru.tandemservice.uniepp.component.settings.DefaultGroupTypeForICA;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;

/**
 * @author oleyba
 * @since 8/24/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setCaList(this.getCatalogItemList(EppIControlActionType.class));
        model.setGroupTypeModel(new LazySimpleSelectModel<>(getList(EppGroupType.class, EppGroupType.priority().s())));
    }

    @Override
    public void update(final Model model)
    {
        for (final EppIControlActionType type : model.getCaList()) {
            this.update(type);
        }
    }
}
