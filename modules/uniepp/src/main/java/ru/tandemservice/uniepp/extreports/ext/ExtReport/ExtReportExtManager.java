package ru.tandemservice.uniepp.extreports.ext.ExtReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.extreports.bo.ExtReport.ExtReportManager;
import org.tandemframework.shared.commonbase.extreports.bo.ExtReport.IExtReportContextDefinition;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.OrgUnitExtReportContextDefinition;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.extreports.entity.codes.ExtReportContextCodes;


/**
 * @author vdanilov
 */
@Configuration
public class ExtReportExtManager extends BusinessObjectExtensionManager {

    @Autowired
    private ExtReportManager _extReportManager;

    @Bean
    public ItemListExtension<IExtReportContextDefinition> contextDefinitionListExtension()
    {
        IItemListExtensionBuilder<IExtReportContextDefinition> ext = this.itemListExtension(_extReportManager.contextDefinitionList());

        // Головное подразделение
        ext.add(ExtReportContextCodes.EPP_GROUP_ORG_UNIT_CONTEXT, new OrgUnitExtReportContextDefinition(ExtReportContextCodes.EPP_GROUP_ORG_UNIT_CONTEXT) {
            @Override public boolean isExtReportContext(IEntity entity) {
                return null != entity && IEppSettingsDAO.instance.get().isGroupOrgUnit(entity.getId());
            }
        });

        return ext.create();
    }


}
