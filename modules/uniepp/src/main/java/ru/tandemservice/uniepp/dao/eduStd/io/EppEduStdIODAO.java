package ru.tandemservice.uniepp.dao.eduStd.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniepp.dao.eduStd.io.export.EppEduStdExportUtils;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;
import ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineComment;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation;
import ru.tandemservice.uniepp.entity.std.data.gen.EppStdRow2SkillRelationGen;

/**
 *
 * @author nkokorina
 *
 */
public class EppEduStdIODAO extends UniBaseDao implements IEppEduStdIODAO
{
    @Override
    public byte[] export(final Collection<Long> eduStdIds)
    {
        final Session session = this.getSession();

        // список ГОСов для импорта
        final MQBuilder builder = new MQBuilder(EppStateEduStandard.ENTITY_NAME, "std");
        builder.add(MQExpression.in("std", EppStateEduStandard.id().s(), eduStdIds));
        final List<EppStateEduStandard> stdDataList = builder.getResultList(session);

        final Map<Long, EppStateEduStandard> idStd2std = new HashMap<Long, EppStateEduStandard>();
        for (final EppStateEduStandard element : stdDataList)
        {
            idStd2std.put(element.getId(), element);
        }

        ByteArrayOutputStream out;
        try
        {
            out = new ByteArrayOutputStream();
            final ZipOutputStream zipOut = new ZipOutputStream(out);

            for (final Long id : eduStdIds)
            {
                final EppStateEduStandard std = idStd2std.get(id);

                final String fileName = EppEduStdExportUtils.getFileName(std);
                final byte[] fileContent = EppEduStdExportUtils.getFileContent(std);

                if ((null != fileName) && (null != fileContent))
                {
                    EppEduStdIODAO.putEntryToZipOut(zipOut, fileName, fileContent);
                }

                // если в ГОСе есть приложенный файл, то ложим его в архив с ГОСом
                if (null != std.getContent())
                {
                    final String contentFileName = EppEduStdExportUtils.getContentFileName(std);
                    final byte[] stdFileContent = std.getContent().getContent();

                    if ((null != contentFileName) && (null != stdFileContent))
                    {
                        EppEduStdIODAO.putEntryToZipOut(zipOut, contentFileName, stdFileContent);
                    }
                }
            }

            zipOut.close();
            return out.toByteArray();
        }
        catch (final IOException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Упаковывает в архив zipOut файл с именем - fileName, содержимое которого - content
     * @param zipOut
     * @param fileName
     * @param content
     */
    public static void putEntryToZipOut(final ZipOutputStream zipOut, final String fileName, final byte[] content)
    {
        try
        {
            zipOut.putNextEntry(new ZipEntry(fileName));
            zipOut.write(content);
            zipOut.closeEntry();
        }
        catch (final IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public List<EppStateEduStandardBlock> getEduStdBlockList(final Long eduStdId)
    {
        final MQBuilder builder = new MQBuilder(EppStateEduStandardBlock.ENTITY_CLASS, "block");
        builder.add(MQExpression.eq("block", EppStateEduStandardBlock.stateEduStandard().id().s(), eduStdId));
        builder.addOrder("block", EppStateEduStandardBlock.programSpecialization().title().s());
        return builder.getResultList(this.getSession());
    }

    @Override
    public Map<String, EppStdDisciplineComment> getEduStdDisciplineCommentMap(final Collection<Long> rowIds)
    {
        final MQBuilder commentBuilder = new MQBuilder(EppStdDisciplineComment.ENTITY_NAME, "c");
        commentBuilder.add(MQExpression.in("c", EppStdDisciplineComment.row().id(), rowIds));
        final List<EppStdDisciplineComment> rowCommentList = commentBuilder.getResultList(this.getSession());

        final Map<String, EppStdDisciplineComment> uuid2comment = new HashMap<String, EppStdDisciplineComment>();
        for (final EppStdDisciplineComment comment : rowCommentList)
        {
            uuid2comment.put(comment.getRow().getUuid(), comment);
        }
        return uuid2comment;
    }

    @Override
    public Map<INaturalId<EppStdRow2SkillRelationGen>, EppStdRow2SkillRelation> getEduStdRow2SkillRel(final Collection<Long> rowIds)
    {
        final MQBuilder builder = new MQBuilder(EppStdRow2SkillRelation.ENTITY_NAME, "r");
        builder.add(MQExpression.in("r", EppStdRow2SkillRelation.row().id().s(), rowIds));
        final List<EppStdRow2SkillRelation> relList = builder.getResultList(this.getSession());

        final Map<INaturalId<EppStdRow2SkillRelationGen>, EppStdRow2SkillRelation> rowId2skillRel = new HashMap<INaturalId<EppStdRow2SkillRelationGen>, EppStdRow2SkillRelation>();
        for (final EppStdRow2SkillRelation rel : relList)
        {
            rowId2skillRel.put(rel.getNaturalId(), rel);
        }
        return rowId2skillRel;
    }

    @Override
    public boolean doImportEduStdFromFile(final Long eduStdId, final byte[] content, final boolean ignoreErrors)
    {
        try
        {
            final JAXBContext jc = JAXBContext.newInstance(XmlEduStd.class);
            final Unmarshaller um = jc.createUnmarshaller();
            final XmlEduStd xmlEduStd = (XmlEduStd)um.unmarshal(new ByteArrayInputStream(content));

            final EduStdImportWrapper stdImportWrapper = new EduStdImportWrapper(this.getNotNull(EppStateEduStandard.class, eduStdId), xmlEduStd);

            final Session session = this.getSession();

            // обновляем ГОС
            final EppStateEduStandard eduStd = stdImportWrapper.getUpdatedEduStandard();
            session.saveOrUpdate(eduStd);

            // обновляем компетенции ГОС
            final Collection<EppStateEduStandardSkill> skillList = stdImportWrapper.getUpdatedStdSkill();
            for (final EppStateEduStandardSkill skill : skillList)
            {
                session.saveOrUpdate(skill);
            }
            for (final EppStateEduStandardSkill skill : stdImportWrapper.getRemovedStdSkills())
            {
                session.delete(skill);
            }

            // обновляем блоки ГОСа
            final Collection<EppStateEduStandardBlock> eduStandardBlocks = stdImportWrapper.getUpdatedEduStandardBlocks();
            for (final EppStateEduStandardBlock block : eduStandardBlocks)
            {
                session.saveOrUpdate(block);
            }

            // обновляем строки ГОСа
            {
                final LinkedHashMap<String, EppStdRow> newRowMap = stdImportWrapper.getUpdatedEduStandardRows();
                for (final Entry<String, EppStdRow> newRow : newRowMap.entrySet()) {
                    session.saveOrUpdate(newRow.getValue());
                }

                // обновляем комментарии к дисциплинам
                final Set<EppStdDisciplineComment> newCommentsSet = stdImportWrapper.getUpdatedDisciplineComments();
                for (final EppStdDisciplineComment newComment : newCommentsSet) {
                    session.saveOrUpdate(newComment);
                }

                for (final EppStdDisciplineComment comment : stdImportWrapper.getRemovedDisciplineComments()) {
                    session.delete(comment);
                }

                // обновляем связи компетенций и дисциплин ГОСа
                final Collection<EppStdRow2SkillRelation> newRelSkillSet = stdImportWrapper.getUpdatedDisciplineSkill();
                for (final EppStdRow2SkillRelation rel : newRelSkillSet) {
                    session.saveOrUpdate(rel);
                }
                for (final EppStdRow2SkillRelation rel : stdImportWrapper.getRemovedDisciplineSkill()) {
                    session.delete(rel);
                }

                // удаляем старые строки и каскадно все что с ними связано - комментарии, связи с компетенциями
                // такая последовательность, чтобы не было попыток дважды удалить одно и тоже
                // причем начинаем с конца, из-за того же
                final LinkedHashMap<String, EppStdRow> removedStdRows = stdImportWrapper.getRemovedStdRows();
                final List<EppStdRow> removedList = new ArrayList<EppStdRow>(removedStdRows.values());
                Collections.reverse(removedList);
                for (final EppStdRow row : removedList) {
                    session.delete(row);
                }
            }

            return true;
        }
        catch (final Throwable t)
        {
            if (ignoreErrors) {
                this.logger.error(t.getMessage(), t);
                return false;
            }
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }
}
