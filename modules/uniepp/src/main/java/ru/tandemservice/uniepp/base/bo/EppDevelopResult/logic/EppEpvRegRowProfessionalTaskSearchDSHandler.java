/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowProfessionalTask;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 27.03.2017
 */
public class EppEpvRegRowProfessionalTaskSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String PROP_EDU_PLAN_VERSION_ID = "eduPlanVersionId";

    public EppEpvRegRowProfessionalTaskSearchDSHandler(String ownerId)
    {
        super(ownerId, EppEpvRegRowProfessionalTask.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long eduPlanVersionId = context.get(PROP_EDU_PLAN_VERSION_ID);
        // версия УП должна быть в любом случае
        assert eduPlanVersionId != null;

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(EppEpvRegRowProfessionalTask.class, "epvr_pt");
        builder.where(eq(
                property("epvr_pt", EppEpvRegRowProfessionalTask.epvRegistryRow().owner().eduPlanVersion().id()),
                value(eduPlanVersionId)));

        builder.order(property("epvr_pt", EppEpvRegRowProfessionalTask.professionalTask().title()));
        builder.order(caseExpr(
                exists(new DQLSelectBuilder().fromEntity(EppEduPlanVersionRootBlock.class, "epvb_r")
                        .where(eq(
                                property("epvr_pt", EppEpvRegRowProfessionalTask.epvRegistryRow().owner().id()),
                                property("epvb_r", EppEduPlanVersionRootBlock.id())))
                        .column("epvb_r.id").buildQuery()),
                value(0), value(1)));
        builder.order(property("epvr_pt", EppEpvRegRowProfessionalTask.epvRegistryRow().title()));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().pageable(true).build();
    }
}
