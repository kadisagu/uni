/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.StudentHistory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.StudentHistory.logic.EppWorkPlanStudentHistoryDSHandler;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.StudentHistory.logic.EppWorkPlanStudentHistoryVO;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

/**
 * @author nvankov
 * @since 10/8/14
 */
@Configuration
public class EppWorkPlanStudentHistory extends BusinessComponentManager
{
    public static final String HISTORY_DS = "historyDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(HISTORY_DS, historyDSColumns(), historyDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint historyDSColumns()
    {
        final IMergeRowIdResolver mergeBySepv = entity -> String.valueOf(((EppWorkPlanStudentHistoryVO) entity).getSepv().getId());

        return this.columnListExtPointBuilder(HISTORY_DS)
                .addColumn(publisherColumn("epvTitle", EppStudent2EduPlanVersion.eduPlanVersion().fullTitle().fromAlias("sepv"))
                                   .primaryKeyPath(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("sepv"))
                                   .merger(mergeBySepv))
                .addColumn(publisherColumn("epvBlock", EppStudent2EduPlanVersion.block().title().fromAlias("sepv"))
                                   .primaryKeyPath(EppStudent2EduPlanVersion.block().id().fromAlias("sepv"))
                                   .merger(mergeBySepv))
                .addColumn(publisherColumn("customPlan", EppStudent2EduPlanVersion.customEduPlan().titleWithGrid().fromAlias("sepv"))
                                   .primaryKeyPath(EppStudent2EduPlanVersion.customEduPlan().id().fromAlias("sepv"))
                                   .merger(mergeBySepv))
                .addColumn(dateColumn("epvRemovalDate", EppStudent2EduPlanVersion.removalDate().fromAlias("sepv"))
                                   .merger(mergeBySepv))
                .addColumn(actionColumn("deleteSepv", CommonDefines.ICON_DELETE, "onClickDeleteSepv")
                                   .permissionKey("connect_eppEduPlan_student")
                                   .merger(mergeBySepv)
                                   .alert(alert("historyDS.deleteSepv.alert", EppStudent2EduPlanVersion.eduPlanVersion().title().fromAlias("sepv"))))
                .addColumn(publisherColumn("wpTitle", EppStudent2WorkPlan.workPlan().title().fromAlias("swp"))
                                   .publisherLinkResolver(new IPublisherLinkResolver()
                                   {
                                       @Override
                                       public Object getParameters(IEntity entity)
                                       {
                                           return ((EppWorkPlanStudentHistoryVO) entity).getSwp().getWorkPlan().getWorkPlan().getId();
                                       }

                                       @Override
                                       public String getComponentName(IEntity entity)
                                       {
                                           return ru.tandemservice.uniepp.component.workplan.WorkPlanPub.Model.class.getPackage().getName();
                                       }
                                   }))
                .addColumn(dateColumn("swpRemovalDate", EppStudent2WorkPlan.removalDate().fromAlias("swp")))
                .addColumn(actionColumn("deleteSwp", CommonDefines.ICON_DELETE, "onClickDeleteSwp").disabled("deleteSpwDisabled").permissionKey("connect_eppEduPlan_student")
                                   .alert(alert("historyDS.deleteSwp.alert", EppStudent2WorkPlan.workPlan().title().fromAlias("swp"))))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler historyDSHandler()
    {
        return new EppWorkPlanStudentHistoryDSHandler(getName());
    }

}



    