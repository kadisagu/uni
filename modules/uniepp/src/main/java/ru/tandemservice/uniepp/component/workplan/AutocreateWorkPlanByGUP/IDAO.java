package ru.tandemservice.uniepp.component.workplan.AutocreateWorkPlanByGUP;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {

    void onChangeEppYear(Model model);
    void onChangeEppWorkGraph(Model model);
    void save(Model model);

}
