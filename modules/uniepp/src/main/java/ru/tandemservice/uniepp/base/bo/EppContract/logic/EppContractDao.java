package ru.tandemservice.uniepp.base.bo.EppContract.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.gen.CtrContractTypeGen;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.bo.EduProgramContract.EduProgramContractManager;
import ru.tandemservice.uniepp.IUniEppDefines;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * @author vdanilov
 */
@SuppressWarnings("deprecation")
public class EppContractDao extends UniBaseDao implements IEppContractDao
{
    /** специапльное свойство, чтобы показывать или не показывать договоры */
    private final SingleValueCache<Boolean> SHOW_STUDENT_CONTRACTS = new SingleValueCache<Boolean>() {
        @Override protected Boolean resolve() {
            try {
                final String property = StringUtils.trimToNull(ApplicationRuntime.getProperty(EppContractDao.class.getName()+".show"));
                return (null != property) && Boolean.valueOf(property);
            } catch (final Throwable t) {
                return Boolean.TRUE;
            }
        }
    };


    @Override
    public Collection<Student> listStudents(final CtrContractObject contract)
    {
        final List<Long> studentIds = new DQLSelectBuilder()
        .fromEntity(EppCtrEducationResult.class, "r").column(property(EppCtrEducationResult.target().student().id().fromAlias("r")))
        .where(eq(property(EppCtrEducationResult.contract().fromAlias("r")), value(contract)))
        .predicate(DQLPredicateType.distinct)
        .createStatement(getSession()).list();

        // делать сразу distinct нельзя - по объектам он не работает
        return getList(Student.class, "id", studentIds);
    }

    @Override public boolean canShowContractStudentTab(final Long studentId) {
        return Boolean.TRUE.equals(this.SHOW_STUDENT_CONTRACTS.get());
    }

    @Override
    public CtrContractType getEppContractRootType() {
        return this.getByNaturalId(new CtrContractTypeGen.NaturalId(IUniEppDefines.CTR_CONTRACT_TYPE_EPP));
    }

    @Override
    public CtrContractType getEppContractType(final EppEduPlanVersion eduPlanVersion)
    {
        final CtrContractType eppContractRootType = this.getEppContractRootType();

        Map<String,String> classMap = EduProgramContractManager.instance().eduProgramClassContractTypeExtPoint().getItems();

        Class programClass = eduPlanVersion.getEduPlan().getEduProgramClass();
        while (null != programClass) {
            final String code = classMap.get(programClass.getName());
            if (null != code)
            {
                // по коду берем элемент справочника, если нет - возвращаем eppRoot
                final CtrContractType type = this.getByNaturalId(new CtrContractTypeGen.NaturalId(code));
                if (null == type) { return eppContractRootType; }

                // проверяем, что то, что мы взяли - содержится в ветке eppRoot
                CtrContractType tp = type;
                while (null != tp) {
                    if (tp.equals(eppContractRootType)) { return type; }
                    tp = tp.getParent();
                }
                return eppContractRootType;
            }

            // пробуем для другого класса
            programClass = programClass.getSuperclass();
        }

        // если так ничего и не нашли - то возвращаем eppRoot
        return eppContractRootType;
    }


    @Override
    public Collection<? extends ContactorPerson> getAcademyPresenters (final OrgUnit orgUnit) {
        Collection<ContactorPerson> collection = EduContractManager.instance().dao().getAcademyPresenters(orgUnit);
        if (collection.isEmpty()) { throw new ApplicationException(EduProgramContractManager.instance().getProperty("error.edu-academy-presenter-not-configured", orgUnit.getFullTitle())); }
        return collection;
    }

    @SuppressWarnings("deprecation")
    @Override
    public CtrContractVersion doCreateContractObject(final EppStudent2EduPlanVersion student, final String contractNumber, final Date date, final CtrPriceElementCost cost, final CtrContractType contractType)
    {
        final CtrContractVersion contractVersion = new EppStudentContractObjectFactory() {
            @Override protected CtrContractType getCtrContractObjectType() { return contractType; }

            @Override
            protected CtrContractVersionCreateData getVersionCreateData()
            {
                return null;
            }

            @Override protected EppStudent2EduPlanVersion getStduent2EduPlanVersion() { return student; }
            @Override protected String getCtrContractObjectNumber() { return contractNumber; }
            @Override protected Date getStudentEnrollmentDate(final Student student) {  return date; }

            @Override protected Collection<CtrPriceElementCost> getEppCtrEducationPromiceCostList(
                final Student student,
                final EppCtrEducationPromice eduPromice,
                final EppEduPlanVersionBlock epvBlock,
                final CtrContractVersionContractor paymentSource,
                final Currency defaultCurrency
            ) {
                return (null == cost ? Collections.<CtrPriceElementCost>emptyList() : Collections.singleton(cost));
            }

        }.buildContractObject();

        final EppCtrEducationResult relation = new EppCtrEducationResult();
        relation.setContract(contractVersion.getContract());
        relation.setTarget(student);
        relation.setTimestamp(new Date());
        this.save(relation);

        return contractVersion;
    }


    @Override
    public void doDeleteContract(final EppCtrEducationResult educationResult) {
        this.delete(educationResult);
        CtrContractVersionManager.instance().dao().doDelete(educationResult.getContract());
    }

    @Override
    @Deprecated
    public List<CtrContractType> getEppContractTypes() {
        return EduContractManager.instance().dao().getContractTypes(null);
    }

    @Override
    @Deprecated
    public String getNewNumber(int eduYear) {
        return EduContractManager.instance().dao().doGetNextNumber(String.valueOf(eduYear));
    }
}
