package ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvRegistryElement;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.LoadAddEditModel;
import ru.tandemservice.uniepp.dao.registry.EppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public class Model extends LoadAddEditModel<EppEpvRegistryRow> {

    @Override protected EppEpvRegistryRow newInstance(final EppEduPlanVersionBlock block) {
        return new EppEpvRegistryRow(block);
    }
    @Override protected EppEpvRegistryRow check(final EppEpvRegistryRow entity) {
        return entity;
    }

    private ISelectModel ownerSelectModel;
    public ISelectModel getOwnerSelectModel() { return this.ownerSelectModel; }
    public void setOwnerSelectModel(final ISelectModel ownerSelectModel) { this.ownerSelectModel = ownerSelectModel; }

    private List<HSelectOption> typeList = Collections.emptyList();
    public List<HSelectOption> getTypeList() { return this.typeList; }
    public void setTypeList(final List<HSelectOption> typeList) { this.typeList = typeList; }

    private ISelectModel registrySelectModel;
    public ISelectModel getRegistrySelectModel() { return this.registrySelectModel; }
    public void setRegistrySelectModel(final ISelectModel registrySelectModel) { this.registrySelectModel = registrySelectModel; }


    public boolean isFillLoadFromRegistryElementAllowed() {
        return (null != this.getRow().getRegistryElement());
    }

    public String getRegistryElementLoadAsString() {
        if (!this.isFillLoadFromRegistryElementAllowed()) { return ""; }

        final EppRegistryElement regEl = this.getRow().getRegistryElement();
        if (null == regEl) { return ""; }

        final ViewWrapper<IEntity> wrapper = EppRegistryDAO.fillRegistryElementWrappers(ViewWrapper.getPatchedList(Collections.<IEntity>singleton(regEl))).iterator().next();
        return (String) wrapper.getViewProperty("load");
    }

    public void fillLoadFromRegistryElement() {
        if (!this.isFillLoadFromRegistryElementAllowed()) { return; }

        final EppRegistryElement regEl = this.getRow().getRegistryElement();
        if (null == regEl) { return; }

        // если есть элемент - то можем сохранить общую нагрузку
        this.getRow().setTotalLaborAsDouble(regEl.getLaborAsDouble());
        this.getRow().setHoursTotalAsDouble(regEl.getSizeAsDouble());

        final Map<String, EppRegistryElementLoad> m = IEppRegistryDAO.instance.get().getRegistryElementTotalLoadMap(Collections.singleton(regEl.getId())).get(regEl.getId());
        if (null == m) { return; }

        // если есть данные по видам нагрузки - обновляем их
        final Map<String, Double> loadMap = this.getLoadMap();
        loadMap.clear();
        for (final EppRegistryElementLoad l: m.values()) {
            loadMap.put(l.getLoadType().getFullCode(), l.getLoadAsDouble());
        }

    }


}

