package ru.tandemservice.uniepp.entity.student.slot.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Мероприятие студента из РУП (МСРП)
 *
 * Показывает, что для указанного студента в рамках его УП
 * в указанном семестре (в соответствующей части года и учебного года, например, если студент восстанавливался несколько раз - годов будет много)
 * будет изучаться указанная дисциплина (элемент реестра с указанием конкретной части)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStudentWorkPlanElementGen extends EntityBase
 implements INaturalIdentifiable<EppStudentWorkPlanElementGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement";
    public static final String ENTITY_NAME = "eppStudentWorkPlanElement";
    public static final int VERSION_HASH = -2034486919;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_REGISTRY_ELEMENT_PART = "registryElementPart";
    public static final String L_YEAR = "year";
    public static final String L_COURSE = "course";
    public static final String L_TERM = "term";
    public static final String L_PART = "part";
    public static final String L_STUDENT_EDU_PLAN_VERSION = "studentEduPlanVersion";
    public static final String L_SOURCE_ROW = "sourceRow";
    public static final String P_MODIFICATION_DATE = "modificationDate";
    public static final String P_REMOVAL_DATE = "removalDate";

    private Student _student;     // Студент
    private EppRegistryElementPart _registryElementPart;     // Часть элемента реестра
    private EppYearEducationProcess _year;     // ПУПнаГ
    private Course _course;     // Курс
    private Term _term;     // Семестр
    private YearDistributionPart _part;     // Часть учебного года
    private EppStudent2EduPlanVersion _studentEduPlanVersion;     // Актуальная связь студента с УПв
    private EppWorkPlanRow _sourceRow;     // Строка РУП (источник)
    private Date _modificationDate;     // Дата изменения
    private Date _removalDate;     // Дата утраты актуальности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getRegistryElementPart()
    {
        return _registryElementPart;
    }

    /**
     * @param registryElementPart Часть элемента реестра. Свойство не может быть null.
     */
    public void setRegistryElementPart(EppRegistryElementPart registryElementPart)
    {
        dirty(_registryElementPart, registryElementPart);
        _registryElementPart = registryElementPart;
    }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getYear()
    {
        return _year;
    }

    /**
     * @param year ПУПнаГ. Свойство не может быть null.
     */
    public void setYear(EppYearEducationProcess year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Семестр. Свойство не может быть null.
     */
    @NotNull
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр. Свойство не может быть null.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getPart()
    {
        return _part;
    }

    /**
     * @param part Часть учебного года. Свойство не может быть null.
     */
    public void setPart(YearDistributionPart part)
    {
        dirty(_part, part);
        _part = part;
    }

    /**
     * @return Актуальная связь студента с УПв.
     */
    public EppStudent2EduPlanVersion getStudentEduPlanVersion()
    {
        return _studentEduPlanVersion;
    }

    /**
     * @param studentEduPlanVersion Актуальная связь студента с УПв.
     */
    public void setStudentEduPlanVersion(EppStudent2EduPlanVersion studentEduPlanVersion)
    {
        dirty(_studentEduPlanVersion, studentEduPlanVersion);
        _studentEduPlanVersion = studentEduPlanVersion;
    }

    /**
     * @return Строка РУП (источник).
     */
    public EppWorkPlanRow getSourceRow()
    {
        return _sourceRow;
    }

    /**
     * @param sourceRow Строка РУП (источник).
     */
    public void setSourceRow(EppWorkPlanRow sourceRow)
    {
        dirty(_sourceRow, sourceRow);
        _sourceRow = sourceRow;
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getModificationDate()
    {
        return _modificationDate;
    }

    /**
     * @param modificationDate Дата изменения. Свойство не может быть null.
     */
    public void setModificationDate(Date modificationDate)
    {
        dirty(_modificationDate, modificationDate);
        _modificationDate = modificationDate;
    }

    /**
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppStudentWorkPlanElementGen)
        {
            if (withNaturalIdProperties)
            {
                setStudent(((EppStudentWorkPlanElement)another).getStudent());
                setRegistryElementPart(((EppStudentWorkPlanElement)another).getRegistryElementPart());
                setYear(((EppStudentWorkPlanElement)another).getYear());
                setCourse(((EppStudentWorkPlanElement)another).getCourse());
                setTerm(((EppStudentWorkPlanElement)another).getTerm());
                setPart(((EppStudentWorkPlanElement)another).getPart());
            }
            setStudentEduPlanVersion(((EppStudentWorkPlanElement)another).getStudentEduPlanVersion());
            setSourceRow(((EppStudentWorkPlanElement)another).getSourceRow());
            setModificationDate(((EppStudentWorkPlanElement)another).getModificationDate());
            setRemovalDate(((EppStudentWorkPlanElement)another).getRemovalDate());
        }
    }

    public INaturalId<EppStudentWorkPlanElementGen> getNaturalId()
    {
        return new NaturalId(getStudent(), getRegistryElementPart(), getYear(), getCourse(), getTerm(), getPart());
    }

    public static class NaturalId extends NaturalIdBase<EppStudentWorkPlanElementGen>
    {
        private static final String PROXY_NAME = "EppStudentWorkPlanElementNaturalProxy";

        private Long _student;
        private Long _registryElementPart;
        private Long _year;
        private Long _course;
        private Long _term;
        private Long _part;

        public NaturalId()
        {}

        public NaturalId(Student student, EppRegistryElementPart registryElementPart, EppYearEducationProcess year, Course course, Term term, YearDistributionPart part)
        {
            _student = ((IEntity) student).getId();
            _registryElementPart = ((IEntity) registryElementPart).getId();
            _year = ((IEntity) year).getId();
            _course = ((IEntity) course).getId();
            _term = ((IEntity) term).getId();
            _part = ((IEntity) part).getId();
        }

        public Long getStudent()
        {
            return _student;
        }

        public void setStudent(Long student)
        {
            _student = student;
        }

        public Long getRegistryElementPart()
        {
            return _registryElementPart;
        }

        public void setRegistryElementPart(Long registryElementPart)
        {
            _registryElementPart = registryElementPart;
        }

        public Long getYear()
        {
            return _year;
        }

        public void setYear(Long year)
        {
            _year = year;
        }

        public Long getCourse()
        {
            return _course;
        }

        public void setCourse(Long course)
        {
            _course = course;
        }

        public Long getTerm()
        {
            return _term;
        }

        public void setTerm(Long term)
        {
            _term = term;
        }

        public Long getPart()
        {
            return _part;
        }

        public void setPart(Long part)
        {
            _part = part;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppStudentWorkPlanElementGen.NaturalId) ) return false;

            EppStudentWorkPlanElementGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudent(), that.getStudent()) ) return false;
            if( !equals(getRegistryElementPart(), that.getRegistryElementPart()) ) return false;
            if( !equals(getYear(), that.getYear()) ) return false;
            if( !equals(getCourse(), that.getCourse()) ) return false;
            if( !equals(getTerm(), that.getTerm()) ) return false;
            if( !equals(getPart(), that.getPart()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudent());
            result = hashCode(result, getRegistryElementPart());
            result = hashCode(result, getYear());
            result = hashCode(result, getCourse());
            result = hashCode(result, getTerm());
            result = hashCode(result, getPart());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudent());
            sb.append("/");
            sb.append(getRegistryElementPart());
            sb.append("/");
            sb.append(getYear());
            sb.append("/");
            sb.append(getCourse());
            sb.append("/");
            sb.append(getTerm());
            sb.append("/");
            sb.append(getPart());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStudentWorkPlanElementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStudentWorkPlanElement.class;
        }

        public T newInstance()
        {
            return (T) new EppStudentWorkPlanElement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "registryElementPart":
                    return obj.getRegistryElementPart();
                case "year":
                    return obj.getYear();
                case "course":
                    return obj.getCourse();
                case "term":
                    return obj.getTerm();
                case "part":
                    return obj.getPart();
                case "studentEduPlanVersion":
                    return obj.getStudentEduPlanVersion();
                case "sourceRow":
                    return obj.getSourceRow();
                case "modificationDate":
                    return obj.getModificationDate();
                case "removalDate":
                    return obj.getRemovalDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "registryElementPart":
                    obj.setRegistryElementPart((EppRegistryElementPart) value);
                    return;
                case "year":
                    obj.setYear((EppYearEducationProcess) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "part":
                    obj.setPart((YearDistributionPart) value);
                    return;
                case "studentEduPlanVersion":
                    obj.setStudentEduPlanVersion((EppStudent2EduPlanVersion) value);
                    return;
                case "sourceRow":
                    obj.setSourceRow((EppWorkPlanRow) value);
                    return;
                case "modificationDate":
                    obj.setModificationDate((Date) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "registryElementPart":
                        return true;
                case "year":
                        return true;
                case "course":
                        return true;
                case "term":
                        return true;
                case "part":
                        return true;
                case "studentEduPlanVersion":
                        return true;
                case "sourceRow":
                        return true;
                case "modificationDate":
                        return true;
                case "removalDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "registryElementPart":
                    return true;
                case "year":
                    return true;
                case "course":
                    return true;
                case "term":
                    return true;
                case "part":
                    return true;
                case "studentEduPlanVersion":
                    return true;
                case "sourceRow":
                    return true;
                case "modificationDate":
                    return true;
                case "removalDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "registryElementPart":
                    return EppRegistryElementPart.class;
                case "year":
                    return EppYearEducationProcess.class;
                case "course":
                    return Course.class;
                case "term":
                    return Term.class;
                case "part":
                    return YearDistributionPart.class;
                case "studentEduPlanVersion":
                    return EppStudent2EduPlanVersion.class;
                case "sourceRow":
                    return EppWorkPlanRow.class;
                case "modificationDate":
                    return Date.class;
                case "removalDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStudentWorkPlanElement> _dslPath = new Path<EppStudentWorkPlanElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStudentWorkPlanElement");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getRegistryElementPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
    {
        return _dslPath.registryElementPart();
    }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> part()
    {
        return _dslPath.part();
    }

    /**
     * @return Актуальная связь студента с УПв.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getStudentEduPlanVersion()
     */
    public static EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion> studentEduPlanVersion()
    {
        return _dslPath.studentEduPlanVersion();
    }

    /**
     * @return Строка РУП (источник).
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getSourceRow()
     */
    public static EppWorkPlanRow.Path<EppWorkPlanRow> sourceRow()
    {
        return _dslPath.sourceRow();
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getModificationDate()
     */
    public static PropertyPath<Date> modificationDate()
    {
        return _dslPath.modificationDate();
    }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    public static class Path<E extends EppStudentWorkPlanElement> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private EppRegistryElementPart.Path<EppRegistryElementPart> _registryElementPart;
        private EppYearEducationProcess.Path<EppYearEducationProcess> _year;
        private Course.Path<Course> _course;
        private Term.Path<Term> _term;
        private YearDistributionPart.Path<YearDistributionPart> _part;
        private EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion> _studentEduPlanVersion;
        private EppWorkPlanRow.Path<EppWorkPlanRow> _sourceRow;
        private PropertyPath<Date> _modificationDate;
        private PropertyPath<Date> _removalDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getRegistryElementPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
        {
            if(_registryElementPart == null )
                _registryElementPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_REGISTRY_ELEMENT_PART, this);
            return _registryElementPart;
        }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> year()
        {
            if(_year == null )
                _year = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_YEAR, this);
            return _year;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> part()
        {
            if(_part == null )
                _part = new YearDistributionPart.Path<YearDistributionPart>(L_PART, this);
            return _part;
        }

    /**
     * @return Актуальная связь студента с УПв.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getStudentEduPlanVersion()
     */
        public EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion> studentEduPlanVersion()
        {
            if(_studentEduPlanVersion == null )
                _studentEduPlanVersion = new EppStudent2EduPlanVersion.Path<EppStudent2EduPlanVersion>(L_STUDENT_EDU_PLAN_VERSION, this);
            return _studentEduPlanVersion;
        }

    /**
     * @return Строка РУП (источник).
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getSourceRow()
     */
        public EppWorkPlanRow.Path<EppWorkPlanRow> sourceRow()
        {
            if(_sourceRow == null )
                _sourceRow = new EppWorkPlanRow.Path<EppWorkPlanRow>(L_SOURCE_ROW, this);
            return _sourceRow;
        }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getModificationDate()
     */
        public PropertyPath<Date> modificationDate()
        {
            if(_modificationDate == null )
                _modificationDate = new PropertyPath<Date>(EppStudentWorkPlanElementGen.P_MODIFICATION_DATE, this);
            return _modificationDate;
        }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(EppStudentWorkPlanElementGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

        public Class getEntityClass()
        {
            return EppStudentWorkPlanElement.class;
        }

        public String getEntityName()
        {
            return "eppStudentWorkPlanElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
