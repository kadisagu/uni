package ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvStructure;

import ru.tandemservice.uni.dao.IPrepareable;

public interface IDAO extends IPrepareable<Model> {
    void save(Model model);
}
