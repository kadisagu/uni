package ru.tandemservice.uniepp.migration;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.entity.*;
import org.tandemframework.core.meta.entity.model.IEntityModel;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.IDBConnect;
import org.tandemframework.dbsupport.ddl.ResultRowProcessor;
import org.tandemframework.dbsupport.ddl.catalog.CTable;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.ddl.schema.constraints.UniqueDBConstraint;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static ru.tandemservice.uniedu.migration.MS_uniedu_2x6x6_0to1.processor;


/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x6x8_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.8")
        };
    }

    private static class Field
    {
        final private String columnName; // название поля ключа
        final private Class type;
        final private String refTableName; // таблица, на которую ссылается полеs

        public Field(String columnName, Class type) {
            this.columnName = columnName;
            this.type = type;
            this.refTableName = null;
        }

        public Field(String columnName, String refTableName) {
            this.columnName = columnName;
            this.type = Long.class;
            this.refTableName = refTableName;
        }
    }

    private static class TableMergeRule
    {
        final protected boolean unique;
        final protected String tableName;
        final protected Map<String, Field> keyMap;

        public TableMergeRule(String tableName, Field ... key) {
            this(tableName, true, key);
        }
        public TableMergeRule(String tableName, boolean unique, Field ... key) {
            this.tableName = tableName;
            this.unique = unique;
            this.keyMap = new LinkedHashMap<>();
            if (null != key && key.length > 0) {
                for (Field f : key) {
                    if (null != this.keyMap.put(f.columnName, f)) {
                        throw new IllegalStateException();
                    }
                }
            }
        }

        public Long findCandidate(DBTool tool, Object[] row, List<Long> rowIds) throws Exception {
            return rowIds.get(0); // TODO
        }

        // все родительские таблицы, текущая таблица и все дочерние таблицы (по классам)
        public List<String> getTables() {
            return Collections.singletonList(tableName);
        }
    }

    private List<TableMergeRule> RULES = new ArrayList<TableMergeRule>();

    {
        if (false) {
            Collection<IEntityMeta> entities = EntityRuntime.getInstance().getEntities();

            Set<IEntityMeta> level = Collections.<IEntityMeta>singleton(EntityRuntime.getMeta(EppStudentWorkPlanElement.class));
            Set<IRelationMeta> relations = new LinkedHashSet<>();

            while (level.size() > 0) {
                Set<IEntityMeta> nextLevel = new LinkedHashSet<IEntityMeta>();

                for (IEntityMeta meta: level) {
                    for (IEntityMeta foreignEntity: entities) {
                        for (IRelationMeta rel: foreignEntity.getOutgoingRelations()) {
                            if (rel.getForeignEntity().isInterface()) { continue; }
                            if (!rel.getPrimaryEntity().equals(meta)) { continue; }
                            if (!relations.add(rel)) { continue; }

                            for (IEntityModel m: rel.getForeignEntity().getModel().getFinalModels()) {
                                nextLevel.add(m.getMeta());
                            }

                            List<IUniqueKeyMeta> uniqueConstraints = rel.getForeignProperty().getUniqueConstraints();
                            if (uniqueConstraints.size() > 0) {
                                for (IUniqueKeyMeta uniq: uniqueConstraints) {
                                    final StringBuilder p = new StringBuilder();
                                    for (IFieldPropertyMeta uniqProp: uniq.getProperties()) {
                                        if (p.length() > 0) { p.append(", "); }
                                        p.append(uniqProp.getColumnName());
                                        if (uniqProp instanceof IManyToOneMeta) {
                                            p.append(" -> ").append(((IManyToOneMeta)uniqProp).getRelation().getPrimaryEntity().getTableName());
                                        }
                                    }
                                    System.out.println("UNIQ: " + meta.getTableName() + " <- " + rel.getForeignEntity().getTableName() + " (" + p.toString() + ")");
                                }
                            } else {
                                System.out.println("REF:  " + meta.getTableName() + " <- " + rel.getForeignEntity().getTableName() + "." + rel.getForeignProperty().getColumnName());
                            }
                        }
                    }
                }

                level = nextLevel;
            }
            throw new IllegalStateException();

            //            UNIQ: epp_student_wpe_t <- session_att_slot_t (bulletin_id -> session_att_bull_t, studentwpe_id -> epp_student_wpe_t)
            //            UNIQ: epp_student_wpe_t <- epp_student_wpe_aload_t (studentwpe_id -> epp_student_wpe_t, type_id -> epp_c_loadtype_a_t)
            //            UNIQ: epp_student_wpe_t <- tr_journal_group_student (group_id -> tr_journal_group, studentwpe_id -> epp_student_wpe_t)
            //            UNIQ: epp_student_wpe_t <- tr_edugrp_event_student (event_id -> tr_edugrp_event, studentwpe_id -> epp_student_wpe_t)
            //            UNIQ: epp_student_wpe_t <- epp_student_wpe_caction_t (studentwpe_id -> epp_student_wpe_t, type_id -> epp_c_cactiontype_f_t)
            //            UNIQ: session_att_slot_t <- session_att_slot_adddata_t (slot_id -> session_att_slot_t)
            //            UNIQ: epp_student_wpe_aload_t <- epp_rgrp_g4ld_row_t (studentwpepart_id -> epp_student_wpe_aload_t, group_id -> epp_rgrp_g4ld_t)
            //            UNIQ: epp_student_wpe_caction_t <- session_doc_slot_t (document_id -> session_doc_t, studentwpecaction_id -> epp_student_wpe_caction_t, insession_p)
            //            UNIQ: epp_student_wpe_caction_t <- epp_rgrp_g4ca_row_t (studentwpepart_id -> epp_student_wpe_caction_t, group_id -> epp_rgrp_g4ca_t)
            //            UNIQ: session_doc_slot_t <- session_mark_t (slot_id -> session_doc_slot_t)
            //            UNIQ: session_doc_slot_t <- session_slot_rating_data_t (slot_id -> session_doc_slot_t)
            //            UNIQ: session_doc_slot_t <- session_project_theme_t (slot_id -> session_doc_slot_t)

        }
    }

    {

        RULES.add(new TableMergeRule(
            "session_doc_stgbook_t",
            new Field("student_id", "student_t")
        ) {
            @Override
            public List<String> getTables() {
                return Arrays.asList(this.tableName, "session_doc_t");
            }
        });

        RULES.add(new TableMergeRule(
            "epp_student_wpe_t",
            new Field("student_id", "student_t"),
            new Field("year_id", "epp_year_epp_t"),
            new Field("gridterm_id", "developgridterm_t"),
            new Field("registryelementpart_id", "epp_reg_element_part_t")
        ));

        // UNIQ: epp_student_wpe_t <- session_att_slot_t (bulletin_id -> session_att_bull_t, studentwpe_id -> epp_student_wpe_t)
        RULES.add(new TableMergeRule(
            "session_att_slot_t",
            new Field("studentwpe_id", "epp_student_wpe_t"),
            new Field("bulletin_id", "session_att_bull_t")
        ));

        // UNIQ: epp_student_wpe_t <- epp_student_wpe_aload_t (studentwpe_id -> epp_student_wpe_t, type_id -> epp_c_loadtype_a_t)
        RULES.add(new TableMergeRule(
            "epp_student_wpe_aload_t",
            new Field("studentwpe_id", "epp_student_wpe_t"),
            new Field("type_id", "epp_year_epp_t")
        ));

        // UNIQ: epp_student_wpe_t <- tr_journal_group_student (group_id -> tr_journal_group, studentwpe_id -> epp_student_wpe_t)
        RULES.add(new TableMergeRule(
            "tr_journal_group_student",
            new Field("studentwpe_id", "epp_student_wpe_t"),
            new Field("group_id", "tr_journal_group")
        ));

        // UNIQ: epp_student_wpe_t <- tr_edugrp_event_student (event_id -> tr_edugrp_event, studentwpe_id -> epp_student_wpe_t)
        RULES.add(new TableMergeRule(
            "tr_edugrp_event_student",
            new Field("studentwpe_id", "epp_student_wpe_t"),
            new Field("event_id", "tr_edugrp_event")
        ));

        // UNIQ: epp_student_wpe_t <- epp_student_wpe_caction_t (studentwpe_id -> epp_student_wpe_t, type_id -> epp_c_cactiontype_f_t)
        RULES.add(new TableMergeRule(
            "epp_student_wpe_caction_t",
            new Field("studentwpe_id", "epp_student_wpe_t"),
            new Field("type_id", "epp_c_loadtype_a_t")
        ));

        // UNIQ: session_att_slot_t <- session_att_slot_adddata_t (slot_id -> session_att_slot_t)
        RULES.add(new TableMergeRule(
            "session_att_slot_adddata_t",
            new Field("slot_id", "session_att_slot_t")
        ));

        // UNIQ: epp_student_wpe_aload_t <- epp_rgrp_g4ld_row_t (studentwpepart_id -> epp_student_wpe_aload_t, group_id -> epp_rgrp_g4ld_t)
        RULES.add(new TableMergeRule(
            "epp_rgrp_g4ld_row_t",
            new Field("studentwpepart_id", "epp_student_wpe_aload_t"),
            new Field("group_id", "epp_rgrp_g4ld_t")
        ));

        // UNIQ: epp_student_wpe_caction_t <- session_doc_slot_t (document_id -> session_doc_t, studentwpecaction_id -> epp_student_wpe_caction_t, insession_p)
        RULES.add(new TableMergeRule(
            "session_doc_slot_t",
            new Field("studentwpecaction_id", "epp_student_wpe_caction_t"),
            new Field("document_id", "session_doc_t"),
            new Field("insession_p", Boolean.class)
        ));

        // UNIQ: epp_student_wpe_caction_t <- epp_rgrp_g4ca_row_t (studentwpepart_id -> epp_student_wpe_caction_t, group_id -> epp_rgrp_g4ca_t)
        RULES.add(new TableMergeRule(
            "epp_rgrp_g4ca_row_t",
            new Field("studentwpepart_id", "epp_student_wpe_caction_t"),
            new Field("group_id", "epp_rgrp_g4ca_t")
        ));

        // UNIQ: session_doc_slot_t <- session_mark_t (slot_id -> session_doc_slot_t)
        RULES.add(new TableMergeRule(
            "session_mark_t",
            new Field("slot_id", "session_doc_slot_t")
        ) {
            @Override
            public List<String> getTables() {
                return Arrays.asList("session_mark_link_t", "session_mark_value_t", "session_mark_state_t", "session_mark_reg_t", this.tableName);
            }
            @Override public Long findCandidate(final DBTool tool, Object[] row, List<Long> rowIds) throws Exception
            {
                Long candidateId = findCandidateInt(tool, row, rowIds);

                for (Long rowId: rowIds) {
                    if (rowId.equals(candidateId)) { continue; } // оставляем только те записи, которые ссылаются на candidate - остальные удаляем
                    tool.executeUpdate("delete from session_slot_rating_data_t where slot_id=?", rowId);
                    tool.executeUpdate("delete from session_project_theme_t where slot_id=?", rowId);
                }

                return candidateId;
            }
            private Long findCandidateInt(final DBTool tool, Object[] row, List<Long> rowIds) throws Exception
            {
                // сначала ищем оценку (берем максимуальную - по шкале и внутри шкалы)
                {
                    Long candidateId = null;
                    int max_sp = Integer.MIN_VALUE, max_vp = Integer.MIN_VALUE;

                    List<Object[]> rows = tool.executeQuery(
                        processor(Long.class, Integer.class, Integer.class),
                        "select m.id, s.priority_p, v.priority_p " +
                        " from session_mark_value_t m " +
                        " inner join session_c_mark_grade_t v on (v.id = m.value_id) " +
                        " inner join epp_c_gradescale_t s on (s.id = v.scale_id) " +
                        " where m.id in ("+ StringUtils.repeat("?,", rowIds.size())+"0)",
                        (Object[])rowIds.toArray()
                    );

                    for (Object[] r: rows) {
                        int sp = ((Integer)r[1]).intValue();
                        if (sp > max_sp) {
                            max_sp = sp;
                            max_vp = Integer.MIN_VALUE;
                        }

                        if (sp == max_sp) {
                            int vp = ((Integer)r[2]).intValue();
                            if (vp > max_vp) {
                                max_vp = vp;
                                candidateId = (Long)r[0];
                            }
                        }
                    }

                    if (null != candidateId) {
                        return candidateId;
                    }
                }

                // затем ищем отметку (берем по приоритетам)
                {
                    Long candidateId = null;
                    int max_sp = Integer.MIN_VALUE;

                    List<Object[]> rows = tool.executeQuery(
                        processor(Long.class, Integer.class),
                        "select m.id, s.priority_p " +
                        " from session_mark_value_t m " +
                        " inner join session_mark_state_t v on (v.id = m.id) " +
                        " inner join session_c_mark_state_t s on (s.id = v.value_id) " +
                        " where m.id in ("+StringUtils.repeat("?,", rowIds.size())+"0)",
                        (Object[])rowIds.toArray()
                    );

                    for (Object[] r: rows) {
                        int sp = ((Integer)r[1]).intValue();
                        if (sp > max_sp) {
                            max_sp = sp;
                            candidateId = (Long)r[0];
                        }
                    }

                    if (null != candidateId) {
                        return candidateId;
                    }
                }

                // берем первую попавшуюся
                return super.findCandidate(tool, row, rowIds);
            }
        });

        // UNIQ: session_doc_slot_t <- session_slot_rating_data_t (slot_id -> session_doc_slot_t)
        RULES.add(new TableMergeRule(
            "session_slot_rating_data_t",
            new Field("slot_id", "session_doc_slot_t")
        ));

        // UNIQ: session_doc_slot_t <- session_project_theme_t (slot_id -> session_doc_slot_t)
        RULES.add(new TableMergeRule(
            "session_project_theme_t",
            new Field("slot_id", "session_doc_slot_t")
        ));

        // REF: session_mark_reg_t <- session_mark_link_t (target_id -> session_mark_reg_t)
        RULES.add(new TableMergeRule(
                "session_mark_link_t", false,
                new Field("target_id", "session_mark_t" /* здесь ссылка на корневую таблицу */)
        ));

    }


    private void merge_records(DBTool tool, final TableMergeRule rule) throws Exception
    {
        CTable table = tool.table(rule.tableName);
        String tableName = table.name().toLowerCase();
        final List<String> keys = new ArrayList<>(rule.keyMap.keySet());

        // перечень всех, кто ссылается на данную таблицу через уникальные поля
        final Map<Field, TableMergeRule> refFields = new LinkedHashMap<>();
        for (TableMergeRule r: RULES) {
            if (!tool.tableExists(r.tableName)) { continue; }
            boolean used = false;
            for (Map.Entry<String, Field> e: r.keyMap.entrySet()) {
                for (String tblName: rule.getTables()) {
                    if (tblName.equalsIgnoreCase(e.getValue().refTableName)) {
                        if (!tool.columnExists(r.tableName, e.getKey())) {
                            throw new IllegalStateException("columnExists(table="+r.tableName+", column="+e.getKey()+") = false");
                        }
                        refFields.put(e.getValue(), r);
                        used = true;
                    }
                }
            }
            if (used) {
                // все таблицы, которые ссылались на данную должны остаться без констрейнтов (в текущей таблице мы будем делать мерж - в ссылающихся появятся дубли)
                tool.table(r.tableName).uniqueKeys().clear();
            }
        }

        // удалять констрейнты в getTables (там fk по id на вышестоящую таблицу)
        for (String tblName: rule.getTables()) {
            tool.table(tblName).foreignKeys().clear();
        }

        // мержим, если оно уникально и ключи указаны
        if (rule.unique && keys.size() > 0) {

            // колонки ключа для запроса
            StringBuilder columnsBuilder = new StringBuilder();
            for (String key: keys) {
                if (columnsBuilder.length() > 0) { columnsBuilder.append(", "); }
                columnsBuilder.append(key);
            }

            // список ключей
            final List<Object[]> rows;
            {
                String nonUniqSql = new StringBuilder("select ")
                        .append(columnsBuilder)
                        .append(" from ")
                        .append(tableName)
                        .append(" group by ")
                        .append(columnsBuilder)
                        .append(" having count(id) > 1")
                        .toString();

                rows = tool.executeQuery(new ResultRowProcessor<List<Object[]>>(new ArrayList<Object[]>()) {
                    @Override
                    protected void processRow(IDBConnect ctool, ResultSet rs, List<Object[]> result) throws SQLException {
                        final List<Object> row = new ArrayList<Object>(keys.size());
                        int i = 1;
                        for (String key : keys) {
                            Field field = rule.keyMap.get(key);
                            Object raw = rs.getObject(i++);
                            Object value = (null == raw ? null : ConvertUtils.convert(raw, field.type));
                            row.add(value);
                        }
                        result.add(row.toArray());
                    }
                }, nonUniqSql);

                ((ArrayList) rows).trimToSize();
            }

            // список id таблицы, которые соотвутствуют каждому ключу
            for (Object[] row : rows) {
                StringBuilder idsBuilder = new StringBuilder("select id from ")
                        .append(tableName)
                        .append(" where 1=1 ");
                for (String key : keys) {
                    idsBuilder.append(" and ").append(key).append("= ? ");
                }

                List<Long> rowIds = tool.executeQuery(
                        new ResultRowProcessor<List<Long>>(new ArrayList<Long>()) {
                            @Override
                            protected void processRow(IDBConnect ctool, ResultSet rs, List<Long> result) throws SQLException {
                                result.add((Long) ConvertUtils.convert(rs.getObject(1), Long.class));
                            }
                        },
                        idsBuilder.toString(),
                        (Object[]) row
                );

                if (rowIds.size() <= 1) {
                    throw new IllegalStateException(); // что-то пошло не так, выбирали комбинации, у который id больше одного
                }

                Long candidateId = rule.findCandidate(tool, row, rowIds);
                for (Long rowId : rowIds) {
                    if (rowId.equals(candidateId)) {
                        continue;
                    }
                    for (Map.Entry<Field, TableMergeRule> e : refFields.entrySet()) {
                        String columnName = e.getKey().columnName;
                        tool.executeUpdate("update " + e.getValue().tableName + " set " + columnName + " = ? where " + columnName + " = ?", candidateId, rowId);
                    }

                    // удаляем дубль (связи уже переброшены)
                    for (String t : rule.getTables()) {
                        tool.executeUpdate("delete from " + t + " where id=?", rowId);
                    }
                }
            }

            // теперь во всех ссылающихся таблицах надо делать мерж (разумеется, уникальный перечень)
            for (TableMergeRule r : new LinkedHashSet<TableMergeRule>(refFields.values())) {
                merge_records(tool, r);
            }

            // устанавливаем уникальный констрейнт на ключ (проверяем, что все в базе хорошо)
            String name = "tmp_key_" + rule.hashCode();
            tool.createConstraint(tableName, new UniqueDBConstraint(name, rule.keyMap.keySet()));
            tool.dropConstraint(tableName, name);
        }
    }

    private void merge_records(DBTool tool, String tableName) throws Exception
    {
        List<TableMergeRule> rules = new ArrayList<>();
        for (TableMergeRule r: RULES) {
            if (!tool.tableExists(r.tableName)) { continue; }
            if (tableName.equalsIgnoreCase(r.tableName)) { rules.add(r); }
        }

        for (TableMergeRule r: rules) {
            merge_records(tool, r);
        }

        for (TableMergeRule r: rules) {
            String name = "tmp_key_"+r.hashCode();
            tool.createConstraint(tableName, new UniqueDBConstraint(name, r.keyMap.keySet()));
            tool.dropConstraint(tableName, name);
        }
    }


    @Override
    public void run(DBTool tool) throws Exception
    {

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppStudentWpCActionSlot -> eppStudentWpeCAction
        {
            tool.renameTable("epp_studentepv_slot_caction_t", "epp_student_wpe_caction_t");
            tool.renameColumn("epp_student_wpe_caction_t", "slot_id", "studentwpe_id");
            tool.entityCodes().rename("eppStudentWpCActionSlot", "eppStudentWpeCAction");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppStudentWpALoadSlot -> eppStudentWpeALoad
        {
            tool.renameTable("epp_studentepv_slot_aload_t", "epp_student_wpe_aload_t");
            tool.renameColumn("epp_student_wpe_aload_t", "slot_id", "studentwpe_id");
            tool.entityCodes().rename("eppStudentWpALoadSlot", "eppStudentWpeALoad");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppStudentEpvSlot -> eppStudentWorkPlanElement
        {
            tool.renameTable("epp_studentepv_slot_t", "epp_student_wpe_t");
            tool.entityCodes().rename("eppStudentEpvSlot", "eppStudentWorkPlanElement");
            tool.renameColumn("epp_student_wpe_t", "activityelementpart_id", "registryelementpart_id");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppRealEduGroup4ActionTypeRow
        {
            tool.renameColumn("epp_rgrp_g4ca_row_t", "student_id", "studentwpepart_id");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppRealEduGroup4LoadTypeRow
        {
            tool.renameColumn("epp_rgrp_g4ld_row_t", "student_id", "studentwpepart_id");
        }

        // код из MS_unisession_2x6x8_0to1
        if (tool.tableExists("session_doc_t"))
        {
            ////////////////////////////////////////////////////////////////////////////////
            // сущность sessionGlobalDocument
            {
                // создать таблицу
                DBTable dbt = new DBTable("session_doc_global_t",
                    new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                    new DBColumn("educationyear_id", DBType.LONG).setNullable(false),
                    new DBColumn("yeardistributionpart_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("sessionGlobalDocument");

            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность sessionStudentNotAllowed
            if (tool.tableExists("session_student_na_t")) {
                tool.renameColumn("session_student_na_t", "document_id", "session_id");
            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность sessionStudentNotAllowedForBulletin
            if (tool.tableExists("session_student_na4b_t")) {
                tool.renameColumn("session_student_na4b_t", "document_id", "bulletin_id");
            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность sessionAttestationSlot
            if (tool.tableExists("session_att_slot_t")) {
                tool.renameColumn("session_att_slot_t", "student_id", "studentwpe_id");
            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность sessionDocumentSlot
            if (tool.tableExists("session_doc_slot_t")) {
                tool.renameColumn("session_doc_slot_t", "student_id", "studentwpecaction_id");
            }

            ////////////////////////////////////////////////////////////////////////////////
            // session_doc_stgbook_t.student теперь ссылается не на s2epv а на student
            {
                tool.renameColumn("session_doc_stgbook_t", "student_id", "studenteduplanversion_id"); // теперь связь студента с УП(в) хранится отдельным полем
                tool.createColumn("session_doc_stgbook_t", new DBColumn("student_id", DBType.LONG).setNullable(true)); // новое поле - ссылка на студента

                // переносим данные
                tool.executeUpdate("update session_doc_stgbook_t set student_id=(select s2epv.student_id from epp_student_eduplanversion_t s2epv where s2epv.id=studenteduplanversion_id)");
                tool.setColumnNullable("session_doc_stgbook_t", "student_id", true);
                tool.dropColumn("session_doc_stgbook_t", "studenteduplanversion_id");

                // теперь надо мержить по ключу
                merge_records(tool, "session_doc_stgbook_t");
            }


            ////////////////////////////////////////////////////////////////////////////////
            // сущность sessionObject
            if (tool.tableExists("session_object_t")) {
                // создать колонку (она переехала из child в root)
                tool.createColumn("session_object_t", new DBColumn("discriminator", DBType.SHORT));

                // заполнить discriminator, вытащив код сущности из id
                String functionPrefix = tool.getDataSource().getSqlFunctionPrefix();
                tool.executeUpdate("update session_object_t set discriminator=" + functionPrefix + "getentitycode(id) where discriminator is null");

                // сделать колонку NOT NULL
                tool.setColumnNullable("session_object_t", "discriminator", false);

                // создать колонку number_p
                tool.createColumn("session_object_t", new DBColumn("number_p", DBType.createVarchar(255)));
                tool.executeUpdate("update session_object_t set number_p=(select x.number_p from session_doc_t x where x.id=session_object_t.id)");

                // создать колонку formingDate
                tool.createColumn("session_object_t", new DBColumn("formingdate_p", DBType.DATE));
                tool.executeUpdate("update session_object_t set formingdate_p=(select x.formingdate_p from session_doc_t x where x.id=session_object_t.id)");

                // создать колонку closeDate
                tool.createColumn("session_object_t", new DBColumn("closedate_p", DBType.DATE));
                tool.executeUpdate("update session_object_t set closedate_p=(select x.closedate_p from session_doc_t x where x.id=session_object_t.id)");
            }

            ////////////////////////////////////////////////////
            // перенос оценок из session_object_t в session_doc_global_t (мерж)
            {
                tool.table("session_doc_t").checkConstraints().clear(); // здесь констрейнт на id и discriminator (они еще не знают про sessionGlobalDocument)

                // создаем session_doc_global_t
                short entityCode_sessionGlobalDocument = tool.entityCodes().ensure("sessionGlobalDocument");

                List<Object[]> yearPartRows = tool.executeQuery(
                    processor(Long.class, Long.class),
                    "select distinct so.educationyear_id, so.yeardistributionpart_id" +
                    " from session_doc_slot_t slot " +
                    " inner join session_object_t so on (so.id=slot.document_id)"
                );

                for (Object[] row: yearPartRows) {
                    Long newId = EntityIDGenerator.generateNewId(entityCode_sessionGlobalDocument);
                    Long yearId = (Long)row[0];
                    Long partId = (Long)row[1];
                    tool.executeUpdate("insert into session_doc_t(id, discriminator, number_p, formingdate_p) values (?, ?, ?, ?)", newId, entityCode_sessionGlobalDocument, yearId+"."+partId, new java.sql.Date(System.currentTimeMillis()));
                    tool.executeUpdate("insert into session_doc_global_t(id, educationyear_id, yeardistributionpart_id) values (?, ?, ?)", newId, yearId, partId);
                }

                // сбрасываем все ограничения (ниже будут неуникальные записи)
                tool.table("session_doc_slot_t").uniqueKeys().clear();

                // перебрасываем все на session_doc_global_t (здесь появляются дубли)
                tool.executeUpdate("update session_doc_slot_t set document_id=(select x.id from session_object_t so inner join session_doc_global_t x on (x.educationyear_id = so.educationyear_id and x.yeardistributionpart_id = so.yeardistributionpart_id) where so.id=document_id) where document_id in (select id from session_object_t)");

                // теперь надо мержить по ключу
                merge_records(tool, "session_doc_slot_t");

                // удалить session_doc_t
                tool.table("session_object_t").foreignKeys().clear(); // здесь будем удалять session_doc_t а на них ссылка по id из session_object_t
                tool.executeUpdate("delete from session_doc_t where id in (select id from session_object_t)");
            }
        }

        // код из MS_unitraining_2x6x8_0to1
        if (tool.tableExists("tr_journal"))
        {
            ////////////////////////////////////////////////////////////////////////////////
            // сущность trEduGroupEventStudent
            if (tool.tableExists("tr_edugrp_event_student")) {
                tool.renameColumn("tr_edugrp_event_student", "studentslot_id", "studentwpe_id");
            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность trJournalGroupStudent
            if (tool.tableExists("tr_journal_group_student")) {
                tool.renameColumn("tr_journal_group_student", "studentslot_id", "studentwpe_id");
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        // МЕРЖ: eppStudentEpvSlot.student теперь ссылается не на s2epv а на student
        {
            tool.renameColumn("epp_student_wpe_t", "student_id", "studenteduplanversion_id"); // теперь связь студента с УП(в) хранится отдельным полем
            tool.createColumn("epp_student_wpe_t", new DBColumn("student_id", DBType.LONG).setNullable(true)); // новое поле - ссылка на студента

            // переносим данные
            tool.executeUpdate("update epp_student_wpe_t set student_id=(select s2epv.student_id from epp_student_eduplanversion_t s2epv where s2epv.id=studenteduplanversion_id)");
            tool.setColumnNullable("epp_student_wpe_t", "student_id", true);

            // теперь надо мержить по ключу
            merge_records(tool, "epp_student_wpe_t");
        }
    }


}