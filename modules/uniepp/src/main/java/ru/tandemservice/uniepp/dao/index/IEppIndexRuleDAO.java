package ru.tandemservice.uniepp.dao.index;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;

/**
 * @author vdanilov
 */
public interface IEppIndexRuleDAO {
    public static final SpringBeanCache<IEppIndexRuleDAO> instance = new SpringBeanCache<IEppIndexRuleDAO>(IEppIndexRuleDAO.class.getName());

    /**
     * @return правило вычисления индекса
     * @param subjectIndex
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    IEppIndexRule getRule(EduProgramSubjectIndex subjectIndex);


}
