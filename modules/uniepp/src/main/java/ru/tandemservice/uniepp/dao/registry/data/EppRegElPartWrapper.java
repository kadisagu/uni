package ru.tandemservice.uniepp.dao.registry.data;

import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.LongAsDoubleFormatter;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.gen.EppControlActionTypeGen;
import ru.tandemservice.uniepp.entity.catalog.gen.EppFControlActionTypeGen;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public class EppRegElPartWrapper extends EppBaseRegElWrapper<EppRegistryElementPart> implements IEppRegElPartWrapper
{
    public static final IFormatter<Long> LOAD_LONG_2_DIGITS_FORMATTER = new LongAsDoubleFormatter(2);
    public static final String ERROR_NON_UNIQUE_GRADE_SCALE = EppRegElPartWrapper.class.getSimpleName()+".non-unique-grade-scale";
    public static final String ERROR_NON_UNIQUE_CONTROL_ACTION = EppRegElPartWrapper.class.getSimpleName()+".non-unique-control-action";

    private static final String MULTIPLE_SCALE_CODE_PLACEHOLDER = "*";
    private static final Long LONG_1 = (long) 1;

    // module.number -> module
    private final Map<Integer, EppRegElPartModuleWrapper> moduleMap = new TreeMap<>();
    // (control-action.full-code | group.full-code) -> scale.code
    private final Map<String, String> gradeScaleMap = new HashMap<>(4);
    private boolean multipleActionsForGroupError = false;

    // Строка нагрузки части элемента реестра
    private String loadString;
    // Типы форм итогового контроля части
    private final List<EppFControlActionType> fControlActions = new ArrayList<>();
    // Типы ФИК части в виде строки
    private String fControlActionsString;

    public EppRegElPartWrapper(final EppRegistryElementPart item) { super(item); }

    @Override protected long resolveLocalLoad(final String fullCode) {

        if (EppELoadType.FULL_CODE_SELFWORK.equals(fullCode)) {
            // самостоятельная по части - есть разница между часами всего и полной аудиторной
            final long totalAudit = this.load(EppELoadType.FULL_CODE_AUDIT);
            final long totalSize = this.load(EppLoadType.FULL_CODE_TOTAL_HOURS);
            return Math.max(0, (totalSize - totalAudit));
        }
        else if (EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL.equals(fullCode))
        {
            // самостоятельная без контроля есть разность самостоятельной (в базе) и контроля
            return Math.max(0, this.load(EppELoadType.FULL_CODE_SELFWORK) - this.load(EppLoadType.FULL_CODE_CONTROL));
        }

        return super.resolveLocalLoad(fullCode);
    }

    @Override public String getGradeScale(final String fullCode) {
        return this.getGradeScaleMap().get(fullCode);
    }

    @Override public String getGradeScale4Group(final String groupCode) {
        final String result = this.getGradeScaleMap().get(this.getGroupGradeScaleKey(groupCode));
        if (MULTIPLE_SCALE_CODE_PLACEHOLDER.equals(result)) {
            // это свидетельство того, что у нас более одной шкалы ( на группу, см process(EppRegistryElementPartFControlAction) ) - кидаем исключение
            throw new IllegalStateException(ERROR_NON_UNIQUE_GRADE_SCALE+": group="+ groupCode);
        }
        return result;
    }

    public boolean isMultipleActionsForGroupError()
    {
        return multipleActionsForGroupError;
    }

    @Override
    public boolean hasActions4Groups(String groupCode) throws IllegalStateException {
        return (null != this.getGradeScaleMap().get(this.getGroupGradeScaleKey(groupCode)));
    }

    @Override
    public String getLoadAsString()
    {
        if (loadString == null) {
            long lecturesLoad = load(EppALoadType.FULL_CODE_TOTAL_LECTURES);
            long practicesLoad = load(EppALoadType.FULL_CODE_TOTAL_PRACTICE);
            long labsLoad = load(EppALoadType.FULL_CODE_TOTAL_LABS);
            long totalAudLoad = lecturesLoad + practicesLoad + labsLoad;
            long totalLoad = getItem().getSize();

            IFormatter<Long> formatter = LOAD_LONG_2_DIGITS_FORMATTER;
            StringBuilder str = new StringBuilder();
            str.append("ауд: ").append(formatter.format(totalAudLoad)).append("ч. (");
            str.append(formatter.format(lecturesLoad)).append("/").append(formatter.format(practicesLoad)).append("/").append(formatter.format(labsLoad)).append("), ");
            str.append("сам. ").append(formatter.format(totalLoad - totalAudLoad)).append("ч.");
            loadString = str.toString();
        }
        return loadString;
    }

    @Override
    public String getActionsAsString()
    {
        if (fControlActionsString == null)
            fControlActionsString = fControlActions.stream()
                .sorted(Comparator.comparing(EppFControlActionTypeGen::getPriority))
                .map(EppControlActionTypeGen::getShortTitle)
                .collect(Collectors.joining(", "));
        return fControlActionsString;
    }

    @Override
    public boolean hasActions4ControlActionType(EppFControlActionType actionType)
    {
        for(EppFControlActionType action : fControlActions) {
            if (null != action.getCode() && action.getCode().equals(actionType.getCode()))
                return true;
        }
        return false;
    }

    /**
     * Служебный метод (вызывается из EppRegistryDao) добавляет во wrapper данные из связи EppRegistryElementPartFControlAction
     * @param relation связь дисциплиночасти с типом ФИК
     */
    public void process(final EppRegistryElementPartFControlAction relation)
    {
        final EppFControlActionType controlAction = relation.getControlAction();
        final String fullCode = controlAction.getFullCode();

        fControlActions.add(controlAction);

        // добавляем нагрузку (количество)
        final Map<String, Long> localLoadMap = this.getLocalLoadMap();
        if (null != localLoadMap.put(fullCode, LONG_1)) { throw new IllegalStateException(ERROR_NON_UNIQUE_CONTROL_ACTION+": control="+ fullCode); }

        // добавляем шкалу (по типу фИК)
        final Map<String, String> gradeScaleMap = this.getGradeScaleMap();
        final String gradeScaleCode = relation.getGradeScale().getCode();
        if (null != gradeScaleMap.put(fullCode, gradeScaleCode)) { throw new IllegalStateException(ERROR_NON_UNIQUE_GRADE_SCALE+": control="+ fullCode); }

        // добавляем шкалу по группе ФИК
        // XXX: в идеале в части цисциплины не может быть двух мероприятий из одной группы ФИК (экзамен и экзамен накопительный одновременно)
        // XXX: тем не менее, такая ситуация возможна в ВУЗе на этапе проектирования, а иногда и на этапе эксплуатации дисциплины :(
        // XXX: поэтому, здесь более сложная проверка (которая написана из расчета, что она никогда не сработает)
        final String groupKey = this.getGroupGradeScaleKey(controlAction.getGroup().getCode());
        final String prevScale = gradeScaleMap.put(groupKey, gradeScaleCode);
        multipleActionsForGroupError = multipleActionsForGroupError || null != prevScale;
        if ((null != prevScale) && !prevScale.equals(gradeScaleCode)) {
            // итак, мы в ситуации, когда у нас есть предыдущее значение и оно не совпадает с тем, которое мы только что засунули в map.
            // эта ситуация не должна случаться, но, если случилась, то необходимо перетереть значение в map (еще раз - и это и есть неоптимальность кода).
            // перетирать будем значением "*" - гарантировано не код справочника - свидетельствует, что увы, у нас более одного элемента,
            // при этом также обеспечит срабатывание условия выше - что гарантирует, что если "*" попала в справочник, она там и останется - что и нужно
            gradeScaleMap.put(groupKey, MULTIPLE_SCALE_CODE_PLACEHOLDER);
        }
    }

    protected String getGroupGradeScaleKey(final String groupKey) {
        return "group."+groupKey;
    }

    // getters

    @Override public Map<Integer, IEppRegElPartModuleWrapper> getModuleMap() { return (Map)this.moduleMap; }
    @Override protected Collection<? extends EppBaseRegElWrapper> getChildren() { return this.moduleMap.values(); }
    @Override public int getNumber() { return this.getItem().getNumber(); }
    public Map<String, String> getGradeScaleMap() { return this.gradeScaleMap; }
}
