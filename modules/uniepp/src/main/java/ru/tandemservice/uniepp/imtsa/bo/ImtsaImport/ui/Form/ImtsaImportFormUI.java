/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.ui.Form;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.ImtsaImportManager;

/**
 * @author azhebko
 * @since 28.10.2014
 */
@Input(@Bind(key = "block", binding = "blockHolder.id"))
@Return({@Bind(key = "warningMessage", binding = "warningMessage")})
public class ImtsaImportFormUI extends UIPresenter
{
    private EntityHolder<EppEduPlanVersionBlock> _blockHolder = new EntityHolder<>();
    public EntityHolder<EppEduPlanVersionBlock> getBlockHolder() { return _blockHolder; }

    private IUploadFile _source;
    public IUploadFile getSource() { return _source; }
    public void setSource(IUploadFile source) { _source = source; }

    private String _warningMessage;

    @Override
    public void onComponentRefresh()
    {
        _blockHolder.refresh(EppEduPlanVersionBlock.class);
    }

    public void onClickApply()
    {
        _warningMessage = ImtsaImportManager.instance().dao().doImportImtsaEduPlan(getBlockHolder().getValue(), getSource());
        this.deactivate();
    }


    public String getWarningMessage()
    {
        return _warningMessage;
    }
}