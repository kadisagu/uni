/**
 * $Id$
 */
package ru.tandemservice.uniepp.dao.eduStd.io;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduStd.IEppEduStdDataDAO;
import ru.tandemservice.uniepp.dao.eduStd.data.IEppStdRowWrapper;
import ru.tandemservice.uniepp.dao.eduStd.io.XmlEduStd.*;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.uniepp.entity.catalog.EppSkillType;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;
import ru.tandemservice.uniepp.entity.std.data.*;
import ru.tandemservice.uniepp.entity.std.data.gen.EppStdRow2SkillRelationGen;
import ru.tandemservice.uniepp.entity.std.gen.EppStateEduStandardSkillGen;

import java.util.*;

/**
 * @author nkokorina
 * Created on: 05.08.2010
 */
public class EduStdImportWrapper
{
    public static final String ROOT_BLOCK = "Основной блок";

    private final Map<String, EduProgramSpecialization> strKey2Spec;
    private final Map<String, EppPlanStructure> code2PlanStruct;
    private final Map<String, EppRegistryStructure> code2RegStruct;
    private final Map<String, EppSkillType> code2skillType;

    private final EppStateEduStandard eduStd;
    private final XmlEduStd xmlEduStd;

    private final Map<String, EppStateEduStandardBlock> strKey2block;

    private final LinkedHashMap<String, EppStdRow> stdRows = new LinkedHashMap<String, EppStdRow>();
    private final LinkedHashMap<String, EppStdRow> uuid2currentRow;

    private final Set<EppStdDisciplineComment> commentDiscSet = new HashSet<EppStdDisciplineComment>();
    private final Map<String, EppStdDisciplineComment> uuid2currentComment;

    private final Map<INaturalId<EppStateEduStandardSkillGen>, EppStateEduStandardSkill> key2skill;
    private final Map<INaturalId<EppStateEduStandardSkillGen>, EppStateEduStandardSkill> newSkillMap = new HashMap<INaturalId<EppStateEduStandardSkillGen>, EppStateEduStandardSkill>();
    private final Map<String, EppSkillGroup> code2skillGroup;

    private final Map<INaturalId<EppStdRow2SkillRelationGen>, EppStdRow2SkillRelation> rowId2skillRel;
    private final Set<EppStdRow2SkillRelation> relSkillSet = new HashSet<EppStdRow2SkillRelation>();


    public EduStdImportWrapper(final EppStateEduStandard eduStandard, final XmlEduStd xmlEduStandard)
    {
        this.eduStd = eduStandard;
        this.xmlEduStd = xmlEduStandard;

        // получаем все компетепции для данного госа
        final List<EppStateEduStandardSkill> skillList = UniDaoFacade.getCoreDao().getList(EppStateEduStandardSkill.class, EppStateEduStandardSkillGen.parent().id().s(), eduStandard.getId());
        final Map<INaturalId<EppStateEduStandardSkillGen>, EppStateEduStandardSkill> key2skill = new HashMap<INaturalId<EppStateEduStandardSkillGen>, EppStateEduStandardSkill>();
        for (final EppStateEduStandardSkill skill : skillList) {
            key2skill.put(skill.getNaturalId(), skill);
        }
        this.key2skill = key2skill;

        // все типы компетенций
        final List<EppSkillType> skillType = UniDaoFacade.getCoreDao().getList(EppSkillType.class);
        final Map<String, EppSkillType> code2skillType = new HashMap<String, EppSkillType>();
        for (final EppSkillType type : skillType) {
            code2skillType.put(type.getCode() + " " + type.getTitle(), type);
        }
        this.code2skillType = code2skillType;

        // все группы компетенций
        final List<EppSkillGroup> skillGroupList = UniDaoFacade.getCoreDao().getCatalogItemList(EppSkillGroup.class);
        final Map<String, EppSkillGroup> code2skillGroup = new HashMap<String, EppSkillGroup>();
        for (final EppSkillGroup group : skillGroupList) {
            code2skillGroup.put(group.getCode() + " " + group.getTitle(), group);
        }
        this.code2skillGroup = code2skillGroup;

        // все блоки госа
        final List<EppStateEduStandardBlock> blockList = IEppEduStdIODAO.instance.get().getEduStdBlockList(this.eduStd.getId());

        final Map<String, EppStateEduStandardBlock> str2block = new HashMap<String, EppStateEduStandardBlock>(blockList.size());
        for (final EppStateEduStandardBlock block : blockList) {
            final String key = block.isRootBlock() ? ROOT_BLOCK : block.getProgramSpecialization().getTitle();
            str2block.put(key, block);
        }
        this.strKey2block = str2block;

        final Map<String, EduProgramSpecialization> str2Level = new HashMap<String, EduProgramSpecialization>();
        for (final EduProgramSpecialization spec : DataAccessServices.dao().getList(EduProgramSpecialization.class, EduProgramSpecialization.programSubject(), eduStandard.getProgramSubject())) {
            final String key = spec.isRootSpecialization() ? ROOT_BLOCK : spec.getTitle();
            str2Level.put(key, spec);
        }
        this.strKey2Spec = str2Level;

        final List<EppPlanStructure> planStructureList = UniDaoFacade.getCoreDao().getCatalogItemList(EppPlanStructure.class);
        final Map<String, EppPlanStructure> code2struct = new HashMap<String, EppPlanStructure>(planStructureList.size());
        for (final EppPlanStructure struct : planStructureList) {
            code2struct.put(struct.getCode() + " " + struct.getTitle(), struct);
        }
        this.code2PlanStruct = code2struct;

        final List<EppRegistryStructure> registryStructureList = UniDaoFacade.getCoreDao().getCatalogItemList(EppRegistryStructure.class);
        final Map<String, EppRegistryStructure> code2RegStructure = new HashMap<String, EppRegistryStructure>(registryStructureList.size());
        for (final EppRegistryStructure struct : registryStructureList) {
            code2RegStructure.put(struct.getCode() + " " + struct.getTitle(), struct);
        }
        this.code2RegStruct = code2RegStructure;

        final Map<Long, IEppStdRowWrapper> currentRowMap = IEppEduStdDataDAO.instance.get().getEduStdDataMap(Collections.singleton(this.eduStd.getId()), true).get(this.eduStd.getId()).getRowMap();
        final LinkedHashMap<String, EppStdRow> uuid2currentRow = new LinkedHashMap<String, EppStdRow>(currentRowMap.size());
        for (final IEppStdRowWrapper wrapper : currentRowMap.values()) {
            uuid2currentRow.put(wrapper.getRow().getUuid(), (EppStdRow)wrapper.getRow());
        }
        this.uuid2currentRow = uuid2currentRow;

        this.uuid2currentComment = IEppEduStdIODAO.instance.get().getEduStdDisciplineCommentMap(UniBaseDao.ids(uuid2currentRow.values()));

        // все связи дисциплин с компетенциями
        this.rowId2skillRel = IEppEduStdIODAO.instance.get().getEduStdRow2SkillRel(UniBaseDao.ids(uuid2currentRow.values()));
    }

    public EppStateEduStandard getUpdatedEduStandard() throws Exception
    {
        final String eduStdlevelType = this.eduStd.getProgramSubject().getEduProgramKind().getTitle();
        final String eduStdlevelName = this.eduStd.getProgramSubject().getTitleWithCode();
        final String eduStdGeneration = String.valueOf(this.eduStd.getGeneration().getNumber());

        if (!eduStdlevelType.equals(this.xmlEduStd.levelType) || !eduStdlevelName.equals(this.xmlEduStd.levelName) || !eduStdGeneration.equals(this.xmlEduStd.generation))
        {
            throw new ApplicationException("Направление подготовки (специальность) данного ГОСа не соответсвует направлению (специальности), указанному в файле.");
        }

        this.eduStd.setRegistrationNumber(this.xmlEduStd.regNumber);
        this.eduStd.setConfirmDate(DateUtils.parseDate(this.xmlEduStd.regDate, new String[]{"dd.MM.yyyy"}));
        return this.eduStd;
    }

    public Collection<EppStateEduStandardSkill> getUpdatedStdSkill()
    {
        if (null != this.xmlEduStd.stdSkillList)
        {
            for (final XmlStdSkill xmlSkill : this.xmlEduStd.stdSkillList)
            {
                final EppSkillGroup skillGroup = this.code2skillGroup.get(xmlSkill.group);
                if (null == skillGroup)
                {
                    UserContext.getInstance().getErrorCollector().add("Неизвестный код/название: group=`"+xmlSkill.group+"`.");
                }

                final EppSkillType skillType = this.code2skillType.get(xmlSkill.type);
                if (null == skillType)
                {
                    UserContext.getInstance().getErrorCollector().add("Неизвестный код/название: type=`"+xmlSkill.type+"`.");
                }

                EppStateEduStandardSkill databaseSkill = this.key2skill.remove(new EppStateEduStandardSkill.NaturalId(this.eduStd, xmlSkill.code));
                if (null == databaseSkill)
                {
                    databaseSkill = new EppStateEduStandardSkill();
                    databaseSkill.setCode(xmlSkill.code);
                    databaseSkill.setParent(this.eduStd);
                }
                databaseSkill.setComment(xmlSkill.comment);
                databaseSkill.setSkillGroup(skillGroup);
                databaseSkill.setSkillType(skillType);

                this.newSkillMap.put(databaseSkill.getNaturalId(), databaseSkill);
            }
        }
        return this.newSkillMap.values();
    }

    public Collection<EppStateEduStandardSkill> getRemovedStdSkills()
    {
        return this.key2skill.values();
    }

    public Collection<EppStateEduStandardBlock> getUpdatedEduStandardBlocks()
    {
        if (null != this.xmlEduStd.stdBlockList)
        {
            for (final XmlStdBlock xmlBlock : this.xmlEduStd.stdBlockList)
            {
                final String key = xmlBlock.levelName + xmlBlock.levelType;
                EppStateEduStandardBlock currentBlock = this.strKey2block.get(key);

                // если в файле добавили новый блок, то создаем, в других случайх ничего не делаем
                if (null == currentBlock)
                {
                    currentBlock = new EppStateEduStandardBlock();
                    currentBlock.setStateEduStandard(this.eduStd);

                    final String levelKey = xmlBlock.levelName + " " + xmlBlock.levelType;
                    final EduProgramSpecialization currentSpec = this.strKey2Spec.get(levelKey);
                    if (null != currentSpec) {
                        currentBlock.setProgramSpecialization(currentSpec.isRootSpecialization() ? null : currentSpec);
                        this.strKey2block.put(key, currentBlock);
                    }
                    else {
                        throw new ApplicationException("Недопустимая направленность блока для данного ГОСа.");
                    }
                }
            }
        }
        return this.strKey2block.values();
    }

    public LinkedHashMap<String, EppStdRow> getUpdatedEduStandardRows()
    {
        this.stdRows.clear();
        this.commentDiscSet.clear();

        if (null != this.xmlEduStd.stdBlockList)
        {
            for (final XmlStdBlock xmlBlock : this.xmlEduStd.stdBlockList)
            {
                final String key = xmlBlock.levelName + xmlBlock.levelType;
                final EppStateEduStandardBlock currentBlock = this.strKey2block.get(key);

                // начинаем работать со строками
                if (null != xmlBlock.xmlRowList) {
                    for (final XmlRow currentRow : xmlBlock.xmlRowList) {
                        this.parseXmlRowList(currentRow, null, currentBlock);
                    }
                }
            }
        }

        return this.stdRows;
    }

    private void parseXmlRowList(final XmlRow currentXmlRow, final EppStdRow parentRow, final EppStateEduStandardBlock owner)
    {
        final EppStdRow currentRow = this.getStdRowFromXml(currentXmlRow);
        currentRow.setOwner(owner);
        currentRow.setHierarhyParent(parentRow);

        final String uuid = (null == currentXmlRow.uuid) ? String.valueOf(System.identityHashCode(currentXmlRow)) : currentXmlRow.uuid;
        currentRow.setUuid(uuid);
        this.stdRows.put(uuid, currentRow);

        this.assignStdRowDescriptionFromXml(currentRow, currentXmlRow);

        this.assignStdRowSkillFromXml(currentRow, currentXmlRow);

        if (null != currentXmlRow.xmlRowList) {
            for (final XmlRow xmlRow : currentXmlRow.xmlRowList) {
                this.parseXmlRowList(xmlRow, currentRow, owner);
            }
        }
    }

    private void assignStdRowSkillFromXml(final EppStdRow row, final XmlRow xmlRow)
    {
        final List<XmlStdRowSkill> skillList = (null == xmlRow.skillList ? Collections.<XmlStdRowSkill>emptyList() : xmlRow.skillList);
        for (final XmlStdRowSkill xmlSkill : skillList)
        {
            // компетенция для дисциплины должна быть в ГОСе
            final EppStateEduStandardSkill skill = new EppStateEduStandardSkill();
            skill.setCode(xmlSkill.code);
            skill.setParent(this.eduStd);

            final EppStateEduStandardSkill lastSkill = this.newSkillMap.get(skill.getNaturalId());
            if (null == lastSkill)
            {
                UserContext.getInstance().getErrorCollector().add("Неизвестный код/название: code=`"+xmlSkill.code+"`.");
            }

            final EppStdRow2SkillRelation rel = new EppStdRow2SkillRelation();
            rel.setRow(row);
            rel.setSkill(lastSkill);

            EppStdRow2SkillRelation lastRel = this.rowId2skillRel.remove(rel.getNaturalId());
            if (null == lastRel)
            {
                lastRel = rel;
            }
            this.relSkillSet.add(lastRel);
        }
    }

    public Collection<EppStdRow2SkillRelation> getUpdatedDisciplineSkill()
    {
        return this.relSkillSet;
    }

    public Collection<EppStdRow2SkillRelation> getRemovedDisciplineSkill()
    {
        return this.rowId2skillRel.values();
    }

    private EppStdRow getStdRowFromXml(final XmlRow xmlRow)
    {
        final Long totalLaborMin = this.getMinValue(xmlRow.totalLabor);
        final Long totalLaborMax = this.getMaxValue(xmlRow.totalLabor);
        final Long totalSizeMin = this.getMinValue(xmlRow.totalSize);
        final Long totalSizeMax = this.getMaxValue(xmlRow.totalSize);

        if (xmlRow instanceof XmlStdStructureRow)
        {
            EppStdStructureRow stdRow = (EppStdStructureRow)this.uuid2currentRow.remove(xmlRow.uuid);
            if (null == stdRow)
            {
                stdRow = new EppStdStructureRow();
            }

            stdRow.setTotalLaborMin(totalLaborMin);
            stdRow.setTotalLaborMax(totalLaborMax);
            stdRow.setTotalSizeMin(totalSizeMin);
            stdRow.setTotalSizeMax(totalSizeMax);

            stdRow.setValue(this.code2PlanStruct.get(xmlRow.name));
            if (null == stdRow.getValue()) {
                UserContext.getInstance().getErrorCollector().add("Неизвестный код/название: name=`"+xmlRow.name+"`.");
            }


            return stdRow;
        }

        if (xmlRow instanceof XmlStdGroupImRow)
        {
            EppStdGroupImRow stdRow = (EppStdGroupImRow)this.uuid2currentRow.remove(xmlRow.uuid);
            if (null == stdRow)
            {
                stdRow = new EppStdGroupImRow();
            }

            stdRow.setNumber(((XmlStdGroupImRow)xmlRow).number);
            stdRow.setSize(new Integer(((XmlStdGroupImRow)xmlRow).size));
            stdRow.setTitle(((XmlStdGroupImRow)xmlRow).name);
            stdRow.setTotalLaborMin(totalLaborMin);
            stdRow.setTotalLaborMax(totalLaborMax);
            stdRow.setTotalSizeMin(totalSizeMin);
            stdRow.setTotalSizeMax(totalSizeMax);
            return stdRow;
        }

        if (xmlRow instanceof XmlStdGroupReRow)
        {
            EppStdGroupReRow stdRow = (EppStdGroupReRow)this.uuid2currentRow.remove(xmlRow.uuid);
            if (null == stdRow)
            {
                stdRow = new EppStdGroupReRow();
            }
            stdRow.setNumber(((XmlStdGroupReRow)xmlRow).number);
            stdRow.setTitle(((XmlStdGroupReRow)xmlRow).name);
            stdRow.setTotalLaborMin(totalLaborMin);
            stdRow.setTotalLaborMax(totalLaborMax);
            stdRow.setTotalSizeMin(totalSizeMin);
            stdRow.setTotalSizeMax(totalSizeMax);
            return stdRow;
        }

        if (xmlRow instanceof XmlStdDisciplineTopRow)
        {
            EppStdDisciplineTopRow stdRow = (EppStdDisciplineTopRow)this.uuid2currentRow.remove(xmlRow.uuid);
            if (null == stdRow)
            {
                stdRow = new EppStdDisciplineTopRow();
            }
            stdRow.setNumber(((XmlStdDisciplineTopRow)xmlRow).number);
            stdRow.setTitle(((XmlStdDisciplineTopRow)xmlRow).name);
            stdRow.setTotalLaborMin(totalLaborMin);
            stdRow.setTotalLaborMax(totalLaborMax);
            stdRow.setTotalSizeMin(totalSizeMin);
            stdRow.setTotalSizeMax(totalSizeMax);

            final String typeCode = ((XmlStdDisciplineTopRow)xmlRow).type;
            stdRow.setType(this.code2RegStruct.get(typeCode));
            if (null == stdRow.getType()) {
                UserContext.getInstance().getErrorCollector().add("Неизвестный код/название: type=`"+typeCode+"`.");
            }

            return stdRow;
        }

        if (xmlRow instanceof XmlStdDisciplineNestedRow)
        {
            EppStdDisciplineNestedRow stdRow = (EppStdDisciplineNestedRow)this.uuid2currentRow.remove(xmlRow.uuid);
            if (null == stdRow)
            {
                stdRow = new EppStdDisciplineNestedRow();
            }
            stdRow.setNumber(((XmlStdDisciplineNestedRow)xmlRow).number);
            stdRow.setTitle(((XmlStdDisciplineNestedRow)xmlRow).name);
            stdRow.setTotalLaborMin(totalLaborMin);
            stdRow.setTotalLaborMax(totalLaborMax);
            stdRow.setTotalSizeMin(totalSizeMin);
            stdRow.setTotalSizeMax(totalSizeMax);

            final String typeCode = ((XmlStdDisciplineNestedRow)xmlRow).type;
            stdRow.setType(this.code2RegStruct.get(typeCode));
            if (null == stdRow.getType()) {
                UserContext.getInstance().getErrorCollector().add("Неизвестный код/название: type=`"+typeCode+"`.");
            }

            return stdRow;
        }

        if (xmlRow instanceof XmlStdActionRow)
        {
            final Long totalWeeksMin = this.getMinValue(((XmlStdActionRow)xmlRow).totalWeeks);
            final Long totalWeeksMax = this.getMaxValue(((XmlStdActionRow)xmlRow).totalWeeks);

            EppStdActionRow stdRow = (EppStdActionRow)this.uuid2currentRow.remove(xmlRow.uuid);
            if (null == stdRow)
            {
                stdRow = new EppStdActionRow();
            }
            stdRow.setNumber(((XmlStdActionRow)xmlRow).number);
            stdRow.setTitle(((XmlStdActionRow)xmlRow).name);
            stdRow.setTotalLaborMin(totalLaborMin);
            stdRow.setTotalLaborMax(totalLaborMax);
            stdRow.setTotalSizeMin(totalSizeMin);
            stdRow.setTotalSizeMax(totalSizeMax);
            stdRow.setTotalWeeksMin(totalWeeksMin);
            stdRow.setTotalWeeksMax(totalWeeksMax);

            final String typeCode = ((XmlStdActionRow)xmlRow).type;
            stdRow.setType(this.code2RegStruct.get(typeCode));
            if (null == stdRow.getType()) {
                UserContext.getInstance().getErrorCollector().add("Неизвестный код/название: type=`"+typeCode+"`.");
            }

            return stdRow;
        }
        return null;
    }

    public LinkedHashMap<String, EppStdRow> getRemovedStdRows()
    {
        return this.uuid2currentRow;
    }

    private boolean assignStdRowDescriptionFromXml(final EppStdRow row, final XmlRow xmlRow)
    {
        if (xmlRow instanceof XmlStdDisciplineTopRow) {
            return this.assignStdDisciplineRowDescription(row, ((XmlStdDisciplineTopRow)xmlRow).description);
        }
        if (xmlRow instanceof XmlStdDisciplineNestedRow) {
            return this.assignStdDisciplineRowDescription(row, ((XmlStdDisciplineNestedRow)xmlRow).description);
        }
        return false;
    }

    private boolean assignStdDisciplineRowDescription(final EppStdRow row, final XmlStdComment xmlDescription)
    {
        final String description = null == xmlDescription ? null : StringUtils.trimToNull(xmlDescription.description);
        if (null != description)
        {
            EppStdDisciplineComment comment = this.uuid2currentComment.remove(row.getUuid());
            if (null == comment) {
                comment = new EppStdDisciplineComment();
            }
            comment.setRow((EppStdDisciplineBaseRow)row);
            comment.setComment(description);
            this.commentDiscSet.add(comment);
            return true;
        }
        return false;
    }

    public Set<EppStdDisciplineComment> getUpdatedDisciplineComments()
    {
        return this.commentDiscSet;
    }

    public Collection<EppStdDisciplineComment> getRemovedDisciplineComments()
    {
        return this.uuid2currentComment.values();
    }

    protected Long toLong(final String str) {
        return UniEppUtils.unwrap(UniEppUtils.toDouble(str));
    }

    private Long getMinValue(final String str)
    {
        if (StringUtils.isEmpty(str))
        {
            return null;
        }
        if (str.startsWith("?-"))
        {
            return null;
        }
        final String min = StringUtils.substringBefore(str, "-");
        if (StringUtils.isEmpty(min))
        {
            return this.toLong(str);
        }
        return this.toLong(min);
    }



    private Long getMaxValue(final String str)
    {
        if (StringUtils.isEmpty(str))
        {
            return null;
        }
        if (str.startsWith("-?"))
        {
            return null;
        }
        final String max = StringUtils.substringAfter(str, "-");
        if (StringUtils.isEmpty(max))
        {
            return this.toLong(str);
        }
        return this.toLong(max);
    }
}
