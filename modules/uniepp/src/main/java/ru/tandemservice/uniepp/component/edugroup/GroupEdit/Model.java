// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.edugroup.GroupEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;

/**
 * @author iolshvang
 * @since 12.04.2011
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="groupHolder.id", required=true),
    @Bind(key="levelId", binding="levelHolder.id")
})
public class Model
{
    final EntityHolder<EppRealEduGroup> _groupHolder = new EntityHolder<>();
    public EntityHolder<EppRealEduGroup> getGroupHolder() { return this._groupHolder; }
    public EppRealEduGroup getGroup() { return this._groupHolder.getValue(); }

    private final EntityHolder<EppRealEduGroupCompleteLevel> levelHolder = new EntityHolder<>();
    public EntityHolder<EppRealEduGroupCompleteLevel> getLevelHolder() { return this.levelHolder; }
    public EppRealEduGroupCompleteLevel getLevel() { return this.getLevelHolder().refresh(EppRealEduGroupCompleteLevel.class); }

    private ISelectModel regElementSelectModel;
    public ISelectModel getRegElementSelectModel() { return this.regElementSelectModel; }
    public void setRegElementSelectModel(ISelectModel regElementSelectModel) { this.regElementSelectModel = regElementSelectModel; }


}
