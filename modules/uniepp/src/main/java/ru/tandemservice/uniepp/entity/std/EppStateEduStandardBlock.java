package ru.tandemservice.uniepp.entity.std;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.uniepp.dao.index.IEppIndexRule;
import ru.tandemservice.uniepp.dao.index.IEppIndexRuleDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.std.gen.EppStateEduStandardBlockGen;

import java.util.Comparator;

/**
 * Государственый образовательный стандарт (блок направления подготовки)
 */
public class EppStateEduStandardBlock extends EppStateEduStandardBlockGen implements ITitled
{
    public static final Comparator<EppStateEduStandardBlock> COMPARATOR = new Comparator<EppStateEduStandardBlock>() {
        @Override public int compare(final EppStateEduStandardBlock o1, final EppStateEduStandardBlock o2) {
            if (o1.isRootBlock()) { return -1; }
            if (o2.isRootBlock()) { return 1; }

            final String dt1 = o1.getProgramSpecialization().getTitle();
            final String dt2 = o2.getProgramSpecialization().getTitle();
            return dt1.compareTo(dt2);
        }
    };

    private EppGeneration getGeneration() {
        return this.getStateEduStandard().getGeneration();
    }

    private IEppIndexRule _rule_cache = null;
    public IEppIndexRule getRule() {
        if (null == this._rule_cache) { return (this._rule_cache = IEppIndexRuleDAO.instance.get().getRule(getStateEduStandard().getProgramSubject().getSubjectIndex())); }
        return this._rule_cache;
    }


    /** @return true, если блок является основным (по направлению подготовки УП) */
    public boolean isRootBlock() {
        return getProgramSpecialization() == null;
    }


    public String getTitle() {
        if (getProgramSpecialization() != null) {
            return getProgramSpecialization().getTitle();
        }
        if (getStateEduStandard() == null) {
            return this.getClass().getSimpleName();
        }
        return getStateEduStandard().getProgramSubject().getTitleWithCode();
    }

}