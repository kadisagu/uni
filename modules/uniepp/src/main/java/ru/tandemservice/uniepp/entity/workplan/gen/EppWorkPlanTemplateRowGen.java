package ru.tandemservice.uniepp.entity.workplan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка РУП: шаблон строки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkPlanTemplateRowGen extends EppWorkPlanRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow";
    public static final String ENTITY_NAME = "eppWorkPlanTemplateRow";
    public static final int VERSION_HASH = -1057961267;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String P_PART = "part";
    public static final String P_EDUPLAN_TERM_COUNT = "eduplanTermCount";
    public static final String P_EDUPLAN_TOTAL_SIZE = "eduplanTotalSize";
    public static final String P_EDUPLAN_TOTAL_LABOR = "eduplanTotalLabor";
    public static final String P_EDUPLAN_TOTAL_WEEKS = "eduplanTotalWeeks";

    private EppRegistryStructure _type;     // Тип элемента реестра
    private int _part = 1;     // Номер части элемента реестра
    private Integer _eduplanTermCount;     // Семестров по УП
    private Long _eduplanTotalSize;     // Часов по УП
    private Long _eduplanTotalLabor;     // Трудоемкость по УП
    private Long _eduplanTotalWeeks;     // Недель по УП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип элемента реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryStructure getType()
    {
        return _type;
    }

    /**
     * @param type Тип элемента реестра. Свойство не может быть null.
     */
    public void setType(EppRegistryStructure type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Номер части элемента реестра. Свойство не может быть null.
     */
    @NotNull
    public int getPart()
    {
        return _part;
    }

    /**
     * @param part Номер части элемента реестра. Свойство не может быть null.
     */
    public void setPart(int part)
    {
        dirty(_part, part);
        _part = part;
    }

    /**
     * @return Семестров по УП.
     */
    public Integer getEduplanTermCount()
    {
        return _eduplanTermCount;
    }

    /**
     * @param eduplanTermCount Семестров по УП.
     */
    public void setEduplanTermCount(Integer eduplanTermCount)
    {
        dirty(_eduplanTermCount, eduplanTermCount);
        _eduplanTermCount = eduplanTermCount;
    }

    /**
     * @return Часов по УП.
     */
    public Long getEduplanTotalSize()
    {
        return _eduplanTotalSize;
    }

    /**
     * @param eduplanTotalSize Часов по УП.
     */
    public void setEduplanTotalSize(Long eduplanTotalSize)
    {
        dirty(_eduplanTotalSize, eduplanTotalSize);
        _eduplanTotalSize = eduplanTotalSize;
    }

    /**
     * @return Трудоемкость по УП.
     */
    public Long getEduplanTotalLabor()
    {
        return _eduplanTotalLabor;
    }

    /**
     * @param eduplanTotalLabor Трудоемкость по УП.
     */
    public void setEduplanTotalLabor(Long eduplanTotalLabor)
    {
        dirty(_eduplanTotalLabor, eduplanTotalLabor);
        _eduplanTotalLabor = eduplanTotalLabor;
    }

    /**
     * @return Недель по УП.
     */
    public Long getEduplanTotalWeeks()
    {
        return _eduplanTotalWeeks;
    }

    /**
     * @param eduplanTotalWeeks Недель по УП.
     */
    public void setEduplanTotalWeeks(Long eduplanTotalWeeks)
    {
        dirty(_eduplanTotalWeeks, eduplanTotalWeeks);
        _eduplanTotalWeeks = eduplanTotalWeeks;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppWorkPlanTemplateRowGen)
        {
            setType(((EppWorkPlanTemplateRow)another).getType());
            setPart(((EppWorkPlanTemplateRow)another).getPart());
            setEduplanTermCount(((EppWorkPlanTemplateRow)another).getEduplanTermCount());
            setEduplanTotalSize(((EppWorkPlanTemplateRow)another).getEduplanTotalSize());
            setEduplanTotalLabor(((EppWorkPlanTemplateRow)another).getEduplanTotalLabor());
            setEduplanTotalWeeks(((EppWorkPlanTemplateRow)another).getEduplanTotalWeeks());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkPlanTemplateRowGen> extends EppWorkPlanRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkPlanTemplateRow.class;
        }

        public T newInstance()
        {
            return (T) new EppWorkPlanTemplateRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                    return obj.getType();
                case "part":
                    return obj.getPart();
                case "eduplanTermCount":
                    return obj.getEduplanTermCount();
                case "eduplanTotalSize":
                    return obj.getEduplanTotalSize();
                case "eduplanTotalLabor":
                    return obj.getEduplanTotalLabor();
                case "eduplanTotalWeeks":
                    return obj.getEduplanTotalWeeks();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "type":
                    obj.setType((EppRegistryStructure) value);
                    return;
                case "part":
                    obj.setPart((Integer) value);
                    return;
                case "eduplanTermCount":
                    obj.setEduplanTermCount((Integer) value);
                    return;
                case "eduplanTotalSize":
                    obj.setEduplanTotalSize((Long) value);
                    return;
                case "eduplanTotalLabor":
                    obj.setEduplanTotalLabor((Long) value);
                    return;
                case "eduplanTotalWeeks":
                    obj.setEduplanTotalWeeks((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                        return true;
                case "part":
                        return true;
                case "eduplanTermCount":
                        return true;
                case "eduplanTotalSize":
                        return true;
                case "eduplanTotalLabor":
                        return true;
                case "eduplanTotalWeeks":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                    return true;
                case "part":
                    return true;
                case "eduplanTermCount":
                    return true;
                case "eduplanTotalSize":
                    return true;
                case "eduplanTotalLabor":
                    return true;
                case "eduplanTotalWeeks":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "type":
                    return EppRegistryStructure.class;
                case "part":
                    return Integer.class;
                case "eduplanTermCount":
                    return Integer.class;
                case "eduplanTotalSize":
                    return Long.class;
                case "eduplanTotalLabor":
                    return Long.class;
                case "eduplanTotalWeeks":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkPlanTemplateRow> _dslPath = new Path<EppWorkPlanTemplateRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkPlanTemplateRow");
    }
            

    /**
     * @return Тип элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow#getType()
     */
    public static EppRegistryStructure.Path<EppRegistryStructure> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Номер части элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow#getPart()
     */
    public static PropertyPath<Integer> part()
    {
        return _dslPath.part();
    }

    /**
     * @return Семестров по УП.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow#getEduplanTermCount()
     */
    public static PropertyPath<Integer> eduplanTermCount()
    {
        return _dslPath.eduplanTermCount();
    }

    /**
     * @return Часов по УП.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow#getEduplanTotalSize()
     */
    public static PropertyPath<Long> eduplanTotalSize()
    {
        return _dslPath.eduplanTotalSize();
    }

    /**
     * @return Трудоемкость по УП.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow#getEduplanTotalLabor()
     */
    public static PropertyPath<Long> eduplanTotalLabor()
    {
        return _dslPath.eduplanTotalLabor();
    }

    /**
     * @return Недель по УП.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow#getEduplanTotalWeeks()
     */
    public static PropertyPath<Long> eduplanTotalWeeks()
    {
        return _dslPath.eduplanTotalWeeks();
    }

    public static class Path<E extends EppWorkPlanTemplateRow> extends EppWorkPlanRow.Path<E>
    {
        private EppRegistryStructure.Path<EppRegistryStructure> _type;
        private PropertyPath<Integer> _part;
        private PropertyPath<Integer> _eduplanTermCount;
        private PropertyPath<Long> _eduplanTotalSize;
        private PropertyPath<Long> _eduplanTotalLabor;
        private PropertyPath<Long> _eduplanTotalWeeks;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow#getType()
     */
        public EppRegistryStructure.Path<EppRegistryStructure> type()
        {
            if(_type == null )
                _type = new EppRegistryStructure.Path<EppRegistryStructure>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Номер части элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow#getPart()
     */
        public PropertyPath<Integer> part()
        {
            if(_part == null )
                _part = new PropertyPath<Integer>(EppWorkPlanTemplateRowGen.P_PART, this);
            return _part;
        }

    /**
     * @return Семестров по УП.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow#getEduplanTermCount()
     */
        public PropertyPath<Integer> eduplanTermCount()
        {
            if(_eduplanTermCount == null )
                _eduplanTermCount = new PropertyPath<Integer>(EppWorkPlanTemplateRowGen.P_EDUPLAN_TERM_COUNT, this);
            return _eduplanTermCount;
        }

    /**
     * @return Часов по УП.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow#getEduplanTotalSize()
     */
        public PropertyPath<Long> eduplanTotalSize()
        {
            if(_eduplanTotalSize == null )
                _eduplanTotalSize = new PropertyPath<Long>(EppWorkPlanTemplateRowGen.P_EDUPLAN_TOTAL_SIZE, this);
            return _eduplanTotalSize;
        }

    /**
     * @return Трудоемкость по УП.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow#getEduplanTotalLabor()
     */
        public PropertyPath<Long> eduplanTotalLabor()
        {
            if(_eduplanTotalLabor == null )
                _eduplanTotalLabor = new PropertyPath<Long>(EppWorkPlanTemplateRowGen.P_EDUPLAN_TOTAL_LABOR, this);
            return _eduplanTotalLabor;
        }

    /**
     * @return Недель по УП.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow#getEduplanTotalWeeks()
     */
        public PropertyPath<Long> eduplanTotalWeeks()
        {
            if(_eduplanTotalWeeks == null )
                _eduplanTotalWeeks = new PropertyPath<Long>(EppWorkPlanTemplateRowGen.P_EDUPLAN_TOTAL_WEEKS, this);
            return _eduplanTotalWeeks;
        }

        public Class getEntityClass()
        {
            return EppWorkPlanTemplateRow.class;
        }

        public String getEntityName()
        {
            return "eppWorkPlanTemplateRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
