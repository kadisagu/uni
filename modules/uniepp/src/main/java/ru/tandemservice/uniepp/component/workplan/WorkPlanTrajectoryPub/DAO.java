/* $Id$ */
package ru.tandemservice.uniepp.component.workplan.WorkPlanTrajectoryPub;

import org.hibernate.Session;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.TitledFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/6/11
 */
public class DAO extends UniBaseDao implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        final Map<Long, IEppWorkPlanWrapper> workplanDataMap = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(model.getIds());
        if (workplanDataMap.isEmpty()) {
            model.setEmpty(true);
            return;
        }

        model.setEmpty(false);

        final EppWorkPlanBase workPlanSample = workplanDataMap.values().iterator().next().getWorkPlan();
        model.setEduPlanVersion(workPlanSample.getEduPlanVersion());
        final EppEduPlan eduPlan = workPlanSample.getEduPlan();

        final Map<Integer, IEppWorkPlanWrapper> workplanDataByTerm = new TreeMap<>();
        for (final IEppWorkPlanWrapper w: workplanDataMap.values()) {
            if (null != workplanDataByTerm.put(w.getWorkPlan().getTerm().getIntValue(), w)) {
                throw new IllegalStateException("duplicate-workplans-for-term: term="+w.getWorkPlan().getTerm().getIntValue());
            }
            if (!w.getWorkPlan().getEduPlanVersion().equals(model.getEduPlanVersion())) {
                throw new IllegalStateException("unexpected-eduplan-version: epv="+w.getWorkPlan().getEduPlanVersion().getId()+": "+w.getWorkPlan().getEduPlanVersion().getFullNumber());
            }
        }

        final List<Term> termList = this.getTermList(eduPlan);

        // список рабочих планов для отображения
        model.setWorkplanWrapperList(this.prepareWorkplanWrapperList(termList, workplanDataByTerm));

        // студенты
        this.prepareStudentList(model, termList.size(), workplanDataByTerm, model.getEduPlanVersion());
    }

    private void prepareStudentList(final Model model, final int termCount, final Map<Integer, IEppWorkPlanWrapper> workplanDataByTerm, final EppEduPlanVersion eduPlanVersion)
    {
        if (!model.isShowAsPub())
        {
            final SimpleListDataSource<Student> studentDataSource = new SimpleListDataSource<>();
            studentDataSource.setEntityCollection(Collections.<Student>emptyList());
            studentDataSource.clearColumns();
            studentDataSource.addColumn(new PublisherColumnBuilder("ФИО", Student.FIO_KEY, "studentTab").subTab("studentEppWorkplanTab").tabBind(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY).build());
            model.setStudentDataSource(studentDataSource);
            return;
        }

        final Session session = this.getSession();

        final DQLSelectBuilder studentIdsDQL = new DQLSelectBuilder()
        .fromEntity(EppStudent2EduPlanVersion.class, "s2ep")
        .joinPath(DQLJoinType.inner, EppStudent2EduPlanVersion.student().fromAlias("s2ep"), "st")
        .column("st.id")
        .where(eq(property(Student.status().active().fromAlias("st")), value(Boolean.TRUE)))
        .where(eq(property(Student.archival().fromAlias("st")), value(Boolean.FALSE)))
        .where(eq(property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("s2ep")), value(eduPlanVersion.getId())))
        .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2ep"))));

        final DQLSelectBuilder studentsDQL = new DQLSelectBuilder()
        .fromEntity(Student.class, "student")
        .column("student")
        .where(in(property(Student.id().fromAlias("student")), studentIdsDQL.buildQuery()))
        .order(property(Student.person().identityCard().fullFio().fromAlias("student")));

        final List<Student> studentList = studentsDQL.createStatement(session).<Student>list();

        final Map<Student, Map<Integer, EppWorkPlanBase>> trajectoryByStudent = org.tandemframework.core.util.cache.SafeMap.get(HashMap.class);
        final DQLSelectBuilder workplanRelaionsDQL = new DQLSelectBuilder()
        .fromEntity(EppStudent2WorkPlan.class, "s2wp")
        .column("s2wp")
        .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias("s2wp"), "wp")
        .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("s2wp"), "s2epv")
        .fetchPath(DQLJoinType.inner, EppStudent2EduPlanVersion.student().fromAlias("s2epv"), "st")

        .where(in(property(Student.id().fromAlias("st")), studentIdsDQL.buildQuery()));

        for (final EppStudent2WorkPlan rel : workplanRelaionsDQL.createStatement(session).<EppStudent2WorkPlan>list()) {
            trajectoryByStudent.get(rel.getStudentEduPlanVersion().getStudent()).put(rel.getWorkPlan().getTerm().getIntValue(), rel.getWorkPlan());
        }

        final Object[] trajectoryKey = new Object[termCount];
        for (int i = 0; i < termCount; i++) {
            trajectoryKey[i] = workplanDataByTerm.get(i+1);
        }

        final List<Student> studentsOnCurrentTrajectoryList = new ArrayList<>();
        for (final Student student : studentList)
        {
            final Map<Integer, EppWorkPlanBase> studentTrajectoryMap = trajectoryByStudent.get(student);
            if (null == student) {
                continue;
            }
            final Object[] studentTrajectoryKey = new Object[termCount];
            for (int i = 0; i < termCount; i++) {
                studentTrajectoryKey[i] = studentTrajectoryMap.get(i+1);
            }
            if (Arrays.equals(studentTrajectoryKey, trajectoryKey)) {
                studentsOnCurrentTrajectoryList.add(student);
            }
        }

        final SimpleListDataSource<Student> studentDataSource = new SimpleListDataSource<>();
        studentDataSource.setEntityCollection(studentsOnCurrentTrajectoryList);
        studentDataSource.clearColumns();
        studentDataSource.addColumn(new PublisherColumnBuilder("ФИО", Student.FIO_KEY, "studentTab").subTab("studentEppWorkplanTab").tabBind(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY).build());
        studentDataSource.addColumn(new SimpleColumn("Группа", Student.group(), TitledFormatter.INSTANCE).setOrderable(false).setClickable(false));
        studentDataSource.addColumn(new SimpleColumn("Состояние", Student.STATUS_KEY).setClickable(false).setOrderable(false));
        model.setStudentDataSource(studentDataSource);
    }

    private ArrayList<IdentifiableWrapper> prepareWorkplanWrapperList(final List<Term> termList, final Map<Integer, IEppWorkPlanWrapper> workplanDataByTerm)
    {
        final ArrayList<IdentifiableWrapper> workplanWrapperList = new ArrayList<>();
        for (final Term term : termList)
        {
            final IEppWorkPlanWrapper wrapper = workplanDataByTerm.get(term.getIntValue());
            workplanWrapperList.add(new IdentifiableWrapper(wrapper == null ? term.getId() : wrapper.getId(), wrapper == null ? term.getTitle() + " семестр - не указан РУП" : wrapper.getWorkPlan().getTitle()));
        }
        return workplanWrapperList;
    }

    private List<Term> getTermList(final EppEduPlan eduPlan)
    {
        return IDevelopGridDAO.instance.get().getDevelopGridMap(eduPlan.getDevelopGrid().getId()).values().stream()
                .map(DevelopGridTerm::getTerm)
                .collect(Collectors.toList());
    }
}
