package ru.tandemservice.uniepp.component.student.StudentEduplanBlock;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "student.id") })
public class Model
{
    private Student student = new Student();
    public Student getStudent() { return this.student; }
    public void setStudent(final Student student) { this.student = student; }

    private EppStudent2EduPlanVersion relation;
    public EppStudent2EduPlanVersion getRelation() { return this.relation; }
    public void setRelation(final EppStudent2EduPlanVersion relation) { this.relation = relation; }

    private final StaticListDataSource<IEppEpvRowWrapper> dataSource = new StaticListDataSource<IEppEpvRowWrapper>();
    public StaticListDataSource<IEppEpvRowWrapper> getDataSource() { return this.dataSource; }

    private final StaticListDataSource<IEppEpvRowWrapper> eppActionsDataSource  = new StaticListDataSource<IEppEpvRowWrapper>();
    public StaticListDataSource<IEppEpvRowWrapper> getEppActionsDataSource() { return this.eppActionsDataSource; }

}
