package ru.tandemservice.uniepp.component.eduplan.row.AttachRegistryElement;

import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author zhuj
 */
public interface IDAO extends IPrepareable<Model>
{
    EppRegistryElement doSelect(Model model);
    EppRegistryElement doCreate(Model model);
}
