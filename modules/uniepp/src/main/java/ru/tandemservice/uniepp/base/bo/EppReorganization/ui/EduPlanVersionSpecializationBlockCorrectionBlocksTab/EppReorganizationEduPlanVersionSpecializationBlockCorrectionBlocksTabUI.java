/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.ui.EduPlanVersionSpecializationBlockCorrectionBlocksTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSettingsUtil;
import ru.tandemservice.uniepp.base.bo.EppReorganization.ui.EditEduPlanVersionSpecializationBlockOwner.EppReorganizationEditEduPlanVersionSpecializationBlockOwner;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author rsizonenko
 * @since 05.04.2016
 */
public class EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTabUI extends UIPresenter {

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource
                .put(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.SUBJECT,
                        getSettings().get(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.SUBJECT));
        dataSource
                .put(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.SPECIALIZATION,
                        getSettings().get(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.SPECIALIZATION));
        dataSource
                .put(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.PROGRAM_FORM,
                        getSettings().get(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.PROGRAM_FORM));
        dataSource
                .put(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.DEV_CONDITION,
                        getSettings().get(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.DEV_CONDITION));
        dataSource
                .put(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.EDU_PROGRAM_TRAIT,
                        getSettings().get(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.EDU_PROGRAM_TRAIT));
        dataSource
                .put(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.DEVELOP_GRID,
                        getSettings().get(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.DEVELOP_GRID));
        dataSource
                .put(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.STATE,
                        getSettings().get(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.STATE));
        dataSource
                .put(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.OWNER,
                        getSettings().get(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.OWNER));

        saveSettings();
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((PageableSearchListDataSource) getConfig().getDataSource(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.BLOCKS_DS)).getOptionColumnSelectedObjects("checkboxColumn");
            selected.clear();
        }
    }

    public boolean isOwnerSelected()
    {
        return null != getSettings().get("owner");
    }

    public void onClickChangeEpvBlockOwner()
    {
        final Collection<IEntity> selected = ((PageableSearchListDataSource) getConfig().getDataSource(EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.BLOCKS_DS)).getOptionColumnSelectedObjects("checkboxColumn");
        if (selected.size() == 0) throw new ApplicationException("Не выбран ни один элемент из списка.");
        getActivationBuilder().asRegionDialog(EppReorganizationEditEduPlanVersionSpecializationBlockOwner.class)
                .parameter("multiValue",
                        selected.stream().collect(Collectors.toList())
                ).activate();
    }

    public void onClickResetFilters()
    {
        CommonBaseSettingsUtil.clearSettingsExcept(getSettings(), EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.OWNER);
    }
}
