package ru.tandemservice.uniepp.component.workplan.AutocreateWorkPlanByGUP;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.util.EppFilterUtil;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="contextId"),
    @Bind(key = "selectedYear", binding = "selectedYear")
})
public class Model {

    public static final IdentifiableWrapper<IEntity> OVERCOPY_ALL = new IdentifiableWrapper<IEntity>(1L, "Копировать РУП всегда");
    public static final IdentifiableWrapper<IEntity> OVERCOPY_NEW = new IdentifiableWrapper<IEntity>(2L, "Копировать только новые РУП");

    public static final IdentifiableWrapper<IEntity> LOOKUP_LOOKUP = new IdentifiableWrapper<IEntity>(1L, "Создать на основе РУП прошлых лет");
    public static final IdentifiableWrapper<IEntity> LOOKUP_CREATE = new IdentifiableWrapper<IEntity>(2L, "Всегда создавать на основе УП");

    private Long contextId;
    public Long getContextId() { return this.contextId; }
    public void setContextId(final Long contextId) { this.contextId = contextId; }

    private Integer selectedYearNumber = null;
    public Integer getSelectedYearNumber() { return this.selectedYearNumber; }

    public void setSelectedYear(final Object selectedYear) {
        this.selectedYearNumber = EppFilterUtil.getSelectedYear(selectedYear);
    }

    private final SelectModel<EppYearEducationProcess> yearSelectModel = new SelectModel<EppYearEducationProcess>();
    public SelectModel<EppYearEducationProcess> getYearSelectModel() { return this.yearSelectModel; }

    private final SelectModel<EppWorkGraph> workGraphSelectModel = new SelectModel<EppWorkGraph>();
    public SelectModel<EppWorkGraph> getWorkGraphSelectModel() { return this.workGraphSelectModel; }

    @SuppressWarnings("unchecked")
    private final SelectModel<IdentifiableWrapper<IEntity>> overcopySelectModel = new SelectModel<IdentifiableWrapper<IEntity>>() {
        {
            this.setSource(Model.OVERCOPY_ALL, Model.OVERCOPY_NEW);
            this.setValue(Model.OVERCOPY_ALL);
        }
    };
    public SelectModel<IdentifiableWrapper<IEntity>> getOvercopySelectModel() { return this.overcopySelectModel; }


    @SuppressWarnings("unchecked")
    private final SelectModel<IdentifiableWrapper<IEntity>> lookupSelectModel = new SelectModel<IdentifiableWrapper<IEntity>>() {
        {
            this.setSource(Model.LOOKUP_LOOKUP, Model.LOOKUP_CREATE);
            this.setValue(Model.LOOKUP_LOOKUP);
        }
    };
    public SelectModel<IdentifiableWrapper<IEntity>> getLookupSelectModel() { return this.lookupSelectModel; }

    private String comment;
    public String getComment() { return this.comment; }
    public void setComment(final String comment) { this.comment = comment; }



}
