package ru.tandemservice.uniepp.component.orgunit.EppEpvAcceptionTab;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.PredicateUtils;
import org.apache.commons.collections15.functors.NotNullPredicate;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade.Lock;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.dao.ListPage;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.component.orgunit.EppEpvAcceptionTab.Model.RegistryElementWrapper;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.EppControlFormatter;
import ru.tandemservice.uniepp.dao.registry.data.EppLoadFormatter;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author zhuj
 */
public class DAO extends UniBaseDao implements IDAO
{

    private static final Long ERROR_NO_REGEL = 1L;
    private static final Long ERROR_MISMATCH_ORG_UNIT = 2L;
    private static final Long ERROR_MISMATCH_TYPE = 3L;
    private static final Long ERROR_MISMATCH_TOTAL_LOAD = 4L;
    //    private static final Long ERROR_MISMATCH_PARTS = 5l;
    //    private static final Long ERROR_MISMATCH_LOAD_DISTRIBUTION = 6l;

    private static final List<IdentifiableWrapper<IEntity>> ERRORS = ImmutableList.of(
            new IdentifiableWrapper<>(ERROR_NO_REGEL, "Элемент реестра не указан"),
            new IdentifiableWrapper<>(ERROR_MISMATCH_ORG_UNIT, "Не совпадает читающее подразделение"),
            new IdentifiableWrapper<>(ERROR_MISMATCH_TYPE, "Не совпадает тип элемента реестра"),
            new IdentifiableWrapper<>(ERROR_MISMATCH_TOTAL_LOAD, "Не совпадают часы/трудоемкость")
            //        new IdentifiableWrapper<IEntity>(ERROR_MISMATCH_PARTS, "Не совпадает число частей"),
            //        new IdentifiableWrapper<IEntity>(ERROR_MISMATCH_LOAD_DISTRIBUTION, "Не совпадает распределение часов в частях"),
    );

    @Override
    public void prepare(final Model model)
    {
        // очищаем все, что есть в кэше сессии
        this.getSession().clear();

        model.getOrgUnitHolder().refresh();
        model.getData().setPageSize(20);

        model.setRegistryStructureList(HierarchyUtil.listHierarchyNodesWithParents(this.getList(EppRegistryStructure.class, EppRegistryStructure.P_CODE), true));
        model.setStateListModel(UniEppUtils.getStateSelectModel(null/*, getList(EppState.class, EppState.code(), Arrays.asList(EppState.STATE_ACCEPTABLE, EppState.STATE_ACCEPTED), EppState.P_CODE)*/));

        model.setProgramSubjectModel(new DQLFullCheckSelectModel(EduProgramSubject.titleWithCode().s())
        {
            @Override
            protected DQLSelectBuilder query(final String alias, final String filter)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EduProgramSubject.class, alias)
                    .order(property(alias, EduProgramSubject.subjectCode()))
                    .order(property(alias, EduProgramSubject.title()))
                    ;

                FilterUtils.applyLikeFilter(dql, filter, EduProgramSubject.subjectCode().fromAlias(alias), EduProgramSubject.title().fromAlias(alias));

                DQLSelectBuilder rowDql = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanProf.class, "p").column("p")
                    .fromEntity(EppEpvRegistryRow.class, "r")
                    .where(eq(property("r", EppEpvRegistryRow.owner().eduPlanVersion().eduPlan()), property("p")))
                    .where(eq(property(EppEpvRegistryRow.registryElementOwner().fromAlias("r")), value(model.getOrgUnit())))
                    .where(eq(property("p", EppEduPlanProf.programSubject()), property(alias)));
                dql.where(exists(rowDql.buildQuery()));

                return dql;
            }
        });

        // надо грузить явно перед кодом ниже
        model.setProgramFormListModel(new LazySimpleSelectModel<>(EduProgramForm.class));
        model.setDevelopConditionListModel(new LazySimpleSelectModel<>(this.getCatalogItemListOrderByCode(DevelopCondition.class)));
        model.setProgramTraitListModel(new LazySimpleSelectModel<>(EduProgramTrait.class));
        model.setDevelopGridListModel(new LazySimpleSelectModel<>(this.getList(DevelopGrid.class, DevelopGrid.P_CODE)));

        // ошибки
        model.setErrorListModel(new LazySimpleSelectModel<>(ERRORS));

        // предварительно грузим данные
        final Map<String, EppLoadType> ldTypeMap = EppEduPlanVersionDataDAO.getLoadTypeMap();
        final Map<String, EppControlActionType> caTypeMap = EppEduPlanVersionDataDAO.getActionTypeMap();
        final Map<Long, Map<Integer, DevelopGridTerm>> gridMap = IDevelopGridDAO.instance.get().getDevelopGridDistributionMap(null);

        final EppLoadFormatter<IEppRegElPartWrapper> ldFormatter = new EppLoadFormatter<IEppRegElPartWrapper>(ldTypeMap) {
            @Override protected Double load(final IEppRegElPartWrapper element, final String loadFullCode) {
                return element.getLoadAsDouble(loadFullCode);
            }
        };

        final EppControlFormatter<IEppRegElPartWrapper> caFormatter = new EppControlFormatter<IEppRegElPartWrapper>(caTypeMap) {
            @Override protected boolean allow(final IEppRegElPartWrapper element, final EppControlActionType actionType) {
                return (actionType instanceof EppFControlActionType);
            }
            @Override protected int size(final IEppRegElPartWrapper element, final String actionFullCode) {
                return element.getActionSize(actionFullCode);
            }
        };

        Debug.begin("prepare.list");
        try {

            // итак. делаем глобальный КЭШ для элементов реестра
            // поскольку элементов реестра мало и они могут повторятся в разных УП(в), то их имеет смысл кэшировать, чтобы сократить использование памяти
            // в то время как строки УП(в) гарантированно уникальны (даже если их данные одинаковы) и кэшировать их не нужно
            final Map<IEppRegElWrapper, Model.RegistryElementWrapper> registryElementWrapperMap = SafeMap.get(key -> {

                // распредеяем данные для отображения из частей
                final String[][] data = new String[1+key.getPartMap().size()][2];

                // нагрузка общая
                data[0][0] = UniEppUtils.formatLoad(key.getItem().getSizeAsDouble(), false) + " / " + UniEppUtils.formatLoad(key.getItem().getLaborAsDouble(), false);

                for (final IEppRegElPartWrapper p: key.getPartMap().values())
                {
                    // нагрузка и формы контроля
                    data[p.getNumber()][0] = ldFormatter.formatLoadByType(p);
                    data[p.getNumber()][1] = caFormatter.format(p);
                }

                return new RegistryElementWrapper(key.getItem(), data);
            });

            // получаем данные
            final MutableInt parts = new MutableInt(0);
            final ListPage<Object[]> rows = this.getRows(model);
            final Map<Long, IEppRegElWrapper> regElWrapperMap = this.getRegElWrapperMap(rows.getList());
            final Map<Long, List<EppEpvRegistryRow>> rowMap = this.getRowMap(rows.getList());
            final Map<Long, Model.RowWrapper> wrapperMap = new LinkedHashMap<>(rows.getList().size());

            for (List<Long> blockIds : Iterables.partition(rowMap.keySet(), 64/* маленькое число - мы экономим память*/)) {

                // важно, что все строки УПВ (с оболочками) одновременно заргужать в память НЕЛЬЗЯ //
                final Map<Long, IEppEpvBlockWrapper> iBlockWrapperMap = DAO.this.getBlockWrapperMap(blockIds);

                Debug.begin("prepare-epv-block-eduHs");
                try {

                    // грузим в сессию УП и ГОСы (для поколений)
                    new DQLSelectBuilder()
                            .fromEntity(EppEduPlan.class, "ep").column("ep")
                            .fetchPath(DQLJoinType.inner, EppEduPlan.parent().fromAlias("ep"), "std")
                            .where(in(
                                    property("ep.id"),
                                    new HashSet<>(CollectionUtils.collect(iBlockWrapperMap.values(), input -> {
                                        return input.getVersion().getEduPlan().getId();
                                    }))
                            ))
                            .createStatement(DAO.this.getSession()).list();

                    // грузим в сессию блоки УП(в) и данные по ним
                    new DQLSelectBuilder()
                            .fromEntity(EppEduPlanVersionBlock.class, "epvBlock").column("epvBlock")
                            //                        .fetchPath(DQLJoinType.inner, EppEduPlanVersionBlock.educationLevelHighSchool().fromAlias("epvBlock"), "eduHs")
                            //                        .fetchPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().fromAlias("eduHs"), "eduLvl")
                            .where(in(property(EppEduPlanVersionBlock.id().fromAlias("epvBlock")), iBlockWrapperMap.keySet()))
                            .createStatement(DAO.this.getSession()).list();

                } finally {
                    Debug.end();
                }

                for (final Long blockId: blockIds) {
                    final IEppEpvBlockWrapper iBlockWrapper = iBlockWrapperMap.get(blockId);

                    // загружаем рананее все данные по всем дисциплинам
                    for (final EppEpvRegistryRow row: rowMap.get(blockId)) {
                        final EppEduPlanVersion eduPlanVersion = row.getOwner().getEduPlanVersion();

                        final Map<Integer, DevelopGridTerm> gridTerms = gridMap.get(eduPlanVersion.getEduPlan().getDevelopGrid().getId());

                        final IEppEpvRowWrapper iEppEpvRowWrapper = iBlockWrapper.getRowMap().get(row.getId());
                        if (null == iEppEpvRowWrapper) {
                            continue; // проблемы в иерархии строк в базе - пропускаем
                        }

                        final IEppRegElWrapper iEppRegElWrapper = (null == row.getRegistryElement() ? null : regElWrapperMap.get(row.getRegistryElement().getId()));
                        final Set<Integer> activeTermSet = iEppEpvRowWrapper.getActiveTermSet();

                        final String[][] data = new String[1+activeTermSet.size()][2];

                        // общая нагрузка
                        data[0][0] =
                                UniEppUtils.formatLoad(iEppEpvRowWrapper.getTotalInTermLoad(0, EppLoadType.FULL_CODE_TOTAL_HOURS), false) + " / " +
                                UniEppUtils.formatLoad(iEppEpvRowWrapper.getTotalInTermLoad(0, EppLoadType.FULL_CODE_LABOR), false);

                        // начало обучения
                        final Integer startTermNumber = (activeTermSet.isEmpty() ? 0 : activeTermSet.iterator().next());
                        final DevelopGridTerm gridTerm = gridTerms.get(startTermNumber);
                        data[0][1] = null == gridTerm ? "" : (gridTerm.getPart().getTitle() + ", " + gridTerm.getCourse().getIntValue() + " курс");

                        int n = 1;
                        for (final Integer p: activeTermSet)
                        {
                            // нагрузка
                            {
                                data[n][0] = new EppLoadFormatter<IEppEpvRowWrapper>(ldTypeMap) {
                                    @Override protected Double load(final IEppEpvRowWrapper element, final String loadFullCode) {
                                        return element.getTotalInTermLoad(p, loadFullCode);
                                    }
                                }.formatLoadByType(iEppEpvRowWrapper);
                            }

                            // формы контроля
                            {
                                data[n][1] = new EppControlFormatter<IEppEpvRowWrapper>(caTypeMap) {
                                    @Override protected boolean allow(final IEppEpvRowWrapper element, final EppControlActionType actionType) {
                                        return (actionType instanceof EppFControlActionType);
                                    }
                                    @Override protected int size(final IEppEpvRowWrapper element, final String actionFullCode) {
                                        return element.getActionSize(p, actionFullCode);
                                    }
                                }.format(iEppEpvRowWrapper);
                            }

                            // следующая часть
                            n++;
                        }

                        final Model.RowWrapper r = new Model.RowWrapper(
                                row,
                                (null == iEppRegElWrapper ? null : registryElementWrapperMap.get(iEppRegElWrapper)),
                                data, iEppEpvRowWrapper.getIndex()
                        );

                        wrapperMap.put(r.getId(), r);

                        // обрабатываем число частей
                        parts.setValue(Math.max(
                                parts.intValue(),
                                Math.max(
                                        activeTermSet.size(),
                                        null == iEppRegElWrapper ? 0 : iEppRegElWrapper.getPartMap().size()
                                )
                        ));
                    }
                }
            }

            // ====================== //
            // сохраняем все в модель //
            // ====================== //

            // сохраняем количество частей (список номеров частей) в модель
            {
                final List<Integer> partNumbers = new ArrayList<>(parts.intValue());
                for (int i=1;i<=Math.max(1, parts.intValue());i++) { partNumbers.add(i); }
                model.setPartNumbers(partNumbers);
            }

            // нужно выстроить элементы в изначальном порядке
            model.getData().setup(rows.getCount(),
                CollectionUtils.select(
                    CollectionUtils.collect(rows.getList(), input -> {
                        return wrapperMap.get(((EppEpvRegistryRow)input[1]).getId());
                    }),
                    PredicateUtils.notNullPredicate()
                )
            );

        } finally {
            Debug.end();
        }
    }

    protected Map<Long, IEppRegElWrapper> getRegElWrapperMap(final Collection<Object[]> rows)
    {
        final Set<Long> ids = new HashSet<>();
        for (final Object[] r: rows) {
            final EppRegistryElement registryElement = ((EppEpvRegistryRow) r[1]).getRegistryElement();
            if (null != registryElement) {
                ids.add(registryElement.getId());
            }
        }
        return IEppRegistryDAO.instance.get().getRegistryElementDataMap(ids);
    }

    protected Map<Long, List<EppEpvRegistryRow>> getRowMap(final Collection<Object[]> rows)
    {
        final Map<Long, List<EppEpvRegistryRow>> rowMap = SafeMap.get(ArrayList.class);
        for (final Object[] row: rows) {
            rowMap.get((Long) row[0]).add((EppEpvRegistryRow) row[1]);
        }
        return rowMap;
    }

    /** @return ListPage (block.id, eppEpvRegRow) */
    protected ListPage<Object[]> getRows(final Model model)
    {
        final OrgUnit ou = model.getOrgUnit();
        final Session session = this.getSession();
        final IDataSettings settings = model.getSettings();

        final Collection<EppRegistryStructure> registryStructureList = this.getSelectedRegistryStructureList(model);
        final Collection<EppState> stateList = this.getSelectedStateList(model);

        final DQLSelectBuilder idsDql = new DQLSelectBuilder()
        .fromEntity(EppEpvRegistryRow.class, "r").column(property("r.id"), "id")
        .where(eq(property(EppEpvRegistryRow.registryElementOwner().fromAlias("r")), value(ou)));

        if (null != stateList) {
            idsDql.where(in(property(EppEpvRegistryRow.owner().eduPlanVersion().state().fromAlias("r")), stateList));
        }

        if (null != registryStructureList) {
            idsDql.where(in(property(EppEpvRegistryRow.registryElementType().fromAlias("r")), registryStructureList));
        }

        EduProgramSubject programSubject = settings.get("programSubject");
        if (programSubject != null) {
            idsDql.fromEntity(EppEduPlanProf.class, "prof");
            idsDql.where(eq(property("r", EppEpvRegistryRow.owner().eduPlanVersion().eduPlan()), property("prof")));
            idsDql.where(eq(property("prof", EppEduPlanProf.programSubject()), value(programSubject)));
        }
        FilterUtils.applySelectFilter(idsDql, EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().programForm().fromAlias("r"), settings.get("programForm"));
        FilterUtils.applySelectFilter(idsDql, EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().developCondition().fromAlias("r"), settings.get("developCondition"));
        FilterUtils.applySelectFilter(idsDql, EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().programTrait().fromAlias("r"), settings.get("programTrait"));
        FilterUtils.applySelectFilter(idsDql, EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().developGrid().fromAlias("r"), settings.get("developGrid"));

        FilterUtils.applyLikeFilter(idsDql, settings.<String>get("title"), EppEpvRegistryRow.title().fromAlias("r"));

        {
            final List<IdentifiableWrapper<IEntity>> errors = settings.get("errors");
            if ((null != errors) && (errors.size() > 0)) {
                final Collection<Long> errorIds = new HashSet<>(ids(errors));
                IDQLExpression e = null;

                // нет элемента реестра
                if (errorIds.contains(ERROR_NO_REGEL)) {
                    e = or(e, isNull(property(EppEpvRegistryRow.registryElement().fromAlias("r"))));
                }

                // не совпадают подразделения
                if (errorIds.contains(ERROR_MISMATCH_ORG_UNIT)) {
                    e = or(e, ne(property(EppEpvRegistryRow.registryElementOwner().fromAlias("r")), property(EppRegistryElement.owner().fromAlias("rel"))));
                }

                // не совпадают типы элементов
                if (errorIds.contains(ERROR_MISMATCH_TYPE)) {
                    e = or(e, ne(property(EppEpvRegistryRow.registryElementType().fromAlias("r")), property(EppRegistryElement.parent().fromAlias("rel"))));
                }

                // не совпадают часы и трудоемкость
                if (errorIds.contains(ERROR_MISMATCH_TOTAL_LOAD)) {
                    final int threshold = 99;
                    e = or(e, DQL.parseExpression("abs("+EppRegistryElement.size().fromAlias("rel")+"-"+EppEpvRegistryRow.hoursTotal().fromAlias("r")+") > "+threshold));
                    e = or(e, DQL.parseExpression("abs("+EppRegistryElement.labor().fromAlias("rel")+"-"+EppEpvRegistryRow.totalLabor().fromAlias("r")+") > "+threshold));
                }

                if (null != e) {
                    idsDql.joinPath(DQLJoinType.left, EppEpvRegistryRow.registryElement().fromAlias("r"), "rel");
                    idsDql.where(e);
                }
            }
        }

        customizeEpvRowIdsBuilder(idsDql, "r", settings);

        final Number totalNumber = new DQLSelectBuilder()
        .fromDataSource(idsDql.buildQuery(), "x")
        .column(DQLFunctions.count(property("x.id")))
        .createStatement(session).<Number>uniqueResult();
        final int totalSize = null == totalNumber ? 0 : totalNumber.intValue();

        final List<Object[]> rows = new DQLSelectBuilder()
        .fromEntity(EppEpvRegistryRow.class, "r")
        .column(property(EppEpvRegistryRow.owner().id().fromAlias("r")))
        .column(property("r"))
        .where(in(property("r.id"), idsDql.buildQuery()))

        // Сортировка записей в таблице (строк УПв): по данным строки (чтобы сортировка не менялась), статична (не настраивается пользователем).
        // Порядок сортировки: тип, название, ??число частей??, часов всего, трудоемкость, id - все данные по строке получаем.

        .order(property(EppEpvRegistryRow.registryElementType().code().fromAlias("r") /* TODO: sort index */))
        .order(property(EppEpvRegistryRow.title().fromAlias("r")))
        .order(property(EppEpvRegistryRow.totalLabor().fromAlias("r")))
        .order(property(EppEpvRegistryRow.hoursTotal().fromAlias("r")))

        //
        .order(property(EppEpvRegistryRow.registryElement().id().fromAlias("r")))
        .order(property(EppEpvRegistryRow.id().fromAlias("r")))

        //
        .createStatement(session)
        .setMaxResults(model.getData().getPageSize())
        .setFirstResult(model.getData().getStartIndex(totalSize))
        .list();

        return new ListPage<>(totalSize, rows);
    }

    /**
     * Модифицирует запрос, добавляет доп. критерии отбора строк учебного плана.
     * @param epvRowIdsBuilder builder строк учебного плана
     * @param epvRowAlias алиас
     * @param settings настройки пользователя
     */
    protected void customizeEpvRowIdsBuilder(DQLSelectBuilder epvRowIdsBuilder, String epvRowAlias, IDataSettings settings)
    {

    }

    protected Collection<EppState> getSelectedStateList(final Model model) {
        final EppState selected = model.getState();
        return null == selected ? /*model.getStateListModel().findValues(null).getObjects()*/ null : Collections.singleton(selected);
    }


    protected Collection<EppRegistryStructure> getSelectedRegistryStructureList(final Model model) {
        final EppRegistryStructure selected = model.getRegistryStructure();
        if (null == selected) { return null; }

        // если что-то выбранно - берем все элементы поддерева
        return CollectionUtils.select(
            CollectionUtils.collect(
                model.getRegistryStructureList(),
                input -> {
                    EppRegistryStructure object = (EppRegistryStructure) input.getObject();
                    while (null != object) {
                        if (object.equals(selected)) { return (EppRegistryStructure) input.getObject(); }
                        object = object.getParent();
                    }
                    return null;
                }
            ),
            NotNullPredicate.getInstance()
        );
    }


    // БЕЗ КЭШа
    protected Map<Long, IEppEpvBlockWrapper> getBlockWrapperMap(final Collection<Long> blockIds) {
        final Lock lock = DaoCacheFacade.lock();
        final Map<Long, IEppEpvBlockWrapper> result = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(blockIds, true);
        lock.release();
        return result;
    }
}