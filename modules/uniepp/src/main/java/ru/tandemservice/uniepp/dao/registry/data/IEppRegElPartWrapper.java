package ru.tandemservice.uniepp.dao.registry.data;

import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;

/**
 * @author vdanilov
 */
public interface IEppRegElPartWrapper extends IEppBaseRegElWrapper<EppRegistryElementPart> {

    List<String> PART_LOAD_CODES = ImmutableList.of(EppLoadType.FULL_CODE_TOTAL_HOURS, EppLoadType.FULL_CODE_LABOR, EppLoadType.FULL_CODE_WEEKS);

    /** @return часть дисциплины реестра */
    @Override EppRegistryElementPart getItem();

    /** @return номер части */
    int getNumber();

    /** @return { номер модуля -> модуль в части дисциплины } */
    Map<Integer, IEppRegElPartModuleWrapper> getModuleMap();

    /**
     * Возвращает шкалу для итогового контрольного мероприятия
     * @param fullCode {@link ru.tandemservice.uniepp.entity.catalog.EppFControlActionType}.fullCode
     * @return {@link ru.tandemservice.uniepp.entity.catalog.EppGradeScale}.code
     **/
    String getGradeScale(String fullCode);

    /**
     * Возвращает шкалу для группы типов итогового контроля (если она единственная)
     * @param groupCode {@link ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup}
     * @return {@link ru.tandemservice.uniepp.entity.catalog.EppGradeScale}.code
     * @throws IllegalStateException, если значение не единственное
     */
    String getGradeScale4Group(String groupCode) throws IllegalStateException;

    /**
     * Определяет, есть ли мероприятия (итоговые) указанной группы в части элемента реестра
     * @param groupCode {@link ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup}
     * @return true, если есть мероприятия из данной группы, false - если мероприятий нет
     */
    boolean hasActions4Groups(String groupCode);

    /**
     * @return есть ли несколько мероприятий одной группы
     */
    boolean isMultipleActionsForGroupError();

    /**
     * Возвращает строку нагрузки части элемента реестра (например, "ауд. 126ч. (54/0/72), сам. 162ч.")
     * @return Строка нагрузки части элемента реестра (Вариант 5)
     */
    String getLoadAsString();

    /**
     * Возвращает строку контрольных мероприятий элемента реестра (сокращенные названия, порядок по приоритету) (например, "Э, КР")
     * @return Строка контрольных мероприятий (Вариант 6)
     */
    String getActionsAsString();

    /**
     * Определяет, есть ли мероприятия (итоговые) указанной Формы итогового контроля в части элемента реестра
     * @param actionType {@link ru.tandemservice.uniepp.entity.catalog.EppFControlActionType}
     * @return true, если есть мероприятия из данной Формы итогового контроля, false - если мероприятий нет
     */
    boolean hasActions4ControlActionType(EppFControlActionType actionType);

}
