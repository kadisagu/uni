/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author Igor Belanov
 * @since 28.02.2017
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduPlanVersion.id"),
})
public class EppDevelopResultTabUI extends UIPresenter
{
    private EppEduPlanVersion _eduPlanVersion = new EppEduPlanVersion();

    private String _selectedSubPage;

    @Override
    public void onComponentRefresh()
    {
        if (_eduPlanVersion != null && _eduPlanVersion.getId() != null)
        {
            setEduPlanVersion(DataAccessServices.dao().get(_eduPlanVersion.getId()));
        }
    }

    public String getRegionName()
    {
        return EppDevelopResultTab.TAB_PANEL_REGION_NAME;
    }

    // getters & setters
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        _eduPlanVersion = eduPlanVersion;
    }

    public String getSelectedSubPage()
    {
        return _selectedSubPage;
    }

    public void setSelectedSubPage(String selectedSubPage)
    {
        _selectedSubPage = selectedSubPage;
    }
}
