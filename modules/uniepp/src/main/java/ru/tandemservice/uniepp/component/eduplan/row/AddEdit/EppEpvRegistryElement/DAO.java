package ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvRegistryElement;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.BaseAddEditDao;
import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.LoadAddEditDao;
import ru.tandemservice.uniepp.component.registry.TutorOrgUnitAsOwnerSelectModel;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.ui.EppRegistryElementSelectModel;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends LoadAddEditDao<EppEpvRegistryRow, Model> implements IDAO
{
    @Override public void prepare(final Model model)
    {
        super.prepare(model);

        final List<HSelectOption> parentList = IEppEduPlanVersionDataDAO.instance.get().getEpvRowHierarchyListFromBlock(model.getBlock(), EppEpvRegistryRow.PARENT_PREDICATE);
        BaseAddEditDao.disablePossibleLoop(parentList, Collections.singleton(model.getRow().getId()));
        model.setParentList(parentList);

        model.setOwnerSelectModel(new TutorOrgUnitAsOwnerSelectModel() {
            @Override protected OrgUnit getCurrentValue() { return model.getRow().getRegistryElementOwner(); }
        });

        model.setTypeList(HierarchyUtil.listHierarchyNodesWithParents(IEppRegistryDAO.instance.get().getParentItems4Class(null), true));

        model.setRegistrySelectModel(new EppRegistryElementSelectModel<EppRegistryElement>(EppRegistryElement.class) {
            @Override protected DQLSelectBuilder query(final String alias, final String filter)
            {
                final EppRegistryStructure type = model.getRow().getType();
                if (null == type) { return null; }
                final Collection<Long> childTypeIds = getChilds(type);

                final EppRegistryElement registryElement = model.getRow().getRegistryElement();
                final IDQLExpression selectedElement = (null == registryElement ? null : eq(property(alias, "id"), value(registryElement.getId())));

                return super.query(alias, filter).where(or(
                        selectedElement,
                        and(
                                notIn(property(EppRegistryElement.state().code().fromAlias(alias)), EppState.UNACTUAL_STATES),
                                eq(property(EppRegistryElement.owner().fromAlias(alias)), value(model.getRow().getRegistryElementOwner())),
                                in(property(EppRegistryElement.parent().id().fromAlias(alias)), childTypeIds)
                        )
                ));
            }

            private Collection<Long> getChilds(EppRegistryStructure type) {
                final Set<Long> ids = new HashSet<Long>();
                ids.add(type.getId());
                collect(ids);
                return ids;
            }

            private void collect(Set<Long> ids) {
                while (true) {
                    boolean exit = true;
                    for (HSelectOption option: model.getTypeList()) {
                        EppRegistryStructure s = (EppRegistryStructure) option.getObject();
                        if (null != s.getParent() && ids.contains(s.getParent().getId())) {
                            if (ids.add(s.getId())) {
                                exit = false;
                            }
                        }
                    }
                    if (exit) { return; }
                }
            }

            @Override
            protected IDQLExpression getFilterCondition(final String alias, final String filter)
            {
                // проверяем еще и по всем названиям дисциплины (в других УП(в))
                final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, "r")
                .column(property(EppEpvRegistryRow.registryElement().id().fromAlias("r")))
                .where(this.like(EppEpvRegistryRow.title().fromAlias("r"), filter));

                return or(
                        in(property(alias, "id"), dql.buildQuery()),
                        super.getFilterCondition(alias, filter)
                );
            }
        });
    }

    @Override public void save(final Model model) {
        this.doSaveRow(model);
    }
}
