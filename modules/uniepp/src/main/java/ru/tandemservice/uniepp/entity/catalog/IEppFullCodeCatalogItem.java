package ru.tandemservice.uniepp.entity.catalog;

import org.tandemframework.common.catalog.entity.ICatalogItem;

/**
 * @author vdanilov
 */
public interface IEppFullCodeCatalogItem extends ICatalogItem {

    /** @return unique code of element */
    String getFullCode();

    /** @return short title of item */
    String getShortTitle();

}
