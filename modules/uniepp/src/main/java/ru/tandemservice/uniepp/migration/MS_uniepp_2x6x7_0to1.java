package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x6x7_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.7")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppEduPlan

        // создано обязательное свойство eduStartYear
        {
            if (!tool.columnExists("epp_eduplan_t", "edustartyear_p")) {
                tool.createColumn("epp_eduplan_t", new DBColumn("edustartyear_p", DBType.INTEGER));

                if (!tool.columnExists("epp_eduplan_ver_t", "year_id")) {
                    throw new IllegalStateException();
                }

                // пробуем восстановить из
                tool.executeUpdate("update epp_eduplan_t set edustartyear_p=(select min(y.intvalue_p) from epp_eduplan_ver_t v inner join educationyear_t y on (y.id=v.year_id) where v.eduplan_id=epp_eduplan_t.id) where edustartyear_p is null");

                // если нет УП(в), то превед - 2000
                tool.executeUpdate("update epp_eduplan_t set edustartyear_p=? where edustartyear_p is null", 2000);

                // сделать колонку NOT NULL
                tool.setColumnNullable("epp_eduplan_t", "edustartyear_p", false);
            }
        }

        // создано свойство eduEndYear
        {
            if (!tool.columnExists("epp_eduplan_t", "eduendyear_p")) {
                tool.createColumn("epp_eduplan_t", new DBColumn("eduendyear_p", DBType.INTEGER));
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppEduPlanVersion

        // удалено свойство year
        {
            if (tool.columnExists("epp_eduplan_ver_t", "year_id")) {
                tool.dropColumn("epp_eduplan_ver_t", "year_id");
            }
        }


    }
}