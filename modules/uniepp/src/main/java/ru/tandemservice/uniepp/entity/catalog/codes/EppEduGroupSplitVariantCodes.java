package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Способ деления потоков"
 * Имя сущности : eppEduGroupSplitVariant
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppEduGroupSplitVariantCodes
{
    /** Константа кода (code) элемента : По полу (title) */
    String PO_POLU = "bySex";
    /** Константа кода (code) элемента : По иностранному языку (title) */
    String PO_INOSTRANNOMU_YAZYKU = "byForeignLang";

    Set<String> CODES = ImmutableSet.of(PO_POLU, PO_INOSTRANNOMU_YAZYKU);
}
