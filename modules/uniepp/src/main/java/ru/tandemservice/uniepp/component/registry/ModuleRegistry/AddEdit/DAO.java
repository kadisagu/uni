package ru.tandemservice.uniepp.component.registry.ModuleRegistry.AddEdit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.NotNullPredicate;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.gen.OrgUnitGen;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.component.registry.TutorOrgUnitAsOwnerSelectModel;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryModuleDAO;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppModuleStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.gen.EppStateGen;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model) {
        {
            final IEntity entity = (null == model.getId() ? null : this.get(model.getId()));
            this.prepareElement(model, entity);
            this.prepareOwnerSelectModel(model, entity);
        }

        {
            model.setLoadList(CollectionUtils.collect(
                new DQLSelectBuilder()
                .fromEntity(EppALoadType.class, "lt").column(property("lt"))
                .joinEntity("lt", DQLJoinType.left, EppRegistryModuleALoad.class, "mlt", and(
                    eq(property(EppRegistryModuleALoad.loadType().fromAlias("mlt")), property("lt")),
                    eq(property(EppRegistryModuleALoad.module().fromAlias("mlt")), value(model.getElement()))
                )).column(property("mlt"))
                .order(property(EppALoadType.code().fromAlias("lt")))
                .createStatement(this.getSession()).<Object[]>list(),
                input -> {
                    final Object[] row = (Object[])input;
                    if (null != row[1]) { return row[1]; }
                    return new EppRegistryModuleALoad(model.getElement(), (EppALoadType)row[0]);
                }
            ));
        }

        {
            model.setControlActionList(
                CollectionUtils.select(
                    CollectionUtils.collect(
                        new DQLSelectBuilder()
                        .fromEntity(EppIControlActionType.class, "c").column(property("c"))
                        .joinEntity("c", DQLJoinType.left, EppRegistryModuleIControlAction.class, "mc", and(
                            eq(property(EppRegistryModuleIControlAction.controlAction().fromAlias("mc")), property("c")),
                            eq(property(EppRegistryModuleIControlAction.module().fromAlias("mc")), value(model.getElement()))
                        )).column(property("mc"))
                        .order(property(EppIControlActionType.code().fromAlias("c")))
                        .createStatement(this.getSession()).<Object[]>list(),
                        input -> {
                            final Object[] row = (Object[])input;
                            if (null != row[1]) {
                                return row[1]; // если объект уже есть - показываем его
                            }

                            // если объекта нет - создает его только если контроль доступен для выбора
                            final EppIControlActionType controlAction = (EppIControlActionType)row[0];
                            if (controlAction.isEnabled()) {
                                return new EppRegistryModuleIControlAction(model.getElement(), controlAction);
                            }

                            // если форма недоступна - не показываем
                            return null;
                        }
                    ),
                    NotNullPredicate.getInstance()
                )
            );
        }

        model.setParentList(HierarchyUtil.listHierarchyNodesWithParents(this.getList(EppModuleStructure.class, EppModuleStructure.P_TITLE), true));
    }

    private void prepareOwnerSelectModel(final Model model, final IEntity entity) {
        if (entity instanceof OrgUnit) {
            // если указано подразделение, то ничего не сделать (оно и есть)
            model.setOwnerSelectModel(new StaticSelectModel("id", OrgUnitGen.fullTitle().s(), Collections.singleton(entity)));

        } else {
            // если подразделение не указано - то показываем все из настройки
            model.setOwnerSelectModel(new TutorOrgUnitAsOwnerSelectModel() {
                @Override protected OrgUnit getCurrentValue() {
                    return (null == model.getElement() ? null : model.getElement().getOwner());
                }
            });

        }
    }

    @SuppressWarnings("unchecked")
    protected void prepareElement(final Model model, final IEntity entity) {
        if (entity instanceof EppRegistryModule) {
            model.setElement((EppRegistryModule)entity);
        }

        if (null == model.getElement()) {
            try {
                model.setElement(new EppRegistryModule());
                model.getElement().setNumber(INumberQueueDAO.instance.get().getNextNumber(model.getElement()));
            } catch (final Throwable t) {
                throw new RuntimeException(t);
            }
        }
        if (null == model.getElement().getOwner()) {
            if (entity instanceof OrgUnit) {
                model.getElement().setOwner((OrgUnit) entity);
            }
        }
        if (null == model.getElement().getState()) {
            model.getElement().setState(this.get(EppState.class, EppStateGen.P_CODE, EppState.STATE_FORMATIVE));
        }
    }

    @Override
    public void save(final Model model) {
        IEppRegistryModuleDAO.instance.get().doSaveRegistryModule(model.getElement());

        model.setLoadList(
            IEppRegistryModuleDAO.instance.get().doSaveRegistryModuleLoad(model.getElement(), model.getLoadList())
        );
        model.setControlActionList(
            IEppRegistryModuleDAO.instance.get().doSaveRegistryModuleControl(model.getElement(), model.getControlActionList())
        );
    }


}
