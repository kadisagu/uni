/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppContract.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Collection;
import java.util.Collections;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.ctr.base.bo.Contactor.ContactorManager;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.EppContract.EppContractManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * @author oleyba
 * @since 7/19/12
 * @deprecated договоры на УП(в) будут переведены на договоры на ОП
 * @deprecated use EduProgramContractObjectFactory
 */
@Deprecated
@SuppressWarnings("deprecation")
public abstract class EppStudentContractObjectFactory extends EppContractObjectFactory<Student>
{
    protected abstract EppStudent2EduPlanVersion getStduent2EduPlanVersion();
    protected final EppStudent2EduPlanVersion s2epv = this.getStduent2EduPlanVersion();

    @Override protected OrgUnit getCtrContractObjectOrgUnit() {
        final EducationOrgUnit educationOrgUnit = this.s2epv.getStudent().getEducationOrgUnit();
        final OrgUnit groupOrgUnit = educationOrgUnit.getGroupOrgUnit();
        if (null == groupOrgUnit) {
            throw new ApplicationException(EppContractManager.instance().getProperty(
                "error.epp-no-group-org-unit",
                educationOrgUnit.getTitle(),
                educationOrgUnit.getDevelopCombinationTitle(),
                educationOrgUnit.getFormativeOrgUnit().getFullTitle(),
                educationOrgUnit.getTerritorialOrgUnit().getTerritorialTitle()

            ));
        }
        return groupOrgUnit;
    }

    @Override protected ContactorPerson getRequestor() { return this.getStudentContactPersion(this.s2epv.getStudent()); }
    @Override protected Collection<Student> getStudentList() { return Collections.singleton(this.s2epv.getStudent()); }

    @Override protected String getStudentTitle(final Student student) { return student.getPerson().getFullFio(); }
    @Override protected ContactorPerson getStudentContactPersion(final Student student) { return ContactorManager.instance().dao().savePhysicalContactor(student.getPerson(), "", "", false); }

    @Override protected StudentCategory getStudentCategory(final Student student) { return student.getStudentCategory(); }
    @Override protected EducationOrgUnit getStudentEducationOrgUnit(final Student student) { return student.getEducationOrgUnit(); }
    @Override protected EppEduPlanVersionBlock getStudentEduPlanVersionBlock(final Student student) {
        return s2epv.getBlock();
    }
}
