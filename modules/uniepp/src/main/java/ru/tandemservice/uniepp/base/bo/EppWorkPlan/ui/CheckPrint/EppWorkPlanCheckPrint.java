/* $Id: $ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.CheckPrint;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.base.bo.EppOrgUnit.EppOrgUnitManager;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.EppWorkPlanManager;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 16.03.2017
 */
@Configuration
public class EppWorkPlanCheckPrint extends BusinessComponentManager
{
    public final static String ORG_UNIT_ID = "orgUnitId";

    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String REG_TYPE_DS = "regTypeDS";

    public static final String EDUCATION_YEAR_PARAM = "educationYearList";
    public static final String WORK_PLAN_STATUS_PARAM = "workPlanStatusList";
    public static final String ORG_UNIT_PARAM = "orgUnitList";
    public static final String REG_STATUS_PARAM = "regStatusList";
    public static final String REG_TYPE_PARAM = "regTypeList";


    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUCATION_YEAR_DS, educationYearDSHandler()))
                .addDataSource(EppWorkPlanManager.instance().stateListDSConfig())
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()))
                .addDataSource(selectDS(REG_TYPE_DS, regTypeDSHandler()).treeable(true))
                .create();
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return EppOrgUnitManager.instance().eppOrgUnitDSHandlerForAddEditForm(getName())
                .customize((alias, dql, context, filter) ->
                           {
                               Long orgUnitId = context.<Long>get(ORG_UNIT_ID);
                               if (orgUnitId != null)
                                   dql.where(or(eq(property(alias, OrgUnit.id()), value(orgUnitId)), eq(property(alias, OrgUnit.parent().id()), value(orgUnitId))));

                               return dql;
                           });
    }

    @Bean
    public IDefaultComboDataSourceHandler regTypeDSHandler()
    {
        return EppRegistryStructure.defaultSelectDSHandler(getName());
    }
}
