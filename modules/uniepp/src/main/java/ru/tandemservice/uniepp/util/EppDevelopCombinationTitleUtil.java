/* $Id:$ */
package ru.tandemservice.uniepp.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniepp.entity.plan.IEppEduPlanDevelopCombination;

/**
 * @author oleyba
 * @since 8/19/14
 */
@Zlo("удалить это, и сделать единую утиль с DevelopUtil, когда будет возможность")
public class EppDevelopCombinationTitleUtil
{
    private static void append(StringBuilder builder, String title) {
        if (null != title) {
            if (builder.length() > 0) {
                builder.append(", ").append(title);
            } else {
                builder.append(StringUtils.capitalize(title));
            }
        }
    }

    private static StringBuilder getTitle(EduProgramForm programForm, DevelopCondition developCondition, EduProgramTrait trait)
    {
        final StringBuilder builder = new StringBuilder();
        append(builder, (null == programForm ? null : StringUtils.trimToNull(programForm.getShortTitle())));
        append(builder, (null == developCondition ? null : StringUtils.trimToNull(developCondition.getShortTitle())));
        append(builder, (null == trait ? null : StringUtils.trimToNull(trait.getShortTitle())));
        return builder;
    }

    public static String getTitle(EduProgramForm programForm, DevelopCondition developCondition, EduProgramTrait trait, DevelopGrid developGrid)
    {
        StringBuilder builder = getTitle(programForm, developCondition, trait);
        append(builder, (null == developGrid ? null : StringUtils.trimToNull(developGrid.getTitle())));
        return builder.toString();
    }

    public static String getTitle(IEppEduPlanDevelopCombination developCombination)
    {
        return getTitle(developCombination.getProgramForm(), developCombination.getDevelopCondition(), developCombination.getProgramTrait(), developCombination.getDevelopGrid());
    }

    public static String getTitle(EduProgramForm programForm, DevelopCondition developCondition, EduProgramTrait trait, DevelopPeriod developPeriod, EduProgramOrientation orientation)
    {
        StringBuilder builder = new StringBuilder();

        append(builder, (null == programForm ? null : StringUtils.trimToNull(programForm.getShortTitle())));

        if (developCondition != null && !UniDefines.DEVELOP_CONDITION_FULL_TIME.equals(developCondition.getCode()))
            append(builder, StringUtils.trimToNull(developCondition.getShortTitle()));

        if (trait != null && !UniDefines.DEVELOP_TECH_SIMPLE.equals(trait.getCode()))
            append(builder, StringUtils.trimToNull(trait.getShortTitle()));

        append(builder, null == developPeriod ? null : StringUtils.trimToNull(developPeriod.getTitle()));

        append(builder, null == orientation ? null : orientation.getShortTitle());

        return builder.toString();
    }
}
