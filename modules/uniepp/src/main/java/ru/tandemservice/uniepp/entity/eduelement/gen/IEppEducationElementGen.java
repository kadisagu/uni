package ru.tandemservice.uniepp.entity.eduelement.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement;

/**
 * Учебная единица
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IEppEducationElementGen extends InterfaceStubBase
 implements IEppEducationElement{
    public static final int VERSION_HASH = -1074542798;

    public static final String P_EDUCATION_ELEMENT_SIMPLE_TITLE = "educationElementSimpleTitle";
    public static final String P_EDUCATION_ELEMENT_TITLE = "educationElementTitle";


    private static final Path<IEppEducationElement> _dslPath = new Path<IEppEducationElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement");
    }
            

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement#getEducationElementSimpleTitle()
     */
    public static SupportedPropertyPath<String> educationElementSimpleTitle()
    {
        return _dslPath.educationElementSimpleTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement#getEducationElementTitle()
     */
    public static SupportedPropertyPath<String> educationElementTitle()
    {
        return _dslPath.educationElementTitle();
    }

    public static class Path<E extends IEppEducationElement> extends EntityPath<E>
    {
        private SupportedPropertyPath<String> _educationElementSimpleTitle;
        private SupportedPropertyPath<String> _educationElementTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement#getEducationElementSimpleTitle()
     */
        public SupportedPropertyPath<String> educationElementSimpleTitle()
        {
            if(_educationElementSimpleTitle == null )
                _educationElementSimpleTitle = new SupportedPropertyPath<String>(IEppEducationElementGen.P_EDUCATION_ELEMENT_SIMPLE_TITLE, this);
            return _educationElementSimpleTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement#getEducationElementTitle()
     */
        public SupportedPropertyPath<String> educationElementTitle()
        {
            if(_educationElementTitle == null )
                _educationElementTitle = new SupportedPropertyPath<String>(IEppEducationElementGen.P_EDUCATION_ELEMENT_TITLE, this);
            return _educationElementTitle;
        }

        public Class getEntityClass()
        {
            return IEppEducationElement.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
