/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.EppEPVBlockProfActivityTypeList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.logic.EppEPVBlockProfActivityTypeSearchDSHandler;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.EppEPVBlockProfActivityTypeAdd.EppDevelopResultEppEPVBlockProfActivityTypeAdd;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author Igor Belanov
 * @since 28.02.2017
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduPlanVersion.id", required = true)
})
public class EppDevelopResultEppEPVBlockProfActivityTypeListUI extends UIPresenter
{
    // версия УП в рамках которой работаем
    private EppEduPlanVersion _eduPlanVersion = new EppEduPlanVersion();

    @Override
    public void onComponentRefresh()
    {
        if (_eduPlanVersion != null && _eduPlanVersion.getId() != null)
        {
            setEduPlanVersion(DataAccessServices.dao().get(_eduPlanVersion.getId()));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppDevelopResultEppEPVBlockProfActivityTypeList.EPP_EPV_BLOCK_PROF_ACTIVITY_TYPE_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppEPVBlockProfActivityTypeSearchDSHandler.PROP_EDU_PLAN_VERSION_ID, _eduPlanVersion.getId());
        }
    }

    public boolean isDisableAddEdit()
    {
        return !_eduPlanVersion.getState().isFormative();
    }

    // listeners
    public void onClickAdd()
    {
        _uiActivation.asRegionDialog(EppDevelopResultEppEPVBlockProfActivityTypeAdd.class)
                .parameter(PUBLISHER_ID, _eduPlanVersion.getId())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    // getters & setters
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        _eduPlanVersion = eduPlanVersion;
    }
}
