package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLDeleteQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_2x11x3_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		ISQLTranslator translator = tool.getDialect().getSQLTranslator();

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppRegistryModuleIncomingSkill

		// сущность была удалена
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsByEntityCode(tool.entityCodes().get("eppRegistryModuleIncomingSkill"), "epp_reg_module_skill_t");

			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("eppRegistryModuleIncomingSkill");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppRegistryModuleOutgoingSkill

		// сущность была удалена
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsByEntityCode(tool.entityCodes().get("eppRegistryModuleOutgoingSkill"), "epp_reg_module_skill_t");

			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("eppRegistryModuleOutgoingSkill");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppRegistryModuleSkill

		// сущность была удалена
		{
			// =============== Проверим таблицу перед очисткой ===============
			long count = tool.getNumericResult("select count(*) from epp_reg_module_skill_t");

			// ! ЭТУ СТРОЧКУ НУЖНО ЗАКОММЕНТИТЬ ЕСЛИ ТАБЛИЦА НЕ ПУСТАЯ И ЕЁ МОЖНО ОЧИСТИТЬ !
			if (count > 0) throw new IllegalStateException("Таблица \"Учебный модуль: компетенция\" не пуста.");

			// ================= Очистим таблицу компетенций =================
			if (count > 0)
			{
				tool.warn("Таблица \"Учебный модуль: компетенция\" не пуста.");
				SQLDeleteQuery deleteSkills = new SQLDeleteQuery("epp_reg_module_skill_t");
				tool.executeUpdate(translator.toSql(deleteSkills));
			}

			// удалить таблицу
			tool.dropTable("epp_reg_module_skill_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistryModuleSkill");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppRegistrySkill

		// сущность была удалена
		{
			// =============== Проверим таблицу перед очисткой ===============
			long count = tool.getNumericResult("select count(*) from epp_reg_skill_t");

			// ! ЭТУ СТРОЧКУ НУЖНО ЗАКОММЕНТИТЬ ЕСЛИ ТАБЛИЦА НЕ ПУСТАЯ И ЕЁ МОЖНО ОЧИСТИТЬ !
			if (count > 0) throw new IllegalStateException("Таблица компетенций не пуста.");

			// ================= Очистим таблицу компетенций =================
			if (count > 0)
			{
				tool.warn("Таблица компетенций не пуста.");
				SQLDeleteQuery deleteSkills = new SQLDeleteQuery("epp_reg_skill_t");
				tool.executeUpdate(translator.toSql(deleteSkills));
			}

			// удалить таблицу
			tool.dropTable("epp_reg_skill_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("eppRegistrySkill");
		}
    }
}
