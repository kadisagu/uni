/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.logic.studentList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.StudentList.EppWorkPlanStudentListUI;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 01.02.13
 */
public class EppWorkPlanStudentSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public EppWorkPlanStudentSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        final Long id = context.get(EppWorkPlanStudentListUI.PARAM_ID);

        // только неархивные студенты
        builder.where(eq(property(alias, Student.P_ARCHIVAL), value(Boolean.FALSE)));

        // id самого плана
        final Set<Long> ids = new HashSet<>(Collections.singleton(id));

        // добавляем id всех версий
        ids.addAll(
            new DQLSelectBuilder().fromEntity(EppWorkPlanVersion.class, "wpv").column(property("wpv.id"))
            .where(in(property(EppWorkPlanVersion.parent().id().fromAlias("wpv")), ids))
            .createStatement(context.getSession()).<Long>list()
        );

        builder.where(exists(
            new DQLSelectBuilder().fromEntity(EppStudent2WorkPlan.class, "s2wp").column(property("s2wp.id"))
            .where(isNull(property(EppStudent2WorkPlan.removalDate().fromAlias("s2wp"))))
            .where(eq(property(EppStudent2WorkPlan.studentEduPlanVersion().student().fromAlias("s2wp")), property(alias)))
            .where(in(property(EppStudent2WorkPlan.workPlan().id().fromAlias("s2wp")), ids))
            .buildQuery()
        ));
    }
}
