package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.ParametersEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.runtime.BusinessComponentRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.EduProgramSubjectIndexManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.EppEduPlanManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;

import java.util.Map;

/**
 * @author avedernikov
 * @since 12.04.2016
 */
@State({
		@Bind(key = EppEduPlanParametersEditUI.PARAM_EDU_PLAN_ID, binding = "eduPlanId", required = true)
		})
public class EppEduPlanParametersEditUI extends UIPresenter
{
	public static final String PARAM_EDU_PLAN_ID = "eduPlanId";

	public static final String PARAM_PROGRAM_KIND = "programKind";
	public static final String PARAM_SUBJECT_INDEX = "subjectIndex";
	public static final String PARAM_PROGRAM_SUBJECT = "programSubject";

	private Long eduPlanId;
	private EppEduPlanProf eduPlan;
	private EduProgramSubjectIndex subjectIndex;
	private EduProgramSubject programSubject;
	private EduProgramQualification programQualification;
	private EduProgramOrientation programOrientation;

	@Override
	public void onComponentRefresh()
	{
		if (eduPlan == null)
			eduPlan = DataAccessServices.dao().getNotNull(eduPlanId);
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			// Реальные датасурсы для новых значений
			case EppEduPlanParametersEdit.SUBJECT_INDEX_DS:
			{
				dataSource.put(PARAM_PROGRAM_KIND, getEduPlan().getProgramSubject().getEduProgramKind());
				break;
			}
			case EppEduPlanParametersEdit.PROGRAM_SUBJECT_DS:
			{
				dataSource.put(PARAM_SUBJECT_INDEX, subjectIndex);
				break;
			}
			case EppEduPlanParametersEdit.PROGRAM_QUALIFICATION_DS:
			{
				dataSource.put(PARAM_PROGRAM_SUBJECT, programSubject);
				break;
			}
			case EppEduPlanParametersEdit.PROGRAM_ORIENTATION_DS:
			{
				dataSource.put(EduProgramSubjectIndexManager.PARAM_SUBJECT_INDEX, subjectIndex);
				break;
			}
		}
	}

	public void onClickApply()
	{
		EppEduPlanManager.instance().dao().updateEduPlan(getEduPlan(), getProgramSubject(), getProgramQualification(), getProgramOrientation());
        deactivate();
	}

	public Map<String, Object> getEduPlanPublisherParameters()
	{
		return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, getEduPlan().getId());
	}

	public String getEduPlanPublisher()
	{
		return BusinessComponentRuntime.getInstance().getPublisher(getEduPlan().getClass());
	}

	public boolean isOrientationVisible()
	{
		return isEpHigherProf() && (subjectIndex == null || DataAccessServices.dao().getCount(EduProgramOrientation.class, EduProgramOrientation.subjectIndex().s(), subjectIndex) != 0);
	}

    public boolean isEpHigherProf()
    {
        return eduPlan instanceof EppEduPlanHigherProf;
    }

	public Long getEduPlanId()
	{
		return eduPlanId;
	}

	public void setEduPlanId(Long eduPlanId)
	{
		this.eduPlanId = eduPlanId;
	}

	public EppEduPlanProf getEduPlan()
	{
		return eduPlan;
	}

	public void setEduPlan(EppEduPlanProf eduPlan)
	{
		this.eduPlan = eduPlan;
	}

	public EduProgramSubjectIndex getSubjectIndex()
	{
		return subjectIndex;
	}

	public void setSubjectIndex(EduProgramSubjectIndex subjectIndex)
	{
		this.subjectIndex = subjectIndex;
	}

	public EduProgramSubject getProgramSubject()
	{
		return programSubject;
	}

	public void setProgramSubject(EduProgramSubject programSubject)
	{
		this.programSubject = programSubject;
	}

	public EduProgramQualification getProgramQualification()
	{
		return programQualification;
	}

	public void setProgramQualification(EduProgramQualification programQualification)
	{
		this.programQualification = programQualification;
	}

	public EduProgramOrientation getProgramOrientation()
	{
		return programOrientation;
	}

	public void setProgramOrientation(EduProgramOrientation programOrientation)
	{
		this.programOrientation = programOrientation;
	}
}
