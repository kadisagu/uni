/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.plan.*;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 01.03.2017
 */
public class EppDevelopResultDAO extends UniBaseDao implements IEppDevelopResultDAO
{
    @Override
    public void saveEppEPVBlockProfActivityType(EppEduPlanVersionBlock EPVBlock, List<EppProfActivityType> profActivityTypeList)
    {
        if (profActivityTypeList != null && !profActivityTypeList.isEmpty())
        {
            // ==================== VALIDATION ====================
            EppEduPlan eduPlan = EPVBlock.getEduPlanVersion().getEduPlan();
            // добавлять эти сущности можно только для УП проф. образования
            if (!(eduPlan instanceof EppEduPlanProf))
            {
                throw new ApplicationException("Попытка добавить «Вид профессиональной деятельности в блоке версии УП»" +
                        " для УП, который не является учебным планом проф. образования");
            }
            EppEduPlanProf eduPlanProf = ((EppEduPlanProf) eduPlan);
            for (EppProfActivityType profActivityType : profActivityTypeList)
            {
                if (!eduPlanProf.getProgramSubject().equals(profActivityType.getProgramSubject()))
                {
                    throw new ApplicationException("Попытка добавить «Вид профессиональной деятельности в блоке версии УП»" +
                            " для УП проф. образования с несоответствующим параметром «Направление подготовки»");
                }
            }
            // если у нас СПО, то нужно проверить флаг "углублённое изучение"
            if (eduPlan instanceof EppEduPlanSecondaryProf)
            {
                EppEduPlanSecondaryProf SPOEduPlan = ((EppEduPlanSecondaryProf) eduPlan);
                for (EppProfActivityType profActivityType : profActivityTypeList)
                {
                    if (profActivityType.getInDepthStudy() != SPOEduPlan.isInDepthStudy())
                    {
                        throw new ApplicationException("Попытка добавить «Вид профессиональной деятельности в блоке версии УП»" +
                                " для СПО с несоответствующим параметром «Углубленное изучение»");
                    }
                }
            }


            // ======================= SAVE =======================
            for (EppProfActivityType profActivityType : profActivityTypeList)
            {
                EppEduPlanVersionBlockProfActivityType eppEduPlanVersionBlockProfActivityType = new EppEduPlanVersionBlockProfActivityType();
                eppEduPlanVersionBlockProfActivityType.setEduPlanVersionBlock(EPVBlock);
                eppEduPlanVersionBlockProfActivityType.setProfActivityType(profActivityType);
                save(eppEduPlanVersionBlockProfActivityType);
            }
            getSession().flush();


            // ===================== NORMALIZE ====================
            // (нормализация таблицы сущности "Вид профессиональной деятельности в блоке версии УП")
            // нужно удалить все записи с необщим блоком, для которых есть общий блок
            // (для той же проф. деятельности в пределах той же версии УП)
            DQLSelectBuilder select = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanVersionBlockProfActivityType.class, "epvb_pat").column("epvb_pat.id")
                    .joinPath(DQLJoinType.inner, EppEduPlanVersionBlockProfActivityType.eduPlanVersionBlock().fromAlias("epvb_pat"), "epvb")
                    .joinPath(DQLJoinType.inner, EppEduPlanVersionBlock.eduPlanVersion().fromAlias("epvb"), "epv")
                    // нормализуем только в рамках текущей версии УП (это не обязательно, но всё же)
                    .where(eq(property("epv", EppEduPlanVersion.id()), value(EPVBlock.getEduPlanVersion().getId())))
                    // выбираем записи с необщим блоком
                    .where(notIn(property("epvb.id"), new DQLSelectBuilder().fromEntity(EppEduPlanVersionRootBlock.class, "epvb_r").column("epvb_r.id").buildQuery()))
                    // где существует запись...
                    .where(exists(new DQLSelectBuilder()
                            .fromEntity(EppEduPlanVersionBlockProfActivityType.class, "epvb_pat1").column(property("epvb_pat1.id"))
                            .joinPath(DQLJoinType.inner, EppEduPlanVersionBlockProfActivityType.eduPlanVersionBlock().fromAlias("epvb_pat1"), "epvb1")
                            .joinPath(DQLJoinType.inner, EppEduPlanVersionBlock.eduPlanVersion().fromAlias("epvb1"), "epv1")
                            // ...из общего блока
                            .where(in(property("epvb1.id"), new DQLSelectBuilder().fromEntity(EppEduPlanVersionRootBlock.class, "epvb_r1").column("epvb_r1.id").buildQuery()))
                            // ...в той же версии УП и того же вида проф.деятельности
                            .where(eq(property("epv.id"), property("epv1.id")))
                            .where(eq(
                                    property("epvb_pat", EppEduPlanVersionBlockProfActivityType.profActivityType().id()),
                                    property("epvb_pat1", EppEduPlanVersionBlockProfActivityType.profActivityType().id())))
                            .buildQuery()));
            // поудаляем выбранные
            DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(EppEduPlanVersionBlockProfActivityType.class)
                    .where(in(property(EppEduPlanVersionBlockProfActivityType.P_ID), select.buildQuery()));
            deleteBuilder.createStatement(getSession()).execute();
        }
    }

    @Override
    public List<EppProfActivityType> getProfActivityTypes(Long EPVBlockId)
    {
        // подзапрос для вытаскивания общего блока версии УП
        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersionBlock.class, "epvb").column("epvb_r.id")
                .joinPath(DQLJoinType.inner, EppEduPlanVersionBlock.eduPlanVersion().fromAlias("epvb"), "epv")
                .joinEntity("epv", DQLJoinType.inner, EppEduPlanVersionBlock.class, "epvb1",
                        eq(property("epvb1", EppEduPlanVersionBlock.eduPlanVersion().id()), property("epv.id")))
                .joinEntity("epvb1", DQLJoinType.inner, EppEduPlanVersionRootBlock.class, "epvb_r",
                        eq(property("epvb1.id"), property("epvb_r.id")))
                .where(eq(property("epvb.id"), value(EPVBlockId)));

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersionBlockProfActivityType.class, "epvb_pat").column("pat")
                .joinPath(DQLJoinType.inner, EppEduPlanVersionBlockProfActivityType.profActivityType().fromAlias("epvb_pat"), "pat")
                .where(or(
                        // берём либо соответствующие конкретному блоку либо соответствующие общему блоку
                        eq(property("epvb_pat", EppEduPlanVersionBlockProfActivityType.eduPlanVersionBlock().id()), value(EPVBlockId)),
                        in(property("epvb_pat", EppEduPlanVersionBlockProfActivityType.eduPlanVersionBlock().id()), subBuilder.buildQuery())))
                .order(property("pat", EppProfActivityType.priority()));

        return builder.createStatement(getSession()).list();
    }
}
