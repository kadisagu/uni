package ru.tandemservice.uniepp.component.student.StudentTrajectoryTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.uni.entity.employee.Student;


@Input({
    @Bind(key = "studentId", binding = "studentHolder.id")
})
public class Model
{
    private final EntityHolder<Student> studentHolder = new EntityHolder<Student>();
    public EntityHolder<Student> getStudentHolder() { return this.studentHolder; }
    public Student getStudent() { return this.studentHolder.getValue(); }

}
