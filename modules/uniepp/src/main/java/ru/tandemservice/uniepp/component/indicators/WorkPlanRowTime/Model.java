package ru.tandemservice.uniepp.component.indicators.WorkPlanRowTime;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

/**
 * @author vdanilov
 */

public class Model
{

    private final StaticListDataSource<IEntity> dataSource = new StaticListDataSource<IEntity>();
    public StaticListDataSource<IEntity> getDataSource() { return this.dataSource; }

    private final SelectModel<EducationYear> yearModel = new SelectModel<EducationYear>(new EducationYearModel());
    public SelectModel<EducationYear> getYearModel() { return this.yearModel; }

}
