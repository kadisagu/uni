package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Мероприятие (из реестра)
 *
 * Итоговые мероприятия (отличаются тем, что действуют в рамках одного семестра и для них можно задавать общее число недель)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegistryActionGen extends EppRegistryElement
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppRegistryAction";
    public static final String ENTITY_NAME = "eppRegistryAction";
    public static final int VERSION_HASH = 750048736;
    private static IEntityMeta ENTITY_META;

    public static final String P_WEEKS = "weeks";

    private long _weeks;     // Продолжительность (в неделях)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Продолжительность (в неделях). Свойство не может быть null.
     */
    @NotNull
    public long getWeeks()
    {
        return _weeks;
    }

    /**
     * @param weeks Продолжительность (в неделях). Свойство не может быть null.
     */
    public void setWeeks(long weeks)
    {
        dirty(_weeks, weeks);
        _weeks = weeks;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppRegistryActionGen)
        {
            setWeeks(((EppRegistryAction)another).getWeeks());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegistryActionGen> extends EppRegistryElement.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegistryAction.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppRegistryAction is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "weeks":
                    return obj.getWeeks();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "weeks":
                    obj.setWeeks((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "weeks":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "weeks":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "weeks":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRegistryAction> _dslPath = new Path<EppRegistryAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegistryAction");
    }
            

    /**
     * @return Продолжительность (в неделях). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryAction#getWeeks()
     */
    public static PropertyPath<Long> weeks()
    {
        return _dslPath.weeks();
    }

    public static class Path<E extends EppRegistryAction> extends EppRegistryElement.Path<E>
    {
        private PropertyPath<Long> _weeks;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Продолжительность (в неделях). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryAction#getWeeks()
     */
        public PropertyPath<Long> weeks()
        {
            if(_weeks == null )
                _weeks = new PropertyPath<Long>(EppRegistryActionGen.P_WEEKS, this);
            return _weeks;
        }

        public Class getEntityClass()
        {
            return EppRegistryAction.class;
        }

        public String getEntityName()
        {
            return "eppRegistryAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
