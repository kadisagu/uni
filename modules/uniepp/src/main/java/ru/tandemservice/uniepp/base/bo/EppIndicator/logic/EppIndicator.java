/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.logic;

import org.tandemframework.core.tool.tree.IHierarchyItem;
import ru.tandemservice.uniepp.base.bo.EppIndicator.EppIndicatorManager;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author oleyba
 * @since 9/15/14
 */
public class EppIndicator
{
    private String name;
    private String componentName;
    private String groupName;

    public EppIndicator(String name, String componentName, String groupName)
    {
        this.componentName = componentName;
        this.name = name;
        this.groupName = groupName;
    }

    public String getComponentName()
    {
        return componentName;
    }

    public String getName()
    {
        return name;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }
}
