package ru.tandemservice.uniepp.settings.bo.PlanStructureQualification.ui.List;

import org.apache.commons.collections15.keyvalue.MultiKey;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex;
import ru.tandemservice.uniepp.settings.bo.PlanStructureQualification.PlanStructureQualificationManager;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 07.09.2015
 */

public class PlanStructureQualificationListUI extends UIPresenter
{
	private DynamicListDataSource<RowWrapper> dataSource;
	private List<EduProgramSubjectIndex> indexList;

	@Override
	public void onComponentRefresh()
	{
		setupIndexList();
		prepareDataSource();
	}

	private void prepareDataSource()
	{
		dataSource = new DynamicListDataSource<>(getConfig().getBusinessComponent(), iBusinessComponent -> {
			final DynamicListDataSource<RowWrapper> ds = getDataSource();
			final List<RowWrapper> list = getRows();
			ds.setCountRow(list.size());
			ds.setTotalSize(list.size());
			ds.createPage(list);
		});
		dataSource.addColumn(new SimpleColumn("Структура ГОСов / УП", EppPlanStructure.title()).setTreeColumn(true).setOrderable(false).setClickable(false));
		indexList.forEach(program -> dataSource.addColumn(createColumn(program)));
	}

	public void onClickChangeUsed()
	{
		final Object[] listenerParameters = getListenerParameter();
		final Long planStructureId = (Long)listenerParameters[0];
		final Long indexId = (Long)listenerParameters[1];
		PlanStructureQualificationManager.instance().modifyDAO().doSwitchPlanStructureQualification(planStructureId, indexId);
	}

	private void setupIndexList()
	{
		DQLSelectBuilder dql = new DQLSelectBuilder()
			.fromEntity(EduProgramSubjectIndex.class, "program")
			.order(property("program", EduProgramSubjectIndex.priority()));
		indexList = IUniBaseDao.instance.get().<EduProgramSubjectIndex>getList(dql);
	}

	private ToggleColumn createColumn(final EduProgramSubjectIndex element)
	{
		final ToggleColumn column = new ToggleColumn(element.getShortTitle(), element.getCode(), "Элемент используется", "Элемент не используется")
		{
			@Override
			public Item getContent(final IEntity entity)
			{
				// проверяем отображение переключателя
				// будет только у элементов самого нижнего уровня
				if (Boolean.TRUE.equals(entity.getProperty("isParent")))
				{
					return null;
				}
				return super.getContent(entity);
			}
		};
		column.setParametersResolver((entity, valueEntity) -> new Object[]{valueEntity.getId(), element.getId()});
		column.setListener("onClickChangeUsed");
		return column;
	}

	private List<RowWrapper> getRows()
	{
		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppPlanStructure.class, "planStruct").order(property("planStruct", EppPlanStructure.priority()));
		applyBelongsToStandardFilter(dql, "planStruct", getStandard());
		List<EppPlanStructure> planStructures = IUniBaseDao.instance.get().getList(dql);
		Set<Long> parentIds = planStructures.stream()
			.filter(ps -> ps.getHierarhyParent() != null)
			.map(ps -> ps.getHierarhyParent().getId())
			.collect(Collectors.toSet());
		List<RowWrapper> rows = planStructures.stream().map(RowWrapper::new).collect(Collectors.toList());
		RowWrapper.assignWrapperParents(rows);
		Set<MultiKey<Long>> existingRelations = getExistedPlanStructure2SubjectIndexRelations();
		for (RowWrapper wrapper : rows)
		{
			wrapper.setViewProperty("isParent", parentIds.contains(wrapper.getId()));
			for (EduProgramSubjectIndex index : indexList)
				wrapper.setViewProperty(index.getCode(), existingRelations.contains(new MultiKey<>(index.getId(), wrapper.getId())));
		}
		return rows;
	}
	private void applyBelongsToStandardFilter(DQLSelectBuilder dql, String alias, EppPlanStructure standard)
	{
		IDQLExpression isStandardChild = eq(property(alias, EppPlanStructure.parent()), value(standard));
		IDQLExpression isStandardGrandchild = eq(property(alias, EppPlanStructure.parent().parent()), value(standard));
		dql.where(or(isStandardChild, isStandardGrandchild));
	}

	private Set<MultiKey<Long>> getExistedPlanStructure2SubjectIndexRelations()
	{
		DQLSelectBuilder relDql = new DQLSelectBuilder().fromEntity(EppPlanStructure4SubjectIndex.class, "rel");
		return IUniBaseDao.instance.get().<EppPlanStructure4SubjectIndex>getList(relDql).stream()
			.map(rel -> new MultiKey<>(rel.getEduProgramSubjectIndex().getId(), rel.getEppPlanStructure().getId()))
			.collect(Collectors.toSet());
	}

	public DynamicListDataSource<RowWrapper> getDataSource()
	{
		return dataSource;
	}

	public EppPlanStructure getStandard()
	{
		return getSettings().get("standard");
	}

	public boolean isShowTable()
	{
		return getStandard() != null;
	}
}
