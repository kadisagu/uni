package ru.tandemservice.uniepp.entity.catalog;

import ru.tandemservice.uniepp.entity.catalog.gen.EppViewTableDisciplinesGen;

/**
 * Вид таблицы дисциплин
 */
public class EppViewTableDisciplines extends EppViewTableDisciplinesGen
{
    public static final String CODE_SIMPLE = "1";
    public static final String CODE_EXTEND = "2";

    public boolean isSimple() { return EppViewTableDisciplines.CODE_SIMPLE.equals(this.getCode()); }
    public boolean isExtend() { return EppViewTableDisciplines.CODE_EXTEND.equals(this.getCode()); }

}