/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppState.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.base.bo.EppState.util.IEppStateConfig;
import ru.tandemservice.uniepp.entity.IEppStateObject;

import java.util.Collection;

/**
 * @author Nikolay Fedorovskih
 * @since 13.10.2014
 */
public interface IEppStateDao extends IUniBaseDao
{
    /**
     * @param clazz класс объекта, который имеет интерфейс IEppStateObject
     * @return Возвращает интерфейс по состояниям объекта
     */
    IEppStateConfig getStateConfig(Class<? extends IEppStateObject> clazz);

    /**
     * Проверка возможности смены состояния объекта учебного процесса (рабочие/учебные планы и др.)
     * Если метод вернет строку, то смена состояния будет прервана с пользовательским сообщение об ошибке в виде этой самой строки.
     *
     * @param oldStateCode   код старого состояния
     * @param newStateCode   код нового состояния
     * @param eppStateObject сам объект
     * @return сообщение об ошибке, если таковое нужно, либо null, если никакой ошибки нет.
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    String canChangeStateEppObject(String oldStateCode, String newStateCode, IEppStateObject eppStateObject);

    /**
     * Массовая смена состояний объектов на указанное.
     * Если для объекта смена состояния невозможна, то объект пропускается.
     *
     * @param newStateCode      код нового состояния
     * @param eppStateObjectIds идентификаторы объектов, у которых надо изменить состояние на указанное
     * @return количество объектов, у которых сменилось состояние
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    int massChangeState(String newStateCode, Collection<Long> eppStateObjectIds);
}