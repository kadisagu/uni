package ru.tandemservice.uniepp.entity.workplan;

import java.util.Comparator;

import org.tandemframework.core.entity.IEntity;

/**
 * @author vdanilov
 */
public interface IEppWorkPlanPart extends IEntity {

    // проверяет, что объекты одинаковые
    public static final Comparator<IEppWorkPlanPart> EQUALS_COMPARATOR = new Comparator<IEppWorkPlanPart>() {
        @Override public int compare(final IEppWorkPlanPart o1, final IEppWorkPlanPart o2) {
            if (o1 == o2) { return 0; }
            if (o1 == null) { return 1; }
            if (o2 == null) { return -1; }

            {
                final int i = (o1.getNumber() - o2.getNumber());
                if (0 != i) { return i; }
            }

            {
                final int i = (o1.getTotalWeeks() - o2.getTotalWeeks());
                if (0 != i) { return i; }
            }

            {
                final int i = (o1.getAuditWeeks() - o2.getAuditWeeks());
                if (0 != i) { return i; }
            }

            {
                final int i = (o1.getSelfworkWeeks() - o2.getSelfworkWeeks());
                if (0 != i) { return i; }
            }

            return 0;
        }
    };


    /** @return номер части */
    int getNumber();

    /** @return общее число недель */
    int getTotalWeeks();

    /** @return число аудиторных недель */
    int getAuditWeeks();

    /** @return число самостоятельных недель */
    int getSelfworkWeeks();

    /** @return true, если нет недель */
    boolean isEmpty();

}
