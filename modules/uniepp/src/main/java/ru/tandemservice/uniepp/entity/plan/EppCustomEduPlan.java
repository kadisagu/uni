package ru.tandemservice.uniepp.entity.plan;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateMutable;
import ru.tandemservice.uniepp.dao.eduplan.IEppCustomEduPlanDAO;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.plan.gen.EppCustomEduPlanGen;
import ru.tandemservice.uniepp.util.EppDevelopCombinationTitleUtil;

/** @see ru.tandemservice.uniepp.entity.plan.gen.EppCustomEduPlanGen */
@EppStateMutable({EppCustomEduPlan.P_COMMENT})
public class EppCustomEduPlan extends EppCustomEduPlanGen implements ITitled, IEppEduPlanDevelopCombination, IEppStateObject, INumberObject
{
    @EntityDSLSupport
    @Override
    public String getTitle()
    {
        final EppEduPlanVersionSpecializationBlock block = getEpvBlock();
        if (block != null) {
            return "№" + block.getEduPlanVersion().getFullNumber() + "." + this.getNumber();
        }
        return this.getClass().getSimpleName();
    }

    @EntityDSLSupport
    @Override
    public String getFullTitle()
    {
        return getTitle() + " " + getPlan().getProgramSubject().getShortTitleWithCode() + " " + getDevelopCombinationTitle();
    }

    @EntityDSLSupport
    @Override
    public String getTitleWithGrid()
    {
        return getTitle() + " " + getDevelopGrid().getTitle();
    }

    @EntityDSLSupport
    @Override
    public String getDevelopCombinationTitle() {
        return EppDevelopCombinationTitleUtil.getTitle(this);
    }

    @Override
    public EduProgramForm getProgramForm()
    {
        return getEpvBlock().getEduPlanVersion().getEduPlan().getProgramForm();
    }

    @Override
    public EduProgramTrait getProgramTrait()
    {
        return getEpvBlock().getEduPlanVersion().getEduPlan().getProgramTrait();
    }

    @Override
    public EduProgramOrientation getOrientation() {
        return getEpvBlock().getEduPlanVersion().getEduPlan().getOrientation();
    }

    @Override
    public INumberGenerationRule getNumberGenerationRule()
    {
        return IEppCustomEduPlanDAO.instance.get().getCustomEduPlanNumberGenerationRule();
    }
}