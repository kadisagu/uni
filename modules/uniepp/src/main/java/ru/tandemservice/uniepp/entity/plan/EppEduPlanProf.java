package ru.tandemservice.uniepp.entity.plan;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanProfGen;

/**
 * Учебный план проф. образования
 */
public abstract class EppEduPlanProf extends EppEduPlanProfGen
{
    @Override
    public String getEducationElementSimpleTitle()
    {
        return getProgramSubject().getTitleWithCode();
    }

    @Override
    public String getEducationElementShortTitle()
    {
        return getProgramSubject().getShortTitleWithCode();
    }

    @Override
    public String getEducationElementCode()
    {
        return getProgramSubject().getSubjectCode();
    }

    public String getStandardProgramDurationStr()
    {
        return CommonBaseDateUtil.getDatePeriodWithNames(getStdProgramDurationYears(), getStdProgramDurationMonths(), null);
    }

    // Общая трудоемкость образовательной программы (в ЗЕ)
    public Double getLaborAsDouble()
    {
        return getLaborAsLong() != null ? wrap(getLaborAsLong()) : null;
    }

    public void setLaborAsDouble(Double laborAsDouble)
    {
        setLaborAsLong(laborAsDouble != null ? unwrap(laborAsDouble) : null);
    }

    // Общая трудоемкость образовательной программы (в неделях)
    public Double getWeekAsDouble()
    {

        return getWeeksAsLong() != null ? wrap(getWeeksAsLong()) : null;
    }

    public void setWeekAsDouble(Double weekAsDouble)
    {
        setWeeksAsLong(weekAsDouble != null ? unwrap(weekAsDouble) : null);
    }

    public static Double wrap(final long load) {
        return UniEppUtils.wrap(load < 0 ? null : load);
    }
    public static long unwrap(final Double value) {
        return UniEppUtils.unwrap(value);
    }

    public String getLabTitle()
    {
        return getProgramSubject().isLabor() ? "Общая трудоемкость (в з.е.)" : "Общая трудоемкость (в неделях)";
    }

    public String getLab()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getProgramSubject().isLabor() ? getLaborAsDouble() : getWeekAsDouble());
    }
}