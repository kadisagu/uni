package ru.tandemservice.uniepp.dao.eduplan;

import org.hibernate.Session;
import org.tandemframework.core.entity.dsl.EntityPath;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EppEpvRowHierarchyCheckDSetEventListenerBean {

    private static class Listener extends TransactionCompleteAction<Boolean> implements IDSetEventListener {
        private static final Listener INSTANCE = new Listener();


        private boolean correct(Class<? extends EppEpvRow> klass, EntityPath<? extends EppEpvRow> path, Session session)
        {

            // сначала меняем блок, если не совпали версии
            if (CommonDAO.executeAndClear(
                new DQLUpdateBuilder(klass)
                .fromEntity(EppEpvRow.class, "p").where(eq(property("p"), property(path)))
                .set(EppEpvRow.L_OWNER, property(EppEpvRow.owner().id().fromAlias("p")))
                .where(ne(property(EppEpvRow.owner().eduPlanVersion()), property(EppEpvRow.owner().eduPlanVersion().fromAlias("p"))) /* не совпадают УП(в) */)
                .where(notIn(property(EppEpvRow.owner().eduPlanVersion().state().code()), EppState.READ_ONLY_CODES)) /* УП должен быть доступен для изменения */,
                session
            ) > 0) {
                return true;
            }

            // затем меняем блок, если не совпали направления
            if (CommonDAO.executeAndClear(
                new DQLUpdateBuilder(klass)
                .fromEntity(EppEpvRow.class, "p").where(eq(property("p"), property(path)))
                .set(EppEpvRow.L_OWNER, property(EppEpvRow.owner().id().fromAlias("p")))
                .where(instanceOf(EppEpvRow.owner().fromAlias("p").s(), EppEduPlanVersionSpecializationBlock.class) /* основной элемент из дочернего блока */)
                .where(ne(property(EppEpvRow.owner()), property(EppEpvRow.owner().fromAlias("p"))) /* и это не тот же самый блок */)
                .where(notIn(property(EppEpvRow.owner().eduPlanVersion().state().code()), EppState.READ_ONLY_CODES)) /* УП должен быть доступен для изменения */,
                session
            ) > 0) {
                return true;
            }

            return false;
        }

        @Override public Boolean beforeCompletion(Session session) {
            final EventListenerLocker.Lock lock = EppDSetUpdateCheckEventListener.LOCKER.lock(new EventListenerLocker.Handler<IDSetEventListener>() {
                @Override public boolean handle(IDSetEventListener listener, IEntityMeta entityMeta, Set<String> properties, Number count, DSetEvent event) {
                    return EppEpvRow.class.isAssignableFrom(entityMeta.getEntityClass()); /* если это строка УП(в), то ошибки нет */
                }
            });
            try {

                // идея этой сказки, а может и не сказки: нашли ошибку - исправляем
                // (поскольку исправление идет по принципу - что у родителя, то и у меня, то ошибки исправтся максимум за столько итераций, какова максимальная из глубин иерархий среди всех УП(в))
                // 10 - магическое число, если глубина ошибок более 10 (что трудно представить даже если в базе сделать shuffle), то все упадет
                for (int i=0; i<10; i++) {
                    boolean changed = false;
                    changed = correct(EppEpvStructureRow.class, EppEpvStructureRow.parent(), session) | changed;
                    changed = correct(EppEpvGroupReRow.class, EppEpvGroupReRow.parent(), session) | changed;
                    changed = correct(EppEpvTermDistributedRow.class, EppEpvTermDistributedRow.parent(), session) | changed;

                    if (!changed) { return true; /* все в порядке, багов больше нет */ }
                }
                throw new IllegalStateException("Unrecoverable problem");

            } finally {
                lock.close();
            }
        }

        @Override public void onEvent(DSetEvent event) {
            final Set<String> affectedProperties = event.getMultitude().getAffectedProperties();
            if (affectedProperties.contains(EppEpvRow.L_OWNER) || affectedProperties.contains("parent")) {
                this.register(event);
            }
        }
    }


    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppEpvRow.class, Listener.INSTANCE);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppEpvRow.class, Listener.INSTANCE);
    }
}
