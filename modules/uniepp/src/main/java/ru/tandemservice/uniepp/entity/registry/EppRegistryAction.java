package ru.tandemservice.uniepp.entity.registry;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryActionGen;

/**
 * Мероприятие (из реестра)
 */
public abstract class EppRegistryAction extends EppRegistryActionGen
{
    public Double getWeeksAsDouble() {
        final long weeks = this.getWeeks();
        return UniEppUtils.wrap(weeks < 0 ? null : weeks);
    }

    public void setWeeksAsDouble(final Double value) {
        final Long weeks = UniEppUtils.unwrap(value);
        this.setWeeks(null == weeks ? -1 : weeks.longValue());
    }

    @Override
    public String getFormattedLoad() {
        return UniEppUtils.formatLoad(this.getWeeksAsDouble(), false) + " нед.";
    }

    @Override
    public String getDescriptionString() {
        return (super.getDescriptionString()+'\n'+this.getWeeks());
    }

    /**
     * [Название дисциплины] ([всего недель] ч., [Сокр. название читающего подразделения], [тип] №[номер из реестра мероприятий (дисциплин)])
     **/
    @Override
    public String getEducationElementTitle() {
        return this.getTitle()+" ("+this.getFormattedLoad()+", "+this.getOwner().getShortTitle()+", "+this.getNumberWithAbbreviation()+")";
    }

    /**
     * [Название дисциплины]
     */
    @Override
    public String getEducationElementSimpleTitle()
    {
        return this.getTitle();
    }
}