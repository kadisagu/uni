/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionCopyScheduleData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author nkokorina
 *         Created on: 24.09.2010
 */

@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduplanVersion.id")
})
public class Model
{
    private EppEduPlanVersion _eduplanVersion = new EppEduPlanVersion();

    private ISelectModel sourceVersionSelectModel;
    private List<Course> courseList;

    private EppEduPlanVersion sourceVersion;
    private Course course;
    private final Map<Course, Boolean> courseSelectedMap = new HashMap<Course, Boolean>();

    public boolean isShowCourseBlock()
    {
        return (null != this.courseList) && !this.courseList.isEmpty();
    }

    public Boolean getCourseSelected()
    {
        return this.courseSelectedMap.containsKey(this.course) ? this.courseSelectedMap.get(this.course) : false;
    }

    public void setCourseSelected(final Boolean courseSelected)
    {
        this.courseSelectedMap.put(this.course, courseSelected);
    }

    public Map<Course, Boolean> getCourseSelectedMap()
    {
        return this.courseSelectedMap;
    }

    public List<Course> getCourseList()
    {
        return this.courseList;
    }

    public void setCourseList(final List<Course> courseList)
    {
        this.courseList = courseList;
    }

    public Course getCourse()
    {
        return this.course;
    }

    public void setCourse(final Course course)
    {
        this.course = course;
    }

    public EppEduPlanVersion getEduplanVersion()
    {
        return this._eduplanVersion;
    }

    public void setEduplanVersion(final EppEduPlanVersion eduplanVersion)
    {
        this._eduplanVersion = eduplanVersion;
    }

    public ISelectModel getSourceVersionSelectModel()
    {
        return this.sourceVersionSelectModel;
    }

    public void setSourceVersionSelectModel(final ISelectModel sourceVersionSelectModel)
    {
        this.sourceVersionSelectModel = sourceVersionSelectModel;
    }


    public EppEduPlanVersion getSourceVersion()
    {
        return this.sourceVersion;
    }

    public void setSourceVersion(final EppEduPlanVersion sourceVersion)
    {
        this.sourceVersion = sourceVersion;
    }

}
