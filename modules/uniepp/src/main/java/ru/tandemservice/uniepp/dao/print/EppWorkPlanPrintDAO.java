package ru.tandemservice.uniepp.dao.print;

import java.util.Collection;
import java.util.Map;

import jxl.format.PageOrientation;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.DevelopUtil;
import ru.tandemservice.uni.util.jxl.ListDataSourcePrinter;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.ui.WorkPlanDataSourceGenerator;
import ru.tandemservice.uniepp.util.EppDevelopCombinationTitleUtil;

/**
 * @author vdanilov
 */
public class EppWorkPlanPrintDAO extends UniBaseDao implements IEppWorkPlanPrintDAO {


    protected void addTextLineCell(final WritableSheet sheet, final int top, final int left, final int width, final String text) {
        try {
            final Label cell = new Label(left, top, text);
            sheet.addCell(cell);
            sheet.mergeCells(cell.getColumn(), cell.getRow(), (cell.getColumn()+Math.max(1, width))-1, cell.getRow());
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    protected WritableSheet createWritableSheet(final WritableWorkbook book, final String title, final int index) {
        final WritableSheet sheet = book.createSheet(title, index);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);
        sheet.getSettings().setScaleFactor(70);
        sheet.getSettings().setBottomMargin(.3937);
        sheet.getSettings().setTopMargin(.3937);
        sheet.getSettings().setLeftMargin(.3937);
        sheet.getSettings().setRightMargin(.3937);

        for (int i=0;i<256;i++) {
            sheet.setColumnView(i, 2);
        }
        return sheet;
    }

    @SuppressWarnings("unchecked")
    protected ListDataSourcePrinter buildListDataSourcePrinter(final WritableSheet sheet, final AbstractListDataSource source) {
        return new EppListDataSourcePrinter(sheet, source) {

            @Override protected WritableCellFormat getBodyCellFormat(final AbstractColumn column, final IEntity e, final String bodyCellContent) {
                final String name = column.getName();

                if (name.startsWith("load.")) { return this.STYLE_TABLE_BODY_NUMBER; }
                if (name.startsWith("action.")) { return this.STYLE_TABLE_BODY_NUMBER; }
                if ("null.displayableTitle".equals(name)) { return this.STYLE_TABLE_BODY_TITLE; }
                if ("owner".equals(name)) { return this.STYLE_TABLE_BODY_TITLE; }

                return super.getBodyCellFormat(column, e, bodyCellContent);
            }

            @Override protected int getColumnWidth(final AbstractColumn column) {
                final String name = column.getName();

                if (name.startsWith("action.")) { return 2; }
                if (name.startsWith("load.")) { return 2; }

                if ("index".equals(name)) { return 5; }
                if ("owner".equals(name)) { return 32; }
                if ("beginDate".equals(name)) { return 9; }
                if ("endDate".equals(name)) { return 9; }
                if ("actionList".equals(name)) { return 15; }
                if (StringUtils.contains(name, "displayableTitle")) { return 18; }
                return 2;
            }

        };
    }

    @Override
    public void printGroupWorkPlan(final WritableWorkbook book, final Map<Long, Map<Long, Collection<Long>>> wp2gsIdsMap, final Map<String, Object> context)
    {
        final Map<Long, IEppWorkPlanWrapper> wpDataMap = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(wp2gsIdsMap.keySet());
        for (final Map.Entry<Long, Map<Long, Collection<Long>>> wpEntry: wp2gsIdsMap.entrySet())
        {
            final IEppWorkPlanWrapper wpData = wpDataMap.get(wpEntry.getKey());
            for (final Map.Entry<Long, Collection<Long>> groupEntry: wpEntry.getValue().entrySet())
            {

                final Long groupId = groupEntry.getKey();
                final Group group = (null == groupId ? null : this.get(Group.class, groupId));
                final WritableSheet sheet = this.createWritableSheet(book, "№" + wpData.getWorkPlan().getFullNumber(), 0);
                {
                    int top = 0;

                    {
                        this.addTextLineCell(sheet, top, 0, 15, "Группа: ");
                        this.addTextLineCell(sheet, top, 20, 40, (null == group ? "вне группы" : group.getTitle()) + " (количество студентов - "+groupEntry.getValue().size()+")");
                        top++;
                    }

                    {
                        this.addTextLineCell(sheet, top, 0, 15, "Направление подготовки: ");
                        this.addTextLineCell(sheet, top, 20, 60, (null == group ? "" : (group.getEducationOrgUnit().getTitleWithFormAndCondition() + ", " + group.getEducationOrgUnit().getEducationLevelHighSchool().getOrgUnit().getPrintTitle())));
                        top++;
                    }

                    {
                        this.addTextLineCell(sheet, top, 0, 15, "Рабочий учебный план: ");
                        this.addTextLineCell(sheet, top, 20, 40, wpData.getTitle());
                        top++;
                    }


                    final WorkPlanDataSourceGenerator generator = new WorkPlanDataSourceGenerator() {
                        @Override protected boolean isEditable() { return false; }
                        @Override protected String getPermissionKeyEdit() { return null; }
                        @Override protected Long getWorkPlanBaseId() { return wpData.getId(); }
                    };

                    {
                        top += 1;
                        final StaticListDataSource<IEppWorkPlanRowWrapper> source = new StaticListDataSource<IEppWorkPlanRowWrapper>();
                        generator.fillDisciplineDataSource(source);
                        top = this.buildListDataSourcePrinter(sheet, source).print(top, 0);
                    }

                    {
                        top += 1;
                        final StaticListDataSource<IEppWorkPlanRowWrapper> source = new StaticListDataSource<IEppWorkPlanRowWrapper>();
                        generator.fillActionDataSource(source);
                        top = this.buildListDataSourcePrinter(sheet, source).print(top, 0);
                    }

                }
            }
        }
    }


    @Override
    public void printRegistryWorkPlan(final WritableWorkbook book, final Collection<Long> wpIds, final Map<String, Object> context)
    {
        final Map<Long, IEppWorkPlanWrapper> wpDataMap = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(wpIds);
        for (final IEppWorkPlanWrapper wrapper: wpDataMap.values()) {
            final WritableSheet sheet = this.createWritableSheet(book, "№" + wrapper.getWorkPlan().getFullNumber(), 0);
            {
                int top = 0;

                {
                    this.addTextLineCell(sheet, top, 0, 15, "Рабочий учебный план: ");
                    this.addTextLineCell(sheet, top, 20, 40, wrapper.getTitle());
                    top++;
                }

                {
                    this.addTextLineCell(sheet, top, 0, 15, "Направление (специальность, профессия) и направленность (профиль, специализация): ");
                    this.addTextLineCell(sheet, top, 20, 60, wrapper.getWorkPlan().getBlock().getEduPlanVersion().getEduPlan().getEducationElementSimpleTitle() + (wrapper.getWorkPlan().getBlock().isRootBlock() ? "" : ", " + wrapper.getWorkPlan().getBlock().getEducationElementSimpleTitle()));
                    top++;
                }

                {
                    this.addTextLineCell(sheet, top, 0, 15, "Форма обучения: ");
                    this.addTextLineCell(sheet, top, 20, 60, EppDevelopCombinationTitleUtil.getTitle(wrapper.getWorkPlan().getEduPlan()));
                    top++;
                }


                final WorkPlanDataSourceGenerator generator = new WorkPlanDataSourceGenerator() {
                    @Override protected boolean isEditable() { return false; }
                    @Override protected String getPermissionKeyEdit() { return null; }
                    @Override protected Long getWorkPlanBaseId() { return wrapper.getId(); }
                };

                {
                    top += 1;
                    final StaticListDataSource<IEppWorkPlanRowWrapper> source = new StaticListDataSource<IEppWorkPlanRowWrapper>();
                    generator.fillDisciplineDataSource(source);
                    top = this.buildListDataSourcePrinter(sheet, source).print(top, 0);
                }

                {
                    top += 1;
                    final StaticListDataSource<IEppWorkPlanRowWrapper> source = new StaticListDataSource<IEppWorkPlanRowWrapper>();
                    generator.fillActionDataSource(source);
                    top = this.buildListDataSourcePrinter(sheet, source).print(top, 0);
                }
            }
        }
    }

    @Override
    public void printRegistryWorkPlanWithVersions(final WritableWorkbook book, final Collection<Long> wpIds, final Map<String, Object> context) {
        // TODO:
    }


}
