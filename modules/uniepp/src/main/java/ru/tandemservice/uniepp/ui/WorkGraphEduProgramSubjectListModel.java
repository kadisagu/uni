/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.ui;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Collection;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan;

/**
 * Фильтр по направлениям подготовки (специальностям) при работе с ГУП
 *
 * @author vip_delete
 * @since 23.03.2010
 */
public class WorkGraphEduProgramSubjectListModel extends DQLFullCheckSelectModel
{
    private final EppWorkGraph _workGraph;
    private final CoursesProvider _coursesProvider;

    public WorkGraphEduProgramSubjectListModel(final EppWorkGraph workGraph, final CoursesProvider coursesProvider)
    {
        super(EduProgramSubject.P_TITLE_WITH_CODE);
        this._workGraph = workGraph;
        this._coursesProvider = coursesProvider;
    }

    @Override
    protected DQLSelectBuilder query(final String alias, final String filter)
    {

        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppWorkGraphRow2EduPlan.class, "rel");
        dql.fromEntity(EppEduPlanProf.class, "prof");
        dql.where(eq(
                property("prof"),
                property(EppWorkGraphRow2EduPlan.eduPlanVersion().eduPlan().fromAlias("rel"))
        ));

        dql.where(eq(property(EppWorkGraphRow2EduPlan.row().graph().fromAlias("rel")), value(this._workGraph)));

        if (null != this._coursesProvider) {
            final Collection<Course> courses = this._coursesProvider.getCourses();
            if ((null != courses) && (courses.size() > 0)) {
                dql.where(in(property(EppWorkGraphRow2EduPlan.row().course().fromAlias("rel")), courses));
            }
        }

        dql.column(property(EppEduPlanProf.programSubject().fromAlias("prof")));

        final DQLSelectBuilder tmp = new DQLSelectBuilder();
        tmp.fromEntity(EduProgramSubject.class, alias);
        tmp.where(in(property(alias, "id"), dql.buildQuery()));
        FilterUtils.applyLikeFilter(tmp, filter, EduProgramSubject.code().fromAlias(alias), EduProgramSubject.title().fromAlias(alias));
        tmp.order(property(EduProgramSubject.code().fromAlias(alias)));
        tmp.order(property(EduProgramSubject.title().fromAlias(alias)));
        return tmp;
    }

    public static interface CoursesProvider
    {
        Collection<Course> getCourses();
    }
}
