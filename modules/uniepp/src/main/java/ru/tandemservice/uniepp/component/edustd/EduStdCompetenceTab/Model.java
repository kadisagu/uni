package ru.tandemservice.uniepp.component.edustd.EduStdCompetenceTab;

import java.util.Collections;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eppStateEduStandard.id") })
public class Model
{
    private EppStateEduStandard eppStateEduStandard = new EppStateEduStandard();
    private List<SkillGroupBlock> blockList = Collections.emptyList();
    private SkillGroupBlock block;

    public List<SkillGroupBlock> getBlockList()
    {
        return this.blockList;
    }

    public void setBlockList(final List<SkillGroupBlock> blockList)
    {
        this.blockList = blockList;
    }

    public SkillGroupBlock getBlock()
    {
        return this.block;
    }

    public void setBlock(final SkillGroupBlock block)
    {
        this.block = block;
    }

    public void setEppStateEduStandard(final EppStateEduStandard _eppStateEduStandard)
    {
        this.eppStateEduStandard = _eppStateEduStandard;
    }

    public EppStateEduStandard getEppStateEduStandard()
    {
        return this.eppStateEduStandard;
    }

    public static class SkillGroupBlock
    {
        private String title;
        private StaticListDataSource<EppStateEduStandardSkill> dataSource;

        public SkillGroupBlock(final String title, final StaticListDataSource<EppStateEduStandardSkill> dataSource)
        {
            this.title = title;
            this.dataSource = dataSource;
        }

        public String getTitle()
        {
            return this.title;
        }

        public void setTitle(final String title)
        {
            this.title = title;
        }

        public StaticListDataSource<EppStateEduStandardSkill> getDataSource()
        {
            return this.dataSource;
        }

        public void setDataSource(final StaticListDataSource<EppStateEduStandardSkill> dataSource)
        {
            this.dataSource = dataSource;
        }
    }
}
