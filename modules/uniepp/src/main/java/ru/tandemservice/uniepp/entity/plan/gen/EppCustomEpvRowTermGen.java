package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Перемещение, переаттестация или перезачет семестра строки УП
 *
 * Содержит информацию о модификации семестра строки УПв.
 * Семестр строки УПв можно переместить (сменить семестр).
 * Семестр строки УПв можно переаттестовать либо перезачесть.
 * Семестр строки УПв можно удалить (только для факультативных дисциплин).
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppCustomEpvRowTermGen extends EntityBase
 implements INaturalIdentifiable<EppCustomEpvRowTermGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm";
    public static final String ENTITY_NAME = "eppCustomEpvRowTerm";
    public static final int VERSION_HASH = -2025567712;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPV_ROW_TERM = "epvRowTerm";
    public static final String L_CUSTOM_EDU_PLAN = "customEduPlan";
    public static final String L_NEW_TERM = "newTerm";
    public static final String P_NEED_RETAKE = "needRetake";
    public static final String P_EXCLUDED = "excluded";

    private EppEpvRowTerm _epvRowTerm;     // Семестр строки УПв
    private EppCustomEduPlan _customEduPlan;     // Индивидуальный учебный план
    private Term _newTerm;     // Новый семестр
    private Boolean _needRetake;     // Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение)
    private boolean _excluded;     // Удалено (факультативная дисциплина)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Семестр строки УПв. Свойство не может быть null.
     */
    @NotNull
    public EppEpvRowTerm getEpvRowTerm()
    {
        return _epvRowTerm;
    }

    /**
     * @param epvRowTerm Семестр строки УПв. Свойство не может быть null.
     */
    public void setEpvRowTerm(EppEpvRowTerm epvRowTerm)
    {
        dirty(_epvRowTerm, epvRowTerm);
        _epvRowTerm = epvRowTerm;
    }

    /**
     * @return Индивидуальный учебный план. Свойство не может быть null.
     */
    @NotNull
    public EppCustomEduPlan getCustomEduPlan()
    {
        return _customEduPlan;
    }

    /**
     * @param customEduPlan Индивидуальный учебный план. Свойство не может быть null.
     */
    public void setCustomEduPlan(EppCustomEduPlan customEduPlan)
    {
        dirty(_customEduPlan, customEduPlan);
        _customEduPlan = customEduPlan;
    }

    /**
     * Если новый семестр отличается от семестра строки УПв, это является перемещением.
     *
     * @return Новый семестр. Свойство не может быть null.
     */
    @NotNull
    public Term getNewTerm()
    {
        return _newTerm;
    }

    /**
     * @param newTerm Новый семестр. Свойство не может быть null.
     */
    public void setNewTerm(Term newTerm)
    {
        dirty(_newTerm, newTerm);
        _newTerm = newTerm;
    }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     * null  - обычное освоение: необходимо изучать дисциплину и сдавать контрольные мероприятия, как обычно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение).
     */
    public Boolean getNeedRetake()
    {
        return _needRetake;
    }

    /**
     * @param needRetake Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение).
     */
    public void setNeedRetake(Boolean needRetake)
    {
        dirty(_needRetake, needRetake);
        _needRetake = needRetake;
    }

    /**
     * Удалять можно только факультативные дисциплины. Проверки на это есть только в коде.
     *
     * @return Удалено (факультативная дисциплина). Свойство не может быть null.
     */
    @NotNull
    public boolean isExcluded()
    {
        return _excluded;
    }

    /**
     * @param excluded Удалено (факультативная дисциплина). Свойство не может быть null.
     */
    public void setExcluded(boolean excluded)
    {
        dirty(_excluded, excluded);
        _excluded = excluded;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppCustomEpvRowTermGen)
        {
            if (withNaturalIdProperties)
            {
                setEpvRowTerm(((EppCustomEpvRowTerm)another).getEpvRowTerm());
                setCustomEduPlan(((EppCustomEpvRowTerm)another).getCustomEduPlan());
            }
            setNewTerm(((EppCustomEpvRowTerm)another).getNewTerm());
            setNeedRetake(((EppCustomEpvRowTerm)another).getNeedRetake());
            setExcluded(((EppCustomEpvRowTerm)another).isExcluded());
        }
    }

    public INaturalId<EppCustomEpvRowTermGen> getNaturalId()
    {
        return new NaturalId(getEpvRowTerm(), getCustomEduPlan());
    }

    public static class NaturalId extends NaturalIdBase<EppCustomEpvRowTermGen>
    {
        private static final String PROXY_NAME = "EppCustomEpvRowTermNaturalProxy";

        private Long _epvRowTerm;
        private Long _customEduPlan;

        public NaturalId()
        {}

        public NaturalId(EppEpvRowTerm epvRowTerm, EppCustomEduPlan customEduPlan)
        {
            _epvRowTerm = ((IEntity) epvRowTerm).getId();
            _customEduPlan = ((IEntity) customEduPlan).getId();
        }

        public Long getEpvRowTerm()
        {
            return _epvRowTerm;
        }

        public void setEpvRowTerm(Long epvRowTerm)
        {
            _epvRowTerm = epvRowTerm;
        }

        public Long getCustomEduPlan()
        {
            return _customEduPlan;
        }

        public void setCustomEduPlan(Long customEduPlan)
        {
            _customEduPlan = customEduPlan;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppCustomEpvRowTermGen.NaturalId) ) return false;

            EppCustomEpvRowTermGen.NaturalId that = (NaturalId) o;

            if( !equals(getEpvRowTerm(), that.getEpvRowTerm()) ) return false;
            if( !equals(getCustomEduPlan(), that.getCustomEduPlan()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEpvRowTerm());
            result = hashCode(result, getCustomEduPlan());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEpvRowTerm());
            sb.append("/");
            sb.append(getCustomEduPlan());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppCustomEpvRowTermGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppCustomEpvRowTerm.class;
        }

        public T newInstance()
        {
            return (T) new EppCustomEpvRowTerm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "epvRowTerm":
                    return obj.getEpvRowTerm();
                case "customEduPlan":
                    return obj.getCustomEduPlan();
                case "newTerm":
                    return obj.getNewTerm();
                case "needRetake":
                    return obj.getNeedRetake();
                case "excluded":
                    return obj.isExcluded();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "epvRowTerm":
                    obj.setEpvRowTerm((EppEpvRowTerm) value);
                    return;
                case "customEduPlan":
                    obj.setCustomEduPlan((EppCustomEduPlan) value);
                    return;
                case "newTerm":
                    obj.setNewTerm((Term) value);
                    return;
                case "needRetake":
                    obj.setNeedRetake((Boolean) value);
                    return;
                case "excluded":
                    obj.setExcluded((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "epvRowTerm":
                        return true;
                case "customEduPlan":
                        return true;
                case "newTerm":
                        return true;
                case "needRetake":
                        return true;
                case "excluded":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "epvRowTerm":
                    return true;
                case "customEduPlan":
                    return true;
                case "newTerm":
                    return true;
                case "needRetake":
                    return true;
                case "excluded":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "epvRowTerm":
                    return EppEpvRowTerm.class;
                case "customEduPlan":
                    return EppCustomEduPlan.class;
                case "newTerm":
                    return Term.class;
                case "needRetake":
                    return Boolean.class;
                case "excluded":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppCustomEpvRowTerm> _dslPath = new Path<EppCustomEpvRowTerm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppCustomEpvRowTerm");
    }
            

    /**
     * @return Семестр строки УПв. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm#getEpvRowTerm()
     */
    public static EppEpvRowTerm.Path<EppEpvRowTerm> epvRowTerm()
    {
        return _dslPath.epvRowTerm();
    }

    /**
     * @return Индивидуальный учебный план. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm#getCustomEduPlan()
     */
    public static EppCustomEduPlan.Path<EppCustomEduPlan> customEduPlan()
    {
        return _dslPath.customEduPlan();
    }

    /**
     * Если новый семестр отличается от семестра строки УПв, это является перемещением.
     *
     * @return Новый семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm#getNewTerm()
     */
    public static Term.Path<Term> newTerm()
    {
        return _dslPath.newTerm();
    }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     * null  - обычное освоение: необходимо изучать дисциплину и сдавать контрольные мероприятия, как обычно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение).
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm#getNeedRetake()
     */
    public static PropertyPath<Boolean> needRetake()
    {
        return _dslPath.needRetake();
    }

    /**
     * Удалять можно только факультативные дисциплины. Проверки на это есть только в коде.
     *
     * @return Удалено (факультативная дисциплина). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm#isExcluded()
     */
    public static PropertyPath<Boolean> excluded()
    {
        return _dslPath.excluded();
    }

    public static class Path<E extends EppCustomEpvRowTerm> extends EntityPath<E>
    {
        private EppEpvRowTerm.Path<EppEpvRowTerm> _epvRowTerm;
        private EppCustomEduPlan.Path<EppCustomEduPlan> _customEduPlan;
        private Term.Path<Term> _newTerm;
        private PropertyPath<Boolean> _needRetake;
        private PropertyPath<Boolean> _excluded;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Семестр строки УПв. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm#getEpvRowTerm()
     */
        public EppEpvRowTerm.Path<EppEpvRowTerm> epvRowTerm()
        {
            if(_epvRowTerm == null )
                _epvRowTerm = new EppEpvRowTerm.Path<EppEpvRowTerm>(L_EPV_ROW_TERM, this);
            return _epvRowTerm;
        }

    /**
     * @return Индивидуальный учебный план. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm#getCustomEduPlan()
     */
        public EppCustomEduPlan.Path<EppCustomEduPlan> customEduPlan()
        {
            if(_customEduPlan == null )
                _customEduPlan = new EppCustomEduPlan.Path<EppCustomEduPlan>(L_CUSTOM_EDU_PLAN, this);
            return _customEduPlan;
        }

    /**
     * Если новый семестр отличается от семестра строки УПв, это является перемещением.
     *
     * @return Новый семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm#getNewTerm()
     */
        public Term.Path<Term> newTerm()
        {
            if(_newTerm == null )
                _newTerm = new Term.Path<Term>(L_NEW_TERM, this);
            return _newTerm;
        }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     * null  - обычное освоение: необходимо изучать дисциплину и сдавать контрольные мероприятия, как обычно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение).
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm#getNeedRetake()
     */
        public PropertyPath<Boolean> needRetake()
        {
            if(_needRetake == null )
                _needRetake = new PropertyPath<Boolean>(EppCustomEpvRowTermGen.P_NEED_RETAKE, this);
            return _needRetake;
        }

    /**
     * Удалять можно только факультативные дисциплины. Проверки на это есть только в коде.
     *
     * @return Удалено (факультативная дисциплина). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppCustomEpvRowTerm#isExcluded()
     */
        public PropertyPath<Boolean> excluded()
        {
            if(_excluded == null )
                _excluded = new PropertyPath<Boolean>(EppCustomEpvRowTermGen.P_EXCLUDED, this);
            return _excluded;
        }

        public Class getEntityClass()
        {
            return EppCustomEpvRowTerm.class;
        }

        public String getEntityName()
        {
            return "eppCustomEpvRowTerm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
