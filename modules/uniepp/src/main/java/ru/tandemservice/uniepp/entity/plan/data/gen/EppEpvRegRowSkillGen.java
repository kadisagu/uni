package ru.tandemservice.uniepp.entity.plan.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppSkill;
import ru.tandemservice.uniepp.entity.catalog.EppSkillDevelopLevel;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowSkill;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Компетенция строки УП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvRegRowSkillGen extends EntityBase
 implements INaturalIdentifiable<EppEpvRegRowSkillGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowSkill";
    public static final String ENTITY_NAME = "eppEpvRegRowSkill";
    public static final int VERSION_HASH = -22157199;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPV_REGISTRY_ROW = "epvRegistryRow";
    public static final String L_SKILL = "skill";
    public static final String L_SKILL_DEVELOP_LEVEL = "skillDevelopLevel";

    private EppEpvRegistryRow _epvRegistryRow;     // Строка УП (элемент реестра)
    private EppSkill _skill;     // Компетенция
    private EppSkillDevelopLevel _skillDevelopLevel;     // Уровень освоения компетенции

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка УП (элемент реестра). Свойство не может быть null.
     */
    @NotNull
    public EppEpvRegistryRow getEpvRegistryRow()
    {
        return _epvRegistryRow;
    }

    /**
     * @param epvRegistryRow Строка УП (элемент реестра). Свойство не может быть null.
     */
    public void setEpvRegistryRow(EppEpvRegistryRow epvRegistryRow)
    {
        dirty(_epvRegistryRow, epvRegistryRow);
        _epvRegistryRow = epvRegistryRow;
    }

    /**
     * @return Компетенция. Свойство не может быть null.
     */
    @NotNull
    public EppSkill getSkill()
    {
        return _skill;
    }

    /**
     * @param skill Компетенция. Свойство не может быть null.
     */
    public void setSkill(EppSkill skill)
    {
        dirty(_skill, skill);
        _skill = skill;
    }

    /**
     * @return Уровень освоения компетенции.
     */
    public EppSkillDevelopLevel getSkillDevelopLevel()
    {
        return _skillDevelopLevel;
    }

    /**
     * @param skillDevelopLevel Уровень освоения компетенции.
     */
    public void setSkillDevelopLevel(EppSkillDevelopLevel skillDevelopLevel)
    {
        dirty(_skillDevelopLevel, skillDevelopLevel);
        _skillDevelopLevel = skillDevelopLevel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEpvRegRowSkillGen)
        {
            if (withNaturalIdProperties)
            {
                setEpvRegistryRow(((EppEpvRegRowSkill)another).getEpvRegistryRow());
                setSkill(((EppEpvRegRowSkill)another).getSkill());
            }
            setSkillDevelopLevel(((EppEpvRegRowSkill)another).getSkillDevelopLevel());
        }
    }

    public INaturalId<EppEpvRegRowSkillGen> getNaturalId()
    {
        return new NaturalId(getEpvRegistryRow(), getSkill());
    }

    public static class NaturalId extends NaturalIdBase<EppEpvRegRowSkillGen>
    {
        private static final String PROXY_NAME = "EppEpvRegRowSkillNaturalProxy";

        private Long _epvRegistryRow;
        private Long _skill;

        public NaturalId()
        {}

        public NaturalId(EppEpvRegistryRow epvRegistryRow, EppSkill skill)
        {
            _epvRegistryRow = ((IEntity) epvRegistryRow).getId();
            _skill = ((IEntity) skill).getId();
        }

        public Long getEpvRegistryRow()
        {
            return _epvRegistryRow;
        }

        public void setEpvRegistryRow(Long epvRegistryRow)
        {
            _epvRegistryRow = epvRegistryRow;
        }

        public Long getSkill()
        {
            return _skill;
        }

        public void setSkill(Long skill)
        {
            _skill = skill;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppEpvRegRowSkillGen.NaturalId) ) return false;

            EppEpvRegRowSkillGen.NaturalId that = (NaturalId) o;

            if( !equals(getEpvRegistryRow(), that.getEpvRegistryRow()) ) return false;
            if( !equals(getSkill(), that.getSkill()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEpvRegistryRow());
            result = hashCode(result, getSkill());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEpvRegistryRow());
            sb.append("/");
            sb.append(getSkill());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvRegRowSkillGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvRegRowSkill.class;
        }

        public T newInstance()
        {
            return (T) new EppEpvRegRowSkill();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "epvRegistryRow":
                    return obj.getEpvRegistryRow();
                case "skill":
                    return obj.getSkill();
                case "skillDevelopLevel":
                    return obj.getSkillDevelopLevel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "epvRegistryRow":
                    obj.setEpvRegistryRow((EppEpvRegistryRow) value);
                    return;
                case "skill":
                    obj.setSkill((EppSkill) value);
                    return;
                case "skillDevelopLevel":
                    obj.setSkillDevelopLevel((EppSkillDevelopLevel) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "epvRegistryRow":
                        return true;
                case "skill":
                        return true;
                case "skillDevelopLevel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "epvRegistryRow":
                    return true;
                case "skill":
                    return true;
                case "skillDevelopLevel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "epvRegistryRow":
                    return EppEpvRegistryRow.class;
                case "skill":
                    return EppSkill.class;
                case "skillDevelopLevel":
                    return EppSkillDevelopLevel.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvRegRowSkill> _dslPath = new Path<EppEpvRegRowSkill>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvRegRowSkill");
    }
            

    /**
     * @return Строка УП (элемент реестра). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowSkill#getEpvRegistryRow()
     */
    public static EppEpvRegistryRow.Path<EppEpvRegistryRow> epvRegistryRow()
    {
        return _dslPath.epvRegistryRow();
    }

    /**
     * @return Компетенция. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowSkill#getSkill()
     */
    public static EppSkill.Path<EppSkill> skill()
    {
        return _dslPath.skill();
    }

    /**
     * @return Уровень освоения компетенции.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowSkill#getSkillDevelopLevel()
     */
    public static EppSkillDevelopLevel.Path<EppSkillDevelopLevel> skillDevelopLevel()
    {
        return _dslPath.skillDevelopLevel();
    }

    public static class Path<E extends EppEpvRegRowSkill> extends EntityPath<E>
    {
        private EppEpvRegistryRow.Path<EppEpvRegistryRow> _epvRegistryRow;
        private EppSkill.Path<EppSkill> _skill;
        private EppSkillDevelopLevel.Path<EppSkillDevelopLevel> _skillDevelopLevel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка УП (элемент реестра). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowSkill#getEpvRegistryRow()
     */
        public EppEpvRegistryRow.Path<EppEpvRegistryRow> epvRegistryRow()
        {
            if(_epvRegistryRow == null )
                _epvRegistryRow = new EppEpvRegistryRow.Path<EppEpvRegistryRow>(L_EPV_REGISTRY_ROW, this);
            return _epvRegistryRow;
        }

    /**
     * @return Компетенция. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowSkill#getSkill()
     */
        public EppSkill.Path<EppSkill> skill()
        {
            if(_skill == null )
                _skill = new EppSkill.Path<EppSkill>(L_SKILL, this);
            return _skill;
        }

    /**
     * @return Уровень освоения компетенции.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowSkill#getSkillDevelopLevel()
     */
        public EppSkillDevelopLevel.Path<EppSkillDevelopLevel> skillDevelopLevel()
        {
            if(_skillDevelopLevel == null )
                _skillDevelopLevel = new EppSkillDevelopLevel.Path<EppSkillDevelopLevel>(L_SKILL_DEVELOP_LEVEL, this);
            return _skillDevelopLevel;
        }

        public Class getEntityClass()
        {
            return EppEpvRegRowSkill.class;
        }

        public String getEntityName()
        {
            return "eppEpvRegRowSkill";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
