/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsSlotList;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListManager;

/**
 * @author Alexander Shaburov
 * @since 01.03.13
 */
@Configuration
public class EppIndicatorStudentsSlotList extends AbstractEppIndicatorStudentListManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return  super.presenterExtPoint(
            presenterExtPointBuilder()
            .addDataSource(searchListDS(STUDENT_SEARCH_LIST_DS, studentSearchListDSColumnExtPoint(), studentSearchListDSHandler()))
        );
    }

    @Bean
    public ColumnListExtPoint studentSearchListDSColumnExtPoint() {
        return createStudentSearchListColumnsBuilder()
        .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchListDSHandler() {
        return new EppIndicatorStudentsSlotSearchListDSHandler(getName());
    }

    @Override
    protected List<ColumnBase> prepareColumnList() {
        List<ColumnBase> columnList = super.prepareColumnList();
        columnList.add(textColumn("slotList", EppIndicatorStudentsSlotSearchListDSHandler.PROP_SLOT_LIST).formatter(UniEppUtils.NEW_LINE_FORMATTER).required(true).width("80").create());
        return columnList;
    }
}
