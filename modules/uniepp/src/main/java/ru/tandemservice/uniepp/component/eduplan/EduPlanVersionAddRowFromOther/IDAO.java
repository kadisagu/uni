/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddRowFromOther;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author nkokorina
 * Created on: 20.09.2010
 */
public interface IDAO extends IPrepareable<Model>
{

    void prepareListDataSource(Model model);

    void update(Model model);

}
