package ru.tandemservice.uniepp.component.registry.ModuleRegistry.Pub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.component.base.SimpleOrgUnitBasedModel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction;

import java.util.Map;

/**
 * @author iolshvang
 * @since 03.08.11 19:09
 */
@State({
    @Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding="module.id"),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
@Output({
    @Bind(key = "eppRegistryElementSec", binding = "sec")
})
public class Model extends SimpleOrgUnitBasedModel
{
    private EppRegistryModule _module = new EppRegistryModule();
    private String _selectedTab;
    private CommonPostfixPermissionModel _sec = new CommonPostfixPermissionModel(EppRegistryModule.class.getSimpleName());
    private DynamicListDataSource<EppRegistryElementPartModule> _disciplineDataSource;
    private DynamicListDataSource<EppRegistryModuleALoad> _loadTypeDataSource;
    private DynamicListDataSource<EppRegistryModuleIControlAction> _controlActionDataSource;
    private boolean _hasLoad, _hasCA;

    public Class<EppRegistryModule> getElementClass() { return EppRegistryModule.class; }
    public String getPermissionPrefix() { return "eppModule"; }

    public boolean isHasLoad()
    {
        return this._hasLoad;
    }

    public void setHasLoad(final boolean hasLoad)
    {
        this._hasLoad = hasLoad;
    }

    public boolean isHasCA()
    {
        return this._hasCA;
    }

    public void setHasCA(final boolean hasCA)
    {
        this._hasCA = hasCA;
    }

    public Map<String, Object> getInContractListParameters() {
        return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModule().getId());
    }

    public DynamicListDataSource<EppRegistryElementPartModule> getDisciplineDataSource()
    {
        return this._disciplineDataSource;
    }

    public void setDisciplineDataSource(final DynamicListDataSource<EppRegistryElementPartModule> disciplineDataSource)
    {
        this._disciplineDataSource = disciplineDataSource;
    }

    public DynamicListDataSource<EppRegistryModuleALoad> getLoadTypeDataSource()
    {
        return this._loadTypeDataSource;
    }

    public void setLoadTypeDataSource(final DynamicListDataSource<EppRegistryModuleALoad> loadTypeDataSource)
    {
        this._loadTypeDataSource = loadTypeDataSource;
    }

    public DynamicListDataSource<EppRegistryModuleIControlAction> getControlActionDataSource()
    {
        return this._controlActionDataSource;
    }

    public void setControlActionDataSource(final DynamicListDataSource<EppRegistryModuleIControlAction> controlActionDataSource)
    {
        this._controlActionDataSource = controlActionDataSource;
    }

    public CommonPostfixPermissionModel getSec()
    {
        return this._sec;
    }

    public void setSec(final CommonPostfixPermissionModel sec)
    {
        this._sec = sec;
    }

    public EppRegistryModule getModule()
    {
        return this._module;
    }

    public void setModule(final EppRegistryModule module)
    {
        this._module = module;
    }

    public String getSelectedTab()
    {
        return this._selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        this._selectedTab = selectedTab;
    }

    @Override
    public OrgUnit getOrgUnit()
    {
        return (null == this.getModule() ? null : this.getModule().getOwner());
    }
}
