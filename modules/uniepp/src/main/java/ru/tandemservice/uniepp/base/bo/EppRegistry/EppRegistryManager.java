/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.EppRegistryMassSplitDAO;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.IEppRegistryMassSplitDAO;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractList;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 11/8/12
 */
@Configuration
public class EppRegistryManager extends BusinessObjectManager
{
    public static EppRegistryManager instance()
    {
        return instance(EppRegistryManager.class);
    }

    /**
     * Хэндлер для селекта подразделений в реестрах мероприятий
     */
    @Bean
    public IDefaultComboDataSourceHandler ownerDSHandler() {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
            .customize((alias, dql, context, filter) -> dql.where(exists(EppRegistryElement.class, EppRegistryElement.owner().s(), property(alias))))
                .titleProperty(OrgUnit.fullTitle().s())
                .filter(OrgUnit.title())
                .filter(OrgUnit.orgUnitType().title())
                .order(OrgUnit.title());
    }

    @Bean
    public UIDataSourceConfig splitDSConfig()
    {
        return SelectDSConfig.with(EppRegistryAbstractList.EDU_GROUP_SPLIT_VARIANT_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(splitDSHandler())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler splitDSHandler()
    {
        return EppEduGroupSplitVariant.selectWithNoSplitVariantDSHandler(getName()).filtered(false);
    }

    @Bean
    public IEppRegistryMassSplitDAO massSplitDAO()
    {
        return new EppRegistryMassSplitDAO();
    }
}
