/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.student.StudentEduplanTab;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vip_delete
 * @since 24.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setStudent(this.getNotNull(Student.class, model.getStudent().getId()));
        EppStudent2EduPlanVersion s2epv = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(model.getStudent().getId());
        model.setRelation(s2epv);

        if (null == s2epv || !(s2epv.getEduPlanVersion().getEduPlan() instanceof EppEduPlanProf)) {
            model.setErrors(null);
            return;
        }

        EducationOrgUnit eduOu = s2epv.getStudent().getEducationOrgUnit();
        EppEduPlanProf prof = (EppEduPlanProf) s2epv.getEduPlanVersion().getEduPlan();
        EduProgramSubject eduProgramSubject = eduOu.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();
        EduProgramSpecialization eduProgramSpec = eduOu.getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization();

        List<String> errors = new ArrayList<>();

        if (eduProgramSubject == null) {
            errors.add("Для параметров обучения студента не указано направление (специальность, профессия) по обновленному классификатору.");
        } else if (!eduProgramSubject.equals(prof.getProgramSubject())) {
            errors.add("Направление (специальность, профессия) «" + eduProgramSubject.getTitleWithCode() + "» параметров обучения студента не совпадает с указанным в учебном плане: «" + prof.getProgramSubject().getTitleWithCode() + "».");
        }
        if (s2epv.getBlock() == null && eduProgramSpec != null) {
            errors.add("В параметрах обучения студента установлена направленность «" + eduProgramSpec.getDisplayableTitle() + "», но для студента не указан соответствующий ей блок учебного плана.");
        } else if (s2epv.getBlock() != null && s2epv.getBlock() instanceof EppEduPlanVersionRootBlock && eduProgramSpec != null) {
            // пока убираем, потому что у них сейчас по факту куча планов с единственным общим блоком, и непонятно, стоит ли их заставлять создавать в них конкретные блоки
            // errors.add("В параметрах обучения студента установлена направленность «" + eduProgramSpec.getDisplayableTitle() + "», но для студента указан общий блок учебного плана.");
        } else if (s2epv.getBlock() != null && s2epv.getBlock() instanceof EppEduPlanVersionSpecializationBlock && eduProgramSpec != null) {
            EduProgramSpecialization blockSpec = ((EppEduPlanVersionSpecializationBlock) s2epv.getBlock()).getProgramSpecialization();
            if (!eduProgramSpec.equals(blockSpec)) {
                errors.add("Направленность «" + eduProgramSpec.getDisplayableTitle() + "» параметров обучения студента не совпадает с направленностью указанного блока: «" + blockSpec.getDisplayableTitle() + "».");
            }
        }
        if (!eduOu.getDevelopForm().getProgramForm().equals(prof.getProgramForm())) {
            errors.add("Форма обучения «" + eduOu.getDevelopForm().getProgramForm().getTitle() + "» параметров обучения студента не совпадает с указанной в УП: «" + prof.getProgramForm().getTitle() + "».");
        }
        if (!eduOu.getDevelopCondition().equals(prof.getDevelopCondition())) {
            errors.add("Условия освоения «" + eduOu.getDevelopCondition().getTitle() + "» параметров обучения студента не совпадают с указанными в УП: «" + prof.getDevelopCondition().getTitle() + "».");
        }
        if (!ObjectUtils.equals(eduOu.getDevelopTech().getProgramTrait(), prof.getProgramTrait())) {
            errors.add("Технология освоения «" + eduOu.getDevelopTech().getTitle() + "» параметров обучения студента не соответствует указанной в УП особенности реализации ОП: «" + prof.getProgramTraitTitleNullSafe() + "».");
        }
        if (!eduOu.getDevelopPeriod().equals(prof.getDevelopGrid().getDevelopPeriod())) {
            errors.add("Срок обучения «" + eduOu.getDevelopPeriod().getTitle() + "» параметров обучения студента не соответствует сетке УП: «" + prof.getDevelopGrid().getTitle() + "».");
        }
        model.setErrors(StringUtils.trimToNull(NewLineFormatter.SIMPLE.format(StringUtils.join(errors, "\n"))));
    }
}
