package ru.tandemservice.uniepp.component.registry.base.Pub;

import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * 
 * @author nkokorina
 *
 */

public interface IDAO<T extends EppRegistryElement> extends IPrepareable<Model<T>>
{

}
