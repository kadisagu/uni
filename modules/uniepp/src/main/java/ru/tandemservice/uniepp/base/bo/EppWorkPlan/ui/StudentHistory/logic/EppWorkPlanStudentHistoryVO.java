/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.StudentHistory.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

/**
 * @author nvankov
 * @since 10/8/14
 */
public class EppWorkPlanStudentHistoryVO extends DataWrapper
{
    private EppStudent2EduPlanVersion _sepv;
    private EppStudent2WorkPlan _swp;

    public EppWorkPlanStudentHistoryVO(EppStudent2EduPlanVersion sepv, EppStudent2WorkPlan swp)
    {
        if(swp != null) setId(swp.getId());
        else setId(sepv.getId());
        _sepv = sepv;
        _swp = swp;
    }

    public EppStudent2EduPlanVersion getSepv()
    {
        return _sepv;
    }

    public EppStudent2WorkPlan getSwp()
    {
        return _swp;
    }

    public boolean isDeleteSpwDisabled()
    {
        return _swp == null;
    }
}
