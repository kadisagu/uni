package ru.tandemservice.uniepp.entity.std.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.uniepp.entity.catalog.EppSkillType;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Государственный образовательный стандарт (компетенция)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStateEduStandardSkillGen extends EntityBase
 implements INaturalIdentifiable<EppStateEduStandardSkillGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill";
    public static final String ENTITY_NAME = "eppStateEduStandardSkill";
    public static final int VERSION_HASH = -21562180;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";
    public static final String P_CODE = "code";
    public static final String L_SKILL_GROUP = "skillGroup";
    public static final String L_SKILL_TYPE = "skillType";
    public static final String P_COMMENT = "comment";

    private EppStateEduStandard _parent;     // Государственный образовательный стандарт
    private String _code;     // Код
    private EppSkillGroup _skillGroup;     // Группа компетенций
    private EppSkillType _skillType;     // Тип компетенции
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null.
     */
    @NotNull
    public EppStateEduStandard getParent()
    {
        return _parent;
    }

    /**
     * @param parent Государственный образовательный стандарт. Свойство не может быть null.
     */
    public void setParent(EppStateEduStandard parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Код. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Код. Свойство не может быть null.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Группа компетенций. Свойство не может быть null.
     */
    @NotNull
    public EppSkillGroup getSkillGroup()
    {
        return _skillGroup;
    }

    /**
     * @param skillGroup Группа компетенций. Свойство не может быть null.
     */
    public void setSkillGroup(EppSkillGroup skillGroup)
    {
        dirty(_skillGroup, skillGroup);
        _skillGroup = skillGroup;
    }

    /**
     * @return Тип компетенции. Свойство не может быть null.
     */
    @NotNull
    public EppSkillType getSkillType()
    {
        return _skillType;
    }

    /**
     * @param skillType Тип компетенции. Свойство не может быть null.
     */
    public void setSkillType(EppSkillType skillType)
    {
        dirty(_skillType, skillType);
        _skillType = skillType;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppStateEduStandardSkillGen)
        {
            if (withNaturalIdProperties)
            {
                setParent(((EppStateEduStandardSkill)another).getParent());
                setCode(((EppStateEduStandardSkill)another).getCode());
            }
            setSkillGroup(((EppStateEduStandardSkill)another).getSkillGroup());
            setSkillType(((EppStateEduStandardSkill)another).getSkillType());
            setComment(((EppStateEduStandardSkill)another).getComment());
        }
    }

    public INaturalId<EppStateEduStandardSkillGen> getNaturalId()
    {
        return new NaturalId(getParent(), getCode());
    }

    public static class NaturalId extends NaturalIdBase<EppStateEduStandardSkillGen>
    {
        private static final String PROXY_NAME = "EppStateEduStandardSkillNaturalProxy";

        private Long _parent;
        private String _code;

        public NaturalId()
        {}

        public NaturalId(EppStateEduStandard parent, String code)
        {
            _parent = ((IEntity) parent).getId();
            _code = code;
        }

        public Long getParent()
        {
            return _parent;
        }

        public void setParent(Long parent)
        {
            _parent = parent;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppStateEduStandardSkillGen.NaturalId) ) return false;

            EppStateEduStandardSkillGen.NaturalId that = (NaturalId) o;

            if( !equals(getParent(), that.getParent()) ) return false;
            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getParent());
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getParent());
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStateEduStandardSkillGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStateEduStandardSkill.class;
        }

        public T newInstance()
        {
            return (T) new EppStateEduStandardSkill();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "parent":
                    return obj.getParent();
                case "code":
                    return obj.getCode();
                case "skillGroup":
                    return obj.getSkillGroup();
                case "skillType":
                    return obj.getSkillType();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "parent":
                    obj.setParent((EppStateEduStandard) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "skillGroup":
                    obj.setSkillGroup((EppSkillGroup) value);
                    return;
                case "skillType":
                    obj.setSkillType((EppSkillType) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "parent":
                        return true;
                case "code":
                        return true;
                case "skillGroup":
                        return true;
                case "skillType":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "parent":
                    return true;
                case "code":
                    return true;
                case "skillGroup":
                    return true;
                case "skillType":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "parent":
                    return EppStateEduStandard.class;
                case "code":
                    return String.class;
                case "skillGroup":
                    return EppSkillGroup.class;
                case "skillType":
                    return EppSkillType.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStateEduStandardSkill> _dslPath = new Path<EppStateEduStandardSkill>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStateEduStandardSkill");
    }
            

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill#getParent()
     */
    public static EppStateEduStandard.Path<EppStateEduStandard> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Код. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Группа компетенций. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill#getSkillGroup()
     */
    public static EppSkillGroup.Path<EppSkillGroup> skillGroup()
    {
        return _dslPath.skillGroup();
    }

    /**
     * @return Тип компетенции. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill#getSkillType()
     */
    public static EppSkillType.Path<EppSkillType> skillType()
    {
        return _dslPath.skillType();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends EppStateEduStandardSkill> extends EntityPath<E>
    {
        private EppStateEduStandard.Path<EppStateEduStandard> _parent;
        private PropertyPath<String> _code;
        private EppSkillGroup.Path<EppSkillGroup> _skillGroup;
        private EppSkillType.Path<EppSkillType> _skillType;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill#getParent()
     */
        public EppStateEduStandard.Path<EppStateEduStandard> parent()
        {
            if(_parent == null )
                _parent = new EppStateEduStandard.Path<EppStateEduStandard>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Код. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EppStateEduStandardSkillGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Группа компетенций. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill#getSkillGroup()
     */
        public EppSkillGroup.Path<EppSkillGroup> skillGroup()
        {
            if(_skillGroup == null )
                _skillGroup = new EppSkillGroup.Path<EppSkillGroup>(L_SKILL_GROUP, this);
            return _skillGroup;
        }

    /**
     * @return Тип компетенции. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill#getSkillType()
     */
        public EppSkillType.Path<EppSkillType> skillType()
        {
            if(_skillType == null )
                _skillType = new EppSkillType.Path<EppSkillType>(L_SKILL_TYPE, this);
            return _skillType;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EppStateEduStandardSkillGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return EppStateEduStandardSkill.class;
        }

        public String getEntityName()
        {
            return "eppStateEduStandardSkill";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
