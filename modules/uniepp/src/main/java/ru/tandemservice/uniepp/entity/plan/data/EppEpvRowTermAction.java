package ru.tandemservice.uniepp.entity.plan.data;

import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvRowTermActionGen;

/**
 * Нагрузка строки версии УП (по видам контрольных мероприятий)
 */
public class EppEpvRowTermAction extends EppEpvRowTermActionGen implements IEppEpvRowTermDetail
{
    public EppEpvRowTermAction()
    {
    }

    public EppEpvRowTermAction(EppEpvRowTerm rowTerm, EppControlActionType controlActionType)
    {
        setRowTerm(rowTerm);
        setControlActionType(controlActionType);
    }

    public EppEpvRowTermAction(EppEpvRowTerm rowTerm, EppControlActionType actionType, Integer value)
    {
        setRowTerm(rowTerm);
        setControlActionType(actionType);
        setValueAsInteger(value);
    }

    @Override public String getRelationTargetCode() {
        return this.getControlActionType().getFullCode();
    }

    public int getValueAsInteger() { return this.getSize(); }
    public void setValueAsInteger(final int value) { this.setSize(value); }

    public boolean getValueAsBoolean() { return (this.getValueAsInteger()>0); }
    public void setValueAsBoolean(final boolean value) { this.setValueAsInteger(value ? 1 : 0); }

}