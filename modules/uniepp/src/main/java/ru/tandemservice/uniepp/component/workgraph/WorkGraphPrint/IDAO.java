package ru.tandemservice.uniepp.component.workgraph.WorkGraphPrint;

import jxl.write.WriteException;
import org.tandemframework.core.document.IDocumentRenderer;

import ru.tandemservice.uni.dao.IUpdateable;

import java.io.IOException;

/**
 * @author nkokorina
 */

public interface IDAO extends IUpdateable<Model>
{
    IDocumentRenderer createDocument(Model model) throws IOException, WriteException;

}
