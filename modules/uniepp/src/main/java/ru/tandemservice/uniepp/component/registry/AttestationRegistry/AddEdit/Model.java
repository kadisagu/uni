package ru.tandemservice.uniepp.component.registry.AttestationRegistry.AddEdit;

import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;

/**
 * @author vdanilov
 */
public class Model extends ru.tandemservice.uniepp.component.registry.base.AddEdit.Model<EppRegistryAttestation> {
    @Override public Class<EppRegistryAttestation> getElementClass() {
        return EppRegistryAttestation.class;
    }

    @Override
    public void setupElementDefaultLoad() {
        super.setupElementDefaultLoad();
        this.getElement().setWeeksAsDouble(this.getDefaultLoadMap().get(EppLoadType.FULL_CODE_WEEKS));
    }

}
