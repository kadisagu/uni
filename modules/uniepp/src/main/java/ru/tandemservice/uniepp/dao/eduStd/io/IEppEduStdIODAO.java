package ru.tandemservice.uniepp.dao.eduStd.io;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.util.cache.SpringBeanCache;

import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineComment;
import ru.tandemservice.uniepp.entity.std.data.EppStdRow2SkillRelation;
import ru.tandemservice.uniepp.entity.std.data.gen.EppStdRow2SkillRelationGen;

public interface IEppEduStdIODAO
{
    public static final SpringBeanCache<IEppEduStdIODAO> instance = new SpringBeanCache<IEppEduStdIODAO>(IEppEduStdIODAO.class.getName());

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public byte[] export(Collection<Long> eduStdIds);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<EppStateEduStandardBlock> getEduStdBlockList(Long eduStdId);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
    public boolean doImportEduStdFromFile(Long eduStdId, byte[] content, boolean ignoreErrors);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<String, EppStdDisciplineComment> getEduStdDisciplineCommentMap(Collection<Long> rowIds);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<INaturalId<EppStdRow2SkillRelationGen>, EppStdRow2SkillRelation> getEduStdRow2SkillRel(Collection<Long> rowIds);
}
