package ru.tandemservice.uniepp.dao.registry.data;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppModuleStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;

/**
 * @author vdanilov
 */
public interface IEppRegElPartModuleWrapper extends IEppBaseRegElWrapper<EppRegistryElementPartModule> {

    /** @return модуль в части элемента реестра */
    @Override
    EppRegistryElementPartModule getItem();

    /** @return разделяемый ли модуль */
    boolean shared();

    /** @return структура */
    EppModuleStructure getModuleParent();

    /** @return Читающее подразделение */
    OrgUnit getModuleOwner();

    /** @return Номер модуля в части */
    int getNumber();
}
