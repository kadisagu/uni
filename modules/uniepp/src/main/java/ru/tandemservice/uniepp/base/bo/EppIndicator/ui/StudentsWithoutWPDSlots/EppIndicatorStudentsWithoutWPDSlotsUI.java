/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithoutWPDSlots;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListPresenter;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
@State ({
    @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
public class EppIndicatorStudentsWithoutWPDSlotsUI extends AbstractEppIndicatorStudentListPresenter
{
    public static final String PROP_YEAR_PART = "yearPart";

    private List<HSelectOption> _partsList;

    @Override
    public String getSettingsKey()
    {
        return "epp.StudentsWithoutWPDSlots.filter";
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _partsList = HierarchyUtil.listHierarchyNodesWithParents(DataAccessServices.dao().getList(YearDistributionPart.class), false);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(PROP_YEAR_PART, getSettings().get(PROP_YEAR_PART));
    }

    // Getters & Setters

    public List<HSelectOption> getPartsList()
    {
        return _partsList;
    }

    public void setPartsList(List<HSelectOption> partsList)
    {
        _partsList = partsList;
    }
}
