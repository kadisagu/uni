package ru.tandemservice.uniepp.component.workgraph.WorkGraphPrint;

import jxl.write.WriteException;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.year.IEppYearDAO;
import ru.tandemservice.uniepp.dao.year.IEppYearWorkGraphDAO;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

/**
 * @author nkokorina
 */

public class DAO extends UniDao<Model> implements IDAO
{
    private static final String PRINT_TYPE_COURSE = "onCourses";
    private static final String PRINT_TYPE_SPECIALITIES = "onSpecialities";

    public static final Comparator<EppWorkGraphRowWeek> weekNumberComparator = (o1, o2) -> {
        final Integer num1 = o1.getWeek();
        final Integer num2 = o2.getWeek();

        return num1.compareTo(num2);
    };

    @SuppressWarnings("unchecked")
    @Override
    public void prepare(final Model model)
    {
        final WorkGraphPrintVersion printVersion = model.getPrintVersion();

        // ОУ
        final TopOrgUnit academy = TopOrgUnit.getInstance();
        // данные о названии и руководителе ОУ
        printVersion.setAcademyTitle(academy.getShortTitle());
        printVersion.setAcademyHead((EmployeePost)academy.getHead());

        // учебный график
        printVersion.setWorkGraph(this.getNotNull(EppWorkGraph.class, model.getId()));
        // учебные недели
        printVersion.setWeeks(IEppYearDAO.instance.get().getYearEducationWeeks(printVersion.getWorkGraph().getYear().getId()));

        final Long developGridId = printVersion.getWorkGraph().getDevelopGrid().getId();
        //{ term.number -> DevelopGridTerm (term, course, part) } }
        final Map<Integer, DevelopGridTerm> termNum2grid = IDevelopGridDAO.instance.get().getDevelopGridMap(developGridId);

        // данные по неделям по всем специальностям
        final Map<EduProgramSubject, Map<EppWorkGraphRow, Set<EppWorkGraphRowWeek>>> dataMap = IEppYearWorkGraphDAO.instance.get().getWorkGraphRowMapGroupByProgramSubject(printVersion.getWorkGraph(), null, null);

        // id недели -> номер для печати
        final Map<Long, Integer> idWeek2PrintNumber = this.fillPrintWeeksNumber(termNum2grid, dataMap);

        // id таблицы(специаль) -> название
        final Map<Long, String> id2tableTitle = new LinkedHashMap<>();

        // id таблицы(специаль) -> { id курса -> упорядоченный по номеру список недель}
        final Map<Long, Map<Long, List<String>>> id2idRow2weeksList = SafeMap.get(LinkedHashMap.class);
        // { id курса -> название}
        final Map<Long, String> idRow2title = new HashMap<>();

        // id таблицы(специаль) -> { id курса -> название}
        //Map<Long, Map<Long, String>> id2idRow2title = SafeMap.get(LinkedHashMap.class);
        // id курса -> упорядоченный по номеру список недель
        //Map<Long, List<String>> idRow2weeksList = SafeMap.get(ArrayList.class);

        // сокращение имца для типа недели - цвет
        final Map<String, Long> weekType2color = new HashMap<>();
        // сокращение для типа недели - полное название
        final Map<String, String> weekType2fullTitle = new HashMap<>();

        // заполняем мапы в зависимоти от типа печати
        if (DAO.PRINT_TYPE_SPECIALITIES.equals(model.getPrintType()))
        {
            for (final EduProgramSubject level : dataMap.keySet())
            {
                id2tableTitle.put(level.getId(), level.getTitleWithCode());
                for (final EppWorkGraphRow graphRow : dataMap.get(level).keySet())
                {
                    final Long courseId = graphRow.getCourse().getId();
                    id2idRow2weeksList.get(level.getId()).put(courseId, new ArrayList<>());
                    idRow2title.put(courseId, graphRow.getCourse().getTitle() + " курс");

                    final List<String> row = id2idRow2weeksList.get(level.getId()).get(courseId);
                    if (row.isEmpty())
                    {
                        final Set<EppWorkGraphRowWeek> rowSet = dataMap.get(level).get(graphRow);
                        final List<EppWorkGraphRowWeek> weekList = new ArrayList<>(rowSet);
                        Collections.sort(weekList, DAO.weekNumberComparator);

                        int currentWeekNumber = 0;
                        for (final EppWorkGraphRowWeek week : weekList)
                        {
                            currentWeekNumber++;
                            while (currentWeekNumber < week.getWeek()) {
                                row.add("");
                                currentWeekNumber++;
                            }

                            final String weekTitle = this.getPrintWeekTitle(idWeek2PrintNumber, week);
                            row.add(weekTitle);

                            if (!EppWeekType.CODE_THEORY.equals(week.getType().getCode()))
                            {
                                weekType2color.put(week.getType().getAbbreviationView(), week.getType().getColor());
                                weekType2fullTitle.put(week.getType().getAbbreviationView(), week.getType().getTitle());
                            }
                        }
                    }
                }
            }
            this.fillMapsPrintVersion(printVersion, id2tableTitle, id2idRow2weeksList, idRow2title, weekType2color, weekType2fullTitle);
            return;
        }


        if (DAO.PRINT_TYPE_COURSE.equals(model.getPrintType()))
        {
            for (final Entry<EduProgramSubject, Map<EppWorkGraphRow, Set<EppWorkGraphRowWeek>>> entry : dataMap.entrySet())
            {
                final EduProgramSubject subject = entry.getKey();
                for (final Entry<EppWorkGraphRow, Set<EppWorkGraphRowWeek>> entrySub : entry.getValue().entrySet())
                {
                    final EppWorkGraphRow graphRow = entrySub.getKey();
                    final Long courseId = graphRow.getCourse().getId();
                    id2idRow2weeksList.get(courseId).put(subject.getId(), new ArrayList<>());
                    idRow2title.put(subject.getId(), subject.getShortTitle());
                    id2tableTitle.put(courseId, graphRow.getCourse().getTitle() + " курс");

                    final List<String> row = id2idRow2weeksList.get(courseId).get(subject.getId());
                    if (row.isEmpty())
                    {
                        final Set<EppWorkGraphRowWeek> rowSet = dataMap.get(subject).get(graphRow);
                        final List<EppWorkGraphRowWeek> weekList = new ArrayList<>(rowSet);
                        Collections.sort(weekList, DAO.weekNumberComparator);

                        int currentWeekNumber = 0;
                        for (final EppWorkGraphRowWeek week : weekList)
                        {
                            currentWeekNumber++;
                            while (currentWeekNumber < week.getWeek()) {
                                row.add("");
                                currentWeekNumber++;
                            }

                            final String weekTitle = this.getPrintWeekTitle(idWeek2PrintNumber, week);
                            row.add(weekTitle);

                            if (!EppWeekType.CODE_THEORY.equals(week.getType().getCode()))
                            {
                                weekType2color.put(week.getType().getAbbreviationView(), week.getType().getColor());
                                weekType2fullTitle.put(week.getType().getAbbreviationView(), week.getType().getTitle());
                            }
                        }
                    }
                }
            }
            this.fillMapsPrintVersion(printVersion, id2tableTitle, id2idRow2weeksList, idRow2title, weekType2color, weekType2fullTitle);
        }
    }

    private String getPrintWeekTitle(final Map<Long, Integer> idWeek2PrintNumber, final EppWorkGraphRowWeek week)
    {
        if (idWeek2PrintNumber.containsKey(week.getId())) {
            return String.valueOf(idWeek2PrintNumber.get(week.getId()).intValue());
        }
        if (EppWeekType.CODE_NOT_USED.equals(week.getType().getCode())) {
            return "";
        }
        return week.getType().getAbbreviationView();
    }

    @SuppressWarnings("unchecked")
    private Map<Long, Integer> fillPrintWeeksNumber(final Map<Integer, DevelopGridTerm> termNum2grid, final Map<EduProgramSubject, Map<EppWorkGraphRow, Set<EppWorkGraphRowWeek>>> dataMap)
    {
        final Map<Long, Integer> idWeek2PrintNumber = new HashMap<>();
        for (final Map<EppWorkGraphRow, Set<EppWorkGraphRowWeek>> value : dataMap.values())
        {
            for (final Entry<EppWorkGraphRow, Set<EppWorkGraphRowWeek>> entry : value.entrySet())
            {
                final Set<EppWorkGraphRowWeek> weeks = entry.getValue();
                for (final Integer term : termNum2grid.keySet())
                {
                    final Collection<EppWorkGraphRowWeek> sortedWeeks = CollectionUtils.select(weeks, object -> {
                        final EppWorkGraphRowWeek week = (EppWorkGraphRowWeek)object;
                        return (week.getTerm().getIntValue() == term) && (EppWeekType.CODE_THEORY.equals((week.getType().getCode())));
                    });
                    final List<EppWorkGraphRowWeek> weekList = new ArrayList<>(sortedWeeks);
                    Collections.sort(weekList, DAO.weekNumberComparator);
                    for (int i = 0; i < weekList.size(); i++)
                    {
                        idWeek2PrintNumber.put(weekList.get(i).getId(), i + 1);
                    }
                }

            }
        }
        return idWeek2PrintNumber;
    }

    private void fillMapsPrintVersion(final WorkGraphPrintVersion printVersion, final Map<Long, String> id2tableTitle, final Map<Long, Map<Long, List<String>>> id2idRow2weeksList, final Map<Long, String> idRow2title, final Map<String, Long> weekType2color, final Map<String, String> weekType2fullTitle)
    {
        printVersion.setId2tableTitle(id2tableTitle);
        printVersion.setId2idRow2weeksList(id2idRow2weeksList);
        printVersion.setIdRow2title(idRow2title);
        printVersion.setWeekType2color(weekType2color);
        printVersion.setWeekType2fullTitle(weekType2fullTitle);
    }

    @Override
    public IDocumentRenderer createDocument(final Model model) throws IOException, WriteException
    {
        return new CommonBaseRenderer().xls().fileName("gup.xls")
        .document(new WorkGraphReportContentGenerator().generateReportContent(model.getPrintVersion()));
    }
}
