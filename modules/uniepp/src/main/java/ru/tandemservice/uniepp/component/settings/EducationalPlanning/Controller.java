package ru.tandemservice.uniepp.component.settings.EducationalPlanning;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.IUnieppComponents;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;

/**
 * 
 * @author nkokorina
 * @since 26.02.2010
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.prepareListDateSource(component);
    }

    private void prepareListDateSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        final DynamicListDataSource<IEntity> dataSource = UniBaseUtils.createDataSource(component, this.getDao());
        dataSource.addColumn(new SimpleColumn("Название", EppYearEducationProcess.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Учебный год", EppYearEducationProcess.educationYear().title()));
        dataSource.addColumn(new SimpleColumn("Начало учебного года", EppYearEducationProcess.startEducationDate(), DateFormatter.DEFAULT_DATE_FORMATTER));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditEducationalPlanning"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteEducationalPlanning", "Удалить {0}?", EppYearEducationProcess.P_TITLE));

        model.setDataSource(dataSource);
    }

    public void onClickAddEducationalPlanning(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUnieppComponents.EDUCATIONAL_PLANNING_ADD_EDIT, ParametersMap.createWith("educationProcessId", null)));
    }

    public void onClickEditEducationalPlanning(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUnieppComponents.EDUCATIONAL_PLANNING_ADD_EDIT, ParametersMap.createWith("educationProcessId", component.getListenerParameter())));
    }

    public void onClickDeleteEducationalPlanning(final IBusinessComponent component)
    {
        this.getDao().deleteRow(component);
    }
}
