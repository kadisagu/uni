package ru.tandemservice.uniepp.entity.student.group.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись студента в УГС (по форме контроля)
 *
 * Связь записи студента с УГС-по-ФИК
 * @warn ссылки на этот объект должны быть автоудаляемыми (т.к. неактуальная запись может быть удалена в любое время демоном)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRealEduGroup4ActionTypeRowGen extends EppRealEduGroupRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow";
    public static final String ENTITY_NAME = "eppRealEduGroup4ActionTypeRow";
    public static final int VERSION_HASH = 1029186002;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppRealEduGroup4ActionTypeRowGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRealEduGroup4ActionTypeRowGen> extends EppRealEduGroupRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRealEduGroup4ActionTypeRow.class;
        }

        public T newInstance()
        {
            return (T) new EppRealEduGroup4ActionTypeRow();
        }
    }
    private static final Path<EppRealEduGroup4ActionTypeRow> _dslPath = new Path<EppRealEduGroup4ActionTypeRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRealEduGroup4ActionTypeRow");
    }
            

    public static class Path<E extends EppRealEduGroup4ActionTypeRow> extends EppRealEduGroupRow.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EppRealEduGroup4ActionTypeRow.class;
        }

        public String getEntityName()
        {
            return "eppRealEduGroup4ActionTypeRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
