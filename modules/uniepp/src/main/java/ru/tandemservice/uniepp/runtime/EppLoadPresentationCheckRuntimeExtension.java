/* $Id:$ */
package ru.tandemservice.uniepp.runtime;

import org.tandemframework.core.runtime.IRuntimeExtension;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author oleyba
 * @since 11/18/14
 */
public class EppLoadPresentationCheckRuntimeExtension implements IRuntimeExtension
{
    @Override
    public void init(Object object)
    {
        int count = DataAccessServices.dao().getCount(EppEduPlanVersion.class, EppEduPlanVersion.loadPresentationInWeeks().s(), Boolean.TRUE);
        if (0 < count) {
            throw new IllegalStateException("Eduplans with load presentation in weeks are found (" + count + "), application cannot be started without a migration.");
        }
    }

    @Override
    public void destroy()
    {
    }
}
