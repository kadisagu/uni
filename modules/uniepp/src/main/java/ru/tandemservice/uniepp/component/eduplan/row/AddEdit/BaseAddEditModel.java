package ru.tandemservice.uniepp.component.eduplan.row.AddEdit;

import org.springframework.util.ClassUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;

import java.util.Collections;
import java.util.List;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id")
})
public abstract class BaseAddEditModel<T extends EppEpvRow> {
    protected abstract T newInstance(EppEduPlanVersionBlock block);

    protected abstract T check(T entity);

    private final EntityHolder<T> rowHolder = new EntityHolder<>();
    public EntityHolder<T> getRowHolder() { return this.rowHolder; }
    public T getRow() { return this.getRowHolder().getValue(); }

    private Long templateId;
    public Long getTemplateId() { return this.templateId; }

    public EppEduPlanVersionBlock getBlock() { return this.getRow().getOwner(); }

    private List<HSelectOption> parentList = Collections.emptyList();
    public List<HSelectOption> getParentList() { return this.parentList; }
    public void setParentList(final List<HSelectOption> parentList) { this.parentList = parentList; }

    public IEppEpvRowWrapper getParent() {
        final IEppEpvRow parent = this.getRow().getHierarhyParent();
        if (null == parent) { return null; }
        for (final HSelectOption option: this.parentList) {
            if (parent.equals(option.getObject())) { return (IEppEpvRowWrapper) option.getObject(); }
        }
        return null;
    }

    public void setParent(final IEppEpvRowWrapper parent) {
        this.getRow().setHierarhyParent((EppEpvRow)(null == parent ? null : parent.getRow()));
    }


    @SuppressWarnings("unchecked")
    public void setId(final Long id) {
        final T value = IUniBaseDao.instance.get().getCalculatedValue(session -> {
            final IEntity entity = IUniBaseDao.instance.get().getNotNull(id);
            if (entity instanceof EppEduPlanVersionBlock) {
                BaseAddEditModel.this.rowHolder.setValue(BaseAddEditModel.this.newInstance((EppEduPlanVersionBlock) entity));
                BaseAddEditModel.this.templateId = null;

            } else if (entity instanceof EppEpvRow) {
                BaseAddEditModel.this.rowHolder.setValue(BaseAddEditModel.this.check((T) entity));
                BaseAddEditModel.this.templateId = null;

            } else {
                throw new IllegalArgumentException(ClassUtils.getUserClass(entity.getClass()).getName());
            }

            final EppEduPlanVersion eduPlanVersion = BaseAddEditModel.this.rowHolder.getValue().getOwner().getEduPlanVersion();
            eduPlanVersion.getState().check_editable(eduPlanVersion);

            return BaseAddEditModel.this.rowHolder.getValue();
        });

        if (null == value) {
            throw new IllegalStateException();
        }
    }

}
