package ru.tandemservice.uniepp.entity.pupnag.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ГУП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkGraphGen extends EntityBase
 implements INaturalIdentifiable<EppWorkGraphGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph";
    public static final String ENTITY_NAME = "eppWorkGraph";
    public static final int VERSION_HASH = 1499790758;
    private static IEntityMeta ENTITY_META;

    public static final String L_YEAR = "year";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_DEVELOP_TECH = "developTech";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_DEVELOP_GRID = "developGrid";
    public static final String L_STATE = "state";
    public static final String P_CONFIRM_DATE = "confirmDate";
    public static final String P_TITLE = "title";

    private EppYearEducationProcess _year;     // ПУПнаГ
    private DevelopForm _developForm;     // Форма освоения
    private DevelopTech _developTech;     // Технология освоения
    private DevelopCondition _developCondition;     // Условие освоения
    private DevelopGrid _developGrid;     // Учебная сетка
    private EppState _state;     // Состояние
    private Date _confirmDate;     // Дата утверждения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     */
    @NotNull
    public EppYearEducationProcess getYear()
    {
        return _year;
    }

    /**
     * @param year ПУПнаГ. Свойство не может быть null.
     */
    public void setYear(EppYearEducationProcess year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения. Свойство не может быть null.
     */
    public void setDevelopTech(DevelopTech developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения. Свойство не может быть null.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Учебная сетка. Свойство не может быть null.
     */
    @NotNull
    public DevelopGrid getDevelopGrid()
    {
        return _developGrid;
    }

    /**
     * @param developGrid Учебная сетка. Свойство не может быть null.
     */
    public void setDevelopGrid(DevelopGrid developGrid)
    {
        dirty(_developGrid, developGrid);
        _developGrid = developGrid;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public EppState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(EppState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Дата утверждения.
     */
    public Date getConfirmDate()
    {
        return _confirmDate;
    }

    /**
     * @param confirmDate Дата утверждения.
     */
    public void setConfirmDate(Date confirmDate)
    {
        dirty(_confirmDate, confirmDate);
        _confirmDate = confirmDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppWorkGraphGen)
        {
            if (withNaturalIdProperties)
            {
                setYear(((EppWorkGraph)another).getYear());
                setDevelopForm(((EppWorkGraph)another).getDevelopForm());
                setDevelopTech(((EppWorkGraph)another).getDevelopTech());
                setDevelopCondition(((EppWorkGraph)another).getDevelopCondition());
                setDevelopGrid(((EppWorkGraph)another).getDevelopGrid());
            }
            setState(((EppWorkGraph)another).getState());
            setConfirmDate(((EppWorkGraph)another).getConfirmDate());
        }
    }

    public INaturalId<EppWorkGraphGen> getNaturalId()
    {
        return new NaturalId(getYear(), getDevelopForm(), getDevelopTech(), getDevelopCondition(), getDevelopGrid());
    }

    public static class NaturalId extends NaturalIdBase<EppWorkGraphGen>
    {
        private static final String PROXY_NAME = "EppWorkGraphNaturalProxy";

        private Long _year;
        private Long _developForm;
        private Long _developTech;
        private Long _developCondition;
        private Long _developGrid;

        public NaturalId()
        {}

        public NaturalId(EppYearEducationProcess year, DevelopForm developForm, DevelopTech developTech, DevelopCondition developCondition, DevelopGrid developGrid)
        {
            _year = ((IEntity) year).getId();
            _developForm = ((IEntity) developForm).getId();
            _developTech = ((IEntity) developTech).getId();
            _developCondition = ((IEntity) developCondition).getId();
            _developGrid = ((IEntity) developGrid).getId();
        }

        public Long getYear()
        {
            return _year;
        }

        public void setYear(Long year)
        {
            _year = year;
        }

        public Long getDevelopForm()
        {
            return _developForm;
        }

        public void setDevelopForm(Long developForm)
        {
            _developForm = developForm;
        }

        public Long getDevelopTech()
        {
            return _developTech;
        }

        public void setDevelopTech(Long developTech)
        {
            _developTech = developTech;
        }

        public Long getDevelopCondition()
        {
            return _developCondition;
        }

        public void setDevelopCondition(Long developCondition)
        {
            _developCondition = developCondition;
        }

        public Long getDevelopGrid()
        {
            return _developGrid;
        }

        public void setDevelopGrid(Long developGrid)
        {
            _developGrid = developGrid;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppWorkGraphGen.NaturalId) ) return false;

            EppWorkGraphGen.NaturalId that = (NaturalId) o;

            if( !equals(getYear(), that.getYear()) ) return false;
            if( !equals(getDevelopForm(), that.getDevelopForm()) ) return false;
            if( !equals(getDevelopTech(), that.getDevelopTech()) ) return false;
            if( !equals(getDevelopCondition(), that.getDevelopCondition()) ) return false;
            if( !equals(getDevelopGrid(), that.getDevelopGrid()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getYear());
            result = hashCode(result, getDevelopForm());
            result = hashCode(result, getDevelopTech());
            result = hashCode(result, getDevelopCondition());
            result = hashCode(result, getDevelopGrid());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getYear());
            sb.append("/");
            sb.append(getDevelopForm());
            sb.append("/");
            sb.append(getDevelopTech());
            sb.append("/");
            sb.append(getDevelopCondition());
            sb.append("/");
            sb.append(getDevelopGrid());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkGraphGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkGraph.class;
        }

        public T newInstance()
        {
            return (T) new EppWorkGraph();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "year":
                    return obj.getYear();
                case "developForm":
                    return obj.getDevelopForm();
                case "developTech":
                    return obj.getDevelopTech();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developGrid":
                    return obj.getDevelopGrid();
                case "state":
                    return obj.getState();
                case "confirmDate":
                    return obj.getConfirmDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "year":
                    obj.setYear((EppYearEducationProcess) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((DevelopTech) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "developGrid":
                    obj.setDevelopGrid((DevelopGrid) value);
                    return;
                case "state":
                    obj.setState((EppState) value);
                    return;
                case "confirmDate":
                    obj.setConfirmDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "year":
                        return true;
                case "developForm":
                        return true;
                case "developTech":
                        return true;
                case "developCondition":
                        return true;
                case "developGrid":
                        return true;
                case "state":
                        return true;
                case "confirmDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "year":
                    return true;
                case "developForm":
                    return true;
                case "developTech":
                    return true;
                case "developCondition":
                    return true;
                case "developGrid":
                    return true;
                case "state":
                    return true;
                case "confirmDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "year":
                    return EppYearEducationProcess.class;
                case "developForm":
                    return DevelopForm.class;
                case "developTech":
                    return DevelopTech.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "developGrid":
                    return DevelopGrid.class;
                case "state":
                    return EppState.class;
                case "confirmDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkGraph> _dslPath = new Path<EppWorkGraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkGraph");
    }
            

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getYear()
     */
    public static EppYearEducationProcess.Path<EppYearEducationProcess> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getDevelopTech()
     */
    public static DevelopTech.Path<DevelopTech> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Учебная сетка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getDevelopGrid()
     */
    public static DevelopGrid.Path<DevelopGrid> developGrid()
    {
        return _dslPath.developGrid();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getState()
     */
    public static EppState.Path<EppState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getConfirmDate()
     */
    public static PropertyPath<Date> confirmDate()
    {
        return _dslPath.confirmDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EppWorkGraph> extends EntityPath<E>
    {
        private EppYearEducationProcess.Path<EppYearEducationProcess> _year;
        private DevelopForm.Path<DevelopForm> _developForm;
        private DevelopTech.Path<DevelopTech> _developTech;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private DevelopGrid.Path<DevelopGrid> _developGrid;
        private EppState.Path<EppState> _state;
        private PropertyPath<Date> _confirmDate;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ПУПнаГ. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getYear()
     */
        public EppYearEducationProcess.Path<EppYearEducationProcess> year()
        {
            if(_year == null )
                _year = new EppYearEducationProcess.Path<EppYearEducationProcess>(L_YEAR, this);
            return _year;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getDevelopTech()
     */
        public DevelopTech.Path<DevelopTech> developTech()
        {
            if(_developTech == null )
                _developTech = new DevelopTech.Path<DevelopTech>(L_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Учебная сетка. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getDevelopGrid()
     */
        public DevelopGrid.Path<DevelopGrid> developGrid()
        {
            if(_developGrid == null )
                _developGrid = new DevelopGrid.Path<DevelopGrid>(L_DEVELOP_GRID, this);
            return _developGrid;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getState()
     */
        public EppState.Path<EppState> state()
        {
            if(_state == null )
                _state = new EppState.Path<EppState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getConfirmDate()
     */
        public PropertyPath<Date> confirmDate()
        {
            if(_confirmDate == null )
                _confirmDate = new PropertyPath<Date>(EppWorkGraphGen.P_CONFIRM_DATE, this);
            return _confirmDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EppWorkGraphGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EppWorkGraph.class;
        }

        public String getEntityName()
        {
            return "eppWorkGraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
