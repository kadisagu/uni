/* $Id$ */
package ru.tandemservice.uniepp.dao.workplan;

import java.util.Date;

/**
 * Поставщик данных для создания строки рабочего плана
 *
 * @author Nikolay Fedorovskih
 * @since 28.10.2015
 */
public class WorkPlanRowSource
{
    private final int termNumber;
    private final Date dateStart;
    private final Date dateEnd;

    public WorkPlanRowSource(int termNumber, Date dateStart, Date dateEnd)
    {
        this.termNumber = termNumber;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }

    /** @return семестр */
    public int getTermNumber() {
        return this.termNumber;
    }

    /** @return дата начала мероприятия (для доп. мероприятий: практики и ГИА) */
    public Date getDateStart() {
        return this.dateStart;
    }

    /** @return дата ококнчания мероприятия (для доп. мероприятий: практики и ГИА) */
    public Date getDateEnd() {
        return this.dateEnd;
    }

}