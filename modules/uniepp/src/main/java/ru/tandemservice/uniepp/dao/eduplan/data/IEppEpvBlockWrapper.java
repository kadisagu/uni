package ru.tandemservice.uniepp.dao.eduplan.data;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import java.util.Map;

/**
 * Оболочка для блока версии УП (указанного и корневого)
 * @author vdanilov
 */
public interface IEppEpvBlockWrapper extends IEntity, Comparable<IEppEpvBlockWrapper> {

    /** @return Корневой блок версии УП */
    EppEduPlanVersionBlock getRootBlock();

    /** @return Блок версии УП, для которого производится суммирование нагрузки (корневой или один из дочерних) */
    EppEduPlanVersionBlock getBlock();

    /** @return Версия УП из базы */
    EppEduPlanVersion getVersion();

    /** @return { row.id -> wrapper(row) по всем блокам, отсортированный иерархически } */
    Map<Long, IEppEpvRowWrapper> getRowMap();

    /** полная нагрузка (всего недель обучения) */
    String ELOAD_FULL_CODE_ALL = "";

    /** @return { eLoadType.fullCode, term.number -> число недель } */
    int getTermSize(String eLoadFullCode, int term);

    /**
     * @param workplanRow - строка РУП
     * @return строка УП, на основании которой, предположительно, была создана строка РУП
     */
    IEppEpvRowWrapper findSuitableRow(EppWorkPlanRegistryElementRow workplanRow);
}
