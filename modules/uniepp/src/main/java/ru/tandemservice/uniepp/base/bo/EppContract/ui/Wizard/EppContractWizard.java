package ru.tandemservice.uniepp.base.bo.EppContract.ui.Wizard;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.config.itemList.IItemListExtPointBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.wizard.ISimpleWizardManager;

/**
 * @author vdanilov
 */
@Configuration
public class EppContractWizard extends BusinessComponentManager implements ISimpleWizardManager
{

    @Bean
    @Override
    @SuppressWarnings("unchecked")
    public ItemListExtPoint<Class<? extends BusinessComponentManager>> getStepList() {
        return ((IItemListExtPointBuilder)this.itemList(Class.class))
        // .add("step.data", CtrContractVersionInWizardDataStep.class)
        // .add("step.contracts", CtrContractVersionStepContractors.class)
        // .add("step.promice.edu", EppCtrEducationPromiceInWizardStep.class)
        // .add("step.promice.pay", CtrPaymentPromiceInWizardStep.class)
        .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

}
