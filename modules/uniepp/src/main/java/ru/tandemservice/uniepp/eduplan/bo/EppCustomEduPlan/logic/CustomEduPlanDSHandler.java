/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.logic;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil.applySelectFilter;

/**
 * @author Nikolay Fedorovskih
 * @since 17.08.2015
 */
public class CustomEduPlanDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String VIEW_PROP_STUDENT = "studentWrapperList";
    public static final String VIEW_PROP_READ_ONLY = "readOnly";

    public static final String PARAM_PROGRAM_KIND = "programKind";
    public static final String PARAM_NUMBER = "planNumber";
    public static final String PARAM_SUBJECT_INDEX = "subjectIndex";
    public static final String PARAM_PROGRAM_SUBJECT = "programSubject";
    public static final String PARAM_PROGRAM_FORM = "programForm";
    public static final String PARAM_DEVELOP_CONDITION = "developCondition";
    public static final String PARAM_PROGRAM_TRAIT = "programTrait";
    public static final String PARAM_DEVELOP_GRID = "developGrid";
    public static final String PARAM_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String PARAM_STATE = "state";
    public static final String PARAM_STUDENT_LAST_NAME = "lastName";

    private static final String customPlanAlias = "customPlan";
    private static final String eduPlanAlias = "plan";

    // Сортировки
    // По номерам УПв/УП/ИУП сортируется пока так (как строки). Это надо исправлять системно.
    private static final OrderDescription planOrder = new OrderDescription(eduPlanAlias, EppEduPlanProf.number());
    private static final OrderDescription versionOrder = new OrderDescription(customPlanAlias, EppCustomEduPlan.epvBlock().eduPlanVersion().number());
    private static final OrderDescription numberOrder = new OrderDescription(customPlanAlias, EppCustomEduPlan.number());

    private static final DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(EppCustomEduPlan.class, customPlanAlias)
            .setOrders(EppCustomEduPlan.title(), planOrder, versionOrder, numberOrder)
            .setOrders(EppEduPlanProf.programSubject().subjectIndex().generation().title(), new OrderDescription(eduPlanAlias, EppEduPlanProf.programSubject().subjectIndex().generation().title()), planOrder, versionOrder, numberOrder)
            .setOrders(EppEduPlanProf.programSubject().titleWithCode(), new OrderDescription(eduPlanAlias, EppEduPlanProf.programSubject().titleWithCode()), planOrder, versionOrder, numberOrder)
            .setOrders(EppEduPlanProf.programQualification().title(), new OrderDescription(eduPlanAlias, EppEduPlanProf.programQualification().title()), planOrder, versionOrder, numberOrder)
            .setOrders(EppCustomEduPlan.state().title(), new OrderDescription(customPlanAlias, EppCustomEduPlan.state().title()), planOrder, versionOrder, numberOrder)
            .setOrders(EppCustomEduPlan.developGrid().title(), new OrderDescription(customPlanAlias, EppCustomEduPlan.developGrid().title()), planOrder, versionOrder, numberOrder);

    public CustomEduPlanDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final Object programKind = context.get(PARAM_PROGRAM_KIND);

        if (programKind == null) {
            return new DSOutput(input);
        }

        final DQLSelectBuilder dql = orderRegistry.buildDQLSelectBuilder();
        dql.column(property(customPlanAlias));
        dql.fetchPath(DQLJoinType.inner, EppCustomEduPlan.epvBlock().fromAlias(customPlanAlias), "block");
        dql.fetchPath(DQLJoinType.inner, EppEduPlanVersionSpecializationBlock.eduPlanVersion().fromAlias("block"), "ver");
        dql.fetchPath(DQLJoinType.inner, EppCustomEduPlan.plan().fromAlias(customPlanAlias), eduPlanAlias);

        applySelectFilter(dql, eduPlanAlias, EppEduPlanProf.programKind(), programKind);
        applySelectFilter(dql, eduPlanAlias, EppEduPlanProf.programSubject().subjectIndex(), context.get(PARAM_SUBJECT_INDEX));
        applySelectFilter(dql, eduPlanAlias, EppEduPlanProf.programSubject(), context.get(PARAM_PROGRAM_SUBJECT));
        applySelectFilter(dql, eduPlanAlias, EppEduPlanProf.programForm(), context.get(PARAM_PROGRAM_FORM));
        applySelectFilter(dql, customPlanAlias, EppCustomEduPlan.developCondition(), context.get(PARAM_DEVELOP_CONDITION));
        applySelectFilter(dql, eduPlanAlias, EppEduPlanProf.programTrait(), context.get(PARAM_PROGRAM_TRAIT));
        applySelectFilter(dql, customPlanAlias, EppCustomEduPlan.developGrid(), context.get(PARAM_DEVELOP_GRID));
        applySelectFilter(dql, customPlanAlias, EppCustomEduPlan.epvBlock().eduPlanVersion(), context.get(PARAM_EDU_PLAN_VERSION));
        applySelectFilter(dql, customPlanAlias, EppCustomEduPlan.state(), context.get(PARAM_STATE));

        final String number = context.get(PARAM_NUMBER);
        if (StringUtils.isNotBlank(number)) {
            final IDQLExpression numberExpr = DQLFunctions.concat(
                    property(eduPlanAlias, EppEduPlanProf.number()), value("."),
                    property("v", EppEduPlanVersion.number()), value("."),
                    property(customPlanAlias, EppCustomEduPlan.number())
            );
            dql.where(likeUpper(numberExpr, value(CoreStringUtils.escapeLike(number))));
        }

        final String studentLastName = context.get(PARAM_STUDENT_LAST_NAME);
        if (StringUtils.isNotBlank(studentLastName)) {
            dql.where(exists(
                    new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "rel").column(value(1))
                            .where(eq(property("rel", EppStudent2EduPlanVersion.customEduPlan()), property(customPlanAlias)))
                            .where(isNull(property("rel", EppStudent2EduPlanVersion.removalDate())))
                            .where(likeUpper(property("rel", EppStudent2EduPlanVersion.student().person().identityCard().lastName()), value(CoreStringUtils.escapeLike(studentLastName))))
                            .buildQuery()
            ));
        }

        orderRegistry.applyOrder(dql, input.getEntityOrder());

        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();

        final ListMultimap<Long, DataWrapper> studentMap = ArrayListMultimap.create();
        new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "rel")
                .column(property("rel", EppStudent2EduPlanVersion.customEduPlan().id()))
                .column(property("rel", EppStudent2EduPlanVersion.student().id()))
                .column(property("rel", EppStudent2EduPlanVersion.student().person().identityCard()))
                .where(isNull(property("rel", EppStudent2EduPlanVersion.removalDate())))
                .where(in(property("rel", EppStudent2EduPlanVersion.customEduPlan()), output.getRecordList()))
                .createStatement(context.getSession()).<Object[]>list().stream().forEach(item ->
                    studentMap.put((Long) item[0], new DataWrapper((Long) item[1], ((IdentityCard) item[2]).getFio()))
                );

        return output.transform((EppCustomEduPlan eppCustomEduPlan) -> {
            final ViewWrapper<EppCustomEduPlan> wrapper = new ViewWrapper<>(eppCustomEduPlan);
            final List<DataWrapper> studentWrappers = studentMap.get(eppCustomEduPlan.getId());
            studentWrappers.sort(CommonCollator.TITLED_WITH_ID_COMPARATOR);
            wrapper.setViewProperty(VIEW_PROP_STUDENT, studentWrappers);
            wrapper.setViewProperty(VIEW_PROP_READ_ONLY, eppCustomEduPlan.getState().isReadOnlyState());
            return wrapper;
        });
    }
}