package ru.tandemservice.uniepp.migration;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.entity.IRelationMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes;

import java.util.*;

import static ru.tandemservice.uniedu.migration.MS_uniedu_2x6x6_0to1.processor;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_2x6x6_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.6")
        };
    }

    /**
     * 
     * @param old2newIdMap { old_id -> new_id }
     * @param incomingRelations { table-name:column-name }
     */
    public static void migrate_id(Map<Long, Long> old2newIdMap, Set<String> incomingRelations, DBTool tool) throws Exception
    {
        final List<String> sqlList = new ArrayList<>();
        for (String rel: incomingRelations) {
            String[] s = rel.split(":");
            if (s.length != 2) { throw new IllegalStateException("wrong incoming relation format: " + rel); }
            String t = s[0], c = s[1];
            if (!tool.tableExists(t)) { continue; }
            if (!tool.columnExists(t, c)) { continue; }
            tool.table(t).foreignKeys().clear(); // referecnces
            tool.table(t).checkConstraints().clear(); // id constraint check
            tool.table(t).triggerNames().clear(); // complex check constraints
            sqlList.add("update "+t+" set "+c+"=? where "+c+"=?");
        }

        for (Map.Entry<Long, Long> e: old2newIdMap.entrySet()) {
            for (String sql: sqlList) {
                tool.executeUpdate(sql, e.getValue(), e.getKey());
            }
        }

    }

    @Override
    public void run(final DBTool tool) throws Exception
    {
        // update version_s set scriptnumber=6 where module='uni_002x006x005';
        // delete from entitycode_s where name_p in ('enrreportadmissionresultsbyenrollmentstages','dipassistantmanager');
        // delete from storablereport_t where discriminator in (3648);
        // update educationlevels_t set eduprogramsubject_id=1459364664588653861 where id in (1387060165668246295, 1387068229487170327);
        // update educationlevelshighschool_t set educationlevel_id=1466700636265403159 where id=1387068272708424237;

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppStudent2EduPlanVersion

        // создано обязательное свойство block
        {
            // создать колонку
            tool.createColumn("epp_student_eduplanversion_t", new DBColumn("block_id", DBType.LONG));

            // задать значение по умолчанию
            tool.executeUpdate(
                "update epp_student_eduplanversion_t set block_id = (" +
                "select b.id from epp_student_eduplanversion_t s2epv " +
                "inner join student_t s on (s.id=s2epv.student_id) " +
                "inner join educationorgunit_t sou on (sou.id=s.educationorgunit_id) " +
                "inner join epp_eduplan_ver_t epv on (s2epv.eduplanversion_id=epv.id) " +
                "inner join epp_eduplan_t ep on (epv.eduplan_id=ep.id) " +
                "inner join epp_eduplan_verblock_t b on (b.eduplanversion_id=epv.id and b.educationlevelhighschool_id=sou.educationlevelhighschool_id) " +
                "where s2epv.id=epp_student_eduplanversion_t.id" +
                ") where block_id is null and removaldate_p is null"
            );
        }

        /////////////////////////////////////////////////////
        // ГОС(ы), блоки
        /////////////////////////////////////////////////////

        {
            // создано свойство programSubject
            {
                // создать колонку
                tool.createColumn("epp_stateedustd_t", new DBColumn("programsubject_id", DBType.LONG));

                // устанавливаем направление для ГОС (из сопоставления - на данный момент там есть ДПО и прочая ересь, которой будет сопоставлено null)
                tool.executeUpdate("update epp_stateedustd_t set programsubject_id = (select x.eduprogramsubject_id from educationlevels_t x where x.id=educationlevel_id)");
            }

            // создано свойство programSpecialization
            {
                // создать колонку
                tool.createColumn("epp_stateedustd_block_t", new DBColumn("programspecialization_id", DBType.LONG));

                // устанавливаем направление для блока (здесь могут появиться блоки без направленности - ну и хрен с ними)
                tool.executeUpdate("update epp_stateedustd_block_t set programspecialization_id = (select x.eduprogramspecialization_id from educationlevels_t x where x.id=educationlevel_id)");

                // здесь могут появиться дубли, что печально
                {
                    List<Object[]> dupList = tool.executeQuery(processor(Long.class, Long.class), "select x.stateedustandard_id, x.programspecialization_id from epp_stateedustd_block_t x group by x.stateedustandard_id, x.programspecialization_id having count(*)>1");
                    if (dupList.size() > 0) {
                        throw new IllegalStateException(
                            "epp_stateedustd_block_t(stateedustandard_id, programspecialization_id) is not unique:\n" +
                            StringUtils.join(
                                CollectionUtils.collect(dupList, input -> "  stateedustandard_id = " + input[0] + ", programspecialization_id = " + input[1]),
                                ";\n")
                        );
                    }
                }
            }

            //  свойство parent стало необязательным
            {
                // сделать колонку NULL
                tool.setColumnNullable("epp_eduplan_t", "parent_id", true);
            }

            // перечень ГОС, которые не по направлениям классификатора (их надо будет удалить), либо те, направление которых не помигрировались
            tool.executeUpdate(
                "select g.id into tmp_epp_stateedustd_t " +
                "from epp_stateedustd_t g " +
                "inner join educationlevels_t gl on (gl.id=g.educationlevel_id) " +
                "inner join entitycode_s glc on (gl.discriminator=glc.code_id) " +
                "where programsubject_id is null " +
                "or lower(glc.name_p) not in ('" +
                StringUtils.join(Arrays.asList(
                    EducationLevelHighGos3s.class.getSimpleName().toLowerCase(),
                    EducationLevelHighGos3.class.getSimpleName().toLowerCase(),
                    EducationLevelHighGos2.class.getSimpleName().toLowerCase(),
                    EducationLevelMiddleGos3s.class.getSimpleName().toLowerCase(),
                    EducationLevelMiddleGos3.class.getSimpleName().toLowerCase(),
                    EducationLevelMiddleGos2.class.getSimpleName().toLowerCase(),
                    EducationLevelBasic.class.getSimpleName().toLowerCase()
                ), "','") +
                "')"
            );

            // сбрасываем ссылку из УП
            tool.executeUpdate("update epp_eduplan_t set parent_id=null where parent_id in (select id from tmp_epp_stateedustd_t)");

            // перечень строк ГОС, которые надо удалить
            tool.executeUpdate(
                "select r.id into tmp_epp_stdrow_base_t " +
                "from epp_stdrow_base_t r " +
                "where r.owner_id in (select b.id from epp_stateedustd_block_t b where b.stateedustandard_id in (select id from tmp_epp_stateedustd_t))"
            );

            // удаляем скиллы и комментарии
            tool.executeUpdate("delete from epp_stdrow_skill_t where skill_id in (select id from epp_stateedustd_skill_t where parent_id in (select id from tmp_epp_stateedustd_t))");
            tool.executeUpdate("delete from epp_stddscpn_cmmnt_t where row_id in (select id from tmp_epp_stdrow_base_t)");

            // на время удаления структуры ГОС делаем все колонки nullable
            tool.setColumnNullable("epp_stdrow_structure_t", "parent_id", true);
            tool.setColumnNullable("epp_stdrow_group_t", "parent_id", true);
            tool.setColumnNullable("epp_stdrow_discipline_t_t", "parent_id", true);
            tool.setColumnNullable("epp_stdrow_discipline_n_t", "parent_id", true);
            tool.setColumnNullable("epp_stdrow_action_t", "parent_id", true);

            // сбрасываем ссылки для удаляемых элементов (на время удаления)
            tool.executeUpdate("update epp_stdrow_structure_t set parent_id=null where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("update epp_stdrow_group_t set parent_id=null where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("update epp_stdrow_discipline_t_t set parent_id=null where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("update epp_stdrow_discipline_n_t set parent_id=null where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("update epp_stdrow_action_t set parent_id=null where id in (select id from tmp_epp_stdrow_base_t)");

            // удаляем строки
            tool.executeUpdate("delete from epp_stdrow_action_t where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("delete from epp_stdrow_discipline_n_t where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("delete from epp_stdrow_discipline_t_t where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("delete from epp_stdrow_discipline_t where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("delete from epp_stdrow_group_im_t where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("delete from epp_stdrow_group_re_t where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("delete from epp_stdrow_group_t where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("delete from epp_stdrow_structure_t where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("delete from epp_stdrow_hierarchy_t where id in (select id from tmp_epp_stdrow_base_t)");
            tool.executeUpdate("delete from epp_stdrow_base_t where id in (select id from tmp_epp_stdrow_base_t)");

            // удаляем скилы, блоки, ГОСы
            tool.executeUpdate("delete from epp_stateedustd_skill_t where parent_id in (select id from tmp_epp_stateedustd_t)");
            tool.executeUpdate("delete from epp_stateedustd_block_t where stateedustandard_id in (select id from tmp_epp_stateedustd_t)");
            tool.executeUpdate("delete from epp_stateedustd_t where id in (select id from tmp_epp_stateedustd_t)");

            // чистим
            tool.dropTable("tmp_epp_stdrow_base_t");
            tool.dropTable("tmp_epp_stateedustd_t");

            // проверяем, что всем оставшимся ГОСам выдали направление
            {
                List<Object[]> emptyList = tool.executeQuery(processor(Long.class, Long.class), "select id, educationlevel_id from epp_stateedustd_t where programsubject_id is null");
                if (emptyList.size() > 0) {
                    throw new IllegalStateException(
                        "epp_stateedustd_t.programsubject_id is null:\n" +
                        StringUtils.join(
                            CollectionUtils.collect(emptyList, input -> "  gos.id = " + input[0] + ", edulvl.id = " + input[1]),
                            ";\n"
                        )
                    );
                }
            }

            // свойство programSubject обязательное
            {
                // сделать колонку NOT NULL
                tool.setColumnNullable("epp_stateedustd_t", "programsubject_id", false);
            }

            // удалено свойство generation
            {
                // удалить колонку
                tool.dropColumn("epp_stateedustd_t", "generation_id");
            }

            // удалено свойство educationLevel
            {
                // удалить колонку
                tool.dropColumn("epp_stateedustd_t", "educationlevel_id");
            }

            // удалено свойство educationLevel
            {
                // удалить колонку
                tool.dropColumn("epp_stateedustd_block_t", "educationlevel_id");
            }
        }

        /////////////////////////////////////////////////////
        // БЛОКИ
        /////////////////////////////////////////////////////

        // orgunit.id - > eduOwnerOrgUnit.id
        final short codeEduOwnerOrgUnit = tool.entityCodes().get("eduOwnerOrgUnit");
        final Map<Long, Long> ownerOrgUnitIdMap = SafeMap.get(key -> {
            Long id = EntityIDGenerator.generateNewId(codeEduOwnerOrgUnit);
            try { tool.executeUpdate("insert into edu_ourole__owner_t(id, discriminator, orgunit_id) values(?,?,?)", id, codeEduOwnerOrgUnit, key); }
            catch (Exception t) { throw new IllegalStateException(t); }
            return id;
        });

        {
            final List<Object[]> ownerOrgUnitRows = tool.executeQuery(
                processor(Long.class, Long.class),
                "select id, orgunit_id from edu_ourole__owner_t"
            );
            for (Object[] row: ownerOrgUnitRows) {
                if (null != ownerOrgUnitIdMap.put((Long)row[1], (Long)row[0])) {
                    throw new IllegalStateException();
                }
            }
        }

        // orgunit.id - > eduInstitutionOrgUnit.id
        final short codeEduInstitutionOrgUnit = tool.entityCodes().get("eduInstitutionOrgUnit");
        final Map<Long, Long> intOrgUnitIdMap = SafeMap.get(key -> {
            Long id = EntityIDGenerator.generateNewId(codeEduInstitutionOrgUnit);
            try { tool.executeUpdate("insert into edu_ourole__inst_t(id, discriminator, orgunit_id) values(?,?,?)", id, codeEduInstitutionOrgUnit, key); }
            catch (Exception t) { throw new IllegalStateException(t); }
            return id;
        });
        {
            final List<Object[]> instOrgUnitRows = tool.executeQuery(
                processor(Long.class, Long.class),
                "select id, orgunit_id from edu_ourole__inst_t"
            );
            for (Object[] row: instOrgUnitRows) {
                if (null != intOrgUnitIdMap.put((Long)row[1], (Long)row[0])) {
                    throw new IllegalStateException();
                }
            }
        }

        Long topOrgUnitId = (Long)tool.executeQuery(processor(Long.class), "select id from orgunit_t where parent_id is null").iterator().next()[0];
        Long instTopOrgUnitId = intOrgUnitIdMap.get(topOrgUnitId);

        // грохаем констрейны (иначе по дадут менять id и discriminator)
        tool.table("epp_eduplan_verblock_t").constraints().clear();
        tool.table("epp_eduplan_verblock_t").triggers().clear();

        // и вот тут начинается АД - надо будет сабклассить элементы - т.е. менять id (с сохранением ссылок)
        {

            // удалено свойство parentEducationDirection
            {
                // удалить колонку
                tool.dropColumn("epp_eduplan_ver_t", "parenteducationdirection_id"); // оно ссылалось на блок (и больше нигде не нужно)
            }

            Set<String> epvBlockReferences = new HashSet<>(Arrays.asList(
                "epp_eduplan_verblock_t:id",
                "epp_eduplan_verblock_r_t:id",
                "epp_eduplan_verblock_s_t:id",
                "dpp_demand_extract__row:element_id",
                "epp_student_workplan_t:cachededuplanversionblock_id",
                "dpp_request_row:element_id",
                "ctrpr_prel_period:element_id",
                "epp_workplan_t:parent_id",
                "epp_epvrow_base_t:owner_id"
            ));
            for (IRelationMeta rel: EntityRuntime.getMeta("eppEduPlanVersionBlock").getIncomingRelations()) {
                epvBlockReferences.add(rel.getForeignEntity().getTableName()+":"+rel.getForeignProperty().getColumnName());
            }

            DBTable dbtRootBlock = new DBTable("epp_eduplan_verblock_r_t",
                new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey()
            );
            tool.createTable(dbtRootBlock);

            // гарантировать наличие кода сущности
            short eppEduPlanVersionRootBlock_entityCode = tool.entityCodes().ensure("eppEduPlanVersionRootBlock");

            // создать таблицу
            DBTable dbtSpecBlock = new DBTable("epp_eduplan_verblock_s_t",
                new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                new DBColumn("programspecialization_id", DBType.LONG),
                new DBColumn("ownerorgunit_id", DBType.LONG)
            );
            tool.createTable(dbtSpecBlock);

            // гарантировать наличие кода сущности
            short eppEduPlanVersionSpecializationBlock_entityCode = tool.entityCodes().ensure("eppEduPlanVersionSpecializationBlock");
            short eduProgramSpecializationRoot_entityCode = tool.entityCodes().ensure("eduProgramSpecializationRoot");

            // block.old_id -> block.new_id
            Map<Long, Long> epvBlockOld2NewIdMap = new HashMap<>();

            // создана новая сущность eppEduPlanVersionRootBlock
            {
                tool.setColumnNullable("epp_eduplan_verblock_t", "educationlevelhighschool_id", true); // в таблицу будем добавлять новые записи

                // создать таблицу

                List<Object[]> rootBlockRows = tool.executeQuery(
                    processor(Long.class, Long.class, Long.class, Long.class, Long.class, Long.class),
                    "select b.id, lvl.eduprogramsubject_id, hs.orgunit_id, lvl.eduprogramspecialization_id, b.eduplanversion_id, hs.id " +
                    "from epp_eduplan_verblock_t b " +
                    "inner join epp_eduplan_ver_t v on (v.id=b.eduplanversion_id) " +
                    "inner join epp_eduplan_t p on (p.id=v.eduplan_id) " +
                    "left join educationlevelshighschool_t hs on (hs.id=b.educationlevelhighschool_id) " +
                    "left join educationlevels_t lvl on (lvl.id=hs.educationlevel_id) " +
                    "where p.educationlevelhighschool_id=b.educationlevelhighschool_id"
                );

                for (Object[] row: rootBlockRows) {
                    Long oldId = (Long)row[0];
                    Long subjId = (Long)row[1];
                    Long newId = EntityIDGenerator.generateNewId(eppEduPlanVersionRootBlock_entityCode);
                    if (null != epvBlockOld2NewIdMap.put(oldId, newId)) {
                        throw new IllegalStateException();
                    }
                    tool.executeUpdate("update epp_eduplan_verblock_t set discriminator=? where id=?", eppEduPlanVersionRootBlock_entityCode, oldId);
                    tool.executeUpdate("insert into epp_eduplan_verblock_r_t(id) values (?)", oldId);

                    if (null != subjId)
                    {
                        Long ouId = (Long)row[2];
                        Long specId = (Long)row[3];
                        Long epvId = (Long)row[4];
                        Long hsId = (Long)row[5];

                        if (null == specId)
                        {
                            List<Object[]> specRows = tool.executeQuery(
                                processor(Long.class),
                                "select r.id from edu_specialization_root_t r inner join edu_specialization_base_t b on (r.id=b.id) where b.programSubject_id=?",
                                subjId
                            );
                            if (specRows.size() > 0) {
                                specId = (Long)specRows.iterator().next()[0];
                            }
                        }

                        if (null == specId) {
                            specId = EntityIDGenerator.generateNewId(eduProgramSpecializationRoot_entityCode);
                            String specCode = "root." + Long.toString(subjId, 32) + "-m";
                            String specTitle = null;
                            String specShortTitle = null;

                            tool.executeUpdate("insert into edu_specialization_base_t(id, discriminator, programsubject_id, code_p, title_p, shorttitle_p) values(?,?,?,?,?,?)", specId, eduProgramSpecializationRoot_entityCode, subjId, specCode, specTitle, specShortTitle);
                            tool.executeUpdate("insert into edu_specialization_root_t(id) values (?)", specId);
                        }

                        Long spId = EntityIDGenerator.generateNewId(eppEduPlanVersionSpecializationBlock_entityCode);
                        tool.executeUpdate("insert into epp_eduplan_verblock_t(id, discriminator, eduplanversion_id, educationlevelhighschool_id) values(?,?,?,?)", spId, eppEduPlanVersionSpecializationBlock_entityCode, epvId, hsId);
                        tool.executeUpdate("insert into epp_eduplan_verblock_s_t(id, programspecialization_id, ownerorgunit_id) values (?, ?, ?)", spId, specId, ownerOrgUnitIdMap.get(ouId));
                    }
                }
            }

            // создана новая сущность eppEduPlanVersionSpecializationBlock
            {


                List<Object[]> childBlockRows = tool.executeQuery(
                    processor(Long.class, Long.class, Long.class, Long.class, Long.class),
                    "select b.id, lvl.eduprogramspecialization_id, hs.orgunit_id, hs.id, lvl.id " +
                    "from epp_eduplan_verblock_t b " +
                    "inner join epp_eduplan_ver_t v on (v.id=b.eduplanversion_id) " +
                    "inner join epp_eduplan_t p on (p.id=v.eduplan_id) " +
                    "left join educationlevelshighschool_t hs on (hs.id=b.educationlevelhighschool_id) " +
                    "left join educationlevels_t lvl on (lvl.id=hs.educationlevel_id) " +
                    "where p.educationlevelhighschool_id<>b.educationlevelhighschool_id"
                );

                // по всем НП(в)-НП(м) не корневых блоков есть направления и направленности
                {
                    Collection<Object[]> emptyList = CollectionUtils.select(childBlockRows, row -> null == (Long)row[1]);
                    if (emptyList.size() > 0) {
                        throw new IllegalStateException(
                            "epp_eduplan_verblock_t.eduprogramspecialization_id is null:\n" +
                            StringUtils.join(
                                CollectionUtils.collect(emptyList, input -> "  epvBlock.id = " + input[0] + ", eduHs.id = " + input[3] + ", lvl.id=" + input[4]),
                                ";\n"
                            )
                        );
                    }
                }

                // поехали
                for (Object[] row: childBlockRows) {
                    Long oldId = (Long)row[0];
                    Long specId = (Long)row[1];
                    if (null == specId) { throw new IllegalStateException(); }
                    Long ouId = (Long)row[2];
                    Long newId = EntityIDGenerator.generateNewId(eppEduPlanVersionSpecializationBlock_entityCode);
                    if (null != epvBlockOld2NewIdMap.put(oldId, newId)) {
                        throw new IllegalStateException();
                    }
                    tool.executeUpdate("update epp_eduplan_verblock_t set discriminator=? where id=?", eppEduPlanVersionSpecializationBlock_entityCode, oldId);
                    tool.executeUpdate("insert into epp_eduplan_verblock_s_t(id, programspecialization_id, ownerorgunit_id) values (?, ?, ?)", oldId, specId, ownerOrgUnitIdMap.get(ouId));
                }

                // проверяем, что все заполненно верно
                tool.setColumnNullable("epp_eduplan_verblock_s_t", "programspecialization_id", false);
                tool.setColumnNullable("epp_eduplan_verblock_s_t", "ownerorgunit_id", false);
            }

            // вот он ужин то....
            migrate_id(epvBlockOld2NewIdMap, epvBlockReferences, tool);
        }

        /////////////////////////////////////////////////////
        // УП, УП(в)
        /////////////////////////////////////////////////////

        {
            // создано свойство eduProgramTrait
            {
                // создать колонку
                tool.createColumn("epp_eduplan_t", new DBColumn("programtrait_id", DBType.LONG));
            }

            // создано обязательное свойство kind
            {
                // создать колонку
                tool.createColumn("epp_eduplan_t", new DBColumn("programkind_id", DBType.LONG));

                // задать значение по умолчанию
                final String SQL = "update epp_eduplan_t " +
                "set programkind_id=(select x.id from edu_c_program_kind_t x where x.code_p=?) " +
                "where educationlevelhighschool_id in (" +
                "select hs.id from educationlevelshighschool_t hs " +
                "inner join educationlevels_t lvl on (lvl.id=hs.educationlevel_id) " +
                "inner join structureeducationlevels_t lt on (lt.id=lvl.leveltype_id) " +
                "where lt.code_p=?" +
                ")";

                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_BACHELOR_FIELD);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_BACHELOR_PROFILE);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_BACHELOR_FIELD);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_BACHELOR_PROFILE);

                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_MAGISTRATURY, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_MASTER_FIELD);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_MAGISTRATURY, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_MASTER_PROFILE);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_MAGISTRATURY, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_MASTER_FIELD);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_MAGISTRATURY, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_MASTER_PROFILE);

                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY_FIELD);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY_FIELD_SPECIALTY);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY_FIELD_SPECIALTY_SPECIALIZATION);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY_SPECIALIZATION);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_SPECIALTY);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_SPECIALTY_FIELD);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_SPECIALIZATION);

                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_, StructureEducationLevelsCodes.BASIC_GROUP);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_, StructureEducationLevelsCodes.BASIC_GROUP_PROFESSION);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_, StructureEducationLevelsCodes.BASIC_GROUP_PROFESSION_FIELD);

                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA, StructureEducationLevelsCodes.MIDDLE_GOS2_GROUP);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA, StructureEducationLevelsCodes.MIDDLE_GOS2_GROUP_SPECIALTY_ADVANCED);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA, StructureEducationLevelsCodes.MIDDLE_GOS2_GROUP_SPECIALTY_BASIC);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA, StructureEducationLevelsCodes.MIDDLE_GOS2_GROUP_SPECIALTY_FIELD);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA, StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA, StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP_SPECIALTY_ADVANCED);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA, StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP_SPECIALTY_BASIC);
                tool.executeUpdate(SQL, EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA, StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP_SPECIALTY_FIELD);

                tool.executeUpdate(SQL, EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA, StructureEducationLevelsCodes.ADDITIONAL_GROUP);
                tool.executeUpdate(SQL, EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA, StructureEducationLevelsCodes.ADDITIONAL_GROUP_ADDITIONAL);
                tool.executeUpdate(SQL, EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA, StructureEducationLevelsCodes.ADDITIONAL_GROUP_PROBATION);
                tool.executeUpdate(SQL, EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA, StructureEducationLevelsCodes.ADDITIONAL_GROUP_RETRAINING_ADDITIONAL);
                tool.executeUpdate(SQL, EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA, StructureEducationLevelsCodes.ADDITIONAL_GROUP_RETRAINING_FIELD);
                tool.executeUpdate(SQL, EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA, StructureEducationLevelsCodes.ADDITIONAL_GROUP_RETRAINING_FULL);
                tool.executeUpdate(SQL, EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA, StructureEducationLevelsCodes.ADDITIONAL_GROUP_TRAINING_FIELD);
                tool.executeUpdate(SQL, EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA, StructureEducationLevelsCodes.ADDITIONAL_GROUP_TRAINING_PROFILE);

                // сделать колонку NOT NULL
                tool.setColumnNullable("epp_eduplan_t", "programkind_id", false);
            }

            // проверяем, что для всех УП (СПО, НПО, ВПО) НП(м) сопоставлено
            {

                final List<Object[]> emptyList = tool.executeQuery(
                    processor(Long.class, Long.class, Long.class),
                    "select p.id, hs.id, lvl.id " +
                    "from epp_eduplan_t p " +
                    "inner join educationlevelshighschool_t hs on (hs.id=p.educationlevelhighschool_id) " +
                    "inner join educationlevels_t lvl on (lvl.id=hs.educationlevel_id) " +
                    "where lvl.eduprogramsubject_id is null and p.programkind_id in (select x.id from edu_c_program_kind_t x where x.code_p in ('"+
                    StringUtils.join(Arrays.asList(
                        EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA,
                        EduProgramKindCodes.PROGRAMMA_MAGISTRATURY,
                        EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV,
                        EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_,
                        EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA
                    ), "','")
                    +"'))"
                );

                if (emptyList.size() > 0) {
                    throw new IllegalStateException(
                        "epp_eduplan_t(prof).programsubject_id is null:\n" +
                        StringUtils.join(
                            CollectionUtils.collect(emptyList, input -> "  paln.id = " + input[0] + ", eduHs.id = " + input[1] + ", lvl.id=" + input[2]),
                            ";\n"
                        )
                    );
                }
            }

            // создано обязательное свойство form
            {
                // создать колонку
                tool.createColumn("epp_eduplan_t", new DBColumn("programform_id", DBType.LONG));

                // задать значение по умолчанию
                tool.executeUpdate("update epp_eduplan_t set programform_id=(select x.programform_id from developform_t x where x.id = developform_id)");

                // сделать колонку NOT NULL
                tool.setColumnNullable("epp_eduplan_t", "programform_id", false);
            }

            // создано обязательное свойство loadPresentationInWeeks
            {
                // создать колонку
                tool.createColumn("epp_eduplan_ver_t", new DBColumn("loadpresentationinweeks_p", DBType.BOOLEAN));

                // задать значение по умолчанию
                tool.executeUpdate("update epp_eduplan_ver_t set loadpresentationinweeks_p=? where loadpresentation_id in (select id from epp_c_loadpresent_t where code_p in (?, ?))", Boolean.TRUE, "week-term", "week-week");
                tool.executeUpdate("update epp_eduplan_ver_t set loadpresentationinweeks_p=? where loadpresentationinweeks_p is null", Boolean.FALSE);

                // сделать колонку NOT NULL
                tool.setColumnNullable("epp_eduplan_ver_t", "loadpresentationinweeks_p", false);
            }


            // грохаем констрейны (иначе по дадут менять id и discriminator)
            tool.table("epp_eduplan_t").constraints().clear();
            tool.table("epp_eduplan_t").triggers().clear();

            Set<String> epReferences = new HashSet<>(Arrays.asList(
                "epp_eduplan_t:id",
                "epp_eduplan_prof_addit_t:id",
                "epp_eduplan_prof_t:id",
                "epp_eduplan_prof_higher_t:id",
                "epp_eduplan_prof_secondary_t:id",
                "epp_eduplan_ver_t:eduplan_id"
            ));
            for (IRelationMeta rel: EntityRuntime.getMeta("eppEduPlan").getIncomingRelations()) {
                epReferences.add(rel.getForeignEntity().getTableName()+":"+rel.getForeignProperty().getColumnName());
            }

            // ep.old_id -> ep.new_id
            Map<Long, Long> epOld2NewIdMap = new HashMap<>();


            // заполняем все developperiod_t.eduprogramduration_id если не заполнено
            {
                final List<Object[]> emptyList = tool.executeQuery(
                    processor(Long.class, String.class, String.class),
                    "select dp.id, dp.code_p, dp.title_p from developperiod_t dp where dp.eduprogramduration_id is null and (dp.id in (select dg.developperiod_id from developgrid_t dg where dg.id in (select p.developgrid_id from epp_eduplan_t p)))"
                );
                if (emptyList.size() > 0) {
                    throw new IllegalStateException(
                        "developperiod_t.eduprogramduration_id is null:\n" +
                        StringUtils.join(
                            CollectionUtils.collect(emptyList, input -> "  dp.id = " + input[0] + ", dp.code = " + input[1] + ", dp.title = " + input[2]),
                            ";\n"
                        )
                    );
                }
            }

            // создана новая сущность eppEduPlanAdditionalProf (ДПО)
            {
                // создать таблицу
                DBTable dbt = new DBTable("epp_eduplan_prof_addit_t",
                    new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                    new DBColumn("eduprogram_id", DBType.LONG)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short eppEduPlanAdditionalProf_entityCode = tool.entityCodes().ensure("eppEduPlanAdditionalProf");
                short eduProgramAdditionalProf_entityCode = tool.entityCodes().ensure("eduProgramAdditionalProf");

                // здесь надо создавать ОП ДПО
                final List<Object[]> epRows = tool.executeQuery(
                    processor(Long.class, Long.class, Long.class, Long.class, Long.class, Long.class, String.class, String.class, String.class),
                    "select p.id, hs.orgunit_id, p.programform_id, p.programkind_id, dp.eduprogramduration_id, y.id, hs.displayabletitle_p, hs.shorttitle_p, p.number_p " +
                    "from epp_eduplan_t p " +
                    "inner join educationlevelshighschool_t hs on (hs.id=p.educationlevelhighschool_id) " +
                    "inner join developgrid_t dg on (dg.id=p.developgrid_id) " +
                    "inner join developperiod_t dp on (dp.id=dg.developperiod_id) " +
                    "left join educationyear_t y on (y.intvalue_p=p.edustartyear_p) " +
                    "where p.programkind_id in (select id from edu_c_program_kind_t where code_p=?)",
                    EduProgramKindCodes.DOPOLNITELNAYA_PROFESSIONALNAYA_PROGRAMMA
                );

                for (Object[] row: epRows) {
                    Long epId = (Long)row[0];
                    Long eduHsOuId = (Long)row[1];
                    Long programformId = (Long)row[2];
                    Long kindId = (Long)row[3];
                    Long durationId = (Long)row[4];
                    Long yearId = (Long)row[5];
                    String eduHsTitle = (String)row[6];
                    String eduHsShortTitle = (String)row[7];
                    String planNumber = (String)row[8];

                    Long programId = EntityIDGenerator.generateNewId(eduProgramAdditionalProf_entityCode);
                    tool.executeUpdate(
                        "insert into edu_program_t " +
                        "(id, discriminator, programuniquekey_p, kind_id, year_id, institutionorgunit_id, ownerorgunit_id, form_id, duration_id, eduprogramtrait_id, title_p, shorttitle_p)" +
                        "values(?,?,?,?,?,?,?,?,?,?,?,?)",
                        programId, eduProgramAdditionalProf_entityCode,
                        "tmp."+epId,
                        kindId,
                        yearId,
                        instTopOrgUnitId,
                        ownerOrgUnitIdMap.get(eduHsOuId),
                        programformId,
                        durationId,
                        null,
                        eduHsTitle + ", УП №" + planNumber,
                        eduHsShortTitle
                    );
                    tool.executeUpdate("insert into edu_program_additional_t(id) values (?)", programId);
                    tool.executeUpdate("insert into edu_program_additional_prof_t(id) values (?)", programId);

                    tool.executeUpdate("update epp_eduplan_t set discriminator=? where id=?", eppEduPlanAdditionalProf_entityCode, epId);
                    tool.executeUpdate("insert into epp_eduplan_prof_addit_t(id, eduprogram_id) values (?,?)", epId, programId);

                    Long newId = EntityIDGenerator.generateNewId(eppEduPlanAdditionalProf_entityCode);
                    if (null != epOld2NewIdMap.put(epId, newId)) {
                        throw new IllegalStateException();
                    }
                }

                // проверяем, что все заполненно верно
                tool.setColumnNullable("epp_eduplan_prof_addit_t", "eduprogram_id", false);
            }

            // создана новая сущность eppEduPlanProf
            {
                // создать таблицу
                DBTable dbt = new DBTable("epp_eduplan_prof_t",
                    new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                    new DBColumn("baselevel_id", DBType.LONG),
                    new DBColumn("programsubject_id", DBType.LONG)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short eppEduPlanProf_entityCode = tool.entityCodes().ensure("eppEduPlanProf");
            }

            // select distinct y.c from (
            // select x.idx_code c, x.s_id, count(x.q_id) from (
            //  select idx.code_p idx_code, s.id s_id, q.id q_id
            //  from edu_c_pr_subject_t s
            //  inner join edu_c_pr_subject_index_t idx on (idx.id = s.subjectindex_id)
            //  left join edu_c_pr_subject_qual_t sq on (sq.programsubject_id = s.id)
            //  left join edu_c_pr_qual_t q on (q.id = sq.programqualification_id)
            // ) x group by x.idx_code, x.s_id
            // having count(x.q_id) = 0
            // ) y
            // order by y.c


            // квалификации (берем первую)
            final List<Object[]> qRows = tool.executeQuery(
                processor(Long.class, Long.class, String.class),
                "select sq.programsubject_id, sq.programqualification_id, q.title_p from edu_c_pr_subject_qual_t sq " +
                "left join edu_c_pr_qual_t q on (q.id = sq.programqualification_id) " +
                "order by q.title_p"
            );

            // квалификации (берем первую)
            Map<Long, Map<String, Long>> s2qIdMap = new HashMap<>();
            for (Object[] row: qRows) {
                final Long sId = (Long)row[0];
                SafeMap.safeGet(s2qIdMap, sId, TreeMap.class).put(StringUtils.trimToEmpty((String)row[2]).toLowerCase(), (Long)row[1]);
            }


            // только 2009.40 и 2009.50 не имеют квалификации - к ВПО это не относится (=> считаем что у ВПО квалификация есть всегда - берем первую)
            // создана новая сущность eppEduPlanHigherProf (ВПО)
            {
                // создать таблицу
                DBTable dbt = new DBTable("epp_eduplan_prof_higher_t",
                    new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                    new DBColumn("programqualification_id", DBType.LONG)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short eppEduPlanHigherProf_entityCode = tool.entityCodes().ensure("eppEduPlanHigherProf");

                // УП (ВПО), используется таблица qualifications_t
                final List<Object[]> epRows = tool.executeQuery(
                    processor(Long.class, Long.class, Long.class, String.class, String.class, Long.class),
                    "select p.id, s.id, si.id, si.code_p, q.title_p, hs.assignedqualification_id " +
                    "from epp_eduplan_t p " +
                    "inner join educationlevelshighschool_t hs on (hs.id=p.educationlevelhighschool_id) " +
                    "inner join educationlevels_t lvl on (lvl.id=hs.educationlevel_id) " +
                    "inner join edu_c_pr_subject_t s on (s.id=lvl.eduprogramsubject_id) " +
                    "inner join edu_c_pr_subject_index_t si on (si.id=s.subjectindex_id) " +
                    "inner join qualifications_t q on (q.id=lvl.qualification_id) " +
                    "where si.code_p in ('"+
                    StringUtils.join(Arrays.asList(
                        EduProgramSubjectIndexCodes.TITLE_2005_62,
                        EduProgramSubjectIndexCodes.TITLE_2005_65,
                        EduProgramSubjectIndexCodes.TITLE_2005_68,
                        EduProgramSubjectIndexCodes.TITLE_2009_62,
                        EduProgramSubjectIndexCodes.TITLE_2009_65,
                        EduProgramSubjectIndexCodes.TITLE_2009_68,
                        EduProgramSubjectIndexCodes.TITLE_2013_03,
                        EduProgramSubjectIndexCodes.TITLE_2013_04,
                        EduProgramSubjectIndexCodes.TITLE_2013_05,
                        EduProgramSubjectIndexCodes.TITLE_2013_06,
                        EduProgramSubjectIndexCodes.TITLE_2013_07,
                        EduProgramSubjectIndexCodes.TITLE_2013_08
                    ), "','")
                    +"')"
                );

                // Для остальныз baselevel_id = "2013.1.4";1459108016847607150;"Среднее общее образование"
                Long baseLvl_2013_1_4 = (Long)tool.executeQuery(processor(Long.class), "select id from c_edu_level_t where code_p=?", "2013.1.4").iterator().next()[0];

                // Для магистратуры baselevel_id = "2013.2.2";1459108016853898606;"Высшее образование - бакалавриат"
                Long baseLvl_2013_2_2 = (Long)tool.executeQuery(processor(Long.class), "select id from c_edu_level_t where code_p=?", "2013.2.2").iterator().next()[0];

                // Для остального ВО baselevel_id = "2013.2.3";1459108016853898606;"Высшее образование - специалитет, магистратура"
                Long baseLvl_2013_2_3 = (Long)tool.executeQuery(processor(Long.class), "select id from c_edu_level_t where code_p=?", "2013.2.3").iterator().next()[0];

                // Заливаем данные в базу
                for (Object[] row: epRows)
                {
                    Long epId = (Long)row[0];
                    Long sId = (Long)row[1];
                    String siCode = (String)row[3];
                    String qTitle = StringUtils.trimToEmpty((String)row[4]).toLowerCase();
                    Long qId = (Long)row[5];

                    Long lvlId = baseLvl_2013_1_4;
                    switch(siCode) {
                        case EduProgramSubjectIndexCodes.TITLE_2005_68:
                        case EduProgramSubjectIndexCodes.TITLE_2009_68:
                        case EduProgramSubjectIndexCodes.TITLE_2013_04:
                            lvlId = baseLvl_2013_2_2;
                            break;
                        case EduProgramSubjectIndexCodes.TITLE_2013_06:
                        case EduProgramSubjectIndexCodes.TITLE_2013_07:
                        case EduProgramSubjectIndexCodes.TITLE_2013_08:
                            lvlId = baseLvl_2013_2_3;
                            break;
                    }

                    if (null == qId) {
                        Map<String, Long> qIds = s2qIdMap.get(sId);
                        if (null == qIds || qIds.isEmpty()) { throw new IllegalStateException("sId = "+sId+", qId is null"); }

                        qId = qIds.get(qTitle);
                        if (null == qId) { qId = qIds.values().iterator().next(); }
                    }

                    tool.executeUpdate("update epp_eduplan_t set discriminator=? where id=?", eppEduPlanHigherProf_entityCode, epId);
                    tool.executeUpdate("insert into epp_eduplan_prof_t(id,baselevel_id,programsubject_id) values (?,?,?)", epId, lvlId, sId);
                    tool.executeUpdate("insert into epp_eduplan_prof_higher_t(id,programqualification_id) values (?,?)", epId, qId);

                    Long newId = EntityIDGenerator.generateNewId(eppEduPlanHigherProf_entityCode);
                    if (null != epOld2NewIdMap.put(epId, newId)) {
                        throw new IllegalStateException();
                    }
                }

                // проверяем, что все заполненно верно
                tool.setColumnNullable("epp_eduplan_prof_higher_t", "programqualification_id", false);
            }

            // только 2009.40 и 2009.50 не имеют квалификации - для них создадим пустые
            // создана новая сущность eppEduPlanSecondaryProf (СПО, НПО старые)
            {
                Map<Long, Long> subj2qSetIdMap = new HashMap<>();

                // создать таблицу
                DBTable dbt = new DBTable("epp_eduplan_prof_secondary_t",
                    new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                    new DBColumn("indepthstudy_p", DBType.BOOLEAN).setNullable(false),
                    new DBColumn("programqualification_id", DBType.LONG).setNullable(true)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short eppEduPlanSecondaryProf_entityCode = tool.entityCodes().ensure("eppEduPlanSecondaryProf");

                // УП (СПО, НПО)
                final List<Object[]> epRows = tool.executeQuery(
                    processor(Long.class, Long.class, Long.class, String.class, String.class),
                    "select p.id, s.id, si.id, si.code_p, q.code_p " +
                    "from epp_eduplan_t p " +
                    "inner join educationlevelshighschool_t hs on (hs.id=p.educationlevelhighschool_id) " +
                    "inner join educationlevels_t lvl on (lvl.id=hs.educationlevel_id) " +
                    "inner join edu_c_pr_subject_t s on (s.id=lvl.eduprogramsubject_id) " +
                    "inner join edu_c_pr_subject_index_t si on (si.id=s.subjectindex_id) " +
                    "inner join qualifications_t q on (q.id=lvl.qualification_id) " +
                    "where si.code_p in ('"+
                    StringUtils.join(Arrays.asList(
                        EduProgramSubjectIndexCodes.TITLE_2005_50,
                        EduProgramSubjectIndexCodes.TITLE_2009_40,
                        EduProgramSubjectIndexCodes.TITLE_2009_50,
                        EduProgramSubjectIndexCodes.TITLE_2013_01,
                        EduProgramSubjectIndexCodes.TITLE_2013_02
                    ), "','")
                    +"')"
                );

                // Для всех УП ставим базовый уровень - baselevel_id = "2013.1.4";1459108016847607150;"Среднее общее образование"
                Long baseLvl_2013_1_4 = (Long)tool.executeQuery(processor(Long.class), "select id from c_edu_level_t where code_p=?", "2013.1.4").iterator().next()[0];

                // Заливаем данные в базу
                for (Object[] row: epRows)
                {
                    Long epId = (Long)row[0];
                    Long sId = (Long)row[1];
                    Long siId = (Long)row[2];
                    String siCode = (String)row[3];
                    String qCode = (String)row[4];

                    boolean indepth = "52".equals(qCode);

                    tool.executeUpdate("update epp_eduplan_t set discriminator=? where id=?", eppEduPlanSecondaryProf_entityCode, epId);
                    tool.executeUpdate("insert into epp_eduplan_prof_t(id,baselevel_id,programsubject_id) values (?,?,?)", epId, baseLvl_2013_1_4, sId);
                    tool.executeUpdate("insert into epp_eduplan_prof_secondary_t(id,indepthstudy_p,programqualification_id) values (?,?,?)", epId, indepth, null);

                    Long newId = EntityIDGenerator.generateNewId(eppEduPlanSecondaryProf_entityCode);
                    if (null != epOld2NewIdMap.put(epId, newId)) {
                        throw new IllegalStateException();
                    }
                }
            }

            // проверяем, что все заполненно верно
            tool.setColumnNullable("epp_eduplan_prof_t", "baselevel_id", false);
            tool.setColumnNullable("epp_eduplan_prof_t", "programsubject_id", false);

            // вот он ужин то....
            migrate_id(epOld2NewIdMap, epReferences, tool);

            // здесь надо присвоить вид программы из направления для программ проф. образования
            tool.executeUpdate(
                "update epp_eduplan_t set programkind_id=(" +
                "select si.programkind_id from epp_eduplan_prof_t ep " +
                "inner join edu_c_pr_subject_t s on (s.id=ep.programsubject_id) " +
                "inner join edu_c_pr_subject_index_t si on (si.id=s.subjectindex_id) " +
                "where ep.id=epp_eduplan_t.id) where id in (select id from epp_eduplan_prof_t)"
            );
        }


        /////////////////////////////////////////////////////
        // Остальное
        /////////////////////////////////////////////////////





        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppIndexRule4SubjectIndex

        // создана новая сущность
        {
            if (tool.tableExists("epp_edulevel_gen_rel_t")) {
                tool.dropTable("epp_edulevel_gen_rel_t");
            }

            // создать таблицу
            DBTable dbt = new DBTable("epp_edulevel_gen_rel_t",
                new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                new DBColumn("eduprogramsubjectindex_id", DBType.LONG).setNullable(false),
                new DBColumn("eppindexrule_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eppIndexRule4SubjectIndex");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppPlanStructure4SubjectIndex

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("epp_regstruct_subidx_rel_t",
                new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                new DBColumn("eppplanstructure_id", DBType.LONG).setNullable(false),
                new DBColumn("eduprogramsubjectindex_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eppPlanStructure4SubjectIndex");

        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppEduPlan

        // удалено свойство educationLevelHighSchool
        {
            // удалить колонку
            tool.dropColumn("epp_eduplan_t", "educationlevelhighschool_id");
        }

        // удалено свойство educationLevelStage
        {
            // удалить колонку
            tool.dropColumn("epp_eduplan_t", "educationlevelstage_id");
        }

        // удалено свойство developForm
        {
            // удалить колонку
            tool.dropColumn("epp_eduplan_t", "developform_id");
        }

        // удалено свойство developTech
        {
            // удалить колонку
            tool.dropColumn("epp_eduplan_t", "developtech_id");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppEduPlanVersion


        // удалено свойство loadPresentation
        {
            // удалить колонку
            tool.dropColumn("epp_eduplan_ver_t", "loadpresentation_id");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppEduPlanVersionBlock

        // удалено свойство educationLevelHighSchool
        {
            // удалить колонку
            tool.dropColumn("epp_eduplan_verblock_t", "educationlevelhighschool_id");
        }



        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppPlanStructureQualification

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("epp_regstruct_qual_rel_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eppPlanStructureQualification");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppLoadPresentation

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("epp_c_loadpresent_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eppLoadPresentation");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppIndexRule4Qualification

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("epp_edulevel_gen_rel_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eppIndexRule4Qualification");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppGeneration

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("epp_c_generation_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eppGeneration");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppEduLevelToWorkTypesRel

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("epp_edulevel_worktypes_rel_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("eppEduLevelToWorkTypesRel");
        }


    }
}