package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплина (из реестра)
 *
 * Дисциплина (как правило, разбивается по семестрам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegistryDisciplineGen extends EppRegistryElement
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline";
    public static final String ENTITY_NAME = "eppRegistryDiscipline";
    public static final int VERSION_HASH = -212291063;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppRegistryDisciplineGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegistryDisciplineGen> extends EppRegistryElement.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegistryDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new EppRegistryDiscipline();
        }
    }
    private static final Path<EppRegistryDiscipline> _dslPath = new Path<EppRegistryDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegistryDiscipline");
    }
            

    public static class Path<E extends EppRegistryDiscipline> extends EppRegistryElement.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EppRegistryDiscipline.class;
        }

        public String getEntityName()
        {
            return "eppRegistryDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
