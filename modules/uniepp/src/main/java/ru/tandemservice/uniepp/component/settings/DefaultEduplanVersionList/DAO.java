// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.settings.DefaultEduplanVersionList;

import com.google.common.collect.ImmutableList;
import org.hibernate.Session;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 04.12.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    public static final String APPLY_ONLY_FOR_FIRST_COURSE_PARAM = "applyOnlyForFirstCourse";

    @Override
    public void prepare(final Model model)
    {
        model.setDisplayOptionList(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(0L, "без указанных версий УП"),
                new IdentifiableWrapper(1L, "с неактуальными версиями УП"))));
        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setTerritorialOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setProducingOrgUnitListModel(new LazySimpleSelectModel<>(UniDaoFacade.getOrgstructDao().getOrgUnitList(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING)));
        model.setEducationLevelsModel(new FullCheckSelectModel(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE)
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(final String filter)
            {
                final List<OrgUnit> formativeOrgUnitList = model.getSettings().get("formativeOrgUnitList");
                final List<OrgUnit> territorialOrgUnitList = model.getSettings().get("territorialOrgUnitList");
                final List<EducationLevelsHighSchool> list = UniDaoFacade.getEducationLevelDao().getEducationLevelsHighSchoolList(formativeOrgUnitList, territorialOrgUnitList, filter);
                return new ListResult<>(list);
            }
        });
        model.setQualificationListModel(new QualificationModel(this.getSession()));
        model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
        model.setDevelopConditionListModel(new LazySimpleSelectModel<>(DevelopCondition.class).setSortProperty(DevelopForm.P_CODE));
        model.setDevelopPeriodListModel(new LazySimpleSelectModel<>(DevelopPeriod.class).setSortProperty(DevelopPeriod.P_PRIORITY));
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        final Session session = this.getSession();

        final IDataSettings settings = model.getSettings();

        final IdentifiableWrapper displayOption = settings.get("displayOption");
        final boolean withNoVersion = (displayOption != null) && displayOption.getId().equals(0L);
        final boolean withUnactualVersion = (displayOption != null) && displayOption.getId().equals(1L);
        final Object changeDate = settings.get("changeDate");
        final Object formativeOrgUnitList = settings.get("formativeOrgUnitList");
        final Object territorialOrgUnitList = settings.get("territorialOrgUnitList");
        final Object producingOrgUnitList = settings.get("producingOrgUnitList");
        final Object eduLevelHighSchool = settings.get("eduLevelHighSchool");
        final Object qualificationList = settings.get("qualification");
        final Object developFormList = settings.get("developFormList");
        final Object developConditionList = settings.get("developConditionList");
        final Object developPeriodList = settings.get("developPeriodList");

        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EducationOrgUnit.class, "ou")
        .column(property("ou"));

        FilterUtils.applySelectFilter(dql, "ou", EducationOrgUnit.formativeOrgUnit().s(), formativeOrgUnitList);
        FilterUtils.applySelectFilter(dql, "ou", EducationOrgUnit.territorialOrgUnit().s(), territorialOrgUnitList);
        FilterUtils.applySelectFilter(dql, "ou", EducationOrgUnit.educationLevelHighSchool().orgUnit().s(), producingOrgUnitList);
        FilterUtils.applySelectFilter(dql, "ou", EducationOrgUnit.educationLevelHighSchool().s(), eduLevelHighSchool);
        FilterUtils.applySelectFilter(dql, "ou", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().s(), qualificationList);
        FilterUtils.applySelectFilter(dql, "ou", EducationOrgUnit.developForm().s(), developFormList);
        FilterUtils.applySelectFilter(dql, "ou", EducationOrgUnit.developCondition().s(), developConditionList);
        FilterUtils.applySelectFilter(dql, "ou", EducationOrgUnit.developPeriod().s(), developPeriodList);

        final DQLSelectBuilder relBuilder = new DQLSelectBuilder().fromEntity(EppEducationOrgUnitEduPlanVersion.class, "rel")
        .column(property("rel"), "settings")
        .column(property("rel.educationOrgUnit"), "settings_ou")
        .column(property("rel.eduPlanVersion"), "settings_version")
        .column(property("rel.modificationDate"), "settings_date");

        final DynamicListDataSource<EducationOrgUnit> dataSource = model.getDataSource();
        final EntityOrder order = dataSource.getEntityOrder();
        final OrderDirection orderDirection = order.getDirection().equals(OrderDirection.asc) ? OrderDirection.asc : OrderDirection.desc;

        if (order.getKeyString().equals(EducationOrgUnit.educationLevelHighSchool().displayableTitle().s())) {
            dql.order(property(EducationOrgUnit.educationLevelHighSchool().displayableTitle().fromAlias("ou")), orderDirection);
        }

        if (withNoVersion)
        {
            relBuilder.where(eq(property(EppEducationOrgUnitEduPlanVersion.educationOrgUnit().id().fromAlias("rel")), property("ou.id")));
            dql.where(notExists(relBuilder.buildQuery()));
            dql.order("ou.id", orderDirection);

            final Number totalSize = dql.createCountStatement((new DQLExecutionContext(session))).uniqueResult();
            dataSource.setTotalSize(null == totalSize ? 0 : totalSize.longValue());
            final List<EducationOrgUnit> list = dql.createStatement(session).
            setFirstResult((int) dataSource.getStartRow())
            .setMaxResults((int) dataSource.getCountRow())
            .list();
            dataSource.createPage(list);

            for (final ViewWrapper<EducationOrgUnit> wrapper : ViewWrapper.<EducationOrgUnit>getPatchedList(dataSource))
            {
                wrapper.setViewProperty("version", null);
                wrapper.setViewProperty("developGridTitle", null);
                wrapper.setViewProperty("changeDate", null);
                wrapper.setViewProperty(APPLY_ONLY_FOR_FIRST_COURSE_PARAM, null);
            }
        }
        else
        {
            dql.joinDataSource("ou", null == changeDate ? DQLJoinType.left : DQLJoinType.inner, relBuilder.buildQuery(), "settings", eq(property("settings.settings.educationOrgUnit.id"), property("ou", "id")));
            if (withUnactualVersion) {
                dql.where(in(property("settings.settings_version." + EppEduPlanVersion.state().code().s()), EppState.UNACTUAL_STATES));
            }
            if (order.getKey().equals("changeDate")) {
                dql.order("settings.settings_date", orderDirection);
            }
            if (null != changeDate) {
                dql.where(ge(property("settings.settings_date"), commonValue(changeDate, PropertyType.DATE)));
            }
            dql.order("ou.id", orderDirection);
            dql.column("settings.settings");

            final Number totalSize = dql.createCountStatement((new DQLExecutionContext(session))).uniqueResult();
            dataSource.setTotalSize(null == totalSize ? 0 : totalSize.longValue());
            final List<Object[]> list = dql.createStatement(session).
            setFirstResult((int) dataSource.getStartRow())
            .setMaxResults((int) dataSource.getCountRow())
            .list();

            final Map<EducationOrgUnit, EppEducationOrgUnitEduPlanVersion> eduOuSettingsMap = new LinkedHashMap<>();
            for (final Object[] row : list) {
                eduOuSettingsMap.put((EducationOrgUnit) row[0], (EppEducationOrgUnitEduPlanVersion) row[1]);
            }

            dataSource.createPage(new ArrayList<>(eduOuSettingsMap.keySet()));

            for (final ViewWrapper<EducationOrgUnit> wrapper : ViewWrapper.<EducationOrgUnit>getPatchedList(dataSource))
            {
                final EppEducationOrgUnitEduPlanVersion eduOuSettigs = eduOuSettingsMap.get(wrapper.getEntity());
                if (null == eduOuSettigs)
                {
                    wrapper.setViewProperty("version", null);
                    wrapper.setViewProperty("developGridTitle", null);
                    wrapper.setViewProperty("changeDate", null);
                    wrapper.setViewProperty(APPLY_ONLY_FOR_FIRST_COURSE_PARAM, null);
                }
                else
                {
                    wrapper.setViewProperty("version", eduOuSettigs.getEduPlanVersion());
                    wrapper.setViewProperty("developGridTitle", eduOuSettigs.getEduPlanVersion().getEduPlan().getDevelopGrid().getTitle());
                    wrapper.setViewProperty("changeDate", eduOuSettigs.getModificationDate());
                    wrapper.setViewProperty(APPLY_ONLY_FOR_FIRST_COURSE_PARAM, eduOuSettigs.isApplyOnlyForFirstCourse());
                }
            }
        }
    }

    @Override
    public void doActualize(final List<Long> ids)
    {
        final DQLSelectBuilder actual = new DQLSelectBuilder()
            .fromEntity(EducationOrgUnit.class, "ou")
            .joinPath(DQLJoinType.inner, EducationOrgUnit.developForm().programForm().fromAlias("ou"), "eduou_programForm")
            .joinPath(DQLJoinType.inner, EducationOrgUnit.developTech().fromAlias("ou"), "eduou_developTech")
            .joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().fromAlias("ou"), "eduou_programSubj")
            .column(property("ou"));
        if (null != ids) {
            actual.where(in(property("ou.id"), ids));
        }
        final DQLSelectBuilder version = new DQLSelectBuilder()
            .fromEntity(EppEduPlanVersion.class, "v")
            .joinEntity("v", DQLJoinType.inner, EppEduPlanProf.class, "prof", eq(property("v", EppEduPlanVersion.eduPlan()), property("prof")))
                .column(property("v"))
            .column(property(EppEduPlanProf.programSubject().fromAlias("prof")), "v_programSubj")
            .column(property(EppEduPlanVersion.eduPlan().developGrid().developPeriod().fromAlias("v")), "v_developPeriod")
            .column(property(EppEduPlanVersion.eduPlan().programForm().fromAlias("v")), "v_programForm")
            .column(property(EppEduPlanVersion.eduPlan().developCondition().fromAlias("v")), "v_developCondition")
                .column(property(EppEduPlanVersion.eduPlan().programTrait().fromAlias("v")), "v_programTrait")
                .where(ne(property(EppEduPlanVersion.eduPlan().state().code().fromAlias("v")), value(EppState.STATE_ARCHIVED)))
                .where(ne(property(EppEduPlanVersion.eduPlan().state().code().fromAlias("v")), value(EppState.STATE_REJECTED)))
            ;
        actual.joinDataSource("ou", DQLJoinType.inner, version.buildQuery(), "version", and(
            eq(
                property("version.v_programSubj"),
                property("eduou_programSubj.id")),
            eq(
                property("version.v_developPeriod"),
                property(EducationOrgUnit.developPeriod().fromAlias("ou"))),
            eq(
                property("version.v_programForm"),
                property("eduou_programForm.id")),
            eq(
                property("version.v_developCondition"),
                property(EducationOrgUnit.developCondition().fromAlias("ou"))),
            eqNullSafe(
                property("version.v_programTrait"),
                property("eduou_developTech", DevelopTech.programTrait().s()))
        ));
        actual.column(property("version.v"));
        final Map<EducationOrgUnit, Set<EppEduPlanVersion>> eduPlanVersionMap = new HashMap<>();
        for (final Object[] row : actual.createStatement(this.getSession()).<Object[]>list()) {
            SafeMap.safeGet(eduPlanVersionMap, (EducationOrgUnit) row[0], HashSet.class).add((EppEduPlanVersion) row[1]);
        }

        final DQLSelectBuilder existing = new DQLSelectBuilder()
        .fromEntity(EppEducationOrgUnitEduPlanVersion.class, "settings")
        .column(property("settings"))
        .fetchPath(DQLJoinType.inner, EppEducationOrgUnitEduPlanVersion.educationOrgUnit().fromAlias("settings"), "ou");
        if (null != ids) {
            existing.where(in(property(EppEducationOrgUnitEduPlanVersion.educationOrgUnit().id().fromAlias("settings")), ids));
        }
        final Map<EducationOrgUnit, EppEducationOrgUnitEduPlanVersion> existingSettingsMap = new HashMap<>();
        for (final EppEducationOrgUnitEduPlanVersion settings : existing.createStatement(this.getSession()).<EppEducationOrgUnitEduPlanVersion>list()) {
            existingSettingsMap.put(settings.getEducationOrgUnit(), settings);
        }

        final List<EducationOrgUnit> eduOuList = ids == null ? this.getList(EducationOrgUnit.class) : this.getList(EducationOrgUnit.class, ids);
        for (final EducationOrgUnit ou : eduOuList)
        {
            EppEducationOrgUnitEduPlanVersion settings = existingSettingsMap.get(ou);
            if ((null != settings) && !EppState.UNACTUAL_STATES.contains(settings.getEduPlanVersion().getEduPlan().getState().getCode())) {
                continue;
            }
            final Set<EppEduPlanVersion> versions = eduPlanVersionMap.get(ou);
            if ((versions == null) || (versions.size() != 1)) {
                continue;
            }
            if (null == settings)
            {
                settings = new EppEducationOrgUnitEduPlanVersion();
                settings.setEducationOrgUnit(ou);
            }
            settings.setModificationDate(new Date());
            settings.setEduPlanVersion(versions.iterator().next());
            this.getSession().saveOrUpdate(settings);
        }
    }

    @Override
    public void doToggleApplyOnlyForFirstCourse(Long educationOrgUnitId)
    {
        EppEducationOrgUnitEduPlanVersion settings = getByNaturalId(
                new EppEducationOrgUnitEduPlanVersion.NaturalId((EducationOrgUnit) getSession().load(EducationOrgUnit.class, educationOrgUnitId))
        );
        settings.setApplyOnlyForFirstCourse(!settings.isApplyOnlyForFirstCourse());
        update(settings);
    }
}
