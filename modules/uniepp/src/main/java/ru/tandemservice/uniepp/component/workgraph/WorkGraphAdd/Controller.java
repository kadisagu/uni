/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onChangeCombination(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDevelopCombination() != null)
        {
            model.getWorkGraph().setDevelopForm(model.getDevelopCombination().getDevelopForm());
            model.getWorkGraph().setDevelopCondition(model.getDevelopCombination().getDevelopCondition());
            model.getWorkGraph().setDevelopTech(model.getDevelopCombination().getDevelopTech());
            model.getWorkGraph().setDevelopGrid(model.getDevelopCombination().getDevelopGrid());
        }
    }

    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        this.getDao().update(model);

        this.deactivate(component);

        this.activateInRoot(component, new PublisherActivator(model.getWorkGraph()));
    }
}