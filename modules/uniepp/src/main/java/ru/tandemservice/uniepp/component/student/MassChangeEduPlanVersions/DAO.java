package ru.tandemservice.uniepp.component.student.MassChangeEduPlanVersions;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        // лист студентов
        final List<Student> studentList = this.getList(
                new DQLSimple<>(Student.class)
                        .where(Student.id(), model.getStudentIds())
                        .where(Student.archival(), false).dql()
        );

        final Set<EduProgramForm> programFormSet = new HashSet<>();
        final Set<DevelopCondition> developConditionSet = new HashSet<>();
        final Set<EduProgramTrait> programTraitSet = new HashSet<>();
        final Set<DevelopPeriod> developPeriodSet = new HashSet<>();
        final Set<EducationLevels> levelsHashSet = new HashSet<>();

        for (final Student student : studentList)
        {
            final EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();

            programFormSet.add(educationOrgUnit.getDevelopForm().getProgramForm());
            developConditionSet.add(educationOrgUnit.getDevelopCondition());
            programTraitSet.add(educationOrgUnit.getDevelopTech().getProgramTrait());
            developPeriodSet.add(educationOrgUnit.getDevelopPeriod());
            levelsHashSet.add(educationOrgUnit.getEducationLevelHighSchool().getEducationLevel());
        }



        // лезем в базу если только одна комбинация футс
        List<EppEduPlanVersionBlock> blockList = new ArrayList<>();
        if ((1 == programFormSet.size()) && (1 == developConditionSet.size()) && (1 == programTraitSet.size()) && (1 == developPeriodSet.size()) && (1 == levelsHashSet.size()))
        {
            final EduProgramForm programForm = programFormSet.iterator().next();
            final EduProgramTrait programTrait = programTraitSet.iterator().next();
            final EducationLevels educationLevel = levelsHashSet.iterator().next();
            final boolean isAdditional = educationLevel.getLevelType().isAdditional();

            final DQLSelectBuilder dql = new DQLSelectBuilder().column(property("b"))
                    .fromEntity(EppEduPlanVersionBlock.class, "b")
                    .fromEntity(EppEduPlan.class, "p");

            if (isAdditional)
                dql.joinEntity("p", DQLJoinType.inner, EppEduPlanAdditionalProf.class, "pa", eq(property("p.id"), property("pa.id")));
            else
                dql.joinEntity("p", DQLJoinType.inner, EppEduPlanProf.class, "pp", eq(property("p.id"), property("pp.id")));

            dql.where(eq(property("p"), property("b", EppEduPlanVersionBlock.eduPlanVersion().eduPlan())));
            dql.where(eq(property("b", EppEduPlanVersionBlock.eduPlanVersion().eduPlan().programForm().s()), value(programForm)));
            if (!model.isIndividual()) {
                final DevelopCondition developCondition = developConditionSet.iterator().next();
                final DevelopPeriod developPeriod = developPeriodSet.iterator().next();
                dql.where(eq(property("b", EppEduPlanVersionBlock.eduPlanVersion().eduPlan().developCondition().s()), value(developCondition)));
                dql.where(eq(property("b", EppEduPlanVersionBlock.eduPlanVersion().eduPlan().developGrid().developPeriod().s()), value(developPeriod)));
            } else {
                dql.where(exists(EppCustomEduPlan.class, EppCustomEduPlan.epvBlock().eduPlanVersion().s(), property("b", EppEduPlanVersionBlock.eduPlanVersion())));
            }
            if (!isAdditional)
                dql.where(eq(property("pp", EppEduPlanProf.programSubject().s()), value(educationLevel.getEduProgramSubject())));

            if (null == programTrait) {
                dql.where(isNull(property("b", EppEduPlanVersionBlock.eduPlanVersion().eduPlan().programTrait().s())));
            } else {
                dql.where(eq(property("b", EppEduPlanVersionBlock.eduPlanVersion().eduPlan().programTrait().s()), value(programTrait)));
            }
            blockList = this.getList(dql);
        }

        final Set<Long> epvIds = new HashSet<>();
        final Set<Long> blockIds = new HashSet<>();

        final List<EppEduPlan> eduPlanList = new ArrayList<>();
        for (EppEduPlanVersionBlock block : blockList) {
            if (!eduPlanList.contains(block.getEduPlanVersion().getEduPlan())) {
                eduPlanList.add(block.getEduPlanVersion().getEduPlan());
            }
            epvIds.add(block.getEduPlanVersion().getId());
            blockIds.add(block.getId());
        }
        Collections.sort(eduPlanList, ITitled.TITLED_COMPARATOR);
        model.setEduPlanListModel(new LazySimpleSelectModel<>(eduPlanList, EppEduPlan.titleWithArchive().s()));

        model.setEduPlanVersionListModel(new DQLFullCheckSelectModel(EppEduPlanVersion.titleWithArchive().s())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(EppEduPlanVersion.class, alias)
                        .where(in(alias + ".id", epvIds))
                        .order(property(alias, EppEduPlanVersion.number()));

                if (null == model.getEduPlan()) {
                    dql.where(nothing());
                } else {
                    dql.where(eq(property(alias, EppEduPlanVersion.eduPlan()), value(model.getEduPlan())));
                }
                FilterUtils.applySimpleLikeFilter(dql, alias, EppEduPlanVersion.number().s(), filter);

                return dql;
            }
        });
        model.setBlockListModel(new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(EppEduPlanVersionBlock.class, alias)
                        .where(in(alias + ".id", blockIds));

                if (null == model.getEduPlanVersion()) {
                    dql.where(nothing());
                } else {
                    dql.where(eq(property(alias, EppEduPlanVersionBlock.eduPlanVersion()), value(model.getEduPlanVersion())));
                    if (model.isIndividual()) {
                        dql.where(instanceOf(alias, EppEduPlanVersionSpecializationBlock.class));
                    }
                }
                return dql;
            }

            @Override
            protected void sortOptions(List list)
            {
                Collections.sort(list, (o1, o2) -> EppEduPlanVersionBlock.COMPARATOR.compare((EppEduPlanVersionBlock) o1, (EppEduPlanVersionBlock) o2));
            }
        });

        model.setCustomPlanListModel(new DQLFullCheckSelectModel(EppCustomEduPlan.titleWithGrid().s())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(EppCustomEduPlan.class, alias)
                        .where(in(property(alias, EppCustomEduPlan.epvBlock()), blockIds))
                        .order(property(alias, EppCustomEduPlan.number()));

                if (null == model.getBlock()) {
                    dql.where(nothing());
                } else {
                    dql.where(eq(property(alias, EppCustomEduPlan.epvBlock()), value(model.getBlock())));
                }
                return dql;
            }
        });

        // автоподстановка начальнных значений

        final Map<Long, EppStudent2EduPlanVersion> relationMap = IEppEduPlanDAO.instance.get().getActiveStudentEduplanVersionRelationMap(model.getStudentIds());

        if (!relationMap.isEmpty() && !model.isShowMessage())
        {
            final EppStudent2EduPlanVersion relation = relationMap.values().iterator().next();
            model.setEduPlan(relation.getEduPlanVersion().getEduPlan());
            model.setEduPlanVersion(relation.getEduPlanVersion());
            model.setBlock(relation.getBlock());
            model.setCustomEduPlan(model.isIndividual() ? relation.getCustomEduPlan() : null);
        }

        if (!eduPlanList.isEmpty() && model.getEduPlan() == null)
        {
            model.setEduPlan(eduPlanList.get(0));
        }
    }

    @Override
    public void update(final Model model)
    {
        final List<IEppEduPlanDAO.StudentEduPlanData> data = model.getStudentIds().stream().map(studentId -> new IEppEduPlanDAO.StudentEduPlanData() {
            @Override public Long getStudentId() { return studentId; }
            @Override public Long getVersionId() { return model.getEduPlanVersion().getId(); }
            @Override public Long getBlockId() { return model.getBlock() != null ? model.getBlock().getId() : null; }
            @Override public Long getCustomEduPlanId() { return model.getCustomEduPlan() != null ? model.getCustomEduPlan().getId() : null; }
        }).collect(Collectors.toList());

        IEppEduPlanDAO.instance.get().doUpdateStudentEduPlanVersion(data);
    }
}
