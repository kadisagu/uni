package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.dialect.MSSqlDBDialect;
import org.tandemframework.dbsupport.ddl.dialect.PostgresDBDialect;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x6x9_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.9")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppEduPlanVersion

		// создано обязательное свойство developGridTerm
		{
			// создать колонку
			tool.createColumn("epp_eduplan_ver_t", new DBColumn("developgridterm_id", DBType.LONG));

            if (tool.getDialect() instanceof MSSqlDBDialect)
            {
                System.out.println("MS_uniepp_2x6x9_1to2. updated " +
                                           tool.executeUpdate(
                                                   "update ver " +
                                                           "set developgridterm_id = (" +
                                                           "    select top 1 gt.id from developgridterm_t gt " +
                                                           "    inner join term_t t on gt.term_id=t.id " +
                                                           "    where gt.developGrid_id=epp.developGrid_id " +
                                                           "    order by t.intvalue_p asc" +
                                                           ") " +
                                                           "from epp_eduplan_ver_t ver " +
                                                           "inner join epp_eduplan_t epp on ver.eduplan_id=epp.id"
                                           ) + " rows");
            }
            else if (tool.getDialect() instanceof PostgresDBDialect)
            {
                System.out.println("MS_uniepp_2x6x9_1to2. updated " +
                                           tool.executeUpdate(
                                                   "update epp_eduplan_ver_t ver " +
                                                           "set developgridterm_id = (" +
                                                           "    select gt.id from developgridterm_t gt " +
                                                           "    inner join term_t t on gt.term_id=t.id " +
                                                           "    where gt.developGrid_id=epp.developGrid_id " +
                                                           "    order by t.intvalue_p asc " +
                                                           "    limit 1" +
                                                           ") " +
                                                           "from epp_eduplan_t epp " +
                                                           "where ver.eduplan_id=epp.id "
                                           ) + " rows");
            }
            else
            {
                throw new UnsupportedOperationException("Dialect " + tool.getDialect() + " is not supported.");
            }

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_eduplan_ver_t", "developgridterm_id", false);

		}


    }
}