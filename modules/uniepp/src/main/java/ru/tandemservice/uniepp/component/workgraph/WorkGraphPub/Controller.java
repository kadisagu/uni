/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.RichDataSourceModel;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRowGen;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniepp.tapestry.richTableList.WeekTypeBlockColumn;

import java.util.Collections;


/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRenderComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        model.setSettings(component.getSettings());

        this.getDao().prepare(model);

        if ((model.getSelectedTab() != null) && !"workGraphTab".equals(model.getSelectedTab()))
        {
            return;
        }

        this.prepareGraphDataSource(component);
    }

    @SuppressWarnings( { "unchecked", "deprecation" })
    private void prepareGraphDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        final AbstractColumn courseColumn = new SimpleColumn("Курс", EppWorkGraphRowGen.course().intValue().s()).setClickable(false).setOrderable(false);
        courseColumn.setHeaderStyle("width:25px;min-width:25px;text-align:center;padding:0px;");
        courseColumn.setStyle("width:25px;max-width:25px;min-width:25px;text-align:center;padding:0px;");

        final AbstractColumn versionColumn = new SimpleColumn("Версия УП", "version").setClickable(false).setOrderable(false);
        versionColumn.setHeaderStyle("width:250px;min-width:250px;text-align:center;padding:0px;");
        versionColumn.setStyle("width:250px;max-width:250px;min-width:250px;text-align:center;padding:0px;");

        createHeader(model, courseColumn, versionColumn);


        // создаем колонки для всех RangeSelectionListDataSource
        for (final RangeSelectionWeekTypeListDataSource rangeModel : model.getId2dataSource().values())
        {
            createDataBySubject(model, courseColumn, versionColumn, rangeModel);
        }
    }

    private void createDataBySubject(Model model, AbstractColumn courseColumn, AbstractColumn versionColumn, RangeSelectionWeekTypeListDataSource rangeModel)
    {
        // создаем колонки
        final AbstractListDataSource dataSource = rangeModel.getDataSource();
        dataSource.addColumn(courseColumn);
        for (final EppYearEducationWeek week : model.getWeekData())
        {
            final WeekTypeBlockColumn column = new WeekTypeBlockColumn(week.getId(), week.getNumber() - 1, Integer.toString(week.getNumber()));
            column.setStyle("padding:0;text-align:center;min-width:20px;width:10px;font-size:11px;");
            dataSource.addColumn(column);

        }
        dataSource.addColumn(versionColumn);
    }

    private void createHeader(Model model, AbstractColumn courseColumn, AbstractColumn versionColumn)
    {
        // создаем шапку всех таблиц ГУП
        if (!model.getId2dataSource().isEmpty())
        {
            final AbstractListDataSource<IEntity> dataSource = new SimpleListDataSource<>(Collections.emptyList());
            dataSource.setCountRow(0);

            dataSource.addColumn(courseColumn);
            for (final EppYearEducationWeek week : model.getWeekData())
            {
                final HeadColumn weekColumn = new HeadColumn(Integer.toString(week.getNumber()), week.getTitle());
                weekColumn.setVerticalHeader(true);

                final SimpleColumn column = new SimpleColumn(Integer.toString(week.getNumber()), null);
                column.setHeaderStyle("padding:0;text-align:center;min-width:20px;width:10px;font-size:11px;");

                weekColumn.addColumn(column);
                dataSource.addColumn(weekColumn);
            }
            dataSource.addColumn(versionColumn);

            model.setHeaderDataSource(new RichDataSourceModel<>(dataSource));
        }
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        component.saveSettings();
        this.onRefreshComponent(component);
    }

    public void onClickClear(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.setCourses(null);
        model.setProgramSubject(null);
        this.onClickSearch(component);
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        this.getDao().delete(this.getModel(component).getWorkGraph());
        this.deactivate(component);
    }

    public void onClickPrintWorkGraphCourses(final IBusinessComponent component)
    {
        this.activateInRoot(component,
                new ComponentActivator(
                        ru.tandemservice.uniepp.component.workgraph.WorkGraphPrint.Model.class.getPackage().getName(),
                        new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getWorkGraph().getId())
                        .add("printType", "onCourses")));
    }

    public void onClickPrintWorkGraphSpecialities(final IBusinessComponent component)
    {
        this.activateInRoot(component,
                new ComponentActivator(
                        ru.tandemservice.uniepp.component.workgraph.WorkGraphPrint.Model.class.getPackage().getName(),
                        new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getWorkGraph().getId())
                        .add("printType", "onSpecialities")));
    }
}
