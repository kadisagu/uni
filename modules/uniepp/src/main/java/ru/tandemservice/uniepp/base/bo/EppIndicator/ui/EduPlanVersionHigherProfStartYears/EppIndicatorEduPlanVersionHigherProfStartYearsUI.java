/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.EduPlanVersionHigherProfStartYears;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;

/**
 * @author nvankov
 * @since 4/27/15
 */
public class EppIndicatorEduPlanVersionHigherProfStartYearsUI extends UIPresenter
{

    @Override
    public void onComponentRefresh()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("settings", getSettings());

        if (EppIndicatorEduPlanVersionHigherProfStartYears.PROGRAM_SPECIALIZATION_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppIndicatorEduPlanVersionHigherProfStartYears.PARAM_PROGRAM_SUBJECT, getSettings().get(EppIndicatorEduPlanVersionHigherProfStartYears.PARAM_PROGRAM_SUBJECT));
        }
    }

    public void onClickSearch()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
        saveSettings();
    }

    // getters and setters
}
