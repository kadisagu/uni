package ru.tandemservice.uniepp.catalog.bo.EppPlanStructure.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author avedernikov
 * @since 27.08.2015
 */

@Configuration
public class EppPlanStructureList extends BusinessComponentManager
{
	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint() {
		return this.presenterExtPointBuilder()
			.create();
	}
}