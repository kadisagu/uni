
package ru.tandemservice.uniepp.catalog.bo.EppPlanStructure.ui.List;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicPub.CatalogDynamicPubUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author avedernikov
 * @since 27.08.2015
 */
@State({
	@Bind(key = DefaultCatalogPubModel.CATALOG_CODE, binding = "catalogCode")
})
public class EppPlanStructureListUI extends CatalogDynamicPubUI
{
	// todo сии колхозы (c) не следовало писать, ибо в приоритезированных справочниках надобно юзать implements IPrioritizedCatalogItem

	@Override
	protected void addActionColumns(DynamicListDataSource<ICatalogItem> dataSource)
	{
		dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setPermissionKey(getSec().getPermission("edit")));
		dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setPermissionKey(getSec().getPermission("edit")));
		super.addActionColumns(dataSource);
	}

	public void onClickUp()
	{
		EppPlanStructure planStructure = DataAccessServices.dao().get(EppPlanStructure.class, getListenerParameterAsLong());
		CommonManager.instance().commonPriorityDao().doChangePriorityUp(getListenerParameterAsLong(), true, EppPlanStructure.parent(), planStructure.getParent());
	}

	public void onClickDown()
	{
		EppPlanStructure planStructure = DataAccessServices.dao().get(EppPlanStructure.class, getListenerParameterAsLong());
		CommonManager.instance().commonPriorityDao().doChangePriorityDown(getListenerParameterAsLong(), true, EppPlanStructure.parent(), planStructure.getParent());
	}

	@Override
	protected void prepareItemDS()
	{
		super.prepareItemDS();
		getItemDS().getColumn(ICatalogItem.CATALOG_ITEM_TITLE).setOrderable(false);
	}

	@Override
	protected void updateItemDS()
	{
		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppPlanStructure.class, "planStruct");

		DynamicListDataSource<ICatalogItem> dataSource = getItemDS();
		final long count = ISharedBaseDao.instance.get().getCount(dql);
		dataSource.setCountRow(count);
		dataSource.setTotalSize(count);

		long startRow = dataSource.isPrintEnabled() ? dataSource.getPrintStartRow() : dataSource.getStartRow();
		long countRow = dataSource.isPrintEnabled() ? dataSource.getPrintCountRow() : dataSource.getCountRow();
		List<ICatalogItem> list = ISharedBaseDao.instance.get().<EppPlanStructure>getList(dql, (int) startRow, (int) countRow).stream()
			.map(NestedPlanStructure::new)
			.sorted(EppPlanStructureListUI::compare)
			.map(NestedPlanStructure::getItem)
			.collect(Collectors.toList());
		dataSource.createPage(list);

		final Set<Long> disabledIds = dataSource.getEntityList().stream()
			.filter(e -> CatalogManager.instance().modifyDao().isCatalogItemSystem(e))
			.map(ICatalogItem::getId)
			.collect(Collectors.toSet());
		setDeleteDisabledIds(disabledIds);

		Set<Long> catalogItemIds = CommonBaseEntityUtil.getIdSet(dataSource.getEntityList());
		setEditDisabledIds(getDynamicCatalogDesc().getEditDisabledItems(catalogItemIds));
	}

	private static int nesting(EppPlanStructure st)
	{
		return st.getParent() == null ? 0 : 1 + nesting(st.getParent());
	}
	private static int compare(EppPlanStructure st1, EppPlanStructure st2, int lvlDiff)
	{
		if (lvlDiff > 0)
			return compare(st1.getParent(), st2, lvlDiff - 1);
		if (lvlDiff < 0)
			return compare(st1, st2.getParent(), lvlDiff + 1);
		if (st1.getParent() == st2.getParent())
			return Integer.compare(st1.getPriority(), st2.getPriority());
		return compare(st1.getParent(), st2.getParent(), 0);
	}
	private static int compare(NestedPlanStructure ps1, NestedPlanStructure ps2)
	{
		int lvlDiff = ps1.getLevel() - ps2.getLevel();
		int stCmp = compare(ps1.getItem(), ps2.getItem(), lvlDiff);
		if (stCmp != 0)
			return stCmp;
		return Integer.compare(ps1.getLevel(), ps2.getLevel());
	}
	static class NestedPlanStructure
	{
		public EppPlanStructure ps;
		public int level;
		public NestedPlanStructure(EppPlanStructure ps)
		{
			this.ps = ps;
			this.level = nesting(ps);
		}
		public EppPlanStructure getItem()
		{
			return ps;
		}
		public int getLevel()
		{
			return level;
		}
	}
}
