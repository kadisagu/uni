package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебный модуль: аудиторная нагрузка
 *
 * Аудиторная нагрузка по модулю
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegistryModuleALoadGen extends EntityBase
 implements INaturalIdentifiable<EppRegistryModuleALoadGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad";
    public static final String ENTITY_NAME = "eppRegistryModuleALoad";
    public static final int VERSION_HASH = 305787893;
    private static IEntityMeta ENTITY_META;

    public static final String L_MODULE = "module";
    public static final String L_LOAD_TYPE = "loadType";
    public static final String P_LOAD = "load";

    private EppRegistryModule _module;     // Учебный модуль
    private EppALoadType _loadType;     // Вид аудиторной нагрузки (типы аудиторных занятий)
    private long _load;     // Число часов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный модуль. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryModule getModule()
    {
        return _module;
    }

    /**
     * @param module Учебный модуль. Свойство не может быть null.
     */
    public void setModule(EppRegistryModule module)
    {
        dirty(_module, module);
        _module = module;
    }

    /**
     * @return Вид аудиторной нагрузки (типы аудиторных занятий). Свойство не может быть null.
     */
    @NotNull
    public EppALoadType getLoadType()
    {
        return _loadType;
    }

    /**
     * @param loadType Вид аудиторной нагрузки (типы аудиторных занятий). Свойство не может быть null.
     */
    public void setLoadType(EppALoadType loadType)
    {
        dirty(_loadType, loadType);
        _loadType = loadType;
    }

    /**
     * @return Число часов. Свойство не может быть null.
     */
    @NotNull
    public long getLoad()
    {
        return _load;
    }

    /**
     * @param load Число часов. Свойство не может быть null.
     */
    public void setLoad(long load)
    {
        dirty(_load, load);
        _load = load;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRegistryModuleALoadGen)
        {
            if (withNaturalIdProperties)
            {
                setModule(((EppRegistryModuleALoad)another).getModule());
                setLoadType(((EppRegistryModuleALoad)another).getLoadType());
            }
            setLoad(((EppRegistryModuleALoad)another).getLoad());
        }
    }

    public INaturalId<EppRegistryModuleALoadGen> getNaturalId()
    {
        return new NaturalId(getModule(), getLoadType());
    }

    public static class NaturalId extends NaturalIdBase<EppRegistryModuleALoadGen>
    {
        private static final String PROXY_NAME = "EppRegistryModuleALoadNaturalProxy";

        private Long _module;
        private Long _loadType;

        public NaturalId()
        {}

        public NaturalId(EppRegistryModule module, EppALoadType loadType)
        {
            _module = ((IEntity) module).getId();
            _loadType = ((IEntity) loadType).getId();
        }

        public Long getModule()
        {
            return _module;
        }

        public void setModule(Long module)
        {
            _module = module;
        }

        public Long getLoadType()
        {
            return _loadType;
        }

        public void setLoadType(Long loadType)
        {
            _loadType = loadType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppRegistryModuleALoadGen.NaturalId) ) return false;

            EppRegistryModuleALoadGen.NaturalId that = (NaturalId) o;

            if( !equals(getModule(), that.getModule()) ) return false;
            if( !equals(getLoadType(), that.getLoadType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getModule());
            result = hashCode(result, getLoadType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getModule());
            sb.append("/");
            sb.append(getLoadType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegistryModuleALoadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegistryModuleALoad.class;
        }

        public T newInstance()
        {
            return (T) new EppRegistryModuleALoad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "module":
                    return obj.getModule();
                case "loadType":
                    return obj.getLoadType();
                case "load":
                    return obj.getLoad();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "module":
                    obj.setModule((EppRegistryModule) value);
                    return;
                case "loadType":
                    obj.setLoadType((EppALoadType) value);
                    return;
                case "load":
                    obj.setLoad((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "module":
                        return true;
                case "loadType":
                        return true;
                case "load":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "module":
                    return true;
                case "loadType":
                    return true;
                case "load":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "module":
                    return EppRegistryModule.class;
                case "loadType":
                    return EppALoadType.class;
                case "load":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRegistryModuleALoad> _dslPath = new Path<EppRegistryModuleALoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegistryModuleALoad");
    }
            

    /**
     * @return Учебный модуль. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad#getModule()
     */
    public static EppRegistryModule.Path<EppRegistryModule> module()
    {
        return _dslPath.module();
    }

    /**
     * @return Вид аудиторной нагрузки (типы аудиторных занятий). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad#getLoadType()
     */
    public static EppALoadType.Path<EppALoadType> loadType()
    {
        return _dslPath.loadType();
    }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad#getLoad()
     */
    public static PropertyPath<Long> load()
    {
        return _dslPath.load();
    }

    public static class Path<E extends EppRegistryModuleALoad> extends EntityPath<E>
    {
        private EppRegistryModule.Path<EppRegistryModule> _module;
        private EppALoadType.Path<EppALoadType> _loadType;
        private PropertyPath<Long> _load;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный модуль. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad#getModule()
     */
        public EppRegistryModule.Path<EppRegistryModule> module()
        {
            if(_module == null )
                _module = new EppRegistryModule.Path<EppRegistryModule>(L_MODULE, this);
            return _module;
        }

    /**
     * @return Вид аудиторной нагрузки (типы аудиторных занятий). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad#getLoadType()
     */
        public EppALoadType.Path<EppALoadType> loadType()
        {
            if(_loadType == null )
                _loadType = new EppALoadType.Path<EppALoadType>(L_LOAD_TYPE, this);
            return _loadType;
        }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad#getLoad()
     */
        public PropertyPath<Long> load()
        {
            if(_load == null )
                _load = new PropertyPath<Long>(EppRegistryModuleALoadGen.P_LOAD, this);
            return _load;
        }

        public Class getEntityClass()
        {
            return EppRegistryModuleALoad.class;
        }

        public String getEntityName()
        {
            return "eppRegistryModuleALoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
