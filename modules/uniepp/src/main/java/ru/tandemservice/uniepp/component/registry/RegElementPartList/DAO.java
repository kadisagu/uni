package ru.tandemservice.uniepp.component.registry.RegElementPartList;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uni.util.SafeSimpleColumn;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppBaseRegElWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartModuleWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.ui.EduPlanVersionBlockDataSourceGenerator;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.shared.commonbase.events.CommonbaseImmutableNaturalIdListener.IMMUTABLE_NATURAL_ID_LOCKER;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        final EppRegistryElement regElement = model.getElement();
        final IEppRegElWrapper elementWrapper = IEppRegistryDAO.instance.get().getRegistryElementDataMap(Collections.singleton(regElement.getId())).get(regElement.getId());
        final Map<String, Double> loadMap = this.getLoadMap(regElement);

        final List<Model.Row> rowList = new ArrayList<>();
        final Model.Row elementRow = new Model.Row(elementWrapper, null);
        rowList.add(elementRow);

        final StaticListDataSource<Model.Row> ds = new StaticListDataSource<>();

        // кастомизация списка
        ds.setRowCustomizer(new SimpleRowCustomizer<Model.Row>() {
            @Override  public String getRowStyle(final Model.Row entity) {
                final IEppBaseRegElWrapper w = entity.getEntity();
                if (w instanceof IEppRegElWrapper) { return "font-weight:bold;background-color: "+UniDefines.COLOR_BLUE+";"; }
                if (w instanceof IEppRegElPartWrapper) { return "background-color: "+UniDefines.COLOR_BLUE+";"; }
                return "";
            }
        });

        // базовые колонки
        ds.addColumn(UniEppUtils.getStateColumn("item.module"));
        ds.addColumn(new SimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__TITLE, "title").setClickable(false).setOrderable(false).setTreeColumn(true));
        ds.addColumn(new SafeSimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__OWNER, EppRegistryElementPartModule.module().owner().shortTitle().fromAlias("item")).setClickable(false).setOrderable(false));

        // всего часов, всего трудоемкости
        final HeadColumn h = new HeadColumn("load", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__LOAD);
        h.addColumn(this.column_load(EduPlanVersionBlockDataSourceGenerator.TITLE__LOAD_TOTAL_SIZE, EppLoadType.FULL_CODE_TOTAL_HOURS, loadMap));
        h.addColumn(this.column_load(EduPlanVersionBlockDataSourceGenerator.TITLE__LOAD_TOTAL_LABOR, EppLoadType.FULL_CODE_LABOR, loadMap));
        if (model.getElement() instanceof EppRegistryPractice) {
            h.addColumn(this.column_load(EduPlanVersionBlockDataSourceGenerator.TITLE__LOAD_TOTAL_WEEKS, EppLoadType.FULL_CODE_WEEKS, loadMap));
        }

        // аудиторная нагрузка
        final HeadColumn al = new HeadColumn("aload", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__A_LOAD);
        final List<EppALoadType> loads = this.getList(EppALoadType.class, EppALoadType.P_CODE);
        for (final EppALoadType load: loads) {
            al.addColumn(this.column_load(load, loadMap));
        }
        h.addColumn(al.setWidth(1).setHeaderAlign("center"));
        ds.addColumn(h.setWidth(1).setHeaderAlign("center"));

        // формы контроля (итоговые и текущие)
        final HeadColumn cc = new HeadColumn("cacts", EduPlanVersionBlockDataSourceGenerator.TITLE__HEAD__CONTROL_ACTIONS);
        final List<EppControlActionType> actions = this.getList(EppControlActionType.class, new String[]{ EppControlActionType.P_CATALOG_CODE, EppControlActionType.P_CODE });
        for (final EppControlActionType ca: actions) {
            final int sz = elementWrapper.getActionSize(ca.getFullCode());
            if (sz > 0) { cc.addColumn(this.column_action(ca)); }
        }
        ds.addColumn(cc.setWidth(1).setHeaderAlign("center"));

        // дейсвтия со строками
        ds.addColumn(new BlockColumn("actions", "", "rowActionsBlock").setWidth(1).setPermissionKey(model.getSec().getPermission("editModule")));

        for (final IEppRegElPartWrapper partWrapper: elementWrapper.getPartMap().values())
        {
            final Model.Row partRow = new Model.Row(partWrapper, elementRow);
            rowList.add(partRow);

            for (final IEppRegElPartModuleWrapper moduleWrapper: partWrapper.getModuleMap().values())
            {
                final Model.Row moduleRow = new Model.Row(moduleWrapper, partRow);
                rowList.add(moduleRow);
            }
        }

        ds.setupRows(rowList);
        model.setDataSource(ds);
    }

    protected Map<String, Double> getLoadMap(final EppRegistryElement regElement)
    {
        final Map<String, EppRegistryElementLoad> tmp = IEppRegistryDAO.instance.get().getRegistryElementTotalLoadMap(Collections.singleton(regElement.getId())).get(regElement.getId());
        final Map<String, Double> loadMap = new HashMap<>(tmp.size());
        for (final Map.Entry<String, EppRegistryElementLoad> e: tmp.entrySet()) { loadMap.put(e.getKey(), e.getValue().getLoadAsDouble()); }
        loadMap.put(EppLoadType.FULL_CODE_TOTAL_HOURS, regElement.getSizeAsDouble());
        loadMap.put(EppLoadType.FULL_CODE_LABOR, regElement.getLaborAsDouble());
        loadMap.put(EppLoadType.FULL_CODE_WEEKS, regElement instanceof EppRegistryPractice ? ((EppRegistryPractice) regElement).getWeeksAsDouble() : null);
        return loadMap;
    }

    protected AbstractColumn column_action(final EppControlActionType ca)
    {
        return new SafeSimpleColumn(ca.getShortTitle(), ca.getFullCode()) {
            @Override public String getContent(final IEntity entity) {
                final int sz = ((Model.Row)entity).getEntity().getActionSize(ca.getFullCode());
                return (sz > 0 ? String.valueOf(sz) : "");
            }
        }.setVerticalHeader(true).setHeaderAlign("center").setAlign("right").setWidth(1).setClickable(false).setOrderable(false);
    }

    protected AbstractColumn column_load(final EppALoadType load, final Map<String, Double> loadMap)
    {
        return this.column_load(load.getShortTitle(), load.getFullCode(), loadMap);
    }

    protected AbstractColumn column_load(final String title, final String fullCode, final Map<String, Double> loadMap)
    {
        return new SafeSimpleColumn(title, fullCode) {
            @Override public boolean isRawContent() { return true; }
            @Override public String getContent(final IEntity entity) {
                final IEppBaseRegElWrapper e = ((Model.Row)entity).getEntity();
                final double ll = e.getLoadAsDouble(fullCode);

                if (e.getItem() instanceof EppRegistryElement)
                {
                    final Double lx = loadMap.get(fullCode);
                    if (null != lx) {
                        if (!UniEppUtils.eq(lx.doubleValue(), ll)) {
                            return UniEppUtils.error(UniEppUtils.formatLoad(lx, false), UniEppUtils.formatLoad(ll, false));
                        }
                    }
                }

                return UniEppUtils.formatLoad(ll, true);
            }
        }.setVerticalHeader(true).setHeaderAlign("center").setAlign("right").setWidth(1).setClickable(false).setOrderable(false);
    }

    @Override
    public void deleteRow(final Model model, final Long id) {
        final IEntity e = this.getNotNull(id);
        if (e instanceof EppRegistryElementPart) { deleteElementPart((EppRegistryElementPart)e); }
        if (e instanceof EppRegistryElementPartModule) {this.delete(e); }
    }

    private void deleteElementPart(EppRegistryElementPart part)
    {
        final Session session = this.getSession();
        final EppRegistryElement regElement = part.getRegistryElement();
        session.delete(part);
        session.flush();

        final List<EppRegistryElementPart> partList = this.getList(EppRegistryElementPart.class, EppRegistryElementPart.registryElement(), regElement, EppRegistryElementPart.P_NUMBER);
        regElement.setParts(partList.size());
        session.update(regElement);

        try (EventListenerLocker.Lock ignored = IMMUTABLE_NATURAL_ID_LOCKER.lock(EventListenerLocker.noopHandler())) {
            // Приходится обновлять через DQL, т.к. при обновлении через хибернат обойти блокировку редактирования нат.идентификатора слишком сложно (TF-1037)
            for (int i = 0; i < partList.size(); i++) {
                final EppRegistryElementPart p = partList.get(i);
                new DQLUpdateBuilder(EppRegistryElementPart.class)
                        .set(EppRegistryElementPart.P_NUMBER, value(i + 1))
                        .where(eq(property(EppRegistryElementPart.id()), value(p)))
                        .createStatement(session).execute();
            }
            session.flush();
        }
    }

    public void addPart(final Model model, final Long regElemId)
    {
        final EppRegistryElement registryElement = this.getNotNull(regElemId);
        registryElement.setParts(registryElement.getParts() + 1);
        IEppRegistryDAO.instance.get().doSaveRegistryElement(registryElement);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void doMove(final Model model, final Long id, final int direction)
    {
        if (null == id) { return; }

        final EppRegistryElement regElement = model.getElement();
        final Session session = this.lock(regElement);

        final PlaneTree flatTree = new PlaneTree((List)model.getDataSource().getRowList());
        final List<Model.Row> items = (List)flatTree.getFlatTreeObjectsHierarchicalSorted("");

        final Model.Row row0 = (Model.Row)CollectionUtils.find(items, row -> id.equals(((Model.Row)row).getId()));

        if (! (row0.getEntity().getItem() instanceof EppRegistryElementPartModule)) { return; }
        final EppRegistryElementPartModule m0 = (EppRegistryElementPartModule) row0.getEntity().getItem();

        final Model.Row row1 = items.get(Math.min(Math.max(0, items.indexOf(row0) + direction), items.size()-1));
        if (row1.getId().equals(row0.getId())) { return; }

        if (row1.getEntity().getItem() instanceof EppRegistryElementPartModule)
        {
            final EppRegistryElementPartModule m1 = (EppRegistryElementPartModule) row1.getEntity().getItem();
            if (m0.getPart().equals(m1.getPart()))
            {
                final int n0 = m0.getNumber(), n1 = m1.getNumber();
                m0.setNumber(-1);
                m1.setNumber(-2);
                session.flush();
                m0.setNumber(n1);
                m1.setNumber(n0);
                session.flush();
                session.clear();

            }
        }
        else if (row1.getEntity().getItem() instanceof EppRegistryElementPart)
        {
            EppRegistryElementPart p1 = (EppRegistryElementPart) row1.getEntity().getItem();
            if (direction < 0) {
                // надо взять предыдущую часть
                p1 = new DQLSelectBuilder()
                        .fromEntity(EppRegistryElementPart.class, "x")
                        .where(eq(property(EppRegistryElementPart.registryElement().fromAlias("x")), value(regElement)))
                        .where(lt(property(EppRegistryElementPart.number().fromAlias("x")), value(p1.getNumber())))
                        .order(property(EppRegistryElementPart.number().fromAlias("x")), OrderDirection.desc)
                        .createStatement(session).setMaxResults(1).uniqueResult();
                if (null == p1) { return; }
            }

            if (p1.equals(m0.getPart())) { return; }
            new DQLUpdateBuilder(EppRegistryElementPartModule.class)
                    .set(EppRegistryElementPartModule.L_PART, value(p1))
                    .set(EppRegistryElementPartModule.P_NUMBER, value(direction < 0 ? Integer.MAX_VALUE : Integer.MIN_VALUE))
                    .where(eq(property("id"), value(m0.getId())))
                    .createStatement(session).execute();
            session.clear();

        }

        IEppRegistryDAO.instance.get().doRecalculateRegistryElementNumbers(
                this.get(EppRegistryElement.class, regElement.getId())
        );
    }
}
