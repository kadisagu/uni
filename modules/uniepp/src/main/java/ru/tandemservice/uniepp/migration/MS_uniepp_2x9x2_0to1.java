package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * Руками написанная миграция
 */
@SuppressWarnings({"unused", "SpellCheckingInspection"})
public class MS_uniepp_2x9x2_0to1 extends IndependentMigrationScript {

    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.2")
        };
    }

    @Override
    public void run(final DBTool tool) throws Exception {

        SQLUpdateQuery updateQuery = new SQLUpdateQuery("edu_specialization_base_t", "sb")
                .set("title_p", "ps.title_p")
                .where("sb.title_p is null");
        updateQuery
                .getUpdatedTableFrom()
                .innerJoin(SQLFrom.table("edu_specialization_root_t", "sr"), "sb.id=sr.id")
                .innerJoin(SQLFrom.table("edu_c_pr_subject_t", "ps"),"ps.id=sb.programsubject_id");

        int r = tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery));
        tool.info(r + " rows (EduProgramSpecializationRoot) with title == null was filled.");
    }

}