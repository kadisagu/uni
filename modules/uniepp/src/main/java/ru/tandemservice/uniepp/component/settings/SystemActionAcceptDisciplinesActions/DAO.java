package ru.tandemservice.uniepp.component.settings.SystemActionAcceptDisciplinesActions;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(final Model model)
    {
        final Session session = this.getSession();

        final MQBuilder sb = new MQBuilder(EppRegistryElement.ENTITY_NAME, "o", new String[] { EppRegistryElement.owner().id().s() });
        sb.add(MQExpression.notIn("o", EppRegistryElement.state().code().s(), EppState.STATE_ACCEPTED, EppState.STATE_ARCHIVED));

        final MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_NAME, "o");
        builder.add(MQExpression.in("o", "id", sb));
        builder.addOrder("o", OrgUnit.title().s());
        final List<OrgUnit> orgUnitList = builder.getResultList(session);

        UniBaseUtils.createPage(model.getDataSource(), orgUnitList);
    }

    @Override
    public void doAcceptDisciplinesAndActions(final Set<Long> orgUnitsIds)
    {
        final Session session = this.getSession();

        final EppState state = this.get(EppState.class, EppState.code().s(), EppState.STATE_ACCEPTED);

        if (null != state)
        {
            final MQBuilder builder = new MQBuilder(EppRegistryElement.ENTITY_NAME, "o");
            builder.add(MQExpression.in("o", EppRegistryElement.owner().id().s(), orgUnitsIds));
            builder.add(MQExpression.notIn("o", EppRegistryElement.state().code().s(), EppState.STATE_ACCEPTED, EppState.STATE_ARCHIVED));

            final List<EppRegistryElement> orgUnitList = builder.getResultList(session);
            for (final EppRegistryElement element : orgUnitList)
            {
                element.setState(state);
                session.saveOrUpdate(element);
            }
        }

    }
}
