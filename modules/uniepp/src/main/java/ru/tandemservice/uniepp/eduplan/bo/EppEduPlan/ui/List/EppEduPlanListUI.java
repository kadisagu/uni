/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.sec.ISecured;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.AdditionalProfList.EppEduPlanAdditionalProfList;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfList.EppEduPlanHigherProfList;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.SecondaryProfList.EppEduPlanSecondaryProfList;

/**
 * @author oleyba
 * @since 8/28/14
 */
@Input({
    @Bind(key="orgUnitId", binding="orgUnitHolder.id"),
    @Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding="orgUnitHolder.id")
})
public class EppEduPlanListUI extends EppEduPlanListBaseUI
{
    @Override
    public void onComponentRefresh()
    {
        if (getProgramKindId() != null && _uiSupport.getChildUI("listComponentRegion") == null) {
            _uiActivation.asRegion(getListComponentName(), "listComponentRegion")
                .parameter("programKindId", getProgramKindId())
                .parameter("orgUnitId", getOrgUnitId())
                .activate();
        }
    }

    public void onChangeProgramKind() {
        saveSettings();
        for (IUIPresenter child : _uiSupport.getChildrenUI().values()) {
            child.getSettings().clear();
            child.deactivate();
        }
        if (getProgramKindId() != null) {
            _uiActivation.asRegion(getListComponentName(), "listComponentRegion")
                .parameter("programKindId", getProgramKindId())
                .parameter("orgUnitId", getOrgUnitId())
                .activate();
        }
        _uiSupport.setRefreshScheduled(true);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
    }

    public boolean isNothingSelected() {
        return getSettings().get("programKind") == null;
    }

    public String getListComponentName() {
        final EduProgramKind programKind = getSettings().get("programKind");
        if (programKind == null) {
            throw new IllegalArgumentException();
        }

        if (programKind.isProgramHigherProf()) {
            return EppEduPlanHigherProfList.class.getSimpleName();
        }
        if (programKind.isProgramSecondaryProf()) {
            return EppEduPlanSecondaryProfList.class.getSimpleName();
        }
        if (programKind.isProgramAdditionalProf()) {
            return EppEduPlanAdditionalProfList.class.getSimpleName();
        }

        throw new IllegalArgumentException();
    }

    public Long getProgramKindId() {
        EduProgramKind programKind = getSettings().get("programKind");
        return programKind == null ? null : programKind.getId();
    }

    public Long getOrgUnitId() {
        return getOrgUnit() == null ? null : getOrgUnit().getId();
    }

    @Override
    public ISecured getSecuredObject()
    {
        if (getOrgUnit() != null) return getOrgUnit();
        return super.getSecuredObject();
    }
}