package ru.tandemservice.uniepp.dao.eduplan;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EppEpvDSetEventListenerBean
{


    // http://tracker.tandemservice.ru/browse/DEV-336
    // при переходе по состояниям - переводить дисциплины, если они используются тольво в текущей УП(в)
    // УП(в) можно перевести в состояние "Согласовано", когда ко всем строкам УП(в) прикреплены согласованные элементы реестра (при этом нет фэйков)
    private static class EpvStateCheckDSetEventListener implements IDSetEventListener
    {
        private static final EpvStateCheckDSetEventListener INSTANCE = new EpvStateCheckDSetEventListener();

        private final boolean forwardRelatedDisciplines = false; /*Boolean.valueOf(ApplicationRuntime.getProperty(EpvStateCheckDSetEventListener.class.getName()+".forwardRelatedDisciplines", "false"))*/;

        @Override
        public void onEvent(final DSetEvent event) {
            if (EppEduPlanVersion.class.isAssignableFrom(event.getMultitude().getEntityMeta().getEntityClass()))
            {
                final Set affectedProperties = event.getMultitude().getAffectedProperties();
                if (affectedProperties.contains(EppRegistryElement.L_STATE)) {

                    // уп(в) на согласовании - нет фэйков, дисциплины указаны, дисциплины на согласовании или согласованы
                    // this.apply(event, "Нельзя отправить на согласование УП(в)", EppState.STATE_ACCEPTABLE, EppState.STATE_ACCEPTABLE, EppState.STATE_ACCEPTED);

                    // уп(в) согласнован - нет фейков, дисциплины указаны, дисциплины согласованы
                    this.apply(event, "Нельзя согласовать УП(в)", EppState.STATE_ACCEPTED, EppState.STATE_ACCEPTED);
                }
            }
        }

        private void apply(final DSetEvent event, final String targetStateErrorMessage, final String epvStateCode, final String ...allowRegElStateCodes)
        {
            // проверяем, что нет фэйков и для всех элементов указаны дисциплины
            {
                final List<Long> ids = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersion.class, "epv").column(property("epv.id"))
                .where(in(property("epv.id"), event.getMultitude().getInExpression()))
                .where(eq(property(EppEduPlanVersion.state().code().fromAlias("epv")), value(epvStateCode)))
                .where(
                    exists(
                        new DQLSelectBuilder()
                        .fromEntity(EppEpvRegistryRow.class, "rrow").column(property("rrow.id"))
                        .where(eq(property(EppEpvRegistryRow.owner().eduPlanVersion().id().fromAlias("rrow")), property("epv.id")))
                        .where(isNull(property(EppEpvRegistryRow.registryElement().id().fromAlias("rrow"))))
                        .buildQuery()
                    )
                )
                .createStatement(event.getContext()).<Long>list();

                if (ids.size() > 0) {
                    throw new ApplicationException(targetStateErrorMessage + ": не для всех строк указаны мероприятия реестра.", true);
                }
            }

            // обновляем состояние дисциплин, если они привязаны только к текущему УП(в)
            if (forwardRelatedDisciplines)
            {
                final int sz = new DQLUpdateBuilder(EppRegistryElement.class)

                .fromEntity(EppEpvRegistryRow.class, "rrow")
                .where(eq(property(EppEpvRegistryRow.registryElement().id().fromAlias("rrow")), property("id")))

                .fromEntity(EppEduPlanVersionBlock.class, "epvBlock")
                .where(eq(property(EppEduPlanVersionBlock.id().fromAlias("epvBlock")), property(EppEpvRegistryRow.owner().id().fromAlias("rrow"))))

                .fromEntity(EppEduPlanVersion.class, "epv")
                .where(eq(property(EppEduPlanVersion.id().fromAlias("epv")), property(EppEduPlanVersionBlock.eduPlanVersion().id().fromAlias("epvBlock"))))

                .where(in(property("epv.id"), event.getMultitude().getInExpression()))
                .where(eq(property(EppEduPlanVersion.state().code().fromAlias("epv")), value(epvStateCode)))

                .set(EppRegistryElement.L_STATE, property(EppEduPlanVersion.state().id().fromAlias("epv")))
                .where(ne(property(EppRegistryElement.L_STATE), property(EppEduPlanVersion.state().id().fromAlias("epv"))))
                .where(notIn(property(EppRegistryElement.L_STATE), Arrays.asList(allowRegElStateCodes)))

                .where(
                    notExists(
                        new DQLSelectBuilder()
                        .fromEntity(EppEpvRegistryRow.class, "x")
                        .column(property("x.id"))
                        .where(eq(property(EppEpvRegistryRow.registryElement().id().fromAlias("x")), property("id")))
                        .where(eq(property(EppEpvRegistryRow.owner().eduPlanVersion().id().fromAlias("x")), property("epv.id")))
                        .buildQuery()
                    )
                )

                .createStatement(event.getContext()).execute();

                if (sz > 0) {
                    // TODO: do something
                }
            }

            // проверяем, что дисциплины в нужных состояниях
            {
                final List<Long> ids = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersion.class, "epv").column(property("epv.id"))
                .where(in(property("epv.id"), event.getMultitude().getInExpression()))
                .where(eq(property(EppEduPlanVersion.state().code().fromAlias("epv")), value(epvStateCode)))
                .where(
                    exists(
                        new DQLSelectBuilder()
                        .fromEntity(EppEpvRegistryRow.class, "rrow").column(property("rrow.id"))
                        .where(eq(property(EppEpvRegistryRow.owner().eduPlanVersion().id().fromAlias("rrow")), property("epv.id")))
                        .where(notIn(property(EppEpvRegistryRow.registryElement().state().code().fromAlias("rrow")), Arrays.asList(allowRegElStateCodes)))
                        .buildQuery()
                    )
                )
                .createStatement(event.getContext()).<Long>list();

                if (ids.size() > 0) {
                    throw new ApplicationException(targetStateErrorMessage + ": не для всех строк мероприятия реестра согласованы.", true);
                }
            }
        }
    }

    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppEduPlanVersion.class, EpvStateCheckDSetEventListener.INSTANCE);
    }

}
