package ru.tandemservice.uniepp.ui;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.IRowCustomizer;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppCustomPlanRowWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.*;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public abstract class WorkPlanDataSourceGenerator extends LoadDataSourceGeneratorBase {

    private static final String TITLE__ACTION_DELETE_ROW = "Удалить запись";
    private static final String TITLE__ACTION_EDIT_ROW = "Редактировать запись";
    private static final String TITLE__ACTION_EDIT_ROW_PARTLOAD = "Редактировать нагрузку";

    private static final String TITLE__LOAD_TOTAL_SIZE = "Часов (всего)";
    private static final String TITLE__LOAD_TOTAL_LABOR = "Трудоемкость";

    private static final String TITLE__INDEX = "Индекс";
    private static final String TITLE__TITLE = "Название";
    private static final String TITLE__RETAKE = "Перезачет и переаттестация";
    private static final String TITLE__OWNER = "Читающее\nподразделение";
    private static final String TITLE__REGISTRY_NUMBER = "№ (реестр)";

    // load workplan
    protected abstract Long getWorkPlanBaseId();
    private final EppWorkPlanBase workplan = UniDaoFacade.getCoreDao().getNotNull(EppWorkPlanBase.class, this.getWorkPlanBaseId());

    private final EppGeneration generation = this.workplan.getEduPlan().getGeneration();
    private final boolean showTotalLabor = this.generation.showTotalLabor();


    @Override protected List<EppControlActionType> getPossibleControlActions() {
        return IEppWorkPlanDAO.instance.get().getActiveControlActionTypes(null);
    }

    // private final EppGeneration generation = this.workplan.getEduPlan().getGeneration();
    // private final boolean showTotalSize = this.generation.showTotalSize();
    // private final boolean showTotalLabor = this.generation.showTotalLabor();

    private final List<EppWorkPlanPart> workPlanPartsList = UniDaoFacade.getCoreDao().getList(EppWorkPlanPart.class, EppWorkPlanPart.workPlan().id().s(), this.workplan.getWorkPlan().getId(), EppWorkPlanPart.number().s());

    private final boolean useCyclicLoading = IEppSettingsDAO.instance.get().getGlobalSettings().isUseCyclicLoading();

    // then load wrapper
    private final IEppWorkPlanWrapper wrapper = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(Collections.singleton(this.workplan.getId())).get(this.workplan.getId());

    public void fillDisciplineDataSource(final StaticListDataSource<IEppWorkPlanRowWrapper> dataSource)
    {
        final IEntityHandler globalEditDisabler = LoadDataSourceGeneratorBase.NO_EDIT_DISABLER;

        final IEntityHandler detailEditDisabler = entity -> {
            final IEppWorkPlanRowWrapper row = (IEppWorkPlanRowWrapper) entity;
            return row.getRow() instanceof EppWorkPlanTemplateRow || globalEditDisabler.handle(entity);
        };

        dataSource.setRowCustomizer(wrapRowCustomizer(buildRowCustomizer()));
        dataSource.getColumns().clear();
        dataSource.setupRows(CollectionUtils.select(wrapper.getRowMap().values(), IEppWorkPlanRowWrapper.DISCIPLINE_ROWS));
        this.fillIndexTitleColumns(dataSource);

        if (this.editable) {
            dataSource.addColumn(this.getColumn_action(globalEditDisabler, WorkPlanDataSourceGenerator.TITLE__ACTION_EDIT_ROW, ActionColumn.EDIT, "onClickEditRow"));
        }


        {
            final HeadColumn loadTermHeadColumn = new HeadColumn("loadTerm", "Выдать часов за семестр");

            if (this.showTotalLabor) {
                loadTermHeadColumn.addColumn(getColumn_totalLabor());
            }
            loadTermHeadColumn.addColumn(vwrap(new SimpleColumn(WorkPlanDataSourceGenerator.TITLE__LOAD_TOTAL_SIZE, "row.regelpart.sizeAsDouble") {
                @Override public String getContent(final IEntity entity) {
                    final IEppRegElPartWrapper part = ((IEppWorkPlanRowWrapper)entity).getRegistryElementPart();
                    if (null == part) { return ""; }
                    return UniEppUtils.formatLoad(part.getItem().getSizeAsDouble(), false);
                }
            }));

            final HeadColumn loadAHeadColumn = new HeadColumn("aload", "Аудиторных");
            loadAHeadColumn.addColumn(vwrap(this.getColumn_partRowLoad_byEppELoadType(null /* сумма */, this.eLoadTotalAuditType)).setWidth(1));

            for (final EppALoadType aLoadType : this.aLoadTypes) {
                loadAHeadColumn.addColumn(vwrap(this.getColumn_partRowLoad_byEppALoadType(null /* сумма */, aLoadType, false)).setWidth(1));
            }
            loadTermHeadColumn.addColumn(wrap(loadAHeadColumn));

            /* сумма */
            this.eLoadTypes.stream()
                    .filter(eLoadType -> this.eLoadTotalAuditType != eLoadType)
                    .forEach(eLoadType -> loadTermHeadColumn.addColumn(vwrap(this.getColumn_partRowLoad_byEppELoadType(null /* сумма */, eLoadType)).setWidth(1)));

            for (final EppWorkPlanPart part : this.workPlanPartsList) {
                final String name = "part." + String.valueOf(part.getNumber());
                final String caption = String.valueOf(part.getNumber()) + "-ая часть";

                final HeadColumn partColumn = new HeadColumn(name, caption);
                for (final EppALoadType aLoadType : this.aLoadTypes) {
                    partColumn.addColumn(vwrap(this.getColumn_partRowLoad_byEppALoadType(part.getNumber() /* часть */, aLoadType, this.useCyclicLoading)).setWidth(1));
                }
                /* сумма */
                this.eLoadTypes.stream()
                        .filter(eLoadType -> this.eLoadTotalAuditType != eLoadType)
                        .forEach(eLoadType -> partColumn.addColumn(vwrap(this.getColumn_partRowLoad_byEppELoadType(part.getNumber() /* сумма */, eLoadType)).setWidth(1)));

                loadTermHeadColumn.addColumn(wrap(partColumn));
            }

            dataSource.addColumn(wrap(loadTermHeadColumn));
            if (this.editable) {
                dataSource.addColumn(this.getColumn_action(detailEditDisabler, WorkPlanDataSourceGenerator.TITLE__ACTION_EDIT_ROW_PARTLOAD, ActionColumn.EDIT, "onClickEditTotalPartLoad"));
            }
        }

        {
            final HeadColumn caHeadColumn = new HeadColumn("controlActions", "Формы контроля");
            final HeadColumn icaHeadColumn = new HeadColumn("intermediateActions", "Текущий контроль");
            final HeadColumn fcaHeadColumn = new HeadColumn("finalActions", "Итоговый контроль");

            for (final EppControlActionType tp : this.disciplineCATypes)  {
                (tp instanceof EppFControlActionType ? fcaHeadColumn : icaHeadColumn).addColumn(vwrap(this.getColumn_totalRowUsage_byActionType(tp)).setWidth(1));
            }
            if (icaHeadColumn.getColumns().size() > 0) {
                caHeadColumn.addColumn(wrap(icaHeadColumn).setWidth(1));
            }
            if (fcaHeadColumn.getColumns().size() > 0) {
                caHeadColumn.addColumn(wrap(fcaHeadColumn).setWidth(1));
            }

            dataSource.addColumn(wrap(caHeadColumn));
        }

        dataSource.addColumn(wrap(this.getColumn_owner(true)));
        dataSource.addColumn(wrap(this.getColumn_retake()));

        dataSource.addColumn(vwrap(new SimpleColumn("Вид дисциплины", EppWorkPlanRow.kind().shortTitle().s())).setClickable(false));

        if (this.editable) {
            dataSource.addColumn(this.getColumn_action(globalEditDisabler, WorkPlanDataSourceGenerator.TITLE__ACTION_EDIT_ROW, ActionColumn.EDIT, "onClickEditRow"));
            dataSource.addColumn(this.getColumn_action(globalEditDisabler, WorkPlanDataSourceGenerator.TITLE__ACTION_DELETE_ROW, ActionColumn.DELETE, "onClickDeleteRow").setAlert("Удалить строку?"));
        }
    }


    public void fillActionDataSource(final StaticListDataSource<IEppWorkPlanRowWrapper> dataSource)
    {
        final IEntityHandler globalEditDisabler = LoadDataSourceGeneratorBase.NO_EDIT_DISABLER;

        dataSource.setRowCustomizer(wrapRowCustomizer(buildRowCustomizer()));
        dataSource.getColumns().clear();
        dataSource.setupRows(CollectionUtils.select(wrapper.getRowMap().values(), IEppWorkPlanRowWrapper.NON_DISCIPLINE_ROWS));
        this.fillIndexTitleColumns(dataSource);

        if (this.showTotalLabor) {
            dataSource.addColumn(getColumn_totalLabor());
        }

        final HeadColumn dateFromToColumn = new HeadColumn("dateFromTo", "Время проведения");
        dateFromToColumn.addColumn(wrap(new SimpleColumn("Дата начала", "beginDate") {
            @Override public String getContent(final IEntity entity) {
                final IEppWorkPlanRow row = ((IEppWorkPlanRowWrapper)entity).getRow();
                return DateFormatter.DEFAULT_DATE_FORMATTER.format(((EppWorkPlanRow)row).getBeginDate());
            }
        }));
        dateFromToColumn.addColumn(wrap(new SimpleColumn("Дата окончания", "endDate") {
            @Override public String getContent(final IEntity entity) {
                final IEppWorkPlanRow row = ((IEppWorkPlanRowWrapper)entity).getRow();
                return DateFormatter.DEFAULT_DATE_FORMATTER.format(((EppWorkPlanRow)row).getEndDate());
            }
        }));
        dataSource.addColumn(wrap(dateFromToColumn));

        dataSource.addColumn(wrap(new SimpleColumn("Контрольное мероприятие", "actionList") {
            @Override public String getContent(final IEntity entity) {
                final IEppWorkPlanRowWrapper row = (IEppWorkPlanRowWrapper)entity;
                if (row.getRow() instanceof EppWorkPlanRegistryElementRow) {
                    final List<EppControlActionType> actionList = WorkPlanDataSourceGenerator.this.actionsCATypes.stream()
                            .filter(tp -> row.getActionSize(tp.getFullCode()) > 0)
                            .collect(Collectors.toList());
                    return CollectionFormatter.COLLECTION_FORMATTER.format(actionList);
                }
                return  "";
            }
        }));

        dataSource.addColumn(wrap(this.getColumn_owner(true)));
        dataSource.addColumn(wrap(this.getColumn_retake()));

        if (this.editable) {
            dataSource.addColumn(this.getColumn_action(globalEditDisabler, WorkPlanDataSourceGenerator.TITLE__ACTION_EDIT_ROW, ActionColumn.EDIT, "onClickEditActionElement"));
            dataSource.addColumn(this.getColumn_action(globalEditDisabler, WorkPlanDataSourceGenerator.TITLE__ACTION_DELETE_ROW, ActionColumn.DELETE, "onClickDeleteRow").setAlert("Удалить строку?"));
        }
    }

    protected IRowCustomizer<IEppWorkPlanRowWrapper> buildRowCustomizer() {
        return new SimpleRowCustomizer<IEppWorkPlanRowWrapper>() {
            @Override public String getRowStyle(final IEppWorkPlanRowWrapper row) {
                final StringBuilder sb = new StringBuilder();
                if ((row.getRow() instanceof EppWorkPlanTemplateRow) || isEmpty(row)) {
                    sb.append("background-color: " + UniDefines.COLOR_ERROR + ";");
                }
                return sb.toString();
            }

            protected boolean isEmpty(final IEppWorkPlanRowWrapper row)
            {
                // проверяем аудиторную нагрузку
                for (String fullCode: EppALoadType.FULL_CODES) {
                    double load = row.getTotalPartLoad(null, fullCode);
                    if (!UniEppUtils.eq(load, 0d)) { return false; }
                }

                // проверяем только итоговый контроль (текущий контроль проверять смысла нет)
                for (String code: EppFControlActionTypeCodes.CODES) {
                    double load = row.getActionSize(EppFControlActionType.getFullCode(code));
                    if (!UniEppUtils.eq(load, 0d)) { return false; }
                }

                return true;
            }
        };
    }


    protected void fillIndexTitleColumns(final StaticListDataSource<IEppWorkPlanRowWrapper> dataSource)
    {
        if (this.editable) {
            dataSource.addColumn(UniEppUtils.getStateColumn("row."+EppWorkPlanRegistryElementRow.L_REGISTRY_ELEMENT_PART));
        }

        dataSource.addColumn(wrap(new SimpleColumn(WorkPlanDataSourceGenerator.TITLE__INDEX, "index") {
            @Override public String getContent(final IEntity entity) {
                return ((IEppWorkPlanRowWrapper)entity).getNumber();
            }
        }.setWidth(1)));

        dataSource.addColumn(wrap(
            new PublisherLinkColumn(WorkPlanDataSourceGenerator.TITLE__TITLE, "displayableTitle")
            .setResolver(new SimplePublisherLinkResolver(EppRegistryElementPart.registryElement().id().fromAlias("registryElementPart.item")))
            .setWidth(30)
        ));

        dataSource.addColumn(vwrap(getColumn_registryNumber()).setWidth(1));
    }


    // номер в реестре
    public static PublisherLinkColumn getColumn_registryNumber() {
        final PublisherLinkColumn column = new PublisherLinkColumn(WorkPlanDataSourceGenerator.TITLE__REGISTRY_NUMBER, "registryNumber") {
            @Override public List<IEntity> getEntityList(final IEntity entity) {
                final IEppWorkPlanRowWrapper wrapper = (IEppWorkPlanRowWrapper)entity;
                if (wrapper.getRow() instanceof EppWorkPlanRegistryElementRow) {
                    final EppRegistryElement registryElement = ((EppWorkPlanRegistryElementRow)wrapper.getRow()).getRegistryElement();
                    if (null != registryElement) {
                        return Collections.<IEntity>singletonList(registryElement);
                    }
                }
                return Collections.emptyList();
            }
            @Override public String getContent(final IEntity entity) {
                return this.getFormatter().format(((EppRegistryElement)entity).getNumberWithAbbreviation());
            }
        };
        column.setFormatter(UniEppUtils.NOBR_COMMA_FORMATTER);
        return column;
    }

    // Трудоемкость части элемента реестра
    public static AbstractColumn getColumn_totalLabor() {
        return vwrap(new SimpleColumn(WorkPlanDataSourceGenerator.TITLE__LOAD_TOTAL_LABOR, "row.regelpart.laborAsDouble") {
            @Override public String getContent(final IEntity entity) {
                final IEppRegElPartWrapper part = ((IEppWorkPlanRowWrapper) entity).getRegistryElementPart();
                if (null == part) {
                    return "";
                }
                return UniEppUtils.formatLoad(part.getItem().getLaborAsDouble(), false);
            }
        });
    }

    // распределение теоретической нагрузки по частям - по видам аудиторной нагрузки
    protected SimpleColumn getColumn_partRowLoad_byEppALoadType(final Integer part, final EppALoadType aLoadType, final boolean useCycleLoad)
    {
        return new SimpleColumn(aLoadType.getShortTitle(), aLoadType.getFullCode())
        {
            @Override public boolean isRawContent() { return true; }

            @Override public String getContent(final IEntity entity)
            {
                final IEppWorkPlanRowWrapper row = (IEppWorkPlanRowWrapper)entity;
                final double partLoad = row.getTotalPartLoad(part, aLoadType.getFullCode());

                if (null == part) {
                    // сумматрая нагрузка (здесь ВСЕГДА нет цикловой нагрузки)
                    final String calculatedLoad = UniEppUtils.formatLoad(partLoad, false);

                    final IEppRegElPartWrapper regElPart = row.getRegistryElementPart();
                    if (null == regElPart) { return calculatedLoad; /* нет элемента реестра */ }

                    final String registryLoad = UniEppUtils.formatLoad(regElPart.getLoadAsDouble(aLoadType.getFullCode()), false);
                    if (calculatedLoad.equals(registryLoad)) { return calculatedLoad; }

                    // выводим данные по строке (если отличаются, то данные реестра)
                    return LoadDataSourceGeneratorBase.error(calculatedLoad /*в документе - как сумма по строке*/, registryLoad /*в реестре*/);
                }

                final String formatLoad = UniEppUtils.formatLoad(partLoad, true);
                if (useCycleLoad) {
                    // цикловая нагрузка
                    final double partDays = row.getTotalPartLoad(part, aLoadType.getFullCode() + IEppWorkPlanRowWrapper.DAYS_LOAD_POSTFIX);
                    if ((partLoad <= 0) && (partDays <= 0)) { return ""; }
                    final String formatDays = (partDays <= 0) ? "" : "/" + UniEppUtils.formatLoad(partDays, false);
                    return formatLoad + formatDays;
                }

                return formatLoad;
            }
        };
    }

    // распределение учебной нагрузки по частям
    protected SimpleColumn getColumn_partRowLoad_byEppELoadType(final Integer part, final EppELoadType aLoadType)
    {
        return new SimpleColumn(aLoadType.getShortTitle(), aLoadType.getFullCode())
        {
            @Override
            public String getContent(final IEntity entity)
            {
                final IEppWorkPlanRowWrapper row = (IEppWorkPlanRowWrapper)entity;
                return UniEppUtils.formatLoad(row.getTotalPartLoad(part, aLoadType.getFullCode()), null != part);
            }
        };
    }

    // количество мероприятий данного типа для данной дисциплины
    protected SimpleColumn getColumn_totalRowUsage_byActionType(final EppControlActionType tp)
    {
        final String fullCode = tp.getFullCode();
        return new SimpleColumn(tp.getShortTitle(), fullCode)
        {
            @Override
            public String getContent(final IEntity entity)
            {
                final IEppWorkPlanRowWrapper row = (IEppWorkPlanRowWrapper)entity;
                final int actionSize = row.getActionSize(fullCode);
                if (actionSize > 0)
                {
                    return String.valueOf(actionSize);
                }
                return "";
            }
        };
    }

    // читающее подразделение
    protected SimpleColumn getColumn_owner(final boolean fullTitle) {
        return new SimpleColumn(WorkPlanDataSourceGenerator.TITLE__OWNER, "owner") {
            @Override public String getContent(final IEntity entity) {
                final OrgUnit owner = ((IEppWorkPlanRowWrapper)entity).getOwner();
                if (owner instanceof TopOrgUnit) { return ""; } // не показывать title для академии - это все равно фэйк
                if (fullTitle) {
                    return WorkPlanDataSourceGenerator.this.getOrgUnitFullTitle(owner);
                } else {
                    return WorkPlanDataSourceGenerator.this.getOrgUnitShortTitle(owner);
                }
            }
        };
    }

    // Перезачет и переаттестация
    protected SimpleColumn getColumn_retake() {
        return new SimpleColumn(WorkPlanDataSourceGenerator.TITLE__RETAKE, "retake") {
            @Override public String getContent(final IEntity entity) {
                IEppWorkPlanRowWrapper row = (IEppWorkPlanRowWrapper) entity;
                if (row.isReattestation()) {
                    return IEppCustomPlanRowWrapper.REATTESTATION_DONE_TITLE;
                } else if (row.isReexamination()) {
                    return IEppCustomPlanRowWrapper.REEXAMINATION_DONE_TITLE;
                }
                return "";
            }
        };
    }
}
