package ru.tandemservice.uniepp.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uniepp.entity.catalog.EppState;

/**
 * @author vdanilov
 */
public interface IEppStateObject extends IEntity, ITitled {

    final String FIELD_STATE = "state";
    final String FIELD_COMMENT = "comment";

    /**
     * @return состояние объекта
     */
    EppState getState();
}
