/* $Id$ */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionRegElementCheckList;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 5/23/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
		final List<EppRegistryElement> regElList = new DQLSelectBuilder()
				.fromEntity(EppEpvRegistryRow.class, "r")
				.column(property("r", EppEpvRegistryRow.registryElement()))
				.where(eq(property("r", EppEpvRegistryRow.owner().eduPlanVersion().id()), value(model.getId())))
				.createStatement(getSession()).list();

        final Map<Long, Model.RowWrapper> wrapperMap = new HashMap<>();
        for (final EppRegistryElement disc: regElList) {
            wrapperMap.put(disc.getId(), new Model.RowWrapper(disc));
        }

        this.checkDiscList(wrapperMap);

        final StaticListDataSource<Model.RowWrapper> dataSource = model.getDataSource();
        final List<Model.RowWrapper> wrapperList = new ArrayList<>(wrapperMap.values());
        Collections.sort(wrapperList, ITitled.TITLED_COMPARATOR);
        dataSource.setupRows(wrapperList);
        dataSource.clearColumns();

        dataSource.addColumn(new PublisherLinkColumn("Дисциплина", "title").setResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(final IEntity entity)
            {
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, entity.getProperty("id")).add("showAsPub", true);
            }

            @Override
            public String getComponentName(final IEntity entity)
            {
                return "ru.tandemservice.uniepp.component.registry.RegElementEduPlanRowVariations";
            }
        }).setClickable(true).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Есть в УП(в)", "versionCount").setClickable(false).setOrderable(false));

        final AbstractColumn stateColumn = new SimpleColumn("Различия в чтении в разных УП(в)", "state").setClickable(false).setOrderable(false);
        stateColumn.setStyleResolver(rowEntity -> {
            if ((rowEntity instanceof Model.RowWrapper) && ((Model.RowWrapper) rowEntity).isHasErrors()) {
                return "background-color: #FAB5B0;";
            }
            return "";
        });
        dataSource.addColumn(stateColumn);

    }

    private void checkDiscList(final Map<Long, Model.RowWrapper> wrapperMap)
    {
        final Session session = this.getSession();

        // получим версии УП для всех дисциплин
        final Map<Long, Set<Long>> blockIdsMap = new HashMap<>();

        final DQLSelectBuilder discRowDQL = new DQLSelectBuilder()
				.fromEntity(EppEpvRegistryRow.class, "row")
				.column(property("row", EppEpvRegistryRow.registryElement().id()))
				.column(property("row", EppEpvRegistryRow.owner().id()))
				.where(in(property("row", EppEpvRegistryRow.registryElement().id()), wrapperMap.keySet()));
        for (final Object[] row : discRowDQL.createStatement(session).<Object[]>list()) {
            SafeMap.safeGet(blockIdsMap, (Long) row[0], HashSet.class).add((Long) row[1]);
        }

        // получим строки этих версий в готовом виде
        final Set<Long> blockIds = new HashSet<>();
        for (final Set<Long> set : blockIdsMap.values()) {
            blockIds.addAll(set);
        }
        final Map<Long, IEppEpvBlockWrapper> blockWrapperMap = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(blockIds, true);

        for (final Model.RowWrapper wrapper : wrapperMap.values()) {
            this.checkDisc(wrapper, blockIdsMap.get(wrapper.getId()), blockWrapperMap);
        }
    }

    private void checkDisc(final Model.RowWrapper discWrapper, final Set<Long> blockIds, final Map<Long, IEppEpvBlockWrapper> blockWrapperMap)
    {
        // вычислим все семестры, в каких дисциплина читалась
        int termCount = 0;
        for (final IEppEpvBlockWrapper blockWrapper: blockWrapperMap.values())
        {
            for (final IEppEpvRowWrapper rowWrapper : blockWrapper.getRowMap().values())
            {
                if (!this.checkDisc(discWrapper.getId(), rowWrapper)) {
                    continue;
                }
                termCount = Math.max(termCount, rowWrapper.getActiveTermSet().size());
            }
        }

        // проверяем, одинаково ли читается во всех УПв
        String key = null;
        boolean hasErrors = false;
        int versionCount = 0;
        for (final long blockId: blockWrapperMap.keySet())
        {
            if (!blockIds.contains(blockId))
                continue;

			final IEppEpvBlockWrapper blockWrapper = blockWrapperMap.get(blockId);
            for (final IEppEpvRowWrapper rowWrapper : blockWrapper.getRowMap().values())
            {
                if (!this.checkDisc(discWrapper.getId(), rowWrapper)) {
                    continue;
                }
                versionCount++;
                final String complexKey = this.getComplexKey(termCount, rowWrapper);
                if (null == key) {
                    key = complexKey;
                } else if (!key.equals(complexKey)) {
                    hasErrors = true;
                }
            }
        }

        discWrapper.setHasErrors(hasErrors);
        discWrapper.setVersionCount(versionCount);
    }

    private String getComplexKey(final int termCount, final IEppEpvRowWrapper rowWrapper)
    {
        final StringBuilder result = new StringBuilder();
        final Iterator<Integer> termIterator = rowWrapper.getActiveTermSet().iterator();
        for (Integer part = 1; part <= termCount; part++) {
            if (termIterator.hasNext()) {
                result.append(rowWrapper.getTermKey(termIterator.next())).append(".");
            } else {
                result.append(rowWrapper.getTermKey(null)).append(".");
            }
        }
        return result.toString();
    }

    private EppRegistryElement getDisc(final IEppEpvRowWrapper rowWrapper)
    {
        final IEppEpvRow row = rowWrapper.getRow();
        if (row instanceof EppEpvRegistryRow) {
            return ((EppEpvRegistryRow)row).getRegistryElement();
        }
        return null;
    }


    private boolean checkDisc(final Long eppRegElementId, final IEppEpvRowWrapper rowWrapper)
    {
        final EppRegistryElement disc = this.getDisc(rowWrapper);
        return (null != disc) && eppRegElementId.equals(disc.getId());
    }
}
