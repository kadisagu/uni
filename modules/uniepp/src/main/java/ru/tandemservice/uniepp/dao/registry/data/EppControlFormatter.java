package ru.tandemservice.uniepp.dao.registry.data;

import java.util.Map;

import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;

/**
 * @author vdanilov
 */
public abstract class EppControlFormatter<T> {

    private final Map<String, EppControlActionType> controlActionTypeMap;
    public EppControlActionType getControlAction(String actionFullCode) { return controlActionTypeMap.get(actionFullCode); }

    public EppControlFormatter(Map<String, EppControlActionType> controlActionTypeMap) {
        this.controlActionTypeMap = controlActionTypeMap;
    }

    public EppControlFormatter() {
        this(EppEduPlanVersionDataDAO.getActionTypeMap());
    }

    protected boolean allow(T element, EppControlActionType actionType) { return true; }
    protected abstract int size(T element, String actionFullCode);

    public String format(T element)
    {
        final StringBuilder result = new StringBuilder();
        for (final EppControlActionType controlActionType : controlActionTypeMap.values()) {
            if (allow(element, controlActionType)) {
                final int actionSize = size(element, controlActionType.getFullCode());
                if (actionSize <= 0) { continue; }
                if (result.length() > 0) { result.append(", "); }
                result.append(controlActionType.getShortTitle());
                if (actionSize > 1) { result.append("-").append(actionSize); }
            }
        }
        return result.toString();
    }

}
