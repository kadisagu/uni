package ru.tandemservice.uniepp.entity.student.group;


/**
 * @author vdanilov
 */
public class EppRealEduGroupTitleUtil {

    /**
     * Полное название УГС (положение в траектории + данные самой УГС)
     * @return [activity-title], [название УГС], [согласование]
     **/
    public static String getFullTitle(EppRealEduGroup group) {
        StringBuilder sb = new StringBuilder(group.getActivityTitle()).append(", ").append(group.getTitle());
        if (null != group.getLevel()) { sb.append(", ").append(group.getLevel().getDisplayableShortTitle()); }
        return sb.toString();
    }

    /**
     * Название УГС как элемента тракетории
     * @return [Название дисциплины] ([нагрузка в часах]ч., [номер части]/[число частей], [сокращение типа мероприятия реестра] №[номер]), [часть года] [год], [вид нагрузки]
     **/
    public static String getActivityTitle(EppRealEduGroup group) {
        return new StringBuilder(group.getActivityPart().getTitleWithNumber())
        .append(", ").append(group.getSummary().getYearPart().getPart().getShortTitle()).append(" ").append(group.getSummary().getYearPart().getYear().getTitle())
        .append(", ").append(group.getType().getAbbreviation())
        .toString();
    }

}
