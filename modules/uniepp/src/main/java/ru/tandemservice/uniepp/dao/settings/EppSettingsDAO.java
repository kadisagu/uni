package ru.tandemservice.uniepp.dao.settings;

import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.transaction.DaoCache;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp.base.bo.EppSettings.logic.EppSettingsWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.settings.EppPlanOrgUnit;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.uniepp.entity.settings.gen.EppELoadWeekTypeGen;

import java.util.*;

/**
 *
 * @author nkokorina
 *
 */
public class EppSettingsDAO extends UniBaseDao implements IEppSettingsDAO
{
    private static final String CACHE_SETTINGS_ELOAD_WEEK_TYPE = EppSettingsDAO.class.getName()+".eload-week-type-map";
    private static final String CACHE_EPP_GLOBAL_SETTINGS = EppSettingsDAO.class.getSimpleName()+".epp-global-settings";

    @Override
    public EppSettingsWrapper getGlobalSettings()
    {
        EppSettingsWrapper settingsWrapper = DaoCache.get(CACHE_EPP_GLOBAL_SETTINGS);
        if (settingsWrapper == null)
        {
            settingsWrapper = new EppSettingsWrapper(true);
            DaoCache.put(CACHE_EPP_GLOBAL_SETTINGS, settingsWrapper);
        }
        return settingsWrapper;
    }

    @Override
    public void saveGlobalSettings(EppSettingsWrapper settingsWrapper)
    {
        DataSettingsFacade.saveSettings(settingsWrapper.getDataSettings());
        DaoCache.put(CACHE_EPP_GLOBAL_SETTINGS, null);
    }

    @SuppressWarnings("unchecked")
    private static <T> T cache_get(final String key) {
        return (T)(ContextLocal.isRun() ? ContextLocal.getRequestAttribute(key) : null);
    }


    private static <T> T cache_set(final String key, final T value) {
        if (ContextLocal.isRun()) { ContextLocal.setRequestAttribute(key, value); }
        return value;
    }

    @Override
    public boolean isShowEppOrgUnitTab(final Long orgUnitId)
    {
        final String key = "EppSettingsDAO.isShowEppOrgUnitTab."+orgUnitId;
        final Boolean result = cache_get(key);
        if (null != result) { return result; }

        Debug.begin("isShowEppOrgUnitTab");
        try {
            return cache_set(
                key,
                this.isTutorOrgUnit(orgUnitId) ||
                this.isPlanOrgUnit(orgUnitId)
            );
        } finally {
            Debug.end();
        }
    }

    @Override
    public boolean isShowRealGroupOrgUnitTab(final Long orgUnitId)
    {
        final String key = "EppSettingsDAO.isShowRealGroupOrgUnitTab."+orgUnitId;
        final Boolean result = cache_get(key);
        if (null != result) { return result; }

        Debug.begin("isShowRealGroupOrgUnitTab");
        try {
            return cache_set(
                key,
                this.isTutorOrgUnit(orgUnitId) ||
                this.isGroupOrgUnit(orgUnitId) ||
                this.isOperationOrgUnit(orgUnitId)
            );
        } finally {
            Debug.end();
        }
    }


    @Override
    public boolean isGroupOrgUnit(final Long orgUnitId)
    {
        final String key = "EppSettingsDAO.isGroupOrgUnit."+orgUnitId;
        final Boolean result = cache_get(key);
        if (null != result) { return result; }

        Debug.begin("isGroupOrgUnit");
        try {
            return cache_set(
                key,
                existsEntity(EducationOrgUnit.class, EducationOrgUnit.L_GROUP_ORG_UNIT, orgUnitId)
            );
        } finally {
            Debug.end();
        }
    }

    @Override
    public boolean isOperationOrgUnit(final Long orgUnitId)
    {
        final String key = "EppSettingsDAO.isOperationOrgUnit."+orgUnitId;
        final Boolean result = cache_get(key);
        if (null != result) { return result; }

        Debug.begin("isOperationOrgUnit");
        try {
            return cache_set(
                key,
                existsEntity(EducationOrgUnit.class, EducationOrgUnit.L_OPERATION_ORG_UNIT, orgUnitId)
            );
        } finally {
            Debug.end();
        }
    }

    @Override
    public boolean isPlanOrgUnit(final Long orgUnitId)
    {
        final String key = "EppSettingsDAO.isPlanOrgUnit."+orgUnitId;
        final Boolean result = cache_get(key);
        if (null != result) { return result; }

        Debug.begin("isPlanOrgUnit");
        try {
            return cache_set(
                key,
                orExistsEntity(
                        // подразделение УП (настройка)
                        DQLExpressions.exists(EppPlanOrgUnit.class, EppPlanOrgUnit.L_ORG_UNIT, orgUnitId),
                        // подразделение УП (факт)
                        DQLExpressions.exists(EppEduPlan.class, EppEduPlan.L_OWNER, orgUnitId)
                )
            );
        } finally {
            Debug.end();
        }
    }


    @Override
    public boolean isTutorOrgUnit(final Long orgUnitId)
    {
        final String key = "EppSettingsDAO.isTutorOrgUnit."+orgUnitId;
        final Boolean result = cache_get(key);
        if (null != result) { return result; }

        Debug.begin("isTutorOrgUnit");
        try {
            return cache_set(
                key,
                orExistsEntity(
                        // обучающие подразделения (настройка)
                        DQLExpressions.exists(EppTutorOrgUnit.class, EppTutorOrgUnit.L_ORG_UNIT, orgUnitId),
                        // обучающие подразделения (факт)
                        DQLExpressions.exists(EppRegistryElement.class, EppRegistryElement.L_OWNER, orgUnitId)
                )
            );
        } finally {
            Debug.end();
        }
    }


    @Override
    public Map<String, Map<String, Set<String>>> getELoadWeekTypeMap() {
        Map<String, Map<String, Set<String>>> cache = DaoCache.get(EppSettingsDAO.CACHE_SETTINGS_ELOAD_WEEK_TYPE);
        if (null == cache) {
            DaoCache.put(EppSettingsDAO.CACHE_SETTINGS_ELOAD_WEEK_TYPE, cache = new HashMap<>(4));

            final MQBuilder builder = new MQBuilder(EppELoadWeekTypeGen.ENTITY_CLASS, "rel", new String[] {
                EppELoadWeekTypeGen.developForm().code().s(),
                EppELoadWeekTypeGen.loadType().code().s(),
                EppELoadWeekTypeGen.weekType().code().s()
            });
            final List<Object[]> rows = builder.getResultList(this.getSession());
            for (final Object[] row: rows) {
                SafeMap.safeGet(SafeMap.safeGet(cache, (String)row[0], HashMap.class), (String)row[1], HashSet.class).add((String)row[2]);
            }
        }
        return cache;
    }


    @Override
    public Map<String, EppWeekType> getWeekTypeEditAbbreviationMap() {
        final String CACHE_KEY_EPP_LOAD_TYPE_MAP = "epp-week-type-map";
        Map<String, EppWeekType> loadTypeMap = DaoCache.get(CACHE_KEY_EPP_LOAD_TYPE_MAP);
        if (null == loadTypeMap) {
            DaoCache.put(CACHE_KEY_EPP_LOAD_TYPE_MAP, loadTypeMap = new LinkedHashMap<>());
            for (final EppWeekType type : UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppWeekType.class)) {
                if (null != loadTypeMap.put(type.getAbbreviationEdit().toUpperCase(), type)) {
                    throw new IllegalStateException("Duplicate weekType abbreviationEdit: " + type.getAbbreviationEdit());
                }
            }
        }
        return loadTypeMap;
    }


}
