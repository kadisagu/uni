
package ru.tandemservice.uniepp.base.bo.EppContract.ui.AddStudentContract;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author vdanilov
 */
@Configuration
public class EppContractAddStudentContract extends BusinessComponentManager {

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder().create();
    }

}

