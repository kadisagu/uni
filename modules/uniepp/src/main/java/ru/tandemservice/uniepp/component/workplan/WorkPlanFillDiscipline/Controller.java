/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.workplan.WorkPlanFillDiscipline;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;

/**
 * @author nkokorina
 * Created on: 06.08.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().update(model);
        this.deactivate(component, new ParametersMap().add(Model.RETURN_SUCCESS_PARAM, 1));
    }
}
