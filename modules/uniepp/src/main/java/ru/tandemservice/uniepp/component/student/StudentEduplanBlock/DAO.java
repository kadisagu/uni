package ru.tandemservice.uniepp.component.student.StudentEduplanBlock;

import org.apache.commons.collections15.Predicate;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.ui.EduPlanVersionBlockDataSourceGenerator;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        final boolean colorizeParts = true;

        model.setStudent(this.getNotNull(Student.class, model.getStudent().getId()));
        model.setRelation(IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(model.getStudent().getId()));

        if (null != model.getRelation() && null != model.getRelation().getBlock())
        {
            final EppEduPlanVersionBlock block = model.getRelation().getBlock();
            final boolean isExtendedView = model.getRelation().getEduPlanVersion().getViewTableDiscipline().isExtend();

            final Map<Integer, Map<String, Boolean>> part2epvRowsMap = SafeMap.get(HashMap.class);
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppStudentWorkPlanElement.class, "slot")
                .where(eq(property(EppStudentWorkPlanElement.student().fromAlias("slot")), value(model.getRelation())))
                .where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias("slot"))))
                .column(property(EppStudentWorkPlanElement.registryElementPart().number().fromAlias("slot")))
                .column(property(EppStudentWorkPlanElement.registryElementPart().registryElement().id().fromAlias("slot")))
                .column(property(EppStudentWorkPlanElement.sourceRow().number().fromAlias("slot")))
                .column(property(EppStudentWorkPlanElement.sourceRow().needRetake().fromAlias("slot")));

                for (final Object[] row: CommonDAO.scrollRows(dql.createStatement(this.getSession()))) {
                    final Map<String, Boolean> epvRowMap = part2epvRowsMap.get(row[0]);
                    epvRowMap.put(String.valueOf(row[1]), Boolean.TRUE.equals(row[3]));
                    epvRowMap.put(String.valueOf(row[2]), null == row[3]);
                }
            }

            final EduPlanVersionBlockDataSourceGenerator dataSourceGenerator = new EduPlanVersionBlockDataSourceGenerator() {
                @Override public boolean isCheckMode() { return false; /* для студента не важно, верен ли план; важно, что он привязан к стеденту таким, как его утвердили в УМУ */ }
                @Override public boolean isExtendedView() { return isExtendedView; }
                @Override protected boolean isEditable() { return false; /* no edit actions */ }
                @Override protected String getPermissionKeyEdit() { return null; /* no edit actions */ }
                @Override protected Long getVersionBlockId() { return block.getId(); }

                @Override protected AbstractColumn wrapTermLoadColumn(final DevelopGridTerm term, final AbstractColumn column) {
                    if (colorizeParts) {
                        column.setStyleResolver(new IStyleResolver() {
                            @Override public String getStyle(final IEntity rowEntity) {
                                final IEppEpvRowWrapper rowWrapper = (IEppEpvRowWrapper)rowEntity;
                                if (rowWrapper.getRow() instanceof IEppEpvTermDistributedRow) {
                                    if (rowWrapper.hasInTermActivity(term.getTermNumber())) {
                                        if (rowWrapper.getRow() instanceof EppEpvRegistryRow) {
                                            final int partNumber = rowWrapper.getActiveTermNumber(term.getTermNumber());
                                            final Boolean state = this.getState(rowWrapper, part2epvRowsMap.get(partNumber));

                                            if (null != state) {
                                                if (Boolean.FALSE.equals(state)) {
                                                    // покрыто строкой МСРП - красим в ЗЕЛЕНЫЙ
                                                    return "background-color:"+ UniDefines.COLOR_GREEN;
                                                }
                                                // покрыто МСРП, но не в рамках данного учебного процесса - красим в СИНЕ-ЗЕЛЕНЫЙs
                                                return "background-color:"+ UniDefines.COLOR_CYAN;
                                            }
                                            if (term.getCourseNumber() > model.getStudent().getCourse().getIntValue()) {
                                                // не покрыто, но курс еще не наступил - красим в СИНИЙ
                                                return "background-color:"+ UniDefines.COLOR_BLUE;
                                            }
                                            // курс уже пришел, а у студента нет строки - красим в КРАСНЫЙ
                                            return "background-color:"+UniDefines.COLOR_RED;
                                        }
                                        // если не дисциплина, но есть нагрузка - рисуем СИНИЙ
                                        return "background-color:"+UniDefines.COLOR_BLUE;
                                    }
                                }
                                return null;
                            }

                            protected Boolean getState(final IEppEpvRowWrapper rowWrapper, final Map<String, Boolean> epvRowMap)
                            {
                                // сначала пробуем по id дисциплины
                                if (rowWrapper.getRow() instanceof EppEpvRegistryRow) {
                                    final EppEpvRegistryRow row = (EppEpvRegistryRow)rowWrapper.getRow();
                                    final EppRegistryElement registryElement = row.getRegistryElement();
                                    if (null != registryElement) {
                                        final Boolean stateByRegElement = epvRowMap.get(String.valueOf(registryElement.getId()));
                                        if (null != stateByRegElement) { return stateByRegElement; }
                                    }
                                }

                                // если ничего не получилось - по индексу
                                return epvRowMap.get(rowWrapper.getIndex());
                            }
                        });
                    }
                    return column;
                }
            };

            @Deprecated
            final Predicate<IEppEpvRowWrapper> predicate = object -> object.getOwner().isRootBlock() || object.getOwner().equals(model.getRelation().getBlock());

            ///////////////////////////
            // данные по дисциплинам //
            ///////////////////////////
            {
                dataSourceGenerator.doPrepareBlockDisciplineDataSource(model.getDataSource(), new EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext() {
                    @Override public IEntityHandler getAdditionalEditDisabler() { return null; /* no edit actions */ }
                    @Override public Collection<IEppEpvRowWrapper> getBlockRows(final IEppEpvBlockWrapper versionWrapper) {
                        return EppEduPlanVersionDataDAO.filterEduPlanBlockRows(versionWrapper.getRowMap(), object -> (predicate.evaluate(object) && IEppEpvRowWrapper.DISCIPLINES_ROWS.evaluate(object)), true);
                    }
                });
            }

            ////////////////////////////
            // данные по мероприятиям //
            ////////////////////////////
            {
                dataSourceGenerator.doPrepareBlockActionsDataSource(model.getEppActionsDataSource(), new EduPlanVersionBlockDataSourceGenerator.IBlockRenderContext() {
                    @Override public IEntityHandler getAdditionalEditDisabler() { return null; /* no edit actions */ }
                    @Override public Collection<IEppEpvRowWrapper> getBlockRows(final IEppEpvBlockWrapper versionWrapper) {
                        return EppEduPlanVersionDataDAO.filterEduPlanBlockRows(versionWrapper.getRowMap(), object -> (predicate.evaluate(object) && IEppEpvRowWrapper.ACTIONS_ROWS.evaluate(object)), true);
                    }
                });
            }
        }
    }
}
