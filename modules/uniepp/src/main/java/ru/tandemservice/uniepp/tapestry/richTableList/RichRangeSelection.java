/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.tapestry.richTableList;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Клас для управления границами набора отрезков
 * Для демонстрации смотри метод main
 * Если где-то еще понадобится, то можно перенести в ядро уни
 *
 * @author vip_delete
 * @since 04.03.2010
 */
public class RichRangeSelection
{
    public static final int EMPTY = 0;
    public static final int LEFT = 1;
    public static final int RIGHT = 2;
    public static final int BOTH = 3;
    public static final int ACTIVE = 4;

    protected final int[] _points; // пары точек         [(1,3),(5,8)]
    protected final int[] _data;   // логическая карта   [.(.).(..)..], len = 11
    protected final int[] _index;  // индексная карта    [-1,0,-1,1,-1,2,-1,-1,3,-1,-1], len = 11
    protected int _activePointIndex = -1;


    public RichRangeSelection(final int count, final int[] points)
    {
        // не нулевое количество точек
        if (points.length == 0) {
            throw new RuntimeException();
        }

        // четное количество точек
        if ((points.length % 2) != 0) {
            throw new RuntimeException();
        }

        // все числа из отрезка [0, count - 1] и строго по возрастанию
        int prev = -1;
        for (final int p : points) {
            if ((p < prev) || (p < 0) || (p >= count)) {
                throw new RuntimeException();
            }
            prev = p;
        }


        this._data = new int[count];
        this._index = new int[count];
        this._points = points;
        this.update();
    }

    /**
     * Для каждой точки расчитывается номер отрезка, которому точка принадлежит
     * Всего точек ровно столько, сколько было указано в конструкторе
     *
     * @return array[i] = индекс отрезка, которому пренадлежит точка i
     */
    public int[] getRanges()
    {
        final int[] range = new int[this._data.length];  // индексы диапазонов [-1,0,0,0,-1,1,1,1,1,-1,-1], len = 11
        Arrays.fill(range, -1);
        for (int i = 0; i < (this._points.length / 2); i++) {
            for (int j = this._points[2 * i]; j <= this._points[(2 * i) + 1]; j++) {
                range[j] = i;
            }
        }
        return range;
    }

    /** @return Копия логической карты */
    public int[] getData() { return this._data.clone(); }

    /** @return Копия интервалов */
    public int[] getPoints() { return this._points.clone(); }


    /**
     * @param index точка
     * @return тип точки, он хранится в логической карте. возможные значения EMPTY, LEFT, RIGHT, ACTIVE
     */
    public int getPointType(final int index)
    {
        return this._data[index];
    }

    /**
     * @return true, если одна из границ отрезка "снята"
     */
    public boolean isHasActivePoint()
    {
        return this._activePointIndex >= 0;
    }

    /**
     * @param index точка
     * @return true, если на точку можно нажать
     */
    public boolean isCanClick(final int index)
    {
        if (this._activePointIndex < 0) {
            final int type = this._data[index];
            return ((type == RichRangeSelection.LEFT) || (type == RichRangeSelection.RIGHT) || (type == RichRangeSelection.BOTH));
        }

        final int pointPairIndex = (this._activePointIndex/2);
        final int pointIndexLeft = 2*pointPairIndex;
        final int pointIndexRight = pointIndexLeft+1;

        if (index < this._points[pointIndexLeft]) {
            // левее, чем левая граница
            if (pointIndexLeft == 0) { return true; }

            // если слева что-то есть, то можно кликать вплоть до левой границы предыдущего промежутка
            return (index > this._points[pointIndexLeft-2]);

        } else if (index > this._points[pointIndexRight]) {
            // правее, чем правая граница
            if ((1+pointIndexRight) == this._points.length) { return true; }

            // если есть что-то справа, то можно кликать вплоть до правой границы следующего промежутка
            return (index < this._points[pointIndexRight+2]);
        }

        // между точками можно делать что угодно
        return true;
    }

    /**
     * Метод нажатия на точку. Если на точку нельзя нажать, то действие игнорируется
     *
     * @param index точка
     */
    public boolean doPointClick(final int index)
    {
        if (!this.isCanClick(index)) { return false; }

        if (this._activePointIndex < 0) {
            this._activePointIndex = this._index[index];
            return true;
        }

        final int pointPairIndex = (this._activePointIndex/2);
        final int pointIndexLeft = 2*pointPairIndex;
        final int pointIndexRight = pointIndexLeft+1;

        if (index < this._points[pointIndexLeft]) {
            // левее, чем левая граница - двагаем левую

            if (pointIndexLeft > 0) {
                // если есть что-то слева
                if (index <= this._points[pointIndexLeft-1]) {
                    // задели предыдущий кусок
                    if (index <= this._points[pointIndexLeft-2]) {
                        // перекрытие учатков - так делать нельзя
                        return false;
                    }
                    // подвинули правую границу предыдущего участка
                    this._points[pointIndexLeft-1] = (index-1);
                }
            }

            // подвинули левую границу текущего
            this._points[pointIndexLeft] = index;

        } else if (index > this._points[pointIndexRight]) {
            // правее, чем правая граница - двигаем ее

            if ((1+pointIndexRight) < this._points.length) {
                // если есть что-то справа
                if (index >= this._points[pointIndexRight+1]) {
                    // задели следующий участок
                    if (index >= this._points[pointIndexRight+2]) {
                        // перекрытие учатков - так делать нельзя
                        return false;
                    }
                    // подвинули левую границу следующего участка
                    this._points[pointIndexRight+1] = (index+1);
                }
            }

            // подуинули правую границу текущего участка
            this._points[pointIndexRight] = index;

        } else {

            // мы внутри промежутка - двигаем ту границу, которую мы выбрали
            this._points[this._activePointIndex] = index;

        }
        this._activePointIndex = -1;

        this.update();
        return true;
    }

    /**
     * Приватный метод пересчета логической и индексной карт
     */
    protected void update()
    {
        for (int i = 0; i < this._data.length; i++) {
            this._data[i] = RichRangeSelection.EMPTY;
        }
        for (int i = 0; i < this._index.length; i++) {
            this._index[i] = -1;
        }

        // наносим на логическую карту точки
        for (int i = 0; i < this._points.length; i++)
        {
            final int p = this._points[i];
            this._data[p] = this._data[p] == RichRangeSelection.LEFT ? RichRangeSelection.BOTH : (0 == (i%2) ? RichRangeSelection.LEFT : RichRangeSelection.RIGHT);
            this._index[p] = i;
        }

        // часть логической карты, в которую входит активная точка, обозначаем активным типом
        if (this._activePointIndex >= 0)
        {
            int min = this._activePointIndex - 1;
            min = min < 0 ? 0 : this._points[min] + 1;
            int max = this._activePointIndex + 1;
            max = max > (this._points.length - 1) ? this._data.length : this._points[max];
            for (int i = min; i < max; i++) {
                this._data[i] = RichRangeSelection.ACTIVE;
            }
        }
    }

    // T E S T

    public static void main(final String[] args)
    {
        final Scanner scanner = new Scanner(System.in);
        final RichRangeSelection selection = new RichRangeSelection(52, new int[]{1, 2, 3, 24, 25, 51});
        int control;

        // задаем общее количество точек и границы отрезков
        // в данном примере 52 точки и 3 отрезка [1,2],[3,24],[25,51]
        while (true)
        {
            // показываем логическую карту. всего существует 4 типа точек:
            // 1. пусто
            // 2. левая границы отрезка
            // 3. правая граница отрезка
            // 4. активная точка, на которую требуется нажать для установки границы отрезка
            //    активные точки существуют, только если "снята" граница одного из отрезка
            RichRangeSelection.draw(selection);
            // считываем управляющее число
            switch (control = scanner.nextInt())
            {
                case -1:
                    // показывает точки, на которые можно нажимать (метод isCanClick)
                    for (int i = 0; i < selection._data.length; i++) {
                        System.out.print(selection.isCanClick(i) ? 'x' : ' ');
                    }
                    System.out.println();
                    break;
                case -2:
                    // показывает номера отрезков, к которым пренадлежит каждая точка
                    final int[] ranges = selection.getRanges();
                    for (final int i : ranges) {
                        System.out.print(i < 0 ? "." : Integer.toString(i));
                    }
                    System.out.println();
                    break;
                default:
                    // кликаем на точку
                    // если точка не кликабельна, то действие будет проигнорировано
                    // если точка кликабельна, то произойдет смена состояния
                    selection.doPointClick(control);
            }
        }
    }

    private static void draw(final RichRangeSelection selection)
    {
        for (int i = 0; i < selection._data.length; i++) {
            System.out.print(i % 10);
        }
        System.out.println();
        for (final int i : selection._data)
        {
            switch (i)
            {
                case RichRangeSelection.EMPTY:
                    System.out.print('.');
                    break;
                case RichRangeSelection.LEFT:
                    System.out.print('(');
                    break;
                case RichRangeSelection.RIGHT:
                    System.out.print(')');
                    break;
                case RichRangeSelection.BOTH:
                    System.out.print('#');
                    break;
                case RichRangeSelection.ACTIVE:
                    System.out.print('x');
                    break;
            }
        }
        System.out.println();
    }
}
