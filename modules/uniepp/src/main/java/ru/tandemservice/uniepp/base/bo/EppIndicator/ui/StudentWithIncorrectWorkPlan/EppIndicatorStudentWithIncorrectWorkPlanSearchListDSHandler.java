/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentWithIncorrectWorkPlan;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListPresenter;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 05.03.2015
 */
public class EppIndicatorStudentWithIncorrectWorkPlanSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public EppIndicatorStudentWithIncorrectWorkPlanSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);

        OrgUnit orgUnit = context.get(AbstractEppIndicatorStudentListPresenter.PROP_ORG_UNIT);
        Term term = context.get(EppIndicatorStudentWithIncorrectWorkPlanUI.PROP_TERM);
        List<DevelopTech> developTechList = context.get(EppIndicatorStudentWithIncorrectWorkPlanUI.PROP_DEVELOP_TECH);

        builder.where(eq(property(alias, Student.archival()), value(Boolean.FALSE)));
        builder.where(eq(property(alias, Student.status().active()), value(Boolean.TRUE)));
        builder.where(exists(
                new DQLSelectBuilder().fromEntity(EppStudent2WorkPlan.class, "s2wp").column(property("s2wp.id"))
                        .joinPath(DQLJoinType.inner, EppStudent2WorkPlan.cachedGridTerm().fromAlias("s2wp"), "dgt")
                        .where(isNull(property(EppStudent2WorkPlan.removalDate().fromAlias("s2wp"))))
                        .where(eq(property("s2wp", EppStudent2WorkPlan.studentEduPlanVersion().student()), property(alias)))
                        .where(eq(property("s2wp", EppStudent2WorkPlan.cachedEppYear().educationYear().current()), value(Boolean.FALSE)))
                        .where(eq(property("dgt", DevelopGridTerm.term()), value(term)))
                        .where(eq(property("dgt", DevelopGridTerm.course()), property(alias, Student.course())))
                        .buildQuery()
        ));

        if (null != developTechList && !developTechList.isEmpty())
        {
            builder.where(in(property(alias, Student.educationOrgUnit().developTech()), developTechList));
        }

        // этого деканата
        if (null != orgUnit && null != orgUnit.getId())
        {
            builder.where(eq(property(alias, Student.educationOrgUnit().groupOrgUnit().id()), value(orgUnit.getId())));
        }
    }
}
