package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockIndexEdit;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.index.IEppIndexedRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.*;

@Input({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="holder.id", required=true)
})
public class EppEduPlanVersionBlockIndexEditUI extends UIPresenter
{
    private final EntityHolder<EppEduPlanVersionBlock> holder = new EntityHolder<>();
    private List<EppEpvRow> rowList;
    private Map<EppEpvRow, IEppIndexedRowWrapper> wrapperMap;
    private PlaneTree tree;
    private EppEpvRow row;

    @Override
    public void onComponentRefresh() {
        final EppEduPlanVersionBlock entity = this.getHolder().refresh(EppEduPlanVersionBlock.class);
        final List<EppEpvRow> rowList = new ArrayList<>();
        for (IEppEpvRowWrapper rowWrapper: IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockRowMap(entity.getId(), true).values()) {
            rowList.add((EppEpvRow)rowWrapper.getRow());
        }
        this.setRowList(rowList);
        rebuildTree();
    }

    @SuppressWarnings("unchecked")
    public void rebuildTree() {
        fixNonUniqueNumbers();
        Collections.sort(getRowList(), EppEpvRow.GLOBAL_ROW_COMPARATOR);
        this.setTree(new PlaneTree((Collection)getRowList()));
        this.setWrapperMap(EppEduPlanVersionDataDAO.buildIndexRuleRowWrapperMap(getTreeRowList()));
    }

    // actions

    public void onClickRowDown()
    {
        final EppEpvRow row = getRowById(getSupport().getListenerParameterAsLong());
        if (null == row) { return; }

        ListIterator<EppEpvRow> it = getSameParentRowIterator(row);

        it.next();
        if (!it.hasNext()) { return; }
        final EppEpvRow other = it.next();

        swap(row, other);
    }

    public void onClickApply()
    {
        DataAccessServices.dao().doInTransaction(session -> {
            for (EppEpvRow row: getTreeRowList()) {
                session.saveOrUpdate(row);
            }
            IEppEduPlanVersionDataDAO.instance.get().doUpdateEpvRowStoredIndex(getBlock().getEduPlanVersion().getId());
            return null;
        });
        this.deactivate();
    }

    public void onClickCalculateUserIndex() {
        final EppEpvRow row = getRowById(getSupport().getListenerParameterAsLong());
        if (null == row) { return; }
        row.setUserIndex(getWrapperMap().get(row).getIndex());
    }

    public void onClickRowUp()
    {
        final EppEpvRow row = getRowById(getSupport().getListenerParameterAsLong());
        if (null == row) { return; }

        ListIterator<EppEpvRow> it = getSameParentRowIterator(row);

        if (!it.hasPrevious()) { return; }
        final EppEpvRow other = it.previous();

        swap(row, other);
    }


    // utils

    private void swap(final EppEpvRow row, final EppEpvRow other)
    {
        if (!getBlock().equals(row.getOwner())) { return; }
        if (!getBlock().equals(other.getOwner())) { return; }

        String property = getRowOrderProperty(row);
        if (null == property) { return; }

        if (!ObjectUtils.equals(other.getHierarhyParent(), row.getHierarhyParent())) { return; }
        if (other.getComparatorClassIndex() != row.getComparatorClassIndex()) { return; }

        {
            String otherProperty = getRowOrderProperty(other);
            if (!ObjectUtils.equals(property, otherProperty)) { return; }
        }

        property = StringUtils.substringAfter(property, ".");
        Object tmp = row.getProperty(property);
        row.setProperty(property, other.getProperty(property));
        other.setProperty(property, tmp);
        rebuildTree();
    }

    // итератор строк того же родителя, что и у переданного элемента
    // итератор установлен на переданном элементе (next - вернет row)
    private ListIterator<EppEpvRow> getSameParentRowIterator(final EppEpvRow row) {
        final List<EppEpvRow> rows = new ArrayList<>();
        for (EppEpvRow tmp: getTreeRowList()) {
            if (ObjectUtils.equals(tmp.getHierarhyParent(), row.getHierarhyParent())) {
                rows.add(tmp);
            }
        }
        return rows.listIterator(rows.indexOf(row));
    }

    // presenter

    @SuppressWarnings("unchecked")
    public List<EppEpvRow> getTreeRowList() {
        return (List)Arrays.asList(getTree().getFlatTreeObjects());
    }

    public String getRowDisplayableTitle() { return this.getRow().getDisplayableTitle(); }
    public int getRowHierarchyLevel() { return this.getTree().getLevel(this.getRow()); }
    public boolean isRowLeaf() { return !this.getTree().hasChildren(this.getRow()); }
    public EppRegistryElement getRowRegistryElement() { return ((getRow() instanceof EppEpvRegistryRow) ? ((EppEpvRegistryRow)getRow()).getRegistryElement() : null); }
    public String getRowType() { return this.getRow().getRowType(); }

    public String getRowCalculatedIndex() { return getWrapperMap().get(getRow()).getIndex(); }

    public boolean isRowFromCurrentBlock() {
        return getBlock().equals(getRow().getOwner());
    }

    public boolean isRowMovable() {
        return isRowFromCurrentBlock() && (null != getRowOrderProperty(getRow()));
    }

    private String getRowOrderProperty(final EppEpvRow row) {
        if (row instanceof EppEpvTermDistributedRow) { return "EppEpvTermDistributedRow." + EppEpvTermDistributedRow.P_NUMBER; }
        return null;
    }

    private EppEpvRow getRowById(final Long id) {
        return CollectionUtils.find(getTreeRowList(), object -> object.getId().equals(id));
    }

    private void fixNonUniqueNumbers()
    {
        final Set<EppEpvTermDistributedRow> nonUniqueOrderPropertyRows = new HashSet<>();
        final Set<String> usedNumbers = new HashSet<>();
        for (EppEpvRow row : getRowList()) {
            if (!(row instanceof EppEpvTermDistributedRow)) continue;
            EppEpvTermDistributedRow termDistributedRow = (EppEpvTermDistributedRow) row;
            if (usedNumbers.contains(termDistributedRow.getNumber())) {
                nonUniqueOrderPropertyRows.add(termDistributedRow);
            } else {
                usedNumbers.add(termDistributedRow.getNumber());
            }
        }

        for (EppEpvTermDistributedRow row : nonUniqueOrderPropertyRows) {
            row.setNumber(IEppEduPlanVersionDataDAO.instance.get().getNextNumber(row, Collections.<Long>emptyList()));
        }
    }

    // getters and setters

    public EntityHolder<EppEduPlanVersionBlock> getHolder() { return this.holder; }
    public EppEduPlanVersionBlock getBlock() { return this.getHolder().getValue(); }

    public List<EppEpvRow> getRowList() { return this.rowList; }
    public void setRowList(List<EppEpvRow> rowList) { this.rowList = rowList;  }

    public Map<EppEpvRow, IEppIndexedRowWrapper> getWrapperMap() { return this.wrapperMap; }
    public void setWrapperMap(Map<EppEpvRow, IEppIndexedRowWrapper> wrapperMap) { this.wrapperMap = wrapperMap; }

    public PlaneTree getTree() { return this.tree; }
    public void setTree(PlaneTree tree) { this.tree = tree; }

    public EppEpvRow getRow() { return this.row; }
    public void setRow(EppEpvRow row) { this.row = row; }
}
