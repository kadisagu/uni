package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x7x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.renameColumn("epp_epvrow_distr_t", "totalsize_p", "hourstotal_p");
        tool.renameColumn("epp_epvrowterm_t", "totaltermloadsize_p", "hourstotal_p");
        tool.renameColumn("epp_epvrowterm_t", "totaltermloadlabor_p", "labor_p");
        tool.renameColumn("epp_epvrowterm_t", "totaltermloadweeks_p", "weeks_p");
        tool.renameColumn("epp_epvrowterm_load_t", "load_p", "hours_p");
        tool.renameColumn("epp_epvrow_distr_load_t", "load_p", "hours_p");

		{
            // строки

			tool.createColumn("epp_epvrow_distr_t", new DBColumn("hoursaudit_p", DBType.LONG));
            tool.createColumn("epp_epvrow_distr_t", new DBColumn("hoursselfwork_p", DBType.LONG));
            tool.createColumn("epp_epvrow_distr_t", new DBColumn("hourscontrol_p", DBType.LONG));
            tool.createColumn("epp_epvrow_distr_t", new DBColumn("hourscontrole_p", DBType.LONG));

            tool.executeUpdate("update epp_epvrow_distr_t set hourscontrol_p=? where hourscontrol_p is null", 0l);
            tool.executeUpdate("update epp_epvrow_distr_t set hourscontrole_p=? where hourscontrole_p is null", 0l);

            long auditId = getLoadTypeId(tool, EppELoadTypeCodes.TYPE_TOTAL_AUDIT);
            long selfworkId = getLoadTypeId(tool, EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);

            moveLoad(tool, auditId, "hoursaudit_p", "epp_epvrow_distr_t", "epp_epvrow_distr_load_t", "row_id", 0l);
            moveLoad(tool, selfworkId, "hoursselfwork_p", "epp_epvrow_distr_t", "epp_epvrow_distr_load_t", "row_id", 0l);

            tool.executeUpdate("update epp_epvrow_distr_t set hoursaudit_p=? where hourscontrol_p is null", 0l);
            tool.executeUpdate("update epp_epvrow_distr_t set hoursselfwork_p=? where hourscontrole_p is null", 0l);

            tool.setColumnNullable("epp_epvrow_distr_t", "hoursaudit_p", false);
            tool.setColumnNullable("epp_epvrow_distr_t", "hoursselfwork_p", false);
            tool.setColumnNullable("epp_epvrow_distr_t", "hourscontrol_p", false);
            tool.setColumnNullable("epp_epvrow_distr_t", "hourscontrole_p", false);

            // семестры строк

            tool.createColumn("epp_epvrowterm_t", new DBColumn("hoursaudit_p", DBType.LONG));
            tool.createColumn("epp_epvrowterm_t", new DBColumn("hoursselfwork_p", DBType.LONG));
            tool.createColumn("epp_epvrowterm_t", new DBColumn("hourscontrol_p", DBType.LONG));
            tool.createColumn("epp_epvrowterm_t", new DBColumn("hourscontrole_p", DBType.LONG));

            moveLoad(tool, auditId, "hoursaudit_p", "epp_epvrowterm_t", "epp_epvrowterm_load_t", "rowTerm_id", -1l);
            moveLoad(tool, selfworkId, "hoursselfwork_p", "epp_epvrowterm_t", "epp_epvrowterm_load_t", "rowTerm_id", -1l);

            tool.executeUpdate("update epp_epvrowterm_t set hourscontrol_p=? where hourscontrol_p is null", -1l);
            tool.executeUpdate("update epp_epvrowterm_t set hourscontrole_p=? where hourscontrole_p is null", -1l);

            tool.setColumnNullable("epp_epvrowterm_t", "hoursaudit_p", false);
            tool.setColumnNullable("epp_epvrowterm_t", "hoursselfwork_p", false);
            tool.setColumnNullable("epp_epvrowterm_t", "hourscontrol_p", false);
            tool.setColumnNullable("epp_epvrowterm_t", "hourscontrole_p", false);
		}


		{
			tool.createColumn("epp_epvrowterm_load_t", new DBColumn("hoursi_p", DBType.LONG));
            tool.createColumn("epp_epvrowterm_load_t", new DBColumn("hourse_p", DBType.LONG));

            tool.executeUpdate("update epp_epvrowterm_load_t set hoursi_p=? where hoursi_p is null", 0l);
            tool.executeUpdate("update epp_epvrowterm_load_t set hourse_p=? where hourse_p is null", 0l);

			tool.setColumnNullable("epp_epvrowterm_load_t", "hoursi_p", false);
            tool.setColumnNullable("epp_epvrowterm_load_t", "hourse_p", false);

		}

		{
            tool.createColumn("epp_epvrow_distr_load_t", new DBColumn("hourse_p", DBType.LONG));
			tool.createColumn("epp_epvrow_distr_load_t", new DBColumn("hoursi_p", DBType.LONG));

			tool.executeUpdate("update epp_epvrow_distr_load_t set hoursi_p=? where hoursi_p is null", 0L);
            tool.executeUpdate("update epp_epvrow_distr_load_t set hourse_p=? where hourse_p is null", 0L);

			tool.setColumnNullable("epp_epvrow_distr_load_t", "hoursi_p", false);
            tool.setColumnNullable("epp_epvrow_distr_load_t", "hourse_p", false);
		}
    }

    private void moveLoad(DBTool tool, long loadTypeId, String propName, String tableName, String loadTableName, String ownerPropName, long defaultValue) throws SQLException
    {
        final Map<Long, Long> map = new HashMap<>();
        PreparedStatement ps = tool.prepareStatement("select l." + ownerPropName + ", l.hours_p from " + loadTableName + " l where l.loadType_id = ?", loadTypeId);
        ps.execute();
        ResultSet rs = ps.getResultSet();
        while (rs.next()) {
            map.put(rs.getLong(1), rs.getLong(2));
        }

        PreparedStatement update = tool.prepareStatement("update " + tableName + " set " + propName + " = ? where id = ?");
        for (Map.Entry<Long, Long> e : map.entrySet()) {
            update.setLong(1, e.getValue());
            update.setLong(2, e.getKey());
            update.execute();
        }

        tool.executeUpdate("update " + tableName + " set " + propName + "=? where " + propName + " is null", defaultValue);
        tool.executeUpdate("delete from " + loadTableName + " where loadType_id = ?", loadTypeId);
    }

    private long getLoadTypeId(DBTool tool, String code) throws SQLException
    {
        long id = 0;
        Statement s = tool.getConnection().createStatement();
        s.execute("select t.id from epp_c_loadtype_t t where t.code_p='" + code + "' and t.catalogCode_p='eppELoadType'");
        ResultSet rs = s.getResultSet();
        while (rs.next()) {
            id = rs.getLong(1);
        }

        if (id == 0) {
            throw new IllegalStateException();
        }

        return id;
    }
}