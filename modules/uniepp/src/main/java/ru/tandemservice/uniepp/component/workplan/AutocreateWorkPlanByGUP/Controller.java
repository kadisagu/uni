package ru.tandemservice.uniepp.component.workplan.AutocreateWorkPlanByGUP;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.event.IEventServiceLock;


/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
    }

    public void onChangeEppYear(final IBusinessComponent component) {
        this.getDao().onChangeEppYear(this.getModel(component));
    }

    public void onChangeEppWorkGraph(final IBusinessComponent component) {
        this.getDao().onChangeEppWorkGraph(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            this.getDao().save(model);
        } finally {
            eventLock.release();
        }

        if (UserContext.getInstance().getErrorCollector().hasErrors()) { return; }
        this.deactivate(component);
    }

}
