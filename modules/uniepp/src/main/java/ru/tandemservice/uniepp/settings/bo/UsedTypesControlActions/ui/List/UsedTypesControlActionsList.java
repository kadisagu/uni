package ru.tandemservice.uniepp.settings.bo.UsedTypesControlActions.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.settings.bo.UsedTypesControlActions.logic.UsedTypesDSHandler;

/**
 * @author avedernikov
 * @since 03.09.2015
 */

@Configuration
public class UsedTypesControlActionsList extends BusinessComponentManager
{
	public static final String USED_TYPES_DS = "usedTypesDS";

	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
			.addDataSource(searchListDS(USED_TYPES_DS, usedTypesDSColumns(), usedTypesDSHandler()))
			.create();
	}

	@Bean
	public ColumnListExtPoint usedTypesDSColumns()
	{
		return columnListExtPointBuilder(USED_TYPES_DS)
			.addColumn(textColumn("title", EppControlActionType.title()))
			.addColumn(booleanColumn("finalAction", "finalAction"))
			.addColumn(toggleColumn("enabled", "enabled").toggleOnListener("onEnableType").toggleOffListener("onDisableType"))
			.addColumn(toggleColumn("activeInEduPlan", EppControlActionType.activeInEduPlan())
				.toggleOnListener("onToggledActiveInEduPlan").toggleOffListener("onToggledActiveInEduPlan")
				.disabled(EppControlActionType.disabled().s()))
			.addColumn(toggleColumn("usedWithDisciplines", EppControlActionType.usedWithDisciplines())
				.toggleOnListener("onToggledUsedWithDisciplines").toggleOffListener("onToggledUsedWithDisciplines")
				.disabled(EppControlActionType.disabled().s()))
			.addColumn(toggleColumn("usedWithPractice", EppControlActionType.usedWithPractice())
				.toggleOnListener("onToggledUsedWithPractice").toggleOffListener("onToggledUsedWithPractice")
				.disabled(EppControlActionType.disabled().s()))
			.addColumn(toggleColumn("usedWithAttestation", EppControlActionType.usedWithAttestation())
				.toggleOnListener("onToggledUsedWithAttestation").toggleOffListener("onToggledUsedWithAttestation")
				.disabled(EppControlActionType.disabled().s()))
			.create();
	}

	@Bean
	public IReadAggregateHandler<DSInput, DSOutput> usedTypesDSHandler()
	{
		return new UsedTypesDSHandler(getName());
	}
}
