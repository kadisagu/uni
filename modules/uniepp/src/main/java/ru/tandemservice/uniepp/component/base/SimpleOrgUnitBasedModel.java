package ru.tandemservice.uniepp.component.base;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author vdanilov
 */
public abstract class SimpleOrgUnitBasedModel {
    public abstract OrgUnit getOrgUnit();

    public IEntity getSecuredObject() {
        return (IEntity)((null == this.getOrgUnit()) ? SecurityRuntime.getInstance().getCommonSecurityObject() : this.getOrgUnit());
    }
}
