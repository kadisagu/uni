/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.ext.SystemAction.ui.Pub;

import com.healthmarketscience.jackcess.Database;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.BackgroundProcessBase;
import org.tandemframework.core.process.ProcessDisplayMode;
import org.tandemframework.core.process.ProcessResult;
import org.tandemframework.core.process.ProcessState;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.transaction.sync.DaemonThreadFactory;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniepp.base.bo.EppSystemAction.EppSystemActionManager;
import ru.tandemservice.uniepp.dao.eduplan.io.IEppEduPlanVersionIODao;
import ru.tandemservice.uniepp.dao.group.EppRealGroupRowDAO;
import ru.tandemservice.uniepp.dao.group.IEppRealGroupRowDAO;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public class EppSystemActionPubAddon extends UIAddon
{
    public EppSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private final Logger log = Logger.getLogger(this.getClass());

    public void onClickGetFileNames4ImportToMdb()
    {
        final RtfDocument document = EppSystemActionManager.instance().dao().doGetFileNames4ImportToMdb();
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("fileNames.rtf").document(document), true);
    }

    public void onClickExportTemplate4EduPlan()
    {
        final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(this.getTemplate4EduPlan(false), "eduplan_template.mdb");
        getActivationBuilder().asDesktopRoot(IUniComponents.PRINT_REPORT)
        .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "mdb"))
        .activate();
    }

    public void onClickExportEduPlanRows()
    {
        final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(this.getTemplate4EduPlan(true), "eduplan_rows.mdb");
        getActivationBuilder().asDesktopRoot(IUniComponents.PRINT_REPORT)
        .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "mdb"))
        .activate();
    }


    private byte[] getTemplate4EduPlan(boolean exportRowData) {
        try {
            final File file = File.createTempFile("mdb-io-", "mdb");
            try {
                final Database mdb = Database.create(Database.FileFormat.V2003, file, false);
                IEppEduPlanVersionIODao.instance.get().export_EppEduPlanVersionList(mdb, null, exportRowData);
                mdb.close();
                return FileUtils.readFileToByteArray(file);
            } finally {
                file.delete();
            }
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }


    public void onClickImportEduPlans()
    {
        try {
            final String path = ApplicationRuntime.getAppInstallPath() + "data/";
            String fileName = "eduplan_data.mdb";
            final File file = new File(path, fileName);

            if (!file.exists()) { throw new ApplicationException("Не найден файл с данными для импорта. Разместите файл, содержащий исходные данные, на сервере приложения в каталоге «" + path + "». Файл должен иметь название «" + fileName + "».");  }
            if (!file.canRead()) { throw new ApplicationException("Файл с данными для импорта на сервере приложения ("+path + fileName+") не доступен для чтения."); }

            try (Database mdb = Database.open(file))
            {
                final IEventServiceLock eventLock = CoreServices.eventService().lock();
                Debug.stopLogging();
                try
                {
                    IEppEduPlanVersionIODao.instance.get().doImport_EppEduPlanVersionList(mdb);
                }
                finally
                {
                    Debug.resumeLogging();
                    eventLock.release();
                }
            }
        } catch (final Throwable t) {
            if (t instanceof ApplicationException) { throw (ApplicationException) t; }
            else
            {
                Logger.getLogger(IEppEduPlanVersionIODao.class).error("Error occured during getting import file, see exception info below. ", t);
                throw new ApplicationException("Не удалось открыть файл для импорта, обратитесь к логу для получения информации об ошибке.");
            }
        }
    }

    public void onClickSyncEppStudentSlots() {
        EppStudentSlotDAO.DAEMON.wakeUpDaemon();
    }

    public void onClickSyncEppRealGroupRows() {
        EppRealGroupRowDAO.DAEMON.wakeUpDaemon();
    }

    public void onClickSyncEppRealGroupStudentRowData() {
        IEppRealGroupRowDAO.instance.get().doUpdateStudentRowData();
    }

    public void onClickFixEduGroupOperationOrgUnit() {
        IEppRealGroupRowDAO.instance.get().doFixCurrentYearEduGroupSummary();
    }


    public void onClickMergeRegistryElements()
    {
        final Collection<Long> ids = UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
            final MQBuilder b = new MQBuilder(EppRegistryElementGen.ENTITY_CLASS, "e", new String[] { EppRegistryElementGen.L_OWNER+".id" });
            b.setNeedDistinct(true);
            return b.getResultList(session);
        });

        // делаем в несколько потоков - иначе юзатеся один проц, при этом вся система курит
        final ExecutorService executor = Executors.newScheduledThreadPool(2, new DaemonThreadFactory("sched-pool-epp-reg-merge"));
        for (final Long id: ids) {
            final Map<Long, Collection<Long>> map = EppSystemActionManager.instance().dao().getProcessMap(id);
            if ((null == map) || map.isEmpty()) { continue; }

            BatchUtils.execute(map.entrySet(), 1, e -> {

                // нужно копировать элементы, потому что BatchUtils не threadsafe
                final Collection<Map.Entry<Long, Collection<Long>>> elements = new ArrayList<>(e);

                executor.submit(() -> {
                    Debug.begin(String.valueOf(id) + ":" + elements.size());
                    try
                    {

                        final IEventServiceLock eventLock = CoreServices.eventService().lock();
                        try
                        {
                            // нужно, чтобы каждая порция была в своей сессии и своей транзакции
                            EppSystemActionManager.instance().dao().doMergeRegistryElements(elements);
                        } catch (final Throwable t)
                        {
                            Debug.exception(t);
                            log.fatal(t.getMessage(), t);
                        } finally
                        {
                            eventLock.release();
                        }

                    } finally
                    {
                        log.info(Debug.end().toFullString());
                    }
                });
            });
        }

        executor.shutdown();
    }


    //    public void onClickImportEduStdBlockFromEduPlan()
    //    {
    //        final IEventServiceLock eventLock = CoreServices.eventService().lock();
    //        try  {
    //            EppSystemActionManager.instance().dao().doImportEduStdBlockFromEduPlan();
    //        } finally {
    //            eventLock.release();
    //        }
    //
    //    }

    public void onClickAcceptDisciplinesActions()
    {
        getActivationBuilder().asDesktopRoot(ru.tandemservice.uniepp.component.settings.SystemActionAcceptDisciplinesActions.Model.class.getPackage().getName())
        .activate();
    }

    public void onClickCreateSkills()
    {
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try  {
            EppSystemActionManager.instance().dao().doCreateSkills();
        } finally {
            eventLock.release();
        }
    }

    public void onClickShowEppForTutorOrgUnits()
    {
        EppSystemActionManager.instance().dao().doShowEppForTutorOrgUnits();
    }

    public void onClickFixEduPlanNumbers()
    {
        EppSystemActionManager.instance().dao().doFixEduPlanNumbers();
    }

    public void onClickCorrectEduGroupLevels()
    {
        EppSystemActionManager.instance().dao().doCorrectEduGroupLevels();
    }

    public void onClickSetContractRoles()
    {
        EppSystemActionManager.instance().dao().doSetContractRoles();
    }

    public void onClickDev5818CheckList()
    {
        // getActivationBuilder().asRegion(EppIndicatorDev5818CheckList.class).activate();
    }

    public void onClickMoveWorkPlans2specializationBlock()
    {
        new BackgroundProcessHolder().start("Перенос РУПов на блоки специализаций", new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                final int fixedWPCount = EppSystemActionManager.instance().dao().moveWorkPlans2specializationBlock(log);
                return new ProcessResult("Исправлено РУПов: " + fixedWPCount);
            }
        }, ProcessDisplayMode.unknown);
    }
}
