package ru.tandemservice.uniepp.component.settings.SystemActionAcceptDisciplinesActions;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        final DynamicListDataSource<OrgUnit> dataSource = UniBaseUtils.createDataSource(component, this.getDao());
        dataSource.addColumn(new CheckboxColumn("select", "").setRequired(true).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Название", OrgUnit.P_TYPE_TITLE));

        model.setDataSource(dataSource);
    }

    public void onClickAccept(final IBusinessComponent component)
    {
        final Collection<IEntity> selectedObject = ((CheckboxColumn)this.getModel(component).getDataSource().getColumn("select")).getSelectedObjects();

        final Set<Long> selectedIds = new HashSet<Long>(selectedObject.size());
        for (final IEntity entity : selectedObject)
        {
            final IEntityMeta meta = EntityRuntime.getMeta(entity.getId());
            if (null != meta)
            {
                if (OrgUnit.class.isAssignableFrom(meta.getEntityClass()))
                {
                    selectedIds.add(entity.getId());
                }
            }
        }

        if (selectedIds.isEmpty())
        {
            ContextLocal.getErrorCollector().add("Выберите подразделения.");
            return;
        }

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {
            this.getDao().doAcceptDisciplinesAndActions(selectedIds);
        } finally {
            eventLock.release();
        }
    }

}
