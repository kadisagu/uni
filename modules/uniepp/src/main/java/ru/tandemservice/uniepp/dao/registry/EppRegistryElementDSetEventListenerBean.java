package ru.tandemservice.uniepp.dao.registry;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EppRegistryElementDSetEventListenerBean {

    // при переходе по состояниям - переводить модули, если они используются тольво в текущей дисциплине (и они не общие и они с текущего подразделения)
    // далее, при переводе элемента реестра в состояние "на согласовании" и "согласован" - все модули должны быть согласованы
    // (обратное не верно - т.е. допускается, что в согласованном состоянии у дисциплины есть несогласованные модули (модуль потом перевели в архив, например))
    private static class RegistryElementStateCheckDSetEventListener implements IDSetEventListener {
        private static final RegistryElementStateCheckDSetEventListener INSTANCE = new RegistryElementStateCheckDSetEventListener();

        @Override
        public void onEvent(DSetEvent event) {
            if (EppRegistryElement.class.isAssignableFrom(event.getMultitude().getEntityMeta().getEntityClass())) {
                final Set affectedProperties = event.getMultitude().getAffectedProperties();
                if (affectedProperties.contains(EppRegistryElement.L_STATE)) {

                    /* обновляем состояне модулей, если они привязаны только к одной дисциплине */
                    {
                        new DQLUpdateBuilder(EppRegistryModule.class)

                        .fromEntity(EppRegistryElementPartModule.class, "relpm")
                        .where(eq(property(EppRegistryElementPartModule.module().id().fromAlias("relpm")), property("id")))

                        .fromEntity(EppRegistryElementPart.class, "relp")
                        .where(eq(property(EppRegistryElementPart.id().fromAlias("relp")), property(EppRegistryElementPartModule.part().id().fromAlias("relpm"))))

                        .fromEntity(EppRegistryElement.class, "rel")
                        .where(eq(property(EppRegistryElement.id().fromAlias("rel")), property(EppRegistryElementPart.registryElement().id().fromAlias("relp"))))

                        .where(eq(property(EppRegistryModule.P_SHARED), value(Boolean.FALSE))) /*только если модуль не общий*/

                        .where(eq(property(EppRegistryModule.L_OWNER), property(EppRegistryElement.owner().id().fromAlias("rel"))) /*только с текущей кафедры*/)
                        .where(in(property("rel.id"), event.getMultitude().getInExpression()) /*только среди изменяемых*/)

                        .set(EppRegistryModule.L_STATE, property(EppRegistryElement.state().id().fromAlias("rel")))
                        .where(ne(property(EppRegistryModule.L_STATE), property(EppRegistryElement.state().id().fromAlias("rel"))))

                        .where(notExists(
                                new DQLSelectBuilder()
                                .fromEntity(EppRegistryElementPartModule.class, "x")
                                .column(property("x.id"))
                                .where(eq(property(EppRegistryElementPartModule.module().id().fromAlias("x")), property(EppRegistryElementPartModule.module().id().fromAlias("relpm"))))
                                .where(ne(property(EppRegistryElementPartModule.id().fromAlias("x")), property(EppRegistryElementPartModule.id().fromAlias("relpm"))))
                                .buildQuery()
                        )).createStatement(event.getContext()).execute();

                    }


                    /* дисциплина на согласовании - модули на согласовании или согласованы */
                    {
                        List<Long> ids = new DQLSelectBuilder()
                        .fromEntity(EppRegistryElement.class, "rel").column(property("rel.id"))
                        .where(in(property("rel.id"), event.getMultitude().getInExpression()))
                        .where(eq(property(EppRegistryElement.state().code().fromAlias("rel")), value(EppState.STATE_ACCEPTABLE)))
                        .where(exists(
                                new DQLSelectBuilder()
                                .fromEntity(EppRegistryElementPartModule.class, "relpm").column(property("relpm.id"))
                                .where(eq(property(EppRegistryElementPartModule.part().registryElement().id().fromAlias("relpm")), property("rel.id")))
                                .where(notIn(property(EppRegistryElementPartModule.module().state().code().fromAlias("relpm")), Arrays.asList(EppState.STATE_ACCEPTABLE, EppState.STATE_ACCEPTED)))
                                .buildQuery()
                        ))
                        .createStatement(event.getContext()).<Long>list();

                        if (ids.size() > 0) {
                            throw new ApplicationException("Нельзя отправить на согласование элемент реестра: содержащиеся в нем модули не согласованы.", true);
                        }
                    }

                    /* дисциплина согласнована - модули согласованы */
                    {
                        List<Long> ids = new DQLSelectBuilder()
                        .fromEntity(EppRegistryElement.class, "rel").column(property("rel.id"))
                        .where(in(property("rel.id"), event.getMultitude().getInExpression()))
                        .where(eq(property(EppRegistryElement.state().code().fromAlias("rel")), value(EppState.STATE_ACCEPTED)))
                        .where(exists(
                                new DQLSelectBuilder()
                                .fromEntity(EppRegistryElementPartModule.class, "relpm").column(property("relpm.id"))
                                .where(eq(property(EppRegistryElementPartModule.part().registryElement().id().fromAlias("relpm")), property("rel.id")))
                                .where(ne(property(EppRegistryElementPartModule.module().state().code().fromAlias("relpm")), value(EppState.STATE_ACCEPTED)))
                                .buildQuery()
                        ))
                        .createStatement(event.getContext()).<Long>list();

                        if (ids.size() > 0) {
                            throw new ApplicationException("Нельзя согласовать элемент реестра: содержащиеся в нем модули не согласованы.", true);
                        }
                    }
                }
            }
        }
    }

    public void init() {
        // при изменении элемента реестра, проверяем, что его состояние перешло в "согласовано" или "на согласовании" - в этот момент все модули должны быть согласованы, иначе break
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppRegistryElement.class, RegistryElementStateCheckDSetEventListener.INSTANCE);
    }

}
