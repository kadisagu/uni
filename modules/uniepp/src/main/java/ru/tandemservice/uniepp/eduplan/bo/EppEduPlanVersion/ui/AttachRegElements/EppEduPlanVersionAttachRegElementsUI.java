/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.AttachRegElements;

import com.google.common.collect.Iterables;
import org.apache.commons.collections15.Predicate;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.process.BackgroundProcessBase;
import org.tandemframework.core.process.IBackgroundProcess;
import org.tandemframework.core.process.ProcessResult;
import org.tandemframework.core.process.ProcessState;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.IRowCustomizer;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.CheckboxColumn;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.TreeColumn;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.EppRegElementKeyGenerator.KeyGenRule;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvHierarchyRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;

import java.util.*;
import java.util.function.Consumer;

/**
 * @author Nikolay Fedorovskih
 * @since 20.05.2015
 */
@Input({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "blockHolder.id", required = true)
})
public class EppEduPlanVersionAttachRegElementsUI extends UIPresenter
{
    public static final String ATTACH_RULE_SETTING_NAME = "attachRule";

    private EntityHolder<EppEduPlanVersionBlock> _blockHolder = new EntityHolder<>();
    private StaticListDataSource<IEppEpvRowWrapper> _rowDataSource = new StaticListDataSource<>();

    @Override
    public void onComponentRefresh()
    {
        _blockHolder.refresh(EppEduPlanVersionBlock.class);

        prepareDataSource();
    }

    @Override
    public void onComponentActivate()
    {
        if (getAttacheRuleId() == null) {

            // По умолчанию выбрано - создавать новые, если не найдено в реестре
            final SelectDataSource ds = getConfig().getDataSource(EppEduPlanVersionAttachRegElements.ATTACHE_RULE_DS);
            getSettings().set(ATTACH_RULE_SETTING_NAME, ds.getRecordById(EppEduPlanVersionAttachRegElements.ATTACH_RULE_NEW_IF_MISSING));
        }
    }

    final static IRowCustomizer<IEppEpvRowWrapper> rowCustomizer = new SimpleRowCustomizer<IEppEpvRowWrapper>()
    {
        @Override
        public String getRowStyle(final IEppEpvRowWrapper entity)
        {
            final StringBuilder sb = new StringBuilder();
            if (entity.getRow() instanceof EppEpvHierarchyRow) { sb.append("font-weight:bold;"); }
            return sb.toString();
        }
    };

    private void prepareDataSource()
    {
        _rowDataSource.clear();
        _rowDataSource.getColumns().clear();
        _rowDataSource.setRowCustomizer(rowCustomizer);
        CheckboxColumn checkboxColumn = new CheckboxColumn("select", "");
        checkboxColumn.setScriptResolver(
                (tableName, columnNumber, rowEntity, entity, rowIndex) ->
                        "eduPlanVersionSourceCheckChildren(event, this, '" + tableName + "', " + columnNumber + ", '" + rowEntity.getId() + "');"
        );
        checkboxColumn.setWidth(1);
        _rowDataSource.addColumn(checkboxColumn);

        _rowDataSource.addColumn(this.wrap(new SimpleColumn("Индекс", "index").setWidth(1)));
        _rowDataSource.addColumn(this.wrap(new TreeColumn("Название", "title")));
        _rowDataSource.addColumn(this.wrap(new SimpleColumn("Нагрузка", "index")
        {
            @Override
            public String getContent(final IEntity entity)
            {
                final IEppEpvRowWrapper row = (IEppEpvRowWrapper) entity;
                if (row.getRow() instanceof EppEpvRegistryRow) {
                    return UniEppUtils.formatLoad(row.getTotalLoad(0, EppLoadType.FULL_CODE_TOTAL_HOURS), false);
                }
                return "";
            }

        }));


        final Map<Long, IEppEpvRowWrapper> wrapperMap = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockRowMap(getBlock().getId(), true);
        final Predicate<IEppEpvRowWrapper> rowPredicate = rowWrapper -> {
            // Только реестровые строки, для которых элемент реестра не указан
            return rowWrapper.getRow() instanceof EppEpvRegistryRow && ((EppEpvRegistryRow)rowWrapper.getRow()).getRegistryElement() == null;
        };
        _rowDataSource.setupRows(EppEduPlanVersionDataDAO.filterEduPlanBlockRows(wrapperMap, rowPredicate, true));

        // По умолчанию всё выбрано
        checkboxColumn.setSelected(CommonBaseEntityUtil.getIdSet(_rowDataSource.getEntityList()));
    }

    protected AbstractColumn wrap(final AbstractColumn column) {
        return column.setHeaderAlign("center").setOrderable(false).setClickable(false).setRequired(true);
    }

    // listeners

    public void onClickApply()
    {
        saveSettings();

        final Set<Long> selectedIds = ((CheckboxColumn) getRowDataSource().getColumn("select")).getSelected();
        final List<IEppEpvRowWrapper> selected = new ArrayList<>();
        for (IEppEpvRowWrapper wrapper : getRowDataSource().getEntityList())
        {
            if (selectedIds.contains(wrapper.getId())) {
                selected.add(wrapper);
            }
        }

        final EppState newState = getSettings().get("newState");

	    Consumer<Collection<IEppEpvRowWrapper>> worker;
	    if (isAlwaysCreateNew())
		    worker = selectedPart -> IEppRegistryDAO.instance.get().createAndAttachRegElements(selectedPart, newState, false);
	    else
	    {
		    final KeyGenRule keyGenRule = Boolean.TRUE.equals(getSettings().get("withIControl")) ? KeyGenRule.FULL : KeyGenRule.FULL_WITHOUT_I_CONTROL;
		    final Collection<EppState> filterStates = getSettings().get("filterStateList");
		    worker = selectedPart -> IEppRegistryDAO.instance.get().doAutoAttachRegElements(selectedPart, isCreateNewIfMissing(), keyGenRule, filterStates, newState);
	    }

	    doInProgressBar(worker, selected);

        deactivate();
    }

	private void doInProgressBar(Consumer<Collection<IEppEpvRowWrapper>> action, List<IEppEpvRowWrapper> selected)
	{
		IBackgroundProcess process = new BackgroundProcessBase()
		{
			@Override
			public ProcessResult run(ProcessState processState)
			{
				int currIndex = 0;
				final int step = 10;
				final int total = selected.size();
				for (List<IEppEpvRowWrapper> selectedPart : Iterables.partition(selected, step))
				{
					currIndex += selectedPart.size();
					action.accept(selectedPart);
					processState.setCurrentValue(currIndex * 100 / total);
				}
				return null;
			}
		};

		new BackgroundProcessHolder().start("Создание и прикрепление элементов реестра", process);
	}

    // G & S

    public EppEduPlanVersionBlock getBlock() {
        return _blockHolder.getValue();
    }

    public StaticListDataSource<IEppEpvRowWrapper> getRowDataSource()
    {
        return _rowDataSource;
    }

    public EntityHolder<EppEduPlanVersionBlock> getBlockHolder()
    {
        return _blockHolder;
    }

    protected Long getAttacheRuleId()
    {
        final DataWrapper wrapper = getSettings().get(ATTACH_RULE_SETTING_NAME);
        return wrapper != null ? wrapper.getId() : null;
    }

    public boolean isAlwaysCreateNew()
    {
        return EppEduPlanVersionAttachRegElements.ATTACH_RULE_ALWAYS_NEW.equals(getAttacheRuleId());
    }

    public boolean isCreateNewIfMissing()
    {
        return EppEduPlanVersionAttachRegElements.ATTACH_RULE_NEW_IF_MISSING.equals(getAttacheRuleId());
    }

    public boolean isAttachOnlyExists()
    {
        return EppEduPlanVersionAttachRegElements.ATTACH_RULE_ONLY_EXISTS.equals(getAttacheRuleId());
    }
}