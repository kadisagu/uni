/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionCopyScheduleData;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author nkokorina
 * Created on: 24.09.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final ErrorCollector errors = component.getUserContext().getErrorCollector();
        this.getDao().validate(model, errors);
        if (!errors.hasErrors())
        {
            this.getDao().update(model);
            this.deactivate(component);
        }
    }

    public void onChangeSourceEduPlanVersion(final IBusinessComponent component)
    {
        this.getDao().prepareCourseList(this.getModel(component));
    }

}
