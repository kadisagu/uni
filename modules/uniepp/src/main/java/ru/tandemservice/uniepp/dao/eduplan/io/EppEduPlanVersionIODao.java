package ru.tandemservice.uniepp.dao.eduplan.io;

import com.healthmarketscience.jackcess.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCache;
import ru.tandemservice.uni.dao.mdbio.BaseIODao;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;

import java.io.IOException;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EppEduPlanVersionIODao extends BaseIODao implements IEppEduPlanVersionIODao
{
    private static final String ROW_TYPE_CODE_GROUP = "g";
    private static final String ROW_TYPE_CODE_STRUCTURE = "s";
    private static final String ROW_TYPE_CODE_SELECTION = "v";
    private static final String ROW_TYPE_CODE_SELECTION_EMPTY = "x";
    private static final String ROW_TYPE_CODE_REGISTRY_ELEMENT = "r";
    private static final String ROW_TYPE_CODE_TEMPLATE = "t";

    private static final Set<String> ROW_TYPES = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(ROW_TYPE_CODE_GROUP, ROW_TYPE_CODE_STRUCTURE, ROW_TYPE_CODE_SELECTION, ROW_TYPE_CODE_SELECTION_EMPTY, ROW_TYPE_CODE_REGISTRY_ELEMENT, ROW_TYPE_CODE_TEMPLATE)));

    private static final String TABLE_VERSION_2_BLOCK = "epp_eduplanversion_block_t";
    private static final String COLUMN_VERSION_ID = "version_id";
    private static final String COLUMN_EDUHS_ID = "eduhs_id";

    private static final String TABLE_TUTOR_ORGUNIT = "epp_tutor_orgunit_t";
    private static final String COLUMN_TUTOR_ORGUNIT_ID = "orgunit_id";
    private static final String COLUMN_TUTOR_IMTSA_CODE = "imtsa_code";

    private static final String TABLE_EDUPLAN_ROWS = "epp_eduplanversion_row_t";
    private static final String COLUMN_ROW_VERSION_ID = "row_version_id";
    private static final String COLUMN_ROW_PATH = "row_path";
    private static final String COLUMN_ROW_EDUHS_ID = "row_eduhs_id";
    private static final String COLUMN_ROW_REG_ELEMENT_OWNER_ID = "row_owner_id";
    private static final String COLUMN_ROW_DSC_TYPE_P = "row_dsc_type_p";
    private static final String COLUMN_ROW_DSC_REL = "row_dsc_rel";
    private static final String COLUMN_ROW_INDEX_P = "row_index_p";
    private static final String COLUMN_ROW_TITLE_P = "row_title_p";
    private static final String COLUMN_ROW_TERMS_P = "row_terms_p";

    private static final String TABLE_EDUPLAN_ROW_TERM = "epp_eduplanversion_row_term_t";
    private static final String COLUMN_TERM = "term";
    private static final String COLUMN_WEEKS = "weeks_p";
    private static final String COLUMN_SIZE = "size_p";
    private static final String COLUMN_LABOR = "labor_p";
    private static final String COLUMN_AUDIT = "e_audit_p";
    private static final String COLUMN_A_LECT = "a_lect_p";
    private static final String COLUMN_A_PRACT = "a_pract_p";
    private static final String COLUMN_A_LAB = "a_lab_p";
    private static final String COLUMN_E_SELF = "e_self_p";
    private static final String COLUMN_ACTIONS = "actions_p";

    private static final String TABLE_EDUPLAN_VERSION = "epp_eduplanversion_t";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_STD_EDULVL_ID = "std_edulvl_id";
    private static final String COLUMN_STD_NUMBER = "std_number";
    private static final String COLUMN_PLAN_EDUHS_ID = "plan_eduhs_id";
    private static final String COLUMN_PLAN_DEVELOP_FORM_R = "plan_develop_form_r";
    private static final String COLUMN_PLAN_DEVELOP_COND_R = "plan_develop_cond_r";
    private static final String COLUMN_PLAN_DEVELOP_TECH_R = "plan_develop_tech_r";
    private static final String COLUMN_PLAN_DEVELOP_GRID_R = "plan_develop_grid_r";
    private static final String COLUMN_PLAN_NUMBER = "plan_number";
    private static final String COLUMN_VERSION_NUMBER = "version_number";
    private static final String COLUMN_VERSION_TITLE = "version_title";

    private static final Object IMPORT_MUTEX = new Object();

    private ICatalogIO<EppPlanStructure> catalogEppPlanStructure() { return this.catalog(EppPlanStructure.class, "title"); }
    private ICatalogIO<EppRegistryStructure> catalogEppRegistryStructure() { return this.catalog(EppRegistryStructure.class, "title"); }

    /** выгружаем связи УП(в) с НПВ */
    private void export_EppEduPlanVersionBlockList(final Database mdb, final Collection<Long> epvIds) throws IOException
    {
        if (null != mdb.getTable(TABLE_VERSION_2_BLOCK)) { return; }

        final Table epp_eduplanversion_block_t = new TableBuilder(TABLE_VERSION_2_BLOCK)
        .addColumn(new ColumnBuilder(COLUMN_VERSION_ID, DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder(COLUMN_EDUHS_ID, DataType.TEXT).setCompressedUnicode(true).toColumn())
        .toTable(mdb);

        final Session session = this.getSession();
        // todo DEV-6000
//        BatchUtils.execute(epvIds, 128, new BatchUtils.Action<Long>() {
//            @Override public void execute(final Collection<Long> ids) {
//                try {
//                    final List<EppEduPlanVersionBlock> blocks =  new DQLSelectBuilder()
//                    .fromEntity(EppEduPlanVersionBlock.class, "e").column(property("e"))
//                    .where(in(property(EppEduPlanVersionBlock.eduPlanVersion().id().fromAlias("e")), ids))
//                    .order(property(EppEduPlanVersionBlock.eduPlanVersion().id().fromAlias("e")))
//                    .order(property(EppEduPlanVersionBlock.educationLevelHighSchool().id().fromAlias("e")))
//                    .createStatement(session).list();
//
//                    for (final EppEduPlanVersionBlock block: blocks) {
//                        epp_eduplanversion_block_t.addRow(
//                            EppEduPlanVersionIODao.this.id(block.getEduPlanVersion().getId()),
//                            EppEduPlanVersionIODao.this.id(block.getEducationLevelHighSchool().getId())
//                        );
//                    }
//                } catch (final Throwable t) {
//                    throw CoreExceptionUtils.getRuntimeException(t);
//                }
//                session.clear();
//            }
//        });
    }

    /** выгружаем строки УП(в) с распределением нагрузки по семестрам */
    private void export_EppEduPlanVersionRowList(final Database mdb, final Collection<Long> epvIds, boolean exportRowData)
    {
        // todo DEV-6000
//        if (null != mdb.getTable(TABLE_EDUPLAN_ROWS)) { return; }
//
//        final Map<EppPlanStructure, String> eppPlanStructureMap = this.catalogEppPlanStructure().export(mdb);
//        final Map<EppRegistryStructure, String> eppRegistryStructureMap = this.catalogEppRegistryStructure().export(mdb);
//
//        this.export_EppEduPlanVersionBlockList(mdb, epvIds);
//
//        final Table epp_eduplanversion_row_t = new TableBuilder(TABLE_EDUPLAN_ROWS)
//        .addColumn(new ColumnBuilder(COLUMN_ROW_VERSION_ID, DataType.TEXT).setCompressedUnicode(true).toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_ROW_PATH, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//
//        .addColumn(new ColumnBuilder(COLUMN_ROW_EDUHS_ID, DataType.TEXT).setCompressedUnicode(true).toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_ROW_REG_ELEMENT_OWNER_ID, DataType.TEXT).setCompressedUnicode(true).toColumn())
//
//        .addColumn(new ColumnBuilder(COLUMN_ROW_DSC_TYPE_P, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_ROW_DSC_REL, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//
//        .addColumn(new ColumnBuilder(COLUMN_ROW_INDEX_P, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_ROW_TITLE_P, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_ROW_TERMS_P, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .toTable(mdb);
//
//        final Table epp_eduplanversion_row_term_t = new TableBuilder(TABLE_EDUPLAN_ROW_TERM)
//        .addColumn(new ColumnBuilder(COLUMN_ROW_VERSION_ID, DataType.TEXT).setCompressedUnicode(true).toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_ROW_PATH, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_TERM, DataType.INT).toColumn())
//
//        .addColumn(new ColumnBuilder(COLUMN_WEEKS, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_SIZE, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_LABOR, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_AUDIT, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_A_LECT, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_A_PRACT, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_A_LAB, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_E_SELF, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_ACTIONS, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .toTable(mdb);
//
//        // если грузить ничего не надо, то просто создаем таблицы и уходим
//        if (!exportRowData) { return; }
//
//        final Map<String, EppControlActionType> actionTypeMap = EppEduPlanVersionDataDAO.getActionTypeMap();
//
//        String rowTypeName;
//        String rowTypeParams;
//
//        final Map<Long, IEppEpvWrapper> map = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionDataMap(epvIds, true);
//        for (final IEppEpvWrapper wrapper: map.values())
//        {
//            for (final IEppEpvRowWrapper row: wrapper.getRowMap().values()) {
//                final Set<Integer> activeTermSet = row.getActiveTermSet();
//
//                // параметры записи
//                {
//                    rowTypeName = rowTypeParams = "";
//                    if (row.getRow() instanceof EppEpvGroupReRow)
//                    {
//                        rowTypeName = ROW_TYPE_CODE_GROUP;
//                    }
//                    else if (row.getRow() instanceof EppEpvGroupImRow)
//                    {
//                        rowTypeName = (((EppEpvGroupImRow)row.getRow()).getSize() > 0 ? ROW_TYPE_CODE_SELECTION : ROW_TYPE_CODE_SELECTION_EMPTY);
//                        rowTypeParams = String.valueOf(((EppEpvGroupImRow)row.getRow()).getSize());
//                    }
//                    else if (row.getRow() instanceof EppEpvStructureRow)
//                    {
//                        rowTypeName = ROW_TYPE_CODE_STRUCTURE;
//                        rowTypeParams = eppPlanStructureMap.get(((EppEpvStructureRow)row.getRow()).getValue());
//
//                    }
//                    else if (row.getRow() instanceof EppEpvRegistryRow)
//                    {
//                        rowTypeName = ROW_TYPE_CODE_REGISTRY_ELEMENT;
//                        rowTypeParams = eppRegistryStructureMap.get(((EppEpvRegistryRow)row.getRow()).getRegistryElementType());
//                    }
//                }
//
//                // добавляем запись
//                epp_eduplanversion_row_t.addRow(
//                    this.id(wrapper.getVersion().getId()),
//                    row.getHierarhyIndex(),
//                    this.id(row.getRow().getOwner().getEducationLevelHighSchool().getId()),
//
//                    ((row.getRow() instanceof EppEpvRegistryRow) ? this.id(((EppEpvRegistryRow)row.getRow()).getRegistryElementOwner().getId()) : ""),
//                    rowTypeName, // type.name
//                    rowTypeParams, // type.params
//
//                    row.getSelfIndexPart(),
//                    StringUtils.trimToEmpty(row.getTitle()),
//                    StringUtils.join(activeTermSet, ", ")
//                );
//
//                // добавляем распределение
//                if (row.getRow() instanceof EppEpvTermDistributedRow)
//                {
//                    activeTermSet.add(0);
//
//                    final Integer[] terms = activeTermSet.toArray(new Integer[activeTermSet.size()]);
//                    Arrays.sort(terms);
//
//                    for (final Integer term: terms)
//                    {
//                        // формы контроля (только для семестров)
//                        Set<String> actions = null;
//                        if (term > 0) {
//                            actions = new TreeSet<String>();
//                            for (final Map.Entry<String, EppControlActionType> a: actionTypeMap.entrySet()) {
//                                if ((a.getValue() instanceof EppFControlActionType) && (row.getActionSize(term, a.getKey()) > 0)) {
//                                    actions.add(a.getValue().getAbbreviation());
//                                }
//                            }
//                        }
//
//                        // всегда часами в семестр
//                        epp_eduplanversion_row_term_t.addRow(
//                            this.id(wrapper.getId()),
//                            row.getHierarhyIndex(),
//                            term,
//                            UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppLoadType.TYPE_TOTAL_WEEKS_FULL_CODE), false),
//                            UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppLoadType.TYPE_TOTAL_SIZE_FULL_CODE), false),
//                            UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppLoadType.TYPE_TOTAL_LABOR_FULL_CODE), false),
//                            UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppELoadType.FULL_CODE_TOTAL_AUDIT), false),
//                            UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_LECTURES), false),
//                            UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_PRACTICE), false),
//                            UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppALoadType.FULL_CODE_TOTAL_LABS), false),
//                            UniEppUtils.formatLoad(row.getTotalInTermLoad(term, EppELoadType.FULL_CODE_TOTAL_SELFWORK), false),
//                            (null == actions ? "" : StringUtils.join(actions, ", "))
//                        );
//                    }
//                }
//            }
//        }
    }

    public void export_EppTutorOrgUnitList(final Database mdb) throws IOException
    {
        if (null != mdb.getTable(TABLE_TUTOR_ORGUNIT)) { return; }

        final Table epp_tutor_orgunit_t = new TableBuilder(TABLE_TUTOR_ORGUNIT)
        .addColumn(new ColumnBuilder(COLUMN_TUTOR_ORGUNIT_ID, DataType.TEXT).setCompressedUnicode(true).toColumn())
        .addColumn(new ColumnBuilder(COLUMN_TUTOR_IMTSA_CODE, DataType.TEXT).setCompressedUnicode(true).toColumn())
        .toTable(mdb);

        final List<EppTutorOrgUnit> list = new DQLSelectBuilder()
        .fromEntity(EppTutorOrgUnit.class, "e").column(property("e"))
        .where(eq(property(EppTutorOrgUnit.orgUnit().archival().fromAlias("e")), value(Boolean.FALSE)))
        .order(property(EppTutorOrgUnit.orgUnit().title().fromAlias("e")))
        .createStatement(getSession()).list();

        final Set<String> imtsaCodeSet = new HashSet<>();
        for (final EppTutorOrgUnit tutorOu: list) {
            String codeImtsa = StringUtils.trimToNull(tutorOu.getCodeImtsa());
            if (null != codeImtsa) {
                if (!imtsaCodeSet.add(codeImtsa)) {
                    throw new ApplicationException("Получение шаблона импорта невозможно: поле «Код подразделения в ИМЦА» должно быть уникальным среди читающих подразделений.");
                }
                epp_tutor_orgunit_t.addRow(
                    Long.toHexString(tutorOu.getOrgUnit().getId()),
                    codeImtsa
                );
            }
        }

    }

    @Override
    public void export_EppEduPlanVersionList(final Database mdb, Collection<Long> epvIds, boolean exportRowData)
    {
        // todo DEV-6000
//        if (null != mdb.getTable(TABLE_EDUPLAN_VERSION)) { return; }
//
//        if (null == epvIds) {
//            epvIds = new DQLSelectBuilder()
//            .fromEntity(EppEduPlanVersion.class, "e").column(property("e.id"))
//            .order(property(EppEduPlanVersion.eduPlan().number().fromAlias("e")))
//            .order(property(EppEduPlanVersion.number().fromAlias("e")))
//            .createStatement(this.getSession()).list();
//        }
//
//        IStudentIODao.instance.get().export_EducationLevelsHighSchoolList(mdb);
//
//        this.export_EppTutorOrgUnitList(mdb);
//        this.export_EppEduPlanVersionRowList(mdb, epvIds, exportRowData);
//
//        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
//        final Map<DevelopForm, String> developFormMap = catalogIO.catalogDevelopForm().export(mdb);
//        final Map<DevelopCondition, String> developCondMap = catalogIO.catalogDevelopCondition().export(mdb);
//        final Map<DevelopTech, String> developTechMap = catalogIO.catalogDevelopTech().export(mdb);
//        final Map<DevelopGrid, String> developGridMap = catalogIO.catalogDevelopGrid().export(mdb);
//
//        final Table epp_eduplanversion_t = new TableBuilder(TABLE_EDUPLAN_VERSION)
//        .addColumn(new ColumnBuilder(COLUMN_ID, DataType.TEXT).setCompressedUnicode(true).toColumn())
//
//        .addColumn(new ColumnBuilder(COLUMN_STD_EDULVL_ID, DataType.TEXT).setCompressedUnicode(true).toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_STD_NUMBER, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//
//        .addColumn(new ColumnBuilder(COLUMN_PLAN_EDUHS_ID, DataType.TEXT).setCompressedUnicode(true).toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_PLAN_DEVELOP_FORM_R, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_PLAN_DEVELOP_COND_R, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_PLAN_DEVELOP_TECH_R, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_PLAN_DEVELOP_GRID_R, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_PLAN_NUMBER, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//
//        .addColumn(new ColumnBuilder(COLUMN_VERSION_NUMBER, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .addColumn(new ColumnBuilder(COLUMN_VERSION_TITLE, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
//        .toTable(mdb);
//
//        final Session session = this.getSession();
//        BatchUtils.execute(epvIds, 128, new BatchUtils.Action<Long>() {
//            @Override public void execute(final Collection<Long> ids) {
//                try {
//                    for (final EppEduPlanVersion ver: EppEduPlanVersionIODao.this.getList(
//                        EppEduPlanVersion.class,
//                        COLUMN_ID, ids,
//                        new String[] {
//                            EppEduPlanVersion.eduPlan().number().s(),
//                            EppEduPlanVersion.number().s()
//                        }
//                    )) {
//                        epp_eduplanversion_t.addRow(
//                            EppEduPlanVersionIODao.this.id(ver.getId()),
//
//                            // данные ГОС
//                            EppEduPlanVersionIODao.this.id(ver.getEduPlan().getParent().getEducationLevel().getId()),
//                            ver.getEduPlan().getParent().getNumber(),
//
//                            // данные УП
//                            EppEduPlanVersionIODao.this.id(ver.getEduPlan().getEducationLevelHighSchool().getId()),
//                            developFormMap.get(ver.getEduPlan().getDevelopForm()),
//                            developCondMap.get(ver.getEduPlan().getDevelopCondition()),
//                            developTechMap.get(ver.getEduPlan().getDevelopTech()),
//                            developGridMap.get(ver.getEduPlan().getDevelopGrid()),
//
//                            // данные версии
//                            StringUtils.trimToEmpty(ver.getEduPlan().getNumber()),
//                            StringUtils.trimToEmpty(ver.getNumber()),
//                            StringUtils.trimToEmpty(ver.getTitlePostfix())
//                        );
//                    }
//                } catch (final Throwable t) {
//                    throw CoreExceptionUtils.getRuntimeException(t);
//                }
//                session.clear();
//            }
//        });
//
//        this.export_AdditionalData(mdb);
    }

    protected void export_AdditionalData(final Database mdb)
    {

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // импорт //////////////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public Map<String, Long> doImport_EppEduPlanVersionList(final Database mdb) throws Exception
    {
        synchronized (IMPORT_MUTEX) {
            return doLoggedAction(() -> {
                try {
                    log4j_logger.info("Import started.");

                    final String CACHE_KEY = "EppEduPlanVersionIODao.doImport_EppEduPlanVersionList";
                    {
                        final Map<String, Long> epvIdMap = DaoCache.get(CACHE_KEY);
                        if (null != epvIdMap) { return epvIdMap; }
                    }

                    final Map<String, Long> epvIdMap = new HashMap<>();
                    doImport_EppEduPlanVersionList(mdb, epvIdMap);

                    DaoCache.put(CACHE_KEY, epvIdMap);

                    log4j_logger.info("Import finished.");

                    return epvIdMap ;
                }
                catch (ApplicationException e) {
                    throw e;
                }
                catch (Throwable t) {
                    log4j_logger.fatal(t.getMessage(), t);
                    throw new ApplicationException("Произошла ошибка при импорте данных, информацию об ошибке смотрите в логе.");
                }
            }, "eppEduPlanVersionImport");
        }
    }

    private void doImport_EppEduPlanVersionList(Database mdb, final Map<String, Long> epvIdMap)
    {
        // todo DEV-6000
//        final Table epp_eduplanversion_t = mdb.getTable(TABLE_EDUPLAN_VERSION);
//        if (null == epp_eduplanversion_t) { throw new ApplicationException("Таблица «"+TABLE_EDUPLAN_VERSION+"» отсутствует в базе данных."); }
//
//        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();
//        final Map<String, DevelopForm> developFormMap = catalogIO.catalogDevelopForm().lookup(true);
//        final Map<String, DevelopCondition> developCondMap = catalogIO.catalogDevelopCondition().lookup(true);
//        final Map<String, DevelopTech> developTechMap = catalogIO.catalogDevelopTech().lookup(true);
//        final Map<String, DevelopGrid> developGridMap = catalogIO.catalogDevelopGrid().lookup(true);
//
//        doImport_AdditionalEduPlanVersionData(mdb);
//
//        final Session session = this.getSession();
//
//        BatchUtils.execute(epp_eduplanversion_t, 32, new BatchUtils.Action<Map<String, Object>>()
//            {
//            // состояние по умолчанию
//            final EppState state = EppEduPlanVersionIODao.this.getCatalogItem(EppState.class, EppState.STATE_FORMATIVE);
//
//            // { eduLvl -> { number -> std } }
//            final Map<Long, Map<String, EppStateEduStandard>> eduStdCache = new HashMap<Long, Map<String, EppStateEduStandard>>();
//
//            // { std -> { eduHs -> { developcombination -> { number -> plan } } } }
//            final Map<Long, Map<Long, Map<IDevelopCombination, Map<String, EppEduPlan>>>> eduPlanCache = new HashMap<Long, Map<Long, Map<IDevelopCombination, Map<String, EppEduPlan>>>>();
//
//            final EppState stateFormative = EppEduPlanVersionIODao.this.getByNaturalId(new EppStateGen.NaturalId(EppState.STATE_FORMATIVE));
//            final EppLoadPresentation loadPresentation = EppLoadPresentation.TERM_TERM;
//            final EppViewTableDisciplines viewTableDiscipline = EppEduPlanVersionIODao.this.getByNaturalId(new EppViewTableDisciplinesGen.NaturalId(EppViewTableDisciplines.CODE_EXTEND));
//
//            @Override
//            public void execute(final Collection<Map<String, Object>> rows)
//            {
//                for (final Map<String, Object> row : rows) {
//                    final String version_id = (String) row.get(COLUMN_ID);
//                    final String context = "epp_eduplanversion_t[" + version_id + "]";
//                    try {
//                        final EppEduPlan plan = this.getEduPlan(row);
//                        final String version_number = StringUtils.trimToEmpty((String) row.get(COLUMN_VERSION_NUMBER));
//
//                        EppEduPlanVersion epv = new DQLSelectBuilder()
//                        .fromEntity(EppEduPlanVersion.class, "epv").column(property("epv"))
//                        .where(eq(property(EppEduPlanVersion.eduPlan().fromAlias("epv")), value(plan)))                // совпадает план
//                        .where(eq(property(EppEduPlanVersion.number().fromAlias("epv")), value(version_number)))       // совпадает номер
//                        .order(property("epv.id"))
//                        .createStatement(EppEduPlanVersionIODao.this.getSession()).setMaxResults(1).uniqueResult();
//
//                        if (null == epv) {
//                            epv = new EppEduPlanVersion();
//                            epv.setEduPlan(plan);
//                            epv.setState(this.stateFormative);
//                            epv.setNumber(version_number);
//                            epv.setRegistrationNumber(epv.getFullNumber());
//                            epv.setLoadPresentation(this.loadPresentation);
//                            epv.setViewTableDiscipline(this.viewTableDiscipline);
//                            session.save(epv);
//                        }
//
//                        if (!this.stateFormative.equals(epv.getState())) {
//                            log4j_logger.warn(context + ": state is not «formative». Version skipped.");
//                            continue;
//                        }
//
//                        if (!this.loadPresentation.equals(epv.getLoadPresentation())) {
//                            log4j_logger.warn(context + ": loadPresentation is not «term-term». Version skipped.");
//                            continue;
//                        }
//
//                        epvIdMap.put(version_id, epv.getId());
//
//                    }
//                    catch (final Throwable t) {
//                        log4j_logger.error(context + ": Exception while importing, see trace below. Version skipped.");
//                        log4j_logger.error(t);
//                    }
//                }
//
//                session.flush();
//                session.clear();
//            }
//
//            protected EppStateEduStandard getEduStd(final Map<String, Object> row)
//            {
//                final Long eduLvlId = EppEduPlanVersionIODao.this.id(row, COLUMN_STD_EDULVL_ID);
//                Map<String, EppStateEduStandard> number2stdMap = this.eduStdCache.get(eduLvlId);
//                if (null == number2stdMap) {
//                    this.eduStdCache.put(eduLvlId, number2stdMap = new HashMap<String, EppStateEduStandard>());
//
//                    final List<EppStateEduStandard> stds = new DQLSelectBuilder()
//                    .fromEntity(EppStateEduStandard.class, "x").column(property("x"))
//                    .where(eq(property(EppStateEduStandard.educationLevel().id().fromAlias("x")), value(eduLvlId)))
//                    .order(property(EppStateEduStandard.id().fromAlias("x")), OrderDirection.desc)
//                    .createStatement(EppEduPlanVersionIODao.this.getSession()).list();
//
//                    for (final EppStateEduStandard std : stds) {
//                        if (number2stdMap.containsKey(std.getNumber()))
//                            number2stdMap.put(std.getNumber(), null);
//                        number2stdMap.put(StringUtils.trimToEmpty(std.getNumber()), std);
//                    }
//                }
//
//                final String std_number = StringUtils.trimToEmpty((String) row.get(COLUMN_STD_NUMBER));
//
//                // смотрим в кэш (там уже есть значения из базы)
//                EppStateEduStandard std = number2stdMap.get(std_number);
//                if (null != std) { return std; }
//
//                // значение не уникально - не будем импортировать эту версию, исключение будет обработано выше
//                if (number2stdMap.containsKey(std_number))
//                    throw new IllegalStateException("EppStateEduStandard for row with version_id = [" + row.get(COLUMN_ID) + "] is not unique.");
//
//                // создаем ГОС
//                std = new EppStateEduStandard();
//                std.setEducationLevel((EducationLevels) session.load(EducationLevels.class, eduLvlId));
//                std.setComment("import");
//                std.setNumber(std_number);
//                std.setRegistrationNumber(std_number);
//                std.setTitlePostfix("i");
//                std.setState(this.state);
//                session.save(std);
//                session.flush();
//
//                number2stdMap.put(std_number, std);
//                return std;
//            }
//
//            protected EppEduPlan getEduPlan(final Map<String, Object> row)
//            {
//                final EppStateEduStandard std = this.getEduStd(row);
//                final Map<Long, Map<IDevelopCombination, Map<String, EppEduPlan>>> map = SafeMap.safeGet(this.eduPlanCache, std.getId(), HashMap.class);
//
//                final Long eduHSId = EppEduPlanVersionIODao.this.id(row, COLUMN_PLAN_EDUHS_ID);
//                final Map<IDevelopCombination, Map<String, EppEduPlan>> develop2PlanMap = SafeMap.safeGet(map, eduHSId, HashMap.class);
//
//                final IDevelopCombination develop = DevelopCombinationDAO.getDevelopCombinationDefinition(
//                    developFormMap.get(row.get(COLUMN_PLAN_DEVELOP_FORM_R)),
//                    developCondMap.get(row.get(COLUMN_PLAN_DEVELOP_COND_R)),
//                    developTechMap.get(row.get(COLUMN_PLAN_DEVELOP_TECH_R)),
//                    developGridMap.get(row.get(COLUMN_PLAN_DEVELOP_GRID_R))
//                );
//
//                Map<String, EppEduPlan> number2planMap = develop2PlanMap.get(develop);
//                if (null == number2planMap) {
//                    develop2PlanMap.put(develop, number2planMap = new HashMap<String, EppEduPlan>());
//
//                    final List<EppEduPlan> plans = new DQLSelectBuilder()
//                    .fromEntity(EppEduPlan.class, "p").column(property("p"))
//                    .order(property(EppEduPlan.id().fromAlias("p")))
//                    .where(eq(property(EppEduPlan.parent().id().fromAlias("p")), value(std.getId())))
//                    .where(eq(property(EppEduPlan.educationLevelHighSchool().id().fromAlias("p")), value(eduHSId)))
//                    .where(eq(property(EppEduPlan.developForm().fromAlias("p")), value(develop.getDevelopForm())))
//                    .where(eq(property(EppEduPlan.developCondition().fromAlias("p")), value(develop.getDevelopCondition())))
//                    .where(eq(property(EppEduPlan.developTech().fromAlias("p")), value(develop.getDevelopTech())))
//                    .where(eq(property(EppEduPlan.developGrid().fromAlias("p")), value(develop.getDevelopGrid())))
//                    .createStatement(EppEduPlanVersionIODao.this.getSession()).list();
//
//                    for (final EppEduPlan plan : plans) {
//                        if (number2planMap.containsKey(plan.getNumber()))
//                            number2planMap.put(plan.getNumber(), null);
//                        number2planMap.put(StringUtils.trimToEmpty(plan.getNumber()), plan);
//                    }
//                }
//
//                final String plan_number = StringUtils.trimToEmpty((String) row.get(COLUMN_PLAN_NUMBER));
//
//                // смотрим в кэш (там уже есть значения из базы)
//                EppEduPlan plan = number2planMap.get(plan_number);
//                if (null != plan) { return plan; }
//
//                // значение не уникально - не будем импортировать эту версию, исключение будет обработано выше
//                if (number2planMap.containsKey(plan_number))
//                    throw new IllegalStateException("EppEduPlan for row with version_id = [" + row.get(COLUMN_ID) + "] is not unique.");
//
//                // создаем УП
//                plan = new EppEduPlan();
//                plan.setParent(std);
//                plan.setNumber(plan_number /*uniq*/);
//                plan.setDevelopForm(develop.getDevelopForm());
//                plan.setDevelopCondition(develop.getDevelopCondition());
//                plan.setDevelopTech(develop.getDevelopTech());
//                plan.setDevelopGrid(develop.getDevelopGrid());
//                plan.setEducationLevelHighSchool((EducationLevelsHighSchool) session.load(EducationLevelsHighSchool.class, eduHSId));
//                plan.setEduStartYear(2000);
//                plan.setRegistrationNumber(plan_number);
//                plan.setTitlePostfix("i");
//                plan.setComment("import");
//                plan.setState(this.state);
//                session.save(plan);
//                session.flush();
//
//                number2planMap.put(plan_number, plan);
//                return plan;
//            }
//            });
//
//        session.flush();
//        session.clear();
//
//        this.doImport_EppEduPlanVersionBlockList(mdb, epvIdMap);
//        this.doImport_EppEduPlanVersionBlockRows(mdb, epvIdMap);
    }

    protected void doImport_AdditionalEduPlanVersionData(Database mdb)
    {

    }

    // список явно указанных блоков
    protected void doImport_EppEduPlanVersionBlockList(final Database mdb, final Map<String, Long> epvIdMap)
    {
        // todo DEV-6000
//        final Table epp_eduplanversion_block_t = mdb.getTable(TABLE_VERSION_2_BLOCK);
//        if (null == epp_eduplanversion_block_t) { throw new ApplicationException("Таблица «epp_eduplanversion_block_t» отсутствует в базе данных."); }
//
//        final Session session = this.getSession();
//        BatchUtils.execute(epp_eduplanversion_block_t, 256, new BatchUtils.Action<Map<String, Object>>() {
//            @Override public void execute(final Collection<Map<String, Object>> rows) {
//                boolean flag = false;
//                for (final Map<String, Object> row: rows) {
//                    final Long version_id = epvIdMap.get(row.get(COLUMN_VERSION_ID));
//                    if (null == version_id) { continue; }
//
//                    final Long eduhs_id = EppEduPlanVersionIODao.this.id(row, COLUMN_EDUHS_ID);
//
//                    final EppEduPlanVersion epv = (EppEduPlanVersion)session.load(EppEduPlanVersion.class, version_id);
//                    final EducationLevelsHighSchool eduHS = (EducationLevelsHighSchool)session.load(EducationLevelsHighSchool.class, eduhs_id);
//
//                    if (null == EppEduPlanVersionIODao.this.getByNaturalId(new EppEduPlanVersionBlockGen.NaturalId(epv, eduHS))) {
//                        session.save(new EppEduPlanVersionBlock(epv, eduHS));
//                        flag = true;
//                    }
//                }
//
//                if (flag) {
//                    session.flush();
//                    session.clear();
//                }
//            }
//        });
    }

    // список строк (вместе с блоками)
    protected void doImport_EppEduPlanVersionBlockRows(final Database mdb, final Map<String, Long> epvIdMap)
    {
        // todo DEV-6000
//        final Map<Integer, Term> termMap = new HashMap<Integer, Term>();
//        for (Term term : getList(Term.class))
//            termMap.put(term.getIntValue(), term);
//        final Map<String, String> caTypeMap = new HashMap<String, String>();
//        for (EppFControlActionType ca : getList(EppFControlActionType.class))
//            caTypeMap.put(ca.getAbbreviation(), ca.getFullCode());
//        final Map<String, EppPlanStructure> eppPlanStructureMap = catalogEppPlanStructure().lookup(true);
//        final Map<String, EppRegistryStructure> eppRegistryStructureMap = catalogEppRegistryStructure().lookup(true);
//        final Map<String, EppLoadType> loadTypeMap = new HashMap<String, EppLoadType>();
//        for (EppLoadType loadType : getList(EppLoadType.class))
//            loadTypeMap.put(loadType.getFullCode(), loadType);
//
//        final Table epp_eduplanversion_row_t = mdb.getTable(TABLE_EDUPLAN_ROWS);
//        if (null == epp_eduplanversion_row_t) { throw new ApplicationException("Таблица «epp_eduplanversion_row_t» отсутствует в базе данных."); }
//
//        final Session session = this.getSession();
//
//        final Set<Long> versionsWithErrors = new HashSet<Long>();
//
//        final Map<Long, Map<String, EpvIORowWrapper>> rowMap = new HashMap<Long, Map<String, EpvIORowWrapper>>();
//        final List<EpvIORowWrapper> rowList = new ArrayList<EpvIORowWrapper>();
//
//        for (final Map<String, Object> row: epp_eduplanversion_row_t) {
//            final Long version_id = epvIdMap.get((String) row.get(COLUMN_ROW_VERSION_ID));
//            if (null == version_id) {
//                continue; // не импортируем для версий, которые не обработаны на шаге выше
//            }
//
//            final Long eduhs_id = EppEduPlanVersionIODao.this.id(row, COLUMN_ROW_EDUHS_ID);
//
//            final EppEduPlanVersion epv = (EppEduPlanVersion)session.load(EppEduPlanVersion.class, version_id);
//            final EducationLevelsHighSchool eduHS = (EducationLevelsHighSchool)session.load(EducationLevelsHighSchool.class, eduhs_id);
//
//            INaturalIdentifiable block = EppEduPlanVersionIODao.this.getByNaturalId(new EppEduPlanVersionBlockGen.NaturalId(epv, eduHS));
//            if (null == block) {
//                session.save(block = new EppEduPlanVersionBlock(epv, eduHS));
//            }
//
//            EpvIORowWrapper rowWrapper = new EpvIORowWrapper(version_id, ( (IEntity) block).getId(), row, eppPlanStructureMap, eppRegistryStructureMap);
//            if (null != SafeMap.safeGet(rowMap, version_id, HashMap.class).put(rowWrapper.path, rowWrapper)) {
//                log4j_logger.error("Row with version_id = [" + rowWrapper.versionIdHex + "] and path = [" + rowWrapper.path + "] is mentioned in epp_eduplanversion_row more than once. Version[" + rowWrapper.versionIdHex + "] skipped.");
//                versionsWithErrors.add(version_id);
//                continue;
//            }
//            rowList.add(rowWrapper);
//        }
//
//        final Table epp_term_t = mdb.getTable(TABLE_EDUPLAN_ROW_TERM);
//        if (null == epp_term_t) { throw new ApplicationException("Таблица «epp_eduplanversion_row_term_t» отсутствует в базе данных."); }
//
//        for (final Map<String, Object> row: epp_term_t) {
//            Object versionIdHex = row.get(COLUMN_ROW_VERSION_ID);
//            final Long version_id = epvIdMap.get(versionIdHex);
//            if (null == version_id) {
//                continue; // не импортируем для версий, которые не обработаны на шаге выше
//            }
//
//            final String path = (String) row.get(COLUMN_ROW_PATH);
//
//            Map<String, EpvIORowWrapper> versionMap = rowMap.get(version_id);
//            if (null == versionMap) {
//                log4j_logger.error("Row with version_id = [" + versionIdHex + "] and path = [" + path + "] is mentioned in epp_eduplanversion_row_term_t and was not found or is incorrect. Version[" + versionIdHex + "] skipped.");
//                versionsWithErrors.add(version_id);
//                continue;
//            }
//
//            EpvIORowWrapper rowWrapper = versionMap.get(path);
//
//            if (null == rowWrapper) {
//                log4j_logger.error("Row with version_id = [" + versionIdHex + "] and path = [" + path + "] is mentioned in epp_eduplanversion_row_term_t and was not found or is incorrect. Version[" + versionIdHex + "] skipped.");
//                versionsWithErrors.add(version_id);
//                continue;
//            }
//
//            final MutableInt term = new MutableInt();
//            try {
//                term.setValue((Short) row.get(COLUMN_TERM));
//            }
//            catch (Exception e) {
//                log4j_logger.error("Can't recognize term = [" + row.get(COLUMN_TERM) + "] for row with version_id = [" + versionIdHex + "] and path = [" + path + "]. Version[" + versionIdHex + "] skipped.");
//                versionsWithErrors.add(version_id);
//                continue;
//            }
//
//            if (term.intValue() != 0 && !termMap.containsKey(term.intValue())) {
//                log4j_logger.error("Can't recognize term = [" + row.get(COLUMN_TERM) + "] for row with version_id = [" + versionIdHex + "] and path = [" + path + "]. Version[" + versionIdHex + "] skipped.");
//                versionsWithErrors.add(version_id);
//                continue;
//            }
//
//            if (term.intValue() != 0 && (rowWrapper.termActionMap.containsKey(term.intValue()) || rowWrapper.termLoadMap.containsKey(term.intValue()))) {
//                log4j_logger.error("Term = [" + row.get(COLUMN_TERM) + "] is mentioned in epp_eduplanversion_row_term_t more than one time for row with version_id = [" + versionIdHex + "] and path = [" + path + "]. Version[" + versionIdHex + "] skipped.");
//                versionsWithErrors.add(version_id);
//                continue;
//            }
//
//            if (0 == term.intValue() && null != rowWrapper.loadMap) {
//                log4j_logger.error("Term = [" + row.get(COLUMN_TERM) + "] is mentioned in epp_eduplanversion_row_term_t more than one time for row with version_id = [" + versionIdHex + "] and path = [" + path + "]. Version[" + versionIdHex + "] skipped.");
//                versionsWithErrors.add(version_id);
//                continue;
//            }
//
//            if (0 == term.intValue() && !rowWrapper.fillRowLoad(row)) {
//                versionsWithErrors.add(version_id);
//                continue;
//            }
//
//            if (0 != term.intValue() && !rowWrapper.fillRowTermLoad(term.intValue(), row)) {
//                versionsWithErrors.add(version_id);
//                continue;
//            }
//
//            if (0 != term.intValue() && !rowWrapper.fillRowTermActions(term.intValue(), row, caTypeMap)) {
//                versionsWithErrors.add(version_id);
//            }
//        }
//
//        Collections.sort(rowList, new Comparator<EpvIORowWrapper>()
//            {
//            @Override
//            public int compare(EpvIORowWrapper o1, EpvIORowWrapper o2)
//            {
//                return NumberAsStringComparator.INSTANCE.compare(o1.path, o2.path);
//            }
//            });
//
//        for (EpvIORowWrapper row : rowList) {
//            if (row.hasErrors) {
//                versionsWithErrors.add(row.versionId);
//                continue;
//            }
//
//            String parentPath = row.parentPath();
//            if (null != parentPath) {
//                EpvIORowWrapper parentRow = rowMap.get(row.versionId).get(parentPath);
//                if (null == parentRow) {
//                    versionsWithErrors.add(row.versionId);
//                    log4j_logger.error("Parent row is not found for row with version_id = [" + row.versionIdHex + "] and path = [" + row.path + "]. Version[" + row.versionIdHex + "] skipped.");
//                    continue;
//                }
//                if (ROW_TYPE_CODE_GROUP.equals(row.rowTypeName) && !(ROW_TYPE_CODE_GROUP.equals(parentRow.rowTypeName) || ROW_TYPE_CODE_STRUCTURE.equals(parentRow.rowTypeName))) {
//                    versionsWithErrors.add(row.versionId);
//                    log4j_logger.error("Parent row is wrong type for row with version_id = [" + row.versionIdHex + "] and path = [" + row.path + "]. Version[" + row.versionIdHex + "] skipped.");
//                    continue;
//                }
//                if (ROW_TYPE_CODE_STRUCTURE.equals(row.rowTypeName) && !ROW_TYPE_CODE_STRUCTURE.equals(parentRow.rowTypeName)) {
//                    log4j_logger.error("Parent row is wrong type for row with version_id = [" + row.versionIdHex + "] and path = [" + row.path + "]. Version[" + row.versionIdHex + "] skipped.");
//                    versionsWithErrors.add(row.versionId);
//                }
//            }
//        }
//
//        final OrgUnit academy = TopOrgUnit.getInstance();
//        final IEppEduPlanVersionDataDAO epvDataDAO = IEppEduPlanVersionDataDAO.instance.get();
//
//        for (Map.Entry<Long, Map<String, EpvIORowWrapper>> entry : rowMap.entrySet()) {
//            try {
//                Long versionId = entry.getKey();
//                if (versionsWithErrors.contains(versionId))
//                    continue;
//
//                if (existsEntity(EppEpvRow.class, EppEpvRow.owner().eduPlanVersion().id().s(), versionId)) {
//                    log4j_logger.error("Version with version_id = [" + id(versionId) + "] is not empty. Version[" + id(versionId) + "] skipped.");
//                    continue;
//                }
//
//                final Map<Long, Map<String, Double>> rowLoadMap = new HashMap<Long, Map<String, Double>>();
//                final Map<Long, Set<Integer>> rowTermMap = new HashMap<Long, Set<Integer>>();
//                final Map<Long, Map<Integer, Map<String, Double>>> rowTermLoadMap = new HashMap<Long, Map<Integer, Map<String, Double>>>();
//                final Map<Long, Map<Integer, Map<String, Integer>>> rowTermActionMap = new HashMap<Long, Map<Integer, Map<String, Integer>>>();
//
//                final Map<String, MutableInt> numberMap = SafeMap.get(MutableInt.class);
//
//                for (EpvIORowWrapper wrapper : rowList) {
//                    if (!versionId.equals(wrapper.versionId))
//                        continue;
//
//                    String parentPath = wrapper.parentPath();
//                    EppEduPlanVersionBlock block = (EppEduPlanVersionBlock) session.load(EppEduPlanVersionBlock.class, wrapper.blockId);
//
//                    if (ROW_TYPE_CODE_STRUCTURE.equals(wrapper.rowTypeName)) {
//                        EppEpvStructureRow newRow = new EppEpvStructureRow(block);
//                        newRow.setValue(wrapper.structureLevel);
//                        wrapper.createdRow = newRow;
//                    }
//                    else if (ROW_TYPE_CODE_GROUP.equals(wrapper.rowTypeName)) {
//                        EppEpvGroupReRow newRow = new EppEpvGroupReRow(block);
//                        newRow.setTitle(wrapper.rowTitle);
//                        newRow.setNumber(getNewNumber(numberMap, parentPath));
//                        wrapper.createdRow = newRow;
//                    }
//                    else if (ROW_TYPE_CODE_SELECTION.equals(wrapper.rowTypeName) || ROW_TYPE_CODE_SELECTION_EMPTY.equals(wrapper.rowTypeName)) {
//                        EppEpvGroupImRow newRow = new EppEpvGroupImRow(block);
//                        newRow.setSize(wrapper.size);
//                        newRow.setTitle(wrapper.rowTitle);
//                        newRow.setNumber(getNewNumber(numberMap, parentPath));
//                        newRow.setTotalLaborAsDouble(wrapper.loadLabor);
//                        newRow.setTotalSizeAsDouble(wrapper.loadSize);
//                        wrapper.createdRow = newRow;
//                    }
//                    else if (ROW_TYPE_CODE_REGISTRY_ELEMENT.equals(wrapper.rowTypeName)) {
//                        EppEpvRegistryRow newRow = new EppEpvRegistryRow(block);
//                        newRow.setRegistryElementType(wrapper.regLevel);
//                        if (null != wrapper.regElementOwnerId)
//                            newRow.setRegistryElementOwner((OrgUnit) session.load(OrgUnit.class, wrapper.regElementOwnerId));
//                        else
//                            newRow.setRegistryElementOwner(academy);
//                        newRow.setTitle(wrapper.rowTitle);
//                        newRow.setNumber(getNewNumber(numberMap, parentPath));
//                        newRow.setTotalLaborAsDouble(wrapper.loadLabor);
//                        newRow.setTotalSizeAsDouble(wrapper.loadSize);
//                        wrapper.createdRow = newRow;
//                    }
//
//                    if (null == wrapper.createdRow)
//                        continue;
//
//                    EpvIORowWrapper parentRowWrapper = rowMap.get(wrapper.versionId).get(parentPath);
//                    if (null != parentRowWrapper)
//                        wrapper.createdRow.setHierarhyParent(parentRowWrapper.createdRow);
//
//                    save(wrapper.createdRow);
//
//                    if (wrapper.createdRow instanceof EppEpvTermDistributedRow) {
//                        if (wrapper.loadMap != null)
//                            rowLoadMap.put(wrapper.createdRow.getId(), wrapper.loadMap);
//
//                        Set<Integer> terms = new HashSet<Integer>(wrapper.termLoadMap.keySet());
//                        terms.addAll(wrapper.termActionMap.keySet());
//                        rowTermMap.put(wrapper.createdRow.getId(), terms);
//
//                        rowTermLoadMap.put(wrapper.createdRow.getId(), wrapper.termLoadMap);
//                        rowTermActionMap.put(wrapper.createdRow.getId(), wrapper.termActionMap);
//                    }
//                }
//
//                epvDataDAO.doUpdateRowLoadMap(rowLoadMap);
//
//                Map<Long, Map<Integer, EppEpvRowTerm>> eppEpvRowTermMap = epvDataDAO.doUpdateTermRows(rowTermMap, true);
//                for (Map<Integer, EppEpvRowTerm> m: eppEpvRowTermMap.values()) {
//                    for (EppEpvRowTerm t: m.values()) {
//                        Map<String, Double> map = rowTermLoadMap.get(t.getRow().getId()).get(t.getTerm().getIntValue());
//                        t.setTotalTermLoadLaborAsDouble(map.get(EppLoadType.TYPE_TOTAL_LABOR_FULL_CODE));
//                        t.setTotalTermLoadSizeAsDouble(map.get(EppLoadType.TYPE_TOTAL_SIZE_FULL_CODE));
//                        t.setTotalTermLoadWeeksAsDouble(map.get(EppLoadType.TYPE_TOTAL_WEEKS_FULL_CODE));
//                        getSession().saveOrUpdate(t);
//                    }
//                }
//
//                epvDataDAO.doUpdateRowTermLoadMap(rowTermLoadMap);
//                epvDataDAO.doUpdateRowTermActionMap(rowTermActionMap);
//
//                getSession().flush();
//                getSession().clear();
//
//                doImport_AdditionalRowData(mdb, epvIdMap, rowMap);
//
//            } catch (Exception e) {
//                log4j_logger.error("Exception occured while importing rows. See trace below. Version[" + id(entry.getKey()) + "] skipped.");
//                log4j_logger.error(e);
//                getSession().clear();
//            }
//        }
    }

    protected void doImport_AdditionalRowData(final Database mdb, final Map<String, Long> epvIdMap, final Map<Long, Map<String, EpvIORowWrapper>> rowMap)
    {

    }

    private String getNewNumber(Map<String, MutableInt> numberMap, String parentPath)
    {
        MutableInt number = numberMap.get(parentPath);
        number.increment();
        return number.toString();
    }


    protected class EpvIORowWrapper
    {
        private String versionIdHex;
        private Long versionId;
        private Long blockId;

        private String path;
        private Long regElementOwnerId;

        private String rowTypeName;

        private String rowTitle;

        private Map<String, Double> loadMap;
        private Map<Integer, Map<String, Double>> termLoadMap = new HashMap<>();
        private Map<Integer, Map<String, Integer>> termActionMap = new HashMap<>();

        private boolean hasErrors;

        private int size;
        private EppPlanStructure structureLevel;
        private EppRegistryStructure regLevel;

        private double loadLabor;
        private double loadSize;

        private EppEpvRow createdRow;
        public EppEpvRow getCreatedRow() {return createdRow;}

        private EpvIORowWrapper(Long versionId, Long blockId, Map<String, Object> valueMap, Map<String, EppPlanStructure> eppPlanStructureMap, Map<String, EppRegistryStructure> eppRegistryStructureMap)
        {
            this.versionId = versionId;
            this.versionIdHex = id(versionId);
            this.blockId = blockId;
            this.path = (String) valueMap.get(COLUMN_ROW_PATH);
            this.regElementOwnerId = EppEduPlanVersionIODao.this.id(valueMap, COLUMN_ROW_REG_ELEMENT_OWNER_ID);
            this.rowTypeName = (String) valueMap.get(COLUMN_ROW_DSC_TYPE_P);
            if (!ROW_TYPES.contains(rowTypeName)) {
                hasErrors = true;
                log4j_logger.error("Unknown row type = [" + rowTypeName + "] for row with versionId = [" + versionIdHex + "] and path = [" + path + "]. Version[" + versionIdHex + "] skipped.");
            }
            Object rowParam = valueMap.get(COLUMN_ROW_DSC_REL);
            if (ROW_TYPE_CODE_SELECTION.equals(rowTypeName)) {
                try {
                    size = Integer.parseInt((String) rowParam);
                }
                catch (Exception e) {
                    hasErrors = true;
                    log4j_logger.error("Can't parse size = [" + rowParam + "] for row with versionId = [" + versionIdHex + "] and path = [" + path + "]. Version[" + versionIdHex + "] skipped.");
                }
            }
            else if (ROW_TYPE_CODE_STRUCTURE.equals(rowTypeName)) {
                structureLevel = eppPlanStructureMap.get(rowParam);
                if (null == structureLevel) {
                    hasErrors = true;
                    log4j_logger.error("Can't find structure level for id = [" + rowParam + "] for row with versionId = [" + versionIdHex + "] and path = [" + path + "]. Version[" + versionIdHex + "] skipped.");
                }
            }
            else if (ROW_TYPE_CODE_REGISTRY_ELEMENT.equals(rowTypeName) || ROW_TYPE_CODE_TEMPLATE.equals(rowTypeName)) {
                regLevel = eppRegistryStructureMap.get(rowParam);
                if (null == regLevel) {
                    hasErrors = true;
                    log4j_logger.error("Can't find registry element level for id = [" + rowParam + "] for row with versionId = [" + versionIdHex + "] and path = [" + path + "]. Version[" + versionIdHex + "] skipped.");
                }
            }
            this.rowTitle = (String) valueMap.get(COLUMN_ROW_TITLE_P);
        }

        private String parentPath()
        {
            if (StringUtils.isEmpty(path))
                return null;
            int index = path.lastIndexOf(".");
            if (-1 == index)
                return null;
            return path.substring(0, index);
        }

        public boolean fillRowLoad(Map<String, Object> valueMap)
        {
            try {
                loadSize = UniEppUtils.toDouble((String) valueMap.get(COLUMN_SIZE));
                loadLabor = UniEppUtils.toDouble((String) valueMap.get(COLUMN_LABOR));
                loadMap = new HashMap<>();
                loadMap.put(EppELoadType.FULL_CODE_AUDIT, UniEppUtils.toDouble((String) valueMap.get(COLUMN_AUDIT)));
                loadMap.put(EppALoadType.FULL_CODE_TOTAL_LECTURES, UniEppUtils.toDouble((String) valueMap.get(COLUMN_A_LECT)));
                loadMap.put(EppALoadType.FULL_CODE_TOTAL_PRACTICE, UniEppUtils.toDouble((String) valueMap.get(COLUMN_A_PRACT)));
                loadMap.put(EppALoadType.FULL_CODE_TOTAL_LABS, UniEppUtils.toDouble((String) valueMap.get(COLUMN_A_LAB)));
                loadMap.put(EppELoadType.FULL_CODE_SELFWORK, UniEppUtils.toDouble((String) valueMap.get(COLUMN_E_SELF)));
                return true;
            }
            catch (Exception e) {
                log4j_logger.error("Exception occured while parsing term load values for row with versionId = [" + versionIdHex + "] and path = [" + path + "]. See trace below. Version[" + versionIdHex + "] skipped.", e);
                return false;
            }
        }

        public boolean fillRowTermLoad(Integer term, Map<String, Object> valueMap)
        {
            try {
                HashMap<String, Double> termMap = new HashMap<>();
                termLoadMap.put(term, termMap);
                termMap.put(EppLoadType.FULL_CODE_WEEKS, UniEppUtils.toDouble((String) valueMap.get(COLUMN_WEEKS)));
                termMap.put(EppLoadType.FULL_CODE_TOTAL_HOURS, UniEppUtils.toDouble((String) valueMap.get(COLUMN_SIZE)));
                termMap.put(EppLoadType.FULL_CODE_LABOR, UniEppUtils.toDouble((String) valueMap.get(COLUMN_LABOR)));
                termMap.put(EppELoadType.FULL_CODE_AUDIT, UniEppUtils.toDouble((String) valueMap.get(COLUMN_AUDIT)));
                termMap.put(EppALoadType.FULL_CODE_TOTAL_LECTURES, UniEppUtils.toDouble((String) valueMap.get(COLUMN_A_LECT)));
                termMap.put(EppALoadType.FULL_CODE_TOTAL_PRACTICE, UniEppUtils.toDouble((String) valueMap.get(COLUMN_A_PRACT)));
                termMap.put(EppALoadType.FULL_CODE_TOTAL_LABS, UniEppUtils.toDouble((String) valueMap.get(COLUMN_A_LAB)));
                termMap.put(EppELoadType.FULL_CODE_SELFWORK, UniEppUtils.toDouble((String) valueMap.get(COLUMN_E_SELF)));
                return true;
            }
            catch (Exception e) {
                log4j_logger.error("Exception occured while parsing term load values for row with versionId = [" + versionIdHex + "] and path = [" + path + "]. See trace below. Version[" + versionIdHex + "] skipped.", e);
                return false;
            }
        }

        public boolean fillRowTermActions(Integer term, Map<String, Object> valueMap, Map<String, String> caTypeMap)
        {
            String actionsStr = (String) valueMap.get(COLUMN_ACTIONS);
            if (StringUtils.isEmpty(actionsStr))
                return true;
            Map<String, Integer> actions = new HashMap<>();
            for (String actionKey : actionsStr.split(", ")) {
                String code = caTypeMap.get(actionKey);
                if (code == null) {
                    log4j_logger.error("Exception occured while parsing term control actions for row with versionId = [" + versionIdHex + "] and path = [" + path + "]. See trace below. Version[" + versionIdHex + "] skipped.");
                    return false;
                }
                actions.put(code, 1);
            }
            termActionMap.put(term, actions);
            return true;
        }
    }

}
