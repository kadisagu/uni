package ru.tandemservice.uniepp.entity.plan.data;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.base.bo.EppState.EppStatePath;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvRowGen;

import java.util.*;

/**
 * Запись версии УП (базовая)
 */
@EppStatePath(EppEpvRow.L_OWNER+"."+EppEduPlanVersionBlock.L_EDU_PLAN_VERSION)
public abstract class EppEpvRow extends EppEpvRowGen implements IEppEpvRow, IEppStateObject
{

    /** сравнивает строки УП между собой внутри одного родительского элемента */
    public static final Comparator<EppEpvRow> IN_SAME_PARENT_ROW_COMPARATOR = (o1, o2) -> {
        {
            // сначала по блоку
            final int diff = EppEduPlanVersionBlock.COMPARATOR.compare(o1.getOwner(), o2.getOwner());
            if (0 != diff) { return diff; }
        }

        {
            // затем по типу строки
            final int diff = (o1.getComparatorClassIndex() - o2.getComparatorClassIndex());
            if (0 != diff) { return diff; }
        }

        final String v1 = StringUtils.trimToEmpty(o1.getComparatorString());
        final String v2 = StringUtils.trimToEmpty(o2.getComparatorString());
        if (v1.length() == v2.length()) { return v1.compareTo(v2); }

        final int max = Math.max(v1.length(), v2.length());
        final String p1 = StringUtils.leftPad(v1, max, '0');
        final String p2 = StringUtils.leftPad(v2, max, '0');
        return p1.compareTo(p2);
    };

    /** сравнивает строки внутри дерева (между разными узлами) */
    public static final Comparator<EppEpvRow> GLOBAL_ROW_COMPARATOR = new Comparator<EppEpvRow>() {

        private Iterator<EppEpvRow> path(EppEpvRow row) {
            final List<EppEpvRow> path = new ArrayList<>(10);
            while (null != row) {
                if (path.contains(row)) {
                    // ! в УП(в) есть циклы, увы, но это так !
                    Collections.reverse(path);
                    return path.iterator();
                }
                path.add(row);
                row = row.getHierarhyParent();
            }
            Collections.reverse(path);
            return path.iterator();
        }

        @Override public int compare(final EppEpvRow o1, final EppEpvRow o2)
        {
            // quick test
            {
                final EppEpvRow p1 = o1.getHierarhyParent();
                final EppEpvRow p2 = o2.getHierarhyParent();
                if (ObjectUtils.equals(p1, p2)) {
                    return IN_SAME_PARENT_ROW_COMPARATOR.compare(o1, o2);
                }
            }

            // long test
            final Iterator<EppEpvRow> p1 = path(o1);
            final Iterator<EppEpvRow> p2 = path(o2);

            while (p1.hasNext() && p2.hasNext()) {
                final EppEpvRow i1 = p1.next();
                final EppEpvRow i2 = p2.next();

                int i = IN_SAME_PARENT_ROW_COMPARATOR.compare(i1, i2);
                if (0 != i) { return i; }
            }

            // non-equal-length
            return Boolean.compare(p1.hasNext(), p2.hasNext());
        }
    };



    @Override public final EppState getState() {
        return this.getOwner().getEduPlanVersion().getState();
    }

    @Override public abstract EppEpvRow getHierarhyParent();
    public abstract void setHierarhyParent(EppEpvRow parent);

    public abstract int getComparatorClassIndex();
    public abstract String getComparatorString();

    @Override
    public abstract String getSelfIndexPart();

    @Override
    public abstract String getTitle();

    @Override
    @EntityDSLSupport
    public String getDisplayableTitle() {
        return this.getTitle();
    }


    @Override public String toString() {
        return (this.getId() + "@" + this.getClass().getSimpleName()+": " +this.getTitle());
    }

    @Override public String getQualificationCode() {
        final EppEduPlan plan = getOwner().getEduPlanVersion().getEduPlan();
        if (plan instanceof EppEduPlanProf) {
            return ((EppEduPlanProf) plan).getProgramQualification().getSubjectIndex().getPrefix();
        }
        return null;
    }

    @Override public EppGeneration getGeneration() {
        return this.getOwner().getGeneration();
    }


    public boolean isShowTotalWeeks() {
        return true;
    }

    public boolean isShowTotalSize() {
        return true; /*this.getOwner().getEduPlanVersion().getEduPlan().getParent().getGeneration().showTotalSize()*/
    }

    public boolean isShowTotalLabor() {
        return true; /*this.getOwner().getEduPlanVersion().getEduPlan().getParent().getGeneration().showTotalLabor()*/
    }


    @Override
    public Boolean getUsedInLoad()
    {
        if ((null != this.getHierarhyParent()) && Boolean.FALSE.equals(this.getHierarhyParent().getUsedInLoad()))
        {
            return Boolean.FALSE;
        }
        return !Boolean.TRUE.equals(this.isExcludedFromLoad());
    }

    public void setUsedInLoad(final Boolean used)
    {
        this.setExcludedFromLoad(!Boolean.TRUE.equals(used));
    }

    @Override
    public Boolean getUsedInActions()
    {
        if ((null != this.getHierarhyParent()) && Boolean.FALSE.equals(this.getHierarhyParent().getUsedInActions()))
        {
            return Boolean.FALSE;
        }
        return !Boolean.TRUE.equals(this.isExcludedFromActions());
    }

    public void setUsedInActions(final Boolean used)
    {
        this.setExcludedFromActions(!Boolean.TRUE.equals(used));
    }

    @Override
    public Class getRowEntityClass()
    {
        return getClass();
    }
}