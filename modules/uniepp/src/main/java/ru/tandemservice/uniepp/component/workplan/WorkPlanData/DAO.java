package ru.tandemservice.uniepp.component.workplan.WorkPlanData;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;

import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

public class DAO extends WorkPlanDataBaseDAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final EppWorkPlanBase wp = model.getHolder().refresh(EppWorkPlanBase.class);
        model.setSec(new CommonPostfixPermissionModel(wp instanceof EppWorkPlanVersion ? "eppWorkPlanVersion" : "eppWorkPlan"));
        super.prepare(model);
    }
}
