package ru.tandemservice.uniepp.component.edustd.EduStdImport;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.dao.eduStd.io.IEppEduStdIODAO;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void validate(final Model model, final ErrorCollector errors)
    {
        if (null != model.getUploadFile())
        {
            if (!model.getUploadFile().getFileName().endsWith(".xml"))
            {
                errors.add("Файл данных ГОСа должен быть в формате *.xml.");
            }
        }
    }

    @Override
    public void update(final Model model)
    {
        if (null != model.getUploadFile())
        {
            try
            {
                final byte[] content = IOUtils.toByteArray(model.getUploadFile().getStream());
                if ((null != content) && (content.length > 0))
                {
                    IEppEduStdIODAO.instance.get().doImportEduStdFromFile(model.getId(), content, false);
                }
            }
            catch (final IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
