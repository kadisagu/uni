package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Структура модулей"
 * Имя сущности : eppModuleStructure
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppModuleStructureCodes
{
    /** Константа кода (code) элемента : Общеобразовательные модули (title) */
    String BASE_MODULES = "1";

    Set<String> CODES = ImmutableSet.of(BASE_MODULES);
}
