package ru.tandemservice.uniepp.settings.bo.UsedTypesControlActions.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author avedernikov
 * @since 03.09.2015
 */

public class UsedTypesDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
	public UsedTypesDSHandler(String ownerId)
	{
		super(ownerId);
	}

	@Override
	protected DSOutput execute(DSInput input, ExecutionContext context)
	{
		final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppControlActionType.class, "ca")
                .column(property("ca"))
                .order(property("ca", EppControlActionType.title()));

		return DQLSelectOutputBuilder.get(input, dql, context.getSession()).build().transform(actionType -> {
            final DataWrapper wrapper = new DataWrapper(actionType);
            wrapper.put("finalAction", wrapper.<EppControlActionType>getWrapped() instanceof EppFControlActionType);
            wrapper.put("enabled", !wrapper.<EppControlActionType>getWrapped().isDisabled());
            return wrapper;
        });
	}
}
