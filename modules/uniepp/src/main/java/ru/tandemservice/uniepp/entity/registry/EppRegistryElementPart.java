package ru.tandemservice.uniepp.entity.registry;

import org.tandemframework.core.common.IEntityDebugTitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.BaseRegistryElementPartDSHandler;
import ru.tandemservice.uniepp.base.bo.EppState.EppStatePath;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementPartGen;

import java.util.Comparator;

/**
 * Часть (семестр) в элементе реестра
 */
@EppStatePath(EppRegistryElementPart.L_REGISTRY_ELEMENT)
public class EppRegistryElementPart extends EppRegistryElementPartGen implements IEppRegistryElementItem, IHierarchyItem, IEppStateObject, IEntityDebugTitled
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId) {
        return new BaseRegistryElementPartDSHandler(ownerId)
            .titleProperty(EppRegistryElementPart.titleWithNumber().s())
            ;
    }

    public static final Comparator<EppRegistryElementPart> FULL_COMPARATOR = (o1, o2) -> {
        int i;
        if (0 != (i = o1.getRegistryElement().getTitle().compareTo(o2.getRegistryElement().getTitle()))) { return i; }
        if (0 != (i = o1.getRegistryElement().getNumber().compareTo(o2.getRegistryElement().getNumber()))) { return i; }
        if (0 != (i = o1.getRegistryElement().getId().compareTo(o2.getRegistryElement().getId()))) { return i; }
        if (0 != (i = Integer.compare(o1.getNumber(), o2.getNumber()))) { return i; }
        return o1.getId().compareTo(o2.getId());
    };


    public EppRegistryElementPart() {}
    public EppRegistryElementPart(final EppRegistryElement registryElement, final int number) {
        this.setRegistryElement(registryElement);
        this.setNumber(number);
    }

    @Override public EppState getState() {
        return this.getRegistryElement().getState();
    }

    @Override public EppRegistryElement getHierarhyParent() {
        return this.getRegistryElement();
    }

    /** @return ([нагрузка в часах]ч., [номер части]/[число частей], [сокращение типа мероприятия реестра] №[номер]) */
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
    public String getTitlePostfix() {
        final EppRegistryElement rel = this.getRegistryElement();
        return " ("+rel.getFormattedLoad()+", "+(this.getNumber()+"/"+rel.getParts())+", "+rel.getNumberWithAbbreviation()+")";
    }

    /**
     * Вариант 1
     *
     * @return [Название дисциплины] ([нагрузка в часах]ч., [номер части]/[число частей], [сокращение типа мероприятия реестра] №[номер])
     */
    @Override
    @EntityDSLSupport(parts={EppRegistryElementPartGen.L_REGISTRY_ELEMENT+"."+EppRegistryElement.P_TITLE_WITH_NUMBER_AND_LOAD, EppRegistryElementPart.P_NUMBER})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
    public String getTitleWithNumber() {
        return this.getRegistryElement().getTitle() + this.getTitlePostfix();
    }

    /**
     * Вариант 2
     *
     * @return [Название дисциплины] ([нагрузка в часах]ч., [номер части]/[число частей])
     */
    @Override
    @EntityDSLSupport(parts={EppRegistryElementPartGen.L_REGISTRY_ELEMENT+"."+EppRegistryElement.P_TITLE_WITH_LOAD, EppRegistryElementPart.P_NUMBER})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
    public String getTitle() {
        final EppRegistryElement rel = this.getRegistryElement();
        if (rel == null) {
            return this.getClass().getSimpleName();
        }
        return rel.getTitle()+" ("+rel.getFormattedLoad()+", "+(this.getNumber()+"/"+rel.getParts())+")";
    }

    /**
     * Вариант 3
     *
     * @return [Название дисциплины] [номер части]/[число частей]
     */
    @Override
    @EntityDSLSupport(parts={EppRegistryElementPartGen.L_REGISTRY_ELEMENT+"."+EppRegistryElement.P_TITLE, EppRegistryElementPart.P_NUMBER})
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
    public String getTitleWithoutBraces() {
        final EppRegistryElement rel = this.getRegistryElement();
        return rel.getTitle()+" "+(this.getNumber()+"/"+rel.getParts());
    }

    /**
     * Вариант 4
     *
     * @return [сокр. название дисциплины] [номер части]/[число частей] [сокращение типа мероприятия реестра] №[номер]
     */
    @Override
    @EntityDSLSupport()
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
    public String getShortTitle() {
        final EppRegistryElement rel = this.getRegistryElement();
        return rel.getShortTitleSafe()+" "+(rel.getParts() > 1 ? this.getNumber()+"/"+rel.getParts() : "") + " " + rel.getNumberWithAbbreviation();
    }

    /**
     * Вариант 7
     */
    @Override
    @EntityDSLSupport()
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1061421")
    public String getTitleWithLabor()
    {
        final EppRegistryElement reg = getRegistryElement();
        String laborTitle = getLabor() < 0 ? "" : UniEppUtils.formatLoad(reg.getLaborAsDouble(), false) + " з.е., ";
        String titlePostfix = reg.getFormattedLoad() + ", " + (getNumber() + "/" + reg.getParts()) + ", " + reg.getNumberWithAbbreviation();
        return getRegistryElement().getTitle() + " (" + laborTitle + titlePostfix + ")";
    }

    public String getNumberWithAbbreviationWithPart() {
        final EppRegistryElement rel = this.getRegistryElement();
        return rel.getNumberWithAbbreviation() + " " + (rel.getParts() > 1 ? this.getNumber()+"/"+rel.getParts() : "");
    }

    public Double getSizeAsDouble() {
        final long size = this.getSize();
        return UniEppUtils.wrap(size < 0 ? null : size);
    }

    public void setSizeAsDouble(final Double value) {
        final Long size = UniEppUtils.unwrap(value);
        this.setSize(null == size ? -1 : size.longValue());
    }

    public Double getLaborAsDouble() {
        final long labor = this.getLabor();
        return UniEppUtils.wrap(labor < 0 ? null : labor);
    }

    public void setLaborAsDouble(final Double value) {
        final Long labor = UniEppUtils.unwrap(value);
        this.setLabor(null == labor ? -1 : labor.longValue());
    }

    public Double getWeeksAsDouble() {
        final long weeks = this.getWeeks();
        return UniEppUtils.wrap(weeks < 0 ? null : weeks);
    }

    public void setWeeksAsDouble(final Double value) {
        final Long weeks = UniEppUtils.unwrap(value);
        this.setWeeks(null == weeks ? -1 : weeks.longValue());
    }

    public OrgUnit getTutorOu()
    {
        return getRegistryElement().getOwner();
    }

    @Override
    public String getEntityDebugTitle()
    {
        return getTitleWithNumber();
    }
}