/* $Id$ */
package ru.tandemservice.uniepp.dao.eduplan.data;

import com.google.common.base.Preconditions;
import com.google.common.collect.ComparisonChain;

import java.util.Objects;

/**
 * @author Nikolay Fedorovskih
 * @since 21.08.2015
 */
public class EppCustomPlanRowWrapper implements IEppCustomPlanRowWrapper, Comparable
{
    private final IEppEpvRowWrapper sourceRow;
    private final int sourceTermNumber;
    private final String key;
    private String title;
    private int newTermNumber;
    private Boolean needRetake;
    private boolean excluded;

    public EppCustomPlanRowWrapper(IEppEpvRowWrapper sourceRow, int sourceTermNumber)
    {
        Preconditions.checkNotNull(sourceRow);

        this.sourceRow = sourceRow;
        this.sourceTermNumber = sourceTermNumber;
        this.key = sourceRow.getId() + "_" + sourceTermNumber;
    }

    @Override
    public int getNewTermNumber()
    {
        return this.newTermNumber;
    }

    @Override
    public void setNewTermNumber(int newTermNumber)
    {
        this.newTermNumber = newTermNumber;
    }

    @Override
    public int getSourceTermNumber()
    {
        return this.sourceTermNumber;
    }

    @Override
    public IEppEpvRowWrapper getSourceRow()
    {
        return this.sourceRow;
    }

    /** @return Строковый идентификатор вида sourceRowId_sourceTermNumber */
    @Override
    public String getKey()
    {
        return this.key;
    }

    @Override
    public String getTitle()
    {
        return this.title != null ? this.title : getSourceRow().getTitle();
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    @Override
    public Boolean getNeedRetake()
    {
        return this.needRetake;
    }

    @Override
    public void setNeedRetake(Boolean value)
    {
        this.needRetake = value;
    }

    @Override
    public boolean isExcluded()
    {
        // Удалять из плана можно только факультативные дисциплины
        return this.excluded && this.getSourceRow().isOptionalItem();
    }

    @Override
    public void setExcluded(boolean excluded)
    {
        this.excluded = excluded;
        if (excluded) {
            this.setNeedRetake(null);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || this.getClass() != o.getClass()) { return false; }
        final EppCustomPlanRowWrapper that = (EppCustomPlanRowWrapper) o;
        return Objects.equals(this.sourceRow, that.sourceRow) &&
               Objects.equals(this.sourceTermNumber, that.sourceTermNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.sourceRow.getRow(), this.sourceTermNumber);
    }

    @Override
    public int compareTo(Object o) {
        if (this.equals(o)) { return 0; }
        final EppCustomPlanRowWrapper that = (EppCustomPlanRowWrapper) o;
        return ComparisonChain.start()
                .compare(this.newTermNumber, that.newTermNumber)
                .compare(this.sourceRow.getIndex(), that.sourceRow.getIndex())
                .compare(this.sourceTermNumber, that.sourceTermNumber)
                .compare(this.sourceRow.getId(), that.sourceRow.getId())
                .result();
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName() + " [row: \"" + getSourceRow().getTitleWithIndex() + "\", term: " + getSourceTermNumber() + "]";
    }
}