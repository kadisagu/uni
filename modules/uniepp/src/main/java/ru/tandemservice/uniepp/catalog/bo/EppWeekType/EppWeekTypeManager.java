package ru.tandemservice.uniepp.catalog.bo.EppWeekType;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author avedernikov
 * @since 21.09.2015
 */

@Configuration
public class EppWeekTypeManager extends BusinessObjectManager
{
	public static EppWeekTypeManager instance()
	{
		return instance(EppWeekTypeManager.class);
	}
}