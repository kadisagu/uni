package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.ParametersEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 12.04.2016
 */
@Configuration
public class EppEduPlanParametersEdit extends BusinessComponentManager
{

	public static final String SUBJECT_INDEX_DS = "subjectIndexDS";
	public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
	public static final String PROGRAM_QUALIFICATION_DS = "programQualificationDS";
	public static final String PROGRAM_ORIENTATION_DS = "programOrientationDS";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
				.addDataSource(selectDS(SUBJECT_INDEX_DS, subjectIndexDSHandler()))
				.addDataSource(selectDS(PROGRAM_SUBJECT_DS, programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
				.addDataSource(selectDS(PROGRAM_QUALIFICATION_DS, programQualificationDSHandler()))
				.addDataSource(CommonBaseStaticSelectDataSource.selectDS(PROGRAM_ORIENTATION_DS, getName(), EduProgramOrientation.defaultSelectDSHandler(getName())))
				.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler subjectIndexDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
				.where(EduProgramSubjectIndex.programKind(), EppEduPlanParametersEditUI.PARAM_PROGRAM_KIND)
				.order(EduProgramSubjectIndex.title())
				.filter(EduProgramSubjectIndex.title());
	}

	@Bean
	public IDefaultComboDataSourceHandler programSubjectDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
				.customize((alias, dql, context, filter) ->
				{
					dql.where(eq(property(alias, EduProgramSubject.subjectIndex()), value((EduProgramSubjectIndex) context.get(EppEduPlanParametersEditUI.PARAM_SUBJECT_INDEX))));
					FilterUtils.applyLikeFilter(dql, filter, EduProgramSubject.subjectCode().fromAlias(alias), EduProgramSubject.title().fromAlias(alias));
					return dql;
				})
				.order(EduProgramSubject.subjectCode());

	}

	@Bean
	public IDefaultComboDataSourceHandler programQualificationDSHandler()
	{
		return new EntityComboDataSourceHandler(getName(), EduProgramQualification.class)
				.customize(((alias, dql, context, filter) ->
						dql.where(exists(EduProgramSubjectQualification.class,
								EduProgramSubjectQualification.programQualification().s(), property(alias),
								EduProgramSubjectQualification.programSubject().s(), value((EduProgramSubject)context.get(EppEduPlanParametersEditUI.PARAM_PROGRAM_SUBJECT))))))
				.order(EduProgramQualification.title())
				.filter(EduProgramQualification.title());
	}

}
