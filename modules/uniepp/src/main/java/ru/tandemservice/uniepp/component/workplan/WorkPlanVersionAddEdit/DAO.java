/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.workplan.WorkPlanVersionAddEdit;

import org.springframework.util.ClassUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

/**
 * @author nkokorina
 * Created on: 10.08.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final IEntity entity = (null == model.getContextId() ? null : this.get(model.getContextId()));
        if (entity instanceof EppWorkPlanVersion)
        {
            model.setElement((EppWorkPlanVersion) entity);
        }
        else if (entity instanceof EppWorkPlan)
        {
            if (null == model.getElement()) {
                model.setElement(new EppWorkPlanVersion());
            }
            model.getElement().setParent((EppWorkPlan) entity);
            model.getElement().setState(this.getCatalogItem(EppState.class, EppState.STATE_FORMATIVE));
            model.getElement().setNumber(INumberQueueDAO.instance.get().getNextNumber(model.getElement()));
            model.getElement().setRegistrationNumber(model.getElement().getFullNumber());
        }
        else
        {
            throw new IllegalArgumentException(null == entity ? "" : ClassUtils.getUserClass(entity).getName());
        }
    }

    @Override
    public void update(final Model model)
    {
        final EppWorkPlanVersion element = model.getElement();
        if (null != element.getId())
        {
            this.getSession().saveOrUpdate(element);
        }
        else
        {
            this.getSession().saveOrUpdate(element);
            IEppWorkPlanDAO.instance.get().doGenerateWorkPlanVersionRows(element);
        }
    }
}
