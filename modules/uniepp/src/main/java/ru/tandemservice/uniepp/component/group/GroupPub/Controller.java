package ru.tandemservice.uniepp.component.group.GroupPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import ru.tandemservice.uni.entity.orgstruct.Group;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Group group = ((ru.tandemservice.uni.component.group.GroupPub.Model)component.getModel(component.getName())).getGroup();

        final Model model = this.getModel(component);
        model.setGroup(group);

        this.getDao().prepare(model);
    }
}
