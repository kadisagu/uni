package ru.tandemservice.uniepp.entity.student.group;

import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroup4LoadTypeRowGen;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeALoad;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;

import java.util.Date;

/**
 * Студент в группе (по виду нагрузки)
 */
public class EppRealEduGroup4LoadTypeRow extends EppRealEduGroup4LoadTypeRowGen
{

    public static final IEppRealEduGroupRowDescription RELATION = new IEppRealEduGroupRowDescription()
    {
        @Override public Class<EppStudentWpeALoad> studentClass() { return EppStudentWpeALoad.class; }
        @Override public Class<EppRealEduGroup4LoadType> groupClass() { return EppRealEduGroup4LoadType.class; }
        @Override public Class<EppRealEduGroup4LoadTypeRow> relationClass() { return EppRealEduGroup4LoadTypeRow.class; }
        @Override public Class<? extends EppGroupType> typeKlass() { return EppGroupTypeALT.class; }

        @Override public EppRealEduGroup buildGroup(final EppRealEduGroupSummary summary, final EppRegistryElementPart activityPart, final EppGroupType groupType, final EppRealEduGroupCompleteLevel level, final String title, final Date creationDate) {
            final EppRealEduGroup4LoadType group = new EppRealEduGroup4LoadType(summary, activityPart, groupType);
            group.setLevel(level);
            group.setTitle(title);
            group.setCreationDate(creationDate);
            group.setModificationDate(creationDate);
            return group;
        }

        @Override public EppRealEduGroupRow buildRelation(final EppRealEduGroup group, final EppStudentWpePart student) {
            return new EppRealEduGroup4LoadTypeRow((EppRealEduGroup4LoadType)group, (EppStudentWpeALoad)student);
        }
    };


    public EppRealEduGroup4LoadTypeRow() {}

    public EppRealEduGroup4LoadTypeRow(final EppRealEduGroup4LoadType group, final EppStudentWpeALoad student) {
        this.setGroup(group);
        this.setStudentWpePart(student);
    }

    @Override
    public EppRealEduGroup4LoadTypeRow clone(final EppRealEduGroup group) {
        final EppRealEduGroup4LoadTypeRow rel = new EppRealEduGroup4LoadTypeRow();
        rel.update(this);
        rel.setGroup(group);
        rel.setRemovalDate(null);
        rel.setConfirmDate(null);
        return rel;
    }

    @Override
    public EppRealEduGroup4LoadType getGroup()
    {
        return (EppRealEduGroup4LoadType) super.getGroup();
    }

    @Override
    public EppStudentWpeALoad getStudentWpePart()
    {
        return (EppStudentWpeALoad) super.getStudentWpePart();
    }
}