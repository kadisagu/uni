package ru.tandemservice.uniepp.extreports.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Контекст внешнего отчета"
 * Имя сущности : extReportContext
 * Файл data.xml : uniepp.extreport.data.xml
 */
public interface ExtReportContextCodes
{
    /** Константа кода (code) элемента : Глобальные отчеты (title) */
    String GLOBAL_REPORT_CONTEXT = "globalReportContext";
    /** Константа кода (code) элемента : Головное подразделение (title) */
    String TOP_ORG_UNIT_CONTEXT = "uniTopOrgUnitContext";
    /** Константа кода (code) элемента : Деканат (title) */
    String EPP_GROUP_ORG_UNIT_CONTEXT = "uniEppGroupOrgUnitContext";

    Set<String> CODES = ImmutableSet.of(GLOBAL_REPORT_CONTEXT, TOP_ORG_UNIT_CONTEXT, EPP_GROUP_ORG_UNIT_CONTEXT);
}
