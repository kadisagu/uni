/* $Id$ */
package ru.tandemservice.uniepp.component.settings.DefaultGroupTypeForICA;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;

import java.util.List;

/**
 * @author oleyba
 * @since 8/24/11
 */
public class Model
{
    private List<EppIControlActionType> caList;
    private EppIControlActionType currentCA;
    private ISelectModel groupTypeModel;

    public List<EppIControlActionType> getCaList()
    {
        return this.caList;
    }

    public void setCaList(final List<EppIControlActionType> caList)
    {
        this.caList = caList;
    }

    public EppIControlActionType getCurrentCA()
    {
        return this.currentCA;
    }

    public void setCurrentCA(final EppIControlActionType currentCA)
    {
        this.currentCA = currentCA;
    }

    public ISelectModel getGroupTypeModel()
    {
        return groupTypeModel;
    }

    public void setGroupTypeModel(ISelectModel groupTypeModel)
    {
        this.groupTypeModel = groupTypeModel;
    }
}
