package ru.tandemservice.uniepp.dao.eduplan.export.highSchool;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author nkokorina
 *
 */

@XmlRootElement(name = "Документ")
public class XmlDocumentData
{
    @XmlAttribute(name = "Тип")
    public String type;
    @XmlAttribute(name = "КонтрольнаяСумма")
    public String checkSum;
    @XmlElement(name = "План", type = XmlEppEduPlan.class)
    public XmlEppEduPlan element;

    @XmlType
    public static class XmlEppEduPlan
    {
        @XmlAttribute(name = "ПодТип")
        public String subType;
        @XmlAttribute(name = "Шифр")
        public String code;
        @XmlAttribute(name = "ОбразовательнаяПрограмма")
        public String educationProgram;
        @XmlAttribute(name = "ФормаОбучения")
        public String developForm;
        @XmlAttribute(name = "УровеньОбразования")
        public String educationLevel;

        @XmlElement(name = "Титул", type = XmlPlanTitul.class)
        public XmlPlanTitul planTitle;
        @XmlElement(name = "СтрокиПлана", type = XmlPlanLines.class)
        public XmlPlanLines planLines;
        @XmlElement(name = "СпецВидыРабот", type = XmlPlanTypeWorks.class)
        public XmlPlanTypeWorks planTypeWorks;
        @XmlElement(name = "Нормы", type = XmlPlanStandards.class)
        public XmlPlanStandards planStandards;
    }

    @XmlType
    public static class XmlPlanTitul
    {
        @XmlAttribute(name = "ИмяПлана")
        public String titulplanTitle;
        @XmlAttribute(name = "ПолноеИмяПлана")
        public String titulplanFullTitle;
        @XmlAttribute(name = "НомерПользователя")
        public String titulUserNumber;
        @XmlAttribute(name = "Головная")
        public String titulHead;
        @XmlAttribute(name = "ПоследнийШифр")
        public String titullastCode;
        @XmlAttribute(name = "НеАвтоНедели")
        public String titulNoAutoWeeks;
        @XmlAttribute(name = "ДатаГОСа")
        public String titulDateGos;
        @XmlAttribute(name = "ДатаСертификатаИМЦА")
        public String titulDateCertificate;
        @XmlAttribute(name = "ВидПлана")
        public String titulPlanType;
        @XmlAttribute(name = "ГодНачалаПодготовки")
        public String titulYearStartTraining;
        @XmlAttribute(name = "КодКафедры")
        public String titulCodeCathedra;
        @XmlAttribute(name = "Факультет")
        public String titulFaculty;
        @XmlAttribute(name = "Приложение")
        public String titulApplication;
        @XmlAttribute(name = "ДатаПриложения")
        public String titulDateApplication;
        @XmlAttribute(name = "ВерсияПриложения")
        public String titulVersionApplication;

        @XmlElement(name = "АтрибутыЦиклов", type = XmlPlanCyclesAttributes.class)
        public XmlPlanCyclesAttributes cyclesAttributes;
        @XmlElement(name = "Утверждение", type = XmlPlanApproval.class)
        public XmlPlanApproval approval;
        @XmlElement(name = "Разработчики", type = XmlPlanDevelopers.class)
        public XmlPlanDevelopers developers;
        @XmlElement(name = "Квалификации", type = XmlPlanQualifications.class)
        public XmlPlanQualifications qualifications;
        @XmlElement(name = "Специальности", type = XmlPlanSpecialties.class)
        public XmlPlanSpecialties specialties;
        @XmlElement(name = "ГрафикУчПроцесса", type = XmlPlanScheduleProcess.class)
        public XmlPlanScheduleProcess scheduleProcess;
    }

    @XmlType
    public static class XmlPlanCyclesAttributes
    {
        @XmlElements(@XmlElement(name = "Цикл", type = XmlPlanCycle.class))
        public List<XmlPlanCycle> planCycles;
    }

    @XmlType
    public static class XmlPlanApproval
    {
        @XmlAttribute(name = "Дата")
        public String approvalDate;
        @XmlAttribute(name = "НомерПротокола")
        public String approvalProtocolNumber;
    }

    @XmlType
    public static class XmlPlanDevelopers
    {
        @XmlElements(@XmlElement(name = "Разработчик", type = XmlPlanDeveloper.class))
        public List<XmlPlanDeveloper> planDevelopers;
    }

    @XmlType
    public static class XmlPlanDeveloper
    {
        @XmlAttribute(name = "Ном")
        public String developerNom;
        @XmlAttribute(name = "Должность")
        public String developerPost;
    }

    @XmlType
    public static class XmlPlanQualifications
    {
        @XmlElements(@XmlElement(name = "Квалификация", type = XmlPlanQualification.class))
        public List<XmlPlanQualification> planQualifications;
    }

    @XmlType
    public static class XmlPlanQualification
    {
        @XmlAttribute(name = "Ном")
        public String qualificationNom;
        @XmlAttribute(name = "Название")
        public String qualificationTitle;
        @XmlAttribute(name = "СрокОбучения")
        public String qualificationLearning;
    }

    @XmlType
    public static class XmlPlanSpecialties
    {
        @XmlElements(@XmlElement(name = "Специальность", type = XmlPlanSpecialitie.class))
        public List<XmlPlanSpecialitie> planSpecialities;
    }

    @XmlType
    public static class XmlPlanSpecialitie
    {
        @XmlAttribute(name = "Ном")
        public String specialitieNom;
        @XmlAttribute(name = "Название")
        public String specialitieTitle;
    }

    @XmlType
    public static class XmlPlanCycle
    {
        @XmlAttribute(name = "Ном")
        public String cycleNom;
        @XmlAttribute(name = "ДопустимоеОтклонениеНеФ")
        public String cycleDeviation;
        @XmlAttribute(name = "Абревиатура")
        public String cycleAbbreviation;
        @XmlAttribute(name = "Название")
        public String cycleTitle;
    }

    @XmlType
    public static class XmlPlanScheduleProcess
    {
        @XmlElements(@XmlElement(name = "Курс", type = XmlPlanScheduleCourse.class))
        public List<XmlPlanScheduleCourse> planScheduleCourses;
    }

    @XmlType
    public static class XmlPlanScheduleCourse
    {
        @XmlAttribute(name = "Ном")
        public String scheduleNom;
        @XmlAttribute(name = "График")
        public String scheduleGraph;
        @XmlAttribute(name = "НедОсень")
        public String scheduleWeekAut;
        @XmlAttribute(name = "НедВесна")
        public String scheduleWeekSp;
        @XmlAttribute(name = "Студентов")
        public String scheduleStudent;
        @XmlAttribute(name = "Групп")
        public String scheduleGroup;
    }

    @XmlType
    public static class XmlPlanLines
    {
        @XmlElements(@XmlElement(name = "Строка", type = XmlPlanLine.class))
        public List<XmlPlanLine> planLineList;
    }

    @XmlType
    public static class XmlPlanLine
    {
        @XmlAttribute(name = "Дис")
        public String lineTitle;
        @XmlAttribute(name = "Цикл")
        public String lineCycle;
        @XmlAttribute(name = "ИдентификаторВидаПлана")
        public String lineIdPlanType;
        @XmlAttribute(name = "ГОС")
        public String lineGos;
        @XmlAttribute(name = "СР")
        public String lineSR;
        @XmlAttribute(name = "СемЭкз")
        public String lineTermExam;
        @XmlAttribute(name = "СемЗач")
        public String lineTermTest;
        @XmlAttribute(name = "СемКР")
        public String lineTermCW;
        @XmlAttribute(name = "СемКП")
        public String lineTermCP;
        @XmlAttribute(name = "Кафедра")
        public String lineCathedra;
        @XmlAttribute(name = "ПодлежитИзучению")
        public String lineBeStudied;
        @XmlAttribute(name = "ИдентификаторДисциплины")
        public String lineIdDiscipline;
        @XmlAttribute(name = "ДисциплинаДляРазделов")
        public String lineDisciplineToCategories;
        @XmlAttribute(name = "Раздел")
        public String lineCategorie;
        @XmlAttribute(name = "НеСчитатьКонтроль")
        public String lineNotCountControl;

        @XmlElement(name = "КурсовойПроект", type = XmlCourseProject.class)
        public XmlCourseProject planCourseProject;
        @XmlElement(name = "КурсоваяРабота", type = XmlCourseWork.class)
        public XmlCourseWork planCourseWork;
        @XmlElements(@XmlElement(name = "Сем", type = XmlPlanLineTerm.class))
        public List<XmlPlanLineTerm> planLineTerm;
    }

    @XmlType
    public static class XmlCourseProject
    {
        @XmlElements(@XmlElement(name = "Семестр", type = XmlCourseProjectTerm.class))
        public List<XmlCourseProjectTerm> planCourseProjectTerm;
    }

    @XmlType
    public static class XmlCourseWork
    {
        @XmlElements(@XmlElement(name = "Семестр", type = XmlCourseProjectTerm.class))
        public List<XmlCourseProjectTerm> planCourseWorkTerm;
    }

    @XmlType
    public static class XmlCourseProjectTerm
    {
        @XmlAttribute(name = "Семестр")
        public String term;
        @XmlElement(name = "РаспределениеСтудентовПоКафедрам", type = XmlCPDistributionStudents.class)
        public XmlCPDistributionStudents planCPDistributionStudent;
    }

    @XmlType
    public static class XmlCPDistributionStudents
    {
        @XmlAttribute(name = "Ном")
        public String planCPNom;
        @XmlAttribute(name = "Студентов")
        public String planCPStudents;
        @XmlAttribute(name = "Кафедра")
        public String planCPCathedra;
    }

    @XmlType
    public static class XmlPlanLineTerm
    {
        @XmlAttribute(name = "Ном")
        public String planLineTermNom;
        @XmlAttribute(name = "Лек")
        public String planLineTermLec;
        @XmlAttribute(name = "Пр")
        public String planLineTermPR;
        @XmlAttribute(name = "Лаб")
        public String planLineTermLab;
        @XmlAttribute(name = "СРС")
        public String planLineTermCPC;
    }

    @XmlType
    public static class XmlPlanTypeWorks
    {
        @XmlElement(name = "УчебПрактики", type = XmlPlanSpecWorkTypeEduPracticeList.class)
        public XmlPlanSpecWorkTypeEduPracticeList eduPracticeList = new XmlPlanSpecWorkTypeEduPracticeList();
        @XmlElement(name = "ПрочиеПрактики", type = XmlPlanSpecWorkTypeAnotherPracticeList.class)
        public XmlPlanSpecWorkTypeAnotherPracticeList anotherPracticeList = new XmlPlanSpecWorkTypeAnotherPracticeList();
    }

    @XmlType
    public static class XmlPlanSpecWorkTypePractice
    {
        @XmlAttribute(name = "Вид")
        public String title;
        @XmlAttribute(name = "Сем")
        public String term;
        @XmlAttribute(name = "Нед")
        public String weeks;
        @XmlAttribute(name = "Часов")
        public String size;
        @XmlAttribute(name = "ВидНорматива")
        public String normativeKind="";
        @XmlAttribute(name = "Суммировать")
        public String summ="1";
    }

    @XmlType
    public static class XmlPlanSpecWorkTypeEduPracticeList
    {
        @XmlElements(@XmlElement(name = "УчебПрактика", type = XmlPlanSpecWorkTypePractice.class))
        public List<XmlPlanSpecWorkTypePractice> list = new ArrayList<XmlPlanSpecWorkTypePractice>(0);
    }

    @XmlType
    public static class XmlPlanSpecWorkTypeAnotherPracticeList
    {
        @XmlElements(@XmlElement(name = "ПрочаяПрактика", type = XmlPlanSpecWorkTypePractice.class))
        public List<XmlPlanSpecWorkTypePractice> list = new ArrayList<XmlPlanSpecWorkTypePractice>(0);
    }

    @XmlType
    public static class XmlPlanStandards
    {

    }

}