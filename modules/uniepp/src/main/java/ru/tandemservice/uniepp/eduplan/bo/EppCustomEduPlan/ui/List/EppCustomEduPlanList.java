/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.EduProgramHigherProfManager;
import ru.tandemservice.uniepp.base.bo.EppState.EppStateManager;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.logic.CustomEduPlanDSHandler;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil.applySelectFilter;
import static ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.logic.CustomEduPlanDSHandler.*;

/**
 * @author Nikolay Fedorovskih
 * @since 14.08.2015
 */
@Configuration
public class EppCustomEduPlanList extends BusinessComponentManager
{
    public static final String CUSTOM_EDU_PLAN_DS = "customEduPlanDS";
    public static final String SUBJECT_INDEX_DS = "subjectIndexDS";
    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
    public static final String PROGRAM_FORM_DS = "programFormDS";
    public static final String DEVELOP_CONDITION_DS = "developConditionDS";
    public static final String PROGRAM_TRAIT_DS = "programTraitDS";
    public static final String DEVELOP_GRID_DS = "developGridDS";
    public static final String EDU_PLAN_VERSION_DS = "eduPlanVersionDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EduProgramHigherProfManager.instance().programKindHigherProfDSConfig())
                .addDataSource(selectDS(SUBJECT_INDEX_DS, subjectIndexDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_DS, programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(selectDS(PROGRAM_FORM_DS, programFormDSHandler()))
                .addDataSource(selectDS(DEVELOP_CONDITION_DS, developConditionDSHandler()))
                .addDataSource(selectDS(PROGRAM_TRAIT_DS, programTraitDSHandler()))
                .addDataSource(selectDS(DEVELOP_GRID_DS, developGridDSHandler()))
                .addDataSource(selectDS(EDU_PLAN_VERSION_DS, eduPlanVersionDSHandler()))
                .addDataSource(EppStateManager.instance().eppStateDSConfig())
                .addDataSource(searchListDS(CUSTOM_EDU_PLAN_DS, customEduPlanDSColumnExtPoint(), customEduPlanDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint customEduPlanDSColumnExtPoint()
    {
        return columnListExtPointBuilder(CUSTOM_EDU_PLAN_DS)
                .addColumn(publisherColumn("title", EppCustomEduPlan.title()).order())
                .addColumn(publisherColumn("version", EppCustomEduPlan.epvBlock().eduPlanVersion().title()).primaryKeyPath(EppCustomEduPlan.epvBlock().eduPlanVersion().id()))
                .addColumn(publisherColumn("block", EppCustomEduPlan.epvBlock().title()).primaryKeyPath(EppCustomEduPlan.epvBlock().id()))
                .addColumn(textColumn("generation", EppCustomEduPlan.plan().programSubject().subjectIndex().generation().title()).order())
                .addColumn(textColumn("programSubject", EppCustomEduPlan.plan().programSubject().titleWithCode()).order())
                .addColumn(textColumn("qualification", EppCustomEduPlan.plan().programQualification().title()).order())
                .addColumn(textColumn("developCombination", EppCustomEduPlan.developCombinationTitle()))
                .addColumn(textColumn("state", EppCustomEduPlan.state().title()).order())
                .addColumn(publisherColumn("students", "title")
                                   .entityListProperty(CustomEduPlanDSHandler.VIEW_PROP_STUDENT)
                                   .parameters("mvel:['selectedStudentTab':'studentTab', 'selectedDataTab':'studentEppEduplanTab']")
                                   .formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("edit_eppCustomEduPlan_list")
                                   .disabled(CustomEduPlanDSHandler.VIEW_PROP_READ_ONLY))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("delete_eppCustomEduPlan_list")
                                   .alert(alert(CUSTOM_EDU_PLAN_DS + DELETE_ALERT_TAIL, EppEduPlanProf.P_TITLE))
                                   .disabled(CustomEduPlanDSHandler.VIEW_PROP_READ_ONLY))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler subjectIndexDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
                .where(EduProgramSubjectIndex.programKind(), CustomEduPlanDSHandler.PARAM_PROGRAM_KIND, false)
                .filter(EduProgramSubjectIndex.title())
                .order(EduProgramSubjectIndex.code())
                .pageable(false);
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final EduProgramKind programKind = context.get(PARAM_PROGRAM_KIND);
                final DQLSelectBuilder customPlanDQL = new DQLSelectBuilder()
                        .fromEntity(EppCustomEduPlan.class, "p")
                        .where(eq(property("p", EppCustomEduPlan.plan().programKind()), value(programKind)))
                        .where(eq(property("p", EppCustomEduPlan.plan().programSubject()), property(alias)));

                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programSubject().subjectIndex(), context.get(PARAM_SUBJECT_INDEX));
                applySelectFilter(dql, alias, EduProgramSubject.subjectIndex().programKind(), programKind);

                dql.where(exists(customPlanDQL.buildQuery()));
            }
        }
        .filter(EduProgramSubject.code())
        .filter(EduProgramSubject.title())
        .order(EduProgramSubject.code())
        .order(EduProgramKind.title())
        .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler programFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final DQLSelectBuilder customPlanDQL = new DQLSelectBuilder()
                        .fromEntity(EppCustomEduPlan.class, "p")
                        .where(eq(property("p", EppCustomEduPlan.plan().programKind()), value(context.<IEntity>get(PARAM_PROGRAM_KIND))))
                        .where(eq(property("p", EppCustomEduPlan.plan().programForm()), property(alias)));

                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programSubject().subjectIndex(), context.get(PARAM_SUBJECT_INDEX));
                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programSubject(), context.get(PARAM_PROGRAM_SUBJECT));

                dql.where(exists(customPlanDQL.buildQuery()));
            }
        }
        .filter(EduProgramForm.title())
        .order(EduProgramForm.title())
        .pageable(false);
    }

    @Bean
    public IDefaultComboDataSourceHandler developConditionDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopCondition.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final DQLSelectBuilder customPlanDQL = new DQLSelectBuilder()
                        .fromEntity(EppCustomEduPlan.class, "p")
                        .where(eq(property("p", EppCustomEduPlan.plan().programKind()), value(context.<IEntity>get(PARAM_PROGRAM_KIND))))
                        .where(eq(property("p", EppCustomEduPlan.developCondition()), property(alias)));

                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programSubject().subjectIndex(), context.get(PARAM_SUBJECT_INDEX));
                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programSubject(), context.get(PARAM_PROGRAM_SUBJECT));
                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programForm(), context.get(PARAM_PROGRAM_FORM));

                dql.where(exists(customPlanDQL.buildQuery()));
            }
        }
        .filter(DevelopCondition.title())
        .order(DevelopCondition.code())
        .pageable(false);
    }

    @Bean
    public IDefaultComboDataSourceHandler programTraitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramTrait.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final DQLSelectBuilder customPlanDQL = new DQLSelectBuilder()
                        .fromEntity(EppCustomEduPlan.class, "p")
                        .where(eq(property("p", EppCustomEduPlan.plan().programKind()), value(context.<IEntity>get(PARAM_PROGRAM_KIND))))
                        .where(eq(property("p", EppCustomEduPlan.plan().programTrait()), property(alias)));

                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programSubject().subjectIndex(), context.get(PARAM_SUBJECT_INDEX));
                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programSubject(), context.get(PARAM_PROGRAM_SUBJECT));
                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programForm(), context.get(PARAM_PROGRAM_FORM));
                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.developCondition(), context.get(PARAM_DEVELOP_CONDITION));

                dql.where(exists(customPlanDQL.buildQuery()));
            }
        }
        .filter(EduProgramTrait.title())
        .order(EduProgramTrait.title())
        .pageable(false);
    }

    @Bean
    public IDefaultComboDataSourceHandler developGridDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopGrid.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final DQLSelectBuilder customPlanDQL = new DQLSelectBuilder()
                        .fromEntity(EppCustomEduPlan.class, "p")
                        .where(eq(property("p", EppCustomEduPlan.plan().programKind()), value(context.<IEntity>get(PARAM_PROGRAM_KIND))))
                        .where(eq(property("p", EppCustomEduPlan.developGrid()), property(alias)));

                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programSubject().subjectIndex(), context.get(PARAM_SUBJECT_INDEX));
                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programSubject(), context.get(PARAM_PROGRAM_SUBJECT));
                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programForm(), context.get(PARAM_PROGRAM_FORM));
                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.developCondition(), context.get(PARAM_DEVELOP_CONDITION));
                applySelectFilter(customPlanDQL, "p", EppCustomEduPlan.plan().programTrait(), context.get(PARAM_PROGRAM_TRAIT));

                dql.where(exists(customPlanDQL.buildQuery()));
            }
        }
        .filter(DevelopGrid.title())
        .order(DevelopGrid.developPeriod().priority())
        .order(DevelopGrid.title())
        .pageable(false);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduPlanVersionDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppEduPlanVersion.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                dql.joinEntity(alias, DQLJoinType.inner, EppEduPlanProf.class, "plan", eq(property(alias, EppEduPlanVersion.eduPlan()), property("plan")));

                applySelectFilter(dql, "plan", EppEduPlanProf.programKind(), context.get(PARAM_PROGRAM_KIND));
                applySelectFilter(dql, "plan", EppEduPlanProf.programSubject().subjectIndex(), context.get(PARAM_SUBJECT_INDEX));
                applySelectFilter(dql, "plan", EppEduPlanProf.programSubject(), context.get(PARAM_PROGRAM_SUBJECT));
                applySelectFilter(dql, "plan", EppEduPlanProf.programForm(), context.get(PARAM_PROGRAM_FORM));
                applySelectFilter(dql, "plan", EppEduPlanProf.developCondition(), context.get(PARAM_DEVELOP_CONDITION));
                applySelectFilter(dql, "plan", EppEduPlanProf.developGrid(), context.get(PARAM_DEVELOP_GRID));
            }

            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                final DQLSelectBuilder dql = super.query(alias, filter);
                if (StringUtils.isNotBlank(filter)) {
                    final IDQLExpression fullNumberExpr = DQLFunctions.concat(
                            property(alias, EppEduPlanVersion.number()), value("."),
                            property(alias, EppEduPlanVersion.eduPlan().number())
                    );
                    dql.where(likeUpper(fullNumberExpr, value(CoreStringUtils.escapeLike(filter))));
                }
                return dql;
            }
        }
        .order(EppEduPlanVersion.eduPlan().number())
        .order(EppEduPlanVersion.number())
        .pageable(true);
    }

    @Bean
    public IDefaultSearchDataSourceHandler customEduPlanDSHandler()
    {
        return new CustomEduPlanDSHandler(getName());
    }
}