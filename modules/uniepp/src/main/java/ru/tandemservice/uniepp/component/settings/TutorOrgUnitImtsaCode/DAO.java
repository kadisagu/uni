package ru.tandemservice.uniepp.component.settings.TutorOrgUnitImtsaCode;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final List<EppTutorOrgUnit> list = new DQLSelectBuilder()
        .fromEntity(EppTutorOrgUnit.class, "e").column(property("e"))
        .where(eq(property(EppTutorOrgUnit.orgUnit().archival().fromAlias("e")), value(Boolean.FALSE)))
        .order(property(EppTutorOrgUnit.orgUnit().title().fromAlias("e")))
        .createStatement(getSession()).list();

        model.getTutorOrgUnitListHolder().setup(list);
    }

    @Override
    public void validate(final Model model, ErrorCollector errors)
    {
        final Collection<EppTutorOrgUnit> rows = model.getTutorOrgUnitListHolder().getRows();

        // проверяем уникальность сохраняемых кодов
        final Map<String, List<Long>> codeImtsaMap = SafeMap.get(ArrayList.class);
        for (EppTutorOrgUnit ou: rows) {
            String codeImtsa = StringUtils.trimToNull(ou.getCodeImtsa());
            if (null != codeImtsa) {
                codeImtsaMap.get(codeImtsa).add(ou.getId());
            }
        }

        for (Map.Entry<String, List<Long>> e: codeImtsaMap.entrySet()) {
            if (e.getValue().size() > 1) {
                errors.add(
                    "Поле «Код подразделения в ИМЦА» должно быть уникальным среди читающих подразделений. Код подразделения ИМЦА «"+e.getKey()+"» встречается более одного раза.",
                    CollectionUtils.collect(e.getValue(), model::getCodeImtsaFieldId).toArray(new String[0])
                );
            }
        }
    }

    @Override
    public void update(final Model model)
    {
        final Collection<EppTutorOrgUnit> rows = model.getTutorOrgUnitListHolder().getRows();
        for (EppTutorOrgUnit ou: rows) {
            saveOrUpdate(ou);
        }
    }

}
