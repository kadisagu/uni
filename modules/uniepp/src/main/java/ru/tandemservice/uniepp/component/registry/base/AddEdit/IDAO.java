package ru.tandemservice.uniepp.component.registry.base.AddEdit;

import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author vdanilov
 */
public interface IDAO<T extends EppRegistryElement> extends IPrepareable<Model<T>> {
    void save(Model<T> model);
}
