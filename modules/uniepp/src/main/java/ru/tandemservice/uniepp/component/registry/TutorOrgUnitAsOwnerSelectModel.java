package ru.tandemservice.uniepp.component.registry;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class TutorOrgUnitAsOwnerSelectModel extends DQLFullCheckSelectModel
{

    protected OrgUnit getCurrentValue()
    {
        return null;
    }

    public TutorOrgUnitAsOwnerSelectModel() {
        super(OrgUnit.P_FULL_TITLE);
    }

    @Override
    protected DQLSelectBuilder query(final String alias, final String filter) {
        final DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(OrgUnit.class, alias);
        b.order(property(OrgUnit.title().fromAlias(alias)));

        FilterUtils.applyLikeFilter(b, filter,
            OrgUnit.title().fromAlias(alias),
            OrgUnit.fullTitle().fromAlias(alias),
            OrgUnit.shortTitle().fromAlias(alias)
            );

        final OrgUnit owner = this.getCurrentValue();
        b.where(or(
                exists(EppTutorOrgUnit.class, EppTutorOrgUnit.orgUnit().s(), property(alias)),
                owner != null ? eq(property(alias), value(owner)) : null
        ));

        return b;
    }
}
