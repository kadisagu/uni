package ru.tandemservice.uniepp.component.settings.TypesControlActionsInterfacePriority;

import org.hibernate.Session;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;

import java.util.List;

/**
 * 
 * @author nkokorina
 * @since 16.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(final Model model)
    {
        final MQBuilder builder = new MQBuilder(EppFControlActionType.ENTITY_CLASS, "eyep");
        builder.addOrder("eyep", EppFControlActionType.P_PRIORITY);
        UniBaseUtils.createPage(model.getDataSource(), builder, this.getSession());

        for (final ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource())) {
            wrapper.setViewProperty("finalAction", wrapper.getEntity() instanceof EppFControlActionType);
        }
    }
 @Override
    public void updatePriorityUp(final Long markValueId)
    {
        this.updatePriority(this.get(markValueId), -1);
    }

    @Override
    public void updatePriorityVeryUp(final Long markValueId)
    {
        final Session session = this.getSession();
        final EppFControlActionType markValue = this.get(markValueId);
        final MQBuilder builder = new MQBuilder(EppFControlActionType.ENTITY_CLASS, "v");
        builder.addOrder("v", EppFControlActionType.P_PRIORITY);
        final List<EppFControlActionType> result = builder.getResultList(this.getSession());
        markValue.setPriority(result.get(0).getPriority()-1);
        session.update(markValue);
        session.flush();
    }

    @Override
    public void updatePriorityDown(final Long markValueId)
    {
        this.updatePriority(this.get(markValueId), 1);
    }

    @Override
    public void updatePriorityVeryDown(final Long markValueId)
    {
        final Session session = this.getSession();
        final EppFControlActionType markValue = this.get(markValueId);
        final MQBuilder builder = new MQBuilder(EppFControlActionType.ENTITY_CLASS, "v");
        builder.addOrder("v", EppFControlActionType.P_PRIORITY);
        final List<EppFControlActionType> result = builder.getResultList(this.getSession());
        markValue.setPriority(result.get(result.size()-1).getPriority()+1);
        session.update(markValue);
        session.flush();
    }

    private void updatePriority(final EppFControlActionType markValue, final int direction)
    {
        final Session session = this.getSession();
        final MQBuilder builder = new MQBuilder(EppFControlActionType.ENTITY_CLASS, "v");
        builder.addOrder("v", EppFControlActionType.P_PRIORITY);
        final List<EppFControlActionType> result = builder.getResultList(this.getSession());
        final int near = result.indexOf(markValue)+direction;
        if (near>=0 && near<result.size())
        {
            final int priorNear = result.get(near).getPriority(), priorCur = markValue.getPriority();
            result.get(near).setPriority(result.get(0).getPriority()-1);
            session.update(result.get(near));
            session.flush();
            markValue.setPriority(priorNear);
            session.update(markValue);
            session.flush();
            result.get(near).setPriority(priorCur);
            session.update(result.get(near));
            session.flush();
        }
    }

    @Override
    public IEntityHandler getUpDisabledEntityHandler()
    {
        return (entity -> {
            final MQBuilder builder = new MQBuilder(EppFControlActionType.ENTITY_CLASS, "v");
            builder.addOrder("v", EppFControlActionType.P_PRIORITY);
            final List<EppFControlActionType> result = builder.getResultList(DAO.this.getSession());
            return ((EppFControlActionType)((ViewWrapper) entity).getEntity()).getPriority()==result.get(0).getPriority();
        });
    }

    @Override
    public IEntityHandler getDownDisabledEntityHandler()
    {
        return (entity -> {
            final MQBuilder builder = new MQBuilder(EppFControlActionType.ENTITY_CLASS, "v");
            builder.addOrder("v", EppFControlActionType.P_PRIORITY);
            final List<EppFControlActionType> result = builder.getResultList(DAO.this.getSession());
            return ((EppFControlActionType)((ViewWrapper) entity).getEntity()).getPriority()==result.get(result.size()-1).getPriority();
        });
    }
}
