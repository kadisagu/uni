package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBySexElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент деления потоков (пол)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduGroupSplitBySexElementGen extends EppEduGroupSplitBaseElement
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBySexElement";
    public static final String ENTITY_NAME = "eppEduGroupSplitBySexElement";
    public static final int VERSION_HASH = 1453702856;
    private static IEntityMeta ENTITY_META;

    public static final String L_SEX = "sex";

    private Sex _sex;     // Пол

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Пол. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Sex getSex()
    {
        return _sex;
    }

    /**
     * @param sex Пол. Свойство не может быть null и должно быть уникальным.
     */
    public void setSex(Sex sex)
    {
        dirty(_sex, sex);
        _sex = sex;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEduGroupSplitBySexElementGen)
        {
            setSex(((EppEduGroupSplitBySexElement)another).getSex());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduGroupSplitBySexElementGen> extends EppEduGroupSplitBaseElement.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduGroupSplitBySexElement.class;
        }

        public T newInstance()
        {
            return (T) new EppEduGroupSplitBySexElement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "sex":
                    return obj.getSex();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "sex":
                    obj.setSex((Sex) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "sex":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "sex":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "sex":
                    return Sex.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduGroupSplitBySexElement> _dslPath = new Path<EppEduGroupSplitBySexElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduGroupSplitBySexElement");
    }
            

    /**
     * @return Пол. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBySexElement#getSex()
     */
    public static Sex.Path<Sex> sex()
    {
        return _dslPath.sex();
    }

    public static class Path<E extends EppEduGroupSplitBySexElement> extends EppEduGroupSplitBaseElement.Path<E>
    {
        private Sex.Path<Sex> _sex;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Пол. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBySexElement#getSex()
     */
        public Sex.Path<Sex> sex()
        {
            if(_sex == null )
                _sex = new Sex.Path<Sex>(L_SEX, this);
            return _sex;
        }

        public Class getEntityClass()
        {
            return EppEduGroupSplitBySexElement.class;
        }

        public String getEntityName()
        {
            return "eppEduGroupSplitBySexElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
