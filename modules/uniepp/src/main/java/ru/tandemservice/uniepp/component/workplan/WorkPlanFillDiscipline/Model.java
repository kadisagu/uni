/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.workplan.WorkPlanFillDiscipline;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;

/**
 * @author nkokorina
 * Created on: 06.08.2010
 */

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id") })
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    public static final String RETURN_SUCCESS_PARAM = "workPlanFilledRowsSize";

    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    private final StaticListDataSource<IEppEpvRowWrapper> rowDataSource = new StaticListDataSource<IEppEpvRowWrapper>();
    public StaticListDataSource<IEppEpvRowWrapper> getRowDataSource() { return this.rowDataSource; }

}
