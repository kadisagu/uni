package ru.tandemservice.uniepp.base.bo.Student.ui.LifecycleTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.Student.StudentManager;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 05.12.2016
 */
@Configuration
public class StudentLifecycleTab extends BusinessComponentManager
{
	public static final String ACTIVE_WPE_DS = "activeWpeDS";
	public static final String YEAR_PART_DS = "yearPartDS";

	public static final String FILTER_STUDENT = "student";
	public static final String FILTER_ACTIVE_WPE = "activeWpeFilter";

	@Bean
	@Override
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
				.addDataSource(selectDS(ACTIVE_WPE_DS, removalStatusDSHandler()))
				.addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEAR_PART_DS, getName(), yearPartDSHandler()))
				.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler removalStatusDSHandler()
	{
		return new TwinComboDataSourceHandler(getName())
				.yesTitle("Только активные")
				.noTitle("Только неактивные");
	}

	/** Части учебного года, для которых у данного студента есть МСРП (возможно, с фильтрацией по активности МСРП) */
	public EntityComboDataSourceHandler yearPartDSHandler()
	{
		return EppYearPart.defaultSelectDSHandler(getName())
				.customize((alias, dql, context, filter) -> {
					Student student = context.getNotNull(FILTER_STUDENT);
					Boolean activeWpeFilter = context.get(FILTER_ACTIVE_WPE);

					final String studentWpeAlias = "studentWpe";
					DQLSelectBuilder studentWpeDql = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, studentWpeAlias)
							.where(eq(property(studentWpeAlias, EppStudentWorkPlanElement.student()), value(student)))
							.where(eq(property(studentWpeAlias, EppStudentWorkPlanElement.year()), property(alias, EppYearPart.year())))
							.where(eq(property(studentWpeAlias, EppStudentWorkPlanElement.part()), property(alias, EppYearPart.part())));
					StudentManager.instance().dao().applyActiveWpeFilter(studentWpeDql, studentWpeAlias, activeWpeFilter);
					return dql.where(exists(studentWpeDql.buildQuery()));
				});
	}
}