package ru.tandemservice.uniepp.base.bo.EppContract.ui.StudentTab;

import org.hibernate.Session;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.DelegatePropertyComparator;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplate;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.PubCtr.CtrContractVersionPubCtr;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.PubVer.CtrContractVersionPubVer;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="studentHolder.id", required=true)
})
public class EppContractStudentTabUI extends UIPresenter {

    private static final boolean showVersionColumn = false;

    private static final String VIEW_PROPERTY_VERSION_LIST = "versionList";

    private final EntityHolder<Student> studentHolder = new EntityHolder<Student>();
    public EntityHolder<Student> getStudentHolder() { return this.studentHolder; }
    public Student getStudent() { return this.getStudentHolder().getValue(); }

    @Override public ISecured getSecuredObject() { return this.getStudent(); }

    private final StaticListDataSource<ViewWrapper<EppCtrEducationResult>> dataSource = new StaticListDataSource<ViewWrapper<EppCtrEducationResult>>();
    public StaticListDataSource<ViewWrapper<EppCtrEducationResult>> getDataSource() { return this.dataSource; }

    @Override
    public void onComponentActivate() {
        StaticListDataSource<ViewWrapper<EppCtrEducationResult>> ds = this.dataSource;

        ds.addColumn(
            new PublisherLinkColumn("Договор", EppCtrEducationResult.contract().number()) {
                @Override public List<IEntity> getEntityList(final IEntity entity) {
                    return Collections.singletonList(entity);
                }
            }
            .setResolver(
                new SimplePublisherLinkResolver.Safe("id")
                .setComponent(CtrContractVersionPubCtr.class)
            )
            .setOrderable(false)
            .setWidth(1)
        );

        if (showVersionColumn) {
            ds.addColumn(
                new PublisherLinkColumn("Версии", "") {
                    @SuppressWarnings("unchecked")
                    @Override public List<IEntity> getEntityList(final IEntity entity) {
                        return (List<IEntity>) ((ViewWrapper<EppCtrEducationResult>)entity).getViewProperty(VIEW_PROPERTY_VERSION_LIST);
                    }
                    @Override public String getContent(IEntity entity) {
                        CtrContractVersion version = (CtrContractVersion)entity;
                        return version.getNumber() + (null == version.getActivationDate() ? "" : (" от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(version.getActivationDate())));
                    }
                }
                .setResolver(
                    new SimplePublisherLinkResolver.Safe("id")
                    .setComponent(CtrContractVersionPubVer.class)
                )
                .setOrderable(false)
                .setWidth(1)
            );
        }

        ds.addColumn(
            new SimpleColumn("Тип", EppCtrEducationResult.contract().type().title())
            .setClickable(false)
            .setOrderable(false)
            .setWidth(1)
        );
    }

    @Override
    public void onComponentRefresh()
    {
        // обновляем студента
        final Student student = this.getStudentHolder().refresh(Student.class);
        Session session = this._uiSupport.getSession();

        // список зачислений по договору (всех)
        final List<EppCtrEducationResult> list = new DQLSelectBuilder()
        .fromEntity(EppCtrEducationResult.class, "r").column(property("r"))
        .fetchPath(DQLJoinType.inner, EppCtrEducationResult.target().fromAlias("r"), "s2epv")
        .where(eq(property(EppStudent2EduPlanVersion.student().id().fromAlias("s2epv")), value(student.getId())))
        .createStatement(session).<EppCtrEducationResult>list();

        // сортируем по номеру (договора)
        Collections.sort(
            list,
            new DelegatePropertyComparator(
                NumberAsStringComparator.INSTANCE,
                EppCtrEducationResult.contract().number().s()
            )
        );

        // теперь нужно сделать так, чтобы договор встречался только один раз (предпочтение отдаем связи с активный ПУ(в), затем - по id)
        final Map<Long, EppCtrEducationResult> contractId2firstResultMap = new LinkedHashMap<Long, EppCtrEducationResult>();
        for (EppCtrEducationResult current: list) {
            Long cid = current.getContract().getId();
            EppCtrEducationResult prev = contractId2firstResultMap.get(cid);
            if (null == prev) {
                // если еще ничего не нашли - сохраняем
                contractId2firstResultMap.put(cid, current);
                continue;
            }

            Date prevRemovalDate = prev.getTarget().getRemovalDate();
            if (null != prevRemovalDate) {
                // если созраняенный жлемент неактуален - то пробуем его заменить на более актуальный вариант

                final Date currentRemovalDate = current.getTarget().getRemovalDate();
                if (null == currentRemovalDate) {
                    // если текущий элемент активен - сохраняем (переписываем)
                    contractId2firstResultMap.put(cid, current);
                    continue;
                }

                if (currentRemovalDate.after(prevRemovalDate)) {
                    // если текущий элемент утратил актуальность после сохраненного - сохраняем (переписываем)
                    contractId2firstResultMap.put(cid, current);
                    continue;
                }
            }
        }

        // оболочки
        List<ViewWrapper<EppCtrEducationResult>> wrapperList = ViewWrapper.getPatchedList(contractId2firstResultMap.values());

        // прицепляем версии (здесь мы знаем, что договор всегда один)
        final List<CtrContractVersion> versionList = new DQLSelectBuilder()
        .fromEntity(CtrContractVersion.class, "v").column(property("v"))
        .where(in(property(CtrContractVersion.contract().id().fromAlias("v")), contractId2firstResultMap.keySet()))
        .order(
            DQLFunctions.coalesce(
                property(CtrContractVersion.activationDate().fromAlias("v")), // сначала по дате активации
                property(CtrContractVersion.creationDate().fromAlias("v")) // затем, если первая null - по дате создания
            ),
            OrderDirection.asc
        ).createStatement(session).list();

        Map<Long, List<CtrContractVersion>> contractId2versionListMap = SafeMap.get(ArrayList.class);
        for (CtrContractVersion v: versionList) { contractId2versionListMap.get(v.getContract().getId()).add(v); }

        for (ViewWrapper<EppCtrEducationResult> w: wrapperList) {
            w.setViewProperty(VIEW_PROPERTY_VERSION_LIST, new ArrayList<CtrContractVersion>(contractId2versionListMap.get(w.getEntity().getContract().getId())) /* пересоздаем список - чтобы минимизировать число элементов в нем */);
        }

        // создаем список
        this.dataSource.setupRows(wrapperList);

    }

    public void onClickAddStudentContract() {
        this._uiActivation.asRegionDialog(CtrContractVersionAddByTemplate.class).parameter(IUIPresenter.PUBLISHER_ID, getStudent().getId()).activate();
    }

}
