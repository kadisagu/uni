/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.StudentList;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.logic.studentList.EppWorkPlanStudentSearchListDSHandler;

/**
 * @author Alexander Shaburov
 * @since 01.02.13
 */
@Configuration
public class EppWorkPlanStudentList extends AbstractUniStudentList
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(
            presenterExtPointBuilder()
            .addDataSource(searchListDS(STUDENT_SEARCH_LIST_DS, studentSearchListDSColumnExtPoint(), studentSearchListDSHandler())));

    }

    @Bean
    public ColumnListExtPoint studentSearchListDSColumnExtPoint()
    {
        return createStudentSearchListColumnsBuilder()
        .create();
    }

    @Override
    protected List<ColumnBase> customizeStudentSearchListColumns(List<ColumnBase> columnList)
    {
        removeColumns(
            columnList,
            CITIZENSHIP_COLUMN,
            PERSONAL_NUMBER_COLUMN,
            PERSONAL_FILE_NUMBER_COLUMN
        );
        return columnList;
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchListDSHandler()
    {
        return new EppWorkPlanStudentSearchListDSHandler(getName());
    }
}
