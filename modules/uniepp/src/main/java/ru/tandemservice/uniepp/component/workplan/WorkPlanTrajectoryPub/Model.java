/* $Id$ */
package ru.tandemservice.uniepp.component.workplan.WorkPlanTrajectoryPub;

import java.util.Collection;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.source.SimpleListDataSource;

import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author oleyba
 * @since 5/6/11
 */
@Input({
    @Bind(key="ids", binding="ids", required=true),
    @Bind(key="showAsPub", binding="showAsPub"),
    @Bind(key="showWorkplans", binding="showWorkplans")
})
public class Model
{
    public static final String COMPONENT = Model.class.getPackage().getName();

    private Collection<Long> ids;
    public Collection<Long> getIds() { return this.ids; }
    public void setIds(final Collection<Long> ids) { this.ids = ids; }

    private EppEduPlanVersion eduPlanVersion;
    public EppEduPlanVersion getEduPlanVersion() { return this.eduPlanVersion; }
    public void setEduPlanVersion(final EppEduPlanVersion eduPlanVersion) { this.eduPlanVersion = eduPlanVersion; }

    private List<IdentifiableWrapper> workplanWrapperList;
    public List<IdentifiableWrapper> getWorkplanWrapperList() { return this.workplanWrapperList; }
    public void setWorkplanWrapperList(final List<IdentifiableWrapper> workplanWrapperList) { this.workplanWrapperList = workplanWrapperList; }

    private IdentifiableWrapper workplanWrapper;
    public IdentifiableWrapper getWorkplanWrapper() { return this.workplanWrapper; }
    public void setWorkplanWrapper(final IdentifiableWrapper workplanWrapper) { this.workplanWrapper = workplanWrapper; }
    public Boolean getCurrentWrapperEmpty() { return EntityRuntime.getMeta(this.getWorkplanWrapper().getId()).getEntityClass().equals(Term.class); }

    private boolean showAsPub;
    public boolean isShowAsPub() { return this.showAsPub; }
    public void setShowAsPub(final boolean showAsPub) { this.showAsPub = showAsPub; }

    private boolean showWorkplans;
    public boolean isShowWorkplans() { return this.showWorkplans; }
    public void setShowWorkplans(final boolean showWorkplans) { this.showWorkplans = showWorkplans; }

    private String selectedTabId;
    public String getSelectedTabId() { return this.selectedTabId; }
    public void setSelectedTabId(final String selectedTabId) { this.selectedTabId = selectedTabId; }

    private SimpleListDataSource<Student> studentDataSource;
    public SimpleListDataSource<Student> getStudentDataSource() { return this.studentDataSource; }
    public void setStudentDataSource(final SimpleListDataSource<Student> studentDataSource) { this.studentDataSource = studentDataSource; }

    private boolean empty = false;
    public boolean isEmpty() { return this.empty; }
    public void setEmpty(final boolean empty) { this.empty = empty; }
}