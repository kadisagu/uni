package ru.tandemservice.uniepp.component.registry.ModuleRegistry;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {
    void prepareDataSource(Model model);
    void prepareLoadColumns(Model model, DynamicListDataSource dataSource);
}
