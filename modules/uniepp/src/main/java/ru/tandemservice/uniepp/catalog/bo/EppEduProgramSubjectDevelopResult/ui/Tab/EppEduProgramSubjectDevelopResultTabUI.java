/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppEduProgramSubjectDevelopResult.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.catalog.EppProfessionalTask;
import ru.tandemservice.uniepp.entity.catalog.EppSkill;

/**
 * @author Igor Belanov
 * @since 03.03.2017
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "programSubjectId", required = true),
})
public class EppEduProgramSubjectDevelopResultTabUI extends UIPresenter
{
    private Long _programSubjectId;
    private EduProgramSubject _programSubject;

    private String _selectedSubPage;

    @Override
    public void onComponentRefresh()
    {
        if (_programSubjectId != null && _programSubject == null)
        {
            setProgramSubject(IUniBaseDao.instance.get().getNotNull(EduProgramSubject.class, _programSubjectId));
        }
    }

    public ParametersMap getProfActivityTypeTabParams()
    {
        return ParametersMap.createWith(DefaultCatalogPubModel.CATALOG_CODE, EppProfActivityType.ENTITY_NAME);
    }

    public ParametersMap getProfessionalTaskTabParams()
    {
        return ParametersMap.createWith(DefaultCatalogPubModel.CATALOG_CODE, EppProfessionalTask.ENTITY_NAME);
    }

    public ParametersMap getSkillTabParams()
    {
        return ParametersMap.createWith(DefaultCatalogPubModel.CATALOG_CODE, EppSkill.ENTITY_NAME);
    }

    public String getRegionName()
    {
        return EppEduProgramSubjectDevelopResultTab.TAB_PANEL_REGION_NAME;
    }

    public Long getProgramSubjectId()
    {
        return _programSubjectId;
    }

    public void setProgramSubjectId(Long programSubjectId)
    {
        _programSubjectId = programSubjectId;
    }

    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    public void setProgramSubject(EduProgramSubject programSubject)
    {
        _programSubject = programSubject;
    }

    public String getSelectedSubPage()
    {
        return _selectedSubPage;
    }

    public void setSelectedSubPage(String selectedSubPage)
    {
        _selectedSubPage = selectedSubPage;
    }
}
