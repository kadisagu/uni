package ru.tandemservice.uniepp.entity.plan;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import ru.tandemservice.uniepp.base.bo.EppContract.EppContractManager;
import ru.tandemservice.uniepp.base.bo.EppState.EppStatePath;
import ru.tandemservice.uniepp.dao.index.IEppIndexRule;
import ru.tandemservice.uniepp.dao.index.IEppIndexRuleDAO;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionBlockGen;

import java.util.Comparator;

/**
 * Блок версии учебного плана
 */
@EppStatePath(EppEduPlanVersionBlock.L_EDU_PLAN_VERSION)
public abstract class EppEduPlanVersionBlock extends EppEduPlanVersionBlockGen implements IHierarchyItem, ITitled, IEppStateObject, Comparable<EppEduPlanVersionBlock>
{
    public static final String PROP_EDU_PLAN_VERSION = "eduPlanVersion";

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EppEduPlanVersionBlock.class)
                .titleProperty(EppEduPlanVersionBlock.title().s())
                .where(EppEduPlanVersionBlock.eduPlanVersion(), PROP_EDU_PLAN_VERSION);
    }

    /** @return true, если блок является основным (по направлению подготовки УП) */
    public abstract boolean isRootBlock();

    @Override
    @EntityDSLSupport
    public abstract String getTitle();

    @Override
    @EntityDSLSupport
    public abstract String getEducationElementSimpleTitle();

    @Override
    public abstract String getEducationElementTitle();

    @Override
    @EntityDSLSupport
    public String getFullTitle()
    {
        return "УП(в) " + getEduPlanVersion().getTitle() + ", " + getTitle();
    }

    /**
     * Название "Вариант 1. Название для селектов/списков"
     * @see <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=24216359">Формат вывода блока версии УП</a>
     */
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=24216359")
    @Override
    @EntityDSLSupport
    public String getFullTitleExtended()
    {
        return "№" + getEduPlanVersion().getFullNumber() + " " + getEduPlanVersion().getEducationElementSimpleTitle() + " | " + getSpecializationExtendedTitle();
    }

    @Override
    @EntityDSLSupport
    public String getTitleWithFullNumber()
    {
        return "№" + getEduPlanVersion().getFullNumber() + " " + getTitle();
    }

    @Override
    @EntityDSLSupport
    public String getTitleForListWithVersionAndSpecialization()
    {
        String specTitle;
        if (this instanceof EppEduPlanVersionSpecializationBlock)
            specTitle = ((EppEduPlanVersionSpecializationBlock) this).getProgramSpecialization().getDisplayableTitle();
        else specTitle = this.getTitle();
        final EppEduPlan eduPlan = getEduPlanVersion().getEduPlan();
        String profTitle = "";
        if (eduPlan instanceof EppEduPlanProf)
            profTitle = ((EppEduPlanProf) eduPlan).getProgramSubject().getTitleWithCode();
        else if (eduPlan instanceof EppEduPlanAdditionalProf)
            profTitle = ((EppEduPlanAdditionalProf) eduPlan).getEduProgram().getTitle();
        return "№" + eduPlan.getNumber() + "." + getEduPlanVersion().getNumber() + " " + profTitle + " | " + specTitle;
    }


    /**
     *
     * @return
     */
    @Override
    @EntityDSLSupport
    public String getVersionFullTitleWithBlockTitle()
    {
        String specTitle = this instanceof EppEduPlanVersionSpecializationBlock ? ((EppEduPlanVersionSpecializationBlock) this).getProgramSpecialization().getDisplayableTitle() : "Общий блок";

        return getEduPlanVersion().getFullTitleWithEducationsCharacteristics() + " | " + specTitle;
    }

    /**
     * Используется как часть названия "Вариант 1. Название для селектов/списков"
     * @see <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=24216359">Формат вывода блока версии УП</a>
     */
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=24216359")
    public abstract String getSpecializationExtendedTitle();

    public static final Comparator<EppEduPlanVersionBlock> COMPARATOR = (o1, o2) -> {
        if (null == o1 && null == o2) return 0;
        return (null == o2) ? o1.compareTo(null) : -o2.compareTo(o1);
    };

    @Override
    public int compareTo(EppEduPlanVersionBlock o)
    {
        if (o == null) { return 1; }

        int rootCmp = Boolean.compare(isRootBlock(), o.isRootBlock());
        if (0 != rootCmp) { return -rootCmp; }

        if (this instanceof EppEduPlanVersionSpecializationBlock && o instanceof EppEduPlanVersionSpecializationBlock) {
            return ((EppEduPlanVersionSpecializationBlock)this).getProgramSpecialization().compareTo(((EppEduPlanVersionSpecializationBlock)o).getProgramSpecialization());
        }
        return 0;
    }

    public EppEduPlanVersionBlock() {}

    @Override public EppState getState() { return this.getEduPlanVersion().getState(); }
    @Override public EppEduPlanVersion getHierarhyParent() { return this.getEduPlanVersion(); }

    public EppGeneration getGeneration() {
        return this.getEduPlanVersion().getEduPlan().getGeneration();
    }

    private IEppIndexRule _rule_cache = null;

    /** @return правило формирования индекса */
    public IEppIndexRule getRule() {
        if (! (getEduPlanVersion().getEduPlan() instanceof EppEduPlanProf)) return IEppIndexRule.RULE_SIMPLE_CONCAT;

        if (null == this._rule_cache) {
            return (this._rule_cache = IEppIndexRuleDAO.instance.get().getRule(((EppEduPlanProf) getEduPlanVersion().getEduPlan()).getProgramSubject().getSubjectIndex()));
        }
        return this._rule_cache;
    }


    @SuppressWarnings("deprecation")
    @Override
    public CtrContractType getContractType() {
        return EppContractManager.instance().dao().getEppContractType(this.getEduPlanVersion());
    }

    @Override
    public String getPriceElementTitle() {
        return this.getEducationElementTitle();
    }
}