/**
 *$Id: EppWorkPlanStudentListUI.java 26089 2013-02-11 08:34:37Z vdanilov $
 */
package ru.tandemservice.uniepp.base.bo.EppGroupOrgUnit.ui.StudentsInNewGrpsList;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.base.bo.EppEduGroup.ui.Lookup4Students.EppEduGroupLookup4Students;
import ru.tandemservice.uniepp.base.bo.EppEduGroup.ui.Lookup4Students.EppEduGroupLookup4StudentsUI;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@State({
    @Bind(key="orgUnitId", binding="orgUnitHolder.id", required=true)
})
public class EppGroupOrgUnitStudentsInNewGrpsListUI extends AbstractUniStudentListUI
{
    public static final String PARAM_ID = "id";
    public static final String PARAM_GROUP_LIST = "eppGroupList";

    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    public OrgUnitHolder getOrgUnitHolder() { return this.orgUnitHolder; }
    public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }
    public CommonPostfixPermissionModelBase getSec() { return getOrgUnitHolder().getSecModel(); }


    @Override
    public String getSettingsKey()
    {
        return "epp.eppGroupOrgUnit.studentsInNewGrpsList.filter";
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);

        dataSource.put(PARAM_ID, getOrgUnitHolder().getId());

        if (EppGroupOrgUnitStudentsInNewGrpsList.STUDENT_SEARCH_LIST_DS.equals(dataSource.getName())) {
            dataSource.put(PARAM_GROUP_LIST, getSettings().get(PARAM_GROUP_LIST));
        }
    }

    @SuppressWarnings("unchecked")
    public void onClickMoveSelected()
    {
        final ErrorCollector errors = UserContext.getInstance().getErrorCollector();

        final Collection<IEntity> selected = getConfig()
        .<BaseSearchListDataSource>getDataSource(EppGroupOrgUnitStudentsInNewGrpsList.STUDENT_SEARCH_LIST_DS)
        .getOptionColumnSelectedObjects(EppGroupOrgUnitStudentsInNewGrpsList.CHECKBOX_COLUMN);

        if (selected.isEmpty()){
            errors.add(getConfig().getProperty("studentSearchListDS.no-checked-error"));
            return;
        }

        final List<Long> grpRowIds = new ArrayList<>();
        for (IEntity row: selected) {
            grpRowIds.addAll(
                CommonDAO.ids(
                    (Collection<IEntity>)row.getProperty(EppGroupOrgUnitStudentsInNewGrpsList.EDU_GROUP_LIST)
                )
            );
        }

        getActivationBuilder().asRegion(EppEduGroupLookup4Students.class)
        .parameters(new ParametersMap().add(EppEduGroupLookup4StudentsUI.PARAM_ROW_IDS, grpRowIds))
        .activate();

    }


}
