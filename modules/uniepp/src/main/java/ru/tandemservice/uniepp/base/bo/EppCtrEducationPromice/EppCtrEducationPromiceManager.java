package ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrPromiceManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;

import ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.ui.InContractList.EppCtrEducationPromiceInContractList;
import ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.ui.InWizardStep.EppCtrEducationPromiceInWizardStep;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;

/**
 * @author vdanilov
 */
@Configuration
public class EppCtrEducationPromiceManager extends BusinessObjectManager implements ICtrPromiceManager
{
    public static EppCtrEducationPromiceManager instance() {
        return BusinessObjectManager.instance(EppCtrEducationPromiceManager.class);
    }

    @Override public Class<? extends CtrContractPromice> promiceClass() {
        return EppCtrEducationPromice.class;
    }

    @Override public Class<? extends BusinessComponentManager> rootSectionComponent() {
        return EppCtrEducationPromiceInContractList.class;
    }

    @Override public Class<? extends BusinessComponentManager> inWizardPromiceStepComponent() {
        return EppCtrEducationPromiceInWizardStep.class;
    }

}
