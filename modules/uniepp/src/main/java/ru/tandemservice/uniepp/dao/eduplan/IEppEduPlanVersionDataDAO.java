package ru.tandemservice.uniepp.dao.eduplan;

import org.apache.commons.collections15.Predicate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvTotalRow;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppProfessionalTask;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.data.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Операции с таблицей данных версии УП
 *
 * @author vdanilov
 */
public interface IEppEduPlanVersionDataDAO
{
    SpringBeanCache<IEppEduPlanVersionDataDAO> instance = new SpringBeanCache<>(IEppEduPlanVersionDataDAO.class.getName());

    /**
     * @param eppEduPlanVersionId - идентификатор версии УП
     * @return true, если в версии нет записей
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    boolean isEmpty(Long eppEduPlanVersionId);


    /**
     * @param epvBlockIds - идентификаторы блоков версий УП (not-null)
     * @param loadDetails - загружать нагрузку, распределение по семестрам, ....
     * @see EppEduPlanVersionDataDAO#filterEduPlanBlockRows(Map, Predicate, boolean)
     * @return { block.id -> wrapper(block) }
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<Long, IEppEpvBlockWrapper> getEduPlanVersionBlockDataMap(Collection<Long> epvBlockIds, boolean loadDetails);

    /**
     * Получение враппера блока версии УПв
     *
     * @param epvBlockId идентификатор блока версии УП (not-null)
     * @param loadDetails загружать нагрузку, распределение по семестрам, ....
     * @return wrapper(block)
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    IEppEpvBlockWrapper getEduPlanVersionBlockData(Long epvBlockId, boolean loadDetails);

    /**
     * Получение врапперов строк блока версии УПв
     *
     * @param epvBlockId идентификатор блока версии УП (not-null)
     * @param loadDetails загружать нагрузку, распределение по семестрам, ....
     * @return {row.id -> wrapper(row)}
     */
    Map<Long, IEppEpvRowWrapper> getEduPlanVersionBlockRowMap(Long epvBlockId, boolean loadDetails);

    /**
     * @return wrapper(row)
     */
    IEppEpvRowWrapper getEpvRowWrapper(EppEpvRow row, boolean loadDetails);

    /**
     * Выдает нагрузку для строки УП
     *
     * @return { row.id -> { load.fullcode -> load-as-double } }
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<String, Double> getRowLoad(Long rowId);

    interface IRowLoadWrapper {
        EppEpvTermDistributedRow row();
        Map<String, Double> rowLoadMap();
        Map<Integer, Map<String, Double>> rowTermLoadMap();
        Map<Integer, Map<String, Integer>> rowTermActionMap();
    }

    void doUpdateRowLoad(Map<Long, IRowLoadWrapper> rowLoadMap, boolean loadWithSelfworkWoControl);

    IRowLoadWrapper getRowTermLoad(Long rowId);


    /** уловие, что строка является структурной (ссылка на эл. справочника EppPlanStructure) */
    Predicate<IEppEpvRow> PREDICATE_STRUCTURE_ROW = object -> EppEpvStructureRow.class.isAssignableFrom(object.getRowEntityClass());

    /** уловие, что строка является иерархической */
    Predicate<IEppEpvRow> PREDICATE_HIERARCHY_ROW = object -> EppEpvHierarchyRow.class.isAssignableFrom(object.getRowEntityClass());

    /**
     * Возвращает элементы блока версии УП (указанного и основного)
     *
     * @param block - блок, из которого брать элементы
     * @param predicate - условие отбора
     * @return список структурных элкментов блоков версии уп
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    List<HSelectOption> getEpvRowHierarchyListFromBlock(EppEduPlanVersionBlock block, Predicate<IEppEpvRow> predicate);

    /**
     * Возвращает элементы справочника EppPlanStructure, доступные для использования в блоке УП
     *
     * @param block блок
     * @return HSelectOption, где для выбора доступны только листовые элементы
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    List<HSelectOption> getPlanStructureHierarchyList4Add(EppEduPlanVersionBlock block);

    /**
     * Генерирует слеюующий номер для записи
     *
     * @param row строка
     * @param excludeRowIdsIterable - список строк, которые не нужно учитывать при проверки уникальности номера
     * @return следующий номер
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    String getNextNumber(IEppEpvRow row, Iterable<Long> excludeRowIdsIterable);

    /**
     * надохит подмножество элементов <code>rows</code>,
     * такое, что любые два элемента в нем не являются иерархически связанными
     *
     * @return R ⊆ rows | ∀ a,b ∊ R : a ∉ Parents(b)
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Collection<IEppEpvRow> getTopLevelRows(Collection<IEppEpvRow> rows);

    /**
     * @param eppRegistryStructure элемент структуры реестра (null - для всех)
     * @return используемые контрольные мероприятия
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Stream<EppControlActionType> getActiveControlActionTypes(EppRegistryStructure eppRegistryStructure);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    EppEduPlanVersionRootBlock getRootBlock(EppEduPlanVersion version);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    List<Long> getEpvIds4UpdateRowStoredIndex();

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateEpvRowStoredIndex(Long epvId);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Collection<EppEpvTotalRow> getTotalRows(final IEppEpvBlockWrapper versionWrapper, final Collection<IEppEpvRowWrapper> versionRows);

    void doSaveOrUpdateRow(EppEpvRow row, IRowLoadWrapper load);

    /**
     * вытаскивает профессиональные задачи из строк УП
     * @param rowList строки УП
     * @return мапа из строки УП в список проф.задач
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<IEppEpvRow, List<EppProfessionalTask>> getProfessionalTaskMap(List<IEppEpvRow> rowList);

    @Transactional(propagation = Propagation.REQUIRED)
    void doSaveOrUpdateRegRowProfessionalTask(List<EppProfessionalTask> professionalTaskList, EppEpvRegistryRow row);

    /**
     * В каждом семестре для всех строк УП (с нагрузкой), кроме строк с типом элемента, подчиненным "Мероприятия ГИА",
     * выставляет указанное на форме число часов на контроль (экзамен).
     * Затем для всей строки суммирует часы на контроль (экзамен) и записывает в часы на контроль (экзамен).
     *
     * @param eppEpvBlockId id блока УПв
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void specifyControlHoursForAllRows(Long eppEpvBlockId, Double value);

    int getTermSize(Long version, String code, Integer term);
}
