package ru.tandemservice.uniepp.entity.std.data;

import ru.tandemservice.uniepp.dao.index.IEppGroupImIndexedRow;
import ru.tandemservice.uniepp.entity.std.data.gen.EppStdGroupImRowGen;

/**
 * Запись ГОС (дисциплина по выбору)
 */
public class EppStdGroupImRow extends EppStdGroupImRowGen implements IEppGroupImIndexedRow
{
    @Override public boolean isStructureElement() { return false; }

}