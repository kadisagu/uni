package ru.tandemservice.uniepp.entity.workplan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Нагрузка для строки РУП в части (по видам теоретической нагрузки)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkPlanRowPartLoadGen extends EntityBase
 implements INaturalIdentifiable<EppWorkPlanRowPartLoadGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad";
    public static final String ENTITY_NAME = "eppWorkPlanRowPartLoad";
    public static final int VERSION_HASH = 1033312436;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW = "row";
    public static final String L_LOAD_TYPE = "loadType";
    public static final String P_PART = "part";
    public static final String P_LOAD = "load";
    public static final String P_DAYS = "days";
    public static final String P_STUDY_PERIOD_START_DATE = "studyPeriodStartDate";
    public static final String P_STUDY_PERIOD_FINISH_DATE = "studyPeriodFinishDate";
    public static final String P_DAYS_AS_DOUBLE = "daysAsDouble";
    public static final String P_LOAD_AS_DOUBLE = "loadAsDouble";

    private EppWorkPlanRow _row;     // Строка РУП
    private EppLoadType _loadType;     // Базовый класс для вида теоретической нагрузки
    private int _part; 
    private long _load;     // Число часов
    private Long _days;     // Число дней (цикловая нагрузка)
    private Date _studyPeriodStartDate;     // Дата начала учебной деятельности (цикловая нагрузка)
    private Date _studyPeriodFinishDate;     // Дата окончания учебной деятельности (цикловая нагрузка)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка РУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkPlanRow getRow()
    {
        return _row;
    }

    /**
     * @param row Строка РУП. Свойство не может быть null.
     */
    public void setRow(EppWorkPlanRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Базовый класс для вида теоретической нагрузки. Свойство не может быть null.
     */
    @NotNull
    public EppLoadType getLoadType()
    {
        return _loadType;
    }

    /**
     * @param loadType Базовый класс для вида теоретической нагрузки. Свойство не может быть null.
     */
    public void setLoadType(EppLoadType loadType)
    {
        dirty(_loadType, loadType);
        _loadType = loadType;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public int getPart()
    {
        return _part;
    }

    /**
     * @param part  Свойство не может быть null.
     */
    public void setPart(int part)
    {
        dirty(_part, part);
        _part = part;
    }

    /**
     * @return Число часов. Свойство не может быть null.
     */
    @NotNull
    public long getLoad()
    {
        return _load;
    }

    /**
     * @param load Число часов. Свойство не может быть null.
     */
    public void setLoad(long load)
    {
        dirty(_load, load);
        _load = load;
    }

    /**
     * @return Число дней (цикловая нагрузка).
     */
    public Long getDays()
    {
        return _days;
    }

    /**
     * @param days Число дней (цикловая нагрузка).
     */
    public void setDays(Long days)
    {
        dirty(_days, days);
        _days = days;
    }

    /**
     * @return Дата начала учебной деятельности (цикловая нагрузка).
     */
    public Date getStudyPeriodStartDate()
    {
        return _studyPeriodStartDate;
    }

    /**
     * @param studyPeriodStartDate Дата начала учебной деятельности (цикловая нагрузка).
     */
    public void setStudyPeriodStartDate(Date studyPeriodStartDate)
    {
        dirty(_studyPeriodStartDate, studyPeriodStartDate);
        _studyPeriodStartDate = studyPeriodStartDate;
    }

    /**
     * @return Дата окончания учебной деятельности (цикловая нагрузка).
     */
    public Date getStudyPeriodFinishDate()
    {
        return _studyPeriodFinishDate;
    }

    /**
     * @param studyPeriodFinishDate Дата окончания учебной деятельности (цикловая нагрузка).
     */
    public void setStudyPeriodFinishDate(Date studyPeriodFinishDate)
    {
        dirty(_studyPeriodFinishDate, studyPeriodFinishDate);
        _studyPeriodFinishDate = studyPeriodFinishDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppWorkPlanRowPartLoadGen)
        {
            if (withNaturalIdProperties)
            {
                setRow(((EppWorkPlanRowPartLoad)another).getRow());
                setLoadType(((EppWorkPlanRowPartLoad)another).getLoadType());
                setPart(((EppWorkPlanRowPartLoad)another).getPart());
            }
            setLoad(((EppWorkPlanRowPartLoad)another).getLoad());
            setDays(((EppWorkPlanRowPartLoad)another).getDays());
            setStudyPeriodStartDate(((EppWorkPlanRowPartLoad)another).getStudyPeriodStartDate());
            setStudyPeriodFinishDate(((EppWorkPlanRowPartLoad)another).getStudyPeriodFinishDate());
        }
    }

    public INaturalId<EppWorkPlanRowPartLoadGen> getNaturalId()
    {
        return new NaturalId(getRow(), getLoadType(), getPart());
    }

    public static class NaturalId extends NaturalIdBase<EppWorkPlanRowPartLoadGen>
    {
        private static final String PROXY_NAME = "EppWorkPlanRowPartLoadNaturalProxy";

        private Long _row;
        private Long _loadType;
        private int _part;

        public NaturalId()
        {}

        public NaturalId(EppWorkPlanRow row, EppLoadType loadType, int part)
        {
            _row = ((IEntity) row).getId();
            _loadType = ((IEntity) loadType).getId();
            _part = part;
        }

        public Long getRow()
        {
            return _row;
        }

        public void setRow(Long row)
        {
            _row = row;
        }

        public Long getLoadType()
        {
            return _loadType;
        }

        public void setLoadType(Long loadType)
        {
            _loadType = loadType;
        }

        public int getPart()
        {
            return _part;
        }

        public void setPart(int part)
        {
            _part = part;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppWorkPlanRowPartLoadGen.NaturalId) ) return false;

            EppWorkPlanRowPartLoadGen.NaturalId that = (NaturalId) o;

            if( !equals(getRow(), that.getRow()) ) return false;
            if( !equals(getLoadType(), that.getLoadType()) ) return false;
            if( !equals(getPart(), that.getPart()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRow());
            result = hashCode(result, getLoadType());
            result = hashCode(result, getPart());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRow());
            sb.append("/");
            sb.append(getLoadType());
            sb.append("/");
            sb.append(getPart());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkPlanRowPartLoadGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkPlanRowPartLoad.class;
        }

        public T newInstance()
        {
            return (T) new EppWorkPlanRowPartLoad();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "row":
                    return obj.getRow();
                case "loadType":
                    return obj.getLoadType();
                case "part":
                    return obj.getPart();
                case "load":
                    return obj.getLoad();
                case "days":
                    return obj.getDays();
                case "studyPeriodStartDate":
                    return obj.getStudyPeriodStartDate();
                case "studyPeriodFinishDate":
                    return obj.getStudyPeriodFinishDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "row":
                    obj.setRow((EppWorkPlanRow) value);
                    return;
                case "loadType":
                    obj.setLoadType((EppLoadType) value);
                    return;
                case "part":
                    obj.setPart((Integer) value);
                    return;
                case "load":
                    obj.setLoad((Long) value);
                    return;
                case "days":
                    obj.setDays((Long) value);
                    return;
                case "studyPeriodStartDate":
                    obj.setStudyPeriodStartDate((Date) value);
                    return;
                case "studyPeriodFinishDate":
                    obj.setStudyPeriodFinishDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "row":
                        return true;
                case "loadType":
                        return true;
                case "part":
                        return true;
                case "load":
                        return true;
                case "days":
                        return true;
                case "studyPeriodStartDate":
                        return true;
                case "studyPeriodFinishDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "row":
                    return true;
                case "loadType":
                    return true;
                case "part":
                    return true;
                case "load":
                    return true;
                case "days":
                    return true;
                case "studyPeriodStartDate":
                    return true;
                case "studyPeriodFinishDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "row":
                    return EppWorkPlanRow.class;
                case "loadType":
                    return EppLoadType.class;
                case "part":
                    return Integer.class;
                case "load":
                    return Long.class;
                case "days":
                    return Long.class;
                case "studyPeriodStartDate":
                    return Date.class;
                case "studyPeriodFinishDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkPlanRowPartLoad> _dslPath = new Path<EppWorkPlanRowPartLoad>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkPlanRowPartLoad");
    }
            

    /**
     * @return Строка РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getRow()
     */
    public static EppWorkPlanRow.Path<EppWorkPlanRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Базовый класс для вида теоретической нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getLoadType()
     */
    public static EppLoadType.Path<EppLoadType> loadType()
    {
        return _dslPath.loadType();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getPart()
     */
    public static PropertyPath<Integer> part()
    {
        return _dslPath.part();
    }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getLoad()
     */
    public static PropertyPath<Long> load()
    {
        return _dslPath.load();
    }

    /**
     * @return Число дней (цикловая нагрузка).
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getDays()
     */
    public static PropertyPath<Long> days()
    {
        return _dslPath.days();
    }

    /**
     * @return Дата начала учебной деятельности (цикловая нагрузка).
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getStudyPeriodStartDate()
     */
    public static PropertyPath<Date> studyPeriodStartDate()
    {
        return _dslPath.studyPeriodStartDate();
    }

    /**
     * @return Дата окончания учебной деятельности (цикловая нагрузка).
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getStudyPeriodFinishDate()
     */
    public static PropertyPath<Date> studyPeriodFinishDate()
    {
        return _dslPath.studyPeriodFinishDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getDaysAsDouble()
     */
    public static SupportedPropertyPath<Double> daysAsDouble()
    {
        return _dslPath.daysAsDouble();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getLoadAsDouble()
     */
    public static SupportedPropertyPath<Double> loadAsDouble()
    {
        return _dslPath.loadAsDouble();
    }

    public static class Path<E extends EppWorkPlanRowPartLoad> extends EntityPath<E>
    {
        private EppWorkPlanRow.Path<EppWorkPlanRow> _row;
        private EppLoadType.Path<EppLoadType> _loadType;
        private PropertyPath<Integer> _part;
        private PropertyPath<Long> _load;
        private PropertyPath<Long> _days;
        private PropertyPath<Date> _studyPeriodStartDate;
        private PropertyPath<Date> _studyPeriodFinishDate;
        private SupportedPropertyPath<Double> _daysAsDouble;
        private SupportedPropertyPath<Double> _loadAsDouble;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getRow()
     */
        public EppWorkPlanRow.Path<EppWorkPlanRow> row()
        {
            if(_row == null )
                _row = new EppWorkPlanRow.Path<EppWorkPlanRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Базовый класс для вида теоретической нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getLoadType()
     */
        public EppLoadType.Path<EppLoadType> loadType()
        {
            if(_loadType == null )
                _loadType = new EppLoadType.Path<EppLoadType>(L_LOAD_TYPE, this);
            return _loadType;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getPart()
     */
        public PropertyPath<Integer> part()
        {
            if(_part == null )
                _part = new PropertyPath<Integer>(EppWorkPlanRowPartLoadGen.P_PART, this);
            return _part;
        }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getLoad()
     */
        public PropertyPath<Long> load()
        {
            if(_load == null )
                _load = new PropertyPath<Long>(EppWorkPlanRowPartLoadGen.P_LOAD, this);
            return _load;
        }

    /**
     * @return Число дней (цикловая нагрузка).
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getDays()
     */
        public PropertyPath<Long> days()
        {
            if(_days == null )
                _days = new PropertyPath<Long>(EppWorkPlanRowPartLoadGen.P_DAYS, this);
            return _days;
        }

    /**
     * @return Дата начала учебной деятельности (цикловая нагрузка).
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getStudyPeriodStartDate()
     */
        public PropertyPath<Date> studyPeriodStartDate()
        {
            if(_studyPeriodStartDate == null )
                _studyPeriodStartDate = new PropertyPath<Date>(EppWorkPlanRowPartLoadGen.P_STUDY_PERIOD_START_DATE, this);
            return _studyPeriodStartDate;
        }

    /**
     * @return Дата окончания учебной деятельности (цикловая нагрузка).
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getStudyPeriodFinishDate()
     */
        public PropertyPath<Date> studyPeriodFinishDate()
        {
            if(_studyPeriodFinishDate == null )
                _studyPeriodFinishDate = new PropertyPath<Date>(EppWorkPlanRowPartLoadGen.P_STUDY_PERIOD_FINISH_DATE, this);
            return _studyPeriodFinishDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getDaysAsDouble()
     */
        public SupportedPropertyPath<Double> daysAsDouble()
        {
            if(_daysAsDouble == null )
                _daysAsDouble = new SupportedPropertyPath<Double>(EppWorkPlanRowPartLoadGen.P_DAYS_AS_DOUBLE, this);
            return _daysAsDouble;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad#getLoadAsDouble()
     */
        public SupportedPropertyPath<Double> loadAsDouble()
        {
            if(_loadAsDouble == null )
                _loadAsDouble = new SupportedPropertyPath<Double>(EppWorkPlanRowPartLoadGen.P_LOAD_AS_DOUBLE, this);
            return _loadAsDouble;
        }

        public Class getEntityClass()
        {
            return EppWorkPlanRowPartLoad.class;
        }

        public String getEntityName()
        {
            return "eppWorkPlanRowPartLoad";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getDaysAsDouble();

    public abstract Double getLoadAsDouble();
}
