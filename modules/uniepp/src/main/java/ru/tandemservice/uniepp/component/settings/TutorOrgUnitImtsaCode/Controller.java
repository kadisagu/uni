package ru.tandemservice.uniepp.component.settings.TutorOrgUnitImtsaCode;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent context) {
        Model model = this.getModel(context);
        IDAO dao = this.getDao();
        {
            ErrorCollector errors = context.getUserContext().getErrorCollector();
            dao.validate(model, errors);
            if (errors.hasErrors()) {
                throw new ApplicationException();
            }
        }
        dao.update(model);
    }

    public void onClickSave(final IBusinessComponent context) {
        this.onClickApply(context);
        this.deactivate(context);
    }

}
