package ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvRegistryElement;

import ru.tandemservice.uni.dao.IPrepareable;

public interface IDAO extends IPrepareable<Model> {
    void save(Model model);
}
