package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x7x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.16"),
                        new ScriptDependency("org.tandemframework.shared", "1.7.1")
                };
    }


    private void moveSetting(DBTool tool, String srcColumnName, String settingsKey, boolean defaultValue) throws Exception
    {
        if (tool.hasResultRows("select value_p from settings_s where owner_p='eppGlobalSettings' and key_p=?", settingsKey))
            return;

        List<Object[]> values = tool.executeQuery(
                MigrationUtils.processor(Boolean.class),
                "select " + srcColumnName + " from epp_year_epp_t where educationyear_id=(select id from educationyear_t where current_p=?)", true
        );
        Boolean value = !values.isEmpty() ? (Boolean) values.get(0)[0] : defaultValue;
        tool.executeUpdate("insert into settings_s (owner_p, key_p, value_p) values ('eppGlobalSettings', ?, ?)", settingsKey, Boolean.class.getName() + "-" + value.toString());
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppYearEducationProcess

        moveSetting(tool, "usecyclicloading_p", "useCyclicLoading", false);
        moveSetting(tool, "allowwpwowg_p", "allowWPwoWG", true);
        moveSetting(tool, "checkelementstate_p", "checkElementState", true);
        moveSetting(tool, "allowemptywp_p", "allowEmptyWP", false);


        // удалено свойство useCyclicLoading
        {
            // удалить колонку
            tool.dropColumn("epp_year_epp_t", "usecyclicloading_p");

        }

        // удалено свойство allowWPwoWG
        {
            // удалить колонку
            tool.dropColumn("epp_year_epp_t", "allowwpwowg_p");

        }

        // удалено свойство checkElementState
        {
            // удалить колонку
            tool.dropColumn("epp_year_epp_t", "checkelementstate_p");

        }

        // удалено свойство allowEmptyWP
        {
            // удалить колонку
            tool.dropColumn("epp_year_epp_t", "allowemptywp_p");

        }


    }
}