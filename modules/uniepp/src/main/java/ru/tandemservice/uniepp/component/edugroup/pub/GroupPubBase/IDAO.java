/* $Id: IDAO.java 19933 2011-09-13 11:50:59Z oleyba $ */
package ru.tandemservice.uniepp.component.edugroup.pub.GroupPubBase;


/**
 * @author oleyba
 * @since 9/13/11
 */
public interface IDAO
{
    /** @return true, если компонент надо показывать */
    boolean prepare(Model model);
}
