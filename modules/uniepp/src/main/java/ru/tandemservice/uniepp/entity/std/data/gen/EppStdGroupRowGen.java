package ru.tandemservice.uniepp.entity.std.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.std.data.EppStdGroupRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdHierarchyRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись ГОС (группа)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStdGroupRowGen extends EppStdHierarchyRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.std.data.EppStdGroupRow";
    public static final String ENTITY_NAME = "eppStdGroupRow";
    public static final int VERSION_HASH = 172017612;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";
    public static final String P_NUMBER = "number";
    public static final String P_TITLE = "title";

    private EppStdHierarchyRow _parent;     // Запись ГОС (иерархия элементов)
    private String _number;     // Номер записи
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись ГОС (иерархия элементов).
     */
    public EppStdHierarchyRow getParent()
    {
        return _parent;
    }

    /**
     * @param parent Запись ГОС (иерархия элементов).
     */
    public void setParent(EppStdHierarchyRow parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Номер записи. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер записи. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppStdGroupRowGen)
        {
            setParent(((EppStdGroupRow)another).getParent());
            setNumber(((EppStdGroupRow)another).getNumber());
            setTitle(((EppStdGroupRow)another).getTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStdGroupRowGen> extends EppStdHierarchyRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStdGroupRow.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppStdGroupRow is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return obj.getParent();
                case "number":
                    return obj.getNumber();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "parent":
                    obj.setParent((EppStdHierarchyRow) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                        return true;
                case "number":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return true;
                case "number":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return EppStdHierarchyRow.class;
                case "number":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStdGroupRow> _dslPath = new Path<EppStdGroupRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStdGroupRow");
    }
            

    /**
     * @return Запись ГОС (иерархия элементов).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdGroupRow#getParent()
     */
    public static EppStdHierarchyRow.Path<EppStdHierarchyRow> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Номер записи. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdGroupRow#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdGroupRow#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EppStdGroupRow> extends EppStdHierarchyRow.Path<E>
    {
        private EppStdHierarchyRow.Path<EppStdHierarchyRow> _parent;
        private PropertyPath<String> _number;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись ГОС (иерархия элементов).
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdGroupRow#getParent()
     */
        public EppStdHierarchyRow.Path<EppStdHierarchyRow> parent()
        {
            if(_parent == null )
                _parent = new EppStdHierarchyRow.Path<EppStdHierarchyRow>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Номер записи. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdGroupRow#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppStdGroupRowGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdGroupRow#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppStdGroupRowGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EppStdGroupRow.class;
        }

        public String getEntityName()
        {
            return "eppStdGroupRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
