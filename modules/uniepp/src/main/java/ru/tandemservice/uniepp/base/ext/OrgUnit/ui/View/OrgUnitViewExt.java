/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniepp.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        /**
         <component-tab name="orgUnitEppTab" label="Учебный процесс"
         component-name="ru.tandemservice.uniepp.component.orgUnit.OrgUnitEppTab"
         secured-object="fast:model.orgUnit"
         permission-key="ognl:model.getPermissionKey('orgUnitEppTab_')"	
         visible="ognl:@ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO@instance.get().isShowEppOrgUnitTab(model.getOrgUnitId())"			
         before="abstractOrgUnit_OrgUnitSecSettings"					
         />
         <component-tab name="orgUnitEppRealGroupTab" label="Учебные группы"
         component-name="ru.tandemservice.uniepp.component.orgUnit.OrgUnitRealGroupTab"
         secured-object="fast:model.orgUnit"
         permission-key="ognl:model.getPermissionKey('orgUnitRealGroupTab_')"	
         visible="ognl:@ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO@instance.get().isShowRealGroupOrgUnitTab(model.getOrgUnitId())"			
         before="abstractOrgUnit_OrgUnitSecSettings"					
         />         */
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab("orgUnitEppTab", "ru.tandemservice.uniepp.component.orgunit.OrgUnitEppTab").permissionKey("ui:secModel.orgUnitEppTab").visible("ognl:@ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO@instance.get().isShowEppOrgUnitTab(presenter.orgUnit.id)").parameters("mvel:['orgUnitId':presenter.orgUnit.id]"))
                .addTab(componentTab("orgUnitEppRealGroupTab", "ru.tandemservice.uniepp.component.orgunit.OrgUnitRealGroupTab").permissionKey("ui:secModel.orgUnitRealGroupTab").visible("ognl:@ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO@instance.get().isShowRealGroupOrgUnitTab(presenter.orgUnit.id)").parameters("mvel:['orgUnitId':presenter.orgUnit.id]"))
                .create();
    }
}
