/* $Id:$ */
package ru.tandemservice.uniepp.component.indicators.EppRegistryCheck;

import org.tandemframework.shared.commonbase.base.util.Wiki;

/**
 * @author oleyba
 * @since 5/30/12
 */
@Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=8945678")
public interface IDAO
{
    void prepare(Model model);
}
