/* $Id$ */
package ru.tandemservice.uniepp.component.registry.RegElementGradeScales;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author oleyba
 * @since 11/15/11
 */
public interface IDAO extends IPrepareable<Model>
{
    void update(Model model);
}
