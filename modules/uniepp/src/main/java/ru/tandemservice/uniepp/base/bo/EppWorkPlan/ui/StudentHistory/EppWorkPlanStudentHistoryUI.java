/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.StudentHistory;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.StudentHistory.logic.EppWorkPlanStudentHistoryDSHandler;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

/**
 * @author nvankov
 * @since 10/8/14
 */
@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "student.id") })
public class EppWorkPlanStudentHistoryUI extends UIPresenter
{
    private Student _student = new Student();

    @Override
    public void onComponentRefresh()
    {
        _student = DataAccessServices.dao().get(_student.getId());
        int i = 0;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(EppWorkPlanStudentHistory.HISTORY_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppWorkPlanStudentHistoryDSHandler.STUDENT_ID, _student.getId());
        }
    }

    public void onClickDeleteSepv()
    {
        IEntity entity = DataAccessServices.dao().get(getListenerParameterAsLong());
        if(entity instanceof EppStudent2WorkPlan)
        {
            DataAccessServices.dao().delete(((EppStudent2WorkPlan) entity).getStudentEduPlanVersion());
        }
        else if(entity instanceof EppStudent2EduPlanVersion)
        {
            DataAccessServices.dao().delete(entity);
        }
        else throw new IllegalStateException();
    }

    public void onClickDeleteSwp()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }
}
