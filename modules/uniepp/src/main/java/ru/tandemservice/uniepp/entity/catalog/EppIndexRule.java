package ru.tandemservice.uniepp.entity.catalog;

import ru.tandemservice.uniepp.entity.catalog.codes.EppIndexRuleCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppIndexRuleGen;

/**
 * Алгоритмы формирования индексов
 */
public class EppIndexRule extends EppIndexRuleGen
{
    public static final String CODE_SIMPLE_CONCAT = EppIndexRuleCodes.SIMPLE_CONCAT;
    public static final String CODE_POSTFIX_CONCAT = EppIndexRuleCodes.POSTFIX_CONCAT;
    public static final String CODE_PREFIX_HIERARCHY = EppIndexRuleCodes.PREFIX_HIERARCHY;
    public static final String CODE_PREFIX_COMPONENT_HIERARCHY = EppIndexRuleCodes.PREFIX_COMPONENT_HIERARCHY;
    public static final String CODE_TRIMMED_CONCAT = EppIndexRuleCodes.TRIMMED_CONCAT;


}