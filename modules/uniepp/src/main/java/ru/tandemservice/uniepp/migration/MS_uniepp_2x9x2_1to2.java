/* $Id$ */
package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Andrey Andreev
 * @since 11.02.2016
 */
public class MS_uniepp_2x9x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.2")
        };
    }

    @Override
    public void run(final DBTool tool) throws Exception {
        // Никогда нельзя менять коды элементов справочников
    }
}