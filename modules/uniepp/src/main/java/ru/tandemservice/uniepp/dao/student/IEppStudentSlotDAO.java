package ru.tandemservice.uniepp.dao.student;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

/**
 * @author vdanilov
 */
public interface IEppStudentSlotDAO {
    SpringBeanCache<IEppStudentSlotDAO> instance = new SpringBeanCache<>(IEppStudentSlotDAO.class.getName());

    /**
     * обновляет список слотов по текущим данным системы
     * @param activeStudentOnly Учитывать только неархивных студентов в активном состоянии.
     * @throws RuntimeTimeoutException если протухла блокировка
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    boolean doUpdateStudentSlotList(boolean activeStudentOnly);

    /**
     * проверяет актуальность связей студентов с УП и РУП
     * создает (актуализирует) новые связи на основе настройки
     * проверяет дубли активных связей
     * устраняет найденные ошибки
     * 
     * @param forceRefreshWorkPlanRelations обновлять РУП даже если связи с УП не изменились
     * @throws RuntimeTimeoutException если протухла блокировка
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    boolean doUpdateStudentRelations(boolean forceRefreshWorkPlanRelations);

}
