/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockContent;

import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Nikolay Fedorovskih
 * @since 13.01.2015
 */
public abstract class EppFakeRowWrapper implements IEppEpvRowWrapper
{
    private IEppEpvRowWrapper _parentRow;
    private long _id;

    public EppFakeRowWrapper(IEppEpvRowWrapper parentRow)
    {
        _parentRow = parentRow;
        _id = EntityIDGenerator.generateNewId((short) 0);
    }

    public abstract String wrapFullCode(String loadFullCode);

    @Override
    public double getTotalLoad(Integer term, String loadFullCode)
    {
        if (term == null && EppELoadType.FULL_CODE_AUDIT.equals(loadFullCode))
            return _parentRow.getTotalLoad(0, wrapFullCode(loadFullCode)); // Чтобы "Всего аудиторн." была просто сумма по "Всего" видов нагрузки

        return _parentRow.getTotalLoad(term, wrapFullCode(loadFullCode));
    }

    @Override
    public String getTotalDisplayableLoadString(Integer term, String loadFullCode, boolean hideZero, int divider)
    {
        if (EppLoadType.FULL_CODE_LABOR.equals(loadFullCode))
            return "";

        return UniEppUtils.formatLoad(this.getTotalLoad(term, loadFullCode) / divider, hideZero);
    }

    @Override
    public String getRowWrapperTotalByLoadTypeRaw(String loadFullCode, int divider)
    {
        if (EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL.equals(loadFullCode))
            return "";

        final double registryValue = this.getTotalLoad(0, loadFullCode) / divider;
        final String registryValueString = UniEppUtils.formatLoad(registryValue, false);

        if (this.isTermDataOwner()) {
            /* если элемент содержит данные по нагрузке. придется считать соответствие */
            final double calculatedValue = this.getTotalLoad(null, loadFullCode) / divider;
            if ((calculatedValue <= 0) || UniEppUtils.eq(registryValue, calculatedValue)) {
                // если не распределено или совпало - то выводим то, что есть
                return registryValueString;
            }
            final String calculatedValueString = UniEppUtils.formatLoad(calculatedValue, false);
            return UniEppUtils.error(calculatedValueString /*по УП-как сумма по строке*/, registryValueString /*из реестра*/);
        }

        return registryValueString;
    }

    @Override
    public double getTotalDisplayableLoad(Integer term, String loadFullCode)
    {
        return _parentRow.getTotalDisplayableLoad(term, wrapFullCode(loadFullCode));
    }

    @Override
    public double getTotalInTermLoad(Integer term, String loadFullCode)
    {
        return _parentRow.getTotalInTermLoad(term, wrapFullCode(loadFullCode));
    }

    @Override
    public List<EppProfessionalTask> getProfessionalTaskList()
    {
        return null;
    }

    @Override
    public void setProfessionalTaskList(List<EppProfessionalTask> professionalTaskList)
    {
    }

    @Override
    public IEppEpvBlockWrapper getVersion()
    {
        return _parentRow.getVersion();
    }

    @Override
    public IEppEpvRow getRow()
    {
        return _parentRow.getRow();
    }

    @Override
    public IEppEpvRowWrapper getHierarhyParent()
    {
        return _parentRow;
    }

    @Override
    public List<IEppEpvRowWrapper> getChilds()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isTermDataOwner()
    {
        return true;
    }

    @Override
    public String getDisplayableControlActionUsage(String controlActionFullCode)
    {
        return "";
    }

    @Override
    public String getDisplayableTitle()
    {
        return null;
    }

    @Override
    public int getActionSize(Integer term, String actionFullCode)
    {
        return 0;
    }

    @Override
    public List<Integer> getActionTermSet(String actionFullCode)
    {
        return null;
    }

    @Override
    public boolean hasInTermActivity(Integer term)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<Integer> getActiveTermSet()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getActiveTermNumber(int term)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getTermNumber(int activeTermNumber)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public Map<Integer, Map<String, Number>> getWrapperDataMap()
    {
        return null;
    }

    @Override
    public String getTermKey(Integer term)
    {
        return null;
    }

    @Override
    public EppEduPlanVersionBlock getOwner()
    {
        return this.getRow().getOwner();
    }

    @Override
    public EppRegistryStructure getType()
    {
        return this.getRow().getType();
    }

    @Override
    public String getRowType()
    {
        return "";
    }

    @Override
    public Boolean getUsedInLoad()
    {
        return _parentRow.getUsedInLoad();
    }

    @Override
    public Boolean getUsedInActions()
    {
        return _parentRow.getUsedInActions();
    }

    @Override
    public String getStoredIndex()
    {
        return "";
    }

    @Override
    public String getUserIndex()
    {
        return "";
    }

    @Override
    public Class getRowEntityClass()
    {
        return this.getRow().getRowEntityClass();
    }

    @Override
    public void setId(Long id)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object getProperty(Object propertyPath)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setProperty(String propertyPath, Object propertyValue)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getHierarhyPath()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getHierarhyIndex()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getSequenceNumber()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getIndex()
    {
        return "";
    }

    @Override
    public String getTitleWithIndex()
    {
        return "";
    }

    @Override
    public EppGeneration getGeneration()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getQualificationCode()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getSelfIndexPart()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public Long getId()
    {
        return _id;
    }

    @Override
    public String getTitle()
    {
        return this.getDisplayableTitle();
    }

    @Override
    public Integer getEpvRegistryRowNumber()
    {
        return null;
    }
}