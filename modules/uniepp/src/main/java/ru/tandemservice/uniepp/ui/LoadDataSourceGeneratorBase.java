package ru.tandemservice.uniepp.ui;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.functors.InstanceofPredicate;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.IRowCustomizer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;

import java.util.Collection;
import java.util.List;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public abstract class LoadDataSourceGeneratorBase {

    protected static final Predicate ELOAD_TOTALAUDIT_PREDICATE = object -> (((EppELoadType)object).isAuditTotal());
    protected static final Predicate ELOAD_SELFWORK_PREDICATE = object -> (((EppELoadType)object).isSelfWork());
    protected static final Predicate DISCIPLINE_CA_TYPE_PREDICATE = object -> (((EppControlActionType)object).isUsedWithDisciplines());
    protected static final Predicate ACTIONS_CA_TYPE_PREDICATE = object -> (((EppControlActionType)object).isUsedWithAttestation() || ((EppControlActionType)object).isUsedWithPractice());
    protected static final IEntityHandler NO_EDIT_DISABLER = entity -> false;


    // load edit permission key
    protected abstract String getPermissionKeyEdit();
    protected final String permissionKeyEdit = this.getPermissionKeyEdit();

    protected abstract boolean isEditable();
    protected final boolean editable = this.isEditable();


    // first, preload all data
    protected final List<EppControlActionType> controlActionTypes = this.getPossibleControlActions();
    protected final List<EppControlActionType> disciplineCATypes = (List<EppControlActionType>)CollectionUtils.select(this.controlActionTypes, LoadDataSourceGeneratorBase.DISCIPLINE_CA_TYPE_PREDICATE);
    protected final List<EppControlActionType> actionsCATypes = (List<EppControlActionType>)CollectionUtils.select(this.controlActionTypes, LoadDataSourceGeneratorBase.ACTIONS_CA_TYPE_PREDICATE);
    protected final List<EppLoadType> loadTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppLoadType.class);
    protected final Collection<EppELoadType> eLoadTypes = CollectionUtils.select(this.loadTypes, new InstanceofPredicate(EppELoadType.class));
    protected final Collection<EppALoadType> aLoadTypes = CollectionUtils.select(this.loadTypes, new InstanceofPredicate(EppALoadType.class));
    protected final EppELoadType eLoadTotalAuditType = (EppELoadType) CollectionUtils.find(this.eLoadTypes, LoadDataSourceGeneratorBase.ELOAD_TOTALAUDIT_PREDICATE);
    protected final EppELoadType eLoadSelfWorkType = (EppELoadType) CollectionUtils.find(this.eLoadTypes, LoadDataSourceGeneratorBase.ELOAD_SELFWORK_PREDICATE);

    // действие
    protected IndicatorColumn getColumn_action(final IEntityHandler editDisabler, final String actionTitle, final String actionIcon, final String listener) {
        return UniEppUtils.getColumn_action(editDisabler, this.permissionKeyEdit, actionIcon, actionTitle, listener);
    }

    protected IRowCustomizer wrapRowCustomizer(IRowCustomizer rowCustomizer) {
        return rowCustomizer;
    }

    protected abstract List<EppControlActionType> getPossibleControlActions();

    public static AbstractColumn wrap(AbstractColumn column) {
        column = column.setHeaderAlign("center").setOrderable(false).setRequired(true);
        if (column instanceof PublisherLinkColumn) { return column; }
        return column.setClickable(false);
    }

    public static AbstractColumn vwrap(final AbstractColumn column) {
        return wrap(column).setVerticalHeader(true).setWidth(1);
    }

    protected String getOrgUnitFullTitle(final OrgUnit orgUnit) {
        return (null == orgUnit ? "" : orgUnit.getFullTitle());
    }

    protected String getOrgUnitShortTitle(final OrgUnit orgUnit) {
        return (null == orgUnit ? "" : orgUnit.getShortTitle());
    }

    //    // разделитель
    //    protected AbstractColumn getColumn_separator(final String key) {
    //        return new SeparatorColumn(key);
    //    }

    // error decorator
    protected static String error(final String documentValue, final String targetValue) {
        return UniEppUtils.error(documentValue, targetValue);
    }


}
