package ru.tandemservice.uniepp.component.edustd.EduStdBlockList;

import ru.tandemservice.uni.dao.IUpdateable;

public interface IDAO extends IUpdateable<Model>
{
    void doSwitchUsedInLoad(Long rowWrapperId);

    void doSwitchUsedInActions(Long rowWrapperId);
}