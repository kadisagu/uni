package ru.tandemservice.uniepp.entity.catalog;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniepp.catalog.bo.EppIControlActionType.ui.AddEdit.EppIControlActionTypeAddEdit;
import ru.tandemservice.uniepp.entity.catalog.gen.EppIControlActionTypeGen;

import java.util.Collection;
import java.util.List;

/**
 * Форма текущего контроля
 */
public class EppIControlActionType extends EppIControlActionTypeGen implements IDynamicCatalogItem
{
	private static final List<String> HIDDEN_PROPERTIES = ImmutableList.of(
		P_ACTIVE_IN_EDU_PLAN,
		L_DEFAULT_GROUP_TYPE,
		P_USED_WITH_ATTESTATION,
		P_USED_WITH_DISCIPLINES,
		P_USED_WITH_PRACTICE,
		P_MAX_COUNT,
		P_DISABLED
	);

	@Override protected int getClassComparatorPriority() {
        return 1;
    }
    @Override protected int getInClassComparatorPriority() {
        return 0;
    }

    public EppIControlActionType() {
        this.setCatalogCode(EppIControlActionTypeGen.ENTITY_NAME);
    }

    @Override
    @EntityDSLSupport
    public boolean isEnabled() { return !isDisabled(); }
    public void setEnabled(boolean value) { setDisabled(!value); }

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public Collection<String> getHiddenFields() {
                return HIDDEN_PROPERTIES;
            }
            @Override public String getAddEditComponentName() {
                return EppIControlActionTypeAddEdit.class.getSimpleName();
            }
        };
    }

}