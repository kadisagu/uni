package ru.tandemservice.uniepp.entity.catalog;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.gen.EppRealEduGroupCompleteLevelGen;

/**
 * Степень сформированности учеьной группы
 */
public class EppRealEduGroupCompleteLevel extends EppRealEduGroupCompleteLevelGen implements IHierarchyItem
{
    @Override
    public IHierarchyItem getHierarhyParent() {
        return this.getParent();
    }

    protected String getParentPathTitle(EppRealEduGroupCompleteLevel parent) {
        final StringBuilder sb = new StringBuilder();
        while (null != parent) {
            if (sb.length() > 0) { sb.append(", "); }
            sb.append(parent.getShortTitleSafe());
            parent = parent.getParent();
        }
        return sb.toString();
    }

    /** @return [название] ([сокращенные названия вышестоящих элементов через запятую]) */
    @Override
    @EntityDSLSupport(parts={EppRealEduGroupCompleteLevelGen.P_CODE})
    public String getDisplayableTitle() {
        final EppRealEduGroupCompleteLevel parent = this.getParent();
        if (null == parent) { return this.getTitle(); }
        return  this.getTitle() + " ("+this.getParentPathTitle(parent)+")";
    }

    /** @return [сокращенное название] ([сокращенные названия вышестоящих элементов через запятую]) */
    @Override
    @EntityDSLSupport(parts={EppRealEduGroupCompleteLevelGen.P_CODE})
    public String getDisplayableShortTitle() {
        final EppRealEduGroupCompleteLevel parent = this.getParent();
        if (null == parent) { return this.getShortTitleSafe(); }
        return  this.getShortTitleSafe() + " ("+this.getParentPathTitle(parent)+")";
    }

    private String getShortTitleSafe() {
        final String title = StringUtils.trimToNull(this.getShortTitle());
        if (null != title) { return title; }
        return this.getTitle();
    }

    public EppRealEduGroupCompleteLevel getRoot() {
        final EppRealEduGroupCompleteLevel parent = this.getParent();
        return (null == parent) ? this : parent.getRoot();
    }

    public String getTakedCode() {
        return this.getRoot().getCode()+".1";
    }

    public String getApprovedCode() {
        return this.getRoot().getCode()+".2";
    }



    public EppRealEduGroupCompleteLevel getTaked() {
        return IUniBaseDao.instance.get().<EppRealEduGroupCompleteLevel>getByNaturalId(new EppRealEduGroupCompleteLevelGen.NaturalId(getTakedCode()));
    }

    private static int p(final EppRealEduGroupCompleteLevel o) {
        if (null == o)  { return Integer.MIN_VALUE; }
        return o.getRoot().getPriority();
    }

    public static boolean gt(final EppRealEduGroupCompleteLevel l1, final EppRealEduGroupCompleteLevel l2) {
        return EppRealEduGroupCompleteLevel.p(l1) > EppRealEduGroupCompleteLevel.p(l2);
    }

    public static boolean ge(final EppRealEduGroupCompleteLevel l1, final EppRealEduGroupCompleteLevel l2) {
        return EppRealEduGroupCompleteLevel.p(l1) >= EppRealEduGroupCompleteLevel.p(l2);
    }

    /** @return текущий элемент равен или является потомком для selected */
    public boolean isChildOf(final EppRealEduGroupCompleteLevel selected) {
        if (this.equals(selected)) { return true; }
        if (null == this.getParent()) { return false; }
        return this.getParent().isChildOf(selected);
    }

}