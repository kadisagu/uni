package ru.tandemservice.uniepp.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import ru.tandemservice.uniepp.entity.catalog.gen.EppSkillGroupGen;

/**
 * Группа компетенций
 */
public class EppSkillGroup extends EppSkillGroupGen implements IDynamicCatalogItem, IPrioritizedCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EppSkillGroup.class)
                .titleProperty(EppSkillGroup.title().s())
                .filter(EppSkillGroup.title())
                .order(EppSkillGroup.title());
    }
}