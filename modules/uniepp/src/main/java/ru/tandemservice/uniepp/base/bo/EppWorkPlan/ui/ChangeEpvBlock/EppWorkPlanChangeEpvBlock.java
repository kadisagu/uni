/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.ChangeEpvBlock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Nikolay Fedorovskih
 * @since 20.10.2014
 */
@Configuration
public class EppWorkPlanChangeEpvBlock extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}