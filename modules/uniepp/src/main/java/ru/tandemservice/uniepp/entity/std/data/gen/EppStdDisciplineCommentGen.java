package ru.tandemservice.uniepp.entity.std.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineBaseRow;
import ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineComment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Описание дисциплины
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStdDisciplineCommentGen extends EntityBase
 implements INaturalIdentifiable<EppStdDisciplineCommentGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineComment";
    public static final String ENTITY_NAME = "eppStdDisciplineComment";
    public static final int VERSION_HASH = 1844174479;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW = "row";
    public static final String P_COMMENT = "comment";

    private EppStdDisciplineBaseRow _row;     // Запись ГОС (базовая запись для дисциплины)
    private String _comment;     // Описание дисциплины

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись ГОС (базовая запись для дисциплины). Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppStdDisciplineBaseRow getRow()
    {
        return _row;
    }

    /**
     * @param row Запись ГОС (базовая запись для дисциплины). Свойство не может быть null и должно быть уникальным.
     */
    public void setRow(EppStdDisciplineBaseRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Описание дисциплины. Свойство не может быть null.
     */
    @NotNull
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Описание дисциплины. Свойство не может быть null.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppStdDisciplineCommentGen)
        {
            if (withNaturalIdProperties)
            {
                setRow(((EppStdDisciplineComment)another).getRow());
            }
            setComment(((EppStdDisciplineComment)another).getComment());
        }
    }

    public INaturalId<EppStdDisciplineCommentGen> getNaturalId()
    {
        return new NaturalId(getRow());
    }

    public static class NaturalId extends NaturalIdBase<EppStdDisciplineCommentGen>
    {
        private static final String PROXY_NAME = "EppStdDisciplineCommentNaturalProxy";

        private Long _row;

        public NaturalId()
        {}

        public NaturalId(EppStdDisciplineBaseRow row)
        {
            _row = ((IEntity) row).getId();
        }

        public Long getRow()
        {
            return _row;
        }

        public void setRow(Long row)
        {
            _row = row;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppStdDisciplineCommentGen.NaturalId) ) return false;

            EppStdDisciplineCommentGen.NaturalId that = (NaturalId) o;

            if( !equals(getRow(), that.getRow()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRow());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRow());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStdDisciplineCommentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStdDisciplineComment.class;
        }

        public T newInstance()
        {
            return (T) new EppStdDisciplineComment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "row":
                    return obj.getRow();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "row":
                    obj.setRow((EppStdDisciplineBaseRow) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "row":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "row":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "row":
                    return EppStdDisciplineBaseRow.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppStdDisciplineComment> _dslPath = new Path<EppStdDisciplineComment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStdDisciplineComment");
    }
            

    /**
     * @return Запись ГОС (базовая запись для дисциплины). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineComment#getRow()
     */
    public static EppStdDisciplineBaseRow.Path<EppStdDisciplineBaseRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Описание дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineComment#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends EppStdDisciplineComment> extends EntityPath<E>
    {
        private EppStdDisciplineBaseRow.Path<EppStdDisciplineBaseRow> _row;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись ГОС (базовая запись для дисциплины). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineComment#getRow()
     */
        public EppStdDisciplineBaseRow.Path<EppStdDisciplineBaseRow> row()
        {
            if(_row == null )
                _row = new EppStdDisciplineBaseRow.Path<EppStdDisciplineBaseRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Описание дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.std.data.EppStdDisciplineComment#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EppStdDisciplineCommentGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return EppStdDisciplineComment.class;
        }

        public String getEntityName()
        {
            return "eppStdDisciplineComment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
