/**
 *$Id: EppWorkPlanStudentListUI.java 26089 2013-02-11 08:34:37Z vdanilov $
 */
package ru.tandemservice.uniepp.base.bo.EppEduGroup.ui.List4Student;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Collections;
import java.util.Map;

@Input({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="studentHolder.id", required=true)
})
public class EppEduGroupList4StudentUI extends UIPresenter
{
    private final EntityHolder<Student> studentHolder = new EntityHolder<Student>();
    public EntityHolder<Student> getStudentHolder() { return this.studentHolder; }

    public OrgUnit getGroupOrgUnit() {
        return getStudentHolder().getValue().getEducationOrgUnit().getGroupOrgUnit();
    }

    public Map<String, Object> getParameters() {
        OrgUnit groupOrgUnit = getGroupOrgUnit();
        return new ParametersMap().add("studentIds", Collections.singleton(getStudentHolder().getId())).add("groupOrgUnitId", (null == groupOrgUnit ? null : groupOrgUnit.getId()));
    }
}
