package ru.tandemservice.uniepp.component.registry.RegElementEduPlanList;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.SafeSimpleColumn;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        final StaticListDataSource<ViewWrapper<IEntity>> dataSource = model.getDataSource();

        dataSource.clearColumns();

        dataSource.addColumn(UniEppUtils.getStateColumn());
        dataSource.addColumn(new PublisherLinkColumn("УП(в)", "title", EppEpvRegistryRow.owner().eduPlanVersion()).setClickable(true).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направление (направленность)", EppEpvRegistryRow.owner().educationElementSimpleTitle()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Характеристики обучения", EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().developCombinationTitle()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Индекс", "index").setClickable(false).setOrderable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Название", EppEpvRegistryRow.title()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Частей по УП", "parts").setClickable(false).setOrderable(false).setWidth(1));

        for (final EppRegistryElementPart p: this.getList(EppRegistryElementPart.class, EppRegistryElementPart.registryElement().id(), model.getId(), EppRegistryElementPart.number().s())) {
            final String name = "part."+p.getNumber();
            final HeadColumn h = new HeadColumn(name, String.valueOf(p.getNumber()));
            h.addColumn(new SafeSimpleColumn("Н", name+".l").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setOrderable(false).setWidth(1));
            h.addColumn(new SafeSimpleColumn("Ж", name+".j").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setOrderable(false).setWidth(1));
            h.addColumn(new SafeSimpleColumn("С", name+".s").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setOrderable(false).setWidth(1));
            dataSource.addColumn(h);
        }


        final DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(EppEpvRegistryRow.class, "e").column(property("e"))
        .where(eq(property(EppEpvRegistryRow.registryElement().id().fromAlias("e")), value(model.getId())))
        .order(property(EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().number().fromAlias("e")))
        .order(property(EppEpvRegistryRow.owner().eduPlanVersion().number().fromAlias("e")))
        ;

        final List<ViewWrapper<IEntity>> patchedList = ViewWrapper.<IEntity>getPatchedList(dql.createStatement(this.getSession()).<IEntity>list());

        final Map<String, EppLoadType> loadMap = EppEduPlanVersionDataDAO.getLoadTypeMap();
        final Map<String, EppControlActionType> actionMap = EppEduPlanVersionDataDAO.getActionTypeMap();

        final Collection<Long> blockIds = new DQLSelectBuilder()
        .fromEntity(EppEpvRegistryRow.class, "r")
        .where(in(property(EppEpvRegistryRow.id().fromAlias("r")), CommonDAO.ids(patchedList)))
        .column(property(EppEpvRegistryRow.owner().id().fromAlias("r")))
        .predicate(DQLPredicateType.distinct)
        .createStatement(this.getSession()).list();
        final Map<Long, IEppEpvBlockWrapper> blockWrapperMap = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(blockIds, true);

        for (final Iterator<ViewWrapper<IEntity>> wi = patchedList.iterator(); wi.hasNext(); ){
            final ViewWrapper<IEntity> w = wi.next();
            final IEppEpvBlockWrapper epvWrapper = blockWrapperMap.get(((EppEpvRegistryRow)w.getEntity()).getOwner().getId());
            final IEppEpvRowWrapper row = epvWrapper.getRowMap().get(w.getId());

            if (null == row) {
                // если строки не найденно (хм, дисциплина есть, но не показалась - баг какой-то) - удаляем строку из результата
                wi.remove();
                continue;
            }

            w.setViewProperty("index", row.getIndex());
            w.setViewProperty("parts", row.getActiveTermSet().size());

            for (final Integer t: row.getActiveTermSet()) {
                final String name = "part."+row.getActiveTermNumber(t);

                {
                    final StringBuilder sb = new StringBuilder();
                    for (final EppLoadType lt: loadMap.values()) {
                        final double l = row.getTotalInTermLoad(t, lt.getFullCode());
                        if (l <= 0) { continue; }
                        if (sb.length() > 0) { sb.append("\n"); }
                        sb.append(lt.getShortTitle()).append(": ").append(UniEppUtils.LOAD_FORMATTER.format(l));
                    }
                    w.setViewProperty(name+".l", sb.toString());
                }

                {
                    final StringBuilder sb = new StringBuilder();
                    for (final EppControlActionType cat: actionMap.values()) {
                        if (cat instanceof EppFControlActionType) { continue; }
                        final int s = row.getActionSize(t, cat.getFullCode());
                        if (s <= 0) { continue; }
                        if (sb.length() > 0) { sb.append("\n"); }
                        sb.append(cat.getShortTitle()).append(": ").append(s);
                    }
                    w.setViewProperty(name+".j", sb.toString());
                }

                {
                    final StringBuilder sb = new StringBuilder();
                    for (final EppControlActionType cat: actionMap.values()) {
                        if (!(cat instanceof EppFControlActionType)) { continue; }
                        final int s = row.getActionSize(t, cat.getFullCode());
                        if (s <= 0) { continue; }
                        if (sb.length() > 0) { sb.append(", "); }
                        sb.append(cat.getShortTitle());
                    }
                    w.setViewProperty(name+".s", sb.toString());
                }
            }
        }

        // результат - в датасорс
        dataSource.setupRows(patchedList);
    }



}
