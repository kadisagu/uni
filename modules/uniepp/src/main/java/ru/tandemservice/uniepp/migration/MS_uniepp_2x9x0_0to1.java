package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x9x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.createColumn("epp_eduplan_prof_secondary_t", new DBColumn("ownerorgunit_id", DBType.LONG));

        tool.executeUpdate("update epp_eduplan_prof_secondary_t set ownerorgunit_id = " +
            "(select distinct(s.ownerorgunit_id) from " +
            "  epp_eduplan_verblock_s_t s " +
            "  inner join epp_eduplan_verblock_t b on s.id = b.id " +
            "  inner join epp_eduplan_ver_t v on v.id=b.eduPlanVersion_id " +
            " where v.eduPlan_id = epp_eduplan_prof_secondary_t.id)");

        tool.executeUpdate("update epp_epvrow_base_t set " +
            " owner_id = (" +
            "  select r.id from " +
            "   epp_eduplan_verblock_r_t r" +
            "   inner join epp_eduplan_verblock_t b1 on r.id = b1.id," +
            "   epp_eduplan_verblock_s_t s " +
            "   inner join epp_eduplan_verblock_t b2 on s.id = b2.id" +
            "  where " +
            "   epp_epvrow_base_t.owner_id = s.id and" +
            "   b1.eduPlanVersion_id = b2.eduPlanVersion_id)" +
            " where exists(" +
            "  select s.id from " +
            "   epp_eduplan_verblock_s_t s" +
            "   inner join epp_eduplan_verblock_t b on s.id = b.id" +
            "   inner join epp_eduplan_ver_t v on v.id=b.eduPlanVersion_id" +
            "   inner join epp_eduplan_prof_secondary_t p on v.eduPlan_id = p.id" +
            "  where epp_epvrow_base_t.owner_id = s.id)");

        tool.executeUpdate("update epp_workplan_t set " +
            " parent_id = (" +
            "  select r.id from " +
            "   epp_eduplan_verblock_r_t r" +
            "   inner join epp_eduplan_verblock_t b1 on r.id = b1.id," +
            "   epp_eduplan_verblock_s_t s " +
            "   inner join epp_eduplan_verblock_t b2 on s.id = b2.id" +
            "  where " +
            "   epp_workplan_t.parent_id = s.id and" +
            "   b1.eduPlanVersion_id = b2.eduPlanVersion_id)" +
            " where exists(" +
            "  select s.id from " +
            "   epp_eduplan_verblock_s_t s" +
            "   inner join epp_eduplan_verblock_t b on s.id = b.id" +
            "   inner join epp_eduplan_ver_t v on v.id=b.eduPlanVersion_id" +
            "   inner join epp_eduplan_prof_secondary_t p on v.eduPlan_id = p.id" +
            "  where epp_workplan_t.parent_id = s.id)");

        final int n = tool.executeUpdate(
                "update epp_student_eduplanversion_t set " +
                        " block_id = (" +
                        "  select r.id from " +
                        "   epp_eduplan_verblock_r_t r" +
                        "   inner join epp_eduplan_verblock_t b1 on r.id = b1.id," +
                        "   epp_eduplan_verblock_s_t s " +
                        "   inner join epp_eduplan_verblock_t b2 on s.id = b2.id" +
                        "  where " +
                        "   epp_student_eduplanversion_t.block_id = s.id and" +
                        "   b1.eduPlanVersion_id = b2.eduPlanVersion_id)" +
                        " where exists(" +
                        "  select s.id from " +
                        "   epp_eduplan_verblock_s_t s" +
                        "   inner join epp_eduplan_verblock_t b on s.id = b.id" +
                        "   inner join epp_eduplan_ver_t v on v.id=b.eduPlanVersion_id" +
                        "   inner join epp_eduplan_prof_secondary_t p on v.eduPlan_id = p.id" +
                        "  where epp_student_eduplanversion_t.block_id = s.id)");
        //System.out.println(n);

        tool.executeUpdate("delete from epp_eduplan_verblock_s_t" +
            " where id in (" +
            "  select s.id from " +
            "   epp_eduplan_verblock_s_t s" +
            "   inner join epp_eduplan_verblock_t b on s.id = b.id" +
            "   inner join epp_eduplan_ver_t v on v.id=b.eduPlanVersion_id" +
            "   inner join epp_eduplan_prof_secondary_t p on v.eduPlan_id = p.id)");

        tool.executeUpdate("delete from epp_eduplan_verblock_t where " +
            " id not in (select id from epp_eduplan_verblock_s_t) and" +
            " id not in (select id from epp_eduplan_verblock_r_t)");

        tool.executeUpdate("delete from edu_specialization_root_t where id in (" +
            " select s.id from" +
            "  edu_specialization_base_t s" +
            "  inner join edu_c_pr_subject_t p on p.id = s.programSubject_id " +
            "  inner join edu_c_pr_subject_index_t i on i.id=p.subjectIndex_id " +
            "  inner join edu_c_program_kind_t k on k.id = i.programKind_id " +
            "  inner join c_edu_level_t l on k.eduLevel_id = l.id" +
            " where l.code_p = ?" +
            ")", "2013.2.1");

        tool.executeUpdate("delete from edu_specialization_child_t where id in (" +
            " select s.id from" +
            "  edu_specialization_base_t s" +
            "  inner join edu_c_pr_subject_t p on p.id = s.programSubject_id " +
            "  inner join edu_c_pr_subject_index_t i on i.id=p.subjectIndex_id " +
            "  inner join edu_c_program_kind_t k on k.id = i.programKind_id " +
            "  inner join c_edu_level_t l on k.eduLevel_id = l.id" +
            " where l.code_p = ?" +
            ")", "2013.2.1");

        tool.executeUpdate("delete from edu_specialization_base_t where " +
            " id not in (select id from edu_specialization_root_t) and" +
            " id not in (select id from edu_specialization_child_t)");

    }
}