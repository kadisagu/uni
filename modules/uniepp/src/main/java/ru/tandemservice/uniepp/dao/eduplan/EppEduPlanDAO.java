package ru.tandemservice.uniepp.dao.eduplan;

import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.gen.EducationOrgUnitGen;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanGen;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionGen;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.gen.EppStudent2EduPlanVersionGen;
import ru.tandemservice.uniepp.util.WeekTypeLegendRow;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EppEduPlanDAO extends UniBaseDao implements IEppEduPlanDAO
{


    protected static EppEduPlan check_editable(final EppEduPlan eduPlan) throws IllegalStateException {
        final EppState state = (null == eduPlan ? null : eduPlan.getState());
        if (null != state) { state.check_editable(eduPlan); }
        return eduPlan;
    }

    protected static EppEduPlanVersion check_editable(final EppEduPlanVersion eduPlan) throws IllegalStateException {
        final EppState state = (null == eduPlan ? null : eduPlan.getState());
        if (null != state) { state.check_editable(eduPlan); }
        return eduPlan;
    }

    @Override
    public Collection<IEntity> getSecLocalEntities(final EppEduPlan eppEduPlan) {

        if (! (eppEduPlan instanceof EppEduPlanProf)) {
            return Collections.singletonList((IEntity) eppEduPlan.getOwner());
        }

        EppEduPlanProf prof = (EppEduPlanProf) eppEduPlan;

        final DQLSelectBuilder secOuDql = new DQLSelectBuilder()
            .fromEntity(OrgUnit.class, "ou").column("ou.id");

        secOuDql.fromEntity(EducationOrgUnit.class, "eduOU");
        secOuDql.where(or(
            eq(property("eduOU", EducationOrgUnitGen.groupOrgUnit().id()), property("ou", "id")),
            eq(property("eduOU", EducationOrgUnitGen.operationOrgUnit().id()), property("ou", "id")),
            eq(property("eduOU", EducationOrgUnitGen.formativeOrgUnit().id()), property("ou", "id")),
            eq(property("eduOU", EducationOrgUnitGen.territorialOrgUnit().id()), property("ou", "id")),
            eq(property("eduOU", EducationOrgUnitGen.educationLevelHighSchool().orgUnit().id()), property("ou", "id"))
        ));

        secOuDql.fromEntity(EppEduPlan.class, "eppEduPlan");
        secOuDql.where(eq(property("eppEduPlan", "id"), value(eppEduPlan.getId())));
        secOuDql.where(eq(property("eppEduPlan", EppEduPlanGen.programForm()), property("eduOU", EducationOrgUnitGen.developForm().programForm())));
        secOuDql.where(eq(property("eppEduPlan", EppEduPlanGen.developCondition()), property("eduOU", EducationOrgUnitGen.developCondition())));
        if (eppEduPlan.getProgramTrait() != null) {
            secOuDql.where(eq(property("eppEduPlan", EppEduPlanGen.programTrait()), property("eduOU", EducationOrgUnitGen.developTech().programTrait())));
        } else {
            secOuDql.where(isNull(property("eduOU", EducationOrgUnitGen.developTech().programTrait())));
        }
        secOuDql.where(eq(property("eppEduPlan", EppEduPlanGen.developGrid().developPeriod()), property("eduOU", EducationOrgUnitGen.developPeriod())));

        secOuDql.where(eq(property("eduOU", EducationOrgUnitGen.educationLevelHighSchool().educationLevel().eduProgramSubject()), value(prof.getProgramSubject())));

        final DQLSelectBuilder orgUnits = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou");
        orgUnits.where(in(property("ou", "id"), secOuDql.buildQuery()));

        final Collection<IEntity> result = new HashSet<>(orgUnits.createStatement(getSession()).<OrgUnit>list());

        {
            final OrgUnit ou = eppEduPlan.getOwner();
            if (null != ou) { result.add(ou); }
        }

        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    public INumberGenerationRule<EppEduPlan> getEduPlanNumberGenerationRule() {
        return new SimpleNumberGenerationRule<EppEduPlan>() {
            @Override public Set<String> getUsedNumbers(final EppEduPlan object) {
                final List list = EppEduPlanDAO.this.getSession().createCriteria(EppEduPlan.class).setProjection(Projections.property(EppEduPlanGen.number().toString())).list();
                return new HashSet<>(list);
            }
            @Override public String getNumberQueueName(final EppEduPlan object) {
                return EppEduPlan.class.getSimpleName();
            }
        };
    }

    @Override
    @SuppressWarnings("unchecked")
    public INumberGenerationRule<EppEduPlanVersion> getEduPlanVersionNumberGenerationRule() {
        return new SimpleNumberGenerationRule<EppEduPlanVersion>() {
            @Override public Set<String> getUsedNumbers(final EppEduPlanVersion object) {
                final List list = EppEduPlanDAO.this.getSession().createCriteria(ClassUtils.getUserClass(object))
                .add(Restrictions.eq(EppEduPlanVersionGen.eduPlan().toString(), object.getEduPlan()))
                .setProjection(Projections.property(EppEduPlanVersionGen.number().toString())).list();
                return new HashSet<>(list);
            }
            @Override public String getNumberQueueName(final EppEduPlanVersion object) {
                return ClassUtils.getUserClass(object).getSimpleName()+"."+object.getEduPlan().getId();
            }
        };
    }

    @Override
    public List<WeekTypeLegendRow> getWeekTypeLegendRowList(Collection<EppWeekType> weekTypeCollection)
    {
        final List<WeekTypeLegendRow> list = new ArrayList<>();
        long id = 0;
        final int COLUMN_COUNT = 4;

        if (null == weekTypeCollection)
        {
            weekTypeCollection = this.getCatalogItemListOrderByCode(EppWeekType.class);
        }

        for (final Iterator<EppWeekType> iter = weekTypeCollection.iterator(); iter.hasNext();)
        {
            final String[] shortTitle = new String[COLUMN_COUNT];
            final String[] title = new String[COLUMN_COUNT];
            for (int i = 0; i < COLUMN_COUNT; i++)
            {
                final EppWeekType weekType = iter.hasNext() ? iter.next() : null;
                if (null != weekType) {
                    String aV = StringUtils.trimToEmpty(weekType.getAbbreviationView());
                    String aE = StringUtils.trimToEmpty(weekType.getAbbreviationEdit());
                    if (aV.equals(aE)) {
                        shortTitle[i] = aV;
                    } else {
                        shortTitle[i] = aV+" ("+aE+")";
                    }
                    title[i] = weekType.getShortTitle();
                }
            }
            list.add(new WeekTypeLegendRow(id++, shortTitle, title));
        }
        return list;
    }

    @Override
    public void doUpdateBlockList(final Long eduPlanVersionId, final Map<EduProgramSpecialization, EduOwnerOrgUnit> specMap)
    {
        final Session session = this.getSession();
        final EppEduPlanVersion eppPlanVersion = EppEduPlanDAO.check_editable(this.getNotNull(EppEduPlanVersion.class, eduPlanVersionId));

        // блокируем УП
        NamedSyncInTransactionCheckLocker.register(session, eduPlanVersionId.toString());

        // создаем корневой блок
        EppEduPlanVersionRootBlock rootBlock = get(EppEduPlanVersionRootBlock.class, EppEduPlanVersionRootBlock.eduPlanVersion().id(), eduPlanVersionId);
        if (null == rootBlock) {
            rootBlock = new EppEduPlanVersionRootBlock();
            rootBlock.setEduPlanVersion(eppPlanVersion);
            save(rootBlock);
        }

        // берем ранее созданные блоки

        // дисциплина -> соответсвие блок
        final Map<EduProgramSpecialization, EppEduPlanVersionSpecializationBlock> blockMap = new HashMap<>();
        for (final EppEduPlanVersionSpecializationBlock block : getList(EppEduPlanVersionSpecializationBlock.class, EppEduPlanVersionSpecializationBlock.eduPlanVersion(), eppPlanVersion)) {
            blockMap.put(block.getProgramSpecialization(), block);
        }

        // новые блоки
        for (final Map.Entry<EduProgramSpecialization, EduOwnerOrgUnit> entry : specMap.entrySet()) {
            EppEduPlanVersionSpecializationBlock block = blockMap.remove(entry.getKey());
            if (block == null) {
                block = new EppEduPlanVersionSpecializationBlock();
                block.setEduPlanVersion(eppPlanVersion);
                block.setProgramSpecialization(entry.getKey());
            }
            block.setOwnerOrgUnit(entry.getValue());
            session.saveOrUpdate(block);
        }

        // удаляем оставшиеся
        for (final Map.Entry<EduProgramSpecialization, EppEduPlanVersionSpecializationBlock> entry : blockMap.entrySet()) {
            session.delete(entry.getValue());
        }
    }

    @Override
    public Map<Long, EppStudent2EduPlanVersion> getActiveStudentEduplanVersionRelationMap(final Collection<Long> fullStudentIds) {
        final Map<Long, EppStudent2EduPlanVersion> result = new HashMap<>(fullStudentIds.size());
        for (List<Long> idsPart : Iterables.partition(fullStudentIds, DQL.MAX_VALUES_ROW_NUMBER)) {
            final MQBuilder relBuilder = new MQBuilder(EppStudent2EduPlanVersionGen.ENTITY_NAME, "rel");
            relBuilder.add(MQExpression.in("rel", EppStudent2EduPlanVersionGen.student().id().s(), idsPart));
            relBuilder.add(MQExpression.isNull("rel", EppStudent2EduPlanVersionGen.removalDate().s()));
            relBuilder.addDescOrder("rel", EppStudent2EduPlanVersionGen.confirmDate().s());
            relBuilder.addDescOrder("rel", EntityBase.id().s());
            final List<EppStudent2EduPlanVersion> relList = relBuilder.getResultList(this.getSession());
            for (final EppStudent2EduPlanVersion rel: relList) {
                if (null != result.put(rel.getStudent().getId(), rel)) {
                    EppEduPlanDAO.this.logger.warn("More then one eppEduPlanVersion is connected with student: " + rel.getStudent().getFullTitle());
                }
            }
        }
        return result;
    }

    @Override
    public EppStudent2EduPlanVersion getActiveStudentEduPlanVersionRelation(final Long studentId) {
        return this.getActiveStudentEduplanVersionRelationMap(Collections.singleton(studentId)).get(studentId);
    }

    @Override
    public EppEduPlanVersionBlock getActiveStudentEduPlanVersionBlock(Long studentId) {
        return new DQLSelectBuilder()
        .fromEntity(Student.class, "s")
        .where(eq(property("s.id"), value(studentId)))
        .fromEntity(EppStudent2EduPlanVersion.class, "s2epv")
        .where(eq(property(EppStudent2EduPlanVersion.student().fromAlias("s2epv")), property("s")))
        .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
        .column(property("s2epv", EppStudent2EduPlanVersion.block()))
        .order(property(EppStudent2EduPlanVersion.confirmDate().fromAlias("s2epv")))
        .order(property(EppStudent2EduPlanVersion.id().fromAlias("s2epv")))
        .createStatement(getSession()).setMaxResults(1).uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void doUpdateStudentEduPlanVersion(Collection<StudentEduPlanData> data) {

        if (data.isEmpty()) { return; }

        final Session session = this.getSession();
        final Date now = new Date();

        // блокируем студентов
        final Collection<Long> studentIds = data.stream().map(StudentEduPlanData::getStudentId).distinct().collect(Collectors.toList());
        NamedSyncInTransactionCheckLocker.register(session, studentIds);

        for (List<StudentEduPlanData> elements : Iterables.partition(data, DQL.MAX_VALUES_ROW_NUMBER)) {

            final Collection<Long> ids = elements.stream().map(StudentEduPlanData::getStudentId).distinct().collect(Collectors.toList());
            final Map<INaturalId, EppStudent2EduPlanVersion> relationMap = getList(EppStudent2EduPlanVersion.class, EppStudent2EduPlanVersion.student().id().s(), ids)
                    .stream().collect(Collectors.toMap(EppStudent2EduPlanVersionGen::getNaturalId, Function.<EppStudent2EduPlanVersion>identity()));

            for (final StudentEduPlanData element : elements) {
                if (null == element.getVersionId()) {
                    continue;
                }
                final EppEduPlanVersion version = (EppEduPlanVersion) session.load(EppEduPlanVersion.class, element.getVersionId());
                final Student student = (Student) session.load(Student.class, element.getStudentId());
                final INaturalId key = new EppStudent2EduPlanVersionGen.NaturalId(student, version);

                final Long blockId = element.getBlockId();
                final EppEduPlanVersionBlock block = blockId != null ? (EppEduPlanVersionBlock) session.load(EppEduPlanVersionBlock.class, blockId) : null;

                final Long customPlanId = element.getCustomEduPlanId();
                final EppCustomEduPlan customEduPlan = customPlanId != null ? (EppCustomEduPlan) session.load(EppCustomEduPlan.class, customPlanId) : null;

                EppStudent2EduPlanVersion rel = relationMap.remove(key);
                if (null == rel) {
                    rel = new EppStudent2EduPlanVersion(student, version);
                }

                rel.setBlock(block);
                rel.setCustomEduPlan(customEduPlan);
                rel.setRemovalDate(null);
                session.saveOrUpdate(rel);
            }

            for (final EppStudent2EduPlanVersion rel: relationMap.values()) {
                if (null == rel.getRemovalDate()) {
                    rel.setRemovalDate(now);
                }
                session.saveOrUpdate(rel);
            }
        }

        session.flush();
    }


}
