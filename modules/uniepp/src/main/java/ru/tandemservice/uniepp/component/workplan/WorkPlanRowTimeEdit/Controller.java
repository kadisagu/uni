package ru.tandemservice.uniepp.component.workplan.WorkPlanRowTimeEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author vdanilov
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    @SuppressWarnings("unchecked")
    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final ErrorCollector errorCollector = component.getUserContext().getErrorCollector();


        if (errorCollector.hasErrors())
        {
            return;
        }

        this.getDao().update(model);
        this.deactivate(component);
    }
}
