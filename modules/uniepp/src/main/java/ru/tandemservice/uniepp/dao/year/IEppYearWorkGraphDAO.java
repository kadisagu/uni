package ru.tandemservice.uniepp.dao.year;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Операции с ГУП (в рамках ПУПнаГ)
 *
 * @author vdanilov
 */
public interface IEppYearWorkGraphDAO
{
    public static final SpringBeanCache<IEppYearWorkGraphDAO> instance = new SpringBeanCache<IEppYearWorkGraphDAO>(IEppYearWorkGraphDAO.class.getName());


    /**
     * @param workGraph ГУП
     * @param subjectSet выбранные направления подготовки
     * @return { eppWorkGraphRow2EduPlan.id | в УП(в) есть соответствующее направление }
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Set<Long> getFilteredEduPlanRowIds(EppWorkGraph workGraph, Collection<EduProgramSubject> subjectSet);

    /**
     * Получает все данные для ГУП с группировкой по направлению подготовки (специальности)
     *
     * @param workGraph     ГУП
     * @param courseSet     Фильтр курс
     * @param highSchoolSet Фильтр направление подготовки (специальность)
     * @return УО -> строка ГУП -> неделя ГУП -> тип недели
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<EduProgramSubject, Map<EppWorkGraphRow, Map<EppYearEducationWeek, EppWeekType>>> getWorkGraphMapGroupByProgramSubject(EppWorkGraph workGraph, Collection<Course> courseSet, Collection<EduProgramSubject> highSchoolSet);


    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<EduProgramSubject, Map<EppWorkGraphRow, Set<EppWorkGraphRowWeek>>> getWorkGraphRowMapGroupByProgramSubject(EppWorkGraph workGraph, Collection<Course> courseSet, Collection<EduProgramSubject> highSchoolSet);

    /**
     * Обновляет строку ГУП после ее редактирования
     *
     * @param editRowId редактируемая строка ГУП (EppWorkGraphRow)
     * @param dataMap   данные из RangeSelectionListDataSource
     * @param ranges    номера диапазонов из RangeSelectionListDataSource
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateWorkGraphRow(Long editRowId, Map<PairKey<Long, Long>, EppWeekType> dataMap, int[] ranges);

    /**
     * Объединяет выбранные строки ГУП на базе шаблона
     *
     * @param templateRowId шаблон строки ГУП (EppWorkGraphRow)
     * @param selectedIds   выбранные строки ГУП (EppWorkGraphRow)
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateCombineRows(Long templateRowId, Set<Long> selectedIds);

    /**
     * Выделяет версию УП из строки ГУП в отдельную строку ГУП
     *
     * @param excludeRowId версия УП которую надо вынести в отдельную строку ГУП (EppWorkGraphRow2EduPlan)
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateExcludeRow(Long excludeRowId);
}
