/* $Id$ */
package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Nikolay Fedorovskih
 * @since 15.10.2014
 */
public class MS_uniepp_2x6x9_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.9")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.getStatement().execute(
                "select count(*) from epp_eduplan_verblock_s_t t join epp_eduplan_verblock_t b on t.id=b.id " +
                        "where exists(" +
                            "select * from epp_eduplan_verblock_s_t t2 join epp_eduplan_verblock_t b2 on t2.id=b2.id " +
                            "where t2.id<>t.id and b2.eduPlanVersion_id=b.eduPlanVersion_id and t2.programSpecialization_id=t.programSpecialization_id" +
                        ")"
        );

        if (tool.getStatement().getResultSet().next() && tool.getStatement().getResultSet().getLong(1) > 0)
        {
            throw new IllegalStateException("В базе данных есть УП(в) с блоками на одну и ту же направленность. Это должно быть исправленно в версии 2.6.8 - см. " +
                                                    "системное действие «Проверить корректность данных для версии 2.6.9.»");
        }
    }
}