package ru.tandemservice.uniepp.dao.registry;

import java.util.Collection;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;

import ru.tandemservice.uniepp.entity.registry.EppRegistryModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleIControlAction;

/**
 * @author vdanilov
 */
public interface IEppRegistryModuleDAO {
    public static final SpringBeanCache<IEppRegistryModuleDAO> instance = new SpringBeanCache<IEppRegistryModuleDAO>(IEppRegistryModuleDAO.class.getName());

    /** @return правило генерации номеров элементов реестра модулей */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    INumberGenerationRule<EppRegistryModule> getNumberGenerationRule();

    /**
     * Сохраняет данные модуля
     * @param element модуль
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSaveRegistryModule(EppRegistryModule element);

    /**
     * Сохраняет данные по видам аудиторной нагрузки
     * @param element
     * @param loadList (null игнорируются)
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    Collection<EppRegistryModuleALoad> doSaveRegistryModuleLoad(EppRegistryModule element, Collection<EppRegistryModuleALoad> loadList);

    /**
     * Сохраняет данные по формам контроля
     * @param element
     * @param controlActionList (null игнорируются)
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    Collection<EppRegistryModuleIControlAction> doSaveRegistryModuleControl(EppRegistryModule element, Collection<EppRegistryModuleIControlAction> controlActionList);
}
