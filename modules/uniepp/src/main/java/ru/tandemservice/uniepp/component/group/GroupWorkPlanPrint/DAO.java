package ru.tandemservice.uniepp.component.group.GroupWorkPlanPrint;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.gen.YearDistributionPartGen;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearEducationProcessGen;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.gen.EppStudent2WorkPlanGen;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    @Override
    public void prepare(final Model model) {

        model.getDataMap().clear();
        BatchUtils.execute(model.getIds(), 100, new BatchUtils.Action<Long>() {
            @Override public void execute(final Collection<Long> studentIds)
            {
                final List<EppStudent2WorkPlan> relations = new DQLSelectBuilder()
                .fromEntity(EppStudent2WorkPlan.class, "s2wp").column(property("s2wp"))
                .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias("s2wp"), "wp")
                .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("s2wp"), "s2epv")
                .fetchPath(DQLJoinType.inner, EppStudent2EduPlanVersion.student().fromAlias("s2epv"), "s")
                .where(isNull(property(EppStudent2WorkPlanGen.removalDate().fromAlias("s2wp"))))
                .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
                .where(in(property(Student.id().fromAlias("s")), studentIds))
                .createStatement(DAO.this.getSession()).list();

                for (final EppStudent2WorkPlan rel: relations)
                {
                    final Student student = rel.getStudentEduPlanVersion().getStudent();
                    final Group group = student.getGroup();
                    final Map<Long, Map<Integer, EppStudent2WorkPlan>> byGroup = SafeMap.safeGet(model.getDataMap(), (null == group ? 0L : group.getId()), HashMap.class);
                    final Map<Integer, EppStudent2WorkPlan> byStudent = SafeMap.safeGet(byGroup, student.getId(), HashMap.class);

                    final int termNumber = rel.getWorkPlan().getGridTerm().getTermNumber();
                    if (null != byStudent.put(termNumber, rel)) {
                        UserContext.getInstance().getErrorCollector().add("У студента "+student.getFullTitle()+" указано несколько РУП на семестр №" + termNumber+".");
                        // ошибка, но поидее можно и дальше продолжить - так что исключение не фигачем
                    }
                }
            }
        });

        // все года, по которым есть РУП
        model.getYearModel().setSource(new UniQueryFullCheckSelectModel() {
            @Override protected MQBuilder query(final String alias, final String filter)
            {
                final Set<Long> eppYearIds = new HashSet<Long>();
                for (final Map<Long, Map<Integer, EppStudent2WorkPlan>> byGroup: model.getDataMap().values()) {
                    for (final Map<Integer, EppStudent2WorkPlan> byStudent: byGroup.values()) {
                        for (final EppStudent2WorkPlan rel: byStudent.values()) {
                            eppYearIds.add(rel.getWorkPlan().getYear().getId());
                        }
                    }
                }

                // билдеры нужны, чтобы грузить объекты из базы каждый раз
                final MQBuilder subBuilder = new MQBuilder(EppYearEducationProcessGen.ENTITY_CLASS, "eppy", new String[] { EppYearEducationProcessGen.educationYear().id().s() });
                subBuilder.add(MQExpression.in("eppy", "id", eppYearIds));

                final MQBuilder builder = new MQBuilder(EducationYear.ENTITY_CLASS, alias);
                builder.add(MQExpression.in(alias, "id", subBuilder));
                builder.addOrder(alias, EducationYear.intValue().s());
                return builder;
            }
        });

        // части лет
        model.getPartModel().setSource(new UniQueryFullCheckSelectModel() {
            @Override protected MQBuilder query(final String alias, final String filter)
            {
                if (null == model.getYearModel().getValue()) { return null; }

                final Set<Long> partIds = new HashSet<Long>();
                for (final Map<Long, Map<Integer, EppStudent2WorkPlan>> byGroup: model.getDataMap().values()) {
                    for (final Map<Integer, EppStudent2WorkPlan> byStudent: byGroup.values()) {
                        for (final EppStudent2WorkPlan rel: byStudent.values()) {
                            partIds.add(rel.getWorkPlan().getGridTerm().getPart().getId());
                        }
                    }
                }

                // билдеры нужны, чтобы грузить объекты из базы каждый раз
                final MQBuilder builder = new MQBuilder(YearDistributionPartGen.ENTITY_CLASS, alias);
                builder.add(MQExpression.in(alias, "id", partIds));
                builder.addOrder(alias, YearDistributionPartGen.yearDistribution().amount().s());
                builder.addOrder(alias, YearDistributionPartGen.number().s());
                return builder;
            }
        });
    }


}
