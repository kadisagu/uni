package ru.tandemservice.uniepp.component.edustd.EduStdImport;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="id")
})
public class Model
{
    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    private IUploadFile uploadFile;
    public IUploadFile getUploadFile() { return this.uploadFile; }
    public void setUploadFile(final IUploadFile uploadFile) { this.uploadFile = uploadFile; }
}
