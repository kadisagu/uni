package ru.tandemservice.uniepp.component.registry.RegElementPartEdit;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {
    void save(Model model);
}
