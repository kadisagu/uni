package ru.tandemservice.uniepp.component.registry.base;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;

import ru.tandemservice.uniepp.component.base.SimpleOrgUnitBasedModel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author vdanilov
 */
public abstract class ModelBase<T extends EppRegistryElement> extends SimpleOrgUnitBasedModel {

    public abstract Class<T> getElementClass();
    public abstract String getPermissionPrefix();

    private CommonPostfixPermissionModel sec = new CommonPostfixPermissionModel("xxx"); //todo заглушка
    public CommonPostfixPermissionModel getSec() { return this.sec; }
    public void setSec(final CommonPostfixPermissionModel sec) { this.sec = sec; }

    public CommonPostfixPermissionModelBase setupPermissionModel()
    {
        String postfix = this.getPermissionPrefix();
        if (this.getOrgUnit() != null) {
            postfix = postfix + "_" + StringUtils.uncapitalize(this.getOrgUnit().getOrgUnitType().getCode());
        }
        this.setSec(new CommonPostfixPermissionModel(postfix));
        return this.getSec();
    }

}
