/* $Id$ */
package ru.tandemservice.uniepp.base.bo.CtrReceiptPaymentOrder.logic;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.ctr.base.bo.CtrPaymentPromice.logic.ICtrPaymentOrderPrinter;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentOrder;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.uniepp.base.bo.CtrReceiptPaymentOrder.CtrReceiptPaymentOrderManager;
import ru.tandemservice.uniepp.entity.catalog_ctr.EppCtrDocTemplate;
import ru.tandemservice.uniepp.entity.catalog_ctr.codes.EppCtrDocTemplateCodes;


/**
 * @author Dmitry Seleznev
 * @since 02.12.2011
 */
public class CtrReceiptPaymentOrderPrinter implements ICtrPaymentOrderPrinter
{
    @SuppressWarnings("unused")
    @Override
    public RtfDocument print(final CtrPaymentOrder order)
    {
        final ITemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EppCtrDocTemplate.class, EppCtrDocTemplateCodes.IZVETSHENIE_DLYA_DOGOVORA_NA_OBUCHENIE); // TODO
        final RtfDocument template = new RtfReader().read(templateDocument.getContent());
        final RtfInjectModifier modifier = new RtfInjectModifier();

        final TopOrgUnit academy = TopOrgUnit.getInstance(true);
        modifier.put("organizationShortTitle", academy.getShortTitle());
        modifier.put("inn", academy.getInn());
        modifier.put("kpp", academy.getKpp());
        modifier.put("bik", academy.getBic());
        modifier.put("bankTitle", academy.getBank());
        modifier.put("bankBranch", academy.getBankBranch());
        modifier.put("settlementAccount", academy.getCurAccount());
        modifier.put("personalAccount", academy.getPerAccount());
        modifier.put("correspondentAccount", academy.getCorAccount());
        modifier.put("accountingCode", null != order.getContract().getOrgUnit() ? order.getContract().getOrgUnit().getAccountingCode() : "");
        modifier.put("bankSettlement", ((null != academy.getAddress()) && (null != academy.getAddress().getSettlement())) ? academy.getAddress().getSettlement().getTitleWithType() : "");

        modifier.put("paymentOrderNumber", order.getOrderNumber());
        modifier.put("paymentOrderIssuanceDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getOrderDate()));
        modifier.put("deadlineDateMonthStr", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(order.getTimestamp()));

        modifier.put("payerFio", order.getSrc().getPerson().getFullFio());

        final Student student = CtrReceiptPaymentOrderManager.instance().dao().getSingleStudent(order.getContract());
        if (null == student)
        {
            modifier.put("studentFio", "");
            modifier.put("studentPersonalNumber", "");
        }
        else
        {
            modifier.put("studentFio", student.getPerson().getFullFio());
            modifier.put("studentPersonalNumber", student.getPersonalNumber());
        }

        modifier.put("paymentPurpose", order.getComment());

        //        final boolean negSum = order.getCostAsDouble() < 0;
        final Double absoluteSum = Math.abs(order.getCostAsDouble());
        final long rub = absoluteSum.longValue();
        final long kop = Math.round((absoluteSum - rub) * 100);
        modifier.put("sumInteger", String.valueOf(rub));
        modifier.put("sumFractionalPart", (kop < 10 ? "0" : "") + String.valueOf(kop));
        modifier.put("sumValueSpelledOut", NumberConvertingUtil.writeSum(order.getCostAsLong(), 4, order.getCurrency().getCode()));

        modifier.modify(template);
        return template;
    }
}