/* $Id$ */
package ru.tandemservice.uniepp.ctrTemplate.ext.EduCtrTermEducationCostAgreementTemplate.logic;

import com.google.common.base.Preconditions;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.logic.EduCtrTermEducationCostAgreementTemplateDao;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrTermEducationCostAgreementTemplate.logic.IEduCtrTermEducationCostAgreementTemplateDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * @author azhebko
 * @since 04.12.2014
 */
public class EppEduCtrTermEducationCostAgreementTemplateDao extends EduCtrTermEducationCostAgreementTemplateDao implements IEduCtrTermEducationCostAgreementTemplateDao
{
    @Override
    public DevelopGrid getStudentDevelopGrid(Student student)
    {
        Preconditions.checkNotNull(student);
        EppStudent2EduPlanVersion studentEpv = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());
        return studentEpv == null ? null : studentEpv.getEduPlanVersion().getEduPlan().getDevelopGrid();
    }
}