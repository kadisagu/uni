package ru.tandemservice.uniepp.component.base;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.dao.UniDaoFacade;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key="orgUnitId", binding="orgUnitId"),
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="orgUnitId")
})
public abstract class SimpleOrgUnitBasedListModel<T extends IEntity> extends SimpleOrgUnitBasedModel {

    public Long getOrgUnitId() {
        return (null == this.getOrgUnit() ? null : this.getOrgUnit().getId());
    }
    public void setOrgUnitId(final Long orgUnitId) {
        final IEntityMeta meta = EntityRuntime.getMeta(orgUnitId);
        if ((null != meta) && OrgUnit.class.isAssignableFrom(meta.getEntityClass())) {
            this.setOrgUnit(UniDaoFacade.getCoreDao().get(OrgUnit.class, orgUnitId));
        } else {
            this.setOrgUnit(null);
        }
    }

    private OrgUnit orgUnit;
    @Override public OrgUnit getOrgUnit() { return this.orgUnit; }
    public void setOrgUnit(final OrgUnit orgUnit) { this.orgUnit = orgUnit; }

    private IDataSettings settings;
    public IDataSettings getSettings() { return this.settings; }
    public void setSettings(final IDataSettings settings) { this.settings = settings; }

    private DynamicListDataSource<T> dataSource;
    public DynamicListDataSource<T> getDataSource() { return this.dataSource; }
    public void setDataSource(final DynamicListDataSource<T> dataSource) { this.dataSource = dataSource; }
}
