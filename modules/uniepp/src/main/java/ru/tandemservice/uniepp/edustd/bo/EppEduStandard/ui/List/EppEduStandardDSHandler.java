/* $Id:$ */
package ru.tandemservice.uniepp.edustd.bo.EppEduStandard.ui.List;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniepp.edustd.bo.EppEduStandard.EppEduStandardManager;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/10/14
 */
public class EppEduStandardDSHandler extends DefaultSearchDataSourceHandler
{
    public EppEduStandardDSHandler(String ownerID)
    {
        super(ownerID);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {

        DQLOrderDescriptionRegistry orderDescription = new DQLOrderDescriptionRegistry(EppStateEduStandard.class, "p");
        DQLSelectBuilder dql = orderDescription.buildDQLSelectBuilder();

        IDataSettings settings = context.get("settings");
        EduProgramKind programKind = settings.get(EppEduStandardManager.BIND_PROGRAM_KIND);
        EppGeneration generation = context.get(EppEduStandardManager.BIND_GENERATION);

        if (EppGeneration.GENERATION_2.equals(generation)) {
            dql.where(likeUpper(property("p", EppStateEduStandard.programSubject().subjectIndex().code()), value(CoreStringUtils.escapeLike("2005"))));
        } else if (EppGeneration.GENERATION_3.equals(generation)){
            dql.where(not(likeUpper(property("p", EppStateEduStandard.programSubject().subjectIndex().code()), value(CoreStringUtils.escapeLike("2005")))));
        }

        FilterUtils.applySimpleLikeFilter(dql, "p", EppStateEduStandard.number(), settings.<String>get("number"));
        FilterUtils.applySelectFilter(dql, "p", EppStateEduStandard.programSubject().subjectIndex().programKind(), programKind);
        FilterUtils.applySelectFilter(dql, "p", EppStateEduStandard.programSubject(), settings.get("programSubject"));
        FilterUtils.applySelectFilter(dql, "p", EppStateEduStandard.state(), settings.get("state"));

        orderDescription.applyOrder(dql, input.getEntityOrder());

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();
    }

}


