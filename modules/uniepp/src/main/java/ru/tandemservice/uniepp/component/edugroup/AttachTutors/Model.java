package ru.tandemservice.uniepp.component.edugroup.AttachTutors;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author vdanilov
 */
@Input({
        @Bind(key="ids", binding="ids"),
        @Bind(key="levelId", binding="levelHolder.id")
})
public class Model
{
    private PpsEntrySelectBlockData ppsData;

    private Collection<Long> ids = Collections.emptyList();
    public Collection<Long> getIds() { return this.ids; }
    public void setIds(final Collection<Long> ids) { this.ids = ids; }

    private final EntityHolder<EppRealEduGroupCompleteLevel> levelHolder = new EntityHolder<EppRealEduGroupCompleteLevel>();
    public EntityHolder<EppRealEduGroupCompleteLevel> getLevelHolder() { return this.levelHolder; }
    public EppRealEduGroupCompleteLevel getLevel() { return this.getLevelHolder().refresh(EppRealEduGroupCompleteLevel.class); }

    private final SelectModel<List<PpsEntry>> ppsSelectModel = new SelectModel<List<PpsEntry>>();
    public SelectModel<List<PpsEntry>> getPpsSelectModel() { return this.ppsSelectModel; }

    public PpsEntrySelectBlockData getPpsData()
    {
        return ppsData;
    }

    public void setPpsData(PpsEntrySelectBlockData ppsData)
    {
        this.ppsData = ppsData;
    }

    public List<PpsEntry> getPpsList()
    {
        return getPpsData().getSelectedPpsList();
    }

    public void setPpsList(final List<PpsEntry> ppsList)
    {
        getPpsData().setSelectedPps(ppsList);
    }

}
