/* $Id:$ */
package ru.tandemservice.uniepp.catalog.bo.EppIControlActionType;

import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.springframework.context.annotation.Configuration;

/**
 * @author rsizonenko
 * @since 10.11.2015
 */
@Configuration
public class EppIControlActionTypeManager extends BusinessObjectManager {

    public static EppIControlActionTypeManager instance() {
        return instance(EppIControlActionTypeManager.class);
    }

}
