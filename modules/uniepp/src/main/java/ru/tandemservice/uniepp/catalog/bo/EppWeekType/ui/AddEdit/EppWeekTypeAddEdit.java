package ru.tandemservice.uniepp.catalog.bo.EppWeekType.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author avedernikov
 * @since 21.09.2015
 */

@Configuration
public class EppWeekTypeAddEdit extends BusinessComponentManager
{
	@Override
	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return this.presenterExtPointBuilder()
				.create();
	}
}