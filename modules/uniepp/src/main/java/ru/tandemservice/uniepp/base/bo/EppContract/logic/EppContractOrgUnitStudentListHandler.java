package ru.tandemservice.uniepp.base.bo.EppContract.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * @author vdanilov
 */
public class EppContractOrgUnitStudentListHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    protected final DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(Student.class, "s");

    public static final String PARAM_ORGUNIT = "orgUnit";

    public static final String PARAM_PERSON_LAST_NAME = "personLastName";
    public static final String PARAM_PERSONAL_NUMBER = "personalNumber";
    public static final String PARAM_PERSONAL_FILE_NUMBER = "personalFileNumber";
    public static final String PARAM_BOOK_NUMBER = "bookNumber";

    public static final String PARAM_STUDENT_CATEGORY = "studentCategory";
    public static final String PARAM_STUDENT_STATUS = "studentStatus";
    public static final String PARAM_COURSE = "course";
    public static final String PARAM_DEVELOP_FORM = "developForm";
    public static final String PARAM_DEVELOP_CONDITION = "developCondition";
    public static final String PARAM_DEVELOP_PERIOD = "developPeriod";


    /* list{ EppCtrEducationResult } */
    public static final String P_CONTRACT_RESULT_LIST = "contractEducationResultList";


    public EppContractOrgUnitStudentListHandler(final String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(final DSInput input, final ExecutionContext context)
    {
        final Session session = context.getSession();
        final OrgUnit orgUnit = context.get(EppContractOrgUnitStudentListHandler.PARAM_ORGUNIT);

        final DQLSelectBuilder dql = this.registry.buildDQLSelectBuilder().column(property("s"))
        .where(eq(property(Student.archival().fromAlias("s")), value(Boolean.FALSE)))

        .fetchPath(DQLJoinType.inner, Student.person().fromAlias("s"), "p")
        .fetchPath(DQLJoinType.inner, Person.identityCard().fromAlias("p"), "idc")
        .where(or(
            eq(property(Student.educationOrgUnit().formativeOrgUnit().fromAlias("s")), value(orgUnit)),
            eq(property(Student.educationOrgUnit().territorialOrgUnit().fromAlias("s")), value(orgUnit)),
            eq(property(Student.educationOrgUnit().educationLevelHighSchool().orgUnit().fromAlias("s")), value(orgUnit))
        ));

        FilterUtils.applySimpleLikeFilter(dql, "s", Student.P_PERSONAL_NUMBER, context.<String>get(EppContractOrgUnitStudentListHandler.PARAM_PERSONAL_NUMBER));
        FilterUtils.applySimpleLikeFilter(dql, "idc", IdentityCard.P_LAST_NAME, context.<String>get(EppContractOrgUnitStudentListHandler.PARAM_PERSON_LAST_NAME));
        FilterUtils.applySimpleLikeFilter(dql, "s", Student.P_PERSONAL_FILE_NUMBER, context.<String>get(EppContractOrgUnitStudentListHandler.PARAM_PERSONAL_FILE_NUMBER));
        FilterUtils.applySimpleLikeFilter(dql, "s", Student.P_BOOK_NUMBER, context.<String>get(EppContractOrgUnitStudentListHandler.PARAM_BOOK_NUMBER));

        FilterUtils.applySelectFilter(dql, Student.studentCategory().fromAlias("s"), context.get(EppContractOrgUnitStudentListHandler.PARAM_STUDENT_CATEGORY));
        FilterUtils.applySelectFilter(dql, Student.status().fromAlias("s"), context.get(EppContractOrgUnitStudentListHandler.PARAM_STUDENT_STATUS));
        FilterUtils.applySelectFilter(dql, Student.course().fromAlias("s"), context.get(EppContractOrgUnitStudentListHandler.PARAM_COURSE));
        FilterUtils.applySelectFilter(dql, Student.educationOrgUnit().developForm().fromAlias("s"), context.get(EppContractOrgUnitStudentListHandler.PARAM_DEVELOP_FORM));
        FilterUtils.applySelectFilter(dql, Student.educationOrgUnit().developCondition().fromAlias("s"), context.get(EppContractOrgUnitStudentListHandler.PARAM_DEVELOP_CONDITION));
        FilterUtils.applySelectFilter(dql, Student.educationOrgUnit().developPeriod().fromAlias("s"), context.get(EppContractOrgUnitStudentListHandler.PARAM_DEVELOP_PERIOD));

        final DSOutput output = DQLSelectOutputBuilder.get(input, dql, session).order(this.registry).build();
        final List<DataWrapper> wrappers = DataWrapper.wrap(output, DataWrapper.ID, DataWrapper.TITLE);
        BatchUtils.execute(wrappers, 256, new BatchUtils.Action<DataWrapper>() {
            @Override public void execute(final Collection<DataWrapper> wrappers)
            {
                final List<EppCtrEducationResult> studentContractList =  new DQLSelectBuilder()
                .fromEntity(EppCtrEducationResult.class, "r").column("r")
                .fetchPath(DQLJoinType.inner, EppCtrEducationResult.target().fromAlias("r"), "s2epv")
                .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv")))) // только активные связи
                .where(in(property(EppStudent2EduPlanVersion.student().id().fromAlias("s2epv")), CommonDAO.ids(wrappers)))
                .createStatement(session).list();

                final Map<Long, List<EppCtrEducationResult>> studentContractsMap = SafeMap.get(ArrayList.class);
                for (final EppCtrEducationResult r: studentContractList) {
                    studentContractsMap.get(r.getTarget().getStudent().getId()).add(r);
                }

                for (final DataWrapper w: wrappers) {
                    w.setProperty(
                        EppContractOrgUnitStudentListHandler.P_CONTRACT_RESULT_LIST,
                        Arrays.asList(studentContractsMap.get(w.getId()).toArray()) /* минимизация итоговой памяти + read-only список */
                    );
                }

                studentContractsMap.clear();
            }
        });


        return output;
    }


}
