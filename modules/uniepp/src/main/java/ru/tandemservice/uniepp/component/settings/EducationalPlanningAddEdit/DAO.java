package ru.tandemservice.uniepp.component.settings.EducationalPlanningAddEdit;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.dao.year.IEppYearDAO;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author nkokorina
 * @since 26.02.2010
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null != model.getEducationProcessId())
        {
            final EppYearEducationProcess yearEducationProcess = this.getNotNull(EppYearEducationProcess.class, model.getEducationProcessId());
            model.setYearEducationProcess(yearEducationProcess);
            model.setEducationYearList(new StaticSelectModel("id", "title", Collections.singleton(model.getYearEducationProcess().getEducationYear())));
        }
        else
        {
            model.setEducationYearList(new UniQueryFullCheckSelectModel()
            {
                @Override
                protected MQBuilder query(final String domainName, final String filter)
                {
                    final MQBuilder subBuilder = new MQBuilder(EppYearEducationProcess.ENTITY_CLASS, "eyep", new String[] { EppYearEducationProcess.educationYear().id().s() });

                    final MQBuilder bulder = new MQBuilder(EducationYear.ENTITY_CLASS, domainName);
                    bulder.add(MQExpression.notIn(domainName, EducationYear.id().s(), subBuilder));
                    bulder.addOrder(domainName, EducationYear.P_TITLE);
                    return bulder;
                }
            });

            model.setYearEducationProcess(new EppYearEducationProcess());

            // текущий учебный год
            final EducationYear educationYear = this.get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE);
            Integer maxYear = educationYear.getIntValue();

            final MQBuilder builder = new MQBuilder(EppYearEducationProcess.ENTITY_CLASS, "eyep", new String[] { EppYearEducationProcess.educationYear().toString() });
            builder.add(MQExpression.great("eyep", EppYearEducationProcess.educationYear().intValue().toString(), educationYear.getIntValue()));
            builder.addOrder("eyep", EppYearEducationProcess.educationYear().intValue().toString(), OrderDirection.desc);
            final List<EducationYear> years = builder.getResultList(this.getSession());

            if (!years.isEmpty())
            {
                maxYear = years.get(0).getIntValue();
            }

            final EducationYear year = this.get(EducationYear.class, EducationYear.P_INT_VALUE, maxYear + 1);
            model.getYearEducationProcess().setEducationYear(year);
            if (year != null)
            {
                model.getYearEducationProcess().setTitle(year.getTitle());
                model.getYearEducationProcess().setStartEducationDate(DAO.createDate(1, 9, year.getIntValue()));
            }
        }
    }

    @Override
    public void update(final Model model)
    {
        final EppYearEducationProcess yearEducationProcess = model.getYearEducationProcess();
        this.getSession().saveOrUpdate(yearEducationProcess);

        for (YearDistributionPart distributionPart : getList(YearDistributionPart.class, YearDistributionPart.yearDistribution().inUse(), true)) {
            if (!existsEntity(EppYearPart.class, EppYearPart.L_YEAR, yearEducationProcess, EppYearPart.L_PART, distributionPart)) {
                save(new EppYearPart(yearEducationProcess, distributionPart));
            }
        }

        IEppYearDAO.instance.get().doRefreshYearEducationWeeks(yearEducationProcess.getId());
    }

    private static Date createDate(final int day, final int month, final int year)
    {
        final Calendar calendar = Calendar.getInstance();
        calendar.clear();
        //noinspection MagicConstant
        calendar.set(year, month - 1, day);
        return calendar.getTime();
    }
}
