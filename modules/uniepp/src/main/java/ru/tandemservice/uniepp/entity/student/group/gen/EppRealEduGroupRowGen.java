package ru.tandemservice.uniepp.entity.student.group.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись студента в УГС
 *
 * Связь записи студента соответствующей с УГС
 * @warn ссылки на этот объект должны быть автоудаляемыми (т.к. неактуальная запись может быть удалена в любое время демоном)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRealEduGroupRowGen extends EntityBase
 implements INaturalIdentifiable<EppRealEduGroupRowGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow";
    public static final String ENTITY_NAME = "eppRealEduGroupRow";
    public static final int VERSION_HASH = 177726999;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_WPE_PART = "studentWpePart";
    public static final String L_GROUP = "group";
    public static final String P_CONFIRM_DATE = "confirmDate";
    public static final String P_REMOVAL_DATE = "removalDate";
    public static final String L_STUDENT_EDUCATION_ORG_UNIT = "studentEducationOrgUnit";
    public static final String P_STUDENT_GROUP_TITLE = "studentGroupTitle";

    private EppStudentWpePart _studentWpePart;     // Студент
    private EppRealEduGroup _group;     // Группа
    private Date _confirmDate;     // Дата утверждения
    private Date _removalDate;     // Дата утраты актуальности
    private EducationOrgUnit _studentEducationOrgUnit;     // НПП Студента
    private String _studentGroupTitle;     // Название группы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public EppStudentWpePart getStudentWpePart()
    {
        return _studentWpePart;
    }

    /**
     * @param studentWpePart Студент. Свойство не может быть null.
     */
    public void setStudentWpePart(EppStudentWpePart studentWpePart)
    {
        dirty(_studentWpePart, studentWpePart);
        _studentWpePart = studentWpePart;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public EppRealEduGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(EppRealEduGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Дата утверждения.
     */
    public Date getConfirmDate()
    {
        return _confirmDate;
    }

    /**
     * @param confirmDate Дата утверждения.
     */
    public void setConfirmDate(Date confirmDate)
    {
        dirty(_confirmDate, confirmDate);
        _confirmDate = confirmDate;
    }

    /**
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    /**
     * @return НПП Студента.
     */
    public EducationOrgUnit getStudentEducationOrgUnit()
    {
        return _studentEducationOrgUnit;
    }

    /**
     * @param studentEducationOrgUnit НПП Студента.
     */
    public void setStudentEducationOrgUnit(EducationOrgUnit studentEducationOrgUnit)
    {
        dirty(_studentEducationOrgUnit, studentEducationOrgUnit);
        _studentEducationOrgUnit = studentEducationOrgUnit;
    }

    /**
     * @return Название группы.
     */
    @Length(max=255)
    public String getStudentGroupTitle()
    {
        return _studentGroupTitle;
    }

    /**
     * @param studentGroupTitle Название группы.
     */
    public void setStudentGroupTitle(String studentGroupTitle)
    {
        dirty(_studentGroupTitle, studentGroupTitle);
        _studentGroupTitle = studentGroupTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRealEduGroupRowGen)
        {
            if (withNaturalIdProperties)
            {
                setStudentWpePart(((EppRealEduGroupRow)another).getStudentWpePart());
                setGroup(((EppRealEduGroupRow)another).getGroup());
            }
            setConfirmDate(((EppRealEduGroupRow)another).getConfirmDate());
            setRemovalDate(((EppRealEduGroupRow)another).getRemovalDate());
            setStudentEducationOrgUnit(((EppRealEduGroupRow)another).getStudentEducationOrgUnit());
            setStudentGroupTitle(((EppRealEduGroupRow)another).getStudentGroupTitle());
        }
    }

    public INaturalId<EppRealEduGroupRowGen> getNaturalId()
    {
        return new NaturalId(getStudentWpePart(), getGroup());
    }

    public static class NaturalId extends NaturalIdBase<EppRealEduGroupRowGen>
    {
        private static final String PROXY_NAME = "EppRealEduGroupRowNaturalProxy";

        private Long _studentWpePart;
        private Long _group;

        public NaturalId()
        {}

        public NaturalId(EppStudentWpePart studentWpePart, EppRealEduGroup group)
        {
            _studentWpePart = ((IEntity) studentWpePart).getId();
            _group = ((IEntity) group).getId();
        }

        public Long getStudentWpePart()
        {
            return _studentWpePart;
        }

        public void setStudentWpePart(Long studentWpePart)
        {
            _studentWpePart = studentWpePart;
        }

        public Long getGroup()
        {
            return _group;
        }

        public void setGroup(Long group)
        {
            _group = group;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppRealEduGroupRowGen.NaturalId) ) return false;

            EppRealEduGroupRowGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudentWpePart(), that.getStudentWpePart()) ) return false;
            if( !equals(getGroup(), that.getGroup()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudentWpePart());
            result = hashCode(result, getGroup());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudentWpePart());
            sb.append("/");
            sb.append(getGroup());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRealEduGroupRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRealEduGroupRow.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppRealEduGroupRow is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentWpePart":
                    return obj.getStudentWpePart();
                case "group":
                    return obj.getGroup();
                case "confirmDate":
                    return obj.getConfirmDate();
                case "removalDate":
                    return obj.getRemovalDate();
                case "studentEducationOrgUnit":
                    return obj.getStudentEducationOrgUnit();
                case "studentGroupTitle":
                    return obj.getStudentGroupTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentWpePart":
                    obj.setStudentWpePart((EppStudentWpePart) value);
                    return;
                case "group":
                    obj.setGroup((EppRealEduGroup) value);
                    return;
                case "confirmDate":
                    obj.setConfirmDate((Date) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
                case "studentEducationOrgUnit":
                    obj.setStudentEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "studentGroupTitle":
                    obj.setStudentGroupTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentWpePart":
                        return true;
                case "group":
                        return true;
                case "confirmDate":
                        return true;
                case "removalDate":
                        return true;
                case "studentEducationOrgUnit":
                        return true;
                case "studentGroupTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentWpePart":
                    return true;
                case "group":
                    return true;
                case "confirmDate":
                    return true;
                case "removalDate":
                    return true;
                case "studentEducationOrgUnit":
                    return true;
                case "studentGroupTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentWpePart":
                    return EppStudentWpePart.class;
                case "group":
                    return EppRealEduGroup.class;
                case "confirmDate":
                    return Date.class;
                case "removalDate":
                    return Date.class;
                case "studentEducationOrgUnit":
                    return EducationOrgUnit.class;
                case "studentGroupTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRealEduGroupRow> _dslPath = new Path<EppRealEduGroupRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRealEduGroupRow");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow#getStudentWpePart()
     */
    public static EppStudentWpePart.Path<EppStudentWpePart> studentWpePart()
    {
        return _dslPath.studentWpePart();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow#getGroup()
     */
    public static EppRealEduGroup.Path<EppRealEduGroup> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow#getConfirmDate()
     */
    public static PropertyPath<Date> confirmDate()
    {
        return _dslPath.confirmDate();
    }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    /**
     * @return НПП Студента.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow#getStudentEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> studentEducationOrgUnit()
    {
        return _dslPath.studentEducationOrgUnit();
    }

    /**
     * @return Название группы.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow#getStudentGroupTitle()
     */
    public static PropertyPath<String> studentGroupTitle()
    {
        return _dslPath.studentGroupTitle();
    }

    public static class Path<E extends EppRealEduGroupRow> extends EntityPath<E>
    {
        private EppStudentWpePart.Path<EppStudentWpePart> _studentWpePart;
        private EppRealEduGroup.Path<EppRealEduGroup> _group;
        private PropertyPath<Date> _confirmDate;
        private PropertyPath<Date> _removalDate;
        private EducationOrgUnit.Path<EducationOrgUnit> _studentEducationOrgUnit;
        private PropertyPath<String> _studentGroupTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow#getStudentWpePart()
     */
        public EppStudentWpePart.Path<EppStudentWpePart> studentWpePart()
        {
            if(_studentWpePart == null )
                _studentWpePart = new EppStudentWpePart.Path<EppStudentWpePart>(L_STUDENT_WPE_PART, this);
            return _studentWpePart;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow#getGroup()
     */
        public EppRealEduGroup.Path<EppRealEduGroup> group()
        {
            if(_group == null )
                _group = new EppRealEduGroup.Path<EppRealEduGroup>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow#getConfirmDate()
     */
        public PropertyPath<Date> confirmDate()
        {
            if(_confirmDate == null )
                _confirmDate = new PropertyPath<Date>(EppRealEduGroupRowGen.P_CONFIRM_DATE, this);
            return _confirmDate;
        }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(EppRealEduGroupRowGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

    /**
     * @return НПП Студента.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow#getStudentEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> studentEducationOrgUnit()
        {
            if(_studentEducationOrgUnit == null )
                _studentEducationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_STUDENT_EDUCATION_ORG_UNIT, this);
            return _studentEducationOrgUnit;
        }

    /**
     * @return Название группы.
     * @see ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow#getStudentGroupTitle()
     */
        public PropertyPath<String> studentGroupTitle()
        {
            if(_studentGroupTitle == null )
                _studentGroupTitle = new PropertyPath<String>(EppRealEduGroupRowGen.P_STUDENT_GROUP_TITLE, this);
            return _studentGroupTitle;
        }

        public Class getEntityClass()
        {
            return EppRealEduGroupRow.class;
        }

        public String getEntityName()
        {
            return "eppRealEduGroupRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
