/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphEduPlanAdd;

import org.apache.commons.collections.ListUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionWeekTypeGen;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRow2EduPlanGen;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRowGen;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 09.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        // инициализируем модель
        model.setWorkGraph(this.getNotNull(EppWorkGraph.class, model.getWorkGraph().getId()));
        model.setProgramSubjectListModel(new LazySimpleSelectModel<>(this.getFilterList(model.getWorkGraph()), EduProgramSubject.P_TITLE_WITH_CODE));
        model.setCourseList(new ArrayList<>(IDevelopGridDAO.instance.get().getDevelopGridCourseSet(model.getWorkGraph().getDevelopGrid())));

        // загружаем чекбоксы
        final MQBuilder builder = new MQBuilder(EppWorkGraphRow2EduPlanGen.ENTITY_CLASS, "e");
        builder.addJoin("e", EppWorkGraphRow2EduPlanGen.L_ROW, "r");
        builder.add(MQExpression.eq("r", EppWorkGraphRowGen.L_GRAPH, model.getWorkGraph()));
        final List<EppWorkGraphRow2EduPlan> list = builder.getResultList(this.getSession());

        // создаем мап чекбоксов
        final Set<PairKey<Long, Long>> checkSet = new HashSet<>();
        for (final EppWorkGraphRow2EduPlan item : list) {
            checkSet.add(PairKey.create(item.getEduPlanVersion().getId(), item.getRow().getCourse().getId()));
        }
        model.setCheckSet(checkSet);
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppEduPlanVersion.class, "epv").column(property("epv"));
        dql.order(property(EppEduPlanVersion.eduPlan().number().fromAlias("epv")));
        dql.order(property(EppEduPlanVersion.number().fromAlias("epv")));

        final EppWorkGraph workGraph = model.getWorkGraph();
        dql.joinPath(DQLJoinType.inner, EppEduPlanVersion.eduPlan().fromAlias("epv"), "ep");
        dql.where(eq(property(EppEduPlanVersion.state().code().fromAlias("epv")), value(EppState.STATE_ACCEPTED)));
        dql.where(eq(property(EppEduPlan.programForm().fromAlias("ep")), value(workGraph.getDevelopForm().getProgramForm())));
        dql.where(eq(property(EppEduPlan.programTrait().fromAlias("ep")), value(workGraph.getDevelopTech().getProgramTrait())));
        dql.where(eq(property(EppEduPlan.developCondition().fromAlias("ep")), value(workGraph.getDevelopCondition())));
        dql.where(eq(property(EppEduPlan.developGrid().fromAlias("ep")), value(workGraph.getDevelopGrid())));
        if (null != model.getProgramSubject()) {
            dql.joinEntity("epv", DQLJoinType.inner, EppEduPlanProf.class, "prof", eq(property("epv", EppEduPlanVersion.eduPlan()), property("prof")));
            dql.where(eq(property("prof", EppEduPlanProf.programSubject()), value(model.getProgramSubject())));
        }

        UniBaseUtils.createFullPage(model.getDataSource(), dql.createStatement(this.getSession()).<EppEduPlanVersion>list());
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(final Model model)
    {
        final Session session = this.getSession();

        final MQBuilder builder = new MQBuilder(EppWorkGraphRow2EduPlanGen.ENTITY_CLASS, "e");
        builder.addJoin("e", EppWorkGraphRow2EduPlanGen.L_ROW, "r");
        builder.add(MQExpression.eq("r", EppWorkGraphRowGen.L_GRAPH, model.getWorkGraph()));
        final List<EppWorkGraphRow2EduPlan> list = builder.getResultList(session);
        final Map<PairKey<Long, Long>, EppWorkGraphRow2EduPlan> existMap = new HashMap<>();
        for (final EppWorkGraphRow2EduPlan item : list) {
            existMap.put(PairKey.create(item.getEduPlanVersion().getId(), item.getRow().getCourse().getId()), item);
        }

        final Collection<PairKey<Long, Long>> forSave = ListUtils.removeAll(model.getCheckSet(), existMap.keySet());
        final Collection<PairKey<Long, Long>> forDelete = ListUtils.removeAll(existMap.keySet(), model.getCheckSet());

        for (final PairKey<Long, Long> key : forSave)
        {
            final EppEduPlanVersion eduPlanVersion = this.getNotNull(EppEduPlanVersion.class, key.getFirst());
            final Course course = this.getNotNull(Course.class, key.getSecond());

            // save row
            final EppWorkGraphRow row = new EppWorkGraphRow();
            row.setCourse(course);
            row.setGraph(model.getWorkGraph());
            session.save(row);

            // save relation
            final EppWorkGraphRow2EduPlan rel = new EppWorkGraphRow2EduPlan();
            rel.setRow(row);
            rel.setEduPlanVersion(eduPlanVersion);
            session.save(rel);

            // грузим ячейки учебного графика
            final MQBuilder b = new MQBuilder(EppEduPlanVersionWeekTypeGen.ENTITY_CLASS, "e");
            b.add(MQExpression.eq("e", EppEduPlanVersionWeekTypeGen.L_EDU_PLAN_VERSION, eduPlanVersion));
            b.add(MQExpression.eq("e", EppEduPlanVersionWeekTypeGen.L_COURSE, course));
            final List<EppEduPlanVersionWeekType> weekTypeList = b.getResultList(session);

            // для каждой создаем ячейку в учебном графике
            for (final EppEduPlanVersionWeekType item : weekTypeList)
            {
                final EppWorkGraphRowWeek rowWeek = new EppWorkGraphRowWeek();
                rowWeek.setRow(row);
                rowWeek.setWeek(item.getWeek().getNumber());
                rowWeek.setType(item.getWeekType());
                rowWeek.setTerm(item.getTerm());
                session.save(rowWeek);
            }
        }

        for (final PairKey<Long, Long> key : forDelete)
        {
            session.delete(existMap.get(key));
        }
    }

    // private methods

    private List<EducationLevelsHighSchool> getFilterList(final EppWorkGraph workGraph)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EduProgramSubject.class, "edu").column(property("edu"));
        dql.order(property(EduProgramSubject.subjectCode().fromAlias("edu")));
        dql.order(property(EduProgramSubject.title().fromAlias("edu")));

        dql.where(exists(new DQLSelectBuilder()
            .fromEntity(EppEduPlanVersion.class, "epv")
            .joinEntity("epv", DQLJoinType.inner, EppEduPlanProf.class, "prof", eq(property("epv", EppEduPlanVersion.eduPlan()), property("prof")))
            .joinPath(DQLJoinType.inner, EppEduPlanVersion.eduPlan().fromAlias("epv"), "ep")
            .where(eq(property(EppEduPlanProf.programSubject().fromAlias("prof")), property("edu")))
            .where(eq(property(EppEduPlanVersion.state().code().fromAlias("epv")), value(EppState.STATE_ACCEPTED)))
            .where(eq(property(EppEduPlan.programForm().fromAlias("ep")), value(workGraph.getDevelopForm().getProgramForm())))
            .where(eq(property(EppEduPlan.programTrait().fromAlias("ep")), value(workGraph.getDevelopTech().getProgramTrait())))
            .where(eq(property(EppEduPlan.developCondition().fromAlias("ep")), value(workGraph.getDevelopCondition())))
            .where(eq(property(EppEduPlan.developGrid().fromAlias("ep")), value(workGraph.getDevelopGrid())))
            .buildQuery()));

        return dql.createStatement(this.getSession()).list();
    }


}
