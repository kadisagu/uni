package ru.tandemservice.uniepp.dao.registry.data;

import ru.tandemservice.uniepp.entity.registry.IEppRegistryElementItem;

/**
 * @author vdanilov
 */
public interface IEppBaseRegElWrapper<T extends IEppRegistryElementItem> extends IEppRegistryElementItem
{

    /** @return элемент */
    T getItem();

    /** @return нагрузка */
    double getLoadAsDouble(String loadFullCode);

    /** @return количество конрольных мероприятий */
    int getActionSize(String actionFullCode);

}
