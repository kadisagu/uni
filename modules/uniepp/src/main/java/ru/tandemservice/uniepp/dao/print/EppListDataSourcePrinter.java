package ru.tandemservice.uniepp.dao.print;

import jxl.format.Alignment;
import jxl.format.VerticalAlignment;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.view.list.source.AbstractListDataSource;

import ru.tandemservice.uni.util.jxl.ListDataSourcePrinter;

/**
 * @author vdanilov
 */
public class EppListDataSourcePrinter extends ListDataSourcePrinter {

    @SuppressWarnings("unchecked")
    public EppListDataSourcePrinter(final WritableSheet sheet, final AbstractListDataSource source) {
        super(sheet, source);
    }


    protected final WritableCellFormat STYLE_TABLE_BODY_TITLE = this.buildStyleTableTitle();
    protected WritableCellFormat buildStyleTableTitle() {
        try {
            final WritableCellFormat format = this.buildStyleTableNorm();
            format.setVerticalAlignment(VerticalAlignment.TOP);
            format.setWrap(true);
            return format;
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    protected final WritableCellFormat STYLE_TABLE_BODY_NUMBER = this.buildStyleTableNumber();
    protected WritableCellFormat buildStyleTableNumber() {
        try {
            final WritableCellFormat format = this.buildStyleTableNorm();
            format.setAlignment(Alignment.RIGHT);
            format.setVerticalAlignment(VerticalAlignment.TOP);
            return format;
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

}
