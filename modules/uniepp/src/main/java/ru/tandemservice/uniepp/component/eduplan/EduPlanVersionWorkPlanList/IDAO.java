package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionWorkPlanList;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {
    void prepareDataSource(Model model);
}
