package ru.tandemservice.uniepp.entity.workplan;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanRegistryElementRowGen;

/**
 * Строка РУП: часть элемента реестра
 *
 * Строка РУП "часть элемента реестра" определяет, какая часть элемента реестра используется в РУП
 * и задает по ней распределение нагрузки и форм контроля
 */
public class EppWorkPlanRegistryElementRow extends EppWorkPlanRegistryElementRowGen
{

    public EppWorkPlanRegistryElementRow() {}

    public EppWorkPlanRegistryElementRow(final EppWorkPlanBase workPlan, final EppRegistryElementPart registryElementPart) {
        this.setWorkPlan(workPlan);
        this.setRegistryElementPart(registryElementPart);
    }

    @Override public String getTitle() {
        // метод в базе required, значит значение для объектов из базы точно не null (и будет браться именно оно)
        final String title = StringUtils.trimToNull(super.getTitle());
        if (null != title) { return title; }

        // рисуем название дисциплины
        final EppRegistryElementPart p = this.getRegistryElementPart();
        return (null == p ? null : p.getRegistryElement().getTitle());
    }

    public EppRegistryElement getRegistryElement() {
        final EppRegistryElementPart p = this.getRegistryElementPart();
        return (null == p ? null : p.getRegistryElement());
    }

    @Override public String getFormattedLoad() {
        return this.getRegistryElement().getFormattedLoad();
    }

    @Override public EppRegistryStructure getType() {
        final EppRegistryElement rel = this.getRegistryElement();
        return (null == rel ? null : rel.getParent());
    }

    @Override public OrgUnit getOwner() {
        final EppRegistryElement rel = this.getRegistryElement();
        return (null == rel ? null : rel.getOwner());
    }

    @Override public int getRegistryElementPartAmount() {
        final EppRegistryElement rel = this.getRegistryElement();
        return (null == rel ? 0 : rel.getParts());
    }

    @Override public int getRegistryElementPartNumber() {
        final EppRegistryElementPart p = this.getRegistryElementPart();
        return (null == p ? 0 : p.getNumber());
    }

    @Override
    public String getDescriptionString() {
        return (super.getDescriptionString()+'\n'+this.getRegistryElement().getDescriptionString()+"\n"+this.getRegistryElementPart().getNumber());
    }
}