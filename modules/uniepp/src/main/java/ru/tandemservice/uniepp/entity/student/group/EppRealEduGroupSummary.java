package ru.tandemservice.uniepp.entity.student.group;

import java.util.Comparator;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroupSummaryGen;

/**
 * Фактическая сводка контенгента студентов
 *
 * Ведет фактический учет студентов (реальных студентов) в рамках года и чего части на подразделении
 * (не версионируется)
 */
public class EppRealEduGroupSummary extends EppRealEduGroupSummaryGen
{

    public static final Comparator<EppRealEduGroupSummary> COMPARATOR = new Comparator<EppRealEduGroupSummary>() {
        @Override public int compare(EppRealEduGroupSummary o1, EppRealEduGroupSummary o2) {
            int i;
            if (0 != (i = EppYearPart.COMPARATOR.compare(o1.getYearPart(), o2.getYearPart()))) { return i; }
            if (0 != (i = o1.getOwner().getTitle().compareTo(o2.getOwner().getTitle()))) { return i; }
            if (0 != (i = o1.getId().compareTo(o2.getId()))) { return i; }
            return o1.getId().compareTo(o2.getId());
        }
    };

    public EppRealEduGroupSummary() {}
    public EppRealEduGroupSummary(final OrgUnit owner, final EppYearPart yearPart) {
        this.setOwner(owner);
        this.setYearPart(yearPart);
    }
}