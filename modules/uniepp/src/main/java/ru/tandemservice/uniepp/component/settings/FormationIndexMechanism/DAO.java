package ru.tandemservice.uniepp.component.settings.FormationIndexMechanism;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.catalog.EppIndexRule;
import ru.tandemservice.uniepp.entity.settings.EppIndexRule4SubjectIndex;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setIndexList(getList(EppIndexRule4SubjectIndex.class, EppIndexRule4SubjectIndex.eduProgramSubjectIndex().priority().s()));
        model.setRuleList(this.getCatalogItemList(EppIndexRule.class));
        model.setRuleListModel(new LazySimpleSelectModel<>(model.getRuleList()));
    }

    @Override
    public void update(final Model model)
    {
        for (final EppIndexRule4SubjectIndex index: model.getIndexList()) {
            this.getSession().saveOrUpdate(index);
        }
    }

}
