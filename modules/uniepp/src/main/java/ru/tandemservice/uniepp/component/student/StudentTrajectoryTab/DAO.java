package ru.tandemservice.uniepp.component.student.StudentTrajectoryTab;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.getStudentHolder().refresh(Student.class);
    }

    @Override
    public Collection<Long> getWorkPlanIds(final Model model) {
        final EppStudent2EduPlanVersion s2epv = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(model.getStudent().getId());
        if (null == s2epv) { return Collections.emptyList(); }

        final Set<EppStudent2WorkPlan> workplanSet = IEppWorkPlanDAO.instance.get().getStudentEpvWorkPlanMap(Collections.singleton(s2epv.getId()), true).get(s2epv.getId());
        if ((null == workplanSet) || workplanSet.isEmpty()) { return Collections.emptyList(); }

        final Set<Long> result = new HashSet<Long>(workplanSet.size());
        for (final EppStudent2WorkPlan p: workplanSet) { result.add(p.getWorkPlan().getId()); }
        return result;
    }
}
