/**
 * $Id$
 */
package ru.tandemservice.uniepp.component.edustd.EduStdDisciplinePub;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author nkokorina
 * Created on: 08.09.2010
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{

}
