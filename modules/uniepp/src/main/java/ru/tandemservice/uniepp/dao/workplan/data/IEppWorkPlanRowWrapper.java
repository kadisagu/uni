package ru.tandemservice.uniepp.dao.workplan.data;

import org.apache.commons.collections.Predicate;

import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.IEppWorkPlanRow;

/**
 * @author vdanilov
 */
public interface IEppWorkPlanRowWrapper extends IEppWorkPlanRow
{

    Predicate DISCIPLINE_ROWS = object -> {
        final IEppWorkPlanRow row = ((IEppWorkPlanRowWrapper)object).getRow();
        if (row instanceof EppWorkPlanRegistryElementRow) {
            return (((EppWorkPlanRegistryElementRow)row).getRegistryElement() instanceof EppRegistryDiscipline);
        }

        final EppRegistryStructure type = row.getType();
        return ((null != type) && type.isDisciplineElement());
    };

    Predicate NON_DISCIPLINE_ROWS = object -> {
        final IEppWorkPlanRow row = ((IEppWorkPlanRowWrapper)object).getRow();
        if (row instanceof EppWorkPlanRegistryElementRow) {
            return !(((EppWorkPlanRegistryElementRow)row).getRegistryElement() instanceof EppRegistryDiscipline);
        }

        final EppRegistryStructure type = row.getType();
        return ((null != type) && !type.isDisciplineElement());
    };

    String DAYS_LOAD_POSTFIX = "$days";

    /** @return ссылку на оболочку РУП */
    IEppWorkPlanWrapper getWorkPlan();

    /** @return строка РУП из базы */
    IEppWorkPlanRow getRow();

    /** @return описание части элемента реестра, если есть */
    IEppRegElPartWrapper getRegistryElementPart();

    /**
     * возвращает часы нагрузки по строке
     * @param part номер части (null - сумма по строке)
     * @param loadFullCode полный код нагрузки
     * @return часы (в семестр) теоретической нагрузки в части
     **/
    double getTotalPartLoad(Integer part, String loadFullCode);

    /**
     * возвращает количество контольных мероприятий указанного типа
     * @param actionFullCode - полный код контрольного мероприятия
     * @return коли-во контрольных мероприятий
     */
    int getActionSize(String actionFullCode);

}
