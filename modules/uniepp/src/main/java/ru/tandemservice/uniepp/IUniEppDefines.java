package ru.tandemservice.uniepp;

import ru.tandemservice.unieductr.base.bo.EduContract.logic.IEduContractDAO;

/**
 * @author vdanilov
 */
public interface IUniEppDefines
{
    String CTR_CONTRACT_TYPE_EPP = IEduContractDAO.CTR_CONTRACT_TYPE_EDUCATION;

    String EPP_ATTESTATION_REGISTRY_PUB = ru.tandemservice.uniepp.component.registry.AttestationRegistry.Pub.Model.class.getPackage().getName();
    String EPP_DISCIPLINE_REGISTRY_PUB = ru.tandemservice.uniepp.component.registry.DisciplineRegistry.Pub.Model.class.getPackage().getName();
    String EPP_PRACTICE_REGISTRY_PUB = ru.tandemservice.uniepp.component.registry.PracticeRegistry.Pub.Model.class.getPackage().getName();
}
