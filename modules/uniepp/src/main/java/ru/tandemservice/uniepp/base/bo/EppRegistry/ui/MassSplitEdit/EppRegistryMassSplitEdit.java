/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.MassSplitEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;

/**
 * @author Denis Katkov
 * @since 27.04.2016
 */
@Configuration
public class EppRegistryMassSplitEdit extends BusinessComponentManager
{
    public static final String GROUP_SPLIT_VARIANT_DS = "groupSplitVariantDS";
    public static final String GROUP_TYPE_DS = "groupTypeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(GROUP_SPLIT_VARIANT_DS, getName(), EppEduGroupSplitVariant.defaultSelectDSHandler(getName())))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(GROUP_TYPE_DS, getName(), EppGroupType.defaultSelectDSHandler(getName())))
                .create();
    }
}