package ru.tandemservice.uniepp.dao.index;

import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;

/**
 * @author vdanilov
 */
public interface IEppStructureIndexedRow extends IEppIndexedRow {

    /** @return элемент справочника структуры ГОС и УП */
    public EppPlanStructure getValue();
}
