package ru.tandemservice.uniepp.component.eduplan.row.AddEdit;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.IComponentRegion;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.component.IComponentMeta;
import org.tandemframework.core.runtime.BusinessComponentRuntime;
import org.tandemframework.core.view.UIDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

    private static final String FORM__EPP_EPV_REGISTRY_ELEMENT = "EppEpvRegistryElement";
    private static final String FORM__EPP_EPV_SELECTIVE_GROUP = "EppEpvSelectiveGroup";
    private static final String FORM__EPP_EPV_SIMPLE_GROUP = "EppEpvSimpleGroup";
    private static final String FORM__EPP_EPV_STRUCTURE = "EppEpvStructure";

    private static final String[][] components = {
        new String[] { FORM__EPP_EPV_STRUCTURE, Model.GROUP_DISCIPLINE, /*Model.GROUP_ACTION*/ },
        new String[] { FORM__EPP_EPV_SIMPLE_GROUP, Model.GROUP_DISCIPLINE },
        new String[] { FORM__EPP_EPV_SELECTIVE_GROUP, Model.GROUP_DISCIPLINE },
        new String[] { FORM__EPP_EPV_REGISTRY_ELEMENT, Model.GROUP_ACTION, Model.GROUP_DISCIPLINE },

        //new String[] { "EppEpvDiscipline", Model.GROUP_DISCIPLINE },
        //new String[] { "EppEpvNestedDiscipline", Model.GROUP_DISCIPLINE },
        //new String[] { "EppEpvPracticeAction", Model.GROUP_ACTION },
        //new String[] { "EppEpvAttestationAction", Model.GROUP_ACTION },
    };

    protected String[][] getComponentData() {
        return Controller.components;
    }

    @Override
    public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        if (null != model.getElementType()) {
            final IComponentRegion region = component.getChildRegion(UIDefines.DEFAULT_REGION_NAME);
            if (null == region) {
                // мы что-то выбырали, а формы нет - значит дочернюю форму закрыли - закрываем и родительскую
                this.deactivate(component);
                return;
            }
        } else {

            final IEntity entity = UniDaoFacade.getCoreDao().getNotNull(model.getId());
            if (entity instanceof EppEduPlanVersionBlock) {
                // форма добавления
                // проверяем, что редактирование разрешено
                final EppEduPlanVersionBlock block = (EppEduPlanVersionBlock)entity;
                final EppEduPlanVersion eduPlanVersion = block.getEduPlanVersion();
                eduPlanVersion.getState().check_editable(eduPlanVersion);

                // нам нужны только те, которые подходят по правилу
                this.fillElementTypeList(component, model, object -> {
                    final List<String> list = Arrays.asList(object);
                    return list.contains(model.getElementTypeGroupName());
                });

            } else if (entity instanceof EppEpvRow) {
                // форма редактирования
                // проверяем, что редактирование разрешено
                final EppEpvRow row = (EppEpvRow)entity;
                final EppEduPlanVersion eduPlanVersion = row.getOwner().getEduPlanVersion();
                eduPlanVersion.getState().check_editable(eduPlanVersion);

                // нам нужны все компоненты, среди них находим подходящий
                this.fillElementTypeList(component, model, null);
                if (!this.findSuitableBusinessComponent4Edit(component, (EppEpvRow)entity)) {
                    // не один из компонентов не пригоден для редактирования - закрываемся нафиг
                    this.deactivate(component);
                    return;
                }

            } else {
                throw new IllegalArgumentException(ClassUtils.getUserClass(entity.getClass()).getName());
            }
        }
        this.getDao().prepare(model);
    }

    private void fillElementTypeList(final IBusinessComponent component, final Model model, final Predicate<String[]> predicate) {
        /* заполняем возможные типы */ {
            final List<Model.ElementType> elementTypeList = new ArrayList<>();
            for (final String[] data: this.getComponentData()) {
                if ((null == predicate) || predicate.evaluate(data)) {
                    final String name = data[0];
                    final IComponentMeta meta = BusinessComponentRuntime.getInstance().getComponentMeta(component.getPackage()+"."+name);
                    elementTypeList.add(new Model.ElementType(name, meta.getTitle()));
                }
            }
            // Collections.sort(elementTypeList, ITitled.TITLED_COMPARATOR);
            model.setElementTypeList(elementTypeList);
        }
    }

    private boolean findSuitableBusinessComponent4Edit(final IBusinessComponent component, EppEpvRow row) {
        final Model model = this.getModel(component);

        final String form;
        if (row instanceof EppEpvRegistryRow) { form = FORM__EPP_EPV_REGISTRY_ELEMENT; }
        else if (row instanceof EppEpvGroupReRow) { form = FORM__EPP_EPV_SIMPLE_GROUP; }
        else if (row instanceof EppEpvGroupImRow) { form = FORM__EPP_EPV_SELECTIVE_GROUP; }
        else if (row instanceof EppEpvStructureRow) { form = FORM__EPP_EPV_STRUCTURE; }
        else { form = null; }

        if (null != form)
        {
            // выбираем нужную форму
            model.setElementType(CollectionUtils.find(model.getElementTypeList(), object -> form.equals(object.getName())));
            this.onClickChangeType(component);
            return true;
        }

        // закрываем все, что могло остаться
        model.setElementType(null);
        this.onClickChangeType(component);
        return false;
    }

    public void onClickChangeType(final IBusinessComponent component) {
        final Model model = this.getModel(component);

        final IComponentRegion region = component.getChildRegion(UIDefines.DEFAULT_REGION_NAME);
        if (null != region) { region.deactivateComponent(false); }

        if (null != model.getElementType()) {
            final String name = model.getElementType().getName();
            component.createChildRegion(UIDefines.DEFAULT_REGION_NAME, new ComponentActivator(
                    component.getPackage() + "." + name,
                    Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, model.getId())
            ));
        }
    }

}
