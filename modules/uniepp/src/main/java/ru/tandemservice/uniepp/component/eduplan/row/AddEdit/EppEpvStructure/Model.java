package ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvStructure;

import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.BaseAddEditModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow;

import java.util.Collections;
import java.util.List;

/**
 * @author vdanilov
 */
public class Model extends BaseAddEditModel<EppEpvStructureRow> {

    @Override protected EppEpvStructureRow newInstance(final EppEduPlanVersionBlock block) {
        return new EppEpvStructureRow(block);
    }
    @Override protected EppEpvStructureRow check(final EppEpvStructureRow entity) {
        return entity;
    }

    private List<HSelectOption> valueList = Collections.emptyList();
    public List<HSelectOption> getValueList() { return this.valueList; }
    public void setValueList(final List<HSelectOption> valueList) { this.valueList = valueList; }

}

