/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.SecondaryProfAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.EppEduPlanManager;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.Pub.EppEduPlanPub;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanSecondaryProf;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 9/5/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "element.id"),
    @Bind(key = EppEduPlanManager.BIND_PROGRAM_KIND_ID, binding = "programKindId"),
    @Bind(key = EppEduPlanManager.BIND_ORG_UNIT_ID, binding = "orgUnitId")
})
public class EppEduPlanSecondaryProfAddEditUI extends UIPresenter
{
    private EppEduPlanSecondaryProf element = new EppEduPlanSecondaryProf();
    private DevelopCombination developCombination;

    private Long programKindId;
    private Long orgUnitId;

    private ISelectModel ownerOrgUnitModel;

    @Override
    public void onComponentRefresh()
    {
        if (getElement().getId() != null) {
            setElement(DataAccessServices.dao().get(EppEduPlanSecondaryProf.class, getElement().getId()));
            setProgramKindId(getElement().getProgramKind().getId());
        } else {
            getElement().setNumber(INumberQueueDAO.instance.get().getNextNumber(getElement()));
            if (getOrgUnitId() != null && getElement().getOwner() == null) {
                getElement().setOwner(DataAccessServices.dao().get(OrgUnit.class, getOrgUnitId()));
            }
        }

        onChangeSubject();

        if (isEditForm() && (null != getElement().getState()) && (getElement().getState().isReadOnlyState())) {
            throw new ApplicationException("Учебный план находится в состоянии «" + getElement().getState().getTitle() + "», редактирование запрещено.");
        }
    }

    public void onChangeSubject() {

        // При добавлении направленности в УПв (в блок) список вып. подразделений ограничивать перечнем таковых из НПв, если они имеются
        final List<EduOwnerOrgUnit> eduOwnerOrgUnits = DataAccessServices.dao().getList(
            new DQLSelectBuilder().fromEntity(EduOwnerOrgUnit.class, "o")
                .where(exists(EducationLevelsHighSchool.class,
                    EducationLevelsHighSchool.L_ORG_UNIT, property("o", EduOwnerOrgUnit.L_ORG_UNIT),
                    EducationLevelsHighSchool.educationLevel().eduProgramSubject().s(), getElement().getProgramSubject()))
        );
        if (eduOwnerOrgUnits.isEmpty())
            eduOwnerOrgUnits.addAll(DataAccessServices.dao().getList(EduOwnerOrgUnit.class));

        setOwnerOrgUnitModel(new LazySimpleSelectModel<>(eduOwnerOrgUnits));
    }

    public boolean isEditForm() {
        return ((null != getElement()) && (null != getElement().getId()));
    }

    public boolean isDevelopCombinationPartsDisabled()
    {
        return this.isEditForm() || (null != this.getDevelopCombination());
    }

    public void onClickApply() {
        if (getElement().getProgramKind() == null) {
            getElement().setProgramKind(getElement().getProgramSubject().getEduProgramKind());
        }
        if (getElement().getState() == null) {
            getElement().setState(DataAccessServices.dao().get(EppState.class, EppState.code(), EppState.STATE_FORMATIVE));
        }
        if(isLabor())
            element.setWeeksAsLong(null);
        else
            element.setLaborAsLong(null);

        boolean justCreated = ! isEditForm();
        EppEduPlanManager.instance().dao().saveOrUpdateEduPlanProf(getElement());
        if (justCreated)
            _uiActivation.asDesktopRoot(EppEduPlanPub.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getElement().getId()).activate();
        deactivate();
    }

    public void onChangeCombination()
    {
        if (getDevelopCombination() != null) {
            getElement().setProgramForm(getDevelopCombination().getDevelopForm().getProgramForm());
            getElement().setDevelopCondition(getDevelopCombination().getDevelopCondition());
            getElement().setProgramTrait(getDevelopCombination().getDevelopTech().getProgramTrait());
            getElement().setDevelopGrid(getDevelopCombination().getDevelopGrid());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EppEduPlanSecondaryProfAddEdit.BIND_PROGRAM_SUBJECT, getElement().getProgramSubject());
        dataSource.put(EppEduPlanSecondaryProfAddEdit.BIND_PROGRAM_KIND_ID, getProgramKindId());
    }

    // getters and setters

    public EppEduPlanSecondaryProf getElement()
    {
        return element;
    }

    public void setElement(EppEduPlanSecondaryProf element)
    {
        this.element = element;
    }

    public DevelopCombination getDevelopCombination()
    {
        return developCombination;
    }

    public void setDevelopCombination(DevelopCombination developCombination)
    {
        this.developCombination = developCombination;
    }

    public Long getProgramKindId()
    {
        return programKindId;
    }

    public void setProgramKindId(Long programKindId)
    {
        this.programKindId = programKindId;
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }

    public boolean isLabor()
    {
        return element.getProgramSubject().isLabor();
    }

    public boolean isShowLabor()
    {
        return element.getProgramSubject() != null;
    }

    public ISelectModel getOwnerOrgUnitModel()
    {
        return ownerOrgUnitModel;
    }

    public void setOwnerOrgUnitModel(ISelectModel ownerOrgUnitModel)
    {
        this.ownerOrgUnitModel = ownerOrgUnitModel;
    }
}