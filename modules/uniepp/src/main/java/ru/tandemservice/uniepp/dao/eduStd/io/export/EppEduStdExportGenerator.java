package ru.tandemservice.uniepp.dao.eduStd.io.export;

import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.dao.eduStd.IEppEduStdDataDAO;
import ru.tandemservice.uniepp.dao.eduStd.data.IEppStdRowWrapper;
import ru.tandemservice.uniepp.dao.eduStd.io.EduStdImportWrapper;
import ru.tandemservice.uniepp.dao.eduStd.io.IEppEduStdIODAO;
import ru.tandemservice.uniepp.dao.eduStd.io.XmlEduStd;
import ru.tandemservice.uniepp.dao.eduStd.io.XmlEduStd.*;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;
import ru.tandemservice.uniepp.entity.std.data.*;
import ru.tandemservice.uniepp.entity.std.data.gen.EppStdRow2SkillRelationGen;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * 
 * @author nkokorina
 *
 */

public class EppEduStdExportGenerator
{
    private final EppStateEduStandard eduStd;
    private final List<EppStateEduStandardBlock> blockList;
    private final List<EppStateEduStandardSkill> skillList;
    private final Map<String, EppStdDisciplineComment> uuid2comment;
    private final Map<Long, Set<XmlRow>> blockId2rows;
    private final Map<INaturalId<EppStdRow2SkillRelationGen>, EppStdRow2SkillRelation> rowId2skillRel;

    @SuppressWarnings("unchecked")
    public EppEduStdExportGenerator(final EppStateEduStandard std)
    {
        this.eduStd = std;
        this.blockList = IEppEduStdIODAO.instance.get().getEduStdBlockList(std.getId());
        this.skillList = UniDaoFacade.getCoreDao().getList(EppStateEduStandardSkill.class, EppStateEduStandardSkill.parent().id().s(), std.getId());

        final Map<Long, IEppStdRowWrapper> rowMap = IEppEduStdDataDAO.instance.get().getEduStdDataMap(Collections.singleton(std.getId()), true).get(std.getId()).getRowMap();

        this.uuid2comment = IEppEduStdIODAO.instance.get().getEduStdDisciplineCommentMap(rowMap.keySet());

        this.rowId2skillRel = IEppEduStdIODAO.instance.get().getEduStdRow2SkillRel(rowMap.keySet());

        // получаем мап: идентификатор родителя - список детей
        final Map<Long, Set<XmlRow>> parent2Rows = SafeMap.get(LinkedHashSet.class);

        // получаем мап: идентификотор строки в ГОСе - ее преобразование в XmlRow
        final Map<Long, XmlRow> id2XmlRow = new HashMap<>(rowMap.size());

        for (final IEppStdRowWrapper wrapper : rowMap.values())
        {
            final IEppStdRow row = wrapper.getRow();

            final XmlRow xmlRow = this.getXmlRow(row);
            id2XmlRow.put(row.getId(), xmlRow);

            if (null != row.getHierarhyParent())
            {
                parent2Rows.get(row.getHierarhyParent().getId()).add(xmlRow);
            }
        }

        // для всех строк проставляем дочерние
        for (final IEppStdRowWrapper wrapper : rowMap.values())
        {
            final Long rowId = wrapper.getRow().getId();

            final List<XmlRow> xmlRows = new ArrayList<>(parent2Rows.get(rowId));
            if (!xmlRows.isEmpty())
            {
                final XmlRow xmlRow = id2XmlRow.get(rowId);
                xmlRow.xmlRowList = xmlRows;
            }
        }

        // мап идентификатор блока - сет из строк
        final Map<Long, Set<XmlRow>> blockId2rows = SafeMap.get(LinkedHashSet.class);
        for (final IEppStdRowWrapper wrapper : rowMap.values())
        {
            if (null == wrapper.getHierarhyParent())
            {
                final Long blockId = wrapper.getOwner().getId();
                blockId2rows.get(blockId).add(id2XmlRow.get(wrapper.getRow().getId()));
            }
        }
        this.blockId2rows = blockId2rows;
    }

    public byte[] generateContent() throws JAXBException, IOException
    {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        final JAXBContext jc = JAXBContext.newInstance(XmlEduStd.class);

        final Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        final XmlEduStd document = new XmlEduStd();
        this.convertStdToXml(document);

        m.marshal(document, out);

        out.close();

        return out.toByteArray();
    }

    private void convertStdToXml(final XmlEduStd xmlEduStd)
    {
        xmlEduStd.levelType = this.eduStd.getProgramSubject().getEduProgramKind().getTitle();
        xmlEduStd.levelName = this.eduStd.getProgramSubject().getTitleWithCode();
        xmlEduStd.regNumber = this.eduStd.getRegistrationNumber();

        xmlEduStd.regDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(this.eduStd.getConfirmDate());
        xmlEduStd.generation = String.valueOf(this.eduStd.getGeneration().getNumber());
        xmlEduStd.file = null;

        if (!this.skillList.isEmpty())
        {
            final List<XmlStdSkill> xmlSkillList = new ArrayList<>();
            for (final EppStateEduStandardSkill skill : this.skillList)
            {
                final XmlStdSkill xmlSkill = new XmlStdSkill();
                xmlSkill.code = skill.getCode();
                xmlSkill.comment = skill.getComment();
                xmlSkill.group = skill.getSkillGroup().getCode() + " " + skill.getSkillGroup().getTitle();
                xmlSkill.type = skill.getSkillType().getCode() + " " + skill.getSkillType().getTitle();

                xmlSkillList.add(xmlSkill);
            }
            xmlEduStd.stdSkillList = xmlSkillList;
        }

        if (!this.blockList.isEmpty())
        {
            final List<XmlStdBlock> xmlBlockList = new ArrayList<>();
            for (final EppStateEduStandardBlock block : this.blockList)
            {
                final XmlStdBlock xmlBlock = new XmlStdBlock();
                xmlBlock.levelName = block.isRootBlock() ? EduStdImportWrapper.ROOT_BLOCK : block.getProgramSpecialization().getTitle();
                xmlEduStd.levelType = this.eduStd.getProgramSubject().getEduProgramKind().getTitle();
                xmlBlock.xmlRowList = new ArrayList<>(this.blockId2rows.get(block.getId()));

                xmlBlockList.add(xmlBlock);
            }
            xmlEduStd.stdBlockList = xmlBlockList;
        }
    }

    /**
     * 
     * @param row - строка ГОС
     * @return XmlRow соответствующего типа
     */
    private XmlRow getXmlRow(final IEppStdRow row)
    {
        if (row instanceof EppStdStructureRow)
        {
            final EppStdStructureRow stdRow = (EppStdStructureRow)row;

            final XmlStdStructureRow xmlrow = new XmlStdStructureRow();
            xmlrow.uuid = row.getUuid();
            xmlrow.name = stdRow.getValue().getCode() + " " + stdRow.getTitle();
            xmlrow.totalLabor = EppEduStdExportUtils.formatRage(stdRow.getTotalLaborMin(), stdRow.getTotalLaborMax());
            xmlrow.totalSize = EppEduStdExportUtils.formatRage(stdRow.getTotalSizeMin(), stdRow.getTotalSizeMax());
            xmlrow.skillList = this.getXmlDisciplineSkillList(row);

            return xmlrow;
        }

        if (row instanceof EppStdGroupImRow)
        {
            final EppStdGroupImRow stdRow = (EppStdGroupImRow)row;

            final XmlStdGroupImRow xmlrow = new XmlStdGroupImRow();
            xmlrow.uuid = row.getUuid();
            xmlrow.name = stdRow.getTitle();
            xmlrow.number = stdRow.getNumber();
            xmlrow.totalLabor = EppEduStdExportUtils.formatRage(stdRow.getTotalLaborMin(), stdRow.getTotalLaborMax());
            xmlrow.totalSize = EppEduStdExportUtils.formatRage(stdRow.getTotalSizeMin(), stdRow.getTotalSizeMax());
            xmlrow.size = String.valueOf(stdRow.getSize());
            xmlrow.skillList = this.getXmlDisciplineSkillList(row);

            return xmlrow;
        }

        if (row instanceof EppStdGroupReRow)
        {
            final EppStdGroupReRow stdRow = (EppStdGroupReRow)row;

            final XmlStdGroupReRow xmlrow = new XmlStdGroupReRow();
            xmlrow.uuid = row.getUuid();
            xmlrow.name = stdRow.getTitle();
            xmlrow.number = stdRow.getNumber();
            xmlrow.totalLabor = EppEduStdExportUtils.formatRage(stdRow.getTotalLaborMin(), stdRow.getTotalLaborMax());
            xmlrow.totalSize = EppEduStdExportUtils.formatRage(stdRow.getTotalSizeMin(), stdRow.getTotalSizeMax());
            xmlrow.skillList = this.getXmlDisciplineSkillList(row);

            return xmlrow;
        }

        if (row instanceof EppStdDisciplineTopRow)
        {
            final EppStdDisciplineTopRow stdRow = (EppStdDisciplineTopRow)row;

            final XmlStdDisciplineTopRow xmlrow = new XmlStdDisciplineTopRow();
            xmlrow.uuid = row.getUuid();
            xmlrow.name = stdRow.getTitle();
            xmlrow.number = stdRow.getNumber();
            xmlrow.totalLabor = EppEduStdExportUtils.formatRage(stdRow.getTotalLaborMin(), stdRow.getTotalLaborMax());
            xmlrow.totalSize = EppEduStdExportUtils.formatRage(stdRow.getTotalSizeMin(), stdRow.getTotalSizeMax());
            xmlrow.type = stdRow.getType().getCode() + " " + stdRow.getType().getTitle();

            xmlrow.description = this.getXmlDescription(row);
            xmlrow.skillList = this.getXmlDisciplineSkillList(row);

            return xmlrow;
        }

        if (row instanceof EppStdDisciplineNestedRow)
        {
            final EppStdDisciplineNestedRow stdRow = (EppStdDisciplineNestedRow)row;

            final XmlStdDisciplineNestedRow xmlrow = new XmlStdDisciplineNestedRow();
            xmlrow.uuid = row.getUuid();
            xmlrow.name = stdRow.getTitle();
            xmlrow.number = stdRow.getNumber();
            xmlrow.totalLabor = EppEduStdExportUtils.formatRage(stdRow.getTotalLaborMin(), stdRow.getTotalLaborMax());
            xmlrow.totalSize = EppEduStdExportUtils.formatRage(stdRow.getTotalSizeMin(), stdRow.getTotalSizeMax());
            xmlrow.type = stdRow.getType().getCode() + " " + stdRow.getType().getTitle();

            xmlrow.description = this.getXmlDescription(row);
            xmlrow.skillList = this.getXmlDisciplineSkillList(row);

            return xmlrow;
        }

        if (row instanceof EppStdActionRow)
        {
            final EppStdActionRow stdRow = (EppStdActionRow)row;

            final XmlStdActionRow xmlrow = new XmlStdActionRow();
            xmlrow.uuid = row.getUuid();
            xmlrow.name = stdRow.getTitle();
            xmlrow.number = stdRow.getNumber();
            xmlrow.totalLabor = EppEduStdExportUtils.formatRage(stdRow.getTotalLaborMin(), stdRow.getTotalLaborMax());
            xmlrow.totalSize = EppEduStdExportUtils.formatRage(stdRow.getTotalSizeMin(), stdRow.getTotalSizeMax());
            xmlrow.totalWeeks = EppEduStdExportUtils.formatRage(stdRow.getTotalWeeksMin(), stdRow.getTotalWeeksMax());
            xmlrow.type = stdRow.getType().getCode() + " " + stdRow.getType().getTitle();
            xmlrow.skillList = this.getXmlDisciplineSkillList(row);

            return xmlrow;
        }
        return null;
    }

    private List<XmlStdRowSkill> getXmlDisciplineSkillList(final IEppStdRow row)
    {
        final List<XmlStdRowSkill> skillList = new ArrayList<>();

        for (final EppStdRow2SkillRelation rel : this.rowId2skillRel.values())
        {
            final XmlStdRowSkill xmlSkill = new XmlStdRowSkill();
            xmlSkill.code = rel.getSkill().getCode();

            skillList.add(xmlSkill);
        }

        return skillList;
    }

    private XmlStdComment getXmlDescription(final IEppStdRow row)
    {
        final EppStdDisciplineComment comment = this.uuid2comment.get(row.getUuid());
        if (null != comment)
        {
            final XmlStdComment description = new XmlStdComment();
            description.description = comment.getComment();
            return description;
        }
        return null;
    }

}