/* $Id:$ */
package ru.tandemservice.uniepp.edustd.bo.EppEduStandard.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.bo.EduProgram.EduProgramManager;
import ru.tandemservice.uniepp.edustd.bo.EppEduStandard.EppEduStandardManager;

/**
 * @author oleyba
 * @since 9/10/14
 */
@Configuration
public class EppEduStandardAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(EduProgramManager.instance().programKindDSConfig())
            .addDataSource(selectDS("programSubjectDS", EppEduStandardManager.instance().programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
            .create();
    }
}