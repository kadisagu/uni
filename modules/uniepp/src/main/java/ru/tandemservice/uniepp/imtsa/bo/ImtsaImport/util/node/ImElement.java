/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.node;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;

import javax.validation.constraints.NotNull;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;

/**
 * Элемент структуры DOM XML учебного плана ИМЦА. Содержит атрибуты и ссылки на дочерние элементы.
 * @author azhebko
 * @since 27.10.2014
 */
public class ImElement
{
    private final String _tagName;
    private final Map<String, String> _attributes;
    private final Map<String, Collection<ImElement>> _childMap;

    public ImElement(String tagName)
    {
        _tagName = tagName;
        _attributes = new LinkedHashMap<>();
        _childMap = new LinkedHashMap<>();
    }

    /** Значение атрибута как есть (строка). */
    private String getAttribute(String attrName) { return _attributes.get(attrName); }

    /** Значение атрибута: строка. */
    public String getString(String name) { return getAttribute(name); }

    /** Значение атрибута: целое число. */
    public Integer getInteger(String name)
    {
        String v = getAttribute(name);
        if (StringUtils.isEmpty(v))
            return null;

        try
        {
            return Integer.valueOf(v);
        }
        catch (NumberFormatException e)
        {
            throw new ApplicationException("Атрибут «" + name + "» должен быть целым положительным числом.");
        }
    }

    /** Значение атрибута: число с плавающей точкой. */
    public Double getDouble(String name)
    {
        String v = getAttribute(name);
        if (StringUtils.isEmpty(v))
            return null;

        DecimalFormat decimalFormat = new DecimalFormat();
        char decimalSeparator = decimalFormat.getDecimalFormatSymbols().getDecimalSeparator();
        try
        {
            return decimalFormat.parse(v.replace('.', decimalSeparator).replace(',', decimalSeparator)).doubleValue();

        } catch (ParseException e)
        {
            throw new ApplicationException("Атрибут «" + name + "» должен быть положительным числом.");
        }
    }

    /** Значение атрибута: логическое. */
    public Boolean getBoolean(String name)
    {
        String v = getAttribute(name);
        return v == null ? null : "1".equals(v);
    }

    /** Значение атрибута: логическое строгое (<code>null</code> интерпретируется как <code>false</code>). */
    public boolean getBooleanWithNullAsFalse(String name) { return Boolean.TRUE.equals(getBoolean(name)); }

    /** Название элемента. */
    public String getTagName() { return _tagName; }

    /** Задает значение атрибута. */
    public void setAttribute(String name, String value) { _attributes.put(name, value); }

    /** Добавляет элемент к списку вложенных. */
    public void addChildElement(ImElement childNode) { SafeMap.safeGet(_childMap, childNode.getTagName(), ArrayList.class).add(childNode); }

    /** Вложенный элемент с заданным именем. Предполагается быть уникальным. */
    public ImElement child(String name)
    {
        Collection<ImElement> childList = childList(name);
        if (childList.isEmpty())
            return null;

        if (childList.size() > 1)
            throw new IllegalStateException();

        return childList.iterator().next();
    }

    /** Вложенные элементы с заданным именем. */
    @NotNull public Collection<ImElement> childList(String name)
    {
        return Collections.unmodifiableCollection(SafeMap.safeGet(_childMap, name, ArrayList.class));
    }

    @Override
    public String toString() { return toString(""); }

    /** Каждый следующий уровень вложенности элементов форматируется дополнительным отступом. */
    private String toString(String prefix)
    {
        StringBuilder builder = new StringBuilder();
        builder
            .append(prefix)
            .append(_tagName)
            .append(": ")
            .append(_attributes.toString())
            .append('\n');

        for (Collection<ImElement> c: _childMap.values())
            for (ImElement e: c)
                builder.append(e.toString(prefix + '\t'));

        return builder.toString();
    }
}