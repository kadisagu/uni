package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Неделя строки курса в учебном графике
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanVersionWeekTypeGen extends EntityBase
 implements INaturalIdentifiable<EppEduPlanVersionWeekTypeGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType";
    public static final String ENTITY_NAME = "eppEduPlanVersionWeekType";
    public static final int VERSION_HASH = -357410213;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String L_COURSE = "course";
    public static final String L_WEEK = "week";
    public static final String L_TERM = "term";
    public static final String L_WEEK_TYPE = "weekType";

    private EppEduPlanVersion _eduPlanVersion;     // Версия учебного плана
    private Course _course;     // Курс
    private EppWeek _week;     // Учебная неделя
    private Term _term;     // Семестр
    private EppWeekType _weekType;     // Типы деятельности в учебном графике

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion Версия учебного плана. Свойство не может быть null.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Учебная неделя. Свойство не может быть null.
     */
    @NotNull
    public EppWeek getWeek()
    {
        return _week;
    }

    /**
     * @param week Учебная неделя. Свойство не может быть null.
     */
    public void setWeek(EppWeek week)
    {
        dirty(_week, week);
        _week = week;
    }

    /**
     * @return Семестр. Свойство не может быть null.
     */
    @NotNull
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр. Свойство не может быть null.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Типы деятельности в учебном графике. Свойство не может быть null.
     */
    @NotNull
    public EppWeekType getWeekType()
    {
        return _weekType;
    }

    /**
     * @param weekType Типы деятельности в учебном графике. Свойство не может быть null.
     */
    public void setWeekType(EppWeekType weekType)
    {
        dirty(_weekType, weekType);
        _weekType = weekType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEduPlanVersionWeekTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setEduPlanVersion(((EppEduPlanVersionWeekType)another).getEduPlanVersion());
                setCourse(((EppEduPlanVersionWeekType)another).getCourse());
                setWeek(((EppEduPlanVersionWeekType)another).getWeek());
            }
            setTerm(((EppEduPlanVersionWeekType)another).getTerm());
            setWeekType(((EppEduPlanVersionWeekType)another).getWeekType());
        }
    }

    public INaturalId<EppEduPlanVersionWeekTypeGen> getNaturalId()
    {
        return new NaturalId(getEduPlanVersion(), getCourse(), getWeek());
    }

    public static class NaturalId extends NaturalIdBase<EppEduPlanVersionWeekTypeGen>
    {
        private static final String PROXY_NAME = "EppEduPlanVersionWeekTypeNaturalProxy";

        private Long _eduPlanVersion;
        private Long _course;
        private Long _week;

        public NaturalId()
        {}

        public NaturalId(EppEduPlanVersion eduPlanVersion, Course course, EppWeek week)
        {
            _eduPlanVersion = ((IEntity) eduPlanVersion).getId();
            _course = ((IEntity) course).getId();
            _week = ((IEntity) week).getId();
        }

        public Long getEduPlanVersion()
        {
            return _eduPlanVersion;
        }

        public void setEduPlanVersion(Long eduPlanVersion)
        {
            _eduPlanVersion = eduPlanVersion;
        }

        public Long getCourse()
        {
            return _course;
        }

        public void setCourse(Long course)
        {
            _course = course;
        }

        public Long getWeek()
        {
            return _week;
        }

        public void setWeek(Long week)
        {
            _week = week;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppEduPlanVersionWeekTypeGen.NaturalId) ) return false;

            EppEduPlanVersionWeekTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getEduPlanVersion(), that.getEduPlanVersion()) ) return false;
            if( !equals(getCourse(), that.getCourse()) ) return false;
            if( !equals(getWeek(), that.getWeek()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEduPlanVersion());
            result = hashCode(result, getCourse());
            result = hashCode(result, getWeek());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEduPlanVersion());
            sb.append("/");
            sb.append(getCourse());
            sb.append("/");
            sb.append(getWeek());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanVersionWeekTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlanVersionWeekType.class;
        }

        public T newInstance()
        {
            return (T) new EppEduPlanVersionWeekType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
                case "course":
                    return obj.getCourse();
                case "week":
                    return obj.getWeek();
                case "term":
                    return obj.getTerm();
                case "weekType":
                    return obj.getWeekType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "week":
                    obj.setWeek((EppWeek) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "weekType":
                    obj.setWeekType((EppWeekType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlanVersion":
                        return true;
                case "course":
                        return true;
                case "week":
                        return true;
                case "term":
                        return true;
                case "weekType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlanVersion":
                    return true;
                case "course":
                    return true;
                case "week":
                    return true;
                case "term":
                    return true;
                case "weekType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
                case "course":
                    return Course.class;
                case "week":
                    return EppWeek.class;
                case "term":
                    return Term.class;
                case "weekType":
                    return EppWeekType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlanVersionWeekType> _dslPath = new Path<EppEduPlanVersionWeekType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlanVersionWeekType");
    }
            

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Учебная неделя. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType#getWeek()
     */
    public static EppWeek.Path<EppWeek> week()
    {
        return _dslPath.week();
    }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Типы деятельности в учебном графике. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType#getWeekType()
     */
    public static EppWeekType.Path<EppWeekType> weekType()
    {
        return _dslPath.weekType();
    }

    public static class Path<E extends EppEduPlanVersionWeekType> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;
        private Course.Path<Course> _course;
        private EppWeek.Path<EppWeek> _week;
        private Term.Path<Term> _term;
        private EppWeekType.Path<EppWeekType> _weekType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Учебная неделя. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType#getWeek()
     */
        public EppWeek.Path<EppWeek> week()
        {
            if(_week == null )
                _week = new EppWeek.Path<EppWeek>(L_WEEK, this);
            return _week;
        }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * @return Типы деятельности в учебном графике. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType#getWeekType()
     */
        public EppWeekType.Path<EppWeekType> weekType()
        {
            if(_weekType == null )
                _weekType = new EppWeekType.Path<EppWeekType>(L_WEEK_TYPE, this);
            return _weekType;
        }

        public Class getEntityClass()
        {
            return EppEduPlanVersionWeekType.class;
        }

        public String getEntityName()
        {
            return "eppEduPlanVersionWeekType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
