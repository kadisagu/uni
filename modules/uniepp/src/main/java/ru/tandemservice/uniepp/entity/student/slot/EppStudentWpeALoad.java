package ru.tandemservice.uniepp.entity.student.slot;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeALT;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.slot.gen.EppStudentWpeALoadGen;

/**
 * Мероприятие студента из РУП по виду аудиторной нагрузки (МСРП-АН)
 */
public class EppStudentWpeALoad extends EppStudentWpeALoadGen implements ITitled
{

    public EppStudentWpeALoad() {

    }
    public EppStudentWpeALoad(final EppStudentWorkPlanElement slot, final EppGroupType type) {
        this();
        this.setStudentWpe(slot);
        this.setType(type);
    }

    public EppStudentWpeALoad(final EppStudentWorkPlanElement slot, final EppALoadType type) {
        this(slot, type.getEppGroupType());
    }

    @Override
    @EntityDSLSupport(parts = {EppStudentWpeALoad.L_STUDENT_WPE + "." + EppStudentWorkPlanElement.L_REGISTRY_ELEMENT_PART + "." + EppRegistryElement.P_TITLE, EppStudentWpeALoad.L_TYPE + "." + EppALoadType.P_TITLE})
    public String getRegistryElementTitle()
    {
        return this.getStudentWpe().getRegistryElementPart().getTitle() + " (" + StringUtils.uncapitalize(this.getType().getTitle()) + ")";
    }

    @Override
    public String getTitle()
    {
        if (getType() == null) {
            return this.getClass().getSimpleName();
        }
        return "МСРП-ВАН: " + getType().getTitle() + ", " + getStudentWpe().getTitle();
    }

    @Override
    public EppGroupTypeALT getType()
    {
        return (EppGroupTypeALT) super.getType();
    }

}