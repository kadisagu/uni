package ru.tandemservice.uniepp.component.settings.EducationalPlanningAddEdit;

import java.util.Arrays;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;

/**
 * 
 * @author nkokorina
 * @since 26.02.2010
 */

@Input( { @Bind(key = "educationProcessId", binding = "educationProcessId") })
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    private Long _educationProcessId;
    private EppYearEducationProcess _yearEducationProcess;
    private ISelectModel _educationYearList;

    public void setEducationProcessId(final Long _educationProcessId)
    {
        this._educationProcessId = _educationProcessId;
    }

    public Long getEducationProcessId()
    {
        return this._educationProcessId;
    }

    public void setYearEducationProcess(final EppYearEducationProcess _yearEducationProcess)
    {
        this._yearEducationProcess = _yearEducationProcess;
    }

    public EppYearEducationProcess getYearEducationProcess()
    {
        return this._yearEducationProcess;
    }

    public void setEducationYearList(final ISelectModel _educationYearList)
    {
        this._educationYearList = _educationYearList;
    }

    public ISelectModel getEducationYearList()
    {
        return this._educationYearList;
    }
}
