/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.ui.EditRegistryElementOwner;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp.base.bo.EppOrgUnit.EppOrgUnitManager;

/**
 * @author rsizonenko
 * @since 30.03.2016
 */
@Configuration
public class EppReorganizationEditRegistryElementOwner extends BusinessComponentManager {

    public static final String OWNER_DS = "ownerDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(OWNER_DS, getName(), EppOrgUnitManager.instance().eppOrgUnitDSHandlerForAddEditForm(getName())))
                .create();
    }

}
