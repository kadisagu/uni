/**
 *$Id: OrgSystemActionPubAddon.java 4443 2014-05-21 08:10:28Z nvankov $
 */
package ru.tandemservice.uniepp.base.ext.DataCorrection.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.ChangeParent.OrgUnitChangeParent;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.ChangeType.OrgUnitChangeType;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.ReorganizationCodeList.OrgUnitReorganizationCodeList;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.ReorganizationJournal.OrgUnitReorganizationJournal;
import ru.tandemservice.uniepp.base.bo.EppReorganization.ui.EduPlanVersionSpecializationBlockCorrection.EppReorganizationEduPlanVersionSpecializationBlockCorrection;
import ru.tandemservice.uniepp.base.bo.EppReorganization.ui.RegistryElementCorrection.EppReorganizationRegistryElementCorrection;

/**
 * @author rsizonenko
 * @since 30/03/16
 */
public class OrgDataCorrectionPubAddon extends UIAddon
{
    public OrgDataCorrectionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickShowRegistryElementCorrection()
    {
        getActivationBuilder().asCurrent(EppReorganizationRegistryElementCorrection.class).activate();
    }

    public void onClickShowEduPlanVersionSpecBlockCorrection()
    {
        getActivationBuilder().asCurrent(EppReorganizationEduPlanVersionSpecializationBlockCorrection.class).activate();
    }
}
