/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.student.StudentWorkPlanTab;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.gen.EppStudent2WorkPlanGen;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vip_delete
 * @since 24.03.2010
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final Student student = model.getHolder().refresh(Student.class);

        // модель фильтра "Семестр"
        final EppStudent2EduPlanVersion relation = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());
        if (null == relation) {
            model.getTermModel().setSource((ISelectModel)null);
            model.getTermModel().setValue(null);
            model.setSelectedWorkPlan(null);
            return;
        }

        // текущее распределение РУП у студента
        final List<EppStudent2WorkPlan> student2WorkPlan = new DQLSelectBuilder()
        .fromEntity(EppStudent2WorkPlan.class, "s2wp").column(property("s2wp"))
        .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias("s2wp"), "wp")
        .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("s2wp"), "s2epv")
        .where(isNull(property(EppStudent2WorkPlanGen.removalDate().fromAlias("s2wp"))))
        .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
        .where(eq(property(EppStudent2EduPlanVersion.student().fromAlias("s2epv")), value(model.getStudent())))
        .createStatement(this.getSession()).list();

        final HashMap<Integer, EppWorkPlanBase> planTermMap = new HashMap<>(student2WorkPlan.size());
        for (final EppStudent2WorkPlan planTerm : student2WorkPlan) {
            planTermMap.put(planTerm.getWorkPlan().getTerm().getIntValue(), planTerm.getWorkPlan());
        }

        // текущие семестры студента
        final List<ViewWrapper<DevelopGridTerm>> wrapperList = ViewWrapper.getPatchedList(
                IDevelopGridDAO.instance.get().getDevelopGridTermList(relation.getEduPlanVersion().getEduPlan().getDevelopGrid())
        );

        for (final ViewWrapper<DevelopGridTerm> wrapper: wrapperList) {
            final DevelopGridTerm term = wrapper.getEntity();
            final int termNumber = term.getTermNumber();

            final EppWorkPlanBase wp = planTermMap.get(termNumber);
            wrapper.setViewProperty("partTitle", term.getPart().getTitle());
            wrapper.setViewProperty("courseTitle", term.getCourseNumber()+" курс");
            wrapper.setViewProperty("wpTitle", null == wp ? "" : wp.getShortTitle());
        }

        model.getTermModel().setSource(new StaticSelectModel("id", new String[] { DevelopGridTerm.P_TERM_NUMBER, "partTitle", "courseTitle", "wpTitle" }, wrapperList));

        // выставляем текущий семестр
        if (model.getTermModel().getValue() == null) {
            model.getTermModel().setupFirstValue();
        }

        // рисуем РУП из текущего семестра
        model.setSelectedWorkPlan(planTermMap.get(model.getTermModel().getValue().getEntity().getTermNumber()));
    }

    @Override
    public Long doSpecializeWorkPlan(final Model model)
    {
        final EppStudent2EduPlanVersion s2epv = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(model.getStudent().getId());
        if (null == s2epv) { return null; }

        final EppWorkPlanBase wp = model.getWorkplan();
        if (null == wp) { return null; }

        final EppStudent2WorkPlan s2wp = this.getByNaturalId(new EppStudent2WorkPlanGen.NaturalId(s2epv, wp));
        if (null == s2wp) { return null; }

        final Date now = new Date();

        final EppWorkPlanVersion wpv = new EppWorkPlanVersion(wp.getWorkPlan());
        final String nextNumber = INumberQueueDAO.instance.get().getNextNumber(wpv);
        wpv.setRegistrationNumber(wp.getWorkPlan().getNumber()+"."+nextNumber);
        wpv.setNumber(nextNumber + "*");

        // состояние формируется, чтобы можно было редактировать
        wpv.setState(this.getCatalogItem(EppState.class, EppState.STATE_FORMATIVE));
        wpv.setConfirmDate(now);
        this.save(wpv);

        // заполняем индивидуальный РУП(в) на основе текущего привязанного
        IEppWorkPlanDAO.instance.get().doGenerateWorkPlanRowsFromWorkplan(wpv.getId(), wp.getId());

        // новая связь сразу делается неактуальной (чтобы не влиять на процесс до момента утверждения)
        final EppStudent2WorkPlan rel = new EppStudent2WorkPlan();
        rel.update(s2wp);
        rel.setWorkPlan(wpv);
        rel.setRemovalDate(wpv.getConfirmDate());
        this.save(rel);

        return rel.getId();
    }
}
