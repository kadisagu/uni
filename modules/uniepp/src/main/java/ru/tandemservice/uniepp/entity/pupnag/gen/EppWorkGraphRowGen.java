package ru.tandemservice.uniepp.entity.pupnag.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ГУП (Строка для курса)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkGraphRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow";
    public static final String ENTITY_NAME = "eppWorkGraphRow";
    public static final int VERSION_HASH = 654870830;
    private static IEntityMeta ENTITY_META;

    public static final String L_GRAPH = "graph";
    public static final String L_COURSE = "course";

    private EppWorkGraph _graph;     // ГУП
    private Course _course;     // КУРС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ГУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkGraph getGraph()
    {
        return _graph;
    }

    /**
     * @param graph ГУП. Свойство не может быть null.
     */
    public void setGraph(EppWorkGraph graph)
    {
        dirty(_graph, graph);
        _graph = graph;
    }

    /**
     * @return КУРС. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course КУРС. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppWorkGraphRowGen)
        {
            setGraph(((EppWorkGraphRow)another).getGraph());
            setCourse(((EppWorkGraphRow)another).getCourse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkGraphRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkGraphRow.class;
        }

        public T newInstance()
        {
            return (T) new EppWorkGraphRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "graph":
                    return obj.getGraph();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "graph":
                    obj.setGraph((EppWorkGraph) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "graph":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "graph":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "graph":
                    return EppWorkGraph.class;
                case "course":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkGraphRow> _dslPath = new Path<EppWorkGraphRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkGraphRow");
    }
            

    /**
     * @return ГУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow#getGraph()
     */
    public static EppWorkGraph.Path<EppWorkGraph> graph()
    {
        return _dslPath.graph();
    }

    /**
     * @return КУРС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends EppWorkGraphRow> extends EntityPath<E>
    {
        private EppWorkGraph.Path<EppWorkGraph> _graph;
        private Course.Path<Course> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ГУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow#getGraph()
     */
        public EppWorkGraph.Path<EppWorkGraph> graph()
        {
            if(_graph == null )
                _graph = new EppWorkGraph.Path<EppWorkGraph>(L_GRAPH, this);
            return _graph;
        }

    /**
     * @return КУРС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return EppWorkGraphRow.class;
        }

        public String getEntityName()
        {
            return "eppWorkGraphRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
