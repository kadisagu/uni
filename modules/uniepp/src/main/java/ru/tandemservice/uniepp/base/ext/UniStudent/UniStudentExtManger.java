/* $Id$ */
package ru.tandemservice.uniepp.base.ext.UniStudent;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uni.base.bo.UniStudent.logic.IndividualEduPlan.IIndividualEduPlanDao;
import ru.tandemservice.uniepp.base.ext.UniStudent.logic.IndividualEduPlanDao;

/**
 * @author Andrey Andreev
 * @since 10.12.2015
 */
@Configuration
public class UniStudentExtManger extends BusinessObjectExtensionManager
{



    @Bean
    @BeanOverride
    public IIndividualEduPlanDao individualEduPlanDao()
    {
        return new IndividualEduPlanDao();
    }

}