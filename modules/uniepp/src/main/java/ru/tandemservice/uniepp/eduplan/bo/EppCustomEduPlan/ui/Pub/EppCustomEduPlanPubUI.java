/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.Pub;

import com.google.common.collect.ImmutableMap;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.catalog.entity.EppScriptItem;
import ru.tandemservice.uniepp.catalog.entity.codes.EppScriptItemCodes;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.AddEdit.EppCustomEduPlanAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.Content.EppCustomEduPlanContent;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.Content.EppCustomEduPlanContentUI;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 14.08.2015
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id", required = true)
})
@Input({
        @Bind(key = EppCustomEduPlanPubUI.OPEN_EDIT_CONTENT_FORM_BIND, binding = "autoEdit"),
        @Bind(key = EppCustomEduPlanPubUI.INLINE_MODE_BIND, binding = "inlineMode"),
        @Bind(key = EppCustomEduPlanPubUI.PERMISSION_KEY_BIND, binding = "permissionKey"),
        @Bind(key = EppCustomEduPlanPubUI.STUDENT_BIND, binding = "student")
})
public class EppCustomEduPlanPubUI extends UIPresenter
{
    public static final String OPEN_EDIT_CONTENT_FORM_BIND = "autoEdit";
    public static final String INLINE_MODE_BIND = "inlineMode";
    public static final String PERMISSION_KEY_BIND = "permissionKey";
    public static final String STUDENT_BIND = "student";

    private final EntityHolder<EppCustomEduPlan> _holder = new EntityHolder<>();
    private boolean _autoEdit;
    private boolean _inlineMode;
    private String _permissionKey;
    private Student _student;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh(EppCustomEduPlan.class);
    }

    @Override
    public void onComponentActivate()
    {
        if (isAutoEdit()) {
            setAutoEdit(false);
            onClickEditContent();
        }
    }

    // Listeners

    public void onClickEdit()
    {
        if (!isInlineMode()) {
            getActivationBuilder().asRegionDialog(EppCustomEduPlanAddEdit.class)
                    .parameter(UIPresenter.PUBLISHER_ID, getHolder().getId())
                    .activate();
        }
    }

    public void onClickEditContent()
    {
        if (!isInlineMode()) {
            getActivationBuilder()
                    .asRegion(EppCustomEduPlanContent.class).top()
                    .parameter(EppCustomEduPlanContentUI.EDIT_MODE_BIND, true)
                    .parameter(EppCustomEduPlanContentUI.PERMISSION_KEY_BIND, "edit_eppCustomEduPlan")
                    .parameter(EppCustomEduPlanContentUI.STUDENT_BIND, _student)
                    .activate();
        }
    }

    public void onClickDelete()
    {
        if (!isInlineMode()) {
            IUniBaseDao.instance.get().delete(getHolder().getId());
            deactivate();
        }
    }

    public void onClickPrint()
    {
        EppScriptItem scriptItem = DataAccessServices.dao().getByCode(EppScriptItem.class, EppScriptItemCodes.UNIEPP_CUSTOM_EDU_PLAN);

        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem,
                                                                          IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                                                                          IScriptExecutor.OBJECT_VARIABLE, getHolder().getId());
    }

    // Getters & Setters

    public EntityHolder<EppCustomEduPlan> getHolder()
    {
        return _holder;
    }

    public EppCustomEduPlan getCustomPlan()
    {
        return getHolder().getValue();
    }

    public String getPageTitle()
    {
        return "Индивидуальный учебный план: " + getCustomPlan().getFullTitle();
    }

    public boolean isReadOnly()
    {
        return getCustomPlan().getState().isReadOnlyState();
    }

    public boolean isAutoEdit()
    {
        return _autoEdit;
    }

    public void setAutoEdit(boolean autoEdit)
    {
        _autoEdit = autoEdit;
    }

    public boolean isLinkToCustomPlan()
    {
        return isInlineMode();
    }

    public boolean isInlineMode()
    {
        return _inlineMode;
    }

    public void setInlineMode(boolean inlineMode)
    {
        _inlineMode = inlineMode;
    }

    public String getPermissionKey()
    {
        return _permissionKey;
    }

    public void setPermissionKey(String permissionKey)
    {
        _permissionKey = permissionKey;
    }

    public Map<Object, String> getRowsComponentParameters()
    {
        if (isInlineMode()) {
            return ImmutableMap.of(EppCustomEduPlanContentUI.PERMISSION_KEY_BIND, getPermissionKey());
        } else {
            return ImmutableMap.of();
        }
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }
}