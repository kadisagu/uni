/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.dto;

import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author azhebko
 * @since 31.10.2014
 */
public abstract class ImEpvTermDistributedRowDTO extends ImEpvRowDTO implements IEppEduPlanVersionDataDAO.IRowLoadWrapper
{
    private String _number;     // Номер строки
    private EppEpvTermDistributedRow row;

    public String getNumber() { return _number; }
    public void setNumber(String number) { _number = number; }

    public EppEpvTermDistributedRow getRow() { return row; }
    public void setRow(EppEpvTermDistributedRow row) { this.row = row; }

    private final Map<String, Double> _rowLoadMap = new TreeMap<>();
    private final Map<Integer, Map<String, Double>> _rowTermLoadMap = new TreeMap<>();
    private final Map<Integer, Map<String, Integer>> _rowTermActionMap = new TreeMap<>();

    public Map<String, Double> getRowLoadMap() { return _rowLoadMap; }
    public Map<Integer, Map<String, Double>> getRowTermLoadMap() { return _rowTermLoadMap; }
    public Map<Integer, Map<String, Integer>> getRowTermActionMap() { return _rowTermActionMap; }

    @Override public EppEpvTermDistributedRow row() { return getRow(); }
    @Override public Map<String, Double> rowLoadMap() { return getRowLoadMap(); }
    @Override public Map<Integer, Map<String, Double>> rowTermLoadMap() { return getRowTermLoadMap(); }
    @Override public Map<Integer, Map<String, Integer>> rowTermActionMap() { return getRowTermActionMap(); }

    @Override
    public String toString()
    {
        return getNumber() + ", " + getTitle() + ", " + getRowLoadMap() + ", " + getRowTermLoadMap() + ", " + getRowTermActionMap() + '\n' + super.toString();
    }
}