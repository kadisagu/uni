/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppProfActivityType.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.IRootComponentActivationBuilder;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicPub.CatalogDynamicPubUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.catalog.bo.EppProfActivityType.EppProfActivityTypeManager;
import ru.tandemservice.uniepp.catalog.bo.EppProfActivityType.ui.AddEdit.EppProfActivityTypeAddEditUI;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;

import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 16.02.2017
 */
@State({
        @Bind(key = DefaultCatalogPubModel.CATALOG_CODE, binding = "catalogCode"),
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "programSubjectId")
})
public class EppProfActivityTypeListUI extends CatalogDynamicPubUI
{
    // этот публикатор справочника может открываться из элемента справочника "Направление подготовки",
    // поэтому, возможно, оттуда придёт конкретное направление
    private Long _programSubjectId; // not null если зашли из справочника направления
    private EduProgramSubject _programSubject; // not null если зашли из справочника направления

    @Override
    public void onComponentRefresh()
    {
        // если к нам пришёл id направления, то вытащим направление
        if (_programSubjectId != null)
            _programSubject = DataAccessServices.dao().getNotNull(_programSubjectId);
        super.onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppProfActivityTypeList.PROGRAM_SUBJECT_INDEX_DS.equals(dataSource.getName()) ||
                EppProfActivityTypeList.PROGRAM_SUBJECT_DS.equals(dataSource.getName()))
        {
            EduProgramKind programKind = getProgramKind();
            EduProgramSubjectIndex programSubjectIndex = getProgramSubjectIndex();
            if (programKind != null)
                dataSource.put(EppProfActivityTypeManager.PROP_PROGRAM_KIND, programKind);
            if (programSubjectIndex != null)
                dataSource.put(EppProfActivityTypeManager.PROP_PROGRAM_SUBJECT_INDEX, programSubjectIndex);
        }
    }

    @Override
    public String getSettingsKey()
    {
        String additionalKey = "_" + (_programSubjectId != null ? Long.toString(_programSubjectId) : "common");
        return super.getSettingsKey() + additionalKey;
    }

    @Override
    public String getItemDsSettingsKey()
    {
        String additionalKey = "_" + (_programSubjectId != null ? Long.toString(_programSubjectId) : "common");
        return super.getSettingsKey() + additionalKey;
    }

    @Override
    protected void prepareItemDS()
    {
        super.prepareItemDS();

        // костыль (чтобы платформа не мерджила ячейки в столбце с нумерацией)
        getItemDS().getColumn(ActionColumn.DELETE).setMergeRowIdResolver(entity -> String.valueOf(entity.getId()));
    }

    @Override
    protected void addTitleColumns(DynamicListDataSource<ICatalogItem> dataSource)
    {
        // если мы в рамках конкретного направления, то колонка направления не нужна
        if (_programSubjectId == null)
        {
            // мердж направлений
            final IMergeRowIdResolver programSubjectResolver = entity -> {
                EppProfActivityType profActivityType = ((EppProfActivityType) entity);
                return String.valueOf(profActivityType.getProgramSubject().getId());
            };
            dataSource.addColumn(
                    new PublisherLinkColumn("Направление подготовки", EduProgramSubject.titleWithCode().s(), EppProfActivityType.programSubject())
                            .setOrderable(false).setMergeRowIdResolver(programSubjectResolver));
        }
        super.addTitleColumns(dataSource);
    }

    @Override
    protected void addDynamicColumns(DynamicListDataSource<ICatalogItem> dataSource)
    {
        // если зашли из направления, то углублённое изучение только для СПО
        if (_programSubjectId != null && getProgramKind().isProgramSecondaryProf())
        {
            dataSource.addColumn(new SimpleColumn("Углубленное изучение", EppProfActivityType.inDepthStudy().s())
                    .setFormatter(YesNoFormatter.STRICT).setOrderable(false));
        }
        dataSource.addColumn(new SimpleColumn("Импортировано из ИМЦА", EppProfActivityType.fromIMCA().s())
                .setFormatter(YesNoFormatter.STRICT).setOrderable(false));
        super.addDynamicColumns(dataSource);
    }

    @Override
    public String getAdditionalFiltersPageName()
    {
        return EppProfActivityTypeListUI.class.getPackage() + ".AdditionalFilters";
    }

    @Override
    protected void applyFilters(DQLSelectBuilder itemDql, String alias)
    {
        EduProgramKind programKind = getProgramKind();
        EduProgramSubjectIndex programSubjectIndex = getProgramSubjectIndex();
        EduProgramSubject programSubject = getProgramSubject();

        IdentifiableWrapper status = getSettings().get("status");
        IdentifiableWrapper fromIMCA = getSettings().get("fromIMCA");
        if (programKind != null)
            CommonBaseFilterUtil.applySelectFilter(itemDql, alias, EppProfActivityType.programSubject().subjectIndex().programKind().s(), programKind);
        if (programSubjectIndex != null)
            CommonBaseFilterUtil.applySelectFilter(itemDql, alias, EppProfActivityType.programSubject().subjectIndex().s(), programSubjectIndex);
        if (programSubject != null)
            CommonBaseFilterUtil.applySelectFilter(itemDql, alias, EppProfActivityType.programSubject().s(), programSubject);
        if (status != null)
        {
            if (EppProfActivityTypeManager.ONLY_ACTUAL.equals(status))
                itemDql.where(isNull(property(alias, EppProfActivityType.removalDate())));
            else if (EppProfActivityTypeManager.ONLY_NOT_ACTUAL.equals(status))
                itemDql.where(isNotNull(property(alias, EppProfActivityType.removalDate())));
        }
        if (fromIMCA != null)
        {
            if (EppProfActivityTypeManager.FROM_IMCA.equals(fromIMCA))
                itemDql.where(eq(property(alias, EppProfActivityType.fromIMCA()), value(Boolean.TRUE)));
            if (EppProfActivityTypeManager.NOT_FROM_IMCA.equals(fromIMCA))
                itemDql.where(eq(property(alias, EppProfActivityType.fromIMCA()), value(Boolean.FALSE)));
        }
    }

    @Override
    protected void applyOrders(DQLSelectBuilder itemDql, String alias, DQLOrderDescriptionRegistry orderDescriptionRegistry, EntityOrder entityOrder)
    {
        // это не совсем приоритезированный справочник, его элементы группируются по нескольким полям
        // и приоритезируются в этих пределах, поэтому и сортировка несколько другая
        itemDql.order(property(EppProfActivityType.programSubject().subjectCode().fromAlias(alias)));
        itemDql.order(property(EppProfActivityType.programSubject().title().fromAlias(alias)));
        itemDql.order(property(EppProfActivityType.programSubject().id().fromAlias(alias))); // foolproof (если code+title совпадёт)
        itemDql.order(property(EppProfActivityType.priority().fromAlias(alias)));
        itemDql.order(property(EppProfActivityType.id().fromAlias(alias))); // foolproof (если приоритет совпадёт)
    }

    @Override
    protected void updateItemDS()
    {
        super.updateItemDS();

        DynamicListDataSource<ICatalogItem> dataSource = getItemDS();
        final Set<Long> disabledIds = dataSource.getEntityList().stream()
                .map(e -> (EppProfActivityType) e)
                .filter(EppProfActivityType::isFromIMCA)
                .map(ICatalogItem::getId)
                .collect(Collectors.toSet());
        setDeleteDisabledIds(disabledIds);
        setEditDisabledIds(disabledIds);
    }

    public boolean isShowProgramSubjectFilters()
    {
        // выключаем фильтры если мы работаем в пределах конкретного направления подготовки
        return _programSubjectId == null;
    }

    // listeners

    @Override
    public void onClickAddItem()
    {
        IRootComponentActivationBuilder activationBuilder = _uiActivation
                .asDesktopRoot(CommonManager.catalogComponentProvider().getAddEditComponent(getItemClass()))
                .parameter(DefaultCatalogAddEditModel.CATALOG_CODE, getCatalogCode());
        if (_programSubjectId != null)
            activationBuilder.parameter(EppProfActivityTypeAddEditUI.EDU_PROGRAM_SUBJECT_ID, _programSubjectId);
        activationBuilder.activate();
    }

    @Override
    public void onClickEditItem()
    {
        _uiActivation.asDesktopRoot(CommonManager.catalogComponentProvider().getAddEditComponent(getItemClass()))
                .parameter(DefaultCatalogAddEditModel.CATALOG_ITEM_ID, getListenerParameterAsLong())
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()) // немножко костыль
                .activate();
    }

    @Override
    public boolean isStickerVisible()
    {
        return _programSubjectId == null && super.isStickerVisible();
    }

    @Override
    public String getListCaption()
    {
        return _programSubjectId != null ? "Виды профессиональной деятельности" : super.getListCaption();
    }

    private EduProgramKind getProgramKind()
    {
        return _programSubjectId != null ? _programSubject.getEduProgramKind() : getSettings().get("programKind");
    }

    private EduProgramSubjectIndex getProgramSubjectIndex()
    {
        return _programSubjectId != null ? _programSubject.getSubjectIndex() : getSettings().get("programSubjectIndex");
    }

    private EduProgramSubject getProgramSubject()
    {
        return _programSubjectId != null ? _programSubject : getSettings().get("programSubject");
    }

    // getters & setters
    public Long getProgramSubjectId()
    {
        return _programSubjectId;
    }

    public void setProgramSubjectId(Long programSubjectId)
    {
        _programSubjectId = programSubjectId;
    }
}
