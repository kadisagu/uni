package ru.tandemservice.uniepp.dao.eduStd.data;

import java.util.Map;

import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

/**
 * @author vdanilov
 */
public interface IEppStdWrapper extends IEntity {

    /** @return ГОС */
    EppStateEduStandard getStateEduStandard();

    /** @return { row.id -> wrapper(row) по всем блокам, отсортированный иерархически } */
    Map<Long, IEppStdRowWrapper> getRowMap();


}
