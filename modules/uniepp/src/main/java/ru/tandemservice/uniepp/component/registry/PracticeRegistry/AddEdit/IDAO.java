package ru.tandemservice.uniepp.component.registry.PracticeRegistry.AddEdit;

import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;

/**
 * @author vdanilov
 */
public interface IDAO extends ru.tandemservice.uniepp.component.registry.base.AddEdit.IDAO<EppRegistryPractice> {
}
