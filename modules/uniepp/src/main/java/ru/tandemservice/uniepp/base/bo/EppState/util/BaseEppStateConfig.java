/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppState.util;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Alexey Lopatin
 * @since 17.02.2016
 */
public class BaseEppStateConfig implements IEppStateConfig
{
    private static final Map<String, List<String>> POSSIBLE_TRANSITIONS_MAP = ImmutableMap.<String, List<String>>builder()
            .put(EppState.STATE_FORMATIVE, ImmutableList.of(EppState.STATE_ACCEPTABLE))
            .put(EppState.STATE_ACCEPTABLE, ImmutableList.of(EppState.STATE_REJECTED, EppState.STATE_ACCEPTED))
            .put(EppState.STATE_ACCEPTED, ImmutableList.of(EppState.STATE_ARCHIVED, EppState.STATE_REJECTED))
            .put(EppState.STATE_REJECTED, ImmutableList.of(EppState.STATE_FORMATIVE))
            .put(EppState.STATE_ARCHIVED, ImmutableList.of(EppState.STATE_FORMATIVE))
            .build();

    @Override
    public Map<String, List<String>> getPossibleTransitionsMap()
    {
        return POSSIBLE_TRANSITIONS_MAP;
    }

    @Override
    public boolean canChangeState(String fromStateCode, String toStateCode)
    {
        return getPossibleTransitionsMap().get(fromStateCode).contains(toStateCode);
    }

    @Override
    public List<String> getPossibleTransitions(IEppStateObject stateObject)
    {
        if (null == stateObject || null == stateObject.getState()) return Lists.newArrayList();
        return getPossibleTransitionsMap().get(stateObject.getState().getCode());
    }

    @Override
    public Map<String, List<String>> getFromPossibleTransitions()
    {
        final Map<String, List<String>> map = SafeMap.get(ArrayList.class);
        for (Map.Entry<String, List<String>> transEntry : getPossibleTransitionsMap().entrySet())
        {
            for (String code : transEntry.getValue())
            {
                map.get(code).add(transEntry.getKey());
            }
        }
        return map;
    }
}
