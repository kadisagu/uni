/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppSkill.ui.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.IRootComponentActivationBuilder;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicPub.CatalogDynamicPubUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.catalog.bo.EppSkill.EppSkillManager;
import ru.tandemservice.uniepp.catalog.bo.EppSkill.ui.AddEdit.EppSkillAddEditUI;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.catalog.EppSkill;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;

import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 03.04.2017
 */
@State({
        @Bind(key = DefaultCatalogPubModel.CATALOG_CODE, binding = "catalogCode"),
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "programSubjectId")
})
public class EppSkillListUI extends CatalogDynamicPubUI
{
    // этот публикатор справочника может открываться из элемента справочника "Направление подготовки",
    // поэтому, возможно, оттуда придёт конкретное направление
    private Long _programSubjectId; // not null если зашли из справочника направления
    private EduProgramSubject _programSubject; // not null если зашли из справочника направления

    @Override
    public void onComponentRefresh()
    {
        // если к нам пришёл id направления, то вытащим направление
        if (_programSubjectId != null)
            _programSubject = DataAccessServices.dao().getNotNull(_programSubjectId);
        super.onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppSkillList.PROGRAM_SUBJECT_INDEX_DS.equals(dataSource.getName()) ||
                EppSkillList.PROGRAM_SUBJECT_DS.equals(dataSource.getName()))
        {
            EduProgramKind programKind = getProgramKind();
            EduProgramSubjectIndex programSubjectIndex = getProgramSubjectIndex();
            if (programKind != null)
                dataSource.put(EppSkillManager.PROP_PROGRAM_KIND, programKind);
            if (programSubjectIndex != null)
                dataSource.put(EppSkillManager.PROP_PROGRAM_SUBJECT_INDEX, programSubjectIndex);
        }
        if (EppSkillList.PROF_ACTIVITY_TYPE_DS.equals(dataSource.getName()))
        {
            EduProgramKind programKind = getProgramKind();
            EduProgramSubjectIndex programSubjectIndex = getProgramSubjectIndex();
            EduProgramSubject programSubject = getProgramSubject();
            if (programKind != null)
                dataSource.put(EppProfActivityType.PROP_PROGRAM_KIND, programKind);
            if (programSubjectIndex != null)
                dataSource.put(EppProfActivityType.PROP_PROGRAM_SUBJECT_INDEX, programSubjectIndex);
            if (programSubject != null)
                dataSource.put(EppProfActivityType.PROP_PROGRAM_SUBJECT, programSubject);
        }
    }

    @Override
    public String getSettingsKey()
    {
        String additionalKey = "_" + (_programSubjectId != null ? Long.toString(_programSubjectId) : "common");
        return super.getSettingsKey() + additionalKey;
    }

    @Override
    public String getItemDsSettingsKey()
    {
        String additionalKey = "_" + (_programSubjectId != null ? Long.toString(_programSubjectId) : "common");
        return super.getSettingsKey() + additionalKey;
    }

    @Override
    protected void prepareItemDS()
    {
        super.prepareItemDS();

        // костыль (чтобы платформа не мерджила ячейки в столбце с нумерацией)
        getItemDS().getColumn(ActionColumn.DELETE).setMergeRowIdResolver(entity -> String.valueOf(entity.getId()));
    }

    @Override
    protected void addTitleColumns(DynamicListDataSource<ICatalogItem> dataSource)
    {
        if (_programSubjectId == null)
        {
            // Направление подготовки
            final IMergeRowIdResolver programSubjectResolver = entity -> {
                EppSkill skill = ((EppSkill) entity);
                return String.valueOf(skill.getProgramSubject().getId());
            };
            dataSource.addColumn(
                    new PublisherLinkColumn("Направление подготовки", EduProgramSubject.titleWithCode().s(), EppSkill.programSubject())
                            .setOrderable(false).setMergeRowIdResolver(programSubjectResolver));
        }
        // Группа компетенций
        final IMergeRowIdResolver skillGroupResolver = entity -> {
            EppSkill skill = ((EppSkill) entity);
            return String.valueOf(skill.getProgramSubject().getId()) +
                    String.valueOf(skill.getSkillGroup().getId());
        };
        // Вид профессиональной деятельности (его может и не быть, поэтому null группируем отдельно)
        final IMergeRowIdResolver profActivityTypeResolver = entity -> {
            EppSkill skill = ((EppSkill) entity);
            StringBuilder sb = new StringBuilder();
            sb.append(String.valueOf(skill.getProgramSubject().getId()));
            sb.append(String.valueOf(skill.getSkillGroup().getId()));
            if (skill.getProfActivityType() != null) sb.append(String.valueOf(skill.getProfActivityType().getId()));
            return sb.toString();
        };
        dataSource.addColumn(
                new PublisherLinkColumn("Группа компетенций", EppSkillGroup.title().s(), EppSkill.skillGroup())
                        .setOrderable(false).setMergeRowIdResolver(skillGroupResolver));
        dataSource.addColumn(
                new PublisherLinkColumn("Вид профессиональной деятельности", EppProfActivityType.title().s(), EppSkill.profActivityType())
                        .setOrderable(false).setMergeRowIdResolver(profActivityTypeResolver));

        super.addTitleColumns(dataSource);
    }

    @Override
    protected void addDynamicColumns(DynamicListDataSource<ICatalogItem> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("Код компетенции", EppSkill.skillCode().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направленность", EppSkill.programSpecializationTitle()).setOrderable(false));
        // если зашли из направления, то углублённое изучение только для СПО
        if (_programSubjectId != null && getProgramKind().isProgramSecondaryProf())
        {
            dataSource.addColumn(new SimpleColumn("Углубленное изучение", EppSkill.inDepthStudy().s())
                    .setFormatter(YesNoFormatter.STRICT).setOrderable(false));
        }
        dataSource.addColumn(new SimpleColumn("Импортировано из ИМЦА", EppSkill.fromIMCA().s())
                .setFormatter(YesNoFormatter.STRICT).setOrderable(false));
        super.addDynamicColumns(dataSource);
    }

    @Override
    public String getAdditionalFiltersPageName()
    {
        return EppSkillListUI.class.getPackage() + ".AdditionalFilters";
    }

    @Override
    protected void applyFilters(DQLSelectBuilder itemDql, String alias)
    {
        EduProgramKind programKind = getProgramKind();
        EduProgramSubjectIndex programSubjectIndex = getProgramSubjectIndex();
        EduProgramSubject programSubject = getProgramSubject();

        EppSkillGroup skillGroup = getSettings().get("skillGroup");
        EppProfActivityType profActivityType = getSettings().get("profActivityType");
        String skillCode = getSettings().get("skillCode");
        IdentifiableWrapper status = getSettings().get("status");
        IdentifiableWrapper fromIMCA = getSettings().get("fromIMCA");

        if (programKind != null)
            CommonBaseFilterUtil.applySelectFilter(itemDql, alias, EppSkill.programSubject().subjectIndex().programKind().s(), programKind);
        if (programSubjectIndex != null)
            CommonBaseFilterUtil.applySelectFilter(itemDql, alias, EppSkill.programSubject().subjectIndex().s(), programSubjectIndex);
        if (programSubject != null)
            CommonBaseFilterUtil.applySelectFilter(itemDql, alias, EppSkill.programSubject().s(), programSubject);
        if (skillGroup != null)
            CommonBaseFilterUtil.applySelectFilter(itemDql, alias, EppSkill.skillGroup().s(), skillGroup);
        if (profActivityType != null)
            CommonBaseFilterUtil.applySelectFilter(itemDql, alias, EppSkill.profActivityType().s(), profActivityType);
        if (StringUtils.isNotEmpty(skillCode))
            itemDql.where(like(DQLFunctions.upper(property(alias, EppSkill.skillCode())), value(CoreStringUtils.escapeLike(skillCode, true))));
        if (status != null)
        {
            if (EppSkillManager.ONLY_ACTUAL.equals(status))
                itemDql.where(isNull(property(alias, EppSkill.removalDate())));
            else if (EppSkillManager.ONLY_NOT_ACTUAL.equals(status))
                itemDql.where(isNotNull(property(alias, EppSkill.removalDate())));
        }
        if (fromIMCA != null)
        {
            if (EppSkillManager.FROM_IMCA.equals(fromIMCA))
                itemDql.where(eq(property(alias, EppSkill.fromIMCA()), value(Boolean.TRUE)));
            if (EppSkillManager.NOT_FROM_IMCA.equals(fromIMCA))
                itemDql.where(eq(property(alias, EppSkill.fromIMCA()), value(Boolean.FALSE)));
        }
    }

    @Override
    protected void applyOrders(DQLSelectBuilder itemDql, String alias, DQLOrderDescriptionRegistry orderDescriptionRegistry, EntityOrder entityOrder)
    {
        // это не совсем приоритезированный справочник, его элементы группируются по нескольким полям
        // и приоритезируются в этих пределах, поэтому и сортировка несколько другая
        itemDql.order(property(EppSkill.programSubject().subjectCode().fromAlias(alias)));
        itemDql.order(property(EppSkill.programSubject().title().fromAlias(alias)));
        itemDql.order(property(EppSkill.programSubject().id().fromAlias(alias))); // foolproof (если code+title совпадёт)
        itemDql.order(property(EppSkill.skillGroup().priority().fromAlias(alias)));
        itemDql.order(property(EppSkill.skillGroup().id().fromAlias(alias))); // foolproof (если приоритет совпадёт)
        itemDql.fetchPath(DQLJoinType.left, EppSkill.profActivityType().fromAlias(alias), "pat");
        itemDql.order(caseExpr(isNull(property("pat", EppProfActivityType.priority())), value(0), value(1)));
        itemDql.order(property(EppProfActivityType.priority().fromAlias("pat")));
        itemDql.order(property(EppProfActivityType.id().fromAlias("pat"))); // foolproof (если приоритет совпадёт)
        itemDql.order(property(EppSkill.priority().fromAlias(alias)));
        itemDql.order(property(EppSkill.id().fromAlias(alias))); // foolproof (если приоритет совпадёт)
    }

    @Override
    protected void updateItemDS()
    {
        super.updateItemDS();

        DynamicListDataSource<ICatalogItem> dataSource = getItemDS();
        final Set<Long> disabledDeleteIds = dataSource.getEntityList().stream()
                .map(e -> (EppSkill) e)
                .filter(EppSkill::isFromIMCA)
                .map(ICatalogItem::getId)
                .collect(Collectors.toSet());
        final Set<Long> disabledEditIds = dataSource.getEntityList().stream()
                .map(e -> (EppSkill) e)
                .filter(e -> e.isFromIMCA() && (e.getProgramSpecialization() == null || e.getProgramSpecializationVal() != null))
                .map(ICatalogItem::getId)
                .collect(Collectors.toSet());
        setDeleteDisabledIds(disabledDeleteIds);
        setEditDisabledIds(disabledEditIds);
    }

    public boolean isShowProgramSubjectFilters()
    {
        // выключаем фильтры если мы работаем в пределах конкретного направления подготовки
        return _programSubjectId == null;
    }

    @Override
    public void onClickAddItem()
    {
        IRootComponentActivationBuilder activationBuilder = _uiActivation
                .asDesktopRoot(CommonManager.catalogComponentProvider().getAddEditComponent(getItemClass()))
                .parameter(DefaultCatalogAddEditModel.CATALOG_CODE, getCatalogCode());
        if (_programSubjectId != null)
            activationBuilder.parameter(EppSkillAddEditUI.EDU_PROGRAM_SUBJECT_ID, _programSubjectId);
        activationBuilder.activate();
    }

    @Override
    public void onClickEditItem()
    {
        _uiActivation.asDesktopRoot(CommonManager.catalogComponentProvider().getAddEditComponent(getItemClass()))
                .parameter(DefaultCatalogAddEditModel.CATALOG_ITEM_ID, getListenerParameterAsLong())
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()) // немножко костыль
                .activate();
    }

    @Override
    public boolean isStickerVisible()
    {
        return _programSubjectId == null && super.isStickerVisible();
    }

    @Override
    public String getListCaption()
    {
        return _programSubjectId != null ? "Компетенции направления" : super.getListCaption();
    }

    private EduProgramKind getProgramKind()
    {
        return _programSubjectId != null ? _programSubject.getEduProgramKind() : getSettings().get("programKind");
    }

    private EduProgramSubjectIndex getProgramSubjectIndex()
    {
        return _programSubjectId != null ? _programSubject.getSubjectIndex() : getSettings().get("programSubjectIndex");
    }

    private EduProgramSubject getProgramSubject()
    {
        return _programSubjectId != null ? _programSubject : getSettings().get("programSubject");
    }

    // getters & setters
    public Long getProgramSubjectId()
    {
        return _programSubjectId;
    }

    public void setProgramSubjectId(Long programSubjectId)
    {
        _programSubjectId = programSubjectId;
    }
}
