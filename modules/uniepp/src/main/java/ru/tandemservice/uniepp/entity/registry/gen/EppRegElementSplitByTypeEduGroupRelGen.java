package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppRegElementSplitByTypeEduGroupRel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ограничение деления потоков по виду нагрузки
 *
 * Связь элемента реестра с видом нагрузки для ограничения способа деления потоков по виду нагрузки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegElementSplitByTypeEduGroupRelGen extends EntityBase
 implements INaturalIdentifiable<EppRegElementSplitByTypeEduGroupRelGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppRegElementSplitByTypeEduGroupRel";
    public static final String ENTITY_NAME = "eppRegElementSplitByTypeEduGroupRel";
    public static final int VERSION_HASH = -242087821;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_ELEMENT = "registryElement";
    public static final String L_GROUP_TYPE = "groupType";

    private EppRegistryElement _registryElement;     // Элемент реестра
    private EppGroupType _groupType;     // Вид потока

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getRegistryElement()
    {
        return _registryElement;
    }

    /**
     * @param registryElement Элемент реестра. Свойство не может быть null.
     */
    public void setRegistryElement(EppRegistryElement registryElement)
    {
        dirty(_registryElement, registryElement);
        _registryElement = registryElement;
    }

    /**
     * @return Вид потока. Свойство не может быть null.
     */
    @NotNull
    public EppGroupType getGroupType()
    {
        return _groupType;
    }

    /**
     * @param groupType Вид потока. Свойство не может быть null.
     */
    public void setGroupType(EppGroupType groupType)
    {
        dirty(_groupType, groupType);
        _groupType = groupType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRegElementSplitByTypeEduGroupRelGen)
        {
            if (withNaturalIdProperties)
            {
                setRegistryElement(((EppRegElementSplitByTypeEduGroupRel)another).getRegistryElement());
                setGroupType(((EppRegElementSplitByTypeEduGroupRel)another).getGroupType());
            }
        }
    }

    public INaturalId<EppRegElementSplitByTypeEduGroupRelGen> getNaturalId()
    {
        return new NaturalId(getRegistryElement(), getGroupType());
    }

    public static class NaturalId extends NaturalIdBase<EppRegElementSplitByTypeEduGroupRelGen>
    {
        private static final String PROXY_NAME = "EppRegElementSplitByTypeEduGroupRelNaturalProxy";

        private Long _registryElement;
        private Long _groupType;

        public NaturalId()
        {}

        public NaturalId(EppRegistryElement registryElement, EppGroupType groupType)
        {
            _registryElement = ((IEntity) registryElement).getId();
            _groupType = ((IEntity) groupType).getId();
        }

        public Long getRegistryElement()
        {
            return _registryElement;
        }

        public void setRegistryElement(Long registryElement)
        {
            _registryElement = registryElement;
        }

        public Long getGroupType()
        {
            return _groupType;
        }

        public void setGroupType(Long groupType)
        {
            _groupType = groupType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppRegElementSplitByTypeEduGroupRelGen.NaturalId) ) return false;

            EppRegElementSplitByTypeEduGroupRelGen.NaturalId that = (NaturalId) o;

            if( !equals(getRegistryElement(), that.getRegistryElement()) ) return false;
            if( !equals(getGroupType(), that.getGroupType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRegistryElement());
            result = hashCode(result, getGroupType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRegistryElement());
            sb.append("/");
            sb.append(getGroupType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegElementSplitByTypeEduGroupRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegElementSplitByTypeEduGroupRel.class;
        }

        public T newInstance()
        {
            return (T) new EppRegElementSplitByTypeEduGroupRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "registryElement":
                    return obj.getRegistryElement();
                case "groupType":
                    return obj.getGroupType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "registryElement":
                    obj.setRegistryElement((EppRegistryElement) value);
                    return;
                case "groupType":
                    obj.setGroupType((EppGroupType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "registryElement":
                        return true;
                case "groupType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "registryElement":
                    return true;
                case "groupType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "registryElement":
                    return EppRegistryElement.class;
                case "groupType":
                    return EppGroupType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRegElementSplitByTypeEduGroupRel> _dslPath = new Path<EppRegElementSplitByTypeEduGroupRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegElementSplitByTypeEduGroupRel");
    }
            

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegElementSplitByTypeEduGroupRel#getRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> registryElement()
    {
        return _dslPath.registryElement();
    }

    /**
     * @return Вид потока. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegElementSplitByTypeEduGroupRel#getGroupType()
     */
    public static EppGroupType.Path<EppGroupType> groupType()
    {
        return _dslPath.groupType();
    }

    public static class Path<E extends EppRegElementSplitByTypeEduGroupRel> extends EntityPath<E>
    {
        private EppRegistryElement.Path<EppRegistryElement> _registryElement;
        private EppGroupType.Path<EppGroupType> _groupType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegElementSplitByTypeEduGroupRel#getRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> registryElement()
        {
            if(_registryElement == null )
                _registryElement = new EppRegistryElement.Path<EppRegistryElement>(L_REGISTRY_ELEMENT, this);
            return _registryElement;
        }

    /**
     * @return Вид потока. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppRegElementSplitByTypeEduGroupRel#getGroupType()
     */
        public EppGroupType.Path<EppGroupType> groupType()
        {
            if(_groupType == null )
                _groupType = new EppGroupType.Path<EppGroupType>(L_GROUP_TYPE, this);
            return _groupType;
        }

        public Class getEntityClass()
        {
            return EppRegElementSplitByTypeEduGroupRel.class;
        }

        public String getEntityName()
        {
            return "eppRegElementSplitByTypeEduGroupRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
