package ru.tandemservice.uniepp.component.student.StudentTrajectoryTab;

import java.util.Collection;

import ru.tandemservice.uni.dao.IPrepareable;

public interface IDAO extends IPrepareable<Model>
{
    Collection<Long> getWorkPlanIds(Model model);
}
