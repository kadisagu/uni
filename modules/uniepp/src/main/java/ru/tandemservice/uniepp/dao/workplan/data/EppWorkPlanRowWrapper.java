package ru.tandemservice.uniepp.dao.workplan.data;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.uniepp.entity.workplan.IEppWorkPlanRow;

import java.util.*;

/**
 * @author vdanilov
 */
public class EppWorkPlanRowWrapper extends EntityBase implements IEppWorkPlanRowWrapper {
    protected static final Logger logger = Logger.getLogger(EppWorkPlanRowWrapper.class);

    private final EppWorkPlanRow row;
    @Override public IEppWorkPlanRow getRow() { return this.row; }

    private final IEppWorkPlanWrapper workPlan;
    @Override public IEppWorkPlanWrapper getWorkPlan() { return this.workPlan; }

    private final IEppRegElPartWrapper registryElementPart;
    @Override public IEppRegElPartWrapper getRegistryElementPart() { return this.registryElementPart; }

    @Override public String getDisplayableTitle() {
        return this.row.getDisplayableTitle();
    }
    @Override public String getTitle() { return this.getDisplayableTitle(); }

    @Override public String getTitleWithIndex() { return this.row.getTitleWithIndex(); }
    @Override public String getNumber() { return StringUtils.trimToEmpty(this.row.getNumber()); }
    @Override public OrgUnit getOwner() { return this.row.getOwner(); }
    @Override public EppRegistryStructure getType() { return this.row.getType(); }
    @Override public EppWorkPlanRowKind getKind() { return this.row.getKind(); }

    @Override public int getRegistryElementPartAmount() { return this.row.getRegistryElementPartAmount(); }
    @Override public int getRegistryElementPartNumber() { return this.row.getRegistryElementPartNumber(); }

    /** { partNumber -> { loadFullCode -> value } } */
    private final Map<Integer, Map<String, Number>> localPartLoadMap = new HashMap<>();
    public Map<Integer, Map<String, Number>> getLocalPartLoadMap() { return this.localPartLoadMap; }

    public EppWorkPlanRowWrapper(final IEppWorkPlanWrapper workPlan, final EppWorkPlanRow row, final IEppRegElPartWrapper registryElementPart) {
        this.workPlan = workPlan;
        this.row = row;
        this.registryElementPart = registryElementPart;
    }

    @Override
    public boolean isReattestation()
    {
        return this.row.isReattestation();
    }

    @Override
    public boolean isReexamination()
    {
        return this.row.isReexamination();
    }

    //////////////////////////////////////////////////////////
    //////////////// контрольные мероприятия /////////////////
    //////////////////////////////////////////////////////////

    @Override
    public int getActionSize(final String actionFullCode) {
        final IEppRegElPartWrapper registryElementPart = this.getRegistryElementPart();

        if (null == registryElementPart) { return 0; }
        return registryElementPart.getActionSize(actionFullCode);
    }

    //////////////////////////////////////////////////////////
    /////////////////////// нагрузка /////////////////////////
    //////////////////////////////////////////////////////////

    private static final SafeMap.Callback<Integer, Map<String, Double>> PART_LOAD_MAP_CALLBACK = key -> new HashMap<>(4);

    /** @return справочник нагрузки для части */
    @SuppressWarnings("unchecked")
    private Map<String, Double> getLoadMap4Part(final Integer part) {
        final Map<Integer, Map<String, Double>> partLoadMap = (Map)this.localPartLoadMap;
        if (null != part) {
            return SafeMap.safeGet(partLoadMap, part, EppWorkPlanRowWrapper.PART_LOAD_MAP_CALLBACK);
        }

        // если не указана часть, то суммируем по частям данные из нагрузки (по каждому виду нагрузки)
        return SafeMap.safeGet(partLoadMap, Integer.MIN_VALUE, key -> {
            final Map<String, Double> resultMap = EppLoadTypeUtils.newLoadMap();
            for (final Map.Entry<Integer, Map<String, Double>> partEntry: partLoadMap.entrySet()) {
                if (partEntry.getKey() > 0) {
                    for (final Map.Entry<String, Double> e: resultMap.entrySet()) {
                        final Double value = partEntry.getValue().get(e.getKey());
                        if ((null != value) && (value > 0)) { e.setValue(e.getValue() + value); }
                    }
                }
            }
            return resultMap;
        });
    }

    @Override
    public double getTotalPartLoad(final Integer part, final String loadFullCode) {
        final Number value = this.getLoadMap4Part(part).get(loadFullCode);
        return (null == value ? 0d : value.doubleValue());
    }


    @Override public Long getId() { return this.row.getId(); }

    @Override public void setId(final Long id) {
        throw new UnsupportedOperationException();
    }
    @Override public void setProperty(final String propertyName, final Object propertyValue) {
        throw new UnsupportedOperationException();
    }
    @Override public Object getProperty(final Object propertyPath) {
        try {
            return super.getProperty(propertyPath);
        } catch(final Throwable t) {
            return null;
        }
    }


    @SuppressWarnings("unchecked")
    private static final Comparator<Map.Entry> MAP_ENTRY_BYKEY_COMPARATOR = (o1, o2) -> ((Comparable) o1.getKey()).compareTo(o2.getKey());


    private String _description_string_cache = null;

    @Override
    public String getDescriptionString() {
        if (null != this._description_string_cache) { return this._description_string_cache; }

        final StringBuilder sb = new StringBuilder(this.row.getDescriptionString());

        final ArrayList<Map.Entry<Integer, Map<String, Number>>> partEntryList = new ArrayList<>(this.localPartLoadMap.entrySet());
        Collections.sort(partEntryList, EppWorkPlanRowWrapper.MAP_ENTRY_BYKEY_COMPARATOR);

        for (final Map.Entry<Integer, Map<String, Number>> partEntry: partEntryList) {
            final ArrayList<Map.Entry<String, Number>> loadEntryList = new ArrayList<>(partEntry.getValue().entrySet());
            Collections.sort(loadEntryList, EppWorkPlanRowWrapper.MAP_ENTRY_BYKEY_COMPARATOR);
            for (final Map.Entry<String, Number> loadEntry: loadEntryList) {
                sb.append('\n').append(partEntry.getKey()).append('_').append(loadEntry.getKey()).append('_').append(loadEntry.getValue());
            }
        }

        return (this._description_string_cache = sb.toString());
    }

    /** закрывает враппер, убивает все данные (в помощь GC) */
    public void close() {
        for (final Map<String, Number> map : this.localPartLoadMap.values()) { map.clear(); }
        this.localPartLoadMap.clear();
    }

}
