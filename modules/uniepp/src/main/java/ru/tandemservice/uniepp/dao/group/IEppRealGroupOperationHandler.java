package ru.tandemservice.uniepp.dao.group;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow.IEppRealEduGroupRowDescription;

import java.util.Collection;
import java.util.Map;

/**
 * @author vdanilov
 */
public interface IEppRealGroupOperationHandler<G extends EppRealEduGroup> {


    boolean canHandle(Class<G> groupClass);

    /**
     * @param groups группы
     * @throws ApplicationException если обрабочтик считает, что с группой (с любой из) ничего нельзя делать
     */
    void checkLocked(Collection<EppRealEduGroup> groups) throws ApplicationException;

    /**
     * @param rows - список строк, которые необходимо объединить в группе
     * @return { group.id } набор групп (из сиписка групп строк), которые можно использовать в качестве итоговорй группы при объединении
     * важно понимать, что системой объединений может быть выбрана любая группа из возвращенных значений
     * null - означает, что для обработчика нет принципиальной разницы какая группа будет выбрана в качестве основной
     * (в том числе, обработчик допускает, что может быть создана новая группа)
     * @throws ApplicationException если нельзя объединять группы
     */
    Collection<Long> getJoinCandidate(Collection<EppRealEduGroupRow> rows) throws ApplicationException;

    /**
     * @param group новая группа
     * @param rowMap { old-group.relation -> new-group.relation }
     * @throws ApplicationException если нельзя перемещать указанные строки
     */
    void move(EppRealEduGroup group, Map<EppRealEduGroupRow, EppRealEduGroupRow> rowMap)  throws ApplicationException;

    /**
     * вносит изменения в запрос на удаление УГС из демона (запрос на группы. которые МОЖНО удалять)
     * (проверять, что на УГС никто не ссылается не нужно - это гарантируется фасадом)
     * @param relation - описание связи УГС со студентами
     * @param dql - запрос
     * @param alias - алиас УГС (на который требуется добавить ограничение)
     */
    void checkAutoDelete(IEppRealEduGroupRowDescription relation, DQLSelectBuilder dql, String alias);

}
