package ru.tandemservice.uniepp.catalog.bo.EppWeekType.ui.AddEdit;

import com.google.common.base.Strings;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogItemAddEditUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;

import java.util.Optional;

/**
 * @author avedernikov
 * @since 21.09.2015
 */

@Input({
		@Bind(key = "catalogItemId", binding = "holder.id"),
		@Bind(key = DefaultCatalogAddEditModel.CATALOG_CODE, binding = "catalogCode")
})
public class EppWeekTypeAddEditUI extends BaseCatalogItemAddEditUI<EppWeekType>
{
	private String _hexColor;

	@Override
	public void onComponentRefresh()
	{
		_hexColor = Optional.ofNullable(getCatalogItem())
				.map(EppWeekType::getColor)
				.map(col -> "#" + Long.toHexString(col))
				.orElse("");
		super.onComponentRefresh();
	}

	@Override
	public void onClickApply()
	{
		try
		{
			Long color = parseColor(_hexColor);
			if (color != null)
				getCatalogItem().setColor(color);
		}
		catch (NumberFormatException ex)
		{
			ContextLocal.getErrorCollector().add("Цвет должен вводиться в формате «#FFFFFF».");
			return;
		}
		super.onClickApply();
	}

	private static Long parseColor(final String hexColor) throws NumberFormatException
	{
		if (Strings.isNullOrEmpty(hexColor))
			return null;
		if (hexColor.charAt(0) != '#' || hexColor.length() > 7)
			throw new NumberFormatException();
		return Long.valueOf(hexColor.substring(1), 16);
	}

	public String getHexColor()
	{
		return _hexColor;
	}
	public void setHexColor(String color)
	{
		_hexColor = color;
	}

	@Override
	public String getAdditionalPropertiesPageName()
	{
		return "ru.tandemservice.uniepp.catalog.bo.EppWeekType.ui.AddEdit.AdditProps";
	}
}
