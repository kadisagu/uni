package ru.tandemservice.uniepp.component.workplan.WorkPlanRowTimeTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;

/**
 * 
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }


    public void onClickFillFromWorkGraph(final IBusinessComponent component)
    {
        this.getDao().doFillFromWorkGraph(this.getModel(component));
        this.onRefreshComponent(component);
    }

    public void onClickEditTime(final IBusinessComponent component)
    {
        Long id = component.getListenerParameter();
        if (null == id) { id = this.getModel(component).getId(); }

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.workplan.WorkPlanRowTimeEdit.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, id)
        ));
    }



}
