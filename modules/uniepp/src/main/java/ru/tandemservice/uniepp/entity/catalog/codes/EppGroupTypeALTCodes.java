package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид учебной группы (ВАН)"
 * Имя сущности : eppGroupTypeALT
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppGroupTypeALTCodes
{
    /** Константа кода (code) элемента : Лекции (title) */
    String TYPE_LECTURES = "alt.lectures";
    /** Константа кода (code) элемента : Практические занятия (title) */
    String TYPE_PRACTICE = "alt.practice";
    /** Константа кода (code) элемента : Лабораторные занятия (title) */
    String TYPE_LABS = "alt.labs";

    Set<String> CODES = ImmutableSet.of(TYPE_LECTURES, TYPE_PRACTICE, TYPE_LABS);
}
