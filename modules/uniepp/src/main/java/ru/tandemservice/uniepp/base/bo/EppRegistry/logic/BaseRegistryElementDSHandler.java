/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.logic;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * Список элементов реестра
 * @author Zhuj
 */
public class BaseRegistryElementDSHandler extends EntityComboDataSourceHandler
{
    public BaseRegistryElementDSHandler(String ownerId)
    {
        this(ownerId, EppRegistryElement.class);
    }

    public BaseRegistryElementDSHandler(String ownerId, Class<? extends EppRegistryElement> entityClass)
    {
        super(ownerId, entityClass);
        this
        .order(EppRegistryElement.title())
        .order(EppRegistryElement.number())
        .filter(EppRegistryElement.title())
        .filter(EppRegistryElement.shortTitle())
        .filter(EppRegistryElement.fullTitle())
        .filter(EppRegistryElement.number())
        .pageable(true)
        ;

    }
}
