package ru.tandemservice.uniepp.entity.registry;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;

import ru.tandemservice.uniepp.base.bo.EppState.EppStateMutable;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryModuleDAO;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryModuleGen;

import java.util.Collection;
import java.util.Collections;

/**
 * Учебный модуль
 *
 * Учебный модуль (тема) - часть рабочей прогнаммы дисциплины (может использоваться в нескольких дисциплинах)
 * показывает как модуль (тема) будет читаться: какие виды аудиторной нагрузки предусмотрены, какие формы текущего контроля необходимо провести
 */

@EppStateMutable({ EppRegistryModule.P_SHARED })
public class EppRegistryModule extends EppRegistryModuleGen implements IEppStateObject, INumberObject, ISecLocalEntityOwner
{

    @Override
    public INumberGenerationRule getNumberGenerationRule() {
        return IEppRegistryModuleDAO.instance.get().getNumberGenerationRule();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities() {
        return Collections.singletonList((IEntity) this.getOwner());
    }
}