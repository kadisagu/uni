package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.CheckPrint.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Andrey Andreev
 * @since 16.03.2017
 */
public interface IEppWorkPlanCheckPrintDao
{

    @Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
    byte[] printReport(EppWorkPlanCheckPrintData data);
}
