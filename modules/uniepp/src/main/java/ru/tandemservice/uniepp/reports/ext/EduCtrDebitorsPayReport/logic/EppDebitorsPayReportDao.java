/* $Id$ */
package ru.tandemservice.uniepp.reports.ext.EduCtrDebitorsPayReport.logic;

import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import ru.tandemservice.unieductr.reports.bo.EduCtrDebitorsPayReport.logic.EduCtrDebitorsPayReportDao;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexey Lopatin
 * @since 16.04.2015
 */
public class EppDebitorsPayReportDao extends EduCtrDebitorsPayReportDao
{
    @Override
    public Map<Long, Long> getStudentContractRel(List<Long> studentIds)
    {
        final Map<Long, Long> contractStudentIdMap = super.getStudentContractRel(studentIds);

        // получаем список id договоров, для которых есть факт исполнения зачисления(EppCtrEducationResult) студентов из списка студентов полученых выше
        BatchUtils.execute(studentIds, 512, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                        new DQLSelectBuilder().fromEntity(EppCtrEducationResult.class, "r")
                                .where(in(property("r", EppCtrEducationResult.target().student().id()), elements))
                );
                int contract_id_col = dql.column(property("r", EppCtrEducationResult.contract().id()));
                int student_id_col = dql.column(property("r", EppCtrEducationResult.target().student().id()));

                for (Object[] row : dql.getDql().createStatement(getSession()).<Object[]>list())
                {
                    contractStudentIdMap.put((Long) row[contract_id_col], (Long) row[student_id_col]);
                }
            }
        });
        return contractStudentIdMap;
    }
}
