/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.ControlHoursEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author Nikolay Fedorovskih
 * @since 26.12.2014
 */
@Input(
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id", required = true)
)
public class EppEduPlanVersionControlHoursEditUI extends UIPresenter
{
    private EntityHolder<EppEduPlanVersionBlock> _holder = new EntityHolder<>();
    private Double _value;

    // overrides

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
    }

    // listeners

    public void onClickApply()
    {
        IEppEduPlanVersionDataDAO.instance.get().specifyControlHoursForAllRows(getBlock().getId(), getValue());
        deactivate();
    }

    // getters & setters

    public Double getValue()
    {
        return _value;
    }

    public void setValue(Double value)
    {
        _value = value;
    }

    public EppEduPlanVersionBlock getBlock()
    {
        return getHolder().getValue();
    }

    public EntityHolder<EppEduPlanVersionBlock> getHolder()
    {
        return _holder;
    }
}