package ru.tandemservice.uniepp.component.registry.AttestationRegistry.AddEdit;

import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;

/**
 * @author vdanilov
 */
public interface IDAO extends ru.tandemservice.uniepp.component.registry.base.AddEdit.IDAO<EppRegistryAttestation> {
}
