package ru.tandemservice.uniepp.component.workplan.WorkPlanTrajectory;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uni.util.SafeSimpleColumn;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import java.util.*;

public class DAO extends UniBaseDao implements IDAO
{

    private static final String COLUMN_INDEXES = "indexes";
    private static final String COLUMN_REGISTRY_ELEMENT_OWNER_TITLE = "registryElementOwnerTitle";
    private static final String COLUMN_REGISTRY_ELEMENT_TITLE = "registryElementTitle";
    private static final String COLUMN_WORK_PLAN_TITLE = "workPlanTitle";

    @Override
    public void prepare(final Model model)
    {
        final Map<Long, IEppWorkPlanWrapper> workplanDataMap = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(model.getIds());
        if (workplanDataMap.isEmpty())
        {
            final StaticListDataSource<ViewWrapper<IEntity>> ds = model.getDataSource();
            ds.clearColumns();
            ds.setupRows(Collections.<ViewWrapper<IEntity>>emptyList());
            return;
        }

        final EppWorkPlanBase workPlanSample = workplanDataMap.values().iterator().next().getWorkPlan();
        final EppEduPlanVersion eduPlanVersion = workPlanSample.getEduPlanVersion();

        final Map<Integer, IEppWorkPlanWrapper> workplanDataByTerm = new TreeMap<>();
        for (final IEppWorkPlanWrapper w: workplanDataMap.values()) {
            if (null != workplanDataByTerm.put(w.getWorkPlan().getTerm().getIntValue(), w)) {
                throw new IllegalStateException("duplicate term");
            }
            if (!w.getWorkPlan().getEduPlanVersion().equals(eduPlanVersion)) {
                throw new IllegalStateException("eduplanversion is not unique");
            }
        }

        // дисциплины
        this.prepareDisciplinesList(model, workplanDataByTerm);
    }

    @SuppressWarnings("unchecked")
    private void prepareDisciplinesList(final Model model, final Map<Integer, IEppWorkPlanWrapper> workplanDataByTerm)
    {
        // final Long developGridId = eduPlanVersion.getEduPlan().getDevelopGrid().getId();
        // final Map<Integer, DevelopGridTerm> developGridMap = IDevelopGridDAO.instance.get().getDevelopGridDistributionMap(Collections.singleton(developGridId)).get(developGridId);
        // TODO: выделять цветом (отдельно) перекрытие по частям года (темно-красный - вообще смерть)


        final Map<Long, ViewWrapper<IEntity>> disciplines = new HashMap<>();
        final Map<Long, List<Integer>> discTrajectoryMap = new HashMap<>();

        final StaticListDataSource<ViewWrapper<IEntity>> ds = model.getDataSource();
        ds.clearColumns();

        final CollectionFormatter formatter = new CollectionFormatter(SimpleFormatter.INSTANCE);
        ds.addColumn(new SimpleColumn("Индекс", COLUMN_INDEXES, formatter).setWidth(1).setOrderable(false).setClickable(false));
        ds.addColumn(new SimpleColumn("Название в РУП", COLUMN_WORK_PLAN_TITLE, formatter).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Дисциплина", COLUMN_REGISTRY_ELEMENT_TITLE).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Читающее\nподразделение", COLUMN_REGISTRY_ELEMENT_OWNER_TITLE).setClickable(false).setOrderable(false));

        // ds.addColumn(new SimpleColumn("Дисциплина", "title").setOrderable(false).setClickable(true));
        final HeadColumn head = new HeadColumn("terms", "Части в семестрах");
        for (final Map.Entry<Integer, IEppWorkPlanWrapper> entry: workplanDataByTerm.entrySet()) {
            final String columnKey = "term."+entry.getKey();
            head.addColumn(new SafeSimpleColumn(String.valueOf(entry.getKey())+"c", columnKey).setFormatter(formatter).setOrderable(false).setClickable(false).setWidth(1));

            for (final IEppWorkPlanRowWrapper row : entry.getValue().getRowMap().values())
            {
                ViewWrapper<IEntity> dsc;
                if (row.getRow() instanceof EppWorkPlanRegistryElementRow)
                {
                    final EppWorkPlanRegistryElementRow discRow = (EppWorkPlanRegistryElementRow) row.getRow();
                    final EppRegistryElement registryElement = discRow.getRegistryElementPart().getRegistryElement();
                    final Long rowId = registryElement.getId();
                    dsc = disciplines.get(rowId);
                    if (null == dsc) {
                        final String title = registryElement.getEducationElementTitle();
                        disciplines.put(rowId, dsc = new ViewWrapper<>(new IdentifiableWrapper<>(rowId, title)));
                        dsc.setViewProperty(COLUMN_REGISTRY_ELEMENT_TITLE, registryElement.getTitleWithNumber());
                        dsc.setViewProperty(COLUMN_REGISTRY_ELEMENT_OWNER_TITLE, registryElement.getOwner().getShortTitle());
                    }
                }
                else
                {
                    final Long rowId = row.getId();
                    dsc = disciplines.get(rowId);
                    if (null == dsc) {
                        final String title = row.getDisplayableTitle();
                        disciplines.put(rowId, dsc = new ViewWrapper<>(new IdentifiableWrapper<>(rowId, title)));
                        dsc.setViewProperty(COLUMN_REGISTRY_ELEMENT_TITLE, "");
                        dsc.setViewProperty(COLUMN_REGISTRY_ELEMENT_OWNER_TITLE, "");
                    }
                }

                // добавляем номер части в траекторию дисциплины
                SafeMap.safeGet(discTrajectoryMap, dsc.getId(), ArrayList.class).add(row.getRegistryElementPartNumber());

                // название
                {
                    Set<String> v = (Set<String>)dsc.getViewProperty(COLUMN_WORK_PLAN_TITLE);
                    if (null == v) { dsc.setViewProperty(COLUMN_WORK_PLAN_TITLE, v = new TreeSet<>()); }
                    v.add(row.getRow().getTitle());
                }

                // части в семестре (их может быть много в одном - это допустимо)
                {
                    Set<Integer> v = (Set<Integer>)dsc.getViewProperty(columnKey);
                    if (null == v) { dsc.setViewProperty(columnKey, v = new TreeSet<>()); }
                    v.add(row.getRegistryElementPartNumber());
                }

                // идексы (они могут быть разными в разных РУП - это допустимо)
                {
                    Set<String> v = (Set<String>)dsc.getViewProperty(COLUMN_INDEXES);
                    if (null == v) { dsc.setViewProperty(COLUMN_INDEXES, v = new TreeSet<>()); }
                    v.add(row.getNumber());
                }
            }
        }

        ds.addColumn(head.setWidth(1));
        ds.setupRows(disciplines.values());
        Collections.sort(ds.getRowList(), (o1, o2) -> {

            // по индексу
            {
                final Iterator<String> i1 = ((Collection<String>) o1.getViewProperty(COLUMN_INDEXES)).iterator();
                final Iterator<String> i2 = ((Collection<String>) o2.getViewProperty(COLUMN_INDEXES)).iterator();
                while (i1.hasNext() && i2.hasNext()) {
                    final String t1 = StringUtils.trimToEmpty(i1.next());
                    final String t2 = StringUtils.trimToEmpty(i2.next());
                    final int i = t1.compareTo(t2);
                    if (0 != i) { return i; }
                }

                if (i1.hasNext()) { return 1; }
                if (i2.hasNext()) { return -1; }
            }

            // по названию
            final String t1 = o1.getTitle();
            final String t2 = o2.getTitle();
            return t1.compareTo(t2);
        });

        ds.setRowCustomizer(new SimpleRowCustomizer<ViewWrapper<IEntity>>() {
            @Override public String getRowStyle(final ViewWrapper<IEntity> entity) {
                final List<Integer> t = discTrajectoryMap.get(entity.getId());
                if (t.isEmpty() || (t.size() == 1)) { return null; }

                final Set<Integer> s = new TreeSet<>(t);

                if (s.size() < t.size()) {
                    return "background-color: #FAB5B0"; // что-то повторяется
                }

                if (t.size() < ((1 + Collections.max(t)) - Collections.min(t))) {
                    return "background-color: #FAEEB0"; // что-то пропустили
                }

                if (!CollectionUtils.isEqualCollection(t, s)) {
                    return "background-color: #FAEEB0"; // не в той последовательности
                }

                return null;
            }
        });
    }
}
