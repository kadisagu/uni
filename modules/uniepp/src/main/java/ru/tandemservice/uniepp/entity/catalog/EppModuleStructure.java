package ru.tandemservice.uniepp.entity.catalog;

import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.uniepp.entity.catalog.gen.EppModuleStructureGen;

/**
 * Структура модулей
 */
public class EppModuleStructure extends EppModuleStructureGen implements IHierarchyItem
{
    @Override public EppModuleStructure getHierarhyParent() {
        return this.getParent();
    }
}