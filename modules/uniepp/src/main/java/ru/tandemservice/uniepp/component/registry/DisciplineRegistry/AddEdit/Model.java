package ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit;

import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;

/**
 * @author vdanilov
 */
public class Model extends ru.tandemservice.uniepp.component.registry.base.AddEdit.Model<EppRegistryDiscipline> {
    @Override public Class<EppRegistryDiscipline> getElementClass() {
        return EppRegistryDiscipline.class;
    }


}
