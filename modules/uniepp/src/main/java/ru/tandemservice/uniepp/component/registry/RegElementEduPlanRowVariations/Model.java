/* $Id$ */
package ru.tandemservice.uniepp.component.registry.RegElementEduPlanRowVariations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.core.bean.FastAnyObject;
import org.tandemframework.core.bean.IFastBean;
import org.tandemframework.core.bean.IFastBeanOwner;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author oleyba
 * @since 5/18/11
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eppRegElementId", required = true),
    @Bind(key = "showAsPub", binding = "showAsPub", required = false)
})
public class Model
{
    private Long eppRegElementId;
    private EppRegistryElement element;
    private StaticListDataSource<Model.RowWrapper> dataSource = new StaticListDataSource<RowWrapper>();
    private Set<Integer> brokenTerms = new HashSet<Integer>();
    boolean showAsPub;

    public Long getEppRegElementId()
    {
        return this.eppRegElementId;
    }

    public void setEppRegElementId(final Long eppRegElementId)
    {
        this.eppRegElementId = eppRegElementId;
    }

    public EppRegistryElement getElement()
    {
        return this.element;
    }

    public void setElement(final EppRegistryElement element)
    {
        this.element = element;
    }

    public StaticListDataSource<RowWrapper> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final StaticListDataSource<RowWrapper> dataSource)
    {
        this.dataSource = dataSource;
    }

    public Set<Integer> getBrokenTerms()
    {
        return this.brokenTerms;
    }

    public void setBrokenTerms(final Set<Integer> brokenTerms)
    {
        this.brokenTerms = brokenTerms;
    }

    public boolean isShowAsPub()
    {
        return this.showAsPub;
    }

    public void setShowAsPub(final boolean showAsPub)
    {
        this.showAsPub = showAsPub;
    }

    protected static class RowWrapper extends IdentifiableWrapper implements IFastBeanOwner
    {
        private static final long serialVersionUID = 1L;
        private List<EppEduPlanVersion> versionList = new ArrayList<EppEduPlanVersion>();
        private Map<Integer, String> columnContentMap = new HashMap<Integer, String>();
        private Map<Integer, String> keyMap = new HashMap<Integer, String>();

        public RowWrapper(final Long id, final String title)
        {
            super(id, title);
        }

        public List<EppEduPlanVersion> getVersionList()
        {
            return this.versionList;
        }

        public void setVersionList(final List<EppEduPlanVersion> versionList)
        {
            this.versionList = versionList;
        }

        public Map<Integer, String> getColumnContentMap()
        {
            return this.columnContentMap;
        }

        public void setColumnContentMap(final Map<Integer, String> columnContentMap)
        {
            this.columnContentMap = columnContentMap;
        }

        public Map<Integer, String> getKeyMap()
        {
            return this.keyMap;
        }

        public void setKeyMap(final Map<Integer, String> keyMap)
        {
            this.keyMap = keyMap;
        }

        private static final IFastBean FAST_BEAN = new FastAnyObject()
        {
            @Override
            public Object getPropertyValue(final Object obj, final String propertyName)
            {
                return ((RowWrapper)obj).getContentForTerm(propertyName);
            }
        };

        private Object getContentForTerm(final String propertyName)
        {
            if ("versions".equalsIgnoreCase(propertyName)) {
                return this.getVersionList();
            }
            if ("id".equalsIgnoreCase(propertyName)) {
                return this.getId();
            }
            try
            {
                return this.columnContentMap.get(Integer.valueOf(propertyName.substring(4)));
            }
            catch (final NumberFormatException e)
            {
                return null;
            }
        }

        @Override public IFastBean getFastBean() { return RowWrapper.FAST_BEAN; }
    }
}

