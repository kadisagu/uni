package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionSetupRowsOwners;

import java.util.HashMap;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id") })
@SuppressWarnings("unchecked")
public class Model
{
    private final EntityHolder<EppEduPlanVersionBlock> holder = new EntityHolder<EppEduPlanVersionBlock>();
    public EntityHolder<EppEduPlanVersionBlock> getHolder() { return this.holder; }
    public Long getId() { return this.getHolder().getId(); }

    private final StaticListDataSource<IEppEpvRowWrapper> eppDisciplinesDataSource = new StaticListDataSource<IEppEpvRowWrapper>();
    public StaticListDataSource<IEppEpvRowWrapper> getEppDisciplinesDataSource() { return this.eppDisciplinesDataSource; }

    private final StaticListDataSource<IEppEpvRowWrapper> eppActionsDataSource = new StaticListDataSource<IEppEpvRowWrapper>();
    public StaticListDataSource<IEppEpvRowWrapper> getEppActionsDataSource() { return this.eppActionsDataSource; }

    private ISelectModel selectModel;
    public ISelectModel getSelectModel() { return this.selectModel; }
    public void setSelectModel(final ISelectModel selectModel) { this.selectModel = selectModel; }

    private final Map<Long, OrgUnit> valueMap = new HashMap<Long, OrgUnit>();
    public Map<Long, OrgUnit> getValueMap() { return this.valueMap; }

    public boolean isEditable(final IEppEpvRowWrapper w) {
        return (w.getRow() instanceof EppEpvRegistryRow) && (w.getOwner().getId().equals(getId()));
    }

    public OrgUnit getDisciplineOrgUnit() {  return this.valueMap.get(this.getEppDisciplinesDataSource().getCurrentEntity().getId()); }
    public void setDisciplineOrgUnit(final OrgUnit value) { this.valueMap.put(this.getEppDisciplinesDataSource().getCurrentEntity().getId(), value); }

    public OrgUnit getActionOrgUnit() { return this.valueMap.get(this.getEppActionsDataSource().getCurrentEntity().getId()); }
    public void setActionOrgUnit(final OrgUnit value) { this.valueMap.put(this.getEppActionsDataSource().getCurrentEntity().getId(), value); }

}
