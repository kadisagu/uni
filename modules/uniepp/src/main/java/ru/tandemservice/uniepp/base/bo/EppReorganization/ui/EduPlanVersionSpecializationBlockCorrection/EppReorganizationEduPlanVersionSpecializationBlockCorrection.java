/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.ui.EduPlanVersionSpecializationBlockCorrection;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.ReorganizationJournal.OrgUnitReorganizationJournal;
import ru.tandemservice.uniepp.base.bo.EppReorganization.ui.EduPlanVersionSpecializationBlockCorrectionBlocksTab.EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab;
import ru.tandemservice.uniepp.base.bo.EppReorganization.ui.RegistryElementCorrectionElementsTab.EppReorganizationRegistryElementCorrectionElementsTab;

/**
 * @author rsizonenko
 * @since 05.04.2016
 */
@Configuration
public class EppReorganizationEduPlanVersionSpecializationBlockCorrection extends BusinessComponentManager {

    public static final String TAB_PANEL = "epvBlocksCorrectionTabPanel";


    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab("blocksTab", EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab.class))
                .addTab(componentTab("journalTab", OrgUnitReorganizationJournal.class).permissionKey("orgUnitReorganizationJournal").after("blocksTab").parameters("ui:journalParams"))
                .create();
    }


}
