/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsSlotList;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;

import java.util.Date;

/**
 * @author Alexander Shaburov
 * @since 01.03.13
 */
public class EppIndicatorStudentsSlotListUI extends AbstractUniStudentListUI
{
    @Override
    public String getSettingsKey()
    {
        return "epp.StudentSlotList.filter";
    }

    public void onClickSync()
    {
        EppStudentSlotDAO.DAEMON.wakeUpDaemon();
        getSupport().doRefresh();
    }

    public String getDaemonStatus(final String message)
    {
        final Long date = EppStudentSlotDAO.DAEMON.getCompleteStatus();
        if (null != date)
            return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));

        return message;
    }
}
