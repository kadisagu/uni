/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.SecondaryProfList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.plan.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 8/28/14
 */
public class EppEduPlanSecondaryProfDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String  VIEW_PROP_VERSION = "version";

    public EppEduPlanSecondaryProfDSHandler(String ownerID)
    {
        super(ownerID);
    }

    @Override
    protected final DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLOrderDescriptionRegistry orderDescription = new DQLOrderDescriptionRegistry(EppEduPlanSecondaryProf.class, "p");
        DQLSelectBuilder builder = orderDescription.buildDQLSelectBuilder().column("p");

        filter(builder, "p", context);

        //сортировка по столбцу "Нормативный срок обучения по очной форме"
        if (input.getEntityOrder().getKeyString().equals(EppEduPlanSecondaryProfList.EDUCATION_PERIOD)) {
            builder.joinEntity("p", DQLJoinType.left, EppEduPlanProf.class, "eepp", eq(EppEduPlanSecondaryProf.id().fromAlias("p"), EppEduPlanProf.id().fromAlias("eepp")))
                    .order(property(EppEduPlanProf.stdProgramDurationYears().fromAlias("p")), input.getEntityOrder().getDirection())
                    .order(property(EppEduPlanProf.stdProgramDurationMonths().fromAlias("p")), input.getEntityOrder().getDirection());
        }

        orderDescription.applyOrderWithLeftJoins(builder, input.getEntityOrder());

        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
        return wrapOutput(output);
    }

    protected void filter(DQLSelectBuilder builder, String alias, ExecutionContext context)
    {
        IDataSettings settings = context.get("settings");

        builder.where(eq(property(alias, EppEduPlanSecondaryProf.programKind()), commonValue(settings.get("programKind"))));
        FilterUtils.applySimpleLikeFilter(builder, alias, EppEduPlanSecondaryProf.number(), settings.<String>get("number"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanSecondaryProf.programSubject().subjectIndex(), settings.get("subjectIndex"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanSecondaryProf.programSubject(), settings.get("programSubject"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanSecondaryProf.programForm(), settings.get("programForm"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanSecondaryProf.developCondition(), settings.get("developCondition"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanSecondaryProf.programTrait(), settings.get("programTrait"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanSecondaryProf.developGrid(), settings.get("developGrid"));
        FilterUtils.applySelectFilter(builder, alias, EppEduPlanSecondaryProf.state(), settings.get("state"));

        List<EduProgramSpecialization> programSpecializations = settings.get("programSpecializations");
        if (programSpecializations != null && !programSpecializations.isEmpty())
        {
            builder.where(exists(
                    new DQLSelectBuilder()
                            .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                            .where(eq(property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan()), property(alias)))
                            .where(in(property("b", EppEduPlanVersionSpecializationBlock.programSpecialization()), programSpecializations))
                            .buildQuery()));
        }

        OrgUnit orgUnit = context.get("orgUnit");
        applyOrgUnitFilter(builder, alias, orgUnit);
    }

    protected void applyOrgUnitFilter(DQLSelectBuilder builder, String alias, OrgUnit orgUnit)
    {
        if (orgUnit != null) {
            builder.where(or(
                eq(property(alias, EppEduPlanSecondaryProf.owner()), value(orgUnit)),
                eq(property(alias, EppEduPlanSecondaryProf.ownerOrgUnit().orgUnit()), value(orgUnit)),
                exists(new DQLSelectBuilder()
                    .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                    .where(eq(property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan()), property(alias)))
                    .where(eq(property("b", EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit()), value(orgUnit)))
                    .buildQuery()),
                exists(new DQLSelectBuilder()
                    .fromEntity(EducationOrgUnit.class, "e")
                    .where(or(
                        eq(property("e", EducationOrgUnit.formativeOrgUnit()), value(orgUnit)),
                        eq(property("e", EducationOrgUnit.territorialOrgUnit()), value(orgUnit))
                    ))
                    .where(eq(property("e", EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject()), property(alias, EppEduPlanSecondaryProf.programSubject())))
                    .where(eq(property("e", EducationOrgUnit.developForm().programForm()), property(alias, EppEduPlan.programForm())))
                    .where(eq(property("e", EducationOrgUnit.developCondition()), property(alias, EppEduPlan.developCondition())))
                    .where(eqNullSafe(property("e", EducationOrgUnit.developTech().programTrait()), property(alias, EppEduPlan.programTrait())))
                    .where(eq(property("e", EducationOrgUnit.developPeriod()), property(alias, EppEduPlan.developGrid().developPeriod())))
                    .buildQuery())));
        }
    }

    protected DSOutput wrapOutput(DSOutput output)
    {
        Map<Long, List<EppEduPlanVersion>> versionMap = SafeMap.get(ArrayList.class);
        for (EppEduPlanVersion version : DataAccessServices.dao().getList(EppEduPlanVersion.class, EppEduPlanVersion.eduPlan().s(), output.getRecordList(), EppEduPlanVersion.number().s()))
            versionMap.get(version.getEduPlan().getId()).add(version);

        final IFormatter formatter = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS;
        for (DataWrapper wrapper : DataWrapper.wrap(output)) {
            wrapper.setProperty(VIEW_PROP_VERSION, versionMap.get(wrapper.getId()));

            //значение столбца нормативный срок обучения по очной форме
            EppEduPlanSecondaryProf prof = wrapper.getWrapped();
            wrapper.setProperty(EppEduPlanSecondaryProfList.EDUCATION_PERIOD, prof.getStandardProgramDurationStr());

            //значение столбца общая трудоемкость
            String labor = null;
            if (prof.getLaborAsDouble() != null) {
                if (prof.getLaborAsDouble() == prof.getLaborAsDouble().longValue()) {
                    labor = CommonBaseStringUtil.numberWithPostfixCase(prof.getLaborAsDouble().longValue(), "з.е.", "з.е.", "з.е.");
                } else {
                    labor = CommonBaseStringUtil.numberWithPostfixCase(prof.getLaborAsDouble(), 2, "з.е.", "з.е.", "з.е.", formatter);
                }
            }
            if (prof.getWeekAsDouble() != null) {
                if (prof.getWeekAsDouble() == prof.getWeekAsDouble().longValue()) {
                    labor = CommonBaseStringUtil.numberWithPostfixCase(prof.getWeekAsDouble().longValue(), "неделя", "недели", "недель");
                } else {
                    labor = CommonBaseStringUtil.numberWithPostfixCase(prof.getWeekAsDouble(), 2, "неделя", "недели", "недель", formatter);
                }
            }
            wrapper.setProperty(EppEduPlanSecondaryProfList.LABOR, labor);
        }

        return output;
    }
}

