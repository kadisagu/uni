/* $Id$ */
package ru.tandemservice.uniepp.util;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.IOrgUnitReportVisibleResolver;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;

/**
 * @author oleyba
 * @since 12/16/11
 */
public class GroupOrgUnitReportVisibleResolver implements IOrgUnitReportVisibleResolver
{
    @Override
    public boolean isVisible(OrgUnit orgUnit)
    {
        return IEppSettingsDAO.instance.get().isGroupOrgUnit(orgUnit.getId());
    }
}
