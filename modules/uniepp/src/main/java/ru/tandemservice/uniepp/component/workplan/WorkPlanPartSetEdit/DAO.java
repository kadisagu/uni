package ru.tandemservice.uniepp.component.workplan.WorkPlanPartSetEdit;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.workplan.EppWorkPlanPartSetUtil;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearEducationWeekGen;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniepp.tapestry.richTableList.RichRangeSelection;
import ru.tandemservice.uniepp.tapestry.richTableList.WeekTypeBlockColumn;

import java.util.*;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    private Map<Integer, EppWorkPlanPart> fillPartMap(final Model model, final Collection<EppWorkPlanPart> partList, final int parts, final int targetWeeks) {
        final Map<Integer, EppWorkPlanPart> partMap = EppWorkPlanPartSetUtil.fillPartMap(model.getElement(), partList, parts, targetWeeks);
        model.setPartMap(partMap);
        return partMap;
    }

    @Override
    public void prepare(final Model model) {
        model.setElement(this.get(EppWorkPlan.class, model.getId()));
        model.getElement().getState().check_editable(model.getElement());

        final Collection<EppWorkPlanPart> partList = IEppWorkPlanDAO.instance.get().getWorkPlanPartMap(model.getElement().getId()).values();
        final Map<Integer, EppWorkPlanPart> partMap = this.fillPartMap(model, partList, -1, -1);
        model.setPartsInTerm(Model.getPartsInTerm(partMap.size()));
    }


    @Override
    public void prepareWorkGraph(final Model model) {

        final Map<Integer, String> graphWeekCodeMap = model.getGraphWeekCodeMap();
        graphWeekCodeMap.clear();

        final RangeSelectionWeekTypeListDataSource<EppWorkPlan> scheduleDataSource = model.getScheduleDataSource();
        final AbstractListDataSource<EppWorkPlan> dataSource = scheduleDataSource.getDataSource();
        dataSource.getColumns().clear();

        final EppWorkPlan workplan = model.getElement();

        final int parts = Math.max(1, ((null == model.getPartsInTerm()) ? 0 : model.getPartsInTerm().getId().intValue()));

        final List<EppWorkGraphRowWeek> graphWeeks = IEppWorkPlanDAO.instance.get().getWorkGraphWeeks4Plan(workplan.getId());

        if (graphWeeks.isEmpty()) {

            // если в ГУПе нет недель, то берем текущее число распределенных недель
            int targetWeeks = 0;
            for (final EppWorkPlanPart w: model.getPartMap().values()) {
                targetWeeks += w.getTotalWeeks();
            }

            // если недель нет - то делаем все вручную
            this.fillPartMap(model, model.getPartMap().values(), parts, Math.max(targetWeeks, parts));

        } else {


            // если недели есть, то распределяем
            final Map<Integer, EppWorkPlanPart> partMap = this.fillPartMap(model, model.getPartMap().values(), parts, graphWeeks.size());

            dataSource.getColumns().add(new SimpleColumn("", "") {
                @Override public String getContent(final IEntity entity) {
                    return (workplan.getShortTitle());
                }
            });

            final Map<PairKey<Long, Long>, EppWeekType> dataMap = new HashMap<PairKey<Long, Long>, EppWeekType>();
            scheduleDataSource.setDataMap(dataMap);

            final Map<Integer, EppYearEducationWeek> yearWeekMap = this.getYearWeekMap4Plan(workplan);

            int columnIndex = 0;
            for (final EppWorkGraphRowWeek graphWeek: graphWeeks) {
                graphWeekCodeMap.put(columnIndex, graphWeek.getType().getCode());

                final Long weekId = graphWeek.getId();
                dataMap.put(PairKey.create(workplan.getId(), weekId), graphWeek.getType());

                final HeadColumn weekColumn = new HeadColumn("week"+String.valueOf(graphWeek.getWeek()), yearWeekMap.get(graphWeek.getWeek()).getTitle());
                weekColumn.setHeaderStyle("height:140px;");
                weekColumn.setVerticalHeader(true);

                final WeekTypeBlockColumn column = new WeekTypeBlockColumn(weekId, columnIndex, String.valueOf(graphWeek.getWeek()));
                column.setHeaderStyle("padding-left:0;padding-right:0;text-align:center;min-width:17px;font-size:11px;");
                column.setHasPermission(false);
                weekColumn.addColumn(column);

                dataSource.addColumn(weekColumn);
                columnIndex++;
            }

            final RichRangeSelection richRangeSelection = new RichRangeSelection(graphWeeks.size(), DAO.getPointsFromPartMap(partMap)) {
                @Override public boolean doPointClick(final int index) {
                    if (this._activePointIndex < 0) { return super.doPointClick(index); }
                    if (!super.doPointClick(index)) { return false; }

                    // нужно убрать пустоты
                    final int p = this._index[index];
                    if (0 == (p%2)) {
                        if (p>0) { this._points[p-1]=index-1; }
                    } else {
                        if (p<(this._points.length-1)) { this._points[p+1]=index+1; }
                    }

                    // прижать крайние точки к концам
                    this._points[0] = 0;
                    this._points[this._points.length-1] = (this._data.length-1);

                    // и еще раз пообдейтить
                    this.update();

                    // и обновить то, что было в модели
                    DAO.updatePartMapFromPoints(partMap, this._points);

                    // обновление прошло успешно
                    return true;
                }
            };

            scheduleDataSource.setSelection(richRangeSelection);
            scheduleDataSource.setEditId(workplan.getId());
        }
    }

    private static int[] getPointsFromPartMap(final Map<Integer, EppWorkPlanPart> partMap) {
        final int points[] = new int[partMap.size()*2];
        DAO.updatePointsFromPartMap(partMap, points);
        return points;
    }

    private static void updatePartMapFromPoints(final Map<Integer, EppWorkPlanPart> partMap, final int[] points) {
        for (int i=0;i<points.length;i+=2) {
            final EppWorkPlanPart part = partMap.get(1 + (i / 2));
            part.setTotalWeeks((1+points[i+1])-points[i]);
        }
        DAO.updatePointsFromPartMap(partMap, points);
    }

    private static void updatePointsFromPartMap(final Map<Integer, EppWorkPlanPart> partMap, final int[] points) {
        int index = 0, position = 0;
        for (final EppWorkPlanPart part: partMap.values()) {
            points[index++] = position;
            position += (part.getTotalWeeks()-1);

            points[index++] = position;
            position += 1;
        }
    }

    private Map<Integer, EppYearEducationWeek> getYearWeekMap4Plan(final EppWorkPlan workplan) {
        final List<EppYearEducationWeek> yearWeeks = this.getList(EppYearEducationWeek.class, EppYearEducationWeekGen.year().s(), workplan.getYear());
        final Map<Integer, EppYearEducationWeek> map = new HashMap<Integer, EppYearEducationWeek>();
        for (final EppYearEducationWeek week: yearWeeks) {
            map.put(week.getNumber(), week);
        }
        return map;
    }

    @Override
    public void save(final Model model) {
        IEppWorkPlanDAO.instance.get().doUpdateWorkPlanPartSet(
            model.getElement().getId(),
            new HashSet<EppWorkPlanPart>(model.getPartMap().values())
        );
    }


}
