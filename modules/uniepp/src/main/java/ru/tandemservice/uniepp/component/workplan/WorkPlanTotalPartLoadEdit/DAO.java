package ru.tandemservice.uniepp.component.workplan.WorkPlanTotalPartLoadEdit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanPartGen;

/**
 * 
 * @author nkokorina
 *
 */

public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        final EppWorkPlanRegistryElementRow row = model.getRowHolder().refresh(EppWorkPlanRegistryElementRow.class);
        row.getWorkPlan().getState().check_editable(row.getWorkPlan());

        model.setRegistryElementPartWrapper(
                IEppRegistryDAO.instance.get().getRegistryElementPartDataMap(
                        Collections.singleton(row.getRegistryElementPart().getId())
                ).get(row.getRegistryElementPart().getId())
        );

        final Set<EppLoadType> loadTypes = new LinkedHashSet<EppLoadType>();
        loadTypes.add(this.getCatalogItem(EppELoadType.class, EppELoadType.TYPE_TOTAL_AUDIT));
        loadTypes.addAll(this.getCatalogItemListOrderByCode(EppALoadType.class));
        loadTypes.addAll(this.getCatalogItemListOrderByCode(EppELoadType.class));
        model.setLoadTypeList(new ArrayList<EppLoadType>(loadTypes));

        final List<EppWorkPlanPart> partsList = this.getList(EppWorkPlanPart.class, EppWorkPlanPartGen.workPlan().id().s(), row.getWorkPlan().getWorkPlan().getId(), EppWorkPlanPartGen.number().s());
        if (partsList.isEmpty())
        {
            throw new ApplicationException("В рабочем учебном плане не указаны части.");
        }
        model.setPartsList(partsList);

        model.getPartLoadMap().clear();
        model.getLoadCode2useCycle().clear();

        final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> map = IEppWorkPlanDataDAO.instance.get().getRowPartLoadDataMap(Collections.singleton(row.getId())).get(row.getId());
        if (null != map)  {
            model.getPartLoadMap().putAll(map);
            this.fillLoadCode2useCycleMap(model, map);
        }
    }

    // заполняем мар с признаком цикловой нагрузки, если такая имеется
    private void fillLoadCode2useCycleMap(final Model model, final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> map)
    {
        for (final Map<String, EppWorkPlanRowPartLoad> value : map.values())
        {
            for (final EppWorkPlanRowPartLoad load : value.values())
            {
                if (null != load.getDaysAsDouble())
                {
                    model.getLoadCode2useCycle().put(load.getLoadType().getFullCode(), Model.SECOND_OPTION_WRAPPER);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(final Model model)
    {
        // значения переключателей на форме
        final Map<String, IdentifiableWrapper> loadCode2useCycle = model.getLoadCode2useCycle();

        // новые значения, введенные пользователем
        final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> partLoadMap = model.getPartLoadMap();

        // удаляем значения дней, где не используется цикловая нагрузка
        for (final Map<String, EppWorkPlanRowPartLoad> value : partLoadMap.values())
        {
            for (final EppWorkPlanRowPartLoad load : value.values())
            {
                final String fullCode = load.getLoadType().getFullCode();
                final IdentifiableWrapper wrapper = loadCode2useCycle.get(fullCode);
                if (!((null != wrapper) && (Model.SECOND_OPTION_ID == wrapper.getId())))
                {
                    load.setDaysAsDouble(null);
                }
            }
        }

        IEppWorkPlanDataDAO.instance.get().doUpdateRowPartLoadElementsMap(Collections.singletonMap(model.getId(), partLoadMap));
    }
}
