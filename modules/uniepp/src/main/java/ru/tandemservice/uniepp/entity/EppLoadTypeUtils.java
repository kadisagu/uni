package ru.tandemservice.uniepp.entity;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.collections15.CollectionUtils;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author vdanilov
 */
public class EppLoadTypeUtils {

    private static final Set<String> XLOAD_FULL_CODES = ImmutableSet.of(
            EppLoadType.FULL_CODE_TOTAL_HOURS,
            EppLoadType.FULL_CODE_LABOR,
            EppLoadType.FULL_CODE_WEEKS,
            EppLoadType.FULL_CODE_CONTROL
    );

    /* @return true, если нагрузка не является учебной
    public static boolean isXLoad(final String loadFullCode) {
        return EppLoadTypeUtils.XLOAD_FULL_CODES.contains(loadFullCode);
    } */


    private static final Map<String, String> LOAD_TYPE_2_ELOADTYPE_CODE;
    static {
        final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        EppALoadType.FULL_CODES.forEach(fullCode -> builder.put(fullCode, EppELoadType.FULL_CODE_AUDIT));
        EppELoadType.FULL_CODES.forEach(fullCode -> builder.put(fullCode, fullCode));
        LOAD_TYPE_2_ELOADTYPE_CODE = builder.build();
    }

    /** @return полный код учебной наргузки по полному коду нагрузки (loadtype.fullCode -> eloadtype.fullcode) */
    public static String getELoadTypeFullCode(final String loadFullCode) {
        return EppLoadTypeUtils.LOAD_TYPE_2_ELOADTYPE_CODE.get(loadFullCode);
    }

    public static final Double ZERO = (double) 0f;

    private static final Map<String, Double> RESULT_MAP_PROTOTYPE;
    static {
        final ImmutableMap.Builder<String, Double> builder = ImmutableMap.builder();
        EppALoadType.FULL_CODES.forEach(fullCode -> {
            builder.put(fullCode, ZERO);
            builder.put(EppALoadType.iFullCode(fullCode), ZERO);
            builder.put(EppALoadType.eFullCode(fullCode), ZERO);
        });
        EppELoadType.FULL_CODES.forEach(fullCode -> builder.put(fullCode, ZERO));
        EppLoadTypeUtils.XLOAD_FULL_CODES.forEach(fullCode -> builder.put(fullCode, ZERO));
        RESULT_MAP_PROTOTYPE = builder.build();
    }

    /** @return справочник нагрузки с 0 значениями по нагрузке */
    public static Map<String, Double> newLoadMap() {
        return new HashMap<>(EppLoadTypeUtils.RESULT_MAP_PROTOTYPE); // так быстрее, чем по-одному добавлять (см. код hashMap)
    }

    // нагрузка по full-code
    public static Map<String, EppLoadType> getLoadTypeMap() {
        return EppEduPlanVersionDataDAO.getLoadTypeMap();
    }

    @SuppressWarnings("unchecked")
    public static Collection<EppALoadType> getALoadTypeList(Map<String, EppLoadType> loadTypeMap) {
        return (Collection) CollectionUtils.select(loadTypeMap.values(), t -> t instanceof EppALoadType);
    }

    @SuppressWarnings("unchecked")
    public static Collection<EppELoadType> getELoadTypeList(Map<String, EppLoadType> loadTypeMap) {
        return (Collection) CollectionUtils.select(loadTypeMap.values(), t -> t instanceof EppELoadType);
    }


}
