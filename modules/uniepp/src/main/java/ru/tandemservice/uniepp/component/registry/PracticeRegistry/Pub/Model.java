package ru.tandemservice.uniepp.component.registry.PracticeRegistry.Pub;

import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;

/**
 * 
 * @author nkokorina
 *
 */

public class Model extends ru.tandemservice.uniepp.component.registry.base.Pub.Model<EppRegistryPractice>
{
    @Override public Class<EppRegistryPractice> getElementClass() { return EppRegistryPractice.class; }
    @Override public String getPermissionPrefix() { return "eppPractice"; }
}
