package ru.tandemservice.uniepp.component.workgraph.WorkGraphPrint;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.biff.EmptyCell;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.gen.EmployeePostGen;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.util.*;
import java.util.List;

/**
 * @author nkokorina
 */

public class WorkGraphReportContentGenerator
{
    // create cell formats
    WritableFont tahoma8 = new WritableFont(WritableFont.TAHOMA, 8);
    WritableFont tahoma8grey = new WritableFont(WritableFont.TAHOMA, 8);
    WritableFont tahoma9 = new WritableFont(WritableFont.TAHOMA, 9);
    WritableFont tahoma10 = new WritableFont(WritableFont.TAHOMA, 10);
    WritableFont tahoma12bold = new WritableFont(WritableFont.TAHOMA, 12, WritableFont.BOLD);
    WritableFont tahoma14 = new WritableFont(WritableFont.TAHOMA, 14);
    WritableFont tahoma18 = new WritableFont(WritableFont.TAHOMA, 18);

    public ByteArrayOutputStream generateReportContent(final WorkGraphPrintVersion printVersion) throws IOException, WriteException
    {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        final WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);

        final WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // Modify the colour palette
        // мап переопределенных цветов
        final Map<Long, Colour> colorMap = new HashMap<>();

        // цвет заливки месяцев в шапке таблицы, он определен заранее будет под 10 номером
        final Color monthColor = Color.decode(String.valueOf("0xccccff"));
        workbook.setColourRGB(Colour.getAllColours()[10], monthColor.getRed(), monthColor.getGreen(), monthColor.getBlue());
        colorMap.put(1L, Colour.getAllColours()[10]);
        //  цвет номеров недель и палок в шапке (в задаче они разные, но никто не увидит =) )
        final Color grayColor = Color.decode(String.valueOf("0x999999"));
        workbook.setColourRGB(Colour.getAllColours()[11], grayColor.getRed(), grayColor.getGreen(), grayColor.getBlue());
        colorMap.put(2L, Colour.getAllColours()[11]);
        // цвет заливки яцейки с палкой в шапке таблицы
        final Color grayFillColor = Color.decode(String.valueOf("0xe6e6e6"));
        workbook.setColourRGB(Colour.getAllColours()[12], grayFillColor.getRed(), grayFillColor.getGreen(), grayFillColor.getBlue());
        colorMap.put(3L, Colour.getAllColours()[12]);

        // цвет номеров недель
        this.tahoma8grey.setColour(colorMap.get(2L));

        // список цветов недель
        final List<Long> colorList = new ArrayList<>(printVersion.getWeekType2color().values());
        for (int i = 0; i < colorList.size(); i++)
        {
            final Long color = colorList.get(i);
            if (null != color)
            {
                final Color realColor = Color.decode(String.valueOf(color));
                workbook.setColourRGB(Colour.getAllColours()[13 + i], realColor.getRed(), realColor.getGreen(), realColor.getBlue());
                colorMap.put(color, Colour.getAllColours()[13 + i]);
            }
            else
            {
                colorMap.put(color, Colour.WHITE);
            }
        }

        final Map<String, String> developFormMap = new HashMap<>();
        developFormMap.put("1", "Очное обучение");
        developFormMap.put("2", "Заочное обучение");
        developFormMap.put("3", "Очно-заочное обучение");
        developFormMap.put("4", "Экстернат");
        developFormMap.put("5", "Самостоятельное обучение и итоговая аттестация");

        final String developForm = developFormMap.get(printVersion.getWorkGraph().getDevelopForm().getCode());

        // create list
        final WritableSheet sheet = workbook.createSheet(developForm, 0);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);

        // текущий учебный год
        final String currentYear = printVersion.getWorkGraph().getYear().getTitle();
        // число учебных недель
        final int weeksCount = printVersion.getWeeks().length;
        // считаем число недель в месяце (чтобы объединить ячейки)
        final LinkedHashMap<Integer, Integer> month2weeksCount = this.getMonth2weeksCount(printVersion.getWeeks());

        // устанавливаем ширину в 10 символов для первой колонки
        sheet.setColumnView(0, 10);

        // расставляем ширину строк для таблицы в 2 символа
        for (int i = 0; i < weeksCount; i++)
        {
            sheet.setColumnView(i + 1, 2);
        }

        // заполняем шапку
        final String postTitle = (String)FastBeanUtils.getValue(printVersion.getAcademyHead(), EmployeePostGen.postRelation().postBoundedWithQGandQL().post().title().s());
        final String academyHead = StringUtils.isEmpty(postTitle) ? "ректор " : postTitle + " ";
        final String academyHeadTitle = (null != postTitle) ? StringUtils.trimToEmpty(printVersion.getAcademyHead().getEmployee().getPerson().getFio()) : "";

        // формат для заголовка с реквизитами
        final WritableCellFormat headFormat = new WritableCellFormat(this.tahoma10);
        headFormat.setAlignment(Alignment.LEFT);
        // формат ячейки с заголовке с подчеркиванием
        final WritableCellFormat headLineFormat = new WritableCellFormat(this.tahoma10);
        headLineFormat.setBorder(Border.BOTTOM, BorderLineStyle.HAIR);

        sheet.addCell(new Label(0, 0, "Утверждаю", headFormat));
        sheet.addCell(new Label(0, 1, academyHead.toLowerCase() + printVersion.getAcademyTitle(), headFormat));
        sheet.addCell(new Label(0, 2, "", headLineFormat));
        sheet.addCell(new Label(1, 2, academyHeadTitle, headFormat));

        // заголовок первого уровня
        // формат для заголовка с названием ГУПа
        final WritableCellFormat titleFormat = new WritableCellFormat(this.tahoma18);
        titleFormat.setAlignment(Alignment.CENTRE);
        titleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        sheet.addCell(new Label(1, 3, "График учебного процесса на " + currentYear + " учебный год", titleFormat));
        sheet.setRowView(3, 700); // выставляем высоту ряда
        sheet.mergeCells(1, 3, weeksCount, 3);// объединяем ячейки

        // заголовок второго уровня
        final WritableCellFormat titleFormFormat = new WritableCellFormat(this.tahoma14);
        titleFormFormat.setAlignment(Alignment.CENTRE);
        titleFormFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        sheet.addCell(new Label(1, 4, developForm, titleFormFormat));
        sheet.mergeCells(1, 4, weeksCount, 4);
        sheet.setRowView(4, 500);

        // вставляем пустую строку
        sheet.addCell(new EmptyCell(0, 4));

        // тут должен начаться цикл
        // высота предыдущих таблиц
        int height = 0;
        // ряд с которого начинается формирование таблицы
        int beginRowNum;
        // число строк в таблице с курсами или специальностями
        int rowTablecount;
        for (final Long tableId : printVersion.getId2tableTitle().keySet())
        {
            // ряд с которого начинается формирование таблицы
            beginRowNum = 6 + height;
            // число строк в таблице с курсами или специальностями
            rowTablecount = printVersion.getId2idRow2weeksList().get(tableId).size();

            // подпись к таблице
            // формат подписи к таблице
            final WritableCellFormat tableTitleFormat = new WritableCellFormat(this.tahoma12bold);
            tableTitleFormat.setAlignment(Alignment.CENTRE);
            tableTitleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

            final String tableTitle = printVersion.getId2tableTitle().get(tableId);
            sheet.addCell(new Label(0, beginRowNum, tableTitle, tableTitleFormat));
            sheet.mergeCells(0, beginRowNum, weeksCount + 2, beginRowNum);
            sheet.setRowView(beginRowNum, 300);

            // вставляем пустую строку
            sheet.addCell(new EmptyCell(0, beginRowNum + 1));

            // учебный год в шапке таблицы в ее начале
            // формат учеек с годом
            final WritableCellFormat tableYearFormat = new WritableCellFormat(this.tahoma8);
            tableYearFormat.setAlignment(Alignment.CENTRE);
            tableYearFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            tableYearFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

            sheet.mergeCells(0, beginRowNum + 2, 0, beginRowNum + 5);
            sheet.addCell(new Label(0, beginRowNum + 2, currentYear, tableYearFormat));

            // колонки с названием месяца в шапке таблицы
            // формат учеек с названием месяца без заливки
            final WritableCellFormat monthTitleFormat = new WritableCellFormat(this.tahoma8);
            monthTitleFormat.setAlignment(Alignment.CENTRE);
            monthTitleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            monthTitleFormat.setBorder(Border.TOP, BorderLineStyle.THIN);

            // формат учеек с названием месяца с заливкой
            final WritableCellFormat monthFillTitleFormat = new WritableCellFormat(this.tahoma8);
            monthFillTitleFormat.setAlignment(Alignment.CENTRE);
            monthFillTitleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            monthFillTitleFormat.setBorder(Border.TOP, BorderLineStyle.THIN);
            monthFillTitleFormat.setBackground(colorMap.get(1L));

            // номер месяца -> необходимость заливки
            boolean fill = false;
            final Map<Integer, Boolean> month2fill = new HashMap<>();
            for (final Integer month : month2weeksCount.keySet())
            {
                month2fill.put(month, fill);
                fill = !fill;
            }

            // заполняем учейки с на званием месяца в шапке таблицы
            int startCol = 1;
            for (final Integer month : month2weeksCount.keySet())
            {
                final int mergeCount = month2weeksCount.get(month);
                final String monthTitle = RussianDateFormatUtils.getMonthName(month, true);
                sheet.addCell(new Label(startCol, beginRowNum + 2, monthTitle, month2fill.get(month) ? monthFillTitleFormat : monthTitleFormat));
                sheet.mergeCells(startCol, beginRowNum + 2, (startCol + mergeCount) - 1, beginRowNum + 2);
                startCol += mergeCount;
            }

            // учебный год в шапке таблицы и в конце
            sheet.mergeCells(weeksCount + 1, beginRowNum + 2, weeksCount + 1, beginRowNum + 5);
            sheet.setColumnView(weeksCount + 1, 12);
            sheet.addCell(new Label(weeksCount + 1, beginRowNum + 2, currentYear, tableYearFormat));

            // формат учеек с числом месяца
            final WritableCellFormat monthNumFormat = new WritableCellFormat(this.tahoma8);
            monthNumFormat.setAlignment(Alignment.CENTRE);
            monthNumFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

            // формат учеек с числом месяца c заливкой
            final WritableCellFormat monthNumFillFormat = new WritableCellFormat(this.tahoma8);
            monthNumFillFormat.setAlignment(Alignment.CENTRE);
            monthNumFillFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            monthNumFillFormat.setBackground(colorMap.get(1L));

            // формат учеек с числом месяца с нижней границей
            final WritableCellFormat monthNumBotFormat = new WritableCellFormat(this.tahoma8);
            monthNumBotFormat.setAlignment(Alignment.CENTRE);
            monthNumBotFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            monthNumBotFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN);

            // формат учеек с числом месяца с нижней границей
            final WritableCellFormat monthNumFillBotFormat = new WritableCellFormat(this.tahoma8);
            monthNumFillBotFormat.setAlignment(Alignment.CENTRE);
            monthNumFillBotFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            monthNumFillBotFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
            monthNumFillBotFormat.setBackground(colorMap.get(1L));

            // заполняем недели в шапке таблицы
            for (int i = 0; i < printVersion.getWeeks().length; i++)
            {
                final Date startDate = printVersion.getWeeks()[i].getDate();

                final Calendar calendar = Calendar.getInstance();
                calendar.setTime(WorkGraphReportContentGenerator.moveDate(startDate));
                calendar.add(Calendar.DAY_OF_YEAR, -1);

                final Date endDate = calendar.getTime();

                final int monthStartDate = CommonBaseDateUtil.getMonthStartingWithOne(startDate);
                final int monthEndDate = CommonBaseDateUtil.getMonthStartingWithOne(endDate);
                final int dayStartDate = CoreDateUtils.getDayOfMonth(startDate);
                final int dayEndDate = CoreDateUtils.getDayOfMonth(endDate);

                sheet.addCell(new Number(i + 1, beginRowNum + 3, dayStartDate, month2fill.get(monthStartDate) ? monthNumFillFormat : monthNumFormat));
                sheet.addCell(new Label(i + 1, beginRowNum + 4, "|", this.getStickFormat(colorMap, month2fill, startDate, endDate)));
                sheet.addCell(new Number(i + 1, beginRowNum + 5, dayEndDate, month2fill.get(monthEndDate) ? monthNumFillBotFormat : monthNumBotFormat));

            }

            // вставляем пустую строку
            sheet.addCell(new EmptyCell(0, beginRowNum + 6));

            // таблица с неделями
            // формат учеек с крайних текстом "№ недели"
            final WritableCellFormat tableNumWeekFormatExtr = new WritableCellFormat(this.tahoma9);
            tableNumWeekFormatExtr.setAlignment(Alignment.LEFT);
            tableNumWeekFormatExtr.setVerticalAlignment(VerticalAlignment.CENTRE);
            tableNumWeekFormatExtr.setBorder(Border.ALL, BorderLineStyle.THIN);

            // формат учеек с собственно номером недели
            final WritableCellFormat tableNumWeekFormat = new WritableCellFormat(this.tahoma8);
            tableNumWeekFormat.setAlignment(Alignment.CENTRE);
            tableNumWeekFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            tableNumWeekFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

            // формат учеек с собственно номером недели
            final WritableCellFormat tableTitleRowFormat = new WritableCellFormat(this.tahoma8);
            tableTitleRowFormat.setAlignment(Alignment.LEFT);
            tableTitleRowFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            tableTitleRowFormat.setBorder(Border.TOP, BorderLineStyle.HAIR);
            tableTitleRowFormat.setBorder(Border.BOTTOM, BorderLineStyle.HAIR);
            tableTitleRowFormat.setBorder(Border.LEFT, BorderLineStyle.THIN);
            tableTitleRowFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN);

            // формат учеек с обозначение недели в таблице недель
            final WritableCellFormat tableWeekBaseFormat = new WritableCellFormat(this.tahoma8grey);
            tableWeekBaseFormat.setAlignment(Alignment.CENTRE);
            tableWeekBaseFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

            sheet.addCell(new Label(0, beginRowNum + 7, "№ недели", tableNumWeekFormatExtr));
            for (int i = 0; i < weeksCount; i++)
            {
                sheet.addCell(new Number(i + 1, beginRowNum + 7, printVersion.getWeeks()[i].getNumber(), tableNumWeekFormat));
                sheet.setColumnView(i + 1, 3);
            }
            sheet.addCell(new Label(weeksCount + 1, beginRowNum + 7, "№ недели", tableNumWeekFormatExtr));

            // тут в зависимости от группировки рисуем либо курс либо краткое название специальности
            final Map<Long, List<String>> idRow2rowData = printVersion.getId2idRow2weeksList().get(tableId);
            final List<Long> idRowList = new ArrayList<>(idRow2rowData.keySet());

            for (int i = 0; i < rowTablecount; i++)
            {
                final String rowTitle = printVersion.getIdRow2title().get(idRowList.get(i));
                sheet.addCell(new Label(0, beginRowNum + 8 + i, rowTitle, tableTitleRowFormat));
                // ячейки недель - строка
                final List<String> weeksList = idRow2rowData.get(idRowList.get(i));
                for (int j = 0; j < weeksList.size(); j++)
                {
                    final String week = weeksList.get(j);
                    if (printVersion.getWeekType2color().containsKey(week))
                    {
                        // каждый раз создаем формат заново
                        final WritableCellFormat weekTypeFormat = new WritableCellFormat(this.tahoma9);
                        weekTypeFormat.setAlignment(Alignment.CENTRE);
                        weekTypeFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
                        weekTypeFormat.setBackground(colorMap.get(printVersion.getWeekType2color().get(week)));

                        sheet.addCell(new Label(j + 1, beginRowNum + 8 + i, week, weekTypeFormat));
                    }
                    else
                    {
                        sheet.addCell(new Label(j + 1, beginRowNum + 8 + i, week, tableWeekBaseFormat));
                    }
                }
                sheet.addCell(new Label(weeksCount + 1, beginRowNum + 8 + i, rowTitle, tableTitleRowFormat));
            }

            // последняя строчка
            sheet.addCell(new Label(0, beginRowNum + rowTablecount + 8, "№ недели", tableNumWeekFormatExtr));
            for (int i = 0; i < weeksCount; i++)
            {
                sheet.addCell(new Number(i + 1, beginRowNum + rowTablecount + 8, printVersion.getWeeks()[i].getNumber(), tableNumWeekFormat));
                sheet.setColumnView(i + 1, 3);
            }
            sheet.addCell(new Label(weeksCount + 1, beginRowNum + rowTablecount + 8, "№ недели", tableNumWeekFormatExtr));

            // вставляем пустую строку
            sheet.addCell(new EmptyCell(0, beginRowNum + rowTablecount + 9));

            height += 10 + rowTablecount;
        }

        // легенда
        final int legendRow = 6 + height;

        // формат учейки с названием цвета легенды
        final WritableCellFormat weekTypeTitleCellFormat = new WritableCellFormat(this.tahoma9);
        weekTypeTitleCellFormat.setAlignment(Alignment.LEFT);

        final List<String> legendKey = new ArrayList<>(printVersion.getWeekType2color().keySet());
        for (int i = 0; i < legendKey.size(); i++)
        {
            final String weekType = legendKey.get(i);
            final String weekTitle = printVersion.getWeekType2fullTitle().get(weekType);

            // формат учейки с цветом легенды
            final WritableCellFormat weekTypeCellFormat = new WritableCellFormat(this.tahoma9);
            weekTypeCellFormat.setAlignment(Alignment.CENTRE);
            weekTypeCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            weekTypeCellFormat.setBackground(colorMap.get(printVersion.getWeekType2color().get(weekType)));
            weekTypeCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR);

            final int column = (i % 3) * 20;
            final int row = Math.round(i / 3);
            sheet.addCell(new Label(1 + column, legendRow + 1 + row, weekType, weekTypeCellFormat));
            sheet.addCell(new Label(3 + column, legendRow + 1 + row, weekTitle, weekTypeTitleCellFormat));

        }

        workbook.write();
        workbook.close();

        out.close();

        return out;
    }

    private static Date moveDate(final Date date)
    {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, 1);

        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY)
        {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }

        return calendar.getTime();
    }

    // считаем число недель в месяце (чтобы объединить ячейки)
    private LinkedHashMap<Integer, Integer> getMonth2weeksCount(final EppYearEducationWeek[] weeks)
    {
        final int weeksCount = weeks.length;

        final LinkedHashMap<Integer, Integer> month2weeksCount = new LinkedHashMap<>();
        for (int i = 0; i < weeksCount; i++)
        {
            final int month = CommonBaseDateUtil.getMonthStartingWithOne(weeks[i].getDate());
            if (month2weeksCount.containsKey(month))
            {
                int weekCount = month2weeksCount.get(month);
                weekCount++;
                month2weeksCount.put(month, weekCount);
            }
            else
            {
                month2weeksCount.put(month, 1);
            }
        }
        return month2weeksCount;
    }

    private WritableCellFormat getStickFormat(final Map<Long, Colour> colorMap, final Map<Integer, Boolean> month2fill, final Date startDate, final Date endDate) throws WriteException
    {
        // формат ячейки с палкой в шапке таблицы
        final WritableCellFormat stickFormat = new WritableCellFormat(this.tahoma8grey);
        stickFormat.setAlignment(Alignment.CENTRE);
        stickFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        final WritableCellFormat stickFillFormat = new WritableCellFormat(this.tahoma8grey);
        stickFillFormat.setAlignment(Alignment.CENTRE);
        stickFillFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        stickFillFormat.setBackground(colorMap.get(1L));

        final WritableCellFormat stickFillGrayFormat = new WritableCellFormat(this.tahoma8grey);
        stickFillGrayFormat.setAlignment(Alignment.CENTRE);
        stickFillGrayFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        stickFillGrayFormat.setBackground(colorMap.get(3L));

        final int monthOfNextDay = CommonBaseDateUtil.getMonthStartingWithOne(CoreDateUtils.add(startDate, Calendar.DAY_OF_YEAR, 1));
        final int monthStartDate = CommonBaseDateUtil.getMonthStartingWithOne(startDate);
        final int monthEndDate = CommonBaseDateUtil.getMonthStartingWithOne(endDate);

        if ((month2fill.get(monthStartDate)))
        {
            return stickFillFormat;
        }
        if (!month2fill.get(monthStartDate) && month2fill.get(monthEndDate) && (monthOfNextDay != monthEndDate))
        {
            return stickFillGrayFormat;
        }

        return stickFormat;

    }

}
