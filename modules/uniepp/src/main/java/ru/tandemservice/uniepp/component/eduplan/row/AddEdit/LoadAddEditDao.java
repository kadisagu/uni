package ru.tandemservice.uniepp.component.eduplan.row.AddEdit;

import java.util.Collections;
import java.util.HashMap;

import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

/**
 * @author vdanilov
 */
public class LoadAddEditDao<T extends EppEpvTermDistributedRow, M extends LoadAddEditModel<T>> extends BaseAddEditDao<T, M> implements IPrepareable<M>
{
    public void prepareLoadMap(M model)
    {
        model.setLoadTypeMap(EppLoadTypeUtils.getLoadTypeMap());

        final Long rowId = model.getRow().getId();
        if (null != rowId) {
            model.setLoadMap(IEppEduPlanVersionDataDAO.instance.get().getRowLoad(rowId));
        }

        if (null == model.getLoadMap()) {
            model.setLoadMap(new HashMap<String, Double>());
        }
    }

    @Override
    public void prepare(final M model)
    {
        super.prepare(model);
        this.prepareLoadMap(model);
    }

    @Override
    public T doSaveRow(M model)
    {
        T row = super.doSaveRow(model);
        // IEppEduPlanVersionDataDAO.instance.get().doUpdateRowLoadMap(Collections.singletonMap(row.getId(), model.getLoadMap()));
        return row;
    }
}
