package ru.tandemservice.uniepp.component.student.StudentWorkPlanEdit;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;

import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.component.workplan.WorkPlanData.WorkPlanDataBaseDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

public class DAO extends WorkPlanDataBaseDAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.getHolder().refresh(EppStudent2WorkPlan.class);
        model.setSec(new CommonPostfixPermissionModel("eppWorkPlan_student"));
        super.prepare(model);
    }

    @Override
    public void doAccept(final Model model) {
        final EppStudent2WorkPlan s2wp = model.getHolder().getValue();
        try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler()))
        {
            s2wp.getWorkPlan().setState(this.getCatalogItem(EppState.class, EppState.STATE_ACCEPTED));  // выставляем сразу согласование
            this.getSession().flush();
        }

        final Long sepvId = s2wp.getStudentEduPlanVersion().getId();
        final Set<EppStudent2WorkPlan> set = IEppWorkPlanDAO.instance.get().getStudentEpvWorkPlanMap(Collections.singleton(sepvId), true).get(sepvId);
        final Map<Integer, EppWorkPlanBase> map = new HashMap<>(set.size());
        for (final EppStudent2WorkPlan rel: set) {
            map.put(rel.getWorkPlan().getTerm().getIntValue(), rel.getWorkPlan());
        }

        map.put(s2wp.getWorkPlan().getTerm().getIntValue(), s2wp.getWorkPlan());

        IEppWorkPlanDAO.instance.get().doUpdateStudentEpvWorkPlan(
            Collections.<Long, Set<EppWorkPlanBase>>singletonMap(
                sepvId, new HashSet<>(map.values())
            )
        );

        IEppWorkPlanDAO.instance.get().doCleanUpWorkPlanTemporaryVersions(s2wp.getWorkPlan().getWorkPlan().getId());
    }

    @Override
    public void doReject(final Model model) {
        final EppStudent2WorkPlan s2wp = model.getHolder().getValue();
        try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler()))
        {
            s2wp.getWorkPlan().setState(this.getCatalogItem(EppState.class, EppState.STATE_REJECTED));  // выставляем сразу отклонение
            this.getSession().flush();
        }

        this.delete(s2wp);
        this.getSession().flush();

        IEppWorkPlanDAO.instance.get().doCleanUpWorkPlanTemporaryVersions(s2wp.getWorkPlan().getWorkPlan().getId());
    }
}
