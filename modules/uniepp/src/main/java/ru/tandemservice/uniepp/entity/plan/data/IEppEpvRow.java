package ru.tandemservice.uniepp.entity.plan.data;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.uniepp.dao.index.IEppIndexedRow;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * Строка блоку УП (дисциплина, цикл, индекс, компонент, группа, ...)
 * @author vdanilov
 */
public interface IEppEpvRow extends IEppIndexedRow, IHierarchyItem, ITitled, IEntity {

    /** @return блок версии УП (направление подготовки), в рамках которого создан элемент */
    EppEduPlanVersionBlock getOwner();

    /** @return родительский элемент */
    @Override IEppEpvRow getHierarhyParent();

    /** @return тип элемиента в структуре реестра дисциплин */
    EppRegistryStructure getType();

    /** @return тип строки */
    String getRowType();

    /** @return Учитывать в нагрузке*/
    Boolean getUsedInLoad();

    /** @return Учитывать в контрольных мероприятиях*/
    Boolean getUsedInActions();

    /** @return индекс в базе (если задан) */
    String getStoredIndex();

    /** @return индекс, введенный пользователем (если задан) */
    String getUserIndex();

    /** @return класс строки */
    Class getRowEntityClass();
}
