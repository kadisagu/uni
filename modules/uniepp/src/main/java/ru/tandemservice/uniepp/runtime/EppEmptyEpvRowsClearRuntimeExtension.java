/* $Id:$ */
package ru.tandemservice.uniepp.runtime;

import org.tandemframework.core.runtime.IRuntimeExtension;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermLoad;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 11/19/14
 */
public class EppEmptyEpvRowsClearRuntimeExtension implements IRuntimeExtension
{
    @Override
    public void init(Object object)
    {

        DataAccessServices.dao().doInTransaction(session -> {
            DQLDeleteBuilder dql = new DQLDeleteBuilder(EppEpvRowTerm.class)
                .where(le(property(EppEpvRowTerm.hoursTotal()), value(0)))
                .where(le(property(EppEpvRowTerm.labor()), value(0)))
                .where(le(property(EppEpvRowTerm.weeks()), value(0)))
                .where(le(property(EppEpvRowTerm.hoursAudit()), value(0)))
                .where(le(property(EppEpvRowTerm.hoursSelfwork()), value(0)))
                .where(le(property(EppEpvRowTerm.hoursControl()), value(0)))
                .where(le(property(EppEpvRowTerm.hoursControlE()), value(0)))
                .where(notIn("id", new DQLSelectBuilder()
                    .fromEntity(EppEpvRowTermLoad.class, "l")
                    .column(property("l", EppEpvRowTermLoad.rowTerm().id()))
                    .where(gt(property("l", EppEpvRowTermLoad.hours()), value(0)))
                    .where(gt(property("l", EppEpvRowTermLoad.hoursE()), value(0)))
                    .where(gt(property("l", EppEpvRowTermLoad.hoursI()), value(0)))
                    .buildQuery()))
                .where(notIn("id", new DQLSelectBuilder()
                    .fromEntity(EppEpvRowTermAction.class, "a")
                    .column(property("a", EppEpvRowTermAction.rowTerm().id()))
                    .where(gt(property("a", EppEpvRowTermAction.size()), value(0)))
                    .buildQuery()))
                ;

            int count = dql.createStatement(session).execute();
            if (count > 0)
                System.out.println("Empty epv row terms deleted: " + count);
            return null;
        });

    }

    @Override
    public void destroy()
    {
    }
}

