package ru.tandemservice.uniepp.component.student.StudentEduplanWorkGraph;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.util.EppEduPlanVersionScheduleUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author nkokorina
 */

public class DAO extends UniBaseDao implements IDAO
{



    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        Debug.begin("prepare");
        try {

            final Student student = this.getNotNull(Student.class, model.getId());
            final EppStudent2EduPlanVersion relation = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());

            if (null != relation)
            {
                final EppEduPlanVersionScheduleUtils schedule = new EppEduPlanVersionScheduleUtils() {
                    @Override protected boolean isTotalCourseRowPresent() { return true; }
                    @Override protected EppEduPlanVersion getEduPlanVersion() { return relation.getEduPlanVersion(); }
                };

                model.setScheduleDataSource(schedule.getRangeSelectionListDataSource());
                final AbstractListDataSource<ViewWrapper<Course>> scheduleDataSource = schedule.getRangeSelectionListDataSource().getDataSource();
                final List<EppWeekType> weekTypeList = new ArrayList<>(schedule.getWeekTypeSet());

                {
                    // колонки по типам недель
                    Collections.sort(weekTypeList, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
                    for (final EppWeekType weekType : weekTypeList) {
                        final String caption = ((weekType.getId() > 0) ? (weekType.getShortTitle() + " (" + weekType.getAbbreviationView() + ")") : "Всего");
                        scheduleDataSource.addColumn(new SimpleColumn(caption, weekType.getCode()).setClickable(false).setOrderable(false).setVerticalHeader(true).setAlign("right"));
                    }

                    scheduleDataSource.setRowCustomizer(EppEduPlanVersionScheduleUtils.TOTAL_ROW_CUSTOMIZER);
                }

                {
                    // значения колонлок по типам недель
                    final Map<String, Map<String, Number>> weekTypeAggregationMap = schedule.getWeekTypeAggregationMap();
                    for (final ViewWrapper<Course> wrapper : scheduleDataSource.getEntityList())
                    {
                        final Map<String, Number> courseMap = weekTypeAggregationMap.get(wrapper.getEntity().getCode());
                        for (final EppWeekType type : weekTypeList)  {
                            wrapper.setViewProperty(type.getCode(), courseMap.get(type.getCode()));
                        }
                    }
                }
            }
        } finally {
            Debug.end();
        }
    }

}
