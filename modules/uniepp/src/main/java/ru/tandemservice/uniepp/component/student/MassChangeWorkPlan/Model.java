/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.student.MassChangeWorkPlan;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

/**
 * @author nkokorina
 * 
 */
@Input( { @Bind(key = "ids", binding = "studentIds", required = true) })
public class Model
{
    private List<Long> studentIds = Collections.emptyList();
    public void setStudentIds(final List<Long> studentIds) { this.studentIds = studentIds; }
    public List<Long> getStudentIds() { return this.studentIds; }

    private List<DevelopGridTerm> termList;
    public List<DevelopGridTerm> getTermList() { return this.termList;  }
    public void setTermList(final List<DevelopGridTerm> termList) { this.termList = termList; }

    private DevelopGridTerm currentTerm;
    public DevelopGridTerm getCurrentTerm() { return this.currentTerm; }
    public void setCurrentTerm(final DevelopGridTerm currentTerm) { this.currentTerm = currentTerm; }

    private Map<Integer, RowWrapper> rowTermMap = new HashMap<Integer, RowWrapper>();
    public Map<Integer, RowWrapper> getRowTermMap() { return this.rowTermMap; }
    public void setRowTermMap(final Map<Integer, RowWrapper> rowTermMap) { this.rowTermMap = rowTermMap; }

    protected RowWrapper getCurrentRow() {
        return this.getRowTermMap().get(this.getCurrentTerm().getTermNumber());
    }

    public ISelectModel getYearListModel() {
        return this.getCurrentRow().getYearListModel();
    }

    public ISelectModel getWorkplanListModel() {
        return this.getCurrentRow().getWorkplanListModel();
    }

    public EppWorkPlanBase getCurrentWorkPlan() {
        return this.getCurrentRow().getCurrentWorkPlan();
    }

    public void setCurrentWorkPlan(final EppWorkPlanBase workPlan) {
        this.getCurrentRow().setCurrentWorkPlan(workPlan);
    }

    public EppYearEducationProcess getCurrentYear() {
        return this.getCurrentRow().getCurrentYear();
    }

    public void setCurrentYear(final EppYearEducationProcess year) {
        this.getCurrentRow().setCurrentYear(year);
    }

    public boolean isCurrentDisabled() {
        return this.getCurrentRow().isDisabled();
    }

    public void setCurrentDisabled(final boolean disabled) {
        this.getCurrentRow().setDisabled(disabled);
    }

}
