package ru.tandemservice.uniepp.component.edustd.EduStdPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.edustd.bo.EppEduStandard.ui.AddEdit.EppEduStandardAddEdit;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickEditStateEduStandard(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                EppEduStandardAddEdit.class.getSimpleName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getEppStateEduStandard().getId())));
    }

    public void onClickDeleteStateEduStandard(final IBusinessComponent component)
    {
        UniDaoFacade.getCoreDao().delete(this.getModel(component).getEppStateEduStandard().getId());
        this.deactivate(component);
    }

    public void onClickChangeDirectionBlocks(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.edustd.EduStdBlockAddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getEppStateEduStandard().getId())
        ));

    }

    public void onClickViewFileEduStandard(final IBusinessComponent component)
    {
        final byte[] content = this.getModel(component).getEppStateEduStandard().getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл ГОСа пуст.");
        final Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Файл ГОСа");
        this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap()
        .add("id", id).add("zip", true)
        ));
    }

    public void onClickImportEduStandard(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.edustd.EduStdImport.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getEppStateEduStandard().getId())
        ));
    }
}
