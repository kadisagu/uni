/* $Id$ */
package ru.tandemservice.uniepp.component.edugroup.pub.GroupPubBase;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.component.edugroup.EduGroupOwnerModel;
import ru.tandemservice.uniepp.component.edugroup.pub.GroupContent.IGroupContentParameters;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;

/**
 * @author oleyba
 * @since 9/13/11
 */
@State({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="groupHolder.id", required = true),
    @Bind(key="orgUnitId", binding="orgUnitHolder.id")
})
public abstract class Model extends EduGroupOwnerModel implements IGroupContentParameters
{
    private final EntityHolder<EppRealEduGroup> groupHolder = new EntityHolder<EppRealEduGroup>();
    public EntityHolder<EppRealEduGroup> getGroupHolder() { return this.groupHolder; }

    @Override public EppRealEduGroup getGroup() { return this.getGroupHolder().getValue(); }
}
