package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Структура ГОС/УП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppPlanStructureGen extends EntityBase
 implements INaturalIdentifiable<EppPlanStructureGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppPlanStructure";
    public static final String ENTITY_NAME = "eppPlanStructure";
    public static final int VERSION_HASH = 720502188;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_PARENT = "parent";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_PRIORITY = "priority";
    public static final String P_TITLE = "title";
    public static final String P_HIERARCHY_TITLE = "hierarchyTitle";

    private String _code;     // Системный код
    private EppPlanStructure _parent;     // Структура ГОС/УП
    private String _shortTitle;     // Префикс для формирования индекса
    private int _priority;     // Приоритет
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Структура ГОС/УП.
     */
    public EppPlanStructure getParent()
    {
        return _parent;
    }

    /**
     * @param parent Структура ГОС/УП.
     */
    public void setParent(EppPlanStructure parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Префикс для формирования индекса.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Префикс для формирования индекса.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppPlanStructureGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EppPlanStructure)another).getCode());
            }
            setParent(((EppPlanStructure)another).getParent());
            setShortTitle(((EppPlanStructure)another).getShortTitle());
            setPriority(((EppPlanStructure)another).getPriority());
            setTitle(((EppPlanStructure)another).getTitle());
        }
    }

    public INaturalId<EppPlanStructureGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EppPlanStructureGen>
    {
        private static final String PROXY_NAME = "EppPlanStructureNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppPlanStructureGen.NaturalId) ) return false;

            EppPlanStructureGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppPlanStructureGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppPlanStructure.class;
        }

        public T newInstance()
        {
            return (T) new EppPlanStructure();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "parent":
                    return obj.getParent();
                case "shortTitle":
                    return obj.getShortTitle();
                case "priority":
                    return obj.getPriority();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "parent":
                    obj.setParent((EppPlanStructure) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "parent":
                        return true;
                case "shortTitle":
                        return true;
                case "priority":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "parent":
                    return true;
                case "shortTitle":
                    return true;
                case "priority":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "parent":
                    return EppPlanStructure.class;
                case "shortTitle":
                    return String.class;
                case "priority":
                    return Integer.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppPlanStructure> _dslPath = new Path<EppPlanStructure>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppPlanStructure");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppPlanStructure#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Структура ГОС/УП.
     * @see ru.tandemservice.uniepp.entity.catalog.EppPlanStructure#getParent()
     */
    public static EppPlanStructure.Path<EppPlanStructure> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Префикс для формирования индекса.
     * @see ru.tandemservice.uniepp.entity.catalog.EppPlanStructure#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppPlanStructure#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppPlanStructure#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppPlanStructure#getHierarchyTitle()
     */
    public static SupportedPropertyPath<String> hierarchyTitle()
    {
        return _dslPath.hierarchyTitle();
    }

    public static class Path<E extends EppPlanStructure> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EppPlanStructure.Path<EppPlanStructure> _parent;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Integer> _priority;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<String> _hierarchyTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppPlanStructure#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EppPlanStructureGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Структура ГОС/УП.
     * @see ru.tandemservice.uniepp.entity.catalog.EppPlanStructure#getParent()
     */
        public EppPlanStructure.Path<EppPlanStructure> parent()
        {
            if(_parent == null )
                _parent = new EppPlanStructure.Path<EppPlanStructure>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Префикс для формирования индекса.
     * @see ru.tandemservice.uniepp.entity.catalog.EppPlanStructure#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EppPlanStructureGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppPlanStructure#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EppPlanStructureGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppPlanStructure#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppPlanStructureGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppPlanStructure#getHierarchyTitle()
     */
        public SupportedPropertyPath<String> hierarchyTitle()
        {
            if(_hierarchyTitle == null )
                _hierarchyTitle = new SupportedPropertyPath<String>(EppPlanStructureGen.P_HIERARCHY_TITLE, this);
            return _hierarchyTitle;
        }

        public Class getEntityClass()
        {
            return EppPlanStructure.class;
        }

        public String getEntityName()
        {
            return "eppPlanStructure";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getHierarchyTitle();
}
