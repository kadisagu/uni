/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.List;

/**
 * @author Denis Katkov
 * @since 04.05.2016
 */
public interface IEppRegistryMassSplitDAO extends INeedPersistenceSupport
{
    /**
     * Обновляет связи мероприятия и ограничений делений по виду потока для каждого переданного элемента реестра.
     * @param registryElements Список элементов реестра.
     * @param groupTypes Список видов нагрузки.
     */
    void saveRegistryElementsSplitByTypeEduGroupRel(List<EppRegistryElement> registryElements, List<EppGroupType> groupTypes);

    /**
     * Обновляет элементы реестра. Принимается список элементов реестра, с измененными значения и он мерджится
     * с актуальным списком из бд. Изменения в принимаемом списке приоритетные.
     * @param registryElements Список элементов реестра.
     */
    void saveRegistryElements(List<EppRegistryElement> registryElements);
}