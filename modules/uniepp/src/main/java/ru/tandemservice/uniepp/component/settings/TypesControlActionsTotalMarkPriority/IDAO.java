package ru.tandemservice.uniepp.component.settings.TypesControlActionsTotalMarkPriority;

import org.tandemframework.core.view.list.column.IEntityHandler;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * 
 * @author nkokorina
 * @since 16.03.2010
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
    void updatePriorityUp(Long markValueId);
    void updatePriorityVeryUp(Long markValueId);
    void updatePriorityDown(Long markValueId);
    void updatePriorityVeryDown(Long markValueId);
    IEntityHandler getUpDisabledEntityHandler();
    IEntityHandler getDownDisabledEntityHandler();
}
