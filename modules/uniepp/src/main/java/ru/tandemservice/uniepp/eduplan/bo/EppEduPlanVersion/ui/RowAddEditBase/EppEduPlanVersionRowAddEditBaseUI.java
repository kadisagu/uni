/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAddEditBase;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAdd.EppEduPlanVersionRowAddUI;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author oleyba
 * @since 11/12/14
 */
public abstract class EppEduPlanVersionRowAddEditBaseUI<T extends EppEpvRow> extends UIPresenter
{
    public static final String BIND_ROW_ID = "rowId";
    public static final String BIND_BLOCK_ID = "blockId";
    public static final String FIELD_BLOCK_ID = "blockHolder.id";

    private EntityHolder<EppEduPlanVersionBlock> blockHolder = new EntityHolder<>();

    private List<HSelectOption> parentList = Collections.emptyList();
    private EppEpvRowWrapper parent;

    private Long scrollPos;

    public abstract T getRow();
    public abstract void setRow(T row);

    public abstract void onChangeParent();

    @Override
    public void onComponentRefresh()
    {
        if (getRow().getId() != null) {
            setRow((T) DataAccessServices.dao().getNotNull(getRow().getId()));
            getBlockHolder().setId(getRow().getOwner().getId());
        }
        getBlockHolder().refresh();
        if (getRow().getOwner() == null) getRow().setOwner(getBlockHolder().getValue());
    }

    public void onClickApply()
    {
        validate();

        final EppEpvRow row = getRow();

        // проверяем уникальность названия
        if (row instanceof EppEpvTermDistributedRow) {
            checkUniqTitle((EppEpvTermDistributedRow)row);
        }

        saveData();

        deactivate();

        if (_uiSupport.getParentUI() instanceof EppEduPlanVersionRowAddUI) {
            _uiSupport.getParentUI().deactivate();
        }
    }

    public void onClickCancel()
    {
        deactivate();

        if (_uiSupport.getParentUI() instanceof EppEduPlanVersionRowAddUI) {
            _uiSupport.getParentUI().deactivate();
        }
    }

    protected void validate() {
         // for override
    }

    protected void saveData() {
        IEppEduPlanVersionDataDAO.instance.get().doSaveOrUpdateRow(getRow(), null);
    }

    /**
     * DEV-4188: Необходимо на форме добавления элемента в блок УПв проверять уникальность названия элемента, вводимого пользователем. Если в базовом блоке, или в блоке, в который добавляется элемент, есть элемент с таким же названием (без учета регистра), то выводить сообщение "Элемент с таким названием уже существует в базовом, или текущем блоке версии учебного плана".
     * @param row
     */
    public static void checkUniqTitle(final EppEpvTermDistributedRow row)
    {
        if (IEppSettingsDAO.instance.get().getGlobalSettings().isAllowNotUniqueEpvRowName()) return;

        final String title = StringUtils.trimToEmpty(row.getTitle());
        final Collection<IEppEpvRowWrapper> rows = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockRowMap(row.getOwner().getId(), false).values();
        for (final IEppEpvRowWrapper wrapper: rows)
        {
            if (EntityBase.equals(row.getId(), wrapper.getId())) {
                // текущий объект проверять не надо :)
                continue;
            }

            if (wrapper.getRow() instanceof EppEpvTermDistributedRow)
            {
                final String t = StringUtils.trimToEmpty(wrapper.getRow().getTitle());
                if (t.equalsIgnoreCase(title)) {
                    throw new ApplicationException("Элемент с таким названием уже существует в базовом или текущем блоке версии учебного плана.");
                }
            }
        }
    }

    // getters and setters

    public boolean getScroll() {
        return true;
    }

    public EntityHolder<EppEduPlanVersionBlock> getBlockHolder() {
        return blockHolder;
    }

    public EppEduPlanVersionBlock getBlock() {
        return getBlockHolder().getValue();
    }

    public boolean isEditForm() {
        return getRow().getId() != null;
    }

    public IEppEpvRowWrapper getParent() {
        final IEppEpvRow parent = this.getRow().getHierarhyParent();
        if (null == parent) { return null; }
        for (final HSelectOption option: this.parentList) {
            if (parent.equals(option.getObject())) { return (IEppEpvRowWrapper) option.getObject(); }
        }
        return null;
    }

    public void setParent(final IEppEpvRowWrapper parent) {
        this.getRow().setHierarhyParent((EppEpvRow)(null == parent ? null : parent.getRow()));
    }

    public List<HSelectOption> getParentList() { return this.parentList; }
    public void setParentList(final List<HSelectOption> parentList) { this.parentList = parentList; }

    public Long getScrollPos()
    {
        return scrollPos;
    }

    public void setScrollPos(Long scrollPos)
    {
        this.scrollPos = scrollPos;
    }
}
