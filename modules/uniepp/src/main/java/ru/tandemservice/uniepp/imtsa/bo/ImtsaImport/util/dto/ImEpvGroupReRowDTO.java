/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.dto;

/**
 * @author azhebko
 * @since 28.10.2014
 */
public class ImEpvGroupReRowDTO extends ImEpvRowDTO
{
    private String _number;     // Номер строки
    private String _title;     // Название

    public ImEpvGroupReRowDTO() { }
    public ImEpvGroupReRowDTO(String title) { _title = title; }

    public String getNumber() { return _number; }
    public void setNumber(String number) { _number = number; }

    public String getTitle() { return _title; }
    public void setTitle(String title) { _title = title; }

    @Override
    public String toString()
    {
        return "Группа дисциплин: " + getNumber() + ", " + getTitle() + '\n' + super.toString();
    }
}