package ru.tandemservice.uniepp.entity.pupnag.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ГУП (Связь строки с учебным планом)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkGraphRow2EduPlanGen extends EntityBase
 implements INaturalIdentifiable<EppWorkGraphRow2EduPlanGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan";
    public static final String ENTITY_NAME = "eppWorkGraphRow2EduPlan";
    public static final int VERSION_HASH = -1589054250;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW = "row";
    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";

    private EppWorkGraphRow _row;     // Строка ГУП
    private EppEduPlanVersion _eduPlanVersion;     // Учебный план (версия)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка ГУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkGraphRow getRow()
    {
        return _row;
    }

    /**
     * @param row Строка ГУП. Свойство не может быть null.
     */
    public void setRow(EppWorkGraphRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Учебный план (версия). Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion Учебный план (версия). Свойство не может быть null.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppWorkGraphRow2EduPlanGen)
        {
            if (withNaturalIdProperties)
            {
                setRow(((EppWorkGraphRow2EduPlan)another).getRow());
                setEduPlanVersion(((EppWorkGraphRow2EduPlan)another).getEduPlanVersion());
            }
        }
    }

    public INaturalId<EppWorkGraphRow2EduPlanGen> getNaturalId()
    {
        return new NaturalId(getRow(), getEduPlanVersion());
    }

    public static class NaturalId extends NaturalIdBase<EppWorkGraphRow2EduPlanGen>
    {
        private static final String PROXY_NAME = "EppWorkGraphRow2EduPlanNaturalProxy";

        private Long _row;
        private Long _eduPlanVersion;

        public NaturalId()
        {}

        public NaturalId(EppWorkGraphRow row, EppEduPlanVersion eduPlanVersion)
        {
            _row = ((IEntity) row).getId();
            _eduPlanVersion = ((IEntity) eduPlanVersion).getId();
        }

        public Long getRow()
        {
            return _row;
        }

        public void setRow(Long row)
        {
            _row = row;
        }

        public Long getEduPlanVersion()
        {
            return _eduPlanVersion;
        }

        public void setEduPlanVersion(Long eduPlanVersion)
        {
            _eduPlanVersion = eduPlanVersion;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppWorkGraphRow2EduPlanGen.NaturalId) ) return false;

            EppWorkGraphRow2EduPlanGen.NaturalId that = (NaturalId) o;

            if( !equals(getRow(), that.getRow()) ) return false;
            if( !equals(getEduPlanVersion(), that.getEduPlanVersion()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRow());
            result = hashCode(result, getEduPlanVersion());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRow());
            sb.append("/");
            sb.append(getEduPlanVersion());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkGraphRow2EduPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkGraphRow2EduPlan.class;
        }

        public T newInstance()
        {
            return (T) new EppWorkGraphRow2EduPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "row":
                    return obj.getRow();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "row":
                    obj.setRow((EppWorkGraphRow) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "row":
                        return true;
                case "eduPlanVersion":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "row":
                    return true;
                case "eduPlanVersion":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "row":
                    return EppWorkGraphRow.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkGraphRow2EduPlan> _dslPath = new Path<EppWorkGraphRow2EduPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkGraphRow2EduPlan");
    }
            

    /**
     * @return Строка ГУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan#getRow()
     */
    public static EppWorkGraphRow.Path<EppWorkGraphRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Учебный план (версия). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    public static class Path<E extends EppWorkGraphRow2EduPlan> extends EntityPath<E>
    {
        private EppWorkGraphRow.Path<EppWorkGraphRow> _row;
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка ГУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan#getRow()
     */
        public EppWorkGraphRow.Path<EppWorkGraphRow> row()
        {
            if(_row == null )
                _row = new EppWorkGraphRow.Path<EppWorkGraphRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Учебный план (версия). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

        public Class getEntityClass()
        {
            return EppWorkGraphRow2EduPlan.class;
        }

        public String getEntityName()
        {
            return "eppWorkGraphRow2EduPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
