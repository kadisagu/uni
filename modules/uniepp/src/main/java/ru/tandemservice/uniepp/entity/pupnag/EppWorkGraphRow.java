package ru.tandemservice.uniepp.entity.pupnag;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppWorkGraphRowGen;

/**
 * ГУП (Строка)
 */
public class EppWorkGraphRow extends EppWorkGraphRowGen
{
    public EppWorkGraphRow() {

    }

    public EppWorkGraphRow(final EppWorkGraph workGraph, final Course course) {
        this.setGraph(workGraph);
        this.setCourse(course);
    }
}