/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.ui.EditRegistryElementOwner;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.base.bo.EppReorganization.EppReorganizationManager;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @author rsizonenko
 * @since 30.03.2016
 */
@Input({
        @Bind(key = "singleValue", binding = "entity"),
        @Bind(key = "multiValue", binding = "entities")
})
public class EppReorganizationEditRegistryElementOwnerUI extends UIPresenter {

    private List<ViewWrapper<EppRegistryElement>> entities;
    private Long entity;
    private OrgUnit owner;

    @Override
    public void onComponentActivate() {
        super.onComponentActivate();
    }

    public void onClickApply()
    {
        List<EppRegistryElement> output = new ArrayList<>();
        if (null != getEntities())
            output = getEntities().stream().map(ViewWrapper::getEntity).collect(Collectors.toList());
        else if (null != getEntity())
            output.add(DataAccessServices.dao().get(getEntity()));
        EppReorganizationManager.instance().dao().doChangeEppRegistryElementOwner(output, owner);
        deactivate();
    }

    public List<ViewWrapper<EppRegistryElement>> getEntities() {
        return entities;
    }

    public void setEntities(List<ViewWrapper<EppRegistryElement>> entities) {
        this.entities = entities;
    }

    public Long getEntity() {
        return entity;
    }

    public void setEntity(Long entity) {
        this.entity = entity;
    }

    public OrgUnit getOwner() {
        return owner;
    }

    public void setOwner(OrgUnit owner) {
        this.owner = owner;
    }
}