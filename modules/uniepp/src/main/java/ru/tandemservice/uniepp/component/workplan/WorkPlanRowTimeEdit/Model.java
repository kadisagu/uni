package ru.tandemservice.uniepp.component.workplan.WorkPlanRowTimeEdit;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.util.cache.SafeMap;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;

/**
 * @author vdanilov
 */

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id") })
@SuppressWarnings("unchecked")
public class Model
{
    private Long id;
    public Long getId() { return this.id; }
    public void setId(final Long id) { this.id = id; }

    private List<EppLoadType> loadTypeList = Collections.emptyList();
    public List<EppLoadType> getLoadTypeList() { return this.loadTypeList; }
    public void setLoadTypeList(final List<EppLoadType> loadTypeList) { this.loadTypeList = loadTypeList; }

    private EppLoadType loadType;
    public EppLoadType getLoadType() { return this.loadType; }
    public void setLoadType(final EppLoadType loadType) { this.loadType = loadType; }

    private List<Integer> partsList = Collections.emptyList();
    public List<Integer> getPartsList() { return this.partsList; }
    public void setPartsList(final List<Integer> partsList) { this.partsList = partsList; }

    private Integer part;
    public Integer getPart() { return this.part; }
    public void setPart(final Integer part) { this.part = part; }

    private Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> rowPartLoadMap = Collections.emptyMap();
    public Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> getRowPartLoadMap() { return this.rowPartLoadMap; }
    public void setRowPartLoadMap(final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> rowPartLoadMap) { this.rowPartLoadMap = rowPartLoadMap; }

    public Collection<Long> getRowIds() { return this.getRowPartLoadMap().keySet(); }

    private Long currentRowId;
    public Long getCurrentRowId() { return this.currentRowId; }
    public void setCurrentRowId(final Long currentRowId) { this.currentRowId = currentRowId; }

    public EppWorkPlanRow getCurrentRow() {
        return IUniBaseDao.instance.get().get(EppWorkPlanRow.class, this.getCurrentRowId());
    }


    private Map<String, EppWorkPlanRowPartLoad> getLoadMap() {
        final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> partLoadMap = SafeMap.safeGet(this.rowPartLoadMap, this.getCurrentRowId(), HashMap.class);
        return SafeMap.safeGet(partLoadMap, this.getPart(), HashMap.class);
    }

    private EppWorkPlanRowPartLoad getEppWorkPlanRowPartLoadSafe()
    {
        final Map<String, EppWorkPlanRowPartLoad> loadMap = this.getLoadMap();
        EppWorkPlanRowPartLoad eppWorkPlanRowPartLoad = loadMap.get(this.getLoadType().getFullCode());

        if (null == eppWorkPlanRowPartLoad)
        {
            eppWorkPlanRowPartLoad = new EppWorkPlanRowPartLoad();
            eppWorkPlanRowPartLoad.setRow(this.getCurrentRow());
            eppWorkPlanRowPartLoad.setPart(this.getPart());
            eppWorkPlanRowPartLoad.setLoadType(this.getLoadType());
            eppWorkPlanRowPartLoad.setLoadAsDouble(null);
            eppWorkPlanRowPartLoad.setDaysAsDouble(null);

            loadMap.put(this.getLoadType().getFullCode(), eppWorkPlanRowPartLoad);
        }

        return eppWorkPlanRowPartLoad;
    }

    public boolean isElementExists() {
        return (null != this.getLoadMap().get(this.getLoadType().getFullCode()));
    }


    public Date getStudyPeriodStartDate() {
        return this.getEppWorkPlanRowPartLoadSafe().getStudyPeriodStartDate();
    }
    public void setStudyPeriodStartDate(final Date value) {
        this.getEppWorkPlanRowPartLoadSafe().setStudyPeriodStartDate(value);
    }

    public Date getStudyPeriodFinishDate() {
        return this.getEppWorkPlanRowPartLoadSafe().getStudyPeriodFinishDate();
    }
    public void setStudyPeriodFinishDate(final Date value) {
        this.getEppWorkPlanRowPartLoadSafe().setStudyPeriodFinishDate(value);
    }

    public static String getInputId(final Long rowId, final Integer part, final String fullCode) {
        return "load_"+rowId+"_"+part+"_"+fullCode.replace('.', '_');
    }
    public String getInputId() {
        return Model.getInputId(this.getCurrentRowId(), this.getPart(), this.getLoadType().getFullCode());
    }

    public String getTitle() {
        return UniEppUtils.getEppWorkPlanRowFormTitle(this.getId());
    }

}
