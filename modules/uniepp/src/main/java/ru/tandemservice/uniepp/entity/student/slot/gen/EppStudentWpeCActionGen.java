package ru.tandemservice.uniepp.entity.student.slot.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * МСРП по форме итогового контроля (МСРП-ФК)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppStudentWpeCActionGen extends EppStudentWpePart
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction";
    public static final String ENTITY_NAME = "eppStudentWpeCAction";
    public static final int VERSION_HASH = 1265192432;
    private static IEntityMeta ENTITY_META;

    public static final String P_REGISTRY_ELEMENT_TITLE = "registryElementTitle";
    public static final String P_TERM_TITLE = "termTitle";


    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppStudentWpeCActionGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppStudentWpeCActionGen> extends EppStudentWpePart.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppStudentWpeCAction.class;
        }

        public T newInstance()
        {
            return (T) new EppStudentWpeCAction();
        }
    }
    private static final Path<EppStudentWpeCAction> _dslPath = new Path<EppStudentWpeCAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppStudentWpeCAction");
    }
            

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction#getRegistryElementTitle()
     */
    public static SupportedPropertyPath<String> registryElementTitle()
    {
        return _dslPath.registryElementTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction#getTermTitle()
     */
    public static SupportedPropertyPath<String> termTitle()
    {
        return _dslPath.termTitle();
    }

    public static class Path<E extends EppStudentWpeCAction> extends EppStudentWpePart.Path<E>
    {
        private SupportedPropertyPath<String> _registryElementTitle;
        private SupportedPropertyPath<String> _termTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction#getRegistryElementTitle()
     */
        public SupportedPropertyPath<String> registryElementTitle()
        {
            if(_registryElementTitle == null )
                _registryElementTitle = new SupportedPropertyPath<String>(EppStudentWpeCActionGen.P_REGISTRY_ELEMENT_TITLE, this);
            return _registryElementTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction#getTermTitle()
     */
        public SupportedPropertyPath<String> termTitle()
        {
            if(_termTitle == null )
                _termTitle = new SupportedPropertyPath<String>(EppStudentWpeCActionGen.P_TERM_TITLE, this);
            return _termTitle;
        }

        public Class getEntityClass()
        {
            return EppStudentWpeCAction.class;
        }

        public String getEntityName()
        {
            return "eppStudentWpeCAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getRegistryElementTitle();

    public abstract String getTermTitle();
}
