/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.ui.EppEpvRegRowSkillList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import ru.tandemservice.uniepp.base.bo.EppDevelopResult.EppDevelopResultManager;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegRowSkill;

/**
 * @author Igor Belanov
 * @since 12.04.2017
 */
@Configuration
public class EppDevelopResultEppEpvRegRowSkillList extends BusinessComponentManager
{
    public static final String EPP_EPV_REG_ROW_SKILL_DS = "eppEpvRegRowSkillDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(EPP_EPV_REG_ROW_SKILL_DS, eppEpvRegRowSkillCL(), EppDevelopResultManager.instance().eppEpvRegRowSkillSearchDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eppEpvRegRowSkillCL()
    {
        final IMergeRowIdResolver skillMerge = entity -> {
            EppEpvRegRowSkill eppEpvRegRowSkill = ((EppEpvRegRowSkill) entity);
            return String.valueOf(eppEpvRegRowSkill.getSkill().getId());
        };
        final IMergeRowIdResolver EPVBlock = entity -> {
            EppEpvRegRowSkill eppEpvRegRowSkill = ((EppEpvRegRowSkill) entity);
            return String.valueOf(eppEpvRegRowSkill.getEpvRegistryRow().getOwner().getId()) +
                    String.valueOf(eppEpvRegRowSkill.getSkill().getId());
        };
        return columnListExtPointBuilder(EPP_EPV_REG_ROW_SKILL_DS)
                .addColumn(textColumn("EppSkill", EppEpvRegRowSkill.skill().fullTitle()).merger(skillMerge))
                .addColumn(textColumn("EppEPVBlock", EppEpvRegRowSkill.epvRegistryRow().owner().title()).merger(EPVBlock))
                .addColumn(textColumn("EppEpvRegRow", EppEpvRegRowSkill.epvRegistryRow().titleWithIndex()))
                .create();
    }
}
