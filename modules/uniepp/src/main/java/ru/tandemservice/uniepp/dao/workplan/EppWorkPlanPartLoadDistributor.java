package ru.tandemservice.uniepp.dao.workplan;

import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uniepp.dao.eduplan.EppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;
import ru.tandemservice.uniepp.entity.workplan.IEppWorkPlanPart;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * распределяет семестровую нагрузку (по видам) по частям РУП в зависимости от числа теоретического обучения
 * если число нагрузка не делится нацело, то в последнюю часть скидывается остаток
 **/
public class EppWorkPlanPartLoadDistributor {
    private final Map<String, EppLoadType> loadTypeMap = EppEduPlanVersionDataDAO.getLoadTypeMap();
    private final Iterable<IEppWorkPlanPart> partIteratorFactory;
    private final int totalWeeks;

    public EppWorkPlanPartLoadDistributor(final Map<Integer, IEppWorkPlanPart> partMap) {
        this.partIteratorFactory = partMap.values();
        int total = 0;
        for (final IEppWorkPlanPart i: partMap.values()) {
            total += (null == i ? 0 : i.getTotalWeeks());
        }
        this.totalWeeks = total;
    }

    public Map<Integer, Map<String, EppWorkPlanRowPartLoad>> getPartLoadDistributionMap(final EppWorkPlanRow row, final IEppRegElPartWrapper registryElementPartWrapper) {

        final Map<String, Double> loadMap = SafeMap.get(key -> null == registryElementPartWrapper
                ? EppLoadTypeUtils.ZERO
                : registryElementPartWrapper.getLoadAsDouble(key)
        );

        final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> result = new HashMap<>();
        if (this.totalWeeks > 0) {
            // хранит еще не распределенную нагрузку по строке (изначально в ней полная нагрузка из реестра)
            final Map<String, Double> currentLoadMap = EppLoadTypeUtils.newLoadMap();
            for (final Entry<String, Double> currentLoadEntry: currentLoadMap.entrySet()) {
                currentLoadEntry.setValue(loadMap.get(currentLoadEntry.getKey()));
            }

            // теперь бежим по частям и откусываем из этой нагрузки куски (целые), пропорциональные числу недель в части
            final Iterator<IEppWorkPlanPart> it = this.partIteratorFactory.iterator();
            while (it.hasNext()) {
                final IEppWorkPlanPart part = it.next();
                if (it.hasNext()) {
                    final Map<String, Double> partResultMap = new HashMap<>(currentLoadMap.size());
                    for (final Entry<String, Double> currentLoadEntry: currentLoadMap.entrySet()) {
                        final double partLoad = (part.getTotalWeeks() * loadMap.get(currentLoadEntry.getKey())) / this.totalWeeks;
                        final double value = Math.floor(Math.max(Math.min(currentLoadEntry.getValue(), partLoad), 0d));
                        currentLoadEntry.setValue(currentLoadEntry.getValue() - value);
                        partResultMap.put(currentLoadEntry.getKey(), value);
                    }
                    result.put(part.getNumber(), this.convert(row, part, this.loadTypeMap, partResultMap));
                } else {
                    result.put(part.getNumber(), this.convert(row, part, this.loadTypeMap, currentLoadMap));
                }
            }
        }

        return result;
    }

    private Map<String, EppWorkPlanRowPartLoad> convert(final EppWorkPlanRow row, final IEppWorkPlanPart part, final Map<String, EppLoadType> loadTypeMap, final Map<String, Double> partResultMap) {
        final Map<String, EppWorkPlanRowPartLoad> result = new HashMap<>(partResultMap.size());
        for (final Map.Entry<String, Double> entry: partResultMap.entrySet()) {
            final Double value = entry.getValue();
            if ((null != value) && (value > 0)) {
                final EppLoadType loadType = loadTypeMap.get(entry.getKey());
                if (null != loadType) {
                    final EppWorkPlanRowPartLoad rel = new EppWorkPlanRowPartLoad(row, part.getNumber(), loadType);
                    rel.setLoadAsDouble(entry.getValue());
                    result.put(entry.getKey(), rel);
                }
            }
        }
        return result;
    }


}