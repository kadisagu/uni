/* $Id: $ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.CheckPrint.logic;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.JExcelUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.workplan.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 16.03.2017
 */
public class EppWorkPlanCheckPrintDao extends UniBaseDao implements IEppWorkPlanCheckPrintDao
{

    @Override
    public byte[] printReport(EppWorkPlanCheckPrintData data)
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);

        addRows2Data(data);

        try
        {
            initFormats();

            WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

            data.getEducationYearList().forEach(year ->
                                                {
                                                    try
                                                    {
                                                        print(workbook.createSheet(String.valueOf(year.getIntValue()), workbook.getSheets().length), year, data);
                                                    }
                                                    catch (WriteException e)
                                                    {
                                                        e.printStackTrace();
                                                    }
                                                });

            workbook.write();
            workbook.close();
        }
        catch (IOException | WriteException e)
        {
            e.printStackTrace();
        }

        return out.toByteArray();
    }

    protected void print(WritableSheet sheet, EducationYear year, EppWorkPlanCheckPrintData data) throws WriteException
    {
        sheet.setRowView(0, 600);
        JExcelUtil.addTextCell(sheet, 0, 0, 9, 1, "Перечень мероприятий " + year.getTitle() + " учебного года", _titleCF);

        int rowIndex = 2;
        rowIndex = printTableHeader(sheet, rowIndex);
        printTableData(sheet, rowIndex, year, data);


    }


    protected int printTableHeader(WritableSheet sheet, int rowIndex) throws WriteException
    {
        sheet.setRowView(rowIndex, 800);

        int colIndex = 0;

        sheet.setColumnView(colIndex, 5);
        sheet.addCell(new Label(colIndex++, rowIndex, "№ п/п", _headerCF));

        sheet.setColumnView(colIndex, 10);
        sheet.addCell(new Label(colIndex++, rowIndex, "Номер\nв реестре", _headerCF));

        sheet.setColumnView(colIndex, 50);
        sheet.addCell(new Label(colIndex++, rowIndex, "Название", _headerCF));

        JExcelUtil.setColumnAutoWidth(sheet, colIndex);
        sheet.addCell(new Label(colIndex++, rowIndex, "Состояние", _headerCF));

        sheet.setColumnView(colIndex, 30);
        sheet.addCell(new Label(colIndex++, rowIndex, "Читающее\nподразделение", _headerCF));

        sheet.setColumnView(colIndex, 25);
        sheet.addCell(new Label(colIndex++, rowIndex, "Тип (группа)", _headerCF));

        sheet.setColumnView(colIndex, 30);
        sheet.addCell(new Label(colIndex++, rowIndex, "Направление подготовки", _headerCF));

        sheet.setColumnView(colIndex, 50);
        sheet.addCell(new Label(colIndex++, rowIndex, "Направленность", _headerCF));

        sheet.setColumnView(colIndex, 30);
        sheet.addCell(new Label(colIndex++, rowIndex, "Академические группы", _headerCF));

        sheet.setColumnView(colIndex, 10);
        sheet.addCell(new Label(colIndex++, rowIndex, "РУП", _headerCF));

        sheet.setColumnView(colIndex, 23);
        sheet.addCell(new Label(colIndex, rowIndex, "Семестр РУП", _headerCF));

        sheet.getSettings().setVerticalFreeze(++rowIndex);

        return rowIndex;
    }


    //TABLE DATA
    protected int printTableData(WritableSheet sheet, int rowIndex, EducationYear year, EppWorkPlanCheckPrintData data) throws WriteException
    {
        List<EppWorkPlanRegistryElementRow> rows = data.getRows().get(year);

        rows.sort((row1, row2) ->
                  {
                      int result = row1.getOwner().getTitle().compareTo(row2.getOwner().getTitle());
                      if (result != 0) return result;

                      result = row1.getRegistryElement().getTitle().compareTo(row2.getRegistryElement().getTitle());
                      if (result != 0) return result;

                      result = row1.getRegistryElement().getNumber().compareTo(row2.getRegistryElement().getNumber());
                      if (result != 0) return result;

                      result = Integer.compare(row1.getRegistryElementPart().getNumber(), row2.getRegistryElementPart().getNumber());

                      return result;
                  });

        Map<Long, String> groupsByRowMap = data.getGroupsByRowMap();

        int index = 1;
        for (EppWorkPlanRegistryElementRow row : rows)
        {
            int colIndex = 0;
            sheet.addCell(new Number(colIndex++, rowIndex, index++, _valueCenterCF));
            sheet.addCell(new Label(colIndex++, rowIndex, row.getRegistryElement().getNumber(), _valueCenterCF));
            sheet.addCell(new Label(colIndex++, rowIndex, row.getRegistryElementPart().getTitle(), _valueLeftCF));
            sheet.addCell(new Label(colIndex++, rowIndex, row.getRegistryElement().getState().getTitle(), _valueCenterCF));
            sheet.addCell(new Label(colIndex++, rowIndex, row.getOwner().getTitle(), _valueCenterCF));
            sheet.addCell(new Label(colIndex++, rowIndex, row.getRegistryElement().getParent().getTitle(), _valueCenterCF));
            sheet.addCell(new Label(colIndex++, rowIndex, row.getWorkPlan().getEduPlanVersion().getEduPlan().getEducationElementSimpleTitle(), _valueLeftCF));
            sheet.addCell(new Label(colIndex++, rowIndex, row.getWorkPlan().getBlock().getTitle(), _valueLeftCF));
            sheet.addCell(new Label(colIndex++, rowIndex, groupsByRowMap.get(row.getId()), _valueLeftCF));
            sheet.addCell(new Label(colIndex++, rowIndex, row.getWorkPlan().getFullNumber(), _valueCenterCF));

            DevelopGridTerm gridTerm = row.getWorkPlan().getGridTerm();
            String term = gridTerm.getTerm().getTitle() + " (" + gridTerm.getPart().getTitle() + ", " + gridTerm.getCourse().getTitle() + " курс)";
            sheet.addCell(new Label(colIndex, rowIndex, term, _valueCenterCF));
            rowIndex++;
        }

        return rowIndex;
    }


    protected void addRows2Data(EppWorkPlanCheckPrintData data)
    {
        List<EducationYear> yearList = data.getEducationYearList();

        String wpbAlias = "wpb";
        String wpAlias = "wp";
        String wpvAlias = "wpv";
        DQLSelectBuilder workPlanBuilder = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanBase.class, wpbAlias)
                .column(property(wpbAlias))

                .joinEntity(wpbAlias, DQLJoinType.left, EppWorkPlan.class, wpAlias,
                            eq(property(wpbAlias, EppWorkPlanBase.id()), property(wpAlias, EppWorkPlan.id())))
                .joinEntity(wpbAlias, DQLJoinType.left, EppWorkPlanVersion.class, wpvAlias,
                            eq(property(wpbAlias, EppWorkPlanBase.id()), property(wpvAlias, EppWorkPlanVersion.id())))
                .where(or(
                        existsByExpr(EppWorkPlan.class, wpAlias,
                                     and(in(property(wpAlias, EppWorkPlan.year().educationYear()), yearList),
                                         eq(property(wpbAlias, EppWorkPlanBase.id()), property(wpAlias, EppWorkPlan.id())))),
                        existsByExpr(EppWorkPlanVersion.class, wpvAlias,
                                     and(in(property(wpvAlias, EppWorkPlanVersion.parent().year().educationYear()), yearList),
                                         eq(property(wpbAlias, EppWorkPlanBase.id()), property(wpvAlias, EppWorkPlanVersion.id()))))
                ));

        FilterUtils.applySelectFilter(workPlanBuilder, wpbAlias, EppWorkPlanBase.state().s(), data.getWorkPlanStateList());


        String rowAlias = "ewpr";
        DQLSelectBuilder rowBuilder = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, rowAlias)
                .column(property(rowAlias))
                .where(in(property(rowAlias, EppWorkPlanRegistryElementRow.workPlan()), workPlanBuilder.buildQuery()));

        FilterUtils.applySelectFilter(rowBuilder, rowAlias, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().state().s(), data.getRegStateList());
        FilterUtils.applySelectFilter(rowBuilder, rowAlias, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().owner().s(), data.getOrgUnitList());
        FilterUtils.applySelectFilter(rowBuilder, rowAlias, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().parent().s(), data.getRegTypeList());

        data.setRows(this.<EppWorkPlanRegistryElementRow>getList(rowBuilder).stream()
                             .collect(Collectors.groupingBy(row -> row.getWorkPlan().getYear().getEducationYear(), Collectors.mapping(row -> row, Collectors.toList()))));

        if(data.getRows().isEmpty()) throw new ApplicationException("Нет данных для построения отчета");


        DQLSelectBuilder swpeBuilder = new DQLSelectBuilder()
                .fromEntity(EppStudentWorkPlanElement.class, "swpe")
                .column(property("swpe", EppStudentWorkPlanElement.sourceRow().id()))
                .column(property("swpe", EppStudentWorkPlanElement.student().group().title()))
                .where(in(property("swpe", EppStudentWorkPlanElement.sourceRow()), rowBuilder.buildQuery()));


        Map<Long, Set<String>> groupsByRowMap = this.<Object[]>getList(swpeBuilder).stream()
                .collect(Collectors.groupingBy(raw -> (Long) raw[0], Collectors.mapping(raw -> (String) raw[1], Collectors.toSet())));

        data.setGroupsByRowMap(groupsByRowMap.entrySet().stream()
                                       .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().stream().collect(Collectors.joining(", ")))));
    }

    //FORMATS
    protected WritableCellFormat _titleCF;
    protected WritableCellFormat _headerCF;
    protected WritableCellFormat _valueCenterCF;
    protected WritableCellFormat _valueLeftCF;

    protected void initFormats() throws WriteException
    {
        _titleCF = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 15));
        _titleCF.setWrap(true);
        _titleCF.setVerticalAlignment(VerticalAlignment.CENTRE);
        _titleCF.setAlignment(Alignment.LEFT);

        _headerCF = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD));
        _headerCF.setBorder(Border.ALL, BorderLineStyle.THICK, Colour.BLACK);
        _headerCF.setWrap(true);
        _headerCF.setVerticalAlignment(VerticalAlignment.CENTRE);
        _headerCF.setAlignment(Alignment.CENTRE);

        _valueCenterCF = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 10));
        _valueCenterCF.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _valueCenterCF.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _valueCenterCF.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _valueCenterCF.setWrap(true);
        _valueCenterCF.setVerticalAlignment(VerticalAlignment.CENTRE);
        _valueCenterCF.setAlignment(Alignment.CENTRE);

        _valueLeftCF = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 10));
        _valueLeftCF.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
        _valueLeftCF.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
        _valueLeftCF.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
        _valueLeftCF.setWrap(true);
        _valueLeftCF.setVerticalAlignment(VerticalAlignment.CENTRE);
        _valueLeftCF.setAlignment(Alignment.LEFT);
    }

}
