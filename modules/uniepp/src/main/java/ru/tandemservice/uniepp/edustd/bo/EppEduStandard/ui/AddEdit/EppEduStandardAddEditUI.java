/* $Id:$ */
package ru.tandemservice.uniepp.edustd.bo.EppEduStandard.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniepp.edustd.bo.EppEduStandard.EppEduStandardManager;
import ru.tandemservice.uniepp.entity.catalog.EppGeneration;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author oleyba
 * @since 9/10/14
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "stateEduStandard.id")
})
public class EppEduStandardAddEditUI extends UIPresenter
{
    private List<EppGeneration> generationList = Collections.emptyList();
    private EppGeneration generation;

    private EppStateEduStandard stateEduStandard = new EppStateEduStandard();
    private EduProgramKind eduProgramKind;

    @Override
    public void onComponentRefresh()
    {
        setGenerationList(Arrays.asList(EppGeneration.values()));
        if (getStateEduStandard().getId() != null) {
            setStateEduStandard(DataAccessServices.dao().getNotNull(EppStateEduStandard.class, getStateEduStandard().getId()));
            setEduProgramKind(getStateEduStandard().getProgramSubject().getEduProgramKind());
            if (getStateEduStandard().getState().isReadOnlyState()) {
                throw new ApplicationException("ГОС находится в состоянии «" + getStateEduStandard().getState().getTitle() + "», редактирование запрещено.");
            }
            setGeneration(getStateEduStandard().getGeneration());
        } else {
            getStateEduStandard().setNumber(INumberQueueDAO.instance.get().getNextNumber(getStateEduStandard()));
            getStateEduStandard().setState(DataAccessServices.dao().get(EppState.class, EppState.code(), EppState.STATE_FORMATIVE));
        }
    }

    public boolean isEditForm() {
        return getStateEduStandard().getId() != null;
    }

    public void onClickApply() {
        DataAccessServices.dao().saveOrUpdate(getStateEduStandard());
        deactivate();
    }

    // getters and setters

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EppEduStandardManager.BIND_GENERATION, getGeneration());
        dataSource.put(EppEduStandardManager.BIND_PROGRAM_KIND, getEduProgramKind());
    }

    public EppStateEduStandard getStateEduStandard()
    {
        return stateEduStandard;
    }

    public void setStateEduStandard(EppStateEduStandard stateEduStandard)
    {
        this.stateEduStandard = stateEduStandard;
    }

    public EduProgramKind getEduProgramKind()
    {
        return eduProgramKind;
    }

    public void setEduProgramKind(EduProgramKind eduProgramKind)
    {
        this.eduProgramKind = eduProgramKind;
    }

    public List<EppGeneration> getGenerationList()
    {
        return generationList;
    }

    public void setGenerationList(List<EppGeneration> generationList)
    {
        this.generationList = generationList;
    }

    public EppGeneration getGeneration()
    {
        return generation;
    }

    public void setGeneration(EppGeneration generation)
    {
        this.generation = generation;
    }
}