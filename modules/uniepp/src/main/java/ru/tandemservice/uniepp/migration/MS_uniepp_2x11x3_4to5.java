/* $Id$ */
package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.dbsupport.sql.SQLUpdateQuery;

/**
 * Миграция устанавливает приоритеты в справочнике "Типы деятельности в учебном графике"
 * (они не были установлены после добавления поля приоритета)
 * @author Igor Belanov
 * @since 10.04.2017
 */
public class MS_uniepp_2x11x3_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        SQLSelectQuery selectWTPriority = new SQLSelectQuery();
        selectWTPriority.from(SQLFrom.table("epp_c_weektype_t"));
        selectWTPriority.column("id");
        selectWTPriority.column("row_number() over (order by id)", "newPriority");

        SQLUpdateQuery updateWTPriority = new SQLUpdateQuery("epp_c_weektype_t", "wt");
        updateWTPriority.set("priority_p", "t.newPriority");
        updateWTPriority.from(SQLFrom.select(translator.toSql(selectWTPriority), "t"));
        updateWTPriority.where("wt.id=t.id");
        tool.executeUpdate(translator.toSql(updateWTPriority));
    }
}
