package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Базовый класс для вида теоретической нагрузки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppLoadTypeGen extends EntityBase
 implements INaturalIdentifiable<EppLoadTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppLoadType";
    public static final String ENTITY_NAME = "eppLoadType";
    public static final int VERSION_HASH = -985197616;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_CATALOG_CODE = "catalogCode";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_ABBREVIATION = "abbreviation";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _catalogCode;     // Код справочника
    private String _shortTitle;     // Сокращенное название
    private String _abbreviation;     // Условное сокращение
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Код справочника. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCatalogCode()
    {
        return _catalogCode;
    }

    /**
     * @param catalogCode Код справочника. Свойство не может быть null.
     */
    public void setCatalogCode(String catalogCode)
    {
        dirty(_catalogCode, catalogCode);
        _catalogCode = catalogCode;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Условное сокращение.
     */
    @Length(max=255)
    public String getAbbreviation()
    {
        return _abbreviation;
    }

    /**
     * @param abbreviation Условное сокращение.
     */
    public void setAbbreviation(String abbreviation)
    {
        dirty(_abbreviation, abbreviation);
        _abbreviation = abbreviation;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppLoadTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EppLoadType)another).getCode());
                setCatalogCode(((EppLoadType)another).getCatalogCode());
            }
            setShortTitle(((EppLoadType)another).getShortTitle());
            setAbbreviation(((EppLoadType)another).getAbbreviation());
            setTitle(((EppLoadType)another).getTitle());
        }
    }

    public INaturalId<EppLoadTypeGen> getNaturalId()
    {
        return new NaturalId(getCode(), getCatalogCode());
    }

    public static class NaturalId extends NaturalIdBase<EppLoadTypeGen>
    {
        private static final String PROXY_NAME = "EppLoadTypeNaturalProxy";

        private String _code;
        private String _catalogCode;

        public NaturalId()
        {}

        public NaturalId(String code, String catalogCode)
        {
            _code = code;
            _catalogCode = catalogCode;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getCatalogCode()
        {
            return _catalogCode;
        }

        public void setCatalogCode(String catalogCode)
        {
            _catalogCode = catalogCode;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppLoadTypeGen.NaturalId) ) return false;

            EppLoadTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            if( !equals(getCatalogCode(), that.getCatalogCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            result = hashCode(result, getCatalogCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            sb.append("/");
            sb.append(getCatalogCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppLoadTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppLoadType.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppLoadType is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "catalogCode":
                    return obj.getCatalogCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "abbreviation":
                    return obj.getAbbreviation();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "catalogCode":
                    obj.setCatalogCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "abbreviation":
                    obj.setAbbreviation((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "catalogCode":
                        return true;
                case "shortTitle":
                        return true;
                case "abbreviation":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "catalogCode":
                    return true;
                case "shortTitle":
                    return true;
                case "abbreviation":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "catalogCode":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "abbreviation":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppLoadType> _dslPath = new Path<EppLoadType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppLoadType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppLoadType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Код справочника. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppLoadType#getCatalogCode()
     */
    public static PropertyPath<String> catalogCode()
    {
        return _dslPath.catalogCode();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppLoadType#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Условное сокращение.
     * @see ru.tandemservice.uniepp.entity.catalog.EppLoadType#getAbbreviation()
     */
    public static PropertyPath<String> abbreviation()
    {
        return _dslPath.abbreviation();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppLoadType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EppLoadType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _catalogCode;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _abbreviation;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppLoadType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EppLoadTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Код справочника. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppLoadType#getCatalogCode()
     */
        public PropertyPath<String> catalogCode()
        {
            if(_catalogCode == null )
                _catalogCode = new PropertyPath<String>(EppLoadTypeGen.P_CATALOG_CODE, this);
            return _catalogCode;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppLoadType#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EppLoadTypeGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Условное сокращение.
     * @see ru.tandemservice.uniepp.entity.catalog.EppLoadType#getAbbreviation()
     */
        public PropertyPath<String> abbreviation()
        {
            if(_abbreviation == null )
                _abbreviation = new PropertyPath<String>(EppLoadTypeGen.P_ABBREVIATION, this);
            return _abbreviation;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniepp.entity.catalog.EppLoadType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppLoadTypeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EppLoadType.class;
        }

        public String getEntityName()
        {
            return "eppLoadType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
