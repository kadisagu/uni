package ru.tandemservice.uniepp.entity.plan;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditional;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanAdditionalProfGen;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanGen;

/**
 * Учебный план ДПО
 */
public class EppEduPlanAdditionalProf extends EppEduPlanAdditionalProfGen
{
    private String getTitle(boolean withArchive)
    {
        final StringBuilder sb = new StringBuilder();
        sb.append("№").append(this.getNumber());
        if (withArchive && EppState.STATE_ARCHIVED.equals(getState().getCode())) {
            sb.append(" архив");
        }
        sb.append(" ").append(getEduProgram().getShortTitle());

        final String titlePostfix = StringUtils.trimToNull(this.getTitlePostfix());
        if (null != titlePostfix) { sb.append(" ").append(titlePostfix); }

        return sb.toString();
    }

    @EntityDSLSupport(parts = { EppEduPlanGen.P_NUMBER })
    @Override
    public String getTitle()
    {
        if (getEduProgram() == null) {
            return this.getClass().getSimpleName();
        }
        return getTitle(false);
    }

    @Override
    public String getTitleWithArchive() {
        return getTitle(true);
    }

    @Override
    public String getEducationElementSimpleTitle()
    {
        return getEduProgram().getTitle();
    }

    @Override
    public String getEducationElementShortTitle()
    {
        return getEduProgram().getShortTitle();
    }

    @Override
    public String getEducationElementCode()
    {
        return null;
    }

    @Override
    public String getInfoBlockPageName()
    {
        return "ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.Pub.EppEduPlanAdditionalProfInfoBlock";
    }

    @Override
    public Class<? extends EduProgram> getEduProgramClass()
    {
        return EduProgramAdditional.class;
    }
}