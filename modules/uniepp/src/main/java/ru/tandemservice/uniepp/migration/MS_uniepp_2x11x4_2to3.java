package ru.tandemservice.uniepp.migration;

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.beanutils.converters.DateTimeConverter;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author avedernikov
 * @since 13.04.2017
 */
public class MS_uniepp_2x11x4_2to3 extends IndependentMigrationScript
{
	@Override
	public ScriptDependency[] getBoundaryDependencies()
	{
		return new ScriptDependency[]
				{
						new ScriptDependency("org.tandemframework", "1.6.18"),
						new ScriptDependency("org.tandemframework.shared", "1.11.4")
				};
	}

	@Override
	public void run(DBTool tool) throws Exception
	{
        DateTimeConverter dtConverter = new DateConverter();
        dtConverter.setPattern("yyyy-MM-dd");
        ConvertUtils.register(dtConverter, Date.class);

        Map<Integer, Long> intYear2EduYearId = getIntYear2EduYearId(tool);
		Collection<Long> archivalGroupIds = getArchivalGroupIds(tool);
		Collection<Long> updatedGroupIds = updateEndingYearByWorkPlans(tool, intYear2EduYearId);
		Collection<Long> groupWOWorkPlansIds = Sets.difference(new HashSet<>(archivalGroupIds), new HashSet<>(updatedGroupIds));
		for (Collection<Long> groupIds : Iterables.partition(groupWOWorkPlansIds, DQL.MAX_VALUES_ROW_NUMBER))
			updateGroupWOWorkPlans(tool, groupIds, intYear2EduYearId);
		System.out.println("STOP!!!");
	}

	private static Collection<Long> getArchivalGroupIds(DBTool tool) throws SQLException
	{
		final SQLSelectQuery archivalGroupIdQuery = new SQLSelectQuery().from(SQLFrom.table("group_t", "gr")).column("gr.id").where("gr.archival_p = ?");
		List<Object[]> rows = tool.executeQuery(MigrationUtils.processor(Long.class), tool.getDialect().getSQLTranslator().toSql(archivalGroupIdQuery), true);
		return rows.stream().map((Object[] row) -> (Long)row[0]).collect(Collectors.toList());
	}

	private static Collection<Long> updateEndingYearByWorkPlans(DBTool tool, Map<Integer, Long> intYear2EduYearId) throws SQLException
	{
		ISQLTranslator translator = tool.getDialect().getSQLTranslator();
		SQLFrom groupWPEduYearSource = SQLFrom.table("student_t", "student")
				.innerJoin(SQLFrom.table("epp_student_eduplanversion_t", "studentEpv"), "studentEpv.student_id = student.id")
				.innerJoin(SQLFrom.table("epp_student_workplan_t", "studentWP"), "studentWP.studenteduplanversion_id = studentEpv.id")
				.innerJoin(SQLFrom.table("epp_year_epp_t", "yearEduProc"), "studentWP.cachedeppyear_id = yearEduProc.id")
				.innerJoin(SQLFrom.table("educationyear_t", "eduYear"), "yearEduProc.educationyear_id = eduYear.id");
		SQLSelectQuery eduYearForGrupsQuery = new SQLSelectQuery().from(groupWPEduYearSource)
				.column("student.group_id")
				.where("student.group_id in (select id from group_t gr where gr.archival_p = ?)")
				.where("studentEpv.removalDate_p is null")
				.group("student.group_id")
				.column("max( eduYear.intValue_p )");
		List<Object[]> rows = tool.executeQuery(MigrationUtils.processor(Long.class, Integer.class), translator.toSql(eduYearForGrupsQuery), true);

		MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update group_t set endingyear_id=? where id=?", DBType.LONG, DBType.LONG);
		rows.forEach((Object[] row) -> updater.addBatch(intYear2EduYearId.get(row[1]), row[0]));
		updater.executeUpdate(tool);

		return rows.stream().map((Object[] row) -> (Long)row[0]).collect(Collectors.toList());
	}

	private static void updateGroupWOWorkPlans(DBTool tool, Collection<Long> groupWOWorkPlansIds, Map<Integer, Long> intYear2EduYearId) throws SQLException
	{
		final String groupIdsAsString = groupWOWorkPlansIds.stream().map(String::valueOf).collect(Collectors.joining(", "));
		SQLSelectQuery groupWOWorkPlansQuery = new SQLSelectQuery().from(SQLFrom.table("group_t", "gr"))
				.column("gr.id").column("gr.archivingdate_p")
				.where("gr.id in (" + groupIdsAsString + ")");
		List<Object[]> rows = tool.executeQuery(MigrationUtils.processor(Long.class, Date.class), tool.getDialect().getSQLTranslator().toSql(groupWOWorkPlansQuery));

		MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update group_t set endingyear_id=? where id=?", DBType.LONG, DBType.LONG);
		for (Object[] row : rows)
		{
			Date archivalDate = (Date)row[1];
			int year = getIntYear(archivalDate);
			updater.addBatch(intYear2EduYearId.get(year), row[0]);
		}
		updater.executeUpdate(tool);
	}

	private static Map<Integer, Long> getIntYear2EduYearId(DBTool tool) throws SQLException
	{
		SQLSelectQuery eduYearQuery = new SQLSelectQuery().from(SQLFrom.table("educationyear_t", "eduYear")).column("eduYear.id").column("eduYear.intValue_p");
		List<Object[]> rows = tool.executeQuery(MigrationUtils.processor(Long.class, Integer.class), tool.getDialect().getSQLTranslator().toSql(eduYearQuery));
		return rows.stream().collect(Collectors.toMap((Object[] row) -> (Integer)row[1], (Object[] row) -> (Long)row[0]));
	}

	private static int getIntYear(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		return calendar.before(eduYearStart(year)) ? year - 1 : year;
	}

	private static Map<Integer, Calendar> year2StartEduYear = new HashMap<>();

	private static Calendar eduYearStart(int year)
	{
		final Calendar result = year2StartEduYear.get(year);
		if (result != null)
			return result;

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.MONTH, Calendar.SEPTEMBER);
		calendar.set(Calendar.YEAR, year);
		year2StartEduYear.put(year, calendar);
		return calendar;
	}
}
