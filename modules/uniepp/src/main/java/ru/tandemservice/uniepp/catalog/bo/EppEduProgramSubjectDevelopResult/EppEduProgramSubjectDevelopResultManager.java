/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppEduProgramSubjectDevelopResult;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Igor Belanov
 * @since 03.03.2017
 */
@Configuration
public class EppEduProgramSubjectDevelopResultManager extends BusinessObjectManager
{
}
