package ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.ui.InContractList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.config.itemList.IItemListExtPointBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.InContractSectionList.ICtrInContractListComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.util.CtrContractVersionUtil;

import ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.ui.SectionPromice.EppCtrEducationPromiceSectionPromice;
import ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.ui.SectionResult.EppCtrEducationPromiceSectionResult;

/**
 * @author vdanilov
 */
@Configuration
public class EppCtrEducationPromiceInContractList extends BusinessComponentManager implements ICtrInContractListComponentManager {

    @Bean
    @Override
    @SuppressWarnings("unchecked")
    public ItemListExtPoint<Class<? extends BusinessComponentManager>> getSectionList() {
        return ((IItemListExtPointBuilder)this.itemList(Class.class))
        .add("epp.ctr.edu.promice", EppCtrEducationPromiceSectionPromice.class)
        .add("epp.ctr.edu.result", EppCtrEducationPromiceSectionResult.class)
        .create();
    }

    @Override
    public void fillPermissionGroup(final PermissionGroupMeta parent, final String securityPrefix) {
        CtrContractVersionUtil.registerPermissionGroup(parent, securityPrefix, this);
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }


}
