/* $Id$ */
package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionTrajectoryList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.SimpleListDataSource;

/**
 * @author oleyba
 * @since 5/5/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        final SimpleListDataSource<EppTrajectoryWrapper> dataSource = model.getDataSource();
        dataSource.clearColumns();
        dataSource.addColumn(new PublisherLinkColumn("Рабочие планы", EppTrajectoryWrapper.VIEW_PROPERTY_TITLE).setResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(final IEntity entity)
            {
                return new ParametersMap().add("ids", entity.getProperty("workplanIds")).add("showAsPub", true).add("showWorkplans", true);
            }

            @Override
            public String getComponentName(final IEntity entity)
            {
                return "ru.tandemservice.uniepp.component.workplan.WorkPlanTrajectoryPub";
            }
        }).setClickable(true).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Студентов на траектории", EppTrajectoryWrapper.VIEW_PROPERTY_STUDENT_COUNT).setClickable(false).setOrderable(false));
        final AbstractColumn stateColumn = new SimpleColumn("Состояние", EppTrajectoryWrapper.VIEW_PROPERTY_STATE).setClickable(false).setOrderable(false);
        stateColumn.setStyleResolver(rowEntity -> {
            if ((rowEntity instanceof EppTrajectoryWrapper) && ((EppTrajectoryWrapper) rowEntity).isHasErrors()) {
                return "background-color: " + ((EppTrajectoryWrapper) rowEntity).getErrorColor();
            }
            return "";
        });
        dataSource.addColumn(stateColumn);
    }
}
