package ru.tandemservice.uniepp.entity.plan.data;

import ru.tandemservice.uniepp.dao.index.IEppGroupImIndexedRow;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvGroupImRowGen;

/**
 * Запись версии УП (дисциплина по выбору)
 */
public class EppEpvGroupImRow extends EppEpvGroupImRowGen implements IEppGroupImIndexedRow
{
    public EppEpvGroupImRow() {
        super();
    }

    public EppEpvGroupImRow(final EppEduPlanVersionBlock block) {
        this();
        this.setOwner(block);
    }

    @Override public EppRegistryStructure getType() { return null; }

    @Override public String getRowType() { return "ДВ"; }
    @Override public String getDisplayableTitle() { return this.getTitle() /* +" ("+getRowType()+")"*/; }
}