package ru.tandemservice.uniepp.entity.registry;

import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryDisciplineGen;

/**
 * Дисциплина (из реестра)
 */
public class EppRegistryDiscipline extends EppRegistryDisciplineGen
{
    /**
     * [Название дисциплины] ([всего часов по дисциплине] ч., частей [Число частей], [Сокр. название читающего подразделения], [тип] №[номер из реестра мероприятий (дисциплин)])
     **/
    @Override
    public String getEducationElementTitle() {
        return this.getTitle() + " (" +this.getFormattedLoad() + ", частей " + this.getParts()+", " + this.getOwner().getShortTitle() + ", "+this.getNumberWithAbbreviation()+")";
    }

    /**
     * [Название дисциплины]
     */
    @Override
    public String getEducationElementSimpleTitle()
    {
        return this.getTitle();
    }
}