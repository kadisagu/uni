package ru.tandemservice.uniepp.entity.plan.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Нагрузка строки версии УП в семестре (по формам контрольных мероприятий)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvRowTermActionGen extends EntityBase
 implements INaturalIdentifiable<EppEpvRowTermActionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction";
    public static final String ENTITY_NAME = "eppEpvRowTermAction";
    public static final int VERSION_HASH = 1643262806;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW_TERM = "rowTerm";
    public static final String L_CONTROL_ACTION_TYPE = "controlActionType";
    public static final String P_SIZE = "size";

    private EppEpvRowTerm _rowTerm;     // Семестр строки УП
    private EppControlActionType _controlActionType;     // Форма контроля
    private int _size;     // Число элементов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Семестр строки УП. Свойство не может быть null.
     */
    @NotNull
    public EppEpvRowTerm getRowTerm()
    {
        return _rowTerm;
    }

    /**
     * @param rowTerm Семестр строки УП. Свойство не может быть null.
     */
    public void setRowTerm(EppEpvRowTerm rowTerm)
    {
        dirty(_rowTerm, rowTerm);
        _rowTerm = rowTerm;
    }

    /**
     * @return Форма контроля. Свойство не может быть null.
     */
    @NotNull
    public EppControlActionType getControlActionType()
    {
        return _controlActionType;
    }

    /**
     * @param controlActionType Форма контроля. Свойство не может быть null.
     */
    public void setControlActionType(EppControlActionType controlActionType)
    {
        dirty(_controlActionType, controlActionType);
        _controlActionType = controlActionType;
    }

    /**
     * @return Число элементов. Свойство не может быть null.
     */
    @NotNull
    public int getSize()
    {
        return _size;
    }

    /**
     * @param size Число элементов. Свойство не может быть null.
     */
    public void setSize(int size)
    {
        dirty(_size, size);
        _size = size;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEpvRowTermActionGen)
        {
            if (withNaturalIdProperties)
            {
                setRowTerm(((EppEpvRowTermAction)another).getRowTerm());
                setControlActionType(((EppEpvRowTermAction)another).getControlActionType());
            }
            setSize(((EppEpvRowTermAction)another).getSize());
        }
    }

    public INaturalId<EppEpvRowTermActionGen> getNaturalId()
    {
        return new NaturalId(getRowTerm(), getControlActionType());
    }

    public static class NaturalId extends NaturalIdBase<EppEpvRowTermActionGen>
    {
        private static final String PROXY_NAME = "EppEpvRowTermActionNaturalProxy";

        private Long _rowTerm;
        private Long _controlActionType;

        public NaturalId()
        {}

        public NaturalId(EppEpvRowTerm rowTerm, EppControlActionType controlActionType)
        {
            _rowTerm = ((IEntity) rowTerm).getId();
            _controlActionType = ((IEntity) controlActionType).getId();
        }

        public Long getRowTerm()
        {
            return _rowTerm;
        }

        public void setRowTerm(Long rowTerm)
        {
            _rowTerm = rowTerm;
        }

        public Long getControlActionType()
        {
            return _controlActionType;
        }

        public void setControlActionType(Long controlActionType)
        {
            _controlActionType = controlActionType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppEpvRowTermActionGen.NaturalId) ) return false;

            EppEpvRowTermActionGen.NaturalId that = (NaturalId) o;

            if( !equals(getRowTerm(), that.getRowTerm()) ) return false;
            if( !equals(getControlActionType(), that.getControlActionType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRowTerm());
            result = hashCode(result, getControlActionType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRowTerm());
            sb.append("/");
            sb.append(getControlActionType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvRowTermActionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvRowTermAction.class;
        }

        public T newInstance()
        {
            return (T) new EppEpvRowTermAction();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "rowTerm":
                    return obj.getRowTerm();
                case "controlActionType":
                    return obj.getControlActionType();
                case "size":
                    return obj.getSize();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "rowTerm":
                    obj.setRowTerm((EppEpvRowTerm) value);
                    return;
                case "controlActionType":
                    obj.setControlActionType((EppControlActionType) value);
                    return;
                case "size":
                    obj.setSize((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "rowTerm":
                        return true;
                case "controlActionType":
                        return true;
                case "size":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "rowTerm":
                    return true;
                case "controlActionType":
                    return true;
                case "size":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "rowTerm":
                    return EppEpvRowTerm.class;
                case "controlActionType":
                    return EppControlActionType.class;
                case "size":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvRowTermAction> _dslPath = new Path<EppEpvRowTermAction>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvRowTermAction");
    }
            

    /**
     * @return Семестр строки УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction#getRowTerm()
     */
    public static EppEpvRowTerm.Path<EppEpvRowTerm> rowTerm()
    {
        return _dslPath.rowTerm();
    }

    /**
     * @return Форма контроля. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction#getControlActionType()
     */
    public static EppControlActionType.Path<EppControlActionType> controlActionType()
    {
        return _dslPath.controlActionType();
    }

    /**
     * @return Число элементов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction#getSize()
     */
    public static PropertyPath<Integer> size()
    {
        return _dslPath.size();
    }

    public static class Path<E extends EppEpvRowTermAction> extends EntityPath<E>
    {
        private EppEpvRowTerm.Path<EppEpvRowTerm> _rowTerm;
        private EppControlActionType.Path<EppControlActionType> _controlActionType;
        private PropertyPath<Integer> _size;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Семестр строки УП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction#getRowTerm()
     */
        public EppEpvRowTerm.Path<EppEpvRowTerm> rowTerm()
        {
            if(_rowTerm == null )
                _rowTerm = new EppEpvRowTerm.Path<EppEpvRowTerm>(L_ROW_TERM, this);
            return _rowTerm;
        }

    /**
     * @return Форма контроля. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction#getControlActionType()
     */
        public EppControlActionType.Path<EppControlActionType> controlActionType()
        {
            if(_controlActionType == null )
                _controlActionType = new EppControlActionType.Path<EppControlActionType>(L_CONTROL_ACTION_TYPE, this);
            return _controlActionType;
        }

    /**
     * @return Число элементов. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction#getSize()
     */
        public PropertyPath<Integer> size()
        {
            if(_size == null )
                _size = new PropertyPath<Integer>(EppEpvRowTermActionGen.P_SIZE, this);
            return _size;
        }

        public Class getEntityClass()
        {
            return EppEpvRowTermAction.class;
        }

        public String getEntityName()
        {
            return "eppEpvRowTermAction";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
