package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид учебной группы (ФИК)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppGroupTypeFCAGen extends EppGroupType
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA";
    public static final String ENTITY_NAME = "eppGroupTypeFCA";
    public static final int VERSION_HASH = 1136665262;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppGroupTypeFCAGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppGroupTypeFCAGen> extends EppGroupType.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppGroupTypeFCA.class;
        }

        public T newInstance()
        {
            return (T) new EppGroupTypeFCA();
        }
    }
    private static final Path<EppGroupTypeFCA> _dslPath = new Path<EppGroupTypeFCA>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppGroupTypeFCA");
    }
            

    public static class Path<E extends EppGroupTypeFCA> extends EppGroupType.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EppGroupTypeFCA.class;
        }

        public String getEntityName()
        {
            return "eppGroupTypeFCA";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
