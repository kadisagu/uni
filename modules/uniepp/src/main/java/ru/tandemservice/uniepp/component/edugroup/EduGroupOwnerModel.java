package ru.tandemservice.uniepp.component.edugroup;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.component.base.SimpleOrgUnitBasedModel;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;

/**
 * @author vdanilov
 */
public abstract class EduGroupOwnerModel extends SimpleOrgUnitBasedModel {

    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder() {
        @Override protected String getSecModelPostfix() {
            return EduGroupOwnerModel.this.getSecModelPostfix(super.getSecModelPostfix());
        }
    };

    public abstract EppRealEduGroupCompleteLevel getLevel();

    public abstract String getSecModelPostfix(String secModelPostfix);

    public OrgUnitHolder getOrgUnitHolder() { return this.orgUnitHolder; }
    public CommonPostfixPermissionModelBase getSec() { return this.getOrgUnitHolder().getSecModel(); }
    public Long getOrgUnitId() { return this.getOrgUnitHolder().getId(); }

    @Override public OrgUnit getOrgUnit() { return this.getOrgUnitHolder().getValue(); }

}
