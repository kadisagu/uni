package ru.tandemservice.uniepp.base.bo.Student.ui.LifecycleTab;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.process.*;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.IRowCustomizer;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.Student.StudentManager;
import ru.tandemservice.uniepp.dao.group.EppRealGroupRowDAO;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;

import java.util.*;

/**
 * @author avedernikov
 * @since 05.12.2016
 */
@State({
	@Bind(key = StudentLifecycleTabUI.KEY_STUDENT_ID, binding = "studentHolder.id")
	})
public class StudentLifecycleTabUI extends UIPresenter
{
	public static final String KEY_STUDENT_ID = "studentId";

	public static final String PROP_YEAR_PART = "yearPart";
	public static final String PROP_YEAR_PART_INACTIVE = "yearPartInactive";

	private final EntityHolder<Student> studentHolder = new EntityHolder<>();

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case StudentLifecycleTab.YEAR_PART_DS:
				dataSource.put(StudentLifecycleTab.FILTER_STUDENT, getStudent());
				dataSource.put(StudentLifecycleTab.FILTER_ACTIVE_WPE, getActiveWpeFilter());
				break;
		}
	}

	final String DefaultSyncMessage = "обновляется";

	/** Дата последнего обновления МСРП демонами. Если хотя бы один из двух демонов активен в этот момент, вместо даты выводится строка "обновляется". */
	public String getLastSyncDate()
	{
		if (EppStudentSlotDAO.DAEMON.isDaemonNeedWakeUp() || EppRealGroupRowDAO.DAEMON.isDaemonNeedWakeUp())
			return DefaultSyncMessage;

		final long studentSlotLastWakeUp = EppStudentSlotDAO.DAEMON.getLastWakeUpTimestamp();
		final long realGroupRowLastWakeUp = EppRealGroupRowDAO.DAEMON.getLastWakeUpTimestamp();
		return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(Math.max(studentSlotLastWakeUp, realGroupRowLastWakeUp)));
	}

	/** Кнопка "Список актуализирован: (время последнего обновления)". Запустить демона обновления МСРП. */
	public void onClickSync()
	{
		final IBackgroundProcess process = new BackgroundProcessBase()
		{
			@Override
			public ProcessResult run(final ProcessState state)
			{
				try
				{
					state.setMessage("Обновление");

					while (true)
					{
						try { Thread.sleep(1000); }
						catch (final Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }

						state.setMessage("Обновление списка МСРП");
						if (isDaemonExecuting(EppStudentSlotDAO.DAEMON))
							continue;

						state.setMessage("Обновление списка УГС");
						if (isDaemonExecuting(EppRealGroupRowDAO.DAEMON))
							continue;

						break;
					}
				}
				catch (final Throwable t)
				{
					// TODO: логировать исключения
				}

				state.setMessage("Список МСРП обновлен");
				return null; // закрываем диалог
			}
		};

		new BackgroundProcessHolder().start("Обновление списка МСРП", process, ProcessDisplayMode.custom);

		EppStudentSlotDAO.DAEMON.wakeUpDaemon();
	}

	private boolean isDaemonExecuting(SyncDaemon daemon)
	{
		return daemon.getLastRequestTimestamp() > daemon.getLastCompleteTimestamp();
	}

	// Приходится использовать это вместо нормального датасурса, т.к. набор подколонок колонки "Нагрузка" зависит от конкретного студента (передать в конфиг БК информацию о нем невозможно).
	public StaticListDataSource<ViewWrapper<EppStudentWorkPlanElement>> getStudentWpeDS()
	{
		StaticListDataSource<ViewWrapper<EppStudentWorkPlanElement>> dataSource = new StaticListDataSource<>();
		dataSource.setRowCustomizer(getRowCustomizer());
		createColumns().forEach(dataSource::addColumn);
		dataSource.setupRows(getStudentDSRows());
		return dataSource;
	}

	private IRowCustomizer<ViewWrapper<EppStudentWorkPlanElement>> getRowCustomizer()
	{
		return new SimpleRowCustomizer<ViewWrapper<EppStudentWorkPlanElement>>()
		{
			@Override
			public String getRowStyle(ViewWrapper<EppStudentWorkPlanElement> entity)
			{
				if (null != entity.getEntity().getRemovalDate())
					return "color: gray";
				return null;
			}
		};
	}

	/** Если на эту часть года есть хотя бы одно актуальное МСРП, то текст в колонке черный, иначе - серый (приоритет выше, чем у стиля всей строки). */
	private static final IStyleResolver yearPartStyle = row -> ((Boolean)row.getProperty(PROP_YEAR_PART_INACTIVE)) ? "color: gray" : "color: black";

	private List<AbstractColumn> createColumns()
	{
		IMergeRowIdResolver yearPartMerger = new SimpleMergeIdResolver(EppStudentWorkPlanElement.year().educationYear().s(), EppStudentWorkPlanElement.part().s());
		List<AbstractColumn> columns = new ArrayList<>();
		columns.add(new SimpleColumn("Часть учебного года", PROP_YEAR_PART, UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setOrderable(false)
				.setMergeRowIdResolver(yearPartMerger).setStyleResolver(yearPartStyle));
		columns.add(new SimpleColumn("Индекс", EppStudentWorkPlanElement.sourceRow().number()).setClickable(false).setOrderable(false));
		columns.add(new SimpleColumn("Название в РУП", EppStudentWorkPlanElement.sourceRow().title()).setClickable(false).setOrderable(false));
		columns.add(new SimpleColumn("Дисциплина", EppStudentWorkPlanElement.registryElementPart().titleWithNumber()).setClickable(false).setOrderable(false));
		columns.add(new SimpleColumn("Читающее\nподразделение", EppStudentWorkPlanElement.registryElementPart().registryElement().owner().shortTitle()).setClickable(false).setOrderable(false));
		columns.add(new SimpleColumn("Дата утраты\nактуальности", EppStudentWorkPlanElement.removalDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
		columns.add(createGroupTypeHeadColumn().setWidth(1));
		return columns;
	}

	private HeadColumn createGroupTypeHeadColumn()
	{
		HeadColumn headColumn = new HeadColumn("groupTypes", "Нагрузка");
		List<EppGroupType> groupTypes = StudentManager.instance().dao().getStudentWpeGroupTypes(getStudent());
		for (EppGroupType groupType : groupTypes)
		{
			headColumn.addColumn(new BooleanColumn(groupType.getShortTitle(), groupType.getCode())
					.setVerticalHeader(true).setWidth(1).setClickable(false).setOrderable(false));
		}
		return headColumn;
	}

	private List<ViewWrapper<EppStudentWorkPlanElement>> getStudentDSRows()
	{
		List<EppStudentWorkPlanElement> studentWpes = StudentManager.instance().dao().getStudentWpe(getStudent(), getActiveWpeFilter(), getYearPart());
		Table<EducationYear, YearDistributionPart, Boolean> yearPart2Inactive = getYearPart2Inactive(studentWpes);

		List<EppGroupType> totalGroupTypes = StudentManager.instance().dao().getList(EppGroupType.class);

		Table<Long, String, Boolean> wpeIdAndGroupTypeCode2LoadExist = StudentManager.instance().dao().getWpeIdAndGroupTypeCode2LoadExist(getStudent());

		List<ViewWrapper<EppStudentWorkPlanElement>> wrappers = ViewWrapper.getPatchedList(studentWpes);
		for (ViewWrapper<EppStudentWorkPlanElement> wrapper : wrappers)
		{
			EppStudentWorkPlanElement wpe = wrapper.getEntity();
			wrapper.setViewProperty(PROP_YEAR_PART, getYearPartLine(wpe));
			wrapper.setViewProperty(PROP_YEAR_PART_INACTIVE, yearPart2Inactive.get(wpe.getYear().getEducationYear(), wpe.getPart()));
			Map<String, Boolean> groupTypeCode2LoadExist = wpeIdAndGroupTypeCode2LoadExist.row(wrapper.getId());
			for (EppGroupType groupType : totalGroupTypes)
			{
				String code = groupType.getCode();
				if (groupTypeCode2LoadExist.containsKey(code))
					wrapper.setViewProperty(code, groupTypeCode2LoadExist.get(code));
				else
					wrapper.setViewProperty(code, null);
			}
		}
		return wrappers;
	}

	/** Для пары "учебный год, часть учебного года" получить признак того, что все МСРП в этой части неактуальны (либо есть хотя бы одно актуальное). */
	private static Table<EducationYear, YearDistributionPart, Boolean> getYearPart2Inactive(Collection<EppStudentWorkPlanElement> studentWpes)
	{
		Multimap<PairKey<EducationYear, YearDistributionPart>, EppStudentWorkPlanElement> yearPart2Wpes = HashMultimap.create();
		studentWpes.forEach(wpe -> yearPart2Wpes.put(new PairKey<>(wpe.getYear().getEducationYear(), wpe.getPart()), wpe));

		Table<EducationYear, YearDistributionPart, Boolean> result = HashBasedTable.create();
		yearPart2Wpes.asMap().forEach((key, wpes) -> result.put(key.getFirst(), key.getSecond(), allInactive(wpes)));
		return result;
	}

	/** Все МСРП - неактуальны. */
	private static boolean allInactive(Collection<EppStudentWorkPlanElement> wpes)
	{
		return wpes.stream().allMatch(wpe -> wpe.getRemovalDate() != null);
	}

	/** «4: 2011/2013, зимний» (номер: год, краткое название части) */
	private static String getYearPartLine(EppStudentWorkPlanElement studentWpe)
	{
		final String term = String.valueOf(studentWpe.getTerm().getIntValue());
		final String eduYear = studentWpe.getYear().getEducationYear().getTitle();
		final String yearPart = studentWpe.getPart().getShortTitle();
		return term + ": " + eduYear + ",\n" + yearPart;
	}

	public EntityHolder<Student> getStudentHolder()
	{
		return studentHolder;
	}

	public Student getStudent()
	{
		return studentHolder.getValue();
	}

	private Boolean getActiveWpeFilter()
	{
		DataWrapper wrapper = getSettings().get("activeWpeFilter");
		return TwinComboDataSourceHandler.getSelectedValue(wrapper);
	}
	
	private EppYearPart getYearPart()
	{
		return getSettings().get("yearPart");
	}
}