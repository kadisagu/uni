package ru.tandemservice.uniepp.component.registry.RegElementPartAddModule;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.IComponentRegion;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.util.ParametersMap;

/**
 * @author vdanilov
 */
@SuppressWarnings("unchecked")
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override public void onRefreshComponent(final IBusinessComponent component) {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        final IComponentRegion region = component.getChildRegion("moduleInfo");
        if (null != region) { region.deactivateComponent(); }

        if (null == model.getModule().getValue()) {
            component.createChildRegion("moduleInfo", new ComponentActivator(
                    ru.tandemservice.uniepp.component.registry.ModuleRegistry.AddEdit.Model.class.getPackage().getName(),
                    new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getElement().getTutorOu().getId()).add("inline", Boolean.TRUE)
            ));
        } else {
            component.createChildRegion("moduleInfo", new ComponentActivator(
                    ru.tandemservice.uniepp.component.registry.ModuleRegistry.AddEdit.Model.class.getPackage().getName(),
                    new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getModule().getValue().getId()).add("inline", Boolean.TRUE)
            ));
        }


    }

    public void onClickApply(final IBusinessComponent component) {
        try {
            final IComponentRegion region = component.getChildRegion("moduleInfo");
            this.getDao().save(this.getModel(component), region.getActiveComponent());
            if (!UserContext.getInstance().getErrorCollector().hasErrors()) {
                this.deactivate(component);
            }
        } catch (final Throwable t) {
            this.onRefreshComponent(component);
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }
}
