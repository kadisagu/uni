package ru.tandemservice.uniepp.entity.pupnag;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.ISelectableByEducationYear;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearEducationProcessGen;

/**
 * Начало учебного года
 */
public class EppYearEducationProcess extends EppYearEducationProcessGen implements ISelectableByEducationYear, Comparable<EppYearEducationProcess>
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, EppYearEducationProcess.class)
                .titleProperty(EppYearEducationProcess.title().s())
                .filter(EppYearEducationProcess.title())
                .order(EppYearEducationProcess.educationYear().intValue(), OrderDirection.desc);
    }

    @Override
    public int compareTo(EppYearEducationProcess o) {
        return (this.getEducationYear().getIntValue() - o.getEducationYear().getIntValue());
    }
}