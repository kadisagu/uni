/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.EduProgramSubjectIndexManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.EppEduPlanManager;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.Pub.EppEduPlanPub;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;

/**
 * @author oleyba
 * @since 9/4/14
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "element.id"),
    @Bind(key = EppEduPlanManager.BIND_PROGRAM_KIND_ID, binding = "programKindId"),
    @Bind(key = EppEduPlanManager.BIND_ORG_UNIT_ID, binding = "orgUnitId")
})
public class EppEduPlanHigherProfAddEditUI extends UIPresenter
{
    private EppEduPlanHigherProf element = new EppEduPlanHigherProf();
    private DevelopCombination developCombination;

    private Long programKindId;
    private Long orgUnitId;

    @Override
    public void onComponentRefresh()
    {
        if (getElement().getId() != null) {
            setElement(DataAccessServices.dao().get(EppEduPlanHigherProf.class, getElement().getId()));
            setProgramKindId(getElement().getProgramKind().getId());
        } else {
            getElement().setNumber(INumberQueueDAO.instance.get().getNextNumber(getElement()));
            if (getOrgUnitId() != null && getElement().getOwner() == null) {
                getElement().setOwner(DataAccessServices.dao().get(OrgUnit.class, getOrgUnitId()));
            }
        }

        if (isEditForm() && (null != getElement().getState()) && (getElement().getState().isReadOnlyState())) {
            throw new ApplicationException("Учебный план находится в состоянии «" + getElement().getState().getTitle() + "», редактирование запрещено.");
        }
    }

    public boolean isEditForm() {
        return ((null != getElement()) && (null != getElement().getId()));
    }

    public boolean isDevelopCombinationPartsDisabled()
    {
        return this.isEditForm() || (null != this.getDevelopCombination());
    }

    public void onClickApply() {
        if (getElement().getProgramKind() == null) {
            getElement().setProgramKind(getElement().getProgramSubject().getEduProgramKind());
        }
        if (getElement().getState() == null) {
            getElement().setState(DataAccessServices.dao().get(EppState.class, EppState.code(), EppState.STATE_FORMATIVE));
        }
        if(isLabor())
            element.setWeeksAsLong(null);
        else
            element.setLaborAsLong(null);

        boolean justCreated = ! isEditForm();
        EppEduPlanManager.instance().dao().saveOrUpdateEduPlanProf(getElement());
        if (justCreated)
            _uiActivation.asDesktopRoot(EppEduPlanPub.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getElement().getId()).activate();
        deactivate();
    }

    public void onChangeCombination()
    {
        if (getDevelopCombination() != null) {
            getElement().setProgramForm(getDevelopCombination().getDevelopForm().getProgramForm());
            getElement().setDevelopCondition(getDevelopCombination().getDevelopCondition());
            getElement().setProgramTrait(getDevelopCombination().getDevelopTech().getProgramTrait());
            getElement().setDevelopGrid(getDevelopCombination().getDevelopGrid());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EppEduPlanHigherProfAddEdit.BIND_PROGRAM_SUBJECT, getElement().getProgramSubject());
        dataSource.put(EppEduPlanHigherProfAddEdit.BIND_PROGRAM_KIND_ID, getProgramKindId());
        if (EduProgramOrientation.DS_ORIENTATION.equals(dataSource.getName())) {
            dataSource.put(EduProgramSubjectIndexManager.PARAM_SUBJECT_INDEX, getElement().getProgramSubject() != null ? getElement().getProgramSubject().getSubjectIndex() : null);
        }
    }

    // getters and setters

    public EppEduPlanHigherProf getElement()
    {
        return element;
    }

    public void setElement(EppEduPlanHigherProf element)
    {
        this.element = element;
    }

    public DevelopCombination getDevelopCombination()
    {
        return developCombination;
    }

    public void setDevelopCombination(DevelopCombination developCombination)
    {
        this.developCombination = developCombination;
    }

    public Long getProgramKindId()
    {
        return programKindId;
    }

    public void setProgramKindId(Long programKindId)
    {
        this.programKindId = programKindId;
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }

    public boolean isLabor()
    {
        return element.getProgramSubject().isLabor();
    }

    public boolean isShowLabor()
    {
        return element.getProgramSubject() != null;
    }

    public boolean isShowOrientation() {
        return getElement().getProgramSubject() != null &&
                IUniBaseDao.instance.get().existsEntity(EduProgramOrientation.class, EduProgramOrientation.subjectIndex().s(), getElement().getProgramSubject().getSubjectIndex());
    }
}