/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphWeeksTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.BaseRawFormatter;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.callback.IRewindCallback;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.callback.ListenerCallback;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.*;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.tapestry.richTableList.RangeSelectionWeekTypeListDataSource;
import ru.tandemservice.uniepp.tapestry.richTableList.WeekTypeBlockColumn;

import java.util.*;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        model.setSettings(component.getSettings());

        this.getDao().prepare(model);

        this.prepareGraphDataSource(component);

        this.getDao().prepareGraphDataMap(model);
    }

    @SuppressWarnings("deprecation")
    private void prepareGraphDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final DynamicListDataSource<EppWorkGraphRow> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareGraphDataSource(model);
        });
        final RangeSelectionWeekTypeListDataSource<EppWorkGraphRow> rangeModel = new RangeSelectionWeekTypeListDataSource<>(dataSource);

        // мап типов недель
        dataSource.addColumn(new CheckboxColumn("checkbox", ""));
        dataSource.addColumn(new RadioColumn("radio", ""));
        dataSource.addColumn(new RowNumberColumn());

        for (final EppYearEducationWeek week : model.getWeekData())
        {
            final HeadColumn weekColumn = new HeadColumn(Integer.toString(week.getNumber()), week.getTitle());
            weekColumn.setVerticalHeader(true);

            final WeekTypeBlockColumn column = new WeekTypeBlockColumn(week.getId(), week.getNumber() - 1, Integer.toString(week.getNumber()));
            column.setHeaderStyle("padding-left:0;padding-right:0;text-align:center;min-width:17px;font-size:11px;");

            weekColumn.addColumn(column);
            dataSource.addColumn(weekColumn);
        }
        dataSource.addColumn(new RichSaveEditColumn(rangeModel, "onClickRichRowSave", "onClickRichRowEdit").setPermissionKey("edit_eppWorkGraph"));
        dataSource.addColumn(new RichCancelColumn(rangeModel, "onClickRichRowCancel").setPermissionKey("edit_eppWorkGraph"));


        final BaseRawFormatter<EppWorkGraphRow> eduPlanVersionTitleFormatter = new BaseRawFormatter<EppWorkGraphRow>()
        {
            @Override
            public String format(final EppWorkGraphRow entity)
            {
                final StringBuilder sb = new StringBuilder();
                for (final EppWorkGraphRow2EduPlan line : model.getRow2epvs().get(entity))
                {
                    sb.append("<div style='height:15px;white-space:nowrap;")
                    .append((model.getFiltedIds() == null) || model.getFiltedIds().contains(line.getId()) ? "" : "color:#999999;").append("'>")
                    .append(BaseRawFormatter.encode(line.getEduPlanVersion().getFullTitle()))
                    .append("</div>");
                }
                return sb.toString();
            }
        };

        final BaseRawFormatter<EppWorkGraphRow> eduPlanVersionExcludeColumnFormatter = new BaseRawFormatter<EppWorkGraphRow>()
        {
            @Override
            public String format(final EppWorkGraphRow entity)
            {
                final Collection<EppWorkGraphRow2EduPlan> lines = model.getRow2epvs().get(entity);
                if (lines.size() <= 1)
                {
                    return "";
                }
                final StringBuilder sb = new StringBuilder();
                final List<IRewindCallback> callbackList = model.getGraphDataSource().getCallbackList();
                for (final EppWorkGraphRow2EduPlan line : lines)
                {
                    final String id = "graph_exclude_epv_" + line.getId();
                    callbackList.add(new ListenerCallback(id, "onClickExclude", line.getId(), new HashSet<>(Collections.singleton("graph"))));
                    sb.append("<div id='")
                    .append(id)
                    .append("' onclick='buttonClick(event,this);return false;' style='height:15px;width:15px;white-space:nowrap;background-image:url(img/general/delete.png);cursor:pointer;' title='Исключить «")
                    .append(BaseRawFormatter.encode(line.getEduPlanVersion().getFullTitle()))
                    .append("» в отдельную группу'>&nbsp;</div>");
                }
                return sb.toString();
            }
        };

        dataSource.addColumn(new SimpleColumn("", "", eduPlanVersionExcludeColumnFormatter));
        dataSource.addColumn(new SimpleColumn("Версия УП", "", eduPlanVersionTitleFormatter).setClickable(false).setOrderable(false).setHeaderAlign("center"));



        model.setGraphDataSource(rangeModel);
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.ensureNotEditMode(model);
        component.saveSettings();
        this.getDao().prepareGraphDataMap(model);
        model.getGraphDataSource().getDataSource().refresh();
    }

    public void onClickClear(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.ensureNotEditMode(model);
        model.setCourse(null);
        model.setProgramSubject(null);
        this.onClickSearch(component);
    }

    public void onClickRichRowSave(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final Long rowId = component.getListenerParameter();
        this.getDao().updateGraphRow(rowId, model);
        model.getGraphDataSource().setEditId(null);
        this.resetLastVisitedEntityId(model);
    }

    public void onClickRichRowEdit(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final Long rowId = component.getListenerParameter();
        this.getDao().prepareEditRow(model, rowId);
        model.getGraphDataSource().setEditId(rowId);
        this.resetLastVisitedEntityId(model);
    }

    public void onClickRichRowCancel(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.getGraphDataSource().setEditId(null);
        this.getDao().prepareGraphDataMap(model);
        this.resetLastVisitedEntityId(model);
    }

    public void onClickPoint(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final Integer index = component.getListenerParameter();
        model.getGraphDataSource().getSelection().doPointClick(index);
        this.resetLastVisitedEntityId(model);
    }

    public void onClickCombineRows(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.ensureNotEditMode(model);
        final Set<Long> selected = ((CheckboxColumn) model.getGraphDataSource().getDataSource().getColumn("checkbox")).getSelected();
        final Long templateId = ((RadioColumn) model.getGraphDataSource().getDataSource().getColumn("radio")).getSelected();
        this.getDao().updateCombineRows(model, templateId, selected);
    }

    public void onClickExclude(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.ensureNotEditMode(model);
        final Long excludeId = component.getListenerParameter();
        this.getDao().updateExcludeRow(model, excludeId);
        this.resetLastVisitedEntityId(model);
    }

    private void ensureNotEditMode(final Model model)
    {
        if (model.getGraphDataSource().getEditId() != null)
        {
            throw new ApplicationException("В режиме редактирования это действие недоступно.");
        }
    }

    private void resetLastVisitedEntityId(final Model model)
    {
        model.getGraphDataSource().getDataSource().setLastVisitedEntityId(null);
    }
}