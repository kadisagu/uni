package ru.tandemservice.uniepp.base.bo.EppContract.daemon;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.daemon.ICtrContractVersionDaemonBean;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.base.bo.EppContract.logic.EppContractDao;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * @author vdanilov
 */
public class EppContractDaemonBean extends UniBaseDao implements IEppContractDaemonBean {

    public static final boolean linkOnlyActivatedVersion = true;

    public static final SyncDaemon DAEMON = new SyncDaemon(EppContractDao.class.getName(), 60, ICtrContractVersionDaemonBean.GLOBAL_DAEMON_LOCK, EppContractDaemonBean.GLOBAL_DAEMON_LOCK) {
        @Override protected void main() {
            IEppContractDaemonBean.instance.get().doUpdateEppCtrEducationResult();
        }
    };

    @Override
    public boolean doUpdateEppCtrEducationResult() {
        boolean result = false;
        final Session session = this.lock(EppCtrEducationResult.class.getName());
        final Date now = new Date();

        Debug.begin("doUpdateEppCtrEducationResult.insert");
        try {
            DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EppStudent2EduPlanVersion.class, "s2epv")
            .where(eq(property(EppStudent2EduPlanVersion.student().archival().fromAlias("s2epv")), value(Boolean.FALSE)))
            .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))

            .fromEntity(EppCtrEducationPromice.class, "p")
            .where(and(
                eq(property(EppCtrEducationPromice.eduPlanVersion().fromAlias("p")), property(EppStudent2EduPlanVersion.eduPlanVersion().fromAlias("s2epv"))),
                eq(property(EppCtrEducationPromice.educationOrgUnit().fromAlias("p")), property(EppStudent2EduPlanVersion.student().educationOrgUnit().fromAlias("s2epv"))),
                eq(property(EppCtrEducationPromice.dst().contactor().person().fromAlias("p")), property(EppStudent2EduPlanVersion.student().person().fromAlias("s2epv")))
            ))

            .joinPath(DQLJoinType.inner, EppCtrEducationPromice.src().owner().fromAlias("p"), "v")

            .column(property("s2epv", "id"), "s2epv_id")
            .column(property(CtrContractVersion.contract().id().fromAlias("v")), "contract_id");

            if (linkOnlyActivatedVersion) {
                // теккущая версия - плохо (надо по всем активированным, чтобы прицеплял студента, если студент когдато мог учиться по данному договору)
                //                dql
                //                .where(isNotNull(property(CtrContractVersion.activationDate().fromAlias("v"))))
                //                .where(and(
                //                    le(property(CtrContractVersion.activationDate().fromAlias("v")), value(now)),
                //                    or(
                //                        isNull(property(CtrContractVersion.removalDate().fromAlias("v"))),
                //                        gt(property(CtrContractVersion.removalDate().fromAlias("v")), value(now))
                //                    )
                //                ));

                // среди всех активированных
                dql.where(isNotNull(property(CtrContractVersion.activationDate().fromAlias("v"))));
            };

            //
            //            dql.where(notExists(
            //                new DQLSelectBuilder()
            //                .fromEntity(EppCtrEducationResult.class, "res").column(property("res.id"))
            //                .where(eq(property(EppCtrEducationResult.contract().fromAlias("res")), property(CtrContractVersion.contract().fromAlias("v"))))
            //                .where(eq(property(EppCtrEducationResult.target().eduPlanVersion().fromAlias("res")), property(EppCtrEducationPromice.eduPlanVersion().fromAlias("p"))))
            //                .where(eq(property(EppCtrEducationResult.target().student().educationOrgUnit().fromAlias("res")), property(EppCtrEducationPromice.eduPlanVersion().fromAlias("p"))))
            //                ));
            //
            //

            final List<Object[]> rows = new DQLSelectBuilder()
            .fromDataSource(dql.buildQuery(), "ds")
            .column(property("ds.s2epv_id"), "s2epv_id")
            .column(property("ds.contract_id"), "contract_id")

            .joinEntity("ds", DQLJoinType.left, EppCtrEducationResult.class, "r", and(
                eq(property(EppCtrEducationResult.contract().id().fromAlias("r")), property("ds.contract_id")),
                eq(property(EppCtrEducationResult.target().id().fromAlias("r")), property("ds.s2epv_id"))
            ))
            .where(isNull(property("r", "id")))
            .createStatement(session).list();

            if (rows.size() > 0)
            {
                BatchUtils.execute(rows, 256, new BatchUtils.Action<Object[]>() {
                    @Override public void execute(final Collection<Object[]> rows) {
                        for (final Object[] row: rows) {
                            final EppCtrEducationResult result = new EppCtrEducationResult();
                            result.setTarget((EppStudent2EduPlanVersion)session.load(EppStudent2EduPlanVersion.class, (Long)row[0]));
                            result.setContract((CtrContractObject)session.load(CtrContractObject.class, (Long)row[1]));
                            result.setTimestamp(now);
                            session.save(result);
                        }

                        session.flush();
                        session.clear();
                        EppContractDaemonBean.this.infoActivity('.');
                    }
                });
                result = true;
            }

        } finally {
            Debug.end();
        }

        return result;
    }
}
