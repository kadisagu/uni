/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppSystemAction.logic;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;

import java.util.Collection;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public interface IEppSystemActionDao extends INeedPersistenceSupport
{
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    public RtfDocument doGetFileNames4ImportToMdb();

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public void doCorrectEduGroupLevels();

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doShowEppForTutorOrgUnits();

    //    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    //    void doImportEduStdBlockFromEduPlan();

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doCreateSkills();

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doFixEduPlanNumbers();

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doMergeRegistryElements(Collection<Map.Entry<Long, Collection<Long>>> entrySet);

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<Long, Collection<Long>> getProcessMap(Long ownerId);

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doSetContractRoles();

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    int moveWorkPlans2specializationBlock(Logger logger);
}
