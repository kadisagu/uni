package ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.ui.SectionResult;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.shared.organization.sec.bo.Sec.logic.IContextSecuredComponentManager;

/**
 * @author vdanilov
 */
@Configuration
public class EppCtrEducationPromiceSectionResult extends BusinessComponentManager implements IContextSecuredComponentManager {

    @Override
    public void fillPermissionGroup(final PermissionGroupMeta parent, final String securityPostfix) {
        // ничего - действий нет
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

}