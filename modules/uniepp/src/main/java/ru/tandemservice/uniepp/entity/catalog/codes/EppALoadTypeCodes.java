package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид аудиторной нагрузки (типы аудиторных занятий)"
 * Имя сущности : eppALoadType
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppALoadTypeCodes
{
    /** Константа кода (code) элемента : Лекции (title) */
    String TYPE_LECTURES = "1";
    /** Константа кода (code) элемента : Практические занятия (title) */
    String TYPE_PRACTICE = "2";
    /** Константа кода (code) элемента : Лабораторные занятия (title) */
    String TYPE_LABS = "3";

    Set<String> CODES = ImmutableSet.of(TYPE_LECTURES, TYPE_PRACTICE, TYPE_LABS);
}
