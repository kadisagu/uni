/* $Id$ */
package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Alexey Lopatin
 * @since 01.04.2015
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x8x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.17"),
                new ScriptDependency("org.tandemframework.shared", "1.8.0")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement stmt2 = tool.getConnection().createStatement();
        stmt2.execute("select id from epp_group_type_t where code_p = 'alt.practice'");
        ResultSet src2 = stmt2.getResultSet();
        src2.next();
        long practiceId = src2.getLong(1);

        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select id from epp_c_cactiontype_t where title_p = 'Эссе' and catalogcode_p = 'eppIControlActionType'");

        ResultSet src = stmt.getResultSet();
        if (src.next())
            updateEssay(tool, src.getLong(1), practiceId);
        else
            addEssay(tool, practiceId);
    }

    private void updateEssay(DBTool tool, long id, long practiceId) throws Exception
    {
        PreparedStatement update = tool.prepareStatement("update epp_c_cactiontype_t set " +
                                                                 "code_p = ?, " +
                                                                 "title_p = ?, " +
                                                                 "shorttitle_p = ?, " +
                                                                 "abbreviation_p = ?, " +
                                                                 "activeineduplan_p = ?, " +
                                                                 "activeinworkplan_p = ?, " +
                                                                 "usedwithdisciplines_p = ?, " +
                                                                 "usedwithpractice_p = ?, " +
                                                                 "usedwithattestation_p = ? " +
                                                                 "where id = ?");
        update.setString(1, "29");
        update.setString(2, "Эссе");
        update.setString(3, "Эссе");
        update.setString(4, "Эссе");
        update.setBoolean(5, true);
        update.setBoolean(6, true);
        update.setBoolean(7, true);
        update.setBoolean(8, false);
        update.setBoolean(9, false);
        update.setLong(10, id);
        update.executeUpdate();

        PreparedStatement update2 = tool.prepareStatement("update epp_c_cactiontype_i_t set maxcount_p = ?, usercode_p = ?, defaultgrouptype_id = ? where id = ?");

        update2.setInt(1, 5);
        update2.setString(2, "ca_essay");
        update2.setLong(3, practiceId);
        update2.setLong(4, id);
        update2.executeUpdate();
    }

    private void addEssay(DBTool tool, long practiceId) throws Exception
    {
        PreparedStatement insert = tool.prepareStatement("insert into epp_c_cactiontype_t (id, discriminator, code_p, catalogcode_p, title_p, shorttitle_p, abbreviation_p, " +
                                                                 "activeineduplan_p, activeinworkplan_p, usedwithdisciplines_p, usedwithpractice_p, usedwithattestation_p, disabled_p) " +
                                                                 "values (?, ? ,?, ?, ?, ?, ? ,?, ?, ?, ?, ?, ?)");

        PreparedStatement insert2 = tool.prepareStatement("insert into epp_c_cactiontype_i_t (id, maxcount_p, usercode_p, defaultgrouptype_id) values (?, ? ,?, ?)");

        short entityCode = tool.entityCodes().ensure("eppIControlActionType");
        Long id = EntityIDGenerator.generateNewId(entityCode);

        insert.setLong(1, id);
        insert.setShort(2, entityCode);
        insert.setString(3, "29");
        insert.setString(4, "eppIControlActionType");
        insert.setString(5, "Эссе");
        insert.setString(6, "Эссе");
        insert.setString(7, "Эссе");
        insert.setBoolean(8, true);
        insert.setBoolean(9, true);
        insert.setBoolean(10, true);
        insert.setBoolean(11, false);
        insert.setBoolean(12, false);
        insert.setBoolean(13, false);
        insert.execute();

        insert2.setLong(1, id);
        insert2.setInt(2, 5);
        insert2.setString(3, "ca_essay");
        insert2.setLong(4, practiceId);
        insert2.execute();
    }
}