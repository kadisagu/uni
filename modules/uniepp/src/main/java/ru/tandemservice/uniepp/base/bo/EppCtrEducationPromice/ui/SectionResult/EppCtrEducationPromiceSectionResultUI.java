package ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.ui.SectionResult;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.Section.CtrSectionResultListUIPresenter;

import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;

/**
 * @author vdanilov
 */
public class EppCtrEducationPromiceSectionResultUI extends CtrSectionResultListUIPresenter<EppCtrEducationResult>
{
    @Override protected Class<EppCtrEducationResult> getResultClass() {
        return EppCtrEducationResult.class;
    }

    @Override
    protected boolean is_readonly() {
        return true; // всегда - факты исполнения добавляются демонами
    }
}

