package ru.tandemservice.uniepp.entity.pps.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pps.IEppPPSCollectionOwner;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент списка ППС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppPpsCollectionItemGen extends EntityBase
 implements INaturalIdentifiable<EppPpsCollectionItemGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem";
    public static final String ENTITY_NAME = "eppPpsCollectionItem";
    public static final int VERSION_HASH = -1684549296;
    private static IEntityMeta ENTITY_META;

    public static final String L_LIST = "list";
    public static final String L_PPS = "pps";

    private IEppPPSCollectionOwner _list;     // Список ППС
    private PpsEntry _pps;     // ППС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Список ППС. Свойство не может быть null.
     */
    @NotNull
    public IEppPPSCollectionOwner getList()
    {
        return _list;
    }

    /**
     * @param list Список ППС. Свойство не может быть null.
     */
    public void setList(IEppPPSCollectionOwner list)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && list!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IEppPPSCollectionOwner.class);
            IEntityMeta actual =  list instanceof IEntity ? EntityRuntime.getMeta((IEntity) list) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_list, list);
        _list = list;
    }

    /**
     * @return ППС. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getPps()
    {
        return _pps;
    }

    /**
     * @param pps ППС. Свойство не может быть null.
     */
    public void setPps(PpsEntry pps)
    {
        dirty(_pps, pps);
        _pps = pps;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppPpsCollectionItemGen)
        {
            if (withNaturalIdProperties)
            {
                setList(((EppPpsCollectionItem)another).getList());
                setPps(((EppPpsCollectionItem)another).getPps());
            }
        }
    }

    public INaturalId<EppPpsCollectionItemGen> getNaturalId()
    {
        return new NaturalId(getList(), getPps());
    }

    public static class NaturalId extends NaturalIdBase<EppPpsCollectionItemGen>
    {
        private static final String PROXY_NAME = "EppPpsCollectionItemNaturalProxy";

        private Long _list;
        private Long _pps;

        public NaturalId()
        {}

        public NaturalId(IEppPPSCollectionOwner list, PpsEntry pps)
        {
            _list = ((IEntity) list).getId();
            _pps = ((IEntity) pps).getId();
        }

        public Long getList()
        {
            return _list;
        }

        public void setList(Long list)
        {
            _list = list;
        }

        public Long getPps()
        {
            return _pps;
        }

        public void setPps(Long pps)
        {
            _pps = pps;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppPpsCollectionItemGen.NaturalId) ) return false;

            EppPpsCollectionItemGen.NaturalId that = (NaturalId) o;

            if( !equals(getList(), that.getList()) ) return false;
            if( !equals(getPps(), that.getPps()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getList());
            result = hashCode(result, getPps());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getList());
            sb.append("/");
            sb.append(getPps());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppPpsCollectionItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppPpsCollectionItem.class;
        }

        public T newInstance()
        {
            return (T) new EppPpsCollectionItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "list":
                    return obj.getList();
                case "pps":
                    return obj.getPps();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "list":
                    obj.setList((IEppPPSCollectionOwner) value);
                    return;
                case "pps":
                    obj.setPps((PpsEntry) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "list":
                        return true;
                case "pps":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "list":
                    return true;
                case "pps":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "list":
                    return IEppPPSCollectionOwner.class;
                case "pps":
                    return PpsEntry.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppPpsCollectionItem> _dslPath = new Path<EppPpsCollectionItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppPpsCollectionItem");
    }
            

    /**
     * @return Список ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem#getList()
     */
    public static IEppPPSCollectionOwnerGen.Path<IEppPPSCollectionOwner> list()
    {
        return _dslPath.list();
    }

    /**
     * @return ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem#getPps()
     */
    public static PpsEntry.Path<PpsEntry> pps()
    {
        return _dslPath.pps();
    }

    public static class Path<E extends EppPpsCollectionItem> extends EntityPath<E>
    {
        private IEppPPSCollectionOwnerGen.Path<IEppPPSCollectionOwner> _list;
        private PpsEntry.Path<PpsEntry> _pps;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Список ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem#getList()
     */
        public IEppPPSCollectionOwnerGen.Path<IEppPPSCollectionOwner> list()
        {
            if(_list == null )
                _list = new IEppPPSCollectionOwnerGen.Path<IEppPPSCollectionOwner>(L_LIST, this);
            return _list;
        }

    /**
     * @return ППС. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem#getPps()
     */
        public PpsEntry.Path<PpsEntry> pps()
        {
            if(_pps == null )
                _pps = new PpsEntry.Path<PpsEntry>(L_PPS, this);
            return _pps;
        }

        public Class getEntityClass()
        {
            return EppPpsCollectionItem.class;
        }

        public String getEntityName()
        {
            return "eppPpsCollectionItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
