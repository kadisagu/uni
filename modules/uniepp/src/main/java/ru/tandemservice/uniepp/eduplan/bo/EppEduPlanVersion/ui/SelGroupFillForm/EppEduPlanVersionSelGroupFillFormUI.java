/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.SelGroupFillForm;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.dao.registry.EppRegistryDAO;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.SelGroupRowAddEdit.EppEduPlanVersionSelGroupRowAddEditUI;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.ui.EppRegistryElementSelectModel;

import java.util.Collections;

/**
 * @author Nikolay Fedorovskih
 * @since 16.02.2015
 */
public class EppEduPlanVersionSelGroupFillFormUI extends UIPresenter
{
    private EppRegistryDiscipline _registryRow;
    private ISelectModel _registrySelectModel;

    @Override
    public void onComponentRefresh()
    {
        setRegistrySelectModel(new EppRegistryElementSelectModel<>(EppRegistryDiscipline.class));
    }

    // listeners

    public void onClickApply()
    {
        deactivate(ParametersMap.createWith(EppEduPlanVersionSelGroupRowAddEditUI.BIND_FILL_FROM_REGISTRY_ELEMENT_ID, getRegistryRow().getId()));
    }

    // getters & setters

    public String getRegistryElementLoadAsString() {
        final EppRegistryElement regEl = this.getRegistryRow();
        if (regEl == null) { return ""; }

        final ViewWrapper<IEntity> wrapper = EppRegistryDAO.fillRegistryElementWrappers(ViewWrapper.getPatchedList(Collections.<IEntity>singleton(regEl))).iterator().next();
        return (String) wrapper.getViewProperty("load");
    }

    public EppRegistryDiscipline getRegistryRow()
    {
        return _registryRow;
    }

    public void setRegistryRow(EppRegistryDiscipline registryRow)
    {
        _registryRow = registryRow;
    }

    public ISelectModel getRegistrySelectModel()
    {
        return _registrySelectModel;
    }

    public void setRegistrySelectModel(ISelectModel registrySelectModel)
    {
        _registrySelectModel = registrySelectModel;
    }
}