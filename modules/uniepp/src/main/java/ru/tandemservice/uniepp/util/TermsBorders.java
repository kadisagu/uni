package ru.tandemservice.uniepp.util;

import ru.tandemservice.uni.entity.catalog.Term;

import java.util.Map;

/**
 * Created by nsvetlov on 18.08.2016.
 */
public class TermsBorders
{
    public Integer[] terms;
    public int[] borders;

    public TermsBorders(Integer[] terms, int[] borders)
    {
        this.terms = terms;
        this.borders = borders;
    }

    public void setFirst(int termInCourse, int week)
    {
        if (termInCourse * 2 < borders.length)
        {
            try
            {
                borders[termInCourse * 2] = week;
            } catch (Exception ex)
            {
                // Ничего не делаем, т.к. либо строка не содержит номера, либо он за пределами массива.
            }
        }
    }

    public void setLast(int termInCourse, int week)
    {
        if (termInCourse * 2 < borders.length)
        {
            try
            {
                borders[termInCourse * 2 + 1] = week;
            } catch (Exception ex)
            {
                // Ничего не делаем, т.к. либо строка не содержит номера, либо он за пределами массива.
            }
        }
    }

    public Term getTerm(Map<Integer, Term> termMap, int week)
    {
        week--;
        for (int i = 0; i < borders.length; i += 2)
        {
            if (borders[i] <= week && week <= borders[i + 1])
            {
                return termMap.get(terms[i / 2]);
            }
        }
        return null;
    }

    public boolean out(String s)
    {
        int week = EppEduPlanVersionScheduleGridUtils.parseWeekCode(s) - 1;
        for (int i = 0; i < terms.length; i++)
        {
            if (borders.length > i*2 && borders[i * 2] <= week && week <= borders[i * 2 + 1])
            {
                return false;
            }
        }
        return true;
    }
}
