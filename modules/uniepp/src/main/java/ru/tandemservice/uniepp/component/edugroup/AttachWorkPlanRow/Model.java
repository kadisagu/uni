package ru.tandemservice.uniepp.component.edugroup.AttachWorkPlanRow;

import java.util.Collection;
import java.util.Collections;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key="ids", binding="ids", required=true),
    @Bind(key="levelId", binding="levelHolder.id")
})
public class Model
{
    private Collection<Long> ids = Collections.emptyList();
    public Collection<Long> getIds() { return this.ids; }
    public void setIds(final Collection<Long> ids) { this.ids = ids; }

    private final EntityHolder<EppRealEduGroupCompleteLevel> levelHolder = new EntityHolder<EppRealEduGroupCompleteLevel>();
    public EntityHolder<EppRealEduGroupCompleteLevel> getLevelHolder() { return this.levelHolder; }
    public EppRealEduGroupCompleteLevel getLevel() { return this.getLevelHolder().refresh(EppRealEduGroupCompleteLevel.class); }

    private final SelectModel<EppRegistryElementPart> regElementSelectModel = new SelectModel<EppRegistryElementPart>();
    public SelectModel<EppRegistryElementPart> getRegElementSelectModel() { return this.regElementSelectModel; }

    private final SelectModel<EppWorkPlanRegistryElementRow> wpRowSelectModel = new SelectModel<EppWorkPlanRegistryElementRow>();
    public SelectModel<EppWorkPlanRegistryElementRow> getWpRowSelectModel() { return this.wpRowSelectModel; }

}
