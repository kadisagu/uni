/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentWithIncorrectWorkPlan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.datasource.column.IPublisherColumnBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListManager;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 04.03.2015
 */
@Configuration
public class EppIndicatorStudentWithIncorrectWorkPlan extends AbstractEppIndicatorStudentListManager
{
    public static final String TERM_DS = "termDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(
                presenterExtPointBuilder()
                        .addDataSource(searchListDS(STUDENT_SEARCH_LIST_DS, studentSearchListDSColumnExtPoint(), studentSearchListDSHandler()))
                        .addDataSource(selectDS(TERM_DS, termDSHandler()))
                        .addDataSource(EducationCatalogsManager.instance().developTechDSConfig())
        );
    }

    @Bean
    public ColumnListExtPoint studentSearchListDSColumnExtPoint()
    {
        return createStudentSearchListColumnsBuilder()
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> termDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Term.class)
                .order(Term.intValue())
                .filter(Term.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchListDSHandler()
    {
        return new EppIndicatorStudentWithIncorrectWorkPlanSearchListDSHandler(getName());
    }

    @Override
    protected IPublisherColumnBuilder newFioColumnBuilder(IMergeRowIdResolver merger)
    {
        final IPublisherLinkResolver resolver = new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(final IEntity entity)
            {
                return new ParametersMap()
                        .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId())
                        .add("selectedStudentTab", "studentTab")
                        .add("selectedDataTab", "studentEppWorkplanTab");
            }
        };
        return publisherColumn(FIO_COLUMN, Student.person().fullFio()).formatter(RawFormatter.INSTANCE).publisherLinkResolver(resolver).order().required(true);
    }

    @Override
    protected List<ColumnBase> customizeStudentSearchListColumns(List<ColumnBase> columnList)
    {
        removeColumns(columnList, STATUS_COLUMN);
        return columnList;
    }
}
