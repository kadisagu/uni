package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_2x10x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppWeekPart

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_eduplan_weekpart_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eppweekpart"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("week_id", DBType.LONG).setNullable(false), 
				new DBColumn("weektype_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppWeekPart");

			Statement stmt = tool.getConnection().createStatement();

			stmt.execute("select distinct id, weektype_id  from epp_eduplan_ver_week_type_t");

			String insertQuery = "insert into epp_eduplan_weekpart_t (id, discriminator, number_p, week_id, weektype_id) values (?,?,?,?,?)";

			ResultSet rs = stmt.getResultSet();

			while (rs.next()) {
				for (int i = 0; i < 6;) {
					try {
						tool.executeUpdate(
								insertQuery,
								EntityIDGenerator.generateNewId(entityCode),
								entityCode,
								++i,
								rs.getLong(1),
								rs.getLong(2)
						);
					} catch (Exception ex) {
						throw ex;
					}
				}
			}

		}
    }
}