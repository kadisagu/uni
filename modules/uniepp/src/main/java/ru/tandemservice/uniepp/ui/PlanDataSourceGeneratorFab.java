/* $Id$ */
package ru.tandemservice.uniepp.ui;

import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;

/**
 * @author Andrey Andreev
 * @since 21.08.2015
 */
public class PlanDataSourceGeneratorFab
{


    public static EduPlanVersionBlockDataSourceGenerator getEduPlanVersionBlockDataSourceGenerator(IEppEpvBlockWrapper wrapper)
    {
        final boolean extendedView = wrapper.getVersion().getViewTableDiscipline().isExtend();

        return new EduPlanVersionBlockDataSourceGenerator()
        {
            @Override
            public boolean isCheckMode() { return false; }

            @Override
            public boolean isExtendedView() { return extendedView; }

            @Override
            protected boolean isEditable() { return false; }

            @Override
            protected String getPermissionKeyEdit() { return null; }

            @Override
            protected Long getVersionBlockId() { return wrapper.getBlock().getId(); }

            @Override
            public void doPrepareBlockDisciplineDataSource(
                    final StaticListDataSource<IEppEpvRowWrapper> dataSource,
                    final IBlockRenderContext context
            )
            {
                super.doPrepareBlockDisciplineDataSource(dataSource, context);
                dataSource.addColumn(wrap(this.getColumn_owner(true)));
            }

            @Override
            public void doPrepareBlockActionsDataSource(
                    final StaticListDataSource<IEppEpvRowWrapper> dataSource,
                    final IBlockRenderContext context
            )
            {
                super.doPrepareBlockActionsDataSource(dataSource, context);
                dataSource.addColumn(wrap(this.getColumn_owner(true)));
            }
        };

    }

    public static WorkPlanDataSourceGenerator getWorkPlanDataSourceGenerator(IEppWorkPlanWrapper wrapper)
    {
        return new WorkPlanDataSourceGenerator()
        {
            @Override
            protected boolean isEditable() { return false; }

            @Override
            protected String getPermissionKeyEdit() { return null; }

            @Override
            protected Long getWorkPlanBaseId() { return wrapper.getId(); }
        };
    }
}
