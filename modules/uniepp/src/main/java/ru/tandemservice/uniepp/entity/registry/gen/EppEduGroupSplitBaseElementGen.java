package ru.tandemservice.uniepp.entity.registry.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Базовый элемент деления потоков
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduGroupSplitBaseElementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement";
    public static final String ENTITY_NAME = "eppEduGroupSplitBaseElement";
    public static final int VERSION_HASH = 1773696405;
    private static IEntityMeta ENTITY_META;

    public static final String L_SPLIT_VARIANT = "splitVariant";
    public static final String P_SPLIT_ELEMENT_ID = "splitElementId";
    public static final String P_SPLIT_ELEMENT_SHORT_TITLE = "splitElementShortTitle";
    public static final String P_SPLIT_ELEMENT_TITLE = "splitElementTitle";

    private EppEduGroupSplitVariant _splitVariant;     // Способ деления потоков

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Способ деления потоков. Свойство не может быть null.
     */
    @NotNull
    public EppEduGroupSplitVariant getSplitVariant()
    {
        return _splitVariant;
    }

    /**
     * @param splitVariant Способ деления потоков. Свойство не может быть null.
     */
    public void setSplitVariant(EppEduGroupSplitVariant splitVariant)
    {
        dirty(_splitVariant, splitVariant);
        _splitVariant = splitVariant;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEduGroupSplitBaseElementGen)
        {
            setSplitVariant(((EppEduGroupSplitBaseElement)another).getSplitVariant());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduGroupSplitBaseElementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduGroupSplitBaseElement.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppEduGroupSplitBaseElement is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "splitVariant":
                    return obj.getSplitVariant();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "splitVariant":
                    obj.setSplitVariant((EppEduGroupSplitVariant) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "splitVariant":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "splitVariant":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "splitVariant":
                    return EppEduGroupSplitVariant.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduGroupSplitBaseElement> _dslPath = new Path<EppEduGroupSplitBaseElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduGroupSplitBaseElement");
    }
            

    /**
     * @return Способ деления потоков. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement#getSplitVariant()
     */
    public static EppEduGroupSplitVariant.Path<EppEduGroupSplitVariant> splitVariant()
    {
        return _dslPath.splitVariant();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement#getSplitElementId()
     */
    public static SupportedPropertyPath<Long> splitElementId()
    {
        return _dslPath.splitElementId();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement#getSplitElementShortTitle()
     */
    public static SupportedPropertyPath<String> splitElementShortTitle()
    {
        return _dslPath.splitElementShortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement#getSplitElementTitle()
     */
    public static SupportedPropertyPath<String> splitElementTitle()
    {
        return _dslPath.splitElementTitle();
    }

    public static class Path<E extends EppEduGroupSplitBaseElement> extends EntityPath<E>
    {
        private EppEduGroupSplitVariant.Path<EppEduGroupSplitVariant> _splitVariant;
        private SupportedPropertyPath<Long> _splitElementId;
        private SupportedPropertyPath<String> _splitElementShortTitle;
        private SupportedPropertyPath<String> _splitElementTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Способ деления потоков. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement#getSplitVariant()
     */
        public EppEduGroupSplitVariant.Path<EppEduGroupSplitVariant> splitVariant()
        {
            if(_splitVariant == null )
                _splitVariant = new EppEduGroupSplitVariant.Path<EppEduGroupSplitVariant>(L_SPLIT_VARIANT, this);
            return _splitVariant;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement#getSplitElementId()
     */
        public SupportedPropertyPath<Long> splitElementId()
        {
            if(_splitElementId == null )
                _splitElementId = new SupportedPropertyPath<Long>(EppEduGroupSplitBaseElementGen.P_SPLIT_ELEMENT_ID, this);
            return _splitElementId;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement#getSplitElementShortTitle()
     */
        public SupportedPropertyPath<String> splitElementShortTitle()
        {
            if(_splitElementShortTitle == null )
                _splitElementShortTitle = new SupportedPropertyPath<String>(EppEduGroupSplitBaseElementGen.P_SPLIT_ELEMENT_SHORT_TITLE, this);
            return _splitElementShortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement#getSplitElementTitle()
     */
        public SupportedPropertyPath<String> splitElementTitle()
        {
            if(_splitElementTitle == null )
                _splitElementTitle = new SupportedPropertyPath<String>(EppEduGroupSplitBaseElementGen.P_SPLIT_ELEMENT_TITLE, this);
            return _splitElementTitle;
        }

        public Class getEntityClass()
        {
            return EppEduGroupSplitBaseElement.class;
        }

        public String getEntityName()
        {
            return "eppEduGroupSplitBaseElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Long getSplitElementId();

    public abstract String getSplitElementShortTitle();

    public abstract String getSplitElementTitle();
}
