package ru.tandemservice.uniepp.component.workplan.WorkPlanRowAddEdit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.HBaseSelectModel;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.EppCustomEduPlanManager;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWorkPlanRowKindCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppRegistryStructureGen;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAction;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO
{
    static final Predicate NON_DISCIPLINE_RPEDICATE = object -> !((EppRegistryStructure)object).isDisciplineElement();

    @Override
    public void prepare(final Model model)
    {
        final IEntity entity = getNotNull(model.getId());
        if (entity instanceof EppWorkPlanRegistryElementRow)
        {
            // форма редактирования
            model.setRow((EppWorkPlanRegistryElementRow)entity);
            final EppRegistryElement rel = model.getRow().getRegistryElement();
            if (rel != null) {
                model.getRegistryElementSelectModel().setValue(rel);
                model.getRegistryElementPartSelectModel().setValue(model.getRow().getRegistryElementPart());
                if (rel instanceof EppRegistryAction) model.getRegistryStructureSelectModel().setValue(rel.getParent());
            }

            if (model.getRow().isReattestation()) {
                model.setNeedRetakeValue(new IdentifiableWrapper<>(EppCustomEduPlanManager.REATTESTATION_ITEM_ID, ""));
            } else if (model.getRow().isReexamination()) {
                model.setNeedRetakeValue(new IdentifiableWrapper<>(EppCustomEduPlanManager.REEXAMINATION_ITEM_ID, ""));
            } else {
                model.setNeedRetakeValue(null);
            }
        }
        else
        {
            // форма создания
            fillAddForm(model, entity);
        }

        // проверяем редактируемость
        if ((null != model.getRow().getId()) && (null != model.getRow().getWorkPlan().getState())) {
            model.getRow().getWorkPlan().getState().check_editable(model.getRow().getWorkPlan());
        }

        // Перезачет/переаттестация
        model.setRetakeSelectModel(EppCustomEduPlanManager.createRetakeSelectModel());

        // вид
        model.setKindList(getCatalogItemListOrderByCode(EppWorkPlanRowKind.class));

        fillSelectableModel(model);
    }

    /**
     * Source для выбираемых полей
     */
    private void fillSelectableModel(final Model model) {

        Iterable<Long> iterable = () -> {
            if (null == model.getRow().getRegistryElement()) { return null; }
            return Collections.singleton(model.getRow().getRegistryElement().getId()).iterator();
        };

        ISelectModel selectModel;

        if (model.isRowAction()) {
            // доп. мероприятия
            final List<EppRegistryStructure> registryList = getCatalogItemListOrderByCode(EppRegistryStructure.class);
            CollectionUtils.filter(registryList, NON_DISCIPLINE_RPEDICATE);
            model.getRegistryStructureSelectModel().setSource(new HBaseSelectModel() {
                private final List<HSelectOption> listHierarchyNodesWithParents = HierarchyUtil.listHierarchyNodesWithParents(registryList, true);
                @Override public ListResult findValues(final String filter) {
                    return new ListResult<>(listHierarchyNodesWithParents);
                }
            });
            // список элементов структуры реестра, откуда брать элементы реестра
            final Iterable<Long> registryStructureIdsIterable = () -> {
                final EppRegistryStructure element = model.getRegistryStructureSelectModel().getValue();
                if (null == element) { return UniBaseDao.ids(registryList).iterator(); }

                final List<Long> list = new ArrayList<>(registryList.size()/2);
                for (EppRegistryStructure rStruct : registryList) {
                    final EppRegistryStructure temp = rStruct;
                    while (null != rStruct) {
                        if (element.equals(rStruct)) {
                            list.add(temp.getId());
                        }
                        rStruct = rStruct.getParent();
                    }
                }
                return list.iterator();
            };
            selectModel = IEppWorkPlanDataDAO.instance.get().getRegistryActionSelectModel4Add(model.getRow().getWorkPlan(), registryStructureIdsIterable, iterable);
        } else {
            // дисциплины
            selectModel = IEppWorkPlanDataDAO.instance.get().getRegistryDisciplineSelectModel4Add(model.getRow().getWorkPlan(), iterable);
        }

        model.getRegistryElementSelectModel().setSource(selectModel);

        // части
        model.getRegistryElementPartSelectModel().setSource(new DQLFullCheckSelectModel(EppRegistryElementPart.P_NUMBER) {
            @Override protected DQLSelectBuilder query(final String alias, final String filter) {
                final EppRegistryElement regel = model.getRegistryElementSelectModel().getValue();
                if (null == regel) { return null; }
                return new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPart.class, alias)
                .where(eq(property(EppRegistryElementPart.registryElement().fromAlias(alias)), value(regel)))
                .order(property(EppRegistryElementPart.number().fromAlias(alias)));
            }
        });
    }

    /**
     * Заполнение формы создания объекта
     */
    private void fillAddForm(final Model model, IEntity entity) {

        // форма создания объекта

        if (entity instanceof EppWorkPlanBase)
        {
            // форма создания
            model.getRow().setWorkPlan((EppWorkPlanBase)entity);
            model.getRow().setKind(getCatalogItem(EppWorkPlanRowKind.class, EppWorkPlanRowKindCodes.MAIN));
            if (model.isRowAction()) model.getRegistryStructureSelectModel().setValue(get(EppRegistryStructure.class, EppRegistryStructureGen.code().s(), EppRegistryStructureCodes.REGISTRY_PRACTICE));
        }
        else if (entity instanceof EppWorkPlanTemplateRow)
        {
            // форма создания из шаблона
            final EppWorkPlanTemplateRow template = (EppWorkPlanTemplateRow)entity;
            model.getRow().setWorkPlan(template.getWorkPlan());
            model.getRow().setTitle(template.getTitle());
            model.getRow().setNumber(template.getNumber());
            model.getRow().setKind(template.getKind());
            model.getRow().setComment(template.getComment());
        }
        else
        {
            final IEntityMeta meta = EntityRuntime.getMeta(model.getId());
            throw new IllegalStateException("Unsupported entity class: " + (null == meta ? ("["+model.getId()+"]") : meta.getEntityClass().getName()));
        }
    }

    @Override
    public void update(final Model model)
    {
        final Session session = getSession();

        if (null == model.getRow().getId())
        {
            // если форма создания, перед сохранением объекта выставляем дисциплино-часть
            model.getRow().setRegistryElementPart(model.getRegistryElementPartSelectModel().getValue());

            // затем, если был шаблон - удаляем его
            final IEntity entity = get(model.getId());
            if (entity instanceof EppWorkPlanTemplateRow) { session.delete(entity); }
        }

        if (model.getNeedRetakeValue() == null) {
            model.getRow().setNeedRetake(null);
        } else {
            model.getRow().setNeedRetake(model.getNeedRetakeValue().getId().equals(EppCustomEduPlanManager.REATTESTATION_ITEM_ID));
        }

        // сохраняем данные в строку
        session.saveOrUpdate(model.getRow());
    }

    @Override
    public void validate(final Model model, final ErrorCollector errors)
    {
        final Date beginDate = model.getRow().getBeginDate();
        final Date endDate = model.getRow().getEndDate();

        if ((null != beginDate) && (null != endDate))
        {
            if (beginDate.compareTo(endDate) > 0)
            {
                errors.add("Дата начала должна быть раньше даты окончания.", "beginDate", "endDate");
            }
        }
    }
}
