package ru.tandemservice.uniepp.component.settings.EppControlActionTypeGrade;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * Created by IntelliJ IDEA.
 * User: Tandem
 * Date: 02.03.11
 * Time: 11:45
 * To change this template use File | Settings | File Templates.
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickApply(final IBusinessComponent context)
    {
        this.getDao().update(this.getModel(context));
    }

    public void onClickSave(final IBusinessComponent context)
    {
        this.getDao().update(this.getModel(context));
        this.deactivate(context);
    }
}
