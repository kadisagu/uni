package ru.tandemservice.uniepp.component.eduplan.row.AttachRegistryElement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.shared.commonbase.base.util.StaticFullCheckSelectModel;
import org.tandemframework.shared.commonbase.tapestry.component.list.SimpleListDataHolder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.data.EppControlFormatter;
import ru.tandemservice.uniepp.dao.registry.data.EppLoadFormatter;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author Zhuj
 */
@Input({
    @Bind(key = "ids", binding = "ids", required = true),
    @Bind(key = "orgUnitId", binding = "orgUnitId", required = true)
})
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();

    private Long orgUnitId;
    public Long getOrgUnitId() { return this.orgUnitId; }
    public void setOrgUnitId(Long orgUnitId) { this.orgUnitId = orgUnitId; }

    private Collection<Long> ids = Collections.emptyList();
    public Collection<Long> getIds() { return this.ids; }
    public void setIds(Collection<Long> ids) { this.ids = ids; }



    private Map<String, EppControlActionType> controlActionMap;
    public Map<String, EppControlActionType> getControlActionMap() { return this.controlActionMap; }
    public void setControlActionMap(Map<String, EppControlActionType> controlActionMap) { this.controlActionMap = controlActionMap; }

    private Map<String, EppLoadType> loadTypeMap;
    public Map<String, EppLoadType> getLoadTypeMap() { return this.loadTypeMap; }
    public void setLoadTypeMap(Map<String, EppLoadType> loadTypeMap) { this.loadTypeMap = loadTypeMap; }


    private final SimpleListDataHolder<IEppEpvRowWrapper> data = new SimpleListDataHolder<IEppEpvRowWrapper>();
    public SimpleListDataHolder<IEppEpvRowWrapper> getData() { return this.data; }

    private List<Integer> dataPartNumbers;
    public List<Integer> getDataPartNumbers() { return this.dataPartNumbers; }
    public void setDataPartNumbers(List<Integer> dataPartNumbers) { this.dataPartNumbers = dataPartNumbers; }

    private Integer dataPartNumber;
    public Integer getDataPartNumber() { return this.dataPartNumber; }
    public void setDataPartNumber(Integer dataPartNumber) { this.dataPartNumber = dataPartNumber; }

    public String getCurrentRowSizeString() {
        final IEppEpvRowWrapper iEppEpvRowWrapper = getData().getCurrent();
        return new StringBuilder()
        .append(UniEppUtils.formatLoad(iEppEpvRowWrapper.getTotalInTermLoad(0, EppLoadType.FULL_CODE_TOTAL_HOURS), false)).append(" / ")
        .append(UniEppUtils.formatLoad(iEppEpvRowWrapper.getTotalInTermLoad(0, EppLoadType.FULL_CODE_LABOR), false)).toString();
    }

    public String getCurrentRowTermLoad4PartNumber() {
        final IEppEpvRowWrapper current = getData().getCurrent();
        final int termNumber = current.getTermNumber(getDataPartNumber());
        if (termNumber > 0) {
            return new EppLoadFormatter<IEppEpvRowWrapper>(getLoadTypeMap()) {
                @Override protected Double load(IEppEpvRowWrapper element, String loadFullCode) {
                    return element.getTotalInTermLoad(termNumber, loadFullCode);
                }
            }.formatLoadByType(current);
        }
        return "";
    }

    public String getCurrentRowTermActions4PartNumber() {
        final IEppEpvRowWrapper current = getData().getCurrent();
        final int termNumber = current.getTermNumber(getDataPartNumber());
        if (termNumber > 0) {
            return new EppControlFormatter<IEppEpvRowWrapper>(getControlActionMap()) {
                @Override protected int size(IEppEpvRowWrapper element, String actionFullCode) {
                    return element.getActionSize(termNumber, actionFullCode);
                }
            }.format(current);
        }
        return "";
    }



    protected static final IdentifiableWrapper<IEntity> OPERATION_SELECT = new IdentifiableWrapper<IEntity>(1L, "Выбрать существующее мероприятие реестра");
    protected static final IdentifiableWrapper<IEntity> OPERATION_CREATE = new IdentifiableWrapper<IEntity>(2L, "Создать новое мероприятие реестра");
    private static final List<IdentifiableWrapper<IEntity>> OPERATION_LIST = Arrays.asList(
        Model.OPERATION_SELECT,
        Model.OPERATION_CREATE
    );

    public List<IdentifiableWrapper<IEntity>> getFormOperationList() { return Model.OPERATION_LIST; }

    private IdentifiableWrapper<IEntity> formOperation = Model.OPERATION_SELECT;
    public IdentifiableWrapper<IEntity> getFormOperation() { return (null != this.formOperation ? this.formOperation : Model.OPERATION_SELECT); }
    public void setFormOperation(IdentifiableWrapper<IEntity> formOperation) { this.formOperation = formOperation; }


    private final ISelectModel selectedRowSource = new StaticFullCheckSelectModel(
        "owner.eduPlanVersion.title",
        "owner.eduPlanVersion.eduPlan.developCombinationTitle",
        "index",
        "title",
        "row.registryElementType.abbreviation"
    ) {
        @Override protected List list() { return new ArrayList<>(getData().getRows()); }
    };
    public ISelectModel getSelectedRowSource() { return this.selectedRowSource; }

    private IEppEpvRowWrapper selectedRow;
    public IEppEpvRowWrapper getSelectedRow() { return this.selectedRow; }
    public void setSelectedRow(IEppEpvRowWrapper selectedRow) { this.selectedRow = selectedRow; }


    protected static final IdentifiableWrapper<IEntity> SOURCE_SIM = new IdentifiableWrapper<IEntity>(1L, "Похожие мероприятия");
    protected static final IdentifiableWrapper<IEntity> SOURCE_ALL = new IdentifiableWrapper<IEntity>(2L, "Все мероприятия");
    private static final List<IdentifiableWrapper<IEntity>> SOURCE_LIST = Arrays.asList(
        Model.SOURCE_SIM,
        Model.SOURCE_ALL
    );

    public List<IdentifiableWrapper<IEntity>> getRegistryElementSourceFilterList() { return Model.SOURCE_LIST; }

    private IdentifiableWrapper<IEntity> sourceFilter = Model.SOURCE_SIM;
    public IdentifiableWrapper<IEntity> getRegistryElementSourceFilter() { return (null != this.sourceFilter ? this.sourceFilter : Model.SOURCE_SIM); }
    public void setRegistryElementSourceFilter(final IdentifiableWrapper<IEntity> sourceFilter) { this.sourceFilter = sourceFilter; }

    private ISelectModel registryElementSource;
    public ISelectModel getRegistryElementSource() { return this.registryElementSource; }
    public void setRegistryElementSource(ISelectModel registryElementSource) { this.registryElementSource = registryElementSource; }

    private EppRegistryElement registryElement;
    public EppRegistryElement getRegistryElement() { return this.registryElement; }
    public void setRegistryElement(EppRegistryElement registryElement) { this.registryElement = registryElement; }

    public boolean isComboSelectMode() {
        return true; // null != getRegistryElementSource();
    }

    private final SimpleListDataHolder<ViewWrapper<IEppRegElWrapper>> result = new SimpleListDataHolder<ViewWrapper<IEppRegElWrapper>>();
    public SimpleListDataHolder<ViewWrapper<IEppRegElWrapper>> getResult() { return this.result; }

    private Map<Long, Double> distanceMap;
    public Map<Long, Double> getDistanceMap() { return this.distanceMap; }
    public void setDistanceMap(Map<Long, Double> distanceMap) { this.distanceMap = distanceMap; }
    public String getCurrentResultDistance() { return String.valueOf(getDistanceMap().get(getResult().getCurrent().getId())); }

}
