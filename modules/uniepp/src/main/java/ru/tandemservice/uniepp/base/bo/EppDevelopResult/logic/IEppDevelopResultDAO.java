/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.logic;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.List;

/**
 * @author Igor Belanov
 * @since 01.03.2017
 */
public interface IEppDevelopResultDAO extends IUniBaseDao
{
    void saveEppEPVBlockProfActivityType(EppEduPlanVersionBlock EPVBlock, List<EppProfActivityType> profActivityTypeList);

    List<EppProfActivityType> getProfActivityTypes(Long EPVBlockId);
}
