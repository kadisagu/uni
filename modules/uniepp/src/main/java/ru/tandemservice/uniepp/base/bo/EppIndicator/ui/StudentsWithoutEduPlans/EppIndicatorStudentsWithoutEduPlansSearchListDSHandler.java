/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithoutEduPlans;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListPresenter;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
public class EppIndicatorStudentsWithoutEduPlansSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public EppIndicatorStudentsWithoutEduPlansSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);

        // только неархивные студенты
        builder.where(eq(property(Student.archival().fromAlias(alias)), value(false)));

        // этого деканата
        final OrgUnit orgUnit = context.get(AbstractEppIndicatorStudentListPresenter.PROP_ORG_UNIT);
        if (null != orgUnit && null != orgUnit.getId()) {
            builder.where(eq(property(Student.educationOrgUnit().groupOrgUnit().id().fromAlias(alias)), value(orgUnit.getId())));
        }

        // не сущестувет актуальная связь с УП(в)
        builder.where(notExists(
            new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "s2epv").column(property("s2epv.id"))
            .where(eq(property(EppStudent2EduPlanVersion.student().fromAlias("s2epv")), property(alias)))
            .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
            .buildQuery()
        ));
    }
}
