// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.sec;

import java.util.List;

import org.tandemframework.core.runtime.ApplicationRuntime;

import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;

/**
 * @author oleyba
 * @since 30.11.2010
 */
public class UnieppStateChangePermissionHolder
{
    public static final String LIST_BEAN_NAME = "eppStateChangePermissionHolderList";

    public interface IUnieppStateChangePermissionResolver
    {
        String getPermission(EppState transition, IEppStateObject stateObject);

        String getMassPermission(EppState transition, String permissionPostfix);
    }

    @SuppressWarnings("unchecked")
    public static String getPermission(final EppState transition, final IEppStateObject stateObject)
    {
        String permission = null;
        if (ApplicationRuntime.containsBean(UnieppStateChangePermissionHolder.LIST_BEAN_NAME)) {
            for (final IUnieppStateChangePermissionResolver resolver : (List<IUnieppStateChangePermissionResolver>) ApplicationRuntime.getBean(UnieppStateChangePermissionHolder.LIST_BEAN_NAME))
            {
                if (permission == null) {
                    permission = resolver.getPermission(transition, stateObject);
                }
            }
        }
        if (null == permission) {
            throw new IllegalStateException();
        }
        return permission;
    }

    @SuppressWarnings("unchecked")
    public static String getMassPermission(final EppState transition, final String permissionPostfix)
    {
        String permission = null;
        if (ApplicationRuntime.containsBean(UnieppStateChangePermissionHolder.LIST_BEAN_NAME)) {
            for (final IUnieppStateChangePermissionResolver resolver : (List<IUnieppStateChangePermissionResolver>) ApplicationRuntime.getBean(UnieppStateChangePermissionHolder.LIST_BEAN_NAME))
            {
                if (permission == null) {
                    permission = resolver.getMassPermission(transition, permissionPostfix);
                }
            }
        }
        if (null == permission) {
            throw new IllegalStateException();
        }
        return permission;
    }
}
