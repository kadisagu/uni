package ru.tandemservice.uniepp.base.bo.EppCtrEducationPromice.ui.SectionPromice;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.developCombination.DevelopCombinationDAO;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.gen.EducationLevelsHighSchoolGen;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.IDevelopCombination;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@SuppressWarnings("serial")
public class EppCtrEducationPromiceWrapper extends IdentifiableWrapper<EppCtrEducationPromice> {

    public EppCtrEducationPromiceWrapper(final EppCtrEducationPromice e) {
        super(e);

        final EducationOrgUnit educationOrgUnit = e.getEducationOrgUnit();
        if (null != educationOrgUnit) {
            educationLevelHighSchoolModel.setValue(educationOrgUnit.getEducationLevelHighSchool());
        }
    }

    public SelectModel<EducationLevelsHighSchool> getEducationLevelHighSchoolModel() { return educationLevelHighSchoolModel; }
    public SelectModel<IDevelopCombination> getDevelopCombinationModel() { return developCombinationModel; }
    public ISelectModel getEduPlanVersionSelectModel() { return eduPlanVersionSelectModel; }
    public ISelectModel getEducationOrgUnitSelectModel() { return educationOrgUnitSelectModel; }
    public List<StudentCategory> getStudentCategoryList() { return IUniBaseDao.instance.get().getCatalogItemListOrderByCode(StudentCategory.class); }

    // выбор НПВ
    private final SelectModel<EducationLevelsHighSchool> educationLevelHighSchoolModel = new SelectModel<>(new UniQueryFullCheckSelectModel(EducationLevelsHighSchoolGen.P_FULL_TITLE) {
        @Override protected MQBuilder query(final String alias, final String filter) {

            final MQBuilder builder = new MQBuilder(EducationLevelsHighSchoolGen.ENTITY_CLASS, alias);
            if (null != filter) {
                builder.add(MQExpression.or(
                        MQExpression.like(alias, EducationLevelsHighSchool.title().s(), CoreStringUtils.escapeLike(filter)),
                        MQExpression.like(alias, EducationLevelsHighSchool.shortTitle().s(), CoreStringUtils.escapeLike(filter)),
                        MQExpression.like(alias, EducationLevelsHighSchool.fullTitle().s(), CoreStringUtils.escapeLike(filter)),
                        MQExpression.like(alias, EducationLevelsHighSchool.educationLevel().okso().s(), CoreStringUtils.escapeLike(filter))
                ));
            }
            return builder;
        }
    });

    // выбор ФУТС из всех возможных УП
    private final SelectModel<IDevelopCombination> developCombinationModel = new SelectModel<>(new FullCheckSelectModel("title", "title") {
        @Override public ListResult findValues(final String filter) {
            return IUniBaseDao.instance.get().getCalculatedValue(session -> {

                final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersionBlock.class, "x")
                .order(property(EppEduPlanVersionBlock.eduPlanVersion().eduPlan().developGrid().developPeriod().code().fromAlias("x")))
                .order(property(EppEduPlanVersionBlock.eduPlanVersion().eduPlan().developGrid().code().fromAlias("x")));

                final Set<IDevelopCombination> combinations = dql.createStatement(session).<Object[]>list().stream()
                        .map(row -> DevelopCombinationDAO.getDevelopCombinationDefinition(
                                (DevelopForm) session.get(DevelopForm.class, (Long) row[0]),
                                (DevelopCondition) session.get(DevelopCondition.class, (Long) row[1]),
                                (DevelopTech) session.get(DevelopTech.class, (Long) row[2]),
                                (DevelopGrid) session.get(DevelopGrid.class, (Long) row[3])
                        )).collect(Collectors.toCollection(LinkedHashSet::new));

                return new ListResult<>(combinations);
            });
        }
    });

    // ищем все УП(в), соотвествующие выбранному НПВ и комбинации ФУТС
    private final ISelectModel eduPlanVersionSelectModel = new DQLFullCheckSelectModel(EppEduPlanVersion.title().s()) {
        @Override protected DQLSelectBuilder query(final String alias, final String filter) {
            if (null == educationLevelHighSchoolModel.getValue()) { return null; }
            if (null == developCombinationModel.getValue()) { return null; }

            return new DQLSelectBuilder()
            .fromEntity(EppEduPlanVersion.class, alias)
            .where(exists(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.L_EDU_PLAN_VERSION, property(alias)))
            .order(property(EppEduPlanVersion.eduPlan().number().fromAlias(alias)))
            .order(property(EppEduPlanVersion.number().fromAlias(alias)));
        }
    };

    // ищем все НПП, соотвествующие выбранному НПВ и комбинации ФУТС
    private final ISelectModel educationOrgUnitSelectModel = new DQLFullCheckSelectModel("") {
        @Override public String getLabelFor(final Object value, final int columnIndex) {
            final EducationOrgUnit e = (EducationOrgUnit)value;
            if (null == e.getTerritorialOrgUnit()) {
                return e.getFormativeOrgUnit().getTitle();
            }
            return e.getFormativeOrgUnit().getTitle() + " ("+e.getTerritorialOrgUnit().getTerritorialTitle()+")";
        }

        @Override protected DQLSelectBuilder query(final String alias, final String filter) {
            if (null == educationLevelHighSchoolModel.getValue()) { return null; }
            if (null == developCombinationModel.getValue()) { return null; }

            return new DQLSelectBuilder()
            .fromEntity(EducationOrgUnit.class, alias)
            .joinPath(DQLJoinType.left, EducationOrgUnit.formativeOrgUnit().fromAlias(alias), alias+"_f")
            .order(property(OrgUnit.title().fromAlias(alias + "_f")))
            .joinPath(DQLJoinType.left, EducationOrgUnit.territorialOrgUnit().fromAlias(alias), alias+"_t")
            .order(property(OrgUnit.title().fromAlias(alias + "_t")))

            .where(eq(property(EducationOrgUnit.educationLevelHighSchool().fromAlias(alias)), value(educationLevelHighSchoolModel.getValue())))
            .where(eq(property(EducationOrgUnit.developForm().fromAlias(alias)), value(developCombinationModel.getValue().getDevelopForm())))
            .where(eq(property(EducationOrgUnit.developCondition().fromAlias(alias)), value(developCombinationModel.getValue().getDevelopCondition())))
            .where(eq(property(EducationOrgUnit.developTech().fromAlias(alias)), value(developCombinationModel.getValue().getDevelopTech())))
            .where(eq(property(EducationOrgUnit.developPeriod().fromAlias(alias)), value(developCombinationModel.getValue().getDevelopGrid().getDevelopPeriod())));
        }
    };
}