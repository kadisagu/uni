package ru.tandemservice.uniepp.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь направления подготовки подразделения с версией учебного плана
 *
 * Устанавливает версию учебного плана по умолчанию для НПП (при указании НПП студенту - с ним автоматически будет связана соответствующая версия УП).
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEducationOrgUnitEduPlanVersionGen extends EntityBase
 implements INaturalIdentifiable<EppEducationOrgUnitEduPlanVersionGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion";
    public static final String ENTITY_NAME = "eppEducationOrgUnitEduPlanVersion";
    public static final int VERSION_HASH = 797490272;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String P_MODIFICATION_DATE = "modificationDate";
    public static final String P_APPLY_ONLY_FOR_FIRST_COURSE = "applyOnlyForFirstCourse";

    private EducationOrgUnit _educationOrgUnit;     // Направление подготовки
    private EppEduPlanVersion _eduPlanVersion;     // Версия учебного плана
    private Date _modificationDate;     // Дата изменения
    private boolean _applyOnlyForFirstCourse = true;     // Применять только для студентов 1 курса

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Направление подготовки. Свойство не может быть null и должно быть уникальным.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion Версия учебного плана. Свойство не может быть null.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getModificationDate()
    {
        return _modificationDate;
    }

    /**
     * @param modificationDate Дата изменения. Свойство не может быть null.
     */
    public void setModificationDate(Date modificationDate)
    {
        dirty(_modificationDate, modificationDate);
        _modificationDate = modificationDate;
    }

    /**
     * @return Применять только для студентов 1 курса. Свойство не может быть null.
     */
    @NotNull
    public boolean isApplyOnlyForFirstCourse()
    {
        return _applyOnlyForFirstCourse;
    }

    /**
     * @param applyOnlyForFirstCourse Применять только для студентов 1 курса. Свойство не может быть null.
     */
    public void setApplyOnlyForFirstCourse(boolean applyOnlyForFirstCourse)
    {
        dirty(_applyOnlyForFirstCourse, applyOnlyForFirstCourse);
        _applyOnlyForFirstCourse = applyOnlyForFirstCourse;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEducationOrgUnitEduPlanVersionGen)
        {
            if (withNaturalIdProperties)
            {
                setEducationOrgUnit(((EppEducationOrgUnitEduPlanVersion)another).getEducationOrgUnit());
            }
            setEduPlanVersion(((EppEducationOrgUnitEduPlanVersion)another).getEduPlanVersion());
            setModificationDate(((EppEducationOrgUnitEduPlanVersion)another).getModificationDate());
            setApplyOnlyForFirstCourse(((EppEducationOrgUnitEduPlanVersion)another).isApplyOnlyForFirstCourse());
        }
    }

    public INaturalId<EppEducationOrgUnitEduPlanVersionGen> getNaturalId()
    {
        return new NaturalId(getEducationOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EppEducationOrgUnitEduPlanVersionGen>
    {
        private static final String PROXY_NAME = "EppEducationOrgUnitEduPlanVersionNaturalProxy";

        private Long _educationOrgUnit;

        public NaturalId()
        {}

        public NaturalId(EducationOrgUnit educationOrgUnit)
        {
            _educationOrgUnit = ((IEntity) educationOrgUnit).getId();
        }

        public Long getEducationOrgUnit()
        {
            return _educationOrgUnit;
        }

        public void setEducationOrgUnit(Long educationOrgUnit)
        {
            _educationOrgUnit = educationOrgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppEducationOrgUnitEduPlanVersionGen.NaturalId) ) return false;

            EppEducationOrgUnitEduPlanVersionGen.NaturalId that = (NaturalId) o;

            if( !equals(getEducationOrgUnit(), that.getEducationOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEducationOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEducationOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEducationOrgUnitEduPlanVersionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEducationOrgUnitEduPlanVersion.class;
        }

        public T newInstance()
        {
            return (T) new EppEducationOrgUnitEduPlanVersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
                case "modificationDate":
                    return obj.getModificationDate();
                case "applyOnlyForFirstCourse":
                    return obj.isApplyOnlyForFirstCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
                case "modificationDate":
                    obj.setModificationDate((Date) value);
                    return;
                case "applyOnlyForFirstCourse":
                    obj.setApplyOnlyForFirstCourse((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "eduPlanVersion":
                        return true;
                case "modificationDate":
                        return true;
                case "applyOnlyForFirstCourse":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "eduPlanVersion":
                    return true;
                case "modificationDate":
                    return true;
                case "applyOnlyForFirstCourse":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
                case "modificationDate":
                    return Date.class;
                case "applyOnlyForFirstCourse":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEducationOrgUnitEduPlanVersion> _dslPath = new Path<EppEducationOrgUnitEduPlanVersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEducationOrgUnitEduPlanVersion");
    }
            

    /**
     * @return Направление подготовки. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion#getModificationDate()
     */
    public static PropertyPath<Date> modificationDate()
    {
        return _dslPath.modificationDate();
    }

    /**
     * @return Применять только для студентов 1 курса. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion#isApplyOnlyForFirstCourse()
     */
    public static PropertyPath<Boolean> applyOnlyForFirstCourse()
    {
        return _dslPath.applyOnlyForFirstCourse();
    }

    public static class Path<E extends EppEducationOrgUnitEduPlanVersion> extends EntityPath<E>
    {
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;
        private PropertyPath<Date> _modificationDate;
        private PropertyPath<Boolean> _applyOnlyForFirstCourse;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion#getModificationDate()
     */
        public PropertyPath<Date> modificationDate()
        {
            if(_modificationDate == null )
                _modificationDate = new PropertyPath<Date>(EppEducationOrgUnitEduPlanVersionGen.P_MODIFICATION_DATE, this);
            return _modificationDate;
        }

    /**
     * @return Применять только для студентов 1 курса. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppEducationOrgUnitEduPlanVersion#isApplyOnlyForFirstCourse()
     */
        public PropertyPath<Boolean> applyOnlyForFirstCourse()
        {
            if(_applyOnlyForFirstCourse == null )
                _applyOnlyForFirstCourse = new PropertyPath<Boolean>(EppEducationOrgUnitEduPlanVersionGen.P_APPLY_ONLY_FOR_FIRST_COURSE, this);
            return _applyOnlyForFirstCourse;
        }

        public Class getEntityClass()
        {
            return EppEducationOrgUnitEduPlanVersion.class;
        }

        public String getEntityName()
        {
            return "eppEducationOrgUnitEduPlanVersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
