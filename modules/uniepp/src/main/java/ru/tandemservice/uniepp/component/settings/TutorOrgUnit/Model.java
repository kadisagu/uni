package ru.tandemservice.uniepp.component.settings.TutorOrgUnit;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

public class Model
{
    private IDataSettings _settings;

    private ISelectModel _orgUnitTypeList;

    private DynamicListDataSource<IEntity> _dataSource;

    public IDataSettings getSettings()
    {
        return this._settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this._settings = settings;
    }

    public ISelectModel getOrgUnitTypeList()
    {
        return this._orgUnitTypeList;
    }

    public void setOrgUnitTypeList(final ISelectModel orgUnitType)
    {
        this._orgUnitTypeList = orgUnitType;
    }

    public DynamicListDataSource<IEntity> getDataSource()
    {
        return this._dataSource;
    }

    public void setDataSource(final DynamicListDataSource<IEntity> dataSource)
    {
        this._dataSource = dataSource;
    }


}
