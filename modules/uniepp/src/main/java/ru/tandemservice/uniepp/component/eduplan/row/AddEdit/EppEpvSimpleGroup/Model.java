package ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvSimpleGroup;

import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.BaseAddEditModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupReRow;

/**
 * @author vdanilov
 */
public class Model extends BaseAddEditModel<EppEpvGroupReRow> {

    @Override protected EppEpvGroupReRow newInstance(final EppEduPlanVersionBlock block) {
        return new EppEpvGroupReRow(block);
    }
    @Override protected EppEpvGroupReRow check(final EppEpvGroupReRow entity) {
        return entity;
    }

}

