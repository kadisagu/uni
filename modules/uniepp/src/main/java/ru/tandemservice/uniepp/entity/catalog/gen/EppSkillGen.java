package ru.tandemservice.uniepp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.catalog.EppSkill;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Компетенция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppSkillGen extends EntityBase
 implements INaturalIdentifiable<EppSkillGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.catalog.EppSkill";
    public static final String ENTITY_NAME = "eppSkill";
    public static final int VERSION_HASH = 1204152900;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String L_PROGRAM_SUBJECT = "programSubject";
    public static final String L_SKILL_GROUP = "skillGroup";
    public static final String L_PROF_ACTIVITY_TYPE = "profActivityType";
    public static final String P_SKILL_CODE = "skillCode";
    public static final String L_PROGRAM_SPECIALIZATION_VAL = "programSpecializationVal";
    public static final String P_PROGRAM_SPECIALIZATION = "programSpecialization";
    public static final String P_IN_DEPTH_STUDY = "inDepthStudy";
    public static final String P_FROM_I_M_C_A = "fromIMCA";
    public static final String P_REMOVAL_DATE = "removalDate";
    public static final String P_PRIORITY = "priority";
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_PROGRAM_SPECIALIZATION_TITLE = "programSpecializationTitle";
    public static final String P_SHORT_TITLE = "shortTitle";

    private String _code;     // Системный код
    private String _title;     // Название
    private EduProgramSubject _programSubject;     // Направление подготовки
    private EppSkillGroup _skillGroup;     // Группа компетенции
    private EppProfActivityType _profActivityType;     // Вид профессиональной деятельности
    private String _skillCode;     // Код компетенции
    private EduProgramSpecialization _programSpecializationVal;     // Направленность
    private String _programSpecialization;     // Направленность (текст)
    private Boolean _inDepthStudy;     // Углубленное изучение
    private boolean _fromIMCA;     // Импортировано из ИМЦА
    private Date _removalDate;     // Дата утраты актуальности
    private int _priority;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление подготовки. Свойство не может быть null.
     */
    public void setProgramSubject(EduProgramSubject programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Группа компетенции. Свойство не может быть null.
     */
    @NotNull
    public EppSkillGroup getSkillGroup()
    {
        return _skillGroup;
    }

    /**
     * @param skillGroup Группа компетенции. Свойство не может быть null.
     */
    public void setSkillGroup(EppSkillGroup skillGroup)
    {
        dirty(_skillGroup, skillGroup);
        _skillGroup = skillGroup;
    }

    /**
     * Компетенции группы "Профессиональные компетенции" (ПК) могут иметь вид профессиональной деятельности.
     *
     * @return Вид профессиональной деятельности.
     */
    public EppProfActivityType getProfActivityType()
    {
        return _profActivityType;
    }

    /**
     * @param profActivityType Вид профессиональной деятельности.
     */
    public void setProfActivityType(EppProfActivityType profActivityType)
    {
        dirty(_profActivityType, profActivityType);
        _profActivityType = profActivityType;
    }

    /**
     * Код компетенции из ФГОС
     *
     * @return Код компетенции.
     */
    @Length(max=255)
    public String getSkillCode()
    {
        return _skillCode;
    }

    /**
     * @param skillCode Код компетенции.
     */
    public void setSkillCode(String skillCode)
    {
        dirty(_skillCode, skillCode);
        _skillCode = skillCode;
    }

    /**
     * Для специальностей ВО регламентируются специализации и перечисляются для них компетенции
     * в рамках отдельной группы "Профессионально-специализированные компетенции".
     *
     * @return Направленность.
     */
    public EduProgramSpecialization getProgramSpecializationVal()
    {
        return _programSpecializationVal;
    }

    /**
     * @param programSpecializationVal Направленность.
     */
    public void setProgramSpecializationVal(EduProgramSpecialization programSpecializationVal)
    {
        dirty(_programSpecializationVal, programSpecializationVal);
        _programSpecializationVal = programSpecializationVal;
    }

    /**
     * Заполняется в результате импорта
     *
     * @return Направленность (текст).
     */
    @Length(max=255)
    public String getProgramSpecialization()
    {
        return _programSpecialization;
    }

    /**
     * @param programSpecialization Направленность (текст).
     */
    public void setProgramSpecialization(String programSpecialization)
    {
        dirty(_programSpecialization, programSpecialization);
        _programSpecialization = programSpecialization;
    }

    /**
     * Разрешено выбирать для специальностей СПО.
     *
     * @return Углубленное изучение.
     */
    public Boolean getInDepthStudy()
    {
        return _inDepthStudy;
    }

    /**
     * @param inDepthStudy Углубленное изучение.
     */
    public void setInDepthStudy(Boolean inDepthStudy)
    {
        dirty(_inDepthStudy, inDepthStudy);
        _inDepthStudy = inDepthStudy;
    }

    /**
     * Если компетенция создана в результате системного действия, то устанавливается значение "true".
     * Если создана пользователем, то значение "false".
     *
     * @return Импортировано из ИМЦА. Свойство не может быть null.
     */
    @NotNull
    public boolean isFromIMCA()
    {
        return _fromIMCA;
    }

    /**
     * @param fromIMCA Импортировано из ИМЦА. Свойство не может быть null.
     */
    public void setFromIMCA(boolean fromIMCA)
    {
        dirty(_fromIMCA, fromIMCA);
        _fromIMCA = fromIMCA;
    }

    /**
     * Если в результате системного действия ранее импортированная компетенция не была найдена, то сохраняется текущая дата.
     *
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    /**
     * Приоритет компетенции в рамках направления, группы и вида деятельности.
     *
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppSkillGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EppSkill)another).getCode());
            }
            setTitle(((EppSkill)another).getTitle());
            setProgramSubject(((EppSkill)another).getProgramSubject());
            setSkillGroup(((EppSkill)another).getSkillGroup());
            setProfActivityType(((EppSkill)another).getProfActivityType());
            setSkillCode(((EppSkill)another).getSkillCode());
            setProgramSpecializationVal(((EppSkill)another).getProgramSpecializationVal());
            setProgramSpecialization(((EppSkill)another).getProgramSpecialization());
            setInDepthStudy(((EppSkill)another).getInDepthStudy());
            setFromIMCA(((EppSkill)another).isFromIMCA());
            setRemovalDate(((EppSkill)another).getRemovalDate());
            setPriority(((EppSkill)another).getPriority());
        }
    }

    public INaturalId<EppSkillGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EppSkillGen>
    {
        private static final String PROXY_NAME = "EppSkillNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppSkillGen.NaturalId) ) return false;

            EppSkillGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppSkillGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppSkill.class;
        }

        public T newInstance()
        {
            return (T) new EppSkill();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "programSubject":
                    return obj.getProgramSubject();
                case "skillGroup":
                    return obj.getSkillGroup();
                case "profActivityType":
                    return obj.getProfActivityType();
                case "skillCode":
                    return obj.getSkillCode();
                case "programSpecializationVal":
                    return obj.getProgramSpecializationVal();
                case "programSpecialization":
                    return obj.getProgramSpecialization();
                case "inDepthStudy":
                    return obj.getInDepthStudy();
                case "fromIMCA":
                    return obj.isFromIMCA();
                case "removalDate":
                    return obj.getRemovalDate();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((EduProgramSubject) value);
                    return;
                case "skillGroup":
                    obj.setSkillGroup((EppSkillGroup) value);
                    return;
                case "profActivityType":
                    obj.setProfActivityType((EppProfActivityType) value);
                    return;
                case "skillCode":
                    obj.setSkillCode((String) value);
                    return;
                case "programSpecializationVal":
                    obj.setProgramSpecializationVal((EduProgramSpecialization) value);
                    return;
                case "programSpecialization":
                    obj.setProgramSpecialization((String) value);
                    return;
                case "inDepthStudy":
                    obj.setInDepthStudy((Boolean) value);
                    return;
                case "fromIMCA":
                    obj.setFromIMCA((Boolean) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "programSubject":
                        return true;
                case "skillGroup":
                        return true;
                case "profActivityType":
                        return true;
                case "skillCode":
                        return true;
                case "programSpecializationVal":
                        return true;
                case "programSpecialization":
                        return true;
                case "inDepthStudy":
                        return true;
                case "fromIMCA":
                        return true;
                case "removalDate":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "programSubject":
                    return true;
                case "skillGroup":
                    return true;
                case "profActivityType":
                    return true;
                case "skillCode":
                    return true;
                case "programSpecializationVal":
                    return true;
                case "programSpecialization":
                    return true;
                case "inDepthStudy":
                    return true;
                case "fromIMCA":
                    return true;
                case "removalDate":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "programSubject":
                    return EduProgramSubject.class;
                case "skillGroup":
                    return EppSkillGroup.class;
                case "profActivityType":
                    return EppProfActivityType.class;
                case "skillCode":
                    return String.class;
                case "programSpecializationVal":
                    return EduProgramSpecialization.class;
                case "programSpecialization":
                    return String.class;
                case "inDepthStudy":
                    return Boolean.class;
                case "fromIMCA":
                    return Boolean.class;
                case "removalDate":
                    return Date.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppSkill> _dslPath = new Path<EppSkill>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppSkill");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Группа компетенции. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getSkillGroup()
     */
    public static EppSkillGroup.Path<EppSkillGroup> skillGroup()
    {
        return _dslPath.skillGroup();
    }

    /**
     * Компетенции группы "Профессиональные компетенции" (ПК) могут иметь вид профессиональной деятельности.
     *
     * @return Вид профессиональной деятельности.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getProfActivityType()
     */
    public static EppProfActivityType.Path<EppProfActivityType> profActivityType()
    {
        return _dslPath.profActivityType();
    }

    /**
     * Код компетенции из ФГОС
     *
     * @return Код компетенции.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getSkillCode()
     */
    public static PropertyPath<String> skillCode()
    {
        return _dslPath.skillCode();
    }

    /**
     * Для специальностей ВО регламентируются специализации и перечисляются для них компетенции
     * в рамках отдельной группы "Профессионально-специализированные компетенции".
     *
     * @return Направленность.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getProgramSpecializationVal()
     */
    public static EduProgramSpecialization.Path<EduProgramSpecialization> programSpecializationVal()
    {
        return _dslPath.programSpecializationVal();
    }

    /**
     * Заполняется в результате импорта
     *
     * @return Направленность (текст).
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getProgramSpecialization()
     */
    public static PropertyPath<String> programSpecialization()
    {
        return _dslPath.programSpecialization();
    }

    /**
     * Разрешено выбирать для специальностей СПО.
     *
     * @return Углубленное изучение.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getInDepthStudy()
     */
    public static PropertyPath<Boolean> inDepthStudy()
    {
        return _dslPath.inDepthStudy();
    }

    /**
     * Если компетенция создана в результате системного действия, то устанавливается значение "true".
     * Если создана пользователем, то значение "false".
     *
     * @return Импортировано из ИМЦА. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#isFromIMCA()
     */
    public static PropertyPath<Boolean> fromIMCA()
    {
        return _dslPath.fromIMCA();
    }

    /**
     * Если в результате системного действия ранее импортированная компетенция не была найдена, то сохраняется текущая дата.
     *
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    /**
     * Приоритет компетенции в рамках направления, группы и вида деятельности.
     *
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getFullTitle()
     */
    public static SupportedPropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getProgramSpecializationTitle()
     */
    public static SupportedPropertyPath<String> programSpecializationTitle()
    {
        return _dslPath.programSpecializationTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getShortTitle()
     */
    public static SupportedPropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    public static class Path<E extends EppSkill> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private EduProgramSubject.Path<EduProgramSubject> _programSubject;
        private EppSkillGroup.Path<EppSkillGroup> _skillGroup;
        private EppProfActivityType.Path<EppProfActivityType> _profActivityType;
        private PropertyPath<String> _skillCode;
        private EduProgramSpecialization.Path<EduProgramSpecialization> _programSpecializationVal;
        private PropertyPath<String> _programSpecialization;
        private PropertyPath<Boolean> _inDepthStudy;
        private PropertyPath<Boolean> _fromIMCA;
        private PropertyPath<Date> _removalDate;
        private PropertyPath<Integer> _priority;
        private SupportedPropertyPath<String> _fullTitle;
        private SupportedPropertyPath<String> _programSpecializationTitle;
        private SupportedPropertyPath<String> _shortTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EppSkillGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EppSkillGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new EduProgramSubject.Path<EduProgramSubject>(L_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Группа компетенции. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getSkillGroup()
     */
        public EppSkillGroup.Path<EppSkillGroup> skillGroup()
        {
            if(_skillGroup == null )
                _skillGroup = new EppSkillGroup.Path<EppSkillGroup>(L_SKILL_GROUP, this);
            return _skillGroup;
        }

    /**
     * Компетенции группы "Профессиональные компетенции" (ПК) могут иметь вид профессиональной деятельности.
     *
     * @return Вид профессиональной деятельности.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getProfActivityType()
     */
        public EppProfActivityType.Path<EppProfActivityType> profActivityType()
        {
            if(_profActivityType == null )
                _profActivityType = new EppProfActivityType.Path<EppProfActivityType>(L_PROF_ACTIVITY_TYPE, this);
            return _profActivityType;
        }

    /**
     * Код компетенции из ФГОС
     *
     * @return Код компетенции.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getSkillCode()
     */
        public PropertyPath<String> skillCode()
        {
            if(_skillCode == null )
                _skillCode = new PropertyPath<String>(EppSkillGen.P_SKILL_CODE, this);
            return _skillCode;
        }

    /**
     * Для специальностей ВО регламентируются специализации и перечисляются для них компетенции
     * в рамках отдельной группы "Профессионально-специализированные компетенции".
     *
     * @return Направленность.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getProgramSpecializationVal()
     */
        public EduProgramSpecialization.Path<EduProgramSpecialization> programSpecializationVal()
        {
            if(_programSpecializationVal == null )
                _programSpecializationVal = new EduProgramSpecialization.Path<EduProgramSpecialization>(L_PROGRAM_SPECIALIZATION_VAL, this);
            return _programSpecializationVal;
        }

    /**
     * Заполняется в результате импорта
     *
     * @return Направленность (текст).
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getProgramSpecialization()
     */
        public PropertyPath<String> programSpecialization()
        {
            if(_programSpecialization == null )
                _programSpecialization = new PropertyPath<String>(EppSkillGen.P_PROGRAM_SPECIALIZATION, this);
            return _programSpecialization;
        }

    /**
     * Разрешено выбирать для специальностей СПО.
     *
     * @return Углубленное изучение.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getInDepthStudy()
     */
        public PropertyPath<Boolean> inDepthStudy()
        {
            if(_inDepthStudy == null )
                _inDepthStudy = new PropertyPath<Boolean>(EppSkillGen.P_IN_DEPTH_STUDY, this);
            return _inDepthStudy;
        }

    /**
     * Если компетенция создана в результате системного действия, то устанавливается значение "true".
     * Если создана пользователем, то значение "false".
     *
     * @return Импортировано из ИМЦА. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#isFromIMCA()
     */
        public PropertyPath<Boolean> fromIMCA()
        {
            if(_fromIMCA == null )
                _fromIMCA = new PropertyPath<Boolean>(EppSkillGen.P_FROM_I_M_C_A, this);
            return _fromIMCA;
        }

    /**
     * Если в результате системного действия ранее импортированная компетенция не была найдена, то сохраняется текущая дата.
     *
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(EppSkillGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

    /**
     * Приоритет компетенции в рамках направления, группы и вида деятельности.
     *
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EppSkillGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getFullTitle()
     */
        public SupportedPropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new SupportedPropertyPath<String>(EppSkillGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getProgramSpecializationTitle()
     */
        public SupportedPropertyPath<String> programSpecializationTitle()
        {
            if(_programSpecializationTitle == null )
                _programSpecializationTitle = new SupportedPropertyPath<String>(EppSkillGen.P_PROGRAM_SPECIALIZATION_TITLE, this);
            return _programSpecializationTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.catalog.EppSkill#getShortTitle()
     */
        public SupportedPropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new SupportedPropertyPath<String>(EppSkillGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

        public Class getEntityClass()
        {
            return EppSkill.class;
        }

        public String getEntityName()
        {
            return "eppSkill";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFullTitle();

    public abstract String getProgramSpecializationTitle();

    public abstract String getShortTitle();
}
