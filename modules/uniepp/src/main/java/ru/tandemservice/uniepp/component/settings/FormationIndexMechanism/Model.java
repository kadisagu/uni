package ru.tandemservice.uniepp.component.settings.FormationIndexMechanism;

import java.util.List;
import java.util.Map;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniepp.entity.catalog.EppIndexRule;
import ru.tandemservice.uniepp.entity.settings.EppIndexRule4SubjectIndex;

public class Model
{
    private List<EppIndexRule4SubjectIndex> indexList;
    private EppIndexRule4SubjectIndex currentIndex;

    private EppIndexRule currentRule;
    private ISelectModel ruleListModel;

    private List<EppIndexRule> ruleList;

    public List<EppIndexRule4SubjectIndex> getIndexList()
    {
        return this.indexList;
    }

    public void setIndexList(final List<EppIndexRule4SubjectIndex> indexList)
    {
        this.indexList = indexList;
    }

    public EppIndexRule getCurrentRule()
    {
        return this.currentRule;
    }

    public void setCurrentRule(final EppIndexRule currentRule)
    {
        this.currentRule = currentRule;
    }

    public EppIndexRule4SubjectIndex getCurrentIndex()
    {
        return this.currentIndex;
    }

    public void setCurrentIndex(final EppIndexRule4SubjectIndex currentIndex)
    {
        this.currentIndex = currentIndex;
    }

    public ISelectModel getRuleListModel()
    {
        return this.ruleListModel;
    }

    public void setRuleListModel(final ISelectModel ruleListModel)
    {
        this.ruleListModel = ruleListModel;
    }

    public List<EppIndexRule> getRuleList()
    {
        return this.ruleList;
    }

    public void setRuleList(final List<EppIndexRule> ruleList)
    {
        this.ruleList = ruleList;
    }
}
