/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanHigherProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author vip_delete
 * @since 27.02.2010
 */
@State( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduplanVersionHolder.id"),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class Model extends ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub.EduPlanVersionPubModel
{
    public boolean isEmpty() {
        return IEppEduPlanVersionDataDAO.instance.get().isEmpty(this.getEduplanVersion().getId());
    }

    public boolean isShowTerm() {
        return getEduPlan() instanceof EppEduPlanHigherProf;
    }

    private String _selectedTab;
    public String getSelectedTab() { return this._selectedTab; }
    public void setSelectedTab(final String selectedTab) { this._selectedTab = selectedTab; }

    private boolean _changeDirectionBlocksDisabled;
    public boolean isChangeDirectionBlocksDisabled() { return this._changeDirectionBlocksDisabled; }
    public void setChangeDirectionBlocksDisabled(final boolean changeDirectionBlocks) { this._changeDirectionBlocksDisabled = changeDirectionBlocks; }

    private StaticListDataSource<ViewWrapper<EppEduPlanVersionBlock>> blockDataSource = new StaticListDataSource<ViewWrapper<EppEduPlanVersionBlock>>();
    public StaticListDataSource<ViewWrapper<EppEduPlanVersionBlock>> getBlockDataSource() { return this.blockDataSource; }

    public Boolean getIsGridTabVisible() {
        return Debug.isEnabled();
    }

    public boolean isDevelopResultTabVisible()
    {
        return (getEduPlan() instanceof EppEduPlanProf);
    }
}
