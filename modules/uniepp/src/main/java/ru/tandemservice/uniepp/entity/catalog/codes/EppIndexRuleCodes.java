package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Алгоритм формирования индексов"
 * Имя сущности : eppIndexRule
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppIndexRuleCodes
{
    /** Константа кода (code) элемента : Конкатенация (title) */
    String SIMPLE_CONCAT = "1";
    /** Константа кода (code) элемента : Конкатенация + постфикс (title) */
    String POSTFIX_CONCAT = "2";
    /** Константа кода (code) элемента : Префикс + компонент + многоуровневая нумерация (title) */
    String PREFIX_COMPONENT_HIERARCHY = "3";
    /** Константа кода (code) элемента : Конкатенация без учета элементов верхнего уровня (title) */
    String TRIMMED_CONCAT = "4";
    /** Константа кода (code) элемента : Префикc + многоуровневая нумерация (title) */
    String PREFIX_HIERARCHY = "5";

    Set<String> CODES = ImmutableSet.of(SIMPLE_CONCAT, POSTFIX_CONCAT, PREFIX_COMPONENT_HIERARCHY, TRIMMED_CONCAT, PREFIX_HIERARCHY);
}
