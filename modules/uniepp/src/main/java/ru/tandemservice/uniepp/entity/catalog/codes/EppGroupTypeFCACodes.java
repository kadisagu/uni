package ru.tandemservice.uniepp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид учебной группы (ФИК)"
 * Имя сущности : eppGroupTypeFCA
 * Файл data.xml : epp-catalogs.data.xml
 */
public interface EppGroupTypeFCACodes
{
    /** Константа кода (code) элемента : Экзамен (title) */
    String CONTROL_ACTION_EXAM = "fca.exam";
    /** Константа кода (code) элемента : Зачет (title) */
    String CONTROL_ACTION_SETOFF = "fca.setoff";
    /** Константа кода (code) элемента : Зачет дифференцированный (title) */
    String CONTROL_ACTION_SETOFF_DIFF = "fca.setoffDiff";
    /** Константа кода (code) элемента : Курсовая работа (title) */
    String CONTROL_ACTION_COURSE_WORK = "fca.courseWork";
    /** Константа кода (code) элемента : Курсовой проект (title) */
    String CONTROL_ACTION_COURSE_PROJECT = "fca.courseProject";
    /** Константа кода (code) элемента : Экзамен накопительный (title) */
    String CONTROL_ACTION_EXAM_ACCUM = "fca.examAccum";
    /** Константа кода (code) элемента : Контрольная работа (title) */
    String CONTROL_ACTION_CONTROL_WORK = "fca.controlWork";

    Set<String> CODES = ImmutableSet.of(CONTROL_ACTION_EXAM, CONTROL_ACTION_SETOFF, CONTROL_ACTION_SETOFF_DIFF, CONTROL_ACTION_COURSE_WORK, CONTROL_ACTION_COURSE_PROJECT, CONTROL_ACTION_EXAM_ACCUM, CONTROL_ACTION_CONTROL_WORK);
}
