/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppDevelopResult.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.entity.catalog.EppProfActivityType;
import ru.tandemservice.uniepp.entity.plan.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * @author Igor Belanov
 * @since 03.03.2017
 */
public class EppProfActivityTypeComboDSHandler extends SimpleTitledComboDataSourceHandler
{
    public static final String PROP_EDU_PLAN_VERSION_ID = "eduPlanVersionId";
    public static final String PROP_EDU_PLAN_VERSION_BLOCK_ID = "eduPlanVersionBlockId";

    public EppProfActivityTypeComboDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String filter = input.getComboFilterByValue();
        Long eduPlanVersionId = context.get(PROP_EDU_PLAN_VERSION_ID);
        Long eduPlanVersionBlockId = context.get(PROP_EDU_PLAN_VERSION_BLOCK_ID);
        // версия УП должна быть в любом случае
        assert eduPlanVersionId != null;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppProfActivityType.class, "pat");

        // 1. не ДПО, 2. с соотв. направлением 3. с соотв. флагом "углублённое изучение" (если СПО)
        DQLSelectBuilder subBuilder1 = new DQLSelectBuilder().fromEntity(EppEduPlanVersion.class, "epv").column("pat1.id")
                .joinEntity("epv", DQLJoinType.inner, EppEduPlanProf.class, "ep_prof", eq(
                        property("epv", EppEduPlanVersion.eduPlan().id()), property("ep_prof", EppEduPlanProf.id())))
                .where(eq(property("epv", EppEduPlanVersion.id()), value(eduPlanVersionId)))
                .joinEntity("ep_prof", DQLJoinType.inner, EppProfActivityType.class, "pat1", eq(
                        property("ep_prof", EppEduPlanProf.programSubject().id()), property("pat1", EppProfActivityType.programSubject().id())));
        EppEduPlan eduPlan = UniDaoFacade.getCoreDao().getProperty(EppEduPlanVersion.class, EppEduPlanVersion.eduPlan().s(), EppEduPlanVersion.P_ID, eduPlanVersionId);
        if (eduPlan instanceof EppEduPlanSecondaryProf)
        {
            subBuilder1.joinEntity("ep_prof", DQLJoinType.inner, EppEduPlanSecondaryProf.class, "ep_spo", eq(
                    property("ep_prof", EppEduPlanProf.id()), property("ep_spo", EppEduPlanSecondaryProf.id())));
            subBuilder1.where(eq(property("ep_spo", EppEduPlanSecondaryProf.inDepthStudy()), property("pat1", EppProfActivityType.inDepthStudy())));
        }
        builder.where(in(property("pat.id"), subBuilder1.buildQuery()));

        // уберём из селекта все виды проф.деятельности, которые уже привязаны к общему блоку (по версии УП)
        DQLSelectBuilder subBuilder2 = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersionBlockProfActivityType.class, "epvb_pat")
                .column(property("epvb_pat", EppEduPlanVersionBlockProfActivityType.profActivityType().id()))
                .joinPath(DQLJoinType.inner, EppEduPlanVersionBlockProfActivityType.eduPlanVersionBlock().fromAlias("epvb_pat"), "epvb")
                .joinPath(DQLJoinType.inner, EppEduPlanVersionBlock.eduPlanVersion().fromAlias("epvb"), "epv")
                .where(eq(property("epv.id"), value(eduPlanVersionId)))
                .where(in(property("epvb.id"), new DQLSelectBuilder().fromEntity(EppEduPlanVersionRootBlock.class, "epvb_r").column("epvb_r.id").buildQuery()));
        builder.where(notIn(property("pat.id"), subBuilder2.buildQuery()));

        // если выбран блок версии УП, то так же уберём из селекта все виды проф. деятельности,
        // которые привязаны к этому конкретному блоку
        if (eduPlanVersionBlockId != null)
        {
            DQLSelectBuilder subBuilder3 = new DQLSelectBuilder()
                    .fromEntity(EppEduPlanVersionBlockProfActivityType.class, "epvb_pat")
                    .column(property("epvb_pat", EppEduPlanVersionBlockProfActivityType.profActivityType().id()))
                    .joinPath(DQLJoinType.inner, EppEduPlanVersionBlockProfActivityType.eduPlanVersionBlock().fromAlias("epvb_pat"), "epvb")
                    .joinPath(DQLJoinType.inner, EppEduPlanVersionBlock.eduPlanVersion().fromAlias("epvb"), "epv")
                    .where(eq(property("epvb.id"), value(eduPlanVersionBlockId)))
                    .where(notIn(property("epvb.id"), new DQLSelectBuilder().fromEntity(EppEduPlanVersionRootBlock.class, "epvb_r").column("epvb_r.id").buildQuery()));
            builder.where(notIn(property("pat.id"), subBuilder3.buildQuery()));
        }

        // фильтр по введённому названию
        if (!StringUtils.isEmpty(filter))
            builder.where(like(upper(property("pat", EppProfActivityType.title())), value(CoreStringUtils.escapeLike(filter, true))));

        builder.order(property("pat", EppProfActivityType.priority()));

        context.put(UIDefines.COMBO_OBJECT_LIST, createStatement(builder).list());
        return super.execute(input, context);
    }
}
