/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.ElementDuplicationMerge;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.component.registry.TutorOrgUnitAsOwnerSelectModel;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.EppRegElementKeyGenerator.KeyGenRule;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.*;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Nikolay Fedorovskih
 * @since 14.05.2015
 */
public class EppRegistryElementDuplicationMergeUI extends UIPresenter
{
    private ISelectModel _ownerSelectModel;
    private ISelectModel _searchParamsSelectModel;
    private ISelectModel _maxDistanceSelectModel;
    private List<RowWrapperBase> _rowList;
    private RowWrapperBase _currentRow;
    private RowWrapper _templateRow;
	private RowWrapper.PartLoadInfo _currentPartLoad;
    private List<IdentifiableWrapper> _partsColumnHeaders = new ArrayList<>();
    private IdentifiableWrapper _currentPartColumnHeader;

    public static final int MAX_DISTANCE_LIMIT = 4;

    public static final String FILTER_SETTING_ELEMENT_TYPE = "type";
    public static final String FILTER_SETTING_TITLE = "title";
    public static final String FILTER_SETTING_OWNER = "owner";
    public static final String FILTER_SETTING_STATE = "stateList";
    public static final String FILTER_SETTING_PARTS_COUNT = "partsCount";
    public static final String FILTER_SETTING_SEARCH_PARAMS = "searchParams";
    public static final String FILTER_SETTING_MAX_DISTANCE = "maxDistance";

    public static final String RULE_WRAPPED_PROPERTY = "rule";

    @Override
    public void onComponentRefresh()
    {
        final List<IIdentifiable> keyGenRules = new ArrayList<>();
        long id = 0;
        for (KeyGenRule rule : KeyGenRule.values())
        {
            final DataWrapper dataWrapper = new DataWrapper(id++, rule.title);
            dataWrapper.setProperty(RULE_WRAPPED_PROPERTY, rule);
            keyGenRules.add(dataWrapper);
        }
        _searchParamsSelectModel = new LazySimpleSelectModel<>(keyGenRules);

        final List<IIdentifiable> maxDistanceItems = new ArrayList<>(MAX_DISTANCE_LIMIT + 1);
        for (long i = 0; i <= MAX_DISTANCE_LIMIT; i++) {
            maxDistanceItems.add(new DataWrapper(i, String.valueOf(i)));
        }
        _maxDistanceSelectModel = new LazySimpleSelectModel<>(maxDistanceItems);

        _ownerSelectModel = new TutorOrgUnitAsOwnerSelectModel();

        updateDS();
    }

    private void updateDS()
    {
        _rowList = new ArrayList<>();

        final EppRegistryStructure type = getSettings().get(FILTER_SETTING_ELEMENT_TYPE);
        final OrgUnit owner = getSettings().get(FILTER_SETTING_OWNER);
        final Collection<EppState> stateList = getSettings().get(FILTER_SETTING_STATE);
        final IEntity parts = getSettings().get(FILTER_SETTING_PARTS_COUNT);
        final IEntity searchParamItem = getSettings().get(FILTER_SETTING_SEARCH_PARAMS);
        final IEntity maxDistanceItem = getSettings().get(FILTER_SETTING_MAX_DISTANCE);

        if (type == null || owner == null || parts == null || searchParamItem == null || maxDistanceItem == null) {
            return;
        }

        _partsColumnHeaders = new ArrayList<>();
        int partsCount = Integer.parseInt(((IdentifiableWrapper) parts).getTitle());
        for (int i = 1; i <= partsCount; i++)
            _partsColumnHeaders.add(new IdentifiableWrapper((long) i, String.valueOf(i)));

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, "e");
        dql.column(property("e.id"));
        dql.where(or(
            eq(property("e", EppRegistryElement.parent()), value(type)),
            eq(property("e", EppRegistryElement.parent().parent()), value(type))
        ));
        FilterUtils.applySelectFilter(dql, "e", EppRegistryElement.state(), stateList);
        FilterUtils.applySelectFilter(dql, "e", EppRegistryElement.owner(), owner);
        FilterUtils.applySimpleLikeFilter(dql, "e", EppRegistryElement.title(), getSettings().<String>get(FILTER_SETTING_TITLE));
        FilterUtils.applySelectFilter(dql, "e", EppRegistryElement.parts(), parts);

        final List<Long> ids = IUniBaseDao.instance.get().getList(dql);
        if (ids.isEmpty()) {
            return;
        }

        // Группируем по ключу
        final Multimap<String, IEppRegElWrapper> multimap = ArrayListMultimap.create();
        final KeyGenRule keyGenRule = (KeyGenRule) searchParamItem.getProperty(RULE_WRAPPED_PROPERTY);
        for (IEppRegElWrapper wrapper : IEppRegistryDAO.instance.get().getRegistryElementDataMap(ids).values())
        {
            multimap.put(wrapper.generateKey(keyGenRule), wrapper);
        }

        // Внутри группы сортируем по названию и номеру. Потом сами группы между собой сортируем по названию и номеру первого элемента
        final List<List<IEppRegElWrapper>> lists = new ArrayList<>(multimap.size());
        for (Collection<IEppRegElWrapper> wrapperList : multimap.asMap().values())
        {
            List<IEppRegElWrapper> list = new ArrayList<>(wrapperList);
			Collections.sort(list, (o1, o2) -> {
				int i;
                if ((i = CommonCollator.TITLED_COMPARATOR.compare(o1, o2)) != 0) return i;
                if ((i = o1.getItem().getNumber().compareTo(o2.getItem().getNumber())) != 0) return i;
                return i;
			});
            lists.add(list);
        }
		Collections.sort(lists, (l1, l2) -> {
                final IEppRegElWrapper o1 = l1.get(0);
                final IEppRegElWrapper o2 = l2.get(0);
                int i;
                if ((i = CommonCollator.TITLED_COMPARATOR.compare(o1, o2)) != 0) return i;
                if ((i = o1.getItem().getNumber().compareTo(o2.getItem().getNumber())) != 0) return i;
                return i;
            });

        // Внутри группы элементы сортируем по названию и группируем уже по похожести соседних
        // Промежутки между группами будут в виде врапперов с параметрами splitterByKey или splitterByTitle
        int index = 1;
        final int maxDistance = maxDistanceItem.getId().intValue();
        for (List<IEppRegElWrapper> wrapperList : lists)
        {
            if (!_rowList.isEmpty()) {
                _rowList.add(new SplitterByKey());
            }


            final Iterator<IEppRegElWrapper> iterator = wrapperList.iterator();
            String lastTitle = null;
            while (iterator.hasNext())
            {
                final IEppRegElWrapper wrapper = iterator.next();
                if (lastTitle != null) {
                    // Сравниваем названия предыдущего элемента и текущего. Если не похожи, вставляем между ними промежуток.
                    final int distance = StringUtils.getLevenshteinDistance(lastTitle, wrapper.getItem().getTitle());
                    if (distance > maxDistance) {
                        _rowList.add(new SplitterByTitle());
                    }
                }


				_rowList.add(new RowWrapper(wrapper.getItem(), getPartsLoadInfo(wrapper), index++, wrapper.getItem().getTitleWithSplitVariant()));
                lastTitle = wrapper.getItem().getTitle();
            }
        }
    }

    // Listeners

    public void onClickMerge()
    {
        if (getTemplateRow() == null) {
            throw new ApplicationException("Необходимо выбрать элемент, на базе которого требуется проводить объединение.");
        }

        final Set<Long> duplicateIds = new HashSet<>();
        for (RowWrapperBase row : _rowList)
        {
            if (row instanceof RowWrapper) {
                RowWrapper current = (RowWrapper) row;

                if (current.isSelected()) {
                    duplicateIds.add(current.getId());
                }
            }
        }

        duplicateIds.remove(getTemplateRow().getId());

        if (duplicateIds.isEmpty()) {
            throw new ApplicationException("Выберите дублирующие элементы, которые необходимо удалить.");
        }

        IEppRegistryDAO.instance.get().doRegElementDuplicatesMerge(getTemplateRow().getId(), duplicateIds);

        updateDS();
    }



    public void onClickApplyFilters()
    {
        saveSettings();
        updateDS();
    }

    public void onClickClearFilters()
    {
        clearSettings();
        updateDS();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if ("partsCountDS".equals(dataSource.getName()))
        {
            dataSource.putAll(getSettings().getAsMap("type", "owner", "partsCount"));
        }
    }

    // G & S

    public boolean isFiltersNotSet() {
        final EppRegistryStructure type = getSettings().get(FILTER_SETTING_ELEMENT_TYPE);
        final OrgUnit owner = getSettings().get(FILTER_SETTING_OWNER);
        final Collection<EppState> stateList = getSettings().get(FILTER_SETTING_STATE);
        final IEntity parts = getSettings().get(FILTER_SETTING_PARTS_COUNT);
        final IEntity searchParamItem = getSettings().get(FILTER_SETTING_SEARCH_PARAMS);
        final IEntity maxDistanceItem = getSettings().get(FILTER_SETTING_MAX_DISTANCE);

        return type == null || owner == null || parts == null || searchParamItem == null || maxDistanceItem == null;

    }

    public ISelectModel getOwnerSelectModel()
    {
        return _ownerSelectModel;
    }

    public ISelectModel getSearchParamsSelectModel()
    {
        return _searchParamsSelectModel;
    }

    public ISelectModel getMaxDistanceSelectModel()
    {
        return _maxDistanceSelectModel;
    }

    public List<RowWrapperBase> getRowList()
    {
        return _rowList;
    }

    public RowWrapperBase getCurrentRow()
    {
        return _currentRow;
    }

    public List<IdentifiableWrapper> getPartsColumnHeaders() { return _partsColumnHeaders; }

    public IdentifiableWrapper getCurrentPartColumnHeader() { return _currentPartColumnHeader; }

    public void setCurrentRow(RowWrapperBase currentRow)
    {
        _currentRow = currentRow;
    }

    public RowWrapper getTemplateRow()
    {
        return _templateRow;
    }

	public RowWrapper.PartLoadInfo getCurrentPartLoad() {
		return _currentPartLoad;
	}

	public void setTemplateRow(RowWrapper templateRow)
    {
        _templateRow = templateRow;
    }

    public void setPartsColumnHeaders(List<IdentifiableWrapper> partsColumnHeaders) { _partsColumnHeaders = partsColumnHeaders; }

    public void setCurrentPartColumnHeader(IdentifiableWrapper currentPartColumnHeader) { _currentPartColumnHeader = currentPartColumnHeader; }

	public void setCurrentPartLoad(RowWrapper.PartLoadInfo _currentPartLoad) {
		this._currentPartLoad = _currentPartLoad;
	}

    private List<RowWrapper.PartLoadInfo> getPartsLoadInfo(IEppRegElWrapper wrapper)
    {
        return new TreeMap<>(wrapper.getPartMap()).values().stream()
            .map(wrap -> new RowWrapper.PartLoadInfo(wrap.getLoadAsString(), wrap.getActionsAsString()))
            .collect(Collectors.toList());
    }
}