package ru.tandemservice.uniepp.dao.settings;

import java.util.Map;
import java.util.Set;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import ru.tandemservice.uniepp.base.bo.EppSettings.logic.EppSettingsWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;

/**
 * 
 * @author nkokorina
 *
 */
public interface IEppSettingsDAO
{
    public static final SpringBeanCache<IEppSettingsDAO> instance = new SpringBeanCache<IEppSettingsDAO>(IEppSettingsDAO.class.getName());

    /**
     * Глобальные настройки учебного процесса
     */
    EppSettingsWrapper getGlobalSettings();

    /**
     * Сохранение глобальных настроек учебного процесса
     */
    void saveGlobalSettings(EppSettingsWrapper settingsWrapper);

    /**
     * @param orgUnitId
     * @return Отображать или нет на подрузделении вкладку Учебный процесс
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isShowEppOrgUnitTab(Long orgUnitId);

    /**
     * @param orgUnitId
     * @return Отображать или нет на подрузделении вкладку Учебные группы
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isShowRealGroupOrgUnitTab(Long orgUnitId);

    /** @param orgUnitId */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isOperationOrgUnit(Long orgUnitId);

    /** @param orgUnitId */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isGroupOrgUnit(Long orgUnitId);

    /** @param orgUnitId */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean isTutorOrgUnit(Long orgUnitId);

    /** @param orgUnitId */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    boolean isPlanOrgUnit(Long orgUnitId);


    /**
     * @return upper(аббревиатура типа недель для редактирования) -> тип недели
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    Map<String, EppWeekType> getWeekTypeEditAbbreviationMap();


    /**
     * @return { developForm.code -> { eppELoadType.code -> { weekType.code }}}
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Map<String, Map<String, Set<String>>> getELoadWeekTypeMap();


}
