/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithDisciplineGaps;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListPresenter;
import ru.tandemservice.uniepp.dao.student.EppStudentSlotDAO;

import java.util.Date;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
@State({
    @Bind(key = "orgUnitId", binding = "orgUnitHolder.id")
})
public class EppIndicatorStudentsWithDisciplineGapsUI extends AbstractEppIndicatorStudentListPresenter
{
    @Override
    public String getSettingsKey()
    {
        return "epp.StudentsWithDisciplineGaps.filter";
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
    }

    public void onClickSync()
    {
        EppStudentSlotDAO.DAEMON.wakeUpDaemon();
        onComponentRefresh();
    }

    public String getDaemonStatus(final String message)
    {
        final Long date = EppStudentSlotDAO.DAEMON.getCompleteStatus();
        if (null != date)
            return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));

        return message;
    }
}
