/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppContract.logic;

import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;

import ru.tandemservice.uni.dao.UniBaseDao;

/**
 * @author oleyba
 * @since 7/24/12
 */
public class EppContractPrintDao extends UniBaseDao implements IEppContractPrintDao
{

    @Override
    public IDocumentRenderer print(CtrContractVersion contract)
    {
        CtrContractVersionTemplateData templateData = get(CtrContractVersionTemplateData.class, CtrContractVersionTemplateData.owner(), contract);
        if (null == templateData || contract.getPrintTemplate() == null) {
            throw new ApplicationException("Версия разрешена к редактированию в свободной форме, и не может быть напечатана из системы по шаблону. Создайте печатную версию вне системы и воспользуйтесь опцией «Загрузить файл», или удалите версию и создайте вновь с использованием шаблона.");
        }

        return templateData.getManager().dao().print(templateData);
    }
}
