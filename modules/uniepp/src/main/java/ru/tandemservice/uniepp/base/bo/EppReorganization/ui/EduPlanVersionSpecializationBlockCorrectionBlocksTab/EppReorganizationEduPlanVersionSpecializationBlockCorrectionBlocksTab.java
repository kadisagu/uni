/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppReorganization.ui.EduPlanVersionSpecializationBlockCorrectionBlocksTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.fias.base.bo.util.NonActiveSelectValueStyle;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 05.04.2016
 */
@Configuration
public class EppReorganizationEduPlanVersionSpecializationBlockCorrectionBlocksTab extends BusinessComponentManager {

    public static final String OWNER_DS = "ownerDS";
    public static final String OWNER = "owner";
    public static final String BLOCKS_DS = "blocksDS";

    public static final String SUBJECT_DS = "subjectDS";
    public static final String SPECIALIZATION_DS = "specializationDS";
    public static final String PROGRAM_FORM_DS = "programFormDS"; //
    public static final String DEV_CONDITION_DS = "devConditionDS";
    public static final String EDU_PROGRAM_TRAIT_DS = "eduProgramTraitDS";
    public static final String DEVELOP_GRID_DS = "developGridDS"; //
    public static final String STATE_DS = "stateDS";
    public static final String SUBJECT = "subject";
    public static final String SPECIALIZATION = "specialization";
    public static final String PROGRAM_FORM = "programForm";
    public static final String DEV_CONDITION = "devCondition";
    public static final String EDU_PROGRAM_TRAIT = "eduProgramTrait";
    public static final String DEVELOP_GRID = "developGrid";
    public static final String STATE = "state";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(OWNER_DS, getName(), EduOwnerOrgUnit.eduOwnerOrgUnitReorganizationCustomizedOrgUnitHandler(getName())
                        .order(OrgUnit.archival()))
                        .valueStyleSource(EduOwnerOrgUnit.archivalWiseStyle))
                .addDataSource(searchListDS(BLOCKS_DS, epvBlockDSColumnListExtPoint(), epvBlocksDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(PROGRAM_FORM_DS, getName(), EduProgramForm.defaultSelectDSHandler(getName())
                        .customize((alias, dql, context, filter) ->
                        {
                            final EduProgramSubject subject = context.get(SUBJECT);
                            final DQLSelectBuilder subDQL = new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, "epf")
                                    .where(eq(property("epf", EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit()), value(((OrgUnit) context.get(OWNER)))))
                                    .where(eq(property(alias, EduProgramForm.id()),
                                            property("epf", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan().programForm().id())));

                            dql.where(exists(subDQL.buildQuery()));

                            return dql;
                        }).filter(EduProgramForm.title())
                ))
                .addDataSource(selectDS(DEVELOP_GRID_DS, developGridHandler()))
                .addDataSource(selectDS(SUBJECT_DS, subjectHandler()).addColumn(EduProgramSubject.titleWithCodeIndexAndGen().s()))
                .addDataSource(selectDS(SPECIALIZATION_DS, specializationHandler()))
                .addDataSource(selectDS(DEV_CONDITION_DS, devConditionHandler()))
                .addDataSource(selectDS(EDU_PROGRAM_TRAIT_DS, eduProgramTraitHandler()))
                .addDataSource(selectDS(STATE_DS, stateHandler()))
                .create();
    }


    @Bean
    public EntityComboDataSourceHandler developGridHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopGrid.class)
                .customize((alias, dql, context, filter) ->
                {
                    final EduProgramSubject subject = context.get(SUBJECT);
                    final DQLSelectBuilder subDQL = new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, "dp")
                            .where(eq(property("dp", EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit()), value(((OrgUnit) context.get(OWNER)))))
                            .where(eq(property(alias, DevelopGrid.id()),
                                    property("dp", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan().developGrid().id())));

                    dql.where(exists(subDQL.buildQuery()));

                    return dql;
                }).filter(DevelopGrid.title());
    }

    @Bean
    public ColumnListExtPoint epvBlockDSColumnListExtPoint()
    {
        return columnListExtPointBuilder(BLOCKS_DS)
                .addColumn(checkboxColumn("checkboxColumn").number("0"))
                .addColumn(publisherColumn("titleColumn", EppEduPlanVersionSpecializationBlock.titleForListWithVersionAndSpecialization()))
                .addColumn(textColumn("subject", EppEduPlanVersionSpecializationBlock.programSpecialization().programSubject().titleWithCode()).order())
                .addColumn(textColumn("specialization", EppEduPlanVersionSpecializationBlock.programSpecialization().title()))
                .addColumn(publisherColumn("epVersion", EppEduPlanVersionSpecializationBlock.eduPlanVersion().title()).order()
                        .publisherLinkResolver(new SimplePublisherLinkResolver(EppEduPlanVersionSpecializationBlock.eduPlanVersion().id())))
                .addColumn(textColumn("developCombination", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan().developCombinationTitle()))
                .addColumn(textColumn("state", EppEduPlanVersionSpecializationBlock.eduPlanVersion().state().title()).order())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> epvBlocksDSHandler() {
       return new DefaultSearchDataSourceHandler(getName(), EppEduPlanVersionSpecializationBlock.class)
       {
           @Override
           protected DSOutput execute(DSInput input, ExecutionContext context) {
               final String alias = "e";
               final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, alias);

               final EduProgramSubject subject = context.get(SUBJECT);
               final List<EduProgramSpecialization> specializations = context.get(SPECIALIZATION);
               final List<EduProgramForm> programForms = context.get(PROGRAM_FORM);
               final List<DevelopCondition> devConditions = context.get(DEV_CONDITION);
               final List<EduProgramTrait> eduProgramTraits = context.get(EDU_PROGRAM_TRAIT);
               final List<DevelopGrid> developGrids = context.get(DEVELOP_GRID);
               final EppState state = context.get(STATE);
               final OrgUnit owner = context.get(OWNER);

               builder.where(eq(property(alias, EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit().id()), value(owner.getId())));

               if (null != subject)
                   builder.where(eq(property(alias, EppEduPlanVersionSpecializationBlock.programSpecialization().programSubject().id()), value(subject.getId())));

               if (null != specializations && !specializations.isEmpty())
                   builder.where(in(property(alias, EppEduPlanVersionSpecializationBlock.programSpecialization()), specializations));

               if (null != programForms && !programForms.isEmpty())
                   builder.where(in(property(alias, EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan().programForm()), programForms));

               if (null != devConditions && !devConditions.isEmpty())
                   builder.where(in(property(alias, EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan().developCondition()), devConditions));

               if (null != eduProgramTraits && !eduProgramTraits.isEmpty())
                   builder.where(in(property(alias, EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan().programTrait()), eduProgramTraits));

               if (null != developGrids && !developGrids.isEmpty())
                   builder.where(in(property(alias, EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan().developGrid()), developGrids));

               if (null != state)
                   builder.where(eq(property(alias, EppEduPlanVersionSpecializationBlock.eduPlanVersion().state()), value(state)));


               final EntityOrder entityOrder = input.getEntityOrder();

               if (null != entityOrder)
               {

                   final String keyString = entityOrder.getKeyString();
                   if (keyString.equals(EppEduPlanVersionSpecializationBlock.programSpecialization().programSubject().titleWithCode().s()))
                       builder.order(property(alias, EppEduPlanVersionSpecializationBlock.programSpecialization().programSubject().subjectCode()), entityOrder.getDirection())
                           .order(property(alias, EppEduPlanVersionSpecializationBlock.programSpecialization().programSubject().title()), entityOrder.getDirection());
                   else if (keyString.equals(EppEduPlanVersionSpecializationBlock.eduPlanVersion().state().title().s()))
                       builder.order(property(alias, EppEduPlanVersionSpecializationBlock.eduPlanVersion().state().code()), entityOrder.getDirection());
                   else if (keyString.equals(EppEduPlanVersionSpecializationBlock.eduPlanVersion().title().s()))
                       builder.order(property(alias, EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan().number()), entityOrder.getDirection())
                               .order(property(alias, EppEduPlanVersionSpecializationBlock.eduPlanVersion().number()), entityOrder.getDirection())
                               .order(property(alias, EppEduPlanVersionSpecializationBlock.eduPlanVersion().titlePostfix()), entityOrder.getDirection());
               }

               return DQLSelectOutputBuilder.get(input, builder, context.getSession()).build();
           }
       };
    }

    @Bean
    public EntityComboDataSourceHandler subjectHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
                .filter(EduProgramSubject.title())
                .filter(EduProgramSubject.code())
                .order(EduProgramSubject.subjectCode())
                .order(EduProgramSubject.title())
                .customize((alias, dql, context, filter) ->
                        dql.where(exists(
                                new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, "epv")
                                        .where(eq(property("epv", EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit()), value(((OrgUnit) context.get(OWNER)))))
                                        .where(eq(property("epv", EppEduPlanVersionSpecializationBlock.programSpecialization().programSubject()), property(alias)))
                                .buildQuery()
                        )))
                            //    .order(property(alias, EduProgramSubject.subjectCode()))
                              //  .order(property(alias, EduProgramSubject.title())))
                ;
    }

    @Bean
    public EntityComboDataSourceHandler specializationHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSpecialization.class)
                .customize((alias, dql, context, filter) -> {

                    final EduProgramSubject subject = context.get(SUBJECT);
                    final DQLSelectBuilder subDQL = new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, "epv")
                            .where(eq(property("epv", EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit()), value(((OrgUnit) context.get(OWNER)))))
                            .where(eq(property(alias, EduProgramSpecialization.id()),
                                    property("epv", EppEduPlanVersionSpecializationBlock.programSpecialization().id())));
                    subDQL.where(eq(property("epv", EppEduPlanVersionSpecializationBlock.programSpecialization().programSubject()), value(subject)));

                    dql.where(exists(subDQL.buildQuery()))
                        .order(property(alias, EduProgramSpecialization.title()));

                    return dql;
                }).filter(EduProgramSpecialization.title());
    }

    @Bean
    public EntityComboDataSourceHandler devConditionHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopCondition.class)
                .customize((alias, dql, context, filter) -> {
                    final DQLSelectBuilder subDQL = new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, "epv")
                            .where(eq(property("epv", EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit()), value(((OrgUnit) context.get(OWNER)))))
                            .where(eq(property(alias, DevelopCondition.id()),
                                    property("epv", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan().developCondition().id())));

                    dql
                            .where(exists(subDQL.buildQuery()))
                            .order(property(alias, DevelopCondition.title()));

                    return dql;
                }).filter(DevelopCondition.title());
    }

    @Bean
    public EntityComboDataSourceHandler eduProgramTraitHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramTrait.class)
                .customize((alias, dql, context, filter) -> {
                    final DQLSelectBuilder subDQL = new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, "epv")
                            .where(eq(property("epv", EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit()), value(((OrgUnit) context.get(OWNER)))))
                            .where(eq(property(alias, EduProgramTrait.id()),
                                    property("epv", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan().programTrait().id())));

                    dql.where(exists(subDQL.buildQuery()))
                            .order(property(alias, EduProgramTrait.title()));


                    return dql;
                }).filter(EduProgramTrait.title());
    }


    @Bean
    public EntityComboDataSourceHandler stateHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppState.class).order(EppState.code());
    }

}
