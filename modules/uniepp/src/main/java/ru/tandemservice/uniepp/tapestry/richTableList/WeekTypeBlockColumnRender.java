/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.tapestry.richTableList;

import org.apache.tapestry.IMarkupWriter;
import org.apache.tapestry.IRequestCycle;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.RichTableList;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.callback.ParamCallback;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.render.SimpleColumnRender;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;

import java.util.Map;

/**
 * @author vip_delete
 * @since 16.03.2010
 */
public class WeekTypeBlockColumnRender extends SimpleColumnRender
{


    @Override
    public void render(final IMarkupWriter writer, final IRequestCycle cycle, final RichTableList tableList)
    {
        final WeekTypeBlockColumn column = (WeekTypeBlockColumn) tableList.getDataSource().getCurrentColumn();

        final Long rowId = tableList.getDataSource().getCurrentEntity().getId();
        final Long colId = column.getColumnId();
        final String prefix = colId+"_"+rowId;

        final RangeSelectionWeekTypeListDataSource<?> dataSource = (RangeSelectionWeekTypeListDataSource<?>) tableList.getModel();
        final PairKey<Long, Long> key = PairKey.create(rowId, colId);

        final EppWeekType value = dataSource.getDataMap().get(key);

        if (rowId.equals(dataSource.getEditId()))
        {
            // активная строка
            final String clientId = tableList.getClientId();

            // проверяем, что строка доступна для редкатирования содержимого (границы двигать можно)
            final boolean editDisabled = column.isDisabled(tableList.getDataSource().getCurrentEntity());

            final ParamCallback.IParamBinding paramListener = editDisabled ? null : new ParamCallback.IParamBinding() {
                @Override public void rewind(final IRequestCycle cycle, final RichTableList tableList, final String value) {
                    final RangeSelectionWeekTypeListDataSource<?> model = (RangeSelectionWeekTypeListDataSource<?>) tableList.getModel();
                    final Map<PairKey<Long, Long>, EppWeekType> dataMap = model.getDataMap();

                    try {
                        final EppWeekType weekType = model.getWeekType(value);
                        if (null == weekType) {
                            dataMap.remove(key);
                        } else {
                            dataMap.put(key, weekType);
                        }
                    } catch (ValidatorException e) {
                        tableList.getForm().getDelegate().record(e);
                        EppWeekType tmp = new EppWeekType();
                        tmp.setAbbreviationEdit(value);
                        dataMap.put(key, tmp);
                    }
                }
            };

            final RichRangeSelection selection = dataSource.getSelection();

            final StringBuilder sb = new StringBuilder("rr-cell rr-edit");
            final int type = selection.getPointType(column.getColumnIdx());
            switch (type) {
                case RichRangeSelection.LEFT:  sb.append(" rr-left");  break;
                case RichRangeSelection.RIGHT: sb.append(" rr-right"); break;
                case RichRangeSelection.BOTH:  sb.append(" rr-both");  break;
            }

            writer.begin("td");
            if (dataSource.getSelection().isCanClick(column.getColumnIdx())) {
                sb.append(" rr-active");
                writer.attribute("id", tableList.registerSubmitListener(prefix+"_submit", "onClickPoint", column.getColumnIdx(), clientId));
                writer.attribute("tabIndex", "0");
                writer.attribute("onclick", "buttonClick(event,this);return false;");
            }
            writer.attribute("class", sb.toString());

            writer.beginEmpty("input");

            if (null != paramListener) {
                writer.attribute("name", tableList.registerParamBinding(prefix+"_input", paramListener));
                writer.attribute("onclick", "event.cancelBubble=true;return false;");
            } else {
                writer.attribute("disabled", "true");
            }

            writer.attribute("type", "text");

            if (null != value) {
                try {
                    final RangeSelectionWeekTypeListDataSource<?> model = (RangeSelectionWeekTypeListDataSource<?>) tableList.getModel();
                    model.getWeekType(value.getAbbreviationEdit());
                } catch (ValidatorException validatorException) {
                    writer.attribute("style", "background-color:#ffe8f3");
                    writer.attribute("title", validatorException.getMessage());
                }
            }

            writer.attribute("maxlength", "2");
            writer.attribute("value", (null == value ? "" : value.getAbbreviationEdit()));
            writer.attribute("onkeydown", "saveonenter(event)");
            writer.closeTag();

            writer.end("td");
        }
        else
        {
            // пассивная строка
            final int[] data = dataSource.getRow2data().get(rowId);
            final int type = data == null ? RichRangeSelection.EMPTY : data[column.getColumnIdx()];

            writer.begin("td");
            switch (type)
            {
                case RichRangeSelection.EMPTY:
                    writer.attribute("class", "rr-cell");
                    break;

                case RichRangeSelection.LEFT:
                    writer.attribute("class", "rr-cell rr-left");
                    break;

                case RichRangeSelection.RIGHT:
                    writer.attribute("class", "rr-cell rr-right");
                    break;

                case RichRangeSelection.BOTH:
                    writer.attribute("class", "rr-cell rr-both");
                    break;
            }

            writer.print(null == value ? "" : value.getAbbreviationView());
            writer.end("td");
        }
    }
}
