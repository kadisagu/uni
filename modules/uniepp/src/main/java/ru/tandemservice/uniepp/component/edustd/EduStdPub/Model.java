package ru.tandemservice.uniepp.component.edustd.EduStdPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

@State( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eppStateEduStandard.id"), @Bind(key = "selectedTab", binding = "selectedTab") })
public class Model
{
    private String _selectedTab;
    private EppStateEduStandard _eppStateEduStandard = new EppStateEduStandard();

    public String getSelectedTab()
    {
        return this._selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        this._selectedTab = selectedTab;
    }

    public EppStateEduStandard getEppStateEduStandard()
    {
        return this._eppStateEduStandard;
    }

    public void setEppStateEduStandard(final EppStateEduStandard eppStateEduStandard)
    {
        this._eppStateEduStandard = eppStateEduStandard;
    }

    public boolean isViewFileEduStandardDisabled()
    {
        return this.getEppStateEduStandard().getContent() == null;
    }
}
