package ru.tandemservice.uniepp.component.workplan.WorkPlanData;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.FormContainerFacade;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanTemplateRow;

import java.util.HashMap;

/**
 * @author vdanilov
 */
public class WorkPlanDataEditableController<ID extends IPrepareable<MODEL>, MODEL extends WorkPlanDataBaseModel> extends AbstractBusinessController<ID, MODEL>
{

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    // удаление строки РУП
    public void onClickDeleteRow(final IBusinessComponent component)
    {
        UniDaoFacade.getCoreDao().delete(component.<Long>getListenerParameter());
        component.refresh();
    }

    // добавление строки с дисциплиной в РУП (из реестра)
    public void onClickAddDisciplineElement(final IBusinessComponent component)
    {
        component.createChildRegion("dialog", new ComponentActivator(
                ru.tandemservice.uniepp.component.workplan.WorkPlanRowAddEdit.Model.class.getPackage().getName(),
                new HashMap<String, Object>(){{
                        put(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getWorkPlanId());
                        put("isRowAction", false);
                    }}
        ));
    }

    // добавление строки с дисциплиной в РУП (из УП(в))
    public void onClickAddDisciplineFromEduPlanVersion(final IBusinessComponent component)
    {
        FormContainerFacade.asForm(component, ru.tandemservice.uniepp.component.workplan.WorkPlanDisciplineAddFromEduPlan.Model.class.getPackage().getName())
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getWorkPlanId())
                .parameter(ru.tandemservice.uniepp.component.workplan.WorkPlanDisciplineAddFromEduPlan.Model.BIND_IS_ACTIONS, false)
                .activate();
    }

    // добавление строки с практикой или ГИА в РУП (из УП(в))
    public void onClickAddActionFromEduPlanVersion(final IBusinessComponent component)
    {
        FormContainerFacade.asForm(component, ru.tandemservice.uniepp.component.workplan.WorkPlanDisciplineAddFromEduPlan.Model.class.getPackage().getName())
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getWorkPlanId())
                .parameter(ru.tandemservice.uniepp.component.workplan.WorkPlanDisciplineAddFromEduPlan.Model.BIND_IS_ACTIONS, true)
                .activate();
    }

    public boolean isEditableRow(final IEntityMeta meta)
    {
        return EppWorkPlanRegistryElementRow.class.isAssignableFrom(meta.getEntityClass()) ||
                EppWorkPlanTemplateRow.class.isAssignableFrom(meta.getEntityClass());
    }


    // редактирование строки с дисциплиной в РУП
    public void onClickEditRow(final IBusinessComponent component)
    {
        final Long rowId = component.getListenerParameter();
        final IEntityMeta meta = EntityRuntime.getMeta(rowId);

        if (isEditableRow(meta))
        {
            component.createChildRegion("dialog", new ComponentActivator(
                    ru.tandemservice.uniepp.component.workplan.WorkPlanRowAddEdit.Model.class.getPackage().getName(),
                    new HashMap<String, Object>(){{
                            put(PublisherActivator.PUBLISHER_ID_KEY, rowId);
                            put("isRowAction", false);
                        }}
            ));
        }

    }

    // изменение распредления нагрузки по частям для строк с дисциплинами РУП
    @SuppressWarnings("UnusedDeclaration")
    public void onClickEditTotalPartLoad(final IBusinessComponent component)
    {
        final Long rowId = component.getListenerParameter();

        final IEntityMeta meta = EntityRuntime.getMeta(rowId);

        if (EppWorkPlanRegistryElementRow.class.isAssignableFrom(meta.getEntityClass()))
        {
            component.createChildRegion("dialog", new ComponentActivator(
                    ru.tandemservice.uniepp.component.workplan.WorkPlanTotalPartLoadEdit.Model.class.getPackage().getName(),
                    new HashMap<String, Object>(){{put(PublisherActivator.PUBLISHER_ID_KEY, rowId);}}
            ));
        }
    }

    // добавление строки с мероприятием в РУП (вторая таблица)
    public void onClickAddActionElement(final IBusinessComponent component)
    {
        component.createChildRegion("dialog", new ComponentActivator(
                ru.tandemservice.uniepp.component.workplan.WorkPlanRowAddEdit.Model.class.getPackage().getName(),
                new HashMap<String, Object>(){{
                    put(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getWorkPlanId());
                    put("isRowAction", true);
                }}
        ));
    }

    // редактирование строки с мероприятием в РУП (вторая таблица)
    @SuppressWarnings("UnusedDeclaration")
    public void onClickEditActionElement(final IBusinessComponent component)
    {
        final Long rowId = component.getListenerParameter();

        final IEntityMeta meta = EntityRuntime.getMeta(rowId);

        if (isEditableRow(meta))
        {
            component.createChildRegion("dialog", new ComponentActivator(
                    ru.tandemservice.uniepp.component.workplan.WorkPlanRowAddEdit.Model.class.getPackage().getName(),
                    new HashMap<String, Object>()
                    {{
                            put(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter());
                            put("isRowAction", true);
                        }}
            ));
        }
    }

}
