/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.student.StudentWorkPlanTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;

import java.util.Collections;

/**
 * @author vip_delete
 * @since 24.03.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        if (null != model.getWorkplan())
        {
            component.createChildRegion("studentWorkplanDataScope", new ComponentActivator(
                    ru.tandemservice.uniepp.component.student.StudentWorkPlanData.Model.class.getPackage().getName(),
                    new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getWorkplan().getId())
            ));
        }
    }

    public void onClickConnetWorkplan(final IBusinessComponent component)
    {
        component.createChildRegion("studentWorkplanScope", new ComponentActivator(
                ru.tandemservice.uniepp.component.student.MassChangeWorkPlan.Model.class.getPackage().getName(),
                new ParametersMap().add("ids", Collections.singletonList(this.getModel(component).getStudent().getId()))
        ));
    }

    public void onClickSpecializeWorkplan(final IBusinessComponent component)
    {
        component.createChildRegion("studentWorkplanScope", new ComponentActivator(
                ru.tandemservice.uniepp.component.student.StudentWorkPlanEdit.Model.class.getPackage().getName(),
                new ParametersMap().add("id", this.getDao().doSpecializeWorkPlan(this.getModel(component)))
        ));
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        this.onRefreshComponent(component);
    }
}