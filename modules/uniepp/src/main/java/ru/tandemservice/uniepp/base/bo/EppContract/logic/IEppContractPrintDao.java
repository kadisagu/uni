/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppContract.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContractVersionPrinter;

/**
 * @author oleyba
 * @since 7/24/12
 */
public interface IEppContractPrintDao extends INeedPersistenceSupport, ICtrContractVersionPrinter
{
}
