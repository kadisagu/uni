/**
 *$Id: EppWorkPlanStudentListUI.java 26089 2013-02-11 08:34:37Z vdanilov $
 */
package ru.tandemservice.uniepp.base.bo.EppEduGroup.ui.List4StudentIds;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uniepp.base.bo.EppEduGroup.logic.EppEduGroupList4StudentIdsDSHandler;
import ru.tandemservice.uniepp.base.bo.EppEduGroup.logic.EppEduGroupRegistryElement4StudentIdsDSHandler;
import ru.tandemservice.uniepp.base.bo.EppEduGroup.logic.EppEduGroupTutorOrgUnit4StudentIdsDSHandler;

import java.util.Collection;

@Input({
    @Bind(key="studentIds", binding="studentIds", required=true),
    @Bind(key="groupOrgUnitId", binding="groupOrgUnitId", required=false)
})
public class EppEduGroupList4StudentIdsUI extends UIPresenter
{

    private Collection<Long> studentIds;
    public Collection<Long> getStudentIds() { return this.studentIds; }
    public void setStudentIds(Collection<Long> studentIds) { this.studentIds = studentIds; }

    private Long groupOrgUnitId;
    public Long getGroupOrgUnitId() { return this.groupOrgUnitId; }
    public void setGroupOrgUnitId(Long groupOrgUnitId) { this.groupOrgUnitId = groupOrgUnitId; }

    @Override
    public String getSettingsKey()
    {
        return getConfig().getName() + "." + String.valueOf(FastBeanUtils.getValue(this, "groupOrgUnitId"));
    }


    @Override
    public void onComponentRefresh() {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (EppEduGroupList4StudentIds.TUTORORGUNIT_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppEduGroupTutorOrgUnit4StudentIdsDSHandler.PARAM_STUDENT_IDS, getStudentIds());
        }
        else if (EppEduGroupList4StudentIds.REGISTRYELEMENT_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppEduGroupRegistryElement4StudentIdsDSHandler.PARAM_STUDENT_IDS, getStudentIds());
            dataSource.put(EppEduGroupRegistryElement4StudentIdsDSHandler.PARAM_TUTOR_ORGUNIT_LIST, getSettings().get("tutorOrgUnitList"));
        }
        else if (EppEduGroupList4StudentIds.EDUGROUP_LIST_DS.equals(dataSource.getName()))
        {
            dataSource.put(EppEduGroupList4StudentIdsDSHandler.PARAM_STUDENT_IDS, getStudentIds());
            dataSource.put(EppEduGroupList4StudentIdsDSHandler.PARAM_YEAR_PART_LIST, getSettings().get("yearPartList"));
            dataSource.put(EppEduGroupList4StudentIdsDSHandler.PARAM_GROUP_TYPE_LIST, getSettings().get("groupTypeList"));
            dataSource.put(EppEduGroupList4StudentIdsDSHandler.PARAM_TUTOR_ORGUNIT_LIST, getSettings().get("tutorOrgUnitList"));
            dataSource.put(EppEduGroupList4StudentIdsDSHandler.PARAM_REGISTRY_ELEMENT_LIST, getSettings().get("registryElementList"));
        }
    }

    public void onClickSearch() {
        getSettings().save();
    }

    public void onClickClear() {
        getSettings().clear();
        onClickSearch();
    }

}
