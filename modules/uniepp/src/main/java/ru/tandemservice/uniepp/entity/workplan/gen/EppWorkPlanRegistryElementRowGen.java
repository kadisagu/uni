package ru.tandemservice.uniepp.entity.workplan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка РУП: часть элемента реестра
 *
 * Строка РУП "часть элемента реестра" определяет, какая часть элемента реестра используется в РУП
 * и задает по ней распределение нагрузки и форм контроля
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkPlanRegistryElementRowGen extends EppWorkPlanRow
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow";
    public static final String ENTITY_NAME = "eppWorkPlanRegistryElementRow";
    public static final int VERSION_HASH = 168106068;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_ELEMENT_PART = "registryElementPart";

    private EppRegistryElementPart _registryElementPart;     // Часть элемента реестра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getRegistryElementPart()
    {
        return _registryElementPart;
    }

    /**
     * @param registryElementPart Часть элемента реестра. Свойство не может быть null.
     */
    public void setRegistryElementPart(EppRegistryElementPart registryElementPart)
    {
        dirty(_registryElementPart, registryElementPart);
        _registryElementPart = registryElementPart;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppWorkPlanRegistryElementRowGen)
        {
            setRegistryElementPart(((EppWorkPlanRegistryElementRow)another).getRegistryElementPart());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkPlanRegistryElementRowGen> extends EppWorkPlanRow.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkPlanRegistryElementRow.class;
        }

        public T newInstance()
        {
            return (T) new EppWorkPlanRegistryElementRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "registryElementPart":
                    return obj.getRegistryElementPart();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "registryElementPart":
                    obj.setRegistryElementPart((EppRegistryElementPart) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "registryElementPart":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "registryElementPart":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "registryElementPart":
                    return EppRegistryElementPart.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkPlanRegistryElementRow> _dslPath = new Path<EppWorkPlanRegistryElementRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkPlanRegistryElementRow");
    }
            

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow#getRegistryElementPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
    {
        return _dslPath.registryElementPart();
    }

    public static class Path<E extends EppWorkPlanRegistryElementRow> extends EppWorkPlanRow.Path<E>
    {
        private EppRegistryElementPart.Path<EppRegistryElementPart> _registryElementPart;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow#getRegistryElementPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
        {
            if(_registryElementPart == null )
                _registryElementPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_REGISTRY_ELEMENT_PART, this);
            return _registryElementPart;
        }

        public Class getEntityClass()
        {
            return EppWorkPlanRegistryElementRow.class;
        }

        public String getEntityName()
        {
            return "eppWorkPlanRegistryElementRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
