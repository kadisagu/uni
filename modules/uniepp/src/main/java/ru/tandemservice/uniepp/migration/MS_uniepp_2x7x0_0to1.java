package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x7x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppEduPlanProf

		// создано свойство laborAsLong
		if(tool.tableExists("epp_eduplan_prof_t") && !tool.columnExists("epp_eduplan_prof_t", "laboraslong_p"))
		{
			// создать колонку
			tool.createColumn("epp_eduplan_prof_t", new DBColumn("laboraslong_p", DBType.LONG));

		}

		// создано свойство weeksAsLong
		if(tool.tableExists("epp_eduplan_prof_t") && !tool.columnExists("epp_eduplan_prof_t", "weeksaslong_p"))
		{
			// создать колонку
			tool.createColumn("epp_eduplan_prof_t", new DBColumn("weeksaslong_p", DBType.LONG));

		}


    }
}