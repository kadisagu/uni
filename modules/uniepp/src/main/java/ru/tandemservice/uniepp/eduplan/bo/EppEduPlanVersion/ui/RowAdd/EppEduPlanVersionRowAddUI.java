/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAdd;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.Return;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.GroupRowAddEdit.EppEduPlanVersionGroupRowAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RegRowAddEdit.EppEduPlanVersionRegRowAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.RowAddEditBase.EppEduPlanVersionRowAddEditBaseUI;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.SelGroupRowAddEdit.EppEduPlanVersionSelGroupRowAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.StructRowAddEdit.EppEduPlanVersionStructRowAddEdit;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author oleyba
 * @since 11/12/14
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id"),
    @Bind(key = "scrollPos", binding = "scrollPos"),
    @Bind(key = "highlightRowId", binding = "scrollRowId")
})
@Return({
    @Bind(key = "scroll", binding = "scroll"),
    @Bind(key = "scrollPos", binding = "scrollPos"),
    @Bind(key = "highlightRowId", binding = "scrollRowId"),
    @Bind(key = "scrollRowId", binding = "scrollRowId")
})
public class EppEduPlanVersionRowAddUI extends UIPresenter
{
    private EntityHolder<EppEduPlanVersionBlock> holder = new EntityHolder<>();

    private ISelectModel elementTypeList = new LazySimpleSelectModel<>(ImmutableList.of(
            new IdentifiableWrapper(0L, "Цикл, часть, компонент"),
            new IdentifiableWrapper(1L, "Группа дисциплин"),
            new IdentifiableWrapper(2L, "Группа дисциплин по выбору"),
            new IdentifiableWrapper(3L, "Мероприятие реестра (дисциплина, практика, ГИА)")
    ));
    private IdentifiableWrapper elementType;

    private Long scrollRowId;
    private Long scrollPos;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
    }

    public void onClickChangeType() {
        IUIPresenter childUI = _uiSupport.getChildUI(UIDefines.DEFAULT_REGION_NAME);
        if (null != childUI) { childUI.deactivate(); }

        if (null != getElementType()) {
            switch (getElementType().getId().intValue()) {
                case 0: _uiActivation.asRegion(EppEduPlanVersionStructRowAddEdit.class).parameter(EppEduPlanVersionRowAddEditBaseUI.BIND_BLOCK_ID, getHolder().getId()).activate(); return;
                case 1: _uiActivation.asRegion(EppEduPlanVersionGroupRowAddEdit.class).parameter(EppEduPlanVersionRowAddEditBaseUI.BIND_BLOCK_ID, getHolder().getId()).activate(); return;
                case 2: _uiActivation.asRegion(EppEduPlanVersionSelGroupRowAddEdit.class).parameter(EppEduPlanVersionRowAddEditBaseUI.BIND_BLOCK_ID, getHolder().getId()).activate(); return;
                case 3: _uiActivation.asRegion(EppEduPlanVersionRegRowAddEdit.class).parameter(EppEduPlanVersionRowAddEditBaseUI.BIND_BLOCK_ID, getHolder().getId()).activate();
            }
        }
    }

    public boolean getScroll() {
        return true;
    }

    // getters and setters

    public EntityHolder<EppEduPlanVersionBlock> getHolder() {
        return holder;
    }

    public EppEduPlanVersionBlock getBlock() {
        return getHolder().getValue();
    }

    public ISelectModel getElementTypeList()
    {
        return elementTypeList;
    }

    public IdentifiableWrapper getElementType()
    {
        return elementType;
    }

    public void setElementType(IdentifiableWrapper elementType)
    {
        this.elementType = elementType;
    }

    public Long getScrollRowId()
    {
        return scrollRowId;
    }

    public void setScrollRowId(Long scrollRowId)
    {
        this.scrollRowId = scrollRowId;
    }

    public Long getScrollPos()
    {
        return scrollPos;
    }

    public void setScrollPos(Long scrollPos)
    {
        this.scrollPos = scrollPos;
    }
}