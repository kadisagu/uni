/**
 *$Id: EppWorkPlanManager.java 26089 2013-02-11 08:34:37Z vdanilov $
 */
package ru.tandemservice.uniepp.base.bo.EppYearPart;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Zhuj
 */
@Configuration
public class EppYearPartManager extends BusinessObjectManager
{
    public static EppYearPartManager instance()
    {
        return instance(EppYearPartManager.class);
    }
}
