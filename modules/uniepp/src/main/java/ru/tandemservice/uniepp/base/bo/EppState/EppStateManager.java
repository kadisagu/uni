// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.base.bo.EppState;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.config.itemList.IItemListExtPointBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppStateDao;
import ru.tandemservice.uniepp.base.bo.EppState.logic.IEppStateDao;
import ru.tandemservice.uniepp.base.bo.EppState.util.IEppStateConfig;
import ru.tandemservice.uniepp.entity.catalog.EppState;

/**
 * @author oleyba
 * @since 8/14/11
 */
@Configuration
public class EppStateManager extends BusinessObjectManager
{
    public static final String EPP_STATE_DS = "eppStateDS";

    public static EppStateManager instance() {
        return BusinessObjectManager.instance(EppStateManager.class);
    }

    @Bean
    public ItemListExtPoint<IEppStateConfig> stateConfigExtPoint()
    {
        IItemListExtPointBuilder<IEppStateConfig> extPointBuilder = itemList(IEppStateConfig.class);
        return extPointBuilder.create();
    }

    @Bean
    public UIDataSourceConfig eppStateDSConfig() {
        return SelectDSConfig.with(EppStateManager.EPP_STATE_DS, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.eppStateDSHandler())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eppStateDSHandler() {
        return new EntityComboDataSourceHandler(this.getName(), EppState.class)
        .order(EppState.code())
        .filter(EppState.title())
        ;
    }

    @Bean
    public IEppStateDao eppStateDao()
    {
        return new EppStateDao();
    }
}
