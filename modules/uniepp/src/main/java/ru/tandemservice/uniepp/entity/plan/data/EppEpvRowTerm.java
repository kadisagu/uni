package ru.tandemservice.uniepp.entity.plan.data;

import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.plan.data.gen.EppEpvRowTermGen;

import java.util.Map;

/**
 * Использование в семестрах
 */
public class EppEpvRowTerm extends EppEpvRowTermGen
{
    public EppEpvRowTerm() {}
    public EppEpvRowTerm(final EppEpvTermDistributedRow row, final Term term) {
        this.setRow(row);
        this.setTerm(term);
    }

    public static Double wrap(final long load) {
        return UniEppUtils.wrap(load < 0 ? null : load);
    }
    public static long unwrap(final Double value) {
        final Long load = UniEppUtils.unwrap(value);
        return null == load ? -1 : load;
    }

    @Deprecated
    public Double getTotalTermLoadSizeAsDouble() {
        return getHoursTotalAsDouble();
    }
    @Deprecated
    public void setTotalTermLoadSizeAsDouble(final Double value) {
        setHoursTotalAsDouble(value);
    }

    @Deprecated
    public Double getTotalTermLoadLaborAsDouble() {
        return getLaborAsDouble();
    }
    @Deprecated
    public void setTotalTermLoadLaborAsDouble(final Double value) {
        setLaborAsDouble(value);
    }

    @Deprecated
    public Double getTotalTermLoadWeeksAsDouble() {
        return getWeeksAsDouble();
    }
    @Deprecated
    public void setTotalTermLoadWeeksAsDouble(final Double value) {
        setWeeksAsDouble(value);
    }

    public Double getHoursTotalAsDouble() {
        return wrap(this.getHoursTotal());
    }
    public void setHoursTotalAsDouble(final Double value) {
        this.setHoursTotal(unwrap(value));
    }

    @Override
    public long getHoursSelfwork()
    {
        return super.getHoursSelfwork();
    }

    public Double getHoursSelfworkAsDouble() {
        return wrap(this.getHoursSelfwork());
    }
    public void setHoursSelfworkAsDouble(final Double value) {
        this.setHoursSelfwork(unwrap(value));
    }

    public Double getHoursAuditAsDouble() {
        return wrap(this.getHoursAudit());
    }
    public void setHoursAuditAsDouble(final Double value) {
        this.setHoursAudit(unwrap(value));
    }

    public Double getLaborAsDouble() {
        return wrap(this.getLabor());
    }
    public void setLaborAsDouble(final Double value) {
        this.setLabor(unwrap(value));
    }

    public Double getWeeksAsDouble() {
        return wrap(this.getWeeks());
    }
    public void setWeeksAsDouble(final Double value) {
        this.setWeeks(unwrap(value));
    }

    @SuppressWarnings("unchecked")
    public void addLoad(Map detailMap)
    {
        detailMap.put(EppLoadType.FULL_CODE_TOTAL_HOURS, getHoursTotalAsDouble());
        detailMap.put(EppLoadType.FULL_CODE_LABOR, getLaborAsDouble());
        detailMap.put(EppLoadType.FULL_CODE_WEEKS, getWeeksAsDouble());
        detailMap.put(EppELoadType.FULL_CODE_AUDIT, wrap(getHoursAudit()));

        // В базе для самостоятельной работы хранится самостоятельная + контроль, а отображается самостоятельная в интерфейсе без контроля (DEV-6531)
        final Double selfwork = wrap(getHoursSelfwork());
        final Double control = wrap(getHoursControl());
        detailMap.put(EppELoadType.FULL_CODE_SELFWORK, selfwork);
        detailMap.put(EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL, selfwork != null && control != null ? wrap(getHoursSelfwork() - getHoursControl()) : selfwork);

        detailMap.put(EppLoadType.FULL_CODE_CONTROL, wrap(getHoursControl()));
        detailMap.put(EppLoadType.FULL_CODE_CONTROL_E, wrap(getHoursControlE()));
    }

    public void updateLoad(Map<String, Double> newDetailMap, boolean withSelfworkWoControl)
    {
        setLaborAsDouble(newDetailMap == null ? null : newDetailMap.get(EppLoadType.FULL_CODE_LABOR));
        setHoursTotalAsDouble(newDetailMap == null ? null : newDetailMap.get(EppLoadType.FULL_CODE_TOTAL_HOURS));
        setWeeksAsDouble(newDetailMap == null ? null : newDetailMap.get(EppLoadType.FULL_CODE_WEEKS));
        setHoursAudit(unwrap(newDetailMap == null ? null : newDetailMap.get(EppELoadType.FULL_CODE_AUDIT)));

        // В базе для самостоятельной работы хранится самостоятельная + контроль, а отображается самостоятельная в интерфейсе без контроля (DEV-6531)
        Double selfworkWithoutControl = newDetailMap == null ? null : newDetailMap.get(EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);
        // FIXME при переходе на произвольные нагрузки надо избавиться от этого ужаса. Не должно быть никаких нагрузок в нагрузках - все в своих клеточках.
        if (withSelfworkWoControl && selfworkWithoutControl == null && newDetailMap != null) {
            selfworkWithoutControl = 0d; // Хак. Надо как-то отличать, в каком виде к нам приходит самостоятельная работа
        }

        final Double control = newDetailMap == null ? null : newDetailMap.get(EppLoadType.FULL_CODE_CONTROL);
        if (selfworkWithoutControl != null)
            setHoursSelfwork(unwrap(selfworkWithoutControl + (control != null ? control : 0)));
        else
            setHoursSelfwork(unwrap(newDetailMap == null ? null : newDetailMap.get(EppELoadType.FULL_CODE_SELFWORK)));

        setHoursControl(unwrap(control));
        setHoursControlE(unwrap(newDetailMap == null ? null : newDetailMap.get(EppLoadType.FULL_CODE_CONTROL_E)));
    }
}