/* $Id$ */
package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Igor Belanov
 * @since 12.04.2017
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_2x11x3_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppEpvRegRowSkill

        // создана новая сущность
        {
            if (!tool.tableExists("epp_epvregrow_skill_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("epp_epvregrow_skill_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eppepvregrowskill"),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("epvregistryrow_id", DBType.LONG).setNullable(false),
                        new DBColumn("skill_id", DBType.LONG).setNullable(false),
                        new DBColumn("skilldeveloplevel_id", DBType.LONG)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("eppEpvRegRowSkill");
            }
        }
    }
}
