package ru.tandemservice.uniepp.component.settings.TutorOrgUnit;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.settings.EppPlanOrgUnit;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setOrgUnitTypeList(new LazySimpleSelectModel<>(OrgUnitManager.instance().dao().getOrgUnitActiveTypeList()));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(final Model model)
    {
        final IDataSettings settings = model.getSettings();
        final Object orgUnitTitle = settings.get("orgUnitTitle");
        final Object orgUnitTypeOption = settings.get("orgUnitTypeOption");

        final Predicate predicate = (object -> {
            boolean isTutorOrgUnitTitle = true;
            boolean isOrgUnitType = true;
            if (orgUnitTitle instanceof String) {
                isTutorOrgUnitTitle = StringUtils.containsIgnoreCase(((OrgUnit) object).getTitle(), (String)orgUnitTitle);
            }
            if (orgUnitTypeOption instanceof OrgUnitType) {
                isOrgUnitType = ((OrgUnit)object).getOrgUnitType().getId().equals(((OrgUnitType)orgUnitTypeOption).getId());
            }
            return isTutorOrgUnitTitle && isOrgUnitType;
        });

        final MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, false));
        final List<OrgUnit> orgUnitList = builder.getResultList(this.getSession());
        final Set<OrgUnit> selectedOrgUnitSet = new HashSet<>(CollectionUtils.select(orgUnitList, predicate));

        final Set<OrgUnit> resultOrgUnitSet = new HashSet<>();
        for (OrgUnit orgUnit : selectedOrgUnitSet) {
            while ((null != orgUnit) && resultOrgUnitSet.add(orgUnit)) {
                orgUnit = orgUnit.getParent();
            }
        }

        final Set<Long> parentIds = new HashSet<>();
        for (final OrgUnit element : resultOrgUnitSet) {
            final OrgUnit parent = element.getParent();
            if ((null != parent) && !selectedOrgUnitSet.contains(parent)) {
                parentIds.add(parent.getId());
            }
        }


        final int orgUnitListSize = resultOrgUnitSet.size();

        final DynamicListDataSource<IEntity> dataSource = model.getDataSource();
        dataSource.setTotalSize(orgUnitListSize);
        dataSource.setCountRow(orgUnitListSize);
        List<IEntity> list = resultOrgUnitSet.stream()
            .map(NestedOrgUnit::new)
            .sorted(DAO::compare)
            .map(NestedOrgUnit::getItem)
            .collect(Collectors.toList());
        dataSource.createPage(list);

        final MQBuilder builderTutor = new MQBuilder(EppTutorOrgUnit.ENTITY_CLASS, "tou", new String[] { EppTutorOrgUnit.orgUnit().id().s() });
        final Set<Long> tutorOrgUnitList = new HashSet<>(builderTutor.<Long> getResultList(this.getSession()));

        final MQBuilder builderPlan = new MQBuilder(EppPlanOrgUnit.ENTITY_CLASS, "pou", new String[] { EppPlanOrgUnit.orgUnit().id().s() });
        final Set<Long> planOrgUnitList = new HashSet<>(builderPlan.<Long> getResultList(this.getSession()));

        for (final ViewWrapper<IEntity> wrapper : ViewWrapper.getPatchedList(dataSource))
        {
            wrapper.setViewProperty("isTutor", tutorOrgUnitList.contains(wrapper.getId()));
            wrapper.setViewProperty("isVisible", planOrgUnitList.contains(wrapper.getId()));
            wrapper.setViewProperty("isParent", parentIds.contains(wrapper.getId()) ? Boolean.TRUE : Boolean.FALSE);
        }
    }

    @Override
    public void doSwitchTutorStatus(final Long orgUnitId)
    {
        synchronized (String.valueOf(orgUnitId).intern())
        {
            final OrgUnit orgUnit = this.get(OrgUnit.class, orgUnitId);

            final List<EppTutorOrgUnit> tutorList = this.getList(EppTutorOrgUnit.class, EppTutorOrgUnit.orgUnit().id().s(), orgUnitId);

            if (tutorList.isEmpty())
            {
                final EppTutorOrgUnit eppOrgUnit = new EppTutorOrgUnit();
                eppOrgUnit.setOrgUnit(orgUnit);
                this.save(eppOrgUnit);
            }
            else
            {
                for (final EppTutorOrgUnit ou : tutorList)
                {
                    this.delete(ou);
                }
            }
        }
    }

    @Override
    public void doSwitchVisiblePlan(final Long orgUnitId)
    {
        synchronized (String.valueOf(orgUnitId).intern())
        {
            final OrgUnit orgUnit = this.get(OrgUnit.class, orgUnitId);

            final List<EppPlanOrgUnit> planList = this.getList(EppPlanOrgUnit.class, EppPlanOrgUnit.orgUnit().id().s(), orgUnitId);

            if (planList.isEmpty())
            {
                final EppPlanOrgUnit eppOrgUnit = new EppPlanOrgUnit();
                eppOrgUnit.setOrgUnit(orgUnit);
                this.save(eppOrgUnit);
            }
            else
            {
                for (final EppPlanOrgUnit ou : planList)
                {
                    this.delete(ou);
                }
            }
        }
    }

    private static int nesting(OrgUnit ou)
    {
        return ou.getParent() == null ? 0 : 1 + nesting(ou.getParent());
    }
    private static int compare(OrgUnit ou1, OrgUnit ou2, int lvlDiff)
    {
        if (lvlDiff > 0)
            return compare(ou1.getParent(), ou2, lvlDiff - 1);
        if (lvlDiff < 0)
            return compare(ou1, ou2.getParent(), lvlDiff + 1);
        if (ou1.getParent() == ou2.getParent())
            return Integer.compare(ou1.getOrgUnitType().getPriority(), ou2.getOrgUnitType().getPriority());
        return compare(ou1.getParent(), ou2.getParent(), 0);
    }
    private static int compare(NestedOrgUnit ou1, NestedOrgUnit ou2)
    {
        int lvlDiff = ou1.getLevel() - ou2.getLevel();
        int stCmp = compare(ou1.getItem(), ou2.getItem(), lvlDiff);
        if (stCmp != 0)
            return stCmp;
        return Integer.compare(ou1.getLevel(), ou2.getLevel());
    }
    static class NestedOrgUnit
    {
        public OrgUnit ou;
        public int level;
        public NestedOrgUnit(OrgUnit ou)
        {
            this.ou = ou;
            this.level = nesting(ou);
        }
        public OrgUnit getItem()
        {
            return ou;
        }
        public int getLevel()
        {
            return level;
        }
    }
}
