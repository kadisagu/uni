package ru.tandemservice.uniepp.component.workplan.WorkPlanPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

/**
 * 
 * @author nkokorina
 * @since 18.03.2010
 */

@State( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eppWorkPlan.id"),
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class Model
{
    private String _selectedTab;
    private EppWorkPlan _eppWorkPlan = new EppWorkPlan();

    public String getSelectedTab()
    {
        return this._selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        this._selectedTab = selectedTab;
    }

    public EppWorkPlan getEppWorkPlan()
    {
        return this._eppWorkPlan;
    }

    public void setEppWorkPlan(final EppWorkPlan eppWorkPlan)
    {
        this._eppWorkPlan = eppWorkPlan;
    }

    public EppEduPlanVersion getEduPlanVersion() {
        return getEppWorkPlan().getEduPlanVersion();
    }

    public EppEduPlan getEduPlan() {
        return getEppWorkPlan().getEduPlanVersion().getEduPlan();
    }

    public boolean isEmpty() {
        return IEppWorkPlanDAO.instance.get().isWorkPlanEmpty(this.getEppWorkPlan().getId());
    }
}
