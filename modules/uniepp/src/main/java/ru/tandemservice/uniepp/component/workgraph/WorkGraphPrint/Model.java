package ru.tandemservice.uniepp.component.workgraph.WorkGraphPrint;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;

/**
 * @author nkokorina
 */

@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"), @Bind(key = "printType", binding = "printType") })
public class Model
{
    private Long id;
    private String printType;
    WorkGraphPrintVersion printVersion = new WorkGraphPrintVersion();

    public WorkGraphPrintVersion getPrintVersion()
    {
        return this.printVersion;
    }
    public void setPrintVersion(final WorkGraphPrintVersion printVersion)
    {
        this.printVersion = printVersion;
    }

    public Long getId()
    {
        return this.id;
    }
    public void setId(final Long id)
    {
        this.id = id;
    }
    public String getPrintType()
    {
        return this.printType;
    }
    public void setPrintType(final String printType)
    {
        this.printType = printType;
    }

}
