/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.parser;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.ImParseResult;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.node.ImElement;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author azhebko
 * @since 27.10.2014
 */
public interface IImParser
{
    public static final SpringBeanCache<IImParser> instance = new SpringBeanCache<IImParser>(IImParser.class.getName());

    /** Разбор входных данных, построение DOM-модели. */
    public Document buildXmlDocument(InputStream input) throws ParserConfigurationException, IOException, SAXException;

    /** Построение структуры из XML: узел -> параметры, вложенные узлы. */
    public ImElement buildImElement(Node node);

    /** Разбор учебного плана ИМЦА, создание артефактов импорта. */
    public ImParseResult parse(ImElement root);
}