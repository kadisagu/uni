package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanSecondaryProf;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебный план СПО
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanSecondaryProfGen extends EppEduPlanProf
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppEduPlanSecondaryProf";
    public static final String ENTITY_NAME = "eppEduPlanSecondaryProf";
    public static final int VERSION_HASH = 1252026094;
    private static IEntityMeta ENTITY_META;

    public static final String P_IN_DEPTH_STUDY = "inDepthStudy";
    public static final String L_OWNER_ORG_UNIT = "ownerOrgUnit";

    private boolean _inDepthStudy;     // Углубленное изучение
    private EduOwnerOrgUnit _ownerOrgUnit;     // Выпускающее подразделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Углубленное изучение. Свойство не может быть null.
     */
    @NotNull
    public boolean isInDepthStudy()
    {
        return _inDepthStudy;
    }

    /**
     * @param inDepthStudy Углубленное изучение. Свойство не может быть null.
     */
    public void setInDepthStudy(boolean inDepthStudy)
    {
        dirty(_inDepthStudy, inDepthStudy);
        _inDepthStudy = inDepthStudy;
    }

    /**
     * @return Выпускающее подразделение.
     */
    public EduOwnerOrgUnit getOwnerOrgUnit()
    {
        return _ownerOrgUnit;
    }

    /**
     * @param ownerOrgUnit Выпускающее подразделение.
     */
    public void setOwnerOrgUnit(EduOwnerOrgUnit ownerOrgUnit)
    {
        dirty(_ownerOrgUnit, ownerOrgUnit);
        _ownerOrgUnit = ownerOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEduPlanSecondaryProfGen)
        {
            setInDepthStudy(((EppEduPlanSecondaryProf)another).isInDepthStudy());
            setOwnerOrgUnit(((EppEduPlanSecondaryProf)another).getOwnerOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanSecondaryProfGen> extends EppEduPlanProf.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlanSecondaryProf.class;
        }

        public T newInstance()
        {
            return (T) new EppEduPlanSecondaryProf();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "inDepthStudy":
                    return obj.isInDepthStudy();
                case "ownerOrgUnit":
                    return obj.getOwnerOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "inDepthStudy":
                    obj.setInDepthStudy((Boolean) value);
                    return;
                case "ownerOrgUnit":
                    obj.setOwnerOrgUnit((EduOwnerOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "inDepthStudy":
                        return true;
                case "ownerOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "inDepthStudy":
                    return true;
                case "ownerOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "inDepthStudy":
                    return Boolean.class;
                case "ownerOrgUnit":
                    return EduOwnerOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlanSecondaryProf> _dslPath = new Path<EppEduPlanSecondaryProf>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlanSecondaryProf");
    }
            

    /**
     * @return Углубленное изучение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanSecondaryProf#isInDepthStudy()
     */
    public static PropertyPath<Boolean> inDepthStudy()
    {
        return _dslPath.inDepthStudy();
    }

    /**
     * @return Выпускающее подразделение.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanSecondaryProf#getOwnerOrgUnit()
     */
    public static EduOwnerOrgUnit.Path<EduOwnerOrgUnit> ownerOrgUnit()
    {
        return _dslPath.ownerOrgUnit();
    }

    public static class Path<E extends EppEduPlanSecondaryProf> extends EppEduPlanProf.Path<E>
    {
        private PropertyPath<Boolean> _inDepthStudy;
        private EduOwnerOrgUnit.Path<EduOwnerOrgUnit> _ownerOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Углубленное изучение. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanSecondaryProf#isInDepthStudy()
     */
        public PropertyPath<Boolean> inDepthStudy()
        {
            if(_inDepthStudy == null )
                _inDepthStudy = new PropertyPath<Boolean>(EppEduPlanSecondaryProfGen.P_IN_DEPTH_STUDY, this);
            return _inDepthStudy;
        }

    /**
     * @return Выпускающее подразделение.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanSecondaryProf#getOwnerOrgUnit()
     */
        public EduOwnerOrgUnit.Path<EduOwnerOrgUnit> ownerOrgUnit()
        {
            if(_ownerOrgUnit == null )
                _ownerOrgUnit = new EduOwnerOrgUnit.Path<EduOwnerOrgUnit>(L_OWNER_ORG_UNIT, this);
            return _ownerOrgUnit;
        }

        public Class getEntityClass()
        {
            return EppEduPlanSecondaryProf.class;
        }

        public String getEntityName()
        {
            return "eppEduPlanSecondaryProf";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
