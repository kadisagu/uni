/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithoutWPDSlots;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListPresenter;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpePart;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
public class EppIndicatorStudentsWithoutWPDSlotsSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public EppIndicatorStudentsWithoutWPDSlotsSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);

        // если часть не выбрали - никуда не бежим
        final IEntity yearPart = context.get(EppIndicatorStudentsWithoutWPDSlotsUI.PROP_YEAR_PART);
        if (null == yearPart) {
            builder.where(nothing());
            return;
        }

        // только неархивные студенты
        builder.where(eq(property(alias, Student.P_ARCHIVAL), value(Boolean.FALSE)));

        // этого деканата
        final OrgUnit orgUnit = context.get(AbstractEppIndicatorStudentListPresenter.PROP_ORG_UNIT);
        if (null != orgUnit && null != orgUnit.getId()) {
            builder.where(eq(property(Student.educationOrgUnit().groupOrgUnit().id().fromAlias(alias)), value(orgUnit.getId())));
        }

        // с привязанным упв
        builder.joinEntity(alias, DQLJoinType.inner, EppStudent2EduPlanVersion.class, "s2epv",
            and(
                eq(property(EppStudent2EduPlanVersion.student().fromAlias("s2epv")), property(alias)),
                isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv")))));

        // у которых на текущем курсе есть в сетке такая часть года
        builder.where(exists(
            new DQLSelectBuilder().fromEntity(DevelopGridTerm.class, "t").column("t.id")
            .where(eq(property(DevelopGridTerm.course().fromAlias("t")), property(Student.course().fromAlias(alias))))
            .where(eq(property(DevelopGridTerm.developGrid().fromAlias("t")), property(EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().developGrid().fromAlias("s2epv"))))
            .where(eq(property(DevelopGridTerm.part().fromAlias("t")), commonValue(yearPart)))
            .buildQuery()));

        // и нет МСРП на эту часть года в текущем учебном году
        builder.where(notExists(
            new DQLSelectBuilder().fromEntity(EppStudentWpePart.class, "wpds").column(property("wpds.id"))
            .where(eq(property(EppStudentWpePart.studentWpe().student().fromAlias("wpds")), property(alias)))
            .where(isNull(property(EppStudentWpePart.removalDate().fromAlias("wpds"))))
            .where(eq(property(EppStudentWpePart.studentWpe().year().educationYear().current().fromAlias("wpds")), value(Boolean.TRUE)))
            .where(eq(property(EppStudentWpePart.studentWpe().part().fromAlias("wpds")), value(yearPart)))
            .buildQuery()
        ));
    }
}
