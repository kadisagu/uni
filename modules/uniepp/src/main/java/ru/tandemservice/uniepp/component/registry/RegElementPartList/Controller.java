package ru.tandemservice.uniepp.component.registry.RegElementPartList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;

import static org.tandemframework.shared.commonbase.events.CommonbaseImmutableNaturalIdListener.IMMUTABLE_NATURAL_ID_LOCKER;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        this.getDao().prepare(this.getModel(component));
    }

    public void onClickAddPart(final IBusinessComponent component)
    {
        this.getDao().addPart(this.getModel(component), component.getListenerParameter());
        component.refresh();
    }

    public void onClickDeleteRow(final IBusinessComponent component)
    {
        this.getDao().deleteRow(this.getModel(component), component.<Long>getListenerParameter());
        component.refresh();
    }

    public void onClickEditPart(final IBusinessComponent component)
    {
        final Long id = component.getListenerParameter();
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.registry.RegElementPartEdit.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, id)
        ));
    }

    public void onClickEditPartModule(final IBusinessComponent component)
    {
        final Long id = IUniBaseDao.instance.get().get(EppRegistryElementPartModule.class, component.<Long>getListenerParameter()).getModule().getId();
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.registry.ModuleRegistry.AddEdit.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, id)
        ));
    }

    public void onClickAddModule(final IBusinessComponent component)
    {
        final Long id = component.getListenerParameter();
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.uniepp.component.registry.RegElementPartAddModule.Model.class.getPackage().getName(),
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, id)
        ));
    }

    public void onClickEditModule(final IBusinessComponent component)
    {

    }

    public void onClickMoveUp(final IBusinessComponent component)
    {
        this.getDao().doMove(this.getModel(component), component.<Long>getListenerParameter(), -1);
        component.refresh();
    }

    public void onClickMoveDown(final IBusinessComponent component)
    {
        this.getDao().doMove(this.getModel(component), component.<Long>getListenerParameter(), 1);
        component.refresh();
    }

}
