package ru.tandemservice.uniepp.entity.workplan;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.hibsupport.HibSupportUtils;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanGen;

/**
 * Рабочий план
 */
public class EppWorkPlan extends EppWorkPlanGen implements ITitled, IEppStateObject, INumberObject
{
    public EppWorkPlan() {

    }

    public EppWorkPlan(final EppYearEducationProcess year, final EppEduPlanVersionBlock block, final Term term) {
        this.setYear(year);
        this.setParent(block);
        this.setTerm(term);
    }

    /** использовать этот конструктор только, если вы действительно уверены в том, что вы делаете */
    @SuppressWarnings("deprecation")
    public EppWorkPlan(final EppYearEducationProcess year, final EppEduPlanVersionBlock block, final DevelopGridTerm developGridTerm) {
        this(year, block, developGridTerm.getTerm());
        this.setCachedGridTerm(developGridTerm);
    }

    @Override
    public INumberGenerationRule<EppWorkPlan> getNumberGenerationRule() {
        return IEppWorkPlanDAO.instance.get().getWorkPlanNumberGenerationRule();
    }

    @Override
    public String getFullNumber() {
        return this.getNumber();
    }

    @Override public EppWorkPlan getWorkPlan() {
        return this;
    }

    @EntityDSLSupport(parts={EppWorkPlanGen.P_NUMBER})
    @Override public String getTitle() {
        if (getTerm() == null) {
            return this.getClass().getSimpleName();
        }
        return super.getTitleBase();
    }

    @Override public EppEduPlanVersionBlock getBlock() {
        return this.getParent();
    }

    @Override public IHierarchyItem getHierarhyParent() {
        return this.getParent();
    }

    @SuppressWarnings("deprecation")
    @Override public DevelopGridTerm getGridTerm() {
        return this.getCachedGridTerm();
    }

    /** @deprecated метод сделан исключительно для поиска в базе */
    @Deprecated
    @Override
    public DevelopGridTerm getCachedGridTerm() {
        final DevelopGridTerm v = super.getCachedGridTerm();
        if (null != v) { return v; }

        // этот код по идее должен выполняться только в момент сохранения объекта в базу
        try {
            // в сущность значение сохранять нельзя
            // здесь используется КЭШ сессии
            final EppCustomEduPlan customEduPlan = EppWorkPlan.this.getCustomEduPlan();
            final DevelopGrid grid = customEduPlan != null ? customEduPlan.getDevelopGrid() : EppWorkPlan.this.getEduPlan().getDevelopGrid();
            final Term term = EppWorkPlan.this.getTerm();
            return UniDaoFacade.getCoreDao().getCalculatedValue(session -> HibSupportUtils.getByNaturalId(session, new DevelopGridTerm.NaturalId(grid, term)));
        } catch (final Exception t) {
            // неосили в силу ряда причин
            return null;
        }
    }

    /** @deprecated метод сделан исключительно для поиска в базе */
    @Deprecated
    @Override
    public void setCachedGridTerm(final DevelopGridTerm cachedGridTerm) {
        super.setCachedGridTerm(cachedGridTerm);
    }
}
