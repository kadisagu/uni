/* $Id$ */
package ru.tandemservice.uniepp.migration;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * @author azhebko
 * @since 29.09.2014
 */
public class MS_uniepp_2x6x7_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.7")
		};
    }

    @Override
    public ScriptDependency[] getBeforeDependencies()
    {
        return new ScriptDependency[] { new ScriptDependency("uniedu", "2.6.7", 2) };
    }

    @SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();
        stmt.execute("select code_p from edu_specialization_base_t ");
        ResultSet rs = stmt.getResultSet();

        Collection<String> codes = new ArrayList<>();
        while (rs.next())
            codes.add(rs.getString(1));

        codes = CollectionUtils.select(codes, NumberUtils::isDigits);
        Integer max = null;
        if (!codes.isEmpty())
            max = Collections.max(CollectionUtils.collect(codes, Integer::parseInt));

        if (max == null)
            max = 0;

        Statement statement = tool.getConnection().createStatement();
        statement.execute("" +
            "select bs.id, sb.title_p, sb.shorttitle_p, pp.programsubject_id, sc.id " +
            "from epp_eduplan_verblock_s_t bs " +
            "inner join epp_eduplan_verblock_t b on b.id = bs.id " +
            "inner join epp_eduplan_ver_t v on v.id = b.eduplanversion_id " +
            "inner join edu_specialization_base_t sb on sb.id = bs.programspecialization_id " +
            "left join edu_specialization_child_t sc on sc.id = sb.id " +
            "inner join epp_eduplan_prof_t pp on pp.id = v.eduplan_id " +
            "where pp.programsubject_id <> sb.programsubject_id ");

        PreparedStatement specBasePreparedStatement = tool.prepareStatement("insert into edu_specialization_base_t (id, discriminator, code_p, title_p, shorttitle_p, programsubject_id) values (?, ?, ?, ?, ?, ?)");
        PreparedStatement specRootPreparedStatement = tool.prepareStatement("insert into edu_specialization_root_t (id) values (?)");
        PreparedStatement specChildPreparedStatement = tool.prepareStatement("insert into edu_specialization_child_t (id) values (?)");

        PreparedStatement blockSpecPreparedStatement = tool.prepareStatement("update epp_eduplan_verblock_s_t set programspecialization_id = ? where id = ?");

        Short rootEntityCode = tool.entityCodes().ensure("eduProgramSpecializationRoot");   // EduProgramSpecializationRoot.ENTITY_NAME
        Short childEntityCode = tool.entityCodes().ensure("eduProgramSpecializationChild"); // EduProgramSpecializationChild.ENTITY_NAME

        ResultSet resultSet = statement.getResultSet();
        while (resultSet.next())
        {
            Long blockId = resultSet.getLong(1);
            String title = resultSet.getString(2);
            String shortTitle = resultSet.getString(3);
            Long subjectId = resultSet.getLong(4);
            Number specializationChildId = (Number) resultSet.getObject(5);

            if (specializationChildId == null)
            {
                // общая направленность, такая уже может быть
                PreparedStatement rootStatement = tool.prepareStatement("" +
                    "select b.id from edu_specialization_base_t b " +
                    "inner join edu_specialization_root_t r on r.id = b.id " +
                    "where b.programsubject_id = ?", subjectId);

                rootStatement.execute();
                ResultSet rResultSet = rootStatement.getResultSet();
                Long rootId = rResultSet.next() ? rResultSet.getLong(1) : null;

                if (rootId == null)
                {
                    // добавляем новый элемент справочника
                    rootId = EntityIDGenerator.generateNewId(rootEntityCode);
                    specBasePreparedStatement.setLong(1, rootId);
                    specBasePreparedStatement.setShort(2, rootEntityCode);
                    specBasePreparedStatement.setString(3, String.valueOf(++max));
                    specBasePreparedStatement.setString(4, title);
                    specBasePreparedStatement.setString(5, shortTitle);
                    specBasePreparedStatement.setLong(6, subjectId);
                    specBasePreparedStatement.execute();

                    specRootPreparedStatement.setLong(1, rootId);
                    specRootPreparedStatement.execute();
                }

                blockSpecPreparedStatement.setLong(1, rootId);
                blockSpecPreparedStatement.setLong(2, blockId);
                blockSpecPreparedStatement.execute();

            } else
            {
                // направленность в рамках направления, ищем среди направленностей данного направления с таким же именем
                PreparedStatement childStatement = tool.prepareStatement("" +
                    "select b.id from edu_specialization_base_t b " +
                    "inner join edu_specialization_child_t c on c.id = b.id " +
                    "where b.programsubject_id = ? and b.title_p = ?", subjectId, title);

                childStatement.execute();
                ResultSet cResultSet = childStatement.getResultSet();
                Long childId = cResultSet.next() ? cResultSet.getLong(1) : null;

                if (childId == null)
                {
                    // создаем новый элемент
                    childId = EntityIDGenerator.generateNewId(childEntityCode);
                    specBasePreparedStatement.setLong(1, childId);
                    specBasePreparedStatement.setShort(2, childEntityCode);
                    specBasePreparedStatement.setString(3, String.valueOf(++max));
                    specBasePreparedStatement.setString(4, title);
                    specBasePreparedStatement.setString(5, shortTitle);
                    specBasePreparedStatement.setLong(6, subjectId);
                    specBasePreparedStatement.execute();

                    specChildPreparedStatement.setLong(1, childId);
                    specChildPreparedStatement.execute();
                }

                blockSpecPreparedStatement.setLong(1, childId);
                blockSpecPreparedStatement.setLong(2, blockId);
                blockSpecPreparedStatement.execute();
            }
        }
    }
}