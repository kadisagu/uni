package ru.tandemservice.uniepp.component.eduplan.EduPlanVersionMoveBlockRow;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author nkokorina
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onClickApply(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().update(model);
        this.deactivate(component);
    }
}
