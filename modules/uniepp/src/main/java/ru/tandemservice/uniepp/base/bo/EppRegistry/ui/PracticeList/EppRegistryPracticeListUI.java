/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.PracticeList;

import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractListUI;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;

/**
 * @author Irina Ugfeld
 * @since 10.03.2016
 */
public class EppRegistryPracticeListUI extends EppRegistryAbstractListUI {

    @Override public String getMenuPermission() { return "menuEppPracticeRegistry"; }
    @Override public String getPermissionPrefix() { return "eppPractice_list"; }
    @Override public Class getElementClass() { return EppRegistryPractice.class; }
}