/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppEduProgramSubjectDevelopResult.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.uniepp.catalog.bo.EppProfActivityType.ui.List.EppProfActivityTypeList;
import ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask.ui.List.EppProfessionalTaskList;
import ru.tandemservice.uniepp.catalog.bo.EppSkill.ui.List.EppSkillList;

/**
 * @author Igor Belanov
 * @since 03.03.2017
 */
@Configuration
public class EppEduProgramSubjectDevelopResultTab extends BusinessComponentManager
{
    public static final String TAB_PANEL = "eduProgramSubjectDevelopResultTabPanel";
    public static final String TAB_PANEL_REGION_NAME = "eppEduProgramSubjectDevelopResultRegion";

    public static final String EPP_PROF_ACTIVITY_TYPE_TAB = "eppProfActivityTypeTab";
    public static final String EPP_PROFESSIONAL_TASK_TAB = "eppProfessionalTaskTab";
    public static final String EPP_SKILL_TAB = "eppSkillTab";

    @Bean
    public TabPanelExtPoint eduProgramSubjectDevelopResultTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(EPP_PROF_ACTIVITY_TYPE_TAB, EppProfActivityTypeList.class)
                        .parameters("ui:profActivityTypeTabParams"))
                .addTab(componentTab(EPP_PROFESSIONAL_TASK_TAB, EppProfessionalTaskList.class)
                        .parameters("ui:professionalTaskTabParams"))
                .addTab(componentTab(EPP_SKILL_TAB, EppSkillList.class)
                        .parameters("ui:skillTabParams"))
                .create();
    }
}
