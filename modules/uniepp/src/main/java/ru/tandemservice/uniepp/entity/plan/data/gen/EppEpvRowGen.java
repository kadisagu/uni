package ru.tandemservice.uniepp.entity.plan.data.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка УП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.data.EppEpvRow";
    public static final String ENTITY_NAME = "eppEpvRow";
    public static final int VERSION_HASH = 2024374357;
    private static IEntityMeta ENTITY_META;

    public static final String L_OWNER = "owner";
    public static final String P_STORED_INDEX = "storedIndex";
    public static final String P_USER_INDEX = "userIndex";
    public static final String P_EXCLUDED_FROM_LOAD = "excludedFromLoad";
    public static final String P_EXCLUDED_FROM_ACTIONS = "excludedFromActions";
    public static final String P_DISPLAYABLE_TITLE = "displayableTitle";

    private EppEduPlanVersionBlock _owner;     // Блок
    private String _storedIndex;     // Индекс (сохраненный)
    private String _userIndex;     // Индекс (введенный пользователем)
    private boolean _excludedFromLoad = false;     // Исключить из нагрузки
    private boolean _excludedFromActions = false;     // Исключить из мероприятий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlock getOwner()
    {
        return _owner;
    }

    /**
     * @param owner Блок. Свойство не может быть null.
     */
    public void setOwner(EppEduPlanVersionBlock owner)
    {
        dirty(_owner, owner);
        _owner = owner;
    }

    /**
     * Индекс строки (всегда присутствует в базе).
     *
     * @return Индекс (сохраненный). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStoredIndex()
    {
        return _storedIndex;
    }

    /**
     * @param storedIndex Индекс (сохраненный). Свойство не может быть null.
     */
    public void setStoredIndex(String storedIndex)
    {
        dirty(_storedIndex, storedIndex);
        _storedIndex = storedIndex;
    }

    /**
     * Индекс, введенный пользователем (null - индекс не введен пользователем).
     *
     * @return Индекс (введенный пользователем).
     */
    @Length(max=255)
    public String getUserIndex()
    {
        return _userIndex;
    }

    /**
     * @param userIndex Индекс (введенный пользователем).
     */
    public void setUserIndex(String userIndex)
    {
        dirty(_userIndex, userIndex);
        _userIndex = userIndex;
    }

    /**
     * @return Исключить из нагрузки. Свойство не может быть null.
     */
    @NotNull
    public boolean isExcludedFromLoad()
    {
        return _excludedFromLoad;
    }

    /**
     * @param excludedFromLoad Исключить из нагрузки. Свойство не может быть null.
     */
    public void setExcludedFromLoad(boolean excludedFromLoad)
    {
        dirty(_excludedFromLoad, excludedFromLoad);
        _excludedFromLoad = excludedFromLoad;
    }

    /**
     * @return Исключить из мероприятий. Свойство не может быть null.
     */
    @NotNull
    public boolean isExcludedFromActions()
    {
        return _excludedFromActions;
    }

    /**
     * @param excludedFromActions Исключить из мероприятий. Свойство не может быть null.
     */
    public void setExcludedFromActions(boolean excludedFromActions)
    {
        dirty(_excludedFromActions, excludedFromActions);
        _excludedFromActions = excludedFromActions;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEpvRowGen)
        {
            setOwner(((EppEpvRow)another).getOwner());
            setStoredIndex(((EppEpvRow)another).getStoredIndex());
            setUserIndex(((EppEpvRow)another).getUserIndex());
            setExcludedFromLoad(((EppEpvRow)another).isExcludedFromLoad());
            setExcludedFromActions(((EppEpvRow)another).isExcludedFromActions());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvRow.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EppEpvRow is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "owner":
                    return obj.getOwner();
                case "storedIndex":
                    return obj.getStoredIndex();
                case "userIndex":
                    return obj.getUserIndex();
                case "excludedFromLoad":
                    return obj.isExcludedFromLoad();
                case "excludedFromActions":
                    return obj.isExcludedFromActions();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "owner":
                    obj.setOwner((EppEduPlanVersionBlock) value);
                    return;
                case "storedIndex":
                    obj.setStoredIndex((String) value);
                    return;
                case "userIndex":
                    obj.setUserIndex((String) value);
                    return;
                case "excludedFromLoad":
                    obj.setExcludedFromLoad((Boolean) value);
                    return;
                case "excludedFromActions":
                    obj.setExcludedFromActions((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "owner":
                        return true;
                case "storedIndex":
                        return true;
                case "userIndex":
                        return true;
                case "excludedFromLoad":
                        return true;
                case "excludedFromActions":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "owner":
                    return true;
                case "storedIndex":
                    return true;
                case "userIndex":
                    return true;
                case "excludedFromLoad":
                    return true;
                case "excludedFromActions":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "owner":
                    return EppEduPlanVersionBlock.class;
                case "storedIndex":
                    return String.class;
                case "userIndex":
                    return String.class;
                case "excludedFromLoad":
                    return Boolean.class;
                case "excludedFromActions":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvRow> _dslPath = new Path<EppEpvRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvRow");
    }
            

    /**
     * @return Блок. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRow#getOwner()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> owner()
    {
        return _dslPath.owner();
    }

    /**
     * Индекс строки (всегда присутствует в базе).
     *
     * @return Индекс (сохраненный). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRow#getStoredIndex()
     */
    public static PropertyPath<String> storedIndex()
    {
        return _dslPath.storedIndex();
    }

    /**
     * Индекс, введенный пользователем (null - индекс не введен пользователем).
     *
     * @return Индекс (введенный пользователем).
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRow#getUserIndex()
     */
    public static PropertyPath<String> userIndex()
    {
        return _dslPath.userIndex();
    }

    /**
     * @return Исключить из нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRow#isExcludedFromLoad()
     */
    public static PropertyPath<Boolean> excludedFromLoad()
    {
        return _dslPath.excludedFromLoad();
    }

    /**
     * @return Исключить из мероприятий. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRow#isExcludedFromActions()
     */
    public static PropertyPath<Boolean> excludedFromActions()
    {
        return _dslPath.excludedFromActions();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRow#getDisplayableTitle()
     */
    public static SupportedPropertyPath<String> displayableTitle()
    {
        return _dslPath.displayableTitle();
    }

    public static class Path<E extends EppEpvRow> extends EntityPath<E>
    {
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _owner;
        private PropertyPath<String> _storedIndex;
        private PropertyPath<String> _userIndex;
        private PropertyPath<Boolean> _excludedFromLoad;
        private PropertyPath<Boolean> _excludedFromActions;
        private SupportedPropertyPath<String> _displayableTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRow#getOwner()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> owner()
        {
            if(_owner == null )
                _owner = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_OWNER, this);
            return _owner;
        }

    /**
     * Индекс строки (всегда присутствует в базе).
     *
     * @return Индекс (сохраненный). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRow#getStoredIndex()
     */
        public PropertyPath<String> storedIndex()
        {
            if(_storedIndex == null )
                _storedIndex = new PropertyPath<String>(EppEpvRowGen.P_STORED_INDEX, this);
            return _storedIndex;
        }

    /**
     * Индекс, введенный пользователем (null - индекс не введен пользователем).
     *
     * @return Индекс (введенный пользователем).
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRow#getUserIndex()
     */
        public PropertyPath<String> userIndex()
        {
            if(_userIndex == null )
                _userIndex = new PropertyPath<String>(EppEpvRowGen.P_USER_INDEX, this);
            return _userIndex;
        }

    /**
     * @return Исключить из нагрузки. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRow#isExcludedFromLoad()
     */
        public PropertyPath<Boolean> excludedFromLoad()
        {
            if(_excludedFromLoad == null )
                _excludedFromLoad = new PropertyPath<Boolean>(EppEpvRowGen.P_EXCLUDED_FROM_LOAD, this);
            return _excludedFromLoad;
        }

    /**
     * @return Исключить из мероприятий. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRow#isExcludedFromActions()
     */
        public PropertyPath<Boolean> excludedFromActions()
        {
            if(_excludedFromActions == null )
                _excludedFromActions = new PropertyPath<Boolean>(EppEpvRowGen.P_EXCLUDED_FROM_ACTIONS, this);
            return _excludedFromActions;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniepp.entity.plan.data.EppEpvRow#getDisplayableTitle()
     */
        public SupportedPropertyPath<String> displayableTitle()
        {
            if(_displayableTitle == null )
                _displayableTitle = new SupportedPropertyPath<String>(EppEpvRowGen.P_DISPLAYABLE_TITLE, this);
            return _displayableTitle;
        }

        public Class getEntityClass()
        {
            return EppEpvRow.class;
        }

        public String getEntityName()
        {
            return "eppEpvRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getDisplayableTitle();
}
