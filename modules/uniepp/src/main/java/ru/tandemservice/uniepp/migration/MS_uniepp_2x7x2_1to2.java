package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniepp_2x7x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppIControlActionType

		// создано обязательное свойство defaultGroupType
		{
			// создать колонку
			tool.createColumn("epp_c_cactiontype_i_t", new DBColumn("defaultgrouptype_id", DBType.LONG));

			// задать значение по умолчанию

            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("select aload.eppgrouptype_id, eicat.id from epp_c_cactiontype_i_t eicat inner join epp_c_loadtype_a_t aload on aload.id = eicat.defaultaloadtype_id");
            PreparedStatement update = tool.prepareStatement("update epp_c_cactiontype_i_t set defaultgrouptype_id = ? where id = ?");

            ResultSet src = stmt.getResultSet();

            while (src.next())
            {
                Long groupTypeId = src.getLong(1);
                Long id = src.getLong(2);

                update.setLong(1, groupTypeId);
                update.setLong(2, id);
                update.executeUpdate();
            }

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_c_cactiontype_i_t", "defaultgrouptype_id", false);
		}

        // удалено свойство defaultALoadType
        {
            // удалить колонку
            tool.dropColumn("epp_c_cactiontype_i_t", "defaultaloadtype_id");
        }
    }
}