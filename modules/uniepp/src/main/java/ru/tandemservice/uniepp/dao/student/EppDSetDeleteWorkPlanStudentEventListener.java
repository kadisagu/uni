package ru.tandemservice.uniepp.dao.student;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;

import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

/**
 * @author vdanilov
 */
public class EppDSetDeleteWorkPlanStudentEventListener implements IDSetEventListener
{
    public void init()
    {
        // при удалении РУП - удалять неактивные связи со студентами
        DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, EppWorkPlanBase.class, this);
    }

    @Override
    public void onEvent(final DSetEvent event)
    {
        new DQLDeleteBuilder(EppStudent2WorkPlan.class)
        .where(isNotNull(property(EppStudent2WorkPlan.P_REMOVAL_DATE)))
        .where(in(property(EppStudent2WorkPlan.L_WORK_PLAN), event.getMultitude().getInExpression()))
        .createStatement(event.getContext()).execute();
    }

}
