/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithoutEduPlanBlock;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.AbstractEppIndicatorStudentListPresenter;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/15/14
 */
public class EppIndicatorStudentsWithoutEduPlanBlockSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public EppIndicatorStudentsWithoutEduPlanBlockSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);

        // только неархивные студенты
        builder.where(eq(property(Student.archival().fromAlias(alias)), value(false)));

        // этого деканата
        final OrgUnit orgUnit = context.get(AbstractEppIndicatorStudentListPresenter.PROP_ORG_UNIT);
        if (null != orgUnit && null != orgUnit.getId()) {
            builder.where(eq(property(Student.educationOrgUnit().groupOrgUnit().id().fromAlias(alias)), value(orgUnit.getId())));
        }

        // в актуальной связи нет блока
        builder.where(exists(
            new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "s2epv").column(property("s2epv.id"))
                .where(eq(property(EppStudent2EduPlanVersion.student().fromAlias("s2epv")), property(alias)))
                .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
                .where(isNull(property(EppStudent2EduPlanVersion.block().fromAlias("s2epv"))))
                .buildQuery()
        ));
    }
}

