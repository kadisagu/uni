package ru.tandemservice.uniepp.component.orgunit.EppEpvAcceptionTab;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.functors.NotNullPredicate;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.Collection;

/**
 * @author zhuj
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final String settingsKey = ("epp."+component.getComponentMeta().getShortName()+"."+model.getOrgUnitId());
        model.setSettings(UniBaseUtils.getDataSettings(component, settingsKey));
        this.getDao().prepare(model);
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        onRefreshComponent(component);
    }

    public void onClickClear(final IBusinessComponent component)
    {
        this.getModel(component).getSettings().clear();
        this.onClickSearch(component);
    }

    protected Collection<Long> getSelectedIds(final Model model) {
        final Collection<Long> selectedIds = CollectionUtils.select(
            CollectionUtils.collect(model.getData().getRows(), input -> (input.isSelected() ? input.getId() : null)),
            NotNullPredicate.getInstance()
        );

        if (selectedIds.isEmpty()) {
            throw new ApplicationException("Не выбрано ни одной строки УП(в).");
        }
        return selectedIds;
    }

    public void onClickAttachRegistryElement(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final Collection<Long> selectedIds = getSelectedIds(model);

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            ru.tandemservice.uniepp.component.eduplan.row.AttachRegistryElement.Model.COMPONENT_NAME,
            new ParametersMap().add("ids", selectedIds).add("orgUnitId", model.getOrgUnitId())
        ));
    }
}
