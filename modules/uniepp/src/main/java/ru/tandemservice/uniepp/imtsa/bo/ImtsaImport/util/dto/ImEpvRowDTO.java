/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * @author azhebko
 * @since 28.10.2014
 */
public abstract class ImEpvRowDTO
{
    private String _storedIndex;     // Индекс (сохраненный)
    private String _userIndex;     // Индекс (введенный пользователем)
    private String _title;     // Название
    private boolean _excludedFromLoad = false;     // Исключить из нагрузки
    private boolean _excludedFromActions = false;     // Исключить из мероприятий

    public String getStoredIndex(){ return _storedIndex; }
    public void setStoredIndex(String storedIndex){ _storedIndex = storedIndex; }

    public String getUserIndex(){ return _userIndex; }
    public void setUserIndex(String userIndex){ _userIndex = userIndex; }

    public String getTitle() { return _title; }
    public void setTitle(String title) { _title = title; }

    public boolean isExcludedFromLoad(){ return _excludedFromLoad; }
    public void setExcludedFromLoad(boolean excludedFromLoad){ _excludedFromLoad = excludedFromLoad; }

    public boolean isExcludedFromActions(){ return _excludedFromActions; }
    public void setExcludedFromActions(boolean excludedFromActions){ _excludedFromActions = excludedFromActions; }

    private final Collection<ImEpvRowDTO> _childListRW = new ArrayList<>();
    private final Collection<ImEpvRowDTO> _childListRO = Collections.unmodifiableCollection(_childListRW);

    /** Вложенные строки УП. */
    public Collection<ImEpvRowDTO> getChildList() { return _childListRO; }

    /** Добавляет вложенную строку УП. */
    public void addChild(ImEpvRowDTO child)
    {
        _childListRW.add(child);
    }

    @Override
    public String toString()
    {
        return getStoredIndex() + ", " + getUserIndex() + ", " + isExcludedFromLoad() + ", " + isExcludedFromActions();
    }
}