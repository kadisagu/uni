package ru.tandemservice.uniepp.component.edugroup.pub.GroupContent;

import java.util.Collection;
import java.util.Map;

import org.tandemframework.core.entity.dsl.PropertyPath;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {

    void prepareDataSource(Model model);

    Map<Object, Collection<Long>> getRowDistributionMap(Model model, Collection<Long> ids, PropertyPath[] key);

}
