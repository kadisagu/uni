package ru.tandemservice.uniepp.entity.student.slot;

import ru.tandemservice.uniepp.entity.student.slot.gen.*;

import java.util.Date;

/** @see ru.tandemservice.uniepp.entity.student.slot.gen.EppStudentWpePartGen */
public abstract class EppStudentWpePart extends EppStudentWpePartGen
{
    public void wakeup(final Date modificationDate) {
        this.setModificationDate(modificationDate);
        this.setRemovalDate(null);
    }

    public void suspend(final Date removalDate) {
        this.setModificationDate(removalDate);
        this.setRemovalDate(removalDate);
    }

    public abstract String getRegistryElementTitle();

    /**
     * @deprecated use getStudentWpe
     */
    public EppStudentWorkPlanElement getSlot()
    {
        return getStudentWpe();
    }
}