/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphList;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.util.IYearEducationProcessSelectModel;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
public class Model implements IYearEducationProcessSelectModel
{
    private IDataSettings _settings;
    private List<EppYearEducationProcess> _yearEducationProcessList;
    private DynamicListDataSource<EppWorkGraph> _dataSource;
    private ISelectModel _stateListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _developTechListModel;
    private ISelectModel _developGridListModel;

    // IYearEducationProcessSelectModel

    @Override
    public EppYearEducationProcess getYearEducationProcess()
    {
        return (EppYearEducationProcess) this._settings.get(IYearEducationProcessSelectModel.YEAR_EDUCATION_PROCESS_FILTER_NAME);
    }

    @Override
    public void setYearEducationProcess(final EppYearEducationProcess yearEducationProcess)
    {
        this._settings.set(IYearEducationProcessSelectModel.YEAR_EDUCATION_PROCESS_FILTER_NAME, yearEducationProcess);
    }

    // Getters & Setters

    @Override
    public IDataSettings getSettings()
    {
        return this._settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this._settings = settings;
    }

    @Override
    public List<EppYearEducationProcess> getYearEducationProcessList()
    {
        return this._yearEducationProcessList;
    }

    @Override
    public void setYearEducationProcessList(final List<EppYearEducationProcess> yearEducationProcessList)
    {
        this._yearEducationProcessList = yearEducationProcessList;
    }

    public DynamicListDataSource<EppWorkGraph> getDataSource()
    {
        return this._dataSource;
    }

    public void setDataSource(final DynamicListDataSource<EppWorkGraph> dataSource)
    {
        this._dataSource = dataSource;
    }

    public ISelectModel getStateListModel()
    {
        return this._stateListModel;
    }

    public void setStateListModel(final ISelectModel stateListModel)
    {
        this._stateListModel = stateListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return this._developFormListModel;
    }

    public void setDevelopFormListModel(final ISelectModel developFormListModel)
    {
        this._developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return this._developConditionListModel;
    }

    public void setDevelopConditionListModel(final ISelectModel developConditionListModel)
    {
        this._developConditionListModel = developConditionListModel;
    }

    public ISelectModel getDevelopTechListModel()
    {
        return this._developTechListModel;
    }

    public void setDevelopTechListModel(final ISelectModel developTechListModel)
    {
        this._developTechListModel = developTechListModel;
    }

    public ISelectModel getDevelopGridListModel()
    {
        return this._developGridListModel;
    }

    public void setDevelopGridListModel(final ISelectModel developGridListModel)
    {
        this._developGridListModel = developGridListModel;
    }
}
