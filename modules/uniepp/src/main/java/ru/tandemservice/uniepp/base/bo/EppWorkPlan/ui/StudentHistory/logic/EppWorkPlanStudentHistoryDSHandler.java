/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.StudentHistory.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 10/8/14
 */
public class EppWorkPlanStudentHistoryDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String STUDENT_ID = "studentId";

    public EppWorkPlanStudentHistoryDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long studentId = context.getNotNull(STUDENT_ID);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "sepv");
        builder.joinEntity("sepv", DQLJoinType.left, EppStudent2WorkPlan.class, "swp", eq(property("swp", EppStudent2WorkPlan.studentEduPlanVersion()), property("sepv")));
        builder.fetchPath(DQLJoinType.left, EppStudent2EduPlanVersion.eduPlanVersion().fromAlias("sepv"), "epv", true);
        builder.fetchPath(DQLJoinType.left, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().fromAlias("sepv"), "ep", true);
        builder.fetchPath(DQLJoinType.left, EppStudent2EduPlanVersion.block().fromAlias("sepv"), "block", true);
        builder.fetchPath(DQLJoinType.left, EppStudent2WorkPlan.workPlan().fromAlias("swp"), "wp", true);
        builder.column("sepv");
        builder.column("swp");
        builder.where(eq(property("sepv", EppStudent2EduPlanVersion.student().id()), value(studentId)));
        builder.where(isNotNull(property("sepv", EppStudent2EduPlanVersion.removalDate())));

        List<Object[]> data = builder.createStatement(context.getSession()).list();

        List<EppWorkPlanStudentHistoryVO> resultList = Lists.newArrayList();
        Map<EppStudent2EduPlanVersion, List<EppWorkPlanStudentHistoryVO>> eduPlanVersionListMap = Maps.newHashMap();
        for(Object[] obj : data)
        {
            EppWorkPlanStudentHistoryVO historyVO = new EppWorkPlanStudentHistoryVO((EppStudent2EduPlanVersion) obj[0], (EppStudent2WorkPlan)obj[1]);
            SafeMap.safeGet(eduPlanVersionListMap, historyVO.getSepv(), ArrayList.class).add(historyVO);
        }

        List<EppStudent2EduPlanVersion> sepvList = Lists.newArrayList(eduPlanVersionListMap.keySet());
        Collections.sort(sepvList, (o1, o2) -> {
            if (o1.getRemovalDate().equals(o2.getRemovalDate()))
            {
                if(o1.getEduPlanVersion().getEduPlan().getNumber().equals(o2.getEduPlanVersion().getEduPlan().getNumber()))
                    return o1.getEduPlanVersion().getNumber().compareTo(o2.getEduPlanVersion().getNumber());

                return o1.getEduPlanVersion().getEduPlan().getNumber().compareTo(o2.getEduPlanVersion().getEduPlan().getNumber());
            }
            else return o1.getRemovalDate().compareTo(o2.getRemovalDate());
        });

        for(EppStudent2EduPlanVersion sepv : sepvList)
        {
            resultList.addAll(
                    eduPlanVersionListMap.get(sepv).stream()
                            .sorted((o1, o2) -> {
                                if (o1.getSwp().getRemovalDate().equals(o2.getSwp().getRemovalDate()))
                                    return o1.getSwp().getWorkPlan().getTitle().compareTo(o2.getSwp().getWorkPlan().getTitle());

                                return o1.getSwp().getRemovalDate().compareTo(o2.getSwp().getRemovalDate());
                            })
                            .collect(Collectors.toList())
            );
        }

        return ListOutputBuilder.get(input, resultList).pageable(false).build();
    }


}
