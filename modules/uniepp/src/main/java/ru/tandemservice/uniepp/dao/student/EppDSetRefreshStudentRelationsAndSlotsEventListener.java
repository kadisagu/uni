package ru.tandemservice.uniepp.dao.student;

import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;

import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;


/**
 * вызывает актуализацию связей (с РУП и УПв) при изменении их набора
 * 
 * @author vdanilov
 */
public class EppDSetRefreshStudentRelationsAndSlotsEventListener implements IDSetEventListener
{
    public void init()
    {
        // связи с УПВ
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppStudent2EduPlanVersion.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppStudent2EduPlanVersion.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, EppStudent2EduPlanVersion.class, this);

        // связи с РУП
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppStudent2WorkPlan.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EppStudent2WorkPlan.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, EppStudent2WorkPlan.class, this);

    }

    @Override public void onEvent(final DSetEvent event)
    {
        if (EppStudentSlotDAO.DAEMON.isCurrentThread()) {
            return; /* уже в демоне */
        }

        // при изменениях УПВ и РУП студентов, надо в этой же транзакции проводить актуализацию данных, затем менять МСРП демоном
        EppStudentSlotDAO.TRANSACTION_AFTER_COMPLETE_REFRESH_RELATIONS_AND_WAKEUP.register(event);
    }
}
