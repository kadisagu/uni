/* $Id$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.SelGroupFillForm;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Nikolay Fedorovskih
 * @since 16.02.2015
 */
@Configuration
public class EppEduPlanVersionSelGroupFillForm extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .create();
    }
}