package ru.tandemservice.uniepp.component.group.GroupEduPlanTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        model.setSettings(component.getSettings());

        if (model.getDataSource() != null)
        {
            return;
        }

        final DynamicListDataSource<Student> dataSource = UniBaseUtils.createDataSource(component, this.getDao());
        dataSource.addColumn(new CheckboxColumn("select", "").setRequired(true).setWidth(1));
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY).setClickable(false).setOrderable(false).setWidth(25));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", Student.educationOrgUnit().educationLevelHighSchool().displayableTitle().s()).setClickable(false).setOrderable(false).setWidth(25));
        dataSource.addColumn(new PublisherLinkColumn("Учебный план", "title", "eduPlanVersion").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Данные версии УП", "epvData").setClickable(false).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("Блок УП", "title", "eduPlanBlock").setOrderable(false));

        model.setDataSource(dataSource);
        this.getDao().prepare(model);
    }

    public void onClickMassChangeEduPlanVersion(final IBusinessComponent component)
    {
        final Collection<IEntity> selectedObject = ((CheckboxColumn)this.getModel(component).getDataSource().getColumn("select")).getSelectedObjects();

        final Set<Long> selectedIds = new HashSet<Long>(selectedObject.size());
        for (final IEntity entity : selectedObject)
        {
            final IEntityMeta meta = EntityRuntime.getMeta(entity.getId());
            if (null != meta)
            {
                if (Student.class.isAssignableFrom(meta.getEntityClass()))
                {
                    selectedIds.add(entity.getId());
                }
            }
        }

        if (selectedIds.isEmpty())
        {
            ContextLocal.getErrorCollector().add("Ни один студент не выбран.");
            return;
        }

        component.createDefaultChildRegion(new ComponentActivator(
                ru.tandemservice.uniepp.component.student.MassChangeEduPlanVersions.Model.class.getPackage().getName(),
                new ParametersMap().add("ids", new ArrayList<Long>(selectedIds))
        ));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.getSettings().set("status", null);
        component.getSettings().set("studentCustomStateCIs", null);
    }

}
