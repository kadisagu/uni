package ru.tandemservice.uniepp.entity.registry;

import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.registry.gen.*;

/** @see ru.tandemservice.uniepp.entity.registry.gen.EppEduGroupSplitBySexElementGen */
public class EppEduGroupSplitBySexElement extends EppEduGroupSplitBySexElementGen
{
    public EppEduGroupSplitBySexElement()
    {
    }

    public EppEduGroupSplitBySexElement(EppEduGroupSplitVariant splitVariant, Sex sex)
    {
        setSplitVariant(splitVariant);
        setSex(sex);
    }

    @Override
    public Long getSplitElementId()
    {
        return getSex().getId();
    }

    @Override
    public String getSplitElementShortTitle()
    {
        return getSex().getShortTitle();
    }

    @Override
    public String getSplitElementTitle()
    {
        return getSex().getTitle();
    }
}