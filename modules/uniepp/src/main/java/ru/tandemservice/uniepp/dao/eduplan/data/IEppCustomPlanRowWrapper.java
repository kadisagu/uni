/* $Id$ */
package ru.tandemservice.uniepp.dao.eduplan.data;

/**
 * Враппер строки индивидуального учебного плана (ИУП).
 * Строка ИУП - это семестр строки УПв измененный (перемещенный, перезачтенный или переаттестованный) или оставленный как есть.
 *
 * @author Nikolay Fedorovskih
 * @since 21.08.2015
 */
public interface IEppCustomPlanRowWrapper extends Comparable
{
    String REATTESTATION_DONE_TITLE = "Переаттестовано";
    String REEXAMINATION_DONE_TITLE = "Перезачтено";

    /** @return Новый номер семестра */
    int getNewTermNumber();

    /** Установка новго номера семестра */
    void setNewTermNumber(int newTermNumber);

    /** @return Номер семестра в исходном УП */
    int getSourceTermNumber();

    /** @return Исключена дисциплина из плана (токльо для факультативной дисциплины) */
    boolean isExcluded();

    /** Исключить дисциплину из плана (токльо для факультативной дисциплины) */
    void setExcluded(boolean excluded);

    /** @return Строка исходного УП */
    IEppEpvRowWrapper getSourceRow();

    /** @return Строковый идентификатор */
    String getKey();

    /** @return название строки УП. Если у строки несколько семестров, то добавить " ([порядковый номер семестра]/[кол-во семестров])". */
    String getTitle();

    /** @return нагрузка за семестр (из исходного УП) */
    default double getTotalLoad(String loadFullCode) {
        return getSourceRow().getTotalInTermLoad(getSourceTermNumber(), loadFullCode);
    }

    /** @return имеется ли контрольное мероприятие в семестре исходного УП */
    default boolean hasControlAction(String caFullCode) {
        return getSourceRow().getActionSize(getSourceTermNumber(), caFullCode) > 0;
    }

    /**
     * true  - переаттестация: необходимо пересдавать контрольные мероприятия.
     * false - перезачтение: изучать и сдавать ничего не нужно.
     * null  - обычное освоение: необходимо изучать дисциплину и сдавать контрольные мероприятия, как обычно.
     *
     * @return Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение)
     */
    Boolean getNeedRetake();

    /** @param needRetake Необходимость пересдачи (true - переаттестация; false - перезачтение; null - обычное освоение) */
    void setNeedRetake(Boolean needRetake);

    /** @return переаттестация */
    default boolean isReattestation() {
        return Boolean.TRUE.equals(getNeedRetake());
    }

    /** @return перезачтение */
    default boolean isReexamination() {
        return Boolean.FALSE.equals(getNeedRetake());
    }

    /** @return Название того, что со строкой сделали: "Переаттестовано" или "Перезачтено" */
    default String getCustomActionTitle() {
        if (isReattestation()) {
            return REATTESTATION_DONE_TITLE;
        } else if (isReexamination()) {
            return REEXAMINATION_DONE_TITLE;
        } else {
            return "";
        }
    }
}