package ru.tandemservice.uniepp.dao.eduplan;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multiset;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.DaoCacheFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.plan.data.*;
import ru.tandemservice.uniepp.entity.settings.EppELoadWeekType;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EppEpvBlockWrapperLoader extends DaoCacheFacade.CacheEntryDefinition<Long, IEppEpvBlockWrapper>{

    // при построении дерева все циклические строки плана попадают в корень
    //private static final boolean forceUnrollRecursion = false;


    private final Session session;
    private final boolean loadDetails;

    private final Map<Long, String> id2fullCodeMap = new HashMap<>(16, 0.5f);
    private final Map<Long, Integer> id2termNumberMap = DevelopGridDAO.getIdToTermNumberMap();

    public EppEpvBlockWrapperLoader(final Session session, final boolean loadDetails) {
        super("eppEduPlanVersionWrapperLoader", 128, true);
        this.session = session;
        this.loadDetails = loadDetails;

        Debug.begin("EppEpvBlockWrapperLoader.preload");
        try {
            for (EppLoadType tp: EppEduPlanVersionDataDAO.getLoadTypeMap().values()) {
                id2fullCodeMap.put(tp.getId(), tp.getFullCode());
            }
            for (EppControlActionType tp: EppEduPlanVersionDataDAO.getActionTypeMap().values()) {
                id2fullCodeMap.put(tp.getId(), tp.getFullCode());
            }
        } finally {
            Debug.end();
        }
    }

    @Override
    public void fill(Map<Long, IEppEpvBlockWrapper> cache, Collection<Long> epvBlockIds) {
        Debug.begin("EppEpvBlockWrapperLoader.fill("+epvBlockIds.size()+")");
        try {
            final Map<Long, EppEpvBlockWrapper> result = new HashMap<>(epvBlockIds.size());
            for (Long id: epvBlockIds) {
                result.put(id, buildEpvBlockWrapper(id));
            }
            cache.putAll(result);
        } finally {
            Debug.end();
        }
    }

    /**
     * создает новую оболочку версии УП (метод для переопределения)
     * @param epvTermSizeMap - размер семестров версии УП (из УГ)
     * @return новая оболочка для версии УП
     */
    protected EppEpvBlockWrapper newEppEpvBlockWrapper(final EppEduPlanVersionRootBlock rootBlock, final EppEduPlanVersionBlock block, final Map<Integer, Multiset<String>> epvTermSizeMap) {
        return new EppEpvBlockWrapper(rootBlock, block, epvTermSizeMap);
    }

    /**
     * создает новую оболочку строки версии УП (метод для переопределения)
     * @param versionWrapper - оболочка версии УП (в рамках которой живет строка)
     * @param parentRowWrapper - оболочка родительской строки
     * @param row -текущая строка
     * @return новая оболочка строки версии УП
     */
    protected EppEpvRowWrapper newEppEpvRowWrapper(final EppEpvBlockWrapper versionWrapper, final EppEpvRowWrapper parentRowWrapper, final EppEpvRow row) {
        return new EppEpvRowWrapper(versionWrapper, parentRowWrapper, row);
    }


    /* @return { term.intValue -> { eLoadType.full-code -> amount }} */
    private Map<Integer, Multiset<String>> getEpvTermSizeMap(final Long epvId) {
        Debug.begin("getEpvTermSizeMap("+epvId+")");
        try {
            Map<Long, Integer> termIdMap = DevelopGridDAO.getIdToTermNumberMap();

            /* { term.intValue -> { weekType.code -> amount }} */
            final Map<Integer, Multiset<String>> resultMap = new HashMap<>();
            {
                final SafeMap.Callback<Integer, Multiset<String>> callback = key -> HashMultiset.create();
                final DQLSelectBuilder sel = new DQLSelectBuilder();
                sel.fromEntity(EppEduPlanVersionWeekType.class, "rel");
                sel.where(eq(property("rel", EppEduPlanVersionWeekType.eduPlanVersion().id()), value(epvId)));
                sel./*0*/column(property("rel", EppEduPlanVersionWeekType.term().id()));
                sel./*1*/column(property("rel", EppEduPlanVersionWeekType.weekType().code()));
                sel./*2*/column(DQLFunctions.countStar());
                sel.group(property("rel", EppEduPlanVersionWeekType.term().id()));
                sel.group(property("rel", EppEduPlanVersionWeekType.weekType().code()));

                sel.createStatement(this.session).<Object[]>list().forEach(row -> {
                    final Integer termNumber = termIdMap.get((Long) row[0]);
                    final String weekTypeCode = (String) row[1];
                    final int relationCount = ((Number) row[2]).intValue();

                    final Multiset<String> epvTermItem = SafeMap.safeGet(resultMap, termNumber, callback);
                    epvTermItem.add(weekTypeCode, relationCount);
                });
            }

            /* { weekType.code -> { eLoadType.code } } */
            final Map<String, List<String>> epvConfigMap = new HashMap<>();
            {
                final SafeMap.Callback<String, List<String>> callback = key -> new ArrayList<>();
                final DQLSelectBuilder sel = new DQLSelectBuilder();
                sel.fromEntity(EppEduPlanVersion.class, "epv");
                sel.fromEntity(EppELoadWeekType.class, "cfg");
                sel.where(eq(property("epv", EppEduPlanVersion.eduPlan().programForm()), property("cfg", EppELoadWeekType.developForm().programForm())));
                sel.where(eq(property("epv.id"), value(epvId)));
                sel./*0*/column(property("cfg", EppELoadWeekType.weekType().code()));
                sel./*1*/column(property("cfg", EppELoadWeekType.loadType().code()));

                sel.createStatement(this.session).<Object[]>list().forEach(row -> {
                    final String weekTypeCode = (String) row[0];
                    final String eLoadTypeFullCode = EppLoadType.getFullCode(EppELoadType.CATALOG_CODE, (String) row[1]);

                    final Collection<String> epvWeekTypeELoadFullCodeList = SafeMap.safeGet(epvConfigMap, weekTypeCode, callback);
                    epvWeekTypeELoadFullCodeList.add(eLoadTypeFullCode);
                });
            }


            /* convert { term.intValue -> { weekType.code -> amount }}
             * to { term.intValue -> { eLoadType.code -> amount }} */

            for (final Map.Entry<Integer, Multiset<String>> epvTermEntry: resultMap.entrySet()) {
                final Multiset<String> result = HashMultiset.create(2);

                for (final Multiset.Entry<String> e: epvTermEntry.getValue().entrySet()) {
                    final Collection<String> epvWeekTypeELoadFullCodeList = epvConfigMap.get(e.getElement());
                    if (null != epvWeekTypeELoadFullCodeList) {
                        final int weeks = e.getCount();
                        for (final String eLoadTypeFullCode: epvWeekTypeELoadFullCodeList) {
                            result.add(eLoadTypeFullCode, weeks);
                        }
                        result.add(IEppEpvBlockWrapper.ELOAD_FULL_CODE_ALL, weeks);
                    }
                }

                epvTermEntry.setValue(result);
            }

            return resultMap;

        } finally {
            Debug.end();
        }
    }


    /*
     * иерархически собирает строки УП
     */
    private void collectHData(final EppEpvBlockWrapper versionWrapper, final EppEpvRowWrapper parentRowWrapper, final List<EppEpvRow> eduPlanRows)
    {
        if (eduPlanRows.isEmpty()) { return; }

        // переносим элементы из исходного списка в список выбранных
        final List<EppEpvRow> selectedRows = new ArrayList<>();
        {
            final IEppEpvRow parentRow = null == parentRowWrapper ? null : parentRowWrapper.getRow();
            for (final Iterator<EppEpvRow> it = eduPlanRows.iterator(); it.hasNext();)
            {
                final EppEpvRow row = it.next();
                final boolean isSameParent = (null == parentRow ? (null == row.getHierarhyParent()) : parentRow.equals(row.getHierarhyParent()));
                if (isSameParent) {
                    it.remove();
                    selectedRows.add(row);
                }
            }
        }

        // все, что нашли - сортируем и обрабатываем
        if (selectedRows.size() > 0)
        {
            final Map<Long, IEppEpvRowWrapper> map = versionWrapper.getRowMap();
            Collections.sort(selectedRows, EppEpvRow.IN_SAME_PARENT_ROW_COMPARATOR);
            for (final EppEpvRow row : selectedRows)
            {
                final EppEpvRowWrapper rowWrapper = this.newEppEpvRowWrapper(versionWrapper, parentRowWrapper, row);
                if (null != map.put(rowWrapper.getId(), rowWrapper)) {
                    throw new IllegalStateException("Recursion detected");
                }
                if (null != parentRowWrapper) {
                    parentRowWrapper.getChildRows().add(rowWrapper);
                }
                this.collectHData(versionWrapper, rowWrapper, eduPlanRows);
            }
        }
    }

    private static Map<String, Object> detailMap(final Map<Long, Map<Integer, Map<String, Object>>> rowTermDetailMap, final Long rowId, final Integer term) {
        Map<Integer, Map<String, Object>> termDetailMap = rowTermDetailMap.get(rowId);
        Map<String, Object> detailMap = null;
        if (termDetailMap == null) {
            rowTermDetailMap.put(rowId, termDetailMap = new HashMap<>());
        } else {
            detailMap = termDetailMap.get(term);
        }

        if (detailMap == null) {
            termDetailMap.put(term, detailMap = new HashMap<>());
        }

        return detailMap;
    }


    protected EppEduPlanVersionRootBlock findRootBlock(EppEduPlanVersionBlock block)
    {
        if (block instanceof EppEduPlanVersionRootBlock) {
            return ((EppEduPlanVersionRootBlock)block);
        }

        EppEduPlanVersionRootBlock rootBlock = new DQLSelectBuilder()
        .fromEntity(EppEduPlanVersionRootBlock.class, "r")
        .column(property("r"))
        .where(eq(property(EppEduPlanVersionRootBlock.eduPlanVersion().fromAlias("r")), value(block.getEduPlanVersion())))
        .order(property(EppEduPlanVersionRootBlock.id().fromAlias("r")))
        .createStatement(this.session).setMaxResults(1).uniqueResult();

        if (null == rootBlock) {
            throw new IllegalStateException("no-root-block for epv.id = "+block.getEduPlanVersion().getId());
        }

        return rootBlock;
    }

    public EppEpvBlockWrapper buildEpvBlockWrapper(Long epvBlockId)
    {
        EppEduPlanVersionBlock block = (EppEduPlanVersionBlock)this.session.get(EppEduPlanVersionBlock.class, epvBlockId);
        EppEduPlanVersionRootBlock rootBlock = findRootBlock(block);

        final Map<Integer, Multiset<String>> epvTermSizeMap = (this.loadDetails ? this.getEpvTermSizeMap(block.getEduPlanVersion().getId()) : null);
        final EppEpvBlockWrapper blockWrapper = this.newEppEpvBlockWrapper(rootBlock, block, epvTermSizeMap);


        /* { row.id -> row }*/
        final Map<Long, EppEpvRow> rowData = new HashMap<>();

        /* { row.id -> { term.number -> { load-code -> value } } }*/
        final Map<Long, Map<Integer, Map<String, Object>>> rowTermDetailMap = new HashMap<>();

        // грузим строки и данные по ним
        Debug.begin("select.rows");
        try {
            final List<EppEpvRow> rows = new DQLSelectBuilder()
            .fromEntity(EppEpvRow.class, "row").column(property("row"))
            .where(in(property(EppEpvRow.owner().id().fromAlias("row")), ImmutableList.of(block.getId(), rootBlock.getId())))
            .createStatement(session).list();

            for (final EppEpvRow row : rows)
            {
                rowData.put(row.getId(), row);

                // тут же грузим данные непосредственно из объекта-строки (если это возможно и требуется)
                // попутно - заполняем список идентификаторов строк, по которым потом будем грузить детализацию
                if (EppEpvBlockWrapperLoader.this.loadDetails && row instanceof EppEpvTermDistributedRow) {
                    final EppEpvTermDistributedRow tdRow = (EppEpvTermDistributedRow) row;
                    final Map<String, Object> detailMap = detailMap(rowTermDetailMap, tdRow.getId(), 0);
                    tdRow.addLoad(detailMap);
                }
            }
        } finally {
            Debug.end();
        }

        // грузим детализацию по связанным объектам
        if (rowTermDetailMap.size() > 0) {
            Debug.begin("select.details");
            try {
                for (List<Long> rowIds : Iterables.partition(rowTermDetailMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER))
                {
                    Debug.begin("sz="+rowIds.size());

                    final List<EppEpvRowTerm> epvTermList = DataAccessServices.dao().getList(EppEpvRowTerm.class, EppEpvRowTerm.row().id(), rowIds);
                    // epvRowTerm.id -> term.id
                    final Map<Long, Long> rowTermToTermMap = new HashMap<>(epvTermList.size());
                    for (EppEpvRowTerm epvRowTerm : epvTermList) {
                        rowTermToTermMap.put(epvRowTerm.getId(), epvRowTerm.getTerm().getId());
                    }

                    // Функция, возвращающая номер семестра для семестра строки УПв
                    final Function<Long, Integer> termFunction = (Long epvRowTermId) -> id2termNumberMap.get(rowTermToTermMap.get(epvRowTermId));

                    /* нагрузка, введенная в строке */
                    {
                        /* по видам (всего) */
                        final List<Object[]> rows = new DQLSelectBuilder()
                                .fromEntity(EppEpvRowLoad.class, "l")
                                .where(in(property(EppEpvRowLoad.row().id().fromAlias("l")), rowIds))
                                .column(property(/*0*/EppEpvRowLoad.row().id().fromAlias("l")))
                                .column(property(/*1*/EppEpvRowLoad.loadType().id().fromAlias("l")))
                                .column(property(/*2*/EppEpvRowLoad.hours().fromAlias("l")))
                                .column(property(/*3*/EppEpvRowLoad.hoursI().fromAlias("l")))
                                .column(property(/*4*/EppEpvRowLoad.hoursE().fromAlias("l")))
                                .createStatement(session).list();

                        for (final Object[] row : rows) {
                            final Map<String, Object> detailMap = detailMap(rowTermDetailMap, (Long)row[0], 0);

                            String fullCode = id2fullCodeMap.get((Long) row[1]);
                            String iFullCode = EppALoadType.iFullCode(fullCode);
                            String eFullCode = EppALoadType.eFullCode(fullCode);

                            detailMap.put(fullCode, EppEpvRowLoad.wrap((Long)row[2]));
                            detailMap.put(iFullCode, EppEpvRowLoad.wrap((Long)row[3]));
                            detailMap.put(eFullCode, EppEpvRowLoad.wrap((Long)row[4]));
                        }
                    }

                        /* нагрузка, распределенная по семестрам */
                    {
                            /* из элемента (по семестрам) */
                        {
                            for (final EppEpvRowTerm term : epvTermList) {
                                final Map<String, Object> detailMap = detailMap(rowTermDetailMap, (Long) term.getRow().getId(), termFunction.apply(term.getId()));
                                term.addLoad(detailMap);
                            }
                        }

                        /* по видам (по семестрам) */
                        {
                            final List<Object[]> rows = new DQLSelectBuilder()
                                    .fromEntity(EppEpvRowTermLoad.class, "l")
                                    .where(in(property(EppEpvRowTermLoad.rowTerm().row().id().fromAlias("l")), rowIds))
                                    .column(property(/*0*/EppEpvRowTermLoad.rowTerm().row().id().fromAlias("l")))
                                    .column(property(/*1*/EppEpvRowTermLoad.rowTerm().id().fromAlias("l")))
                                    .column(property(/*2*/EppEpvRowTermLoad.loadType().id().fromAlias("l")))
                                    .column(property(/*3*/EppEpvRowTermLoad.hours().fromAlias("l")))
                                    .column(property(/*4*/EppEpvRowTermLoad.hoursI().fromAlias("l")))
                                    .column(property(/*5*/EppEpvRowTermLoad.hoursE().fromAlias("l")))
                                    .createStatement(session).list();

                            for (final Object[] row : rows) {
                                final Map<String, Object> detailMap = detailMap(rowTermDetailMap, (Long) row[0], termFunction.apply((Long) row[1]));

                                String fullCode = id2fullCodeMap.get((Long) row[2]);
                                String iFullCode = EppALoadType.iFullCode(fullCode);
                                String eFullCode = EppALoadType.eFullCode(fullCode);

                                detailMap.put(fullCode, EppEpvRowTermLoad.wrap((Long) row[3]));
                                detailMap.put(iFullCode, EppEpvRowTermLoad.wrap((Long) row[4]));
                                detailMap.put(eFullCode, EppEpvRowTermLoad.wrap((Long) row[5]));
                            }
                        }

                        /* контроль (по семестрам) */
                        {
                            final List<Object[]> rows = new DQLSelectBuilder()
                                    .fromEntity(EppEpvRowTermAction.class, "l")
                                    .where(in(property(EppEpvRowTermAction.rowTerm().row().id().fromAlias("l")), rowIds))
                                    .column(property(/*0*/EppEpvRowTermAction.rowTerm().row().id().fromAlias("l")))
                                    .column(property(/*1*/EppEpvRowTermAction.rowTerm().id().fromAlias("l")))
                                    .column(property(/*2*/EppEpvRowTermAction.controlActionType().id().fromAlias("l")))
                                    .column(property(/*3*/EppEpvRowTermAction.size().fromAlias("l")))
                                    .createStatement(session).list();

                            for (final Object[] row : rows) {
                                final Map<String, Object> detailMap = detailMap(rowTermDetailMap, (Long) row[0], termFunction.apply((Long) row[1]));
                                detailMap.put(id2fullCodeMap.get((Long) row[2]), (Integer) row[3]);
                            }
                        }
                    }

                    Debug.end();
                }
            } finally {
                Debug.end();
            }
        }

        // сводим все воедино
        Debug.begin("collect");
        try {
            // сначала грузим иерархически все строки в оболочку версии
            List<EppEpvRow> eduPlanRows = new ArrayList<>(rowData.values());
            collectHData(blockWrapper, null, eduPlanRows);

            // заносим во врапперы строк информацию о профессиональной деятельности
            if (blockWrapper != null && blockWrapper.getRowMap() != null)
            {
                Map<IEppEpvRow, List<EppProfessionalTask>> profTaskMap = IEppEduPlanVersionDataDAO.instance.get()
                        .getProfessionalTaskMap(blockWrapper.getRowMap().values().stream().map(IEppEpvRowWrapper::getRow).collect(Collectors.toList()));
                for (IEppEpvRowWrapper rowWrapper : blockWrapper.getRowMap().values())
                {
                    List<EppProfessionalTask> profTaskList = profTaskMap.get(rowWrapper.getRow());
                    if (profTaskList != null && !profTaskList.isEmpty())
                        rowWrapper.setProfessionalTaskList(profTaskList);
                }
            }

            // проверяем, что в планет нет циклов (DEV-4134)
            /*
            if (!eduPlanRows.isEmpty()) {
                if (forceUnrollRecursion) {
                    // все строки по одной будем добавлять в корень
                    final Map<Long, IEppEpvRowWrapper> map = blockWrapper.getRowMap();
                    Collections.sort(eduPlanRows, EppEpvRow.IN_SAME_PARENT_ROW_COMPARATOR);
                    for (final EppEpvRow row : eduPlanRows)
                    {
                        final EppEpvRowWrapper rowWrapper = this.newEppEpvRowWrapper(blockWrapper, null, row);
                        if (null != map.put(rowWrapper.getId(), rowWrapper)) {
                            throw new IllegalStateException("Recursion detected");
                        }
                    }
                }
            }
            */

            // затем для всех строк (которые уже загрузились) указываем нагрузку
            int idx = 1; // и нумеруем дисциплины в рамках всего УП
            for (final IEppEpvRowWrapper rowWrapper : blockWrapper.getRowMap().values()) {
                final Map<Integer, Map<String, Object>> termDetailMap = rowTermDetailMap.get(rowWrapper.getId());
                if (null != termDetailMap) {
                    ((EppEpvRowWrapper)rowWrapper).getLocalTermDataMap().putAll(termDetailMap);
                }
                if (rowWrapper.getRow() instanceof EppEpvRegistryRow) {
                    ((EppEpvRowWrapper)rowWrapper).setEpvRegistryRowNumber(idx++);
                }
            }
        } finally {
            Debug.end();
        }

        // возвращаем результат
        return blockWrapper;
    }


}
