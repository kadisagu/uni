/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppRegistry.ui.CloneElement;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.runtime.BusinessComponentRuntime;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.component.registry.TutorOrgUnitAsOwnerSelectModel;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 13.05.2015
 */
@Input({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "elementHolder.id", required = true)
})
public class EppRegistryCloneElementUI extends UIPresenter
{
    private EntityHolder<EppRegistryElement> _elementHolder = new EntityHolder<>();
    private OrgUnit _newOwner;
    private String _newTitle;
    private ISelectModel _ownerSelectModel;

    @Override
    public void onComponentRefresh()
    {
        getElementHolder().refresh();

        setNewOwner(getRegistryElement().getOwner());
        setNewTitle(getRegistryElement().getTitle());

        setOwnerSelectModel(new TutorOrgUnitAsOwnerSelectModel() {
            @Override protected OrgUnit getCurrentValue() {
                return getRegistryElement().getOwner();
            }
        });
    }

    // listeners

    public void onClickApply()
    {
        // Оборачиваем элемент во враппер
        Map<Long, IEppRegElWrapper> map = IEppRegistryDAO.instance.get().getRegistryElementDataMap(ImmutableList.of(getRegistryElement().getId()));
        // Создаем новые элемент на основе враппера
        EppRegistryElement newRegistryElement = IEppRegistryDAO.instance.get().doCreateRegistryElement(map.values().iterator().next(), getNewOwner(), getNewTitle(), null);

        deactivate();
        // Переходим на карточку нового элемента
        getActivationBuilder()
                .asCurrent(BusinessComponentRuntime.getInstance().getPublisher(newRegistryElement.getClass()))
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, newRegistryElement.getId())
                .activate();
    }

    // g & s

    public EppRegistryElement getRegistryElement()
    {
        return getElementHolder().getValue();
    }

    public EntityHolder<EppRegistryElement> getElementHolder()
    {
        return _elementHolder;
    }

    public OrgUnit getNewOwner()
    {
        return _newOwner;
    }

    public void setNewOwner(OrgUnit newOwner)
    {
        _newOwner = newOwner;
    }

    public String getNewTitle()
    {
        return _newTitle;
    }

    public void setNewTitle(String newTitle)
    {
        _newTitle = newTitle;
    }

    public ISelectModel getOwnerSelectModel()
    {
        return _ownerSelectModel;
    }

    public void setOwnerSelectModel(ISelectModel ownerSelectModel)
    {
        _ownerSelectModel = ownerSelectModel;
    }
}