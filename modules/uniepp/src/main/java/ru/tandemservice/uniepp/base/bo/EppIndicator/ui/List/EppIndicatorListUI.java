/* $Id:$ */
package ru.tandemservice.uniepp.base.bo.EppIndicator.ui.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.IComponentRegion;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.base.bo.EppIndicator.EppIndicatorManager;
import ru.tandemservice.uniepp.base.bo.EppIndicator.logic.EppIndicator;

import java.util.*;

/**
 * @author oleyba
 * @since 9/15/14
 */
public class EppIndicatorListUI extends UIPresenter
{
    private List<HSelectOption> indicatorList = Collections.emptyList();
    private DataWrapper indicator;

    @Override
    public void onComponentRefresh()
    {
        Collection<EppIndicator> indicators = EppIndicatorManager.instance().indicatorListExtPoint().getItems().values();
        List<DataWrapper> nodes = new ArrayList<>();
        Map<String, DataWrapper> groups = new HashMap<>();

        long id = 1L;
        for (EppIndicator eppIndicator : indicators) {
            DataWrapper group = groups.get(eppIndicator.getGroupName());
            if (null == group) {
                group = new DataWrapper(id++, EppIndicatorManager.instance().getProperty(eppIndicator.getGroupName() + ".title"));
                group.setHierarchyParent(null);
                groups.put(eppIndicator.getGroupName(), group);
            }
            DataWrapper wrapper = new DataWrapper(id++, EppIndicatorManager.instance().getProperty(eppIndicator.getName() + ".title"));
            wrapper.setHierarchyParent(group);
            wrapper.setProperty("componentName", eppIndicator.getComponentName());
            nodes.add(wrapper);
        }

        setIndicatorList(HierarchyUtil.listHierarchyNodesWithParents(nodes, false));

        onChangeIndicator();
    }

    public void onChangeIndicator()
    {
        final IComponentRegion region = _uiConfig.getBusinessComponent().getChildRegion("indicatorScope");
        if (null != region) { region.deactivateComponent(false); }

        final String componentName = null == getIndicator() ? null : (String) getIndicator().getProperty("componentName");
        if (null != componentName) {
            _uiActivation.asRegion(componentName, "indicatorScope").activate();
        }
    }

    // getters and setters

    public List<HSelectOption> getIndicatorList() { return this.indicatorList; }
    public void setIndicatorList(final List<HSelectOption> indicatorList) { this.indicatorList = indicatorList; }

    public DataWrapper getIndicator() { return this.indicator; }
    public void setIndicator(final DataWrapper indicator) { this.indicator = indicator; }
}