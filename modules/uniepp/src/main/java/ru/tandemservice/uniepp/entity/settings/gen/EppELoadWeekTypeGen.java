package ru.tandemservice.uniepp.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.settings.EppELoadWeekType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь типа недель с аудиторными и самостоятельными занятиями
 *
 * Показывает, какие типы недель отвечают каким видам учебной нагрузки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppELoadWeekTypeGen extends EntityBase
 implements INaturalIdentifiable<EppELoadWeekTypeGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.settings.EppELoadWeekType";
    public static final String ENTITY_NAME = "eppELoadWeekType";
    public static final int VERSION_HASH = 192197388;
    private static IEntityMeta ENTITY_META;

    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_LOAD_TYPE = "loadType";
    public static final String L_WEEK_TYPE = "weekType";

    private DevelopForm _developForm;     // Форма освоения
    private EppELoadType _loadType;     // Вид учебной нагрузки (типы учебной нагрузки)
    private EppWeekType _weekType;     // Типы деятельности в учебном графике

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Вид учебной нагрузки (типы учебной нагрузки). Свойство не может быть null.
     */
    @NotNull
    public EppELoadType getLoadType()
    {
        return _loadType;
    }

    /**
     * @param loadType Вид учебной нагрузки (типы учебной нагрузки). Свойство не может быть null.
     */
    public void setLoadType(EppELoadType loadType)
    {
        dirty(_loadType, loadType);
        _loadType = loadType;
    }

    /**
     * @return Типы деятельности в учебном графике. Свойство не может быть null.
     */
    @NotNull
    public EppWeekType getWeekType()
    {
        return _weekType;
    }

    /**
     * @param weekType Типы деятельности в учебном графике. Свойство не может быть null.
     */
    public void setWeekType(EppWeekType weekType)
    {
        dirty(_weekType, weekType);
        _weekType = weekType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppELoadWeekTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setDevelopForm(((EppELoadWeekType)another).getDevelopForm());
                setLoadType(((EppELoadWeekType)another).getLoadType());
                setWeekType(((EppELoadWeekType)another).getWeekType());
            }
        }
    }

    public INaturalId<EppELoadWeekTypeGen> getNaturalId()
    {
        return new NaturalId(getDevelopForm(), getLoadType(), getWeekType());
    }

    public static class NaturalId extends NaturalIdBase<EppELoadWeekTypeGen>
    {
        private static final String PROXY_NAME = "EppELoadWeekTypeNaturalProxy";

        private Long _developForm;
        private Long _loadType;
        private Long _weekType;

        public NaturalId()
        {}

        public NaturalId(DevelopForm developForm, EppELoadType loadType, EppWeekType weekType)
        {
            _developForm = ((IEntity) developForm).getId();
            _loadType = ((IEntity) loadType).getId();
            _weekType = ((IEntity) weekType).getId();
        }

        public Long getDevelopForm()
        {
            return _developForm;
        }

        public void setDevelopForm(Long developForm)
        {
            _developForm = developForm;
        }

        public Long getLoadType()
        {
            return _loadType;
        }

        public void setLoadType(Long loadType)
        {
            _loadType = loadType;
        }

        public Long getWeekType()
        {
            return _weekType;
        }

        public void setWeekType(Long weekType)
        {
            _weekType = weekType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EppELoadWeekTypeGen.NaturalId) ) return false;

            EppELoadWeekTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getDevelopForm(), that.getDevelopForm()) ) return false;
            if( !equals(getLoadType(), that.getLoadType()) ) return false;
            if( !equals(getWeekType(), that.getWeekType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getDevelopForm());
            result = hashCode(result, getLoadType());
            result = hashCode(result, getWeekType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getDevelopForm());
            sb.append("/");
            sb.append(getLoadType());
            sb.append("/");
            sb.append(getWeekType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppELoadWeekTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppELoadWeekType.class;
        }

        public T newInstance()
        {
            return (T) new EppELoadWeekType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "developForm":
                    return obj.getDevelopForm();
                case "loadType":
                    return obj.getLoadType();
                case "weekType":
                    return obj.getWeekType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "loadType":
                    obj.setLoadType((EppELoadType) value);
                    return;
                case "weekType":
                    obj.setWeekType((EppWeekType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "developForm":
                        return true;
                case "loadType":
                        return true;
                case "weekType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "developForm":
                    return true;
                case "loadType":
                    return true;
                case "weekType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "developForm":
                    return DevelopForm.class;
                case "loadType":
                    return EppELoadType.class;
                case "weekType":
                    return EppWeekType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppELoadWeekType> _dslPath = new Path<EppELoadWeekType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppELoadWeekType");
    }
            

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppELoadWeekType#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Вид учебной нагрузки (типы учебной нагрузки). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppELoadWeekType#getLoadType()
     */
    public static EppELoadType.Path<EppELoadType> loadType()
    {
        return _dslPath.loadType();
    }

    /**
     * @return Типы деятельности в учебном графике. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppELoadWeekType#getWeekType()
     */
    public static EppWeekType.Path<EppWeekType> weekType()
    {
        return _dslPath.weekType();
    }

    public static class Path<E extends EppELoadWeekType> extends EntityPath<E>
    {
        private DevelopForm.Path<DevelopForm> _developForm;
        private EppELoadType.Path<EppELoadType> _loadType;
        private EppWeekType.Path<EppWeekType> _weekType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppELoadWeekType#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Вид учебной нагрузки (типы учебной нагрузки). Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppELoadWeekType#getLoadType()
     */
        public EppELoadType.Path<EppELoadType> loadType()
        {
            if(_loadType == null )
                _loadType = new EppELoadType.Path<EppELoadType>(L_LOAD_TYPE, this);
            return _loadType;
        }

    /**
     * @return Типы деятельности в учебном графике. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.settings.EppELoadWeekType#getWeekType()
     */
        public EppWeekType.Path<EppWeekType> weekType()
        {
            if(_weekType == null )
                _weekType = new EppWeekType.Path<EppWeekType>(L_WEEK_TYPE, this);
            return _weekType;
        }

        public Class getEntityClass()
        {
            return EppELoadWeekType.class;
        }

        public String getEntityName()
        {
            return "eppELoadWeekType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
