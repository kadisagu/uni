package ru.tandemservice.uniepp.entity.plan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditionalProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanAdditionalProf;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебный план ДПО
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanAdditionalProfGen extends EppEduPlan
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.plan.EppEduPlanAdditionalProf";
    public static final String ENTITY_NAME = "eppEduPlanAdditionalProf";
    public static final int VERSION_HASH = -578916584;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PROGRAM = "eduProgram";

    private EduProgramAdditionalProf _eduProgram;     // ОП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ОП. Свойство не может быть null.
     */
    @NotNull
    public EduProgramAdditionalProf getEduProgram()
    {
        return _eduProgram;
    }

    /**
     * @param eduProgram ОП. Свойство не может быть null.
     */
    public void setEduProgram(EduProgramAdditionalProf eduProgram)
    {
        dirty(_eduProgram, eduProgram);
        _eduProgram = eduProgram;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppEduPlanAdditionalProfGen)
        {
            setEduProgram(((EppEduPlanAdditionalProf)another).getEduProgram());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanAdditionalProfGen> extends EppEduPlan.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlanAdditionalProf.class;
        }

        public T newInstance()
        {
            return (T) new EppEduPlanAdditionalProf();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "eduProgram":
                    return obj.getEduProgram();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "eduProgram":
                    obj.setEduProgram((EduProgramAdditionalProf) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduProgram":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduProgram":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "eduProgram":
                    return EduProgramAdditionalProf.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlanAdditionalProf> _dslPath = new Path<EppEduPlanAdditionalProf>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlanAdditionalProf");
    }
            

    /**
     * @return ОП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanAdditionalProf#getEduProgram()
     */
    public static EduProgramAdditionalProf.Path<EduProgramAdditionalProf> eduProgram()
    {
        return _dslPath.eduProgram();
    }

    public static class Path<E extends EppEduPlanAdditionalProf> extends EppEduPlan.Path<E>
    {
        private EduProgramAdditionalProf.Path<EduProgramAdditionalProf> _eduProgram;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ОП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.plan.EppEduPlanAdditionalProf#getEduProgram()
     */
        public EduProgramAdditionalProf.Path<EduProgramAdditionalProf> eduProgram()
        {
            if(_eduProgram == null )
                _eduProgram = new EduProgramAdditionalProf.Path<EduProgramAdditionalProf>(L_EDU_PROGRAM, this);
            return _eduProgram;
        }

        public Class getEntityClass()
        {
            return EppEduPlanAdditionalProf.class;
        }

        public String getEntityName()
        {
            return "eppEduPlanAdditionalProf";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
