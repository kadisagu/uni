/* $Id$ */
package ru.tandemservice.uniepp.base.bo.CtrReceiptPaymentOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.shared.ctr.base.bo.CtrPaymentPromice.ICtrPaymentOrderManager;
import org.tandemframework.shared.ctr.base.bo.CtrPaymentPromice.logic.ICtrPaymentOrderPrinter;

import ru.tandemservice.uniepp.base.bo.CtrReceiptPaymentOrder.logic.CtrReceiptPaymentOrderDao;
import ru.tandemservice.uniepp.base.bo.CtrReceiptPaymentOrder.logic.CtrReceiptPaymentOrderPrinter;
import ru.tandemservice.uniepp.base.bo.CtrReceiptPaymentOrder.logic.ICtrReceiptPaymentOrderDao;

/**
 * @author Dmitry Seleznev
 * @since 02.12.2011
 */
@Configuration
public class CtrReceiptPaymentOrderManager extends BusinessObjectManager implements ICtrPaymentOrderManager
{
    public static CtrReceiptPaymentOrderManager instance() {
        return BusinessObjectManager.instance(CtrReceiptPaymentOrderManager.class);
    }

    @Bean
    @Override
    public ICtrPaymentOrderPrinter printer() {
        return new CtrReceiptPaymentOrderPrinter();
    }

    @Bean
    public ICtrReceiptPaymentOrderDao dao() {
        return new CtrReceiptPaymentOrderDao();
    }
}