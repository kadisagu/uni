package ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockContent;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.tool.tree.PlaneTree;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvHierarchyRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvStructureRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.*;
import java.util.stream.Collectors;

/**
 * TODO: addon?
 * @author vdanilov
 */
public abstract class EppEduPlanVersionBlockContentUIPresenter extends UIPresenter
{
    private Collection<EppIControlActionType> intermediateCAList; // список промежуточных форм контроля
    private Collection<EppFControlActionType> finalCAList; // список итоговых форм контроля
    private EppControlActionType controlAction; // текущая форма контроля

    private IEppEpvBlockWrapper blockWrapper; // wrapper
    private PlaneTree tree; // дерево, содержащее строки основного + текуего блока УП(в)
    private IEppEpvRowWrapper rowWrapper; // текущая строка УП(в)

    private Collection<Course> gridCourseList; // курсы сетки из УП(в)
    private Course gridCourse; // текущий курс (используется для итерации по курсам)

    private List<DevelopGridTerm> gridTermList; // семестры сетки из УП(в)
    private DevelopGridTerm gridTerm; // текущий семестр

    private boolean selfWorkPresent;

    private Set<Long> selectedRowIds = new HashSet<>();

    private Boolean scroll = false;
    private Long scrollRowId;
    private Long scrollPos;
    private Long highlightRowId;

    private Map<IEppEpvRowWrapper, EppFakeRowWrapper> interactiveWrappersMap;
    private Map<IEppEpvRowWrapper, EppFakeRowWrapper> electronicWrappersMap;
    private Multiset<String> rowTitleMultiset = HashMultiset.create();
    private Multiset<String> rowRegNumberMultiset = HashMultiset.create();

    public abstract EppEduPlanVersionBlock getBlock();

    /**
     * Добавление фэйковых строк для отображения часов в электронной и интерактивной формах.
     * Данные строки нужны только для отображения части данных из реальной строки УПв.
     */
    protected Collection<IEppEpvRowWrapper> patchRows(Collection<IEppEpvRowWrapper> rows) {
        interactiveWrappersMap = new HashMap<>(rows.size());
        electronicWrappersMap = new HashMap<>(rows.size());
        rowTitleMultiset.clear();
        rowRegNumberMultiset.clear();

        final List<IEppEpvRowWrapper> resultList = new ArrayList<>(rows.size());
        for (IEppEpvRowWrapper item : rows)
        {
            if (item.getRow() instanceof EppEpvRegistryRow) {
                rowTitleMultiset.add(item.getDisplayableTitle());
                final EppRegistryElement regElement = ((EppEpvRegistryRow) item.getRow()).getRegistryElement();
                if (regElement != null) {
                    rowRegNumberMultiset.add(regElement.getParent().getCode() + "$" + regElement.getNumber());
                }
            }
            resultList.add(item);

            if ((item.getType() != null && EppRegistryStructureCodes.REGISTRY_DISCIPLINE.equals(item.getType().getRoot().getCode())) || EppEpvGroupImRow.class.isAssignableFrom(item.getRowEntityClass()))
            {
                // Если это "Дисциплина" или группа дисциплин по выбору, то добавляются строки по интерактивным/электронным часам
                {
                    EppFakeRowWrapper fakeRowWrapper = new EppInteractiveRowWrapper(item);
                    resultList.add(fakeRowWrapper);
                    interactiveWrappersMap.put(item, fakeRowWrapper);
                }

                {
                    EppFakeRowWrapper fakeRowWrapper = new EppElectronicRowWrapper(item);
                    resultList.add(fakeRowWrapper);
                    electronicWrappersMap.put(item, fakeRowWrapper);
                }
            }
        }
        return resultList;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onComponentRefresh() {

        final IEppEduPlanVersionDataDAO versionDataDAO = IEppEduPlanVersionDataDAO.instance.get();
        {
            final List<EppIControlActionType> listI = new ArrayList<>();
            final List<EppFControlActionType> listF = new ArrayList<>();
            versionDataDAO.getActiveControlActionTypes(null).forEach(ca -> {
                if (ca instanceof EppFControlActionType) {
                    listF.add((EppFControlActionType) ca);
                } else if (ca instanceof EppIControlActionType) {
                    listI.add((EppIControlActionType) ca);
                }
            });
            this.setIntermediateCAList(listI);
            this.setFinalCAList(listF);
        }

        {
            this.blockWrapper = versionDataDAO.getEduPlanVersionBlockData(this.getBlock().getId(), true);
            final Collection<IEppEpvRowWrapper> rows = patchRows(blockWrapper.getRowMap().values());
            this.setTree(new PlaneTree((Collection)rows));
            this.setSelfWorkPresent(this.isSelfWorkPresent(rows));
        }

        {
            final IDevelopGridDAO developGridDAO = IDevelopGridDAO.instance.get();
            setGridTermList(developGridDAO.getDevelopGridTermList(getEduPlan().getDevelopGrid()));
            setGridCourseList(developGridDAO.getDevelopGridCourseSet(getEduPlan().getDevelopGrid()));
        }
    }

    // columns

    public String getGridTermCaption() {
        int termNumber = this.getGridTerm().getTermNumber();
        final int termSize = this.blockWrapper.getTermSize(IEppEpvBlockWrapper.ELOAD_FULL_CODE_ALL, termNumber);
        return String.valueOf(termNumber) + "\n" + "("+termSize+")";
    }

    public boolean isRowWrapperSelected() {
        return getSelectedRowIds().contains(this.getRowWrapper().getId());
    }

    public void setRowWrapperSelected(boolean rowWrapperSelected) {
        if (rowWrapperSelected) {
            getSelectedRowIds().add(this.getRowWrapper().getId());
        } else {
            getSelectedRowIds().remove(this.getRowWrapper().getId());
        }
    }

    public boolean isRowWrapperStateExist() {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        if (wrapper.getRow() instanceof EppEpvRegistryRow && !(wrapper instanceof EppFakeRowWrapper)) {
            final EppRegistryElement registryElement = ((EppEpvRegistryRow)wrapper.getRow()).getRegistryElement();
            if (null != registryElement) {
                return true;
            }
        }
        return false;
    }

    public String getRowWrapperStateImage() {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        if (wrapper.getRow() instanceof EppEpvRegistryRow) {
            final EppRegistryElement registryElement = ((EppEpvRegistryRow)wrapper.getRow()).getRegistryElement();
            if (null != registryElement) {
                return "img/general/epp-state/state-"+registryElement.getState().getCode()+".png";
            }
        }
        return "";
    }

    public String getRowWrapperStateTitle() {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        if (wrapper.getRow() instanceof EppEpvRegistryRow) {
            final EppRegistryElement registryElement = ((EppEpvRegistryRow)wrapper.getRow()).getRegistryElement();
            if (null != registryElement) {
                return registryElement.getState().getTitle();
            }
        }
        return "";
    }

    public String getRegistryRowNumber() {
        final Integer number = this.getRowWrapper().getEpvRegistryRowNumber();
        return number != null ? String.valueOf(number) : "";
    }

    public String getRowWrapperIndex() {
        return this.getRowWrapper().getIndex();
    }

    public String getRowWrapperDisplayableTitle() {
        return this.getRowWrapper().getDisplayableTitle();
    }

    public String getRowWrapperType() {
        return this.getRowWrapper().getRowType();
    }

    public String getRowWrapperProfessionalTasks()
    {
        String defaultResult = "";
        if (!(getRowWrapper().getRow() instanceof EppEpvRegistryRow)) return defaultResult;
        List<EppProfessionalTask> profTaskList = getRowWrapper().getProfessionalTaskList();
        if (profTaskList != null && !profTaskList.isEmpty())
            return profTaskList.stream().map(EppProfessionalTask::getShortTitle).collect(Collectors.joining(", "));
        return defaultResult;
    }

    public EppRegistryElement getRowWrapperRegistryElement() {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        if (!(wrapper instanceof EppFakeRowWrapper) && wrapper.getRow() instanceof EppEpvRegistryRow) {
            return ((EppEpvRegistryRow)wrapper.getRow()).getRegistryElement();
        }
        return null;
    }

    public String getRowWrapperPartsRaw() {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        if (!(wrapper instanceof EppFakeRowWrapper) && wrapper.getRow() instanceof EppEpvRegistryRow) {
            final EppEpvRegistryRow row = (EppEpvRegistryRow)wrapper.getRow();
            final int rowSize = Math.max(0, wrapper.getActiveTermSet().size());
            final EppRegistryElement registryElement = row.getRegistryElement();
            if (null != registryElement) {
                final int regSize = Math.max(1, registryElement.getParts());
                if (rowSize != regSize) {
                    return error(String.valueOf(regSize) /*что выбрано*/, String.valueOf(rowSize) /*как должно быть*/);
                }
            }
            return String.valueOf(rowSize);
        }
        return "";
    }

    public String getRowWrapperControlActionUsage() {
        return this.getRowWrapper().getDisplayableControlActionUsage(this.getControlAction().getFullCode());
    }

    private EppEpvGroupImRow getParentSelectGroup(IEppEpvRowWrapper wrapper)
    {
        // Дисциплины реестра, входящие в группу дисциплин по выбору
        if (wrapper instanceof  EppFakeRowWrapper)
            wrapper = wrapper.getHierarhyParent();

        do
        {
            wrapper = wrapper.getHierarhyParent();
            if (wrapper == null)
                return null;
        }
        while (!(wrapper.getRow() instanceof EppEpvGroupImRow));
        return (EppEpvGroupImRow) wrapper.getRow();
    }

    public String getRowWrapperTotalSize() {
        return getRowWrapperTotalRaw(EppLoadType.FULL_CODE_TOTAL_HOURS);
    }

    public String getRowWrapperTotalSizeRaw() {
        return getRowWrapperTotalSize();
    }

    public String getRowWrapperTotalLaborRaw() {
        return getRowWrapperTotalRaw(EppLoadType.FULL_CODE_LABOR);
    }

    protected IEppEpvRowWrapper getParentWrapper()
    {
        // Получение родительской строки соответствующего типа. Если текущая строка - интерактивная/электронная форма, то находится строка соответствующего типа для родительского элемента
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        Preconditions.checkNotNull(wrapper.getHierarhyParent());

        if (wrapper instanceof EppInteractiveRowWrapper)
            return interactiveWrappersMap.get(wrapper.getHierarhyParent().getHierarhyParent());
        if (wrapper instanceof EppElectronicRowWrapper)
            return electronicWrappersMap.get(wrapper.getHierarhyParent().getHierarhyParent());
        return wrapper.getHierarhyParent();
    }

    protected String getRowWrapperTotalRaw(final String loadFullCode) {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        final String disciplineValue = wrapper.getTotalDisplayableLoadString(0, loadFullCode, false, 1);
        final EppEpvGroupImRow group = getParentSelectGroup(wrapper);

        if (group == null)
            return disciplineValue;

        final String groupValue = getParentWrapper().getTotalDisplayableLoadString(0, loadFullCode, false, group.getSize());

        return compareForSelGroupItem(groupValue, disciplineValue);
    }

    protected String getRowWrapperTotalByLoadTypeRaw(final String loadFullCode) {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        final String disciplineValue = wrapper.getRowWrapperTotalByLoadTypeRaw(loadFullCode, 1);
        final EppEpvGroupImRow group = getParentSelectGroup(wrapper);

        if (group == null)
            return disciplineValue;

        final String groupValue = getParentWrapper().getRowWrapperTotalByLoadTypeRaw(loadFullCode, group.getSize());

        return compareForSelGroupItem(groupValue, disciplineValue);
    }

    public String getRowWrapperTotalAuditRaw() {
        return this.getRowWrapperTotalByLoadTypeRaw(EppELoadType.FULL_CODE_AUDIT);
    }

    public String getRowWrapperTotalSelfWorkRaw() {
        return this.getRowWrapperTotalByLoadTypeRaw(EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL) ;
    }

    public String getRowWrapperTotalControl() {
        return this.getRowWrapperTotalByLoadTypeRaw(EppLoadType.FULL_CODE_CONTROL);
    }

    public String getRowWrapperTotalLecturesRaw() {
        return this.getRowWrapperTotalByLoadTypeRaw(EppALoadType.FULL_CODE_TOTAL_LECTURES);
    }

    public String getRowWrapperTotalPracticesRaw() {
        return this.getRowWrapperTotalByLoadTypeRaw(EppALoadType.FULL_CODE_TOTAL_PRACTICE);
    }

    public String getRowWrapperTotalLabsRaw() {
        return this.getRowWrapperTotalByLoadTypeRaw(EppALoadType.FULL_CODE_TOTAL_LABS);
    }

    public String getRowWrapperTermLaborRaw() {
        final DevelopGridTerm gridTerm = this.getGridTerm();
        return this.getRowWrapper().getTotalDisplayableLoadString(gridTerm.getTermNumber(), EppLoadType.FULL_CODE_LABOR, true, 1);
    }

    public String getRowWrapperTermSizeRaw() {
        final DevelopGridTerm gridTerm = this.getGridTerm();
        return this.getRowWrapper().getTotalDisplayableLoadString(gridTerm.getTermNumber(), EppLoadType.FULL_CODE_TOTAL_HOURS, true, 1);
    }

    public String getRowWrapperTermAuditRaw() {
        final DevelopGridTerm gridTerm = this.getGridTerm();
        return this.getRowWrapper().getTotalDisplayableLoadString(gridTerm.getTermNumber(), EppELoadType.FULL_CODE_AUDIT, true, 1);
    }

    public String getRowWrapperTermWeeksRaw() {
        final DevelopGridTerm gridTerm = this.getGridTerm();
        return this.getRowWrapper().getTotalDisplayableLoadString(gridTerm.getTermNumber(), EppLoadType.FULL_CODE_WEEKS, true, 1);
    }

    public String getRowWrapperTermSizeAndWeeksRaw()
    {
        String rowWrapperTermSizeRaw = getRowWrapperTermSizeRaw();
        boolean rowWrapperTermSizeRawEmpty = StringUtils.isEmpty(rowWrapperTermSizeRaw);

        String rowWrapperTermWeeksRaw = getRowWrapperTermWeeksRaw();
        boolean rowWrapperTermWeeksRawEmpty = StringUtils.isEmpty(rowWrapperTermWeeksRaw);

        if (rowWrapperTermSizeRawEmpty) {
            if (rowWrapperTermWeeksRawEmpty) { return ""; }
            return (rowWrapperTermWeeksRaw + " нед.");
        }

        if (rowWrapperTermWeeksRawEmpty) {
            return (rowWrapperTermSizeRaw + " ч.");
        }

        return (rowWrapperTermSizeRaw + " ч. / " + rowWrapperTermWeeksRaw + " нед.");
    }


    public boolean isRowWrapperLoadInWeeks() {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        return IEppEpvRowWrapper.ACTIONS_ROWS.evaluate(wrapper);
    }

    protected String getRowWrapperTermLoadRaw(String fullCode)
    {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        final DevelopGridTerm gridTerm = this.getGridTerm();
        if (wrapper.isTermDataOwner()) {
            final double databaseValue = wrapper.getTotalInTermLoad(gridTerm.getTermNumber(), fullCode);
            return UniEppUtils.formatLoad(databaseValue, true);
        }
        return "";
    }

    public String getRowWrapperTermLecturesRaw() {
        return getRowWrapperTermLoadRaw(EppALoadType.FULL_CODE_TOTAL_LECTURES);
    }

    public String getRowWrapperTermPracticesRaw() {
        return getRowWrapperTermLoadRaw(EppALoadType.FULL_CODE_TOTAL_PRACTICE);
    }

    public String getRowWrapperTermLabsRaw() {
        return getRowWrapperTermLoadRaw(EppALoadType.FULL_CODE_TOTAL_LABS);
    }

    public String getRowWrapperTermSelfWorkRaw() {
        return getRowWrapperTermLoadRaw(EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);
    }

    public String getRowWrapperTermControl() {
        return getRowWrapperTermLoadRaw(EppLoadType.FULL_CODE_CONTROL);
    }

    public int getRowWrapperHierarchyLevel() {
        return this.getTree().getLevel(this.getRowWrapper());
    }

    public boolean isRowWrapperLeaf() {
        return !this.getTree().hasChildren(this.getRowWrapper());
    }

    public String getRowTitleStyle() {
        final IEppEpvRowWrapper rowWrapper = this.getRowWrapper();
        if (rowWrapper.getRow() instanceof EppEpvRegistryRow && this.rowTitleMultiset.count(rowWrapper.getDisplayableTitle()) > 1) {
            // Дублирующиеся названия подсвечиваем красным
            return "background-color: " + UniDefines.COLOR_ERROR;
        }
        return "";
    }

    public String getRowRegNumberStyle() {
        final IEppEpvRowWrapper rowWrapper = this.getRowWrapper();
        if (rowWrapper.getRow() instanceof EppEpvRegistryRow) {
            final EppRegistryElement regElement = ((EppEpvRegistryRow) rowWrapper.getRow()).getRegistryElement();
            if (regElement != null && this.rowRegNumberMultiset.count(regElement.getParent().getCode() + "$" + regElement.getNumber()) > 1) {
                // Дублирующиеся элемеенты реестра подсвечиваем красным
                return "background-color: " + UniDefines.COLOR_ERROR;
            }
        }
        return "";
    }

    public String getRowWrapperOwnerOrgUnitTitle() {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        if (!(wrapper instanceof EppFakeRowWrapper) && wrapper.getRow() instanceof EppEpvRegistryRow) {
            EppEpvRegistryRow regElRow = (EppEpvRegistryRow)wrapper.getRow();
            if (null == regElRow.getRegistryElementOwner()) { return ""; }
            return regElRow.getRegistryElementOwner().getTitle();
        }
        return "";
    }

    public boolean isRowWrapperCountInLoadEnabled() {
        if (!isRowWrapperEditEnabled()) { return false; }
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        return null == wrapper.getHierarhyParent() || Boolean.TRUE.equals(wrapper.getHierarhyParent().getUsedInLoad());
    }

    public boolean isRowWrapperCountInLoad() {
        return Boolean.TRUE.equals(this.getRowWrapper().getUsedInLoad());
    }

    public boolean isRowWrapperCountInActionsEnabled() {
        if (!isRowWrapperEditEnabled()) { return false; }
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        return null == wrapper.getHierarhyParent() || Boolean.TRUE.equals(wrapper.getHierarhyParent().getUsedInActions());
    }

    public boolean isRowWrapperCountInActions() {
        return Boolean.TRUE.equals(this.getRowWrapper().getUsedInActions());
    }

    // utils

    public boolean isViewOnly() { return true; }

    public EppEduPlanVersion getEduPlanVersion() { return this.getBlock().getEduPlanVersion(); }
    public EppEduPlan getEduPlan() { return this.getEduPlanVersion().getEduPlan(); }

    // проверяет, существует ли самостоятельная работа в указаных строках
    protected boolean isSelfWorkPresent(final Collection<IEppEpvRowWrapper> blockRows) {
        for (final IEppEpvRowWrapper row: blockRows) {
            if (row.getTotalLoad(null, EppELoadType.FULL_CODE_SELFWORK) > 0) {
                // если есть хоть в одном семестре (вычисленная > 0)
                return true;
            }
        }
        return false;
    }

    public boolean isRowWrapperEditEnabled(IEppEpvRowWrapper wrapper) {
        return wrapper.getOwner().equals(getBlock());
    }

    public boolean isRowWrapperEditEnabled() {
        return isRowWrapperEditEnabled(this.getRowWrapper());
    }

    public String getRowWrapperRowStyle() {
        final IEppEpvRowWrapper wrapper = this.getRowWrapper();
        final StringBuilder sb = new StringBuilder();
        if (!isRowWrapperEditEnabled(wrapper)) { sb.append("color: gray;"); }
        if (wrapper.getRow() instanceof EppEpvHierarchyRow) { sb.append("font-weight:bold;"); }
        if (wrapper.getRow().getId().equals(getHighlightRowId())) {
           sb.append("background-color: "+UniDefines.COLOR_YELLOW+";");
        }
        else {
            final boolean skipLoad = !Boolean.TRUE.equals(wrapper.getUsedInLoad());
            final boolean skipActions = !Boolean.TRUE.equals(wrapper.getUsedInActions());
            if (skipActions || skipLoad) {
                sb.append("background-color: #e8e8e8");
            }
        }
        return sb.toString();
    }

    public Collection<IEppEpvRowWrapper> getRowWrapperList() {
        final boolean showI = getEduPlanVersion().isHoursI();
        final boolean showE = getEduPlanVersion().isHoursE();
        final List<IEppEpvRowWrapper> allRows = getAllRowWrapperList();
        if (showE && showI) return allRows;

        final List<IEppEpvRowWrapper> visibleRows = new ArrayList<>(allRows.size());
        for (IEppEpvRowWrapper row : allRows) {
            if (!showI && row instanceof EppInteractiveRowWrapper) continue;
            if (!showE && row instanceof EppElectronicRowWrapper) continue;
            visibleRows.add(row);
        }
        return visibleRows;
    }

    @SuppressWarnings("unchecked")
    public List<IEppEpvRowWrapper> getAllRowWrapperList() {
        return (List)Arrays.asList(this.getTree().getFlatTreeObjects()); // там IEppEpvRowWrapper
    }

    public Collection<DevelopGridTerm> getGridCourseTermList() {
        return CollectionUtils.select(getGridTermList(), object -> object.getCourse().equals(getGridCourse()));
    }

    protected static String error(final String documentValue, final String targetValue) {
        return UniEppUtils.error(documentValue, targetValue);
    }

    protected static String compareForSelGroupItem(final String groupValue, final String disciplineValue) {
        if (!Objects.equals(groupValue, disciplineValue))
            return "<span style=\"color:"+ UniDefines.COLOR_CELL_ERROR+"\">"+(groupValue.isEmpty() ? "0" : groupValue)+" / "+(disciplineValue.isEmpty() ? "0" : disciplineValue)+"</span>";

        return disciplineValue;
    }

    public boolean isEditable() { return  (!isViewOnly()) && (!this.getEduPlanVersion().getState().isReadOnlyState()); }
    public boolean isRowWrapperShowEditActions() { return isEditable() && !(getRowWrapper() instanceof EppFakeRowWrapper) && !(getRowWrapper().getRow() instanceof EppEpvStructureRow); }

    public Collection<Long> resetSelectedElements() {
        try { return this.getSelectedRowIds(); }
        finally { this.setSelectedRowIds(new HashSet<>()); }
    }

    // scroll

    public String getScrollJScript() {
        String script = "function submitFormBeginReloaded() {\n" +
            "var w = jQuery(window);\n" +
            "var f = jQuery('#scrollPos_in');\n" +
            "f.val(w.scrollTop());\n" +
            "}\n" +
            "window.submitFormBegin=submitFormBeginReloaded;\n";
        if (Boolean.TRUE.equals(getScroll())) {
            setScroll(false);
            if (getScrollRowId() != null) {
                script = script +
                    "console.log('!!');\n" +
                    "console.log(jQuery('#epv_block_content_0__" + getScrollRowId() + "').offset().top);\n" +
                    "jQuery(window).scrollTop(jQuery('#epv_block_content_0__" + getScrollRowId() + "').offset().top - 400);";
                    // "jQuery('body').scrollTo('#epv_block_content_0__" + getScrollRowId() + "_in');";
                setScrollRowId(null);
                return script;
            } else {
                return script + "jQuery(window).scrollTop("+getScrollPos()+");";
            }
        } else {
            setScroll(false);
            return script;
        }
    }

    // getters & setters

    public PlaneTree getTree() { return this.tree; }
    public void setTree(final PlaneTree tree) { this.tree = tree; }

    public IEppEpvBlockWrapper getBlockWrapper() { return this.blockWrapper; }
    public void setBlockWrapper(IEppEpvBlockWrapper blockWrapper) { this.blockWrapper = blockWrapper; }

    public IEppEpvRowWrapper getRowWrapper() { return this.rowWrapper; }
    public void setRowWrapper(final IEppEpvRowWrapper rowWrapper) { this.rowWrapper = rowWrapper; }

    public boolean isFakeRowWrapper() { return getRowWrapper() instanceof EppFakeRowWrapper; }

    public Collection<EppFControlActionType> getFinalCAList() { return finalCAList; }
    public void setFinalCAList(Collection<EppFControlActionType> finalCAList) { this.finalCAList = finalCAList; }

    public Collection<EppIControlActionType> getIntermediateCAList() { return intermediateCAList; }
    public void setIntermediateCAList(Collection<EppIControlActionType> intermediateCAList) { this.intermediateCAList = intermediateCAList; }

    public EppControlActionType getControlAction() { return this.controlAction; }
    public void setControlAction(final EppControlActionType controlAction) { this.controlAction = controlAction; }

    public Collection<Course> getGridCourseList() { return this.gridCourseList; }
    public void setGridCourseList(Collection<Course> gridCourseList) { this.gridCourseList = gridCourseList; }

    public Course getGridCourse() { return this.gridCourse; }
    public void setGridCourse(Course gridCourse) { this.gridCourse = gridCourse; }

    public List<DevelopGridTerm> getGridTermList() { return this.gridTermList; }
    public void setGridTermList(List<DevelopGridTerm> gridTermList) { this.gridTermList = gridTermList; }

    public DevelopGridTerm getGridTerm() { return this.gridTerm; }
    public void setGridTerm(DevelopGridTerm gridTerm) { this.gridTerm = gridTerm; }

    public boolean isSelfWorkPresent() { return this.selfWorkPresent; }
    public void setSelfWorkPresent(boolean selfWorkPresent) { this.selfWorkPresent = selfWorkPresent; }

    public Set<Long> getSelectedRowIds() { return this.selectedRowIds; }
    public void setSelectedRowIds(Set<Long> selectedRowIds) { this.selectedRowIds = selectedRowIds; }


    public Long getHighlightRowId()
    {
        return highlightRowId;
    }

    public void setHighlightRowId(Long highlightRowId)
    {
        this.highlightRowId = highlightRowId;
    }

    public Boolean getScroll()
    {
        return scroll;
    }

    public void setScroll(Boolean scroll)
    {
        this.scroll = scroll;
    }

    public Long getScrollPos()
    {
        return scrollPos;
    }

    public void setScrollPos(Long scrollPos)
    {
        this.scrollPos = scrollPos;
    }

    public Long getScrollRowId()
    {
        return scrollRowId;
    }

    public void setScrollRowId(Long scrollRowId)
    {
        this.scrollRowId = scrollRowId;
    }
}
