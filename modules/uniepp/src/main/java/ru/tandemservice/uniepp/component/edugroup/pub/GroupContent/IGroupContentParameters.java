package ru.tandemservice.uniepp.component.edugroup.pub.GroupContent;

import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniepp.entity.catalog.EppRealEduGroupCompleteLevel;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;

/**
 * @author vdanilov
 */
public interface IGroupContentParameters {

    OrgUnitHolder getOrgUnitHolder();

    EppRealEduGroupCompleteLevel getLevel();

    EppRealEduGroup getGroup();
}
