package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_2x11x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppEduPlanVersionBlockProfActivityType

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_epv_block_prof_act_type_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_2af7f9d1"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("eduplanversionblock_id", DBType.LONG).setNullable(false), 
				new DBColumn("profactivitytype_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppEduPlanVersionBlockProfActivityType");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eppProfActivityType

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("epp_c_prof_activity_type_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_eppprofactivitytype"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(1200)).setNullable(false), 
				new DBColumn("programsubject_id", DBType.LONG).setNullable(false), 
				new DBColumn("indepthstudy_p", DBType.BOOLEAN), 
				new DBColumn("fromimca_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("removaldate_p", DBType.DATE), 
				new DBColumn("priority_p", DBType.INTEGER).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("eppProfActivityType");

		}


    }
}