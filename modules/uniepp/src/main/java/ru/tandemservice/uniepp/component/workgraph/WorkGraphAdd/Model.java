/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniepp.component.workgraph.WorkGraphAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uni.entity.education.DevelopCombination;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;

/**
 * @author vip_delete
 * @since 03.03.2010
 */
@Input( { @Bind(key = "selectedYear", binding = "selectedYearId") })
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    private EppWorkGraph _workGraph = new EppWorkGraph();
    private ISelectModel _yearEducationProcessListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _developConditionListModel;
    private ISelectModel _developTechListModel;
    private ISelectModel _developGridListModel;
    private ISelectModel _developCombinationListModel;
    private DevelopCombination _developCombination;
    private long _selectedYearId;

    // Getters & Setters

    public EppWorkGraph getWorkGraph()
    {
        return this._workGraph;
    }

    public void setWorkGraph(final EppWorkGraph workGraph)
    {
        this._workGraph = workGraph;
    }

    public ISelectModel getYearEducationProcessListModel()
    {
        return this._yearEducationProcessListModel;
    }

    public void setYearEducationProcessListModel(final ISelectModel yearEducationProcessListModel)
    {
        this._yearEducationProcessListModel = yearEducationProcessListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return this._developFormListModel;
    }

    public void setDevelopFormListModel(final ISelectModel developFormListModel)
    {
        this._developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return this._developConditionListModel;
    }

    public void setDevelopConditionListModel(final ISelectModel developConditionListModel)
    {
        this._developConditionListModel = developConditionListModel;
    }

    public ISelectModel getDevelopTechListModel()
    {
        return this._developTechListModel;
    }

    public void setDevelopTechListModel(final ISelectModel developTechListModel)
    {
        this._developTechListModel = developTechListModel;
    }

    public ISelectModel getDevelopGridListModel()
    {
        return this._developGridListModel;
    }

    public void setDevelopGridListModel(final ISelectModel developGridListModel)
    {
        this._developGridListModel = developGridListModel;
    }

    public ISelectModel getDevelopCombinationListModel()
    {
        return this._developCombinationListModel;
    }

    public void setDevelopCombinationListModel(final ISelectModel developCombinationListModel)
    {
        this._developCombinationListModel = developCombinationListModel;
    }

    public DevelopCombination getDevelopCombination()
    {
        return this._developCombination;
    }

    public void setDevelopCombination(final DevelopCombination developCombination)
    {
        this._developCombination = developCombination;
    }

    public long getSelectedYearId()
    {
        return this._selectedYearId;
    }

    public void setSelectedYearId(final long selectedYearId)
    {
        this._selectedYearId = selectedYearId;
    }
}
