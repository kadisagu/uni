package ru.tandemservice.uniepp.component.registry.RegElementPartAddModule;

import org.tandemframework.core.component.IBusinessComponent;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {
    void save(Model model, IBusinessComponent moduleBusinessComponent);
}
