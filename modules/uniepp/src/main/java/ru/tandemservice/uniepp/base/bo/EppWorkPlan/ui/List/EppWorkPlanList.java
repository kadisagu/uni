/* $Id$ */
package ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.program.bo.EduProgram.EduProgramManager;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.EppWorkPlanManager;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.logic.List.EppWorkPlanListDSHandler;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

/**
 * @author Denis Katkov
 * @since 01.03.2016
 */
@Configuration
public class EppWorkPlanList extends BusinessComponentManager
{
    public static final String EPP_WORK_PLAN_LIST_DS = "eppWorkPlanListDS";
    public static final String CHECKBOX_COLUMN = "checkbox";
    public static final String WORK_PLAN = "workPlan";
    public static final String YEAR_EDUCATION_PROCESS_DS = "yearEducationProcessDS";
    public static final String PROGRAM_SPECIALIZATION_DS = "programSpecializationDS";
    public static final String PROGRAM_FORM_LIST_DS = "programFormListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EppWorkPlanList.EPP_WORK_PLAN_LIST_DS, workPlanListDSColumns(), workPlanListDSHandler()))
                .addDataSource(selectDS(YEAR_EDUCATION_PROCESS_DS, EppWorkPlanManager.instance().yearEducationProcessDSHandler()))
                .addDataSource(selectDS(PROGRAM_SPECIALIZATION_DS, EppWorkPlanManager.instance().programSpecializationDSHandler()))
                .addDataSource(EppWorkPlanManager.instance().programSubjectDSConfig())
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(PROGRAM_FORM_LIST_DS, getName(), EduProgramForm.defaultSelectDSHandler(getName())))
                .addDataSource(EppWorkPlanManager.instance().developConditionListDSConfig())
                .addDataSource(EppWorkPlanManager.instance().programTraitListDSConfig())
                .addDataSource(EppWorkPlanManager.instance().termListDSConfig())
                .addDataSource(EppWorkPlanManager.instance().stateListDSConfig())
                .addDataSource(EduProgramManager.instance().programKindDSConfig())
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler workPlanListDSHandler()
    {
        return new EppWorkPlanListDSHandler(getName());
    }

    @Bean
    public ColumnListExtPoint workPlanListDSColumns()
    {
        return columnListExtPointBuilder(EPP_WORK_PLAN_LIST_DS)
                .addColumn(UniEppUtils.stateColumn(EppWorkPlan.state().code()))
                .addColumn(checkboxColumn(CHECKBOX_COLUMN))
                .addColumn(publisherColumn("title", EppWorkPlan.title().s()).order())
                .addColumn(textColumn(EppWorkPlanListDSHandler.PARAM_PROGRAM_SUBJECT, EppWorkPlan.parent().eduPlanVersion().eduPlan().educationElementSimpleTitle()))
                .addColumn(textColumn(EppWorkPlanListDSHandler.PARAM_PROGRAM_SPECIALIZATION_LIST, EppWorkPlan.parent().title()))
                .addColumn(publisherColumn("eduPlan", EppWorkPlan.parent().eduPlanVersion().title()).order()
                        .publisherLinkResolver(new IPublisherLinkResolver()
                        {
                            @Override
                            public Object getParameters(IEntity entity)
                            {
                                EppEduPlanVersion elPart = ((EppWorkPlan) entity.getProperty(WORK_PLAN)).getEduPlanVersion();
                                return new ParametersMap()
                                        .add(PublisherActivator.PUBLISHER_ID_KEY, elPart.getId());
                            }

                            @Override
                            public String getComponentName(IEntity iEntity)
                            {
                                return "ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub";
                            }
                        }))

                .addColumn(textColumn("individualEduPlan", EppWorkPlan.customEduPlan().title()))
                .addColumn(textColumn("developForm", EppWorkPlan.parent().eduPlanVersion().eduPlan().programForm().title()).order())
                .addColumn(textColumn(EppWorkPlanListDSHandler.PARAM_DEVELOP_CONDITION_LIST, EppWorkPlan.parent().eduPlanVersion().eduPlan().developCondition().title()).order())
                .addColumn(textColumn("developTech", EppWorkPlan.parent().eduPlanVersion().eduPlan().programTrait().title()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("ui:sec.edit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                        alert(EPP_WORK_PLAN_LIST_DS + ".delete.alert", EppWorkPlan.number().s())).permissionKey("ui:sec.delete"))
                .create();
    }
}