package ru.tandemservice.uniepp.component.edustd.EduStdCompetenceTab;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.component.edustd.EduStdCompetenceTab.Model.SkillGroupBlock;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setEppStateEduStandard(this.getNotNull(EppStateEduStandard.class, model.getEppStateEduStandard().getId()));

        final MQBuilder builder = new MQBuilder(EppStateEduStandardSkill.ENTITY_NAME, "sk");
        builder.add(MQExpression.eq("sk", EppStateEduStandardSkill.parent().s(), model.getEppStateEduStandard()));
        builder.addOrder("sk", EppStateEduStandardSkill.code().s());
        final List<EppStateEduStandardSkill> skillList = builder.getResultList(this.getSession());

        final Map<String, List<EppStateEduStandardSkill>> code2skillList = SafeMap.get(ArrayList.class);
        for (final EppStateEduStandardSkill skill : skillList)
        {
            code2skillList.get(skill.getSkillGroup().getCode()).add(skill);
        }

        final List<SkillGroupBlock> blockList = new ArrayList<SkillGroupBlock>();
        for (final Entry<String, List<EppStateEduStandardSkill>> entry : code2skillList.entrySet())
        {
            if (!entry.getValue().isEmpty())
            {
                final String title = entry.getValue().get(0).getSkillGroup().getTitle();
                final StaticListDataSource<EppStateEduStandardSkill> dataSource = new StaticListDataSource<EppStateEduStandardSkill>();

                dataSource.addColumn(new SimpleColumn("Код", EppStateEduStandardSkill.code().s()).setWidth(5).setOrderable(false));
                dataSource.addColumn(new SimpleColumn("Описание", EppStateEduStandardSkill.comment().s()).setOrderable(false));
                dataSource.addColumn(new SimpleColumn("Тип", EppStateEduStandardSkill.skillType().title().s()).setOrderable(false));

                dataSource.setCountRow(entry.getValue().size());
                dataSource.setRowList(entry.getValue());

                blockList.add(new SkillGroupBlock(title, dataSource));
            }
        }

        model.setBlockList(blockList);
    }
}
