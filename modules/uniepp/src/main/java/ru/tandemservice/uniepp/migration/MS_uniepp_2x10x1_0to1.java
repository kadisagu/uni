package ru.tandemservice.uniepp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniepp_2x10x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppRegistryStructure
		// Пустые поля "Условное сокращение" (abbreviation) и "Сокращенное название" (shortTitle) заполняются названием

		//  свойство shortTitle стало обязательным
		{
			tool.executeUpdate("update epp_c_reg_struct_t set shorttitle_p = title_p where shorttitle_p is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_c_reg_struct_t", "shorttitle_p", false);

		}

		//  свойство abbreviation стало обязательным
		{
			tool.executeUpdate("update epp_c_reg_struct_t set abbreviation_p = title_p where abbreviation_p is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("epp_c_reg_struct_t", "abbreviation_p", false);

		}


    }
}