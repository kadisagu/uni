/* $Id$ */
package ru.tandemservice.uniepp.dao.eduplan;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppCustomPlanWrapper;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;

/**
 * @author Nikolay Fedorovskih
 * @since 21.08.2015
 */
public interface IEppCustomEduPlanDAO
{
    SpringBeanCache<IEppCustomEduPlanDAO> instance = new SpringBeanCache<>(IEppCustomEduPlanDAO.class.getName());

    /** @return правило нумерации индивидуального УП */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    INumberGenerationRule<EppCustomEduPlan> getCustomEduPlanNumberGenerationRule();

    /** Сохранение индивидуального УП. Номер выдается автоматически. */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void saveCustomEduPlan(EppCustomEduPlan customEduPlan);

    /** Сохранение содержимого индивидуального УП. */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void saveCustomEduPlanContent(EppCustomEduPlan customEduPlan, IEppCustomPlanWrapper planWrapper);
}