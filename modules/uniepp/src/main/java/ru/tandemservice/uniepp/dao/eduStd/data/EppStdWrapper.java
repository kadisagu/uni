package ru.tandemservice.uniepp.dao.eduStd.data;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.tandemframework.core.entity.EntityBase;

import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;

/**
 * @author vdanilov
 */
public class EppStdWrapper extends EntityBase implements IEppStdWrapper {

    protected static final Logger logger = Logger.getLogger(EppStdWrapper.class);

    private final EppStateEduStandard stateEduStandard;
    @Override public EppStateEduStandard getStateEduStandard() { return this.stateEduStandard; }
    @Override public Long getId() { return this.stateEduStandard.getId(); }

    private final Map<Long, IEppStdRowWrapper> rowMap = new LinkedHashMap<Long, IEppStdRowWrapper>();
    @Override public Map<Long, IEppStdRowWrapper> getRowMap() { return this.rowMap ; }

    public EppStdWrapper(final EppStateEduStandard stateEduStandard) {
        this.stateEduStandard = stateEduStandard;
    }

    @Override public void setId(final Long id) {
        throw new UnsupportedOperationException();
    }
    @Override public void setProperty(final String propertyName, final Object propertyValue) {
        throw new UnsupportedOperationException();
    }
    @Override public Object getProperty(final Object propertyPath) {
        try {
            return super.getProperty(propertyPath);
        } catch(final Throwable t) {
            return null;
        }
    }

    /**
     * порядковый номер записи (нумерация ведется в рамках родительской записи)
     * @param eppEpvRowWrapper - запись
     * @returnпорядковый номер
     */
    protected int getRecordHierarchyIndex(final IEppStdRowWrapper eppStdRowWrapper) {
        int index = 0;
        for (final IEppStdRowWrapper wrapper : this.getRowMap().values()) {
            if (eppStdRowWrapper == wrapper) { break; }
            if (eppStdRowWrapper.getHierarhyParent() == wrapper.getHierarhyParent()) {

                // если это строки из другого блока (не основного - то их уитывать при нумерации не нужно - она уникальна в рамках одного блока)
                if (!wrapper.getOwner().isRootBlock()) {
                    if (!eppStdRowWrapper.getOwner().equals(wrapper.getOwner())) {
                        continue;
                    }
                }


                index ++;
            }
        }
        return index;
    }

}
