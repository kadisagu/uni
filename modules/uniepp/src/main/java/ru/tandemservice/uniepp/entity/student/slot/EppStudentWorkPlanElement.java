package ru.tandemservice.uniepp.entity.student.slot;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.gen.EppStudentWorkPlanElementGen;

import java.util.Comparator;

/**
 * Мероприятие студента из РУП (МСРП)
 */
public class EppStudentWorkPlanElement extends EppStudentWorkPlanElementGen implements ITitled
{
    public static final Comparator<EppStudentWorkPlanElement> FULL_FIO_COMPARATOR =
            ((Comparator<EppStudentWorkPlanElement>)(a, b) -> CommonCollator.RUSSIAN_COLLATOR.compare(a.getStudent().getFullFio(), b.getStudent().getFullFio()))
                    .thenComparingLong(EntityBase::getId); // Для использования в TreeSet/TreeMap это необходимо

    public EppStudentWorkPlanElement() {}

    public EppStudentWorkPlanElement(Student student, EppRegistryElementPart registryElementPart, EppYearEducationProcess year, Course course, Term term, YearDistributionPart part) {
        this.setStudent(student);
        this.setRegistryElementPart(registryElementPart);
        this.setYear(year);
        this.setCourse(course);
        this.setTerm(term);
        this.setPart(part);
    }

    @Override
    public String getTitle()
    {
        if (getRegistryElementPart() == null) {
            return this.getClass().getSimpleName();
        }
        return "МСРП: " + getRegistryElementPart().getTitleWithNumber() + " " + getYear().getTitle() + " " + getTerm().getTitle() + " (" + getStudent().getTitle() + ")";
    }

    public OrgUnit getStudentCurrentGroupOu()
    {
        return getStudent().getEducationOrgUnit().getGroupOrgUnit();
    }

    public OrgUnit getTutorOu()
    {
        return getRegistryElementPart().getTutorOu();
    }

//    @Override
//    public int compareTo(EppStudentWorkPlanElement o)
//    {
//        return CommonCollator.RUSSIAN_COLLATOR.compare(getStudent().getFullFio(), o.getStudent().getFullFio());
//    }

    /**
     * @deprecated use getRegistryElementPart
     */
    @Deprecated
    public EppRegistryElementPart getActivityElementPart() {
        return getRegistryElementPart();
    }

    /**
     * Получение части учебного года
     * Тут запрос к базе!
     */
    public EppYearPart getEppYearPart()
    {
        if (this.getYear() != null && this.getPart() != null)
            return IUniBaseDao.instance.get().getByNaturalId(new EppYearPart.NaturalId(this.getYear(), this.getPart()));

        return null;
    }
}