package ru.tandemservice.uniepp.component.settings.TutorOrgUnitImtsaCode;

import org.tandemframework.shared.commonbase.tapestry.component.list.SimpleListDataHolder;

import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;

public class Model
{

    private SimpleListDataHolder<EppTutorOrgUnit> tutorOrgUnitListHolder = new SimpleListDataHolder<EppTutorOrgUnit>();
    public SimpleListDataHolder<EppTutorOrgUnit> getTutorOrgUnitListHolder() { return this.tutorOrgUnitListHolder; }
    public void setTutorOrgUnitListHolder(SimpleListDataHolder<EppTutorOrgUnit> tutorOrgUnitListHolder) { this.tutorOrgUnitListHolder = tutorOrgUnitListHolder; }

    public EppTutorOrgUnit getTutorOrgUnit() { return getTutorOrgUnitListHolder().getCurrent(); }
    public String getNumber() { return String.valueOf(getTutorOrgUnitListHolder().getCurrentNumber()); }

    public String getCodeImtsaFieldId() {
        return getCodeImtsaFieldId(getTutorOrgUnit().getId());
    }

    public String getCodeImtsaFieldId(Long tutorOrgUnitId) {
        return "code_imtsa_"+tutorOrgUnitId;
    }




}
