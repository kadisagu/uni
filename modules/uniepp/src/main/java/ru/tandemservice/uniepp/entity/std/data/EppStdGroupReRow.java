package ru.tandemservice.uniepp.entity.std.data;

import ru.tandemservice.uniepp.entity.std.data.gen.EppStdGroupReRowGen;

/**
 * Запись ГОС (группа дисциплин)
 */
public class EppStdGroupReRow extends EppStdGroupReRowGen
{
    @Override public boolean isStructureElement() { return true; }

}