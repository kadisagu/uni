package ru.tandemservice.uniepp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings("unused")
public class MS_uniepp_2x9x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppCustomEpvRowTerm
		{
            // Переименовываем reattestation в needRetake,
            // делаем колонку нулабельной
            // и проставляем null всем строкам без перезачтения и переаттестации
            tool.renameColumn("epp_custom_eduplan_row_term_t", "reattestation_p", "needretake_p");
            tool.setColumnNullable("epp_custom_eduplan_row_term_t", "needretake_p", true);
            tool.executeUpdate("update epp_custom_eduplan_row_term_t set needretake_p=null where needretake_p=? and reexamination_p=?", false, false);

		    // reexamination просто удаляем
			tool.dropColumn("epp_custom_eduplan_row_term_t", "reexamination_p");
		}


		////////////////////////////////////////////////////////////////////////////////
		// сущность eppWorkPlanRow
		{
            // создано свойство needRetake
			tool.createColumn("epp_wprow_t", new DBColumn("needretake_p", DBType.BOOLEAN));

            // удалено свойство excludedRow
            // Это свойство нельзя было нигде задать. Поэтому мы колонку просто дропаем без переноса данных.
            // Но если кто-то смог туда что-то записать, пусть у него упадет ошибку - допилим миграцию.
            if (tool.hasResultRows("select id from epp_wprow_t where excludedrow_p=?", true)) {
                throw new IllegalStateException("Tell developers: \"We have data!\"");
            }
            tool.dropColumn("epp_wprow_t", "excludedrow_p");
        }

    }
}