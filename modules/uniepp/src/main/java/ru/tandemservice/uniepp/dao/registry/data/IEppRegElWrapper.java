package ru.tandemservice.uniepp.dao.registry.data;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.dao.registry.data.EppRegElementKeyGenerator.KeyGenRule;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public interface IEppRegElWrapper extends IEppBaseRegElWrapper<EppRegistryElement>, IEppStateObject {

    /** @return элемент реестра */
    @Override
    EppRegistryElement getItem();

    /** @return { номер части -> оболочка части } */
    Map<Integer, IEppRegElPartWrapper> getPartMap();

    /** @return тип элемента реестра (дисциплина / практика / ГИА) */
    EppRegistryStructure getRegistryElementType();

    /** @return читающее подразделение */
    OrgUnit getOwner();

    /** @return Нагрузка (в часах) */
    long getLoadSize();

    /** @return суммарная нагрузка дочерних элементов */
    double getChildrenLoadAsDouble(String loadFullCode);

    /** @return Трудоемкость */
    long getLabor();

    /** @return Кол-во недель (для практик и ГИА) */
    long getWeeks();

    /**
     * Генерация уникального ключа по алгоритму. Ключи нужны для сравнения двух элементов.
     * В ключ входят номера, нагрузки, контроли - то, что определяет содержание элемента (части, модуля).
     * Ключ родительского враппера содержит ключи всех детей.
     */
    String generateKey(KeyGenRule rule);

    /** @return Способ деления потоков */
    EppEduGroupSplitVariant getEduGroupSplitVariant();

    /** @return Ограничение способа деления по виду потока */
    boolean isEduGroupSplitByType();

    /** @return Вид потоков при ограничении способа деления*/
    List<EppGroupType> getSplitGroupTypes();
}
