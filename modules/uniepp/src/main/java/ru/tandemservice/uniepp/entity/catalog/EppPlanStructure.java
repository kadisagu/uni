package ru.tandemservice.uniepp.entity.catalog;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniepp.catalog.bo.EppPlanStructure.ui.AddEdit.EppPlanStructureAddEdit;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppPlanStructureGen;

import java.util.*;

/**
 * Структура ГОС/УП
 */
public class EppPlanStructure extends EppPlanStructureGen implements IHierarchyItem, IDynamicCatalogItem//, IPrioritizedCatalogItem
{
	private static final List<String> HIDDEN_PROPERTIES = ImmutableList.of(EppPlanStructure.L_PARENT, EppPlanStructure.P_PRIORITY);

    // "Факультативные дисциплины" и "Специальные факультативные дисциплины"
    public static final Set<String> OPTIONAL_DISCIPLINES = ImmutableSet.of(
            EppPlanStructureCodes.FGOS_CYCLES_OPTIONAL_DISCIPLINES,
            EppPlanStructureCodes.FGOS_CYCLES_SPECIAL_OPTIONAL_DISCIPLINES,
            EppPlanStructureCodes.FGOS_2013_OPTIONAL_DISCIPLINES,
            EppPlanStructureCodes.GOS_CYCLES_OPTIONAL_DISCIPLINES,
            EppPlanStructureCodes.GOS_CYCLES_SPECIAL_OPTIONAL_DISCIPLINES
    );

	@Override
	public IHierarchyItem getHierarhyParent()
	{
		return this.getParent();
	}

	/**
	 * @return полный путь по иерархии (по всем названиям вышестоящих элементов)
	 */
	@Override
	@EntityDSLSupport
	public String getHierarchyTitle()
	{
		if (null == this.getParent())
		{
			return this.getTitle();
		}
		return this.getParent().getHierarchyTitle() + " / " + this.getTitle();
	}

	/**
	 * @return (сокращенное называние, если есть и название)
	 */
	public String getFullTitle()
	{
		return StringUtils.stripStart(StringUtils.trimToEmpty(this.getShortTitle()) + ". ", ". ") + this.getTitle();
	}

	protected void fillPriorityTrace(List<Integer> trace)
	{
		if (null != this.getParent())
		{
			this.getParent().fillPriorityTrace(trace);
		}
		trace.add(this.getPriority());
	}

	// (╮°-°)╮┳━━┳ Взял стол. ( ╯°□°)╯ ┻━━┻ И устроил дебош!!!
	public List<Integer> getPriorityTrace()
	{
		final List<Integer> trace = new ArrayList<>(4); // смысла кэшировать это нет - глубина вложенности не более 4х
		fillPriorityTrace(trace);
		return trace;
	}

	public static Comparator<EppPlanStructure> COMPARATOR = new Comparator<EppPlanStructure>()
	{
		@Override
		public int compare(final EppPlanStructure o1, final EppPlanStructure o2)
		{
			return this.compare(o1.getPriorityTrace().iterator(), o2.getPriorityTrace().iterator());
		}

		private <T extends Comparable<T>> int compare(final Iterator<T> i1, final Iterator<T> i2)
		{
			while (i1.hasNext() && i2.hasNext())
			{
				final int result = i1.next().compareTo(i2.next());
				if (result != 0)
				{
					return result;
				}
			}

			if (i1.hasNext())
			{
				return 1;
			}
			if (i2.hasNext())
			{
				return -1;
			}
			return 0;
		}
	};

	public static IDynamicCatalogDesc getUiDesc()
	{
		return new BaseDynamicCatalogDesc()
		{
			@Override
			public Collection<String> getHiddenFields()
			{
				return HIDDEN_PROPERTIES;
			}

			@Override
			public String getAddEditComponentName()
			{
				return EppPlanStructureAddEdit.class.getSimpleName();
			}
		};
	}

}