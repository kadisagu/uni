package ru.tandemservice.uniepp.component.base.ChangeState;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;

/**
 * @author vdanilov
 */
public class DAO extends UniBaseDao implements IDAO {

    @Override
    public void prepare(final Model model) {
        model.setStateObject((IEppStateObject) this.getNotNull(model.getId()));
    }

    @Override
    public void changeState(final Model model, final String stateCode) {
        // TODO: lock & validate
        model.setStateObject((IEppStateObject) this.getNotNull(model.getId()));
        model.getStateObject().setProperty("state", this.getCatalogItem(EppState.class, stateCode));
        this.getSession().update(model.getStateObject());
    }

}
