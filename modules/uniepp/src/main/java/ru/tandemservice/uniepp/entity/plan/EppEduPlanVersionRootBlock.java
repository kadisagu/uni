package ru.tandemservice.uniepp.entity.plan;

import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionRootBlockGen;

import java.util.Arrays;
import java.util.List;

/**
 * Общий блок версии УП
 */
public class EppEduPlanVersionRootBlock extends EppEduPlanVersionRootBlockGen
{
    public static final String HARD_TITLE = "Общий блок";

    @Override
    public boolean isRootBlock()
    {
        return true;
    }

    @Override
    public String getEducationElementSimpleTitle()
    {
        return getEduPlanVersion().getEduPlan().getEducationElementSimpleTitle();
    }

    @Override
    public String getTitle() {
        return HARD_TITLE;
    }

    @Override
    public String getEducationElementTitle() {
        return getEduPlanVersion().getEducationElementTitle();
    }

    @Override
    public String getSpecializationExtendedTitle()
    {
        final EppEduPlan plan = getEduPlanVersion().getEduPlan();
        final List<String> parts = Arrays.asList(
                plan.getProgramForm().getShortTitle(),
                plan.getDevelopGrid().getTitle(),
                plan.getProgramTrait() != null ? plan.getProgramTrait().getShortTitle() : "",
                String.valueOf(plan.getEduStartYear()) + (plan.getEduEndYear() != null ? ("-" + plan.getEduEndYear()) : "") + " г."
        );
        return getTitle() + " (" + CommonBaseStringUtil.joinNotEmpty(parts, ", ") + ")";
    }
}