package ru.tandemservice.uniepp.component.workplan.WorkPlanPartSetEdit;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author vdanilov
 */
public interface IDAO extends IPrepareable<Model> {

    void prepareWorkGraph(Model model);

    void save(Model model);

}
