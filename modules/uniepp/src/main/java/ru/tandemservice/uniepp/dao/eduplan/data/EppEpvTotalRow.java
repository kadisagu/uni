package ru.tandemservice.uniepp.dao.eduplan.data;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.log4j.Logger;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.*;

/**
 * @author vdanilov
 */
public abstract class EppEpvTotalRow extends IdentifiableWrapper<IEntity> {
    private static final long serialVersionUID = 1L;
    protected static final Logger logger = Logger.getLogger(EppEpvTotalRow.class);


    public EppEpvTotalRow(final String title) {
        super((long)title.hashCode(), title);
    }

    public String getControlActionValue(final String actionFullCode) {
        return "";
    }

    public String getLoadValue(final int term, final String loadFullCode) {
        return "";
    }

    public boolean isMergeTermCells() { return false; }

    public static class EppEpvTotalControlRow extends EppEpvTotalRow {
        private static final long serialVersionUID = 1L;
        private static final Set<String> LOAD_COLUMN_ODES = new HashSet<>(Arrays.asList(EppELoadType.FULL_CODE_AUDIT, EppALoadType.FULL_CODE_TOTAL_LECTURES));

        private final Collection<IEppEpvRowWrapper> filteredRows;
        private final List<String> fullCodes;

        public EppEpvTotalControlRow(final Collection<IEppEpvRowWrapper> filteredRows, final String title, final String... fullCodes) {
            super(title);
            this.filteredRows = filteredRows;
            this.fullCodes = Arrays.asList(fullCodes);
        }

        @Override
        public String getControlActionValue(final String actionFullCode) {
            if (this.fullCodes.contains(actionFullCode)) {
                int result = 0;
                for (final IEppEpvRowWrapper source: this.filteredRows) {
                    if (Boolean.TRUE.equals(source.getUsedInActions())) {
                        result += source.getActionSize(null, actionFullCode);
                    }
                }
                return String.valueOf(result);
            }
            return "";
        }

        @Override public String getLoadValue(final int term, final String loadFullCode) {
            if ((term > 0) && EppEpvTotalControlRow.LOAD_COLUMN_ODES.contains(loadFullCode)) {
                int result = 0;
                for (final IEppEpvRowWrapper source: this.filteredRows) {
                    for (final String fullCode: this.fullCodes) {
                        if (Boolean.TRUE.equals(source.getUsedInActions())) {
                            result += source.getActionSize(term, fullCode);
                        }
                    }
                }
                return String.valueOf(result);
            }
            return "";
        }

        @Override public boolean isMergeTermCells() { return true; }
    }

    public static Collection<IEppEpvRowWrapper> filterRowsForTotalStats(Collection<IEppEpvRowWrapper> versionRows)
    {
        return CollectionUtils.select(versionRows, wrapper -> !wrapper.isSelectedItem() && (wrapper.getRow() instanceof EppEpvTermDistributedRow));
    }

    public static EppEpvTotalRow getTotalRowAvgLoad(final IEppEpvBlockWrapper versionWrapper, final Collection<IEppEpvRowWrapper> filteredRows)
    {
        // TODO: исключать ненужные элементы согласно правилам исключения из нагрузки

        return new EppEpvTotalRow("Средняя нагрузка") {
            private static final long serialVersionUID = 1L;
            @Override public String getLoadValue(final int term, final String loadFullCode) {
                if (term > 0) {
                    double result = 0;
                    for (final IEppEpvRowWrapper source: filteredRows) {
                        if (Boolean.TRUE.equals(source.getUsedInLoad())) {
                            result += source.getTotalInTermLoad(term, loadFullCode);
                        }
                    }
                    if (result <= 0) { return ""; }

                    final int size = versionWrapper.getTermSize(loadFullCode, term);
                    return UniEppUtils.formatLoad(size > 0 ? (result / size) : 0, true);
                }
                return "";
            }
        };
    }

    public static EppEpvTotalRow getTotalRowTotal(final Collection<IEppEpvRowWrapper> filteredRows, String title)
    {
        // TODO: исключать ненужные элементы согласно правилам исключения из нагрузки

        return new EppEpvTotalRow(title) {
            private static final long serialVersionUID = 1L;
            @Override public String getLoadValue(final int term, final String loadFullCode) {
                double result = 0;
                for (final IEppEpvRowWrapper source: filteredRows) {
                    if (Boolean.TRUE.equals(source.getUsedInLoad())) {
                        result += source.getTotalDisplayableLoad(term, loadFullCode);
                    }
                }
                return UniEppUtils.formatLoad(result, true);
            }
        };
    }

    public static EppEpvTotalRow getTotalRowTotal(final Collection<IEppEpvRowWrapper> filteredRows)
    {
        return getTotalRowTotal(filteredRows, "Всего");
    }

}
