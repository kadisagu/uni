/* $Id: DataCorrectionExtManager.java 7864 2016-03-29 09:45:46Z rsizonenko $ */
package ru.tandemservice.uniepp.base.ext.DataCorrection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.DataCorrection.DataCorrectionManager;
import org.tandemframework.shared.commonbase.base.bo.DataCorrection.util.DataCorrectionAction;
import ru.tandemservice.uniepp.base.ext.DataCorrection.ui.Pub.DataCorrectionPubExt;

/**
 * @author rsizonenko
 * @since 30/03/16
 */
@Configuration
public class DataCorrectionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private DataCorrectionManager _dataCorrectionManager;

    @Bean
    public ItemListExtension<DataCorrectionAction> actionListExtension()
    {
        final IItemListExtensionBuilder<DataCorrectionAction> itemListExtensionBuilder = itemListExtension(_dataCorrectionManager.actionExtPoint());

        return itemListExtensionBuilder
            .add("unieppRegistryElementCorrection", new DataCorrectionAction("organization", "unieppRegistryElementCorrection", "onClickShowRegistryElementCorrection", DataCorrectionPubExt.ORG_DATA_CORRECTION_PUB_ADDON_NAME))
            .add("unieppEduPlanVersionSpecBlockCorrection", new DataCorrectionAction("organization", "unieppEduPlanVersionSpecBlockCorrection", "onClickShowEduPlanVersionSpecBlockCorrection", DataCorrectionPubExt.ORG_DATA_CORRECTION_PUB_ADDON_NAME))
            .create();
    }
}
