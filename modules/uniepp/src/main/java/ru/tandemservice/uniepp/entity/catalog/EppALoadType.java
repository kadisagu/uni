package ru.tandemservice.uniepp.entity.catalog;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppALoadTypeGen;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Вид аудиторной нагрузки
 */
public class EppALoadType extends EppALoadTypeGen
{
    @Override public void setCode(final String code) {
        Preconditions.checkState(EppALoadTypeCodes.CODES.contains(code));
        super.setCode(code);
    }

    public static final String CATALOG_CODE = EppALoadTypeGen.ENTITY_NAME;
    @Override public String getCatalogCode() { return EppALoadType.CATALOG_CODE; }

    public static final String TYPE_LECTURES = EppALoadTypeCodes.TYPE_LECTURES;
    public static final String TYPE_PRACTICE = EppALoadTypeCodes.TYPE_PRACTICE;
    public static final String TYPE_LABS = EppALoadTypeCodes.TYPE_LABS;

    public static final String FULL_CODE_TOTAL_LECTURES = EppLoadType.getFullCode(EppALoadType.CATALOG_CODE, EppALoadType.TYPE_LECTURES);
    public static final String FULL_CODE_TOTAL_PRACTICE = EppLoadType.getFullCode(EppALoadType.CATALOG_CODE, EppALoadType.TYPE_PRACTICE);
    public static final String FULL_CODE_TOTAL_LABS = EppLoadType.getFullCode(EppALoadType.CATALOG_CODE, EppALoadType.TYPE_LABS);

    @NotNull public static String iFullCode(@NotNull String loadTypeFullCode) { return Preconditions.checkNotNull(loadTypeFullCode) + ".i"; }
    @NotNull public static String eFullCode(@NotNull String loadTypeFullCode) { return Preconditions.checkNotNull(loadTypeFullCode) + ".e"; }

    // interactive
    public static final String FULL_CODE_TOTAL_LECTURES_I = iFullCode(FULL_CODE_TOTAL_LECTURES);
    public static final String FULL_CODE_TOTAL_PRACTICE_I = iFullCode(FULL_CODE_TOTAL_PRACTICE);
    public static final String FULL_CODE_TOTAL_LABS_I = iFullCode(FULL_CODE_TOTAL_LABS);

    // electronic
    public static final String FULL_CODE_TOTAL_LECTURES_E = eFullCode(FULL_CODE_TOTAL_LECTURES);
    public static final String FULL_CODE_TOTAL_PRACTICE_E = eFullCode(FULL_CODE_TOTAL_PRACTICE);
    public static final String FULL_CODE_TOTAL_LABS_E = eFullCode(FULL_CODE_TOTAL_LABS);

    public static final List<String> FULL_CODES;
    static {
        final ImmutableList.Builder<String> builder = ImmutableList.builder();
        EppALoadTypeCodes.CODES.forEach(code -> builder.add(EppLoadType.getFullCode(EppALoadType.CATALOG_CODE, code)));
        FULL_CODES = builder.build();
    }

    public boolean isLectures() { return EppALoadType.TYPE_LECTURES.equals(this.getCode()); }
    public boolean isPractice() { return EppALoadType.TYPE_PRACTICE.equals(this.getCode()); }
    public boolean isLabs() { return EppALoadType.TYPE_LABS.equals(this.getCode()); }
}