/* $Id:$ */
package ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.AdditionalProfList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.EppEduPlanManager;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.AdditionalProfAddEdit.EppEduPlanAdditionalProfAddEdit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.List.EppEduPlanListBaseUI;

/**
 * @author oleyba
 * @since 8/28/14
 */
@State({
    @Bind(key = "programKindId", binding = "programKindHolder.id"),
    @Bind(key="orgUnitId", binding="orgUnitHolder.id")
})
public class EppEduPlanAdditionalProfListUI extends EppEduPlanListBaseUI
{
    public void onClickAddEduPlan() {
        _uiActivation
            .asRegionDialog(EppEduPlanAdditionalProfAddEdit.class)
            .parameter(UIPresenter.PUBLISHER_ID, null)
            .parameter(EppEduPlanManager.BIND_PROGRAM_KIND_ID, getProgramKindHolder().getId())
            .parameter(EppEduPlanManager.BIND_ORG_UNIT_ID, getOrgUnitHolder().getId())
            .activate();
    }

    public void onClickEditEduPlan() {
        _uiActivation
            .asRegionDialog(EppEduPlanAdditionalProfAddEdit.class)
            .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
            .activate();
    }
}