package ru.tandemservice.uniepp.dao.eduplan.export;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.hibernate.Session;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelHigh;
import ru.tandemservice.uni.entity.catalog.gen.EducationLevelsHighSchoolGen;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.plan.gen.EppEduPlanVersionBlockGen;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * 
 * @author nkokorina
 *
 */
public class EppEduPlanVersionExportDAO extends UniBaseDao implements IEppEduPlanVersionExportDAO
{
    @Override
    public byte[] export(final Collection<Long> versionIds)
    {
        // todo DEV-6000
        return null;
//        final Session session = this.getSession();
//
//        // получаем данные из уп(в) и гос
//        final MQBuilder builder = new MQBuilder(EppEduPlanVersion.ENTITY_NAME, "epv");
//        builder.add(MQExpression.in("epv", EppEduPlanVersion.id().s(), versionIds));
//        final List<EppEduPlanVersion> versionDataList = builder.getResultList(session);
//
//        // данные о курсах студентов, к которым привязана весия
//        final MQBuilder courseBuilder = new MQBuilder(EppStudent2EduPlanVersion.ENTITY_NAME, "sepv", new String[] { EppStudent2EduPlanVersion.eduPlanVersion().id().s() });
//        courseBuilder.add(MQExpression.isNull("sepv", EppStudent2EduPlanVersion.removalDate()));
//        courseBuilder.add(MQExpression.in("sepv", EppStudent2EduPlanVersion.eduPlanVersion().id().s(), versionIds));
//        courseBuilder.addDomain("s", Student.ENTITY_NAME);
//        courseBuilder.addSelect("s", new String[] { Student.course().intValue().s() });
//        courseBuilder.add(MQExpression.eqProperty("sepv", EppStudent2EduPlanVersion.student().id().s(), "s", Student.id().s()));
//        courseBuilder.setNeedDistinct(true);
//        final List<Object[]> courseDataList = courseBuilder.getResultList(session);
//
//        final Map<Long, EppEduPlanVersion> idVersion2version = new HashMap<Long, EppEduPlanVersion>();
//        for (final EppEduPlanVersion element : versionDataList)
//        {
//            idVersion2version.put(element.getId(), element);
//            if (! (element.getEduPlan().getEducationLevelHighSchool().getEducationLevel() instanceof EducationLevelHigh)) {
//                throw new ApplicationException("Экспортировать в формат ИМЦА можно только УП для направлений ВПО очной формы обучения.");
//            }
//        }
//
//        final Map<Long, Set<Integer>> idVersion2cours = SafeMap.get(HashSet.class);
//        for (final Object[] element : courseDataList)
//        {
//            idVersion2cours.get(element[0]).add((Integer)element[1]);
//        }
//
//        ByteArrayOutputStream out;
//        try
//        {
//            out = new ByteArrayOutputStream();
//            final ZipOutputStream zipOut = new ZipOutputStream(out);
//            boolean smthWasPackaged = false;
//            for (final Long id : versionIds)
//            {
//                final EppEduPlanVersion version = idVersion2version.get(id);
//                final Set<Integer> course = idVersion2cours.get(version.getId());
//                final String fileName = EppEPVExportUtils.getFileName(version, course);
//
//                if ((null != fileName))
//                {
//                    zipOut.putNextEntry(new ZipEntry(fileName));
//                    final byte[] content = EppEPVExportUtils.getFileContent(version);
//                    if (null != content)
//                    {
//                        zipOut.write(content);
//                    }
//                    zipOut.closeEntry();
//                    smthWasPackaged = true;
//                }
//            }
//            if (!smthWasPackaged)
//            {
//                UserContext.getInstance().getErrorCollector().add("Экспорт в формат ИМЦА возможен только для УП, созданных на направления ВПО.");
//                return null;
//            }
//            zipOut.close();
//            return out.toByteArray();
//        }
//        catch (final IOException e)
//        {
//            throw new RuntimeException(e.getMessage(), e);
//        }
    }

    @Override
    public HashSet<Integer> getVersionCourseSet(final Long versionId)
    {
        final MQBuilder courseBuilder = new MQBuilder(EppStudent2EduPlanVersion.ENTITY_NAME, "sepv", new String[] { EppStudent2EduPlanVersion.student().course().intValue().s() });
        courseBuilder.add(MQExpression.isNull("sepv", EppStudent2EduPlanVersion.removalDate()));
        courseBuilder.add(MQExpression.eq("sepv", EppStudent2EduPlanVersion.eduPlanVersion().id().s(), versionId));
        courseBuilder.setNeedDistinct(true);
        final List<Integer> courseDataList = courseBuilder.getResultList(this.getSession());

        return new HashSet<Integer>(courseDataList);
    }

    @Override
    public List<EppEduPlanVersionBlock> getVersionBlockList(final Long versionId)
    {
        // todo DEV-6000
        final MQBuilder builder = new MQBuilder(EppEduPlanVersionBlockGen.ENTITY_CLASS, "block");
//        builder.add(MQExpression.eq("block", EppEduPlanVersionBlockGen.eduPlanVersion().id().s(), versionId));
//        builder.addJoinFetch("block", EppEduPlanVersionBlockGen.educationLevelHighSchool().toString(), "eduHS");
//        builder.addJoinFetch("eduHS", EducationLevelsHighSchoolGen.educationLevel().toString(), "eduLevel");
//        builder.addOrder("eduHS", EducationLevelsHighSchoolGen.title().toString());

        final List<EppEduPlanVersionBlock> list = builder.getResultList(this.getSession());
        Collections.sort(list);

        return list;
    }

    @Override
    public List<EppEduPlanVersionWeekType> getEduPlanVersionWeeks(final Long versionId)
    {
        final MQBuilder builder = new MQBuilder(EppEduPlanVersionWeekType.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", EppEduPlanVersionWeekType.eduPlanVersion().id().s(), versionId));
        return builder.getResultList(this.getSession());
    }

    @Override
    public Map<Integer, Integer> getCourse2GroupCount(final Long versionId)
    {
        final MQBuilder builder = new MQBuilder(EppStudent2EduPlanVersion.ENTITY_NAME, "s2epv", new String[] { EppStudent2EduPlanVersion.student().course().intValue().s(), EppStudent2EduPlanVersion.student().group().id().s() });
        builder.add(MQExpression.isNull("s2epv", EppStudent2EduPlanVersion.removalDate()));
        builder.add(MQExpression.eq("s2epv", EppStudent2EduPlanVersion.eduPlanVersion().id().s(), versionId));
        final List<Object[]> list = builder.getResultList(this.getSession());

        final Map<Integer, Set<Long>> course2group = SafeMap.get(HashSet.class);
        for (final Object[] obj : list)
        {
            final Integer course = (Integer)obj[0];
            final Long groupId = (Long)obj[1];
            course2group.get(course).add(groupId);
        }

        final Map<Integer, Integer> course2groupCount = new HashMap<Integer, Integer>();
        for (final Integer course : course2group.keySet())
        {
            course2groupCount.put(course, course2group.get(course).size());
        }

        return course2groupCount;
    }

    @Override
    public Map<Integer, Integer> getCourse2StudentCount(final Long versionId)
    {
        final MQBuilder builder = new MQBuilder(EppStudent2EduPlanVersion.ENTITY_NAME, "s2epv", new String[] { EppStudent2EduPlanVersion.student().course().intValue().s(), EppStudent2EduPlanVersion.student().id().s() });
        builder.add(MQExpression.isNull("s2epv", EppStudent2EduPlanVersion.removalDate()));
        builder.add(MQExpression.eq("s2epv", EppStudent2EduPlanVersion.eduPlanVersion().id().s(), versionId));
        final List<Object[]> list = builder.getResultList(this.getSession());

        final Map<Integer, Set<Long>> course2student = SafeMap.get(HashSet.class);
        for (final Object[] obj : list)
        {
            final Integer course = (Integer)obj[0];
            final Long studentId = (Long)obj[1];
            course2student.get(course).add(studentId);
        }

        final Map<Integer, Integer> course2studCount = new HashMap<Integer, Integer>();
        for (final Integer course : course2student.keySet())
        {
            course2studCount.put(course, course2student.get(course).size());
        }

        return course2studCount;
    }
}
