package ru.tandemservice.uniepp.component.registry.ModuleRegistry.Pub;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author iolshvang
 * @since 03.08.11 19:09
 */

public interface IDAO  extends IUniDao<Model>
{
    void setShared(final Model model);
}
