package ru.tandemservice.uniepp.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Конфигурация для скриптовой печати модуля «Учебный процесс»"
 * Имя сущности : eppScriptItem
 * Файл data.xml : uniepp.eduplan.data.xml
 */
public interface EppScriptItemCodes
{
    /** Константа кода (code) элемента : Версия учебного плана (title) */
    String UNIEPP_EDU_PLAN_VERSION = "report.unieppEduPlanVersion";
    /** Константа кода (code) элемента : Рабочий учебный план (title) */
    String UNIEPP_WORK_PLAN = "report.unieppWorkPlan";
    /** Константа кода (code) элемента : Индивидуальный учебный план (title) */
    String UNIEPP_CUSTOM_EDU_PLAN = "report.unieppCustomEduPlan";

    Set<String> CODES = ImmutableSet.of(UNIEPP_EDU_PLAN_VERSION, UNIEPP_WORK_PLAN, UNIEPP_CUSTOM_EDU_PLAN);
}
