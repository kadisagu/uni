/**
 *$Id$
 */
package ru.tandemservice.uniepp.base.bo.EppIndicator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import ru.tandemservice.uniepp.base.bo.EppIndicator.logic.EppIndicator;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.EduPlanVersionHigherProfStartYears.EppIndicatorEduPlanVersionHigherProfStartYears;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.EduPlansWithoutWorkPlans.EppIndicatorEduPlansWithoutWorkPlans;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentWithIncorrectWorkPlan.EppIndicatorStudentWithIncorrectWorkPlan;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsSlotList.EppIndicatorStudentsSlotList;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithDisciplineGaps.EppIndicatorStudentsWithDisciplineGaps;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithWrongEduPlan.EppIndicatorStudentsWithWrongEduPlan;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithoutEduPlanBlock.EppIndicatorStudentsWithoutEduPlanBlock;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithoutEduPlans.EppIndicatorStudentsWithoutEduPlans;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.StudentsWithoutWPDSlots.EppIndicatorStudentsWithoutWPDSlots;
import ru.tandemservice.uniepp.base.bo.EppIndicator.ui.WorkPlansWithInvalidTerm.EppIndicatorWorkPlansWithInvalidTerm;

/**
 * @author Alexander Shaburov
 * @since 01.03.13
 */
@Configuration
public class EppIndicatorManager extends BusinessObjectManager
{
    public static EppIndicatorManager instance()
    {
        return instance(EppIndicatorManager.class);
    }

    @Bean
    public ItemListExtPoint<EppIndicator> indicatorListExtPoint()
    {
        return itemList(EppIndicator.class)
            .add("epp-student-without-eduplan", new EppIndicator("epp-student-without-eduplan", EppIndicatorStudentsWithoutEduPlans.class.getSimpleName(), "epp-student"))
            .add("epp-student-without-eduplan-block", new EppIndicator("epp-student-without-eduplan-block", EppIndicatorStudentsWithoutEduPlanBlock.class.getSimpleName(), "epp-student"))
            .add("epp-student-with-wrong-eduplan", new EppIndicator("epp-student-with-wrong-eduplan", EppIndicatorStudentsWithWrongEduPlan.class.getSimpleName(), "epp-student"))
            .add("epp-student-without-wpd-slots", new EppIndicator("epp-student-without-wpd-slots", EppIndicatorStudentsWithoutWPDSlots.class.getSimpleName(), "epp-student"))
            .add("epp-student-slot-test", new EppIndicator("epp-student-slot-test", EppIndicatorStudentsSlotList.class.getSimpleName(), "epp-student"))
            .add("epp-student-with-discipline-gaps", new EppIndicator("epp-student-with-discipline-gaps", EppIndicatorStudentsWithDisciplineGaps.class.getSimpleName(), "epp-student"))
            .add("epp-student-with-incorrect-workplan", new EppIndicator("epp-student-with-incorrect-workplan", EppIndicatorStudentWithIncorrectWorkPlan.class.getSimpleName(), "epp-student"))
            .add("epp-group-with-incorrect-course", new EppIndicator("epp-group-with-incorrect-course", "ru.tandemservice.uniepp.component.indicators.GroupsWithIncorrectCourse", "epp-group"))
            .add("epp-workplan-time", new EppIndicator("epp-workplan-time", "ru.tandemservice.uniepp.component.indicators.WorkPlanRowTime", "epp-workplan"))
            .add("epp-eduplan-without-workplan", new EppIndicator("epp-eduplan-without-workplan", EppIndicatorEduPlansWithoutWorkPlans.class.getSimpleName(), "epp-workplan"))
            .add("epp-workplan-with-invalid-term", new EppIndicator("epp-workplan-with-invalid-term", EppIndicatorWorkPlansWithInvalidTerm.class.getSimpleName(), "epp-workplan"))
            .add("epp-registry-check", new EppIndicator("epp-registry-check", "ru.tandemservice.uniepp.component.indicators.EppRegistryCheck", "epp-registry"))
            .add("epp-eduplan-version-start-years", new EppIndicator("epp-eduplan-version-start-years", EppIndicatorEduPlanVersionHigherProfStartYears.class.getSimpleName(), "epp-eduplan"))
            .create();
    }
}
