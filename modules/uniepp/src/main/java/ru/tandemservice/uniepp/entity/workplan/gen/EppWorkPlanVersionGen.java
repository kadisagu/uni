package ru.tandemservice.uniepp.entity.workplan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Рабочий учебный план (версия)
 *
 * Версия РУП, хранит только набор строк (распределение недель по частям семестра хранится в РУПе)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkPlanVersionGen extends EppWorkPlanBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion";
    public static final String ENTITY_NAME = "eppWorkPlanVersion";
    public static final int VERSION_HASH = -266373725;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";
    public static final String P_NUMBER = "number";

    private EppWorkPlan _parent;     // РУП
    private String _number;     // Номер

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return РУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkPlan getParent()
    {
        return _parent;
    }

    /**
     * @param parent РУП. Свойство не может быть null.
     */
    public void setParent(EppWorkPlan parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppWorkPlanVersionGen)
        {
            setParent(((EppWorkPlanVersion)another).getParent());
            setNumber(((EppWorkPlanVersion)another).getNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkPlanVersionGen> extends EppWorkPlanBase.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkPlanVersion.class;
        }

        public T newInstance()
        {
            return (T) new EppWorkPlanVersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return obj.getParent();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "parent":
                    obj.setParent((EppWorkPlan) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "parent":
                    return EppWorkPlan.class;
                case "number":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkPlanVersion> _dslPath = new Path<EppWorkPlanVersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkPlanVersion");
    }
            

    /**
     * @return РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion#getParent()
     */
    public static EppWorkPlan.Path<EppWorkPlan> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    public static class Path<E extends EppWorkPlanVersion> extends EppWorkPlanBase.Path<E>
    {
        private EppWorkPlan.Path<EppWorkPlan> _parent;
        private PropertyPath<String> _number;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion#getParent()
     */
        public EppWorkPlan.Path<EppWorkPlan> parent()
        {
            if(_parent == null )
                _parent = new EppWorkPlan.Path<EppWorkPlan>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(EppWorkPlanVersionGen.P_NUMBER, this);
            return _number;
        }

        public Class getEntityClass()
        {
            return EppWorkPlanVersion.class;
        }

        public String getEntityName()
        {
            return "eppWorkPlanVersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
