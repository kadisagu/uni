/* $Id$ */
package ru.tandemservice.uniepp.catalog.bo.EppProfessionalTask;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;

/**
 * @author Igor Belanov
 * @since 23.03.2017
 */
@Configuration
public class EppProfessionalTaskManager extends BusinessObjectManager
{
    public static final String PROP_PROGRAM_KIND = "programKind";
    public static final String PROP_PROGRAM_SUBJECT_INDEX = "programSubjectIndex";

    public static EppProfessionalTaskManager instance()
    {
        return instance(EppProfessionalTaskManager.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler programKindDSHandler()
    {
        return EduProgramKind.defaultSelectDSHandler(getName())
                .where(EduProgramKind.programAdditionalProf(), Boolean.FALSE) // ДПО не нужон
                .filter(EduProgramKind.title())
                .order(EduProgramKind.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectIndexDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
                .where(EduProgramSubjectIndex.programKind(), PROP_PROGRAM_KIND)
                .filter(EduProgramSubjectIndex.title())
                .order(EduProgramSubjectIndex.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
                .where(EduProgramSubject.subjectIndex().programKind(), PROP_PROGRAM_KIND)
                .where(EduProgramSubject.subjectIndex(), PROP_PROGRAM_SUBJECT_INDEX)
                .filter(EduProgramSubject.code())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.code())
                .order(EduProgramSubject.title());
    }
}
