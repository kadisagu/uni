package ru.tandemservice.uniepp.entity.registry;

import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.uniepp.base.bo.EppState.EppStatePath;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementPartModuleGen;

/**
 * Часть элемента реестра: модуль
 *
 * Определяет порядок прохождения модулей и аудиторную нагрузку для части элемента реестра
 */
@EppStatePath(EppRegistryElementPartModule.L_PART+"."+EppRegistryElementPart.L_REGISTRY_ELEMENT)
public class EppRegistryElementPartModule extends EppRegistryElementPartModuleGen implements IEppRegistryElementItem, IHierarchyItem, IEppStateObject
{

    @Override public EppRegistryElementPart getHierarhyParent() {
        return this.getPart();
    }

    @Override public EppState getState() {
        return this.getPart().getState(); // влият на изменение ЭТОГО объекта
    }

    @Override public String getTitle() {
        if (getModule() == null) {
            return this.getClass().getSimpleName();
        }
        return this.getModule().getTitle();
    }

}