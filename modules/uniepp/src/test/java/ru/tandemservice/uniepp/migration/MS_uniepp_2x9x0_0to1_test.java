    /* $Id:$ */
    package ru.tandemservice.uniepp.migration;

    import org.junit.Test;
    import org.junit.runner.RunWith;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.test.context.ContextConfiguration;
    import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
    import org.springframework.transaction.annotation.Transactional;
    import org.tandemframework.core.common.DBType;
    import org.tandemframework.dbsupport.ddl.DBTool;
    import org.tandemframework.dbsupport.ddl.connect.DBConnect;
    import org.tandemframework.dbsupport.ddl.schema.DBTable;
    import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
    import org.tandemframework.hibsupport.connection.DataSourceWrapper;
    import org.tandemframework.shared.commonbase.utils.MigrationUtils;

    /**
     * @author oleyba
     * @since 12.08.15
     */
    @RunWith(SpringJUnit4ClassRunner.class)
    @Transactional
    @ContextConfiguration({"classpath:migration-tests-context.xml"})
    public class MS_uniepp_2x9x0_0to1_test
    {
        @Autowired
        DataSourceWrapper mainDataSource;

        @Test
        public void run() throws Exception
        {
            try (DBTool tool = new DBTool(new DBConnect(mainDataSource)))
            {
                tool.createTable(new DBTable("epp_eduplan_prof_secondary_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey()
                ));

                tool.createTable(new DBTable("epp_eduplan_ver_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey(),
                    new DBColumn("eduPlan_id", DBType.LONG).setNullable(false)
                ));

                tool.createTable(new DBTable("epp_eduplan_verblock_s_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey(),
                    new DBColumn("ownerorgunit_id", DBType.LONG).setNullable(false)
                ));
                tool.createTable(new DBTable("epp_eduplan_verblock_r_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey()
                ));
                tool.createTable(new DBTable("epp_eduplan_verblock_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey(),
                    new DBColumn("eduPlanVersion_id", DBType.LONG).setNullable(false)
                ));

                tool.createTable(new DBTable("epp_workplan_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey(),
                    new DBColumn("parent_id", DBType.LONG).setNullable(false)
                ));

                tool.createTable(new DBTable("c_edu_level_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey(),
                    new DBColumn("code_p", DBType.EMPTY_STRING).setNullable(false)
                ));

                tool.createTable(new DBTable("epp_epvrow_base_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey(),
                    new DBColumn("owner_id", DBType.LONG).setNullable(false)
                ));

                tool.createTable(new DBTable("edu_specialization_root_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey()
                ));
                tool.createTable(new DBTable("edu_specialization_child_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey()
                ));
                tool.createTable(new DBTable("edu_specialization_base_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey(),
                    new DBColumn("programSubject_id", DBType.LONG).setNullable(false)
                ));

                tool.createTable(new DBTable("edu_c_pr_subject_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey(),
                    new DBColumn("subjectIndex_id", DBType.LONG).setNullable(false)
                ));
                tool.createTable(new DBTable("edu_c_pr_subject_index_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey(),
                    new DBColumn("programKind_id", DBType.LONG).setNullable(false)
                ));
                tool.createTable(new DBTable("edu_c_program_kind_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey(),
                    new DBColumn("eduLevel_id", DBType.LONG).setNullable(false),
                    new DBColumn("programSecondaryProf_p", DBType.BOOLEAN).setNullable(false)
                ));
                tool.createTable(new DBTable("epp_student_eduplanversion_t",
                    new DBColumn("id", DBType.LONG).setPrimaryKey(),
                    new DBColumn("eduplanversion_id", DBType.LONG).setNullable(false),
                    new DBColumn("block_id", DBType.LONG).setNullable(false)
                ));


                // УП СПО с двумя блоками
                tool.executeUpdate("insert into epp_eduplan_prof_secondary_t (id) values (?)", 101);
                tool.executeUpdate("insert into epp_eduplan_ver_t (id, eduPlan_id) values (?, ?)", 201, 101);
                tool.executeUpdate("insert into epp_eduplan_verblock_t (id, eduPlanVersion_id) values (?, ?)", 301, 201);
                tool.executeUpdate("insert into epp_eduplan_verblock_r_t (id) values (?)", 301);
                tool.executeUpdate("insert into epp_eduplan_verblock_t (id, eduPlanVersion_id) values (?, ?)", 302, 201);
                tool.executeUpdate("insert into epp_eduplan_verblock_s_t (id, ownerorgunit_id) values (?, ?)", 302, 401);

                // УП СПО с одним блоком
                tool.executeUpdate("insert into epp_eduplan_prof_secondary_t (id) values (?)", 102);
                tool.executeUpdate("insert into epp_eduplan_ver_t (id, eduPlan_id) values (?, ?)", 202, 102);
                tool.executeUpdate("insert into epp_eduplan_verblock_t (id, eduPlanVersion_id) values (?, ?)", 303, 202);
                tool.executeUpdate("insert into epp_eduplan_verblock_r_t (id) values (?)", 303);

                // УП ВО с блоками
                tool.executeUpdate("insert into epp_eduplan_ver_t (id, eduPlan_id) values (?, ?)", 203, 103);
                tool.executeUpdate("insert into epp_eduplan_verblock_t (id, eduPlanVersion_id) values (?, ?)", 304, 203);
                tool.executeUpdate("insert into epp_eduplan_verblock_r_t (id) values (?)", 304);
                tool.executeUpdate("insert into epp_eduplan_verblock_t (id, eduPlanVersion_id) values (?, ?)", 305, 203);
                tool.executeUpdate("insert into epp_eduplan_verblock_s_t (id, ownerorgunit_id) values (?, ?)", 305, 401);

                // специализация СПО
                tool.executeUpdate("insert into edu_specialization_root_t (id) values (?)", 501);
                tool.executeUpdate("insert into edu_specialization_base_t (id, programSubject_id) values (?, ?)", 501, 601);
                tool.executeUpdate("insert into edu_c_pr_subject_t (id, subjectIndex_id) values (?, ?)", 601, 701);
                tool.executeUpdate("insert into edu_c_pr_subject_index_t (id, programKind_id) values (?, ?)", 701, 801);
                tool.executeUpdate("insert into c_edu_level_t (id, code_p) values (?, ?)", 901, "2013.2.1");
                tool.executeUpdate("insert into edu_c_program_kind_t (id, programSecondaryProf_p, eduLevel_id) values (?, ?, ?)", 801, Boolean.TRUE, 901);

                tool.executeUpdate("insert into edu_specialization_child_t (id) values (?)", 502);
                tool.executeUpdate("insert into edu_specialization_base_t (id, programSubject_id) values (?, ?)", 502, 601);

                // специализация ВО
                tool.executeUpdate("insert into edu_specialization_root_t (id) values (?)", 503);
                tool.executeUpdate("insert into edu_specialization_base_t (id, programSubject_id) values (?, ?)", 503, 602);
                tool.executeUpdate("insert into edu_c_pr_subject_t (id, subjectIndex_id) values (?, ?)", 602, 702);
                tool.executeUpdate("insert into c_edu_level_t (id, code_p) values (?, ?)", 902, "2013.2.3");
                tool.executeUpdate("insert into edu_c_program_kind_t (id, programSecondaryProf_p, eduLevel_id) values (?, ?, ?)", 802, Boolean.FALSE, 902);
                tool.executeUpdate("insert into edu_c_pr_subject_index_t (id, programKind_id) values (?, ?)", 702, 802);

                tool.executeUpdate("insert into edu_specialization_child_t (id) values (?)", 504);
                tool.executeUpdate("insert into edu_specialization_base_t (id, programSubject_id) values (?, ?)", 504, 602);

                // УП студента
                tool.executeUpdate("insert into epp_student_eduplanversion_t (id, eduplanversion_id, block_id) values(?,?,?)", 951, 202, 305);

                // run
                new MS_uniepp_2x9x0_0to1().run(tool);

                org.junit.Assert.assertTrue(tool.columnExists("epp_eduplan_prof_secondary_t", "ownerorgunit_id"));

                MigrationUtils.assertExists(tool, "select id from epp_eduplan_prof_secondary_t where id = ? and ownerorgunit_id = ?", 101, 401);
                MigrationUtils.assertExists(tool, "select id from epp_eduplan_verblock_r_t where id = ?", 301);
                MigrationUtils.assertExists(tool, "select id from epp_eduplan_verblock_t where id = ? and eduPlanVersion_id = ?", 301, 201);
                MigrationUtils.assertNotExists(tool, "select id from epp_eduplan_verblock_r_t where id = ?", 302);
                MigrationUtils.assertNotExists(tool, "select id from epp_eduplan_verblock_s_t where id = ?", 302);

                MigrationUtils.assertExists(tool, "select id from epp_eduplan_verblock_r_t where id = ?", 304);
                MigrationUtils.assertExists(tool, "select id from epp_eduplan_verblock_s_t where id = ?", 305);

                MigrationUtils.assertCount(tool, "epp_eduplan_prof_secondary_t", 2);
                MigrationUtils.assertCount(tool, "epp_eduplan_verblock_r_t", 3);
                MigrationUtils.assertCount(tool, "epp_eduplan_verblock_s_t", 1);
                MigrationUtils.assertCount(tool, "epp_eduplan_verblock_t", 4);

                MigrationUtils.assertExists(tool, "select id from epp_student_eduplanversion_t where id=? and block_id=?", 951, 305);

//                MigrationUtils.assertNotExists(tool, "select id from edu_specialization_root_t where id = ?", 501);
//                MigrationUtils.assertNotExists(tool, "select id from edu_specialization_base_t where id = ?", 501);
//                MigrationUtils.assertNotExists(tool, "select id from edu_specialization_child_t where id = ?", 502);
//                MigrationUtils.assertNotExists(tool, "select id from edu_specialization_base_t where id = ?", 502);
//
//                MigrationUtils.assertExists(tool, "select id from edu_specialization_root_t where id = ?", 503);
//                MigrationUtils.assertExists(tool, "select id from edu_specialization_base_t where id = ?", 503);
//                MigrationUtils.assertExists(tool, "select id from edu_specialization_child_t where id = ?", 504);
//                MigrationUtils.assertExists(tool, "select id from edu_specialization_base_t where id = ?", 504);

            }
        }
    }
