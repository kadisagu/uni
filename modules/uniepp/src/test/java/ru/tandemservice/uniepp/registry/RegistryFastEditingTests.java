package ru.tandemservice.uniepp.registry;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.registry.*;

import java.util.*;

import static org.junit.Assert.*;

// Todo сслыка на вики на актуальные постановки?

/**
 * DEV-7027  Автозаполнение и копирование в реестре
 *
 * @author okleptsova
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(loader = TestContextLoader.class)
public class RegistryFastEditingTests extends TestCommon {

    /**
     * DEV-7027
     * Копирование дисциплины
     * Копируются все нагрузки элемента, части (с нагрузками, формами контроля) и модули. Учебные модули создаются
     * новые как полные копии оригинальных, если они не разделяемые. Разделяемые модули цепляются к частям как есть.
     * Копируются шкалы оценок для форм итогового контроля частей.После закрытия формы переходить на карточку нового элемента.
     * ------------
     * Ольга К: другими словами, скопировать все слинкованные с дисциплиной сущности классов EppRegistryDiscipline,
     * EppRegistryElementPart, EppRegistryElementPartModule, EppRegistryModule. Проверить что внешние линки скопированных
     * сущностей идентичны тем, кого копируют, кроме линков на подразделение, которое выбирается при клонировании.
     * Исключение: shared EppRegistryModule не копируется, а линкуется к скопированной сущности
     */
    @Test
    public void testCloneDiscipline() throws Exception {
        // Добавить набор сущностей для теста с внешними линками
        TestDataSet disciplineDataSet = new TestDataSet("EppDisciplinePartsModules.graphml", "Какой-то выгруженный JSON");
        disciplineDataSet.deserialize();
        disciplineDataSet.saveToDB();

        // Создать новое подразделение для клонированных объектов
        OrgUnit newDisciplineOwner = new OrgUnit();
        saveOrUpdate(newDisciplineOwner);
        // Новое название для клонированных объектов
        final String newTitle = "Cloned Discipline";

        //Клонируем
        final Long disciplineId = disciplineDataSet.getEntities(EppRegistryDiscipline.class).get(0).getId();
        // TODO может быть первую строчку перенести в дао метод, чтобы в тестах не надо было строить мапу? Но, здесь копипаста не много потому не принципиально
        Map<Long, IEppRegElWrapper> disciplineMap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(ImmutableList.of(disciplineId));
        EppRegistryElement newRegistryElement = IEppRegistryDAO.instance.get().doCreateRegistryElement(disciplineMap.values().iterator().next(), newDisciplineOwner, newTitle, null);

        //Получаем из базы то что получилось
        TestDataSet clonedDisciplineDataSet = new TestDataSet("EppDisciplinePartsModules.graphml");
        clonedDisciplineDataSet.loadFromDB(newRegistryElement.getId());

        // Проверяем, что объекты на самом деле новые. В клонированных дисциплинах не должно быть тех же id.
        // Кроме шаред модуля.
        final List<Class> clonedClasses = Arrays.asList(EppRegistryDiscipline.class, EppRegistryElementPart.class,
                EppRegistryElementPartModule.class, EppRegistryModule.class);

        for (IEntity e : clonedDisciplineDataSet.getEntities()) {
            if (clonedClasses.contains(e.getClass())) {
                if (e.getClass() == EppRegistryModule.class && (boolean) e.getProperty("shared")) {
                    assertNotNull(clonedDisciplineDataSet.getEntity(e.getId()));
                } else {
                    assertNull(clonedDisciplineDataSet.getEntity(e.getId()));
                }
            }
        }

        // Проверяем значения полей c данными и корректность внешних ссылок
        //todo посмотреть diff под отладчиком и решить что делать с полями, которые не упомянуты в постановке
        //todo где требования к state?
        //todo Где требования к "title", "shortTitle", "fullTitle" вложенных объектов?
        //todo скорее всего тест упадет, из-за вышеупомянутых полей, посмотрев дифф будет явно что нужно проверять, и что спросить у аналитика
        EntityDiff diff = compareEntities(disciplineDataSet.getEntity(disciplineId),
                clonedDisciplineDataSet.getEntity(newRegistryElement.getId()));
        diff.assertPropertiesEqualExclude(new String[]{"id", "title", "owner"});
        diff.assertPropertyAfter("title", newTitle);
        diff.assertPropertyAfter("owner", newDisciplineOwner.getId());

        List<EntityDiff> diffList = compareGroups(disciplineDataSet.getEntities(EppRegistryElementPart.class),
                clonedDisciplineDataSet.getEntities(EppRegistryElementPart.class),
                "number");
        diffList.forEach(EntityDiff::assertDataEqual);

        diffList = compareGroups(disciplineDataSet.getEntities(EppRegistryElementPartModule.class),
                clonedDisciplineDataSet.getEntities(EppRegistryElementPartModule.class),
                "number");
        diffList.forEach(EntityDiff::assertDataEqual);

        diffList = compareGroups(disciplineDataSet.getEntities(EppRegistryModule.class),
                clonedDisciplineDataSet.getEntities(EppRegistryModule.class),
                "title");
        for (EntityDiff diff1 : diffList) {
            EppRegistryModule entity = (EppRegistryModule) disciplineDataSet.getEntity(diff1.ids.get("before"));
            if (entity.isShared()) {
                diff1.assertLinksEqual();
                diff1.assertDataEqual();
            } else {
                diff1.assertPropertiesEqualExclude(new String[]{"id", "owner"});
                diff1.assertPropertyAfter("owner", newDisciplineOwner.getId());
            }
        }

    }

    /**
     * DEV-7027
     * Объединение дублей происходит по правилам:
     * 1. Все ссылки на модули частей дублирующих элементов реестра заменяются ссылками на соответствующие модули
     * шаблонного элемента. Соответствие ищется по номеру модуля и номеру части, в которой модуль находится.
     * Удаляются все учебные модули, на которые ссылались модули частей дублей, если это не разделяемые модули и
     * никакие другие элементы на них не ссылаются.
     * 2. Все ссылки на формы контроля частей дублирующих элементов заменяются ссылками на соответствующие формы
     * контроля частей шаблонного элемента. Соответствие ищется по номеру части и итоговой форме контроля.
     * 3. Все ссылки на части дублирующих элементов заменяются ссылками на соответствующие части шаблонного элемента.
     * Соответствие ищется по номеру части.
     * 4. Все ссылки на дубли элементов реестра заменяются ссылкой на шаблон.
     * 5. Все дублирующие элементы реестра удаляются вместе со своими вложенными объектами (часами, частями, модулями и т.д.).
     * -------
     * Ольга К: другими словами, все ссылки на на сущности дублей
     * перекидываются на соотв. элементы шаблона. После этого сущности дублей удаляются.
     * Исключение - это шаред Учебные модули
     */
    @Test
    public void testMergeDisciplines() throws Exception {

        List<TestDataSet> disciplineSets = createDataForMerge();

        // Сделаем какой-то модуль шаредом, чтобы потом проверить, что он не удален
        EppRegistryModule sharedModule = (EppRegistryModule) disciplineSets.get(1).getEntities(EppRegistryModule.class).get(0);
        assert !sharedModule.isShared();
        sharedModule.setShared(true);
        saveOrUpdate(sharedModule);
        Long sharedId = sharedModule.getId();

        doMerge(disciplineSets);

        // BEGIN Проверяем,что ссылки всех добавленных сущностей ведут на новую структуру
        List<Long> linkedItemsIds = new ArrayList<>();

        // Получаем список id-шников, которые прилинкованы ко всем сущностям, которые мержатся в одну
        //Todo уточнить должны ли быть перелинкованы линки на шаред модуль? Я думаю да
        // TODO добавить сюда все сущности, которые должны быть удалены
        final List<Class> mergedClasses = Arrays.asList(EppRegistryDiscipline.class, EppRegistryElementPart.class,
                EppRegistryElementPartModule.class, EppRegistryModule.class, EppRegistryElementPartFControlAction.class);

        for (TestDataSet ds : disciplineSets) {
            List<IEntity> le = ds.getEntities();
            for (IEntity e : le) {
                if (!mergedClasses.contains(e.getClass())) linkedItemsIds.add(e.getId());
            }
        }

        //Выгружаем новое состояние дисциплины-шаблона вместе со всеми прилинкованными объектами
        Long templateId = disciplineSets.get(0).getEntities(EppRegistryDiscipline.class).get(0).getId();
        TestDataSet mergedDisciplineSet = new TestDataSet("LinksToDisciplineModulesTree.graphml");
        mergedDisciplineSet.loadFromDB(templateId);

        // Проверяем, что все линки перекинулись
        for (Long id : linkedItemsIds) {
            assertNotNull(mergedDisciplineSet.getEntity(id));
        }

        // Проверяем соответствие по номеру части
        // Соберем все ID, прилинкованные первой части шаблонного элемента
        // Как то некрасиво и сложно получилось, и еще с копипастом. Пока не придумала как лучше :(
        List<Long> firstPartlinkedItemsIdsExpected = new ArrayList<>();
        for (TestDataSet ds : disciplineSets) {
            final Long oldfirstPartId = ds.getEntity(EppRegistryElementPart.class, "number", 1).getId();
            for (IEntity e : ds.getChildren(oldfirstPartId)) {
                if (e.getClass() == EppRegistryElementPartFControlAction.class) {
                    for (IEntity ee : ds.getChildren(e.getId())) {
                        firstPartlinkedItemsIdsExpected.add(ee.getId());
                    }
                } else {
                    firstPartlinkedItemsIdsExpected.add(e.getId());
                }
            }
        }

        List<Long> firstPartlinkedItemsIdsActual = new ArrayList<>();
        final Long newfirstPartId = mergedDisciplineSet.getEntity(EppRegistryElementPart.class, "number", 1).getId();
        for (IEntity e : mergedDisciplineSet.getChildren(newfirstPartId)) {
            if (e.getClass() == EppRegistryElementPartFControlAction.class) {
                for (IEntity ee : mergedDisciplineSet.getChildren(e.getId())) {
                    firstPartlinkedItemsIdsActual.add(ee.getId());
                }
            } else {
                firstPartlinkedItemsIdsActual.add(e.getId());
            }
        }
        assertEquals(firstPartlinkedItemsIdsExpected, firstPartlinkedItemsIdsActual);
        //todo проверить соотвествии number у линка на модули

        //END проверка ссылок

        //Проверяем что все элементы удалены кроме шареда
        List<Long> deletedIds = new ArrayList<>();
        for (TestDataSet ds : disciplineSets.subList(1, 2)) {
            List<IEntity> le = ds.getEntities();
            for (IEntity e : le) {
                if (mergedClasses.contains(e.getClass())) deletedIds.add(e.getId());
            }
        }
        deletedIds.remove(sharedId);

        for (Long id : deletedIds) {
            assertNull("Сущности не удалены после мержа", IUniBaseDao.instance.get().get(id));
        }
        assertNotNull("После мержа удален разделяемыЙ модуль", IUniBaseDao.instance.get().get(sharedId));

    }

    /**
     * DEV-7027
     * Объединение дублей
     * Ошибка, если в дубле частей больше, чем в шаблоне
     * Если для какой-то части дубля, на которую есть внешние ссылки, не нашло соответствия в шаблонном элементе,
     * выводится сообщение об ошибке: "Набор частей элемента реестра, на основе которого производится объединение,
     * недостаточен для объединения со всеми выбранными дублями."
     */
    @Test(expected = ApplicationException.class)
    public void testMergeDisciplinesExceptionWrongPart() {
        List<TestDataSet> disciplineSets = createDataForMerge();

        //Добавить лишнюю часть
        EppRegistryElementPart somePart = new EppRegistryElementPart();
        somePart.setNumber(3);
        somePart.setRegistryElement((EppRegistryDiscipline) disciplineSets.get(1).getEntities(EppRegistryDiscipline.class).get(0));
        saveOrUpdate(somePart);

        doMerge(disciplineSets);
        // todo По-хорошему надо бы еще проверить что после ошибки все осталось в том состоянии в котором было, но это потом.. когда нибудь
    }

    /**
     * DEV-7027
     * Объединение дублей
     * Ошибка, если в дубле частей большеменьше, чем в шаблоне
     * Если для какого-то модуля, на который есть ссылки, не найдено соответствующего модуля в шаблонном элементе,
     * то выводится сообщение об ошибке: "Набор модулей элемента реестра, на основе которого производится объединение,
     * недостаточен для объединения со всеми выбранными дублями."
     */
    @Test(expected = ApplicationException.class)
    public void testMergeDisciplinesExceptionWrongModule() {
        //todo
    }

    /**
     * DEV-7027
     * Объединение дублей
     * Ошибка, если в дубле форм контроля больше, чем в шаблоне
     * Если для какой-то формы контроля, на которую есть внешние ссылки, нет соответствующей части в шаблонном элементе,
     * выводится сообщение об ошибке: "Набор форм итогового контроля элемента реестра, на основе которого производится
     * объединение, недостаточен для объединения со всеми выбранными дублями."ми."
     */
    @Test(expected = ApplicationException.class)
    public void testMergeDisciplinesExceptionWrongFormControl() {
        //todo
    }

    /**
     * Создать данные для мержа и по несколько ссылок на каждую сущность для мержа
     */
    private List<TestDataSet> createDataForMerge() {
        List<TestDataSet> disciplineSets = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            TestDataSet disciplineSet = new TestDataSet("LinksToDisciplineModulesTree.graphml", "Some JSON");
            //У каждой дисциплины будет 2 части и два модуля у каждой части,
            // Две итоговые формы контроля у каждой части, которые обязаны ссылаться на один и тот же элемент справочника.
            // Так как сущностей для этого теста очень много и не факт что у них есть линки в базе данных, в данному случае
            // видимо наиболее правильно будет создать пустые объекты (как показано ниже), а после
            // вручную в JSON перелинковать все сущности класса EppRegistryElementPartFControlAction :(
            // после использовать данный JSON в этом коде
//            HashMap<Class, Integer> m = new HashMap<>();
//            m.put(EppRegistryElementPart.class, 2);
//            m.put(EppRegistryElementPartModule.class, 2);
//            m.put(EppRegistryElementPartFControlAction.class, 2);
//            disciplineSet.createEmptyObjects(m);
            disciplineSet.deserialize();
            disciplineSet.saveToDB();
            disciplineSets.add(disciplineSet);
        }
        return disciplineSets;
    }

    /**
     * Смержить используя ids из датасетов
     */

    private void doMerge(List<TestDataSet> dataForMerge) {
        // Нулевой элемент будет основным, два других дублями
        Long templateId = dataForMerge.get(0).getEntities(EppRegistryDiscipline.class).get(0).getId();
        Long[] duplicateIds = {dataForMerge.get(1).getEntities(EppRegistryDiscipline.class).get(0).getId(),
                dataForMerge.get(2).getEntities(EppRegistryDiscipline.class).get(0).getId()};

        IEppRegistryDAO.instance.get().doRegElementDuplicatesMerge(templateId, Arrays.asList(duplicateIds));
    }

}
