/* $Id:$ */
package ru.tandemservice.uniepp.registry;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.commonbase.utils.test.CommonBaseTestJsonContext;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;

import java.util.List;
import java.util.Map;


/**
 * @author Nataliya Ivanova
 * @since 2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(loader = TestContextLoader.class)
public class RegistryElementCloneTest
{
    @Test
    public void testEqualParts() throws Exception
    {
        CommonBaseTestJsonContext context = new CommonBaseTestJsonContext(this.getClass().getResource("ClonePart.json"));

        EppRegistryElement element = context.entity(EppRegistryElement.class, 1L);

        Map<Long, IEppRegElWrapper> map = IEppRegistryDAO.instance.get().getRegistryElementDataMap(ImmutableList.of(element.getId()));

        EppRegistryElement newEl = IEppRegistryDAO.instance.get().doCreateRegistryElement(
                map.values().iterator().next(),
                null,
                "123",
                null
        );

        final long newElContextId = 2L;
        context.setId(newElContextId, newEl.getId());

        List<EppRegistryElementPart> partList = IUniBaseDao.instance.get().getList(EppRegistryElementPart.class, EppRegistryElementPart.registryElement(), newEl);
        for (EppRegistryElementPart part : partList) {
            final long partContextId = part.getNumber() + newElContextId * 10;
            context.setId(partContextId, part.getId());
            List<EppRegistryElementPartModule> moduleList = IUniBaseDao.instance.get().getList(EppRegistryElementPartModule.class, EppRegistryElementPartModule.part(), part);
            for (EppRegistryElementPartModule module : moduleList) {
                context.setId((long) module.getNumber() + partContextId * 10, module.getId());
            }
        }

        context.check();
    }
}
