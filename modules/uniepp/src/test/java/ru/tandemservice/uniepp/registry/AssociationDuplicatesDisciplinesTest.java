package ru.tandemservice.uniepp.registry;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import org.tandemframework.shared.commonbase.utils.test.CommonBaseTestJsonContext;
import org.testng.Assert;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.List;


/**
 * @author Nataliya Ivanova
 * @since 2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(loader = ru.tandemservice.uniepp.registry.TestContextLoader.class)
public class AssociationDuplicatesDisciplinesTest
{
    @Test
    public void testEqualParts() throws Exception
    {
        CommonBaseTestJsonContext context = new CommonBaseTestJsonContext(this.getClass().getResource("mergeElData.json"));

        EppRegistryElement elementOne = context.entity(EppRegistryElement.class, 1L);
        EppRegistryElement elementTwo = context.entity(EppRegistryElement.class, 2L);

        IEppRegistryDAO.instance.get().doRegElementDuplicatesMerge(
                elementOne.getId(),
                ImmutableList.of(elementTwo.getId())
        );

        context.check();

        boolean hasElementTwo = new DQLSimple<>(EppRegistryElement.class)
                .where(EppRegistryElement.id(), elementTwo.getId())
                .exists();
        Assert.assertFalse(hasElementTwo);
    }


}
