package ru.tandemservice.uniepp.registry;


import org.tandemframework.core.entity.IEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Группировка произвольного количества объектов разных сущностей, слинкованных между собой
 *
 * @author okleptsova
 */
public class TestDataSet {

    /**
     * @param dataDescriptionFile файл описывающий структуру ссылок, например yed или какой-либо промежуточный
     * @param jsonDataFile        файл для загрузки/выгрузки данных
     */
    public TestDataSet(String dataDescriptionFile, String jsonDataFile) {
        // todo в первой реализации просто путь к файлу. В будущем надо подумать где и как их хранить.
        // Один dataDescriptionFile может иметь от 0 до N jsonDataFile, каждый из которых должен использоваться в отдельном экземпляре TestDataSet

        // При вызове методов loadFromDB(), deserialize(), createEmptyObjects() внутри экземпляра класса создаются соотвествующие объекты IEntity
        // Они берутся соотвественно из базы данных или из json файла или создаются с дефолтными значениями на основе шаблона
        // Объекты могут быть созданы только одним из трех этих методов.
        // Во избежании ошибок использование другого метода должно требовать создания нового экземпляра

        // uuid уникальный ID объекта, созданного в данном классе
        // используется для хранения структуры ссылок между объектами, без привязки к базе данных
        // генерируется в момент создания объектов,новый если loadFromDB() или createEmptyObjects()
        // при  deserialize() берется из json
        // При вызове saveToDB() или loadFromDB() сохраняются ассоциации между uuid и Id в базе данных
        // возможно имеет смысл использовать как внутренний ключ в графе объектов
        //

        // serialize() deserialize() сохраняет или подгружает текущее состояние объектов, без Id базы данных, но с uuid
    }

    public TestDataSet(String dataDescriptionFile){

    }

    /**
     * Создать набор ссылающихся объектов с значениями полей данных "по умолчанию"
     */
    public void createEmptyObjects() {

    }

    /**
     * Создать набор ссылающихся объектов с значениями полей данных "по умолчанию"
     *
     * @param entityCounts При ссылке 1 ко многим создать несколько объектов данной сущности
     */

    public void createEmptyObjects(HashMap<Class, Integer> entityCounts) {
        // При вызове без аргументов создает по одному прилинкованному объекту для всех линков
        // но для тестов желательно использовать миниму два объекта при ссылках 1 ко многим
        // кривовато, но пока не придумала как сделать прямее
        // todo Коля, к тебе вопрос. есть ли способ элегантно и правильно организовать заполнение полей типа number?

        // а еще сюда же запилить дефольные значения для not null полей в базе (скопипастить из CommonBaseTestJsonContext)
    }

    /**
     * Загрузить набор ссылающихся объектов из базы данных, с проходом по всем ссылкам
     *
     * @param enterPointDbId id стартового объекта в базе данных
     */
    public void loadFromDB(Long enterPointDbId) {
        // то есть тот объект с которого начинается проход по ссылкам в базе.
    }

    /**
     * Обновить все ассоциированные объекты из базы данных
     */
    public void refreshFromDB() {

    }

    /**
     * Записать текущее состояние объектов в базу данных
     */
    public void saveToDB() {
        // Тут у меня вопрос, после сохранения объектов в базу, следует ли сразу считать их новое состояние?
        // Например,чтобы заполнить значения автовычисляемых полей, типа даты создания
    }

    /**
     * Обновить данные в базе текущим состоянием объектов
     */
    public void updateInDB() {
    }

    /**
     * Удалить все ассоциированные объекты из базы данных
     */
    public void deleteFromDB() {

    }

    /**
     * Сохранить набор данных (объекты) в JSON
     */
    public void serialize() {
    }

    /**
     * Загрузить набор данных (объекты) из JSON
     */
    public void deserialize() {
    }

    /**
     * Получить все объекты набора данных
     */
    public List<IEntity> getEntities() {
        return new ArrayList<IEntity>();
    }

    /**
     * Получить объекты набора данных
     *
     * @param clazz получить объекты этого класса
     */
    public List<IEntity> getEntities(Class<? extends IEntity> clazz) {
        return new ArrayList<IEntity>();
    }

    /**
     * Получить все объекты, которые имеют ссылки на данный объект
     * @param dbId объекта на который ссылаются другие объекты
     * @param childrenClazz только объекты заданного класса
     */
    public List<IEntity> getChildren(Long dbId, Class<? extends IEntity> childrenClazz) {
        return new ArrayList<IEntity>();
    }
    /**
     * Получить все объекты, которые имеют ссылки на данный объект
     * @param dbId id объекта на который ссылаются другие объекты
     */
    public List<IEntity> getChildren(Long dbId) {
        return new ArrayList<IEntity>();
    }

    /**
     * Получить родителя
     * @param dbId id оъекта который ссылается на другой объект
     * @param parentClazz класс объекта на который ссылается объект
     */
    public IEntity getParent(Long dbId, Class<? extends IEntity> parentClazz) {
        return new ArrayList<IEntity>().get(0);
    }
    

    /**
     * Получить объект набора данных
     *
     * @param dbId Id объекта в базе данных
     */
    public IEntity getEntity(Long dbId) {
        return new ArrayList<IEntity>().get(0);
    }


    /**
     * Получить объект по значению поля
     * @param clazz Класс получаемого объкекта
     * @param property Свойство
     * @param val Значение
     */
    public IEntity getEntity(Class<? extends IEntity> clazz, String property, Object val) {
        return new ArrayList<IEntity>().get(0);
    }

    /**
     *
     */
    public String objectsAsGraph(){
        // Было бы неплохо иметь возможность выгрузить структуру объектов и ссылок между ними, как текстовое описание графа,
        // которое можно было бы записать в файл и открыть в каком-нибудь просмотерщике графов.
        // Это поможет тестировщику проверить себя при создании/выгрузке
        // сложных структур объектов. В качестве описания ноды было бы классно использовать Название класса + какой то
        // универсальный метод toString который возвращает какое то осмысленное название для экземпляра сущности
        // возможно данная фича пригодится не только автотестеру. Возможно, что при должной "огранке" фича может быть
        // полезна ручному тестировщику или даже аналитику чтобы понять какие данные реально используются.
        return "";
    }

}


