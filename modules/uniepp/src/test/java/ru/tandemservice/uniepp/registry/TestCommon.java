package ru.tandemservice.uniepp.registry;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;

import java.util.ArrayList;
import java.util.List;

/**
 * Разные штуки, актуальные для всех тестов
 * @author okleptsova
 */
public class TestCommon {
    /**
     * Сохранить или изменить в базе
     *
     * @param entity объект
     */
    public void saveOrUpdate(IEntity entity) {
        //todo сессия флаш?
        DataAccessServices.dao().saveOrUpdate(entity);
    }

    /**
     * Добавить в тест сравнение двух объектов
     *
     * @param before эталонный объект
     * @param after  измененный тестом объект
     * @return - результат сравнения
     */
    public EntityDiff compareEntities(IEntity before, IEntity after) {
        //todo в будущем позволит довести тест до конца, и лишь потом сделать все проверки. Удобно для регулярного запуска большого кол-ва тестов, неудобно для написания тестов
        return new EntityDiff(before, after);
    }


    /**
     * Добавить в тест сравнение нескольких объектов одного класса
     *
     * @param before       список эталонных объектов
     * @param after        список измененых объектов
     * @param sortProperty перед сравнением отсортировать объекты по значению этого поля
     * @return результат сравнения
     */
    public List<EntityDiff> compareGroups(List<IEntity> before, List<IEntity> after, String sortProperty) {
        // TODO коллекции должны иметь одинаковое кол-во объектов
        // TODO все объекты должны быть одного класса
        return new ArrayList<EntityDiff>();
    }

}
