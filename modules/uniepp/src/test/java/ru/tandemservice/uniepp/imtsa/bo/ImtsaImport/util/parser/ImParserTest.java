/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.parser;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.ImParseResult;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.ImParseResultStandardUtil;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.dto.ImEpvRowDTO;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.node.ImElement;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author azhebko
 * @since 28.10.2014
 */
public class ImParserTest
{
    private static final Logger logger = Logger.getLogger(ImParserTest.class);

    private static final String RESOURCE_URL_PREFIX = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + "/uniepp/imtsaSample/**";
    private static final String RESOURCE_URL_XML = RESOURCE_URL_PREFIX + "/*.xml";
    private static final String RESOURCE_URL_TXT = RESOURCE_URL_PREFIX + "/*.txt";

    private static final PathMatchingResourcePatternResolver RESOLVER = new PathMatchingResourcePatternResolver(Thread.currentThread().getContextClassLoader());

    private Collection<? extends IImParser> imtsaParsers()
    {
        return Collections.singleton(new ImParser());
    }

    private static Resource resource(String name) throws IOException { return RESOLVER.getResource(name); }
    private static Resource[] resources(String pattern) throws IOException { return RESOLVER.getResources(pattern); }


    @Test(description = "Тест построения XML-документа")
    public void testBuildXmlDocument() throws IOException
    {
        boolean exception = false;
        for (Resource resource: RESOLVER.getResources(RESOURCE_URL_XML))
        {
            for (IImParser parser : imtsaParsers())
            {
                InputStream input = resource.getInputStream();
                try
                {
                    parser.buildXmlDocument(input);

                } catch (Exception e)
                {
                    logger.error(parser.getClass().getName() + " failed to parse " + resource.getFilename(), e);
                    exception = true;
                }
            }
        }

        Assert.assertFalse(exception);
    }

    // убирает расширение файла
    private String trimFileName(String fileName) { return fileName.substring(0, fileName.lastIndexOf(".")); }

    @Test
    public void testBuildImElement() throws Exception
    {
        Map<String, Resource> standardMap = new HashMap<>();
        for (Resource standard: resources(RESOURCE_URL_TXT))
            standardMap.put(trimFileName(standard.getFilename()), standard);

        for (Resource candidate: resources(RESOURCE_URL_XML))
        {
            Resource standard = standardMap.get(trimFileName(candidate.getFilename()));
            if (standard == null)
                continue;

            String standardValue = IOUtils.toString(standard.getInputStream());
            for (IImParser parser : imtsaParsers())
            {
                InputStream input = candidate.getInputStream();
                ImElement root = parser.buildImElement(parser.buildXmlDocument(input));
                Assert.assertEquals(standardValue, root.toString(), "Assertion failed for " + candidate.getFilename());
            }
        }
    }

    @Test
    public void testParserForException() throws Exception
    {
        for (Resource candidate: resources(RESOURCE_URL_XML))
        {
            for (IImParser parser : imtsaParsers())
            {
                InputStream input = candidate.getInputStream();
                ImElement root = parser.buildImElement(parser.buildXmlDocument(input));

                parser.parse(root);
            }
        }
    }

    @Test(enabled = false)
    public void testParse() throws Exception
    {
        for (Resource candidate: resources(RESOURCE_URL_XML))
        {
            ImParseResult standard = getStandardParseResult(candidate.getFilename());
            if (standard != null)
            {
                for (IImParser parser : imtsaParsers())
                {
                    InputStream input = candidate.getInputStream();
                    ImElement root = parser.buildImElement(parser.buildXmlDocument(input));
                    ImParseResult result = parser.parse(root);

                    assertParseResultEquals(standard, result);
                }
            }
        }
    }

    private ImParseResult getStandardParseResult(String fileName)
    {
        return ImParseResultStandardUtil.STANDARD_PARSE_RESULT_MAP.get(fileName);
    }

    public static void assertParseResultEquals(ImParseResult standard, ImParseResult candidate)
    {
        assertCyclesEquals(standard.getCycleAbbr2CycleTitleMap(), candidate.getCycleAbbr2CycleTitleMap());
        assertRootRowsEquals(standard.getRootRows(), candidate.getRootRows());
    }

    private static void assertCyclesEquals(Map<String, String> standardCycleAbbr2CycleTitleMap, Map<String, String> candidateCycleAbbr2CycleTitleMap)
    {
        Assert.assertEquals(standardCycleAbbr2CycleTitleMap, candidateCycleAbbr2CycleTitleMap);
    }

    private static void assertRootRowsEquals(Collection<ImEpvRowDTO> standardRootRows, Collection<ImEpvRowDTO> candidateRootRows)
    {
        Assert.assertEquals(candidateRootRows.size(), standardRootRows.size(), "Root has wrong children number: expected" + standardRootRows.size() + ", actual " + candidateRootRows.size() + ".");

        for (Iterator<ImEpvRowDTO> it1 = standardRootRows.iterator(), it2 = candidateRootRows.iterator(); it1.hasNext() || it2.hasNext(); )
        {
            assertRowsEquals(it1.next(), it2.next());
        }
    }

    private static void assertRowsEquals(ImEpvRowDTO expected, ImEpvRowDTO actual)
    {
        if (expected == null)
        {
            Assert.assertNull(actual);
            return;
        }

        Assert.assertEquals(actual.toString(), expected.toString());

        Assert.assertEquals(actual.getChildList().size(), expected.getChildList().size(), expected.toString() + " has wrong children number: expected" + expected.getChildList().size() + ", actual " + actual.getChildList().size() + ".");

        for (Iterator<ImEpvRowDTO> it1 = expected.getChildList().iterator(), it2 = actual.getChildList().iterator(); it1.hasNext() || it2.hasNext(); )
        {
            assertRowsEquals(it1.next(), it2.next());
        }
    }
}