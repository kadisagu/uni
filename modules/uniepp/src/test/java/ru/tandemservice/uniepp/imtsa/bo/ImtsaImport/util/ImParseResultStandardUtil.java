/* $Id$ */
package ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.dto.*;
import ru.tandemservice.uniepp.imtsa.bo.ImtsaImport.util.parser.ImParser;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * @author azhebko
 * @since 12.11.2014
 */
public class ImParseResultStandardUtil
{
    private static ImParseResult buildParseResult(Map<String, String> cycleAbbr2CycleTitleMap, Collection<ImEpvRowDTO> rootRows)
    {
        ImParseResult parseResult = new ImParseResult();
        parseResult.setCycleAbbr2CycleTitleMap(cycleAbbr2CycleTitleMap);
        parseResult.setRootRows(rootRows);
        return parseResult;
    }

    private static abstract class Example2
    {
        private static final Map<String, String> CYCLE_ABBR_2_CYCLE_TITLE_MAP = ImmutableMap.<String, String>builder()
            .put("Б1", "Гуманитарный, социальный и экономический цикл")
            .put("Б2", "Математический и естественнонаучный цикл")
            .put("Б3", "Профессиональный цикл")
            .put("Б4", "Физическая культура")
            .put("Б5", "Практики, НИР")
            .put("ФТД", "Факультативы")
            .build();

        private static final Collection<ImEpvRowDTO> ROOT_ROWS = Arrays.<ImEpvRowDTO>asList(
            structure("Б1")
                .storedIndex("Б1")
                .userIndex("Б1")
                .child(structure("Б")
                    .storedIndex("Б1.Б")
                    .userIndex("Б1.Б")
                    .child(registry("Иностранный язык")
                        .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                        .registryElementOwner("78")
                        .number("01")
                        .rowLoadMap(loadMap(lectureMap(null, null, null), practiceMap(324d, null, null), labsMap(null, null, null), hoursMap(864d, 24d, null, 324d, 432d, null, null)))
                        .rowTermLoadAction(1, loadMap(lectureMap(null, null, null), practiceMap(36d, null, null), labsMap(null, null, null), hoursMap(108d, 3d, null, 36d, 72d, null, null)), actionMap(null, 1, null, null, null))
                        .rowTermLoadAction(2, loadMap(lectureMap(null, null, null), practiceMap(54d, null, null), labsMap(null, null, null), hoursMap(117d, 4d, null, 54d, 63d, null, null)), actionMap(1, null, null, null, null))
                        .rowTermLoadAction(3, loadMap(lectureMap(null, null, null), practiceMap(72d, null, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 72d, 72d, null, null)), actionMap(null, 1, null, null, null))
                        .rowTermLoadAction(4, loadMap(lectureMap(null, null, null), practiceMap(54d, null, null), labsMap(null, null, null), hoursMap(180d, 6d, null, 54d, 126d, null, null)), actionMap(1, null, null, null, null))
                        .rowTermLoadAction(5, loadMap(lectureMap(null, null, null), practiceMap(54d, null, null), labsMap(null, null, null), hoursMap(108d, 3d, null, 54d, 54d, null, null)), actionMap(null, 1, null, null, null))
                        .rowTermLoadAction(6, loadMap(lectureMap(null, null, null), practiceMap(54d, null, null), labsMap(null, null, null), hoursMap(99d, 4d, null, 54d, 45d, null, null)), actionMap(1, null, null, null, null))
                        .storedIndex("Б1.Б.1")
                        .userIndex("Б1.Б.1")
                        .b())
                    .child(registry("Философия")
                        .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                        .registryElementOwner("66")
                        .number("02")
                        .rowLoadMap(loadMap(lectureMap(36d, null, null), practiceMap(36d, null, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 72d, 36d, null, null)))
                        .rowTermLoadAction(4, loadMap(lectureMap(36d, null, null), practiceMap(36d, null, null), labsMap(null, null, null), hoursMap(108d, 4d, null, 72d, 36d, null, null)), actionMap(1, null, null, null, null))
                        .storedIndex("Б1.Б.2")
                        .userIndex("Б1.Б.2")
                        .b())
                    .child(registry("Экономика")
                        .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                        .registryElementOwner("9")
                        .number("03")
                        .rowLoadMap(loadMap(lectureMap(18d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 36d, 72d, null, null)))
                        .rowTermLoadAction(7, loadMap(lectureMap(18d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(108d, 4d, null, 36d, 72d, null, null)), actionMap(1, null, null, null, null))
                        .storedIndex("Б1.Б.3")
                        .userIndex("Б1.Б.3")
                        .b())
                    .b())

                .child(structure("В")
                    .storedIndex("Б1.В")
                    .userIndex("Б1.В")
                    .child(registry("Русский язык и культура речи")
                        .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                        .registryElementOwner("76")
                        .number("01")
                        .rowLoadMap(loadMap(lectureMap(18d, null, null), practiceMap(36d, null, null), labsMap(18d, null, null), hoursMap(108d, 3d, null, 72d, 36d, null, null)))
                        .rowTermLoadAction(2, loadMap(lectureMap(18d, null, null), practiceMap(36d, null, null), labsMap(18d, null, null), hoursMap(108d, 3d, null, 72d, 36d, null, null)), actionMap(null, 1, null, null, null))
                        .storedIndex("Б1.В.ОД.1")
                        .userIndex("Б1.В.ОД.1")
                        .b())
                    .child(registry("Историческая антропология")
                        .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                        .registryElementOwner("64")
                        .number("02")
                        .rowLoadMap(loadMap(lectureMap(72d, null, null), practiceMap(72d, null, null), labsMap(null, null, null), hoursMap(252d, 7d, null, 144d, 81d, null, null)))
                        .rowTermLoadAction(1, loadMap(lectureMap(36d, null, null), practiceMap(36d, null, null), labsMap(null, null, null), hoursMap(108d, 3d, null, 72d, 36d, null, null)), actionMap(null, 1, null, null, null))
                        .rowTermLoadAction(2, loadMap(lectureMap(36d, null, null), practiceMap(36d, null, null), labsMap(null, null, null), hoursMap(117d, 4d, null, 72d, 45d, null, null)), actionMap(1, null, null, null, null))
                        .storedIndex("Б1.В.ОД.2")
                        .userIndex("Б1.В.ОД.2")
                        .b())
                    .child(registry("История науки и техники")
                        .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                        .registryElementOwner("65")
                        .number("03")
                        .rowLoadMap(loadMap(lectureMap(36d, null, null), practiceMap(36d, null, null), labsMap(null, null, null), hoursMap(108d, 3d, null, 72d, 36d, null, null)))
                        .rowTermLoadAction(1, loadMap(lectureMap(36d, null, null), practiceMap(36d, null, null), labsMap(null, null, null), hoursMap(108d, 3d, null, 72d, 36d, null, null)), actionMap(null, 1, null, null, null))
                        .storedIndex("Б1.В.ОД.3")
                        .userIndex("Б1.В.ОД.3")
                        .b())
                    .child(groupIm("Выбор 1")
                        .number("04")
                        .rowLoadMap(loadMap(lectureMap(36d, null, null), practiceMap(null, null, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 36d, 63d, null, null)))
                        .rowTermLoadAction(5, loadMap(lectureMap(36d, null, null), practiceMap(null, null, null), labsMap(null, null, null), hoursMap(99d, 4d, null, 36d, 63d, null, null)), actionMap(1, null, null, null, null))
                        .storedIndex("Б1.В.ДВ.1")
                        .userIndex("Б1.В.ДВ.1")
                        .child(registry("Этнография народов Дальнего Востока России")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("64")
                            .number("01")
                            .rowLoadMap(loadMap(lectureMap(36d, null, null), practiceMap(null, null, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 36d, 63d, null, null)))
                            .rowTermLoadAction(5, loadMap(lectureMap(36d, null, null), practiceMap(null, null, null), labsMap(null, null, null), hoursMap(99d, 4d, null, 36d, 63d, null, null)), actionMap(1, null, null, null, null))
                            .storedIndex("Б1.В.ДВ.1.1")
                            .userIndex("Б1.В.ДВ.1.1")
                            .b())
                        .child(registry("Этнография народов Сибири")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("64")
                            .number("02")
                            .rowLoadMap(loadMap(lectureMap(null, null, null), practiceMap(null, null, null), labsMap(null, null, null), hoursMap(null, null, null, null, null, null, null)))
                            .storedIndex("Б1.В.ДВ.1.2")
                            .userIndex("Б1.В.ДВ.1.2")
                            .b())
                        .b())
                    .child(groupIm("Выбор 2")
                        .number("05")
                        .rowLoadMap(loadMap(lectureMap(18d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(108d, 3d, null, 36d, 72d, null, null)))
                        .rowTermLoadAction(6, loadMap(lectureMap(18d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(108d, 3d, null, 36d, 72d, null, null)), actionMap(null, 1, null, null, null))
                        .storedIndex("Б1.В.ДВ.2")
                        .userIndex("Б1.В.ДВ.2")
                        .child(registry("Политология")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("74")
                            .number("01")
                            .rowLoadMap(loadMap(lectureMap(18d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(108d, 3d, null, 36d, 72d, null, null)))
                            .rowTermLoadAction(6, loadMap(lectureMap(18d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(108d, 3d, null, 36d, 72d, null, null)), actionMap(null, 1, null, null, null))
                            .storedIndex("Б1.В.ДВ.2.1")
                            .userIndex("Б1.В.ДВ.2.1")
                            .b())
                        .child(registry("Социология")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("62")
                            .number("02")
                            .rowLoadMap(loadMap(lectureMap(null, null, null), practiceMap(null, null, null), labsMap(null, null, null), hoursMap(null, null, null, null, null, null, null)))
                            .storedIndex("Б1.В.ДВ.2.2")
                            .userIndex("Б1.В.ДВ.2.2")
                            .b())
                        .b())
                    .child(groupIm("Выбор 3")
                        .number("06")
                        .rowLoadMap(loadMap(lectureMap(36d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 54d, 72d, null, null)))
                        .rowTermLoadAction(7,
                            loadMap(lectureMap(36d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(126d, 4d, null, 54d, 72d, null, null)), actionMap(1, null, null, null, null))
                        .storedIndex("Б1.В.ДВ.3")
                        .userIndex("Б1.В.ДВ.3")
                        .child(registry("Культурология")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("15")
                            .number("01")
                            .rowLoadMap(loadMap(lectureMap(36d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 54d, 72d, null, null)))
                            .rowTermLoadAction(7, loadMap(lectureMap(36d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(126d, 4d, null, 54d, 72d, null, null)), actionMap(1, null, null, null, null))
                            .storedIndex("Б1.В.ДВ.3.1")
                            .userIndex("Б1.В.ДВ.3.1")
                            .b())
                        .child(registry("Этика")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("15")
                            .number("02")
                            .rowLoadMap(loadMap(lectureMap(null, null, null), practiceMap(null, null, null), labsMap(null, null, null), hoursMap(null, null, null, null, null, null, null)))
                            .storedIndex("Б1.В.ДВ.3.2")
                            .userIndex("Б1.В.ДВ.3.2")
                            .b())
                        .b())
                    .b())
                .b());
    }

    private static abstract class Example3
    {
        private static final Map<String, String> CYCLE_ABBR_2_CYCLE_TITLE_MAP = ImmutableMap.<String, String>builder()
            .put("Б1", "Гуманитарный, социальный и экономический цикл")
            .put("Б2", "Математический и естественнонаучный цикл")
            .put("Б3", "Профессиональный цикл")
            .build();

        private static final Collection<ImEpvRowDTO> ROOT_ROWS = Arrays.<ImEpvRowDTO>asList(
            structure("Б2")
                .storedIndex("Б2")
                .userIndex("Б2")
                .child(structure("Б")
                    .storedIndex("Б2.Б")
                    .userIndex("Б2.Б")
                    .child(registry("Концепции современного естествознания")
                        .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                        .registryElementOwner("104")
                        .number("01")
                        .rowLoadMap(loadMap(lectureMap(36d, null, null), practiceMap(null, null, null), labsMap(null, null, null), hoursMap(72d, 2d, null, 36d, 36d, null, null)))
                        .rowTermLoadAction(3, loadMap(lectureMap(36d, null, null), practiceMap(null, null, null), labsMap(null, null, null), hoursMap(72d, 2d, null, 36d, 36d, null, null)), actionMap(null, 1, null, null, null))
                        .storedIndex("Б2.Б.1")
                        .userIndex("Б2.Б.1")
                        .b())

                    .child(groupRe("Теория и практика информатики")
                        .number("02")
                        .storedIndex("Б2.Б.2")
                        .userIndex("Б2.Б.2")
                        .child(registry("Основы компьютерной информатики")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("115")
                            .number("01")
                            .rowLoadMap(loadMap(lectureMap(null, null, null), practiceMap(18d, null, null), labsMap(18d, null, null), hoursMap(72d, 2d, null, 36d, 36d, null, null)))
                            .rowTermLoadAction(4, loadMap(lectureMap(null, null, null), practiceMap(18d, null, null), labsMap(18d, null, null), hoursMap(72d, 2d, null, 36d, 36d, null, null)), actionMap(null, 1, null, null, null))
                            .storedIndex("Б2.Б.2.1")
                            .userIndex("Б2.Б.2.1")
                            .b())
                        .child(registry("Информационные технологии в исторической науке")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("115")
                            .number("02")
                            .rowLoadMap(loadMap(lectureMap(null, null, null), practiceMap(18d, null, null), labsMap(18d, null, null), hoursMap(72d, 2d, null, 36d, 18d, null, null)))
                            .rowTermLoadAction(7, loadMap(lectureMap(null, null, null), practiceMap(18d, null, null), labsMap(18d, null, null), hoursMap(54d, 2d, null, 36d, 18d, null, null)), actionMap(1, null, null, null, null))
                            .storedIndex("Б2.Б.2.2")
                            .userIndex("Б2.Б.2.2")
                            .b())
                        .b())
                    .b())
                .child(structure("В")
                    .storedIndex("Б2.В")
                    .userIndex("Б2.В")
                    .child(groupIm("Выбор 1")
                        .number("01")
                        .rowLoadMap(loadMap(lectureMap(6d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(72d, 2d, null, 24d, 48d, null, null)))
                        .rowTermLoadAction(8, loadMap(lectureMap(6d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(72d, 2d, null, 24d, 48d, null, null)), actionMap(null, 1, null, null, null))
                        .storedIndex("Б2.В.ДВ.1")
                        .userIndex("Б2.В.ДВ.1")
                        .child(registry("Специнформатика (использование иероглифических поисковых систем на японском языке)")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("81")
                            .number("01")
                            .rowLoadMap(loadMap(lectureMap(6d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(72d, 2d, null, 24d, 48d, null, null)))
                            .rowTermLoadAction(8, loadMap(lectureMap(6d, null, null), practiceMap(18d, null, null), labsMap(null, null, null), hoursMap(72d, 2d, null, 24d, 48d, null, null)), actionMap(null, 1, null, null, null))
                            .storedIndex("Б2.В.ДВ.1.1")
                            .userIndex("Б2.В.ДВ.1.1")
                            .b())
                        .child(registry("Компьютерный практикум")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("81")
                            .number("02")
                            .rowLoadMap(loadMap(lectureMap(null, null, null), practiceMap(null, null, null), labsMap(null, null, null), hoursMap(null, null, null, null, null, null, null)))
                            .storedIndex("Б2.В.ДВ.1.2")
                            .userIndex("Б2.В.ДВ.1.2")
                            .b())
                        .b())
                    .b())
                .b());
    }

    private static abstract class Example4
    {
        private static final Map<String, String> CYCLE_ABBR_2_CYCLE_TITLE_MAP = ImmutableMap.<String, String>builder()
            .put("Б1", "Гуманитарный, социальный и экономический цикл")
            .put("Б2", "Математический и естественнонаучный цикл")
            .put("Б3", "Профессиональный цикл")
            .put("Б4", "Физическая культура")
            .put("Б5", "")
            .put("ФТД", "Факультативы")
            .build();

        private static final Collection<ImEpvRowDTO> ROOT_ROWS = Arrays.<ImEpvRowDTO>asList(
            structure("Б1")
                .storedIndex("Б1")
                .userIndex("Б1")
                .child(structure("Б")
                    .storedIndex("Б1.Б")
                    .userIndex("Б1.Б")
                    .child(registry("Иностранный язык")
                        .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                        .registryElementOwner("78")
                        .number("01")
                        .rowLoadMap(loadMap(lectureMap(null, null, null), practiceMap(36d, null, null), labsMap(null, null, null), hoursMap(864d, 24d, null, 36d, 72d, null, null)))
                        .rowTermLoadAction(1, loadMap(lectureMap(null, null, null), practiceMap(36d, null, null), labsMap(null, null, null), hoursMap(108d, 3d, null, 36d, 72d, null, null)), actionMap(null, 1, null, null, null))
                        .rowTermLoadAction(2, loadMap(lectureMap(null, null, null), practiceMap(null, null, null), labsMap(null, null, null), hoursMap(null, null, null, null, null, null, null)), actionMap(null, null, null, null, null))
                        .storedIndex("Б1.Б.1")
                        .userIndex("Б1.Б.1")
                        .b())
                    .b())
                .b());
    }

    private static abstract class Example5
    {
        private static final Map<String, String> CYCLE_ABBR_2_CYCLE_TITLE_MAP = ImmutableMap.<String, String>builder()
            .put("Б1", "Гуманитарный, социальный и экономический цикл")
            .put("Б2", "Математический и естественнонаучный цикл")
            .build();

        private static final Collection<ImEpvRowDTO> ROOT_ROWS = Arrays.<ImEpvRowDTO>asList(
            structure("Б1")
                .storedIndex("Б1")
                .userIndex("Б1")
                .child(structure("В")
                    .storedIndex("Б1.В")
                    .userIndex("Б1.В")
                    .child(groupIm("Право")
                        .number("01")
                        .rowLoadMap(loadMap(lectureMap(36d, null, null), practiceMap(36d, 14d, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 72d, 72d, null, null)))
                        .rowTermLoadAction(6, loadMap(lectureMap(36d, null, null), practiceMap(36d, 14d, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 72d, 72d, null, null)), actionMap(null, null, 1, null, null))
                        .storedIndex("Б1.В.ДВ.1")
                        .userIndex("Б1.В.ДВ.1")
                        .child(registry("Политология и политическая теория")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("7")
                            .number("01")
                            .rowLoadMap(loadMap(lectureMap(36d, null, null), practiceMap(36d, 14d, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 72d, 72d, null, null)))
                            .rowTermLoadAction(6, loadMap(lectureMap(36d, null, null), practiceMap(36d, 14d, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 72d, 72d, null, null)), actionMap(null, null, 1, null, null))
                            .storedIndex("Б1.В.ДВ.1.1")
                            .userIndex("Б1.В.ДВ.1.1")
                            .b())
                        .child(registry("Права человека")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("7")
                            .number("02")
                            .rowLoadMap(loadMap(lectureMap(36d, null, null), practiceMap(36d, 14d, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 72d, 72d, null, null)))
                            .rowTermLoadAction(6, loadMap(lectureMap(36d, null, null), practiceMap(36d, 14d, null), labsMap(null, null, null), hoursMap(144d, 4d, null, 72d, 72d, null, null)), actionMap(null, null, 1, null, null))
                            .storedIndex("Б1.В.ДВ.1.2")
                            .userIndex("Б1.В.ДВ.1.2")
                            .b())
                        .b())
                    .b())
                .b(),

            structure("Б2")
                .storedIndex("Б2")
                .userIndex("Б2")
                .child(structure("В")
                    .storedIndex("Б2.В")
                    .userIndex("Б2.В")
                    .child(groupIm("Выбор 1")
                        .number("01")
                        .rowLoadMap(loadMap(lectureMap(null, null, null), practiceMap(108d, 44d, null), labsMap(null, null, null), hoursMap(288d, 8d, null, 108d, 180d/*126d + 54d*/, 54d, null)))
                        .rowTermLoadAction(6, loadMap(lectureMap(null, null, null), practiceMap(108d, 44d, null), labsMap(null, null, null), hoursMap(288d, 8d, null, 108d, 180d/*126d + 54d*/, 54d, null)), actionMap(1, null, null, null, null))
                        .storedIndex("Б2.В.ДВ.1")
                        .userIndex("Б2.В.ДВ.1")
                        .child(registry("Лабораторный практикум по бухгалтерскому учету и налогообложению")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("2")
                            .number("01")
                            .rowLoadMap(loadMap(lectureMap(null, null, null), practiceMap(108d, 44d, null), labsMap(null, null, null), hoursMap(288d, 8d, null, 108d, 180d/*126d + 54d*/, 54d, null)))
                            .rowTermLoadAction(6, loadMap(lectureMap(null, null, null), practiceMap(108d, 44d, null), labsMap(null, null, null), hoursMap(288d, 8d, null, 108d, 180d/*126d + 54d*/, 54d, null)), actionMap(1, null, null, null, null))
                            .storedIndex("Б2.В.ДВ.1.1")
                            .userIndex("Б2.В.ДВ.1.1")
                            .b())
                        .child(registry("Информационные бухгалтерские системы")
                            .registryElementType(EppRegistryStructureCodes.REGISTRY_DISCIPLINE)
                            .registryElementOwner("2")
                            .number("02")
                            .rowLoadMap(loadMap(lectureMap(null, null, null), practiceMap(108d, 44d, null), labsMap(null, null, null), hoursMap(288d, 8d, null, 108d, 180d/*126d + 54d*/, 54d, null)))
                            .rowTermLoadAction(6, loadMap(lectureMap(null, null, null), practiceMap(108d, 44d, null), labsMap(null, null, null), hoursMap(288d, 8d, null, 108d, 180d/*126d + 54d*/, 54d, null)), actionMap(1, null, null, null, null))
                            .storedIndex("Б2.В.ДВ.1.2")
                            .userIndex("Б2.В.ДВ.1.2")
                            .b())
                        .b())
                    .b())
                .b());
    }

    private static ImEpvStructureRowDTOBuilder structure(String value) { return new ImEpvStructureRowDTOBuilder(value); }
    private static ImEpvRegistryRowDTOBuilder registry(String title) { return new ImEpvRegistryRowDTOBuilder(title); }
    private static ImEpvGroupImRowDTOBuilder groupIm(String title) { return new ImEpvGroupImRowDTOBuilder(title); }
    private static ImEpvGroupReRowDTOBuilder groupRe(String title) { return new ImEpvGroupReRowDTOBuilder(title); }

    @NotNull private static Map<String, Double> loadMap(final Map<String, Double> lectureMap, final Map<String, Double> practiceMap, final Map<String, Double> labsMap, final Map<String, Double> hoursMap)
    {
        return Collections.unmodifiableMap(new TreeMap<String, Double>()
        {
            {
                this.putAll(lectureMap);
                this.putAll(practiceMap);
                this.putAll(labsMap);
                this.putAll(hoursMap);
            }
        });
    }

    @NotNull private static Map<String, Double> hoursMap(final Double hours, final Double labor, final Double weeks, final Double audit, final Double selfwork, final Double control, final Double controlE)
    {
        return Collections.unmodifiableMap(new TreeMap<String, Double>()
        {
            {
                this.put(ImtsaImportDefines.L_TOTAL_SIZE, hours);
                this.put(ImtsaImportDefines.L_TOTAL_LABOR, labor);
                this.put(ImtsaImportDefines.L_TOTAL_WEEKS, weeks);
                this.put(ImtsaImportDefines.L_TOTAL_AUDIT, audit);
                this.put(ImtsaImportDefines.L_TOTAL_SELF_WORK, selfwork);
                this.put(ImtsaImportDefines.L_TOTAL_CONTROL, control);
                this.put(ImtsaImportDefines.L_TOTAL_CONTROL_E, controlE);
            }
        });
    }

    @NotNull private static Map<String, Double> lectureMap(Double hours, Double iHours, Double eHours)
    {
        return loadTypeMap(ImtsaImportDefines.L_LECTURES, hours, iHours, eHours);
    }

    @NotNull private static Map<String, Double> practiceMap(Double hours, Double iHours, Double eHours)
    {
        return loadTypeMap(ImtsaImportDefines.L_PRACTICE, hours, iHours, eHours);
    }

    @NotNull private static Map<String, Double> labsMap(Double hours, Double iHours, Double eHours)
    {
        return loadTypeMap(ImtsaImportDefines.L_LABS, hours, iHours, eHours);
    }

    @NotNull private static Map<String, Double> loadTypeMap(@NotNull final String fullCode, final Double hours, final Double iHours, final Double eHours)
    {
        Preconditions.checkNotNull(fullCode);
        return Collections.unmodifiableMap(new TreeMap<String, Double>()
        {
            {
                this.put(fullCode, hours);
                this.put(EppALoadType.iFullCode(fullCode), iHours);
                this.put(EppALoadType.eFullCode(fullCode), eHours);
            }
        });
    }

    private static Map<String, Integer> actionMap(Integer examValue, Integer setoffValue, Integer setoffDiffValue, Integer cwValue, Integer cpValue)
    {
        return ImParser.buildActionMap(examValue, setoffValue, setoffDiffValue, cwValue, cpValue);
    }

    private abstract static class ImEpvRowDTOBuilder<V extends ImEpvRowDTO, B extends ImEpvRowDTOBuilder<V,B>>
    {
        protected final V _value;

        public ImEpvRowDTOBuilder()
        {
            _value = instance();
        }

        protected abstract V instance();
        protected abstract B self();

        public B storedIndex(String storedIndex)
        {
            _value.setStoredIndex(storedIndex);
            return self();
        }

        public B userIndex(String userIndex)
        {
            _value.setUserIndex(userIndex);
            return self();
        }

        public B child(ImEpvRowDTO child)
        {
            _value.addChild(child);
            return self();
        }

        public V b() { return _value; } // stands for "build"
    }

    private static class ImEpvStructureRowDTOBuilder extends ImEpvRowDTOBuilder<ImEpvStructureRowDTO, ImEpvStructureRowDTOBuilder>
    {
        public ImEpvStructureRowDTOBuilder(String value)
        {
            super();
            _value.setValue(value);
        }

        @Override protected ImEpvStructureRowDTO instance() { return new ImEpvStructureRowDTO(); }
        @Override protected ImEpvStructureRowDTOBuilder self() { return this; }
    }

    private static class ImEpvGroupReRowDTOBuilder extends ImEpvRowDTOBuilder<ImEpvGroupReRowDTO, ImEpvGroupReRowDTOBuilder>
    {
        public ImEpvGroupReRowDTOBuilder(String title)
        {
            super();
            _value.setTitle(title);
        }

        public ImEpvGroupReRowDTOBuilder number(String number)
        {
            _value.setNumber(number);
            return self();
        }

        @Override protected ImEpvGroupReRowDTO instance() { return new ImEpvGroupReRowDTO(); }
        @Override protected ImEpvGroupReRowDTOBuilder self() { return this; }
    }

    private static abstract class ImEpvTermDistributedRowDTOBuilder<V extends ImEpvTermDistributedRowDTO, B extends ImEpvTermDistributedRowDTOBuilder<V, B>> extends ImEpvRowDTOBuilder<V, B>
    {
        protected ImEpvTermDistributedRowDTOBuilder(String title)
        {
            super();
            _value.setTitle(title);
        }

        public B number(String number)
        {
            _value.setNumber(number);
            return self();
        }

        public B rowLoadMap(Map<String, Double> rowLoadMap)
        {
            _value.getRowLoadMap().putAll(rowLoadMap);
            return self();
        }

        public B rowTermLoadAction(int term, Map<String, Double> loadMap, Map<String, Integer> actionMap)
        {
            _value.getRowTermLoadMap().put(term, loadMap);
            _value.getRowTermActionMap().put(term, actionMap);
            return self();
        }
    }

    private static class ImEpvGroupImRowDTOBuilder extends ImEpvTermDistributedRowDTOBuilder<ImEpvGroupImRowDTO, ImEpvGroupImRowDTOBuilder>
    {
        public ImEpvGroupImRowDTOBuilder(String title)
        {
            super(title);
        }

        public ImEpvGroupImRowDTOBuilder size(int size)
        {
            _value.setSize(size);
            return self();
        }

        @Override protected ImEpvGroupImRowDTO instance() { return new ImEpvGroupImRowDTO(); }
        @Override protected ImEpvGroupImRowDTOBuilder self() { return this; }
    }

    private static class ImEpvRegistryRowDTOBuilder extends ImEpvTermDistributedRowDTOBuilder<ImEpvRegistryRowDTO, ImEpvRegistryRowDTOBuilder>
    {
        public ImEpvRegistryRowDTOBuilder(String title)
        {
            super(title);
        }

        public ImEpvRegistryRowDTOBuilder registryElementType(String registryElementType)
        {
            _value.setRegistryElementType(registryElementType);
            return self();
        }

        public ImEpvRegistryRowDTOBuilder registryElementOwner(String registryElementOwner)
        {
            _value.setRegistryElementOwner(registryElementOwner);
            return self();
        }

        @Override protected ImEpvRegistryRowDTO instance() { return new ImEpvRegistryRowDTO(); }
        @Override protected ImEpvRegistryRowDTOBuilder self() { return this; }
    }

    public static final Map<String, ImParseResult> STANDARD_PARSE_RESULT_MAP = ImmutableMap.of(
        "example2.plm.xml", buildParseResult(Example2.CYCLE_ABBR_2_CYCLE_TITLE_MAP, Example2.ROOT_ROWS),
        "example3.plm.xml", buildParseResult(Example3.CYCLE_ABBR_2_CYCLE_TITLE_MAP, Example3.ROOT_ROWS),
        "example4.plm.xml", buildParseResult(Example4.CYCLE_ABBR_2_CYCLE_TITLE_MAP, Example4.ROOT_ROWS),
        "example5.plm.xml", buildParseResult(Example5.CYCLE_ABBR_2_CYCLE_TITLE_MAP, Example5.ROOT_ROWS)
    );
}