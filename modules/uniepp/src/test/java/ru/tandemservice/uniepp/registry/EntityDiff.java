package ru.tandemservice.uniepp.registry;

import org.tandemframework.core.entity.IEntity;

import java.util.HashMap;
import java.util.Map;


/**
 * Сравнение двух объектов test
 * @author okleptsova
 */
public class EntityDiff {
    public EntityDiff(IEntity before, IEntity after) {
        // TODO Исключение если переданные объекты - это ссылки на один и тот же объект в памяти
        // TODO Исключение, при попытке сравнить объекты разных классов
    }

    // Ссылка на объекты по которым строится дифф, объекты могут измениться и дифф перестанет соответствовать
    // Только для отладки
    // А может лучше вообще не сохранять?
    private IEntity _before;
    private IEntity _after;

    // Todo данные параметры должны быть нередактируемыми, заполняться в конструкторе при сравнении объектов
    public HashMap<String, Long> ids;
    public Map<String, HashMap<String, Object>> data;
    public Map<String, HashMap<String, Long>> links;


    private String _formatMessage(String errorDescription) {
        //todo форматирование ошибок сравнения
        return "";
    }

    /**
     * Проверить равенство значений заданого поля/ссылки
     *
     * @param property - название поля
     * @throws AssertionError
     */
    public void assertPropertyEqual(String property) {
    }

    /**
     * Проверить, что значение заданного поля/ссылки у actual объекта совпадает с заданным
     *
     * @param property    - название поля
     * @param expected - ожидаемое значение поля у after объекта
     * @throws AssertionError
     */
    public void assertPropertyAfter(String property, Object expected) {
    }

    /**
     * Проверить равенство всех полей данных
     *
     * @throws AssertionError
     */
    public void assertDataEqual() {
    }

    /**
     * Проверить, что внешние связи сравниваемых объектов идентичны
     *
     * @throws AssertionError
     */
    public void assertLinksEqual() {
    }

    /**
     * Проверить равенство id (из базы данных)
     *
     * @throws AssertionError
     */
    public void assertIdsEqual() {
    }

    /**
     * Проверить равенство всех полей
     *
     * @throws AssertionError
     */
    public void assertFullEqual() {
    }

    /**
     * Проверить равенство всех полей, исключая значения переданных полей
     *
     * @param properties - названия полей, которые не нужно проверять
     * @throws AssertionError
     */
    public void assertPropertiesEqualExclude(String[] properties) {
    }
}
