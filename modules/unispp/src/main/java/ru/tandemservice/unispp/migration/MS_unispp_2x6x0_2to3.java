package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unispp_2x6x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleDaily

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("sppscheduledaily_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("approved_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("archived_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("status_id", DBType.LONG).setNullable(false), 
				new DBColumn("group_id", DBType.LONG).setNullable(false), 
				new DBColumn("season_id", DBType.LONG), 
				new DBColumn("icaldata_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sppScheduleDaily");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleDailyEvent

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("sppscheduledailyevent_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("date_p", DBType.TIMESTAMP).setNullable(false), 
				new DBColumn("lectureroom_p", DBType.createVarchar(255)), 
				new DBColumn("teacher_p", DBType.createVarchar(255)), 
				new DBColumn("subject_p", DBType.createVarchar(255)), 
				new DBColumn("schedule_id", DBType.LONG), 
				new DBColumn("teacherval_id", DBType.LONG), 
				new DBColumn("subjectval_id", DBType.LONG), 
				new DBColumn("subjectloadtype_id", DBType.LONG), 
				new DBColumn("lectureroomval_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sppScheduleDailyEvent");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleDailyPrintForm

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("sppscheduledailyprintform_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("createdate_p", DBType.TIMESTAMP), 
				new DBColumn("groups_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("term_p", DBType.LONG).setNullable(false), 
				new DBColumn("eduyear_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("headersoop_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("edulevels_p", DBType.createVarchar(255)), 
				new DBColumn("courses_p", DBType.createVarchar(255)), 
				new DBColumn("session_p", DBType.createVarchar(255)), 
				new DBColumn("formativeorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("territorialorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("developform_id", DBType.LONG), 
				new DBColumn("season_id", DBType.LONG).setNullable(false), 
				new DBColumn("chief_id", DBType.LONG), 
				new DBColumn("adminoop_id", DBType.LONG), 
				new DBColumn("chiefumu_id", DBType.LONG), 
				new DBColumn("content_id", DBType.LONG).setNullable(false), 
				new DBColumn("createou_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sppScheduleDailyPrintForm");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleDailySeason

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("sppscheduledailyseason_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("startdate_p", DBType.DATE).setNullable(false), 
				new DBColumn("enddate_p", DBType.DATE).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sppScheduleDailySeason");

		}


    }
}