package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppScheduleWarningLog;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Журнал ошибок и предупреждений расписания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppScheduleWarningLogGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppScheduleWarningLog";
    public static final String ENTITY_NAME = "sppScheduleWarningLog";
    public static final int VERSION_HASH = 2021485117;
    private static IEntityMeta ENTITY_META;

    public static final String L_SPP_SCHEDULE = "sppSchedule";
    public static final String P_WARNING_MESSAGE = "warningMessage";

    private SppSchedule _sppSchedule;     // Расписание
    private String _warningMessage;     // Сообщение о предупреждении

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Расписание. Свойство не может быть null.
     */
    @NotNull
    public SppSchedule getSppSchedule()
    {
        return _sppSchedule;
    }

    /**
     * @param sppSchedule Расписание. Свойство не может быть null.
     */
    public void setSppSchedule(SppSchedule sppSchedule)
    {
        dirty(_sppSchedule, sppSchedule);
        _sppSchedule = sppSchedule;
    }

    /**
     * @return Сообщение о предупреждении. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1024)
    public String getWarningMessage()
    {
        return _warningMessage;
    }

    /**
     * @param warningMessage Сообщение о предупреждении. Свойство не может быть null.
     */
    public void setWarningMessage(String warningMessage)
    {
        dirty(_warningMessage, warningMessage);
        _warningMessage = warningMessage;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppScheduleWarningLogGen)
        {
            setSppSchedule(((SppScheduleWarningLog)another).getSppSchedule());
            setWarningMessage(((SppScheduleWarningLog)another).getWarningMessage());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppScheduleWarningLogGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppScheduleWarningLog.class;
        }

        public T newInstance()
        {
            return (T) new SppScheduleWarningLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sppSchedule":
                    return obj.getSppSchedule();
                case "warningMessage":
                    return obj.getWarningMessage();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sppSchedule":
                    obj.setSppSchedule((SppSchedule) value);
                    return;
                case "warningMessage":
                    obj.setWarningMessage((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sppSchedule":
                        return true;
                case "warningMessage":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sppSchedule":
                    return true;
                case "warningMessage":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sppSchedule":
                    return SppSchedule.class;
                case "warningMessage":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppScheduleWarningLog> _dslPath = new Path<SppScheduleWarningLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppScheduleWarningLog");
    }
            

    /**
     * @return Расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleWarningLog#getSppSchedule()
     */
    public static SppSchedule.Path<SppSchedule> sppSchedule()
    {
        return _dslPath.sppSchedule();
    }

    /**
     * @return Сообщение о предупреждении. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleWarningLog#getWarningMessage()
     */
    public static PropertyPath<String> warningMessage()
    {
        return _dslPath.warningMessage();
    }

    public static class Path<E extends SppScheduleWarningLog> extends EntityPath<E>
    {
        private SppSchedule.Path<SppSchedule> _sppSchedule;
        private PropertyPath<String> _warningMessage;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleWarningLog#getSppSchedule()
     */
        public SppSchedule.Path<SppSchedule> sppSchedule()
        {
            if(_sppSchedule == null )
                _sppSchedule = new SppSchedule.Path<SppSchedule>(L_SPP_SCHEDULE, this);
            return _sppSchedule;
        }

    /**
     * @return Сообщение о предупреждении. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleWarningLog#getWarningMessage()
     */
        public PropertyPath<String> warningMessage()
        {
            if(_warningMessage == null )
                _warningMessage = new PropertyPath<String>(SppScheduleWarningLogGen.P_WARNING_MESSAGE, this);
            return _warningMessage;
        }

        public Class getEntityClass()
        {
            return SppScheduleWarningLog.class;
        }

        public String getEntityName()
        {
            return "sppScheduleWarningLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
