/* $Id: SppScheduleDailyEventDSHandler.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
public class SppScheduleDailyEventDSHandler extends DefaultSearchDataSourceHandler
{
    public SppScheduleDailyEventDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long scheduleId = context.getNotNull("scheduleId");
        OrderDirection orderDirection = input.getEntityOrder().getDirection();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "e");
        builder.where(eq(property("e", SppScheduleDailyEvent.schedule().id()), value(scheduleId)));
        builder.order(property("e", SppScheduleDailyEvent.date()), orderDirection);
        builder.order(property("e", SppScheduleDailyEvent.bell().number()), orderDirection);

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }
}
