/* $Id: SppScheduleDailySeasonList.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.SeasonList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@Configuration
public class SppScheduleDailySeasonList extends BusinessComponentManager
{
    public final static String SCHEDULE_DAILY_SEASON_DS = "scheduleDailySeasonDS";

    @Bean
    public ColumnListExtPoint scheduleSeasonCL()
    {
        return columnListExtPointBuilder(SCHEDULE_DAILY_SEASON_DS)
                .addColumn(textColumn("title", SppScheduleDailySeason.title()).order().required(true))
                .addColumn(textColumn("yearPart", SppScheduleDailySeason.eppYearPart().title()).required(true))
                .addColumn(dateColumn("startDate", SppScheduleDailySeason.startDate()).order())
                .addColumn(dateColumn("endDate", SppScheduleDailySeason.endDate()).order())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("sppScheduleDailySeasonListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("message:ui.deleteAlert").permissionKey("sppScheduleDailySeasonListDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SCHEDULE_DAILY_SEASON_DS, scheduleSeasonCL(), SppScheduleDailyManager.instance().sppScheduleDailySeasonDSHandler()))
                .create();
    }
}
