package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unispp_2x6x8_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
                 new ScriptDependency("org.tandemframework", "1.6.15"),
                 new ScriptDependency("org.tandemframework.shared", "1.6.8")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность sppTeacherPreferenceWeekRow

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("sppteacherpreferenceweekrow_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("teacherpreference_id", DBType.LONG).setNullable(false),
				new DBColumn("bell_id", DBType.LONG).setNullable(false),
				new DBColumn("even_p", DBType.BOOLEAN).setNullable(false)
            );
            tool.createTable(dbt);
        }

        // гарантировать наличие кода сущности
        short entityCode = tool.entityCodes().ensure("sppTeacherPreferenceWeekRow");

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sppTeacherPreferenceElement

        // создано свойство weekRow
        {
            // создать колонку
            tool.createColumn("sppteacherpreferenceelement_t", new DBColumn("weekrow_id", DBType.LONG));
        }

        Statement stmt = tool.getConnection().createStatement();
        ResultSet src = stmt.executeQuery("select distinct pr.id, el.bell_id from sppteacherpreferenceelement_t el inner join sppteacherpreferencelist_t pr on pr.id = el.teacherpreference_id");
        PreparedStatement insertWeekRow = tool.prepareStatement("insert into sppteacherpreferenceweekrow_t (id, discriminator, teacherpreference_id, bell_id, even_p) values (?, ? ,?, ?, ?)");
        PreparedStatement updateElement = tool.prepareStatement("update sppteacherpreferenceelement_t set weekrow_id = ? where id = ?");

        // задать значение по умолчанию
        while (src.next())
        {
            Long teacherPreferenceId = src.getLong(1);
            Long bellsId = src.getLong(2);

            Statement stmt2 = tool.getConnection().createStatement();
            ResultSet src2 = stmt2.executeQuery("select id from sppteacherpreferenceelement_t where teacherpreference_id = '" + teacherPreferenceId + "'");

            Long weekRowId = EntityIDGenerator.generateNewId(entityCode);

            insertWeekRow.setLong(1, weekRowId);
            insertWeekRow.setShort(2, entityCode);
            insertWeekRow.setLong(3, teacherPreferenceId);
            insertWeekRow.setLong(4, bellsId);
            insertWeekRow.setBoolean(5, false);
            insertWeekRow.execute();

            while (src2.next())
            {
                Long elementId = src2.getLong(1);

                updateElement.setLong(1, weekRowId);
                updateElement.setLong(2, elementId);
                updateElement.execute();
            }
        }

        // сделать колонку NOT NULL
        tool.setColumnNullable("sppteacherpreferenceelement_t", "weekrow_id", false);

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sppTeacherPreferenceList

        // создано свойство diffEven
        {
            // создать колонку
            tool.createColumn("sppteacherpreferencelist_t", new DBColumn("diffeven_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            tool.executeUpdate("update sppteacherpreferencelist_t set diffeven_p=? where diffeven_p is null", false);

            // сделать колонку NOT NULL
            tool.setColumnNullable("sppteacherpreferencelist_t", "diffeven_p", false);
        }

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherPreferenceElement

		// создано обязательное свойство unwantedTime
		{
			// создать колонку
			tool.createColumn("sppteacherpreferenceelement_t", new DBColumn("unwantedtime_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update sppteacherpreferenceelement_t set unwantedtime_p=? where unwantedtime_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("sppteacherpreferenceelement_t", "unwantedtime_p", false);
		}

        // удалено свойство teacherPreference
        {
            // удалить колонку
            tool.dropColumn("sppteacherpreferenceelement_t", "teacherpreference_id");
        }

        // удалено свойство bell
        {
            // удалить колонку
            tool.dropColumn("sppteacherpreferenceelement_t", "bell_id");
        }
    }
}