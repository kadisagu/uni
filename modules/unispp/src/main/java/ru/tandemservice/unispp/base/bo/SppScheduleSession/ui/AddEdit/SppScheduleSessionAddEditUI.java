/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleGroupComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.View.SppScheduleSessionView;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.catalog.SppScheduleStatus;
import ru.tandemservice.unispp.base.entity.catalog.codes.SppScheduleStatusCodes;

/**
 * @author vnekrasov
 * @since 12/26/13
 */
@Input({
        @Bind(key = "groupId", binding = "groupId"),
        @Bind(key = "orgUnitId", binding = "orgUnitId"),
        @Bind(key = "scheduleId", binding = "scheduleId")
})
public class SppScheduleSessionAddEditUI extends UIPresenter
{
    private Long _groupId;
    private Long _orgUnitId;
    private Long _scheduleId;
    private SppScheduleSession _schedule;
    private ScheduleBell _oldBells;

    public Long getGroupId()
    {
        return _groupId;
    }

    public void setGroupId(Long groupId)
    {
        _groupId = groupId;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Long getScheduleId()
    {
        return _scheduleId;
    }

    public void setScheduleId(Long scheduleId)
    {
        _scheduleId = scheduleId;
    }

    public SppScheduleSession getSchedule()
    {
        return _schedule;
    }

    public void setSchedule(SppScheduleSession schedule)
    {
        _schedule = schedule;
    }

    public Boolean getGroupDisabled()
    {
        return null != _groupId || null != _scheduleId;
    }

    public Boolean getAddForm()
    {
        return null == _scheduleId;
    }

    public ScheduleBell getOldBells()
    {
        return _oldBells;
    }

    public void setOldBells(ScheduleBell oldBells)
    {
        _oldBells = oldBells;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _schedule)
        {
            if(null != _scheduleId)
            {
                _schedule = DataAccessServices.dao().get(_scheduleId);
                _oldBells = _schedule.getBells();
            }
            else
            {
                _schedule = new SppScheduleSession();
                if(null != _groupId)
                {
                    Group group = DataAccessServices.dao().get(_groupId);
                    _schedule.setGroup(group);
                }
                _schedule.setStatus(DataAccessServices.dao().getNotNull(SppScheduleStatus.class, SppScheduleStatus.code(), SppScheduleStatusCodes.FORMATION));
                _schedule.setApproved(false);
            }
            _orgUnitId = _orgUnitId != null ? _orgUnitId : _schedule.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getId();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleSessionAddEdit.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put("orgUnitId", _orgUnitId);
            if(getAddForm())
                dataSource.put(SppScheduleGroupComboDSHandler.PROP_GROUPS_NOT_IN_ARCHIVE, Boolean.TRUE);
        }
    }

    public void onClickApply()
    {
        SppScheduleSessionManager.instance().dao().saveOrUpdateSchedule(_schedule, _oldBells);
        if(getAddForm())
        {
            _uiConfig.deactivateComponent();
            _uiActivation.asDesktopRoot(SppScheduleSessionView.class).parameter("orgUnitId", _orgUnitId).parameter(PUBLISHER_ID, _schedule.getId()).activate();
        }
        else _uiConfig.deactivateComponent();
    }
}
