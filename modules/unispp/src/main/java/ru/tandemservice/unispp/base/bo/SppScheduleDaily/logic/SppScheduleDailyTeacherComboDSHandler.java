/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.EventAddEdit.SppScheduleDailyEventAddEditUI;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.EventAddEdit.SppScheduleSessionEventAddEditUI;
import ru.tandemservice.unispp.base.entity.*;
import ru.tandemservice.unispp.util.SppScheduleUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.*;

/**
 * @author Alexey Lopatin
 * @since 07.11.2014
 */
public class SppScheduleDailyTeacherComboDSHandler extends DefaultComboDataSourceHandler
{
    public SppScheduleDailyTeacherComboDSHandler(String ownerId)
    {
        super(ownerId, PpsEntry.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Date eventDate = context.get(SppScheduleDailyEventAddEditUI.PARAM_EVENT_DATE);
        ScheduleBellEntry bellEntry = context.get(SppScheduleDailyEventAddEditUI.PARAM_EVENT_BELL_ENTRY);
        if (eventDate == null || bellEntry == null)
            return new DSOutput(input);
        return super.execute(input, context);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        DQLSelectBuilder dql = ep.dqlBuilder;
        ExecutionContext context = ep.context;

        Long disciplineId = context.get("disciplineId");
        EppRegistryElement subject = context.get("subject");
        Long eventId = context.get(SppScheduleSessionEventAddEditUI.PARAM_EVENT_ID);
        Date eventDate = context.get(SppScheduleSessionEventAddEditUI.PARAM_EVENT_DATE);
        ScheduleBellEntry bellEntry = context.get(SppScheduleSessionEventAddEditUI.PARAM_EVENT_BELL_ENTRY);
        Long seasonId = context.get("seasonId");
        Long lectureRoomId = context.get("lectureRoomId");
        Boolean checkTeacherPreferences = context.getBoolean("checkTeacherPreferences", Boolean.FALSE);
        Boolean checkFreeTeachers = context.get("checkFreeTeachers");

        if (checkFreeTeachers && null != eventDate && null != bellEntry)
        {
            DQLSelectBuilder builder1 = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "p")
                    .column(property("p", SppSchedulePeriod.teacherVal().id()))
                    .where(eq(property("p", SppSchedulePeriod.dayOfWeek()), value(SppScheduleUtil.getDayOfWeek(eventDate))))
                    // даты пересеклись
                    .where(or(
                            and(
                                    isNotNull(property("p", SppSchedulePeriod.startDate())),
                                    le(property("p", SppSchedulePeriod.startDate()), valueDate(eventDate))),
                            and(
                                    isNull(property("p", SppSchedulePeriod.startDate())),
                                    le(property("p", SppSchedulePeriod.weekRow().schedule().season().startDate()), valueDate(eventDate)))))
                    .where(or(
                            and(
                                    isNotNull(property("p", SppSchedulePeriod.endDate())),
                                    ge(property("p", SppSchedulePeriod.endDate()), valueDate(eventDate))),
                            and(
                                    isNull(property("p", SppSchedulePeriod.endDate())),
                                    ge(property("p", SppSchedulePeriod.weekRow().schedule().season().endDate()), valueDate(eventDate)))))
                    // звонковая пара пересеклась
                    .where(gt(property("p", SppSchedulePeriod.weekRow().bell().endTime()), value(bellEntry.getStartTime())))
                    .where(lt(property("p", SppSchedulePeriod.weekRow().bell().startTime()), value(bellEntry.getEndTime())))
                    .where(isNotNull(property("p", SppSchedulePeriod.teacherVal())));

            DQLSelectBuilder builder2 = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "p")
                    .column(property("p", SppScheduleDailyEvent.teacherVal().id()))
                    .where(eventId == null ? null : ne(property("p", SppScheduleDailyEvent.id()), value(eventId)))
                    // день совпал
                    .where(eq(property("p", SppScheduleDailyEvent.date()), valueDate(eventDate)))
                    // звонковая пара пересеклась
                    .where(gt(property("p", SppScheduleDailyEvent.bell().endTime()), value(bellEntry.getStartTime())))
                    .where(lt(property("p", SppScheduleDailyEvent.bell().startTime()), value(bellEntry.getEndTime())))
                    .where(isNotNull(property("p", SppScheduleDailyEvent.teacherVal())));

            DQLSelectBuilder builder3 = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "p")
                    .column(property("p", SppScheduleSessionEvent.teacherVal().id()))
                    // день совпал
                    .where(eq(property("p", SppScheduleSessionEvent.date()), valueDate(eventDate)))
                    // звонковая пара пересеклась
                    .where(gt(property("p", SppScheduleSessionEvent.bell().endTime()), value(bellEntry.getStartTime())))
                    .where(lt(property("p", SppScheduleSessionEvent.bell().startTime()), value(bellEntry.getEndTime())))
                    .where(isNotNull(property("p", SppScheduleSessionEvent.teacherVal())));

            dql.where(notIn(property("e", PpsEntry.id()), builder1.buildQuery()));
            dql.where(notIn(property("e", PpsEntry.id()), builder2.buildQuery()));
            dql.where(notIn(property("e", PpsEntry.id()), builder3.buildQuery()));
        }

        if (checkTeacherPreferences && eventDate != null && lectureRoomId != null && seasonId != null && bellEntry != null)
        {
            // очень хитрый запрос с двумя подзапросами:
            // вытаскивает все id преподавателей, которые не хотят проводить лекцию в выбранной аудитории

            DQLSelectBuilder subQuery1Builder = new DQLSelectBuilder();
            subQuery1Builder.fromEntity(UniplacesPlace.class, "plc");
            // флаг нежелательности, id преподавателя, и аудитория (берём либо конкретную либо все из указанного здания)
            subQuery1Builder.column(property("tdpe", SppTeacherDailyPreferenceElement.unwantedTime()), "uTime");
            subQuery1Builder.column(property("tdpe", SppTeacherDailyPreferenceElement.teacherPreference().teacher().id()), "teacherId");
            subQuery1Builder.column(caseExpr(
                    isNotNull(property("tdpe", SppTeacherDailyPreferenceElement.lectureRoom().id())),
                    property("tdpe", SppTeacherDailyPreferenceElement.lectureRoom().id()),
                    property("plc", UniplacesPlace.id())), "lectureRoomId");
            // прицепим всё что нужно
            subQuery1Builder.joinPath(DQLJoinType.inner, UniplacesPlace.floor().unit().fromAlias("plc"), "unt");
            subQuery1Builder.joinEntity("unt", DQLJoinType.right, SppTeacherDailyPreferenceElement.class, "tdpe", eq(
                    property("tdpe", SppTeacherDailyPreferenceElement.building().id()),
                    property("unt", UniplacesUnit.building().id())));
            // фильтры
            subQuery1Builder.where(eq(property("tdpe", SppTeacherDailyPreferenceElement.teacherPreference().sppScheduleDailySeason().id()), value(seasonId)));
            subQuery1Builder.where(le(property("tdpe", SppTeacherDailyPreferenceElement.startDate()), valueDate(eventDate)));
            subQuery1Builder.where(ge(property("tdpe", SppTeacherDailyPreferenceElement.endDate()), valueDate(eventDate)));
            subQuery1Builder.where(or(
                    isNull(property("tdpe", SppTeacherDailyPreferenceElement.dayOfWeek())),
                    eq(property("tdpe", SppTeacherDailyPreferenceElement.dayOfWeek()), value(SppScheduleUtil.getDayOfWeek(eventDate)))));
            subQuery1Builder.where(or(
                    isNull(property("tdpe", SppTeacherDailyPreferenceElement.bell())),
                    eq(property("tdpe", SppTeacherDailyPreferenceElement.bell()), value(bellEntry))));
            subQuery1Builder.distinct();

            // так же пронумеруем предпочтения по приоритету (предпочтительные вперёд, без места вперёд)
            DQLSelectBuilder subQuery2Builder = new DQLSelectBuilder();
            subQuery2Builder.fromDataSource(subQuery1Builder.buildQuery(), "tmp1");
            subQuery2Builder.column("tmp1.uTime").column("tmp1.teacherId").column("tmp1.lectureRoomId");
            subQuery2Builder.column(rowNumberBuilder()
                    .partition(property("tmp1.teacherId"))
                    .order(property("tmp1.uTime"))
                    .order(property("tmp1.lectureRoomId"), OrderDirection.desc)
                    .build(), "rn");
            subQuery2Builder.where(or(
                    eq(property("tmp1.uTime"), value(Boolean.FALSE)),
                    isNull(property("tmp1.lectureRoomId")),
                    eq(property("tmp1.lectureRoomId"), value(lectureRoomId))));

            // исключим преподавателя исходя из наиболее приоритетного предпочтения
            DQLSelectBuilder excludeTeacherBuilder = new DQLSelectBuilder();
            excludeTeacherBuilder.fromDataSource(subQuery2Builder.buildQuery(), "tmp2");
            excludeTeacherBuilder.column("tmp2.teacherId");
            excludeTeacherBuilder.where(eq(property("tmp2.rn"), value(1)));
            excludeTeacherBuilder.where(or(
                    // либо предпочтительная не совпала
                    and(
                            eq(property("tmp2.uTime"), value(Boolean.FALSE)),
                            ne(property("tmp2.lectureRoomId"), value(lectureRoomId))),
                    // либо нежелательная совпала (или нежелательны все)
                    and(
                            eq(property("tmp2.uTime"), value(Boolean.TRUE)),
                            or(
                                    isNull(property("tmp2.lectureRoomId")),
                                    eq(property("tmp2.lectureRoomId"), value(lectureRoomId))))));

            dql.where(notIn(property("e", PpsEntry.person().id()), excludeTeacherBuilder.buildQuery()));
        }

        // только активные
        dql.where(exists(new DQLSelectBuilder().fromEntity(EmployeePost4PpsEntry.class, "ep4pps")
                .where(eq(property("ep4pps", EmployeePost4PpsEntry.ppsEntry().id()), property("e", PpsEntry.id())))
                .where(isNull(property("ep4pps", EmployeePost4PpsEntry.removalDate())))
                .buildQuery()));

        if (null != disciplineId)
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(SppDisciplinePreferenceList.class, "e")
                    .column(property("e", SppDisciplinePreferenceList.teacher().id()))
                    .where(eq(property("e", SppDisciplinePreferenceList.discipline().id()), value(disciplineId)));
            dql.where(in(property("e", PpsEntry.id()), subBuilder.buildQuery()));
        }
        if (!StringUtils.isEmpty(filter))
        {
            dql.where(like(upper(concat(
                    property("e", PpsEntry.person().identityCard().lastName()),
//                    property("e", EmployeePost.postRelation().postBoundedWithQGandQL().title()),
                    property("e", PpsEntry.orgUnit().fullTitle()))
            ), value(CoreStringUtils.escapeLike(filter, Boolean.TRUE))));
        }
        if (subject != null)
        {
            dql.where(eq(property("e", PpsEntry.orgUnit()), value(subject.getOwner())));
        }
        setOrderByProperty(PpsEntry.person().identityCard().lastName().s());
    }
}
