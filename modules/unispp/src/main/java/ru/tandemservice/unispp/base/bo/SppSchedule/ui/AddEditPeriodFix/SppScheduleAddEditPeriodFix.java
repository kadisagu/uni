/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddEditPeriodFix;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;

/**
 * @author nvankov
 * @since 9/5/13
 */
@Configuration
public class SppScheduleAddEditPeriodFix extends BusinessComponentManager
{
    public static final String BELL_DS = "bellDS";
    public static final String LECTURE_ROOM_DS = "lectureRoomDS";
    public static final String TEACHER_DS = "teacherDS";
    public static final String SUBJECT_DS = "subjectDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(BELL_DS, bellComboDSHandler()).addColumn("time", null, new IFormatter<ScheduleBellEntry>()
                {
                    @Override
                    public String format(ScheduleBellEntry source)
                    {
                        return source.getTime();
                    }
                }))
                .addDataSource(selectDS(LECTURE_ROOM_DS, SppScheduleManager.instance().lectureRoomComboDSHandler())/*.addColumn("title", UniplacesPlace.title().s()).addColumn("location", UniplacesPlace.fullLocationInfo().s())*/)
                .addDataSource(selectDS(TEACHER_DS, SppScheduleManager.instance().ppsComboDSHandler()).addColumn("fio", PpsEntry.titleFioInfoOrgUnit().s()))
                .addDataSource(selectDS(SUBJECT_DS, SppScheduleManager.instance().subjectComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler bellComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), ScheduleBellEntry.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                Long bellsId = ep.context.get("bellsId");
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", ScheduleBellEntry.schedule().id()), DQLExpressions.value(bellsId)));

            }

            @Override
            protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
            {
                ep.dqlBuilder.order(DQLExpressions.property("e", ScheduleBellEntry.startTime()), OrderDirection.asc);
            }
        };
    }
}
