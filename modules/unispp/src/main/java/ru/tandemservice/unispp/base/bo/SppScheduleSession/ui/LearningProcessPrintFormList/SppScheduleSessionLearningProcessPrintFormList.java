/* $Id: SppScheduleSessionOrgUnitPrintFormList.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.LearningProcessPrintFormList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionPrintFormDSHandler;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionPrintForm;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@Configuration
public class SppScheduleSessionLearningProcessPrintFormList extends BusinessComponentManager
{
    public static final String PRINT_FORM_DS = "schedulePrintFormDS";
    public static final String YEAR_PART_DS = "yearPartDS";
//    public static final String ADMIN_DS = "adminDS"; // перенесено в ДВФУ
    public static final String GROUP_DS = "groupDS";
    public static final String ORG_UNIT_DS = "orgUnitDS";

    @Bean
    public ColumnListExtPoint printFormCL()
    {
        return columnListExtPointBuilder(PRINT_FORM_DS)
                .addColumn(dateColumn("createDate", SppScheduleSessionPrintForm.createDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().required(true))
                .addColumn(textColumn("season", SppScheduleSessionPrintForm.season().titleWithTime()))
                .addColumn(textColumn("yearPart", SppScheduleSessionPrintForm.season().eppYearPart().title()))
                .addColumn(textColumn("ou", SppScheduleSessionPrintForm.formWithTerrTitle()).required(true))
                .addColumn(textColumn("groups", SppScheduleSessionPrintForm.groups()).order())
//                .addColumn(textColumn("admin", SppScheduleSessionPrintForm.adminOOP().titleWithOrgUnitShort())) // перенесено в ДВФУ
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickDownload").permissionKey("sppScheduleSessionLearningProcessPrintFormPrint"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRINT_FORM_DS, printFormCL(), printFormDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEAR_PART_DS, getName(), EppYearPart.defaultSelectDSHandler(getName())))
//                .addDataSource(selectDS(ADMIN_DS, SppScheduleSessionManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s())) // перенесено в ДВФУ
                .addDataSource(selectDS(GROUP_DS, SppScheduleManager.instance().groupComboDSHandler()))
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler printFormDSHandler()
    {
        return new SppScheduleSessionPrintFormDSHandler(getName());
    }

    @Bean
        public IDefaultComboDataSourceHandler orgUnitDSHandler()
        {
            return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
            {
                @Override
                protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
                {
                    super.applyWhereConditions(alias, dql, context);
                    dql.where(exists(
                            OrgUnitToKindRelation.class,
                            OrgUnitToKindRelation.orgUnitKind().code().s(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING,
                            OrgUnitToKindRelation.L_ORG_UNIT, property("e")
                    ));
                }
            }
                    .order(OrgUnit.title())
                    .filter(OrgUnit.title());
        }
}
