/* $Id: */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.EventAddEdit.SppScheduleDailyEventAddEditUI;
import ru.tandemservice.unispp.base.entity.*;
import ru.tandemservice.unispp.util.SppScheduleUtil;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 7/11/14
 */
public class SppScheduleDailyLectureRoomComboDSHandler extends SimpleTitledComboDataSourceHandler
{
    public static final String VIEW_PROP_TITLE = "placeTitle";
    public static final String VIEW_PROP_LOCATION = "placeLocation";
    public static final String VIEW_PROP_CAPACITY = "capacity";

    public SppScheduleDailyLectureRoomComboDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String filter = input.getComboFilterByValue();

        int studentGrpCnt = context.get("studentGrpCnt") == null ? 0 : (int) context.get("studentGrpCnt");
        Long eventId = context.get(SppScheduleDailyEventAddEditUI.PARAM_EVENT_ID);
        Date eventDate = context.get("eventDate");
        Long teacherId = context.get("teacherId");
        Long disciplineId = context.get("disciplineId");
        Long seasonId = context.get("seasonId");
        UniplacesClassroomType lectureRoomType = context.get("lectureRoomType");
        ScheduleBellEntry bellEntry = context.get(SppScheduleDailyEventAddEditUI.PARAM_EVENT_BELL_ENTRY);

        boolean checkFreeLectureRooms = context.get("checkFreeLectureRooms");
        boolean checkTeacherPreferences = context.get("checkTeacherPreferences");

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "s");
        dql.column("s");

        if (!StringUtils.isEmpty(filter))
        {
            dql.where(like(DQLFunctions.upper(property("s", UniplacesPlace.title())), value(CoreStringUtils.escapeLike(filter))));
        }

        dql.where(eq(property("s", UniplacesPlace.condition().code()), value("1")));
        if (studentGrpCnt > 0)
            dql.where(or(ge(property("s", UniplacesPlace.capacity()), value(studentGrpCnt)), isNull(property("s", UniplacesPlace.capacity()))));

        if (lectureRoomType != null)
            dql.where(eq(property("s", UniplacesPlace.classroomType().id()), value(lectureRoomType.getId())));

        if (checkFreeLectureRooms && null != eventDate && null != bellEntry)
        {
            DQLSelectBuilder builder1 = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e")
                    .column(property("e", SppSchedulePeriod.lectureRoomVal().id()))
                    .where(eq(property("e", SppSchedulePeriod.dayOfWeek()), value(SppScheduleUtil.getDayOfWeek(eventDate))))
                    // даты пересеклись
                    .where(or(
                            and(
                                    isNotNull(property("e", SppSchedulePeriod.startDate())),
                                    le(property("e", SppSchedulePeriod.startDate()), valueDate(eventDate))),
                            and(
                                    isNull(property("e", SppSchedulePeriod.startDate())),
                                    le(property("e", SppSchedulePeriod.weekRow().schedule().season().startDate()), valueDate(eventDate)))))
                    .where(or(
                            and(
                                    isNotNull(property("e", SppSchedulePeriod.endDate())),
                                    ge(property("e", SppSchedulePeriod.endDate()), valueDate(eventDate))),
                            and(
                                    isNull(property("e", SppSchedulePeriod.endDate())),
                                    ge(property("e", SppSchedulePeriod.weekRow().schedule().season().endDate()), valueDate(eventDate)))))
                    // звонковая пара пересеклась
                    .where(gt(property("e", SppSchedulePeriod.weekRow().bell().endTime()), value(bellEntry.getStartTime())))
                    .where(lt(property("e", SppSchedulePeriod.weekRow().bell().startTime()), value(bellEntry.getEndTime())))
                    .where(isNotNull(property("e", SppSchedulePeriod.lectureRoomVal())));

            DQLSelectBuilder builder2 = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "e")
                    .column(property("e", SppScheduleDailyEvent.lectureRoomVal().id()))
                    .where(eventId == null ? null : ne(property("e", SppScheduleDailyEvent.id()), value(eventId)))
                    // день совпал
                    .where(eq(property("e", SppScheduleDailyEvent.date()), valueDate(eventDate)))
                    // звонковая пара пересеклась
                    .where(gt(property("e", SppScheduleDailyEvent.bell().endTime()), value(bellEntry.getStartTime())))
                    .where(lt(property("e", SppScheduleDailyEvent.bell().startTime()), value(bellEntry.getEndTime())))
                    .where(isNotNull(property("e", SppScheduleDailyEvent.lectureRoomVal())));

            DQLSelectBuilder builder3 = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "e")
                    .column(property("e", SppScheduleSessionEvent.lectureRoomVal().id()))
                    // день совпал
                    .where(eq(property("e", SppScheduleSessionEvent.date()), valueDate(eventDate)))
                    // звонковая пара пересеклась
                    .where(gt(property("e", SppScheduleSessionEvent.bell().endTime()), value(bellEntry.getStartTime())))
                    .where(lt(property("e", SppScheduleSessionEvent.bell().startTime()), value(bellEntry.getEndTime())))
                    .where(isNotNull(property("e", SppScheduleSessionEvent.lectureRoomVal())));

            dql.where(notIn(property("s", UniplacesPlace.id()), builder1.buildQuery()));
            dql.where(notIn(property("s", UniplacesPlace.id()), builder2.buildQuery()));
            dql.where(notIn(property("s", UniplacesPlace.id()), builder3.buildQuery()));
        }

        if (checkTeacherPreferences && teacherId != null && eventDate != null && seasonId != null && bellEntry != null)
        {
            Long personId = UniDaoFacade.getCoreDao().getProperty(PpsEntry.class, PpsEntry.person().id().s(), PpsEntry.P_ID, teacherId);

            DQLSelectBuilder subQuery1Builder = new DQLSelectBuilder();
            subQuery1Builder.fromEntity(UniplacesPlace.class, "plc");
            // флаг нежелательности и аудитория (берём либо конкретную либо все из указанного здания)
            subQuery1Builder.column(property("tdpe", SppTeacherDailyPreferenceElement.unwantedTime()), "uTime");
            subQuery1Builder.column(caseExpr(
                    isNotNull(property("tdpe", SppTeacherDailyPreferenceElement.lectureRoom().id())),
                    property("tdpe", SppTeacherDailyPreferenceElement.lectureRoom().id()),
                    property("plc", UniplacesPlace.id())), "lectureRoomId");
            // прицепим всё что нужно
            subQuery1Builder.joinPath(DQLJoinType.inner, UniplacesPlace.floor().unit().fromAlias("plc"), "unt");
            subQuery1Builder.joinEntity("unt", DQLJoinType.right, SppTeacherDailyPreferenceElement.class, "tdpe", eq(
                    property("tdpe", SppTeacherDailyPreferenceElement.building().id()),
                    property("unt", UniplacesUnit.building().id())));
            // фильтры
            subQuery1Builder.where(eq(property("tdpe", SppTeacherDailyPreferenceElement.teacherPreference().sppScheduleDailySeason().id()), value(seasonId)));
            subQuery1Builder.where(eq(property("tdpe", SppTeacherDailyPreferenceElement.teacherPreference().teacher().id()), value(personId)));
            subQuery1Builder.where(le(property("tdpe", SppTeacherDailyPreferenceElement.startDate()), valueDate(eventDate)));
            subQuery1Builder.where(ge(property("tdpe", SppTeacherDailyPreferenceElement.endDate()), valueDate(eventDate)));
            subQuery1Builder.where(eq(property("tdpe", SppTeacherDailyPreferenceElement.bell()), value(bellEntry)));
            subQuery1Builder.where(or(
                    isNull(property("tdpe", SppTeacherDailyPreferenceElement.dayOfWeek())),
                    eq(property("tdpe", SppTeacherDailyPreferenceElement.dayOfWeek()), value(SppScheduleUtil.getDayOfWeek(eventDate)))));
            subQuery1Builder.where(or(
                    isNull(property("tdpe", SppTeacherDailyPreferenceElement.bell())),
                    eq(property("tdpe", SppTeacherDailyPreferenceElement.bell()), value(bellEntry))));
            subQuery1Builder.distinct();

            // отсортируем (предпочтительные вперёд, без места вперёд)
            DQLSelectBuilder subQuery2Builder = new DQLSelectBuilder();
            subQuery2Builder.fromDataSource(subQuery1Builder.buildQuery(), "tmp1");
            subQuery2Builder.column("tmp1.uTime").column("tmp1.lectureRoomId");
            subQuery2Builder.order(property("tmp1.uTime")).order(property("tmp1.lectureRoomId"), OrderDirection.desc);

            // false - предпочтительные, true - нежелательные
            Map<Boolean, List<Long>> preferenceLectureRoomMap = subQuery2Builder.createStatement(getSession()).list().stream().map(o -> (Object[])o)
                    .collect(Collectors.groupingBy(o -> (Boolean)o[0], Collectors.mapping(o -> (Long)o[1], Collectors.toList())));

            IDQLExpression expression = null;
            List<Long> includeLectureRoomIds = preferenceLectureRoomMap.get(Boolean.FALSE);
            List<Long> excludeLectureRoomIds = preferenceLectureRoomMap.get(Boolean.TRUE);
            // если правило распространяется на все аудитории, то оно будет в самом начале списка (lectureRoomId = null)
            if (includeLectureRoomIds != null && !includeLectureRoomIds.isEmpty())
            {
                if (includeLectureRoomIds.get(0) != null)
                    expression = in(property("s", UniplacesPlace.id()), includeLectureRoomIds);
            }
            else if (excludeLectureRoomIds != null && !excludeLectureRoomIds.isEmpty())
            {
                if (excludeLectureRoomIds.get(0) == null)
                    expression = nothing();
                else
                    expression = notIn(property("s", UniplacesPlace.id()), excludeLectureRoomIds);
            }

            if (expression != null) dql.where(expression);
        }

        DQLSelectBuilder builder2 = new DQLSelectBuilder().fromEntity(SppDisciplinePreferenceList.class, "e");
        builder2.column("e");
        builder2.where(eq(property("e", SppDisciplinePreferenceList.discipline().id()), value(disciplineId)));

        List<SppDisciplinePreferenceList> elementList = createStatement(builder2).list();

        List<UniplacesBuilding> buildingList = new ArrayList<>();
        List<UniplacesPlace> lectureRoomList = new ArrayList<>();


        for (SppDisciplinePreferenceList element : elementList)
        {
            if (element.getBuilding() != null)
                buildingList.add(element.getBuilding());
            if (element.getLectureRoom() != null)
                lectureRoomList.add(element.getLectureRoom());
        }

        DQLSelectBuilder dql1 = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "p");
        dql1.where(in(property("p", UniplacesPlace.floor().unit().building()), buildingList));
        List<UniplacesPlace> addLectureRoomList = createStatement(dql1).list();
        lectureRoomList.addAll(addLectureRoomList);

        if (!lectureRoomList.isEmpty())
            dql.where(in(property("s", UniplacesPlace.id()), CommonDAO.ids(lectureRoomList)));

        dql.order(property("s", UniplacesPlace.title()));

        List<UniplacesPlace> placesList = dql.createStatement(context.getSession()).list();

        List<DataWrapper> wrappers = new ArrayList<>();

        for (UniplacesPlace lectureRoom : placesList)
        {
            DataWrapper wrapper = new DataWrapper();
            wrapper.setId(lectureRoom.getId());
            wrapper.setTitle(lectureRoom.getTitle() +
                    (lectureRoom.getCapacity() != null ? ", вмст " + lectureRoom.getCapacity().toString() : "") +
                    ", " + lectureRoom.getFullLocationInfo());
            wrapper.setProperty(VIEW_PROP_TITLE, lectureRoom.getTitle());
            wrapper.setProperty(VIEW_PROP_LOCATION, lectureRoom.getFullLocationInfo());
            wrapper.setProperty(VIEW_PROP_CAPACITY, lectureRoom.getCapacity() != null ? ", вмст " + lectureRoom.getCapacity().toString() : "");

            wrappers.add(wrapper);

        }
        setFilterByProperty(VIEW_PROP_TITLE);

        context.put(UIDefines.COMBO_OBJECT_LIST, wrappers);
        return super.execute(input, context);
    }
}
