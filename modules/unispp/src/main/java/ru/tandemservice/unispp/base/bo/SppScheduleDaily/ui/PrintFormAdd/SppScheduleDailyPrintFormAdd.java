/* $Id: SppScheduleDailyPrintFormAdd.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.PrintFormAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyComboDSHandler;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@Configuration
public class SppScheduleDailyPrintFormAdd extends BusinessComponentManager
{
    public static final String FORMATIVE_OU_DS = "formativeOrgUnitDS";
//    public static final String TERRITORIAL_OU_DS = "territorialOrgUnitDS"; // перенесено в ДВФУ
    public static final String EDU_LEVEL_DS = "eduLevelDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String COURSE_DS = "courseDS";
    public static final String SEASON_DS = "seasonDS";
    public static final String BELLS_DS = "bellsDS";
    public static final String SCHEDULES_DS = "schedulesDS";
//    public static final String TERM_DS = "termDS"; // перенесено в ДВФУ
    public static final String DAILY_DS = "dailyDS";
    public static final String EDUCATION_YEAR = "educationYearDS";
    public static final String EMPLOYEE_POST_DS = "employeePostDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(FORMATIVE_OU_DS, SppScheduleDailyManager.instance().sppGroupComboDSHandler()))
//                .addDataSource(selectDS(TERRITORIAL_OU_DS, SppScheduleDailyManager.instance().sppGroupComboDSHandler()).addColumn("title", OrgUnit.territorialFullTitle().s())) // перенесено в ДВФУ
                .addDataSource(selectDS(EDU_LEVEL_DS, SppScheduleDailyManager.instance().sppGroupComboDSHandler()).addColumn("title", EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, SppScheduleDailyManager.instance().sppGroupComboDSHandler()))
                .addDataSource(selectDS(COURSE_DS, SppScheduleDailyManager.instance().sppGroupComboDSHandler()))
                .addDataSource(selectDS(SEASON_DS, schedulesDailyComboDSHandler()).addColumn("title", null, new IFormatter<SppScheduleDailySeason>()
                {
                    @Override
                    public String format(SppScheduleDailySeason source)
                    {
                        return source.getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getStartDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndDate()) + ")";
                    }
                }))
                .addDataSource(selectDS(BELLS_DS, schedulesDailyComboDSHandler()))
                .addDataSource(selectDS(SCHEDULES_DS, schedulesDailyComboDSHandler()).addColumn("group", SppScheduleDaily.group().title().s()).addColumn("schedule", SppScheduleDaily.title().s()))
//                .addDataSource(selectDS(TERM_DS, SppScheduleManager.instance().termComboDSHandler())) // перенесено в ДВФУ
                .addDataSource(selectDS(DAILY_DS, SppScheduleDailyManager.instance().dailyComboDSHandler()))
                .addDataSource(selectDS(EDUCATION_YEAR, educationYearComboDSHandler()))
                .addDataSource(selectDS(EMPLOYEE_POST_DS, SppScheduleDailyManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler educationYearComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EducationYear.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler schedulesDailyComboDSHandler()
    {
        return new SppScheduleDailyComboDSHandler(getName());
    }
}
