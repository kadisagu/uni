/* $Id: */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.Copy;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleGroupComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.View.SppScheduleView;
import ru.tandemservice.unispp.base.entity.SppSchedule;

/**
 * @author vnekrasov
 * @since 7/7/14
 */
@Input({
        @Bind(key = "scheduleId", binding = "scheduleId"),
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class SppScheduleCopyUI extends UIPresenter
{
    private Long _scheduleId;
    private SppSchedule _schedule;
    private SppSchedule _newSchedule;
    private Long _orgUnitId;
    private boolean _swapWeeks;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Long getScheduleId()
    {
        return _scheduleId;
    }

    public void setScheduleId(Long scheduleId)
    {
        _scheduleId = scheduleId;
    }

    public SppSchedule getSchedule()
    {
        return _schedule;
    }

    public void setSchedule(SppSchedule schedule)
    {
        _schedule = schedule;
    }

    public boolean isSwapWeeks()
    {
        return _swapWeeks;
    }

    public void setSwapWeeks(boolean swapWeeks)
    {
        _swapWeeks = swapWeeks;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _newSchedule)
        {
            if(null != _scheduleId)
            {
                _schedule = DataAccessServices.dao().get(_scheduleId);
                _newSchedule = new SppSchedule();
                _orgUnitId = _orgUnitId != null ? _orgUnitId : _schedule.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getId();
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleCopy.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleGroupComboDSHandler.PROP_GROUP_ID, _schedule.getGroup().getId());
            dataSource.put(SppScheduleGroupComboDSHandler.PROP_ORG_UNIT_ID, _orgUnitId);
            dataSource.put(SppScheduleGroupComboDSHandler.PROP_GROUPS_NOT_IN_ARCHIVE, Boolean.TRUE);
        }
    }

    public void onClickApply()
    {
        _newSchedule.setApproved(_schedule.isApproved());
        _newSchedule.setArchived(_schedule.isArchived());
        _newSchedule.setDiffEven(_schedule.isDiffEven());
        _newSchedule.setStatus(_schedule.getStatus());
        _newSchedule.setBells(_schedule.getBells());
        _newSchedule.setSeason(_schedule.getSeason());
        _newSchedule.setIcalData(null);
        SppScheduleManager.instance().dao().copySchedule(_scheduleId, _newSchedule, _swapWeeks);

        deactivate();
        _uiActivation.asDesktopRoot(SppScheduleView.class).parameter(PUBLISHER_ID, _newSchedule.getId()).activate();
    }
}
