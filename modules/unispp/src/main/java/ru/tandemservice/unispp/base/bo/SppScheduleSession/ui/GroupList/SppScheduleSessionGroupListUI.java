/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.GroupList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.AddEdit.SppScheduleSessionAddEdit;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;

/**
 * @author vnekrasov
 * @since 12/26/13
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "groupId", required = true)
})
public class SppScheduleSessionGroupListUI extends UIPresenter
{
    private Long _groupId;
    private Group _group;

    private BaseSearchListDataSource _scheduleDS;

    public BaseSearchListDataSource getScheduleDS()
    {
        if(null == _scheduleDS)
            _scheduleDS = _uiConfig.getDataSource(SppScheduleSessionGroupList.SCHEDULE_DS);
        return _scheduleDS;
    }

    public Long getGroupId()
    {
        return _groupId;
    }

    public void setGroupId(Long groupId)
    {
        _groupId = groupId;
    }

    public Boolean getAddScheduleVisible()
    {
        return !_group.isArchival();
    }

    public Boolean getEntityEditDisabled()
    {
        SppScheduleSession schedule = getScheduleDS().getCurrent();
        return schedule.isApproved() || schedule.isArchived();
    }

    public Boolean getEntityDeleteDisabled()
    {
        return false;
    }

    @Override
    public void onComponentRefresh()
    {
        if(_group == null)
            _group = DataAccessServices.dao().get(_groupId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleSessionGroupList.SCHEDULE_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleSessionDSHandler.PROP_GROUP_ID, _groupId);
            dataSource.put(SppScheduleSessionDSHandler.PROP_TITLE, _uiSettings.get("title"));
        }
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(SppScheduleSessionAddEdit.class).parameter("scheduleId", getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        SppScheduleSessionManager.instance().dao().deleteSchedule(getListenerParameterAsLong());
    }

    public void onClickAddSchedule()
    {
        _uiActivation.asRegionDialog(SppScheduleSessionAddEdit.class)
                .parameter("groupId", _groupId)
                .activate();
    }
}
