/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;

/**
 * @author nvankov
 * @since 9/3/13
 */
public class SppOrgUnitComboDSHandler extends SimpleTitledComboDataSourceHandler
{
    public SppOrgUnitComboDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String kindCode = context.getNotNull("kindCode");

        String titleProperty;
        String orderProperty;

        if (kindCode.equals(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
        {
            titleProperty = OrgUnitToKindRelation.orgUnit().territorialFullTitle().fromAlias("e").s();
            orderProperty = OrgUnitToKindRelation.orgUnit().territorialFullTitle().fromAlias("e").s();
        } else
        {
            titleProperty = OrgUnitToKindRelation.orgUnit().fullTitle().fromAlias("e").s();
            orderProperty = OrgUnitToKindRelation.orgUnit().fullTitle().fromAlias("e").s();
        }

        DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "e")
                .column(DQLExpressions.property(OrgUnitToKindRelation.orgUnit().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(OrgUnitToKindRelation.orgUnit().archival().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)))
                .where(DQLExpressions.eq(DQLExpressions.property(OrgUnitToKindRelation.orgUnitKind().code().fromAlias("e")), DQLExpressions.value(kindCode)))
                .order(DQLExpressions.property(orderProperty));

        setOrderByProperty(orderProperty);
        setFilterByProperty(titleProperty);
        context.put(UIDefines.COMBO_OBJECT_LIST, createStatement(b).list());
        return super.execute(input, context);
    }
}
