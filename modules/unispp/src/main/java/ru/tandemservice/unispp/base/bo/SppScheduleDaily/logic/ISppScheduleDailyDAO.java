/* $Id: ISppScheduleDailyDAO.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic;

import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;

import java.util.List;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
public interface ISppScheduleDailyDAO
{
    void createOrUpdateSeason(SppScheduleDailySeason season);

    void saveSchedules(List<SppScheduleDaily> scheduleList);

    void saveOrUpdateSchedule(SppScheduleDaily schedule, ScheduleBell oldBells);

    void copySchedule(Long scheduleDailyId, SppScheduleDaily newScheduleDaily);

    void sentToArchive(Long scheduleId);

    void restoreFromArchive(Long scheduleId);

    UniplacesClassroomType getDisciplineLectureRoomType(EppRegistryElement discipline, EppALoadType loadType);

    /**
     * Выполнение проверок, необходимых перед утверждением расписания
     *
     * @param schedule расписание
     */
    void validate(SppScheduleDaily schedule);

    void approve(Long scheduleId);

    void sentToFormation(Long scheduleId);

    boolean getScheduleDataOrRecipientsChanged(Long scheduleId);

    void createOrUpdateEvent(SppScheduleDailyEvent event);

    void deleteSchedule(Long scheduleId);

    void sendICalendar(Long scheduleId);

    SppScheduleDailyPrintForm savePrintForm(SppScheduleDailyPrintParams printData);

    void sendICalendars();

    void updateScheduleAsChanged(Long scheduleId);
}
