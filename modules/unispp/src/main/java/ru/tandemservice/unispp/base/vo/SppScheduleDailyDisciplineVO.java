/*$Id$*/
package ru.tandemservice.unispp.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;

/**
 * @author DMITRY KNYAZEV
 * @since 18.11.2015
 */
public class SppScheduleDailyDisciplineVO extends DataWrapper
{
    private final EppRegistryDiscipline _discipline;
    private final EppFControlActionType _formControl;
    private final EppALoadType _loadType;

    public SppScheduleDailyDisciplineVO(EppRegistryDiscipline discipline, EppFControlActionType formControl)
    {
        super(discipline.getId() + formControl.getId(), discipline.getTitle() + " (" + formControl.getTitle() + ")");
        _discipline = discipline;
        _formControl = formControl;
        _loadType = null;
    }

    public SppScheduleDailyDisciplineVO(EppRegistryDiscipline discipline, EppALoadType loadType)
    {
        super(discipline.getId() + loadType.getId(), discipline.getTitle() + " (" + loadType.getTitle() + ")");
        _discipline = discipline;
        _loadType = loadType;
        _formControl = null;
    }

    public EppRegistryDiscipline getDiscipline()
    {
        return _discipline;
    }

    public EppFControlActionType getFormControl()
    {
        return _formControl;
    }

    public EppALoadType getLoadType()
    {
        return _loadType;
    }
}
