/* $Id: SppScheduleDailyPrintFormAddUI.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.PrintFormAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.GroupComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyPrintParams;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@Input({
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class SppScheduleDailyPrintFormAddUI extends UIPresenter
{
    private Long _orgUnitId;
//    private Boolean allPosts = Boolean.FALSE; // перенесено в ДВФУ

    private SppScheduleDailyPrintParams _printData;

    private boolean _isAddForm;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public SppScheduleDailyPrintParams getPrintData()
    {
        return _printData;
    }

    public void setPrintData(SppScheduleDailyPrintParams printData)
    {
        _printData = printData;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _printData)
        {
            _isAddForm = true;
            _printData = new SppScheduleDailyPrintParams();
            OrgUnit currOrgUnit = DataAccessServices.dao().get(_orgUnitId);
            _printData.setCurrentOrgUnit(currOrgUnit);
            _printData.setFormativeOrgUnit(currOrgUnit);
            _printData.setEducationYear(DataAccessServices.dao().get(EducationYear.class, EducationYear.current(), true));
        }
        else
            _isAddForm = false;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("scheduleData", _printData);
//        if (SppScheduleDailyPrintFormAdd.TERRITORIAL_OU_DS.equals(dataSource.getName()))
//        {
//            dataSource.put("column", GroupComboDSHandler.Columns.TERRITORIAL_ORG_UNIT);
//        } // перенесено в ДВФУ
        if (SppScheduleDailyPrintFormAdd.FORMATIVE_OU_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.FORMATIVE_ORG_UNIT);
        }
        if (SppScheduleDailyPrintFormAdd.EDU_LEVEL_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.EDUCATION_LEVEL);
        }
        if (SppScheduleDailyPrintFormAdd.DEVELOP_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.DEVELOP_FORM);
        }
        if (SppScheduleDailyPrintFormAdd.COURSE_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.COURSE);
        }
        if (SppScheduleDailyPrintFormAdd.SEASON_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", SppScheduleDailyComboDSHandler.Columns.SEASON);
        }
        if (SppScheduleDailyPrintFormAdd.BELLS_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", SppScheduleDailyComboDSHandler.Columns.BELLS);
        }
        if (SppScheduleDailyPrintFormAdd.SCHEDULES_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", SppScheduleDailyComboDSHandler.Columns.SCHEDULE);
        }
    }

    public void onChangeFormOU()
    {
        if (null != _printData.getFormativeOrgUnit())
            _printData.setChief((EmployeePost) _printData.getFormativeOrgUnit().getHead());
    }

    public void onClickApply()
    {
        SppScheduleDailyPrintForm printForm = SppScheduleDailyManager.instance().dao().savePrintForm(_printData);
        byte[] content = printForm.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("xls").fileName("schedule.xls").document(content), true);
        deactivate();
    }

    public boolean isAddForm()
    {
        return _isAddForm;
    }

    public void setAddForm(boolean addForm)
    {
        _isAddForm = addForm;
    }
}
