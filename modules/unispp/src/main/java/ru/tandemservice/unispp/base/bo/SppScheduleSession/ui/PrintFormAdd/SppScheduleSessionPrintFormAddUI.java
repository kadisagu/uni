/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.PrintFormAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.GroupComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionPrintParams;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionPrintForm;

/**
 * @author vnekrasov
 * @since 12/27/13
 */
@Input({
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class SppScheduleSessionPrintFormAddUI extends UIPresenter
{
    private Long _orgUnitId;
//    private Boolean allPosts = Boolean.FALSE; // перенесено в ДВФУ

    private SppScheduleSessionPrintParams _printData;

    private boolean _isAddForm;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public SppScheduleSessionPrintParams getPrintData()
    {
        return _printData;
    }

    public void setPrintData(SppScheduleSessionPrintParams printData)
    {
        _printData = printData;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _printData)
        {
            _isAddForm = true;
            _printData = new SppScheduleSessionPrintParams();
            OrgUnit currOrgUnit = DataAccessServices.dao().<OrgUnit>get(_orgUnitId);
            _printData.setCurrentOrgUnit(currOrgUnit);
            _printData.setFormativeOrgUnit(currOrgUnit);
            _printData.setEducationYear(DataAccessServices.dao().get(EducationYear.class, EducationYear.current(), true));
        }
        else
            _isAddForm = false;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("scheduleData", _printData);
//        if (SppScheduleSessionPrintFormAdd.TERRITORIAL_OU_DS.equals(dataSource.getName()))
//        {
//            dataSource.put("column", GroupComboDSHandler.Columns.TERRITORIAL_ORG_UNIT);
//        } // перенесено в ДВФУ
        if (SppScheduleSessionPrintFormAdd.FORMATIVE_OU_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.FORMATIVE_ORG_UNIT);
        }
        if (SppScheduleSessionPrintFormAdd.EDU_LEVEL_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.EDUCATION_LEVEL);
        }
        if (SppScheduleSessionPrintFormAdd.DEVELOP_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.DEVELOP_FORM);
        }
        if (SppScheduleSessionPrintFormAdd.COURSE_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.COURSE);
        }
        if (SppScheduleSessionPrintFormAdd.SEASON_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", SppScheduleSessionComboDSHandler.Columns.SEASON);
        }
        if (SppScheduleSessionPrintFormAdd.BELLS_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", SppScheduleSessionComboDSHandler.Columns.BELLS);
        }
        if (SppScheduleSessionPrintFormAdd.SCHEDULES_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", SppScheduleSessionComboDSHandler.Columns.SCHEDULE);
        }
    }

    public void onChangeFormOU()
    {
        if (null != _printData.getFormativeOrgUnit())
            _printData.setChief((EmployeePost) _printData.getFormativeOrgUnit().getHead());
    }

    public void onClickApply()
    {
        SppScheduleSessionPrintForm printForm = SppScheduleSessionManager.instance().dao().savePrintForm(_printData);
        byte[] content = printForm.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("xls").fileName("schedule.xls").document(content), true);
        deactivate();
    }

    public boolean isAddForm()
    {
        return _isAddForm;
    }

    public void setAddForm(boolean addForm)
    {
        _isAddForm = addForm;
    }
}
