/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.GroupTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddEdit.SppScheduleAddEdit;
import ru.tandemservice.unispp.base.entity.SppSchedule;

/**
 * @author nvankov
 * @since 9/2/13
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "groupId", required = true)
})
public class SppScheduleGroupTabUI extends UIPresenter
{
    private Long _groupId;
    private Group _group;

    private BaseSearchListDataSource _scheduleDS;

    public BaseSearchListDataSource getScheduleDS()
    {
        if(null == _scheduleDS)
            _scheduleDS = _uiConfig.getDataSource(SppScheduleGroupTab.SCHEDULE_DS);
        return _scheduleDS;
    }

    public Long getGroupId()
    {
        return _groupId;
    }

    public void setGroupId(Long groupId)
    {
        _groupId = groupId;
    }

    public Boolean getAddScheduleVisible()
    {
        return !_group.isArchival();
    }

    public Boolean getEntityEditDisabled()
    {
        SppSchedule schedule = getScheduleDS().getCurrent();
        return schedule.isApproved() || schedule.isArchived();
    }

    public Boolean getEntityDeleteDisabled()
    {
        SppSchedule schedule = getScheduleDS().getCurrent();
        return schedule.isApproved();
    }

    @Override
    public void onComponentRefresh()
    {
        if(_group == null)
            _group = DataAccessServices.dao().get(_groupId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleGroupTab.SCHEDULE_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleDSHandler.PROP_GROUP_ID, _groupId);
            dataSource.put(SppScheduleDSHandler.PROP_TITLE, _uiSettings.get("title"));
        }
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(SppScheduleAddEdit.class).parameter("scheduleId", getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        SppScheduleManager.instance().dao().deleteSchedule(getListenerParameterAsLong());
    }

    public void onClickAddSchedule()
    {
        _uiActivation.asRegionDialog(SppScheduleAddEdit.class)
                .parameter("groupId", _groupId)
                .activate();
    }
}
