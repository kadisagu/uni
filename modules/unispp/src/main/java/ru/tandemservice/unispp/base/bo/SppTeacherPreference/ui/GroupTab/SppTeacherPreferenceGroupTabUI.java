/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.GroupTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.AddEdit.SppTeacherPreferenceAddEdit;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.DailyAddEdit.SppTeacherPreferenceDailyAddEdit;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.SessionAddEdit.SppTeacherPreferenceSessionAddEdit;
import ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList;

/**
 * @author vnekrasov
 * @since 6/23/14
 */
@State({
        @Bind(key = "employeePostId", binding = "employeePostId", required = true)
})
public class SppTeacherPreferenceGroupTabUI extends UIPresenter
{
    private BaseSearchListDataSource _teacherPreferenceDS;
    private Long _employeePostId;
    private boolean _himself;

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        _employeePostId = employeePostId;
    }

    public BaseSearchListDataSource getTeacherPreferenceDS()
    {
        if(null == _teacherPreferenceDS)
            _teacherPreferenceDS = _uiConfig.getDataSource(SppTeacherPreferenceGroupTab.TEACHER_PREFERENCE_DS);
        return _teacherPreferenceDS;
    }

    public Boolean getAddTeacherPreferenceVisible()
    {
        return true;
    }

    public Boolean getEntityEditDisabled()
    {
        return true;
    }

    public Boolean getEntityDeleteDisabled()
    {
        return true;
    }

    @Override
    public void onComponentRefresh()
    {
        EmployeePost employeePost = DataAccessServices.dao().getNotNull(EmployeePost.class, _employeePostId);

        IPrincipalContext principalContext = getConfig().getUserContext().getPrincipalContext();
        _himself = principalContext.getPrincipal().equals(employeePost.getPrincipal());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("teacherId",  _employeePostId);
//        if (SppTeacherPreferenceGroupTab.TEACHER_PREFERENCE_DS.equals(dataSource.getName()))
//        {
//            dataSource.put("title", _uiSettings.get("title"));
//        }
    }

    public void onEditEntityFromList()
    {
        Long id = getListenerParameterAsLong();
        IEntity e = DataAccessServices.dao().get(id);
        if (e instanceof SppTeacherPreferenceList)
            _uiActivation.asRegion(SppTeacherPreferenceAddEdit.class).parameter("teacherPreferenceId", id).activate();
        if (e instanceof SppTeacherSessionPreferenceList)
            _uiActivation.asRegion(SppTeacherPreferenceSessionAddEdit.class).parameter("teacherSessionPreferenceId", id).activate();
        if (e instanceof SppTeacherDailyPreferenceList)
            _uiActivation.asRegion(SppTeacherPreferenceDailyAddEdit.class).parameter("teacherDailyPreferenceId", id).activate();
    }

    public void onDeleteEntityFromList()
    {
        SppTeacherPreferenceManager.instance().dao().deleteTeacherPreferenceList(getListenerParameterAsLong());
    }

    public void onClickAddTeacherPreference()
    {
        _uiActivation.asRegion(SppTeacherPreferenceAddEdit.class).parameter("employeePostId", _employeePostId).activate();
    }

    public void onClickAddTeacherSessionPreference()
    {
        _uiActivation.asRegion(SppTeacherPreferenceSessionAddEdit.class).parameter("employeePostId", _employeePostId).activate();
    }

    public void onClickAddTeacherDailyPreference()
    {
        _uiActivation.asRegion(SppTeacherPreferenceDailyAddEdit.class).parameter("employeePostId", _employeePostId).activate();
    }

    public String getViewPermissionKey()
    {
        return SppTeacherPreferenceManager.getPermissionKey("eplIndividualPlanNonEduLoadTab_view", _himself);
    }

    public String getAddPermissionKey()
    {
        return SppTeacherPreferenceManager.getPermissionKey("eplIndividualPlanNonEduLoadTab_add", _himself);
    }

    public String getEditPermissionKey()
    {
        return SppTeacherPreferenceManager.getPermissionKey("eplIndividualPlanNonEduLoadTab_edit", _himself);
    }

    public String getDeletePermissionKey()
    {
        return SppTeacherPreferenceManager.getPermissionKey("eplIndividualPlanNonEduLoadTab_delete", _himself);
    }
}
