/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 6/23/14
 */
public class SppTeacherPreferenceDSHandler extends DefaultSearchDataSourceHandler
{
    public SppTeacherPreferenceDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String title = context.get("title");
        Long teacherId = context.get("teacherId"); // employee post id
//        List<Long> courses = context.get("courses");
//        List<Long> eduLevels = context.get("eduLevels");
//        List<Long> groups = context.get("groups");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppTeacherPreferenceList.class, "s");
        builder.column("s");
        builder.fetchPath(DQLJoinType.inner, SppTeacherPreferenceList.sppScheduleSeason().fromAlias("s"), "season");

        if (teacherId != null)
        {
            Long personId = UniDaoFacade.getCoreDao().getProperty(EmployeePost.class, EmployeePost.person().id().s(), EmployeePost.P_ID, teacherId);
            builder.where(eq(property("s", SppTeacherPreferenceList.teacher().id()), value(personId)));
        }

        if(!StringUtils.isEmpty(title))
            builder.where(like(DQLFunctions.upper(property("s", SppTeacherPreferenceList.title())), value(CoreStringUtils.escapeLike(title, true))));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().pageable(true).build();
    }
}
