/* $Id:$ */
package ru.tandemservice.unispp.component.registry.registryElementSppData.AddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.component.base.SimpleOrgUnitBasedModel;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.unispp.base.entity.SppRegElementExt;
import ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Victor Nekrasov
 * @since 20.05.2014
 */
@Input({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eppRegElementId", required = true)
})
public class Model<T extends EppRegistryElement> extends SimpleOrgUnitBasedModel
{
//    private StaticListDataSource<SppDisciplinePreferenceList> dataSource;
//
//    public StaticListDataSource<SppDisciplinePreferenceList> getDataSource()
//    {
//        return dataSource;
//    }
//
//    public void setDataSource(StaticListDataSource<SppDisciplinePreferenceList> dataSource)
//    {
//        this.dataSource = dataSource;
//    }

    private ISelectModel _classroomTypes;
    private Map<String, Boolean> defaultLoadCheckMap = Collections.emptyMap();

    public void setDefaultLoadCheckMap(final Map<String, Boolean> defaultLoadCheckMap)
    {
        this.defaultLoadCheckMap = defaultLoadCheckMap;
    }

    public Map<String, Boolean> getDefaultLoadCheckMap()
    {
        final Map<String, Boolean> map = this.defaultLoadCheckMap;
        return (null != map ? map : Collections.<String, Boolean>emptyMap());
    }

    private Map<String, UniplacesClassroomType> defaultLoadPlaceMap = Collections.emptyMap();

    public void setDefaultLoadPlaceMap(final Map<String, UniplacesClassroomType> defaultLoadPlaceCheckMap)
    {
        this.defaultLoadPlaceMap = defaultLoadPlaceCheckMap;
    }

    public Map<String, UniplacesClassroomType> getDefaultLoadPlaceMap()
    {
        final Map<String, UniplacesClassroomType> map = this.defaultLoadPlaceMap;
        return (null != map ? map : Collections.<String, UniplacesClassroomType>emptyMap());
    }

    private Map<String, EppLoadType> loadTypeMap;

    public Map<String, EppLoadType> getLoadTypeMap()
    {
        return this.loadTypeMap;
    }

    public void setLoadTypeMap(Map<String, EppLoadType> loadTypeMap)
    {
        this.loadTypeMap = loadTypeMap;
    }

    public Collection<EppALoadType> getEppALoadTypes()
    {
        return EppLoadTypeUtils.getALoadTypeList(getLoadTypeMap());
    }

    private Map<String, SppRegElementLoadCheck> loadMap;

    public Map<String, SppRegElementLoadCheck> getLoadMap()
    {
        return this.loadMap;
    }

    public void setLoadMap(final Map<String, SppRegElementLoadCheck> loadMap)
    {
        this.loadMap = loadMap;
    }

    @Override
    public OrgUnit getOrgUnit()
    {
        return (null == this.getElement() ? null : this.getElement().getOwner());
    }

    public ISelectModel getClassroomTypes()
    {
        return _classroomTypes;
    }

    public void setClassroomTypes(ISelectModel classroomTypes)
    {
        _classroomTypes = classroomTypes;
    }

    private EppLoadType currentLoadType;

    public EppLoadType getCurrentLoadType()
    {
        return this.currentLoadType;
    }

    public void setCurrentLoadType(final EppLoadType currentLoadType)
    {
        this.currentLoadType = currentLoadType;
    }

    public SppRegElementLoadCheck getCurrentDisciplineLoad()
    {
        return this.getDisciplineLoad(this.getCurrentLoadType());
    }

    protected SppRegElementLoadCheck getDisciplineLoad(final EppLoadType loadType)
    {
        return SafeMap.safeGet(this.getLoadMap(), loadType.getFullCode(), key -> {
            final SppRegElementLoadCheck load = new SppRegElementLoadCheck();
            load.setCheckValue(getDefaultLoadCheckMap().get(key) == null ? false : getDefaultLoadCheckMap().get(key));
            load.setUniplacesClassroomType(Model.this.getDefaultLoadPlaceMap().get(key));
            return load;
        });
    }

    private Long eppRegElementId;
    private EppRegistryElement element;

    public SppRegElementExt getSppRegElementExt()
    {
        return _sppRegElementExt;
    }

    public void setSppRegElementExt(SppRegElementExt sppRegElementExt)
    {
        _sppRegElementExt = sppRegElementExt;
    }

    private SppRegElementExt _sppRegElementExt;

    public EppRegistryElement getElement()
    {
        return this.element;
    }

    public void setElement(final EppRegistryElement element)
    {
        this.element = element;
    }

    public Long getEppRegElementId()
    {
        return this.eppRegElementId;
    }

    public void setEppRegElementId(final Long eppRegElementId)
    {
        this.eppRegElementId = eppRegElementId;
    }

    private ISelectModel ownerSelectModel;

    public ISelectModel getOwnerSelectModel()
    {
        return this.ownerSelectModel;
    }

    public void setOwnerSelectModel(final ISelectModel ownerSelectModel)
    {
        this.ownerSelectModel = ownerSelectModel;
    }

    private List<HSelectOption> parentList = Collections.emptyList();

    public List<HSelectOption> getParentList()
    {
        return this.parentList;
    }

    public void setParentList(final List<HSelectOption> parentList)
    {
        this.parentList = parentList;
    }
}
