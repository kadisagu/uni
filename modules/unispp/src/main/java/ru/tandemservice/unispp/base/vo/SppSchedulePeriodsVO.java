/* $Id$ */
package ru.tandemservice.unispp.base.vo;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author nvankov
 * @since 8/30/13
 */
public class SppSchedulePeriodsVO
{
    private List<SppSchedulePeriodVO> _periods = Lists.newArrayList();
    private SppScheduleWeekPeriodsRowVO _weekPeriodsRowVO;
    private SppSchedulePeriodVO _currentPeriod;
    private boolean _view = true;

    public List<SppSchedulePeriodVO> getPeriods()
    {
        return _periods;
    }

    public void setPeriods(List<SppSchedulePeriodVO> periods)
    {
        _periods = periods;
    }

    public SppScheduleWeekPeriodsRowVO getWeekPeriodsRowVO()
    {
        return _weekPeriodsRowVO;
    }

    public void setWeekPeriodsRowVO(SppScheduleWeekPeriodsRowVO weekPeriodsRowVO)
    {
        _weekPeriodsRowVO = weekPeriodsRowVO;
    }

    public SppSchedulePeriodVO getCurrentPeriod()
    {
        return _currentPeriod;
    }

    public void setCurrentPeriod(SppSchedulePeriodVO currentPeriod)
    {
        _currentPeriod = currentPeriod;
    }

    public void setView(boolean view)
    {
        _view = view;
    }

    public boolean isView()
    {
        return _view;
    }

    public boolean getGroupVisible()
    {
        return _periods.size() > 1;
    }

    public boolean getAddActive()
    {
        return true;
    }

    public boolean getEditActive()
    {
        return _view && getVisible();
    }

    public boolean getSaveActive()
    {
        return !_view;
    }

    public boolean getVisible()
    {
        return _periods.size() > 0;
    }
}
