/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 05.11.2016
 */
public class SppScheduleEduLevelComboDSHandler extends DefaultComboDataSourceHandler
{
    // properties
    public static String PROP_ORG_UNIT_ID = "orgUnitId";
    public static String PROP_ORG_UNITS = "orgUnitIds";
    public static String PROP_COURSES = "courses";

    public SppScheduleEduLevelComboDSHandler(String ownerId)
    {
        super(ownerId, EducationLevelsHighSchool.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        Long orgUnitId = ep.context.get(PROP_ORG_UNIT_ID);
        List<OrgUnit> orgUnits = orgUnitId == null ? ep.context.get(PROP_ORG_UNITS) :
                Collections.singletonList(DataAccessServices.dao().get(OrgUnit.class, orgUnitId));

        List<Course> courses = ep.context.get(PROP_COURSES);

        String filter = ep.input.getComboFilterByValue();

        DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                .fromEntity(Group.class, "g")
                .where(eq(property("e"), property("g", Group.educationOrgUnit().educationLevelHighSchool())));
        if (null != orgUnits && !orgUnits.isEmpty())
        {
            DQLSelectBuilder kindRelationsBuilder= new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                    .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                    .where(in(property("rel", OrgUnitToKindRelation.orgUnit()), orgUnits));
            List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

            Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                    .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

            IDQLExpression condition = nothing(); // false
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                condition = or(condition, in(property("g", Group.educationOrgUnit().educationLevelHighSchool().orgUnit()), orgUnits));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                condition = or(condition, in(property("g", Group.educationOrgUnit().formativeOrgUnit()), orgUnits));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                condition = or(condition, in(property("g", Group.educationOrgUnit().territorialOrgUnit()), orgUnits));

            subBuilder.where(condition);
        }

        if (null != courses && !courses.isEmpty())
            subBuilder.where(in(property("g", Group.course()), courses));

        if (!StringUtils.isEmpty(filter))
            subBuilder.where(like(DQLFunctions.upper(property("g", Group.educationOrgUnit().educationLevelHighSchool().title())), value(CoreStringUtils.escapeLike(filter, true))));

        ep.dqlBuilder.where(exists(subBuilder.buildQuery()));
    }
}
