/* $Id: */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.Copy;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleGroupComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.View.SppScheduleSessionView;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;

/**
 * @author vnekrasov
 * @since 7/7/14
 */
@Input({
        @Bind(key = "scheduleSessionId", binding = "scheduleSessionId"),
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class SppScheduleSessionCopyUI extends UIPresenter
{
    private Long _scheduleSessionId;
    private SppScheduleSession _scheduleSession;
    private SppScheduleSession _newScheduleSession;
    private Long _orgUnitId;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Long getScheduleSessionId()
    {
        return _scheduleSessionId;
    }

    public void setScheduleSessionId(Long scheduleSessionId)
    {
        _scheduleSessionId = scheduleSessionId;
    }

    public SppScheduleSession getScheduleSession()
    {
        return _scheduleSession;
    }

    public void setScheduleSession(SppScheduleSession scheduleSession)
    {
        _scheduleSession = scheduleSession;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _newScheduleSession)
        {
            if(null != _scheduleSessionId)
            {
                _scheduleSession = DataAccessServices.dao().get(_scheduleSessionId);
                _newScheduleSession = new SppScheduleSession();
                _orgUnitId = _orgUnitId != null ? _orgUnitId : _scheduleSession.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getId();
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleSessionCopy.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put("groupId", _scheduleSession.getGroup().getId());
            dataSource.put("orgUnitId", _orgUnitId);
            dataSource.put(SppScheduleGroupComboDSHandler.PROP_GROUPS_NOT_IN_ARCHIVE, Boolean.TRUE);
        }
    }

    public void onClickApply()
    {
        _newScheduleSession.setApproved(_scheduleSession.isApproved());
        _newScheduleSession.setArchived(_scheduleSession.isArchived());
        _newScheduleSession.setStatus(_scheduleSession.getStatus());
        _newScheduleSession.setSeason(_scheduleSession.getSeason());
        _newScheduleSession.setIcalData(null);
        SppScheduleSessionManager.instance().dao().copySchedule(_scheduleSessionId, _newScheduleSession);

        deactivate();
        _uiActivation.asDesktopRoot(SppScheduleSessionView.class).parameter(PUBLISHER_ID, _newScheduleSession.getId()).activate();

    }
}
