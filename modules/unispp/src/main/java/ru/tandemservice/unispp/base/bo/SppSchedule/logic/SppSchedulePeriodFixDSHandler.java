/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix;

/**
 * @author nvankov
 * @since 9/3/13
 */
public class SppSchedulePeriodFixDSHandler extends DefaultSearchDataSourceHandler
{
    public SppSchedulePeriodFixDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long scheduleId = context.get("scheduleId");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppSchedulePeriodFix.class, "f");

        if(null != scheduleId)
            builder.where(DQLExpressions.eq(DQLExpressions.property("f", SppSchedulePeriodFix.sppSchedule().id()), DQLExpressions.value(scheduleId)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order().build();
    }
}
