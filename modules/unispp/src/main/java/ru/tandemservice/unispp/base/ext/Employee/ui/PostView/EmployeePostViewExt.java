/* $Id:$ */
package ru.tandemservice.unispp.base.ext.Employee.ui.PostView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.GroupTab.SppTeacherPreferenceGroupTab;

/**
 * @author Victor Nekrasov
 * @since 29.05.2014
 */
@Configuration
public class EmployeePostViewExt extends BusinessComponentExtensionManager
{

    public static final String ADDON_NAME = "unispp" + EmployeePostViewExtUI.class.getSimpleName();

    @Autowired
    private EmployeePostView _employeePostView;

    public static final String SPP_TEACHER_PREFERENCES = "employeePostSppTeacherPreferences";

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_employeePostView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmployeePostViewExtUI.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {

        return tabPanelExtensionBuilder(_employeePostView.employeePostPubTabPanelExtPoint())
                .addTab(componentTab(SPP_TEACHER_PREFERENCES, SppTeacherPreferenceGroupTab.class)
                        .permissionKey("viewTabEmployeePost_SppPreferences")
                        .parameters("addon:" + ADDON_NAME + ".parameters")
                        .visible("addon:" + ADDON_NAME + ".visible")
                )
                .create();
    }
}
