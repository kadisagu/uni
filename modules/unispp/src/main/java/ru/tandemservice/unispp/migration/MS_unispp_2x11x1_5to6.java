package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unispp_2x11x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleDailyPrintForm

		// создано свойство bells
		{
			// создать колонку
			tool.createColumn("sppscheduledailyprintform_t", new DBColumn("bells_id", DBType.LONG));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppSchedulePrintForm

		// свойство groupOrgUnit переименовано в formativeOrgUnit
		{
			// переименовать колонку
			tool.renameColumn("sppscheduleprintform_t", "grouporgunit_id", "formativeorgunit_id");
		}

		// создано свойство eduLevels
		{
			// создать колонку
			tool.createColumn("sppscheduleprintform_t", new DBColumn("edulevels_p", DBType.createVarchar(255)));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleSessionPrintForm

		// создано свойство bells
		{
			// создать колонку
			tool.createColumn("sppschedulesessionprintform_t", new DBColumn("bells_id", DBType.LONG));
		}
    }
}
