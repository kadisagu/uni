/* $Id: */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.*;
import ru.tandemservice.unispp.util.SppScheduleUtil;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 7/11/14
 */
public class SppScheduleLectureRoomComboDSHandler extends SimpleTitledComboDataSourceHandler
{
    public static final String VIEW_PROP_TITLE = "placeTitle";
    public static final String VIEW_PROP_LOCATION = "placeLocation";
    public static final String VIEW_PROP_CAPACITY = "capacity";

    // properties
    public static final String PROP_STUDENT_GRP_CNT = "studentGrpCnt";
    public static final String PROP_CURRENT_PERIOD = "currentPeriod";
    public static final String PROP_LECTURE_ROOM_TYPE = "lectureRoomType";
    public static final String PROP_CHECK_FREE_LECTURE_ROOMS = "checkFreeLectureRooms";
    public static final String PROP_TEACHER_ID = "teacherId";
    public static final String PROP_EVENT_DAY_OF_WEEK = "eventDayOfWeek";
    public static final String PROP_EVENT_BELL_ENTRY_ID = "eventBellEntryId";
    public static final String PROP_SCHEDULE_IDS = "scheduleIds";
    public static final String PROP_SEASON_ID = "seasonId";
    public static final String PROP_DISCIPLINE_ID = "disciplineId";
    public static final String PROP_CHECK_TEACHER_PREFERENCES = "checkTeacherPreferences";
    public static final String PROP_IS_EVEN = "isEven";
    public static final String PROP_START_DATE = "startDate";
    public static final String PROP_END_DATE = "endDate";

    public SppScheduleLectureRoomComboDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String filter = input.getComboFilterByValue();
        int studentGrpCnt = context.get(PROP_STUDENT_GRP_CNT) == null ? 0 : (int) context.get(PROP_STUDENT_GRP_CNT);
        SppSchedulePeriod currentPeriod = context.get(PROP_CURRENT_PERIOD);
        UniplacesClassroomType lectureRoomType = context.get(PROP_LECTURE_ROOM_TYPE);
        boolean checkFreeLectureRooms = context.getBoolean(PROP_CHECK_FREE_LECTURE_ROOMS, Boolean.FALSE);
        Long teacherId = context.get(PROP_TEACHER_ID);
        int eventDayOfWeek = context.get(PROP_EVENT_DAY_OF_WEEK) == null ? 0 : (int) context.get(PROP_EVENT_DAY_OF_WEEK);
        Long bellEntryId = context.get(PROP_EVENT_BELL_ENTRY_ID);
        List<Long> sppScheduleIds = context.get(PROP_SCHEDULE_IDS);
        Long seasonId = context.get(PROP_SEASON_ID);
        Long disciplineId = context.get(PROP_DISCIPLINE_ID);
        boolean checkTeacherPreferences = context.getBoolean(PROP_CHECK_TEACHER_PREFERENCES, Boolean.FALSE);
        boolean isEven = context.getBoolean(PROP_IS_EVEN, Boolean.FALSE);
        Date startDate = context.get(PROP_START_DATE);
        Date endDate = context.get(PROP_END_DATE);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "s").column("s");

        if (!StringUtils.isEmpty(filter))
            dql.where(like(DQLFunctions.upper(property("s", UniplacesPlace.title())), value(CoreStringUtils.escapeLike(filter))));

        //List<Object[]> placesList1 =   dql.createStatement(getSession()).list();

        dql.where(eq(property("s", UniplacesPlace.condition().code()), value("1")));
        if (studentGrpCnt > 0)
            dql.where(or(ge(property("s", UniplacesPlace.capacity()), value(studentGrpCnt)),
                    isNull(property("s", UniplacesPlace.capacity()))));

        if (lectureRoomType != null)
            dql.where(eq(property("s", UniplacesPlace.classroomType().id()), value(lectureRoomType.getId())));

        if (checkFreeLectureRooms && null != bellEntryId && null != seasonId && eventDayOfWeek != 0)
        {
            SppScheduleSeason season = DataAccessServices.dao().getNotNull(seasonId);
            ScheduleBellEntry bellEntry = DataAccessServices.dao().getNotNull(bellEntryId);
            Date periodStartDate = startDate != null ? startDate : season.getStartDate();
            Date periodEndDate = endDate != null ? endDate : season.getEndDate();

            DQLSelectBuilder builder1 = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e")
                    .column(property("e", SppSchedulePeriod.lectureRoomVal().id()))
                    .where(currentPeriod == null ? null : ne(property("e", SppSchedulePeriod.id()), value(currentPeriod.getId())))
                    .where(eq(property("e", SppSchedulePeriod.dayOfWeek()), value(eventDayOfWeek)))
                    // даты пересеклись
                    .where(or(
                            and(
                                    isNotNull(property("e", SppSchedulePeriod.endDate())),
                                    ge(property("e", SppSchedulePeriod.endDate()), valueDate(periodStartDate))),
                            and(
                                    isNull(property("e", SppSchedulePeriod.endDate())),
                                    ge(property("e", SppSchedulePeriod.weekRow().schedule().season().endDate()), valueDate(periodStartDate)))))
                    .where(or(
                            and(
                                    isNotNull(property("e", SppSchedulePeriod.startDate())),
                                    le(property("e", SppSchedulePeriod.startDate()), valueDate(periodEndDate))),
                            and(
                                    isNull(property("e", SppSchedulePeriod.startDate())),
                                    le(property("e", SppSchedulePeriod.weekRow().schedule().season().startDate()), valueDate(periodEndDate)))))
                    // звонковая пара пересеклась
                    .where(gt(property("e", SppSchedulePeriod.weekRow().bell().endTime()), value(bellEntry.getStartTime())))
                    .where(lt(property("e", SppSchedulePeriod.weekRow().bell().startTime()), value(bellEntry.getEndTime())))
                    .where(or(
                            eq(property("e", SppSchedulePeriod.weekRow().schedule().diffEven()), value(Boolean.FALSE)),
                            eq(property("e", SppSchedulePeriod.weekRow().even()), value(isEven)),
                            // либо неявно скопируется пара, которую мы устанавливаем (хотя бы одна)
                            notExists(new DQLSelectBuilder()
                                    .fromEntity(SppSchedulePeriod.class, "sp1")
                                    .where(in(property("sp1", SppSchedulePeriod.weekRow().schedule().id()), sppScheduleIds))
                                    .where(eq(property("sp1", SppSchedulePeriod.weekRow().schedule().diffEven()), value(Boolean.TRUE)))
                                    .where(eq(property("sp1", SppSchedulePeriod.dayOfWeek()), value(eventDayOfWeek)))
                                    .where(eq(property("sp1", SppSchedulePeriod.weekRow().bell().id()), value(bellEntryId)))
                                    .where(eq(property("sp1", SppSchedulePeriod.weekRow().even()), value(!isEven)))
                                    .buildQuery()),
                            // либо среди пар, которые пересекаются с нашей есть те, которые неявно скопируются
                            notExists(new DQLSelectBuilder()
                                    .fromEntity(SppSchedulePeriod.class, "sp2")
                                    .where(eq(property("sp2", SppSchedulePeriod.weekRow().schedule().id()), property("e", SppSchedulePeriod.weekRow().schedule().id())))
                                    .where(eq(property("sp2", SppSchedulePeriod.dayOfWeek()), property("e", SppSchedulePeriod.dayOfWeek())))
                                    .where(eq(property("sp2", SppSchedulePeriod.weekRow().bell().id()), property("e", SppSchedulePeriod.weekRow().bell().id())))
                                    .where(ne(property("sp2", SppSchedulePeriod.weekRow().even()), property("e", SppSchedulePeriod.weekRow().even())))
                                    .buildQuery())))
                    .where(isNotNull(property("e", SppSchedulePeriod.lectureRoomVal())));

            DQLSelectBuilder builder2 = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "e").column("e")
//                    .column(property("e", SppScheduleDailyEvent.lectureRoomVal().id()))
                    // todo добавить сравнение по дню недели (в dql вытаскивание dow не реализовано)
//                    .where(eq(property("e", SppScheduleDailyEvent.date()), value(eventDayOfWeek)))
                    // даты пересеклись
                    .where(ge(property("e", SppScheduleDailyEvent.date()), valueDate(periodStartDate)))
                    .where(le(property("e", SppScheduleDailyEvent.date()), valueDate(periodEndDate)))
                    // звонковая пара пересеклась
                    .where(gt(property("e", SppScheduleDailyEvent.bell().endTime()), value(bellEntry.getStartTime())))
                    .where(lt(property("e", SppScheduleDailyEvent.bell().startTime()), value(bellEntry.getEndTime())))
                    // todo возможно учесть неявное копирование, но тогда нужно вычислять в чётной неделе или в нечётной находится событие
                    .where(isNotNull(property("e", SppScheduleDailyEvent.lectureRoomVal())));
            // костыль (пока нет dow в dql)
            List<Long> list2 = builder2.createStatement(getSession()).list().stream()
                    .map(o -> (SppScheduleDailyEvent)o)
                    .filter(o -> o != null && SppScheduleUtil.getDayOfWeek(o.getDate()) == eventDayOfWeek)
                    .map(EntityBase::getId).collect(Collectors.toList());

            DQLSelectBuilder builder3 = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "e").column("e")
//                    .column(property("e", SppScheduleSessionEvent.lectureRoomVal().id()))
                    // todo добавить сравнение по дню недели (в dql вытаскивание dow не реализовано)
//                    .where(eq(property("e", SppScheduleDailyEvent.date()), value(eventDayOfWeek)))
                    // даты пересеклись
                    .where(ge(property("e", SppScheduleSessionEvent.date()), valueDate(periodStartDate)))
                    .where(le(property("e", SppScheduleSessionEvent.date()), valueDate(periodEndDate)))
                    // звонковая пара пересеклась
                    .where(gt(property("e", SppScheduleSessionEvent.bell().endTime()), value(bellEntry.getStartTime())))
                    .where(lt(property("e", SppScheduleSessionEvent.bell().startTime()), value(bellEntry.getEndTime())))
                    // todo возможно учесть неявное копирование, но тогда нужно вычислять в чётной неделе или в нечётной находится событие
                    .where(isNotNull(property("e", SppScheduleSessionEvent.lectureRoomVal())));
            // костыль (пока нет dow в dql)
            List<Long> list3 = builder3.createStatement(getSession()).list().stream()
                    .map(o -> (SppScheduleSessionEvent)o)
                    .filter(o -> o != null && SppScheduleUtil.getDayOfWeek(o.getDate()) == eventDayOfWeek)
                    .map(EntityBase::getId).collect(Collectors.toList());

            dql.where(notIn(property("s", UniplacesPlace.id()), builder1.buildQuery()));
            if (list2 != null && !list2.isEmpty()) dql.where(notIn(property("s", UniplacesPlace.id()), list2));
            if (list3 != null && !list3.isEmpty()) dql.where(notIn(property("s", UniplacesPlace.id()), list3));
        }

        // фильтрация по предпочтениям места и времени, установленным преподавателем
        if (checkTeacherPreferences && teacherId != null && bellEntryId != null && eventDayOfWeek != 0 && seasonId != null && sppScheduleIds != null)
        {
            Long personId = UniDaoFacade.getCoreDao().getProperty(PpsEntry.class, PpsEntry.person().id().s(), PpsEntry.P_ID, teacherId);

            DQLSelectBuilder subQuery1Builder = new DQLSelectBuilder();
            subQuery1Builder.fromEntity(UniplacesPlace.class, "plc");
            // флаг нежелательности и аудитория (берём либо конкретную либо все из указанного здания)
            subQuery1Builder.column(property("tpe", SppTeacherPreferenceElement.unwantedTime()), "uTime");
            subQuery1Builder.column(caseExpr(
                    isNotNull(property("tpe", SppTeacherPreferenceElement.lectureRoom().id())),
                    property("tpe", SppTeacherPreferenceElement.lectureRoom().id()),
                    property("plc", UniplacesPlace.id())), "lectureRoomId");
            // прицепим всё что нужно
            subQuery1Builder.joinPath(DQLJoinType.inner, UniplacesPlace.floor().unit().fromAlias("plc"), "unt");
            subQuery1Builder.joinEntity("unt", DQLJoinType.right, SppTeacherPreferenceElement.class, "tpe", eq(
                    property("tpe", SppTeacherPreferenceElement.building().id()),
                    property("unt", UniplacesUnit.building().id())));
            // фильтры
            subQuery1Builder.where(eq(property("tpe", SppTeacherPreferenceElement.weekRow().teacherPreference().sppScheduleSeason().id()), value(seasonId)));
            subQuery1Builder.where(eq(property("tpe", SppTeacherPreferenceElement.dayOfWeek()), value(eventDayOfWeek)));
            subQuery1Builder.where(eq(property("tpe", SppTeacherPreferenceElement.weekRow().bell().id()), value(bellEntryId)));
            subQuery1Builder.where(eq(property("tpe", SppTeacherPreferenceElement.weekRow().teacherPreference().teacher().id()), value(personId)));
            // учёт неявного копирования:
            subQuery1Builder.where(or(
                    // либо нет различия по чётности в предпочтении, либо чётность совпала, либо в соседней неделе пусто
                    // и после неявного копирования занятие попадёт в предпочтение
                    eq(property("tpe", SppTeacherPreferenceElement.weekRow().teacherPreference().diffEven()), value(Boolean.FALSE)),
                    eq(property("tpe", SppTeacherPreferenceElement.weekRow().even()), value(isEven)),
                    notExists(new DQLSelectBuilder()
                            .fromEntity(SppSchedulePeriod.class, "sp")
                            .where(in(property("sp", SppSchedulePeriod.weekRow().schedule().id()), sppScheduleIds))
                            .where(eq(property("sp", SppSchedulePeriod.weekRow().schedule().diffEven()), value(Boolean.TRUE)))
                            .where(eq(property("sp", SppSchedulePeriod.dayOfWeek()), value(eventDayOfWeek)))
                            .where(eq(property("sp", SppSchedulePeriod.weekRow().bell()), value(bellEntryId)))
                            .where(eq(property("sp", SppSchedulePeriod.weekRow().even()), value(!isEven)))
                            .buildQuery())));
            subQuery1Builder.distinct();

            // отсортируем (предпочтительные вперёд, без места вперёд)
            DQLSelectBuilder subQuery2Builder = new DQLSelectBuilder();
            subQuery2Builder.fromDataSource(subQuery1Builder.buildQuery(), "tmp1");
            subQuery2Builder.column("tmp1.uTime").column("tmp1.lectureRoomId");
            subQuery2Builder.order(property("tmp1.uTime")).order(property("tmp1.lectureRoomId"), OrderDirection.desc);

            // false - предпочтительные, true - нежелательные
            Map<Boolean, List<Long>> preferenceLectureRoomMap = subQuery2Builder.createStatement(getSession()).list().stream().map(o -> (Object[])o)
                    .collect(Collectors.groupingBy(o -> (Boolean)o[0], Collectors.mapping(o -> (Long)o[1], Collectors.toList())));

            IDQLExpression expression = null;
            List<Long> includeLectureRoomIds = preferenceLectureRoomMap.get(Boolean.FALSE);
            List<Long> excludeLectureRoomIds = preferenceLectureRoomMap.get(Boolean.TRUE);
            // если правило распространяется на все аудитории, то оно будет в самом начале списка (lectureRoomId = null)
            if (includeLectureRoomIds != null && !includeLectureRoomIds.isEmpty())
            {
                if (includeLectureRoomIds.get(0) != null)
                    expression = in(property("s", UniplacesPlace.id()), includeLectureRoomIds);
            }
            else if (excludeLectureRoomIds != null && !excludeLectureRoomIds.isEmpty())
            {
                if (excludeLectureRoomIds.get(0) == null)
                    expression = nothing();
                else
                    expression = notIn(property("s", UniplacesPlace.id()), excludeLectureRoomIds);
            }

            if (expression != null) dql.where(expression);
        }

        DQLSelectBuilder builder2 = new DQLSelectBuilder().fromEntity(SppDisciplinePreferenceList.class, "e");
        builder2.column("e");
        builder2.where(eq(property("e", SppDisciplinePreferenceList.discipline().id()), value(disciplineId)));

        List<SppDisciplinePreferenceList> elementList = createStatement(builder2).list();

        List<UniplacesBuilding> buildingList = new ArrayList<>();
        List<UniplacesPlace> lectureRoomList = new ArrayList<>();

        for (SppDisciplinePreferenceList element : elementList)
        {
            if (element.getBuilding() != null)
                buildingList.add(element.getBuilding());
            if (element.getLectureRoom() != null)
                lectureRoomList.add(element.getLectureRoom());
        }

        DQLSelectBuilder dql1 = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "p");
        dql1.where(in(property("p", UniplacesPlace.floor().unit().building()), buildingList));
        List<UniplacesPlace> addLectureRoomList = createStatement(dql1).list();
        lectureRoomList.addAll(addLectureRoomList);

        if (!lectureRoomList.isEmpty())
            dql.where(in(property("s", UniplacesPlace.id()), CommonDAO.ids(lectureRoomList)));

        dql.order(property("s", UniplacesPlace.title()));


        List<UniplacesPlace> placesList = dql.createStatement(context.getSession()).list();

        List<DataWrapper> wrappers = new ArrayList<>();
        for (UniplacesPlace lectureRoom : placesList)
        {
            DataWrapper wrapper = prepareWrapper(lectureRoom);
            wrappers.add(wrapper);
        }

        setFilterByProperty(VIEW_PROP_TITLE);

        context.put(UIDefines.COMBO_OBJECT_LIST, wrappers);
        return super.execute(input, context);
    }

    public static DataWrapper prepareWrapper(@NotNull UniplacesPlace lectureRoom)
    {
        DataWrapper wrapper = new DataWrapper();
        wrapper.setId(lectureRoom.getId());
        wrapper.setTitle(lectureRoom.getTitle() +
                (lectureRoom.getCapacity() != null ? ", вмст " + lectureRoom.getCapacity().toString() : "") +
                ", " + lectureRoom.getFullLocationInfo());
        wrapper.setProperty(VIEW_PROP_TITLE, lectureRoom.getTitle());
        wrapper.setProperty(VIEW_PROP_LOCATION, lectureRoom.getFullLocationInfo());
        wrapper.setProperty(VIEW_PROP_CAPACITY, lectureRoom.getCapacity() != null ? ", вмст " + lectureRoom.getCapacity().toString() : "");
        return wrapper;
    }
}
