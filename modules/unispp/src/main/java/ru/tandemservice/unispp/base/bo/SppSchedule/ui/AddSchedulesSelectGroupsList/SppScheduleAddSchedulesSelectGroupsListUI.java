/* $Id: $ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddSchedulesSelectGroupsList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.UIDefines;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleEduLevelComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleGroupSearchDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddSchedulesExercise.SppScheduleAddSchedulesExercise;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddSchedulesExercise.SppScheduleAddSchedulesExerciseUI;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Igor Belanov
 * @since 25.10.2016
 */
@Input({
        @Bind(key = SppScheduleAddSchedulesSelectGroupsListUI.BIND_ORG_UNIT_ID, binding = "orgUnitId")
})
public class SppScheduleAddSchedulesSelectGroupsListUI extends UIPresenter
{
    public static final String BIND_ORG_UNIT_ID = "orgUnitId";

    private Long _orgUnitId;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("orgUnitId", getOrgUnitId());
        if (SppScheduleAddSchedulesSelectGroupsList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap("title", "courses", "eduLevels", "developForms", "developConditions", "developTechs", "developPeriods"));
            dataSource.put(SppScheduleGroupSearchDSHandler.PROP_GROUPS_NOT_IN_ARCHIVE, Boolean.TRUE);
        }
        if (SppScheduleAddSchedulesSelectGroupsList.EDU_LEVEL_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleEduLevelComboDSHandler.PROP_COURSES, _uiSettings.get("courses"));
        }
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((BaseSearchListDataSource) this.getConfig().getDataSource(SppScheduleAddSchedulesSelectGroupsList.GROUP_DS)).getOptionColumnSelectedObjects("check");
            selected.clear();
        }
    }

    public void onClickAddSchedules()
    {
        List<Group> groups = getGroupList();

        if (groups.isEmpty()) throw new ApplicationException("Не выбраны группы");

        _uiActivation.asRegionDialog(SppScheduleAddSchedulesExercise.class)
                .parameter(SppScheduleAddSchedulesExerciseUI.BIND_GROUP_IDS_LIST, groups.stream().map(Group::getId).collect(Collectors.toList()))
                .activate();
    }

    private List<Group> getGroupList()
    {
        Collection<IEntity> checked = ((BaseSearchListDataSource) this.getConfig().getDataSource(SppScheduleAddSchedulesSelectGroupsList.GROUP_DS)).getOptionColumnSelectedObjects("check");
        return checked.stream().map(g -> (Group) g).collect(Collectors.toList());
    }

    // getters & setters
    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }
}
