/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.OrgUnitTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;

/**
 * @author nvankov
 * @since 1/14/14
 */
@State({
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public class SppScheduleOrgUnitTabUI extends OrgUnitUIPresenter
{
    private String _selectedTab;

    // Getters & Setters

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public CommonPostfixPermissionModelBase getSecModel()
    {
        return this.getSec();
    }
}
