/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.SeasonList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.SeasonAddEdit.SppScheduleSessionSeasonAddEdit;

/**
 * @author vnekrasov
 * @since 12/26/13
 */
public class SppScheduleSessionSeasonListUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleSessionSeasonList.SCHEDULE_SESSION_SEASON_DS.equals(dataSource.getName()))
        {
            dataSource.put("title", _uiSettings.get("title"));
        }
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(SppScheduleSessionSeasonAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickAdd()
    {
        _uiActivation.asRegionDialog(SppScheduleSessionSeasonAddEdit.class).activate();
    }
}
