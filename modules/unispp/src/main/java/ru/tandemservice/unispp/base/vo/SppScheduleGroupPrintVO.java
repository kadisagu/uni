/* $Id$ */
package ru.tandemservice.unispp.base.vo;

import com.google.common.collect.Maps;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;

import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 9/25/13
 */
public class SppScheduleGroupPrintVO
{
    public static final int MONDAY = 1;
    public static final int TUESDAY = 2;
    public static final int WEDNESDAY = 3;
    public static final int THURSDAY = 4;
    public static final int FRIDAY = 5;
    public static final int SATURDAY = 6;
    public static final int SUNDAY = 7;

    private SppSchedule _schedule;
    private int _columnNum;
    private int _column;
    private int _totalColumnSize;
    private boolean _hasSunday = false;
    private int _students;

    private Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> _monday = Maps.newHashMap();
    private Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> _tuesday = Maps.newHashMap();
    private Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> _wednesday = Maps.newHashMap();
    private Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> _thursday = Maps.newHashMap();
    private Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> _friday = Maps.newHashMap();
    private Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> _saturday = Maps.newHashMap();
    private Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> _sunday = Maps.newHashMap();

    public SppScheduleGroupPrintVO(SppSchedule schedule)
    {
        _schedule = schedule;
    }

    public SppSchedule getSchedule()
    {
        return _schedule;
    }

    public void setSchedule(SppSchedule schedule)
    {
        _schedule = schedule;
    }

    public int getColumnNum()
    {
        return _columnNum;
    }

    public void setColumnNum(int columnNum)
    {
        _columnNum = columnNum;
    }

    public int getColumn()
    {
        return _column;
    }

    public void setColumn(int column)
    {
        _column = column;
    }

    public int getTotalColumnSize()
    {
        return _totalColumnSize;
    }

    public void setTotalColumnSize(int totalColumnSize)
    {
        _totalColumnSize = totalColumnSize;
    }

    public boolean isHasSunday()
    {
        return _hasSunday;
    }

    public void setHasSunday(boolean hasSunday)
    {
        _hasSunday = hasSunday;
    }

    public int getStudents()
    {
        return _students;
    }

    public void setStudents(int students)
    {
        _students = students;
    }

    public Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> getMonday()
    {
        return _monday;
    }

    public void setMonday(Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> monday)
    {
        _monday = monday;
    }

    public Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> getTuesday()
    {
        return _tuesday;
    }

    public void setTuesday(Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> tuesday)
    {
        _tuesday = tuesday;
    }

    public Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> getWednesday()
    {
        return _wednesday;
    }

    public void setWednesday(Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> wednesday)
    {
        _wednesday = wednesday;
    }

    public Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> getThursday()
    {
        return _thursday;
    }

    public void setThursday(Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> thursday)
    {
        _thursday = thursday;
    }

    public Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> getFriday()
    {
        return _friday;
    }

    public void setFriday(Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> friday)
    {
        _friday = friday;
    }

    public Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> getSaturday()
    {
        return _saturday;
    }

    public void setSaturday(Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> saturday)
    {
        _saturday = saturday;
    }

    public Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> getSunday()
    {
        return _sunday;
    }

    public void setSunday(Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> sunday)
    {
        _sunday = sunday;
    }
}
