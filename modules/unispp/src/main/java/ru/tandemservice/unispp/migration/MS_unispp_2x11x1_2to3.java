package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unispp_2x11x1_2to3 extends IndependentMigrationScript
{
	@Override
	public ScriptDependency[] getBoundaryDependencies()
	{
		return new ScriptDependency[]
				{
						new ScriptDependency("org.tandemframework", "1.6.18"),
						new ScriptDependency("org.tandemframework.shared", "1.11.1")
				};
	}

	@Override
	public void run(DBTool tool) throws Exception
	{
		ISQLTranslator translator = tool.getDialect().getSQLTranslator();
		tool.info("migration of teacher fields in unispp entities (from employeePost to ppsEntry)");

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppDisciplinePreferenceList

		// раньше свойство teacher ссылалось на сущность employeePost
		// теперь оно ссылается на сущность ppsEntry
		{
			// сначала нужно удалить те строки для которых мы не сможем проставить ППС
			SQLDeleteQuery deleteQuery = new SQLDeleteQuery("sppdisciplinepreferencelist_t", "dp");
			deleteQuery.getDeletedTableFrom()
					.leftJoin(SQLFrom.table("employee_post4pps_entry_t", "ep4pps"), "ep4pps.employeepost_id=dp.teacher_id");
			deleteQuery.where("ep4pps.ppsentry_id is null");
			int deletedRows = tool.executeUpdate(translator.toSql(deleteQuery));
			tool.info("sppdisciplinepreferencelist_t: " + deletedRows + " rows deleted");

			// 1. переименуем старое свойство
			tool.renameColumn("sppdisciplinepreferencelist_t", "teacher_id", "teacher1_id");
			// 2. создадим новое
			tool.createColumn("sppdisciplinepreferencelist_t", new DBColumn("teacher_id", DBType.LONG).setNullable(true));
			// 3. заполним новое
			SQLUpdateQuery updateQuery = new SQLUpdateQuery("sppdisciplinepreferencelist_t", "dp");
			updateQuery.set("teacher_id", "ep4pps.ppsentry_id");
			updateQuery.getUpdatedTableFrom()
					.leftJoin(SQLFrom.table("employee_post4pps_entry_t", "ep4pps"), "ep4pps.employeepost_id=dp.teacher1_id");
			int affectedRows = tool.executeUpdate(translator.toSql(updateQuery));
			tool.info("sppdisciplinepreferencelist_t: " + affectedRows + " rows updated");
			tool.setColumnNullable("sppdisciplinepreferencelist_t", "teacher_id", false);
			// 4. удалим старое
			tool.dropColumn("sppdisciplinepreferencelist_t", "teacher1_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleDailyEvent

		// раньше свойство teacherVal ссылалось на сущность employeePost
		// теперь оно ссылается на сущность ppsEntry
		{
			// 1. переименуем старое свойство
			tool.renameColumn("sppscheduledailyevent_t", "teacherval_id", "teacherval1_id");
			// 2. создадим новое
			tool.createColumn("sppscheduledailyevent_t", new DBColumn("teacherval_id", DBType.LONG).setNullable(true));
			// 3. заполним новое
			SQLUpdateQuery updateQuery = new SQLUpdateQuery("sppscheduledailyevent_t", "sde");
			updateQuery.set("teacherval_id", "ep4pps.ppsentry_id");
			updateQuery.where("sde.teacherval1_id is not null");
			updateQuery.getUpdatedTableFrom()
					.leftJoin(SQLFrom.table("employee_post4pps_entry_t", "ep4pps"), "ep4pps.employeepost_id=sde.teacherval1_id");
			int affectedRows = tool.executeUpdate(translator.toSql(updateQuery));
			tool.info("sppscheduledailyevent_t: " + affectedRows + " rows updated");
			// 4. удалим старое
			tool.dropColumn("sppscheduledailyevent_t", "teacherval1_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppSchedulePeriod

		// раньше свойство teacherVal ссылалось на сущность employeePost
		// теперь оно ссылается на сущность ppsEntry
		{
			// 1. переименуем старое свойство
			tool.renameColumn("sppscheduleperiod_t", "teacherval_id", "teacherval1_id");
			// 2. создадим новое
			tool.createColumn("sppscheduleperiod_t", new DBColumn("teacherval_id", DBType.LONG).setNullable(true));
			// 3. заполним новое
			SQLUpdateQuery updateQuery = new SQLUpdateQuery("sppscheduleperiod_t", "sp");
			updateQuery.set("teacherval_id", "ep4pps.ppsentry_id");
			updateQuery.where("sp.teacherval1_id is not null");
			updateQuery.getUpdatedTableFrom()
					.leftJoin(SQLFrom.table("employee_post4pps_entry_t", "ep4pps"), "ep4pps.employeepost_id=sp.teacherval1_id");
			int affectedRows = tool.executeUpdate(translator.toSql(updateQuery));
			tool.info("sppscheduleperiod_t: " + affectedRows + " rows updated");
			// 4. удалим старое
			tool.dropColumn("sppscheduleperiod_t", "teacherval1_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppSchedulePeriodFix

		// раньше свойство teacherVal ссылалось на сущность employeePost
		// теперь оно ссылается на сущность ppsEntry
		{
			// 1. переименуем старое свойство
			tool.renameColumn("sppscheduleperiodfix_t", "teacherval_id", "teacherval1_id");
			// 2. создадим новое
			tool.createColumn("sppscheduleperiodfix_t", new DBColumn("teacherval_id", DBType.LONG).setNullable(true));
			// 3. заполним новое
			SQLUpdateQuery updateQuery = new SQLUpdateQuery("sppscheduleperiodfix_t", "spf");
			updateQuery.set("teacherval_id", "ep4pps.ppsentry_id");
			updateQuery.where("spf.teacherval1_id is not null");
			updateQuery.getUpdatedTableFrom()
					.leftJoin(SQLFrom.table("employee_post4pps_entry_t", "ep4pps"), "ep4pps.employeepost_id=spf.teacherval1_id");
			int affectedRows = tool.executeUpdate(translator.toSql(updateQuery));
			tool.info("sppscheduleperiodfix_t: " + affectedRows + " rows updated");
			// 4. удалим старое
			tool.dropColumn("sppscheduleperiodfix_t", "teacherval1_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleSessionEvent

		// раньше свойство teacherVal ссылалось на сущность employeePost
		// теперь оно ссылается на сущность ppsEntry
		{
			// 1. переименуем старое свойство
			tool.renameColumn("sppschedulesessionevent_t", "teacherval_id", "teacherval1_id");
			// 2. создадим новое
			tool.createColumn("sppschedulesessionevent_t", new DBColumn("teacherval_id", DBType.LONG).setNullable(true));
			// 3. заполним новое
			SQLUpdateQuery updateQuery = new SQLUpdateQuery("sppschedulesessionevent_t", "sse");
			updateQuery.set("teacherval_id", "ep4pps.ppsentry_id");
			updateQuery.where("sse.teacherval1_id is not null");
			updateQuery.getUpdatedTableFrom()
					.leftJoin(SQLFrom.table("employee_post4pps_entry_t", "ep4pps"), "ep4pps.employeepost_id=sse.teacherval1_id");
			int affectedRows = tool.executeUpdate(translator.toSql(updateQuery));
			tool.info("sppschedulesessionevent_t: " + affectedRows + " rows updated");
			// 4. удалим старое
			tool.dropColumn("sppschedulesessionevent_t", "teacherval1_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherDailyPreferenceList

		// раньше свойство teacher ссылалось на сущность employeePost
		// теперь оно ссылается на сущность person
		{
			// 1. переименуем старое свойство
			tool.renameColumn("spptchrdlyprfrnclst_t", "teacher_id", "teacher1_id");
			// 2. создадим новое
			tool.createColumn("spptchrdlyprfrnclst_t", new DBColumn("teacher_id", DBType.LONG).setNullable(true));
			// 3. заполним новое
			SQLUpdateQuery updateQuery = new SQLUpdateQuery("spptchrdlyprfrnclst_t", "tdp");
			updateQuery.set("teacher_id", "pr.person_id");
			updateQuery.getUpdatedTableFrom()
					.innerJoin(SQLFrom.table("employeepost_t", "ep"), "ep.id=tdp.teacher1_id")
					.innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=ep.employee_id");
			int affectedRows = tool.executeUpdate(translator.toSql(updateQuery));
			tool.info("spptchrdlyprfrnclst_t: " + affectedRows + " rows updated");
			tool.setColumnNullable("spptchrdlyprfrnclst_t", "teacher_id", false);
			// 4. удалим старое
			tool.dropColumn("spptchrdlyprfrnclst_t", "teacher1_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherPreferenceList

		// раньше свойство teacher ссылалось на сущность employeePost
		// теперь оно ссылается на сущность person
		{
			// 1. переименуем старое свойство
			tool.renameColumn("sppteacherpreferencelist_t", "teacher_id", "teacher1_id");
			// 2. создадим новое
			tool.createColumn("sppteacherpreferencelist_t", new DBColumn("teacher_id", DBType.LONG).setNullable(true));
			// 3. заполним новое
			SQLUpdateQuery updateQuery = new SQLUpdateQuery("sppteacherpreferencelist_t", "tp");
			updateQuery.set("teacher_id", "pr.person_id");
			updateQuery.getUpdatedTableFrom()
					.innerJoin(SQLFrom.table("employeepost_t", "ep"), "ep.id=tp.teacher1_id")
					.innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=ep.employee_id");
			int affectedRows = tool.executeUpdate(translator.toSql(updateQuery));
			tool.info("sppteacherpreferencelist_t: " + affectedRows + " rows updated");
			tool.setColumnNullable("sppteacherpreferencelist_t", "teacher_id", false);
			// 4. удалим старое
			tool.dropColumn("sppteacherpreferencelist_t", "teacher1_id");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherSessionPreferenceList

		// раньше свойство teacher ссылалось на сущность employeePost
		// теперь оно ссылается на сущность person
		{
			// 1. переименуем старое свойство
			tool.renameColumn("spptchrsssnprfrnclst_t", "teacher_id", "teacher1_id");
			// 2. создадим новое
			tool.createColumn("spptchrsssnprfrnclst_t", new DBColumn("teacher_id", DBType.LONG).setNullable(true));
			// 3. заполним новое
			SQLUpdateQuery updateQuery = new SQLUpdateQuery("spptchrsssnprfrnclst_t", "tsp");
			updateQuery.set("teacher_id", "pr.person_id");
			updateQuery.getUpdatedTableFrom()
					.innerJoin(SQLFrom.table("employeepost_t", "ep"), "ep.id=tsp.teacher1_id")
					.innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=ep.employee_id");
			int affectedRows = tool.executeUpdate(translator.toSql(updateQuery));
			tool.info("spptchrsssnprfrnclst_t: " + affectedRows + " rows updated");
			tool.setColumnNullable("spptchrsssnprfrnclst_t", "teacher_id", false);
			// 4. удалим старое
			tool.dropColumn("spptchrsssnprfrnclst_t", "teacher1_id");
		}


		/*
		test(tool, "sppdisciplinepreferencelist_t", "teacher_id");
		test(tool, "sppscheduledailyevent_t", "teacherval_id");
		test(tool, "sppscheduleperiod_t", "teacherval_id");
		test(tool, "sppscheduleperiodfix_t", "teacherval_id");
		test(tool, "sppschedulesessionevent_t", "teacherval_id");
		test(tool, "spptchrdlyprfrnclst_t", "teacher_id");
		test(tool, "sppteacherpreferencelist_t", "teacher_id");
		test(tool, "spptchrsssnprfrnclst_t", "teacher_id");
		throw new UnsupportedOperationException("STOP");
		*/
	}

	private void test(DBTool tool, String table, String teacherFiled) throws Exception
	{
		String alias = "al";
		SQLSelectQuery query = new SQLSelectQuery();
		query.from(SQLFrom.table(table, alias));
		query.column(alias + "." + teacherFiled);

		Statement stmt = tool.getConnection().createStatement();
		stmt.execute(tool.getDialect().getSQLTranslator().toSql(query));
		ResultSet resultSet = stmt.getResultSet();

		tool.info("============= " + table + " =============");
		while(resultSet.next())
			tool.info(String.valueOf(resultSet.getLong(1)));
	}
}
