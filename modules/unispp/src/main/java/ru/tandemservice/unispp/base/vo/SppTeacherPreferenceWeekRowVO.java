/* $Id: */
package ru.tandemservice.unispp.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceWeekRow;

/**
 * @author vnekrasov
 * @since 6/24/14
 */
public class SppTeacherPreferenceWeekRowVO extends DataWrapper
{
    private ScheduleBellEntry _time;
    private SppTeacherPreferenceWeekRow _weekRow;

    private SppTeacherPreferencePeriodsVO _monday;
    private SppTeacherPreferencePeriodsVO _tuesday;
    private SppTeacherPreferencePeriodsVO _wednesday;
    private SppTeacherPreferencePeriodsVO _thursday;
    private SppTeacherPreferencePeriodsVO _friday;
    private SppTeacherPreferencePeriodsVO _saturday;
    private SppTeacherPreferencePeriodsVO _sunday;

    public SppTeacherPreferenceWeekRowVO(SppTeacherPreferenceWeekRow weekRow)
    {
        super.setId(weekRow.getId());
        _weekRow = weekRow;
        _time = weekRow.getBell();
    }

    public ScheduleBellEntry getTime()
    {
        return _time;
    }

    public void setTime(ScheduleBellEntry time)
    {
        _time = time;
    }

    public SppTeacherPreferencePeriodsVO getMonday()
    {
        return _monday;
    }

    public void setMonday(SppTeacherPreferencePeriodsVO monday)
    {
        _monday = monday;
    }

    public SppTeacherPreferencePeriodsVO getTuesday()
    {
        return _tuesday;
    }

    public void setTuesday(SppTeacherPreferencePeriodsVO tuesday)
    {
        _tuesday = tuesday;
    }

    public SppTeacherPreferencePeriodsVO getWednesday()
    {
        return _wednesday;
    }

    public void setWednesday(SppTeacherPreferencePeriodsVO wednesday)
    {
        _wednesday = wednesday;
    }

    public SppTeacherPreferencePeriodsVO getThursday()
    {
        return _thursday;
    }

    public void setThursday(SppTeacherPreferencePeriodsVO thursday)
    {
        _thursday = thursday;
    }

    public SppTeacherPreferencePeriodsVO getFriday()
    {
        return _friday;
    }

    public void setFriday(SppTeacherPreferencePeriodsVO friday)
    {
        _friday = friday;
    }

    public SppTeacherPreferencePeriodsVO getSaturday()
    {
        return _saturday;
    }

    public void setSaturday(SppTeacherPreferencePeriodsVO saturday)
    {
        _saturday = saturday;
    }

    public SppTeacherPreferencePeriodsVO getSunday()
    {
        return _sunday;
    }

    public void setSunday(SppTeacherPreferencePeriodsVO sunday)
    {
        _sunday = sunday;
    }

    public SppTeacherPreferenceWeekRow getWeekRow()
    {
        return _weekRow;
    }

    public void setWeekRow(SppTeacherPreferenceWeekRow weekRow)
    {
        _weekRow = weekRow;
    }
}

