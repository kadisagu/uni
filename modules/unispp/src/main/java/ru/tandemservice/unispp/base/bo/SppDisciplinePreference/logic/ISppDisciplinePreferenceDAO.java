/* $Id:$ */
package ru.tandemservice.unispp.base.bo.SppDisciplinePreference.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList;

/**
 * @author vnekrasov
 * @since 6/23/14
 */
public interface ISppDisciplinePreferenceDAO extends INeedPersistenceSupport
{

    void saveOrUpdateDisciplinePreferenceList(SppDisciplinePreferenceList preference);

    void deleteDisciplinePreferenceList(Long preferenceId);


}
