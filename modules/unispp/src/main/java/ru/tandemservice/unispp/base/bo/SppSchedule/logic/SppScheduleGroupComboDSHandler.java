/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 9/3/13
 */
public class SppScheduleGroupComboDSHandler extends DefaultComboDataSourceHandler
{
    // properties
    public static String PROP_ORG_UNIT_ID = "orgUnitId";
    public static String PROP_ORG_UNIT_IDS = "orgUnitIds";
    public static String PROP_GROUPS_NOT_IN_ARCHIVE = "groupsNotInArchive";
    public static String PROP_COURSES = "courses";
    public static String PROP_EDU_LEVELS = "eduLevels";
    public static String PROP_GROUP_ID = "groupId";

    public SppScheduleGroupComboDSHandler(String ownerId)
    {
        super(ownerId, Group.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        Long orgUnitId = ep.context.get(PROP_ORG_UNIT_ID);
        List<Long> orgUnitIds = orgUnitId == null ? ep.context.get(PROP_ORG_UNIT_IDS) :
                Collections.singletonList(orgUnitId);

        Boolean groupsNotInArchive = ep.context.getBoolean(PROP_GROUPS_NOT_IN_ARCHIVE, Boolean.FALSE);
        List<Long> courses = ep.context.get(PROP_COURSES);
        List<Long> eduLevels = ep.context.get(PROP_EDU_LEVELS);
        Long groupId = ep.context.get(PROP_GROUP_ID);

        String filter = ep.input.getComboFilterByValue();

        if (null != orgUnitIds && !orgUnitIds.isEmpty())
        {
            DQLSelectBuilder kindRelationsBuilder= new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                    .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                    .where(in(property("rel", OrgUnitToKindRelation.orgUnit().id()), orgUnitIds));
            List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

            Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                    .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

            IDQLExpression condition = nothing();
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                condition = or(condition, in(property("e", Group.educationOrgUnit().educationLevelHighSchool().orgUnit().id()), orgUnitIds));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                condition = or(condition, in(property("e", Group.educationOrgUnit().formativeOrgUnit().id()), orgUnitIds));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                condition = or(condition, in(property("e", Group.educationOrgUnit().territorialOrgUnit().id()), orgUnitIds));

            ep.dqlBuilder.where(condition);
        }

        if (groupsNotInArchive)
            ep.dqlBuilder.where(eq(property("e", Group.archival()), value(Boolean.FALSE)));
        if (null != courses && !courses.isEmpty())
            ep.dqlBuilder.where(in(property("e", Group.course().id()), courses));
        if (null != eduLevels && !eduLevels.isEmpty())
            ep.dqlBuilder.where(in(property("e", Group.educationOrgUnit().educationLevelHighSchool().id()), eduLevels));
        if (null != groupId)
            ep.dqlBuilder.where(ne(property("e", Group.id()), value(groupId)));

        if (!StringUtils.isEmpty(filter))
            ep.dqlBuilder.where(like(DQLFunctions.upper(property("e", Group.title())), value(CoreStringUtils.escapeLike(filter, true))));
    }


    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
    {
        ep.dqlBuilder.order(property("e", Group.title()));
    }
}
