package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLDeleteQuery;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unispp_2x11x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		ISQLTranslator translator = tool.getDialect().getSQLTranslator();

		// в предпочтениях расписаний сессий и подневных нужно проставить звонковое расписание (если оно есть)
		SQLSelectQuery selectBells = new SQLSelectQuery();
		selectBells.from(SQLFrom.table("sc_bell", "b"));
		selectBells.column("b.id");
		selectBells.order("b.active_p", false); // сначала активные
		selectBells.top(1);
		Long bellsId = (Long) tool.getUniqueResult(translator.toSql(selectBells));
		if (bellsId == null) tool.info("bells not found! teacher preference (session and daily) will be deleted");

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherDailyPreferenceElement

		// удалено свойство startHour
		{
			// удалить колонку
			tool.dropColumn("spptchrdlyprfrncelmnt_t", "starthour_p");
		}

		// удалено свойство endHour
		{
			// удалить колонку
			tool.dropColumn("spptchrdlyprfrncelmnt_t", "endhour_p");
		}

		// удалено свойство startMinute
		{
			// удалить колонку
			tool.dropColumn("spptchrdlyprfrncelmnt_t", "startminute_p");
		}

		// удалено свойство endMinute
		{
			// удалить колонку
			tool.dropColumn("spptchrdlyprfrncelmnt_t", "endminute_p");
		}

		// создано свойство bell
		{
			// создать колонку
			tool.createColumn("spptchrdlyprfrncelmnt_t", new DBColumn("bell_id", DBType.LONG));
		}

		// создано обязательное свойство unwantedTime
		{
			// создать колонку
			tool.createColumn("spptchrdlyprfrncelmnt_t", new DBColumn("unwantedtime_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update spptchrdlyprfrncelmnt_t set unwantedtime_p=? where unwantedtime_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("spptchrdlyprfrncelmnt_t", "unwantedtime_p", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherDailyPreferenceList

		// создано обязательное свойство bells
		{
			if (bellsId == null)
			{
				SQLDeleteQuery deleteTDPEQuery = new SQLDeleteQuery("spptchrdlyprfrncelmnt_t", "tdpe");
				tool.executeUpdate(translator.toSql(deleteTDPEQuery));
				SQLDeleteQuery deleteTDPLQuery = new SQLDeleteQuery("spptchrdlyprfrnclst_t", "tdpl");
				tool.executeUpdate(translator.toSql(deleteTDPLQuery));
				tool.info("teacher daily preference deleted");
			}

			// создать колонку
			tool.createColumn("spptchrdlyprfrnclst_t", new DBColumn("bells_id", DBType.LONG));

			if (bellsId != null)
			{
				// задать значение по умолчанию
				tool.executeUpdate("update spptchrdlyprfrnclst_t set bells_id=? where bells_id is null", bellsId);
			}

			// сделать колонку NOT NULL
			tool.setColumnNullable("spptchrdlyprfrnclst_t", "bells_id", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherSessionPreferenceElement

		// удалено свойство startHour
		{
			// удалить колонку
			tool.dropColumn("spptchrsssnprfrncelmnt_t", "starthour_p");
		}

		// удалено свойство endHour
		{
			// удалить колонку
			tool.dropColumn("spptchrsssnprfrncelmnt_t", "endhour_p");
		}

		// удалено свойство startMinute
		{
			// удалить колонку
			tool.dropColumn("spptchrsssnprfrncelmnt_t", "startminute_p");
		}

		// удалено свойство endMinute
		{
			// удалить колонку
			tool.dropColumn("spptchrsssnprfrncelmnt_t", "endminute_p");
		}

		// создано свойство bell
		{
			// создать колонку
			tool.createColumn("spptchrsssnprfrncelmnt_t", new DBColumn("bell_id", DBType.LONG));
		}

		// создано обязательное свойство unwantedTime
		{
			// создать колонку
			tool.createColumn("spptchrsssnprfrncelmnt_t", new DBColumn("unwantedtime_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update spptchrsssnprfrncelmnt_t set unwantedtime_p=? where unwantedtime_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("spptchrsssnprfrncelmnt_t", "unwantedtime_p", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherSessionPreferenceList

		// создано обязательное свойство bells
		{
			if (bellsId == null)
			{
				SQLDeleteQuery deleteTSPEQuery = new SQLDeleteQuery("spptchrsssnprfrncelmnt_t", "tspe");
				tool.executeUpdate(translator.toSql(deleteTSPEQuery));
				SQLDeleteQuery deleteTSPLQuery = new SQLDeleteQuery("spptchrsssnprfrnclst_t", "tspl");
				tool.executeUpdate(translator.toSql(deleteTSPLQuery));
				tool.info("teacher session preference deleted");
			}

			// создать колонку
			tool.createColumn("spptchrsssnprfrnclst_t", new DBColumn("bells_id", DBType.LONG));

			if (bellsId != null)
			{
				// задать значение по умолчанию
				tool.executeUpdate("update spptchrsssnprfrnclst_t set bells_id=? where bells_id is null", bellsId);
			}

			// сделать колонку NOT NULL
			tool.setColumnNullable("spptchrsssnprfrnclst_t", "bells_id", false);
		}
    }
}
