/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.SeasonAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;

/**
 * @author vnekrasov
 * @since 12/26/13
 */
@Configuration
public class SppScheduleSessionSeasonAddEdit extends BusinessComponentManager
{
    public static final String YEAR_PART_DS = "yearPartDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEAR_PART_DS, getName(), EppYearPart.defaultSelectDSHandler(getName())))
                .create();
    }
}
