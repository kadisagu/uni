/* $Id:$ */
package ru.tandemservice.unispp.component.registry.registryElementSppData.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.uni.util.AddEditResult;

/**
 * @author Victor Nekrasov
 * @since 21.05.2014
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
//    public void onClickAddPreference(IBusinessComponent component)
//    {
//        Model model = getModel(component);
//        component.createDefaultChildRegion(new ComponentActivator(SppDisciplinePreferenceAdd.class.getSimpleName(), new ParametersMap()
//                .add("disciplineId", model.getElement().getId())));
//    }
//
//    public void onClickEditItem(final IBusinessComponent component)
//    {
//        Model model = getModel(component);
//        component.createDefaultChildRegion(new ComponentActivator(SppDisciplinePreferenceEdit.class.getSimpleName(), new ParametersMap()
//                .add("preferenceId", component.getListenerParameter())
//        ));
//    }
//
//    public void onClickDeleteItem(final IBusinessComponent component)
//    {
//        Model model = getModel(component);
//        SppDisciplinePreferenceManager.instance().dao().deleteDisciplinePreferenceList((Long) component.getListenerParameter());
//    }

    public static Long executeSave(final IBusinessComponent component)
    {
        final Model model = (Model) component.getModel();

        ((Controller) component.getController()).save(component);
        return model.getElement().getId();
    }


    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onClickApply(final IBusinessComponent component)
    {
        AddEditResult.saveAndDeactivate(component, this.getModel(component).getElement(), () -> Controller.this.save(component));
    }

    protected void save(final IBusinessComponent component)
    {
        this.getDao().save(this.getModel(component));
    }
}
