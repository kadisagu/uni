package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент предпочтения преподавателя по расписанию сессии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppTeacherSessionPreferenceElementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement";
    public static final String ENTITY_NAME = "sppTeacherSessionPreferenceElement";
    public static final int VERSION_HASH = -864050386;
    private static IEntityMeta ENTITY_META;

    public static final String L_TEACHER_PREFERENCE = "teacherPreference";
    public static final String P_DAY_OF_WEEK = "dayOfWeek";
    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_BELL = "bell";
    public static final String L_BUILDING = "building";
    public static final String L_LECTURE_ROOM = "lectureRoom";
    public static final String P_UNWANTED_TIME = "unwantedTime";

    private SppTeacherSessionPreferenceList _teacherPreference;     // Список предпочтений преподавателя
    private Integer _dayOfWeek;     // День недели
    private Date _startDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private ScheduleBellEntry _bell;     // Пара
    private UniplacesBuilding _building;     // Здание
    private UniplacesPlace _lectureRoom;     // Аудитория
    private boolean _unwantedTime = false;     // Нежелательное время

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Список предпочтений преподавателя. Свойство не может быть null.
     */
    @NotNull
    public SppTeacherSessionPreferenceList getTeacherPreference()
    {
        return _teacherPreference;
    }

    /**
     * @param teacherPreference Список предпочтений преподавателя. Свойство не может быть null.
     */
    public void setTeacherPreference(SppTeacherSessionPreferenceList teacherPreference)
    {
        dirty(_teacherPreference, teacherPreference);
        _teacherPreference = teacherPreference;
    }

    /**
     * @return День недели.
     */
    public Integer getDayOfWeek()
    {
        return _dayOfWeek;
    }

    /**
     * @param dayOfWeek День недели.
     */
    public void setDayOfWeek(Integer dayOfWeek)
    {
        dirty(_dayOfWeek, dayOfWeek);
        _dayOfWeek = dayOfWeek;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала. Свойство не может быть null.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Пара.
     */
    public ScheduleBellEntry getBell()
    {
        return _bell;
    }

    /**
     * @param bell Пара.
     */
    public void setBell(ScheduleBellEntry bell)
    {
        dirty(_bell, bell);
        _bell = bell;
    }

    /**
     * @return Здание.
     */
    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    /**
     * @param building Здание.
     */
    public void setBuilding(UniplacesBuilding building)
    {
        dirty(_building, building);
        _building = building;
    }

    /**
     * @return Аудитория.
     */
    public UniplacesPlace getLectureRoom()
    {
        return _lectureRoom;
    }

    /**
     * @param lectureRoom Аудитория.
     */
    public void setLectureRoom(UniplacesPlace lectureRoom)
    {
        dirty(_lectureRoom, lectureRoom);
        _lectureRoom = lectureRoom;
    }

    /**
     * @return Нежелательное время. Свойство не может быть null.
     */
    @NotNull
    public boolean isUnwantedTime()
    {
        return _unwantedTime;
    }

    /**
     * @param unwantedTime Нежелательное время. Свойство не может быть null.
     */
    public void setUnwantedTime(boolean unwantedTime)
    {
        dirty(_unwantedTime, unwantedTime);
        _unwantedTime = unwantedTime;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppTeacherSessionPreferenceElementGen)
        {
            setTeacherPreference(((SppTeacherSessionPreferenceElement)another).getTeacherPreference());
            setDayOfWeek(((SppTeacherSessionPreferenceElement)another).getDayOfWeek());
            setStartDate(((SppTeacherSessionPreferenceElement)another).getStartDate());
            setEndDate(((SppTeacherSessionPreferenceElement)another).getEndDate());
            setBell(((SppTeacherSessionPreferenceElement)another).getBell());
            setBuilding(((SppTeacherSessionPreferenceElement)another).getBuilding());
            setLectureRoom(((SppTeacherSessionPreferenceElement)another).getLectureRoom());
            setUnwantedTime(((SppTeacherSessionPreferenceElement)another).isUnwantedTime());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppTeacherSessionPreferenceElementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppTeacherSessionPreferenceElement.class;
        }

        public T newInstance()
        {
            return (T) new SppTeacherSessionPreferenceElement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "teacherPreference":
                    return obj.getTeacherPreference();
                case "dayOfWeek":
                    return obj.getDayOfWeek();
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
                case "bell":
                    return obj.getBell();
                case "building":
                    return obj.getBuilding();
                case "lectureRoom":
                    return obj.getLectureRoom();
                case "unwantedTime":
                    return obj.isUnwantedTime();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "teacherPreference":
                    obj.setTeacherPreference((SppTeacherSessionPreferenceList) value);
                    return;
                case "dayOfWeek":
                    obj.setDayOfWeek((Integer) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "bell":
                    obj.setBell((ScheduleBellEntry) value);
                    return;
                case "building":
                    obj.setBuilding((UniplacesBuilding) value);
                    return;
                case "lectureRoom":
                    obj.setLectureRoom((UniplacesPlace) value);
                    return;
                case "unwantedTime":
                    obj.setUnwantedTime((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "teacherPreference":
                        return true;
                case "dayOfWeek":
                        return true;
                case "startDate":
                        return true;
                case "endDate":
                        return true;
                case "bell":
                        return true;
                case "building":
                        return true;
                case "lectureRoom":
                        return true;
                case "unwantedTime":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "teacherPreference":
                    return true;
                case "dayOfWeek":
                    return true;
                case "startDate":
                    return true;
                case "endDate":
                    return true;
                case "bell":
                    return true;
                case "building":
                    return true;
                case "lectureRoom":
                    return true;
                case "unwantedTime":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "teacherPreference":
                    return SppTeacherSessionPreferenceList.class;
                case "dayOfWeek":
                    return Integer.class;
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "bell":
                    return ScheduleBellEntry.class;
                case "building":
                    return UniplacesBuilding.class;
                case "lectureRoom":
                    return UniplacesPlace.class;
                case "unwantedTime":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppTeacherSessionPreferenceElement> _dslPath = new Path<SppTeacherSessionPreferenceElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppTeacherSessionPreferenceElement");
    }
            

    /**
     * @return Список предпочтений преподавателя. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getTeacherPreference()
     */
    public static SppTeacherSessionPreferenceList.Path<SppTeacherSessionPreferenceList> teacherPreference()
    {
        return _dslPath.teacherPreference();
    }

    /**
     * @return День недели.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getDayOfWeek()
     */
    public static PropertyPath<Integer> dayOfWeek()
    {
        return _dslPath.dayOfWeek();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Пара.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getBell()
     */
    public static ScheduleBellEntry.Path<ScheduleBellEntry> bell()
    {
        return _dslPath.bell();
    }

    /**
     * @return Здание.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getBuilding()
     */
    public static UniplacesBuilding.Path<UniplacesBuilding> building()
    {
        return _dslPath.building();
    }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getLectureRoom()
     */
    public static UniplacesPlace.Path<UniplacesPlace> lectureRoom()
    {
        return _dslPath.lectureRoom();
    }

    /**
     * @return Нежелательное время. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#isUnwantedTime()
     */
    public static PropertyPath<Boolean> unwantedTime()
    {
        return _dslPath.unwantedTime();
    }

    public static class Path<E extends SppTeacherSessionPreferenceElement> extends EntityPath<E>
    {
        private SppTeacherSessionPreferenceList.Path<SppTeacherSessionPreferenceList> _teacherPreference;
        private PropertyPath<Integer> _dayOfWeek;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;
        private ScheduleBellEntry.Path<ScheduleBellEntry> _bell;
        private UniplacesBuilding.Path<UniplacesBuilding> _building;
        private UniplacesPlace.Path<UniplacesPlace> _lectureRoom;
        private PropertyPath<Boolean> _unwantedTime;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Список предпочтений преподавателя. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getTeacherPreference()
     */
        public SppTeacherSessionPreferenceList.Path<SppTeacherSessionPreferenceList> teacherPreference()
        {
            if(_teacherPreference == null )
                _teacherPreference = new SppTeacherSessionPreferenceList.Path<SppTeacherSessionPreferenceList>(L_TEACHER_PREFERENCE, this);
            return _teacherPreference;
        }

    /**
     * @return День недели.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getDayOfWeek()
     */
        public PropertyPath<Integer> dayOfWeek()
        {
            if(_dayOfWeek == null )
                _dayOfWeek = new PropertyPath<Integer>(SppTeacherSessionPreferenceElementGen.P_DAY_OF_WEEK, this);
            return _dayOfWeek;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(SppTeacherSessionPreferenceElementGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(SppTeacherSessionPreferenceElementGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Пара.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getBell()
     */
        public ScheduleBellEntry.Path<ScheduleBellEntry> bell()
        {
            if(_bell == null )
                _bell = new ScheduleBellEntry.Path<ScheduleBellEntry>(L_BELL, this);
            return _bell;
        }

    /**
     * @return Здание.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getBuilding()
     */
        public UniplacesBuilding.Path<UniplacesBuilding> building()
        {
            if(_building == null )
                _building = new UniplacesBuilding.Path<UniplacesBuilding>(L_BUILDING, this);
            return _building;
        }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#getLectureRoom()
     */
        public UniplacesPlace.Path<UniplacesPlace> lectureRoom()
        {
            if(_lectureRoom == null )
                _lectureRoom = new UniplacesPlace.Path<UniplacesPlace>(L_LECTURE_ROOM, this);
            return _lectureRoom;
        }

    /**
     * @return Нежелательное время. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement#isUnwantedTime()
     */
        public PropertyPath<Boolean> unwantedTime()
        {
            if(_unwantedTime == null )
                _unwantedTime = new PropertyPath<Boolean>(SppTeacherSessionPreferenceElementGen.P_UNWANTED_TIME, this);
            return _unwantedTime;
        }

        public Class getEntityClass()
        {
            return SppTeacherSessionPreferenceElement.class;
        }

        public String getEntityName()
        {
            return "sppTeacherSessionPreferenceElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
