package ru.tandemservice.unispp.base.entity;

import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.unispp.base.entity.gen.*;

/**
 * @see ru.tandemservice.unispp.base.entity.gen.SppScheduleSessionEventExtGen
 */
public class SppScheduleSessionEventExt extends SppScheduleSessionEventExtGen
{
    public SppScheduleSessionEventExt()
    {
    }

    public SppScheduleSessionEventExt(SppScheduleSessionEvent event, EppRegistryDiscipline discipline, EppFControlActionType loadType)
    {
        this.setSppScheduleSessionEvent(event);
        this.setEppRegistryDiscipline(discipline);
        this.setEppFControlActionType(loadType);
    }
}