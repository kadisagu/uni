/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import com.google.common.collect.Lists;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.vo.SppScheduleDisciplineVO;
import ru.tandemservice.unispp.base.vo.SppScheduleEventInDayVO;
import ru.tandemservice.unispp.base.vo.SppScheduleLoadVO;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 09.02.2017
 */
public class SppScheduleLoadDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String PROP_SCHEDULE_ID = "scheduleId";

    public SppScheduleLoadDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long scheduleId = context.get(PROP_SCHEDULE_ID);

        Long periodDuration = Integer.toUnsignedLong(UniDaoFacade.getCoreDao().getProperty(SppSchedule.class, SppSchedule.bells().periodDuration().s(), SppSchedule.P_ID, scheduleId));

        // возьмём все занятия, которые идут в этом расписании (и в которых указан предмет))
        List<SppScheduleEventInDayVO> eventList = SppScheduleManager.instance().eventsDao().createAllScheduleEvents(Collections.singletonList(scheduleId), true)
                .stream().filter(e -> e.getSubjectVal() != null && !e.isCanceled()).collect(Collectors.toList());

        // будем группировать по паре (дисциплина, вид нагрузки)
        Function<SppScheduleEventInDayVO, CoreCollectionUtils.Pair<EppRegistryElement, EppALoadType>> groupFunction = event ->
                new CoreCollectionUtils.Pair<>(event.getSubjectVal(), event.getSubjectLoadType());
        // группируем c подсчётом нагрузки (кол-во занятий * продолжительность занятия)
        Map<CoreCollectionUtils.Pair<EppRegistryElement, EppALoadType>, Long> loads = eventList.stream()
                .collect(Collectors.groupingBy(groupFunction, Collectors.reducing(0L, e -> periodDuration, Long::sum)));

        // возьмём все занятия, которые должны быть у этой группы в её текущих (по части уч. года) РУПах
        List<SppScheduleDisciplineVO> allDisciplines = getDisciplines(scheduleId);
        allDisciplines.forEach(d -> {
            CoreCollectionUtils.Pair<EppRegistryElement, EppALoadType> pair = new CoreCollectionUtils.Pair<>(d.getDiscipline(), d.getEppALoadType());
            if (!loads.containsKey(pair)) { loads.put(pair, 0L); }
        });

        List<SppScheduleLoadVO> loadVOList = loads.keySet().stream()
                .map(pair -> new SppScheduleLoadVO(pair.getX(), pair.getY(), getRequiredLoad(pair.getX(), pair.getY()), loads.get(pair)))
                .collect(Collectors.toList());
        Collections.sort(loadVOList);

        return ListOutputBuilder.get(input, loadVOList).pageable(true).build();
    }

    private Double getRequiredLoad(EppRegistryElement discipline, EppALoadType loadType)
    {
        final Double defaultValue = 0D;
        final ICommonDAO dao = DataAccessServices.dao();
        final List<EppRegistryElementPart> registryElementPartList = dao.getList(EppRegistryElementPart.class, EppRegistryElementPart.registryElement(), discipline);
        final EppRegistryElementPart registryElementPart = (registryElementPartList != null && !registryElementPartList.isEmpty()) ? registryElementPartList.get(0) : null;

        if (registryElementPart == null) return defaultValue;
        final List<EppRegistryElementPartModule> elementPartModuleList = dao.getList(EppRegistryElementPartModule.class, EppRegistryElementPartModule.part(), registryElementPart);

        final EppRegistryElementPartModule elementPartModule = (elementPartModuleList != null && !elementPartModuleList.isEmpty()) ? elementPartModuleList.get(0) : null;
        if (elementPartModule == null) return defaultValue;

        final EppRegistryModule registryModule = elementPartModule.getModule();

        final String alias = "erml";
        final DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppRegistryModuleALoad.class, alias).column(alias)
                .where(eq(property(alias, EppRegistryModuleALoad.module()), value(registryModule)))
                .where(eq(property(alias, EppRegistryModuleALoad.loadType()), value(loadType)));

        final List<EppRegistryModuleALoad> registryModuleALoadList = dao.getList(builder);
        final EppRegistryModuleALoad moduleALoad = (registryModuleALoadList != null && !registryModuleALoadList.isEmpty()) ? registryModuleALoadList.get(0) : null;
        if (moduleALoad == null) return defaultValue;

        return UniEppUtils.wrap(moduleALoad.getLoad());
    }

    private List<SppScheduleDisciplineVO> getDisciplines(Long scheduleId)
    {
        // =============== ПОЧТИ КОПИПАСТА ИЗ ХЭНДЛЕРА ДИСЦИПЛИН ===============

        // возьмём нужную группу и часть учебного года
        DQLSelectBuilder scheduleBuilder = new DQLSelectBuilder()
                .fromEntity(SppSchedule.class, "sch")
                .fetchPath(DQLJoinType.inner, SppSchedule.group().fromAlias("sch"), "g")
                .fetchPath(DQLJoinType.inner, SppSchedule.season().eppYearPart().fromAlias("sch"), "yp")
                .fetchPath(DQLJoinType.inner, EppYearPart.year().educationYear().fromAlias("yp"), "year")
                .fetchPath(DQLJoinType.inner, EppYearPart.part().fromAlias("yp"), "part")
                .where(eq(property("sch.id"), value(scheduleId)));
        List<SppSchedule> scheduleList = createStatement(scheduleBuilder).list();
        if (scheduleList.isEmpty()) return Collections.emptyList();
        SppSchedule schedule = scheduleList.get(0);
        Long groupId = schedule.getGroup().getId();
        Long yearId = schedule.getSeason().getEppYearPart().getYear().getEducationYear().getId();
        Long partId = schedule.getSeason().getEppYearPart().getPart().getId();

        // возьмём РУПы студентов этой группы
        DQLSelectBuilder wpBuilder = new DQLSelectBuilder();
        wpBuilder.fromEntity(EppStudent2WorkPlan.class, "st2wp");
        wpBuilder.column(property("wpb.id"), "wpbId");
        wpBuilder.column(caseExpr(isNotNull(property("wp.id")), property("wp.id"), property("wpv", EppWorkPlanVersion.parent().id())), "wpId");
        wpBuilder.joinPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("st2wp"), "st2epv");
        wpBuilder.joinPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias("st2wp"), "wpb");
        wpBuilder.joinEntity("wpb", DQLJoinType.left, EppWorkPlan.class, "wp", eq(property("wp.id"), property("wpb.id")));
        wpBuilder.joinEntity("wpb", DQLJoinType.left, EppWorkPlanVersion.class, "wpv", eq(property("wpv.id"), property("wpb.id")));
        wpBuilder.where(in(property("st2epv", EppStudent2EduPlanVersion.student().id()),
                new DQLSelectBuilder().fromEntity(Student.class, "st").column("st.id")
                        .where(eq(property("st", Student.group().id()), value(groupId)))
                        .where(eq(property("st", Student.archival()), value(Boolean.FALSE)))
                        .buildQuery()));
        // только по соответствующей части уч. года
        wpBuilder.where(eq(property("st2wp", EppStudent2WorkPlan.cachedEppYear().educationYear().id()), value(yearId)));
        wpBuilder.where(eq(property("st2wp", EppStudent2WorkPlan.cachedGridTerm().part().id()), value(partId)));
        // только актуальные
        wpBuilder.where(isNull(property("st2wp", EppStudent2WorkPlan.removalDate())));
        wpBuilder.where(isNull(property("st2epv", EppStudent2EduPlanVersion.removalDate())));
        wpBuilder.distinct();

        // возьмём все дисциплины этих РУПов
        DQLSelectBuilder disciplineBuilder = new DQLSelectBuilder();
        disciplineBuilder.fromDataSource(wpBuilder.buildQuery(), "wpSelect");
        disciplineBuilder.column("d.id", "dId").column("term.id", "termId");
        disciplineBuilder.joinEntity("wpSelect", DQLJoinType.inner, EppWorkPlan.class, "wp", eq(property("wp.id"), property("wpSelect.wpId")));
        disciplineBuilder.joinPath(DQLJoinType.inner, EppWorkPlan.term().fromAlias("wp"), "term");
        disciplineBuilder.joinEntity("wpSelect", DQLJoinType.inner, EppWorkPlanRegistryElementRow.class, "r", eq(
                property("r", EppWorkPlanRegistryElementRow.workPlan().id()), property("wpSelect.wpbId")));
        disciplineBuilder.joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().fromAlias("r"), "el");
        disciplineBuilder.joinEntity("el", DQLJoinType.inner, EppRegistryDiscipline.class, "d", eq(property("el.id"), property("d.id")));
        disciplineBuilder.distinct();

        DQLSelectBuilder finalBuilder = new DQLSelectBuilder().fromDataSource(disciplineBuilder.buildQuery(), "dSelect");
        finalBuilder.column("d").column("al").column("term");
        finalBuilder.joinEntity("dSelect", DQLJoinType.inner, EppRegistryDiscipline.class, "d", eq(property("dSelect.dId"), property("d.id")));
        finalBuilder.joinEntity("dSelect", DQLJoinType.inner, Term.class, "term", eq(property("dSelect.termId"), property("term.id")));
        // подцепим ауд.нагрузку
        finalBuilder.joinEntity("d", DQLJoinType.inner, EppRegistryElementLoad.class, "rel",
                eq(property("d.id"), property("rel", EppRegistryElementLoad.registryElement().id())));
        finalBuilder.where(gt(property("rel", EppRegistryElementLoad.load()), value(0)));
        finalBuilder.joinEntity("rel", DQLJoinType.inner, EppALoadType.class, "al",
                eq(property("rel", EppRegistryElementLoad.loadType().id()), property("al.id")));
        finalBuilder.distinct();

        List<Object[]> finalList = createStatement(finalBuilder).list();
        List<SppScheduleDisciplineVO> disciplineVOs = Lists.newArrayList();
        for (Object[] row : finalList)
        {
            EppRegistryDiscipline discipline = (EppRegistryDiscipline) row[0];
            EppALoadType loadType = (EppALoadType) row[1];
            Term term = (Term) row[2];
            if (discipline != null && loadType != null && term != null)
                disciplineVOs.add(new SppScheduleDisciplineVO(discipline, loadType, term));
        }
        Collections.sort(disciplineVOs);

        return disciplineVOs;
    }
}
