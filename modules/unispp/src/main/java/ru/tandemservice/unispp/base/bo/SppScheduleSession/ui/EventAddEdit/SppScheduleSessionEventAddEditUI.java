/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.EventAddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.EventAddEdit.SppScheduleDailyEventAddEdit;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionSubjectComboDSHandler;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionEvent;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionEventExt;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;
import ru.tandemservice.unispp.base.vo.SppScheduleSessionDisciplineVO;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author vnekrasov
 * @since 12/26/13
 */
@Input({
        @Bind(key = "scheduleId", binding = "scheduleId", required = true),
        @Bind(key = "eventId", binding = "eventId")
})
public class SppScheduleSessionEventAddEditUI extends UIPresenter
{
    public static final String BELLS_ID = "bellsId";

    public static final String PARAM_EVENT_ID = "eventId";
    public static final String PARAM_EVENT_DATE = "eventDate";
    public static final String PARAM_EVENT_BELL_ENTRY = "eventBellEntry";

    private Long _scheduleId;
    private Long _eventId;
    private SppScheduleSessionEvent _event;
    private SppScheduleSession _schedule;
    private SppScheduleSessionDisciplineVO _disciplineVO;
    private List<SppScheduleSessionDisciplineVO> _disciplineVOList;
    private int _studentGrpCnt;
    private DataWrapper _lectureRoomWrapper;


    private boolean _checkFree;
    private boolean _checkLectureRoomCapacity;
    private boolean _checkTeacherPreferences;
    private boolean _checkDuplicates;

    public DataWrapper getLectureRoomWrapper()
    {
        return _lectureRoomWrapper;
    }

    public void setLectureRoomWrapper(DataWrapper lectureRoomWrapper)
    {
        _lectureRoomWrapper = lectureRoomWrapper;
    }

    public UniplacesClassroomType getLectureRoomType()
    {
        return _lectureRoomType;
    }

    public void setLectureRoomType(UniplacesClassroomType lectureRoomType)
    {
        _lectureRoomType = lectureRoomType;
    }

    private UniplacesClassroomType _lectureRoomType;

    public boolean isCheckFree()
    {
        return _checkFree;
    }

    public void setCheckFree(boolean checkFree)
    {
        _checkFree = checkFree;
    }

    public boolean isCheckLectureRoomCapacity()
    {
        return _checkLectureRoomCapacity;
    }

    public void setCheckLectureRoomCapacity(boolean checkLectureRoomCapacity)
    {
        _checkLectureRoomCapacity = checkLectureRoomCapacity;
    }

    public boolean isCheckTeacherPreferences()
    {
        return _checkTeacherPreferences;
    }

    public void setCheckTeacherPreferences(boolean checkTeacherPreferences)
    {
        _checkTeacherPreferences = checkTeacherPreferences;
    }

    public boolean isCheckDuplicates()
    {
        return _checkDuplicates;
    }

    public void setCheckDuplicates(boolean checkDuplicates)
    {
        _checkDuplicates = checkDuplicates;
    }

    public Long getScheduleId()
    {
        return _scheduleId;
    }

    public void setScheduleId(Long scheduleId)
    {
        _scheduleId = scheduleId;
    }

    public Long getEventId()
    {
        return _eventId;
    }

    public void setEventId(Long eventId)
    {
        _eventId = eventId;
    }

    public SppScheduleSessionEvent getEvent()
    {
        return _event;
    }

    public void setEvent(SppScheduleSessionEvent event)
    {
        _event = event;
    }

    public SppScheduleSession getSchedule()
    {
        return _schedule;
    }

    public void setSchedule(SppScheduleSession schedule)
    {
        _schedule = schedule;
    }

    public Boolean getAddForm()
    {
        return null == _eventId;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _schedule)
        {
            _schedule = DataAccessServices.dao().getNotNull(_scheduleId);
            _studentGrpCnt = SppScheduleManager.instance().dao().getGroupStudentCount(_schedule.getGroup().getId());
        }
        if (null == _event)
        {
            if (null != _eventId)
            {
                _event = DataAccessServices.dao().get(_eventId);
                if (null != _event.getSubjectVal())
                {
                    _disciplineVO = new SppScheduleSessionDisciplineVO((EppRegistryDiscipline) _event.getSubjectVal(), _event.getSubjectFormControl());
                }
                if (null != _event.getLectureRoomVal())
                {
                    _lectureRoomWrapper = new DataWrapper();
                    _lectureRoomWrapper.setId(_event.getLectureRoomVal().getId());
                }
                _disciplineVOList = SppScheduleSessionManager.instance().dao().getSppScheduleSessionEventExt(_event)
                        .stream()
                        .map(e -> new SppScheduleSessionDisciplineVO(e.getEppRegistryDiscipline(), e.getEppFControlActionType()))
                        .collect(Collectors.toList());
            } else
            {
                _event = new SppScheduleSessionEvent();
                _event.setSchedule(_schedule);
            }
        }

        setCheckFree(true);
        setCheckLectureRoomCapacity(true);
        setCheckTeacherPreferences(true);
        setCheckDuplicates(true);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleSessionEventAddEdit.SUBJECT_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleSessionSubjectComboDSHandler.PROP_GROUP_ID, _schedule.getGroup().getId());
            dataSource.put(SppScheduleSessionSubjectComboDSHandler.PROP_SEASON_ID, _schedule.getSeason().getId());
            dataSource.put(SppScheduleSessionSubjectComboDSHandler.PROP_CHECK_DUPLICATES, _checkDuplicates);
            dataSource.put(SppScheduleSessionSubjectComboDSHandler.PROP_SCHEDULE_ID, _scheduleId);
            if (_eventId != null)
                dataSource.put(SppScheduleSessionSubjectComboDSHandler.PROP_CURRENT_EVENT_ID, _eventId);
            dataSource.put(SppScheduleSessionSubjectComboDSHandler.PROP_DATE, _event.getDate());
        } else if (SppScheduleSessionEventAddEdit.TEACHER_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_EVENT_ID, _event.getId());
            dataSource.put(PARAM_EVENT_DATE, _event.getDate());
            dataSource.put(PARAM_EVENT_BELL_ENTRY, _event.getBell());
            dataSource.put("seasonId", _schedule.getSeason().getId());
            dataSource.put("checkFreeTeachers", _checkFree);
            dataSource.put("checkTeacherPreferences", _checkTeacherPreferences);
            if (null != _lectureRoomWrapper)
                dataSource.put("lectureRoomId", _lectureRoomWrapper.getId());
        } else if (SppScheduleSessionEventAddEdit.LECTURE_ROOM_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_EVENT_ID, _event.getId());
            dataSource.put(PARAM_EVENT_BELL_ENTRY, _event.getBell());

            if (_checkLectureRoomCapacity)
                dataSource.put("studentGrpCnt", _studentGrpCnt);

            if (_lectureRoomType != null)
                dataSource.put("lectureRoomType", _lectureRoomType);

            dataSource.put("seasonId", _schedule.getSeason().getId());
            dataSource.put("checkFreeLectureRooms", _checkFree);

            dataSource.put(PARAM_EVENT_DATE, _event.getDate());

            if (_event.getTeacherVal() != null)
                dataSource.put("teacherId", _event.getTeacherVal().getId());

            dataSource.put("checkTeacherPreferences", _checkTeacherPreferences);

            if (_disciplineVO != null)
                if (_disciplineVO.getDiscipline() != null)
                    dataSource.put("disciplineId", _disciplineVO.getDiscipline().getId());
        } else if (SppScheduleDailyEventAddEdit.BELL_DS.equals(dataSource.getName()))
        {
            dataSource.put(BELLS_ID, _schedule.getBells().getId());
        }
    }

    public String getPeriodStr()
    {
        return _schedule.getSeason().getTitleWithTime();
    }

    public void onChangeLectureRoom()
    {
        UniplacesPlace lectureRoom = new UniplacesPlace();
        if (_lectureRoomWrapper != null)
            lectureRoom = DataAccessServices.dao().get(_lectureRoomWrapper.getId());
        if (null != lectureRoom.getId())
        {
            _event.setLectureRoomVal(lectureRoom);
            _event.setLectureRoom(lectureRoom.getTitle());
        } else
        {
            _event.setLectureRoomVal(null);
            _event.setLectureRoom(null);
        }
    }

    public void onChangeSubject()
    {
        if (null != _disciplineVO)
        {
            _event.setSubject(_disciplineVO.getTitle());
        }
    }

    public void onChangeTeacher()
    {
        if (null != _event.getTeacherVal())
            _event.setTeacher(_event.getTeacherVal().getFio());
    }

    public void onChangeLectureRoomType()
    {
    }

    public void onClickApply()
    {
        SppScheduleSessionSeason season = _event.getSchedule().getSeason();
        if (_event.getDate().before(season.getStartDate()))
            _uiSupport.error("Событие не может быть раньше чем дата начала периода расписания", "startDate");
        if (_event.getDate().after(season.getEndDate()))
            _uiSupport.error("Событие не может быть позже чем дата окончания периода расписания", "startDate");
        if (getUserContext().getErrorCollector().hasErrors())
            return;

        if (null != _disciplineVO)
        {
            _event.setSubjectVal(_disciplineVO.getDiscipline());
            _event.setSubjectFormControl(_disciplineVO.getFormControl());
        } else
        {
            _event.setSubjectVal(null);
            _event.setSubjectFormControl(null);
        }
        SppScheduleSessionManager.instance().dao().createOrUpdateEvent(_event);
        SppScheduleSessionManager.instance().dao().updateScheduleAsChanged(_schedule.getId());

        final Collection<SppScheduleSessionEventExt> eventExtCollection = _disciplineVOList.stream()
                .map(o -> new SppScheduleSessionEventExt(_event, o.getDiscipline(), o.getFormControl()))
                .collect(Collectors.toList());
        SppScheduleSessionManager.instance().dao().saveSppScheduleSessionEventExtList(_event, eventExtCollection);

        deactivate();
    }
}
