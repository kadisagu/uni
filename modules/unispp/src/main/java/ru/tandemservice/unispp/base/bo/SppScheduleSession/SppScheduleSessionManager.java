/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.*;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.*;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 12/26/13
 */
@Configuration
public class SppScheduleSessionManager extends BusinessObjectManager
{
    public static final String ALL_POSTS_PROP = "allPosts";

    public static SppScheduleSessionManager instance()
    {
        return instance(SppScheduleSessionManager.class);
    }

    @Bean
    public ISppScheduleSessionDAO dao()
    {
        return new SppScheduleSessionDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler sppScheduleSessionSeasonDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                String title = context.get("title");
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleSessionSeason.class, "s");

                if (!StringUtils.isEmpty(title))
                    builder.where(like(DQLFunctions.upper(DQLExpressions.property("s", SppScheduleSessionSeason.title())), value(CoreStringUtils.escapeLike(title, true))));

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order().build();
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler seasonComboDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), SppScheduleSessionSeason.class).filter(SppScheduleSessionSeason.title()).order(SppScheduleSessionSeason.startDate());
    }

    @Bean
    public IDefaultSearchDataSourceHandler sppScheduleSessionDSHandler()
    {
        return new SppScheduleSessionDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> bellsComboDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), ScheduleBell.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(eq(property(alias, ScheduleBell.active()), value(Boolean.TRUE)));
            }
        };
        return handler.filter(ScheduleBell.title()).order(ScheduleBell.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler employeePostComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EmployeePost.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.employee().archival()), DQLExpressions.value(false)));

                Boolean allPosts = ep.context.get(ALL_POSTS_PROP);
                if (allPosts != null && !allPosts)
                {
                    ep.dqlBuilder.where(eq(property("e", EmployeePost.postRelation().headerPost()), value(true)));
                }

                String filter = ep.input.getComboFilterByValue();
                if (!StringUtils.isEmpty(filter))
                    ep.dqlBuilder.where(DQLExpressions.like(DQLFunctions.upper(DQLFunctions.concat(
                            DQLExpressions.property("e", EmployeePost.person().identityCard().lastName()),
                            DQLExpressions.property("e", EmployeePost.postRelation().postBoundedWithQGandQL().title()),
                            DQLExpressions.property("e", EmployeePost.orgUnit().fullTitle()))), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                setOrderByProperty(EmployeePost.person().identityCard().lastName().s());
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler sessionComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).
                addItemList(sessionOptionsExtPoint());
    }

    //Семестры
    public final static Long TERM_WINTER = 1L;
    public final static Long TERM_SUMMER = 2L;


    @Bean
    public ItemListExtPoint<DataWrapper> sessionOptionsExtPoint()
    {
        return itemList(DataWrapper.class).
                add(TERM_WINTER.toString(), new DataWrapper(TERM_WINTER, "ui.session.winter")).
                add(TERM_SUMMER.toString(), new DataWrapper(TERM_SUMMER, "ui.sessiion.summer")).
                create();
    }

    @Bean
    public IDefaultComboDataSourceHandler sppGroupComboDSHandler()
    {
        return new GroupComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler placePurposeComboDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UniplacesClassroomType.class).filter(UniplacesClassroomType.title()).order(UniplacesClassroomType.title());
    }


    @Bean
    public IDefaultComboDataSourceHandler subjectComboDSHandler()
    {
        return new SppScheduleSessionSubjectComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler lectureRoomComboDSHandler()
    {
        return new SppScheduleSessionLectureRoomComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler ppsComboDSHandler()
    {
        return new SppScheduleSessionTeacherComboDSHandler(getName());
    }
}
