/* $Id: */
package ru.tandemservice.unispp.base.vo;

import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement;

/**
 * @author vnekrasov
 * @since 6/24/14
 */
public class SppTeacherPreferencePeriodVO
{
    private SppTeacherPreferenceElement _period;
    private SppTeacherPreferenceWeekRowVO _weekPeriodsRowVO;

    private UniplacesPlace _lectureRoomVal;
    private UniplacesBuilding _buildingVal;
    private boolean _unwantedTime;

    public SppTeacherPreferencePeriodVO(SppTeacherPreferenceElement period)
    {
        _period = period;
        _lectureRoomVal = period.getLectureRoom();
        _buildingVal = period.getBuilding();
        _unwantedTime = period.isUnwantedTime();
    }

    public SppTeacherPreferenceElement getPeriod()
    {
        return _period;
    }

    public void setPeriod(SppTeacherPreferenceElement period)
    {
        _period = period;
    }


    public SppTeacherPreferenceWeekRowVO getWeekPeriodsRowVO()
    {
        return _weekPeriodsRowVO;
    }

    public void setWeekPeriodsRowVO(SppTeacherPreferenceWeekRowVO weekPeriodsRowVO)
    {
        _weekPeriodsRowVO = weekPeriodsRowVO;
    }

    public UniplacesPlace getLectureRoomVal()
    {
        return _lectureRoomVal;
    }

    public void setLectureRoomVal(UniplacesPlace lectureRoomVal)
    {
        _lectureRoomVal = lectureRoomVal;
    }

    public UniplacesBuilding getBuildingVal()
    {
        return _buildingVal;
    }

    public void setBuildingVal(UniplacesBuilding buildingVal)
    {
        _buildingVal = buildingVal;
    }

    public boolean isUnwantedTime()
    {
        return _unwantedTime;
    }

    public void setUnwantedTime(boolean unwantedTime)
    {
        _unwantedTime = unwantedTime;
    }

    public SppTeacherPreferenceElement getPeriodForSave()
    {
        _period.setWeekRow(_weekPeriodsRowVO.getWeekRow());

        _period.setLectureRoom(getLectureRoomVal());
        _period.setBuilding(getBuildingVal());
        _period.setUnwantedTime(isUnwantedTime());

        return _period;
    }
}
