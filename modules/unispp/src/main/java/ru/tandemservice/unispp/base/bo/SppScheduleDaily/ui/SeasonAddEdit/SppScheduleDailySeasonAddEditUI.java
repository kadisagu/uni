/* $Id: SppScheduleDailySeasonAddEditUI.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.SeasonAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@Input({
        @Bind(key= UIPresenter.PUBLISHER_ID, binding="seasonId")
})
public class SppScheduleDailySeasonAddEditUI extends UIPresenter
{
    private Long _seasonId;
    private SppScheduleDailySeason _season;

    public Long getSeasonId()
    {
        return _seasonId;
    }

    public void setSeasonId(Long seasonId)
    {
        _seasonId = seasonId;
    }

    public SppScheduleDailySeason getSeason()
    {
        return _season;
    }

    public void setSeason(SppScheduleDailySeason season)
    {
        _season = season;
    }

    public Boolean getAddForm()
    {
        return null == _seasonId;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null != _seasonId && null == _season)
            _season = DataAccessServices.dao().getNotNull(_seasonId);
        else
            _season = new SppScheduleDailySeason();
    }

    public void onClickApply()
    {
        if(_season.getStartDate().after(_season.getEndDate()))
            _uiSupport.error("Поле «Дата начала» не может быть позже поля «Дата окончания»", "startDate", "endDate");
        if(getUserContext().getErrorCollector().hasErrors())
            return;
        SppScheduleDailyManager.instance().dao().createOrUpdateSeason(_season);
        deactivate();
    }
}
