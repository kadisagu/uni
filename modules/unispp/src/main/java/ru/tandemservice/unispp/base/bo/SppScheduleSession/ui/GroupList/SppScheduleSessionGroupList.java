/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.GroupList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.View.SppScheduleSessionView;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;

/**
 * @author vnekrasov
 * @since 12/26/13
 */
@Configuration
public class SppScheduleSessionGroupList extends BusinessComponentManager
{
    public final static String SCHEDULE_DS = "scheduleDS";

    @Bean
    public ColumnListExtPoint scheduleCL()
    {
        return columnListExtPointBuilder(SCHEDULE_DS)
                .addColumn(publisherColumn("title", SppScheduleSession.title()).businessComponent(SppScheduleSessionView.class).order().required(true))
                .addColumn(dateColumn("startDate", SppScheduleSession.season().startDate()).order())
                .addColumn(dateColumn("endDate", SppScheduleSession.season().endDate()).order())
                .addColumn(booleanColumn("approved", SppScheduleSession.approved()))
                .addColumn(booleanColumn("archived", SppScheduleSession.archived()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled("ui:entityEditDisabled").permissionKey("sppScheduleSessionGroupListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).disabled("ui:entityDeleteDisabled").alert(new FormattedMessage("ui.deleteAlert", SppScheduleSession.title())).permissionKey("sppScheduleSessionGroupListDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SCHEDULE_DS, scheduleCL(), SppScheduleSessionManager.instance().sppScheduleSessionDSHandler()))
                .create();
    }
}
