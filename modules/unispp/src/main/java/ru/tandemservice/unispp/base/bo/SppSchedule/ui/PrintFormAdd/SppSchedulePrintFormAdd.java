/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.PrintFormAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppSchedulesComboDSHandler;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;

/**
 * @author nvankov
 * @since 9/25/13
 */
@Configuration
public class SppSchedulePrintFormAdd extends BusinessComponentManager
{
    public static final String FORMATIVE_OU_DS = "formativeOrgUnitDS";
    public static final String EDU_LEVEL_DS = "eduLevelDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String COURSE_DS = "courseDS";
    public static final String SEASON_DS = "seasonDS";
    public static final String BELLS_DS = "bellsDS";
    public static final String SCHEDULES_DS = "schedulesDS";
    public static final String TERM_DS = "termDS";
    public static final String EDUCATION_YEAR = "educationYearDS";
    public static final String EMPLOYEE_POST_DS = "employeePostDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(FORMATIVE_OU_DS, SppScheduleManager.instance().sppGroupComboDSHandler()))
                .addDataSource(selectDS(EDU_LEVEL_DS, SppScheduleManager.instance().sppGroupComboDSHandler()).addColumn("title", EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, SppScheduleManager.instance().sppGroupComboDSHandler()))
                .addDataSource(selectDS(COURSE_DS, SppScheduleManager.instance().sppGroupComboDSHandler()))
                .addDataSource(selectDS(SEASON_DS, schedulesComboDSHandler()).addColumn("title", null, new IFormatter<SppScheduleSeason>()
                {
                    @Override
                    public String format(SppScheduleSeason source)
                    {
                        return source.getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getStartDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndDate()) + ")";
                    }
                }))
                .addDataSource(selectDS(BELLS_DS, schedulesComboDSHandler()))
                .addDataSource(selectDS(SCHEDULES_DS, schedulesComboDSHandler()).addColumn("group", SppSchedule.group().title().s()).addColumn("schedule", SppSchedule.title().s()))
                .addDataSource(selectDS(TERM_DS, SppScheduleManager.instance().termComboDSHandler()))
                .addDataSource(selectDS(EDUCATION_YEAR, educationYearComboDSHandler()))
//                .addDataSource(selectDS(ADMIN_DS, SppScheduleManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s()))
                .addDataSource(selectDS(EMPLOYEE_POST_DS, SppScheduleManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler educationYearComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EducationYear.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler schedulesComboDSHandler()
    {
        return new SppSchedulesComboDSHandler(getName());
    }
}
