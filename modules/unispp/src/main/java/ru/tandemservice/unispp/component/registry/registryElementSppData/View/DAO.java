/* $Id:$ */
package ru.tandemservice.unispp.component.registry.registryElementSppData.View;

import org.hibernate.Session;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList;
import ru.tandemservice.unispp.base.entity.SppRegElementExt;
import ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck;
import ru.tandemservice.unispp.base.entity.gen.SppRegElementExtGen;
import ru.tandemservice.unispp.base.entity.gen.SppRegElementLoadCheckGen;

import java.util.*;

/**
 * @author Victor Nekrasov
 * @since 20.05.2014
 */
public class DAO<T extends EppRegistryElement> extends UniBaseDao implements IDAO<T>
{
    @Override
    public void prepare(final Model<T> model)
    {
        //final IEntity entity = (null == model.getId() ? null : this.get(model.getId()));
        //final Long eppRegElementId = model.getEppRegElementId();
        model.setElement(this.get(EppRegistryElement.class, model.getEppRegElementId()));
        model.setLoadTypeMap(EppLoadTypeUtils.getLoadTypeMap());
        final Long elementId = model.getElement().getId();
        if (null == elementId)
        {
            model.setLoadMap(new HashMap<>());
        }
        else
        {
            final Map<Long, Map<String, SppRegElementLoadCheck>> disciplineLoadMap = getRegistryElementTotalLoadMap(Collections.singleton(elementId));
            List<SppRegElementExt> extList = getSppRegElementExt(elementId);
            model.setSppRegElementExt(extList.size() == 1 ? extList.get(0) : createSppRegElementExt(model));
            model.setLoadMap(disciplineLoadMap.get(elementId));
        }

        StaticListDataSource<SppDisciplinePreferenceList> dataSource = new StaticListDataSource<>();
        dataSource.addColumn(new SimpleColumn("Преподаватель", SppDisciplinePreferenceList.teacher().fullTitle().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Здание", SppDisciplinePreferenceList.building().fullNumber().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Аудитория", SppDisciplinePreferenceList.lectureRoom().displayableTitle().s()).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить предпочтение из списка?"));

        dataSource.setupRows(getList(SppDisciplinePreferenceList.class, SppDisciplinePreferenceList.discipline().s(), model.getElement(), SppDisciplinePreferenceList.teacher().s()));

        model.setDataSource(dataSource);
    }

    public SppRegElementExt createSppRegElementExt(Model model)
    {
        SppRegElementExt sppRegElementExt = new SppRegElementExt();
        sppRegElementExt.setRegistryElement(model.getElement());
        return sppRegElementExt;
    }

    public List<SppRegElementExt> getSppRegElementExt(Long elementId)
    {
        final MQBuilder builder = new MQBuilder(SppRegElementExtGen.ENTITY_CLASS, "ext");
        if (null != elementId)
        {
            builder.add(MQExpression.in("ext", SppRegElementExtGen.registryElement().id(), elementId));
        }

        return builder.getResultList(getSession());
    }

    public Map<Long, Map<String, SppRegElementLoadCheck>> getRegistryElementTotalLoadMap(final Collection<Long> disciplineIds)
    {
        final Map<Long, Map<String, SppRegElementLoadCheck>> result = SafeMap.get(HashMap.class);
        BatchUtils.executeNullable(disciplineIds, 128, ids -> {
            final MQBuilder builder = new MQBuilder(SppRegElementLoadCheckGen.ENTITY_CLASS, "load");
            if (null != ids)
            {
                builder.add(MQExpression.in("load", SppRegElementLoadCheckGen.registryElement().id(), ids));
            }
            for (final SppRegElementLoadCheck load : builder.<SppRegElementLoadCheck>getResultList(getSession()))
            {
                result.get(load.getRegistryElement().getId()).put(load.getEppALoadType().getFullCode(), load);
            }
        });
        return result;
    }

    @Override
    public void save(final Model<T> model)
    {
        //IEppRegistryDAO.instance.get().doSaveRegistryElement(model.getElement());

        final Long elementId = model.getElement().getId();
        for (final EppALoadType loadType : this.getList(EppALoadType.class))
        {
            final SppRegElementLoadCheck load = model.getLoadMap().get(loadType.getFullCode());
            if (null != load)
            {
                load.setEppALoadType(loadType);
                load.setRegistryElement(model.getElement());
            }
        }

        final Map<Long, Map<String, SppRegElementLoadCheck>> disciplineLoadMap = getRegistryElementTotalLoadMap(Collections.singleton(elementId));
        disciplineLoadMap.put(elementId, model.getLoadMap());
        saveRegistryElementTotalLoadMap(disciplineLoadMap);
        saveRegistryElExt(model.getSppRegElementExt());
    }

    public void saveRegistryElExt(SppRegElementExt newExt)
    {
        final Session session = this.lock("SppRegElExt");
        session.saveOrUpdate(newExt);
        session.flush();
    }

    public void saveRegistryElementTotalLoadMap(final Map<Long, Map<String, SppRegElementLoadCheck>> newMap)
    {
        this.validateSppRegElementLoadCheckMap(newMap);
        final Session session = this.lock("SppRegElementLoadCheckMap");

        final Map<Long, Map<String, SppRegElementLoadCheck>> currentMap = this.getRegistryElementTotalLoadMap(newMap.keySet());

        // обновляем существующие
        for (final Map.Entry<Long, Map<String, SppRegElementLoadCheck>> currentRegElEntry : currentMap.entrySet())
        {
            final Map<String, SppRegElementLoadCheck> currentRegElMap = currentRegElEntry.getValue();
            final Map<String, SppRegElementLoadCheck> newRegElMap = newMap.remove(currentRegElEntry.getKey());

            for (final Map.Entry<String, SppRegElementLoadCheck> newRegElLoadCheckEntry : newRegElMap.entrySet())
            {
                final SppRegElementLoadCheck newLoad = newRegElLoadCheckEntry.getValue();
                if (null == newLoad)
                {
                    continue; /* no-value */
                }

                SppRegElementLoadCheck dbLoad = currentRegElMap.remove(newRegElLoadCheckEntry.getKey());
                if (null == dbLoad)
                {
                    dbLoad = new SppRegElementLoadCheck();
                }
                dbLoad.update(newLoad);
                session.saveOrUpdate(dbLoad);
            }

            for (final Map.Entry<String, SppRegElementLoadCheck> currentRegElLoadCheckEntry : currentRegElMap.entrySet())
            {
                session.delete(currentRegElLoadCheckEntry.getValue());
            }
        }

        // все удаления должны попасть в базу ДО добавлений
        session.flush();

        // создаем новые
        for (final Map.Entry<Long, Map<String, SppRegElementLoadCheck>> newRegElEntry : newMap.entrySet())
        {
            final Map<String, SppRegElementLoadCheck> newRegElMap = newRegElEntry.getValue();
            for (final Map.Entry<String, SppRegElementLoadCheck> newRegElLoadEntry : newRegElMap.entrySet())
            {

                final SppRegElementLoadCheck load = new SppRegElementLoadCheck();
                load.update(newRegElLoadEntry.getValue());
                session.saveOrUpdate(load);
            }
        }

        session.flush();
    }

    protected void validateSppRegElementLoadCheckMap(final Map<Long, Map<String, SppRegElementLoadCheck>> loadMap)
    {
        final Session session = this.getSession();
        final List<EppALoadType> loadTypes = this.getList(EppALoadType.class);

        for (final Map.Entry<Long, Map<String, SppRegElementLoadCheck>> regElEntry : loadMap.entrySet())
        {
            final EppRegistryElement regEl = (EppRegistryElement) session.load(EppRegistryElement.class, regElEntry.getKey());
            for (final EppALoadType loadType : loadTypes)
            {
                final SppRegElementLoadCheck load = regElEntry.getValue().get(loadType.getFullCode());
                if ((null != load) && (null == load.getId()))
                {
                    load.setEppALoadType(loadType);
                    load.setRegistryElement(regEl);
                }
            }

            for (final Map.Entry<String, SppRegElementLoadCheck> regElLoadEntry : regElEntry.getValue().entrySet())
            {
                final SppRegElementLoadCheck newLoad = regElLoadEntry.getValue();
                if (null == newLoad)
                {
                    continue; /* no-value */
                }

                final String code = newLoad.getEppALoadType().getFullCode();
                if (!regElLoadEntry.getKey().equals(code))
                {
                    throw new IllegalArgumentException("SppRegElementLoadCheck(" + newLoad.getId() + ").load(" + code + ") != " + regElLoadEntry.getKey());
                }

                final Long regElId = newLoad.getRegistryElement().getId();
                if (!regElEntry.getKey().equals(regElId))
                {
                    throw new IllegalArgumentException("SppRegElementLoadCheck(" + newLoad.getId() + ").dsc(" + regElId + ") != " + regElEntry.getKey());
                }
            }
        }
    }
}
