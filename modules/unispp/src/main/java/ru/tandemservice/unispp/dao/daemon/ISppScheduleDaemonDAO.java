/* $Id$ */
package ru.tandemservice.unispp.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

/**
 * @author nvankov
 * @since 9/10/13
 */
public interface ISppScheduleDaemonDAO
{
    final String GLOBAL_DAEMON_LOCK = ISppScheduleDaemonDAO.class.getName() + ".global-lock";
    final SpringBeanCache<ISppScheduleDaemonDAO> instance = new SpringBeanCache<>(ISppScheduleDaemonDAO.class.getName());

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doArchiveSchedules();
}
