/* $Id:$ */
package ru.tandemservice.unispp.component.registry.registryElementSppData.View;

import ru.tandemservice.uni.dao.IPrepareable;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author Victor Nekrasov
 * @since 20.05.2014
 */

public interface IDAO<T extends EppRegistryElement> extends IPrepareable<Model<T>>
{
    void save(Model<T> model);
}


