package ru.tandemservice.unispp.base.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unispp.base.entity.gen.*;

/**
 * Печатная форма подневного расписания
 */
public class SppScheduleDailyPrintForm extends SppScheduleDailyPrintFormGen
{
    @EntityDSLSupport
    public String getFormWithTerrTitle()
    {
        return getFormativeOrgUnit().getTitle() +
                (getTerritorialOrgUnit() != null ? (" (" + getTerritorialOrgUnit().getTerritorialTitle() + ")") : "");
    }

}