/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleGroupComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.View.SppScheduleView;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.catalog.SppScheduleStatus;
import ru.tandemservice.unispp.base.entity.catalog.codes.SppScheduleStatusCodes;

/**
 * @author nvankov
 * @since 9/4/13
 */
@Input({
        @Bind(key = "groupId", binding = "groupId"),
        @Bind(key = "orgUnitId", binding = "orgUnitId"),
        @Bind(key = "scheduleId", binding = "scheduleId")
})
public class SppScheduleAddEditUI extends UIPresenter
{
    private Long _groupId;
    private Long _orgUnitId;
    private Long _scheduleId;
    private SppSchedule _schedule;
    private ScheduleBell _oldBells;
    private boolean _oldDiffEven;

    public Long getGroupId()
    {
        return _groupId;
    }

    public void setGroupId(Long groupId)
    {
        _groupId = groupId;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Long getScheduleId()
    {
        return _scheduleId;
    }

    public void setScheduleId(Long scheduleId)
    {
        _scheduleId = scheduleId;
    }

    public SppSchedule getSchedule()
    {
        return _schedule;
    }

    public void setSchedule(SppSchedule schedule)
    {
        _schedule = schedule;
    }

    public Boolean getGroupDisabled()
    {
        return null != _groupId || null != _scheduleId;
    }

    public Boolean getAddForm()
    {
        return null == _scheduleId;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _schedule)
        {
            if (null != _scheduleId)
            {
                _schedule = DataAccessServices.dao().get(_scheduleId);
                _oldBells = _schedule.getBells();
                _oldDiffEven = _schedule.isDiffEven();
            } else
            {
                _schedule = new SppSchedule();
                if (null != _groupId)
                {
                    Group group = DataAccessServices.dao().get(_groupId);
                    _schedule.setGroup(group);
                }
                _schedule.setStatus(DataAccessServices.dao().getNotNull(SppScheduleStatus.class, SppScheduleStatus.code(), SppScheduleStatusCodes.FORMATION));
                _schedule.setApproved(false);
            }
            _orgUnitId = _orgUnitId != null ? _orgUnitId : _schedule.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getId();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleAddEdit.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleGroupComboDSHandler.PROP_ORG_UNIT_ID, _orgUnitId);
            if (getAddForm())
                dataSource.put(SppScheduleGroupComboDSHandler.PROP_GROUPS_NOT_IN_ARCHIVE, Boolean.TRUE);
        }
    }

    public void onClickApply()
    {
        SppScheduleManager.instance().dao().saveOrUpdateSchedule(_schedule, _oldBells, _oldDiffEven);
        if (getAddForm())
        {
            deactivate();
            _uiActivation.asDesktopRoot(SppScheduleView.class).parameter(PUBLISHER_ID, _schedule.getId()).parameter("orgUnitId", _orgUnitId).activate();
        } else deactivate();
    }
}
