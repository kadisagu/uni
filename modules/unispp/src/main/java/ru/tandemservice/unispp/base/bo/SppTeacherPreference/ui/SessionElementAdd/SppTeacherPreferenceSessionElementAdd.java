/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.SessionElementAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;

/**
 * @author vnekrasov
 * @since 7/3/14
 */
@Configuration
public class SppTeacherPreferenceSessionElementAdd extends BusinessComponentManager
{
    public static final String LECTURE_ROOM_DS = "lectureRoomDS";
    public static final String BUILDING_DS = "buildingDS";
    public static final String DAY_OF_WEEK_DS = "dayOfWeekDS";
    public static final String BELL_ENTRY_DS = "bellEntryDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(LECTURE_ROOM_DS, SppTeacherPreferenceManager.instance().lectureRoomComboDSHandler()).addColumn("title", UniplacesPlace.title().s()).addColumn("location", UniplacesPlace.fullLocationInfo().s()))
                .addDataSource(selectDS(BUILDING_DS, SppTeacherPreferenceManager.instance().buildingComboDSHandler()).addColumn("title", UniplacesBuilding.title().s()).addColumn("location", UniplacesBuilding.fullNumber().s()))
                .addDataSource(selectDS(DAY_OF_WEEK_DS, SppTeacherPreferenceManager.instance().dayOfWeekComboDSHandler()))
                .addDataSource(selectDS(BELL_ENTRY_DS, SppTeacherPreferenceManager.instance().bellEntryComboDSHandler()).addColumn("title", ScheduleBellEntry.title().s()))
                .create();
    }
}
