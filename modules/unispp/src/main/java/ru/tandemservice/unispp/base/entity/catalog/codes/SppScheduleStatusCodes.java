package ru.tandemservice.unispp.base.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Статус расписания"
 * Имя сущности : sppScheduleStatus
 * Файл data.xml : spp-catalog.data.xml
 */
public interface SppScheduleStatusCodes
{
    /** Константа кода (code) элемента : formation (code). Название (title) : На формировании */
    String FORMATION = "formation";
    /** Константа кода (code) элемента : approved (code). Название (title) : Утверждено */
    String APPROVED = "approved";

    Set<String> CODES = ImmutableSet.of(FORMATION, APPROVED);
}
