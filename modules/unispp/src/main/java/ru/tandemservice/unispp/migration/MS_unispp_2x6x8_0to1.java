package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unispp_2x6x8_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Long bellScheduleId = getId(tool, "select id from spp_bell_schedule order by active_p desc, code_p asc");
        Long bellScheduleEntryId = bellScheduleId == null ? null : getId(tool, "select id from spp_bell_schedule_entry where schedule_id = '" + bellScheduleId + "' order by number_p asc");

        if (null == bellScheduleId)
        {
            tool.executeUpdate("delete from sppScheduleDailyWarningLog_t");
            tool.executeUpdate("delete from sppscheduledailyevent_t");
            tool.executeUpdate("delete from sppscheduledaily_t");

            tool.executeUpdate("delete from sppschedulesessionwarninglog_t");
            tool.executeUpdate("delete from sppschedulesessionevent_t");
            tool.executeUpdate("delete from sppschedulesession_t");
        }
        else if (null == bellScheduleEntryId)
        {
            tool.executeUpdate("delete from sppscheduledailyevent_t");
            tool.executeUpdate("delete from sppschedulesessionevent_t");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sppScheduleDaily

        // создано обязательное свойство bells
        {
            // создать колонку
            tool.createColumn("sppscheduledaily_t", new DBColumn("bells_id", DBType.LONG));
            // задать значение по умолчанию
            tool.executeUpdate("update sppscheduledaily_t set bells_id=? where bells_id is null", bellScheduleId);
            // сделать колонку NOT NULL
            tool.setColumnNullable("sppscheduledaily_t", "bells_id", false);
        }

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleDailyEvent

        // изменен тип свойства date
        {
            // изменить тип колонки
            tool.changeColumnType("sppscheduledailyevent_t", "date_p", DBType.DATE);
        }
		// создано обязательное свойство bell
		{
			// создать колонку
			tool.createColumn("sppscheduledailyevent_t", new DBColumn("bell_id", DBType.LONG));
			// задать значение по умолчанию
			tool.executeUpdate("update sppscheduledailyevent_t set bell_id=? where bell_id is null", bellScheduleEntryId);
			// сделать колонку NOT NULL
			tool.setColumnNullable("sppscheduledailyevent_t", "bell_id", false);
		}

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sppScheduleSession

        // создано обязательное свойство bells
        {
            // создать колонку
            tool.createColumn("sppschedulesession_t", new DBColumn("bells_id", DBType.LONG));
            // задать значение по умолчанию
            tool.executeUpdate("update sppschedulesession_t set bells_id=? where bells_id is null", bellScheduleId);
            // сделать колонку NOT NULL
            tool.setColumnNullable("sppschedulesession_t", "bells_id", false);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sppScheduleSessionEvent

        // изменен тип свойства date
        {
            // изменить тип колонки
            tool.changeColumnType("sppschedulesessionevent_t", "date_p", DBType.DATE);
        }
		// создано обязательное свойство bell
		{
			// создать колонку
			tool.createColumn("sppschedulesessionevent_t", new DBColumn("bell_id", DBType.LONG));
			// задать значение по умолчанию
			tool.executeUpdate("update sppschedulesessionevent_t set bell_id=? where bell_id is null", bellScheduleEntryId);
			// сделать колонку NOT NULL
			tool.setColumnNullable("sppschedulesessionevent_t", "bell_id", false);
		}
    }

    private Long getId(DBTool tool, String sql) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();
        ResultSet src = stmt.executeQuery(sql);
        while (src.next())
        {
            return src.getLong(1);
        }
        return null;
    }
}