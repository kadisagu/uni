package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;
import ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список предпочтений преподавателя по подневному расписанию
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppTeacherDailyPreferenceListGen extends EntityBase
 implements INaturalIdentifiable<SppTeacherDailyPreferenceListGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList";
    public static final String ENTITY_NAME = "sppTeacherDailyPreferenceList";
    public static final int VERSION_HASH = 667943885;
    private static IEntityMeta ENTITY_META;

    public static final String L_TEACHER = "teacher";
    public static final String L_SPP_SCHEDULE_DAILY_SEASON = "sppScheduleDailySeason";
    public static final String P_TITLE = "title";
    public static final String L_BELLS = "bells";

    private Person _teacher;     // Преподаватель
    private SppScheduleDailySeason _sppScheduleDailySeason;     // Период подневного расписания
    private String _title;     // Название
    private ScheduleBell _bells;     // Звонковое расписание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Преподаватель. Свойство не может быть null.
     */
    @NotNull
    public Person getTeacher()
    {
        return _teacher;
    }

    /**
     * @param teacher Преподаватель. Свойство не может быть null.
     */
    public void setTeacher(Person teacher)
    {
        dirty(_teacher, teacher);
        _teacher = teacher;
    }

    /**
     * @return Период подневного расписания. Свойство не может быть null.
     */
    @NotNull
    public SppScheduleDailySeason getSppScheduleDailySeason()
    {
        return _sppScheduleDailySeason;
    }

    /**
     * @param sppScheduleDailySeason Период подневного расписания. Свойство не может быть null.
     */
    public void setSppScheduleDailySeason(SppScheduleDailySeason sppScheduleDailySeason)
    {
        dirty(_sppScheduleDailySeason, sppScheduleDailySeason);
        _sppScheduleDailySeason = sppScheduleDailySeason;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     */
    @NotNull
    public ScheduleBell getBells()
    {
        return _bells;
    }

    /**
     * @param bells Звонковое расписание. Свойство не может быть null.
     */
    public void setBells(ScheduleBell bells)
    {
        dirty(_bells, bells);
        _bells = bells;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppTeacherDailyPreferenceListGen)
        {
            if (withNaturalIdProperties)
            {
                setTeacher(((SppTeacherDailyPreferenceList)another).getTeacher());
                setSppScheduleDailySeason(((SppTeacherDailyPreferenceList)another).getSppScheduleDailySeason());
            }
            setTitle(((SppTeacherDailyPreferenceList)another).getTitle());
            setBells(((SppTeacherDailyPreferenceList)another).getBells());
        }
    }

    public INaturalId<SppTeacherDailyPreferenceListGen> getNaturalId()
    {
        return new NaturalId(getTeacher(), getSppScheduleDailySeason());
    }

    public static class NaturalId extends NaturalIdBase<SppTeacherDailyPreferenceListGen>
    {
        private static final String PROXY_NAME = "SppTeacherDailyPreferenceListNaturalProxy";

        private Long _teacher;
        private Long _sppScheduleDailySeason;

        public NaturalId()
        {}

        public NaturalId(Person teacher, SppScheduleDailySeason sppScheduleDailySeason)
        {
            _teacher = ((IEntity) teacher).getId();
            _sppScheduleDailySeason = ((IEntity) sppScheduleDailySeason).getId();
        }

        public Long getTeacher()
        {
            return _teacher;
        }

        public void setTeacher(Long teacher)
        {
            _teacher = teacher;
        }

        public Long getSppScheduleDailySeason()
        {
            return _sppScheduleDailySeason;
        }

        public void setSppScheduleDailySeason(Long sppScheduleDailySeason)
        {
            _sppScheduleDailySeason = sppScheduleDailySeason;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SppTeacherDailyPreferenceListGen.NaturalId) ) return false;

            SppTeacherDailyPreferenceListGen.NaturalId that = (NaturalId) o;

            if( !equals(getTeacher(), that.getTeacher()) ) return false;
            if( !equals(getSppScheduleDailySeason(), that.getSppScheduleDailySeason()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getTeacher());
            result = hashCode(result, getSppScheduleDailySeason());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getTeacher());
            sb.append("/");
            sb.append(getSppScheduleDailySeason());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppTeacherDailyPreferenceListGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppTeacherDailyPreferenceList.class;
        }

        public T newInstance()
        {
            return (T) new SppTeacherDailyPreferenceList();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "teacher":
                    return obj.getTeacher();
                case "sppScheduleDailySeason":
                    return obj.getSppScheduleDailySeason();
                case "title":
                    return obj.getTitle();
                case "bells":
                    return obj.getBells();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "teacher":
                    obj.setTeacher((Person) value);
                    return;
                case "sppScheduleDailySeason":
                    obj.setSppScheduleDailySeason((SppScheduleDailySeason) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "bells":
                    obj.setBells((ScheduleBell) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "teacher":
                        return true;
                case "sppScheduleDailySeason":
                        return true;
                case "title":
                        return true;
                case "bells":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "teacher":
                    return true;
                case "sppScheduleDailySeason":
                    return true;
                case "title":
                    return true;
                case "bells":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "teacher":
                    return Person.class;
                case "sppScheduleDailySeason":
                    return SppScheduleDailySeason.class;
                case "title":
                    return String.class;
                case "bells":
                    return ScheduleBell.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppTeacherDailyPreferenceList> _dslPath = new Path<SppTeacherDailyPreferenceList>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppTeacherDailyPreferenceList");
    }
            

    /**
     * @return Преподаватель. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList#getTeacher()
     */
    public static Person.Path<Person> teacher()
    {
        return _dslPath.teacher();
    }

    /**
     * @return Период подневного расписания. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList#getSppScheduleDailySeason()
     */
    public static SppScheduleDailySeason.Path<SppScheduleDailySeason> sppScheduleDailySeason()
    {
        return _dslPath.sppScheduleDailySeason();
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList#getBells()
     */
    public static ScheduleBell.Path<ScheduleBell> bells()
    {
        return _dslPath.bells();
    }

    public static class Path<E extends SppTeacherDailyPreferenceList> extends EntityPath<E>
    {
        private Person.Path<Person> _teacher;
        private SppScheduleDailySeason.Path<SppScheduleDailySeason> _sppScheduleDailySeason;
        private PropertyPath<String> _title;
        private ScheduleBell.Path<ScheduleBell> _bells;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Преподаватель. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList#getTeacher()
     */
        public Person.Path<Person> teacher()
        {
            if(_teacher == null )
                _teacher = new Person.Path<Person>(L_TEACHER, this);
            return _teacher;
        }

    /**
     * @return Период подневного расписания. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList#getSppScheduleDailySeason()
     */
        public SppScheduleDailySeason.Path<SppScheduleDailySeason> sppScheduleDailySeason()
        {
            if(_sppScheduleDailySeason == null )
                _sppScheduleDailySeason = new SppScheduleDailySeason.Path<SppScheduleDailySeason>(L_SPP_SCHEDULE_DAILY_SEASON, this);
            return _sppScheduleDailySeason;
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(SppTeacherDailyPreferenceListGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList#getBells()
     */
        public ScheduleBell.Path<ScheduleBell> bells()
        {
            if(_bells == null )
                _bells = new ScheduleBell.Path<ScheduleBell>(L_BELLS, this);
            return _bells;
        }

        public Class getEntityClass()
        {
            return SppTeacherDailyPreferenceList.class;
        }

        public String getEntityName()
        {
            return "sppTeacherDailyPreferenceList";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
