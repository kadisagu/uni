/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.EventAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 12/26/13
 */
@Configuration
public class SppScheduleSessionEventAddEdit extends BusinessComponentManager
{
    public static final String BELL_DS = "bellDS";
    public static final String LECTURE_ROOM_DS = "lectureRoomDS";
    public static final String TEACHER_DS = "teacherDS";
    public static final String SUBJECT_DS = "subjectDS";
    public static final String LECTURE_ROOM_TYPE_DS = "lectureRoomTypeDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(BELL_DS, bellsEntryComboDSHandler()).addColumn("time", null, new IFormatter<ScheduleBellEntry>()
                {
                    @Override
                    public String format(ScheduleBellEntry source)
                    {
                        return source.getTime();
                    }
                }))
                .addDataSource(selectDS(LECTURE_ROOM_DS, SppScheduleSessionManager.instance().lectureRoomComboDSHandler())/*.addColumn("title", UniplacesPlace.title().s()).addColumn("location", UniplacesPlace.fullLocationInfo().s())*/)
                .addDataSource(selectDS(TEACHER_DS, SppScheduleSessionManager.instance().ppsComboDSHandler()).addColumn("fio", PpsEntry.titleFioInfoOrgUnit().s()))
                .addDataSource(selectDS(SUBJECT_DS, SppScheduleSessionManager.instance().subjectComboDSHandler()))
                .addDataSource(selectDS(LECTURE_ROOM_TYPE_DS, SppScheduleSessionManager.instance().placePurposeComboDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> bellsEntryComboDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), ScheduleBellEntry.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                Long bellsId = context.get(SppScheduleSessionEventAddEditUI.BELLS_ID);
                dql.where(eq(property(alias, ScheduleBellEntry.schedule().id()), value(bellsId)));
            }
        };
        return handler.order(ScheduleBellEntry.startTime());
    }
}
