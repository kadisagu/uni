/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.SeasonAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;

/**
 * @author nvankov
 * @since 9/3/13
 */
@Configuration
public class SppScheduleSeasonAddEdit extends BusinessComponentManager
{
    public static final String YEAR_PART_DS = "yearPartDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEAR_PART_DS, getName(), EppYearPart.defaultSelectDSHandler(getName())))
                .create();
    }
}
