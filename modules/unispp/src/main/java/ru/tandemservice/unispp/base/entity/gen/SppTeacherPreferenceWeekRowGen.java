package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceWeekRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Недельная строка предпочтения преподавателя по расписанию
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppTeacherPreferenceWeekRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppTeacherPreferenceWeekRow";
    public static final String ENTITY_NAME = "sppTeacherPreferenceWeekRow";
    public static final int VERSION_HASH = 1730145505;
    private static IEntityMeta ENTITY_META;

    public static final String L_TEACHER_PREFERENCE = "teacherPreference";
    public static final String L_BELL = "bell";
    public static final String P_EVEN = "even";

    private SppTeacherPreferenceList _teacherPreference;     // Список предпочтений преподавателя
    private ScheduleBellEntry _bell;     // Пара
    private boolean _even;     // Четная неделя

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Список предпочтений преподавателя. Свойство не может быть null.
     */
    @NotNull
    public SppTeacherPreferenceList getTeacherPreference()
    {
        return _teacherPreference;
    }

    /**
     * @param teacherPreference Список предпочтений преподавателя. Свойство не может быть null.
     */
    public void setTeacherPreference(SppTeacherPreferenceList teacherPreference)
    {
        dirty(_teacherPreference, teacherPreference);
        _teacherPreference = teacherPreference;
    }

    /**
     * @return Пара. Свойство не может быть null.
     */
    @NotNull
    public ScheduleBellEntry getBell()
    {
        return _bell;
    }

    /**
     * @param bell Пара. Свойство не может быть null.
     */
    public void setBell(ScheduleBellEntry bell)
    {
        dirty(_bell, bell);
        _bell = bell;
    }

    /**
     * @return Четная неделя. Свойство не может быть null.
     */
    @NotNull
    public boolean isEven()
    {
        return _even;
    }

    /**
     * @param even Четная неделя. Свойство не может быть null.
     */
    public void setEven(boolean even)
    {
        dirty(_even, even);
        _even = even;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppTeacherPreferenceWeekRowGen)
        {
            setTeacherPreference(((SppTeacherPreferenceWeekRow)another).getTeacherPreference());
            setBell(((SppTeacherPreferenceWeekRow)another).getBell());
            setEven(((SppTeacherPreferenceWeekRow)another).isEven());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppTeacherPreferenceWeekRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppTeacherPreferenceWeekRow.class;
        }

        public T newInstance()
        {
            return (T) new SppTeacherPreferenceWeekRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "teacherPreference":
                    return obj.getTeacherPreference();
                case "bell":
                    return obj.getBell();
                case "even":
                    return obj.isEven();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "teacherPreference":
                    obj.setTeacherPreference((SppTeacherPreferenceList) value);
                    return;
                case "bell":
                    obj.setBell((ScheduleBellEntry) value);
                    return;
                case "even":
                    obj.setEven((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "teacherPreference":
                        return true;
                case "bell":
                        return true;
                case "even":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "teacherPreference":
                    return true;
                case "bell":
                    return true;
                case "even":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "teacherPreference":
                    return SppTeacherPreferenceList.class;
                case "bell":
                    return ScheduleBellEntry.class;
                case "even":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppTeacherPreferenceWeekRow> _dslPath = new Path<SppTeacherPreferenceWeekRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppTeacherPreferenceWeekRow");
    }
            

    /**
     * @return Список предпочтений преподавателя. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceWeekRow#getTeacherPreference()
     */
    public static SppTeacherPreferenceList.Path<SppTeacherPreferenceList> teacherPreference()
    {
        return _dslPath.teacherPreference();
    }

    /**
     * @return Пара. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceWeekRow#getBell()
     */
    public static ScheduleBellEntry.Path<ScheduleBellEntry> bell()
    {
        return _dslPath.bell();
    }

    /**
     * @return Четная неделя. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceWeekRow#isEven()
     */
    public static PropertyPath<Boolean> even()
    {
        return _dslPath.even();
    }

    public static class Path<E extends SppTeacherPreferenceWeekRow> extends EntityPath<E>
    {
        private SppTeacherPreferenceList.Path<SppTeacherPreferenceList> _teacherPreference;
        private ScheduleBellEntry.Path<ScheduleBellEntry> _bell;
        private PropertyPath<Boolean> _even;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Список предпочтений преподавателя. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceWeekRow#getTeacherPreference()
     */
        public SppTeacherPreferenceList.Path<SppTeacherPreferenceList> teacherPreference()
        {
            if(_teacherPreference == null )
                _teacherPreference = new SppTeacherPreferenceList.Path<SppTeacherPreferenceList>(L_TEACHER_PREFERENCE, this);
            return _teacherPreference;
        }

    /**
     * @return Пара. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceWeekRow#getBell()
     */
        public ScheduleBellEntry.Path<ScheduleBellEntry> bell()
        {
            if(_bell == null )
                _bell = new ScheduleBellEntry.Path<ScheduleBellEntry>(L_BELL, this);
            return _bell;
        }

    /**
     * @return Четная неделя. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceWeekRow#isEven()
     */
        public PropertyPath<Boolean> even()
        {
            if(_even == null )
                _even = new PropertyPath<Boolean>(SppTeacherPreferenceWeekRowGen.P_EVEN, this);
            return _even;
        }

        public Class getEntityClass()
        {
            return SppTeacherPreferenceWeekRow.class;
        }

        public String getEntityName()
        {
            return "sppTeacherPreferenceWeekRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
