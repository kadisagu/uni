package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список предпочтений преподавателя по расписанию сессии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppTeacherSessionPreferenceListGen extends EntityBase
 implements INaturalIdentifiable<SppTeacherSessionPreferenceListGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList";
    public static final String ENTITY_NAME = "sppTeacherSessionPreferenceList";
    public static final int VERSION_HASH = -447381232;
    private static IEntityMeta ENTITY_META;

    public static final String L_TEACHER = "teacher";
    public static final String L_SPP_SCHEDULE_SESSION_SEASON = "sppScheduleSessionSeason";
    public static final String P_TITLE = "title";
    public static final String L_BELLS = "bells";

    private Person _teacher;     // Преподаватель
    private SppScheduleSessionSeason _sppScheduleSessionSeason;     // Период расписания сессии
    private String _title;     // Название
    private ScheduleBell _bells;     // Звонковое расписание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Преподаватель. Свойство не может быть null.
     */
    @NotNull
    public Person getTeacher()
    {
        return _teacher;
    }

    /**
     * @param teacher Преподаватель. Свойство не может быть null.
     */
    public void setTeacher(Person teacher)
    {
        dirty(_teacher, teacher);
        _teacher = teacher;
    }

    /**
     * @return Период расписания сессии. Свойство не может быть null.
     */
    @NotNull
    public SppScheduleSessionSeason getSppScheduleSessionSeason()
    {
        return _sppScheduleSessionSeason;
    }

    /**
     * @param sppScheduleSessionSeason Период расписания сессии. Свойство не может быть null.
     */
    public void setSppScheduleSessionSeason(SppScheduleSessionSeason sppScheduleSessionSeason)
    {
        dirty(_sppScheduleSessionSeason, sppScheduleSessionSeason);
        _sppScheduleSessionSeason = sppScheduleSessionSeason;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     */
    @NotNull
    public ScheduleBell getBells()
    {
        return _bells;
    }

    /**
     * @param bells Звонковое расписание. Свойство не может быть null.
     */
    public void setBells(ScheduleBell bells)
    {
        dirty(_bells, bells);
        _bells = bells;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppTeacherSessionPreferenceListGen)
        {
            if (withNaturalIdProperties)
            {
                setTeacher(((SppTeacherSessionPreferenceList)another).getTeacher());
                setSppScheduleSessionSeason(((SppTeacherSessionPreferenceList)another).getSppScheduleSessionSeason());
            }
            setTitle(((SppTeacherSessionPreferenceList)another).getTitle());
            setBells(((SppTeacherSessionPreferenceList)another).getBells());
        }
    }

    public INaturalId<SppTeacherSessionPreferenceListGen> getNaturalId()
    {
        return new NaturalId(getTeacher(), getSppScheduleSessionSeason());
    }

    public static class NaturalId extends NaturalIdBase<SppTeacherSessionPreferenceListGen>
    {
        private static final String PROXY_NAME = "SppTeacherSessionPreferenceListNaturalProxy";

        private Long _teacher;
        private Long _sppScheduleSessionSeason;

        public NaturalId()
        {}

        public NaturalId(Person teacher, SppScheduleSessionSeason sppScheduleSessionSeason)
        {
            _teacher = ((IEntity) teacher).getId();
            _sppScheduleSessionSeason = ((IEntity) sppScheduleSessionSeason).getId();
        }

        public Long getTeacher()
        {
            return _teacher;
        }

        public void setTeacher(Long teacher)
        {
            _teacher = teacher;
        }

        public Long getSppScheduleSessionSeason()
        {
            return _sppScheduleSessionSeason;
        }

        public void setSppScheduleSessionSeason(Long sppScheduleSessionSeason)
        {
            _sppScheduleSessionSeason = sppScheduleSessionSeason;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SppTeacherSessionPreferenceListGen.NaturalId) ) return false;

            SppTeacherSessionPreferenceListGen.NaturalId that = (NaturalId) o;

            if( !equals(getTeacher(), that.getTeacher()) ) return false;
            if( !equals(getSppScheduleSessionSeason(), that.getSppScheduleSessionSeason()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getTeacher());
            result = hashCode(result, getSppScheduleSessionSeason());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getTeacher());
            sb.append("/");
            sb.append(getSppScheduleSessionSeason());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppTeacherSessionPreferenceListGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppTeacherSessionPreferenceList.class;
        }

        public T newInstance()
        {
            return (T) new SppTeacherSessionPreferenceList();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "teacher":
                    return obj.getTeacher();
                case "sppScheduleSessionSeason":
                    return obj.getSppScheduleSessionSeason();
                case "title":
                    return obj.getTitle();
                case "bells":
                    return obj.getBells();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "teacher":
                    obj.setTeacher((Person) value);
                    return;
                case "sppScheduleSessionSeason":
                    obj.setSppScheduleSessionSeason((SppScheduleSessionSeason) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "bells":
                    obj.setBells((ScheduleBell) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "teacher":
                        return true;
                case "sppScheduleSessionSeason":
                        return true;
                case "title":
                        return true;
                case "bells":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "teacher":
                    return true;
                case "sppScheduleSessionSeason":
                    return true;
                case "title":
                    return true;
                case "bells":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "teacher":
                    return Person.class;
                case "sppScheduleSessionSeason":
                    return SppScheduleSessionSeason.class;
                case "title":
                    return String.class;
                case "bells":
                    return ScheduleBell.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppTeacherSessionPreferenceList> _dslPath = new Path<SppTeacherSessionPreferenceList>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppTeacherSessionPreferenceList");
    }
            

    /**
     * @return Преподаватель. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList#getTeacher()
     */
    public static Person.Path<Person> teacher()
    {
        return _dslPath.teacher();
    }

    /**
     * @return Период расписания сессии. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList#getSppScheduleSessionSeason()
     */
    public static SppScheduleSessionSeason.Path<SppScheduleSessionSeason> sppScheduleSessionSeason()
    {
        return _dslPath.sppScheduleSessionSeason();
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList#getBells()
     */
    public static ScheduleBell.Path<ScheduleBell> bells()
    {
        return _dslPath.bells();
    }

    public static class Path<E extends SppTeacherSessionPreferenceList> extends EntityPath<E>
    {
        private Person.Path<Person> _teacher;
        private SppScheduleSessionSeason.Path<SppScheduleSessionSeason> _sppScheduleSessionSeason;
        private PropertyPath<String> _title;
        private ScheduleBell.Path<ScheduleBell> _bells;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Преподаватель. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList#getTeacher()
     */
        public Person.Path<Person> teacher()
        {
            if(_teacher == null )
                _teacher = new Person.Path<Person>(L_TEACHER, this);
            return _teacher;
        }

    /**
     * @return Период расписания сессии. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList#getSppScheduleSessionSeason()
     */
        public SppScheduleSessionSeason.Path<SppScheduleSessionSeason> sppScheduleSessionSeason()
        {
            if(_sppScheduleSessionSeason == null )
                _sppScheduleSessionSeason = new SppScheduleSessionSeason.Path<SppScheduleSessionSeason>(L_SPP_SCHEDULE_SESSION_SEASON, this);
            return _sppScheduleSessionSeason;
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(SppTeacherSessionPreferenceListGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList#getBells()
     */
        public ScheduleBell.Path<ScheduleBell> bells()
        {
            if(_bells == null )
                _bells = new ScheduleBell.Path<ScheduleBell>(L_BELLS, this);
            return _bells;
        }

        public Class getEntityClass()
        {
            return SppTeacherSessionPreferenceList.class;
        }

        public String getEntityName()
        {
            return "sppTeacherSessionPreferenceList";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
