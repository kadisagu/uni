/* $Id: */
package ru.tandemservice.unispp.base.bo.SppDisciplinePreference.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unispp.base.bo.SppDisciplinePreference.SppDisciplinePreferenceManager;
import ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author vnekrasov
 * @since 7/3/14
 */
@Input({
        @Bind(key = "disciplineId", binding = "disciplineId")
})
public class SppDisciplinePreferenceAddUI extends UIPresenter
{
    private Long _disciplineId;
    private EppRegistryDiscipline _discipline;
    //private SppDisciplinePreferenceList _preference;
    private List<UniplacesBuilding> _buildingList;
    private List<UniplacesPlace> _lectureRoomList;
    private List<PpsEntry> _ppsList;

    public List<PpsEntry> getPpsList()
    {
        return _ppsList;
    }

    public void setPpsList(List<PpsEntry> ppsList)
    {
        _ppsList = ppsList;
    }

    public List<UniplacesPlace> getLectureRoomList()
    {
        return _lectureRoomList;
    }

    public void setLectureRoomList(List<UniplacesPlace> lectureRoomList)
    {
        _lectureRoomList = lectureRoomList;
    }

    public List<UniplacesBuilding> getBuildingList()
    {
        return _buildingList;
    }

    public void setBuildingList(List<UniplacesBuilding> buildingList)
    {
        _buildingList = buildingList;
    }

    public Long getDisciplineId()
    {
        return _disciplineId;
    }

    public void setDisciplineId(Long disciplineId)
    {
        _disciplineId = disciplineId;
    }

    public Boolean getAddForm()
    {
        return true;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _discipline)
        {
            _discipline = DataAccessServices.dao().getNotNull(_disciplineId);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppDisciplinePreferenceAdd.LECTURE_ROOM_DS.equals(dataSource.getName()))
        {
            Collection<Long> buildingIds = UniBaseUtils.getIdList(_buildingList);
            dataSource.put("buildingIds", buildingIds);
        }
    }

    public void onChangeLectureRoom()
    {
//        if(null != _element.getLectureRoom())
//            _event.setLectureRoom(_event.getLectureRoomVal().getTitle());
    }

    public void onChangeBuilding()
    {

    }

    public void onChangeTeacher()
    {

    }

    public void onChangeDayOfWeek()
    {
//        if(null != _dayOfWeekWrapper)
//            _element.setDayOfWeek(_dayOfWeekWrapper.getId().intValue());
    }



    public void onClickApply()
    {
        if(_buildingList.isEmpty() && _lectureRoomList.isEmpty())
            _uiSupport.error("Выберите здания или аудитории.", "buildings", "rooms");
        if(getUserContext().getErrorCollector().hasErrors())
            return;

        List<UniplacesBuilding> buildingList = new ArrayList<>();
        List<PpsEntry> ppsList = new ArrayList<>();
        List<UniplacesPlace> lectureRoomList = new ArrayList<>();

        if  (_ppsList.isEmpty())
            ppsList.add(null);
        else
            ppsList.addAll(_ppsList);

        if  (_lectureRoomList.isEmpty())
                lectureRoomList.add(null);
        else
            lectureRoomList.addAll(_lectureRoomList);


        if  (_buildingList.isEmpty() || !(_lectureRoomList.isEmpty()))
               buildingList.add(null);
        else
            buildingList.addAll(_buildingList);


        for (PpsEntry pps : ppsList)
        for (UniplacesBuilding building : buildingList)
        for (UniplacesPlace lectureRoom : lectureRoomList)
        {
            SppDisciplinePreferenceList element = new SppDisciplinePreferenceList();
            element.setDiscipline(_discipline);
            element.setBuilding(building);
            element.setLectureRoom(lectureRoom);
            element.setTeacher(pps);
            SppDisciplinePreferenceManager.instance().dao().saveOrUpdateDisciplinePreferenceList(element);
        }
        //SppScheduleSessionManager.instance().dao().updateScheduleAsChanged(_schedule.getId());
        deactivate();
    }
}
