/*$Id$*/
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.MassAddingSelectSchedulesList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleGroupComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.MassAddingExercise.SppScheduleMassAddingExercise;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.MassAddingExercise.SppScheduleMassAddingExerciseUI;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author DMITRY KNYAZEV
 * @since 29.10.2015
 */
@Input({
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class SppScheduleMassAddingSelectSchedulesListUI extends UIPresenter
{
    // settings
    public static final String TITLE = "title";
    public static final String SEASON = "season";
    public static final String BELL_SCHEDULE = "bellSchedule";
    public static final String GROUPS = "groups";
    public static final String FORMATIVE_ORG_UNITS = "formativeOrgUnits";

    private Long _orgUnitId;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("orgUnitId", getOrgUnitId());
        if (SppScheduleMassAddingSelectSchedulesList.SPP_SCHEDULE_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleDSHandler.PROP_TITLE, getFilterTitle());

            if (getFilterSeason() != null)
                dataSource.put("seasons", Collections.singletonList(getFilterSeason().getId()));
            if (getFilterBellSchedule() != null)
                dataSource.put(SppScheduleDSHandler.PROP_BELL_SCHEDULE_ID, getFilterBellSchedule().getId());
            if (getFilterGroups() != null)
                dataSource.put("groups", CommonDAO.ids(getFilterGroups()));
            if (getFilterOrgUnits() != null)
                dataSource.put(SppScheduleDSHandler.PROP_ORG_UNIT_IDS, CommonDAO.ids(getFilterOrgUnits()));

            if (getFilterSeason() == null || getFilterBellSchedule() == null)
                dataSource.put(SppScheduleDSHandler.EMPTY_LIST, Boolean.TRUE);
        }
        if (SppScheduleMassAddingSelectSchedulesList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleGroupComboDSHandler.PROP_GROUPS_NOT_IN_ARCHIVE, Boolean.TRUE);
        }
    }

    @Override
    public void onComponentBindReturnParameters(String childRegionName, Map<String, Object> returnedData)
    {
        if (UIDefines.DIALOG_REGION_NAME.equals(childRegionName)) {
            Collection<IEntity> selected = ((BaseSearchListDataSource) this.getConfig().getDataSource(SppScheduleMassAddingSelectSchedulesList.SPP_SCHEDULE_DS)).getOptionColumnSelectedObjects("check");
            selected.clear();
        }
    }

    // если зашли из орг.структуры, то у нас должен быть id этой структуры,
    // значит селект с формирующим подразделением не нужен
    // (там будет только одно подразделение (из которого зашли))
    public boolean isOneOrgUnit()
    {
        return _orgUnitId != null;
    }

    // ...и нам нужно будет написать титл этого подразделения
    public String getOrgUnitTitle()
    {
        OrgUnit orgUnit = null;
        if (_orgUnitId != null)
            orgUnit = DataAccessServices.dao().get(_orgUnitId);
        return orgUnit != null ? orgUnit.getFullTitle() : "";
    }

    public void onClickAddPeriod()
    {
        // берём список выбранных расписаний
        List<SppSchedule> sppScheduleList = getScheduleList();

        // validation
        if (sppScheduleList.isEmpty()) throw new ApplicationException("Не выбраны расписания");

        // "programming is about things that can't happen" :)
        if (sppScheduleList.stream().map(SppSchedule::getBells).distinct().count() > 1)
            throw new ApplicationException("Добавить занятие можно только в расписания с одинаковым звонковым расписанием");
        if (sppScheduleList.stream().map(SppSchedule::getSeason).distinct().count() > 1)
            throw new ApplicationException("Добавить занятие можно только в расписания с одинаковым периодом");

        _uiActivation.asRegionDialog(SppScheduleMassAddingExercise.class.getSimpleName())
                .parameter(SppScheduleMassAddingExerciseUI.BIND_SCHEDULE_IDS_LIST, CommonDAO.ids(sppScheduleList))
                .activate();
    }

    private List<SppSchedule> getScheduleList()
    {
        Collection<IEntity> checked = ((BaseSearchListDataSource) this.getConfig().getDataSource(SppScheduleMassAddingSelectSchedulesList.SPP_SCHEDULE_DS)).getOptionColumnSelectedObjects("check");
        return checked.stream().map(v -> (SppSchedule) v).collect(Collectors.toList());
    }


    // filters
    private String getFilterTitle()
    {
        return getSettings().get(TITLE);
    }

    private SppScheduleSeason getFilterSeason()
    {
        return getSettings().get(SEASON);
    }

    private ScheduleBell getFilterBellSchedule()
    {
        return getSettings().get(BELL_SCHEDULE);
    }

    private List<Group> getFilterGroups()
    {
        return getSettings().get(GROUPS);
    }

    private List<OrgUnit> getFilterOrgUnits()
    {
        return getSettings().get(FORMATIVE_ORG_UNITS);
    }


    // getters & setters
    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }


    public Map<String, Object> getParametersMap()
    {
        return new ParametersMap().add("orgUnitId", getOrgUnitId());
    }
}
