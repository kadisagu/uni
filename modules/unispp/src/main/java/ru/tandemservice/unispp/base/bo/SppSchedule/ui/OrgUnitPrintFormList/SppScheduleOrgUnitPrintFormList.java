/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.OrgUnitPrintFormList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppSchedulePrintFormDSHandler;
import ru.tandemservice.unispp.base.entity.SppSchedulePrintForm;

/**
 * @author nvankov
 * @since 9/25/13
 */
@Configuration
public class SppScheduleOrgUnitPrintFormList extends BusinessComponentManager
{
    public static final String PRINT_FORM_DS = "schedulePrintFormDS";
    public static final String YEAR_PART_DS = "yearPartDS";
    public static final String ADMIN_DS = "adminDS";
    public static final String GROUP_DS = "groupDS";

    @Bean
    public ColumnListExtPoint printFormCL()
    {
        return columnListExtPointBuilder(PRINT_FORM_DS)
                .addColumn(dateColumn("createDate", SppSchedulePrintForm.createDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().required(true))
                .addColumn(textColumn("season", SppSchedulePrintForm.season().titleWithTime()))
                .addColumn(textColumn("yearPart", SppSchedulePrintForm.season().eppYearPart().title()))
                .addColumn(textColumn("ou", SppSchedulePrintForm.formativeOrgUnit().title()).order().required(true))
                .addColumn(textColumn("groups", SppSchedulePrintForm.groups()).order().required(true))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickDownload").permissionKey("ui:sec.orgUnit_getContentSppSchedulePrintFormTab"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("message:ui.deleteAlert").permissionKey("ui:sec.orgUnit_deleteSppSchedulePrintFormTab"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRINT_FORM_DS, printFormCL(), printFormDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEAR_PART_DS, getName(), EppYearPart.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(ADMIN_DS, SppScheduleManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s()))
                .addDataSource(selectDS(GROUP_DS, SppScheduleManager.instance().groupComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler printFormDSHandler()
    {
        return new SppSchedulePrintFormDSHandler(getName());
    }
}
