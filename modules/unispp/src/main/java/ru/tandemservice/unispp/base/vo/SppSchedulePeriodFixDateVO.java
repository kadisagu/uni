/* $Id$ */
package ru.tandemservice.unispp.base.vo;

import java.util.Date;

/**
 * @author nvankov
 * @since 12/29/13
 */
public class SppSchedulePeriodFixDateVO
{
    private Date _date;

    public Date getDate()
    {
        return _date;
    }

    public void setDate(Date date)
    {
        _date = date;
    }
}
