/* $Id$ */
package ru.tandemservice.unispp.base.ext.Employee;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Victor Nekrasov
 * @since 29.05.2014
 */
@Configuration
public class EmployeeExtManager extends BusinessObjectExtensionManager
{
}
