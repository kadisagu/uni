/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.OrgUnitPrintFormList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionPrintFormDSHandler;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionPrintForm;

/**
 * @author vnekrasov
 * @since 12/27/13
 */
@Configuration
public class SppScheduleSessionOrgUnitPrintFormList extends BusinessComponentManager
{
    public static final String PRINT_FORM_DS = "schedulePrintFormDS";
    public static final String YEAR_PART_DS = "yearPartDS";
//    public static final String ADMIN_DS = "adminDS"; // перенесено в ДВФУ
    public static final String GROUP_DS = "groupDS";

    @Bean
    public ColumnListExtPoint printFormCL()
    {
        return columnListExtPointBuilder(PRINT_FORM_DS)
                .addColumn(dateColumn("createDate", SppScheduleSessionPrintForm.createDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().required(true))
                .addColumn(textColumn("season", SppScheduleSessionPrintForm.season().titleWithTime()))
                .addColumn(textColumn("yearPart", SppScheduleSessionPrintForm.season().eppYearPart().title()))
                .addColumn(textColumn("ou", SppScheduleSessionPrintForm.formWithTerrTitle()).required(true))
                .addColumn(textColumn("groups", SppScheduleSessionPrintForm.groups()).order())
//                .addColumn(textColumn("admin", SppScheduleSessionPrintForm.adminOOP().titleWithOrgUnitShort())) // перенесено в ДВФУ
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickDownload").permissionKey("ui:sec.orgUnit_getContentSppScheduleSessionPrintFormTab"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("message:ui.deleteAlert").permissionKey("ui:sec.orgUnit_deleteSppScheduleSessionPrintFormTab"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRINT_FORM_DS, printFormCL(), printFormDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEAR_PART_DS, getName(), EppYearPart.defaultSelectDSHandler(getName())))
//                .addDataSource(selectDS(ADMIN_DS, SppScheduleSessionManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s())) // перенесено в ДВФУ
                .addDataSource(selectDS(GROUP_DS, SppScheduleManager.instance().groupComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler printFormDSHandler()
    {
        return new SppScheduleSessionPrintFormDSHandler(getName());
    }
}
