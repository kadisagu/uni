/* $Id$ */
package ru.tandemservice.unispp.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.OrgUnitTab.SppScheduleOrgUnitTab;

/**
 * @author nvankov
 * @since 14.02.2014
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    @Autowired
    private OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab("abstractOrgUnit_sppScheduleOrgUnit", SppScheduleOrgUnitTab.class).permissionKey("ui:secModel.orgUnit_viewSppScheduleMainTab").visible("ognl:@ru.tandemservice.uni.dao.IOrgstructDAO@instance.get().isAllowGroups(presenter.orgUnit)").parameters("mvel:['orgUnitId':presenter.orgUnit.id]"))
                .create();
    }
}
