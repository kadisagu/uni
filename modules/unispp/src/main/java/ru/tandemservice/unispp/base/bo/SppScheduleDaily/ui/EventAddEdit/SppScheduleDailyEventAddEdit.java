/* $Id: SppScheduleDailyEventAddEdit.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.EventAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@Configuration
public class SppScheduleDailyEventAddEdit extends BusinessComponentManager
{
    public static final String BELL_DS = "bellDS";
    public static final String LECTURE_ROOM_DS = "lectureRoomDS";
    public static final String TEACHER_DS = "teacherDS";
    public static final String SUBJECT_DS = "subjectDS";
    public static final String LECTURE_ROOM_TYPE_DS = "lectureRoomTypeDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(BELL_DS, bellsEntryComboDSHandler()).addColumn("time", null, new IFormatter<ScheduleBellEntry>()
                {
                    @Override
                    public String format(ScheduleBellEntry source)
                    {
                        return source.getTime();
                    }
                }))
                .addDataSource(selectDS(LECTURE_ROOM_DS, SppScheduleDailyManager.instance().lectureRoomComboDSHandler())/*.addColumn("placeTitle88", SppScheduleDailyLectureRoomComboDSHandler.VIEW_PROP_TITLE).addColumn("capacity", SppScheduleDailyLectureRoomComboDSHandler.VIEW_PROP_CAPACITY).addColumn("location", SppScheduleDailyLectureRoomComboDSHandler.VIEW_PROP_LOCATION)*/)
                .addDataSource(selectDS(TEACHER_DS, SppScheduleDailyManager.instance().ppsComboDSHandler()).addColumn("fio", PpsEntry.titleFioInfoOrgUnit().s()))
                .addDataSource(selectDS(SUBJECT_DS, SppScheduleDailyManager.instance().subjectComboDSHandler()))
                .addDataSource(selectDS(LECTURE_ROOM_TYPE_DS, SppScheduleDailyManager.instance().placePurposeComboDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> bellsEntryComboDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), ScheduleBellEntry.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                Long bellsId = context.get(SppScheduleDailyEventAddEditUI.BELLS_ID);
                dql.where(eq(property(alias, ScheduleBellEntry.schedule().id()), value(bellsId)));
            }
        };
        return handler.order(ScheduleBellEntry.startTime());
    }
}
