/* $Id: SppScheduleDailyDAO.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.entity.*;
import ru.tandemservice.unispp.base.entity.catalog.SppScheduleStatus;
import ru.tandemservice.unispp.base.entity.catalog.codes.SppScheduleStatusCodes;
import ru.tandemservice.unispp.base.entity.gen.SppRegElementExtGen;
import ru.tandemservice.unispp.base.vo.SppScheduleDailyGroupPrintVO;
import ru.tandemservice.unispp.util.SppScheduleUtil;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@Transactional
public class SppScheduleDailyDAO extends BaseModifyAggregateDAO implements ISppScheduleDailyDAO
{
    @Override
    public void createOrUpdateSeason(SppScheduleDailySeason season)
    {
        baseCreateOrUpdate(season);
    }

    @Override
    public void saveSchedules(List<SppScheduleDaily> scheduleList)
    {
        scheduleList.forEach(this::baseCreate);
    }

    @Override
    public void saveOrUpdateSchedule(SppScheduleDaily schedule, ScheduleBell oldBells)
    {
        boolean deleteAndCreateStructure = false;
        if (null == schedule.getId()) deleteAndCreateStructure = true;
        if (null != oldBells && !oldBells.getId().equals(schedule.getBells().getId())) deleteAndCreateStructure = true;

        if (deleteAndCreateStructure)
        {
            new DQLDeleteBuilder(SppScheduleDailyEvent.class)
                    .where(eq(property(SppScheduleDailyEvent.schedule().id()), value(schedule.getId())))
                    .createStatement(getSession()).execute();

            new DQLDeleteBuilder(SppScheduleDailyWarningLog.class)
                    .where(eq(property(SppScheduleDailyWarningLog.sppScheduleDaily().id()), value(schedule.getId())))
                    .createStatement(getSession()).execute();
        }
        baseCreateOrUpdate(schedule);
    }

    @Override
    public void copySchedule(Long scheduleDailyId, SppScheduleDaily newScheduleDaily)
    {
        baseCreateOrUpdate(newScheduleDaily);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "e");
        builder.where(eq(property("e", SppScheduleDailyEvent.schedule().id()), value(scheduleDailyId)));
        List<SppScheduleDailyEvent> eventList = builder.createStatement(getSession()).list();

        for (SppScheduleDailyEvent event : eventList)
        {
            SppScheduleDailyEvent newEvent = new SppScheduleDailyEvent();
            newEvent.setDate(event.getDate());
            newEvent.setLectureRoom(event.getLectureRoom());
            newEvent.setLectureRoomVal(event.getLectureRoomVal());
            newEvent.setTeacher(event.getTeacher());
            newEvent.setTeacherVal(event.getTeacherVal());

            newEvent.setSubject(event.getSubject());
            newEvent.setSubjectVal(event.getSubjectVal());
            newEvent.setSubjectLoadType(event.getSubjectLoadType());
            newEvent.setSchedule(newScheduleDaily);

            baseCreateOrUpdate(newEvent);
        }
    }

    @Override
    public void sentToArchive(Long scheduleId)
    {
        SppScheduleDaily schedule =
//                DataAccessServices.dao().get(scheduleId);
                (SppScheduleDaily) get(scheduleId);
        schedule.setArchived(true);
        baseUpdate(schedule);
    }

    @Override
    public void restoreFromArchive(Long scheduleId)
    {
        SppScheduleDaily schedule = (SppScheduleDaily) get(scheduleId);
        schedule.setArchived(false);
        baseUpdate(schedule);
    }

    @Override
    public void validate(SppScheduleDaily schedule)
    {
        List<SppScheduleDailyEvent> eventList = DataAccessServices.dao().getList(SppScheduleDailyEvent.class, SppScheduleDailyEvent.schedule(), schedule);

        if (eventList.size() == 0)
            throw new ApplicationException("Для утверждения необходимо, чтобы в расписании было добавлено минимум 1 событие");

        SppScheduleDailySeason season = schedule.getSeason();
        Date startDate = new DateTime(season.getStartDate()).withTime(0, 0, 0, 0).toDate();
        Date endDate = new DateTime(season.getEndDate()).withTime(23, 59, 59, 1).toDate();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleDaily.class, "s")
                .where(eq(property("s", SppScheduleDaily.group().id()), value(schedule.getGroup().getId())))
                .where(ne(property("s", SppScheduleDaily.id()), value(schedule.getId())))
                .where(eq(property("s", SppScheduleDaily.approved()), value(Boolean.TRUE)))
                .where(eq(property("s", SppScheduleDaily.archived()), value(Boolean.FALSE)))
                .where(and(
                        le(property("s", SppScheduleDaily.season().startDate()), valueDate(endDate)),
                        ge(property("s", SppScheduleDaily.season().endDate()), valueDate(startDate))));

        List<SppScheduleDaily> schedules = builder.createStatement(getSession()).list();

        if (schedules.size() > 0)
        {
            String scheduleTitles = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(schedules, SppScheduleDaily.title()), ", ");
            throw new ApplicationException("На период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startDate) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate) + " уже есть не архивные утвержденные расписания - " + scheduleTitles);
        }
        updateWarningLog(schedule.getId(), eventList);
    }

    @Override
    public void approve(Long scheduleId)
    {
        SppScheduleDaily schedule = (SppScheduleDaily) getNotNull(scheduleId);

        validate(schedule);

        SppScheduleStatus status = DataAccessServices.dao().get(SppScheduleStatus.class, SppScheduleStatus.code(), SppScheduleStatusCodes.APPROVED);
        schedule.setStatus(status);
        schedule.setApproved(true);
        baseUpdate(schedule);
    }

    private void checkAllDisciplineLoads(Long scheduleId, List<SppScheduleDailyEvent> eventList)
    {
        List<Long> disciplineIds = new ArrayList<>();
        SppScheduleDaily daily = (SppScheduleDaily) get(scheduleId);
        for (SppScheduleDailyEvent event : eventList)
        {
            if (event.getSubjectVal() != null)
                disciplineIds.add(event.getSubjectVal().getId());
        }
        //Collection<Long> eventIds = CommonDAO.ids(eventList);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppRegElementLoadCheck.class, "rc");
        builder.column("rc");
        builder.joinEntity("rc", DQLJoinType.left, SppScheduleDailyEvent.class, "e",
                and(eq(property("e", SppScheduleDailyEvent.subjectLoadType().id()), property("rc", SppRegElementLoadCheck.eppALoadType().id())),
                        eq(property("e", SppScheduleDailyEvent.subjectVal().id()), property("rc", SppRegElementLoadCheck.registryElement().id()))));
        builder.joinPath(DQLJoinType.left, SppScheduleDailyEvent.schedule().fromAlias("e"), "s");
        builder.where(or(isNull(property("s")), eq(property("s"), value(daily))));
        builder.where(in(property("rc", SppRegElementLoadCheck.registryElement().id()), disciplineIds));
        builder.where(eq(property("rc", SppRegElementLoadCheck.checkValue()), value(true)));
        builder.column("e");
        builder.distinct();

        List<Object[]> checkList = createStatement(builder).list();

        for (Object[] objects : checkList)
        {
            SppRegElementLoadCheck check = (SppRegElementLoadCheck) objects[0];

            if (objects[1] == null)
            {
                addWarningMessage(daily, "ОШИБКА: Отсутствуют " + check.getEppALoadType().getTitle() +
                        " по дисциплине «" + check.getRegistryElement().getTitle() + "»");
            }
        }
    }

    @Override
    public UniplacesClassroomType getDisciplineLectureRoomType(EppRegistryElement discipline, EppALoadType loadType)
    {
        if (discipline != null && loadType != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppRegElementLoadCheck.class, "rc");
            builder.column(property("rc", SppRegElementLoadCheck.uniplacesClassroomType()));
            builder.where(eq(property("rc", SppRegElementLoadCheck.registryElement().id()), value(discipline.getId())));
            builder.where(eq(property("rc", SppRegElementLoadCheck.eppALoadType().id()), value(loadType.getId())));
            return builder.createStatement(getSession()).uniqueResult();
        }
        return null;
    }

    private void checkLectureRoomType(Long scheduleId, List<SppScheduleDailyEvent> eventList)
    {
        SppScheduleDaily daily = (SppScheduleDaily) get(scheduleId);
        //List<Long> disciplineIds = new ArrayList<>();
        for (SppScheduleDailyEvent event : eventList)
        {
            UniplacesClassroomType placePurpose = getDisciplineLectureRoomType(event.getSubjectVal(), event.getSubjectLoadType());
            if (placePurpose != null && event.getLectureRoomVal() != null && event.getLectureRoomVal().getClassroomType() != null &&
                    !event.getLectureRoomVal().getClassroomType().equals(placePurpose))
            {
                addWarningMessage(daily, "ПРЕДУПРЕЖДЕНИЕ: тип аудитории в дисциплине «" + event.getSubject() + "»" +
                        " не совпадает с указанным в событии " + DateFormatter.DEFAULT_DATE_FORMATTER.format(event.getDate()));
            }
        }
    }


    private void checkLectureRoomCapacity(Long scheduleId, List<SppScheduleDailyEvent> eventList)
    {
        SppScheduleDaily daily = (SppScheduleDaily) get(scheduleId);

        //int studentGrpCnt = DataAccessServices.dao().getCount(Student.class, Student.group().id().s(), daily.getGroup().getId());
        int studentGrpCnt = SppScheduleManager.instance().dao().getGroupStudentCount(daily.getGroup().getId());

        for (SppScheduleDailyEvent event : eventList)
        {
            if (event.getLectureRoomVal() != null && event.getLectureRoomVal().getCapacity() != null &&
                    event.getLectureRoomVal().getCapacity() < studentGrpCnt)
            {
                addWarningMessage(daily, "ОШИБКА: аудитория «" + event.getLectureRoom() + "»" +
                        " в событии " + DateFormatter.DEFAULT_DATE_FORMATTER.format(event.getDate()) +
                        " не может вместить всех студентов группы");
            }
        }
    }

    private void checkLectureRoomFree(Long scheduleId, List<SppScheduleDailyEvent> eventList)
    {
        SppScheduleDaily schedule = (SppScheduleDaily) getNotNull(scheduleId);

        for (SppScheduleDailyEvent event : eventList)
        {
            if (null != event.getLectureRoomVal())
            {
                Date date = event.getDate();

                UniplacesPlace place = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e")
                        .column(property("e", SppSchedulePeriod.lectureRoomVal())).top(1)
                        .where(eq(property("e", SppSchedulePeriod.lectureRoomVal().id()), value(event.getLectureRoomVal().getId())))
                        .where(le(property("e", SppSchedulePeriod.weekRow().schedule().season().startDate()), valueDate(date)))
                        .where(ge(property("e", SppSchedulePeriod.weekRow().schedule().season().endDate()), valueDate(date)))
                        .where(eq(property("e", SppSchedulePeriod.dayOfWeek()), value(SppScheduleUtil.getDayOfWeek(date))))
                        .where(eq(property("e", SppSchedulePeriod.weekRow().bell()), value(event.getBell())))
                        .createStatement(getSession()).uniqueResult();

                UniplacesPlace placeSessionEvent = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "e")
                        .column(property("e", SppScheduleSessionEvent.lectureRoomVal())).top(1)
                        .where(eq(property("e", SppScheduleSessionEvent.lectureRoomVal().id()), value(event.getLectureRoomVal().getId())))
                        .where(eq(property("e", SppScheduleSessionEvent.date()), valueDate(date)))
                        .where(eq(property("e", SppScheduleSessionEvent.bell()), value(event.getBell())))
                        .createStatement(getSession()).uniqueResult();

                UniplacesPlace placeDailyEvent = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "e")
                        .column(property("e", SppScheduleDailyEvent.lectureRoomVal())).top(1)
                        .where(ne(property("e", SppScheduleDailyEvent.schedule().id()), value(scheduleId)))
                        .where(eq(property("e", SppScheduleDailyEvent.lectureRoomVal().id()), value(event.getLectureRoomVal().getId())))
                        .where(eq(property("e", SppScheduleDailyEvent.date()), valueDate(date)))
                        .where(eq(property("e", SppScheduleDailyEvent.bell()), value(event.getBell())))
                        .createStatement(getSession()).uniqueResult();

                String dateFirstTimeMoment = DateFormatter.DEFAULT_DATE_FORMATTER.format(CoreDateUtils.getDayFirstTimeMoment(event.getDate()));
                if (place != null)
                {
                    addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + event.getLectureRoom() + "»" +
                            " в событии «" + dateFirstTimeMoment + ", " + event.getBell().getTitle() + "»" +
                            " используется в одном из обычных расписаний");
                }
                if (placeDailyEvent != null)
                {
                    addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + event.getLectureRoom() + "»" +
                            " в событии «" + dateFirstTimeMoment + ", " + event.getBell().getTitle() + "»" +
                            " используется в подневном расписании другой группы");
                }
                if (placeSessionEvent != null)
                {
                    addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + event.getLectureRoom() + "»" +
                            " в событии «" + dateFirstTimeMoment + ", " + event.getBell().getTitle() + "»" +
                            " используется в одном из расписаний сессий");
                }
            }
        }
    }

    private void checkTeacherPreference(Long scheduleId, List<SppScheduleDailyEvent> eventList)
    {
        SppScheduleDaily daily = (SppScheduleDaily) get(scheduleId);
        for (SppScheduleDailyEvent event : eventList)
        {
            if (event.getTeacherVal() != null)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppTeacherDailyPreferenceElement.class, "e");
                builder.column("e");
                builder.where(eq(property("e", SppTeacherDailyPreferenceElement.teacherPreference().teacher().id()), value(event.getTeacherVal().getPerson().getId())));
                builder.where(le(property("e", SppTeacherDailyPreferenceElement.startDate()), valueDate(event.getDate())));
                builder.where(ge(property("e", SppTeacherDailyPreferenceElement.endDate()), valueDate(event.getDate())));
                builder.where(or(isNull(property("e", SppTeacherDailyPreferenceElement.dayOfWeek())),
                        eq(property("e", SppTeacherDailyPreferenceElement.dayOfWeek()), value(SppScheduleUtil.getDayOfWeek(event.getDate()))))
                );
                List<SppTeacherDailyPreferenceElement> elementList = createStatement(builder).list();
                List<UniplacesBuilding> buildingList = new ArrayList<>();
                List<UniplacesPlace> lectureRoomList = new ArrayList<>();

                for (SppTeacherDailyPreferenceElement element : elementList)
                {
                    if (element.getBuilding() != null)
                        buildingList.add(element.getBuilding());
                    if (element.getLectureRoom() != null)
                        lectureRoomList.add(element.getLectureRoom());
                }

                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "p");
                dql.where(in(property("p", UniplacesPlace.floor().unit().building()), buildingList));
                List<UniplacesPlace> addLectureRoomList = createStatement(dql).list();
                lectureRoomList.addAll(addLectureRoomList);

                if (!lectureRoomList.isEmpty() && event.getLectureRoomVal() != null && !lectureRoomList.contains(event.getLectureRoomVal()))
                {
                    addWarningMessage(daily, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + event.getLectureRoom() + "»" +
                            " в событии " + DateFormatter.DEFAULT_DATE_FORMATTER.format(event.getDate()) +
                            " отсутствует в предпочтениях преподавателя «" + event.getTeacher() + "»");
                }
            }
        }
    }

    private void checkDisciplinePreference(Long scheduleId, List<SppScheduleDailyEvent> eventList)
    {
        SppScheduleDaily daily = (SppScheduleDaily) get(scheduleId);
        for (SppScheduleDailyEvent event : eventList)
        {
            if (event.getSubjectVal() != null)
            {
                DQLSelectBuilder builder1 = new DQLSelectBuilder().fromEntity(SppDisciplinePreferenceList.class, "e");
                builder1.column(property("e", SppDisciplinePreferenceList.teacher()));
                builder1.where(eq(property("e", SppDisciplinePreferenceList.discipline().id()), value(event.getSubjectVal().getId())));
                List<PpsEntry> teacherList = createStatement(builder1).list();

                if (!teacherList.isEmpty() && event.getTeacherVal() != null && !teacherList.contains(event.getTeacherVal()))
                {
                    addWarningMessage(daily, "ПРЕДУПРЕЖДЕНИЕ: преподаватель «" + event.getTeacher() + "»" +
                            " в событии " + DateFormatter.DEFAULT_DATE_FORMATTER.format(event.getDate()) +
                            " отсутствует в предпочтениях по дисциплине «" + event.getSubject() + "»");
                }

                DQLSelectBuilder builder2 = new DQLSelectBuilder().fromEntity(SppDisciplinePreferenceList.class, "e");
                builder2.column("e");
                builder2.where(eq(property("e", SppDisciplinePreferenceList.discipline().id()), value(event.getSubjectVal().getId())));

                List<SppDisciplinePreferenceList> elementList = createStatement(builder2).list();

                List<UniplacesBuilding> buildingList = new ArrayList<>();
                List<UniplacesPlace> lectureRoomList = new ArrayList<>();


                for (SppDisciplinePreferenceList element : elementList)
                {
                    if (element.getBuilding() != null)
                        buildingList.add(element.getBuilding());
                    if (element.getLectureRoom() != null)
                        lectureRoomList.add(element.getLectureRoom());
                }

                DQLSelectBuilder dql1 = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "p");
                dql1.where(in(property("p", UniplacesPlace.floor().unit().building()), buildingList));
                List<UniplacesPlace> addLectureRoomList = createStatement(dql1).list();
                lectureRoomList.addAll(addLectureRoomList);

                if (lectureRoomList.size() > 0 && !lectureRoomList.contains(event.getLectureRoomVal()))
                {
                    addWarningMessage(daily, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + event.getLectureRoom() + "»" +
                            " в событии " + DateFormatter.DEFAULT_DATE_FORMATTER.format(event.getDate()) +
                            " отсутствует в предпочтениях по дисциплине «" + event.getSubject() + "»");
                }
            }
        }
    }

    private void checkDayDoubleDiscipline(Long scheduleId, List<SppScheduleDailyEvent> eventList)
    {
        SppScheduleDaily schedule = (SppScheduleDaily) getNotNull(scheduleId);
        Set<Date> dateList = eventList.stream().map(e -> CoreDateUtils.getDayFirstTimeMoment(e.getDate())).collect(Collectors.toSet());

        // возможно, по некоторым дисциплинам вообще не нужно проверять дубли
        DQLSelectBuilder regElementExtBuilder = new DQLSelectBuilder();
        regElementExtBuilder.fromEntity(SppRegElementExt.class, "elExt").column("elExt");
        regElementExtBuilder.where(eq(property("elExt", SppRegElementExt.checkDuplicates()), value(Boolean.FALSE)));
        regElementExtBuilder.fetchPath(DQLJoinType.inner, SppRegElementExt.registryElement().fromAlias("elExt"), "el");
        Set<EppRegistryElement> disciplinesWithoutCheckSet = regElementExtBuilder.createStatement(getSession()).list()
                .stream().distinct().map(o -> (SppRegElementExt)o).map(SppRegElementExtGen::getRegistryElement)
                .collect(Collectors.toSet());

        for (Date date : dateList)
        {
            List<EppRegistryElement> duplicateDisciplineList = eventList.stream()
                    .filter(e -> CoreDateUtils.getDayFirstTimeMoment(e.getDate()).equals(CoreDateUtils.getDayFirstTimeMoment(date)))
                    .filter(e -> e.getSubjectVal() != null)
                    .map(SppScheduleDailyEvent::getSubjectVal)
                    .filter(d -> !disciplinesWithoutCheckSet.contains(d))
                    .collect(Collectors.groupingBy(d -> d, Collectors.counting()))
                    .entrySet().stream()
                    .filter(p -> p.getValue() > 1)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
            for (EppRegistryElement discipline : duplicateDisciplineList)
            {
                addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: дисциплина «" + discipline.getTitle() + "»" +
                        " присутствует " + DateFormatter.DEFAULT_DATE_FORMATTER.format(date) + " несколько раз");
            }
        }
    }

    private void addWarningMessage(SppScheduleDaily schedule, String message)
    {
        SppScheduleDailyWarningLog log = new SppScheduleDailyWarningLog();
        log.setWarningMessage(message);
        log.setSppScheduleDaily(schedule);
        baseCreate(log);
    }

    private void updateWarningLog(Long scheduleId, List<SppScheduleDailyEvent> eventList)
    {
        DQLDeleteBuilder dBuilder = new DQLDeleteBuilder(SppScheduleDailyWarningLog.class);
        dBuilder.where(eq(property(SppScheduleDailyWarningLog.sppScheduleDaily().id()), value(scheduleId)));
        dBuilder.createStatement(getSession()).execute();

        checkAllDisciplineLoads(scheduleId, eventList);
        checkDayDoubleDiscipline(scheduleId, eventList);
        checkLectureRoomType(scheduleId, eventList);
        checkLectureRoomCapacity(scheduleId, eventList);
        checkLectureRoomFree(scheduleId, eventList);
        checkTeacherPreference(scheduleId, eventList);
        checkDisciplinePreference(scheduleId, eventList);

    }

    @Override
    public void sentToFormation(Long scheduleId)
    {
        SppScheduleDaily daily = (SppScheduleDaily) get(scheduleId);
        SppScheduleStatus status = DataAccessServices.dao().get(SppScheduleStatus.class, SppScheduleStatus.code(), SppScheduleStatusCodes.FORMATION);
        daily.setStatus(status);
        daily.setApproved(false);
        baseUpdate(daily);
    }


    @Override
    public boolean getScheduleDataOrRecipientsChanged(Long scheduleId)
    {
        return false;
    }

    @Override
    public void createOrUpdateEvent(SppScheduleDailyEvent event)
    {
        SppScheduleDailySeason season = event.getSchedule().getSeason();
        if (event.getDate().before(season.getStartDate()))
            throw new ApplicationException("Событие не может быть раньше чем дата начала периода расписания");
        if (event.getDate().after(season.getEndDate()))
            throw new ApplicationException("Событие не может быть позже чем дата окончания периода расписания");
        baseCreateOrUpdate(event);
    }

    @Override
    public void deleteSchedule(Long scheduleId)
    {
        baseDelete(scheduleId);  // TODO:!!!
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void sendICalendar(Long scheduleId)
    {
    }


    @Override
    public SppScheduleDailyPrintForm savePrintForm(SppScheduleDailyPrintParams printData)
    {
        SppScheduleDailyPrintForm printForm = new SppScheduleDailyPrintForm();
        printForm.setCreateDate(new Date());
        printForm.setFormativeOrgUnit(printData.getFormativeOrgUnit());
//        printForm.setTerritorialOrgUnit(printData.getTerritorialOrgUnit()); // перенесено в ДВФУ
        if (null != printData.getCourseList() && !printData.getCourseList().isEmpty())
            printForm.setCourses(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getCourseList(), Course.title()), ", "));
        printForm.setSeason(printData.getSeason());
        if (null != printData.getEduLevels() && !printData.getEduLevels().isEmpty())
            printForm.setEduLevels(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getEduLevels(), EducationLevelsHighSchool.printTitle()), ", "));
//        printForm.setTerm(printForm.getTerm()); // перенесено в ДВФУ
        printForm.setChief(printData.getChief());
//        printForm.setChiefUMU(printData.getChiefUMU()); // перенесено в ДВФУ
//        printForm.setAdminOOP(printData.getAdmin()); // перенесено в ДВФУ
//        String headers = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getHeaders(), EmployeePost.person().identityCard().iof()), ", ");
//        printForm.setHeadersOOP(headers.length() > 255 ? headers.substring(0, 255) : headers); // перенесено в ДВФУ
        printForm.setCreateOU(printData.getCurrentOrgUnit());
        if (null != printData.getSchedules() && !printData.getSchedules().isEmpty())
            printForm.setGroups(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getSchedules(), SppScheduleDaily.group().title()), ", "));
        else
            printForm.setGroups("-");
        printForm.setEduYear(printData.getEducationYear().getTitle());
        List<SppScheduleDailyGroupPrintVO> schedules = Lists.newArrayList();
        Map<SppScheduleDaily, List<SppScheduleDailyEvent>> schedulesMap = getSchedulesMap(printData);
        for (Map.Entry<SppScheduleDaily, List<SppScheduleDailyEvent>> entry : schedulesMap.entrySet())
            schedules.add(prepareScheduleGroupPrintVO(entry));
        DatabaseFile content = new DatabaseFile();
        try
        {
            content.setContent(SppScheduleDailyPrintFormExcelContentBuilder.buildExcelContent(schedules, printData));
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        baseCreate(content);
        printForm.setContent(content);
        baseCreate(printForm);
//        throw new ApplicationException("test");
        return printForm;

    }

    @Override
    public void sendICalendars()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleDaily.class, "s");
        builder.column(property("s", SppScheduleDaily.id()));
        builder.where(eq(property("s", SppScheduleDaily.approved()), value(true)));
        builder.where(eq(property("s", SppScheduleDaily.group().archival()), value(false)));
        org.joda.time.DateTime date = new org.joda.time.DateTime().withTime(0, 0, 0, 1);
        builder.where(and(
                le(property("s", SppScheduleDaily.season().startDate()), valueDate(date.minusMonths(1).toDate())),
                ge(property("s", SppScheduleDaily.season().endDate()), valueDate(date.toDate()))
        ));

        List<Long> fefuSchedulesIds = createStatement(builder).list();
        for (Long scheduleId : fefuSchedulesIds)
        {
            sendICalendar(scheduleId);
        }
    }

    @Override
    public void updateScheduleAsChanged(Long scheduleId)
    {
        SppScheduleDaily schedule = DataAccessServices.dao().get(scheduleId);
        if (null != schedule.getIcalData())
        {
            SppScheduleICal iCal = schedule.getIcalData();
            iCal.setChanged(true);
            baseUpdate(iCal);
        }
    }

    protected Map<SppScheduleDaily, List<SppScheduleDailyEvent>> getSchedulesMap(SppScheduleDailyPrintParams printData)
    {
        Map<SppScheduleDaily, List<SppScheduleDailyEvent>> schedulesMap = Maps.newHashMap();
        DQLSelectBuilder eventsBuilder = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "ev");
        eventsBuilder.fetchPath(DQLJoinType.inner, SppScheduleDailyEvent.schedule().fromAlias("ev"), "schedule");
        eventsBuilder.where(eq(property("ev", SppScheduleDailyEvent.schedule().status().code()), value(SppScheduleStatusCodes.APPROVED)));
        if (null != printData.getSchedules() && !printData.getSchedules().isEmpty())
        {
            eventsBuilder.where(in(property("ev", SppScheduleDailyEvent.schedule()), printData.getSchedules()));
        } else
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g");
            subBuilder.distinct();

            if (null != printData.getCurrentOrgUnit())
            {
                Long orgUnitId = printData.getCurrentOrgUnit().getId();
                DQLSelectBuilder kindRelationsBuilder = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                        .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                        .where(eq(property("rel", OrgUnitToKindRelation.orgUnit().id()), value(orgUnitId)));
                List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

                Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                        .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

                IDQLExpression condition = nothing(); // false
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                    condition = or(condition, eq(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().educationLevelHighSchool().orgUnit().id()), value(orgUnitId)));
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                    condition = or(condition, eq(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().formativeOrgUnit().id()), value(orgUnitId)));
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                    condition = or(condition, eq(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().territorialOrgUnit().id()), value(orgUnitId)));

                eventsBuilder.where(condition);
            }
            eventsBuilder.where(eq(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().formativeOrgUnit()), value(printData.getFormativeOrgUnit())));
            if (null != printData.getEduLevels() && !printData.getEduLevels().isEmpty())
                eventsBuilder.where(in(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().educationLevelHighSchool()), printData.getEduLevels()));
            eventsBuilder.where(eq(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().developForm()), value(printData.getDevelopForm())));
        }

        for (SppScheduleDailyEvent event : createStatement(eventsBuilder).<SppScheduleDailyEvent>list())
        {
            if (!schedulesMap.containsKey(event.getSchedule()))
                schedulesMap.put(event.getSchedule(), Lists.<SppScheduleDailyEvent>newArrayList());
            if (!schedulesMap.get(event.getSchedule()).contains(event))
                schedulesMap.get(event.getSchedule()).add(event);
        }

        return schedulesMap;
    }

    public SppScheduleDailyGroupPrintVO prepareScheduleGroupPrintVO(Map.Entry<SppScheduleDaily, List<SppScheduleDailyEvent>> scheduleEntry)
    {
        SppScheduleDailyGroupPrintVO groupPrintVO = new SppScheduleDailyGroupPrintVO(scheduleEntry.getKey());

        List<SppScheduleDailyEvent> events = scheduleEntry.getValue();
        for (SppScheduleDailyEvent event : events)
        {
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(event.getDate());
            LocalDate date = new LocalDate().withYear(calendar.get(Calendar.YEAR)).withMonthOfYear(calendar.get(Calendar.MONTH) + 1).withDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
            if (!groupPrintVO.getEventsMap().containsKey(date))
                groupPrintVO.getEventsMap().put(date, Lists.<SppScheduleDailyEvent>newArrayList());
            if (!groupPrintVO.getEventsMap().get(date).contains(event))
                groupPrintVO.getEventsMap().get(date).add(event);
        }
        int columns = getColumns(groupPrintVO.getEventsMap());

        groupPrintVO.setColumnNum(columns);

        return groupPrintVO;
    }

    private int getColumns(Map<LocalDate, List<SppScheduleDailyEvent>> eventsMap)
    {
        int columns = 0;
        for (Map.Entry<LocalDate, List<SppScheduleDailyEvent>> entry : eventsMap.entrySet())
        {
            if (null != entry.getValue() && !entry.getValue().isEmpty() && entry.getValue().size() > columns)
                columns = entry.getValue().size();
        }
        return columns;
    }

    private List<String> getCurrentTestNsiIds()
    {
        List<String> testIds = Lists.newArrayList();
        testIds.add("00000000-0000-0000-4444-000000000000");
        return testIds;
    }
}
