package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unispp_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность sppScheduleSessionEventExt

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("sppschedulesessioneventext_t",
                    new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sppschedulesessioneventext"),
                    new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                    new DBColumn("sppschedulesessionevent_id", DBType.LONG).setNullable(false),
                    new DBColumn("eppregistrydiscipline_id", DBType.LONG).setNullable(false),
                    new DBColumn("eppfcontrolactiontype_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sppScheduleSessionEventExt");

        }
    }
}