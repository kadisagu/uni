package ru.tandemservice.unispp.migration;

import com.google.common.collect.Lists;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unispp_2x11x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		// заранее достанем все id сущности "Часть года (в учебном году)" (EppYearPart) и соответствующие им года.
		String query = "SELECT yp.id, ye.starteducationdate_p\n" +
				"FROM epp_year_part_t yp\n" +
				"INNER JOIN epp_year_epp_t ye ON ye.id=yp.year_id\n" +
				"INNER JOIN yeardistributionpart_t ydp ON ydp.id=yp.part_id\n" +
				"INNER JOIN yeardistribution_t yd ON yd.id=ydp.yeardistribution_id\n" +
				"WHERE yd.inuse_p=?\n" +
				"AND ydp.number_p=?";
		PreparedStatement statement = tool.prepareStatement(query);
		statement.setBoolean(1, Boolean.TRUE);
		statement.setInt(2, 1);
		statement.execute();
		ResultSet resultSet = statement.getResultSet();

		// тут нужна не мапа, а скорее отсортированный (обратно) список пар
		List<CoreCollectionUtils.Pair<Long, Date>> yearPartList = Lists.newArrayList();
		while(resultSet.next())
			yearPartList.add(new CoreCollectionUtils.Pair<>(resultSet.getLong(1), resultSet.getDate(2)));
		// и отсортируем по дате
		Collections.sort(yearPartList, (o1, o2) -> o2.getY().compareTo(o1.getY()));

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleDailySeason

		// создано обязательное свойство eppYearPart
		{
			// создать колонку
			tool.createColumn("sppscheduledailyseason_t", new DBColumn("eppyearpart_id", DBType.LONG));

			SQLSelectQuery selectSeasonDaily = new SQLSelectQuery()
					.from(SQLFrom.table("sppscheduledailyseason_t", "sds"))
					.column("sds.id")
					.column("sds.startdate_p");
			Statement statement1 = tool.getConnection().createStatement();
			statement1.execute(tool.getDialect().getSQLTranslator().toSql(selectSeasonDaily));
			ResultSet resultSet1 = statement1.getResultSet();

			PreparedStatement update1 = tool.prepareStatement("update sppscheduledailyseason_t set eppyearpart_id=? where id=?");

			while (resultSet1.next())
			{
				if (yearPartList.isEmpty()) throw new RuntimeException("Не найдены части уч.года с активным разбиением");

				Long periodId = resultSet1.getLong(1);
				Date periodStartDate = resultSet1.getDate(2);

				Long yearPartId = 0L; // yearPartList тут не может быть пустым, не понятно почему идея ругнулась ниже
				for (CoreCollectionUtils.Pair<Long, Date> pair : yearPartList)
				{
					yearPartId = pair.getX();
					if (!pair.getY().after(periodStartDate))
						break;
				}

				update1.setLong(1, yearPartId);
				update1.setLong(2, periodId);
				update1.addBatch();
			}
			update1.executeBatch();

			// сделать колонку NOT NULL
			tool.setColumnNullable("sppscheduledailyseason_t", "eppyearpart_id", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleSeason

		// создано обязательное свойство eppYearPart
		{
			// создать колонку
			tool.createColumn("sppscheduleseason_t", new DBColumn("eppyearpart_id", DBType.LONG));

			SQLSelectQuery selectSeason = new SQLSelectQuery()
					.from(SQLFrom.table("sppscheduleseason_t", "ss"))
					.column("ss.id")
					.column("ss.startdate_p");
			Statement statement2 = tool.getConnection().createStatement();
			statement2.execute(tool.getDialect().getSQLTranslator().toSql(selectSeason));
			ResultSet resultSet2 = statement2.getResultSet();

			PreparedStatement update2 = tool.prepareStatement("update sppscheduleseason_t set eppyearpart_id=? where id=?");

			while (resultSet2.next())
			{
				if (yearPartList.isEmpty()) throw new RuntimeException("Не найдены части уч.года с активным разбиением");

				Long periodId = resultSet2.getLong(1);
				Date periodStartDate = resultSet2.getDate(2);

				Long yearPartId = 0L; // yearPartList тут не может быть пустым, не понятно почему идея ругнулась ниже
				for (CoreCollectionUtils.Pair<Long, Date> pair : yearPartList)
				{
					yearPartId = pair.getX();
					if (!pair.getY().after(periodStartDate))
						break;
				}

				update2.setLong(1, yearPartId);
				update2.setLong(2, periodId);
				update2.addBatch();
			}
			update2.executeBatch();

			// сделать колонку NOT NULL
			tool.setColumnNullable("sppscheduleseason_t", "eppyearpart_id", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleSessionSeason

		// создано обязательное свойство eppYearPart
		{
			// создать колонку
			tool.createColumn("sppschedulesessionseason_t", new DBColumn("eppyearpart_id", DBType.LONG));

			SQLSelectQuery selectSeasonSession = new SQLSelectQuery()
					.from(SQLFrom.table("sppschedulesessionseason_t", "sss"))
					.column("sss.id")
					.column("sss.startdate_p");
			Statement statement3 = tool.getConnection().createStatement();
			statement3.execute(tool.getDialect().getSQLTranslator().toSql(selectSeasonSession));
			ResultSet resultSet3 = statement3.getResultSet();

			PreparedStatement update3 = tool.prepareStatement("update sppschedulesessionseason_t set eppyearpart_id=? where id=?");

			while (resultSet3.next())
			{
				if (yearPartList.isEmpty()) throw new RuntimeException("Не найдены части уч.года с активным разбиением");

				Long periodId = resultSet3.getLong(1);
				Date periodStartDate = resultSet3.getDate(2);

				Long yearPartId = 0L; // yearPartList тут не может быть пустым, не понятно почему идея ругнулась ниже
				for (CoreCollectionUtils.Pair<Long, Date> pair : yearPartList)
				{
					yearPartId = pair.getX();
					if (!pair.getY().after(periodStartDate))
						break;
				}

				update3.setLong(1, yearPartId);
				update3.setLong(2, periodId);
				update3.addBatch();
			}
			update3.executeBatch();

			// сделать колонку NOT NULL
			tool.setColumnNullable("sppschedulesessionseason_t", "eppyearpart_id", false);
		}
    }
}