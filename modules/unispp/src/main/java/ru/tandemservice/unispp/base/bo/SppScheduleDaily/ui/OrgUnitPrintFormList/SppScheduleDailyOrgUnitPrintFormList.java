/* $Id: SppScheduleDailyOrgUnitPrintFormList.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.OrgUnitPrintFormList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyPrintFormDSHandler;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@Configuration
public class SppScheduleDailyOrgUnitPrintFormList extends BusinessComponentManager
{
    public static final String PRINT_FORM_DS = "schedulePrintFormDS";
    public static final String YEAR_PART_DS = "yearPartDS";
//    public static final String ADMIN_DS = "adminDS"; // перенесено в ДВФУ
    public static final String GROUP_DS = "groupDS";

    @Bean
    public ColumnListExtPoint printFormCL()
    {
        return columnListExtPointBuilder(PRINT_FORM_DS)
                .addColumn(dateColumn("createDate", SppScheduleDailyPrintForm.createDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().required(true))
                .addColumn(textColumn("season", SppScheduleDailyPrintForm.season().titleWithTime()))
                .addColumn(textColumn("yearPart", SppScheduleDailyPrintForm.season().eppYearPart().title()))
                .addColumn(textColumn("ou", SppScheduleDailyPrintForm.formWithTerrTitle()).required(true))
                .addColumn(textColumn("groups", SppScheduleDailyPrintForm.groups()).order())
//                .addColumn(textColumn("admin", SppScheduleDailyPrintForm.adminOOP().titleWithOrgUnitShort())) // перенесено в ДВФУ
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickDownload").permissionKey("ui:sec.orgUnit_getContentSppScheduleDailyPrintFormTab"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("message:ui.deleteAlert").permissionKey("ui:sec.orgUnit_deleteSppScheduleDailyPrintFormTab"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRINT_FORM_DS, printFormCL(), printFormDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEAR_PART_DS, getName(), EppYearPart.defaultSelectDSHandler(getName())))
//                .addDataSource(selectDS(ADMIN_DS, SppScheduleDailyManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s())) // перенесено в ДВФУ
                .addDataSource(selectDS(GROUP_DS, SppScheduleManager.instance().groupComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler printFormDSHandler()
    {
        return new SppScheduleDailyPrintFormDSHandler(getName());
    }
}
