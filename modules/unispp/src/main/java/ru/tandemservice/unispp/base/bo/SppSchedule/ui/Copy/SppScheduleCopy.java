/* $Id: */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.Copy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;

/**
 * @author vnekrasov
 * @since 7/7/14
 */
@Configuration
public class SppScheduleCopy extends BusinessComponentManager
{
    public static final String GROUP_DS = "groupDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(GROUP_DS, SppScheduleManager.instance().groupComboDSHandler()))
                .create();
    }
}
