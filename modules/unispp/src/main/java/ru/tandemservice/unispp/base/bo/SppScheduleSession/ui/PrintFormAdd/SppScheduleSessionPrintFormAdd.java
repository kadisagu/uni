/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.PrintFormAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionComboDSHandler;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;

/**
 * @author vnekrasov
 * @since 12/27/13
 */
@Configuration
public class SppScheduleSessionPrintFormAdd extends BusinessComponentManager
{
    public static final String FORMATIVE_OU_DS = "formativeOrgUnitDS";
//    public static final String TERRITORIAL_OU_DS = "territorialOrgUnitDS"; // перенесено в ДВФУ
    public static final String EDU_LEVEL_DS = "eduLevelDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String COURSE_DS = "courseDS";
    public static final String SEASON_DS = "seasonDS";
    public static final String BELLS_DS = "bellsDS";
    public static final String SCHEDULES_DS = "schedulesDS";
//    public static final String TERM_DS = "termDS"; // перенесено в ДВФУ
    public static final String SESSION_DS = "sessionDS";
    public static final String EDUCATION_YEAR = "educationYearDS";
    public static final String EMPLOYEE_POST_DS = "employeePostDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(FORMATIVE_OU_DS, SppScheduleSessionManager.instance().sppGroupComboDSHandler()))
//                .addDataSource(selectDS(TERRITORIAL_OU_DS, SppScheduleSessionManager.instance().sppGroupComboDSHandler()).addColumn("title", OrgUnit.territorialFullTitle().s())) // перенесено в ДВФУ
                .addDataSource(selectDS(EDU_LEVEL_DS, SppScheduleSessionManager.instance().sppGroupComboDSHandler()).addColumn("title", EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, SppScheduleSessionManager.instance().sppGroupComboDSHandler()))
                .addDataSource(selectDS(COURSE_DS, SppScheduleSessionManager.instance().sppGroupComboDSHandler()))
                .addDataSource(selectDS(SEASON_DS, schedulesSessionComboDSHandler()).addColumn("title", null, new IFormatter<SppScheduleSessionSeason>()
                {
                    @Override
                    public String format(SppScheduleSessionSeason source)
                    {
                        return source.getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getStartDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndDate()) + ")";
                    }
                }))
                .addDataSource(selectDS(BELLS_DS, schedulesSessionComboDSHandler()))
                .addDataSource(selectDS(SCHEDULES_DS, schedulesSessionComboDSHandler()).addColumn("group", SppScheduleSession.group().title().s()).addColumn("schedule", SppScheduleSession.title().s()))
//                .addDataSource(selectDS(TERM_DS, SppScheduleManager.instance().termComboDSHandler())) // перенесено в ДВФУ
                .addDataSource(selectDS(SESSION_DS, SppScheduleSessionManager.instance().sessionComboDSHandler()))
                .addDataSource(selectDS(EDUCATION_YEAR, educationYearComboDSHandler()))
                .addDataSource(selectDS(EMPLOYEE_POST_DS, SppScheduleSessionManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler educationYearComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EducationYear.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler schedulesSessionComboDSHandler()
    {
        return new SppScheduleSessionComboDSHandler(getName());
    }
}
