package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unispp_2x11x1_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherDailyPreferenceElement

		//  свойство teacherPreference стало обязательным
		{
			// удаляем осиротевшие (хотя на самом деле это impossibru, если только руками не правили базу)
			tool.executeUpdate("delete from spptchrdlyprfrncelmnt_t where teacherpreference_id is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("spptchrdlyprfrncelmnt_t", "teacherpreference_id", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherSessionPreferenceElement

		//  свойство teacherPreference стало обязательным
		{
			// удаляем осиротевшие (хотя на самом деле это impossibru, если только руками не правили базу)
			tool.executeUpdate("delete from spptchrsssnprfrncelmnt_t where teacherpreference_id is null");

			// сделать колонку NOT NULL
			tool.setColumnNullable("spptchrsssnprfrncelmnt_t", "teacherpreference_id", false);
		}
    }
}
