package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пара (исправление)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppSchedulePeriodFixGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix";
    public static final String ENTITY_NAME = "sppSchedulePeriodFix";
    public static final int VERSION_HASH = -116253947;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE = "date";
    public static final String P_LECTURE_ROOM = "lectureRoom";
    public static final String P_TEACHER = "teacher";
    public static final String P_SUBJECT = "subject";
    public static final String P_CANCELED = "canceled";
    public static final String P_PERIOD_NUM = "periodNum";
    public static final String P_PERIOD_GROUP = "periodGroup";
    public static final String L_SPP_SCHEDULE = "sppSchedule";
    public static final String L_BELL = "bell";
    public static final String L_TEACHER_VAL = "teacherVal";
    public static final String L_SUBJECT_VAL = "subjectVal";
    public static final String L_SUBJECT_LOAD_TYPE = "subjectLoadType";
    public static final String L_LECTURE_ROOM_VAL = "lectureRoomVal";

    private Date _date;     // Дата
    private String _lectureRoom;     // Аудитория
    private String _teacher;     // Преподаватель
    private String _subject;     // Предмет
    private boolean _canceled;     // Отменена
    private int _periodNum;     // Номер
    private String _periodGroup;     // Подгруппа
    private SppSchedule _sppSchedule;     // Расписание
    private ScheduleBellEntry _bell;     // Пара
    private PpsEntry _teacherVal;     // Преподаватель (сущность)
    private EppRegistryElement _subjectVal;     // Предмет(сущность)
    private EppALoadType _subjectLoadType;     // Тип нагрузки(сущность)
    private UniplacesPlace _lectureRoomVal;     // Аудитория(сущность)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Аудитория.
     */
    @Length(max=255)
    public String getLectureRoom()
    {
        return _lectureRoom;
    }

    /**
     * @param lectureRoom Аудитория.
     */
    public void setLectureRoom(String lectureRoom)
    {
        dirty(_lectureRoom, lectureRoom);
        _lectureRoom = lectureRoom;
    }

    /**
     * @return Преподаватель.
     */
    @Length(max=255)
    public String getTeacher()
    {
        return _teacher;
    }

    /**
     * @param teacher Преподаватель.
     */
    public void setTeacher(String teacher)
    {
        dirty(_teacher, teacher);
        _teacher = teacher;
    }

    /**
     * @return Предмет.
     */
    @Length(max=255)
    public String getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Предмет.
     */
    public void setSubject(String subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Отменена. Свойство не может быть null.
     */
    @NotNull
    public boolean isCanceled()
    {
        return _canceled;
    }

    /**
     * @param canceled Отменена. Свойство не может быть null.
     */
    public void setCanceled(boolean canceled)
    {
        dirty(_canceled, canceled);
        _canceled = canceled;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    public int getPeriodNum()
    {
        return _periodNum;
    }

    /**
     * @param periodNum Номер. Свойство не может быть null.
     */
    public void setPeriodNum(int periodNum)
    {
        dirty(_periodNum, periodNum);
        _periodNum = periodNum;
    }

    /**
     * @return Подгруппа.
     */
    @Length(max=255)
    public String getPeriodGroup()
    {
        return _periodGroup;
    }

    /**
     * @param periodGroup Подгруппа.
     */
    public void setPeriodGroup(String periodGroup)
    {
        dirty(_periodGroup, periodGroup);
        _periodGroup = periodGroup;
    }

    /**
     * @return Расписание. Свойство не может быть null.
     */
    @NotNull
    public SppSchedule getSppSchedule()
    {
        return _sppSchedule;
    }

    /**
     * @param sppSchedule Расписание. Свойство не может быть null.
     */
    public void setSppSchedule(SppSchedule sppSchedule)
    {
        dirty(_sppSchedule, sppSchedule);
        _sppSchedule = sppSchedule;
    }

    /**
     * @return Пара.
     */
    public ScheduleBellEntry getBell()
    {
        return _bell;
    }

    /**
     * @param bell Пара.
     */
    public void setBell(ScheduleBellEntry bell)
    {
        dirty(_bell, bell);
        _bell = bell;
    }

    /**
     * @return Преподаватель (сущность).
     */
    public PpsEntry getTeacherVal()
    {
        return _teacherVal;
    }

    /**
     * @param teacherVal Преподаватель (сущность).
     */
    public void setTeacherVal(PpsEntry teacherVal)
    {
        dirty(_teacherVal, teacherVal);
        _teacherVal = teacherVal;
    }

    /**
     * @return Предмет(сущность).
     */
    public EppRegistryElement getSubjectVal()
    {
        return _subjectVal;
    }

    /**
     * @param subjectVal Предмет(сущность).
     */
    public void setSubjectVal(EppRegistryElement subjectVal)
    {
        dirty(_subjectVal, subjectVal);
        _subjectVal = subjectVal;
    }

    /**
     * @return Тип нагрузки(сущность).
     */
    public EppALoadType getSubjectLoadType()
    {
        return _subjectLoadType;
    }

    /**
     * @param subjectLoadType Тип нагрузки(сущность).
     */
    public void setSubjectLoadType(EppALoadType subjectLoadType)
    {
        dirty(_subjectLoadType, subjectLoadType);
        _subjectLoadType = subjectLoadType;
    }

    /**
     * @return Аудитория(сущность).
     */
    public UniplacesPlace getLectureRoomVal()
    {
        return _lectureRoomVal;
    }

    /**
     * @param lectureRoomVal Аудитория(сущность).
     */
    public void setLectureRoomVal(UniplacesPlace lectureRoomVal)
    {
        dirty(_lectureRoomVal, lectureRoomVal);
        _lectureRoomVal = lectureRoomVal;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppSchedulePeriodFixGen)
        {
            setDate(((SppSchedulePeriodFix)another).getDate());
            setLectureRoom(((SppSchedulePeriodFix)another).getLectureRoom());
            setTeacher(((SppSchedulePeriodFix)another).getTeacher());
            setSubject(((SppSchedulePeriodFix)another).getSubject());
            setCanceled(((SppSchedulePeriodFix)another).isCanceled());
            setPeriodNum(((SppSchedulePeriodFix)another).getPeriodNum());
            setPeriodGroup(((SppSchedulePeriodFix)another).getPeriodGroup());
            setSppSchedule(((SppSchedulePeriodFix)another).getSppSchedule());
            setBell(((SppSchedulePeriodFix)another).getBell());
            setTeacherVal(((SppSchedulePeriodFix)another).getTeacherVal());
            setSubjectVal(((SppSchedulePeriodFix)another).getSubjectVal());
            setSubjectLoadType(((SppSchedulePeriodFix)another).getSubjectLoadType());
            setLectureRoomVal(((SppSchedulePeriodFix)another).getLectureRoomVal());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppSchedulePeriodFixGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppSchedulePeriodFix.class;
        }

        public T newInstance()
        {
            return (T) new SppSchedulePeriodFix();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "date":
                    return obj.getDate();
                case "lectureRoom":
                    return obj.getLectureRoom();
                case "teacher":
                    return obj.getTeacher();
                case "subject":
                    return obj.getSubject();
                case "canceled":
                    return obj.isCanceled();
                case "periodNum":
                    return obj.getPeriodNum();
                case "periodGroup":
                    return obj.getPeriodGroup();
                case "sppSchedule":
                    return obj.getSppSchedule();
                case "bell":
                    return obj.getBell();
                case "teacherVal":
                    return obj.getTeacherVal();
                case "subjectVal":
                    return obj.getSubjectVal();
                case "subjectLoadType":
                    return obj.getSubjectLoadType();
                case "lectureRoomVal":
                    return obj.getLectureRoomVal();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "lectureRoom":
                    obj.setLectureRoom((String) value);
                    return;
                case "teacher":
                    obj.setTeacher((String) value);
                    return;
                case "subject":
                    obj.setSubject((String) value);
                    return;
                case "canceled":
                    obj.setCanceled((Boolean) value);
                    return;
                case "periodNum":
                    obj.setPeriodNum((Integer) value);
                    return;
                case "periodGroup":
                    obj.setPeriodGroup((String) value);
                    return;
                case "sppSchedule":
                    obj.setSppSchedule((SppSchedule) value);
                    return;
                case "bell":
                    obj.setBell((ScheduleBellEntry) value);
                    return;
                case "teacherVal":
                    obj.setTeacherVal((PpsEntry) value);
                    return;
                case "subjectVal":
                    obj.setSubjectVal((EppRegistryElement) value);
                    return;
                case "subjectLoadType":
                    obj.setSubjectLoadType((EppALoadType) value);
                    return;
                case "lectureRoomVal":
                    obj.setLectureRoomVal((UniplacesPlace) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "date":
                        return true;
                case "lectureRoom":
                        return true;
                case "teacher":
                        return true;
                case "subject":
                        return true;
                case "canceled":
                        return true;
                case "periodNum":
                        return true;
                case "periodGroup":
                        return true;
                case "sppSchedule":
                        return true;
                case "bell":
                        return true;
                case "teacherVal":
                        return true;
                case "subjectVal":
                        return true;
                case "subjectLoadType":
                        return true;
                case "lectureRoomVal":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "date":
                    return true;
                case "lectureRoom":
                    return true;
                case "teacher":
                    return true;
                case "subject":
                    return true;
                case "canceled":
                    return true;
                case "periodNum":
                    return true;
                case "periodGroup":
                    return true;
                case "sppSchedule":
                    return true;
                case "bell":
                    return true;
                case "teacherVal":
                    return true;
                case "subjectVal":
                    return true;
                case "subjectLoadType":
                    return true;
                case "lectureRoomVal":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "date":
                    return Date.class;
                case "lectureRoom":
                    return String.class;
                case "teacher":
                    return String.class;
                case "subject":
                    return String.class;
                case "canceled":
                    return Boolean.class;
                case "periodNum":
                    return Integer.class;
                case "periodGroup":
                    return String.class;
                case "sppSchedule":
                    return SppSchedule.class;
                case "bell":
                    return ScheduleBellEntry.class;
                case "teacherVal":
                    return PpsEntry.class;
                case "subjectVal":
                    return EppRegistryElement.class;
                case "subjectLoadType":
                    return EppALoadType.class;
                case "lectureRoomVal":
                    return UniplacesPlace.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppSchedulePeriodFix> _dslPath = new Path<SppSchedulePeriodFix>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppSchedulePeriodFix");
    }
            

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getLectureRoom()
     */
    public static PropertyPath<String> lectureRoom()
    {
        return _dslPath.lectureRoom();
    }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getTeacher()
     */
    public static PropertyPath<String> teacher()
    {
        return _dslPath.teacher();
    }

    /**
     * @return Предмет.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getSubject()
     */
    public static PropertyPath<String> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Отменена. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#isCanceled()
     */
    public static PropertyPath<Boolean> canceled()
    {
        return _dslPath.canceled();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getPeriodNum()
     */
    public static PropertyPath<Integer> periodNum()
    {
        return _dslPath.periodNum();
    }

    /**
     * @return Подгруппа.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getPeriodGroup()
     */
    public static PropertyPath<String> periodGroup()
    {
        return _dslPath.periodGroup();
    }

    /**
     * @return Расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getSppSchedule()
     */
    public static SppSchedule.Path<SppSchedule> sppSchedule()
    {
        return _dslPath.sppSchedule();
    }

    /**
     * @return Пара.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getBell()
     */
    public static ScheduleBellEntry.Path<ScheduleBellEntry> bell()
    {
        return _dslPath.bell();
    }

    /**
     * @return Преподаватель (сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getTeacherVal()
     */
    public static PpsEntry.Path<PpsEntry> teacherVal()
    {
        return _dslPath.teacherVal();
    }

    /**
     * @return Предмет(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getSubjectVal()
     */
    public static EppRegistryElement.Path<EppRegistryElement> subjectVal()
    {
        return _dslPath.subjectVal();
    }

    /**
     * @return Тип нагрузки(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getSubjectLoadType()
     */
    public static EppALoadType.Path<EppALoadType> subjectLoadType()
    {
        return _dslPath.subjectLoadType();
    }

    /**
     * @return Аудитория(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getLectureRoomVal()
     */
    public static UniplacesPlace.Path<UniplacesPlace> lectureRoomVal()
    {
        return _dslPath.lectureRoomVal();
    }

    public static class Path<E extends SppSchedulePeriodFix> extends EntityPath<E>
    {
        private PropertyPath<Date> _date;
        private PropertyPath<String> _lectureRoom;
        private PropertyPath<String> _teacher;
        private PropertyPath<String> _subject;
        private PropertyPath<Boolean> _canceled;
        private PropertyPath<Integer> _periodNum;
        private PropertyPath<String> _periodGroup;
        private SppSchedule.Path<SppSchedule> _sppSchedule;
        private ScheduleBellEntry.Path<ScheduleBellEntry> _bell;
        private PpsEntry.Path<PpsEntry> _teacherVal;
        private EppRegistryElement.Path<EppRegistryElement> _subjectVal;
        private EppALoadType.Path<EppALoadType> _subjectLoadType;
        private UniplacesPlace.Path<UniplacesPlace> _lectureRoomVal;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(SppSchedulePeriodFixGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getLectureRoom()
     */
        public PropertyPath<String> lectureRoom()
        {
            if(_lectureRoom == null )
                _lectureRoom = new PropertyPath<String>(SppSchedulePeriodFixGen.P_LECTURE_ROOM, this);
            return _lectureRoom;
        }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getTeacher()
     */
        public PropertyPath<String> teacher()
        {
            if(_teacher == null )
                _teacher = new PropertyPath<String>(SppSchedulePeriodFixGen.P_TEACHER, this);
            return _teacher;
        }

    /**
     * @return Предмет.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getSubject()
     */
        public PropertyPath<String> subject()
        {
            if(_subject == null )
                _subject = new PropertyPath<String>(SppSchedulePeriodFixGen.P_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Отменена. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#isCanceled()
     */
        public PropertyPath<Boolean> canceled()
        {
            if(_canceled == null )
                _canceled = new PropertyPath<Boolean>(SppSchedulePeriodFixGen.P_CANCELED, this);
            return _canceled;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getPeriodNum()
     */
        public PropertyPath<Integer> periodNum()
        {
            if(_periodNum == null )
                _periodNum = new PropertyPath<Integer>(SppSchedulePeriodFixGen.P_PERIOD_NUM, this);
            return _periodNum;
        }

    /**
     * @return Подгруппа.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getPeriodGroup()
     */
        public PropertyPath<String> periodGroup()
        {
            if(_periodGroup == null )
                _periodGroup = new PropertyPath<String>(SppSchedulePeriodFixGen.P_PERIOD_GROUP, this);
            return _periodGroup;
        }

    /**
     * @return Расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getSppSchedule()
     */
        public SppSchedule.Path<SppSchedule> sppSchedule()
        {
            if(_sppSchedule == null )
                _sppSchedule = new SppSchedule.Path<SppSchedule>(L_SPP_SCHEDULE, this);
            return _sppSchedule;
        }

    /**
     * @return Пара.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getBell()
     */
        public ScheduleBellEntry.Path<ScheduleBellEntry> bell()
        {
            if(_bell == null )
                _bell = new ScheduleBellEntry.Path<ScheduleBellEntry>(L_BELL, this);
            return _bell;
        }

    /**
     * @return Преподаватель (сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getTeacherVal()
     */
        public PpsEntry.Path<PpsEntry> teacherVal()
        {
            if(_teacherVal == null )
                _teacherVal = new PpsEntry.Path<PpsEntry>(L_TEACHER_VAL, this);
            return _teacherVal;
        }

    /**
     * @return Предмет(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getSubjectVal()
     */
        public EppRegistryElement.Path<EppRegistryElement> subjectVal()
        {
            if(_subjectVal == null )
                _subjectVal = new EppRegistryElement.Path<EppRegistryElement>(L_SUBJECT_VAL, this);
            return _subjectVal;
        }

    /**
     * @return Тип нагрузки(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getSubjectLoadType()
     */
        public EppALoadType.Path<EppALoadType> subjectLoadType()
        {
            if(_subjectLoadType == null )
                _subjectLoadType = new EppALoadType.Path<EppALoadType>(L_SUBJECT_LOAD_TYPE, this);
            return _subjectLoadType;
        }

    /**
     * @return Аудитория(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix#getLectureRoomVal()
     */
        public UniplacesPlace.Path<UniplacesPlace> lectureRoomVal()
        {
            if(_lectureRoomVal == null )
                _lectureRoomVal = new UniplacesPlace.Path<UniplacesPlace>(L_LECTURE_ROOM_VAL, this);
            return _lectureRoomVal;
        }

        public Class getEntityClass()
        {
            return SppSchedulePeriodFix.class;
        }

        public String getEntityName()
        {
            return "sppSchedulePeriodFix";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
