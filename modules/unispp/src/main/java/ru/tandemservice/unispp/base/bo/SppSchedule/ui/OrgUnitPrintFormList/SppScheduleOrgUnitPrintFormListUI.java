/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.OrgUnitPrintFormList;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppSchedulePrintFormDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.PrintFormAdd.SppSchedulePrintFormAdd;
import ru.tandemservice.unispp.base.entity.SppSchedulePrintForm;

/**
 * @author nvankov
 * @since 9/25/13
 */
public class SppScheduleOrgUnitPrintFormListUI extends OrgUnitUIPresenter
{
    private BaseSearchListDataSource _printFormDS;

    public BaseSearchListDataSource getPrintFormDS()
    {
        if(null == _printFormDS)
            _printFormDS = _uiConfig.getDataSource(SppScheduleOrgUnitPrintFormList.PRINT_FORM_DS);
        return _printFormDS;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleOrgUnitPrintFormList.PRINT_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.put("orgUnitId", getOrgUnit().getId());
            dataSource.put(SppSchedulePrintFormDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
            dataSource.putAll(_uiSettings.getAsMap(true, "admin", "groups"));
        }
        if (SppScheduleOrgUnitPrintFormList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put("orgUnitId", getOrgUnit().getId());
        }
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickDownload()
    {
        SppSchedulePrintForm printForm = getPrintFormDS().getRecordById(getListenerParameterAsLong());
        byte[] content = printForm.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("xls").fileName("schedule.xls").document(content), true);
    }

    public void onClickAddSchedulePrint()
    {
        _uiActivation.asRegion(SppSchedulePrintFormAdd.class).parameter("orgUnitId", getOrgUnit().getId()).activate();
    }
}
