/* $Id: SppScheduleDailyOrgUnitList.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.LearningProcessList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.View.SppScheduleDailyView;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@Configuration
public class SppScheduleDailyLearningProcessList extends BusinessComponentManager
{
    public static final String SCHEDULE_DS = "scheduleDS";
    public static final String YEAR_PART_DS = "yearPartDS";
    public static final String COURSE_DS = "courseDS";
    public static final String GROUP_DS = "groupDS";
    public static final String EDU_LEVEL_DS = "eduLevelDS";
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String SEASON_DS = "seasonDS";
    public static final String SUBJECT_DS = "subjectDS";

    @Bean
    public ColumnListExtPoint scheduleCL()
    {
        return columnListExtPointBuilder(SCHEDULE_DS)
                .addColumn(publisherColumn("title", SppScheduleDaily.title()).businessComponent(SppScheduleDailyView.class).order().required(true))
                .addColumn(textColumn("group", SppScheduleDaily.group().title()).order().required(true))
                .addColumn(textColumn("yearPart", SppScheduleDaily.season().eppYearPart().title()))
                .addColumn(dateColumn("startDate", SppScheduleDaily.season().startDate()).order())
                .addColumn(dateColumn("endDate", SppScheduleDaily.season().endDate()).order())
                .addColumn(booleanColumn("approved", SppScheduleDaily.approved()))
                .addColumn(booleanColumn("archived", SppScheduleDaily.archived()))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SCHEDULE_DS, scheduleCL(), SppScheduleDailyManager.instance().sppScheduleDailyDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEAR_PART_DS, getName(), EppYearPart.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(COURSE_DS, SppScheduleManager.instance().courseComboDSHandler()))
                .addDataSource(selectDS(EDU_LEVEL_DS, SppScheduleManager.instance().eduLevelComboDSHandler()).addColumn(EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(selectDS(GROUP_DS, SppScheduleManager.instance().groupComboDSHandler()))
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()))
                .addDataSource(selectDS(SUBJECT_DS, SppScheduleManager.instance().registryDisciplineComboDSHandler()))
                .addDataSource(selectDS(SEASON_DS, SppScheduleDailyManager.instance().seasonComboDSHandler()).addColumn("title", null, new IFormatter<SppScheduleDailySeason>()
                {
                    @Override
                    public String format(SppScheduleDailySeason source)
                    {
                        return source.getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getStartDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndDate()) + ")";
                    }
                }))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                        OrgUnitToKindRelation.class,
                        OrgUnitToKindRelation.orgUnitKind().code().s(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING,
                        OrgUnitToKindRelation.L_ORG_UNIT, property("e")
                ));
            }
        }
                .order(OrgUnit.title())
                .filter(OrgUnit.title());
    }
}
