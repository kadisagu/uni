/* $Id$ */
package ru.tandemservice.unispp.base.vo;

/**
 * @author nvankov
 * @since 8/30/13
 */
public class SppSchedulePeriodTimeVO
{
    private Long _id;
    private int _startTime;     // Время начала (в минутах)
    private int _endTime;     // Время окончания (в минутах)

    public int getStartHour()
    {
        return _startTime / 60;
    }

    public void setStartHour(int startHour)
    {
        _startTime = _startTime % 60 + startHour * 60;
    }

    public int getStartMin()
    {
        return _startTime % 60;
    }

    public void setStartMin(int startMin)
    {
        _startTime = ((int) (_startTime / 60)) * 60 + startMin;
    }

    public int getEndHour()
    {
        return _endTime / 60;
    }

    public void setEndHour(int endHour)
    {
        _endTime = _endTime % 60 + endHour * 60;
    }

    public int getEndMin()
    {
        return _endTime % 60;
    }

    public void setEndMin(int endMin)
    {
        _endTime = ((int) (_endTime / 60)) * 60 + endMin;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    @Override
    public int hashCode()
    {
        return _id.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof SppSchedulePeriodTimeVO)
        {
            return  _id.equals(((SppSchedulePeriodTimeVO) obj).getId());
        }
        else throw new UnsupportedOperationException();
    }
}
