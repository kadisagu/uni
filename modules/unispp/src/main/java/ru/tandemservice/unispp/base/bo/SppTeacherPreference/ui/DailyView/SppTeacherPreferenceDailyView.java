/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.DailyView;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.logic.SppTeacherDailyPreferenceElementDSHandler;
import ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceElement;

/**
 * @author vnekrasov
 * @since 7/2/14
 */
@Configuration
public class SppTeacherPreferenceDailyView extends BusinessComponentManager
{
    public final static String DAILY_PREFERENCE_ELEMENTS_DS = "elementsDS";

    @Bean
    public ColumnListExtPoint TeacherDailyPreferenceElementsCL()
    {
        return columnListExtPointBuilder(DAILY_PREFERENCE_ELEMENTS_DS)
                .addColumn(dateColumn("startDate", SppTeacherDailyPreferenceElement.startDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(dateColumn("endDate", SppTeacherDailyPreferenceElement.endDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn("dayOfWeek", SppTeacherDailyPreferenceElement.dayOfWeek()))
                .addColumn(textColumn("bell", SppTeacherDailyPreferenceElement.bell().title()))
                .addColumn(textColumn("building", SppTeacherDailyPreferenceElement.building().title()))
                .addColumn(textColumn("lectureRoom", SppTeacherDailyPreferenceElement.lectureRoom().title()))
                .addColumn(booleanColumn("unwantedTime", SppTeacherDailyPreferenceElement.unwantedTime()))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onClickEditElement")
                        .permissionKey("ui:editPermissionKey")
                        .disabled("ui:elementEditDisabled"))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDeleteElement")
                        .permissionKey("ui:editPermissionKey")
                        .disabled("ui:elementDeleteDisabled"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(DAILY_PREFERENCE_ELEMENTS_DS, TeacherDailyPreferenceElementsCL(), teacherDailyPreferenceElementsDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler teacherDailyPreferenceElementsDSHandler()
    {
        return new SppTeacherDailyPreferenceElementDSHandler(getName());
    }
}
