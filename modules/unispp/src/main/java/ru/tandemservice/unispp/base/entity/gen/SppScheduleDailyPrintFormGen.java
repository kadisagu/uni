package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатная форма подневного расписания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppScheduleDailyPrintFormGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm";
    public static final String ENTITY_NAME = "sppScheduleDailyPrintForm";
    public static final int VERSION_HASH = -528230104;
    private static IEntityMeta ENTITY_META;

    public static final String P_CREATE_DATE = "createDate";
    public static final String P_GROUPS = "groups";
    public static final String P_TERM = "term";
    public static final String P_EDU_YEAR = "eduYear";
    public static final String P_HEADERS_O_O_P = "headersOOP";
    public static final String P_EDU_LEVELS = "eduLevels";
    public static final String P_COURSES = "courses";
    public static final String P_SESSION = "session";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_SEASON = "season";
    public static final String L_BELLS = "bells";
    public static final String L_CHIEF = "chief";
    public static final String L_ADMIN_O_O_P = "adminOOP";
    public static final String L_CHIEF_U_M_U = "chiefUMU";
    public static final String L_CONTENT = "content";
    public static final String L_CREATE_O_U = "createOU";
    public static final String P_FORM_WITH_TERR_TITLE = "formWithTerrTitle";

    private Date _createDate;     // Дата создания
    private String _groups;     // Группы
    private Long _term;     // Семестр
    private String _eduYear;     // Учебный год
    private String _headersOOP;     // Руководители ООП
    private String _eduLevels;     // Направление подготовки/специальности
    private String _courses;     // Курс
    private String _session;     // Экзаменационная сессия
    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private OrgUnit _territorialOrgUnit;     // Территориальное подразделение
    private DevelopForm _developForm;     // Форма освоения
    private SppScheduleDailySeason _season;     // Период расписания
    private ScheduleBell _bells;     // Звонковое расписание
    private EmployeePost _chief;     // Директор подразделения
    private EmployeePost _adminOOP;     // Администратор ООП
    private EmployeePost _chiefUMU;     // Начальник УМУ подразделения
    private DatabaseFile _content;     // Печатная форма
    private OrgUnit _createOU;     // Подразделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата создания.
     */
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата создания.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Группы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getGroups()
    {
        return _groups;
    }

    /**
     * @param groups Группы. Свойство не может быть null.
     */
    public void setGroups(String groups)
    {
        dirty(_groups, groups);
        _groups = groups;
    }

    /**
     * @return Семестр.
     */
    public Long getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр.
     */
    public void setTerm(Long term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год. Свойство не может быть null.
     */
    public void setEduYear(String eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    /**
     * @return Руководители ООП.
     */
    @Length(max=255)
    public String getHeadersOOP()
    {
        return _headersOOP;
    }

    /**
     * @param headersOOP Руководители ООП.
     */
    public void setHeadersOOP(String headersOOP)
    {
        dirty(_headersOOP, headersOOP);
        _headersOOP = headersOOP;
    }

    /**
     * @return Направление подготовки/специальности.
     */
    @Length(max=255)
    public String getEduLevels()
    {
        return _eduLevels;
    }

    /**
     * @param eduLevels Направление подготовки/специальности.
     */
    public void setEduLevels(String eduLevels)
    {
        dirty(_eduLevels, eduLevels);
        _eduLevels = eduLevels;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourses()
    {
        return _courses;
    }

    /**
     * @param courses Курс.
     */
    public void setCourses(String courses)
    {
        dirty(_courses, courses);
        _courses = courses;
    }

    /**
     * @return Экзаменационная сессия.
     */
    @Length(max=255)
    public String getSession()
    {
        return _session;
    }

    /**
     * @param session Экзаменационная сессия.
     */
    public void setSession(String session)
    {
        dirty(_session, session);
        _session = session;
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение. Свойство не может быть null.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подразделение.
     */
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подразделение.
     */
    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Форма освоения.
     */
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Период расписания. Свойство не может быть null.
     */
    @NotNull
    public SppScheduleDailySeason getSeason()
    {
        return _season;
    }

    /**
     * @param season Период расписания. Свойство не может быть null.
     */
    public void setSeason(SppScheduleDailySeason season)
    {
        dirty(_season, season);
        _season = season;
    }

    /**
     * @return Звонковое расписание.
     */
    public ScheduleBell getBells()
    {
        return _bells;
    }

    /**
     * @param bells Звонковое расписание.
     */
    public void setBells(ScheduleBell bells)
    {
        dirty(_bells, bells);
        _bells = bells;
    }

    /**
     * @return Директор подразделения.
     */
    public EmployeePost getChief()
    {
        return _chief;
    }

    /**
     * @param chief Директор подразделения.
     */
    public void setChief(EmployeePost chief)
    {
        dirty(_chief, chief);
        _chief = chief;
    }

    /**
     * @return Администратор ООП.
     */
    public EmployeePost getAdminOOP()
    {
        return _adminOOP;
    }

    /**
     * @param adminOOP Администратор ООП.
     */
    public void setAdminOOP(EmployeePost adminOOP)
    {
        dirty(_adminOOP, adminOOP);
        _adminOOP = adminOOP;
    }

    /**
     * @return Начальник УМУ подразделения.
     */
    public EmployeePost getChiefUMU()
    {
        return _chiefUMU;
    }

    /**
     * @param chiefUMU Начальник УМУ подразделения.
     */
    public void setChiefUMU(EmployeePost chiefUMU)
    {
        dirty(_chiefUMU, chiefUMU);
        _chiefUMU = chiefUMU;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getCreateOU()
    {
        return _createOU;
    }

    /**
     * @param createOU Подразделение. Свойство не может быть null.
     */
    public void setCreateOU(OrgUnit createOU)
    {
        dirty(_createOU, createOU);
        _createOU = createOU;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppScheduleDailyPrintFormGen)
        {
            setCreateDate(((SppScheduleDailyPrintForm)another).getCreateDate());
            setGroups(((SppScheduleDailyPrintForm)another).getGroups());
            setTerm(((SppScheduleDailyPrintForm)another).getTerm());
            setEduYear(((SppScheduleDailyPrintForm)another).getEduYear());
            setHeadersOOP(((SppScheduleDailyPrintForm)another).getHeadersOOP());
            setEduLevels(((SppScheduleDailyPrintForm)another).getEduLevels());
            setCourses(((SppScheduleDailyPrintForm)another).getCourses());
            setSession(((SppScheduleDailyPrintForm)another).getSession());
            setFormativeOrgUnit(((SppScheduleDailyPrintForm)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((SppScheduleDailyPrintForm)another).getTerritorialOrgUnit());
            setDevelopForm(((SppScheduleDailyPrintForm)another).getDevelopForm());
            setSeason(((SppScheduleDailyPrintForm)another).getSeason());
            setBells(((SppScheduleDailyPrintForm)another).getBells());
            setChief(((SppScheduleDailyPrintForm)another).getChief());
            setAdminOOP(((SppScheduleDailyPrintForm)another).getAdminOOP());
            setChiefUMU(((SppScheduleDailyPrintForm)another).getChiefUMU());
            setContent(((SppScheduleDailyPrintForm)another).getContent());
            setCreateOU(((SppScheduleDailyPrintForm)another).getCreateOU());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppScheduleDailyPrintFormGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppScheduleDailyPrintForm.class;
        }

        public T newInstance()
        {
            return (T) new SppScheduleDailyPrintForm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "createDate":
                    return obj.getCreateDate();
                case "groups":
                    return obj.getGroups();
                case "term":
                    return obj.getTerm();
                case "eduYear":
                    return obj.getEduYear();
                case "headersOOP":
                    return obj.getHeadersOOP();
                case "eduLevels":
                    return obj.getEduLevels();
                case "courses":
                    return obj.getCourses();
                case "session":
                    return obj.getSession();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "developForm":
                    return obj.getDevelopForm();
                case "season":
                    return obj.getSeason();
                case "bells":
                    return obj.getBells();
                case "chief":
                    return obj.getChief();
                case "adminOOP":
                    return obj.getAdminOOP();
                case "chiefUMU":
                    return obj.getChiefUMU();
                case "content":
                    return obj.getContent();
                case "createOU":
                    return obj.getCreateOU();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "groups":
                    obj.setGroups((String) value);
                    return;
                case "term":
                    obj.setTerm((Long) value);
                    return;
                case "eduYear":
                    obj.setEduYear((String) value);
                    return;
                case "headersOOP":
                    obj.setHeadersOOP((String) value);
                    return;
                case "eduLevels":
                    obj.setEduLevels((String) value);
                    return;
                case "courses":
                    obj.setCourses((String) value);
                    return;
                case "session":
                    obj.setSession((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((OrgUnit) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "season":
                    obj.setSeason((SppScheduleDailySeason) value);
                    return;
                case "bells":
                    obj.setBells((ScheduleBell) value);
                    return;
                case "chief":
                    obj.setChief((EmployeePost) value);
                    return;
                case "adminOOP":
                    obj.setAdminOOP((EmployeePost) value);
                    return;
                case "chiefUMU":
                    obj.setChiefUMU((EmployeePost) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "createOU":
                    obj.setCreateOU((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "createDate":
                        return true;
                case "groups":
                        return true;
                case "term":
                        return true;
                case "eduYear":
                        return true;
                case "headersOOP":
                        return true;
                case "eduLevels":
                        return true;
                case "courses":
                        return true;
                case "session":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "developForm":
                        return true;
                case "season":
                        return true;
                case "bells":
                        return true;
                case "chief":
                        return true;
                case "adminOOP":
                        return true;
                case "chiefUMU":
                        return true;
                case "content":
                        return true;
                case "createOU":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "createDate":
                    return true;
                case "groups":
                    return true;
                case "term":
                    return true;
                case "eduYear":
                    return true;
                case "headersOOP":
                    return true;
                case "eduLevels":
                    return true;
                case "courses":
                    return true;
                case "session":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "developForm":
                    return true;
                case "season":
                    return true;
                case "bells":
                    return true;
                case "chief":
                    return true;
                case "adminOOP":
                    return true;
                case "chiefUMU":
                    return true;
                case "content":
                    return true;
                case "createOU":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "createDate":
                    return Date.class;
                case "groups":
                    return String.class;
                case "term":
                    return Long.class;
                case "eduYear":
                    return String.class;
                case "headersOOP":
                    return String.class;
                case "eduLevels":
                    return String.class;
                case "courses":
                    return String.class;
                case "session":
                    return String.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "territorialOrgUnit":
                    return OrgUnit.class;
                case "developForm":
                    return DevelopForm.class;
                case "season":
                    return SppScheduleDailySeason.class;
                case "bells":
                    return ScheduleBell.class;
                case "chief":
                    return EmployeePost.class;
                case "adminOOP":
                    return EmployeePost.class;
                case "chiefUMU":
                    return EmployeePost.class;
                case "content":
                    return DatabaseFile.class;
                case "createOU":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppScheduleDailyPrintForm> _dslPath = new Path<SppScheduleDailyPrintForm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppScheduleDailyPrintForm");
    }
            

    /**
     * @return Дата создания.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Группы. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getGroups()
     */
    public static PropertyPath<String> groups()
    {
        return _dslPath.groups();
    }

    /**
     * @return Семестр.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getTerm()
     */
    public static PropertyPath<Long> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getEduYear()
     */
    public static PropertyPath<String> eduYear()
    {
        return _dslPath.eduYear();
    }

    /**
     * @return Руководители ООП.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getHeadersOOP()
     */
    public static PropertyPath<String> headersOOP()
    {
        return _dslPath.headersOOP();
    }

    /**
     * @return Направление подготовки/специальности.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getEduLevels()
     */
    public static PropertyPath<String> eduLevels()
    {
        return _dslPath.eduLevels();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getCourses()
     */
    public static PropertyPath<String> courses()
    {
        return _dslPath.courses();
    }

    /**
     * @return Экзаменационная сессия.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getSession()
     */
    public static PropertyPath<String> session()
    {
        return _dslPath.session();
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подразделение.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getTerritorialOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Период расписания. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getSeason()
     */
    public static SppScheduleDailySeason.Path<SppScheduleDailySeason> season()
    {
        return _dslPath.season();
    }

    /**
     * @return Звонковое расписание.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getBells()
     */
    public static ScheduleBell.Path<ScheduleBell> bells()
    {
        return _dslPath.bells();
    }

    /**
     * @return Директор подразделения.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getChief()
     */
    public static EmployeePost.Path<EmployeePost> chief()
    {
        return _dslPath.chief();
    }

    /**
     * @return Администратор ООП.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getAdminOOP()
     */
    public static EmployeePost.Path<EmployeePost> adminOOP()
    {
        return _dslPath.adminOOP();
    }

    /**
     * @return Начальник УМУ подразделения.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getChiefUMU()
     */
    public static EmployeePost.Path<EmployeePost> chiefUMU()
    {
        return _dslPath.chiefUMU();
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getCreateOU()
     */
    public static OrgUnit.Path<OrgUnit> createOU()
    {
        return _dslPath.createOU();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getFormWithTerrTitle()
     */
    public static SupportedPropertyPath<String> formWithTerrTitle()
    {
        return _dslPath.formWithTerrTitle();
    }

    public static class Path<E extends SppScheduleDailyPrintForm> extends EntityPath<E>
    {
        private PropertyPath<Date> _createDate;
        private PropertyPath<String> _groups;
        private PropertyPath<Long> _term;
        private PropertyPath<String> _eduYear;
        private PropertyPath<String> _headersOOP;
        private PropertyPath<String> _eduLevels;
        private PropertyPath<String> _courses;
        private PropertyPath<String> _session;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private OrgUnit.Path<OrgUnit> _territorialOrgUnit;
        private DevelopForm.Path<DevelopForm> _developForm;
        private SppScheduleDailySeason.Path<SppScheduleDailySeason> _season;
        private ScheduleBell.Path<ScheduleBell> _bells;
        private EmployeePost.Path<EmployeePost> _chief;
        private EmployeePost.Path<EmployeePost> _adminOOP;
        private EmployeePost.Path<EmployeePost> _chiefUMU;
        private DatabaseFile.Path<DatabaseFile> _content;
        private OrgUnit.Path<OrgUnit> _createOU;
        private SupportedPropertyPath<String> _formWithTerrTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата создания.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(SppScheduleDailyPrintFormGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Группы. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getGroups()
     */
        public PropertyPath<String> groups()
        {
            if(_groups == null )
                _groups = new PropertyPath<String>(SppScheduleDailyPrintFormGen.P_GROUPS, this);
            return _groups;
        }

    /**
     * @return Семестр.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getTerm()
     */
        public PropertyPath<Long> term()
        {
            if(_term == null )
                _term = new PropertyPath<Long>(SppScheduleDailyPrintFormGen.P_TERM, this);
            return _term;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getEduYear()
     */
        public PropertyPath<String> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new PropertyPath<String>(SppScheduleDailyPrintFormGen.P_EDU_YEAR, this);
            return _eduYear;
        }

    /**
     * @return Руководители ООП.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getHeadersOOP()
     */
        public PropertyPath<String> headersOOP()
        {
            if(_headersOOP == null )
                _headersOOP = new PropertyPath<String>(SppScheduleDailyPrintFormGen.P_HEADERS_O_O_P, this);
            return _headersOOP;
        }

    /**
     * @return Направление подготовки/специальности.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getEduLevels()
     */
        public PropertyPath<String> eduLevels()
        {
            if(_eduLevels == null )
                _eduLevels = new PropertyPath<String>(SppScheduleDailyPrintFormGen.P_EDU_LEVELS, this);
            return _eduLevels;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getCourses()
     */
        public PropertyPath<String> courses()
        {
            if(_courses == null )
                _courses = new PropertyPath<String>(SppScheduleDailyPrintFormGen.P_COURSES, this);
            return _courses;
        }

    /**
     * @return Экзаменационная сессия.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getSession()
     */
        public PropertyPath<String> session()
        {
            if(_session == null )
                _session = new PropertyPath<String>(SppScheduleDailyPrintFormGen.P_SESSION, this);
            return _session;
        }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подразделение.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getTerritorialOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new OrgUnit.Path<OrgUnit>(L_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Период расписания. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getSeason()
     */
        public SppScheduleDailySeason.Path<SppScheduleDailySeason> season()
        {
            if(_season == null )
                _season = new SppScheduleDailySeason.Path<SppScheduleDailySeason>(L_SEASON, this);
            return _season;
        }

    /**
     * @return Звонковое расписание.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getBells()
     */
        public ScheduleBell.Path<ScheduleBell> bells()
        {
            if(_bells == null )
                _bells = new ScheduleBell.Path<ScheduleBell>(L_BELLS, this);
            return _bells;
        }

    /**
     * @return Директор подразделения.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getChief()
     */
        public EmployeePost.Path<EmployeePost> chief()
        {
            if(_chief == null )
                _chief = new EmployeePost.Path<EmployeePost>(L_CHIEF, this);
            return _chief;
        }

    /**
     * @return Администратор ООП.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getAdminOOP()
     */
        public EmployeePost.Path<EmployeePost> adminOOP()
        {
            if(_adminOOP == null )
                _adminOOP = new EmployeePost.Path<EmployeePost>(L_ADMIN_O_O_P, this);
            return _adminOOP;
        }

    /**
     * @return Начальник УМУ подразделения.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getChiefUMU()
     */
        public EmployeePost.Path<EmployeePost> chiefUMU()
        {
            if(_chiefUMU == null )
                _chiefUMU = new EmployeePost.Path<EmployeePost>(L_CHIEF_U_M_U, this);
            return _chiefUMU;
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getCreateOU()
     */
        public OrgUnit.Path<OrgUnit> createOU()
        {
            if(_createOU == null )
                _createOU = new OrgUnit.Path<OrgUnit>(L_CREATE_O_U, this);
            return _createOU;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm#getFormWithTerrTitle()
     */
        public SupportedPropertyPath<String> formWithTerrTitle()
        {
            if(_formWithTerrTitle == null )
                _formWithTerrTitle = new SupportedPropertyPath<String>(SppScheduleDailyPrintFormGen.P_FORM_WITH_TERR_TITLE, this);
            return _formWithTerrTitle;
        }

        public Class getEntityClass()
        {
            return SppScheduleDailyPrintForm.class;
        }

        public String getEntityName()
        {
            return "sppScheduleDailyPrintForm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFormWithTerrTitle();
}
