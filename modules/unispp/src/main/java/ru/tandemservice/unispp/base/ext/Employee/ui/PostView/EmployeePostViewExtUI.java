/* $Id:$ */
package ru.tandemservice.unispp.base.ext.Employee.ui.PostView;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostViewUI;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;

/**
 * @author Victor Nekrasov
 * @since 29.05.2014
 */
public class EmployeePostViewExtUI extends UIAddon
{

    public EmployeePostViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public boolean isVisible()
    {
        EmployeePost employeePost = ((EmployeePostViewUI) getPresenter()).getEmployeePost();
        if (employeePost.getPostRelation() == null)
            return false;
        return employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getEmployeeType().getCode().equals(EmployeeTypeCodes.EDU_STAFF);
    }

    public ParametersMap getParameters()
    {
        return ParametersMap.createWith("employeePostId", ((EmployeePostViewUI) getPresenter()).getEmployeePost().getId());
    }
}
