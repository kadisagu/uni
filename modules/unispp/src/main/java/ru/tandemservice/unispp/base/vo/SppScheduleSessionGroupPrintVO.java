/* $Id$ */
package ru.tandemservice.unispp.base.vo;

import com.google.common.collect.Maps;
import org.joda.time.LocalDate;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionEvent;

import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 9/25/13
 */
public class SppScheduleSessionGroupPrintVO
{
    private SppScheduleSession _schedule;
    private int _columnNum;
    private int _column;
    private int _totalColumnSize;
    private Map<LocalDate, List<SppScheduleSessionEvent>> _eventsMap = Maps.newHashMap();


    public SppScheduleSessionGroupPrintVO(SppScheduleSession schedule)
    {
        _schedule = schedule;
    }

    public SppScheduleSession getSchedule()
    {
        return _schedule;
    }

    public void setSchedule(SppScheduleSession schedule)
    {
        _schedule = schedule;
    }

    public int getColumnNum()
    {
        return _columnNum;
    }

    public void setColumnNum(int columnNum)
    {
        _columnNum = columnNum;
    }

    public int getColumn()
    {
        return _column;
    }

    public void setColumn(int column)
    {
        _column = column;
    }

    public int getTotalColumnSize()
    {
        return _totalColumnSize;
    }

    public void setTotalColumnSize(int totalColumnSize)
    {
        _totalColumnSize = totalColumnSize;
    }

    public Map<LocalDate, List<SppScheduleSessionEvent>> getEventsMap()
    {
        return _eventsMap;
    }

    public void setEventsMap(Map<LocalDate, List<SppScheduleSessionEvent>> eventsMap)
    {
        _eventsMap = eventsMap;
    }
}
