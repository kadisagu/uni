/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.View.SppTeacherPreferenceView;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList;

/**
 * @author vnekrasov
 * @since 6/23/14
 */
@Input({
        @Bind(key = "teacherPreferenceId", binding = "teacherPreferenceId"),
        @Bind(key = "employeePostId", binding = "employeePostId")
})
public class SppTeacherPreferenceAddEditUI extends UIPresenter
{
    private Long _teacherPreferenceId;
    private SppTeacherPreferenceList _teacherPreference;
    private ScheduleBell _oldBells;
    private Long _employeePostId;
    private boolean _oldDiffEven;

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        _employeePostId = employeePostId;
    }

    public Long getTeacherPreferenceId()
    {
        return _teacherPreferenceId;
    }

    public void setTeacherPreferenceId(Long teacherPreferenceId)
    {
        _teacherPreferenceId = teacherPreferenceId;
    }

    public SppTeacherPreferenceList getTeacherPreference()
    {
        return _teacherPreference;
    }

    public void setTeacherPreference(SppTeacherPreferenceList teacherPreference)
    {
        _teacherPreference = teacherPreference;
    }

    public Boolean getGroupDisabled()
    {
        return  null != _teacherPreferenceId;
    }

    public Boolean getAddForm()
    {
        return null == _teacherPreferenceId;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _teacherPreference)
        {
            if (null != _teacherPreferenceId)
            {
                _teacherPreference = DataAccessServices.dao().get(_teacherPreferenceId);
                _oldBells = _teacherPreference.getBells();
                _oldDiffEven = _teacherPreference.isDiffEven();
            }
            else
            {
                _teacherPreference = new SppTeacherPreferenceList();
                if (_employeePostId != null)
                {
                    Person person = UniDaoFacade.getCoreDao().getProperty(EmployeePost.class, EmployeePost.person().s(), EmployeePost.P_ID, _employeePostId);
                    _teacherPreference.setTeacher(person);
                }
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppTeacherPreferenceAddEdit.SEASON_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppTeacherPreferenceManager.EMPLOYEE, _teacherPreference.getTeacher());
            dataSource.put(SppTeacherPreferenceManager.EDITED, _teacherPreferenceId);
        }
    }

    public void onClickApply()
    {
        SppTeacherPreferenceManager.instance().dao().saveOrUpdateTeacherPreferenceList(_teacherPreference, _oldBells, _oldDiffEven);
        if(getAddForm())
        {
            deactivate();
//            _uiActivation.asDesktopRoot(SppTeacherPreferenceElementAdd.class).parameter("teacherPreferenceId", _teacherPreference.getId()).activate();
            _uiActivation.asDesktopRoot(SppTeacherPreferenceView.class).parameter(PUBLISHER_ID, _teacherPreference.getId()).activate();
        }
        else deactivate();
    }
}
