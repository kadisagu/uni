/* $Id: */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyWarningLog;

/**
 * @author vnekrasov
 * @since 7/14/15
 */
public class SppScheduleDailyWarningLogDSHandler extends DefaultSearchDataSourceHandler
{
    public SppScheduleDailyWarningLogDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long scheduleDailyId = context.get("scheduleDailyId");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleDailyWarningLog.class, "w");

        if(null != scheduleDailyId)
            builder.where(DQLExpressions.eq(DQLExpressions.property("w", SppScheduleDailyWarningLog.sppScheduleDaily().id()), DQLExpressions.value(scheduleDailyId)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order().build();
    }
}
