/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.PrintFormAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.GroupComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppSchedulesComboDSHandler;
import ru.tandemservice.unispp.base.entity.SppSchedulePrintForm;
import ru.tandemservice.unispp.base.vo.SppSchedulePrintDataVO;

/**
 * @author nvankov
 * @since 9/25/13
 */
@Input({
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class SppSchedulePrintFormAddUI extends UIPresenter
{
    private Long _orgUnitId;

    private SppSchedulePrintDataVO _printData;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public SppSchedulePrintDataVO getPrintData()
    {
        return _printData;
    }

    public void setPrintData(SppSchedulePrintDataVO printData)
    {
        _printData = printData;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _printData)
        {
            _printData = new SppSchedulePrintDataVO();
            OrgUnit currOrgUnit = DataAccessServices.dao().get(_orgUnitId);
            _printData.setCurrentOrgUnit(currOrgUnit);
            _printData.setFormativeOrgUnit(currOrgUnit);
            _printData.setEducationYear(DataAccessServices.dao().get(EducationYear.class, EducationYear.current(), true));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("scheduleData", _printData);
        if (SppSchedulePrintFormAdd.FORMATIVE_OU_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.FORMATIVE_ORG_UNIT);
        }
        if (SppSchedulePrintFormAdd.EDU_LEVEL_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.EDUCATION_LEVEL);
        }
        if (SppSchedulePrintFormAdd.DEVELOP_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.DEVELOP_FORM);
        }
        if (SppSchedulePrintFormAdd.COURSE_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.COURSE);
        }
        if (SppSchedulePrintFormAdd.SEASON_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", SppSchedulesComboDSHandler.Columns.SEASON);
        }
        if (SppSchedulePrintFormAdd.BELLS_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", SppSchedulesComboDSHandler.Columns.BELLS);
        }
        if (SppSchedulePrintFormAdd.SCHEDULES_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", SppSchedulesComboDSHandler.Columns.SCHEDULE);
        }
    }

    public void onChangeFormOU()
    {
        if (null != _printData.getFormativeOrgUnit())
            _printData.setChief((EmployeePost) _printData.getFormativeOrgUnit().getHead());
    }

    public void onClickApply()
    {
        SppSchedulePrintForm printForm = SppScheduleManager.instance().dao().savePrintForm(_printData);
        byte[] content = printForm.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("xls").fileName("schedule.xls").document(content), true);
        deactivate();
    }
}
