/* $Id:$ */
package ru.tandemservice.unispp.component.registry.DisciplineRegistry.AddEdit;

import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck;

/**
 * @author Victor Nekrasov
 * @since 19.05.2014
 */
public class DAO extends ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit.DAO
{
    @Override
    public void save(final ru.tandemservice.uniepp.component.registry.base.AddEdit.Model<EppRegistryDiscipline> model) {
        super.save(model);
        EppRegistryDiscipline discipline = model.getElement();

        if (!existsEntity(SppRegElementLoadCheck.class, SppRegElementLoadCheck.L_REGISTRY_ELEMENT, discipline))
        {
            for (EppALoadType loadType : getList(EppALoadType.class))
            {
                SppRegElementLoadCheck sppRegElementLoadCheck = new SppRegElementLoadCheck();
                sppRegElementLoadCheck.setRegistryElement(discipline);
                sppRegElementLoadCheck.setEppALoadType(loadType);
                sppRegElementLoadCheck.setCheckValue(true);
                save(sppRegElementLoadCheck);
            }
        }
    }
}
