/* $Id$ */
package ru.tandemservice.unispp.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;

/**
 * @author Alexey Lopatin
 * @since 20.11.2014
 */
public class SppScheduleSessionDisciplineVO extends DataWrapper
{
    private EppRegistryDiscipline _discipline;
    private EppFControlActionType _formControl;

    public SppScheduleSessionDisciplineVO(EppRegistryDiscipline discipline, EppFControlActionType formControl)
    {
        super(discipline.getId() + formControl.getId(), discipline.getTitle() + " (" + formControl.getTitle() + ")");
        _discipline = discipline;
        _formControl = formControl;
    }

    public EppRegistryDiscipline getDiscipline()
    {
        return _discipline;
    }

    public EppFControlActionType getFormControl()
    {
        return _formControl;
    }
}
