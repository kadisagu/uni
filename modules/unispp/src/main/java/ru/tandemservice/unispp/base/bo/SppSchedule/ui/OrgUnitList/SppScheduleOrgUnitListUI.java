/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.OrgUnitList;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddEdit.SppScheduleAddEdit;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddSchedulesSelectGroupsList.SppScheduleAddSchedulesSelectGroupsList;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddSchedulesSelectGroupsList.SppScheduleAddSchedulesSelectGroupsListUI;
import ru.tandemservice.unispp.base.entity.SppSchedule;

import java.util.Map;

/**
 * @author nvankov
 * @since 9/4/13
 */
public class SppScheduleOrgUnitListUI extends OrgUnitUIPresenter
{
    private BaseSearchListDataSource _scheduleDS;

    public BaseSearchListDataSource getScheduleDS()
    {
        if(null == _scheduleDS)
            _scheduleDS = _uiConfig.getDataSource(SppScheduleOrgUnitList.SCHEDULE_DS);
        return _scheduleDS;
    }

    public Boolean getEntityEditDisabled()
    {
        SppSchedule schedule = getScheduleDS().getCurrent();
        return schedule.isApproved() || schedule.isArchived();
    }

    public Boolean getEntityDeleteDisabled()
    {
        SppSchedule schedule = getScheduleDS().getCurrent();
        return schedule.isApproved();
    }

    public Map<String, Object> getParametersMap()
    {
        return new ParametersMap()
                .add("orgUnitId", getOrgUnit().getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("orgUnitId", getOrgUnit().getId());
        if (SppScheduleOrgUnitList.SCHEDULE_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleDSHandler.PROP_TITLE, _uiSettings.get("title"));
            dataSource.put(SppScheduleDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
            dataSource.putAll(_uiSettings.getAsMap(true, "courses", "eduLevels", "groups"));
        }
        if(SppScheduleOrgUnitList.EDU_LEVEL_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "courses"));
        }
        if(SppScheduleOrgUnitList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "courses", "eduLevels"));
        }
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(SppScheduleAddEdit.class).parameter("scheduleId", getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        SppScheduleManager.instance().dao().deleteSchedule(getListenerParameterAsLong());
    }

    public void onClickAddSchedule()
    {
        _uiActivation.asRegionDialog(SppScheduleAddEdit.class)
                .parameter("orgUnitId", getOrgUnit().getId())
                .activate();
    }

    public void onClickAddSchedules()
    {
        _uiActivation.asRegion(SppScheduleAddSchedulesSelectGroupsList.class)
                .parameter(SppScheduleAddSchedulesSelectGroupsListUI.BIND_ORG_UNIT_ID, getOrgUnit().getId())
                .activate();
    }
}
