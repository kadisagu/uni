/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.MassAddingExercise;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 11.10.2016
 */
@Configuration
public class SppScheduleMassAddingExercise extends BusinessComponentManager
{
    // event
    public static final String BELL_SCHEDULE_ENTRY_DS = "bellScheduleEntryDS";
    public static final String SUBJECT_DS = "subjectDS";
    public static final String TEACHER_DS = "teacherDS";
    public static final String LECTURE_ROOM_TYPE_DS = "lectureRoomTypeDS";
    public static final String LECTURE_ROOM_DS = "lectureRoomDS";

    // properties
    public static final String PROP_BELL_SCHEDULE_ID = "bellScheduleId";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(BELL_SCHEDULE_ENTRY_DS, bellScheduleEntryDSHandler()))
                .addDataSource(selectDS(SUBJECT_DS, SppScheduleManager.instance().subjectComboDSHandler()))
                .addDataSource(selectDS(TEACHER_DS, SppScheduleManager.instance().ppsComboDSHandler()).addColumn("fio", PpsEntry.titleFioInfoOrgUnit().s()))
                .addDataSource(selectDS(LECTURE_ROOM_TYPE_DS, SppScheduleManager.instance().placePurposeComboDSHandler()))
                .addDataSource(selectDS(LECTURE_ROOM_DS, SppScheduleManager.instance().lectureRoomComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler bellScheduleEntryDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), ScheduleBellEntry.class, "")
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                // super.prepareConditions(ep);
                final DQLSelectBuilder dqlBuilder = ep.dqlBuilder;
                final ExecutionContext context = ep.context;

                final Long bellScheduleId = context.get(PROP_BELL_SCHEDULE_ID);
                if (bellScheduleId != null)
                    dqlBuilder.where(eq(property("e", ScheduleBellEntry.schedule().id()), value(bellScheduleId)));
                else
                    dqlBuilder.where(nothing());
            }

            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                final DSOutput output = super.execute(input, context);
                final List<DataWrapper> wrappedList = DataWrapper.wrap(output);
                for (final DataWrapper wrapper : wrappedList)
                {
                    final ScheduleBellEntry scheduleEntry = wrapper.getWrapped();
                    wrapper.setProperty(DataWrapper.TITLE, scheduleEntry.getTitle());
                }
                return output;
            }
        };
    }
}
