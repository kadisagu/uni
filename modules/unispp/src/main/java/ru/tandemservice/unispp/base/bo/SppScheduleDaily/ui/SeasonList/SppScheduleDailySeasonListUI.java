/* $Id: SppScheduleDailySeasonListUI.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.SeasonList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.SeasonAddEdit.SppScheduleDailySeasonAddEdit;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
public class SppScheduleDailySeasonListUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleDailySeasonList.SCHEDULE_DAILY_SEASON_DS.equals(dataSource.getName()))
        {
            dataSource.put("title", _uiSettings.get("title"));
        }
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(SppScheduleDailySeasonAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickAdd()
    {
        _uiActivation.asRegionDialog(SppScheduleDailySeasonAddEdit.class).activate();
    }
}
