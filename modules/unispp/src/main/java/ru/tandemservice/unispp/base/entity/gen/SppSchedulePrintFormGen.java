package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.entity.SppSchedulePrintForm;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатная форма расписания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppSchedulePrintFormGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppSchedulePrintForm";
    public static final String ENTITY_NAME = "sppSchedulePrintForm";
    public static final int VERSION_HASH = -546684855;
    private static IEntityMeta ENTITY_META;

    public static final String P_CREATE_DATE = "createDate";
    public static final String P_GROUPS = "groups";
    public static final String P_EDU_YEAR = "eduYear";
    public static final String P_EDU_LEVELS = "eduLevels";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_COURSE = "course";
    public static final String L_SEASON = "season";
    public static final String L_BELLS = "bells";
    public static final String L_CHIEF = "chief";
    public static final String L_CONTENT = "content";
    public static final String L_CREATE_O_U = "createOU";

    private Date _createDate;     // Дата создания
    private String _groups;     // Группы
    private String _eduYear;     // Учебный год
    private String _eduLevels;     // Направление подготовки/специальности
    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private DevelopForm _developForm;     // Форма освоения
    private Course _course;     // Курс
    private SppScheduleSeason _season;     // Период расписания
    private ScheduleBell _bells;     // Звонковое расписание
    private EmployeePost _chief;     // Руководитель формирующего подразделения
    private DatabaseFile _content;     // Печатная форма
    private OrgUnit _createOU;     // Подразделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата создания.
     */
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата создания.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Группы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getGroups()
    {
        return _groups;
    }

    /**
     * @param groups Группы. Свойство не может быть null.
     */
    public void setGroups(String groups)
    {
        dirty(_groups, groups);
        _groups = groups;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год. Свойство не может быть null.
     */
    public void setEduYear(String eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    /**
     * @return Направление подготовки/специальности.
     */
    @Length(max=255)
    public String getEduLevels()
    {
        return _eduLevels;
    }

    /**
     * @param eduLevels Направление подготовки/специальности.
     */
    public void setEduLevels(String eduLevels)
    {
        dirty(_eduLevels, eduLevels);
        _eduLevels = eduLevels;
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение. Свойство не может быть null.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Форма освоения.
     */
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Период расписания. Свойство не может быть null.
     */
    @NotNull
    public SppScheduleSeason getSeason()
    {
        return _season;
    }

    /**
     * @param season Период расписания. Свойство не может быть null.
     */
    public void setSeason(SppScheduleSeason season)
    {
        dirty(_season, season);
        _season = season;
    }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     */
    @NotNull
    public ScheduleBell getBells()
    {
        return _bells;
    }

    /**
     * @param bells Звонковое расписание. Свойство не может быть null.
     */
    public void setBells(ScheduleBell bells)
    {
        dirty(_bells, bells);
        _bells = bells;
    }

    /**
     * @return Руководитель формирующего подразделения.
     */
    public EmployeePost getChief()
    {
        return _chief;
    }

    /**
     * @param chief Руководитель формирующего подразделения.
     */
    public void setChief(EmployeePost chief)
    {
        dirty(_chief, chief);
        _chief = chief;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getCreateOU()
    {
        return _createOU;
    }

    /**
     * @param createOU Подразделение. Свойство не может быть null.
     */
    public void setCreateOU(OrgUnit createOU)
    {
        dirty(_createOU, createOU);
        _createOU = createOU;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppSchedulePrintFormGen)
        {
            setCreateDate(((SppSchedulePrintForm)another).getCreateDate());
            setGroups(((SppSchedulePrintForm)another).getGroups());
            setEduYear(((SppSchedulePrintForm)another).getEduYear());
            setEduLevels(((SppSchedulePrintForm)another).getEduLevels());
            setFormativeOrgUnit(((SppSchedulePrintForm)another).getFormativeOrgUnit());
            setDevelopForm(((SppSchedulePrintForm)another).getDevelopForm());
            setCourse(((SppSchedulePrintForm)another).getCourse());
            setSeason(((SppSchedulePrintForm)another).getSeason());
            setBells(((SppSchedulePrintForm)another).getBells());
            setChief(((SppSchedulePrintForm)another).getChief());
            setContent(((SppSchedulePrintForm)another).getContent());
            setCreateOU(((SppSchedulePrintForm)another).getCreateOU());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppSchedulePrintFormGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppSchedulePrintForm.class;
        }

        public T newInstance()
        {
            return (T) new SppSchedulePrintForm();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "createDate":
                    return obj.getCreateDate();
                case "groups":
                    return obj.getGroups();
                case "eduYear":
                    return obj.getEduYear();
                case "eduLevels":
                    return obj.getEduLevels();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "developForm":
                    return obj.getDevelopForm();
                case "course":
                    return obj.getCourse();
                case "season":
                    return obj.getSeason();
                case "bells":
                    return obj.getBells();
                case "chief":
                    return obj.getChief();
                case "content":
                    return obj.getContent();
                case "createOU":
                    return obj.getCreateOU();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "groups":
                    obj.setGroups((String) value);
                    return;
                case "eduYear":
                    obj.setEduYear((String) value);
                    return;
                case "eduLevels":
                    obj.setEduLevels((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "season":
                    obj.setSeason((SppScheduleSeason) value);
                    return;
                case "bells":
                    obj.setBells((ScheduleBell) value);
                    return;
                case "chief":
                    obj.setChief((EmployeePost) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "createOU":
                    obj.setCreateOU((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "createDate":
                        return true;
                case "groups":
                        return true;
                case "eduYear":
                        return true;
                case "eduLevels":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "developForm":
                        return true;
                case "course":
                        return true;
                case "season":
                        return true;
                case "bells":
                        return true;
                case "chief":
                        return true;
                case "content":
                        return true;
                case "createOU":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "createDate":
                    return true;
                case "groups":
                    return true;
                case "eduYear":
                    return true;
                case "eduLevels":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "developForm":
                    return true;
                case "course":
                    return true;
                case "season":
                    return true;
                case "bells":
                    return true;
                case "chief":
                    return true;
                case "content":
                    return true;
                case "createOU":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "createDate":
                    return Date.class;
                case "groups":
                    return String.class;
                case "eduYear":
                    return String.class;
                case "eduLevels":
                    return String.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "developForm":
                    return DevelopForm.class;
                case "course":
                    return Course.class;
                case "season":
                    return SppScheduleSeason.class;
                case "bells":
                    return ScheduleBell.class;
                case "chief":
                    return EmployeePost.class;
                case "content":
                    return DatabaseFile.class;
                case "createOU":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppSchedulePrintForm> _dslPath = new Path<SppSchedulePrintForm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppSchedulePrintForm");
    }
            

    /**
     * @return Дата создания.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Группы. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getGroups()
     */
    public static PropertyPath<String> groups()
    {
        return _dslPath.groups();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getEduYear()
     */
    public static PropertyPath<String> eduYear()
    {
        return _dslPath.eduYear();
    }

    /**
     * @return Направление подготовки/специальности.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getEduLevels()
     */
    public static PropertyPath<String> eduLevels()
    {
        return _dslPath.eduLevels();
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Период расписания. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getSeason()
     */
    public static SppScheduleSeason.Path<SppScheduleSeason> season()
    {
        return _dslPath.season();
    }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getBells()
     */
    public static ScheduleBell.Path<ScheduleBell> bells()
    {
        return _dslPath.bells();
    }

    /**
     * @return Руководитель формирующего подразделения.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getChief()
     */
    public static EmployeePost.Path<EmployeePost> chief()
    {
        return _dslPath.chief();
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getCreateOU()
     */
    public static OrgUnit.Path<OrgUnit> createOU()
    {
        return _dslPath.createOU();
    }

    public static class Path<E extends SppSchedulePrintForm> extends EntityPath<E>
    {
        private PropertyPath<Date> _createDate;
        private PropertyPath<String> _groups;
        private PropertyPath<String> _eduYear;
        private PropertyPath<String> _eduLevels;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private DevelopForm.Path<DevelopForm> _developForm;
        private Course.Path<Course> _course;
        private SppScheduleSeason.Path<SppScheduleSeason> _season;
        private ScheduleBell.Path<ScheduleBell> _bells;
        private EmployeePost.Path<EmployeePost> _chief;
        private DatabaseFile.Path<DatabaseFile> _content;
        private OrgUnit.Path<OrgUnit> _createOU;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата создания.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(SppSchedulePrintFormGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Группы. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getGroups()
     */
        public PropertyPath<String> groups()
        {
            if(_groups == null )
                _groups = new PropertyPath<String>(SppSchedulePrintFormGen.P_GROUPS, this);
            return _groups;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getEduYear()
     */
        public PropertyPath<String> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new PropertyPath<String>(SppSchedulePrintFormGen.P_EDU_YEAR, this);
            return _eduYear;
        }

    /**
     * @return Направление подготовки/специальности.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getEduLevels()
     */
        public PropertyPath<String> eduLevels()
        {
            if(_eduLevels == null )
                _eduLevels = new PropertyPath<String>(SppSchedulePrintFormGen.P_EDU_LEVELS, this);
            return _eduLevels;
        }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Период расписания. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getSeason()
     */
        public SppScheduleSeason.Path<SppScheduleSeason> season()
        {
            if(_season == null )
                _season = new SppScheduleSeason.Path<SppScheduleSeason>(L_SEASON, this);
            return _season;
        }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getBells()
     */
        public ScheduleBell.Path<ScheduleBell> bells()
        {
            if(_bells == null )
                _bells = new ScheduleBell.Path<ScheduleBell>(L_BELLS, this);
            return _bells;
        }

    /**
     * @return Руководитель формирующего подразделения.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getChief()
     */
        public EmployeePost.Path<EmployeePost> chief()
        {
            if(_chief == null )
                _chief = new EmployeePost.Path<EmployeePost>(L_CHIEF, this);
            return _chief;
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePrintForm#getCreateOU()
     */
        public OrgUnit.Path<OrgUnit> createOU()
        {
            if(_createOU == null )
                _createOU = new OrgUnit.Path<OrgUnit>(L_CREATE_O_U, this);
            return _createOU;
        }

        public Class getEntityClass()
        {
            return SppSchedulePrintForm.class;
        }

        public String getEntityName()
        {
            return "sppSchedulePrintForm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
