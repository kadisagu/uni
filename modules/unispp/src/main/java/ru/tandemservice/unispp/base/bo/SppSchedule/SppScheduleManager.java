/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.*;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 2/13/14
 */
@Configuration
public class SppScheduleManager extends BusinessObjectManager
{
    public static final String ALL_POSTS_PROP = "allPosts";

    public static SppScheduleManager instance()
    {
        return instance(SppScheduleManager.class);
    }

    @Bean
    public ISppScheduleDAO dao()
    {
        return new SppScheduleDAO();
    }

    @Bean
    public ISppScheduleEventsDAO eventsDao()
    {
        return new SppScheduleEventsDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler sppScheduleDSHandler()
    {
        return new SppScheduleDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler groupSearchDSHandler()
    {
        return new SppScheduleGroupSearchDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitComboDSHandler()
    {
        return new SppOrgUnitComboDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler sppScheduleSeasonDSHandler()
    {
        return new SppScheduleSeasonDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler courseComboDSHandler()
    {
        return new SppScheduleCourseComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler groupComboDSHandler()
    {
        return new SppScheduleGroupComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler seasonComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), SppScheduleSeason.class, SppScheduleSeason.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler bellsComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), ScheduleBell.class, ScheduleBell.title())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", ScheduleBell.active()), DQLExpressions.value(Boolean.TRUE)));
            }

            @Override
            protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
            {
                ep.dqlBuilder.order(DQLExpressions.property("e", ScheduleBell.title()));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler eduLevelComboDSHandler()
    {
        return new SppScheduleEduLevelComboDSHandler(getName());
    }

    @Bean
    public XStream xStream()
    {
        return new XStream(new StaxDriver());
    }

    @Bean
    public IDefaultComboDataSourceHandler termComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).
                addItemList(termOptionsExtPoint());
    }

    //Семестры
    public final static Long TERM_SPRING = 1L;
    public final static Long TERM_AUTUMN = 2L;


    @Bean
    public ItemListExtPoint<DataWrapper> termOptionsExtPoint()
    {
        return itemList(DataWrapper.class).
                add(TERM_SPRING.toString(), new DataWrapper(TERM_SPRING, "ui.term.spring")).
                add(TERM_AUTUMN.toString(), new DataWrapper(TERM_AUTUMN, "ui.term.autumn")).
                create();
    }

    @Bean
    public IDefaultComboDataSourceHandler employeePostComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EmployeePost.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.employee().archival()), DQLExpressions.value(false)));

                Boolean allPosts = ep.context.get(ALL_POSTS_PROP);
                if (allPosts != null && !allPosts)
                {
                    ep.dqlBuilder.where(eq(property("e", EmployeePost.postRelation().headerPost()), value(true)));
                }

                String filter = ep.input.getComboFilterByValue();
                if (!StringUtils.isEmpty(filter))
                    ep.dqlBuilder.where(DQLExpressions.like(DQLFunctions.upper(DQLFunctions.concat(
                            DQLExpressions.property("e", EmployeePost.person().identityCard().lastName()),
                            DQLExpressions.property("e", EmployeePost.postRelation().postBoundedWithQGandQL().title()),
                            DQLExpressions.property("e", EmployeePost.orgUnit().fullTitle()))), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                setOrderByProperty(EmployeePost.person().identityCard().lastName().s());
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler ppsComboDSHandler()
    {
        return new SppScheduleTeacherComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler subjectComboDSHandler()
    {
        return new SppScheduleSubjectComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler placePurposeComboDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UniplacesClassroomType.class).filter(UniplacesClassroomType.title()).order(UniplacesClassroomType.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler lectureRoomComboDSHandler()
    {
        return new SppScheduleLectureRoomComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler sppGroupComboDSHandler()
    {
        return new GroupComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler schedulesComboDSHandler()
    {
        return new SppSchedulesComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler registryDisciplineComboDSHandler()
    {
        return new SppScheduleRegistryDisciplineComboDSHandler(getName());
    }
}
