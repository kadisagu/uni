/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.View;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSearchListUtil;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleWeekDSHandler;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.AddEdit.SppTeacherPreferenceAddEdit;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList;
import ru.tandemservice.unispp.base.vo.SppTeacherPreferencePeriodVO;
import ru.tandemservice.unispp.base.vo.SppTeacherPreferencePeriodsVO;
import ru.tandemservice.unispp.base.vo.SppTeacherPreferenceVO;
import ru.tandemservice.unispp.base.vo.SppTeacherPreferenceWeekRowVO;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author vnekrasov
 * @since 6/23/14
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "teacherPreferenceId", required = true)
})
public class SppTeacherPreferenceViewUI extends UIPresenter
{
    private Long _teacherPreferenceId;
    private Long _buildingId;
    private SppTeacherPreferenceList _teacherPreference;
    private boolean _himself;

    private List<SppTeacherPreferenceWeekRowVO> _oddWeekPeriods = Lists.newArrayList();
    private List<SppTeacherPreferenceWeekRowVO> _evenWeekPeriods = Lists.newArrayList();
    private BaseSearchListDataSource _oddWeekDS;
    private BaseSearchListDataSource _evenWeekDS;

    public BaseSearchListDataSource getOddWeekDS()
    {
        if (_oddWeekDS == null)
            _oddWeekDS = getConfig().getDataSource(SppTeacherPreferenceView.ODD_WEEK_DS);
        return _oddWeekDS;
    }

    public BaseSearchListDataSource getEvenWeekDS()
    {
        if (_evenWeekDS == null)
            _evenWeekDS = getConfig().getDataSource(SppTeacherPreferenceView.EVEN_WEEK_DS);
        return _evenWeekDS;
    }

    public SppTeacherPreferenceWeekRowVO getWeekDSRecord(BaseSearchListDataSource dataSource, Long weekRowId)
    {
        return (SppTeacherPreferenceWeekRowVO) ((PageableSearchListDataSource) dataSource).getLegacyDataSource().getRecordById(weekRowId);
    }

    public Long getTeacherPreferenceId()
    {
        return _teacherPreferenceId;
    }

    public void setTeacherPreferenceId(Long teacherPreferenceId)
    {
        _teacherPreferenceId = teacherPreferenceId;
    }

    public SppTeacherPreferenceList getTeacherPreference()
    {
        return _teacherPreference;
    }

    public void setTeacherPreference(SppTeacherPreferenceList teacherPreference)
    {
        _teacherPreference = teacherPreference;
    }


//    visible="ui:editScheduleVisible"
    public Boolean getEditTeacherPreferenceVisible()
    {
        return true;
    }



//////
//    ui:currentOddRowItem.addActive - currentOddRowItemAddPeriod
    public Boolean getCurrentOddRowItemAddPeriod()
    {
        return getCurrentOddRowItem().getAddActive();
    }

//    ui:currentOddRowItem.editActive - currentOddRowItemEditPeriod
    public Boolean getCurrentOddRowItemEditPeriod()
    {
        return getCurrentOddRowItem().getEditActive();
    }

//    ui:currentOddRowItem.saveActive - currentOddRowItemSavePeriod
    public Boolean getCurrentOddRowItemSavePeriod()
    {
        return getCurrentOddRowItem().getSaveActive();
    }

    //    ui:currentEvenRowItem.addActive - currentEvenRowItemAddPeriod
    public Boolean getCurrentEvenRowItemAddPeriod()
    {
        return getCurrentEvenRowItem().getAddActive();
    }

    //    ui:currentEvenRowItem.editActive - currentEvenRowItemEditPeriod
    public Boolean getCurrentEvenRowItemEditPeriod()
    {
        return getCurrentEvenRowItem().getEditActive();
    }

    //    ui:currentEvenRowItem.saveActive - currentEvenRowItemSavePeriod
    public Boolean getCurrentEvenRowItemSavePeriod()
    {
        return getCurrentEvenRowItem().getSaveActive();
    }

    public Map getGroupParameterMap()
    {
        return new ParametersMap()
                //.add(UIPresenter.PUBLISHER_ID, _teacherPreference.getGroup().getId())
                .add("selectedTabId", "sppTeacherPreferenceTab");
    }

    @Override
    public void onComponentRefresh()
    {
        _teacherPreference = DataAccessServices.dao().getNotNull(_teacherPreferenceId);
        SppTeacherPreferenceVO teacherPreferenceVO = SppTeacherPreferenceManager.instance().dao().prepareTeacherPreferenceVO(_teacherPreference);
        _oddWeekPeriods = teacherPreferenceVO.getOddWeek().getWeekPeriods();
        _evenWeekPeriods = teacherPreferenceVO.getEvenWeek().getWeekPeriods();

        final PageableSearchListDataSource oddDs = _uiConfig.getDataSource(SppTeacherPreferenceView.ODD_WEEK_DS);
        oddDs.setDelegate(component -> {
            oddDs.setCountRecord(_oddWeekPeriods.size());
            CommonBaseSearchListUtil.createPage(oddDs.getLegacyDataSource(), _oddWeekPeriods);
        });

        final PageableSearchListDataSource evenDs = _uiConfig.getDataSource(SppTeacherPreferenceView.EVEN_WEEK_DS);
        evenDs.setDelegate(component -> {
            evenDs.setCountRecord(_evenWeekPeriods.size());
            CommonBaseSearchListUtil.createPage(evenDs.getLegacyDataSource(), _evenWeekPeriods);
        });

        IPrincipalContext principalContext = getConfig().getUserContext().getPrincipalContext();
        List<IPrincipalContext> personPrincipalContextList = _teacherPreference.getTeacher().getPrincipalContextList();

        _himself = false;
        if (personPrincipalContextList != null)
        {
            _himself = personPrincipalContextList.stream().map(IPrincipalContext::getPrincipal)
                    .collect(Collectors.toSet()).contains(principalContext.getPrincipal());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppTeacherPreferenceView.LECTURE_ROOM_DS.equals(dataSource.getName()))
        {
            dataSource.put("buildingId", _buildingId);
        }
        else if (SppTeacherPreferenceView.ODD_WEEK_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleWeekDSHandler.WEEK_PERIODS,_oddWeekPeriods);
        }
        else if (SppTeacherPreferenceView.EVEN_WEEK_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleWeekDSHandler.WEEK_PERIODS, _evenWeekPeriods);
        }
    }

    public SppTeacherPreferencePeriodsVO getCurrentOddRowItem()
    {
        SppTeacherPreferenceWeekRowVO rowVO = getOddWeekDS().getCurrent();
        int day = getOddWeekDS().getCurrentColumn().getNumber();
        return getDay(rowVO, day);
    }

    public SppTeacherPreferencePeriodsVO getCurrentEvenRowItem()
    {
        SppTeacherPreferenceWeekRowVO rowVO = getEvenWeekDS().getCurrent();
        int day = getEvenWeekDS().getCurrentColumn().getNumber();
        return getDay(rowVO, day);
    }

    public Map<String, Object> getCurrentOddRowItemParameters()
    {
        Integer day = getOddWeekDS().getCurrentColumn().getNumber();
        SppTeacherPreferenceWeekRowVO rowVO = getOddWeekDS().getCurrent();
        return ParametersMap.createWith("day", day).add("weekRowId", rowVO.getId());
    }

    public Map<String, Object> getCurrentEvenRowItemParameters()
    {
        Integer day = getEvenWeekDS().getCurrentColumn().getNumber();
        SppTeacherPreferenceWeekRowVO rowVO = getEvenWeekDS().getCurrent();
        return ParametersMap.createWith("day", day).add("weekRowId", rowVO.getId());
    }

    public Map<String, Object> getCurrentOddRowItemPeriodParameters()
    {
        Integer day = getOddWeekDS().getCurrentColumn().getNumber();
        SppTeacherPreferenceWeekRowVO rowVO = getOddWeekDS().getCurrent();
        SppTeacherPreferencePeriodsVO periodVO = getDay(rowVO, day);
        Integer periodIndex = periodVO.getPeriods().indexOf(periodVO.getCurrentPeriod());
        return ParametersMap.createWith("day", day).add("weekRowId", rowVO.getId()).add("periodIndex", periodIndex);
    }

    public Map<String, Object> getCurrentEvenRowItemPeriodParameters()
    {
        Integer day = getEvenWeekDS().getCurrentColumn().getNumber();
        SppTeacherPreferenceWeekRowVO rowVO = getEvenWeekDS().getCurrent();
        SppTeacherPreferencePeriodsVO periodVO = getDay(rowVO, day);
        Integer periodIndex = periodVO.getPeriods().indexOf(periodVO.getCurrentPeriod());
        return ParametersMap.createWith("day", day).add("weekRowId", rowVO.getId()).add("periodIndex", periodIndex);
    }

    public String getCurrentOddId()
    {
        Integer day = getOddWeekDS().getCurrentColumn().getNumber();
        SppTeacherPreferenceWeekRowVO rowVO = getOddWeekDS().getCurrent();
        SppTeacherPreferencePeriodsVO periodVO = getDay(rowVO, day);
        Integer periodIndex = periodVO.getPeriods().indexOf(periodVO.getCurrentPeriod());

        return day + "_" + rowVO.getTime().getNumber() + "_" + periodIndex;
    }

    public String getCurrentEvenId()
    {
        Integer day = getEvenWeekDS().getCurrentColumn().getNumber();
        SppTeacherPreferenceWeekRowVO rowVO = getEvenWeekDS().getCurrent();
        SppTeacherPreferencePeriodsVO periodVO = getDay(rowVO, day);
        Integer periodIndex = periodVO.getPeriods().indexOf(periodVO.getCurrentPeriod());

        return day + "_" + rowVO.getTime().getNumber() + "_" + periodIndex;
    }

    public String getCurrentOddRowItemLectureRoomId()
    {
        return "lectureRoomId_" + getCurrentOddId();
    }

    public String getCurrentOddRowItemBuildingId()
    {
        return "buildingId_" + getCurrentOddId();
    }

    public String getCurrentEvenRowItemLectureRoomId()
    {
        return "lectureRoomId_" + getCurrentEvenId();
    }

    public String getCurrentEvenRowItemBuildingId()
    {
        return "buildingId_" + getCurrentEvenId();
    }


    public void onClickEditTeacherPreference()
    {
        _uiActivation.asRegion(SppTeacherPreferenceAddEdit.class).parameter("teacherPreferenceId", _teacherPreferenceId).activate();
    }

    // odd listeners
    public void onAddOddPair()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");

        SppTeacherPreferenceWeekRowVO rowVO = getOddWeekDS().getRecordById(weekRowId);
        SppTeacherPreferencePeriodsVO periodsVO = getDay(rowVO, day);

        periodsVO.setView(false);
        periodsVO.getPeriods().add(getNewPeriodVO(rowVO, day));
    }

    public void onEditOddPair()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppTeacherPreferenceWeekRowVO rowVO = getOddWeekDS().getRecordById(weekRowId);
        SppTeacherPreferencePeriodsVO periodVO = getDay(rowVO, day);
        periodVO.setView(false);
    }

    public void onSaveOddPair()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppTeacherPreferenceWeekRowVO rowVO = getOddWeekDS().getRecordById(weekRowId);
        SppTeacherPreferencePeriodsVO periodsVO = getDay(rowVO, day);

        if(getUserContext().getErrorCollector().hasErrors())
            return;
        periodsVO.setView(true);
        SppTeacherPreferenceManager.instance().dao().saveOrUpdatePeriods(periodsVO.getPeriods());
    }

    public void onDeleteOddItem()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppTeacherPreferenceWeekRowVO rowVO = getOddWeekDS().getRecordById(weekRowId);
        SppTeacherPreferencePeriodsVO periodsVO = getDay(rowVO, day);
        Integer periodIndex = (Integer) parametersMap.get("periodIndex");
        SppTeacherPreferencePeriodVO periodVO = periodsVO.getPeriods().get(periodIndex);
        periodsVO.getPeriods().remove(periodVO);

        if(periodsVO.getPeriods().isEmpty())
            periodsVO.setView(true);
        if(null != periodVO.getPeriod().getId())
        {
            SppTeacherPreferenceElement period = periodVO.getPeriod();
            DataAccessServices.dao().delete(period);
        }
//        updateScheduleAsChanged();
    }

    public void onChangeBuildingOdd()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppTeacherPreferenceWeekRowVO rowVO = getOddWeekDS().getRecordById(weekRowId);
        SppTeacherPreferencePeriodsVO periodsVO = getDay(rowVO, day);
        Integer periodIndex = (Integer) parametersMap.get("periodIndex");
        SppTeacherPreferencePeriodVO periodVO = periodsVO.getPeriods().get(periodIndex);

        _buildingId = periodVO.getBuildingVal() == null ? null : periodVO.getBuildingVal().getId();
    }

    public void onChangeLectureRoomOdd()
    {
    }

    // even listeners
    public void onAddEvenPair()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");

        SppTeacherPreferenceWeekRowVO rowVO = getEvenWeekDS().getRecordById(weekRowId);
        SppTeacherPreferencePeriodsVO periodsVO = getDay(rowVO, day);

        periodsVO.setView(false);
        periodsVO.getPeriods().add(getNewPeriodVO(rowVO, day));
    }

    public void onEditEvenPair()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppTeacherPreferenceWeekRowVO rowVO = getEvenWeekDS().getRecordById(weekRowId);
        SppTeacherPreferencePeriodsVO periodVO = getDay(rowVO, day);
        periodVO.setView(false);
    }

    public void onSaveEvenPair()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppTeacherPreferenceWeekRowVO rowVO = getEvenWeekDS().getRecordById(weekRowId);
        SppTeacherPreferencePeriodsVO periodsVO = getDay(rowVO, day);

        if(getUserContext().getErrorCollector().hasErrors())
            return;
        periodsVO.setView(true);
        SppTeacherPreferenceManager.instance().dao().saveOrUpdatePeriods(periodsVO.getPeriods());
    }

    public void onDeleteEvenItem()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppTeacherPreferenceWeekRowVO rowVO = getEvenWeekDS().getRecordById(weekRowId);
        SppTeacherPreferencePeriodsVO periodsVO = getDay(rowVO, day);
        Integer periodIndex = (Integer) parametersMap.get("periodIndex");
        SppTeacherPreferencePeriodVO periodVO = periodsVO.getPeriods().get(periodIndex);
        periodsVO.getPeriods().remove(periodVO);

        if(periodsVO.getPeriods().isEmpty())
            periodsVO.setView(true);
        if(null != periodVO.getPeriod().getId())
        {
            SppTeacherPreferenceElement period = periodVO.getPeriod();
            DataAccessServices.dao().delete(period);
        }
//        updateScheduleAsChanged();
    }

    public void onChangeBuildingEven()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppTeacherPreferenceWeekRowVO rowVO = getEvenWeekDS().getRecordById(weekRowId);
        SppTeacherPreferencePeriodsVO periodsVO = getDay(rowVO, day);
        Integer periodIndex = (Integer) parametersMap.get("periodIndex");
        SppTeacherPreferencePeriodVO periodVO = periodsVO.getPeriods().get(periodIndex);

        _buildingId = periodVO.getBuildingVal() == null ? null : periodVO.getBuildingVal().getId();
    }

    public void onChangeLectureRoomEven()
    {
    }

    // listeners

    private SppTeacherPreferencePeriodVO getNewPeriodVO(SppTeacherPreferenceWeekRowVO rowVO, int day)
    {
        SppTeacherPreferenceElement newPeriod = new SppTeacherPreferenceElement();
        newPeriod.setWeekRow(rowVO.getWeekRow());
        //newPeriod.setPeriodNum(periodsVO.getPeriods().isEmpty() ? 1 : 2);
        newPeriod.setDayOfWeek(day);
        //newPeriod.setEmpty(false);
        SppTeacherPreferencePeriodVO newPeriodVO = new SppTeacherPreferencePeriodVO(newPeriod);
        newPeriodVO.setWeekPeriodsRowVO(rowVO);
        return newPeriodVO;
    }

    private SppTeacherPreferencePeriodsVO getDay(SppTeacherPreferenceWeekRowVO rowVO, int day)
    {
        switch (day)
        {
            case SppTeacherPreferenceVO.MONDAY:
                return rowVO.getMonday();
            case SppTeacherPreferenceVO.TUESDAY:
                return rowVO.getTuesday();
            case SppTeacherPreferenceVO.WEDNESDAY:
                return rowVO.getWednesday();
            case SppTeacherPreferenceVO.THURSDAY:
                return rowVO.getThursday();
            case SppTeacherPreferenceVO.FRIDAY:
                return rowVO.getFriday();
            case SppTeacherPreferenceVO.SATURDAY:
                return rowVO.getSaturday();
            default:
                return rowVO.getSunday();
        }
    }

    public String getListTitle()
    {
        return _teacherPreference.isDiffEven() ? "Нечетная неделя" : "Неделя";
    }

    public String getViewPermissionKey()
    {
        return SppTeacherPreferenceManager.getPermissionKey("sppTeacherPreferenceViewView", _himself);
    }

    public String getEditPermissionKey()
    {
        return SppTeacherPreferenceManager.getPermissionKey("sppTeacherPreferenceViewEdit", _himself);
    }
}
