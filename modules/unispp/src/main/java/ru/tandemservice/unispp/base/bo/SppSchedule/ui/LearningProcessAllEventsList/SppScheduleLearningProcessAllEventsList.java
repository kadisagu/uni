/* $Id: SppScheduleDailyView.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.LearningProcessAllEventsList;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniplaces.entity.catalog.codes.UniplacesPlacePurposeCodes;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleAllEventsDSHandler;
import ru.tandemservice.unispp.base.vo.SppScheduleEventInDayVO;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@Configuration
public class SppScheduleLearningProcessAllEventsList extends BusinessComponentManager
{
    public static final String PROP_IS_SHOW_ORIGINAL_DATA = "isShowOriginalData";

    public static final String SCHEDULE_EVENTS_DS = "eventsDS";
    public static final String YEAR_PART_DS = "yearPartDS";
    public static final String TEACHER_DS = "teacherDS";
    public static final String GROUP_DS = "groupDS";
    public static final String EDU_LEVEL_DS = "eduLevelDS";
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String SEASON_DS = "seasonDS";
    public static final String SUBJECT_DS = "subjectDS";
    public static final String PLACES_DS = "placesDS";
    public static final String STATUSES_DS = "statusesDS";

    @Bean
    public ColumnListExtPoint scheduleEventsCL()
    {
        return columnListExtPointBuilder(SCHEDULE_EVENTS_DS)
                .addColumn(dateColumn("date", SppScheduleEventInDayVO.PROPERTY_DATE).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).required(true))
                .addColumn(textColumn("group", SppScheduleEventInDayVO.PROPERTY_GROUP))
                .addColumn(textColumn("lectureRoom", SppScheduleEventInDayVO.PROPERTY_LECTURE_ROOM))
                .addColumn(textColumn("subject", SppScheduleEventInDayVO.PROPERTY_SUBJECT))
                .addColumn(textColumn("teacher", SppScheduleEventInDayVO.PROPERTY_TEACHER))
                .addColumn(textColumn("status", SppScheduleEventInDayVO.PROPERTY_STATUS))
//                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onClickEditEvent").permissionKey("sppScheduleDailyViewEdit").disabled("ui:eventEditDisabled"))
//                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDeleteEvent").permissionKey("sppScheduleDailyViewEdit").disabled("ui:eventDeleteDisabled"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SCHEDULE_EVENTS_DS, scheduleEventsCL(), scheduleEventsDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEAR_PART_DS, getName(), EppYearPart.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(EDU_LEVEL_DS, SppScheduleManager.instance().eduLevelComboDSHandler()).addColumn(EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(selectDS(GROUP_DS, SppScheduleManager.instance().groupComboDSHandler()))
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()))
                .addDataSource(selectDS(TEACHER_DS, SppScheduleManager.instance().ppsComboDSHandler()).addColumn("fio", PpsEntry.titleFioInfoOrgUnit().s()))
                .addDataSource(selectDS(PLACES_DS, lectureRoomComboDSHandler()).addColumn("title", UniplacesPlace.title().s()).addColumn("location", UniplacesPlace.fullLocationInfo().s()))
                .addDataSource(selectDS(SUBJECT_DS, SppScheduleManager.instance().registryDisciplineComboDSHandler()))
                .addDataSource(selectDS(STATUSES_DS, eventStatusDSHandler()))
                .create();
    }

    @Bean
    IDefaultComboDataSourceHandler eventStatusDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            {
                filtered(true);
            }

            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                boolean isShowOriginalData = context.getBoolean(PROP_IS_SHOW_ORIGINAL_DATA, Boolean.FALSE);
                List<IdentifiableWrapper> list = Lists.newArrayList(SppScheduleEventInDayVO.STATUS_ADDED, SppScheduleEventInDayVO.STATUS_CANCELED, SppScheduleEventInDayVO.STATUS_CHANGED);
                if (isShowOriginalData) list.remove(SppScheduleEventInDayVO.STATUS_ADDED);
                context.put(UIDefines.COMBO_OBJECT_LIST, list);
                return super.execute(input, context);
            }
        };
    }

    @Bean
    public IDefaultSearchDataSourceHandler scheduleEventsDSHandler()
    {
        return new SppScheduleAllEventsDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler lectureRoomComboDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UniplacesPlace.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                List<String> placePurposeCodeses = Lists.newArrayList();
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.AUDITORIYA_OBTSHAYA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.AUDITORIYA_SPETSIALIZIROVANNAYA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.LABORATORIYA_OBTSHAYA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.LABORATORIYA_SPETSIALIZIROVANNAYA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.VYCHISLITELNYY_KLASS);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.KABINET);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.SPETSIALIZIROVANNYY_KLASS);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.OBEKT_FIZKULTURY_I_SPORTA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.TRUDOVOE_VOSPITANIE);
                dql.where(DQLExpressions.in(DQLExpressions.property(alias, UniplacesPlace.purpose().code()), placePurposeCodeses));
                dql.where(DQLExpressions.eq(DQLExpressions.property(alias, UniplacesPlace.condition().code()), DQLExpressions.value("1")));
                Long buildingId = context.get("buildingId");
                Collection<Long> buildingIds = context.get("buildingIds");
                if (!(buildingId ==null))
                    dql.where(DQLExpressions.eq(DQLExpressions.property(alias, UniplacesPlace.floor().unit().building().id()), DQLExpressions.value(buildingId)));
                if (buildingIds!=null)
                if (buildingIds.size()>0)
                    dql.where(DQLExpressions.in(DQLExpressions.property(alias, UniplacesPlace.floor().unit().building().id()), buildingIds));
            }


        }.order(UniplacesPlace.title()).filter(UniplacesPlace.title());

//                SppScheduleLectRoomComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                        OrgUnitToKindRelation.class,
                        OrgUnitToKindRelation.orgUnitKind().code().s(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING,
                        OrgUnitToKindRelation.L_ORG_UNIT, property("e")
                ));
            }
        }
                .order(OrgUnit.title())
                .filter(OrgUnit.title());
    }
}
