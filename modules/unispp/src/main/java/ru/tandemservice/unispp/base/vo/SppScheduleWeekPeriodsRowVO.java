/* $Id$ */
package ru.tandemservice.unispp.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppScheduleWeekRow;

import java.time.DayOfWeek;

/**
 * @author nvankov
 * @since 8/30/13
 */
public class SppScheduleWeekPeriodsRowVO extends DataWrapper
{
    private ScheduleBellEntry _time;
    private SppScheduleWeekRow _weekRow;

    private SppSchedulePeriodsVO _monday;
    private SppSchedulePeriodsVO _tuesday;
    private SppSchedulePeriodsVO _wednesday;
    private SppSchedulePeriodsVO _thursday;
    private SppSchedulePeriodsVO _friday;
    private SppSchedulePeriodsVO _saturday;
    private SppSchedulePeriodsVO _sunday;

    public SppScheduleWeekPeriodsRowVO(SppScheduleWeekRow weekRow)
    {
        super.setId(weekRow.getId());
        _weekRow = weekRow;
        _time = weekRow.getBell();
    }

    public ScheduleBellEntry getTime()
    {
        return _time;
    }

    public void setTime(ScheduleBellEntry time)
    {
        _time = time;
    }

    public SppSchedulePeriodsVO getMonday()
    {
        return _monday;
    }

    public void setMonday(SppSchedulePeriodsVO monday)
    {
        _monday = monday;
    }

    public SppSchedulePeriodsVO getTuesday()
    {
        return _tuesday;
    }

    public void setTuesday(SppSchedulePeriodsVO tuesday)
    {
        _tuesday = tuesday;
    }

    public SppSchedulePeriodsVO getWednesday()
    {
        return _wednesday;
    }

    public void setWednesday(SppSchedulePeriodsVO wednesday)
    {
        _wednesday = wednesday;
    }

    public SppSchedulePeriodsVO getThursday()
    {
        return _thursday;
    }

    public void setThursday(SppSchedulePeriodsVO thursday)
    {
        _thursday = thursday;
    }

    public SppSchedulePeriodsVO getFriday()
    {
        return _friday;
    }

    public void setFriday(SppSchedulePeriodsVO friday)
    {
        _friday = friday;
    }

    public SppSchedulePeriodsVO getSaturday()
    {
        return _saturday;
    }

    public void setSaturday(SppSchedulePeriodsVO saturday)
    {
        _saturday = saturday;
    }

    public SppSchedulePeriodsVO getSunday()
    {
        return _sunday;
    }

    public void setSunday(SppSchedulePeriodsVO sunday)
    {
        _sunday = sunday;
    }

    public SppScheduleWeekRow getWeekRow()
    {
        return _weekRow;
    }

    public void setWeekRow(SppScheduleWeekRow weekRow)
    {
        _weekRow = weekRow;
    }


    public SppSchedulePeriodsVO getDay(int day)
    {
        return getDay(DayOfWeek.of(day));
    }

    public SppSchedulePeriodsVO getDay(DayOfWeek day)
    {
        switch (day)
        {
            case MONDAY:
                return this.getMonday();
            case TUESDAY:
                return this.getTuesday();
            case WEDNESDAY:
                return this.getWednesday();
            case THURSDAY:
                return this.getThursday();
            case FRIDAY:
                return this.getFriday();
            case SATURDAY:
                return this.getSaturday();
            case SUNDAY:
                return this.getSunday();
            default:
                throw new ApplicationException("Unknown day of week");
        }
    }
}

