/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 7/1/14
 */
public class SppTeacherSessionPreferenceDSHandler extends DefaultSearchDataSourceHandler
{
    public SppTeacherSessionPreferenceDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String title = context.get("title");
        Long teacherId = context.get("teacherId"); // employee post id
//        List<Long> courses = context.get("courses");
//        List<Long> eduLevels = context.get("eduLevels");
//        List<Long> groups = context.get("groups");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppTeacherSessionPreferenceList.class, "s");
        builder.column("s");
        builder.fetchPath(DQLJoinType.inner, SppTeacherSessionPreferenceList.sppScheduleSessionSeason().fromAlias("s"), "season");

        if (teacherId != null)
        {
            Long personId = UniDaoFacade.getCoreDao().getProperty(EmployeePost.class, EmployeePost.person().id().s(), EmployeePost.P_ID, teacherId);
            builder.where(eq(property("s", SppTeacherSessionPreferenceList.teacher().id()), value(personId)));
        }

        if(!StringUtils.isEmpty(title))
            builder.where(like(DQLFunctions.upper(property("s", SppTeacherSessionPreferenceList.title())), value(CoreStringUtils.escapeLike(title, true))));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().pageable(true).build();
    }
}
