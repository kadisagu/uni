/* $Id: SppScheduleLearningProcessPrintFormListUI.java 37805 2014-09-03 08:21:34Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.LearningProcessPrintFormList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppSchedulePrintFormDSHandler;
import ru.tandemservice.unispp.base.entity.SppSchedulePrintForm;

/**
 * @author nvankov
 * @since 9/25/13
 */
public class SppScheduleLearningProcessPrintFormListUI extends UIPresenter
{
    private BaseSearchListDataSource _printFormDS;

    public BaseSearchListDataSource getPrintFormDS()
    {
        if(null == _printFormDS)
            _printFormDS = _uiConfig.getDataSource(SppScheduleLearningProcessPrintFormList.PRINT_FORM_DS);
        return _printFormDS;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleLearningProcessPrintFormList.PRINT_FORM_DS.equals(dataSource.getName()))
        {
            //dataSource.put("orgUnitId", getOrgUnit().getId());
            dataSource.put(SppSchedulePrintFormDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
            dataSource.putAll(_uiSettings.getAsMap(true, "admin", "groups", "orgUnitIds"));
        }
        if (SppScheduleLearningProcessPrintFormList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put("orgUnitIds", _uiSettings.get("orgUnitIds"));
        }
    }

    public void onClickDownload()
    {
        SppSchedulePrintForm printForm = getPrintFormDS().getRecordById(getListenerParameterAsLong());
        byte[] content = printForm.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("xls").fileName("schedule.xls").document(content), true);
    }
}
