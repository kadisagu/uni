/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.ui.ColorRowCustomizer;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleLoadDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppSchedulePeriodFixDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleWarningLogDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleWeekDSHandler;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix;
import ru.tandemservice.unispp.base.entity.SppScheduleWarningLog;
import ru.tandemservice.unispp.base.vo.SppScheduleLoadVO;

/**
 * @author nvankov
 * @since 9/4/13
 */
@Configuration
public class SppScheduleView extends BusinessComponentManager
{
    public static final String WRONG_LOAD_ROW_COLOR = "#ffe8f3";

    public static final String ODD_WEEK_DS = "oddWeekDS";
    public static final String EVEN_WEEK_DS = "evenWeekDS";
    public static final String PERIOD_FIX_DS = "periodFixDS";
    public static final String LECTURE_ROOM_DS = "lectureRoomDS";
    public static final String TEACHER_DS = "teacherDS";
    public static final String SUBJECT_DS = "subjectDS";
    public static final String LOAD_DS = "loadDS";
    public static final String ACTIONS = "actions";

    public static final String LECTURE_ROOM_TYPE_DS = "lectureRoomTypeDS";

    public final static String WARNING_LOG_DS = "warningLogDS";

    @Bean
    public ColumnListExtPoint oddWeekCL()
    {
        return columnListExtPointBuilder(ODD_WEEK_DS)
                .addColumn(textColumn("time", "time.time").width("100px"))
                .addColumn(blockColumn("monday", "oddDayBlockColumn"))
                .addColumn(blockColumn("tuesday", "oddDayBlockColumn"))
                .addColumn(blockColumn("wednesday", "oddDayBlockColumn"))
                .addColumn(blockColumn("thursday", "oddDayBlockColumn"))
                .addColumn(blockColumn("friday", "oddDayBlockColumn"))
                .addColumn(blockColumn("saturday", "oddDayBlockColumn"))
                .addColumn(blockColumn("sunday", "oddDayBlockColumn"))
                .create();
    }

    @Bean
    public ColumnListExtPoint evenWeekCL()
    {
        return columnListExtPointBuilder(EVEN_WEEK_DS)
                .addColumn(textColumn("time", "time.time").width("100px"))
                .addColumn(blockColumn("monday", "evenDayBlockColumn"))
                .addColumn(blockColumn("tuesday", "evenDayBlockColumn"))
                .addColumn(blockColumn("wednesday", "evenDayBlockColumn"))
                .addColumn(blockColumn("thursday", "evenDayBlockColumn"))
                .addColumn(blockColumn("friday", "evenDayBlockColumn"))
                .addColumn(blockColumn("saturday", "evenDayBlockColumn"))
                .addColumn(blockColumn("sunday", "evenDayBlockColumn"))
                .create();
    }

    @Bean
    public ColumnListExtPoint warningLogCL()
    {
        return columnListExtPointBuilder(WARNING_LOG_DS)
                .addColumn(textColumn("warningMessage", SppScheduleWarningLog.warningMessage()))
                .create();
    }

    @Bean
    public ColumnListExtPoint periodFixCL()
    {
        return columnListExtPointBuilder(PERIOD_FIX_DS)
                .addColumn(dateColumn("date", SppSchedulePeriodFix.date()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().required(true))
                .addColumn(textColumn("time", SppSchedulePeriodFix.bell().time()).required(true))
                .addColumn(textColumn("lectureRoom", SppSchedulePeriodFix.lectureRoom()))
                .addColumn(textColumn("subject", SppSchedulePeriodFix.subject()))
                .addColumn(textColumn("teacher", SppSchedulePeriodFix.teacher()))
                .addColumn(textColumn("periodNum", SppSchedulePeriodFix.periodNum()))
                .addColumn(textColumn("periodGroup", SppSchedulePeriodFix.periodGroup()))
                .addColumn(booleanColumn("canceled", SppSchedulePeriodFix.canceled()))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onClickEditPeriodFix").permissionKey("sppScheduleViewAddEditPeriodFix").disabled("ui:periodFixEditDisabled"))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDeletePeriodFix").permissionKey("sppScheduleViewAddDeletePeriodFix").disabled("ui:periodFixDeleteDisabled"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ODD_WEEK_DS, oddWeekCL(), weekPeriodsDSHandler()))
                .addDataSource(searchListDS(EVEN_WEEK_DS, evenWeekCL(), weekPeriodsDSHandler()))
                .addDataSource(searchListDS(PERIOD_FIX_DS, periodFixCL(), periodFixDSHandler()))
                .addDataSource(searchListDS(WARNING_LOG_DS, warningLogCL(), warningLogDSHandler()))
                .addDataSource(selectDS(LECTURE_ROOM_DS, SppScheduleManager.instance().lectureRoomComboDSHandler())/*.addColumn("placeTitle", SppScheduleLectureRoomComboDSHandler.VIEW_PROP_TITLE).addColumn("capacity", SppScheduleLectureRoomComboDSHandler.VIEW_PROP_CAPACITY).addColumn("location", SppScheduleLectureRoomComboDSHandler.VIEW_PROP_LOCATION)*/)//.addColumn("title", UniplacesPlace.title().s()).addColumn("location", UniplacesPlace.fullLocationInfo().s()))
                .addDataSource(selectDS(TEACHER_DS, SppScheduleManager.instance().ppsComboDSHandler()).addColumn("fio", PpsEntry.titleFioInfoOrgUnit().s()))
                .addDataSource(selectDS(SUBJECT_DS, SppScheduleManager.instance().subjectComboDSHandler()))
                .addDataSource(selectDS(LECTURE_ROOM_TYPE_DS, SppScheduleManager.instance().placePurposeComboDSHandler()))
                .addDataSource(searchListDS(LOAD_DS, loadCL(), loadDSHandler()).rowCustomizer(new ColorRowCustomizer<SppScheduleLoadVO>()
                {
                    @Override
                    public String getRowStyle(SppScheduleLoadVO load)
                    {
                        return (load.coverStatus() != 0) ? ("background-color: " + WRONG_LOAD_ROW_COLOR) : "";
                    }
                }))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler periodFixDSHandler()
    {
        return new SppSchedulePeriodFixDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler warningLogDSHandler()
    {
        return new SppScheduleWarningLogDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler weekPeriodsDSHandler()
    {
        return new SppScheduleWeekDSHandler(getName());
    }

    @Bean
    public ColumnListExtPoint loadCL()
    {
        return columnListExtPointBuilder(LOAD_DS)
                .addColumn(textColumn("title", SppScheduleLoadVO.DISCIPLINE))
                .addColumn(textColumn("requiredLoad", SppScheduleLoadVO.REQUIRED_LOAD))
                .addColumn(textColumn("load", SppScheduleLoadVO.LOAD))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler loadDSHandler()
    {
        return new SppScheduleLoadDSHandler(getName());
    }

    @Bean
    public ButtonListExtPoint actionsButtonExtPoint()
    {
        return buttonListExtPointBuilder(ACTIONS)
                .addButton(submitButton("editSchedule", "onClickEditSchedule").permissionKey("sppScheduleViewEdit").visible("ui:editScheduleVisible").create()) // Редактировать данные расписания
                .addButton(submitButton("copySchedule", "onClickCopySchedule").permissionKey("sppScheduleCopy").visible("ui:editScheduleVisible").create()) // Копировать расписание
                .addButton(submitButton("addPeriodFix", "onClickAddPeriodFix").permissionKey("sppScheduleViewAddAddPeriodFix").visible("ui:addPeriodFixVisible").create()) // Добавить исправление
                .addButton(submitButton("sentToArchive", "onClickSentToArchive").permissionKey("sppScheduleSentToArchive").visible("ui:sentToArchiveVisible").create())        // Отправить в архив
                .addButton(submitButton("restoreFromArchive", "onClickRestoreFromArchive").permissionKey("sppScheduleRestoreFormArchive").visible("ui:restoreFromArchiveVisible").create()) // Вернуть из архива
                .addButton(submitButton("performValidation", "onClickPerformValidation").permissionKey("sppScheduleViewPerformValidation").visible("ui:approveVisible").create())
                .addButton(submitButton("approve", "onClickApprove").permissionKey("sppScheduleApprove").visible("ui:approveVisible").create()) // Утвердить
                .addButton(submitButton("sentToFormation", "onClickSentToFormation").permissionKey("sppScheduleSentToFormation").visible("ui:sentToFormationVisible").create()) // Отправить на формирование
                .create();
    }
}
