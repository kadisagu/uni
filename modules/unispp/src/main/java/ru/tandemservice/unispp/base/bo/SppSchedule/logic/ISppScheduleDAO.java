/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.*;
import ru.tandemservice.unispp.base.vo.SppSchedulePeriodVO;
import ru.tandemservice.unispp.base.vo.SppSchedulePrintDataVO;
import ru.tandemservice.unispp.base.vo.SppScheduleVO;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 9/2/13
 */
public interface ISppScheduleDAO extends INeedPersistenceSupport
{
    List<ScheduleBellEntry> getBells(Long bellsId);

//    void saveOrUpdateBells(SppScheduleBells bells, List<SppScheduleBell> bellList, List<SppScheduleBell> deleteBellList);

    boolean canEditBellsEntries(Long bellsId);

    void createOrUpdateScheduelSeason(SppScheduleSeason scheduleSeason);

    void saveSchedules(List<SppSchedule> scheduleList);

    void saveOrUpdateSchedule(SppSchedule schedule, ScheduleBell oldBells, boolean oldDiffEven);

    void copySchedule(Long scheduleId, SppSchedule newSchedule, boolean swapWeeks);

    void deleteSchedule(Long scheduleId);

    SppScheduleVO prepareScheduleVO(SppSchedule schedule);

    void saveOrUpdatePeriods(List<SppSchedulePeriodVO> periods);

    boolean periodFixExistOnThisDayAndPeriod(SppSchedulePeriodFix periodFix);

    void createOrUpdatePeriodFix(SppSchedulePeriodFix periodFix);

    UniplacesClassroomType getDisciplineLectureRoomType(EppRegistryElement discipline, EppALoadType loadType);

    boolean isSppScheduleListTabVisible(Long orgUnitId);

    void sentToArchive(Long scheduleId);

    void restoreFromArchive(Long scheduleId);

    /**
     * Выполнение проверок, необходимых перед утверждением расписания
     *
     * @param schedule расписание
     */
    void validate(SppSchedule schedule);

    void approve(Long scheduleId);

    void sentToFormation(Long scheduleId);

    SppSchedulePrintForm savePrintForm(SppSchedulePrintDataVO scheduleData);

    int getGroupStudentCount (Long groupId);

    @NotNull
    EppWorkPlan getWorkPlanByDiscipline(@NotNull EppRegistryDiscipline discipline);

    SppSchedulePeriod getPeriod(Long scheduleId, Date date, Long bellId, int number);
}
