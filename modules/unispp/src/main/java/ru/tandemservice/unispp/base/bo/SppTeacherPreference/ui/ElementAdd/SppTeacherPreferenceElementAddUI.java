/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.ElementAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.View.SppTeacherPreferenceView;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceWeekRow;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author vnekrasov
 * @since 7/3/14
 */
@Input({
        @Bind(key = "teacherPreferenceId", binding = "teacherPreferenceId")
})
public class SppTeacherPreferenceElementAddUI extends UIPresenter
{
    private Long _teacherPreferenceId;
    private SppTeacherPreferenceList _teacherPreference;
    private List<UniplacesBuilding> _buildingList;
    private List<UniplacesPlace> _lectureRoomList;
    private List<IdentifiableWrapper> _dayOfWeekList;
    private List<ScheduleBellEntry> _bellEntryList;

    public List<ScheduleBellEntry> getBellEntryList()
    {
        return _bellEntryList;
    }

    public void setBellEntryList(List<ScheduleBellEntry> bellEntryList)
    {
        _bellEntryList = bellEntryList;
    }

    public List<UniplacesPlace> getLectureRoomList()
    {
        return _lectureRoomList;
    }

    public void setLectureRoomList(List<UniplacesPlace> lectureRoomList)
    {
        _lectureRoomList = lectureRoomList;
    }

    public List<IdentifiableWrapper> getDayOfWeekList()
    {
        return _dayOfWeekList;
    }

    public void setDayOfWeekList(List<IdentifiableWrapper> dayOfWeekList)
    {
        _dayOfWeekList = dayOfWeekList;
    }

    public List<UniplacesBuilding> getBuildingList()
    {
        return _buildingList;
    }

    public void setBuildingList(List<UniplacesBuilding> buildingList)
    {
        _buildingList = buildingList;
    }

    public Long getTeacherPreferenceId()
    {
        return _teacherPreferenceId;
    }

    public void setTeacherPreferenceId(Long teacherPreferenceId)
    {
        _teacherPreferenceId = teacherPreferenceId;
    }

    public SppTeacherPreferenceList getTeacherPreference()
    {
        return _teacherPreference;
    }

    public void setTeacherPreference(SppTeacherPreferenceList teacherPreference)
    {
        _teacherPreference = teacherPreference;
    }

    public Boolean getAddForm()
    {
        return true;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _teacherPreference)
        {
            _teacherPreference = DataAccessServices.dao().getNotNull(_teacherPreferenceId);
        }
//        if (null == _element)
//        {
//            if (null != _elementId)
//            {
//                _element = DataAccessServices.dao().get(_elementId);
//                if (_element.getDayOfWeek()==1)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.MONDAY;
//                if (_element.getDayOfWeek()==2)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.TUESDAY;
//                if (_element.getDayOfWeek()==3)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.WEDNESDAY;
//                if (_element.getDayOfWeek()==4)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.THURSDAY;
//                if (_element.getDayOfWeek()==5)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.FRIDAY;
//                if (_element.getDayOfWeek()==6)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.SATURDAY;
//                if (_element.getDayOfWeek()==7)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.SUNDAY;
//            }
//            else
//            {
//                _element = new SppteacherPreferenceElement();
//                _element.setTeacherPreference(_teacherPreference);
//            }
//        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppTeacherPreferenceElementAdd.LECTURE_ROOM_DS.equals(dataSource.getName()))
        {
            Collection<Long> buildingIds = UniBaseUtils.getIdList(_buildingList);
            dataSource.put("buildingIds", buildingIds);
        }
        if (SppTeacherPreferenceElementAdd.BELL_ENTRY_DS.equals(dataSource.getName()))
        {
            dataSource.put("bellScheduleId", _teacherPreference.getBells().getId());
        }
    }

    public String getPeriodStr()
    {
        return _teacherPreference.getSppScheduleSeason().getTitleWithTime();
    }

    public void onChangeLectureRoom()
    {
//        if(null != _element.getLectureRoom())
//            _event.setLectureRoom(_event.getLectureRoomVal().getTitle());
    }

    public void onChangeBuilding()
    {

    }

    public void onChangeBellEntry()
    {

    }

    public void onChangeDayOfWeek()
    {
//        if(null != _dayOfWeekWrapper)
//            _element.setDayOfWeek(_dayOfWeekWrapper.getId().intValue());
    }



    public void onClickApply()
    {
        SppScheduleSeason season = _teacherPreference.getSppScheduleSeason();

        if(getUserContext().getErrorCollector().hasErrors()) return;

        List<IdentifiableWrapper> dayOfWeekList = _dayOfWeekList.isEmpty() ? Collections.singletonList(null) : _dayOfWeekList;
        List<UniplacesBuilding> buildingList = (_buildingList.isEmpty() || !(_lectureRoomList.isEmpty())) ? Collections.singletonList(null) : _buildingList;
        List<UniplacesPlace> lectureRoomList = _lectureRoomList.isEmpty() ? Collections.singletonList(null) : _lectureRoomList;

        for (ScheduleBellEntry bell : _bellEntryList)
        {
            SppTeacherPreferenceWeekRow oddWeekRow = new SppTeacherPreferenceWeekRow();
            oddWeekRow.setTeacherPreference(_teacherPreference);
            oddWeekRow.setBell(bell);
            oddWeekRow.setEven(false);
            DataAccessServices.dao().saveOrUpdate(oddWeekRow);
            saveTeacherPreferenceElement(oddWeekRow, dayOfWeekList, buildingList, lectureRoomList);

            if (_teacherPreference.isDiffEven())
            {
                SppTeacherPreferenceWeekRow evenWeekRow = new SppTeacherPreferenceWeekRow();
                evenWeekRow.setTeacherPreference(_teacherPreference);
                evenWeekRow.setBell(bell);
                evenWeekRow.setEven(true);
                DataAccessServices.dao().saveOrUpdate(evenWeekRow);
                saveTeacherPreferenceElement(evenWeekRow, dayOfWeekList, buildingList, lectureRoomList);
            }
        }
        //SppScheduleSessionManager.instance().dao().updateScheduleAsChanged(_schedule.getId());
        deactivate();
        _uiActivation.asDesktopRoot(SppTeacherPreferenceView.class).parameter(PUBLISHER_ID, _teacherPreference.getId()).activate();
    }

    private void saveTeacherPreferenceElement(SppTeacherPreferenceWeekRow weekRow, List<IdentifiableWrapper> dayOfWeekList, List<UniplacesBuilding> buildingList, List<UniplacesPlace> lectureRoomList)
    {
        for (IdentifiableWrapper dayOfWeek : dayOfWeekList)
        for (UniplacesBuilding building : buildingList)
        for (UniplacesPlace lectureRoom : lectureRoomList)
        {
            SppTeacherPreferenceElement element = new SppTeacherPreferenceElement();
            element.setWeekRow(weekRow);
            element.setBuilding(building);
            element.setLectureRoom(lectureRoom);
            if (null != dayOfWeek)
                element.setDayOfWeek(dayOfWeek.getId().intValue());
            SppTeacherPreferenceManager.instance().dao().createOrUpdateElement(element);
        }
    }
}
