package ru.tandemservice.unispp.base.entity;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.unispp.base.entity.gen.SppScheduleDailyEventGen;

/**
 * Cобытие расписания
 */
public class SppScheduleDailyEvent extends SppScheduleDailyEventGen
{
    public String getPrint()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(getBell().getPrintTime());
        sb.append(" - ");
        if (getSubjectLoadType() != null && getSubjectVal() != null)
            sb.append(getSubjectVal().getTitle()).append(" (").append(getSubjectLoadType().getAbbreviation()).append(")");
        else
            sb.append(getSubject());
        sb.append(" ").append(getTeacherVal() != null ? getTeacherVal().getTitleFioInfoOrgUnit() : getTeacher());
        if (!StringUtils.isEmpty(getLectureRoom())) sb.append(" ").append(getLectureRoom());
        return sb.toString();
    }
}
