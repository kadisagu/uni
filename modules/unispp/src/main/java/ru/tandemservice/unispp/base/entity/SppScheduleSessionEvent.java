package ru.tandemservice.unispp.base.entity;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.unispp.base.entity.gen.SppScheduleSessionEventGen;

/**
 * Cобытие сессии
 */
public class SppScheduleSessionEvent extends SppScheduleSessionEventGen
{
    public String getPrint()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(getBell().getPrintTime());
        sb.append(" - ").append(getSubjectVal() != null ? getSubjectVal().getTitle() : getSubject());
        sb.append(" ").append(getTeacherVal() != null ? getTeacherVal().getTitleFioInfoOrgUnit() : getTeacher());
        if (!StringUtils.isEmpty(getLectureRoom())) sb.append(" ").append(getLectureRoom());
        return sb.toString();
    }
}
