/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleWeekDSHandler;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;

/**
 * @author vnekrasov
 * @since 6/23/14
 */
@Configuration
public class SppTeacherPreferenceView extends BusinessComponentManager
{
    public final static String ODD_WEEK_DS = "oddWeekDS";
    public final static String EVEN_WEEK_DS = "evenWeekDS";
    public static final String LECTURE_ROOM_DS = "lectureRoomDS";
    public static final String BUILDING_DS = "buildingDS";
    public static final String ACTIONS = "actions";

    @Bean
    public ColumnListExtPoint oddWeekCL()
    {
        return columnListExtPointBuilder(ODD_WEEK_DS)
                .addColumn(textColumn("time", "time.time").width("100px"))
                .addColumn(blockColumn("monday", "oddDayBlockColumn"))
                .addColumn(blockColumn("tuesday", "oddDayBlockColumn"))
                .addColumn(blockColumn("wednesday", "oddDayBlockColumn"))
                .addColumn(blockColumn("thursday", "oddDayBlockColumn"))
                .addColumn(blockColumn("friday", "oddDayBlockColumn"))
                .addColumn(blockColumn("saturday", "oddDayBlockColumn"))
                .addColumn(blockColumn("sunday", "oddDayBlockColumn"))
                .create();
    }

    @Bean
    public ColumnListExtPoint evenWeekCL()
    {
        return columnListExtPointBuilder(EVEN_WEEK_DS)
                .addColumn(textColumn("time", "time.time").width("100px"))
                .addColumn(blockColumn("monday", "evenDayBlockColumn"))
                .addColumn(blockColumn("tuesday", "evenDayBlockColumn"))
                .addColumn(blockColumn("wednesday", "evenDayBlockColumn"))
                .addColumn(blockColumn("thursday", "evenDayBlockColumn"))
                .addColumn(blockColumn("friday", "evenDayBlockColumn"))
                .addColumn(blockColumn("saturday", "evenDayBlockColumn"))
                .addColumn(blockColumn("sunday", "evenDayBlockColumn"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ODD_WEEK_DS, oddWeekCL()).handler(weekPeriodsDSHandler()))
                .addDataSource(searchListDS(EVEN_WEEK_DS, evenWeekCL()).handler(weekPeriodsDSHandler()))
                .addDataSource(selectDS(LECTURE_ROOM_DS, SppTeacherPreferenceManager.instance().lectureRoomComboDSHandler()).addColumn("title", UniplacesPlace.title().s()).addColumn("location", UniplacesPlace.fullLocationInfo().s()))
                .addDataSource(selectDS(BUILDING_DS, SppTeacherPreferenceManager.instance().buildingComboDSHandler()).addColumn("title", UniplacesBuilding.title().s()).addColumn("location", UniplacesBuilding.fullNumber().s()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler weekPeriodsDSHandler()
    {
        return new SppScheduleWeekDSHandler(getName());
    }

    @Bean
    public ButtonListExtPoint actionsButtonExtPoint()
    {
        return buttonListExtPointBuilder(ACTIONS)
                .addButton(submitButton("editTeacherPreference", "onClickEditTeacherPreference")
                        .permissionKey("ui:editPermissionKey")
                        .visible("ui:editTeacherPreferenceVisible").create()) // Редактировать данные расписания
                 .create();
    }
}
