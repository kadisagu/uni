/* $Id: */
package ru.tandemservice.unispp.base.bo.SppDisciplinePreference;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniplaces.entity.catalog.codes.UniplacesPlacePurposeCodes;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unispp.base.bo.SppDisciplinePreference.logic.ISppDisciplinePreferenceDAO;
import ru.tandemservice.unispp.base.bo.SppDisciplinePreference.logic.SppDisciplinePreferenceDAO;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author vnekrasov
 * @since 7/10/14
 */
@Configuration
public class SppDisciplinePreferenceManager extends BusinessObjectManager
{
    public static SppDisciplinePreferenceManager instance()
    {
        return instance(SppDisciplinePreferenceManager.class);
    }

    @Bean
    public ISppDisciplinePreferenceDAO dao()
    {
        return new SppDisciplinePreferenceDAO();
    }

    @Bean
   public IDefaultComboDataSourceHandler buildingComboDSHandler()
   {
       return new EntityComboDataSourceHandler(getName(), UniplacesBuilding.class)
               .order(UniplacesBuilding.title());
   }

    @Bean
    public IDefaultComboDataSourceHandler lectureRoomComboDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UniplacesPlace.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                List<String> placePurposeCodeses = Lists.newArrayList();
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.AUDITORIYA_OBTSHAYA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.AUDITORIYA_SPETSIALIZIROVANNAYA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.LABORATORIYA_OBTSHAYA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.LABORATORIYA_SPETSIALIZIROVANNAYA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.VYCHISLITELNYY_KLASS);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.KABINET);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.SPETSIALIZIROVANNYY_KLASS);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.OBEKT_FIZKULTURY_I_SPORTA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.TRUDOVOE_VOSPITANIE);
                dql.where(DQLExpressions.in(DQLExpressions.property(alias, UniplacesPlace.purpose().code()), placePurposeCodeses));
                dql.where(DQLExpressions.eq(DQLExpressions.property(alias, UniplacesPlace.condition().code()), DQLExpressions.value("1")));
                Long buildingId = context.get("buildingId");
                Collection<Long> buildingIds = context.get("buildingIds");
                if (!(buildingId ==null))
                    dql.where(DQLExpressions.eq(DQLExpressions.property(alias, UniplacesPlace.floor().unit().building().id()), DQLExpressions.value(buildingId)));
                if (buildingIds!=null)
                if (buildingIds.size()>0)
                    dql.where(DQLExpressions.in(DQLExpressions.property(alias, UniplacesPlace.floor().unit().building().id()), buildingIds));
            }


        }.order(UniplacesPlace.title()).filter(UniplacesPlace.title());

//                SppScheduleLectRoomComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler ppsComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), PpsEntry.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                String filter = ep.input.getComboFilterByValue();

                ep.dqlBuilder.where(DQLExpressions.isNull(DQLExpressions.property("e", PpsEntry.removalDate())));
                if(!StringUtils.isEmpty(filter))
                    ep.dqlBuilder.where(DQLExpressions.like(DQLFunctions.upper(DQLFunctions.concat(
                            DQLExpressions.property("e", PpsEntry.person().identityCard().lastName()),
//                            DQLExpressions.property("e", EmployeePost.postRelation().postBoundedWithQGandQL().title()),
                            DQLExpressions.property("e", PpsEntry.orgUnit().fullTitle()))
                    ), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                ep.dqlBuilder.where(exists(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.ppsEntry().s(), property("e")));

                setOrderByProperty(PpsEntry.person().identityCard().lastName().s());
            }
        };
    }
}
