/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;

/**
 * @author nvankov
 * @since 9/4/13
 */
@Configuration
public class SppScheduleAddEdit extends BusinessComponentManager
{
    public static final String GROUP_DS = "groupDS";
    public static final String SEASON_DS = "seasonDS";
    public static final String BELLS_DS = "bellsDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(GROUP_DS, SppScheduleManager.instance().groupComboDSHandler()))
                .addDataSource(selectDS(SEASON_DS, SppScheduleManager.instance().seasonComboDSHandler()).addColumn("title", null, new IFormatter<SppScheduleSeason>()
                {
                    @Override
                    public String format(SppScheduleSeason source)
                    {
                        return source.getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getStartDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndDate()) + ")";
                    }
                }))
                .addDataSource(selectDS(BELLS_DS, SppScheduleManager.instance().bellsComboDSHandler()))
                .create();
    }
}
