package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Событие расписания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppScheduleDailyEventGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent";
    public static final String ENTITY_NAME = "sppScheduleDailyEvent";
    public static final int VERSION_HASH = 1772262720;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE = "date";
    public static final String L_BELL = "bell";
    public static final String P_LECTURE_ROOM = "lectureRoom";
    public static final String P_TEACHER = "teacher";
    public static final String P_SUBJECT = "subject";
    public static final String L_SCHEDULE = "schedule";
    public static final String L_TEACHER_VAL = "teacherVal";
    public static final String L_SUBJECT_VAL = "subjectVal";
    public static final String L_SUBJECT_LOAD_TYPE = "subjectLoadType";
    public static final String L_SUBJECT_ACTION_TYPE = "subjectActionType";
    public static final String L_LECTURE_ROOM_VAL = "lectureRoomVal";

    private Date _date;     // Дата события
    private ScheduleBellEntry _bell;     // Пара
    private String _lectureRoom;     // Аудитория
    private String _teacher;     // Преподаватель
    private String _subject;     // Предмет
    private SppScheduleDaily _schedule;     // Расписание сессии
    private PpsEntry _teacherVal;     // Преподаватель (сущность)
    private EppRegistryElement _subjectVal;     // Предмет(сущность)
    private EppALoadType _subjectLoadType;     // Тип нагрузки(сущность)
    private EppFControlActionType _subjectActionType;     // Форма контроля(сущность)
    private UniplacesPlace _lectureRoomVal;     // Аудитория(сущность)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата события. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата события. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Пара. Свойство не может быть null.
     */
    @NotNull
    public ScheduleBellEntry getBell()
    {
        return _bell;
    }

    /**
     * @param bell Пара. Свойство не может быть null.
     */
    public void setBell(ScheduleBellEntry bell)
    {
        dirty(_bell, bell);
        _bell = bell;
    }

    /**
     * @return Аудитория.
     */
    @Length(max=255)
    public String getLectureRoom()
    {
        return _lectureRoom;
    }

    /**
     * @param lectureRoom Аудитория.
     */
    public void setLectureRoom(String lectureRoom)
    {
        dirty(_lectureRoom, lectureRoom);
        _lectureRoom = lectureRoom;
    }

    /**
     * @return Преподаватель.
     */
    @Length(max=255)
    public String getTeacher()
    {
        return _teacher;
    }

    /**
     * @param teacher Преподаватель.
     */
    public void setTeacher(String teacher)
    {
        dirty(_teacher, teacher);
        _teacher = teacher;
    }

    /**
     * @return Предмет.
     */
    @Length(max=255)
    public String getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Предмет.
     */
    public void setSubject(String subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Расписание сессии.
     */
    public SppScheduleDaily getSchedule()
    {
        return _schedule;
    }

    /**
     * @param schedule Расписание сессии.
     */
    public void setSchedule(SppScheduleDaily schedule)
    {
        dirty(_schedule, schedule);
        _schedule = schedule;
    }

    /**
     * @return Преподаватель (сущность).
     */
    public PpsEntry getTeacherVal()
    {
        return _teacherVal;
    }

    /**
     * @param teacherVal Преподаватель (сущность).
     */
    public void setTeacherVal(PpsEntry teacherVal)
    {
        dirty(_teacherVal, teacherVal);
        _teacherVal = teacherVal;
    }

    /**
     * @return Предмет(сущность).
     */
    public EppRegistryElement getSubjectVal()
    {
        return _subjectVal;
    }

    /**
     * @param subjectVal Предмет(сущность).
     */
    public void setSubjectVal(EppRegistryElement subjectVal)
    {
        dirty(_subjectVal, subjectVal);
        _subjectVal = subjectVal;
    }

    /**
     * @return Тип нагрузки(сущность).
     */
    public EppALoadType getSubjectLoadType()
    {
        return _subjectLoadType;
    }

    /**
     * @param subjectLoadType Тип нагрузки(сущность).
     */
    public void setSubjectLoadType(EppALoadType subjectLoadType)
    {
        dirty(_subjectLoadType, subjectLoadType);
        _subjectLoadType = subjectLoadType;
    }

    /**
     * @return Форма контроля(сущность).
     */
    public EppFControlActionType getSubjectActionType()
    {
        return _subjectActionType;
    }

    /**
     * @param subjectActionType Форма контроля(сущность).
     */
    public void setSubjectActionType(EppFControlActionType subjectActionType)
    {
        dirty(_subjectActionType, subjectActionType);
        _subjectActionType = subjectActionType;
    }

    /**
     * @return Аудитория(сущность).
     */
    public UniplacesPlace getLectureRoomVal()
    {
        return _lectureRoomVal;
    }

    /**
     * @param lectureRoomVal Аудитория(сущность).
     */
    public void setLectureRoomVal(UniplacesPlace lectureRoomVal)
    {
        dirty(_lectureRoomVal, lectureRoomVal);
        _lectureRoomVal = lectureRoomVal;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppScheduleDailyEventGen)
        {
            setDate(((SppScheduleDailyEvent)another).getDate());
            setBell(((SppScheduleDailyEvent)another).getBell());
            setLectureRoom(((SppScheduleDailyEvent)another).getLectureRoom());
            setTeacher(((SppScheduleDailyEvent)another).getTeacher());
            setSubject(((SppScheduleDailyEvent)another).getSubject());
            setSchedule(((SppScheduleDailyEvent)another).getSchedule());
            setTeacherVal(((SppScheduleDailyEvent)another).getTeacherVal());
            setSubjectVal(((SppScheduleDailyEvent)another).getSubjectVal());
            setSubjectLoadType(((SppScheduleDailyEvent)another).getSubjectLoadType());
            setSubjectActionType(((SppScheduleDailyEvent)another).getSubjectActionType());
            setLectureRoomVal(((SppScheduleDailyEvent)another).getLectureRoomVal());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppScheduleDailyEventGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppScheduleDailyEvent.class;
        }

        public T newInstance()
        {
            return (T) new SppScheduleDailyEvent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "date":
                    return obj.getDate();
                case "bell":
                    return obj.getBell();
                case "lectureRoom":
                    return obj.getLectureRoom();
                case "teacher":
                    return obj.getTeacher();
                case "subject":
                    return obj.getSubject();
                case "schedule":
                    return obj.getSchedule();
                case "teacherVal":
                    return obj.getTeacherVal();
                case "subjectVal":
                    return obj.getSubjectVal();
                case "subjectLoadType":
                    return obj.getSubjectLoadType();
                case "subjectActionType":
                    return obj.getSubjectActionType();
                case "lectureRoomVal":
                    return obj.getLectureRoomVal();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "bell":
                    obj.setBell((ScheduleBellEntry) value);
                    return;
                case "lectureRoom":
                    obj.setLectureRoom((String) value);
                    return;
                case "teacher":
                    obj.setTeacher((String) value);
                    return;
                case "subject":
                    obj.setSubject((String) value);
                    return;
                case "schedule":
                    obj.setSchedule((SppScheduleDaily) value);
                    return;
                case "teacherVal":
                    obj.setTeacherVal((PpsEntry) value);
                    return;
                case "subjectVal":
                    obj.setSubjectVal((EppRegistryElement) value);
                    return;
                case "subjectLoadType":
                    obj.setSubjectLoadType((EppALoadType) value);
                    return;
                case "subjectActionType":
                    obj.setSubjectActionType((EppFControlActionType) value);
                    return;
                case "lectureRoomVal":
                    obj.setLectureRoomVal((UniplacesPlace) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "date":
                        return true;
                case "bell":
                        return true;
                case "lectureRoom":
                        return true;
                case "teacher":
                        return true;
                case "subject":
                        return true;
                case "schedule":
                        return true;
                case "teacherVal":
                        return true;
                case "subjectVal":
                        return true;
                case "subjectLoadType":
                        return true;
                case "subjectActionType":
                        return true;
                case "lectureRoomVal":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "date":
                    return true;
                case "bell":
                    return true;
                case "lectureRoom":
                    return true;
                case "teacher":
                    return true;
                case "subject":
                    return true;
                case "schedule":
                    return true;
                case "teacherVal":
                    return true;
                case "subjectVal":
                    return true;
                case "subjectLoadType":
                    return true;
                case "subjectActionType":
                    return true;
                case "lectureRoomVal":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "date":
                    return Date.class;
                case "bell":
                    return ScheduleBellEntry.class;
                case "lectureRoom":
                    return String.class;
                case "teacher":
                    return String.class;
                case "subject":
                    return String.class;
                case "schedule":
                    return SppScheduleDaily.class;
                case "teacherVal":
                    return PpsEntry.class;
                case "subjectVal":
                    return EppRegistryElement.class;
                case "subjectLoadType":
                    return EppALoadType.class;
                case "subjectActionType":
                    return EppFControlActionType.class;
                case "lectureRoomVal":
                    return UniplacesPlace.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppScheduleDailyEvent> _dslPath = new Path<SppScheduleDailyEvent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppScheduleDailyEvent");
    }
            

    /**
     * @return Дата события. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Пара. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getBell()
     */
    public static ScheduleBellEntry.Path<ScheduleBellEntry> bell()
    {
        return _dslPath.bell();
    }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getLectureRoom()
     */
    public static PropertyPath<String> lectureRoom()
    {
        return _dslPath.lectureRoom();
    }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getTeacher()
     */
    public static PropertyPath<String> teacher()
    {
        return _dslPath.teacher();
    }

    /**
     * @return Предмет.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getSubject()
     */
    public static PropertyPath<String> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Расписание сессии.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getSchedule()
     */
    public static SppScheduleDaily.Path<SppScheduleDaily> schedule()
    {
        return _dslPath.schedule();
    }

    /**
     * @return Преподаватель (сущность).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getTeacherVal()
     */
    public static PpsEntry.Path<PpsEntry> teacherVal()
    {
        return _dslPath.teacherVal();
    }

    /**
     * @return Предмет(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getSubjectVal()
     */
    public static EppRegistryElement.Path<EppRegistryElement> subjectVal()
    {
        return _dslPath.subjectVal();
    }

    /**
     * @return Тип нагрузки(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getSubjectLoadType()
     */
    public static EppALoadType.Path<EppALoadType> subjectLoadType()
    {
        return _dslPath.subjectLoadType();
    }

    /**
     * @return Форма контроля(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getSubjectActionType()
     */
    public static EppFControlActionType.Path<EppFControlActionType> subjectActionType()
    {
        return _dslPath.subjectActionType();
    }

    /**
     * @return Аудитория(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getLectureRoomVal()
     */
    public static UniplacesPlace.Path<UniplacesPlace> lectureRoomVal()
    {
        return _dslPath.lectureRoomVal();
    }

    public static class Path<E extends SppScheduleDailyEvent> extends EntityPath<E>
    {
        private PropertyPath<Date> _date;
        private ScheduleBellEntry.Path<ScheduleBellEntry> _bell;
        private PropertyPath<String> _lectureRoom;
        private PropertyPath<String> _teacher;
        private PropertyPath<String> _subject;
        private SppScheduleDaily.Path<SppScheduleDaily> _schedule;
        private PpsEntry.Path<PpsEntry> _teacherVal;
        private EppRegistryElement.Path<EppRegistryElement> _subjectVal;
        private EppALoadType.Path<EppALoadType> _subjectLoadType;
        private EppFControlActionType.Path<EppFControlActionType> _subjectActionType;
        private UniplacesPlace.Path<UniplacesPlace> _lectureRoomVal;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата события. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(SppScheduleDailyEventGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Пара. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getBell()
     */
        public ScheduleBellEntry.Path<ScheduleBellEntry> bell()
        {
            if(_bell == null )
                _bell = new ScheduleBellEntry.Path<ScheduleBellEntry>(L_BELL, this);
            return _bell;
        }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getLectureRoom()
     */
        public PropertyPath<String> lectureRoom()
        {
            if(_lectureRoom == null )
                _lectureRoom = new PropertyPath<String>(SppScheduleDailyEventGen.P_LECTURE_ROOM, this);
            return _lectureRoom;
        }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getTeacher()
     */
        public PropertyPath<String> teacher()
        {
            if(_teacher == null )
                _teacher = new PropertyPath<String>(SppScheduleDailyEventGen.P_TEACHER, this);
            return _teacher;
        }

    /**
     * @return Предмет.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getSubject()
     */
        public PropertyPath<String> subject()
        {
            if(_subject == null )
                _subject = new PropertyPath<String>(SppScheduleDailyEventGen.P_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Расписание сессии.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getSchedule()
     */
        public SppScheduleDaily.Path<SppScheduleDaily> schedule()
        {
            if(_schedule == null )
                _schedule = new SppScheduleDaily.Path<SppScheduleDaily>(L_SCHEDULE, this);
            return _schedule;
        }

    /**
     * @return Преподаватель (сущность).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getTeacherVal()
     */
        public PpsEntry.Path<PpsEntry> teacherVal()
        {
            if(_teacherVal == null )
                _teacherVal = new PpsEntry.Path<PpsEntry>(L_TEACHER_VAL, this);
            return _teacherVal;
        }

    /**
     * @return Предмет(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getSubjectVal()
     */
        public EppRegistryElement.Path<EppRegistryElement> subjectVal()
        {
            if(_subjectVal == null )
                _subjectVal = new EppRegistryElement.Path<EppRegistryElement>(L_SUBJECT_VAL, this);
            return _subjectVal;
        }

    /**
     * @return Тип нагрузки(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getSubjectLoadType()
     */
        public EppALoadType.Path<EppALoadType> subjectLoadType()
        {
            if(_subjectLoadType == null )
                _subjectLoadType = new EppALoadType.Path<EppALoadType>(L_SUBJECT_LOAD_TYPE, this);
            return _subjectLoadType;
        }

    /**
     * @return Форма контроля(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getSubjectActionType()
     */
        public EppFControlActionType.Path<EppFControlActionType> subjectActionType()
        {
            if(_subjectActionType == null )
                _subjectActionType = new EppFControlActionType.Path<EppFControlActionType>(L_SUBJECT_ACTION_TYPE, this);
            return _subjectActionType;
        }

    /**
     * @return Аудитория(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent#getLectureRoomVal()
     */
        public UniplacesPlace.Path<UniplacesPlace> lectureRoomVal()
        {
            if(_lectureRoomVal == null )
                _lectureRoomVal = new UniplacesPlace.Path<UniplacesPlace>(L_LECTURE_ROOM_VAL, this);
            return _lectureRoomVal;
        }

        public Class getEntityClass()
        {
            return SppScheduleDailyEvent.class;
        }

        public String getEntityName()
        {
            return "sppScheduleDailyEvent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
