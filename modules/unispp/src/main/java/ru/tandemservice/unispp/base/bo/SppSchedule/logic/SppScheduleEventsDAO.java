/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import com.google.common.collect.Sets;
import org.joda.time.LocalDate;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppScheduleICal;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix;
import ru.tandemservice.unispp.base.vo.SppScheduleEventInDayVO;
import ru.tandemservice.unispp.util.SppScheduleUtil;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author nvankov
 * @since 2/14/14
 */
public class SppScheduleEventsDAO extends UniBaseDao implements ISppScheduleEventsDAO
{
    @Override
    public void sendICalendars()
    {
    }

    @Override
    public void sendICalendar(SppSchedule schedule)
    {
    }

    @Override
    public boolean getScheduleDataOrRecipientsChanged(Long scheduleId)
    {
        return false;
    }

    @Override
    public void cancelICalendar(List<SppScheduleICal> iCalDataList)
    {
    }

    @Override
    public Map<String, LocalDate> getDatesMap(Date startDate, Date endDate)
    {
        return null;
    }


    /**
     * Создаёт все события из некоторого списка расписаний с переданными фильтрами/параметрами
     * @param scheduleIdsList список id-шников расписаний
     * @param withFixes с исправлениями или без
     * @param dateFrom фильтр "от"
     * @param dateTo фильтр "до"
     * @param teacherIds фильтр по преподавателям
     * @param placeIds фильтр по аудиториям
     * @param subjectIds фильтр по предметам
     */
    @Override
    @Transactional(readOnly = true)
    public List<SppScheduleEventInDayVO> createAllScheduleEvents(List<Long> scheduleIdsList, boolean withFixes, Date dateFrom, Date dateTo, List<Long> teacherIds, List<Long> placeIds, List<Long> subjectIds)
    {
        final Set<SppScheduleEventInDayVO> eventSet = Sets.newHashSet();

        // возьмём все пары всех расписаний
        DQLSelectBuilder pBuilder = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "p").column("p")
                .fetchPath(DQLJoinType.inner, SppSchedulePeriod.weekRow().bell().fromAlias("p"), "bell")
                .fetchPath(DQLJoinType.inner, SppSchedulePeriod.weekRow().schedule().group().fromAlias("p"), "sch")
                .fetchPath(DQLJoinType.left, SppSchedulePeriod.teacherVal().fromAlias("p"), "tr")
                .fetchPath(DQLJoinType.left, SppSchedulePeriod.lectureRoomVal().fromAlias("p"), "lr")
                .fetchPath(DQLJoinType.left, SppSchedulePeriod.subjectVal().fromAlias("p"), "sbj")
                .where(in(property("p", SppSchedulePeriod.weekRow().schedule().id()), scheduleIdsList));
        // и сгруппируем их по расписанию
        Map<SppSchedule, Set<SppSchedulePeriod>> periodSets = getList(pBuilder).stream().map(obj -> (SppSchedulePeriod) obj)
                .collect(Collectors.groupingBy(p -> p.getWeekRow().getSchedule(), Collectors.mapping(p -> p, Collectors.toSet())));

        // возьмём все исправления во всех расписаниях
        DQLSelectBuilder fpBuilder = new DQLSelectBuilder().fromEntity(SppSchedulePeriodFix.class, "fp").column("fp")
                .fetchPath(DQLJoinType.inner, SppSchedulePeriodFix.bell().fromAlias("fp"), "bell")
                .fetchPath(DQLJoinType.inner, SppSchedulePeriodFix.sppSchedule().fromAlias("fp"), "sch")
                .where(in(property("fp", SppSchedulePeriodFix.sppSchedule().id()), scheduleIdsList));
        // и сгруппируем их по расписанию
        Map<SppSchedule, Set<SppSchedulePeriodFix>> periodFixSets = getList(fpBuilder).stream().map(obj -> (SppSchedulePeriodFix) obj)
                .collect(Collectors.groupingBy(SppSchedulePeriodFix::getSppSchedule, Collectors.mapping(p -> p, Collectors.toSet())));

        // для каждого расписания
        for (SppSchedule schedule : periodSets.keySet())
        {
            // возьмём все занятия (по дням недели) и все исправления (по дням) для данного расписания
            Map<Integer, Set<SppSchedulePeriod>> periodsByDay = periodSets.get(schedule).stream()
                    .collect(Collectors.groupingBy(SppSchedulePeriod::getDayOfWeek, Collectors.mapping(p -> p, Collectors.toSet())));
            Map<LocalDate, Set<SppSchedulePeriodFix>> periodFixesByDay = periodFixSets.getOrDefault(schedule, Collections.emptySet()).stream()
                    .collect(Collectors.groupingBy(fp -> LocalDate.fromDateFields(fp.getDate()), Collectors.mapping(p -> p, Collectors.toSet())));
            // (P.S. да, я люблю стримы, много стримов)

            Date startDate = dateFrom != null && dateFrom.after(schedule.getSeason().getStartDate()) ? dateFrom : schedule.getSeason().getStartDate();
            Date endDate = dateTo != null && dateTo.before(schedule.getSeason().getEndDate()) ? dateTo : schedule.getSeason().getEndDate();

            Boolean isDiffEven = schedule.isDiffEven();
            Boolean isEven = schedule.getSeason().isStartFromEven();
            if (dateFrom != null && (SppScheduleUtil.getDifferenceInWeeks(schedule.getSeason().getStartDate(), startDate) % 2) != 0)
                isEven = !isEven;

            LocalDate iterLDate = new LocalDate(startDate);
            LocalDate endLDate = new LocalDate(endDate);
            while (!iterLDate.isAfter(endLDate))
            {
                Set<SppScheduleEventInDayVO> eventsInDaySet = Sets.newHashSet();
                // возьмём все занятия этого дня, дня из соседней недели и все исправления этого дня
                final boolean finalIsEven = isEven;
                final LocalDate finalIterLDate = iterLDate;
                Set<SppSchedulePeriod> periodsInDay = periodsByDay.getOrDefault(iterLDate.getDayOfWeek(), Collections.emptySet()).stream()
                        .filter(p -> (!isDiffEven || p.getWeekRow().isEven() == finalIsEven) &&
                                p.isInsidePeriod(finalIterLDate.toDate()))
                        .collect(Collectors.toSet());
                Set<SppSchedulePeriod> periodsInDayFromNeighbourWeek = periodsByDay.getOrDefault(iterLDate.getDayOfWeek(), Collections.emptySet()).stream()
                        .filter(np -> (isDiffEven && np.getWeekRow().isEven() != finalIsEven) &&
                                np.isInsidePeriod(finalIterLDate.toDate()))
                        .collect(Collectors.toSet());
                Set<SppSchedulePeriodFix> periodFixesInDay = periodFixesByDay.getOrDefault(iterLDate, Collections.emptySet());

                // добавим периоды
                eventsInDaySet.addAll(periodsInDay.stream().filter(p -> !p.isEmpty())
                        .map(p -> new SppScheduleEventInDayVO(p, finalIterLDate)).collect(Collectors.toList()));

                // добавим периоды из соседней недели (если для них есть место)
                eventsInDaySet.addAll(periodsInDayFromNeighbourWeek.stream().filter(np -> !np.isEmpty())
                        .filter(np -> periodsInDay.stream().filter(p -> p.getWeekRow().getBell().equals(np.getWeekRow().getBell())).count() == 0)
                        .map(np -> new SppScheduleEventInDayVO(np, finalIterLDate)).collect(Collectors.toList()));

                // ========== налепим заплатки (исправления занятий) ==========
                periodFixesInDay.stream()
                        .forEach(fp -> {
                            // создадим исправление и попробуем найти занятия, которые оно заменяет
                            SppScheduleEventInDayVO fixEvent = new SppScheduleEventInDayVO(fp, SppScheduleEventInDayVO.STATUS_ADDED);
                            List<SppScheduleEventInDayVO> events = eventsInDaySet.stream().filter(e -> e.isFixMatch(fixEvent)).collect(Collectors.toList());
                            // если исправление что-то заменяет
                            if (!events.isEmpty())
                            {
                                // если исправления учитываются, то поставим исправление, и уберём заменяемое занятие
                                if (withFixes)
                                {
                                    fixEvent.setStatus(fp.isCanceled() ? SppScheduleEventInDayVO.STATUS_CANCELED : SppScheduleEventInDayVO.STATUS_CHANGED);
                                    eventsInDaySet.removeAll(events);
                                    eventsInDaySet.add(fixEvent);
                                }
                                // ... если нет, то просто проставим соотв. статус заменяемому занятию (по идее оно одно, но мало ли..)
                                else
                                    events.forEach(e -> e.setStatus(fp.isCanceled() ? SppScheduleEventInDayVO.STATUS_CANCELED : SppScheduleEventInDayVO.STATUS_CHANGED));
                            }
                            // если не нашли что заменять, значит это исправление - добавляющее
                            else if (withFixes && !fp.isCanceled())
                                eventsInDaySet.add(fixEvent);
                        });
                // ============================================================

                eventSet.addAll(eventsInDaySet);

                if (iterLDate.getDayOfWeek() == 7)
                    isEven = !isEven;

                iterLDate = iterLDate.plusDays(1);
            }
        }
        return eventSet.stream()
                .filter(e -> (subjectIds == null || subjectIds.isEmpty() || (e.getSubjectVal() != null && subjectIds.contains(e.getSubjectVal().getId()))) &&
                        (teacherIds == null || teacherIds.isEmpty() || (e.getTeacherVal() != null && teacherIds.contains(e.getTeacherVal().getId()))) &&
                        (placeIds == null || placeIds.isEmpty() || (e.getLectureRoomVal() != null && placeIds.contains(e.getLectureRoomVal().getId()))))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<SppScheduleEventInDayVO> createAllScheduleEvents(List<Long> scheduleIdsList, boolean withFixes)
    {
        // без фильтров
        return createAllScheduleEvents(scheduleIdsList, withFixes, null, null, null, null, null);
    }
}
