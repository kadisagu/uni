/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.SeasonList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.SeasonAddEdit.SppScheduleSeasonAddEdit;

/**
 * @author nvankov
 * @since 9/3/13
 */
public class SppScheduleSeasonListUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleSeasonList.SCHEDULE_SEASON_DS.equals(dataSource.getName()))
        {
            dataSource.put("title", _uiSettings.get("title"));
        }
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(SppScheduleSeasonAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickAddScheduleSeason()
    {
        _uiActivation.asRegionDialog(SppScheduleSeasonAddEdit.class).activate();
    }

}
