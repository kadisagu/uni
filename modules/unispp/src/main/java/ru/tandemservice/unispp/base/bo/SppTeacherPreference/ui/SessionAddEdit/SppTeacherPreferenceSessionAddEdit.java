/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.SessionAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 7/3/14
 */
@Configuration
public class SppTeacherPreferenceSessionAddEdit extends BusinessComponentManager
{
    public static final String SEASON_DS = "seasonDS";
    public static final String BELLS_DS = "bellsDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(SEASON_DS, seasonComboDSHandler()).addColumn("title", null, new IFormatter<SppScheduleSessionSeason>()
                {
                    @Override
                    public String format(SppScheduleSessionSeason source)
                    {
                        return source.getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getStartDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndDate()) + ")";
                    }
                }))
                .addDataSource(selectDS(BELLS_DS, SppTeacherPreferenceManager.instance().bellsComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler seasonComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), SppScheduleSessionSeason.class, SppScheduleSessionSeason.title())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                Object employee = ep.context.get(SppTeacherPreferenceManager.EMPLOYEE);
                Long editedPref = ep.context.get(SppTeacherPreferenceManager.EDITED);
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(SppTeacherSessionPreferenceList.class, "pref")
                        .column(property("pref", SppTeacherSessionPreferenceList.sppScheduleSessionSeason()))
                        .where(eq(property("pref", SppTeacherSessionPreferenceList.teacher()), commonValue(employee)));
                if (editedPref != null)
                    subBuilder.where(ne(property("pref.id"), value(editedPref)));
                ep.dqlBuilder.where(notIn(property("e.id"), subBuilder.buildQuery()));
            }
        };
    }
}
