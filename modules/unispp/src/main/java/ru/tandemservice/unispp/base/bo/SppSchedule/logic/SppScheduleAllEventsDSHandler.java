/* $Id: $ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import com.google.common.collect.Lists;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionEvent;
import ru.tandemservice.unispp.base.vo.SppScheduleEventInDayVO;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
public class SppScheduleAllEventsDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String PROP_SHOW_ORIGINAL_DATA = "showOriginalData";
    public static final String PROP_YEAR_PART_ID = "yearPartId";
    public static final String PROP_DATE_FROM = "dateFrom";
    public static final String PROP_DATE_TO = "dateTo";
    public static final String PROP_ORG_UNIT_IDS = "orgUnitIds";
    public static final String PROP_EDU_LEVEL_IDS = "eduLevelIds";
    public static final String PROP_GROUP_IDS = "groupIds";
    public static final String PROP_SUBJECT_IDS = "subjectIds";
    public static final String PROP_TEACHER_IDS = "teacherIds";
    public static final String PROP_PLACES_IDS = "placeIds";
    public static final String PROP_STATUSES = "statuses";

    public SppScheduleAllEventsDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        boolean isShowOriginalData = context.getBoolean(PROP_SHOW_ORIGINAL_DATA, Boolean.FALSE);

        Date dateFrom = context.get(PROP_DATE_FROM);
        Date dateTo = context.get(PROP_DATE_TO);

        Long yearPartId = context.get(PROP_YEAR_PART_ID);
        List<Long> orgUnitIds = context.get(PROP_ORG_UNIT_IDS);
        List<Long> eduLevelIds = context.get(PROP_EDU_LEVEL_IDS);
        List<Long> groupIds = context.get(PROP_GROUP_IDS);
        List<Long> subjectIds = context.get(PROP_SUBJECT_IDS);
        List<Long> teacherIds = context.get(PROP_TEACHER_IDS);
        List<Long> placeIds = context.get(PROP_PLACES_IDS);
        List<IdentifiableWrapper> statuses = context.get(PROP_STATUSES);

        DSOutput output = new DSOutput(input);

        // часть учебного года обязательна, поэтому если она не пришла, то ничего не формируем и сразу выходим
        if (yearPartId == null) return output;

        DQLSelectBuilder builderDaily = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "e");
        DQLSelectBuilder builderSession = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "e");
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppSchedule.class, "e");

        builderDaily.where(eq(property("e", SppScheduleDailyEvent.schedule().season().eppYearPart().id()), value(yearPartId)));
        builderSession.where(eq(property("e", SppScheduleSessionEvent.schedule().season().eppYearPart().id()), value(yearPartId)));
        builder.where(eq(property("e", SppSchedule.season().eppYearPart().id()), value(yearPartId)));

        builderDaily.where(betweenDays(SppScheduleDailyEvent.date().fromAlias("e"), dateFrom, dateTo));
        builderSession.where(betweenDays(SppScheduleSessionEvent.date().fromAlias("e"), dateFrom, dateTo));
        if (dateFrom != null) builder.where(ge(property("e", SppSchedule.season().endDate()), valueDate(dateFrom)));
        if (dateTo != null) builder.where(le(property("e", SppSchedule.season().startDate()), valueDate(dateTo)));

        if (null != teacherIds && !teacherIds.isEmpty())
        {
            builderDaily.where(in(property("e", SppScheduleDailyEvent.teacherVal().id()), teacherIds));
            builderSession.where(in(property("e", SppScheduleSessionEvent.teacherVal().id()), teacherIds));
        }

        if (null != placeIds && !placeIds.isEmpty())
        {
            builderDaily.where(in(property("e", SppScheduleDailyEvent.lectureRoomVal().id()), placeIds));
            builderSession.where(in(property("e", SppScheduleSessionEvent.lectureRoomVal().id()), placeIds));
        }

        if (null != eduLevelIds && !eduLevelIds.isEmpty())
        {
            builderDaily.where(in(property("e", SppScheduleDailyEvent.schedule().group().educationOrgUnit().educationLevelHighSchool().id()), eduLevelIds));
            builderSession.where(in(property("e", SppScheduleSessionEvent.schedule().group().educationOrgUnit().educationLevelHighSchool().id()), eduLevelIds));
            builder.where(in(property("e", SppSchedule.group().educationOrgUnit().educationLevelHighSchool().id()), eduLevelIds));
        }

        if (null != groupIds && !groupIds.isEmpty())
        {
            builderDaily.where(in(property("e", SppScheduleDailyEvent.schedule().group().id()), groupIds));
            builderSession.where(in(property("e", SppScheduleSessionEvent.schedule().group().id()), groupIds));
            builder.where(in(property("e", SppSchedule.group().id()), groupIds));
        }

        if (null != subjectIds && !subjectIds.isEmpty())
        {
            builderDaily.where(in(property("e", SppScheduleDailyEvent.subjectVal().id()), subjectIds));
            builderSession.where(in(property("e", SppScheduleSessionEvent.subjectVal().id()), subjectIds));
        }


        if (null != orgUnitIds && !orgUnitIds.isEmpty())
        {
            DQLSelectBuilder kindRelationsBuilder = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                    .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                    .where(in(property("rel", OrgUnitToKindRelation.orgUnit().id()), orgUnitIds));
            List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

            Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                    .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

            IDQLExpression condition = nothing(); // false
            IDQLExpression conditionDaily = nothing(); // false
            IDQLExpression conditionSession = nothing(); // false

            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
            {
                condition = or(condition, in(property("e", SppSchedule.group().educationOrgUnit().educationLevelHighSchool().orgUnit().id()), orgUnitIds));
                conditionDaily = or(conditionDaily, in(property("e", SppScheduleDailyEvent.schedule().group().educationOrgUnit().educationLevelHighSchool().orgUnit().id()), orgUnitIds));
                conditionSession = or(conditionSession, in(property("e", SppScheduleSessionEvent.schedule().group().educationOrgUnit().educationLevelHighSchool().orgUnit().id()), orgUnitIds));
            }

            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
            {
                condition = or(condition, in(property("e", SppSchedule.group().educationOrgUnit().formativeOrgUnit().id()), orgUnitIds));
                conditionDaily = or(conditionDaily, in(property("e", SppScheduleDailyEvent.schedule().group().educationOrgUnit().formativeOrgUnit().id()), orgUnitIds));
                conditionSession = or(conditionSession, in(property("e", SppScheduleSessionEvent.schedule().group().educationOrgUnit().formativeOrgUnit().id()), orgUnitIds));
            }

            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
            {
                condition = or(condition, in(property("e", SppSchedule.group().educationOrgUnit().territorialOrgUnit().id()), orgUnitIds));
                conditionDaily = or(conditionDaily, in(property("e", SppScheduleDailyEvent.schedule().group().educationOrgUnit().territorialOrgUnit().id()), orgUnitIds));
                conditionSession = or(conditionSession, in(property("e", SppScheduleSessionEvent.schedule().group().educationOrgUnit().territorialOrgUnit().id()), orgUnitIds));
            }

            builder.where(condition);
            builderDaily.where(conditionDaily);
            builderSession.where(conditionSession);
        }

        // список всех событий
        List<SppScheduleEventInDayVO> eventList = Lists.newArrayList();

        // добавим события подневных расписаний
        List<SppScheduleDailyEvent> dailyList = createStatement(builderDaily).list();
        eventList.addAll(dailyList.stream().map(SppScheduleEventInDayVO::new).collect(Collectors.toList()));

        // добавим события расписаний сессии
        List<SppScheduleSessionEvent> sessionList = createStatement(builderSession).list();
        eventList.addAll(sessionList.stream().map(SppScheduleEventInDayVO::new).collect(Collectors.toList()));

        // добавим события обычных расписаний (занятий)
        List<SppSchedule> scheduleList = createStatement(builder).list();
        eventList.addAll(SppScheduleManager.instance().eventsDao().createAllScheduleEvents(
                scheduleList.stream().map(EntityBase::getId).collect(Collectors.toList()), !isShowOriginalData, dateFrom, dateTo, teacherIds, placeIds, subjectIds));

        // список врапперов на выход (с фильтром по статусу, и сортировкой по дате и группе)
        List<DataWrapper> resultList = eventList.stream().filter(e -> (statuses.isEmpty() || (e.getStatus() != null && statuses.contains(e.getStatus()))))
                .sorted(Comparator.comparing(SppScheduleEventInDayVO::getDate)
                        .thenComparing(e -> e.getBellEntry().getStartTime())
                        .thenComparing(SppScheduleEventInDayVO::getGroupTitle)
                        .thenComparing(SppScheduleEventInDayVO::getNum))
                .map(SppScheduleEventInDayVO::getDataWrapper).collect(Collectors.toList());

        output.setTotalSize(resultList.size());
        final List<DataWrapper> subList = resultList.subList(output.getStartRecord(), Math.min(output.getStartRecord() + output.getCountRecord(), resultList.size()));
        output.getRecordList().addAll(subList);

        return output;
    }
}
