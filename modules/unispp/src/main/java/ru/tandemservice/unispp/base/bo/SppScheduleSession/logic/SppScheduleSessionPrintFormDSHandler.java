/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionPrintForm;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 9/27/13
 */
public class SppScheduleSessionPrintFormDSHandler extends DefaultSearchDataSourceHandler
{
    // properties
    public static final String PROP_YEAR_PART_ID = "yearPartId";

    public SppScheduleSessionPrintFormDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long yearPartId = context.get(PROP_YEAR_PART_ID);

        Long orgUnitId = context.get("orgUnitId");
        Long adminId = context.get("admin");
        List<Long> groups = context.get("groups");

        List<Long> orgUnitIds = context.get("orgUnitIds");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleSessionPrintForm.class, "pf");

        if (null != orgUnitId)
            builder.where(eq(property("pf", SppScheduleSessionPrintForm.createOU().id()), value(orgUnitId)));

        if (null != orgUnitIds && !orgUnitIds.isEmpty())
            builder.where(in(property("pf", SppScheduleSessionPrintForm.createOU().id()), orgUnitIds));

        if (null != adminId)
            builder.where(eq(property("pf", SppScheduleSessionPrintForm.adminOOP().id()), value(adminId)));
        if (null != groups && !groups.isEmpty())
        {
            DQLSelectBuilder groupBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g");
            groupBuilder.column(DQLFunctions.upper(property("g", Group.title())));
            groupBuilder.where(in(property("g", Group.id()), groups));
            List<String> groupsTitles = createStatement(groupBuilder).list();
            List<IDQLExpression> expressions = groupsTitles.stream().map(groupTitle -> like(DQLFunctions.upper(property("pf", SppScheduleSessionPrintForm.groups())), value(CoreStringUtils.escapeLike(groupTitle, true)))).collect(Collectors.toList());
            builder.where(or(expressions.toArray(new IDQLExpression[expressions.size()])));
        }

        if (yearPartId != null)
            builder.where(eq(property("pf", SppScheduleSessionPrintForm.season().eppYearPart().id()), value(yearPartId)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
    }
}
