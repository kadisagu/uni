package ru.tandemservice.unispp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unispp_2x10x6_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sppBellScheduleEntry переименована в scheduleBellEntry
		{
			tool.renameTable("spp_bell_schedule_entry", "sc_bell_entry");
			tool.entityCodes().rename("sppBellScheduleEntry", "scheduleBellEntry");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppBellSchedule переименована в scheduleBell
		{
			tool.renameTable("spp_bell_schedule", "sc_bell");
			tool.entityCodes().rename("sppBellSchedule", "scheduleBell");
		}
    }
}