/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.SessionView;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.logic.SppTeacherSessionPreferenceElementDSHandler;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement;

/**
 * @author vnekrasov
 * @since 7/2/14
 */
@Configuration
public class SppTeacherPreferenceSessionView extends BusinessComponentManager
{
    public final static String SESSION_PREFERENCE_ELEMENTS_DS = "elementsDS";

    @Bean
    public ColumnListExtPoint TeacherSessionPreferenceElementsCL()
    {
        return columnListExtPointBuilder(SESSION_PREFERENCE_ELEMENTS_DS)
                .addColumn(dateColumn("startDate", SppTeacherSessionPreferenceElement.startDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(dateColumn("endDate", SppTeacherSessionPreferenceElement.endDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn("dayOfWeek", SppTeacherSessionPreferenceElement.dayOfWeek()))
                .addColumn(textColumn("bell", SppTeacherSessionPreferenceElement.bell().title()))
                .addColumn(textColumn("building", SppTeacherSessionPreferenceElement.building().title()))
                .addColumn(textColumn("lectureRoom", SppTeacherSessionPreferenceElement.lectureRoom().title()))
                .addColumn(booleanColumn("unwantedTime", SppTeacherSessionPreferenceElement.unwantedTime()))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onClickEditElement")
                        .permissionKey("ui:editPermissionKey")
                        .disabled("ui:elementEditDisabled"))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDeleteElement")
                        .permissionKey("ui:editPermissionKey")
                        .disabled("ui:elementDeleteDisabled"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SESSION_PREFERENCE_ELEMENTS_DS, TeacherSessionPreferenceElementsCL(), teacherSessionPreferenceElementsDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler teacherSessionPreferenceElementsDSHandler()
    {
        return new SppTeacherSessionPreferenceElementDSHandler(getName());
    }
}
