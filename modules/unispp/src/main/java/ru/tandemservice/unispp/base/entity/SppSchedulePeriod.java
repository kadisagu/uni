package ru.tandemservice.unispp.base.entity;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unispp.base.entity.gen.SppSchedulePeriodGen;

import java.util.Date;

/**
 * Пара
 */
public class SppSchedulePeriod extends SppSchedulePeriodGen
{
    public String getPrint()
    {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotEmpty(getPeriodGroup())) sb.append(getPeriodGroup());
        sb.append(" ");
        if (getSubjectLoadType() != null && getSubjectVal() != null)
            sb.append(getSubjectVal().getTitle()).append(" (").append(getSubjectLoadType().getAbbreviation()).append(")");
        else
            sb.append(getSubject());
        if (getStartDate() != null || getEndDate() != null)
            sb.append(" (").append(getPeriodTitle()).append(")");
        sb.append(" ").append(getTeacherVal() != null ? getTeacherVal().getTitleFioInfoOrgUnit() : getTeacher());
        sb.append(" ").append(getLectureRoom());
        return sb.toString();
    }

    /**
     * @return период пары (если указан)
     * P.S. да, получилась небольшая путаница, т.к. сама пара здесь тоже называется period
     */
    public String getPeriodTitle()
    {
        StringBuilder sb = new StringBuilder();
        if (getStartDate() != null || getEndDate() != null)
        {
            sb.append(getStartDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(getStartDate()) : "...");
            sb.append(" - ");
            sb.append(getEndDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndDate()) : "...");
        }
        return sb.toString();
    }

    /**
     * @return входит ли указанная дата в период занятия
     */
    public boolean isInsidePeriod(Date date)
    {
        // чтобы не заморачиваться с временем перегоним в LocalDate из joda
        LocalDate localDate = LocalDate.fromDateFields(date);
        LocalDate startDate = getStartDate() != null ? LocalDate.fromDateFields(getStartDate()) : null;
        LocalDate endDate = getEndDate() != null ?  LocalDate.fromDateFields(getEndDate()) : null;
        return (startDate == null || !localDate.isBefore(startDate)) && (endDate == null || !localDate.isAfter(endDate));
    }
}
