/* $Id$ */
package ru.tandemservice.unispp.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;

import java.util.Date;

/**
 * @author nvankov
 * @since 9/4/13
 */
public class SppSchedulePeriodVO
{
    private SppSchedulePeriod _period;
    private SppScheduleWeekPeriodsRowVO _weekPeriodsRowVO;

    private Date _startDate;
    private Date _endDate;
    private String _lectureRoom;     // Аудитория
    private String _teacher;     // Преподаватель
    private String _subject;     // Предмет
    private String _periodGroup;     // Группа
    private boolean _empty;     // Занятия нет
    private SppScheduleDisciplineVO _subjectVal;
    private PpsEntry _teacherVal;

    private DataWrapper _lectureRoomVal;

    private UniplacesClassroomType _lectureRoomType;

    public SppSchedulePeriodVO(SppSchedulePeriod period)
    {
        _period = period;
        _startDate = period.getStartDate();
        _endDate = period.getEndDate();
        _lectureRoom = period.getLectureRoom();
        _teacher = period.getTeacher();
        _subject = period.getSubject();
        _periodGroup = period.getPeriodGroup();
        _empty = period.isEmpty();
        if (null != period.getSubjectVal())
        {
            EppRegistryDiscipline discipline = (EppRegistryDiscipline) period.getSubjectVal();
            EppWorkPlan workPlan = SppScheduleManager.instance().dao().getWorkPlanByDiscipline(discipline);
            Term term = workPlan != null ? workPlan.getTerm() : null;
            _subjectVal = new SppScheduleDisciplineVO(discipline, period.getSubjectLoadType(), term);
        }
        _teacherVal = period.getTeacherVal();
        if (period.getLectureRoomVal() != null)
            _lectureRoomVal = new DataWrapper(period.getLectureRoomVal().getId(), period.getLectureRoomVal().getTitleWithLocation());
        else
            _lectureRoomVal = null;
        _lectureRoomType = null;
    }

    public SppSchedulePeriod getPeriodForSave()
    {
        _period.setWeekRow(_weekPeriodsRowVO.getWeekRow());
        _period.setStartDate(getStartDate());
        _period.setEndDate(getEndDate());
        _period.setEmpty(isEmpty());
        if (isEmpty())
        {
            _period.setLectureRoom(null);
            _period.setTeacher(null);
            _period.setSubject(null);
            _period.setPeriodGroup(null);
            _period.setSubjectVal(null);
            _period.setSubjectLoadType(null);
            _period.setTeacherVal(null);
            _period.setLectureRoomVal(null);
            setLectureRoom(null);
            setTeacher(null);
            setSubject(null);
            setPeriodGroup(null);
            setLectureRoomVal(null);
            setSubjectVal(null);
        } else
        {
            _period.setLectureRoom(getLectureRoom());
            _period.setTeacher(getTeacher());
            _period.setSubject(getSubject());
            _period.setPeriodGroup(getPeriodGroup());
            if (null != getSubjectVal())
            {
                _period.setSubjectVal(getSubjectVal().getDiscipline());
                _period.setSubjectLoadType(getSubjectVal().getEppALoadType());
            } else
            {
                _period.setSubjectVal(null);
                _period.setSubjectLoadType(null);
            }

            _period.setTeacherVal(getTeacherVal());
            if (getLectureRoomVal() != null)
                _period.setLectureRoomVal(DataAccessServices.dao().getNotNull(getLectureRoomVal().getId()));
            else
                _period.setLectureRoomVal(null);
        }
        return _period;
    }

    public String getPeriodDates()
    {
        StringBuilder sb = new StringBuilder();
        if (getStartDate() != null || getEndDate() != null)
        {
            sb.append(" (");
            sb.append(getStartDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(getStartDate()) : "...").append(" - ");
            sb.append(getEndDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndDate()) : "...");
            sb.append(")");
        }
        return sb.toString();
    }

    // getters & setters
    public SppSchedulePeriod getPeriod()
    {
        return _period;
    }

    public void setPeriod(SppSchedulePeriod period)
    {
        _period = period;
    }

    public Date getStartDate()
    {
        return _startDate;
    }

    public void setStartDate(Date startDate)
    {
        _startDate = startDate;
    }

    public Date getEndDate()
    {
        return _endDate;
    }

    public void setEndDate(Date endDate)
    {
        _endDate = endDate;
    }

    public String getLectureRoom()
    {
        return _lectureRoom;
    }

    public void setLectureRoom(String lectureRoom)
    {
        _lectureRoom = lectureRoom;
    }

    public String getTeacher()
    {
        return _teacher;
    }

    public void setTeacher(String teacher)
    {
        _teacher = teacher;
    }

    public String getSubject()
    {
        return _subject;
    }

    public void setSubject(String subject)
    {
        _subject = subject;
    }

    public String getPeriodGroup()
    {
        return _periodGroup;
    }

    public void setPeriodGroup(String periodGroup)
    {
        _periodGroup = periodGroup;
    }

    public boolean isEmpty()
    {
        return _empty;
    }

    public void setEmpty(boolean empty)
    {
        _empty = empty;
    }

    public SppScheduleWeekPeriodsRowVO getWeekPeriodsRowVO()
    {
        return _weekPeriodsRowVO;
    }

    public void setWeekPeriodsRowVO(SppScheduleWeekPeriodsRowVO weekPeriodsRowVO)
    {
        _weekPeriodsRowVO = weekPeriodsRowVO;
    }

    public SppScheduleDisciplineVO getSubjectVal()
    {
        return _subjectVal;
    }

    public void setSubjectVal(SppScheduleDisciplineVO subjectVal)
    {
        _subjectVal = subjectVal;
    }

    public PpsEntry getTeacherVal()
    {
        return _teacherVal;
    }

    public void setTeacherVal(PpsEntry teacherVal)
    {
        _teacherVal = teacherVal;
    }

    public DataWrapper getLectureRoomVal()
    {
        return _lectureRoomVal;
    }

    public void setLectureRoomVal(DataWrapper lectureRoomVal)
    {
        _lectureRoomVal = lectureRoomVal;
    }

    public UniplacesClassroomType getLectureRoomType()
    {
        return _lectureRoomType;
    }

    public void setLectureRoomType(UniplacesClassroomType lectureRoomType)
    {
        _lectureRoomType = lectureRoomType;
    }
}
