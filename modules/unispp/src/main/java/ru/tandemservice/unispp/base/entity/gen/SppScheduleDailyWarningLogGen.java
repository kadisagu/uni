package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyWarningLog;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Журнал ошибок и предупреждений подневного расписания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppScheduleDailyWarningLogGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppScheduleDailyWarningLog";
    public static final String ENTITY_NAME = "sppScheduleDailyWarningLog";
    public static final int VERSION_HASH = -1280017022;
    private static IEntityMeta ENTITY_META;

    public static final String L_SPP_SCHEDULE_DAILY = "sppScheduleDaily";
    public static final String P_WARNING_MESSAGE = "warningMessage";

    private SppScheduleDaily _sppScheduleDaily;     // Подневное расписание
    private String _warningMessage;     // Сообщение о предупреждении

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подневное расписание. Свойство не может быть null.
     */
    @NotNull
    public SppScheduleDaily getSppScheduleDaily()
    {
        return _sppScheduleDaily;
    }

    /**
     * @param sppScheduleDaily Подневное расписание. Свойство не может быть null.
     */
    public void setSppScheduleDaily(SppScheduleDaily sppScheduleDaily)
    {
        dirty(_sppScheduleDaily, sppScheduleDaily);
        _sppScheduleDaily = sppScheduleDaily;
    }

    /**
     * @return Сообщение о предупреждении. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1024)
    public String getWarningMessage()
    {
        return _warningMessage;
    }

    /**
     * @param warningMessage Сообщение о предупреждении. Свойство не может быть null.
     */
    public void setWarningMessage(String warningMessage)
    {
        dirty(_warningMessage, warningMessage);
        _warningMessage = warningMessage;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppScheduleDailyWarningLogGen)
        {
            setSppScheduleDaily(((SppScheduleDailyWarningLog)another).getSppScheduleDaily());
            setWarningMessage(((SppScheduleDailyWarningLog)another).getWarningMessage());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppScheduleDailyWarningLogGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppScheduleDailyWarningLog.class;
        }

        public T newInstance()
        {
            return (T) new SppScheduleDailyWarningLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sppScheduleDaily":
                    return obj.getSppScheduleDaily();
                case "warningMessage":
                    return obj.getWarningMessage();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sppScheduleDaily":
                    obj.setSppScheduleDaily((SppScheduleDaily) value);
                    return;
                case "warningMessage":
                    obj.setWarningMessage((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sppScheduleDaily":
                        return true;
                case "warningMessage":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sppScheduleDaily":
                    return true;
                case "warningMessage":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sppScheduleDaily":
                    return SppScheduleDaily.class;
                case "warningMessage":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppScheduleDailyWarningLog> _dslPath = new Path<SppScheduleDailyWarningLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppScheduleDailyWarningLog");
    }
            

    /**
     * @return Подневное расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyWarningLog#getSppScheduleDaily()
     */
    public static SppScheduleDaily.Path<SppScheduleDaily> sppScheduleDaily()
    {
        return _dslPath.sppScheduleDaily();
    }

    /**
     * @return Сообщение о предупреждении. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyWarningLog#getWarningMessage()
     */
    public static PropertyPath<String> warningMessage()
    {
        return _dslPath.warningMessage();
    }

    public static class Path<E extends SppScheduleDailyWarningLog> extends EntityPath<E>
    {
        private SppScheduleDaily.Path<SppScheduleDaily> _sppScheduleDaily;
        private PropertyPath<String> _warningMessage;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подневное расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyWarningLog#getSppScheduleDaily()
     */
        public SppScheduleDaily.Path<SppScheduleDaily> sppScheduleDaily()
        {
            if(_sppScheduleDaily == null )
                _sppScheduleDaily = new SppScheduleDaily.Path<SppScheduleDaily>(L_SPP_SCHEDULE_DAILY, this);
            return _sppScheduleDaily;
        }

    /**
     * @return Сообщение о предупреждении. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleDailyWarningLog#getWarningMessage()
     */
        public PropertyPath<String> warningMessage()
        {
            if(_warningMessage == null )
                _warningMessage = new PropertyPath<String>(SppScheduleDailyWarningLogGen.P_WARNING_MESSAGE, this);
            return _warningMessage;
        }

        public Class getEntityClass()
        {
            return SppScheduleDailyWarningLog.class;
        }

        public String getEntityName()
        {
            return "sppScheduleDailyWarningLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
