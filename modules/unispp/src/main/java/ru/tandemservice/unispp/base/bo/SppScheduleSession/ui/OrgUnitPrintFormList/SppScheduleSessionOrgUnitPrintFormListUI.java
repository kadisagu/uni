/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.OrgUnitPrintFormList;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionPrintFormDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.PrintFormAdd.SppScheduleSessionPrintFormAdd;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionPrintForm;

/**
 * @author vnekrasov
 * @since 12/27/13
 */
public class SppScheduleSessionOrgUnitPrintFormListUI extends OrgUnitUIPresenter
{
    private BaseSearchListDataSource _printFormDS;

    public BaseSearchListDataSource getPrintFormDS()
    {
        if (null == _printFormDS)
            _printFormDS = _uiConfig.getDataSource(SppScheduleSessionOrgUnitPrintFormList.PRINT_FORM_DS);
        return _printFormDS;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleSessionOrgUnitPrintFormList.PRINT_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.put("orgUnitId", getOrgUnit().getId());
            dataSource.put(SppScheduleSessionPrintFormDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
            dataSource.putAll(_uiSettings.getAsMap(true, "groups"));
        }

        if (SppScheduleSessionOrgUnitPrintFormList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put("orgUnitId", getOrgUnit().getId());
        }
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickDownload()
    {
        SppScheduleSessionPrintForm printForm = getPrintFormDS().getRecordById(getListenerParameterAsLong());
        byte[] content = printForm.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("xls").fileName("schedule.xls").document(content), true);
    }

    public void onClickAddSchedulePrint()
    {
        _uiActivation.asRegion(SppScheduleSessionPrintFormAdd.class).parameter("orgUnitId", getOrgUnit().getId()).activate();
    }

}
