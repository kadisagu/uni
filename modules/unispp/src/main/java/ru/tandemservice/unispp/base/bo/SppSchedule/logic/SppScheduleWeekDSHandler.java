/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import ru.tandemservice.unispp.base.vo.SppScheduleWeekPeriodsRowVO;

import java.util.List;

/**
 * @author nvankov
 * @since 11/26/14
 */
public class SppScheduleWeekDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String WEEK_PERIODS = "weekPeriods";

    public SppScheduleWeekDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        List<SppScheduleWeekPeriodsRowVO> weekPeriodsRows = context.getNotNull(WEEK_PERIODS);

        return ListOutputBuilder.get(input, weekPeriodsRows).pageable(false).build();
    }
}
