/* $Id$ */
package ru.tandemservice.unispp.util;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Alexey Lopatin
 * @since 07.11.2014
 */
public class SppScheduleUtil
{
    /**
     * @return Выводит день недели от 1 до 7, начиная с понедельника
     */
    public static int getDayOfWeek(Date date)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int result = c.get(Calendar.DAY_OF_WEEK);
        return result > 1 ? result - 1 : 7;
    }

    /**
     * @return разница в неделях между датами
     * (Если даты в одной неделе, то 0. Если в соседних то 1 (-1) и т.д.)
     * (для положительного результата, первая дата должна идти перед второй (date1 < date2 => result >= 0 ))
     */
    public static int getDifferenceInWeeks(Date date1, Date date2)
    {
        LocalDate localDate1 = new LocalDate(date1);
        LocalDate localDate2 = new LocalDate(date2);
        return (Days.daysBetween(localDate1.withDayOfWeek(1), localDate2.withDayOfWeek(1)).getDays()) / 7;
    }
}
