package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unispp_2x7x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sppSchedule

		// создано свойство icalTeacherData
		if(tool.tableExists("sppschedule_t") && !tool.columnExists("sppschedule_t","icalteacherdata_id"))
		{
			// создать колонку
			tool.createColumn("sppschedule_t", new DBColumn("icalteacherdata_id", DBType.LONG));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleSession

		// создано свойство icalTeacherData
		if(tool.tableExists("sppschedulesession_t") && !tool.columnExists("sppschedulesession_t", "icalteacherdata_id"))
		{
			// создать колонку
			tool.createColumn("sppschedulesession_t", new DBColumn("icalteacherdata_id", DBType.LONG));
		}
    }
}