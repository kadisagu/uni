/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.View;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.EntityRenumerator;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.*;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddEdit.SppScheduleAddEdit;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddEditPeriodFix.SppScheduleAddEditPeriodFix;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.Copy.SppScheduleCopy;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppScheduleICal;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;
import ru.tandemservice.unispp.base.entity.catalog.codes.SppScheduleStatusCodes;
import ru.tandemservice.unispp.base.vo.*;

import java.util.*;

/**
 * @author nvankov
 * @since 9/4/13
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "scheduleId", required = true),
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class SppScheduleViewUI extends UIPresenter
{
    private Long _scheduleId;
    private Long _orgUnitId;
    private SppSchedule _schedule;
    private List<SppScheduleWeekPeriodsRowVO> _oddWeekPeriods = new ArrayList<>();
    private List<SppScheduleWeekPeriodsRowVO> _evenWeekPeriods = new ArrayList<>();
    private BaseSearchListDataSource _oddWeekDS;
    private BaseSearchListDataSource _evenWeekDS;
    private BaseSearchListDataSource _periodFixDS;
    private BaseSearchListDataSource _warningLogDS;

    private int _studentGrpCnt;

    private boolean _checkFree;
    private boolean _checkLectureRoomCapacity;
    private boolean _checkTeacherPreferences;
    private boolean _checkDuplicates;

    public boolean isCheckFree()
    {
        return _checkFree;
    }

    public void setCheckFree(boolean checkFree)
    {
        _checkFree = checkFree;
    }

    public boolean isCheckLectureRoomCapacity()
    {
        return _checkLectureRoomCapacity;
    }

    public void setCheckLectureRoomCapacity(boolean checkLectureRoomCapacity)
    {
        _checkLectureRoomCapacity = checkLectureRoomCapacity;
    }

    public boolean isCheckTeacherPreferences()
    {
        return _checkTeacherPreferences;
    }

    public void setCheckTeacherPreferences(boolean checkTeacherPreferences)
    {
        _checkTeacherPreferences = checkTeacherPreferences;
    }

    public boolean isCheckDuplicates()
    {
        return _checkDuplicates;
    }

    public void setCheckDuplicates(boolean checkDuplicates)
    {
        _checkDuplicates = checkDuplicates;
    }

    public BaseSearchListDataSource getOddWeekDS()
    {
        if (_oddWeekDS == null)
            _oddWeekDS = getConfig().getDataSource(SppScheduleView.ODD_WEEK_DS);
        return _oddWeekDS;
    }

    public BaseSearchListDataSource getEvenWeekDS()
    {
        if (_evenWeekDS == null)
            _evenWeekDS = getConfig().getDataSource(SppScheduleView.EVEN_WEEK_DS);
        return _evenWeekDS;
    }

    public BaseSearchListDataSource getPeriodFixDS()
    {
        if (_periodFixDS == null)
            _periodFixDS = getConfig().getDataSource(SppScheduleView.PERIOD_FIX_DS);
        return _periodFixDS;
    }

    public BaseSearchListDataSource getWarningLogDS()
    {
        if (_warningLogDS == null)
            _warningLogDS = getConfig().getDataSource(SppScheduleView.WARNING_LOG_DS);
        return _warningLogDS;
    }

    public Long getScheduleId()
    {
        return _scheduleId;
    }

    public void setScheduleId(Long scheduleId)
    {
        _scheduleId = scheduleId;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public SppSchedule getSchedule()
    {
        return _schedule;
    }

    public void setSchedule(SppSchedule schedule)
    {
        _schedule = schedule;
    }

    //На формировании
    public Boolean getScheduleOnFormation()
    {
        return SppScheduleStatusCodes.FORMATION.equals(_schedule.getStatus().getCode());
    }

    //Утверждено
    public Boolean getScheduleApproved()
    {
        return SppScheduleStatusCodes.APPROVED.equals(_schedule.getStatus().getCode());
    }


    // В архиве
    public Boolean getScheduleArchived()
    {
        return _schedule.isArchived();
    }

    //    visible="ui:editScheduleVisible"
    public Boolean getEditScheduleVisible()
    {
        return getScheduleOnFormation() && !getScheduleArchived();
    }

    //    visible="ui:addPeriodFixVisible"
    public Boolean getAddPeriodFixVisible()
    {
        return !getScheduleArchived();
    }

    //    visible="ui:sentToArchiveVisible"
    public Boolean getSentToArchiveVisible()
    {
        return !getScheduleArchived();
    }

    //    visible="ui:restoreFromArchiveVisible"
    public Boolean getRestoreFromArchiveVisible()
    {
        return getScheduleArchived() && !_schedule.getGroup().isArchival();
    }

    //    visible="ui:approveVisible"
    public Boolean getApproveVisible()
    {
        return getScheduleOnFormation() && !getScheduleArchived();
    }

    //    visible="ui:sentToFormationVisible"
    public Boolean getSentToFormationVisible()
    {
        return getScheduleApproved() && !getScheduleArchived();
    }

    //    ui:periodFixDeleteDisabled
    public Boolean getPeriodFixDeleteDisabled()
    {
        return getScheduleArchived();
    }

    //    ui:periodFixEditDisabled
    public Boolean getPeriodFixEditDisabled()
    {
        return getScheduleArchived();
    }

    //    ui:deleteActive - ui:currentOddRowItemPeriodDeleteActive
    public Boolean getCurrentOddRowItemPeriodDeleteActive()
    {
        return !getScheduleArchived() && !getScheduleApproved();
    }

    //    ui:deleteActive - ui:currentEvenRowItemPeriodDeleteActive
    public Boolean getCurrentEvenRowItemPeriodDeleteActive()
    {
        return !getScheduleArchived() && !getScheduleApproved();
    }

    //ui:currentOddRowItem.addActive - currentOddRowItemAddPeriod
    public Boolean getCurrentOddRowItemAddPeriod()
    {
        return !(getScheduleArchived() || getScheduleApproved()) && getCurrentOddRowItem().getAddActive();
    }

    //    ui:currentOddRowItem.editActive - currentOddRowItemEditPeriod
    public Boolean getCurrentOddRowItemEditPeriod()
    {
        return !(getScheduleArchived() || getScheduleApproved()) && getCurrentOddRowItem().getEditActive();
    }

    //    ui:currentOddRowItem.saveActive - currentOddRowItemSavePeriod
    public Boolean getCurrentOddRowItemSavePeriod()
    {
        return !(getScheduleArchived() || getScheduleApproved()) && getCurrentOddRowItem().getSaveActive();
    }

    //    ui:currentEvenRowItem.addActive - currentEvenRowItemAddPeriod
    public Boolean getCurrentEvenRowItemAddPeriod()
    {
        return !(getScheduleArchived() || getScheduleApproved()) && getCurrentEvenRowItem().getAddActive();
    }

    //    ui:currentEvenRowItem.editActive - currentEvenRowItemEditPeriod
    public Boolean getCurrentEvenRowItemEditPeriod()
    {
        return !(getScheduleArchived() || getScheduleApproved()) && getCurrentEvenRowItem().getEditActive();
    }

    //    ui:currentEvenRowItem.saveActive - currentEvenRowItemSavePeriod
    public Boolean getCurrentEvenRowItemSavePeriod()
    {
        return !(getScheduleArchived() || getScheduleApproved()) && getCurrentEvenRowItem().getSaveActive();
    }

    public Map getGroupParameterMap()
    {
        return new ParametersMap()
                .add(UIPresenter.PUBLISHER_ID, _schedule.getGroup().getId())
                .add("selectedTabId", "sppScheduleTab");
    }

    @Override
    public void onComponentRefresh()
    {
        _schedule = DataAccessServices.dao().getNotNull(_scheduleId);
        SppScheduleVO scheduleVO = SppScheduleManager.instance().dao().prepareScheduleVO(_schedule);
        _oddWeekPeriods = scheduleVO.getOddWeek().getWeekPeriods();
        _evenWeekPeriods = scheduleVO.getEvenWeek().getWeekPeriods();
        _studentGrpCnt = SppScheduleManager.instance().dao().getGroupStudentCount(_schedule.getGroup().getId());

        setCheckFree(true);
        setCheckLectureRoomCapacity(true);
        setCheckTeacherPreferences(true);
        setCheckDuplicates(true);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleView.ODD_WEEK_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleWeekDSHandler.WEEK_PERIODS, _oddWeekPeriods);
        }
        if (SppScheduleView.EVEN_WEEK_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleWeekDSHandler.WEEK_PERIODS, _evenWeekPeriods);
        }
        if (SppScheduleView.WARNING_LOG_DS.equals(dataSource.getName()))
        {
            dataSource.put("scheduleId", _schedule.getId());
        }
        if (SppScheduleView.PERIOD_FIX_DS.equals(dataSource.getName()))
        {
            dataSource.put("scheduleId", _schedule.getId());
        }
        if (SppScheduleView.SUBJECT_DS.equals(dataSource.getName()))
        {
            SppSchedulePeriodsVO curOddPeriods = null;
            SppSchedulePeriodsVO curEvenPeriods = null;

            if (null != getOddWeekDS().getCurrentColumn()) curOddPeriods = getCurrentOddRowItem();
            if (null != getEvenWeekDS().getCurrentColumn()) curEvenPeriods = getCurrentEvenRowItem();

            SppSchedulePeriodVO period = null;
            if (curOddPeriods != null && curOddPeriods.getCurrentPeriod() != null)
                period = curOddPeriods.getCurrentPeriod();
            else if (curEvenPeriods != null && curEvenPeriods.getCurrentPeriod() != null)
                period = curEvenPeriods.getCurrentPeriod();

            dataSource.put(SppScheduleSubjectComboDSHandler.PROP_GROUP_IDS, Collections.singletonList(_schedule.getGroup().getId()));
            dataSource.put(SppScheduleSubjectComboDSHandler.PROP_SEASON_ID, _schedule.getSeason().getId());

            if (period != null)
            {
                dataSource.put(SppScheduleSubjectComboDSHandler.PROP_CHECK_DUPLICATES, _checkDuplicates);
                dataSource.put(SppScheduleSubjectComboDSHandler.PROP_SCHEDULE_ID, _scheduleId);
                dataSource.put(SppScheduleSubjectComboDSHandler.PROP_CURRENT_PERIOD_ID, period.getPeriod().getId());
                dataSource.put(SppScheduleSubjectComboDSHandler.PROP_DAY_OF_WEEK, period.getPeriod().getDayOfWeek());
            }
        }
        if (SppScheduleView.TEACHER_DS.equals(dataSource.getName()))
        {
            SppSchedulePeriodsVO curOddPeriods = null;
            SppSchedulePeriodsVO curEvenPeriods = null;

            if (null != getOddWeekDS().getCurrentColumn()) curOddPeriods = getCurrentOddRowItem();
            if (null != getEvenWeekDS().getCurrentColumn()) curEvenPeriods = getCurrentEvenRowItem();

            SppSchedulePeriodVO period = null;
            if (curOddPeriods != null && curOddPeriods.getCurrentPeriod() != null)
                period = curOddPeriods.getCurrentPeriod();
            else if (curEvenPeriods != null && curEvenPeriods.getCurrentPeriod() != null)
                period = curEvenPeriods.getCurrentPeriod();

            if (null != period)
            {
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_CURRENT_PERIOD, period.getPeriod());
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_SEASON_ID, _schedule.getSeason().getId());
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_EVENT_BELL_ENTRY_ID, period.getWeekPeriodsRowVO().getTime().getId());
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_EVENT_DAY_OF_WEEK, period.getPeriod().getDayOfWeek());
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_CHECK_FREE_TEACHERS, _checkFree);
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_CHECK_TEACHER_PREFERENCES, _checkTeacherPreferences);
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_SCHEDULE_IDS, Collections.singletonList(getScheduleId()));
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_IS_EVEN, period.getWeekPeriodsRowVO().getWeekRow().isEven());

                if (period.getSubjectVal() != null && period.getSubjectVal().getDiscipline() != null)
                    dataSource.put(SppScheduleTeacherComboDSHandler.PROP_ORG_UNIT_ID, period.getSubjectVal().getDiscipline().getOwner().getId());
                if (period.getLectureRoomVal() != null)
                    dataSource.put(SppScheduleTeacherComboDSHandler.PROP_LECTURE_ROOM_ID, period.getLectureRoomVal().getId());
                if (period.getStartDate() != null)
                    dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_START_DATE, period.getStartDate());
                if (period.getEndDate() != null)
                    dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_END_DATE, period.getEndDate());
            }
        }
        if (SppScheduleView.LECTURE_ROOM_DS.equals(dataSource.getName()))
        {
            SppSchedulePeriodsVO curOddPeriods = null;
            SppSchedulePeriodsVO curEvenPeriods = null;

            if (null != getOddWeekDS().getCurrentColumn()) curOddPeriods = getCurrentOddRowItem();
            if (null != getEvenWeekDS().getCurrentColumn()) curEvenPeriods = getCurrentEvenRowItem();

            SppSchedulePeriodVO period = null;
            if (curOddPeriods != null && curOddPeriods.getCurrentPeriod() != null)
                period = curOddPeriods.getCurrentPeriod();
            else if (curEvenPeriods != null && curEvenPeriods.getCurrentPeriod() != null)
                period = curEvenPeriods.getCurrentPeriod();

            if (null != period)
            {
                SppScheduleDisciplineVO subjectVal = period.getSubjectVal();

                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_CURRENT_PERIOD, period.getPeriod());
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_EVENT_DAY_OF_WEEK, period.getPeriod().getDayOfWeek());
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_EVENT_BELL_ENTRY_ID, period.getWeekPeriodsRowVO().getTime().getId());
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_SEASON_ID, _schedule.getSeason().getId());
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_CHECK_FREE_LECTURE_ROOMS, _checkFree);
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_CHECK_TEACHER_PREFERENCES, _checkTeacherPreferences);
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_SCHEDULE_IDS, Collections.singletonList(getScheduleId()));
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_IS_EVEN, period.getWeekPeriodsRowVO().getWeekRow().isEven());

                if (_checkLectureRoomCapacity)
                    dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_STUDENT_GRP_CNT, _studentGrpCnt);
                if (period.getLectureRoomType() != null)
                    dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_LECTURE_ROOM_TYPE, period.getLectureRoomType());
                if (period.getTeacherVal() != null)
                    dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_TEACHER_ID, period.getTeacherVal().getId());
                if (null != subjectVal && null != subjectVal.getDiscipline())
                    dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_DISCIPLINE_ID, subjectVal.getDiscipline().getId());
                if (period.getStartDate() != null)
                    dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_START_DATE, period.getStartDate());
                if (period.getEndDate() != null)
                    dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_END_DATE, period.getEndDate());
            }
        }
        if (SppScheduleView.LOAD_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleLoadDSHandler.PROP_SCHEDULE_ID, _scheduleId);
        }
    }

    public SppSchedulePeriodsVO getCurrentOddRowItem()
    {
        SppScheduleWeekPeriodsRowVO rowVO = getOddWeekDS().getCurrent();
        int day = getOddWeekDS().getCurrentColumn().getNumber();

        return rowVO.getDay(day);
    }

    public SppSchedulePeriodsVO getCurrentEvenRowItem()
    {
        SppScheduleWeekPeriodsRowVO rowVO = getEvenWeekDS().getCurrent();
        int day = getEvenWeekDS().getCurrentColumn().getNumber();

        return rowVO.getDay(day);
    }

    public Map<String, Object> getCurrentOddRowItemParameters()
    {
        Integer day = getOddWeekDS().getCurrentColumn().getNumber();
        SppScheduleWeekPeriodsRowVO rowVO = getOddWeekDS().getCurrent();
        return ParametersMap.createWith("day", day).add("weekRowId", rowVO.getId());
    }


    public Map<String, Object> getCurrentEvenRowItemParameters()
    {
        Integer day = getEvenWeekDS().getCurrentColumn().getNumber();
        SppScheduleWeekPeriodsRowVO rowVO = getEvenWeekDS().getCurrent();
        return ParametersMap.createWith("day", day).add("weekRowId", rowVO.getId());
    }

    public Map<String, Object> getCurrentOddRowItemPeriodParameters()
    {
        Integer day = getOddWeekDS().getCurrentColumn().getNumber();
        SppScheduleWeekPeriodsRowVO rowVO = getOddWeekDS().getCurrent();
        SppSchedulePeriodsVO periodVO = rowVO.getDay(day);
        Integer periodIndex = periodVO.getPeriods().indexOf(periodVO.getCurrentPeriod());
        return ParametersMap.createWith("day", day).add("weekRowId", rowVO.getId()).add("periodIndex", periodIndex);
    }

    public Map<String, Object> getCurrentEvenRowItemPeriodParameters()
    {
        Integer day = getEvenWeekDS().getCurrentColumn().getNumber();
        SppScheduleWeekPeriodsRowVO rowVO = getEvenWeekDS().getCurrent();
        SppSchedulePeriodsVO periodVO = rowVO.getDay(day);
        Integer periodIndex = periodVO.getPeriods().indexOf(periodVO.getCurrentPeriod());
        return ParametersMap.createWith("day", day).add("weekRowId", rowVO.getId()).add("periodIndex", periodIndex);
    }

    public String getCurrentOddId()
    {
        Integer day = getOddWeekDS().getCurrentColumn().getNumber();
        SppScheduleWeekPeriodsRowVO rowVO = getOddWeekDS().getCurrent();
        SppSchedulePeriodsVO periodVO = rowVO.getDay(day);
        Integer periodIndex = periodVO.getPeriods().indexOf(periodVO.getCurrentPeriod());

        return day + "_" + rowVO.getTime().getNumber() + "_" + periodIndex;
    }

    public String getCurrentEvenId()
    {
        Integer day = getEvenWeekDS().getCurrentColumn().getNumber();
        SppScheduleWeekPeriodsRowVO rowVO = getEvenWeekDS().getCurrent();
        SppSchedulePeriodsVO periodVO = rowVO.getDay(day);
        Integer periodIndex = periodVO.getPeriods().indexOf(periodVO.getCurrentPeriod());

        return day + "_" + rowVO.getTime().getNumber() + "_" + periodIndex;
    }

    public String getCurrentOddRowItemGroupEvenId()
    {
        return "groupEvenId_" + getCurrentOddId();
    }

    public String getCurrentOddRowItemLectureRoomId()
    {
        return "lectureRoomId_" + getCurrentOddId();
    }

    public String getCurrentOddRowItemSubjectId()
    {
        return "subjectId_" + getCurrentOddId();
    }

    public String getCurrentOddRowItemTeacherId()
    {
        return "teacherId_" + getCurrentOddId();
    }

    public String getCurrentEvenRowItemGroupEvenId()
    {
        return "groupEvenId_" + getCurrentEvenId();
    }

    public String getCurrentEvenRowItemLectureRoomId()
    {
        return "lectureRoomId_" + getCurrentEvenId();
    }


    public String getCurrentEvenRowItemSubjectId()
    {
        return "subjectId_" + getCurrentEvenId();
    }

    public String getCurrentEvenRowItemTeacherId()
    {
        return "teacherId_" + getCurrentEvenId();
    }

    public String getCurrentOddRowItemTeacherSelectId()
    {
        return "teacherSelectId_" + getCurrentOddId();
    }

    public String getCurrentOddRowItemLectureRoomSelectId()
    {
        return "lectureRoomSelectId_" + getCurrentOddId();
    }

    public String getCurrentOddRowItemLectureRoomTypeSelectId()
    {
        return "lectureRoomTypeSelectId_" + getCurrentOddId();
    }

    public String getCurrentEvenRowItemLectureRoomTypeSelectId()
    {
        return "lectureRoomTypeSelectId_" + getCurrentEvenId();
    }

    public String getCurrentOddRowItemSubjectSelectId()
    {
        return "subjectSelectId_" + getCurrentOddId();
    }

    public String getCurrentEvenRowItemTeacherSelectId()
    {
        return "teacherSelectId_" + getCurrentEvenId();
    }

    public String getCurrentEvenRowItemLectureRoomSelectId()
    {
        return "lectureRoomSelectId_" + getCurrentEvenId();
    }

    public String getCurrentEvenRowItemSubjectSelectId()
    {
        return "subjectSelectId_" + getCurrentEvenId();
    }

    public void onClickEditSchedule()
    {
        _uiActivation.asRegionDialog(SppScheduleAddEdit.class)
                .parameter("scheduleId", _scheduleId)
                .parameter("orgUnitId", _orgUnitId)
                .activate();
    }

    public void onClickCopySchedule()
    {
        _uiActivation.asRegion(SppScheduleCopy.class).parameter("scheduleId", _scheduleId).activate();
    }

    public void onClickAddPeriodFix()
    {
        _uiActivation.asRegion(SppScheduleAddEditPeriodFix.class).parameter("scheduleId", _scheduleId).activate();
    }

    public void onClickSentToArchive()
    {
        SppScheduleManager.instance().dao().sentToArchive(_schedule.getId());
    }

    public void onClickRestoreFromArchive()
    {
        SppScheduleManager.instance().dao().restoreFromArchive(_schedule.getId());
    }

    public void onClickPerformValidation()
    {
        _uiSupport.setRefreshScheduled(true);
        SppScheduleManager.instance().dao().validate(_schedule);
    }

    public void onClickApprove()
    {
        SppScheduleManager.instance().dao().approve(_schedule.getId());
    }

    public void onClickSentToFormation()
    {
        SppScheduleManager.instance().dao().sentToFormation(_schedule.getId());
    }

    public void onClickEditPeriodFix()
    {
        _uiActivation.asRegion(SppScheduleAddEditPeriodFix.class)
                .parameter("scheduleId", _scheduleId)
                .parameter("periodFixId", getListenerParameterAsLong()).activate();
    }

    public void onClickDeletePeriodFix()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        updateScheduleAsChanged();
    }

    // odd listeners
    public void onAddOddPair()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");

        SppScheduleWeekPeriodsRowVO rowVO = getOddWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodsVO = rowVO.getDay(day);

        periodsVO.setView(false);
        periodsVO.getPeriods().add(getNewPeriodVO(rowVO, periodsVO, day));

        new EntityRenumerator<SppSchedulePeriodVO>()
        {
            @Override
            protected int getNumber(final SppSchedulePeriodVO e)
            {
                return e.getPeriod().getPeriodNum();
            }

            @Override
            protected void setNumber(final SppSchedulePeriodVO e, final int number)
            {
                e.getPeriod().setPeriodNum(number);
            }
        }.execute(periodsVO.getPeriods());
    }

    public void onChangeTeacherOdd()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getOddWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodsVO = rowVO.getDay(day);
        Integer periodIndex = (Integer) parametersMap.get("periodIndex");
        SppSchedulePeriodVO periodVO = periodsVO.getPeriods().get(periodIndex);
        if (null != periodVO.getTeacherVal())
            periodVO.setTeacher(periodVO.getTeacherVal().getFio());
    }

    public void onChangeSubjectOdd()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getOddWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodsVO = rowVO.getDay(day);
        Integer periodIndex = (Integer) parametersMap.get("periodIndex");
        SppSchedulePeriodVO periodVO = periodsVO.getPeriods().get(periodIndex);
        if (null != periodVO.getSubjectVal())
        {
            StringBuilder sb = new StringBuilder();
            final String title = periodVO.getSubjectVal().getTitle();
            if (title != null)
            {
                sb.append(title.substring(0, title.length() - 2));
                sb.append(periodVO.getPeriodDates());
            }

            periodVO.setSubject(sb.toString());
            periodVO.setLectureRoomType(SppScheduleManager.instance().dao().getDisciplineLectureRoomType(periodVO.getSubjectVal().getDiscipline(), periodVO.getSubjectVal().getEppALoadType()));
        }
    }

    public void onChangeLectureRoomOdd()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getOddWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodsVO = rowVO.getDay(day);
        Integer periodIndex = (Integer) parametersMap.get("periodIndex");
        SppSchedulePeriodVO periodVO = periodsVO.getPeriods().get(periodIndex);
        if (null != periodVO.getLectureRoomVal())
        {
            UniplacesPlace lectureRoom = DataAccessServices.dao().getNotNull(periodVO.getLectureRoomVal().getId());
            periodVO.setLectureRoom(lectureRoom.getTitle());
        } else periodVO.setLectureRoom(null);
    }

    public void onChangeTeacherEven()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getEvenWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodsVO = rowVO.getDay(day);
        Integer periodIndex = (Integer) parametersMap.get("periodIndex");
        SppSchedulePeriodVO periodVO = periodsVO.getPeriods().get(periodIndex);
        if (null != periodVO.getTeacherVal())
            periodVO.setTeacher(periodVO.getTeacherVal().getFio());
    }

    public void onChangeSubjectEven()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getEvenWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodsVO = rowVO.getDay(day);
        Integer periodIndex = (Integer) parametersMap.get("periodIndex");
        SppSchedulePeriodVO periodVO = periodsVO.getPeriods().get(periodIndex);
        if (null != periodVO.getSubjectVal())
        {
            StringBuilder sb = new StringBuilder();
            final String title = periodVO.getSubjectVal().getTitle();
            if (title != null)
            {
                sb.append(title.substring(0, title.length() - 2));
                sb.append(periodVO.getPeriodDates());
            }

            periodVO.setSubject(sb.toString());
            periodVO.setLectureRoomType(SppScheduleManager.instance().dao().getDisciplineLectureRoomType(periodVO.getSubjectVal().getDiscipline(), periodVO.getSubjectVal().getEppALoadType()));
        }
    }

    public void onChangeLectureRoomEven()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getEvenWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodsVO = rowVO.getDay(day);
        Integer periodIndex = (Integer) parametersMap.get("periodIndex");
        SppSchedulePeriodVO periodVO = periodsVO.getPeriods().get(periodIndex);
        if (null != periodVO.getLectureRoomVal())
        {
            UniplacesPlace lectureRoom = DataAccessServices.dao().getNotNull(periodVO.getLectureRoomVal().getId());
            periodVO.setLectureRoom(lectureRoom.getTitle());
        } else periodVO.setLectureRoom(null);
    }

    public void onEditOddPair()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getOddWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodVO = rowVO.getDay(day);
        periodVO.setView(false);
    }

    public void onSaveOddPair()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getOddWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodsVO = rowVO.getDay(day);
        errorCheck(day, rowVO, periodsVO);
        if (getUserContext().getErrorCollector().hasErrors())
            return;
        periodsVO.setView(true);
        SppScheduleManager.instance().dao().saveOrUpdatePeriods(periodsVO.getPeriods());
        updateScheduleAsChanged();
    }

    private void errorCheck(Integer day, SppScheduleWeekPeriodsRowVO rowVO, SppSchedulePeriodsVO periodsVO)
    {
        for (SppSchedulePeriodVO periodVO : periodsVO.getPeriods())
        {
            Date startDate = periodVO.getStartDate();
            Date endDate = periodVO.getEndDate();

            if (startDate != null && endDate != null && startDate.after(endDate))
                throw new ApplicationException("Дата начала периода проведения идёт после даты его окончания");

            // нужно проверить не выходит ли период проведения пары за пределы периода расписания
            boolean beyondLeft = startDate != null && startDate.before(getSchedule().getSeason().getStartDate());
            boolean beyondRight = endDate != null && endDate.after(getSchedule().getSeason().getEndDate());
            if (beyondLeft || beyondRight)
                throw new ApplicationException("Период проведения пары выходит за пределы периода расписания");

            if (!periodVO.isEmpty())
            {
                String itemId = day + "_" + rowVO.getTime().getNumber() + "_" + periodsVO.getPeriods().indexOf(periodVO);
                if (periodsVO.getPeriods().size() > 1)
                {
                    if (StringUtils.isEmpty(periodVO.getPeriodGroup()))
                    {
                        _uiSupport.error("Поле обязательно для заполнения", "groupEvenId_" + itemId);
                    }
                }
                if (StringUtils.isEmpty(periodVO.getLectureRoom()))
                {
                    _uiSupport.error("Поле обязательно для заполнения", "lectureRoomId_" + itemId);
                }
                if (StringUtils.isEmpty(periodVO.getTeacher()))
                {
                    _uiSupport.error("Поле обязательно для заполнения", "teacherId_" + itemId);
                }
                if (StringUtils.isEmpty(periodVO.getSubject()))
                {
                    _uiSupport.error("Поле обязательно для заполнения", "subjectId_" + itemId);
                }
            }
        }
    }

    private void updateScheduleAsChanged()
    {
        if (null != _schedule.getIcalData())
        {
            SppScheduleICal iCal = _schedule.getIcalData();
            iCal.setChanged(true);
            DataAccessServices.dao().update(iCal);
        }

        if (null != _schedule.getIcalTeacherData())
        {
            SppScheduleICal iCalTeacherData = _schedule.getIcalTeacherData();
            iCalTeacherData.setChanged(true);
            DataAccessServices.dao().update(iCalTeacherData);
        }
    }

    public void onDeleteOddItem()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getOddWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodsVO = rowVO.getDay(day);
        Integer periodIndex = (Integer) parametersMap.get("periodIndex");
        SppSchedulePeriodVO periodVO = periodsVO.getPeriods().get(periodIndex);

        periodsVO.getPeriods().remove(periodVO);
        new EntityRenumerator<SppSchedulePeriodVO>()
        {
            @Override
            protected int getNumber(final SppSchedulePeriodVO e)
            {
                return e.getPeriod().getPeriodNum();
            }

            @Override
            protected void setNumber(final SppSchedulePeriodVO e, final int number)
            {
                e.getPeriod().setPeriodNum(number);
            }
        }.execute(periodsVO.getPeriods());

        if (periodsVO.getPeriods().isEmpty())
            periodsVO.setView(true);
        if (null != periodVO.getPeriod().getId())
        {
            SppSchedulePeriod period = periodVO.getPeriod();
            DataAccessServices.dao().delete(period);
        }
        updateScheduleAsChanged();
    }
    // odd listeners

    //even listeners
    public void onAddEvenPair()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getEvenWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodsVO = rowVO.getDay(day);
        periodsVO.setView(false);
        periodsVO.getPeriods().add(getNewPeriodVO(rowVO, periodsVO, day));
        new EntityRenumerator<SppSchedulePeriodVO>()
        {
            @Override
            protected int getNumber(final SppSchedulePeriodVO e)
            {
                return e.getPeriod().getPeriodNum();
            }

            @Override
            protected void setNumber(final SppSchedulePeriodVO e, final int number)
            {
                e.getPeriod().setPeriodNum(number);
            }
        }.execute(periodsVO.getPeriods());
    }

    public void onEditEvenPair()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getEvenWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodVO = rowVO.getDay(day);
        periodVO.setView(false);
    }

    public void onSaveEvenPair()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getEvenWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodsVO = rowVO.getDay(day);
        errorCheck(day, rowVO, periodsVO);
        if (getUserContext().getErrorCollector().hasErrors())
            return;
        periodsVO.setView(true);
        SppScheduleManager.instance().dao().saveOrUpdatePeriods(periodsVO.getPeriods());
        updateScheduleAsChanged();
    }

    public void onDeleteEvenItem()
    {
        ParametersMap parametersMap = getListenerParameter();
        Integer day = (Integer) parametersMap.get("day");
        Long weekRowId = (Long) parametersMap.get("weekRowId");
        SppScheduleWeekPeriodsRowVO rowVO = getEvenWeekDS().getRecordById(weekRowId);
        SppSchedulePeriodsVO periodsVO = rowVO.getDay(day);
        Integer periodIndex = (Integer) parametersMap.get("periodIndex");
        SppSchedulePeriodVO periodVO = periodsVO.getPeriods().get(periodIndex);
        periodsVO.getPeriods().remove(periodVO);
        new EntityRenumerator<SppSchedulePeriodVO>()
        {
            @Override
            protected int getNumber(final SppSchedulePeriodVO e)
            {
                return e.getPeriod().getPeriodNum();
            }

            @Override
            protected void setNumber(final SppSchedulePeriodVO e, final int number)
            {
                e.getPeriod().setPeriodNum(number);
            }
        }.execute(periodsVO.getPeriods());
        if (periodsVO.getPeriods().isEmpty())
            periodsVO.setView(true);
        if (null != periodVO.getPeriod().getId())
        {
            SppSchedulePeriod period = periodVO.getPeriod();
            DataAccessServices.dao().delete(period);
        }
        updateScheduleAsChanged();
    }
    //even listeners

    private SppSchedulePeriodVO getNewPeriodVO(SppScheduleWeekPeriodsRowVO rowVO, SppSchedulePeriodsVO periodsVO, int day)
    {
        SppSchedulePeriod newPeriod = new SppSchedulePeriod();
        newPeriod.setWeekRow(rowVO.getWeekRow());
        newPeriod.setPeriodNum(periodsVO.getPeriods().isEmpty() ? 1 : 2);
        newPeriod.setDayOfWeek(day);
        newPeriod.setEmpty(false);
        SppSchedulePeriodVO newPeriodVO = new SppSchedulePeriodVO(newPeriod);
        newPeriodVO.setWeekPeriodsRowVO(rowVO);
        return newPeriodVO;
    }

    public String getOddListTitle()
    {
        return _schedule.isDiffEven() ? "Нечетная неделя" : "Неделя";
    }
}
