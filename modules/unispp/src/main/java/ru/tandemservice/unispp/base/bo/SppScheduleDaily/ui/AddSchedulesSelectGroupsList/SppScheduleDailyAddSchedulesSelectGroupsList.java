/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.AddSchedulesSelectGroupsList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;

/**
 * @author Igor Belanov
 * @since 25.10.2016
 */
@Configuration
public class SppScheduleDailyAddSchedulesSelectGroupsList extends BusinessComponentManager
{
    // filters data source
    public static final String COURSE_DS = "courseDS";
    public static final String EDU_LEVEL_DS = "eduLevelDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String DEVELOP_CONDITION_DS = "developConditionDS";
    public static final String DEVELOP_TECH_DS = "developTechDS";
    public static final String DEVELOP_PERIOD_DS = "developPeriodDS";

    // group list data source
    public static final String GROUP_DS = "groupDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(GROUP_DS, groupDSColumns(), SppScheduleManager.instance().groupSearchDSHandler()))
                .addDataSource(selectDS(COURSE_DS, SppScheduleManager.instance().courseComboDSHandler()))
                .addDataSource(selectDS(EDU_LEVEL_DS, SppScheduleManager.instance().eduLevelComboDSHandler()).addColumn(EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, EducationCatalogsManager.instance().developFormDSHandler()))
                .addDataSource(selectDS(DEVELOP_CONDITION_DS, EducationCatalogsManager.instance().developConditionDSHandler()))
                .addDataSource(selectDS(DEVELOP_TECH_DS, EducationCatalogsManager.instance().developTechDSHandler()))
                .addDataSource(selectDS(DEVELOP_PERIOD_DS, EducationCatalogsManager.instance().developPeriodDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint groupDSColumns()
    {
        return columnListExtPointBuilder(GROUP_DS)
                .addColumn(checkboxColumn("check"))
                .addColumn(publisherColumn("title", Group.title()).order().required(true))
                .addColumn(textColumn("eduLevel", Group.educationOrgUnit().educationLevelHighSchool().displayableTitle()).order())
                .addColumn(textColumn("course", Group.course().title()))
                .addColumn(textColumn("developForm", Group.educationOrgUnit().developForm().title()))
                .addColumn(textColumn("developCondition", Group.educationOrgUnit().developCondition().title()))
                .addColumn(textColumn("developTech", Group.educationOrgUnit().developTech().title()))
                .addColumn(textColumn("developPeriod", Group.educationOrgUnit().developPeriod().title()))
                .create();
    }
}
