/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionEvent;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 9/2/13
 */
public class SppScheduleSessionDSHandler extends DefaultSearchDataSourceHandler
{
    // properties
    public static final String PROP_YEAR_PART_ID = "yearPartId";
    public static final String PROP_ORG_UNIT_ID = "orgUnitId";
    public static final String PROP_ORG_UNIT_IDS = "orgUnitIds";
    public static final String PROP_TITLE = "title";
    public static final String PROP_GROUP_ID = "groupId";

    public SppScheduleSessionDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final Long yearPartId = context.get(PROP_YEAR_PART_ID);

        final Long orgUnitId = context.get(PROP_ORG_UNIT_ID);
        final List<Long> orgUnitIds = orgUnitId == null ? context.get(PROP_ORG_UNIT_IDS) :
                Collections.singletonList(orgUnitId);

        final Long groupId = context.get(PROP_GROUP_ID);
        final List<Long> groups = groupId == null ? context.get("groups") :
                Collections.singletonList(groupId);

        final String title = context.get(PROP_TITLE);
        final List<Long> courses = context.get("courses");
        final List<Long> eduLevels = context.get("eduLevels");
        final List<Long> seasons = context.get("seasons");
        final List<Long> subjects = context.get("subjects");

        // создаём билдер, который пойдёт на выход
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SppScheduleSession.class, "s")
                .column("s")
                .fetchPath(DQLJoinType.inner, SppScheduleSession.season().fromAlias("s"), "season")
                .fetchPath(DQLJoinType.inner, SppScheduleSession.group().fromAlias("s"), "scheduleGroup");

        // основной фильтр по подразделениям
        if (null != orgUnitIds && !orgUnitIds.isEmpty())
        {
            DQLSelectBuilder kindRelationsBuilder= new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                    .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                    .where(in(property("rel", OrgUnitToKindRelation.orgUnit().id()), orgUnitIds));
            List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

            Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                    .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

            IDQLExpression condition = nothing(); // false
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                condition = or(condition, in(property("s", SppScheduleSession.group().educationOrgUnit().educationLevelHighSchool().orgUnit().id()), orgUnitIds));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                condition = or(condition, in(property("s", SppScheduleSession.group().educationOrgUnit().formativeOrgUnit().id()), orgUnitIds));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                condition = or(condition, in(property("s", SppScheduleSession.group().educationOrgUnit().territorialOrgUnit().id()), orgUnitIds));

            builder.where(condition);
        }

        // прочие фильтры
        if (null != subjects && !subjects.isEmpty())
        {
            DQLSelectBuilder elementsBuilder= new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "p")
                    .column(property("p", SppScheduleSessionEvent.schedule().id()))
                    .where(in(property("p", SppScheduleSessionEvent.subjectVal().id()), subjects))
                    .where(in(property("p", SppScheduleSessionEvent.schedule().archived()), value(false)));
            List<Long> ids = createStatement(elementsBuilder).list();
            builder.where(in(property("s", SppScheduleSession.id()), ids));
        }

        if (null != yearPartId)
            builder.where(eq(property("s", SppScheduleSession.season().eppYearPart().id()), value(yearPartId)));
        if (null != groups && !groups.isEmpty())
            builder.where(in(property("s", SppScheduleSession.group().id()), groups));
        if (null != courses && !courses.isEmpty())
            builder.where(in(property("s", SppScheduleSession.group().course().id()), courses));
        if (null != eduLevels && !eduLevels.isEmpty())
            builder.where(in(property("s", SppScheduleSession.group().educationOrgUnit().educationLevelHighSchool().id()), eduLevels));
        if (null != seasons && !seasons.isEmpty())
            builder.where(in(property("s", SppScheduleSession.season().id()), seasons));

        // фильтр по введённому названию
        if (!StringUtils.isEmpty(title))
            builder.where(like(DQLFunctions.upper(property("s", SppScheduleSession.title())), value(CoreStringUtils.escapeLike(title, true))));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().pageable(true).build();
    }
}
