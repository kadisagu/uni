package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unispp_2x6x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sppDisciplinePreferenceList

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("sppdisciplinepreferencelist_t"))
            {
			DBTable dbt = new DBTable("sppdisciplinepreferencelist_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("discipline_id", DBType.LONG).setNullable(false), 
				new DBColumn("teacher_id", DBType.LONG), 
				new DBColumn("building_id", DBType.LONG), 
				new DBColumn("lectureroom_id", DBType.LONG)
			);
			tool.createTable(dbt);
            }
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sppDisciplinePreferenceList");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherDailyPreferenceElement

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("spptchrdlyprfrncelmnt_t"))
            {
			DBTable dbt = new DBTable("spptchrdlyprfrncelmnt_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("dayofweek_p", DBType.INTEGER), 
				new DBColumn("startdate_p", DBType.DATE).setNullable(false), 
				new DBColumn("enddate_p", DBType.DATE).setNullable(false), 
				new DBColumn("starthour_p", DBType.INTEGER), 
				new DBColumn("endhour_p", DBType.INTEGER), 
				new DBColumn("startminute_p", DBType.INTEGER), 
				new DBColumn("endminute_p", DBType.INTEGER), 
				new DBColumn("building_id", DBType.LONG), 
				new DBColumn("lectureroom_id", DBType.LONG)
			);
			tool.createTable(dbt);
            }
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sppTeacherDailyPreferenceElement");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherDailyPreferenceList

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("spptchrdlyprfrnclst_t"))
            {
			DBTable dbt = new DBTable("spptchrdlyprfrnclst_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("teacher_id", DBType.LONG).setNullable(false), 
				new DBColumn("sppscheduledailyseason_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);
            }
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sppTeacherDailyPreferenceList");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherPreferenceElement

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("sppteacherpreferenceelement_t"))
            {
			DBTable dbt = new DBTable("sppteacherpreferenceelement_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("dayofweek_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("bell_id", DBType.LONG), 
				new DBColumn("building_id", DBType.LONG), 
				new DBColumn("lectureroom_id", DBType.LONG)
			);
			tool.createTable(dbt);
            }
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sppTeacherPreferenceElement");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherPreferenceList

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("sppteacherpreferencelist_t"))
            {
			DBTable dbt = new DBTable("sppteacherpreferencelist_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("teacher_id", DBType.LONG).setNullable(false), 
				new DBColumn("sppscheduleseason_id", DBType.LONG).setNullable(false), 
				new DBColumn("bells_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);
            }
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sppTeacherPreferenceList");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherSessionPreferenceElement

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("spptchrsssnprfrncelmnt_t"))
            {
			DBTable dbt = new DBTable("spptchrsssnprfrncelmnt_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("dayofweek_p", DBType.INTEGER), 
				new DBColumn("startdate_p", DBType.DATE).setNullable(false), 
				new DBColumn("enddate_p", DBType.DATE).setNullable(false), 
				new DBColumn("starthour_p", DBType.INTEGER), 
				new DBColumn("endhour_p", DBType.INTEGER), 
				new DBColumn("startminute_p", DBType.INTEGER), 
				new DBColumn("endminute_p", DBType.INTEGER), 
				new DBColumn("building_id", DBType.LONG), 
				new DBColumn("lectureroom_id", DBType.LONG)
			);
			tool.createTable(dbt);
            }
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sppTeacherSessionPreferenceElement");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherSessionPreferenceList

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("spptchrsssnprfrnclst_t"))
            {
			DBTable dbt = new DBTable("spptchrsssnprfrnclst_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("teacher_id", DBType.LONG).setNullable(false), 
				new DBColumn("sppschedulesessionseason_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);
            }
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sppTeacherSessionPreferenceList");

		}


    }
}