/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.AddEditPeriodFix;

import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleLectureRoomComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleSubjectComboDSHandler;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;
import ru.tandemservice.unispp.base.vo.SppScheduleDisciplineVO;
import ru.tandemservice.unispp.base.vo.SppSchedulePeriodFixDateVO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author nvankov
 * @since 9/5/13
 */
@Input({
        @Bind(key = "scheduleId", binding = "scheduleId", required = true),
        @Bind(key = "periodFixId", binding = "periodFixId")
})
public class SppScheduleAddEditPeriodFixUI extends UIPresenter
{
    private Long _scheduleId;
    private Long _periodFixId;
    private SppSchedulePeriodFix _periodFix;
    private SppSchedule _schedule;
    private SppScheduleDisciplineVO _disciplineVO;
    private List<SppSchedulePeriodFixDateVO> _fixDates = Lists.newArrayList();
    private SppSchedulePeriodFixDateVO _currentFixDate;
    private DataWrapper _lectureRoomWrapper;
    private boolean _isFillButtonDisabled = true;

    @Override
    public void onComponentRefresh()
    {
        if (null == _schedule)
        {
            _schedule = DataAccessServices.dao().getNotNull(_scheduleId);
        }
        if (null == _periodFix)
        {
            if (null != _periodFixId)
            {
                _periodFix = DataAccessServices.dao().get(_periodFixId);
                if (null != _periodFix.getSubjectVal())
                {
                    EppRegistryDiscipline discipline = (EppRegistryDiscipline) _periodFix.getSubjectVal();
                    EppWorkPlan workPlan = SppScheduleManager.instance().dao().getWorkPlanByDiscipline(discipline);
                    Term term = workPlan != null ? workPlan.getTerm() : null;
                    _disciplineVO = new SppScheduleDisciplineVO(discipline, _periodFix.getSubjectLoadType(), term);
                }
            } else
            {
                _periodFix = new SppSchedulePeriodFix();
                _periodFix.setSppSchedule(_schedule);
                _periodFix.setPeriodNum(1);
            }
        }
        checkReplacePeriodExist();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleAddEditPeriodFix.BELL_DS.equals(dataSource.getName()))
        {
            dataSource.put("bellsId", _periodFix.getSppSchedule().getBells().getId());
        }
        if (SppScheduleAddEditPeriodFix.SUBJECT_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleSubjectComboDSHandler.PROP_GROUP_IDS, Collections.singletonList(_schedule.getGroup().getId()));
            dataSource.put(SppScheduleSubjectComboDSHandler.PROP_SEASON_ID, _schedule.getSeason().getId());
        }
    }

    public void onChangeDate()
    {
        checkReplacePeriodExist();
    }

    public void onChangeBell()
    {
        checkReplacePeriodExist();
    }

    public void onChangeNumber()
    {
        checkReplacePeriodExist();
    }

    public void onChangeLectureRoom()
    {
        UniplacesPlace lectureRoom = new UniplacesPlace();
        if (_lectureRoomWrapper != null)
            lectureRoom = DataAccessServices.dao().get(_lectureRoomWrapper.getId());
        if (null != lectureRoom)
        {
            _periodFix.setLectureRoomVal(lectureRoom);
            _periodFix.setLectureRoom(lectureRoom.getTitle());
        }
    }

    public void onChangeSubject()
    {
        if (null != _disciplineVO)
        {
            String title = _disciplineVO.getTitle();
            if (title != null)
                title = title.substring(0, title.length() - 2);
            _periodFix.setSubject(title);
        }
    }

    public void onChangeTeacher()
    {
        if (null != _periodFix.getTeacherVal())
            _periodFix.setTeacher(_periodFix.getTeacherVal().getFio());
    }

    /**
     * проверяет существует ли заменяемая пара
     * и если существует, то выставляет активность кнопки заполнения
     */
    public void checkReplacePeriodExist()
    {
        _isFillButtonDisabled = true;
        if (!_fixDates.isEmpty()) return;
        if (_periodFix == null) return;
        if (_periodFix.getBell() == null) return;
        SppSchedulePeriod period = SppScheduleManager.instance().dao()
                .getPeriod(_scheduleId, _periodFix.getDate(), _periodFix.getBell().getId(), _periodFix.getPeriodNum());
        if (period == null || period.isEmpty()) return;

        // включаем (все проверки прошли)
        _isFillButtonDisabled = false;
    }

    public void onClickAddDate()
    {
        _fixDates.add(new SppSchedulePeriodFixDateVO());
        checkReplacePeriodExist();
    }

    public void onClickDeleteDate()
    {
        _fixDates.remove(_fixDates.get(getListenerParameter()));
        checkReplacePeriodExist();
    }

    /**
     * заполняет данные из заменяемой пары
     */
    public void onClickFill()
    {
        _isFillButtonDisabled = true;
        if (!_fixDates.isEmpty()) return;
        if (_periodFix == null) return;
        if (_periodFix.getBell() == null) return;
        SppSchedulePeriod period = SppScheduleManager.instance().dao()
                .getPeriod(_scheduleId, _periodFix.getDate(), _periodFix.getBell().getId(), _periodFix.getPeriodNum());
        if (period == null || period.isEmpty()) return;

        DataWrapper lectureRoomWrapper = null;
        if (period.getLectureRoomVal() != null)
            lectureRoomWrapper = SppScheduleLectureRoomComboDSHandler.prepareWrapper(period.getLectureRoomVal());
        SppScheduleDisciplineVO disciplineVO = null;
        if (period.getSubjectVal() != null && period.getSubjectLoadType() != null)
        {
            EppRegistryDiscipline discipline = (EppRegistryDiscipline) period.getSubjectVal();
            EppWorkPlan workPlan = SppScheduleManager.instance().dao().getWorkPlanByDiscipline(discipline);
            Term term = workPlan != null ? workPlan.getTerm() : null;
            disciplineVO = new SppScheduleDisciplineVO(discipline, period.getSubjectLoadType(), term);
        }
        setLectureRoomWrapper(lectureRoomWrapper);
        _periodFix.setLectureRoom(period.getLectureRoom());
        setDisciplineVO(disciplineVO);
        _periodFix.setSubject(period.getSubject());
        _periodFix.setTeacherVal(period.getTeacherVal());
        _periodFix.setTeacher(period.getTeacher());

        _isFillButtonDisabled = false;
    }

    public void onClickApply()
    {
        // нужно проверить не вышла ли какая-то дата за пределы периода расписания
        SppScheduleSeason season = UniDaoFacade.getCoreDao().getProperty(SppSchedule.class, SppSchedule.L_SEASON, SppSchedule.P_ID, _scheduleId);
        List<Date> dates = new ArrayList<>();
        if (_fixDates != null && !_fixDates.isEmpty())
            dates.addAll(_fixDates.stream().map(SppSchedulePeriodFixDateVO::getDate).collect(Collectors.toList()));
        dates.add(_periodFix.getDate());
        for (Date date : dates)
        {
            if (date.before(season.getStartDate()) || date.after(season.getEndDate()))
                throw new ApplicationException("Исправления можно создавать только в пределах периода расписания");
        }

        List<SppSchedulePeriodFix> periodFixes = Lists.newArrayList();
        boolean existOnThisDay = SppScheduleManager.instance().dao().periodFixExistOnThisDayAndPeriod(_periodFix);
        if (existOnThisDay)
            _uiSupport.error("На данные день, пару и подгруппу уже существует исправление", "startDate", "bellDS");
        if (_periodFix.isCanceled())
        {
            _periodFix.setSubject(null);
            _periodFix.setSubjectVal(null);
            _periodFix.setSubjectLoadType(null);
            _periodFix.setLectureRoom(null);
            _periodFix.setLectureRoomVal(null);
            _periodFix.setTeacher(null);
            _periodFix.setTeacherVal(null);
        }
        if (null != _disciplineVO)
        {
            _periodFix.setSubjectVal(_disciplineVO.getDiscipline());
            _periodFix.setSubjectLoadType(_disciplineVO.getEppALoadType());
        } else
        {
            _periodFix.setSubjectVal(null);
            _periodFix.setSubjectLoadType(null);
        }

        periodFixes.add(_periodFix);

        if (getAddForm() && !_fixDates.isEmpty())
        {
            for (SppSchedulePeriodFixDateVO dateVO : _fixDates)
            {
                if (null != dateVO.getDate())
                {
                    SppSchedulePeriodFix periodFix = new SppSchedulePeriodFix();
                    periodFix.update(_periodFix);
                    periodFix.setDate(dateVO.getDate());
                    periodFixes.add(periodFix);
                    boolean exist = SppScheduleManager.instance().dao().periodFixExistOnThisDayAndPeriod(periodFix);
                    if (exist)
                        _uiSupport.error("На данные день, пару и подгруппу уже существует исправление", "fixDate_" + _fixDates.indexOf(dateVO), "bellDS");
                    if (getUserContext().getErrorCollector().hasErrors())
                        return;
                }
            }
        }

        if (getUserContext().getErrorCollector().hasErrors())
            return;
        for (SppSchedulePeriodFix periodFix : periodFixes)
        {
            SppScheduleManager.instance().dao().createOrUpdatePeriodFix(periodFix);
        }
        deactivate();
    }

    public Boolean getAddForm()
    {
        return null == _periodFixId;
    }

    public int getIndexCurrent()
    {
        return _fixDates.indexOf(_currentFixDate);
    }

    public String getCurrentId()
    {
        return "fixDate_" + _fixDates.indexOf(_currentFixDate);
    }

    public SppSchedulePeriod getReplacePeriod(Date date, Long bellId, int number)
    {
        SppScheduleManager.instance().dao().getPeriod(_scheduleId, date, bellId, number);
        return null;
    }

    // getters & setters
    public DataWrapper getLectureRoomWrapper()
    {
        return _lectureRoomWrapper;
    }

    public void setLectureRoomWrapper(DataWrapper lectureRoomWrapper)
    {
        _lectureRoomWrapper = lectureRoomWrapper;
    }

    public Long getScheduleId()
    {
        return _scheduleId;
    }

    public void setScheduleId(Long scheduleId)
    {
        _scheduleId = scheduleId;
    }

    public Long getPeriodFixId()
    {
        return _periodFixId;
    }

    public void setPeriodFixId(Long periodFixId)
    {
        _periodFixId = periodFixId;
    }

    public SppSchedulePeriodFix getPeriodFix()
    {
        return _periodFix;
    }

    public void setPeriodFix(SppSchedulePeriodFix periodFix)
    {
        _periodFix = periodFix;
    }

    public SppSchedule getSchedule()
    {
        return _schedule;
    }

    public void setSchedule(SppSchedule schedule)
    {
        _schedule = schedule;
    }

    public SppScheduleDisciplineVO getDisciplineVO()
    {
        return _disciplineVO;
    }

    public void setDisciplineVO(SppScheduleDisciplineVO disciplineVO)
    {
        _disciplineVO = disciplineVO;
    }

    public List<SppSchedulePeriodFixDateVO> getFixDates()
    {
        return _fixDates;
    }

    public void setFixDates(List<SppSchedulePeriodFixDateVO> fixDates)
    {
        _fixDates = fixDates;
    }

    public SppSchedulePeriodFixDateVO getCurrentFixDate()
    {
        return _currentFixDate;
    }

    public void setCurrentFixDate(SppSchedulePeriodFixDateVO currentFixDate)
    {
        _currentFixDate = currentFixDate;
    }

    public boolean isFillButtonDisabled()
    {
        return _isFillButtonDisabled;
    }

    public void setFillButtonDisabled(boolean fillButtonDisabled)
    {
        _isFillButtonDisabled = fillButtonDisabled;
    }
}
