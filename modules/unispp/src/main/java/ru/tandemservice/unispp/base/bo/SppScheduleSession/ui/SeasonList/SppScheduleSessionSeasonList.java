/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.SeasonList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;

/**
 * @author vnekrasov
 * @since 12/26/13
 */
@Configuration
public class SppScheduleSessionSeasonList extends BusinessComponentManager
{
    public final static String SCHEDULE_SESSION_SEASON_DS = "scheduleSessionSeasonDS";

    @Bean
    public ColumnListExtPoint scheduleSeasonCL()
    {
        return columnListExtPointBuilder(SCHEDULE_SESSION_SEASON_DS)
                .addColumn(textColumn("title", SppScheduleSessionSeason.title()).order().required(true))
                .addColumn(textColumn("yearPart", SppScheduleSessionSeason.eppYearPart().title()).required(true))
                .addColumn(dateColumn("startDate", SppScheduleSessionSeason.startDate()).order())
                .addColumn(dateColumn("endDate", SppScheduleSessionSeason.endDate()).order())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("sppScheduleSessionSeasonListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("message:ui.deleteAlert").permissionKey("sppScheduleSessionSeasonListDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SCHEDULE_SESSION_SEASON_DS, scheduleSeasonCL(), SppScheduleSessionManager.instance().sppScheduleSessionSeasonDSHandler()))
                .create();
    }
}
