/*$Id$*/
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.MassAddingSelectSchedulesList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.*;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.View.SppScheduleView;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;

/**
 * @author DMITRY KNYAZEV
 * @since 29.10.2015
 */
@Configuration
public class SppScheduleMassAddingSelectSchedulesList extends BusinessComponentManager
{
    // schedule list
    public static final String SPP_SCHEDULE_DS = "sppScheduleDS";

    // filters
    public static final String SEASON_DS = "seasonDS";
    public static final String BELL_SCHEDULE_DS = "bellScheduleDS";
    public static final String GROUP_DS = "groupDS";
    public static final String FORMATIVE_ORG_UNITS_DS = "formativeOrgUnitsDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SPP_SCHEDULE_DS, sppScheduleDSColumns(), SppScheduleManager.instance().sppScheduleDSHandler()))
                .addDataSource(selectDS(SEASON_DS, SppScheduleManager.instance().seasonComboDSHandler()).addColumn("title", null, new IFormatter<SppScheduleSeason>()
                {
                    @Override
                    public String format(SppScheduleSeason source)
                    {
                        return source.getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getStartDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndDate()) + ")";
                    }
                }))
                .addDataSource(selectDS(BELL_SCHEDULE_DS, SppScheduleManager.instance().bellsComboDSHandler()))
                .addDataSource(selectDS(GROUP_DS, SppScheduleManager.instance().groupComboDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNITS_DS, formativeOrgUnitsDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint sppScheduleDSColumns()
    {
        return columnListExtPointBuilder(SPP_SCHEDULE_DS)
                .addColumn(checkboxColumn("check"))
                .addColumn(publisherColumn("title", SppSchedule.title())
                        .businessComponent(SppScheduleView.class)
                        .required(true)
                        .parameters("ui:parametersMap"))
                .addColumn(textColumn("season", SppSchedule.season().titleWithTime()))
                .addColumn(textColumn("bellSchedule", SppSchedule.bells().title()))
                .addColumn(textColumn("group", SppSchedule.group().title()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitsDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), OrgUnit.class);
    }
}
