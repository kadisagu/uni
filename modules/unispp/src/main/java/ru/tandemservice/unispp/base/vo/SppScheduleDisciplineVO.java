/* $Id$ */
package ru.tandemservice.unispp.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;

import javax.validation.constraints.NotNull;
import java.util.Comparator;

/**
 * @author nvankov
 * @since 10/1/13
 */
public class SppScheduleDisciplineVO extends DataWrapper implements Comparator<SppScheduleDisciplineVO>, Comparable<SppScheduleDisciplineVO>
{
    private final EppRegistryDiscipline _discipline;
    private final EppALoadType _eppALoadType;
    private final Term _term;

    public SppScheduleDisciplineVO(EppRegistryDiscipline discipline, EppALoadType eppALoadType, Term term)
    {
        super(discipline.getId() + eppALoadType.getId(), createTitle(discipline, eppALoadType, term));
        _discipline = discipline;
        _eppALoadType = eppALoadType;
        _term = term;
    }

    public EppRegistryDiscipline getDiscipline()
    {
        return _discipline;
    }

    public EppALoadType getEppALoadType()
    {
        return _eppALoadType;
    }

    public int compare(SppScheduleDisciplineVO o1, SppScheduleDisciplineVO o2)
    {
        return o1.compareTo(o2);
    }

    @Override
    public int compareTo(@NotNull SppScheduleDisciplineVO o)
    {
        int result;
        if (_term == null && o._term == null) result = 0;
        else if (_term == null) result = -1;
        else if (o._term == null) result = 1;
        else result = Integer.compare(this._term.getIntValue(), o._term.getIntValue());
        if (result != 0) return result;
        return this.getTitle().compareTo(o.getTitle());
    }

    private static String createTitle(EppRegistryDiscipline discipline, EppALoadType eppALoadType, Term term)
    {
        return discipline.getTitle()
                + " (" + eppALoadType.getShortTitle() + ")"
                + (term != null ? (" " + term.getIntValue()) : "  "); // костыль, т.к. у нас часто в интерфейсе обрезается 2 символа в конце
    }
}
