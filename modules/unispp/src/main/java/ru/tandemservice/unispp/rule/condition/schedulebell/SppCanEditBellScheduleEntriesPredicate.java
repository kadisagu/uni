/* $Id$ */
package ru.tandemservice.unispp.rule.condition.schedulebell;


import org.apache.commons.collections15.Predicate;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;

/**
 * @author nvankov
 * @since 2/19/14
 */

public class SppCanEditBellScheduleEntriesPredicate implements Predicate<ScheduleBell>
{
    @Override
    public boolean evaluate(ScheduleBell bellSchedule)
    {
        return SppScheduleManager.instance().dao().canEditBellsEntries(bellSchedule.getId());
    }
}
