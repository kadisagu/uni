/* $Id:$ */
package ru.tandemservice.unispp.component.registry.registryElementSppData.View;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.unispp.base.bo.SppDisciplinePreference.SppDisciplinePreferenceManager;
import ru.tandemservice.unispp.base.bo.SppDisciplinePreference.ui.Add.SppDisciplinePreferenceAdd;
import ru.tandemservice.unispp.base.bo.SppDisciplinePreference.ui.Edit.SppDisciplinePreferenceEdit;


/**
 * @author Victor Nekrasov
 * @since 21.05.2014
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public void onClickAddPreference(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(SppDisciplinePreferenceAdd.class.getSimpleName(), new ParametersMap()
                .add("disciplineId", model.getElement().getId())));
    }

    public void onClickEditSppData(IBusinessComponent component)
    {
        Model model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.unispp.component.registry.registryElementSppData.AddEdit", new ParametersMap()
                .add("eppRegElementId", model.getElement().getId())));
    }

    public void onClickEditItem(final IBusinessComponent component)
    {
        Model model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(SppDisciplinePreferenceEdit.class.getSimpleName(), new ParametersMap()
                .add("preferenceId", component.getListenerParameter())
        ));
    }

    public void onClickDeleteItem(final IBusinessComponent component)
    {
        Model model = getModel(component);
        SppDisciplinePreferenceManager.instance().dao().deleteDisciplinePreferenceList((Long) component.getListenerParameter());
        component.refresh();
    }

//    public static Long executeSave(final IBusinessComponent component) {
//        final Model model = (Model)component.getModel();
//
//        ((Controller)component.getController()).save(component);
//        return model.getElement().getId();
//    }


    @SuppressWarnings("unchecked")
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

//    public void onClickApply(final IBusinessComponent component) {
//        AddEditResult.saveAndDeactivate(component, this.getModel(component).getElement(), new Runnable()
//        {
//            @Override
//            public void run()
//            {
//                Controller.this.save(component);
//            }
//        });
//    }

//    protected void save(final IBusinessComponent component) {
//        this.getDao().save(this.getModel(component));
//    }
}
