/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.DailyView;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.DailyAddEdit.SppTeacherPreferenceDailyAddEdit;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.DailyElementAdd.SppTeacherPreferenceDailyElementAdd;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.DailyElementEdit.SppTeacherPreferenceDailyElementEdit;
import ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author vnekrasov
 * @since 7/2/14
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "teacherDailyPreferenceId", required = true)
})
public class SppTeacherPreferenceDailyViewUI extends UIPresenter
{
    private Long _teacherDailyPreferenceId;
    private SppTeacherDailyPreferenceList _teacherDailyPreference;

    private boolean _himself;

    public Long getTeacherDailyPreferenceId()
    {
        return _teacherDailyPreferenceId;
    }

    public void setTeacherDailyPreferenceId(Long teacherDailyPreferenceId)
    {
        _teacherDailyPreferenceId = teacherDailyPreferenceId;
    }


    //    visible="ui:editScheduleVisible"
    public Boolean getEditTeacherDailyPreferenceVisible()
    {
        return true;
    }

    public Boolean getAddElementVisible()
    {
        return true;
    }

    //    ui:eventDeleteDisabled
    public Boolean getElementDeleteDisabled()
    {
        return false;
    }

    //    ui:eventEditDisabled
    public Boolean getElementEditDisabled()
    {
        return false;
    }

//    public Map getGroupParameterMap()
//    {
//        return new ParametersMap()
//                .add(UIPresenter.PUBLISHER_ID, _teacherDailyPreference.getGroup().getId())
//                .add("selectedTabId", "sppScheduleSessionTab");
//    }

    @Override
    public void onComponentRefresh()
    {
//        if(null == _schedule)
            _teacherDailyPreference = DataAccessServices.dao().getNotNull(_teacherDailyPreferenceId);

        IPrincipalContext principalContext = getConfig().getUserContext().getPrincipalContext();
        List<IPrincipalContext> personPrincipalContextList = _teacherDailyPreference.getTeacher().getPrincipalContextList();

        _himself = false;
        if (personPrincipalContextList != null)
        {
            _himself = personPrincipalContextList.stream().map(IPrincipalContext::getPrincipal)
                    .collect(Collectors.toSet()).contains(principalContext.getPrincipal());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppTeacherPreferenceDailyView.DAILY_PREFERENCE_ELEMENTS_DS.equals(dataSource.getName()))
        {
            dataSource.put("teacherDailyPreferenceId", _teacherDailyPreference.getId());
        }
    }

    public void onClickEditTeacherDailyPreference()
    {
        _uiActivation.asRegion(SppTeacherPreferenceDailyAddEdit.class).parameter("teacherDailyPreferenceId", _teacherDailyPreferenceId).activate();
    }

    public void onClickAddElement()
    {
        _uiActivation.asRegion(SppTeacherPreferenceDailyElementAdd.class).parameter("teacherDailyPreferenceId", _teacherDailyPreferenceId).activate();
    }

    public void onClickEditElement()
    {
        _uiActivation.asRegion(SppTeacherPreferenceDailyElementEdit.class)
                .parameter("teacherDailyPreferenceId", _teacherDailyPreferenceId)
                .parameter("elementId", getListenerParameterAsLong()).activate();
    }

    public void onClickDeleteElement()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        //updateTeacherDailyPreferenceAsChanged();
    }


    public String getViewPermissionKey()
    {
        return SppTeacherPreferenceManager.getPermissionKey("sppTeacherDailyPreferenceViewView", _himself);
    }

    public String getEditPermissionKey()
    {
        return SppTeacherPreferenceManager.getPermissionKey("sppTeacherDailyPreferenceViewEdit", _himself);
    }

    public String getAddPermissionKey()
    {
        return SppTeacherPreferenceManager.getPermissionKey("sppTeacherDailyPreferenceViewAdd", _himself);
    }
}
