/* $Id$ */
package ru.tandemservice.unispp.base.vo;

import ru.tandemservice.unispp.base.entity.SppSchedule;

/**
 * @author nvankov
 * @since 8/30/13
 */
public class SppScheduleVO
{
    private SppSchedule _schedule;
    private SppScheduleWeekVO _oddWeek;
    private SppScheduleWeekVO _evenWeek;

    public SppScheduleVO(SppSchedule schedule)
    {
        _schedule = schedule;
    }

    public SppSchedule getSchedule()
    {
        return _schedule;
    }

    public void setSchedule(SppSchedule schedule)
    {
        _schedule = schedule;
    }

    public SppScheduleWeekVO getOddWeek()
    {
        return _oddWeek;
    }

    public void setOddWeek(SppScheduleWeekVO oddWeek)
    {
        _oddWeek = oddWeek;
    }

    public SppScheduleWeekVO getEvenWeek()
    {
        return _evenWeek;
    }

    public void setEvenWeek(SppScheduleWeekVO evenWeek)
    {
        _evenWeek = evenWeek;
    }
}
