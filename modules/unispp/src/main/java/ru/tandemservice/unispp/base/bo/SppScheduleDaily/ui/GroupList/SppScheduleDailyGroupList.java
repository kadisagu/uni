/* $Id: SppScheduleDailyGroupList.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.GroupList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.View.SppScheduleDailyView;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@Configuration
public class SppScheduleDailyGroupList extends BusinessComponentManager
{
    public final static String SCHEDULE_DS = "scheduleDS";

    @Bean
    public ColumnListExtPoint scheduleCL()
    {
        return columnListExtPointBuilder(SCHEDULE_DS)
                .addColumn(publisherColumn("title", SppScheduleDaily.title()).businessComponent(SppScheduleDailyView.class).order().required(true))
                .addColumn(dateColumn("startDate", SppScheduleDaily.season().startDate()).order())
                .addColumn(dateColumn("endDate", SppScheduleDaily.season().endDate()).order())
                .addColumn(booleanColumn("approved", SppScheduleDaily.approved()))
                .addColumn(booleanColumn("archived", SppScheduleDaily.archived()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled("ui:entityEditDisabled").permissionKey("sppScheduleDailyGroupListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).disabled("ui:entityDeleteDisabled").alert(new FormattedMessage("ui.deleteAlert", SppScheduleDaily.title())).permissionKey("sppScheduleDailyGroupListDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SCHEDULE_DS, scheduleCL(), SppScheduleDailyManager.instance().sppScheduleDailyDSHandler()))
                .create();
    }
}
