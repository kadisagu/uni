/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 12/30/13
 */
public class SppScheduleSessionComboDSHandler extends SimpleTitledComboDataSourceHandler
{
    public SppScheduleSessionComboDSHandler(String ownerId)
    {
        super(ownerId);
        _filtered = true;
    }

    public enum Columns
    {
        SCHEDULE(SppScheduleSession.class, null, SppScheduleSession.id(), SppScheduleSession.title(), SppScheduleSession.title()),
        SEASON(SppScheduleSessionSeason.class, SppScheduleSession.season(), SppScheduleSession.season().id(), SppScheduleSession.season().title(), SppScheduleSessionSeason.title()),
        BELLS(ScheduleBell.class, SppScheduleSession.bells(), SppScheduleSession.bells().id(), SppScheduleSession.bells().title(), ScheduleBell.title());

        Columns(Class entityClass, MetaDSLPath column, MetaDSLPath idColumn, MetaDSLPath orderPath, MetaDSLPath filterPath)
        {
            _entityClass = entityClass;
            _column = column;
            _idColumn = idColumn;
            _orderPath = orderPath;
            _filterPath = filterPath;
        }

        private Class _entityClass;
        private MetaDSLPath _column;
        private MetaDSLPath _idColumn;
        private MetaDSLPath _orderPath;
        private MetaDSLPath _filterPath;

        public MetaDSLPath getColumn()
        {
            return _column;
        }

        public MetaDSLPath getOrderPath()
        {
            return _orderPath;
        }

        public void setOrderPath(MetaDSLPath orderPath)
        {
            _orderPath = orderPath;
        }

        public void setColumn(MetaDSLPath column)
        {
            _column = column;
        }

        public MetaDSLPath getFilterPath()
        {
            return _filterPath;
        }

        public void setFilterPath(MetaDSLPath filterPath)
        {
            _filterPath = filterPath;
        }

        public Class getEntityClass()
        {
            return _entityClass;
        }

        public void setEntityClass(Class entityClass)
        {
            _entityClass = entityClass;
        }

        public MetaDSLPath getIdColumn()
        {
            return _idColumn;
        }

        public void setIdColumn(MetaDSLPath idColumn)
        {
            _idColumn = idColumn;
        }
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Columns column = context.get("column");
        SppScheduleSessionPrintParams scheduleData = context.get("scheduleData");

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(SppScheduleSession.class, "s");
        subBuilder.where(eq(property("s", SppScheduleSession.approved()), value(true)));
        subBuilder.where(eq(property("s", SppScheduleSession.archived()), value(false)));

        if (null != scheduleData.getCurrentOrgUnit())
        {
            Long orgUnitId = scheduleData.getCurrentOrgUnit().getId();
            DQLSelectBuilder kindRelationsBuilder = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                    .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                    .where(eq(property("rel", OrgUnitToKindRelation.orgUnit().id()), value(orgUnitId)));
            List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

            Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                    .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

            IDQLExpression condition = nothing(); // false
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                condition = or(condition, eq(property("s", SppScheduleSession.group().educationOrgUnit().educationLevelHighSchool().orgUnit().id()), value(orgUnitId)));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                condition = or(condition, eq(property("s", SppScheduleSession.group().educationOrgUnit().formativeOrgUnit().id()), value(orgUnitId)));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                condition = or(condition, eq(property("s", SppScheduleSession.group().educationOrgUnit().territorialOrgUnit().id()), value(orgUnitId)));

            subBuilder.where(condition);
        }
        subBuilder.distinct();
        subBuilder.column(property("s", column.getIdColumn()));

        if (!Columns.SEASON.equals(column))
        {
            if (null == scheduleData.getSeason())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            if (null != scheduleData.getSeason())
                subBuilder.where(eq(property("s", SppScheduleSession.season()), value(scheduleData.getSeason())));
        }
        if (!Columns.SEASON.equals(column) && !Columns.BELLS.equals(column))
        {
            if (null == scheduleData.getSeason() && null == scheduleData.getBells())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            if (null != scheduleData.getBells())
                subBuilder.where(eq(property("s", SppScheduleSession.bells()), value(scheduleData.getBells())));
        }

        subBuilder.where(eq(property("s", SppScheduleSession.group().educationOrgUnit().formativeOrgUnit()), value(scheduleData.getFormativeOrgUnit())));
        subBuilder.where(eq(property("s", SppScheduleSession.group().educationOrgUnit().developForm()), value(scheduleData.getDevelopForm())));
//        subBuilder.where(eq(property("s", SppScheduleSession.group().course()), value(scheduleData.getCourse())));

        addAdditionalFilters(subBuilder, scheduleData);

        if (null != scheduleData.getEduLevels() && !scheduleData.getEduLevels().isEmpty())
        {
            subBuilder.where(in(property("s", SppScheduleSession.group().educationOrgUnit().educationLevelHighSchool()), scheduleData.getEduLevels()));
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(column.getEntityClass(), "e");
        builder.where(in(property("e.id"), subBuilder.buildQuery()));

        setOrderByProperty(column.getOrderPath().s());
        setFilterByProperty(column.getFilterPath().s());
        context.put(UIDefines.COMBO_OBJECT_LIST, createStatement(builder).list());
        return super.execute(input, context);
    }

    protected void addAdditionalFilters(DQLSelectBuilder builder, SppScheduleSessionPrintParams scheduleData)
    {
    }
}
