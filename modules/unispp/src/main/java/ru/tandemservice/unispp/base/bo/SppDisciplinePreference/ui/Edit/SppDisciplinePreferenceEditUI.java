/* $Id: */
package ru.tandemservice.unispp.base.bo.SppDisciplinePreference.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppDisciplinePreference.SppDisciplinePreferenceManager;
import ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList;

/**
 * @author vnekrasov
 * @since 7/3/14
 */
@Input({
        @Bind(key = "preferenceId", binding = "preferenceId")
})
public class SppDisciplinePreferenceEditUI extends UIPresenter
{
    private Long _preferenceId;
    private SppDisciplinePreferenceList _preference;

    public Long getPreferenceId()
    {
        return _preferenceId;
    }

    public void setPreferenceId(Long preferenceId)
    {
        _preferenceId = preferenceId;
    }

    public SppDisciplinePreferenceList getPreference()
    {
        return _preference;
    }

    public void setPreference(SppDisciplinePreferenceList preference)
    {
        _preference = preference;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _preference)
        {
            _preference = DataAccessServices.dao().getNotNull(_preferenceId);
        }

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppDisciplinePreferenceEdit.LECTURE_ROOM_DS.equals(dataSource.getName()))
        {
            if(_preference.getBuilding() != null)
                dataSource.put("buildingId", _preference.getBuilding().getId());
        }
    }

    public void onChangeTeacher()
    {

    }

    public void onChangeLectureRoom()
    {
//        if(null != _element.getLectureRoom())
//            _event.setLectureRoom(_event.getLectureRoomVal().getTitle());
    }

    public void onChangeBuilding()
    {
    }

    public void onClickApply()
    {
        if(_preference.getBuilding() == null && _preference.getLectureRoom() == null)
            _uiSupport.error("Выберите здание или аудиторию.", "buildingElemenDS", "lectureRoomElemenDS");

        if(getUserContext().getErrorCollector().hasErrors())
            return;

        SppDisciplinePreferenceManager.instance().dao().saveOrUpdateDisciplinePreferenceList(_preference);
        deactivate();
    }
}
