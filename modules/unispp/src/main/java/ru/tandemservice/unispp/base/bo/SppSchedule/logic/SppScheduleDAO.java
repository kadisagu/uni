/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.*;
import ru.tandemservice.unispp.base.entity.catalog.SppScheduleStatus;
import ru.tandemservice.unispp.base.entity.catalog.codes.SppScheduleStatusCodes;
import ru.tandemservice.unispp.base.vo.*;
import ru.tandemservice.unispp.util.SppScheduleUtil;

import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author nvankov
 * @since 9/2/13
 */
@Transactional
public class SppScheduleDAO extends UniBaseDao implements ISppScheduleDAO
{
    @Override
    public List<ScheduleBellEntry> getBells(Long bellsId)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ScheduleBellEntry.class, "b");
        builder.where(eq(property("b", ScheduleBellEntry.schedule().id()), value(bellsId)));
        builder.order(property("b", ScheduleBellEntry.number()), OrderDirection.asc);

        return builder.createStatement(getSession()).list();
    }

    @Override
    public SppSchedulePrintForm savePrintForm(SppSchedulePrintDataVO scheduleData)
    {
        SppSchedulePrintForm printForm = new SppSchedulePrintForm();
        printForm.setFormativeOrgUnit(scheduleData.getFormativeOrgUnit());
        if (null != scheduleData.getEduLevels() && !scheduleData.getEduLevels().isEmpty())
            printForm.setEduLevels(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(scheduleData.getEduLevels(), EducationLevelsHighSchool.printTitle()), ", "));
        printForm.setCreateDate(new Date());
        printForm.setCourse(scheduleData.getCourse());
        printForm.setSeason(scheduleData.getSeason());
        printForm.setBells(scheduleData.getBells());
        printForm.setChief(scheduleData.getChief());
        printForm.setCreateOU(scheduleData.getCurrentOrgUnit());
        printForm.setGroups(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(scheduleData.getSchedules(), SppSchedule.group().title()), ", "));
        printForm.setEduYear(scheduleData.getEducationYear().getTitle());

        DatabaseFile content = new DatabaseFile();

        content.setContent(getSchedulePrintContent(scheduleData));

        save(content);
        printForm.setContent(content);
        save(printForm);
        return printForm;
    }

    protected byte[] getSchedulePrintContent(SppSchedulePrintDataVO scheduleData)
    {
        List<SppScheduleGroupPrintVO> schedules = Lists.newArrayList();
        for (SppSchedule schedule : scheduleData.getSchedules())
            schedules.add(prepareScheduleGroupPrintVO(schedule));

        try
        {
            return SppSchedulePrintFormExcelContentBuilder.buildExcelContent(schedules, scheduleData);
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    //    @Override
    //    public boolean getBellsEditDisable(Long bellsId)
    //    {
    //        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppSchedule.class, "s");
    //        builder.where(eq(property("s", SppSchedule.bells().id()), value(bellsId)));
    //        builder.where(eq(property("s", SppSchedule.status().code()), value(SppScheduleStatusCodes.APPROVED)));
    //
    //        return !createStatement(builder).list().isEmpty();
    //    }

    @Override
    public boolean canEditBellsEntries(Long bellsId)
    {
        return !existsEntity(SppSchedule.class, SppSchedule.L_BELLS, bellsId);
    }

    @Override
    public void createOrUpdateScheduelSeason(SppScheduleSeason scheduleSeason)
    {
        saveOrUpdate(scheduleSeason);
    }

    @Override
    public void saveSchedules(List<SppSchedule> scheduleList)
    {
        for (SppSchedule schedule : scheduleList)
        {
            save(schedule);
            createAndSaveWeekRows(schedule);
        }
    }

    @Override
    public void saveOrUpdateSchedule(SppSchedule schedule, ScheduleBell oldBells, boolean oldDiffEven)
    {
        boolean deleteAndCreateStructure = false;
        if (null == schedule.getId()) deleteAndCreateStructure = true;
        if (null != oldBells && !oldBells.getId().equals(schedule.getBells().getId())) deleteAndCreateStructure = true;
        if (schedule.isDiffEven() != oldDiffEven) deleteAndCreateStructure = true;
        saveOrUpdate(schedule);
        if (deleteAndCreateStructure)
        {
            DQLDeleteBuilder deleteWeekRowsBuilder = new DQLDeleteBuilder(SppScheduleWeekRow.class);
            deleteWeekRowsBuilder.where(eq(property(SppScheduleWeekRow.schedule().id()), value(schedule.getId())));
            deleteWeekRowsBuilder.createStatement(getSession()).execute();

            DQLDeleteBuilder deletePeriodFixBuilder = new DQLDeleteBuilder(SppSchedulePeriodFix.class);
            deletePeriodFixBuilder.where(eq(property(SppSchedulePeriodFix.sppSchedule().id()), value(schedule.getId())));
            deletePeriodFixBuilder.createStatement(getSession()).execute();

            createAndSaveWeekRows(schedule);
        }
    }

    private void createAndSaveWeekRows(SppSchedule schedule)
    {
        List<ScheduleBellEntry> bells = getBells(schedule.getBells().getId());
        for (ScheduleBellEntry bell : bells)
        {
            SppScheduleWeekRow oddWeekRow = new SppScheduleWeekRow();
            oddWeekRow.setSchedule(schedule);
            oddWeekRow.setBell(bell);
            oddWeekRow.setEven(false);
            save(oddWeekRow);

            if (schedule.isDiffEven())
            {
                SppScheduleWeekRow evenWeekRow = new SppScheduleWeekRow();
                evenWeekRow.setSchedule(schedule);
                evenWeekRow.setBell(bell);
                evenWeekRow.setEven(true);
                save(evenWeekRow);
            }
        }
    }

    @Override
    public void copySchedule(Long scheduleId, SppSchedule newSchedule, boolean swapWeeks)
    {
        save(newSchedule);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleWeekRow.class, "w");
        builder.where(eq(property("w", SppScheduleWeekRow.schedule().id()), value(scheduleId)));
        List<SppScheduleWeekRow> weekRowList = builder.createStatement(getSession()).list();

        for (SppScheduleWeekRow weekRow : weekRowList)
        {
            SppScheduleWeekRow newWeekRow = new SppScheduleWeekRow();
            newWeekRow.setBell(weekRow.getBell());
            newWeekRow.setEven(swapWeeks != weekRow.isEven()); // если нужно поменять недели, то выставится наоборот
            newWeekRow.setSchedule(newSchedule);

            save(newWeekRow);

            DQLSelectBuilder periodBuilder = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "p");
            periodBuilder.where(eq(property("p", SppSchedulePeriod.weekRow().id()), value(weekRow.getId())));
            List<SppSchedulePeriod> periodList = periodBuilder.createStatement(getSession()).list();

            for (SppSchedulePeriod period : periodList)
            {
                SppSchedulePeriod newPeriod = new SppSchedulePeriod();
                newPeriod.setDayOfWeek(period.getDayOfWeek());
                newPeriod.setEmpty(period.isEmpty());
                newPeriod.setLectureRoom(period.getLectureRoom());
                newPeriod.setLectureRoomVal(period.getLectureRoomVal());
                newPeriod.setTeacher(period.getTeacher());
                newPeriod.setTeacherVal(period.getTeacherVal());
                newPeriod.setPeriodGroup(period.getPeriodGroup());
                newPeriod.setPeriodNum(period.getPeriodNum());
                newPeriod.setSubject(period.getSubject());
                newPeriod.setSubjectVal(period.getSubjectVal());
                newPeriod.setSubjectLoadType(period.getSubjectLoadType());
                newPeriod.setWeekRow(newWeekRow);
                save(newPeriod);
            }
        }
    }

    @Override
    public void deleteSchedule(Long scheduleId)
    {
        delete(scheduleId);
    }

    @Override
    public SppScheduleVO prepareScheduleVO(SppSchedule schedule)
    {
        SppScheduleVO scheduleVO = new SppScheduleVO(schedule);
        SppScheduleWeekVO oddWeek = new SppScheduleWeekVO();
        SppScheduleWeekVO evenWeek = new SppScheduleWeekVO();
        scheduleVO.setOddWeek(oddWeek);
        scheduleVO.setEvenWeek(evenWeek);

        //        List<FefuScheduleBell> bells = getBells(schedule.getBells().getId());

        Map<ScheduleBellEntry, SppScheduleWeekPeriodsRowVO> oddWeekPeriodsRowMap = Maps.newHashMap();
        Map<ScheduleBellEntry, SppScheduleWeekPeriodsRowVO> evenWeekPeriodsRowMap = Maps.newHashMap();

        DQLSelectBuilder weekRowsBuilder = new DQLSelectBuilder().fromEntity(SppScheduleWeekRow.class, "w");
        weekRowsBuilder.where(eq(property("w", SppScheduleWeekRow.schedule().id()), value(schedule.getId())));

        List<SppScheduleWeekRow> weekRows = weekRowsBuilder.createStatement(getSession()).list();
        for (SppScheduleWeekRow weekRow : weekRows)
        {
            if (weekRow.isEven())
            {
                evenWeekPeriodsRowMap.put(weekRow.getBell(), new SppScheduleWeekPeriodsRowVO(weekRow));
            } else
            {
                oddWeekPeriodsRowMap.put(weekRow.getBell(), new SppScheduleWeekPeriodsRowVO(weekRow));
            }
        }

        fillDays(oddWeekPeriodsRowMap);
        fillDays(evenWeekPeriodsRowMap);

        oddWeek.setWeekPeriodsMap(oddWeekPeriodsRowMap);
        evenWeek.setWeekPeriodsMap(evenWeekPeriodsRowMap);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "p");
        builder.where(eq(property("p", SppSchedulePeriod.weekRow().schedule().id()), value(schedule.getId())));
        List<SppSchedulePeriod> periods = builder.createStatement(getSession()).list();

        for (SppSchedulePeriod period : periods)
        {
            SppSchedulePeriodVO periodVO = new SppSchedulePeriodVO(period);
            if (!period.getWeekRow().isEven())
            {
                addPeriod(oddWeekPeriodsRowMap, periodVO);
            } else
            {
                addPeriod(evenWeekPeriodsRowMap, periodVO);
            }
        }

        sortWeeks(oddWeekPeriodsRowMap, evenWeekPeriodsRowMap);

        return scheduleVO;
    }

    private SppScheduleGroupPrintVO prepareScheduleGroupPrintVO(SppSchedule schedule)
    {
        SppScheduleGroupPrintVO groupPrintVO = new SppScheduleGroupPrintVO(schedule);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "p")
                .where(eq(property("p", SppSchedulePeriod.weekRow().schedule().id()), value(schedule.getId())))
                .order(property("p", SppSchedulePeriod.weekRow().bell()))
                .order(property("p", SppSchedulePeriod.weekRow().even()));

        List<SppSchedulePeriod> periods = builder.createStatement(getSession()).list();
        List<ScheduleBellEntry> usedBells = Lists.newArrayList();

        for (SppSchedulePeriod period : periods)
        {
            ScheduleBellEntry bell = period.getWeekRow().getBell();
            if (!period.isEmpty() && !usedBells.contains(bell))
            {
                usedBells.add(bell);
            }
        }

        for (SppSchedulePeriod period : periods)
        {
            ScheduleBellEntry bell = period.getWeekRow().getBell();
            if (usedBells.contains(bell))
            {
                Boolean isEven = period.getWeekRow().isEven();
                if (SppScheduleGroupPrintVO.MONDAY == period.getDayOfWeek())
                {
                    if (!groupPrintVO.getMonday().containsKey(bell))
                        groupPrintVO.getMonday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getMonday().get(bell).containsKey(isEven))
                        groupPrintVO.getMonday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getMonday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getMonday().get(bell).get(isEven).add(period);
                } else if (SppScheduleGroupPrintVO.TUESDAY == period.getDayOfWeek())
                {
                    if (!groupPrintVO.getTuesday().containsKey(bell))
                        groupPrintVO.getTuesday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getTuesday().get(bell).containsKey(isEven))
                        groupPrintVO.getTuesday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getTuesday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getTuesday().get(bell).get(isEven).add(period);
                } else if (SppScheduleGroupPrintVO.WEDNESDAY == period.getDayOfWeek())
                {
                    if (!groupPrintVO.getWednesday().containsKey(bell))
                        groupPrintVO.getWednesday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getWednesday().get(bell).containsKey(isEven))
                        groupPrintVO.getWednesday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getWednesday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getWednesday().get(bell).get(isEven).add(period);
                } else if (SppScheduleGroupPrintVO.THURSDAY == period.getDayOfWeek())
                {
                    if (!groupPrintVO.getThursday().containsKey(bell))
                        groupPrintVO.getThursday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getThursday().get(bell).containsKey(isEven))
                        groupPrintVO.getThursday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getThursday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getThursday().get(bell).get(isEven).add(period);
                } else if (SppScheduleGroupPrintVO.FRIDAY == period.getDayOfWeek())
                {
                    if (!groupPrintVO.getFriday().containsKey(bell))
                        groupPrintVO.getFriday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getFriday().get(bell).containsKey(isEven))
                        groupPrintVO.getFriday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getFriday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getFriday().get(bell).get(isEven).add(period);
                } else if (SppScheduleGroupPrintVO.SATURDAY == period.getDayOfWeek())
                {
                    if (!groupPrintVO.getSaturday().containsKey(bell))
                        groupPrintVO.getSaturday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getSaturday().get(bell).containsKey(isEven))
                        groupPrintVO.getSaturday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getSaturday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getSaturday().get(bell).get(isEven).add(period);
                } else if (SppScheduleGroupPrintVO.SUNDAY == period.getDayOfWeek())
                {
                    groupPrintVO.setHasSunday(true);
                    if (!groupPrintVO.getSunday().containsKey(bell))
                        groupPrintVO.getSunday().put(bell, Maps.<Boolean, List<SppSchedulePeriod>>newHashMap());
                    if (!groupPrintVO.getSunday().get(bell).containsKey(isEven))
                        groupPrintVO.getSunday().get(bell).put(isEven, Lists.<SppSchedulePeriod>newArrayList());
                    if (!groupPrintVO.getSunday().get(bell).get(isEven).contains(period))
                        groupPrintVO.getSunday().get(bell).get(isEven).add(period);
                }
            }
        }
        int columns = getColumns(groupPrintVO.getMonday(), 0);
        columns = getColumns(groupPrintVO.getTuesday(), columns);
        columns = getColumns(groupPrintVO.getWednesday(), columns);
        columns = getColumns(groupPrintVO.getThursday(), columns);
        columns = getColumns(groupPrintVO.getFriday(), columns);
        columns = getColumns(groupPrintVO.getSaturday(), columns);
        columns = getColumns(groupPrintVO.getSunday(), columns);
        groupPrintVO.setColumnNum(columns);

        DQLSelectBuilder studentCountBuilder = new DQLSelectBuilder().fromEntity(Student.class, "st");
        studentCountBuilder.column(count(property("st", Student.id())));
        studentCountBuilder.where(eq(property("st", Student.group().id()), value(schedule.getGroup().getId())));

        Number studentCount = studentCountBuilder.createStatement(getSession()).uniqueResult();
        groupPrintVO.setStudents(studentCount.intValue());

        return groupPrintVO;
    }

    private int getColumns(Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> day, int i)
    {
        for (ScheduleBellEntry bell : day.keySet())
        {
            for (Boolean isEven : day.get(bell).keySet())
            {
                if (day.get(bell).get(isEven).size() > i) i = day.get(bell).get(isEven).size();
            }
        }
        return i;
    }

    private void sortWeeks(Map<ScheduleBellEntry, SppScheduleWeekPeriodsRowVO> oddWeekPeriodsRowMap, Map<ScheduleBellEntry, SppScheduleWeekPeriodsRowVO> evenWeekPeriodsRowMap)
    {
        Comparator<SppSchedulePeriodVO> comparator = (o1, o2) -> o1.getPeriod().getPeriodNum() - o2.getPeriod().getPeriodNum();

        for (SppScheduleWeekPeriodsRowVO weekPeriodsRowVO : oddWeekPeriodsRowMap.values())
        {
            Collections.sort(weekPeriodsRowVO.getMonday().getPeriods(), comparator);
            Collections.sort(weekPeriodsRowVO.getTuesday().getPeriods(), comparator);
            Collections.sort(weekPeriodsRowVO.getWednesday().getPeriods(), comparator);
            Collections.sort(weekPeriodsRowVO.getThursday().getPeriods(), comparator);
            Collections.sort(weekPeriodsRowVO.getFriday().getPeriods(), comparator);
            Collections.sort(weekPeriodsRowVO.getSaturday().getPeriods(), comparator);
            Collections.sort(weekPeriodsRowVO.getSunday().getPeriods(), comparator);
        }

        for (SppScheduleWeekPeriodsRowVO weekPeriodsRowVO : evenWeekPeriodsRowMap.values())
        {
            Collections.sort(weekPeriodsRowVO.getMonday().getPeriods(), comparator);
            Collections.sort(weekPeriodsRowVO.getTuesday().getPeriods(), comparator);
            Collections.sort(weekPeriodsRowVO.getWednesday().getPeriods(), comparator);
            Collections.sort(weekPeriodsRowVO.getThursday().getPeriods(), comparator);
            Collections.sort(weekPeriodsRowVO.getFriday().getPeriods(), comparator);
            Collections.sort(weekPeriodsRowVO.getSaturday().getPeriods(), comparator);
            Collections.sort(weekPeriodsRowVO.getSunday().getPeriods(), comparator);
        }
    }

    private void fillDays(Map<ScheduleBellEntry, SppScheduleWeekPeriodsRowVO> weekPeriodsRowMap)
    {
        for (SppScheduleWeekPeriodsRowVO weekPeriodsRowVO : weekPeriodsRowMap.values())
        {
            weekPeriodsRowVO.setMonday(getNewSppSchedulePeriodsVO(weekPeriodsRowVO));
            weekPeriodsRowVO.setTuesday(getNewSppSchedulePeriodsVO(weekPeriodsRowVO));
            weekPeriodsRowVO.setWednesday(getNewSppSchedulePeriodsVO(weekPeriodsRowVO));
            weekPeriodsRowVO.setThursday(getNewSppSchedulePeriodsVO(weekPeriodsRowVO));
            weekPeriodsRowVO.setFriday(getNewSppSchedulePeriodsVO(weekPeriodsRowVO));
            weekPeriodsRowVO.setSaturday(getNewSppSchedulePeriodsVO(weekPeriodsRowVO));
            weekPeriodsRowVO.setSunday(getNewSppSchedulePeriodsVO(weekPeriodsRowVO));
        }
    }

    private void addPeriod(Map<ScheduleBellEntry, SppScheduleWeekPeriodsRowVO> weekPeriodsRowMap, SppSchedulePeriodVO periodVO)
    {
        SppSchedulePeriod period = periodVO.getPeriod();
        SppScheduleWeekPeriodsRowVO weekPeriodsRowVO = weekPeriodsRowMap.get(period.getWeekRow().getBell());
        periodVO.setWeekPeriodsRowVO(weekPeriodsRowVO);
        if (DayOfWeek.MONDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getMonday().getPeriods().add(periodVO);
        } else if (DayOfWeek.TUESDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getTuesday().getPeriods().add(periodVO);
        } else if (DayOfWeek.WEDNESDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getWednesday().getPeriods().add(periodVO);
        } else if (DayOfWeek.THURSDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getThursday().getPeriods().add(periodVO);
        } else if (DayOfWeek.FRIDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getFriday().getPeriods().add(periodVO);
        } else if (DayOfWeek.SATURDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getSaturday().getPeriods().add(periodVO);
        } else if (DayOfWeek.SUNDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getSunday().getPeriods().add(periodVO);
        }
    }

    private SppSchedulePeriodsVO getNewSppSchedulePeriodsVO(SppScheduleWeekPeriodsRowVO weekPeriodsRowVO)
    {
        SppSchedulePeriodsVO periodsVO = new SppSchedulePeriodsVO();
        periodsVO.setWeekPeriodsRowVO(weekPeriodsRowVO);
        return periodsVO;
    }

    @Override
    public void saveOrUpdatePeriods(List<SppSchedulePeriodVO> periods)
    {
        for (SppSchedulePeriodVO periodVO : periods)
        {
            SppSchedulePeriod period = periodVO.getPeriodForSave();
            saveOrUpdate(period);
        }
    }

    @Override
    public boolean periodFixExistOnThisDayAndPeriod(SppSchedulePeriodFix periodFix)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppSchedulePeriodFix.class, "f");
        builder.column(count(property("f", SppSchedulePeriodFix.id())));
        builder.where(eq(property("f", SppSchedulePeriodFix.date()), valueDate(periodFix.getDate())));
        builder.where(eq(property("f", SppSchedulePeriodFix.periodNum()), value(periodFix.getPeriodNum())));
        builder.where(eq(property("f", SppSchedulePeriodFix.bell().id()), value(periodFix.getBell().getId())));
        if (periodFix.getId() != null)
            builder.where(ne(property("f", SppSchedulePeriodFix.id()), value(periodFix.getId())));

        final Number count = builder.createStatement(getSession()).uniqueResult();

        return (null != count && count.intValue() > 0);
    }

    @Override
    public void createOrUpdatePeriodFix(SppSchedulePeriodFix periodFix)
    {
        saveOrUpdate(periodFix);

        SppScheduleICal iCalData = periodFix.getSppSchedule().getIcalData();
        if (null != iCalData)
        {
            iCalData.setChanged(true);
            save(iCalData);
        }

        SppScheduleICal icalTeacherData = periodFix.getSppSchedule().getIcalTeacherData();
        if (null != icalTeacherData)
        {
            icalTeacherData.setChanged(true);
            save(icalTeacherData);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean isSppScheduleListTabVisible(Long orgUnitId)
    {
        Debug.begin("isOrgUnitForming");
        try
        {
            Map<Long, Boolean> cache = null;
            if (ContextLocal.isRun())
            {
                String key = "SppScheduleDAO.isSppScheduleListTabVisible";
                cache = (Map<Long, Boolean>) ContextLocal.getRequestAttribute(key);
                if (null == cache)
                {
                    ContextLocal.setRequestAttribute(key, cache = new HashMap<>(4));
                } else
                {
                    Boolean result = cache.get(orgUnitId);
                    if (null != result)
                    {
                        return result;
                    }
                }
            }

            Debug.begin("dql");
            Number count = new DQLSelectBuilder()
                    .fromEntity(OrgUnitToKindRelation.class, "rel")
                    .column(count(property("rel.id")))
                    .where(eq(property(OrgUnitToKindRelation.orgUnit().id().fromAlias("rel")), value(orgUnitId)))
                    .where(eq(property(OrgUnitToKindRelation.orgUnitKind().code().fromAlias("rel")), value(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)))
                    .createStatement(getSession()).uniqueResult();
            Debug.end();

            boolean result = (null != count && count.intValue() > 0);
            if (null != cache)
            {
                cache.put(orgUnitId, result);
            }

            return result;
        } finally
        {
            Debug.end();
        }
    }

    @Override
    public void sentToArchive(Long scheduleId)
    {
        SppSchedule schedule = get(scheduleId);
        schedule.setArchived(true);
        update(schedule);
    }

    @Override
    public void restoreFromArchive(Long scheduleId)
    {
        SppSchedule schedule = get(scheduleId);
        if (schedule.getGroup().isArchival())
            throw new ApplicationException("Нельзя восстановить из архива расписания архивных групп");
        schedule.setArchived(false);
        update(schedule);
    }

    @Override
    public void validate(SppSchedule schedule)
    {
        List<SppSchedulePeriod> periodList = DataAccessServices.dao().getList(SppSchedulePeriod.class, SppSchedulePeriod.weekRow().schedule(), schedule);

        // проверим есть ли у нас непустые пары
        if (periodList.stream().filter(period -> !period.isEmpty()).count() == 0)
            throw new ApplicationException("Для утверждения необходимо, чтобы в расписании была добавлена минимум 1 непустая пара");

        SppScheduleSeason season = schedule.getSeason();
        Date startDate = new DateTime(season.getStartDate()).withTime(0, 0, 0, 0).toDate();
        Date endDate = new DateTime(season.getEndDate()).withTime(23, 59, 59, 1).toDate();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppSchedule.class, "s")
                .where(eq(property("s", SppSchedule.group().id()), value(schedule.getGroup().getId())))
                .where(ne(property("s", SppSchedule.id()), value(schedule.getId())))
                .where(eq(property("s", SppSchedule.approved()), value(Boolean.TRUE)))
                .where(eq(property("s", SppSchedule.archived()), value(Boolean.FALSE)))
                .where(and(
                        le(property("s", SppSchedule.season().startDate()), valueDate(endDate)),
                        ge(property("s", SppSchedule.season().endDate()), valueDate(startDate))));

        List<SppSchedule> schedules = builder.createStatement(getSession()).list();

        if (schedules.size() > 0)
        {
            String scheduleTitles = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(schedules, SppSchedule.title()), ", ");
            throw new ApplicationException("На период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startDate) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate) + " уже есть не архивные утвержденные расписания - " + scheduleTitles);
        }
        updateWarningLog(schedule.getId(), periodList);
    }

    @Override
    public void approve(Long scheduleId)
    {
        SppSchedule schedule = getNotNull(scheduleId);

        validate(schedule);

        SppScheduleStatus status = DataAccessServices.dao().get(SppScheduleStatus.class, SppScheduleStatus.code(), SppScheduleStatusCodes.APPROVED);
        schedule.setStatus(status);
        schedule.setApproved(true);
        update(schedule);
    }


    private void checkAllDisciplineLoads(Long scheduleId, List<SppSchedulePeriod> periodList)
    {
        List<Long> disciplineIds = new ArrayList<>();
        SppSchedule daily = get(scheduleId);
        for (SppSchedulePeriod period : periodList)
        {
            if (period.getSubjectVal() != null)
                disciplineIds.add(period.getSubjectVal().getId());
        }
        //Collection<Long> eventIds = CommonDAO.ids(eventList);

        String checkAlias = "ch";
        String periodAlias = "p";
        String weekRowAlias = "wr";
        String scheduleAlias = "s";
        String regElementLoadAlias = "el";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SppRegElementLoadCheck.class, checkAlias)
                .column(checkAlias)
                .joinEntity(checkAlias, DQLJoinType.left, SppSchedulePeriod.class, periodAlias, and(
                        eq(property(SppRegElementLoadCheck.registryElement().id().fromAlias(checkAlias)), property(SppSchedulePeriod.subjectVal().id().fromAlias(periodAlias))),
                        eq(property(SppRegElementLoadCheck.eppALoadType().id().fromAlias(checkAlias)), property(SppSchedulePeriod.subjectLoadType().id().fromAlias(periodAlias)))))
                .joinPath(DQLJoinType.left, SppSchedulePeriod.weekRow().fromAlias(periodAlias), weekRowAlias)
                .joinPath(DQLJoinType.left, SppScheduleWeekRow.schedule().fromAlias(weekRowAlias), scheduleAlias)
                .joinEntity(checkAlias, DQLJoinType.inner, EppRegistryElementLoad.class, regElementLoadAlias, and(
                        eq(property(SppRegElementLoadCheck.registryElement().id().fromAlias(checkAlias)), property(EppRegistryElementLoad.registryElement().id().fromAlias(regElementLoadAlias))),
                        eq(property(SppRegElementLoadCheck.eppALoadType().id().fromAlias(checkAlias)), property(EppRegistryElementLoad.loadType().id().fromAlias(regElementLoadAlias)))))
                .where(gt(property(EppRegistryElementLoad.load().fromAlias(regElementLoadAlias)), value(0)))
                .where(or(isNull(property(scheduleAlias)), eq(property(scheduleAlias), value(daily))))
                .where(in(property(SppRegElementLoadCheck.registryElement().id().fromAlias(checkAlias)), disciplineIds))
                .where(eq(property(SppRegElementLoadCheck.checkValue().fromAlias(checkAlias)), value(true)))
                .column(periodAlias)
                .distinct();

        List<Object[]> checkList;
        checkList = builder.createStatement(getSession()).list();

        for (Object[] objects : checkList)
        {
            SppRegElementLoadCheck check = (SppRegElementLoadCheck) objects[0];

            if (objects[1] == null)
            {
                addWarningMessage(daily, "ОШИБКА: Отсутствуют " + check.getEppALoadType().getTitle() +
                        " по дисциплине «" + check.getRegistryElement().getTitle() + "»");
            }
        }
    }

    @Override
    public UniplacesClassroomType getDisciplineLectureRoomType(EppRegistryElement discipline, EppALoadType loadType)
    {
        if (discipline != null && loadType != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppRegElementLoadCheck.class, "rc");
            builder.column(property("rc", SppRegElementLoadCheck.uniplacesClassroomType()));
            builder.where(eq(property("rc", SppRegElementLoadCheck.registryElement().id()), value(discipline.getId())));
            builder.where(eq(property("rc", SppRegElementLoadCheck.eppALoadType().id()), value(loadType.getId())));
            return builder.createStatement(getSession()).uniqueResult();
        }
        return null;
    }

    private void checkLectureRoomType(Long scheduleId, List<SppSchedulePeriod> periodList)
    {
        SppSchedule daily = get(scheduleId);
        for (SppSchedulePeriod period : periodList)
        {
            UniplacesClassroomType placePurpose = getDisciplineLectureRoomType(period.getSubjectVal(), period.getSubjectLoadType());
            if (null != placePurpose && null != period.getLectureRoomVal() && !placePurpose.equals(period.getLectureRoomVal().getClassroomType()))
            {
                addWarningMessage(daily, "ПРЕДУПРЕЖДЕНИЕ: тип аудитории в дисциплине «" + period.getSubject() + "»" +
                        " не совпадает с указанным в событии «" + getEventString(period) + "»");
            }
        }
    }

    private String getEventString(SppSchedulePeriod period)
    {
        String dayOfweek = "";
        if (period.getDayOfWeek() == 1)
            dayOfweek = "Понедельник";
        if (period.getDayOfWeek() == 2)
            dayOfweek = "Вторник";
        if (period.getDayOfWeek() == 3)
            dayOfweek = "Среда";
        if (period.getDayOfWeek() == 4)
            dayOfweek = "Четверг";
        if (period.getDayOfWeek() == 5)
            dayOfweek = "Пятница";
        if (period.getDayOfWeek() == 6)
            dayOfweek = "Суббота";
        if (period.getDayOfWeek() == 7)
            dayOfweek = "Воскресенье";
        return dayOfweek + " " + period.getWeekRow().getBell().getTitle();
    }

    public int getGroupStudentCount(Long groupId)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s");
        builder.where(eq(property("s", Student.group().id()), value(groupId)));
        builder.where(eq(property("s", Student.archival()), value(false)));
        builder.where(eq(property("s", Student.status().active()), value(true)));
        Long studentGrpCnt = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        return studentGrpCnt.intValue();
    }


    private void checkLectureRoomCapacity(Long scheduleId, List<SppSchedulePeriod> periodList)
    {
        SppSchedule daily = get(scheduleId);
        int studentGrpCnt = getGroupStudentCount(daily.getGroup().getId());

        for (SppSchedulePeriod period : periodList)
        {
            if (period.getLectureRoomVal() != null && period.getLectureRoomVal().getCapacity() != null &&
                    period.getLectureRoomVal().getCapacity() < studentGrpCnt)
            {
                addWarningMessage(daily, "ОШИБКА: аудитория «" + period.getLectureRoom() + "»" +
                        " в событии «" + getEventString(period) + "»" +
                        " не может вместить всех студентов группы");
            }
        }
    }

    private void checkLectureRoomFree(Long scheduleId, List<SppSchedulePeriod> periodList)
    {
        SppSchedule schedule = getNotNull(scheduleId);
        SppScheduleSeason season = schedule.getSeason();

        for (SppSchedulePeriod period : periodList)
        {
            if (null != period.getLectureRoomVal())
            {
                ScheduleBellEntry bell = period.getWeekRow().getBell();

                UniplacesPlace place = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e")
                        .column(property("e", SppSchedulePeriod.lectureRoomVal())).top(1)
                        .where(eq(property("e", SppSchedulePeriod.lectureRoomVal().id()), value(period.getLectureRoomVal().getId())))
                        .where(eq(property("e", SppSchedulePeriod.weekRow().schedule().season().id()), value(season.getId())))
                        .where(eq(property("e", SppSchedulePeriod.dayOfWeek()), value(period.getDayOfWeek())))
                        .where(eq(property("e", SppSchedulePeriod.weekRow().bell()), value(bell)))
                        .where(ne(property("e", SppSchedulePeriod.weekRow().schedule().id()), value(schedule.getId())))
                        .createStatement(getSession()).uniqueResult();

                List<SppScheduleDailyEvent> dailyEventList = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "e")
                        .where(eq(property("e", SppScheduleDailyEvent.lectureRoomVal().id()), value(period.getLectureRoomVal().getId())))
                        .where(between(property("e", SppScheduleDailyEvent.date()), valueDate(season.getStartDate()), valueDate(season.getEndDate())))
                        .where(eq(property("e", SppScheduleDailyEvent.bell()), value(bell)))
                        .createStatement(getSession()).list();

                List<SppScheduleSessionEvent> sessionEventList = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "e")
                        .where(eq(property("e", SppScheduleSessionEvent.lectureRoomVal().id()), value(period.getLectureRoomVal().getId())))
                        .where(between(property("e", SppScheduleSessionEvent.date()), valueDate(season.getStartDate()), valueDate(season.getEndDate())))
                        .where(eq(property("e", SppScheduleSessionEvent.bell()), value(bell)))
                        .createStatement(getSession()).list();

                if (place != null)
                    addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + period.getLectureRoom() + "»" +
                            " в событии «" + getEventString(period) + "»" +
                            " используется в обычном расписании другой группы");
                for (SppScheduleDailyEvent event : dailyEventList)
                {
                    if (period.getDayOfWeek() == SppScheduleUtil.getDayOfWeek(event.getDate()))
                    {
                        if (event.getLectureRoomVal() != null)
                        {
                            addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + period.getLectureRoom() + "»" +
                                    " в событии «" + getEventString(period) + "»" +
                                    " используется в одном из подневных расписаний");
                        }
                        break;
                    }
                }
                for (SppScheduleSessionEvent event : sessionEventList)
                {
                    if (period.getDayOfWeek() == SppScheduleUtil.getDayOfWeek(event.getDate()))
                    {
                        if (event.getLectureRoomVal() != null)
                        {
                            addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + period.getLectureRoom() + "»" +
                                    " в событии «" + getEventString(period) + "»" +
                                    " используется в одном из расписаний сессий");
                        }
                        break;
                    }
                }
            }
        }
    }

    private void checkTeacherFree(Long scheduleId, List<SppSchedulePeriod> periodList)
    {
        SppSchedule schedule = getNotNull(scheduleId);
        SppScheduleSeason season = schedule.getSeason();

        for (SppSchedulePeriod period : periodList)
        {
            if (null != period.getTeacherVal())
            {
                ScheduleBellEntry bell = period.getWeekRow().getBell();

                PpsEntry ppsEntry = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e")
                        .column(property("e", SppSchedulePeriod.teacherVal())).top(1)
                        .where(eq(property("e", SppSchedulePeriod.teacherVal().id()), value(period.getTeacherVal().getId())))
                        .where(eq(property("e", SppSchedulePeriod.weekRow().schedule().season().id()), value(season.getId())))
                        .where(eq(property("e", SppSchedulePeriod.dayOfWeek()), value(period.getDayOfWeek())))
                        .where(eq(property("e", SppSchedulePeriod.weekRow().bell()), value(bell)))
                        .where(ne(property("e", SppSchedulePeriod.weekRow().schedule().id()), value(schedule.getId())))
                        .createStatement(getSession()).uniqueResult();

                List<SppScheduleDailyEvent> dailyEventList = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "e")
                        .where(eq(property("e", SppScheduleDailyEvent.teacherVal().id()), value(period.getTeacherVal().getId())))
                        .where(between(property("e", SppScheduleDailyEvent.date()), valueDate(season.getStartDate()), valueDate(season.getEndDate())))
                        .where(eq(property("e", SppScheduleDailyEvent.bell()), value(bell)))
                        .createStatement(getSession()).list();

                List<SppScheduleSessionEvent> sessionEventList = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "e")
                        .where(eq(property("e", SppScheduleSessionEvent.teacherVal().id()), value(period.getTeacherVal().getId())))
                        .where(between(property("e", SppScheduleSessionEvent.date()), valueDate(season.getStartDate()), valueDate(season.getEndDate())))
                        .where(eq(property("e", SppScheduleSessionEvent.bell()), value(bell)))
                        .createStatement(getSession()).list();

                if (ppsEntry != null)
                    addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: преподаватель «" + period.getTeacher() + "»" +
                            " в событии «" + getEventString(period) + "»" +
                            " занят в обычном расписании другой группы");
                for (SppScheduleDailyEvent event : dailyEventList)
                {
                    if (period.getDayOfWeek() == SppScheduleUtil.getDayOfWeek(event.getDate()))
                    {
                        if (event.getTeacherVal() != null)
                        {
                            addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: преподаватель «" + period.getTeacher() + "»" +
                                    " в событии «" + getEventString(period) + "»" +
                                    " занят в одном из подневных расписаний");
                        }
                        break;
                    }
                }
                for (SppScheduleSessionEvent event : sessionEventList)
                {
                    if (period.getDayOfWeek() == SppScheduleUtil.getDayOfWeek(event.getDate()))
                    {
                        if (event.getTeacherVal() != null)
                        {
                            addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: преподаватель «" + period.getTeacher() + "»" +
                                    " в событии «" + getEventString(period) + "»" +
                                    " занят в одном из расписаний сессий");
                        }
                        break;
                    }
                }
            }
        }
    }

    private void checkTeacherPreference(Long scheduleId, List<SppSchedulePeriod> periodList)
    {
        SppSchedule schedule = getNotNull(scheduleId);

        for (SppSchedulePeriod period : periodList)
        {
            if (period.getTeacherVal() != null)
            {
                // вытащим предпочтения для преподавателя, соотв. периода, соотв. дня недели, соотв. пары звонкового расписания
                // (будут взяты предпочтения из обеих недель, если между чётными и нечётными есть различие)
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppTeacherPreferenceElement.class, "e")
                        .column(property("e"))
                        .joinPath(DQLJoinType.inner, SppTeacherPreferenceElement.weekRow().fromAlias("e"), "wr")
                        .joinPath(DQLJoinType.inner, SppTeacherPreferenceWeekRow.teacherPreference().fromAlias("wr"), "tp")
                        .where(eq(property("tp", SppTeacherPreferenceList.teacher().id()), value(period.getTeacherVal().getPerson().getId())))
                        .where(eq(property("tp", SppTeacherPreferenceList.sppScheduleSeason().id()), value(schedule.getSeason().getId())))
                        .where(eq(property("e", SppTeacherPreferenceElement.dayOfWeek()), value(period.getDayOfWeek())))
                        .where(eq(property("wr", SppTeacherPreferenceWeekRow.bell().id()), value(period.getWeekRow().getBell().getId())));
                List<SppTeacherPreferenceElement> preferenceList = builder.createStatement(getSession()).list();

                // нам нужно оставить только те предпочтения, которые подходят к добавляемой паре.
                // Предпочтение подходит если:
                // либо 1. в расписании нет различия недель по чётности или есть, но то же место в соседней неделе пустое
                // либо 2. в предпочтении нет различия недель по чётности
                // либо 3. чётность недели в добавляемой паре соответствует чётности в предпочтении

                boolean isDiffEvenAndNeighbourNotEmpty = false;
                if (schedule.isDiffEven())
                {
                    DQLSelectBuilder sBuilder = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "p")
                            .column(count(property("sch", SppSchedule.id())))
                            .joinPath(DQLJoinType.inner, SppSchedulePeriod.weekRow().fromAlias("p"), "wr")
                            .joinPath(DQLJoinType.inner, SppScheduleWeekRow.schedule().fromAlias("wr"), "sch")
                            .where(eq(property("sch", SppSchedule.id()), value(scheduleId)))
                            .where(eq(property("wr", SppScheduleWeekRow.bell().id()), value(period.getWeekRow().getBell().getId())))
                            .where(eq(property("p", SppSchedulePeriod.dayOfWeek()), value(period.getDayOfWeek())))
                            .where(eq(property("wr", SppScheduleWeekRow.even()), value(!period.getWeekRow().isEven())));
                    isDiffEvenAndNeighbourNotEmpty = ((Long) createStatement(sBuilder).uniqueResult()) > 0;
                }

                // отфильтруем предпочтения хитрым фильтром (описанным выше)
                final boolean isDiffEvenOrNeighbourEmpty1 = isDiffEvenAndNeighbourNotEmpty;
                preferenceList = preferenceList.stream()
                        .filter(p -> !isDiffEvenOrNeighbourEmpty1 ||
                                !p.getWeekRow().getTeacherPreference().isDiffEven() ||
                                period.getWeekRow().isEven() == p.getWeekRow().isEven())
                        .collect(Collectors.toList());

                // нам нужны будут все аудитории из этих предпочтений, для этого сначала возьмём все здания
                Set<Long> buildingIds = preferenceList.stream().filter(p -> p.getBuilding() != null)
                        .map(p -> p.getBuilding().getId()).collect(Collectors.toSet());

                // (здание -> аудитории)
                DQLSelectBuilder bBuilder = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "p")
                        .column(property("p", UniplacesPlace.floor().unit().building().id()))
                        .column(property("p", UniplacesPlace.id()))
                        .where(in(property("p", UniplacesPlace.floor().unit().building().id()), buildingIds));
                List<Object[]> bBuilderResult = createStatement(bBuilder).list();
                Map<Long, Set<Long>> building2places = bBuilderResult.stream()
                        .collect(Collectors.groupingBy(el -> (Long) el[0], Collectors.mapping(el -> (Long) el[1], Collectors.toSet())));

                for (SppTeacherPreferenceElement preference : preferenceList)
                {
                    // список предпочтительных аудиторий (или нежелательных, если флаг unwantedTime установлен)
                    Set<Long> places = new HashSet<>();
                    if (preference.getLectureRoom() != null)
                        places.add(preference.getLectureRoom().getId());
                    else if (preference.getBuilding() != null)
                        places.addAll(building2places.get(preference.getBuilding().getId()));

                    boolean isContains = places.contains(period.getLectureRoomVal().getId());

                    // если список пуст и флаг стоит, значит это нежелательное время
                    if (places.isEmpty() && preference.isUnwantedTime())
                    {
                        addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: событие «" + getEventString(period) + "»" +
                                " не подходит по времени для преподавателя «" + period.getTeacher() + "»");
                    }
                    // если аудитория в паре указана, она есть в (непустом) списке и это список нежелательных аудиторий
                    else if (period.getLectureRoomVal() != null && !places.isEmpty() && isContains && preference.isUnwantedTime())
                    {
                        addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + period.getLectureRoom() + "»" +
                                " в событии «" + getEventString(period) + "»" +
                                " является нежелательной для преподавателя «" + period.getTeacher() + "»");
                    }
                    // если аудитория в паре указана, её нет в (непустом) списке, но это список предпочтительных аудиторий
                    else if (period.getLectureRoomVal() != null && !places.isEmpty() && !isContains && !preference.isUnwantedTime())
                    {
                        addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + period.getLectureRoom() + "»" +
                                " в событии «" + getEventString(period) + "»" +
                                " отсутствует в предпочтениях преподавателя «" + period.getTeacher() + "»");
                    }
                }
            }
        }
    }

    private void checkDisciplinePreference(Long scheduleId, List<SppSchedulePeriod> periodList)
    {
        SppSchedule daily = get(scheduleId);
        for (SppSchedulePeriod period : periodList)
        {
            if (period.getSubjectVal() != null)
            {
                DQLSelectBuilder builder1 = new DQLSelectBuilder().fromEntity(SppDisciplinePreferenceList.class, "e");
                builder1.column(property("e", SppDisciplinePreferenceList.teacher()));
                builder1.where(eq(property("e", SppDisciplinePreferenceList.discipline().id()), value(period.getSubjectVal().getId())));
                List<PpsEntry> teacherList = builder1.createStatement(getSession()).list();

                if (!teacherList.isEmpty() && period.getTeacherVal() != null && !teacherList.contains(period.getTeacherVal()))
                {
                    addWarningMessage(daily, "ПРЕДУПРЕЖДЕНИЕ: преподаватель «" + period.getTeacher() + "»" +
                            " в событии «" + getEventString(period) + "»" +
                            " отсутствует в предпочтениях по дисциплине «" + period.getSubject() + "»");
                }

                DQLSelectBuilder builder2 = new DQLSelectBuilder().fromEntity(SppDisciplinePreferenceList.class, "e");
                builder2.column("e");
                builder2.where(eq(property("e", SppDisciplinePreferenceList.discipline().id()), value(period.getSubjectVal().getId())));

                List<SppDisciplinePreferenceList> elementList = builder2.createStatement(getSession()).list();

                List<UniplacesBuilding> buildingList = new ArrayList<>();
                List<UniplacesPlace> lectureRoomList = new ArrayList<>();


                for (SppDisciplinePreferenceList element : elementList)
                {
                    if (element.getBuilding() != null)
                        buildingList.add(element.getBuilding());
                    if (element.getLectureRoom() != null)
                        lectureRoomList.add(element.getLectureRoom());
                }

                DQLSelectBuilder dql1 = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "p");
                dql1.where(in(property("p", UniplacesPlace.floor().unit().building()), buildingList));
                List<UniplacesPlace> addLectureRoomList = dql1.createStatement(getSession()).list();
                lectureRoomList.addAll(addLectureRoomList);

                if (lectureRoomList.size() > 0 && !lectureRoomList.contains(period.getLectureRoomVal()))
                {
                    addWarningMessage(daily, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + period.getLectureRoom() + "»" +
                            " в событии «" + getEventString(period) + "»" +
                            " отсутствует в предпочтениях по дисциплине «" + period.getSubject() + "»");
                }
            }
        }
    }

    private void checkDayDoubleDiscipline(Long scheduleId, List<SppSchedulePeriod> periodList)
    {
        SppSchedule schedule = get(scheduleId);

        // возможно, по некоторым дисциплинам вообще не нужно проверять дубли
        DQLSelectBuilder regElementExtBuilder = new DQLSelectBuilder();
        regElementExtBuilder.fromEntity(SppRegElementExt.class, "elExt").column("elExt");
        regElementExtBuilder.where(eq(property("elExt", SppRegElementExt.checkDuplicates()), value(Boolean.FALSE)));
        regElementExtBuilder.fetchPath(DQLJoinType.inner, SppRegElementExt.registryElement().fromAlias("elExt"), "el");
        Set<EppRegistryElement> disciplinesWithoutCheckSet = getList(regElementExtBuilder)
                .stream().distinct().map(o -> (SppRegElementExt)o).map(SppRegElementExt::getRegistryElement)
                .collect(Collectors.toSet());

        // для нечётной и чётной
        for (boolean even : new boolean[]{false, true})
        {
            for (int i = 1; i <= 7; i++)
            {
                final int dayOfWeek = i;
                List<EppRegistryElement> duplicateDisciplineList = periodList.stream()
                        .filter(p -> p.getDayOfWeek() == dayOfWeek)
                        .filter(p -> p.getWeekRow().isEven() == even)
                        .filter(p -> p.getSubjectVal() != null)
                        .map(SppSchedulePeriod::getSubjectVal)
                        .filter(d -> !disciplinesWithoutCheckSet.contains(d))
                        .collect(Collectors.groupingBy(d -> d, Collectors.counting()))
                        .entrySet().stream()
                        .filter(p -> p.getValue() > 1)
                        .map(Map.Entry::getKey)
                        .collect(Collectors.toList());
                for (EppRegistryElement discipline : duplicateDisciplineList)
                {
                    addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: дисциплина «" + discipline.getTitle() + "»" +
                            " присутствует в событии " + String.valueOf(i) +
                            " дня " + (even ? "чётной" : "нечётной") + " недели несколько раз");
                }
            }
        }
    }

    private void addWarningMessage(SppSchedule schedule, String message)
    {
        SppScheduleWarningLog log = new SppScheduleWarningLog();
        log.setWarningMessage(message);
        log.setSppSchedule(schedule);
        save(log);
    }

    private void updateWarningLog(Long scheduleId, List<SppSchedulePeriod> periodList)
    {
        DQLDeleteBuilder dBuilder = new DQLDeleteBuilder(SppScheduleWarningLog.class);
        dBuilder.where(eq(property(SppScheduleWarningLog.sppSchedule().id()), value(scheduleId)));
        dBuilder.createStatement(getSession()).execute();

        checkAllDisciplineLoads(scheduleId, periodList);
        checkDayDoubleDiscipline(scheduleId, periodList);
        checkLectureRoomType(scheduleId, periodList);
        checkLectureRoomCapacity(scheduleId, periodList);
        checkLectureRoomFree(scheduleId, periodList);
        checkTeacherFree(scheduleId, periodList);
        checkTeacherPreference(scheduleId, periodList);
        checkDisciplinePreference(scheduleId, periodList);
    }

    @Override
    public void sentToFormation(Long scheduleId)
    {
        SppSchedule schedule = get(scheduleId);
        SppScheduleStatus status = DataAccessServices.dao().get(SppScheduleStatus.class, SppScheduleStatus.code(), SppScheduleStatusCodes.FORMATION);
        schedule.setStatus(status);
        schedule.setApproved(false);
        update(schedule);
    }

    @NotNull
    @Override
    public EppWorkPlan getWorkPlanByDiscipline(@NotNull EppRegistryDiscipline discipline)
    {
        EppRegistryElementPart elementPart = getList(EppRegistryElementPart.class, EppRegistryElementPart.registryElement(), discipline).get(0);
        final List<EppWorkPlanRegistryElementRow> elementRowList = getList(EppWorkPlanRegistryElementRow.class, EppWorkPlanRegistryElementRow.registryElementPart(), elementPart);
        if (elementRowList.isEmpty()) return null;

        EppWorkPlanRegistryElementRow elementRow = elementRowList.get(0);
        return elementRow.getWorkPlan().getWorkPlan();
    }

    /**
     * Возвращает период по следующим данным:
     * @param scheduleId id расписания
     * @param date дата проведения
     * @param bellId id пары звонкового расписания
     * @param number номер подгруппы
     */
    @Override
    public SppSchedulePeriod getPeriod(Long scheduleId, Date date, Long bellId, int number)
    {
        SppSchedulePeriod period = null;
        if (scheduleId != null && date != null && bellId != null)
        {
            int dayOfWeek = SppScheduleUtil.getDayOfWeek(date);
            DQLSelectBuilder builder = new DQLSelectBuilder();
            builder.fromEntity(SppSchedulePeriod.class, "sp").column("sp");
            // в этом расписании, этого дня, этой звонковой пары
            builder.where(eq(property("sp", SppSchedulePeriod.weekRow().schedule().id()), value(scheduleId)));
            builder.where(eq(property("sp", SppSchedulePeriod.dayOfWeek()), value(dayOfWeek)));
            builder.where(eq(property("sp", SppSchedulePeriod.weekRow().bell().id()), value(bellId)));
            builder.where(eq(property("sp", SppSchedulePeriod.periodNum()), value(number)));
            // если день попал на период проведения занятия
            builder.where(or(
                    and(
                            isNotNull(property("sp", SppSchedulePeriod.startDate())),
                            le(property("sp", SppSchedulePeriod.startDate()), valueDate(date))),
                    and(
                            isNull(property("sp", SppSchedulePeriod.startDate())),
                            le(property("sp", SppSchedulePeriod.weekRow().schedule().season().startDate()), valueDate(date)))));
            builder.where(or(
                    and(
                            isNotNull(property("sp", SppSchedulePeriod.endDate())),
                            ge(property("sp", SppSchedulePeriod.endDate()), valueDate(date))),
                    and(
                            isNull(property("sp", SppSchedulePeriod.endDate())),
                            ge(property("sp", SppSchedulePeriod.weekRow().schedule().season().endDate()), valueDate(date)))));
            // ==================== учёт неявного копирования ====================
            // вычислим чётность:
            SppScheduleSeason season = getProperty(SppSchedule.class, SppSchedule.L_SEASON, SppSchedule.P_ID, scheduleId);
            boolean even = season.isStartFromEven();
            if (SppScheduleUtil.getDifferenceInWeeks(season.getStartDate(), date) % 2 != 0)
                even = !even;
            builder.where(or(
                    // либо нет разделения по чётности, либо чётность совпадает,
                    // либо в этом месте ничего нет и пара скопирована из соседней недели
                    eq(property("sp", SppSchedulePeriod.weekRow().schedule().diffEven()), value(Boolean.FALSE)),
                    eq(property("sp", SppSchedulePeriod.weekRow().even()), value(even)),
                    notExists(new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "sp1")
                            .where(eq(property("sp1", SppSchedulePeriod.weekRow().schedule().id()), value(scheduleId)))
                            .where(eq(property("sp1", SppSchedulePeriod.dayOfWeek()), value(dayOfWeek)))
                            .where(eq(property("sp1", SppSchedulePeriod.weekRow().bell().id()), value(bellId)))
                            .where(eq(property("sp1", SppSchedulePeriod.periodNum()), value(number)))
                            .where(eq(property("sp1", SppSchedulePeriod.weekRow().even()), value(even)))
                            .where(or(
                                    and(
                                            isNotNull(property("sp1", SppSchedulePeriod.startDate())),
                                            le(property("sp1", SppSchedulePeriod.startDate()), valueDate(date))),
                                    and(
                                            isNull(property("sp1", SppSchedulePeriod.startDate())),
                                            le(property("sp1", SppSchedulePeriod.weekRow().schedule().season().startDate()), valueDate(date)))))
                            .where(or(
                                    and(
                                            isNotNull(property("sp1", SppSchedulePeriod.endDate())),
                                            ge(property("sp1", SppSchedulePeriod.endDate()), valueDate(date))),
                                    and(
                                            isNull(property("sp1", SppSchedulePeriod.endDate())),
                                            ge(property("sp1", SppSchedulePeriod.weekRow().schedule().season().endDate()), valueDate(date)))))
                            .buildQuery())));
            // ===================================================================
            List<SppSchedulePeriod> periodList = getList(builder);
            if (periodList != null && !periodList.isEmpty())
                period = periodList.get(0);
        }
        return period;
    }
}
