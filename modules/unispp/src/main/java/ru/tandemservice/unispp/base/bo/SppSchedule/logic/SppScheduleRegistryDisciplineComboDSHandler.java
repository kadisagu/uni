/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Хэндлер для дисциплин (используется в списках расписаний уч.процесса и в списке расписаний преподавателей)
 * todo возможно, слить с SppScheduleSubjectComboDSHandler
 * @author Igor Belanov
 * @since 10.01.2017
 */
public class SppScheduleRegistryDisciplineComboDSHandler extends DefaultComboDataSourceHandler
{
    // properties
    public static final String PROP_ORG_UNIT_IDS = "orgUnitIds";
    public static final String PROP_YEAR_PART_ID = "yearPartId";

    public SppScheduleRegistryDisciplineComboDSHandler(String ownerId)
    {
        super(ownerId, EppRegistryDiscipline.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);
        List<Long> orgUnitIds = ep.context.get(PROP_ORG_UNIT_IDS);
//        List<Long> courses = ep.context.get("courses");

        Long yearPartId =  ep.context.get(PROP_YEAR_PART_ID);
        Long yearId = null;
        Long partId = null;
        if (yearPartId != null)
        {
            DQLSelectBuilder yearPartBuilder = new DQLSelectBuilder().fromEntity(EppYearPart.class, "yp")
                    .where(eq(property("yp", EppYearPart.id()), value(yearPartId)))
                    .fetchPath(DQLJoinType.inner, EppYearPart.year().educationYear().fromAlias("yp"), "year")
                    .fetchPath(DQLJoinType.inner, EppYearPart.part().fromAlias("yp"), "part");
            EppYearPart yearPart = ((EppYearPart) DataAccessServices.dao().getList(yearPartBuilder).get(0));
            yearId = yearPart.getYear().getEducationYear().getId();
            partId = yearPart.getPart().getId();
        }


        DQLSelectBuilder kindRelationsBuilder= new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                .where(in(property("rel", OrgUnitToKindRelation.orgUnit().id()), orgUnitIds));
        List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

        Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

        IDQLExpression condition = nothing(); // false

        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
            condition = or(condition, in(property("g", Group.educationOrgUnit().educationLevelHighSchool().orgUnit().id()), orgUnitIds));
        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
            condition = or(condition, in(property("g", Group.educationOrgUnit().formativeOrgUnit().id()), orgUnitIds));
        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
            condition = or(condition, in(property("g", Group.educationOrgUnit().territorialOrgUnit().id()), orgUnitIds));

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g");
        subBuilder.column("g.id");
        subBuilder.where(condition);

        List<Long> groupIds = createStatement(subBuilder).list();

        DQLSelectBuilder studentBuilder = new DQLSelectBuilder().fromEntity(Student.class, "st");
        studentBuilder.column(property("st", Student.id()));
        studentBuilder.where(eq(property("st", Student.archival()), value(false)));
        studentBuilder.where(in(property("st", Student.group().id()), groupIds));


        DQLSelectBuilder eppStudent2WorkPlanBuilder = new DQLSelectBuilder().fromEntity(EppStudent2WorkPlan.class, "stwp");
        eppStudent2WorkPlanBuilder.column(property("stwp", EppStudent2WorkPlan.workPlan().id()));
        eppStudent2WorkPlanBuilder.where(in(property("stwp", EppStudent2WorkPlan.studentEduPlanVersion().student().id()), studentBuilder.buildQuery()));
        eppStudent2WorkPlanBuilder.distinct();


        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppWorkPlanRegistryElementRow.class, "r");
        builder.column(property("r", EppWorkPlanRegistryElementRow.registryElementPart().registryElement()));
        builder.where(exists(new DQLSelectBuilder().fromEntity(EppRegistryDiscipline.class, "d")
                .where(eq(property("r", EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id()), property("d", EppRegistryDiscipline.id())))
                .buildQuery()));
        builder.where(exists(
                new DQLSelectBuilder().fromEntity(EppWorkPlan.class, "wp")
                        .where(in(property("wp", EppWorkPlan.id()), eppStudent2WorkPlanBuilder.buildQuery()))
                        .where(eq(property("wp", EppWorkPlan.year().educationYear().id()), value(yearId)))
                        .where(eq(property("wp", EppWorkPlan.cachedGridTerm().part().id()), value(partId)))
                        .where(eq(property("r", EppWorkPlanRegistryElementRow.workPlan().id()), property("wp", EppWorkPlan.id())))
                        .buildQuery()));


        ep.dqlBuilder.where(DQLExpressions.exists(builder.buildQuery()));
    }
}
