/* $Id: SppScheduleDailyViewUI.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.LearningProcessAllEventsList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleAllEventsDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleGroupComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleRegistryDisciplineComboDSHandler;
import ru.tandemservice.unispp.base.vo.SppScheduleEventInDayVO;

import java.util.Date;
import java.util.List;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
public class SppScheduleLearningProcessAllEventsListUI extends UIPresenter
{
    private Date _dateFrom;
    private Date _dateTo;

    private boolean _showOriginalData;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleLearningProcessAllEventsList.SCHEDULE_EVENTS_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleAllEventsDSHandler.PROP_SHOW_ORIGINAL_DATA, _showOriginalData);

            dataSource.put(SppScheduleAllEventsDSHandler.PROP_DATE_FROM, getDateFrom());
            dataSource.put(SppScheduleAllEventsDSHandler.PROP_DATE_TO, getDateTo());

            if (getSettings().get("yearPart") != null)
                dataSource.put(SppScheduleAllEventsDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
            if (getSettings().get("orgUnitIds") != null)
                dataSource.put(SppScheduleAllEventsDSHandler.PROP_ORG_UNIT_IDS, CommonDAO.ids(getSettings().get("orgUnitIds"))); // todo rename
            if (getSettings().get("eduLevels") != null)
                dataSource.put(SppScheduleAllEventsDSHandler.PROP_EDU_LEVEL_IDS, CommonDAO.ids(getSettings().get("eduLevels")));
            if (getSettings().get("groups") != null)
                dataSource.put(SppScheduleAllEventsDSHandler.PROP_GROUP_IDS, CommonDAO.ids(getSettings().get("groups")));
            if (getSettings().get("subjects") != null)
                dataSource.put(SppScheduleAllEventsDSHandler.PROP_SUBJECT_IDS, CommonDAO.ids(getSettings().get("subjects")));
            if (getSettings().get("teachers") != null)
                dataSource.put(SppScheduleAllEventsDSHandler.PROP_TEACHER_IDS, CommonDAO.ids(getSettings().get("teachers")));
            if (getSettings().get("places") != null)
                dataSource.put(SppScheduleAllEventsDSHandler.PROP_PLACES_IDS, CommonDAO.ids(getSettings().get("places")));
            if (getSettings().get("statuses") != null)
                dataSource.put(SppScheduleAllEventsDSHandler.PROP_STATUSES, getSettings().get("statuses"));
        }
        if(SppScheduleLearningProcessAllEventsList.EDU_LEVEL_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "orgUnitIds"));
        }
        if(SppScheduleLearningProcessAllEventsList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "eduLevels", "orgUnitIds"));
            dataSource.put(SppScheduleGroupComboDSHandler.PROP_GROUPS_NOT_IN_ARCHIVE, Boolean.TRUE);
        }
        if(SppScheduleLearningProcessAllEventsList.SUBJECT_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleRegistryDisciplineComboDSHandler.PROP_ORG_UNIT_IDS, _uiSettings.get("orgUnitIds"));
            dataSource.put(SppScheduleRegistryDisciplineComboDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
        }
        if (SppScheduleLearningProcessAllEventsList.STATUSES_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleLearningProcessAllEventsList.PROP_IS_SHOW_ORIGINAL_DATA, _showOriginalData);
        }
    }

    // listeners
    public void onChangeShowOriginalData()
    {
        // при смене чекбокса необходимо убрать статусы
        List<IdentifiableWrapper> statuses = getSettings().get("statuses");
        if (statuses.contains(SppScheduleEventInDayVO.STATUS_ADDED))
            statuses.remove(SppScheduleEventInDayVO.STATUS_ADDED);
    }

    // getters & setters
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        _dateTo = dateTo;
    }

    public boolean isShowOriginalData()
    {
        return _showOriginalData;
    }

    public void setShowOriginalData(boolean showOriginalData)
    {
        _showOriginalData = showOriginalData;
    }
}
