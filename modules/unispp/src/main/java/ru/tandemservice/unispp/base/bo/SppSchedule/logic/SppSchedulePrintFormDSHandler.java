/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import com.google.common.collect.Lists;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unispp.base.entity.SppSchedulePrintForm;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 9/27/13
 */
public class SppSchedulePrintFormDSHandler extends DefaultSearchDataSourceHandler
{
    // properties
    public static final String PROP_YEAR_PART_ID = "yearPartId";

    public SppSchedulePrintFormDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long yearPartId = context.get(PROP_YEAR_PART_ID);

        Long orgUnitId = context.get("orgUnitId");

        List<Long> orgUnitIds = context.get("orgUnitIds");

        List<Long> groups = context.get("groups");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppSchedulePrintForm.class, "pf");
        builder.fetchPath(DQLJoinType.inner, SppSchedulePrintForm.formativeOrgUnit().fromAlias("pf"), "formou");
        builder.fetchPath(DQLJoinType.inner, SppSchedulePrintForm.season().fromAlias("pf"), "season");
        builder.column("pf");
        if (orgUnitId != null)
            builder.where(eq(property("pf", SppSchedulePrintForm.createOU().id()), value(orgUnitId)));

        if (orgUnitIds != null && !orgUnitIds.isEmpty())
            builder.where(in(property("pf", SppSchedulePrintForm.createOU().id()), orgUnitIds));

        if (null != groups && !groups.isEmpty())
        {
            DQLSelectBuilder groupBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g");
            groupBuilder.column(DQLFunctions.upper(property("g", Group.title())));
            groupBuilder.where(in(property("g", Group.id()), groups));
            List<String> groupsTitles = createStatement(groupBuilder).list();
            List<IDQLExpression> expressions = groupsTitles.stream().map(groupTitle -> like(DQLFunctions.upper(property("pf", SppSchedulePrintForm.groups())),
                    value(CoreStringUtils.escapeLike(groupTitle, true)))).collect(Collectors.toList());
            builder.where(or(expressions.toArray(new IDQLExpression[expressions.size()])));
        }

        if (yearPartId != null)
            builder.where(eq(property("pf", SppSchedulePrintForm.season().eppYearPart().id()), value(yearPartId)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
    }
}
