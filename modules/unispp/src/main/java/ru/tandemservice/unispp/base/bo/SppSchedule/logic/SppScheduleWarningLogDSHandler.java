/* $Id: */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unispp.base.entity.SppScheduleWarningLog;

/**
 * @author vnekrasov
 * @since 7/14/15
 */
public class SppScheduleWarningLogDSHandler extends DefaultSearchDataSourceHandler
{
    public SppScheduleWarningLogDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long scheduleId = context.get("scheduleId");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleWarningLog.class, "w");

        if(null != scheduleId)
            builder.where(DQLExpressions.eq(DQLExpressions.property("w", SppScheduleWarningLog.sppSchedule().id()), DQLExpressions.value(scheduleId)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order().build();
    }
}
