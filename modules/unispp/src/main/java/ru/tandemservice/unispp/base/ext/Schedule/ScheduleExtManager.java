/* $Id$ */
package ru.tandemservice.unispp.base.ext.Schedule;

import org.apache.commons.collections15.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unischedule.base.bo.Schedule.ScheduleManager;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.rule.condition.schedulebell.SppCanEditBellScheduleEntriesPredicate;

/**
 * @author Alexey Lopatin
 * @since 28.07.2016
 */
@Configuration
public class ScheduleExtManager extends BusinessObjectExtensionManager
{
    public static final String UNI_TRAINING_CAN_EDIT_BELL_SCHEDULE_ENTRIES_COND = SppCanEditBellScheduleEntriesPredicate.class.getSimpleName();

    @Autowired
    private ScheduleManager _scheduleManager;

    @Bean
    public ItemListExtension<Predicate<ScheduleBell>> canEditBellScheduleEntriesPredicatesExtension()
    {
        return itemListExtension(_scheduleManager.canEditBellScheduleEntriesPredicatesExtPoint())
                .add(UNI_TRAINING_CAN_EDIT_BELL_SCHEDULE_ENTRIES_COND, new SppCanEditBellScheduleEntriesPredicate()).create();
    }
}