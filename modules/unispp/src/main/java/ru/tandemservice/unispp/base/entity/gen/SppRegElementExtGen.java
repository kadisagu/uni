package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unispp.base.entity.SppRegElementExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Доп. характеристики элемента реестра
 *
 * Чекбокс "Проверять, что разные типы аудиторных занятий проводятся в разные дни недели"
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppRegElementExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppRegElementExt";
    public static final String ENTITY_NAME = "sppRegElementExt";
    public static final int VERSION_HASH = 1853842921;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_ELEMENT = "registryElement";
    public static final String P_CHECK_DUPLICATES = "checkDuplicates";

    private EppRegistryElement _registryElement;     // Элемент реестра
    private boolean _checkDuplicates = true; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getRegistryElement()
    {
        return _registryElement;
    }

    /**
     * @param registryElement Элемент реестра. Свойство не может быть null.
     */
    public void setRegistryElement(EppRegistryElement registryElement)
    {
        dirty(_registryElement, registryElement);
        _registryElement = registryElement;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isCheckDuplicates()
    {
        return _checkDuplicates;
    }

    /**
     * @param checkDuplicates  Свойство не может быть null.
     */
    public void setCheckDuplicates(boolean checkDuplicates)
    {
        dirty(_checkDuplicates, checkDuplicates);
        _checkDuplicates = checkDuplicates;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppRegElementExtGen)
        {
            setRegistryElement(((SppRegElementExt)another).getRegistryElement());
            setCheckDuplicates(((SppRegElementExt)another).isCheckDuplicates());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppRegElementExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppRegElementExt.class;
        }

        public T newInstance()
        {
            return (T) new SppRegElementExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "registryElement":
                    return obj.getRegistryElement();
                case "checkDuplicates":
                    return obj.isCheckDuplicates();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "registryElement":
                    obj.setRegistryElement((EppRegistryElement) value);
                    return;
                case "checkDuplicates":
                    obj.setCheckDuplicates((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "registryElement":
                        return true;
                case "checkDuplicates":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "registryElement":
                    return true;
                case "checkDuplicates":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "registryElement":
                    return EppRegistryElement.class;
                case "checkDuplicates":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppRegElementExt> _dslPath = new Path<SppRegElementExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppRegElementExt");
    }
            

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppRegElementExt#getRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> registryElement()
    {
        return _dslPath.registryElement();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppRegElementExt#isCheckDuplicates()
     */
    public static PropertyPath<Boolean> checkDuplicates()
    {
        return _dslPath.checkDuplicates();
    }

    public static class Path<E extends SppRegElementExt> extends EntityPath<E>
    {
        private EppRegistryElement.Path<EppRegistryElement> _registryElement;
        private PropertyPath<Boolean> _checkDuplicates;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppRegElementExt#getRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> registryElement()
        {
            if(_registryElement == null )
                _registryElement = new EppRegistryElement.Path<EppRegistryElement>(L_REGISTRY_ELEMENT, this);
            return _registryElement;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppRegElementExt#isCheckDuplicates()
     */
        public PropertyPath<Boolean> checkDuplicates()
        {
            if(_checkDuplicates == null )
                _checkDuplicates = new PropertyPath<Boolean>(SppRegElementExtGen.P_CHECK_DUPLICATES, this);
            return _checkDuplicates;
        }

        public Class getEntityClass()
        {
            return SppRegElementExt.class;
        }

        public String getEntityName()
        {
            return "sppRegElementExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
