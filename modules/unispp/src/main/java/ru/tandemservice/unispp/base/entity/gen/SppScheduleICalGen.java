package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unispp.base.entity.SppScheduleICal;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расписание для ICal
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppScheduleICalGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppScheduleICal";
    public static final String ENTITY_NAME = "sppScheduleICal";
    public static final int VERSION_HASH = -56394522;
    private static IEntityMeta ENTITY_META;

    public static final String P_SEQUENCE = "sequence";
    public static final String P_XML = "xml";
    public static final String P_CHANGED = "changed";

    private Integer _sequence;     // Версия
    private String _xml;     // Данные по ICal (XML)
    private boolean _changed;     // Изменено

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Версия.
     */
    public Integer getSequence()
    {
        return _sequence;
    }

    /**
     * @param sequence Версия.
     */
    public void setSequence(Integer sequence)
    {
        dirty(_sequence, sequence);
        _sequence = sequence;
    }

    /**
     * @return Данные по ICal (XML).
     */
    public String getXml()
    {
        return _xml;
    }

    /**
     * @param xml Данные по ICal (XML).
     */
    public void setXml(String xml)
    {
        dirty(_xml, xml);
        _xml = xml;
    }

    /**
     * @return Изменено. Свойство не может быть null.
     */
    @NotNull
    public boolean isChanged()
    {
        return _changed;
    }

    /**
     * @param changed Изменено. Свойство не может быть null.
     */
    public void setChanged(boolean changed)
    {
        dirty(_changed, changed);
        _changed = changed;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppScheduleICalGen)
        {
            setSequence(((SppScheduleICal)another).getSequence());
            setXml(((SppScheduleICal)another).getXml());
            setChanged(((SppScheduleICal)another).isChanged());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppScheduleICalGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppScheduleICal.class;
        }

        public T newInstance()
        {
            return (T) new SppScheduleICal();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sequence":
                    return obj.getSequence();
                case "xml":
                    return obj.getXml();
                case "changed":
                    return obj.isChanged();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sequence":
                    obj.setSequence((Integer) value);
                    return;
                case "xml":
                    obj.setXml((String) value);
                    return;
                case "changed":
                    obj.setChanged((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sequence":
                        return true;
                case "xml":
                        return true;
                case "changed":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sequence":
                    return true;
                case "xml":
                    return true;
                case "changed":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sequence":
                    return Integer.class;
                case "xml":
                    return String.class;
                case "changed":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppScheduleICal> _dslPath = new Path<SppScheduleICal>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppScheduleICal");
    }
            

    /**
     * @return Версия.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleICal#getSequence()
     */
    public static PropertyPath<Integer> sequence()
    {
        return _dslPath.sequence();
    }

    /**
     * @return Данные по ICal (XML).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleICal#getXml()
     */
    public static PropertyPath<String> xml()
    {
        return _dslPath.xml();
    }

    /**
     * @return Изменено. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleICal#isChanged()
     */
    public static PropertyPath<Boolean> changed()
    {
        return _dslPath.changed();
    }

    public static class Path<E extends SppScheduleICal> extends EntityPath<E>
    {
        private PropertyPath<Integer> _sequence;
        private PropertyPath<String> _xml;
        private PropertyPath<Boolean> _changed;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Версия.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleICal#getSequence()
     */
        public PropertyPath<Integer> sequence()
        {
            if(_sequence == null )
                _sequence = new PropertyPath<Integer>(SppScheduleICalGen.P_SEQUENCE, this);
            return _sequence;
        }

    /**
     * @return Данные по ICal (XML).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleICal#getXml()
     */
        public PropertyPath<String> xml()
        {
            if(_xml == null )
                _xml = new PropertyPath<String>(SppScheduleICalGen.P_XML, this);
            return _xml;
        }

    /**
     * @return Изменено. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleICal#isChanged()
     */
        public PropertyPath<Boolean> changed()
        {
            if(_changed == null )
                _changed = new PropertyPath<Boolean>(SppScheduleICalGen.P_CHANGED, this);
            return _changed;
        }

        public Class getEntityClass()
        {
            return SppScheduleICal.class;
        }

        public String getEntityName()
        {
            return "sppScheduleICal";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
