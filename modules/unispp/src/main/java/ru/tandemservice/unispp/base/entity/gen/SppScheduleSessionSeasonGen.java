package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Период расписания сессии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppScheduleSessionSeasonGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason";
    public static final String ENTITY_NAME = "sppScheduleSessionSeason";
    public static final int VERSION_HASH = -128329062;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String L_EPP_YEAR_PART = "eppYearPart";
    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_TITLE_WITH_TIME = "titleWithTime";

    private String _title;     // Название
    private EppYearPart _eppYearPart;     // Часть года (в учебном году)
    private Date _startDate;     // Дата начала
    private Date _endDate;     // Дата окончания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Часть года (в учебном году). Свойство не может быть null.
     */
    @NotNull
    public EppYearPart getEppYearPart()
    {
        return _eppYearPart;
    }

    /**
     * @param eppYearPart Часть года (в учебном году). Свойство не может быть null.
     */
    public void setEppYearPart(EppYearPart eppYearPart)
    {
        dirty(_eppYearPart, eppYearPart);
        _eppYearPart = eppYearPart;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала. Свойство не может быть null.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppScheduleSessionSeasonGen)
        {
            setTitle(((SppScheduleSessionSeason)another).getTitle());
            setEppYearPart(((SppScheduleSessionSeason)another).getEppYearPart());
            setStartDate(((SppScheduleSessionSeason)another).getStartDate());
            setEndDate(((SppScheduleSessionSeason)another).getEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppScheduleSessionSeasonGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppScheduleSessionSeason.class;
        }

        public T newInstance()
        {
            return (T) new SppScheduleSessionSeason();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "eppYearPart":
                    return obj.getEppYearPart();
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "eppYearPart":
                    obj.setEppYearPart((EppYearPart) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "eppYearPart":
                        return true;
                case "startDate":
                        return true;
                case "endDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "eppYearPart":
                    return true;
                case "startDate":
                    return true;
                case "endDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "eppYearPart":
                    return EppYearPart.class;
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppScheduleSessionSeason> _dslPath = new Path<SppScheduleSessionSeason>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppScheduleSessionSeason");
    }
            

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Часть года (в учебном году). Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason#getEppYearPart()
     */
    public static EppYearPart.Path<EppYearPart> eppYearPart()
    {
        return _dslPath.eppYearPart();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason#getTitleWithTime()
     */
    public static SupportedPropertyPath<String> titleWithTime()
    {
        return _dslPath.titleWithTime();
    }

    public static class Path<E extends SppScheduleSessionSeason> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private EppYearPart.Path<EppYearPart> _eppYearPart;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;
        private SupportedPropertyPath<String> _titleWithTime;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(SppScheduleSessionSeasonGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Часть года (в учебном году). Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason#getEppYearPart()
     */
        public EppYearPart.Path<EppYearPart> eppYearPart()
        {
            if(_eppYearPart == null )
                _eppYearPart = new EppYearPart.Path<EppYearPart>(L_EPP_YEAR_PART, this);
            return _eppYearPart;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(SppScheduleSessionSeasonGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(SppScheduleSessionSeasonGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason#getTitleWithTime()
     */
        public SupportedPropertyPath<String> titleWithTime()
        {
            if(_titleWithTime == null )
                _titleWithTime = new SupportedPropertyPath<String>(SppScheduleSessionSeasonGen.P_TITLE_WITH_TIME, this);
            return _titleWithTime;
        }

        public Class getEntityClass()
        {
            return SppScheduleSessionSeason.class;
        }

        public String getEntityName()
        {
            return "sppScheduleSessionSeason";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleWithTime();
}
