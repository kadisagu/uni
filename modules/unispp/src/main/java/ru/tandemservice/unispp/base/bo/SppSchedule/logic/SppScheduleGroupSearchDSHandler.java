/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 08.11.2016
 */
public class SppScheduleGroupSearchDSHandler extends DefaultSearchDataSourceHandler
{
    // properties
    public static String PROP_TITLE = "title";
    public static String PROP_ORG_UNIT_ID = "orgUnitId";
    public static String PROP_ORG_UNITS = "orgUnits";
    public static String PROP_GROUPS_NOT_IN_ARCHIVE = "groupsNotInArchive";
    public static String PROP_COURSES = "courses";
    public static String PROP_EDU_LEVELS = "eduLevels";
    public static String PROP_DEVELOP_FORMS = "developForms";
    public static String PROP_DEVELOP_CONDITIONS = "developConditions";
    public static String PROP_DEVELOP_TECHS = "developTechs";
    public static String PROP_DEVELOP_PERIODS = "developPeriods";

    public SppScheduleGroupSearchDSHandler(String ownerId)
    {
        super(ownerId, Group.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long orgUnitId = context.get(PROP_ORG_UNIT_ID);
        List<OrgUnit> orgUnits = orgUnitId == null ? context.get(PROP_ORG_UNITS) :
                Collections.singletonList(DataAccessServices.dao().get(OrgUnit.class, orgUnitId));

        Boolean groupsNotInArchive = context.getBoolean(PROP_GROUPS_NOT_IN_ARCHIVE, Boolean.FALSE);
        List<Course> courses = context.get(PROP_COURSES);
        List<EducationLevelsHighSchool> eduLevels = context.get(PROP_EDU_LEVELS);
        List<DevelopForm> developForms = context.get(PROP_DEVELOP_FORMS);
        List<DevelopCondition> developConditions = context.get(PROP_DEVELOP_CONDITIONS);
        List<DevelopTech> developTechs = context.get(PROP_DEVELOP_TECHS);
        List<DevelopPeriod> developPeriods = context.get(PROP_DEVELOP_PERIODS);

        String filter = context.get(PROP_TITLE);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, "g").column("g");

        if (null != orgUnits && !orgUnits.isEmpty())
        {
            DQLSelectBuilder kindRelationsBuilder= new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                    .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                    .where(in(property("rel", OrgUnitToKindRelation.orgUnit()), orgUnits));
            List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

            Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                    .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

            IDQLExpression condition = nothing(); // false
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                condition = or(condition, in(property("g", Group.educationOrgUnit().educationLevelHighSchool().orgUnit()), orgUnits));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                condition = or(condition, in(property("g", Group.educationOrgUnit().formativeOrgUnit()), orgUnits));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                condition = or(condition, in(property("g", Group.educationOrgUnit().territorialOrgUnit()), orgUnits));

            builder.where(condition);
        }

        if (groupsNotInArchive)
            builder.where(eq(property("g", Group.archival()), value(Boolean.FALSE)));
        if (null != courses && !courses.isEmpty())
            builder.where(in(property("g", Group.course()), courses));
        if (null != eduLevels && !eduLevels.isEmpty())
            builder.where(in(property("g", Group.educationOrgUnit().educationLevelHighSchool()), eduLevels));
        if (null != developForms && !developForms.isEmpty())
            builder.where(in(property("g", Group.educationOrgUnit().developForm()), developForms));
        if (null != developConditions && !developConditions.isEmpty())
            builder.where(in(property("g", Group.educationOrgUnit().developCondition()), developConditions));
        if (null != developTechs && !developTechs.isEmpty())
            builder.where(in(property("g", Group.educationOrgUnit().developTech()), developTechs));
        if (null != developPeriods && !developPeriods.isEmpty())
            builder.where(in(property("g", Group.educationOrgUnit().developPeriod()), developPeriods));

        if (!StringUtils.isEmpty(filter))
            builder.where(like(DQLFunctions.upper(property("g", Group.title())), value(CoreStringUtils.escapeLike(filter, true))));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().pageable(true).build();
    }
}
