/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.SeasonAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;

/**
 * @author nvankov
 * @since 9/3/13
 */
@Input({
        @Bind(key= UIPresenter.PUBLISHER_ID, binding="scheduleSeasonId")
})
public class SppScheduleSeasonAddEditUI extends UIPresenter
{
    private Long _scheduleSeasonId;
    private SppScheduleSeason _scheduleSeason;

    public Long getScheduleSeasonId()
    {
        return _scheduleSeasonId;
    }

    public void setScheduleSeasonId(Long scheduleSeasonId)
    {
        _scheduleSeasonId = scheduleSeasonId;
    }

    public SppScheduleSeason getScheduleSeason()
    {
        return _scheduleSeason;
    }

    public void setScheduleSeason(SppScheduleSeason scheduleSeason)
    {
        _scheduleSeason = scheduleSeason;
    }

    public Boolean getAddForm()
    {
        return null == _scheduleSeasonId;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null != _scheduleSeasonId)
            _scheduleSeason = DataAccessServices.dao().getNotNull(_scheduleSeasonId);
        else
            _scheduleSeason = new SppScheduleSeason();
    }

    public void onClickApply()
    {
        if(_scheduleSeason.getStartDate().after(_scheduleSeason.getEndDate()))
            _uiSupport.error("Поле «Дата начала» не может быть позже поля «Дата окончания»", "startDate", "endDate");
        if(getUserContext().getErrorCollector().hasErrors())
            return;
        SppScheduleManager.instance().dao().createOrUpdateScheduelSeason(_scheduleSeason);
        deactivate();
    }
}
