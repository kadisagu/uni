package ru.tandemservice.unispp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.tandemframework.dbsupport.sql.SQLFrom.table;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unispp_2x6x8_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8")
		};
    }

	@Override
	public ScriptDependency[] getBeforeDependencies()
	{
		return new ScriptDependency[]
				{
						new ScriptDependency("uniplaces", "2.6.8", 1)
				};
	}

	@Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sppPlacesPlaceExt

		// сущность была удалена
		if(tool.tableExists("spp_places_place_ext"))
		{
			ISQLTranslator translator = tool.getDialect().getSQLTranslator();

			PreparedStatement update = tool.prepareStatement("update places_place set capacity_p = ? where id = ?");


			SQLSelectQuery selectQuery = new SQLSelectQuery()
					.from(table("spp_places_place_ext", "spppl"))
					.column("spppl.uniplacesplace_id", "place")
					.column("spppl.capacity_p", "cap");

			String sql = translator.toSql(selectQuery);

			Statement statement = tool.getConnection().createStatement();
			statement.execute(sql);

			ResultSet result = statement.getResultSet();

			while (result.next())
			{
				Long placeId = result.getLong("place");
				Integer capacity = result.getInt("cap");
				if(capacity == 0) capacity = null;

				if(capacity != null)
				{
					update.clearParameters();
					update.setInt(1, capacity);
					update.setLong(2, placeId);
					update.executeUpdate();
				}
			}

			// удалить таблицу
			tool.dropTable("spp_places_place_ext", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("sppPlacesPlaceExt");

		}
    }
}