/* $Id: SppScheduleLearningProcessListUI.java 37774 2014-09-02 09:12:51Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.LearningProcessList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleRegistryDisciplineComboDSHandler;

/**
 * @author nvankov
 * @since 9/4/13
 */
public class SppScheduleLearningProcessListUI extends UIPresenter
{
    private BaseSearchListDataSource _scheduleDS;

    public BaseSearchListDataSource getScheduleDS()
    {
        if(null == _scheduleDS)
            _scheduleDS = _uiConfig.getDataSource(SppScheduleLearningProcessList.SCHEDULE_DS);
        return _scheduleDS;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleLearningProcessList.SCHEDULE_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleDSHandler.PROP_TITLE, _uiSettings.get("title"));
            dataSource.put(SppScheduleDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
            dataSource.putAll(_uiSettings.getAsMap(true, "courses", "eduLevels", "groups", "seasons", "orgUnitIds", "subjects"));
        }
        if(SppScheduleLearningProcessList.EDU_LEVEL_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "courses", "orgUnitIds"));
        }
        if(SppScheduleLearningProcessList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "courses", "eduLevels", "orgUnitIds"));
        }
        if(SppScheduleLearningProcessList.SUBJECT_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleRegistryDisciplineComboDSHandler.PROP_ORG_UNIT_IDS, _uiSettings.get("orgUnitIds"));
            dataSource.put(SppScheduleRegistryDisciplineComboDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
        }
        if(SppScheduleLearningProcessList.COURSE_DS.equals(dataSource.getName()))
        {
            dataSource.put("orgUnitIds", _uiSettings.get("orgUnitIds"));
        }
    }
}
