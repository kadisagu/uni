package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceWeekRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент предпочтения преподавателя по расписанию
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppTeacherPreferenceElementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement";
    public static final String ENTITY_NAME = "sppTeacherPreferenceElement";
    public static final int VERSION_HASH = -1121966772;
    private static IEntityMeta ENTITY_META;

    public static final String P_DAY_OF_WEEK = "dayOfWeek";
    public static final String L_WEEK_ROW = "weekRow";
    public static final String L_BUILDING = "building";
    public static final String L_LECTURE_ROOM = "lectureRoom";
    public static final String P_UNWANTED_TIME = "unwantedTime";

    private int _dayOfWeek;     // День недели
    private SppTeacherPreferenceWeekRow _weekRow;     // Недельная строка предпочтения
    private UniplacesBuilding _building;     // Здание
    private UniplacesPlace _lectureRoom;     // Аудитория
    private boolean _unwantedTime = false;     // Нежелательное время

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return День недели. Свойство не может быть null.
     */
    @NotNull
    public int getDayOfWeek()
    {
        return _dayOfWeek;
    }

    /**
     * @param dayOfWeek День недели. Свойство не может быть null.
     */
    public void setDayOfWeek(int dayOfWeek)
    {
        dirty(_dayOfWeek, dayOfWeek);
        _dayOfWeek = dayOfWeek;
    }

    /**
     * @return Недельная строка предпочтения. Свойство не может быть null.
     */
    @NotNull
    public SppTeacherPreferenceWeekRow getWeekRow()
    {
        return _weekRow;
    }

    /**
     * @param weekRow Недельная строка предпочтения. Свойство не может быть null.
     */
    public void setWeekRow(SppTeacherPreferenceWeekRow weekRow)
    {
        dirty(_weekRow, weekRow);
        _weekRow = weekRow;
    }

    /**
     * @return Здание.
     */
    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    /**
     * @param building Здание.
     */
    public void setBuilding(UniplacesBuilding building)
    {
        dirty(_building, building);
        _building = building;
    }

    /**
     * @return Аудитория.
     */
    public UniplacesPlace getLectureRoom()
    {
        return _lectureRoom;
    }

    /**
     * @param lectureRoom Аудитория.
     */
    public void setLectureRoom(UniplacesPlace lectureRoom)
    {
        dirty(_lectureRoom, lectureRoom);
        _lectureRoom = lectureRoom;
    }

    /**
     * @return Нежелательное время. Свойство не может быть null.
     */
    @NotNull
    public boolean isUnwantedTime()
    {
        return _unwantedTime;
    }

    /**
     * @param unwantedTime Нежелательное время. Свойство не может быть null.
     */
    public void setUnwantedTime(boolean unwantedTime)
    {
        dirty(_unwantedTime, unwantedTime);
        _unwantedTime = unwantedTime;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppTeacherPreferenceElementGen)
        {
            setDayOfWeek(((SppTeacherPreferenceElement)another).getDayOfWeek());
            setWeekRow(((SppTeacherPreferenceElement)another).getWeekRow());
            setBuilding(((SppTeacherPreferenceElement)another).getBuilding());
            setLectureRoom(((SppTeacherPreferenceElement)another).getLectureRoom());
            setUnwantedTime(((SppTeacherPreferenceElement)another).isUnwantedTime());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppTeacherPreferenceElementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppTeacherPreferenceElement.class;
        }

        public T newInstance()
        {
            return (T) new SppTeacherPreferenceElement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "dayOfWeek":
                    return obj.getDayOfWeek();
                case "weekRow":
                    return obj.getWeekRow();
                case "building":
                    return obj.getBuilding();
                case "lectureRoom":
                    return obj.getLectureRoom();
                case "unwantedTime":
                    return obj.isUnwantedTime();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "dayOfWeek":
                    obj.setDayOfWeek((Integer) value);
                    return;
                case "weekRow":
                    obj.setWeekRow((SppTeacherPreferenceWeekRow) value);
                    return;
                case "building":
                    obj.setBuilding((UniplacesBuilding) value);
                    return;
                case "lectureRoom":
                    obj.setLectureRoom((UniplacesPlace) value);
                    return;
                case "unwantedTime":
                    obj.setUnwantedTime((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "dayOfWeek":
                        return true;
                case "weekRow":
                        return true;
                case "building":
                        return true;
                case "lectureRoom":
                        return true;
                case "unwantedTime":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "dayOfWeek":
                    return true;
                case "weekRow":
                    return true;
                case "building":
                    return true;
                case "lectureRoom":
                    return true;
                case "unwantedTime":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "dayOfWeek":
                    return Integer.class;
                case "weekRow":
                    return SppTeacherPreferenceWeekRow.class;
                case "building":
                    return UniplacesBuilding.class;
                case "lectureRoom":
                    return UniplacesPlace.class;
                case "unwantedTime":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppTeacherPreferenceElement> _dslPath = new Path<SppTeacherPreferenceElement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppTeacherPreferenceElement");
    }
            

    /**
     * @return День недели. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement#getDayOfWeek()
     */
    public static PropertyPath<Integer> dayOfWeek()
    {
        return _dslPath.dayOfWeek();
    }

    /**
     * @return Недельная строка предпочтения. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement#getWeekRow()
     */
    public static SppTeacherPreferenceWeekRow.Path<SppTeacherPreferenceWeekRow> weekRow()
    {
        return _dslPath.weekRow();
    }

    /**
     * @return Здание.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement#getBuilding()
     */
    public static UniplacesBuilding.Path<UniplacesBuilding> building()
    {
        return _dslPath.building();
    }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement#getLectureRoom()
     */
    public static UniplacesPlace.Path<UniplacesPlace> lectureRoom()
    {
        return _dslPath.lectureRoom();
    }

    /**
     * @return Нежелательное время. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement#isUnwantedTime()
     */
    public static PropertyPath<Boolean> unwantedTime()
    {
        return _dslPath.unwantedTime();
    }

    public static class Path<E extends SppTeacherPreferenceElement> extends EntityPath<E>
    {
        private PropertyPath<Integer> _dayOfWeek;
        private SppTeacherPreferenceWeekRow.Path<SppTeacherPreferenceWeekRow> _weekRow;
        private UniplacesBuilding.Path<UniplacesBuilding> _building;
        private UniplacesPlace.Path<UniplacesPlace> _lectureRoom;
        private PropertyPath<Boolean> _unwantedTime;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return День недели. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement#getDayOfWeek()
     */
        public PropertyPath<Integer> dayOfWeek()
        {
            if(_dayOfWeek == null )
                _dayOfWeek = new PropertyPath<Integer>(SppTeacherPreferenceElementGen.P_DAY_OF_WEEK, this);
            return _dayOfWeek;
        }

    /**
     * @return Недельная строка предпочтения. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement#getWeekRow()
     */
        public SppTeacherPreferenceWeekRow.Path<SppTeacherPreferenceWeekRow> weekRow()
        {
            if(_weekRow == null )
                _weekRow = new SppTeacherPreferenceWeekRow.Path<SppTeacherPreferenceWeekRow>(L_WEEK_ROW, this);
            return _weekRow;
        }

    /**
     * @return Здание.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement#getBuilding()
     */
        public UniplacesBuilding.Path<UniplacesBuilding> building()
        {
            if(_building == null )
                _building = new UniplacesBuilding.Path<UniplacesBuilding>(L_BUILDING, this);
            return _building;
        }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement#getLectureRoom()
     */
        public UniplacesPlace.Path<UniplacesPlace> lectureRoom()
        {
            if(_lectureRoom == null )
                _lectureRoom = new UniplacesPlace.Path<UniplacesPlace>(L_LECTURE_ROOM, this);
            return _lectureRoom;
        }

    /**
     * @return Нежелательное время. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceElement#isUnwantedTime()
     */
        public PropertyPath<Boolean> unwantedTime()
        {
            if(_unwantedTime == null )
                _unwantedTime = new PropertyPath<Boolean>(SppTeacherPreferenceElementGen.P_UNWANTED_TIME, this);
            return _unwantedTime;
        }

        public Class getEntityClass()
        {
            return SppTeacherPreferenceElement.class;
        }

        public String getEntityName()
        {
            return "sppTeacherPreferenceElement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
