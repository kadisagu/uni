/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.OrgUnitTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.MassAddingSelectSchedulesList.SppScheduleMassAddingSelectSchedulesList;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.OrgUnitList.SppScheduleOrgUnitList;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.OrgUnitPrintFormList.SppScheduleOrgUnitPrintFormList;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.OrgUnitList.SppScheduleDailyOrgUnitList;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.OrgUnitPrintFormList.SppScheduleDailyOrgUnitPrintFormList;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.OrgUnitList.SppScheduleSessionOrgUnitList;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.OrgUnitPrintFormList.SppScheduleSessionOrgUnitPrintFormList;

/**
 * @author nvankov
 * @since 1/14/14
 */
@Configuration
public class SppScheduleOrgUnitTab extends BusinessComponentManager
{
    // tab panel
    public static final String TAB_PANEL = "tabPanel";

    // tab names
    public static final String SCHEDULE_TAB = "scheduleTab";
    public static final String PRINT_FORM_SCHEDULE_TAB = "printFormScheduleTab";
    public static final String SCHEDULE_SESSION_TAB = "scheduleSessionTab";
    public static final String PRINT_FORM_SCHEDULE_SESSION_TAB = "printFormScheduleSessionTab";
    public static final String SCHEDULE_DAILY_TAB = "scheduleDailyTab";
    public static final String PRINT_FORM_SCHEDULE_DAILY_TAB = "printFormScheduleDailyTab";
    public static final String EXERCISE_MASS_ADDING_TAB = "exerciseMassAddingTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(TAB_PANEL)
                .addTab(componentTab(SCHEDULE_TAB, SppScheduleOrgUnitList.class).permissionKey("ui:sec.orgUnit_viewSppScheduleTab").create())
                .addTab(componentTab(PRINT_FORM_SCHEDULE_TAB, SppScheduleOrgUnitPrintFormList.class).permissionKey("ui:sec.orgUnit_viewSppSchedulePrintFormTab").create())
                .addTab(componentTab(SCHEDULE_SESSION_TAB, SppScheduleSessionOrgUnitList.class).permissionKey("ui:sec.orgUnit_viewSppScheduleSessionTab").create())
                .addTab(componentTab(PRINT_FORM_SCHEDULE_SESSION_TAB, SppScheduleSessionOrgUnitPrintFormList.class).permissionKey("ui:sec.orgUnit_viewSppScheduleSessionPrintFormTab").create())
                .addTab(componentTab(SCHEDULE_DAILY_TAB, SppScheduleDailyOrgUnitList.class).permissionKey("ui:sec.orgUnit_viewSppScheduleDailyTab").create())
                .addTab(componentTab(PRINT_FORM_SCHEDULE_DAILY_TAB, SppScheduleDailyOrgUnitPrintFormList.class).permissionKey("ui:sec.orgUnit_viewSppScheduleDailyPrintFormTab").create())
                .addTab(componentTab(EXERCISE_MASS_ADDING_TAB, SppScheduleMassAddingSelectSchedulesList.class).permissionKey("ui:sec.orgUnit_viewSppScheduleExerciseMassAddingTab").create())
                .create();
    }
}
