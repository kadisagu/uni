/* $Id$ */
package ru.tandemservice.unispp.dao.daemon;

import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unispp.base.entity.SppSchedule;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 9/10/13
 */
public class SppScheduleDaemonDAO extends UniBaseDao implements ISppScheduleDaemonDAO
{
    public static final SyncDaemon DAEMON = new SyncDaemon(SppScheduleDaemonDAO.class.getName(), 120, ISppScheduleDaemonDAO.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            final ISppScheduleDaemonDAO dao = ISppScheduleDaemonDAO.instance.get();

            try
            {
                dao.doArchiveSchedules();
            } catch (final Throwable t)
            {
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            }

        }
    };

    protected Session lock4update()
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, ISppScheduleDaemonDAO.GLOBAL_DAEMON_LOCK);
        return session;
    }


    @Override
    public void doArchiveSchedules()
    {
        final Session session = this.lock4update();

        Debug.begin("SppScheduleDaemonDao.doArchiveSchedules");
        // обновление существующих записей
        try {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(SppSchedule.class, "s").
                    column(property("s", SppSchedule.id())).
                    where(eq(property("s", SppSchedule.group().archival()), value(true))).
                    where(eq(property("s", SppSchedule.archived()), value(false)));

            int count = executeAndClear(
                    new DQLUpdateBuilder(SppSchedule.class)
                            .where(in(property(SppSchedule.id()), subBuilder.buildQuery()))
                            .set(SppSchedule.P_ARCHIVED, value(true)),
                    session
            );

            if (count > 0) {
                Debug.message("Send to archive: "+ count + (count > 1 ? "schedules" : "schedule"));
            }

        } finally {
            Debug.end();
        }
    }
}
