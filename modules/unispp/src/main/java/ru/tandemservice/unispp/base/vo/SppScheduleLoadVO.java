/*$Id$*/
package ru.tandemservice.unispp.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author DMITRY KNYAZEV
 * @since 21.10.2015
 */
public class SppScheduleLoadVO extends DataWrapper implements Comparable<SppScheduleLoadVO>
{
    public static final String DISCIPLINE = "disciplineTitle";
    public static final String REQUIRED_LOAD = "requiredLoadTitle";
    public static final String LOAD = "loadTitle";

    private final EppRegistryElement _discipline;
    private final EppALoadType _loadType;
    private final Double _requiredLoad;
    private final Long _load;

    public SppScheduleLoadVO(EppRegistryElement discipline, EppALoadType loadType, Double requiredLoad, Long load)
    {
        super(discipline.getId() + loadType.getId(), discipline.getTitle() + " (" + loadType.getTitle() + ")");
        _discipline = discipline;
        _loadType = loadType;
        _load = load;
        _requiredLoad = requiredLoad;
    }

    public String getDisciplineTitle()
    {
        return _discipline.getTitle() + " (" + _loadType.getTitle() + ")";
    }

    public String getRequiredLoadTitle()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_requiredLoad);
    }

    public String getLoadTitle()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(_load);
    }

    /**
     * Статус нагрузки (покрыта или нет)
     * @return
     * -1 если нагрузка не соответствует требуемой (не хватает занятий)
     * 0, если нагрузка соответствует требуемой
     * 1, если нагрузка не соответствует требуемой (занятий больше)
     */
    public int coverStatus()
    {
        return Long.compare(_load, _requiredLoad.longValue());
    }

    @Override
    public int compareTo(SppScheduleLoadVO otherLoadVO)
    {
        if (otherLoadVO == null) return 1;
        // сортируем тупо по титлу
        return getDisciplineTitle().compareTo(otherLoadVO.getDisciplineTitle());
    }
}
