/* $Id: SppScheduleDailyOrgUnitListUI.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.OrgUnitList;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.AddEdit.SppScheduleDailyAddEdit;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.AddSchedulesSelectGroupsList.SppScheduleDailyAddSchedulesSelectGroupsList;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.AddSchedulesSelectGroupsList.SppScheduleDailyAddSchedulesSelectGroupsListUI;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;

import java.util.Map;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
public class SppScheduleDailyOrgUnitListUI extends OrgUnitUIPresenter
{
    private BaseSearchListDataSource _scheduleDS;

    public BaseSearchListDataSource getScheduleDS()
    {
        if(null == _scheduleDS)
            _scheduleDS = _uiConfig.getDataSource(SppScheduleDailyOrgUnitList.SCHEDULE_DS);
        return _scheduleDS;
    }

    public Boolean getEntityEditDisabled()
    {
        SppScheduleDaily schedule = getScheduleDS().getCurrent();
        return schedule.isApproved() || schedule.isArchived();
    }

    public Boolean getEntityDeleteDisabled()
    {
        return false;
    }

    public Map<String, Object> getParametersMap()
    {
        return new ParametersMap()
                .add("orgUnitId", getOrgUnit().getId());
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("orgUnitId", getOrgUnit().getId());
        if (SppScheduleDailyOrgUnitList.SCHEDULE_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleDailyDSHandler.PROP_TITLE, _uiSettings.get("title"));
            dataSource.put(SppScheduleDailyDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
            dataSource.putAll(_uiSettings.getAsMap(true, "courses", "eduLevels", "groups"));
        }
        if(SppScheduleDailyOrgUnitList.EDU_LEVEL_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "courses"));
        }
        if(SppScheduleDailyOrgUnitList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "courses", "eduLevels"));
        }
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(SppScheduleDailyAddEdit.class).parameter("scheduleId", getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        SppScheduleDailyManager.instance().dao().deleteSchedule(getListenerParameterAsLong());
    }

    public void onClickAddSchedule()
    {
        _uiActivation.asRegionDialog(SppScheduleDailyAddEdit.class)
                .parameter("orgUnitId", getOrgUnit().getId())
                .activate();
    }

    public void onClickAddSchedules()
    {
        _uiActivation.asRegion(SppScheduleDailyAddSchedulesSelectGroupsList.class)
                .parameter(SppScheduleDailyAddSchedulesSelectGroupsListUI.BIND_ORG_UNIT_ID, getOrgUnit().getId())
                .activate();
    }
}
