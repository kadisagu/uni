/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.*;
import ru.tandemservice.unispp.base.vo.SppTeacherPreferencePeriodVO;
import ru.tandemservice.unispp.base.vo.SppTeacherPreferenceVO;

import java.util.List;

/**
 * @author vnekrasov
 * @since 6/23/14
 */
public interface ISppTeacherPreferenceDAO extends INeedPersistenceSupport
{
    List<ScheduleBellEntry> getBells(Long bellsId);

    void saveOrUpdateTeacherPreferenceList(SppTeacherPreferenceList teacherPreference, ScheduleBell oldBells, boolean oldDiffEven);

    void saveOrUpdateTeacherSessionPreferenceList(SppTeacherSessionPreferenceList teacherPreference, ScheduleBell oldBells);

    void saveOrUpdateTeacherDailyPreferenceList(SppTeacherDailyPreferenceList teacherPreference, ScheduleBell oldBells);

    public void deleteTeacherPreferenceList(Long teacherPreferenceId);

    void saveOrUpdatePeriods(List<SppTeacherPreferencePeriodVO> periods);

    SppTeacherPreferenceVO prepareTeacherPreferenceVO(SppTeacherPreferenceList teacherPreference);

//    boolean isSppScheduleListTabVisible(Long orgUnitId);

    void createOrUpdateElement(SppTeacherPreferenceElement element);

    void createOrUpdateSessionElement(SppTeacherSessionPreferenceElement element);

    void createOrUpdateDailyElement(SppTeacherDailyPreferenceElement element);
}
