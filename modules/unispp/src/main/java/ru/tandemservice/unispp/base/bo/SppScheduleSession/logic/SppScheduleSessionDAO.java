/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.entity.*;
import ru.tandemservice.unispp.base.entity.catalog.SppScheduleStatus;
import ru.tandemservice.unispp.base.entity.catalog.codes.SppScheduleStatusCodes;
import ru.tandemservice.unispp.base.vo.SppScheduleSessionGroupPrintVO;
import ru.tandemservice.unispp.util.SppScheduleUtil;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 12/27/13
 */
@Transactional
public class SppScheduleSessionDAO extends UniBaseDao implements ISppScheduleSessionDAO
{
    @Override
    public void createOrUpdateSeason(SppScheduleSessionSeason season)
    {
        saveOrUpdate(season);
    }

    @Override
    public void saveSchedules(List<SppScheduleSession> scheduleList)
    {
        scheduleList.forEach(this::save);
    }

    @Override
    public void saveOrUpdateSchedule(SppScheduleSession schedule, ScheduleBell oldBells)
    {
        boolean deleteAndCreateStructure = false;
        if (null == schedule.getId()) deleteAndCreateStructure = true;
        if (null != oldBells && !oldBells.getId().equals(schedule.getBells().getId())) deleteAndCreateStructure = true;

        if (deleteAndCreateStructure)
        {
            new DQLDeleteBuilder(SppScheduleSessionEvent.class)
                    .where(eq(property(SppScheduleSessionEvent.schedule().id()), value(schedule.getId())))
                    .createStatement(getSession()).execute();

            new DQLDeleteBuilder(SppScheduleSessionWarningLog.class)
                    .where(eq(property(SppScheduleSessionWarningLog.sppScheduleSession().id()), value(schedule.getId())))
                    .createStatement(getSession()).execute();
        }
        saveOrUpdate(schedule);
    }

    @Override
    public void copySchedule(Long scheduleSessionId, SppScheduleSession newScheduleSession)
    {
        saveOrUpdate(newScheduleSession);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "e");
        builder.where(eq(property("e", SppScheduleSessionEvent.schedule().id()), value(scheduleSessionId)));
        List<SppScheduleSessionEvent> eventList = builder.createStatement(getSession()).list();

        for (SppScheduleSessionEvent event : eventList)
        {
            SppScheduleSessionEvent newEvent = new SppScheduleSessionEvent();
            newEvent.setDate(event.getDate());
            newEvent.setLectureRoom(event.getLectureRoom());
            newEvent.setLectureRoomVal(event.getLectureRoomVal());
            newEvent.setTeacher(event.getTeacher());
            newEvent.setTeacherVal(event.getTeacherVal());

            newEvent.setSubject(event.getSubject());
            newEvent.setSubjectVal(event.getSubjectVal());
            newEvent.setSubjectFormControl(event.getSubjectFormControl());
            newEvent.setSchedule(newScheduleSession);

            saveOrUpdate(newEvent);
        }
    }

    @Override
    public void sentToArchive(Long scheduleId)
    {
        SppScheduleSession schedule =
//                DataAccessServices.dao().get(scheduleId);
                (SppScheduleSession) get(scheduleId);
        schedule.setArchived(true);
        update(schedule);
    }

    @Override
    public void restoreFromArchive(Long scheduleId)
    {
        SppScheduleSession schedule = (SppScheduleSession) get(scheduleId);
        schedule.setArchived(false);
        update(schedule);
    }

    @Override
    public void validate(SppScheduleSession schedule)
    {
        List<SppScheduleSessionEvent> eventList = DataAccessServices.dao().getList(SppScheduleSessionEvent.class, SppScheduleSessionEvent.schedule(), schedule);

        if (eventList.size() == 0)
            throw new ApplicationException("Для утверждения необходимо, чтобы в расписании было добавлено минимум 1 событие");

        SppScheduleSessionSeason season = schedule.getSeason();
        Date startDate = new DateTime(season.getStartDate()).withTime(0, 0, 0, 0).toDate();
        Date endDate = new DateTime(season.getEndDate()).withTime(23, 59, 59, 1).toDate();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleSession.class, "s")
                .where(eq(property("s", SppScheduleSession.group().id()), value(schedule.getGroup().getId())))
                .where(ne(property("s", SppScheduleSession.id()), value(schedule.getId())))
                .where(eq(property("s", SppScheduleSession.approved()), value(Boolean.TRUE)))
                .where(eq(property("s", SppScheduleSession.archived()), value(Boolean.FALSE)))
                .where(and(
                        le(property("s", SppScheduleSession.season().startDate()), valueDate(endDate)),
                        ge(property("s", SppScheduleSession.season().endDate()), valueDate(startDate))));

        List<SppScheduleSession> schedules = builder.createStatement(getSession()).list();

        if (schedules.size() > 0)
        {
            String scheduleTitles = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(schedules, SppScheduleSession.title()), ", ");
            throw new ApplicationException("На период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startDate) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate) + " уже есть не архивные утвержденные расписания - " + scheduleTitles);
        }

        updateWarningLog(schedule.getId(), eventList);
    }

    @Override
    public void approve(Long scheduleId)
    {
        SppScheduleSession schedule = (SppScheduleSession) getNotNull(scheduleId);

        validate(schedule);

        SppScheduleStatus status = DataAccessServices.dao().get(SppScheduleStatus.class, SppScheduleStatus.code(), SppScheduleStatusCodes.APPROVED);
        schedule.setStatus(status);
        schedule.setApproved(true);
        update(schedule);
    }

    @Override
    public void sentToFormation(Long scheduleId)
    {
        SppScheduleSession session = (SppScheduleSession) get(scheduleId);
        SppScheduleStatus status = DataAccessServices.dao().get(SppScheduleStatus.class, SppScheduleStatus.code(), SppScheduleStatusCodes.FORMATION);
        session.setStatus(status);
        session.setApproved(false);
        update(session);
    }

    @Override
    public UniplacesClassroomType getDisciplineLectureRoomType(EppRegistryElement discipline, EppALoadType loadType)
    {
        if (discipline != null && loadType != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppRegElementLoadCheck.class, "rc");
            builder.column(property("rc", SppRegElementLoadCheck.uniplacesClassroomType()));
            builder.where(eq(property("rc", SppRegElementLoadCheck.registryElement().id()), value(discipline.getId())));
            builder.where(eq(property("rc", SppRegElementLoadCheck.eppALoadType().id()), value(loadType.getId())));
            return builder.createStatement(getSession()).uniqueResult();
        }
        return null;
    }

    private void checkLectureRoomCapacity(Long scheduleId, List<SppScheduleSessionEvent> eventList)
    {
        SppScheduleSession daily = (SppScheduleSession) get(scheduleId);

        //int studentGrpCnt = DataAccessServices.dao().getCount(Student.class, Student.group().id().s(), daily.getGroup().getId());
        int studentGrpCnt = SppScheduleManager.instance().dao().getGroupStudentCount(daily.getGroup().getId());

        for (SppScheduleSessionEvent event : eventList)
        {
            if (event.getLectureRoomVal() != null && event.getLectureRoomVal().getCapacity() != null &&
                    event.getLectureRoomVal().getCapacity() < studentGrpCnt)
            {
                addWarningMessage(daily, "ОШИБКА: аудитория «" + event.getLectureRoom() + "»" +
                        " в событии " + DateFormatter.DEFAULT_DATE_FORMATTER.format(event.getDate()) +
                        " не может вместить всех студентов группы");
            }
        }
    }

    private void checkLectureRoomFree(Long scheduleId, List<SppScheduleSessionEvent> eventList)
    {
        SppScheduleSession schedule = (SppScheduleSession) getNotNull(scheduleId);

        for (SppScheduleSessionEvent event : eventList)
        {
            if (null != event.getLectureRoomVal())
            {
                Date date = event.getDate();

                UniplacesPlace place = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "e")
                        .column(property("e", SppSchedulePeriod.lectureRoomVal())).top(1)
                        .where(eq(property("e", SppSchedulePeriod.lectureRoomVal().id()), value(event.getLectureRoomVal().getId())))
                        .where(le(property("e", SppSchedulePeriod.weekRow().schedule().season().startDate()), valueDate(date)))
                        .where(ge(property("e", SppSchedulePeriod.weekRow().schedule().season().endDate()), valueDate(date)))
                        .where(eq(property("e", SppSchedulePeriod.dayOfWeek()), value(SppScheduleUtil.getDayOfWeek(date))))
                        .where(eq(property("e", SppSchedulePeriod.weekRow().bell()), value(event.getBell())))
                        .createStatement(getSession()).uniqueResult();

                UniplacesPlace placeSessionEvent = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "e")
                        .column(property("e", SppScheduleSessionEvent.lectureRoomVal())).top(1)
                        .where(eq(property("e", SppScheduleSessionEvent.lectureRoomVal().id()), value(event.getLectureRoomVal().getId())))
                        .where(ne(property("e", SppScheduleSessionEvent.schedule().id()), value(scheduleId)))
                        .where(eq(property("e", SppScheduleSessionEvent.date()), valueDate(date)))
                        .where(eq(property("e", SppScheduleSessionEvent.bell()), value(event.getBell())))
                        .createStatement(getSession()).uniqueResult();

                UniplacesPlace placeDailyEvent = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "e")
                        .column(property("e", SppScheduleDailyEvent.lectureRoomVal())).top(1)
                        .where(eq(property("e", SppScheduleDailyEvent.lectureRoomVal().id()), value(event.getLectureRoomVal().getId())))
                        .where(eq(property("e", SppScheduleDailyEvent.date()), valueDate(date)))
                        .where(eq(property("e", SppScheduleDailyEvent.bell()), value(event.getBell())))
                        .createStatement(getSession()).uniqueResult();

                String dateFirstTimeMoment = DateFormatter.DEFAULT_DATE_FORMATTER.format(CoreDateUtils.getDayFirstTimeMoment(event.getDate()));
                if (place != null)
                {
                    addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + event.getLectureRoom() + "»" +
                            " в событии «" + dateFirstTimeMoment + ", " + event.getBell().getTitle() + "»" +
                            " используется в одном из обычных расписаний");
                }
                if (placeDailyEvent != null)
                {
                    addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + event.getLectureRoom() + "»" +
                            " в событии «" + dateFirstTimeMoment + ", " + event.getBell().getTitle() + "»" +
                            " используется в одном из подневных расписаний");
                }
                if (placeSessionEvent != null)
                {
                    addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + event.getLectureRoom() + "»" +
                            " в событии «" + dateFirstTimeMoment + ", " + event.getBell().getTitle() + "»" +
                            " используется в расписании сессии другой группы");
                }
            }
        }
    }

    private void checkTeacherPreference(Long scheduleId, List<SppScheduleSessionEvent> eventList)
    {
        SppScheduleSession daily = get(scheduleId);
        for (SppScheduleSessionEvent event : eventList)
        {
            if (event.getTeacherVal() != null)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppTeacherSessionPreferenceElement.class, "e");
                builder.column("e");
                builder.where(eq(property("e", SppTeacherSessionPreferenceElement.teacherPreference().teacher().id()), value(event.getTeacherVal().getPerson().getId())));
                builder.where(le(property("e", SppTeacherSessionPreferenceElement.startDate()), valueDate(event.getDate())));
                builder.where(ge(property("e", SppTeacherSessionPreferenceElement.endDate()), valueDate(event.getDate())));
                builder.where(or(isNull(property("e", SppTeacherSessionPreferenceElement.dayOfWeek())),
                        eq(property("e", SppTeacherSessionPreferenceElement.dayOfWeek()), value(SppScheduleUtil.getDayOfWeek(event.getDate()))))
                );
                List<SppTeacherSessionPreferenceElement> elementList = createStatement(builder).list();
                List<UniplacesBuilding> buildingList = new ArrayList<>();
                List<UniplacesPlace> lectureRoomList = new ArrayList<>();

                for (SppTeacherSessionPreferenceElement element : elementList)
                {
                    if (element.getBuilding() != null)
                        buildingList.add(element.getBuilding());
                    if (element.getLectureRoom() != null)
                        lectureRoomList.add(element.getLectureRoom());
                }

                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "p");
                dql.where(in(property("p", UniplacesPlace.floor().unit().building()), buildingList));
                List<UniplacesPlace> addLectureRoomList = createStatement(dql).list();
                lectureRoomList.addAll(addLectureRoomList);

                if (!lectureRoomList.isEmpty() && event.getLectureRoomVal() != null && !lectureRoomList.contains(event.getLectureRoomVal()))
                {
                    addWarningMessage(daily, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + event.getLectureRoom() + "»" +
                            " в событии " + DateFormatter.DEFAULT_DATE_FORMATTER.format(event.getDate()) +
                            " отсутствует в предпочтениях преподавателя «" + event.getTeacher() + "»");
                }
            }
        }
    }

    private void checkDisciplinePreference(Long scheduleId, List<SppScheduleSessionEvent> eventList)
    {
        SppScheduleSession daily = get(scheduleId);
        for (SppScheduleSessionEvent event : eventList)
        {
            if (event.getSubjectVal() != null)
            {
                DQLSelectBuilder builder1 = new DQLSelectBuilder().fromEntity(SppDisciplinePreferenceList.class, "e");
                builder1.column(property("e", SppDisciplinePreferenceList.teacher()));
                builder1.where(eq(property("e", SppDisciplinePreferenceList.discipline().id()), value(event.getSubjectVal().getId())));
                List<PpsEntry> teacherList = createStatement(builder1).list();

                if (!teacherList.isEmpty() && event.getTeacherVal() != null && !teacherList.contains(event.getTeacherVal()))
                {
                    addWarningMessage(daily, "ПРЕДУПРЕЖДЕНИЕ: преподаватель «" + event.getTeacher() + "»" +
                            " в событии " + DateFormatter.DEFAULT_DATE_FORMATTER.format(event.getDate()) +
                            " отсутствует в предпочтениях по дисциплине «" + event.getSubject() + "»");
                }

                DQLSelectBuilder builder2 = new DQLSelectBuilder().fromEntity(SppDisciplinePreferenceList.class, "e");
                builder2.column("e");
                builder2.where(eq(property("e", SppDisciplinePreferenceList.discipline().id()), value(event.getSubjectVal().getId())));

                List<SppDisciplinePreferenceList> elementList = createStatement(builder2).list();

                List<UniplacesBuilding> buildingList = new ArrayList<>();
                List<UniplacesPlace> lectureRoomList = new ArrayList<>();


                for (SppDisciplinePreferenceList element : elementList)
                {
                    if (element.getBuilding() != null)
                        buildingList.add(element.getBuilding());
                    if (element.getLectureRoom() != null)
                        lectureRoomList.add(element.getLectureRoom());
                }

                DQLSelectBuilder dql1 = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "p");
                dql1.where(in(property("p", UniplacesPlace.floor().unit().building()), buildingList));
                List<UniplacesPlace> addLectureRoomList = createStatement(dql1).list();
                lectureRoomList.addAll(addLectureRoomList);

                if (lectureRoomList.size() > 0 && !lectureRoomList.contains(event.getLectureRoomVal()))
                {
                    addWarningMessage(daily, "ПРЕДУПРЕЖДЕНИЕ: аудитория «" + event.getLectureRoom() + "»" +
                            " в событии " + DateFormatter.DEFAULT_DATE_FORMATTER.format(event.getDate()) +
                            " отсутствует в предпочтениях по дисциплине «" + event.getSubject() + "»");
                }
            }
        }
    }

    private void checkDayDoubleDiscipline(Long scheduleId, List<SppScheduleSessionEvent> eventList)
    {
        SppScheduleSession schedule = getNotNull(scheduleId);
        Set<Date> dateList = eventList.stream().map(e -> CoreDateUtils.getDayFirstTimeMoment(e.getDate())).collect(Collectors.toSet());

        // возможно, по некоторым дисциплинам вообще не нужно проверять дубли
        DQLSelectBuilder regElementExtBuilder = new DQLSelectBuilder();
        regElementExtBuilder.fromEntity(SppRegElementExt.class, "elExt").column("elExt");
        regElementExtBuilder.where(eq(property("elExt", SppRegElementExt.checkDuplicates()), value(Boolean.FALSE)));
        regElementExtBuilder.fetchPath(DQLJoinType.inner, SppRegElementExt.registryElement().fromAlias("elExt"), "el");
        Set<EppRegistryElement> disciplinesWithoutCheckSet = getList(regElementExtBuilder)
                .stream().distinct().map(o -> (SppRegElementExt)o).map(SppRegElementExt::getRegistryElement)
                .collect(Collectors.toSet());

        for (Date date : dateList)
        {
            List<EppRegistryElement> duplicateDisciplineList = eventList.stream()
                    .filter(e -> CoreDateUtils.getDayFirstTimeMoment(e.getDate()).equals(CoreDateUtils.getDayFirstTimeMoment(date)))
                    .filter(e -> e.getSubjectVal() != null)
                    .map(SppScheduleSessionEvent::getSubjectVal)
                    .filter(d -> !disciplinesWithoutCheckSet.contains(d))
                    .collect(Collectors.groupingBy(d -> d, Collectors.counting()))
                    .entrySet().stream()
                    .filter(p -> p.getValue() > 1)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
            for (EppRegistryElement discipline : duplicateDisciplineList)
            {
                addWarningMessage(schedule, "ПРЕДУПРЕЖДЕНИЕ: дисциплина «" + discipline.getTitle() + "»" +
                        " присутствует " + DateFormatter.DEFAULT_DATE_FORMATTER.format(date) + " несколько раз");
            }
        }
    }

    private void addWarningMessage(SppScheduleSession schedule, String message)
    {
        SppScheduleSessionWarningLog log = new SppScheduleSessionWarningLog();
        log.setWarningMessage(message);
        log.setSppScheduleSession(schedule);
        save(log);
    }

    private void updateWarningLog(Long scheduleId, List<SppScheduleSessionEvent> eventList)
    {
        DQLDeleteBuilder dBuilder = new DQLDeleteBuilder(SppScheduleSessionWarningLog.class);
        dBuilder.where(eq(property(SppScheduleSessionWarningLog.sppScheduleSession().id()), value(scheduleId)));
        dBuilder.createStatement(getSession()).execute();

        checkDayDoubleDiscipline(scheduleId, eventList);
        checkLectureRoomCapacity(scheduleId, eventList);
        checkLectureRoomFree(scheduleId, eventList);
        checkTeacherPreference(scheduleId, eventList);
        checkDisciplinePreference(scheduleId, eventList);
    }


    @Override
    public boolean getScheduleDataOrRecipientsChanged(Long scheduleId)
    {
    /*    SppScheduleSession schedule = DataAccessServices.dao().get(scheduleId);
        if(null == schedule.getIcalData()) return true;
        if(schedule.getIcalData().isChanged()) return true;

        boolean recipientsChanged = false;
        List<String> currentNsiIds =
                SppScheduleManager.instance().eventsDao().getCurrentNsiIds(schedule.getGroup().getId());
//                getCurrentTestNsiIds();

        SppScheduleICalXST scheduleICalXST = (SppScheduleICalXST) SppScheduleManager.instance().xStream().fromXML(schedule.getIcalData().getXml());
        List<String> oldNsiIds = scheduleICalXST.getStudentsNSIIds();

        for(String guid : currentNsiIds)
        {
            if(!oldNsiIds.contains(guid))
            {
                recipientsChanged = true;
                break;
            }
        }
        if(recipientsChanged) return true;

        for(String guid : oldNsiIds)
        {
            if(!currentNsiIds.contains(guid))
            {
                recipientsChanged = true;
                break;
            }
        }

        return recipientsChanged;
        */
        return false;
    }

    @Override
    public void createOrUpdateEvent(SppScheduleSessionEvent event)
    {
        SppScheduleSessionSeason season = event.getSchedule().getSeason();
        if (event.getDate().before(season.getStartDate()))
            throw new ApplicationException("Событие не может быть раньше чем дата начала периода расписания");
        if (event.getDate().after(season.getEndDate()))
            throw new ApplicationException("Событие не может быть позже чем дата окончания периода расписания");
        saveOrUpdate(event);
    }

    @Override
    public void deleteSchedule(Long scheduleId)
    {
        delete(scheduleId);  // TODO:!!!
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void sendICalendar(Long scheduleId)
    {
        /*
        NamedSyncInTransactionCheckLocker.register(getSession().getTransaction(), "fefu.schedule.session.ws.export.lock_" + scheduleId, 300);
        if(null == ApplicationRuntime.getProperty(FefuSubmitEventsClient.PORTAL_SERVICE_EVENTS_URL))
        {
            throw new ApplicationException("Приложение не настроено для работы с веб-сервисом.");
        }
        SppScheduleSession schedule = DataAccessServices.dao().get(scheduleId);
        // Проверяем изменился ли список получателей для данного расписания
        List<String> guidsForAdd = Lists.newArrayList();
        List<String> guidsForCancel = Lists.newArrayList();
        List<String> currentNsiIds =
                SppScheduleManager.instance().eventsDao().getCurrentNsiIds(schedule.getGroup().getId());
//                getCurrentTestNsiIds();

        if(null != schedule.getIcalData())
        {
            SppScheduleICal iCalData = schedule.getIcalData();
            SppScheduleICalXST scheduleICalXST = (SppScheduleICalXST) SppScheduleManager.instance().xStream().fromXML(iCalData.getXml());

            List<String> previousNsiIds = scheduleICalXST.getStudentsNSIIds();

            for(String guid : previousNsiIds)
            {
                if(!currentNsiIds.contains(guid))
                    guidsForCancel.add(guid);
            }

            for(String guid : currentNsiIds)
            {
                if(!previousNsiIds.contains(guid))
                    guidsForAdd.add(guid);
            }
        }

        boolean recipientsNotChanged = guidsForAdd.isEmpty() && guidsForCancel.isEmpty();

        if(null != schedule.getIcalData() && !schedule.getIcalData().isChanged() && recipientsNotChanged) return;

        if(!recipientsNotChanged && (!schedule.getIcalData().isChanged() || currentNsiIds.isEmpty()))
        {
            sendICalendarWithChangesRecipientList(schedule, guidsForAdd, guidsForCancel, currentNsiIds);
            return;
        }
        if(currentNsiIds.isEmpty()) return;

        DQLSelectBuilder periodBuilder = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "ev");
        periodBuilder.where(eq(property("ev", SppScheduleSessionEvent.schedule().id()), value(schedule.getId())));

        List<SppScheduleSessionEvent> events = createStatement(periodBuilder).list();

        Date endDate = schedule.getSeason().getEndDate();
        Date startDate = schedule.getSeason().getStartDate();

        Map<String, LocalDate> datesMap = SppScheduleManager.instance().eventsDao().getDatesMap(startDate, endDate);

        List<SppScheduleVEventICalXST> currentEventVEvents = getEventVEvents(events);

        if(null == schedule.getIcalData())
        {
            if(currentEventVEvents.isEmpty()) return;
            sendICalendarWithNullICallData(schedule, currentEventVEvents, currentNsiIds);
        }
        else
        {
            sendICalendarWithChangedICallData(schedule, currentEventVEvents, guidsForAdd, guidsForCancel, currentNsiIds);
        }
        */
    }


    @Override
    public SppScheduleSessionPrintForm savePrintForm(SppScheduleSessionPrintParams printData)
    {
        SppScheduleSessionPrintForm printForm = new SppScheduleSessionPrintForm();
        printForm.setCreateDate(new Date());
        printForm.setFormativeOrgUnit(printData.getFormativeOrgUnit());
//        printForm.setTerritorialOrgUnit(printData.getTerritorialOrgUnit()); // перенесено в ДВФУ
        if (null != printData.getCourseList() && !printData.getCourseList().isEmpty())
            printForm.setCourses(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getCourseList(), Course.title()), ", "));
        printForm.setSeason(printData.getSeason());
        if (null != printData.getEduLevels() && !printData.getEduLevels().isEmpty())
            printForm.setEduLevels(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getEduLevels(), EducationLevelsHighSchool.printTitle()), ", "));
//        printForm.setTerm(printForm.getTerm()); // перенесено в ДВФУ
        printForm.setChief(printData.getChief());
//        printForm.setChiefUMU(printData.getChiefUMU()); // перенесено в ДВФУ
//        printForm.setAdminOOP(printData.getAdmin()); // перенесено в ДВФУ
//        String headers = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getHeaders(), EmployeePost.person().identityCard().iof()), ", ");
//        printForm.setHeadersOOP(headers.length() > 255 ? headers.substring(0, 255) : headers); // перенесено в ДВФУ
        printForm.setCreateOU(printData.getCurrentOrgUnit());
        if (null != printData.getSchedules() && !printData.getSchedules().isEmpty())
            printForm.setGroups(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getSchedules(), SppScheduleSession.group().title()), ", "));
        else
            printForm.setGroups("-");
        printForm.setEduYear(printData.getEducationYear().getTitle());
        List<SppScheduleSessionGroupPrintVO> schedules = Lists.newArrayList();
        Map<SppScheduleSession, List<SppScheduleSessionEvent>> schedulesMap = getSchedulesMap(printData);
        for (Map.Entry<SppScheduleSession, List<SppScheduleSessionEvent>> entry : schedulesMap.entrySet())
            schedules.add(prepareScheduleGroupPrintVO(entry));
        DatabaseFile content = new DatabaseFile();
        try
        {
            content.setContent(SppScheduleSessionPrintFormExcelContentBuilder.buildExcelContent(schedules, printData));
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        save(content);
        printForm.setContent(content);
        save(printForm);
        return printForm;
    }

    @Override
    public void sendICalendars()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleSession.class, "s");
        builder.column(property("s", SppScheduleSession.id()));
        builder.where(eq(property("s", SppScheduleSession.approved()), value(true)));
        builder.where(eq(property("s", SppScheduleSession.group().archival()), value(false)));
        org.joda.time.DateTime date = new org.joda.time.DateTime().withTime(0, 0, 0, 1);
        builder.where(and(
                le(property("s", SppScheduleSession.season().startDate()), valueDate(date.minusMonths(1).toDate())),
                ge(property("s", SppScheduleSession.season().endDate()), valueDate(date.toDate()))
        ));

        List<Long> fefuSchedulesIds = createStatement(builder).list();
        for (Long scheduleId : fefuSchedulesIds)
        {
            sendICalendar(scheduleId);
        }
    }

    @Override
    public void updateScheduleAsChanged(Long scheduleId)
    {
        SppScheduleSession schedule = DataAccessServices.dao().get(scheduleId);
        if (null != schedule.getIcalData())
        {
            SppScheduleICal iCal = schedule.getIcalData();
            iCal.setChanged(true);
            update(iCal);
        }

        if (null != schedule.getIcalTeacherData())
        {
            SppScheduleICal iCalTeacherData = schedule.getIcalTeacherData();
            iCalTeacherData.setChanged(true);
            update(iCalTeacherData);
        }
    }

    protected Map<SppScheduleSession, List<SppScheduleSessionEvent>> getSchedulesMap(SppScheduleSessionPrintParams printData)
    {
        Map<SppScheduleSession, List<SppScheduleSessionEvent>> schedulesMap = Maps.newHashMap();
        DQLSelectBuilder eventsBuilder = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "ev");
        eventsBuilder.fetchPath(DQLJoinType.inner, SppScheduleSessionEvent.schedule().fromAlias("ev"), "schedule");
        eventsBuilder.where(eq(property("ev", SppScheduleSessionEvent.schedule().status().code()), value(SppScheduleStatusCodes.APPROVED)));
        if (null != printData.getSchedules() && !printData.getSchedules().isEmpty())
        {
            eventsBuilder.where(in(property("ev", SppScheduleSessionEvent.schedule()), printData.getSchedules()));
        } else
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g");
            subBuilder.distinct();

            if (null != printData.getCurrentOrgUnit())
            {
                Long orgUnitId = printData.getCurrentOrgUnit().getId();
                DQLSelectBuilder kindRelationsBuilder = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                        .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                        .where(eq(property("rel", OrgUnitToKindRelation.orgUnit().id()), value(orgUnitId)));
                List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

                Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                        .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

                IDQLExpression condition = nothing(); // false
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                    condition = or(condition, eq(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().educationLevelHighSchool().orgUnit().id()), value(orgUnitId)));
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                    condition = or(condition, eq(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().formativeOrgUnit().id()), value(orgUnitId)));
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                    condition = or(condition, eq(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().territorialOrgUnit().id()), value(orgUnitId)));

                eventsBuilder.where(condition);
            }
            eventsBuilder.where(eq(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().formativeOrgUnit()), value(printData.getFormativeOrgUnit())));
            if (null != printData.getEduLevels() && !printData.getEduLevels().isEmpty())
                eventsBuilder.where(in(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().educationLevelHighSchool()), printData.getEduLevels()));
            eventsBuilder.where(eq(property("ev", SppScheduleSessionEvent.schedule().group().educationOrgUnit().developForm()), value(printData.getDevelopForm())));
        }

        for (SppScheduleSessionEvent event : createStatement(eventsBuilder).<SppScheduleSessionEvent>list())
        {
            if (!schedulesMap.containsKey(event.getSchedule()))
                schedulesMap.put(event.getSchedule(), Lists.<SppScheduleSessionEvent>newArrayList());
            if (!schedulesMap.get(event.getSchedule()).contains(event))
                schedulesMap.get(event.getSchedule()).add(event);
        }

        return schedulesMap;
    }

    public SppScheduleSessionGroupPrintVO prepareScheduleGroupPrintVO(Map.Entry<SppScheduleSession, List<SppScheduleSessionEvent>> scheduleEntry)
    {
        SppScheduleSessionGroupPrintVO groupPrintVO = new SppScheduleSessionGroupPrintVO(scheduleEntry.getKey());

        List<SppScheduleSessionEvent> events = scheduleEntry.getValue();
        for (SppScheduleSessionEvent event : events)
        {
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(event.getDate());
            LocalDate date = new LocalDate().withYear(calendar.get(Calendar.YEAR)).withMonthOfYear(calendar.get(Calendar.MONTH) + 1).withDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
            if (!groupPrintVO.getEventsMap().containsKey(date))
                groupPrintVO.getEventsMap().put(date, Lists.<SppScheduleSessionEvent>newArrayList());
            if (!groupPrintVO.getEventsMap().get(date).contains(event))
                groupPrintVO.getEventsMap().get(date).add(event);
        }
        int columns = getColumns(groupPrintVO.getEventsMap());

        groupPrintVO.setColumnNum(columns);

        return groupPrintVO;
    }

    private int getColumns(Map<LocalDate, List<SppScheduleSessionEvent>> eventsMap)
    {
        int columns = 0;
        for (Map.Entry<LocalDate, List<SppScheduleSessionEvent>> entry : eventsMap.entrySet())
        {
            if (null != entry.getValue() && !entry.getValue().isEmpty() && entry.getValue().size() > columns)
                columns = entry.getValue().size();
        }
        return columns;
    }

    @Override
    public List<SppScheduleSessionEventExt> getSppScheduleSessionEventExt(SppScheduleSessionEvent event)
    {
        return getList(SppScheduleSessionEventExt.class, SppScheduleSessionEventExt.sppScheduleSessionEvent(), event);
    }

    @Override
    public void saveSppScheduleSessionEventExtList(SppScheduleSessionEvent event, Collection<SppScheduleSessionEventExt> eventExtList)
    {
        //delete all exist records
        final int res = new DQLDeleteBuilder(SppScheduleSessionEventExt.class)
                .where(eq(property(SppScheduleSessionEventExt.sppScheduleSessionEvent()), value(event)))
                .createStatement(getSession()).execute();
        //save new records
        eventExtList.forEach(this::saveOrUpdate);
    }

    /*
    private void sendICalendarWithChangesRecipientList(SppScheduleSession schedule, List<String> guidsForAdd, List<String> guidsForCancel, List<String> currentNsiIds)
    {

        SppScheduleICal iCalData = schedule.getIcalData();
        SppScheduleICalXST scheduleICalXST = (SppScheduleICalXST) SppScheduleManager.instance().xStream().fromXML(iCalData.getXml());
        SppScheduleICalCommits cancelRequest = getVEventsMap(scheduleICalXST.getEvents());

        if(!guidsForCancel.isEmpty())
        {
            int sequence = iCalData.getSequence();
            String iCalendar = getICalendar(scheduleICalXST.getEvents(), schedule, ++sequence, true);
            cancelRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), schedule.getGroup().getTitle(), guidsForCancel, iCalendar, cancelRequest);
            if(!cancelRequest.isRequestFailed())
            {
                iCalData.setSequence(sequence);
                baseUpdate(iCalData);
            }
        }
        else
        {
            cancelRequest.setRequestFailed(false);
        }

        if(cancelRequest.isRequestFailed()) return;


        SppScheduleICalCommits publishRequest = getVEventsMap(scheduleICalXST.getEvents());
        if(!guidsForAdd.isEmpty())
        {
            int sequence = iCalData.getSequence();
            String iCalendar = getICalendar(scheduleICalXST.getEvents(), schedule, ++sequence, false);
            publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), schedule.getGroup().getTitle(), currentNsiIds, iCalendar, publishRequest);
            if(!publishRequest.isRequestFailed())
            {
                iCalData.setSequence(sequence);
                baseUpdate(iCalData);
            }
            else return;
        }
        scheduleICalXST.setStudentsNSIIds(currentNsiIds);
        scheduleICalXST.setEvents(publishRequest.getCommitedVEvents());
        iCalData.setXml(SppScheduleManager.instance().xStream().toXML(scheduleICalXST));
        baseUpdate(iCalData);

    }


    private SppScheduleICalCommits getVEventsMap(List<SppScheduleVEventICalXST> events)
    {
        SppScheduleICalCommits iCalCommits = new SppScheduleICalCommits();
        Map<String, SppScheduleVEventICalXST> vEventsMap = Maps.newHashMap();
        for(SppScheduleVEventICalXST vEvent : events)
        {
            vEventsMap.put(vEvent.getUidPrefix() + "@" + vEvent.getPairId(), vEvent);
        }
        iCalCommits.setvEventsMap(vEventsMap);
        iCalCommits.setCommitedVEvents(Lists.<SppScheduleVEventICalXST>newArrayList());
        iCalCommits.setFailedVEvents(Lists.<SppScheduleVEventICalXST>newArrayList());
        return iCalCommits;
    }


    private String getICalendar(List<SppScheduleVEventICalXST> vEvents, SppScheduleSession schedule, int sequence, boolean cancel)
    {
//        List<SppScheduleVEventICalXST> vEvents = scheduleICalXST.getEvents();

        // Create a calendar
        net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
        icsCalendar.getProperties().add(Version.VERSION_2_0);
        icsCalendar.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
        Method method = cancel ? Method.CANCEL : Method.PUBLISH;
        icsCalendar.getProperties().add(CalScale.GREGORIAN);
        icsCalendar.getProperties().add(method);

        TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
        net.fortuna.ical4j.model.TimeZone timezone = registry.getTimeZone(java.util.Calendar.getInstance().getTimeZone().getID());
        VTimeZone tz = timezone.getVTimeZone();

        for(SppScheduleVEventICalXST vEvent : vEvents)
        {
            // Create the event
            String eventName = vEvent.getSubject();
            DateTime start = new DateTime(vEvent.getStartDate());
            Calendar c = GregorianCalendar.getInstance();
            c.setTime(vEvent.getStartDate());
            c.add(Calendar.HOUR_OF_DAY, 2);
            DateTime end = new DateTime(c.getTime());
            VEvent meeting = new VEvent(start, end, eventName);
            Description desc = new Description((StringUtils.isEmpty(vEvent.getPeriodGroup()) ? "" : "Аудитория - " + vEvent.getLectureRoom() + " ") + "Преподаватель - " + vEvent.getTeacher());
            if(null != schedule)
            {
                Categories cat = new Categories("Расписание сессии для группы " + schedule.getGroup().getTitle());
                meeting.getProperties().add(cat);
            }
            else
            {
                Categories cat = new Categories("Отмена расписания сессии");
                meeting.getProperties().add(cat);
            }
            meeting.getProperties().add(tz.getTimeZoneId());

            meeting.getProperties().add(desc);
            Uid uid = new Uid(vEvent.getUidPrefix() + "@" + vEvent.getPairId());
            Sequence seq = new Sequence(sequence);
            meeting.getProperties().add(uid);
            meeting.getProperties().add(seq);
            icsCalendar.getComponents().add(meeting);
        }

        return icsCalendar.toString();
    }


    private List<SppScheduleVEventICalXST> getEventVEvents(List<SppScheduleSessionEvent> events)
    {
        List<SppScheduleVEventICalXST> eventVEvents = Lists.newArrayList();
        Calendar calendar = GregorianCalendar.getInstance();
        for(SppScheduleSessionEvent event : events)
        {
            calendar.setTime(event.getDate());
            SppScheduleVEventICalXST vEvent = new SppScheduleVEventICalXST();
            vEvent.setPairId(event.getId());
            vEvent.setUidPrefix(calendar.get(Calendar.DAY_OF_MONTH) + "_" + (calendar.get(Calendar.MONTH) + 1) + "_" + calendar.get(Calendar.YEAR));
            vEvent.setStartDate(event.getDate());
            vEvent.setSubject(event.getSubject());
            vEvent.setLectureRoom(event.getLectureRoom());
            vEvent.setTeacher(event.getTeacher());
            eventVEvents.add(vEvent);
        }
        return eventVEvents;
    }

    private Date getStartDate(LocalDate date, SppBellScheduleEntry bell)
    {
        org.joda.time.DateTime dateTime = date.toDateTimeAtCurrentTime();
        return dateTime.withHourOfDay(bell.getStartHour()).withMinuteOfHour(bell.getStartMin()).toDate();
    }

    private Date getEndDate(LocalDate date, SppBellScheduleEntry bell)
    {
        org.joda.time.DateTime dateTime = date.toDateTimeAtCurrentTime();
        return dateTime.withHourOfDay(bell.getEndHour()).withMinuteOfHour(bell.getEndMin()).toDate();
    }

    private void sendICalendarWithNullICallData(SppScheduleSession schedule, List<SppScheduleVEventICalXST> currentVEvents, List<String> currentNsiIds)
    {
        SppScheduleICalXST scheduleICalXST = new SppScheduleICalXST();
        scheduleICalXST.setStudentsNSIIds(currentNsiIds);
        int sequence = 1;
        String iCalendar = getICalendar(currentVEvents, schedule, sequence, false);
        SppScheduleICalCommits publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), schedule.getGroup().getTitle(), currentNsiIds, iCalendar, getVEventsMap(currentVEvents));
        if(!publishRequest.isRequestFailed())
        {
            SppScheduleICal iCalData = new SppScheduleICal();
            iCalData.setChanged(false);
            iCalData.setSequence(sequence);
            scheduleICalXST.setEvents(publishRequest.getCommitedVEvents());
            iCalData.setXml(SppScheduleManager.instance().xStream().toXML(scheduleICalXST));
            baseCreate(iCalData);
            schedule.setIcalData(iCalData);
            baseUpdate(schedule);
        }
    }

    private void sendICalendarWithChangedICallData(SppScheduleSession schedule, List<SppScheduleVEventICalXST> currentVEvents, List<String> guidsForAdd, List<String> guidsForCancel, List<String> currentNsiIds)
    {

        SppScheduleICal iCalData = schedule.getIcalData();
        SppScheduleICalXST scheduleICalXST = (SppScheduleICalXST) SppScheduleManager.instance().xStream().fromXML(iCalData.getXml());

        List<SppScheduleVEventICalXST> oldVEvents = scheduleICalXST.getEvents();

        List<SppScheduleVEventICalXST> vEventsForCancel = Lists.newArrayList();
        List<SppScheduleVEventICalXST> vEventsForAdd = Lists.newArrayList();

        Map<String, SppScheduleVEventICalXST> currentVEventsMap = Maps.newHashMap();
        Map<String, SppScheduleVEventICalXST> oldVEventsMap = Maps.newHashMap();
        for(SppScheduleVEventICalXST vEvent : currentVEvents)
        {
            currentVEventsMap.put(vEvent.getUidPrefix()+ "@" + vEvent.getPairId(), vEvent);
        }

        for(SppScheduleVEventICalXST vEvent : oldVEvents)
        {
            oldVEventsMap.put(vEvent.getUidPrefix()+ "@" + vEvent.getPairId(), vEvent);
        }

        for(String vEventUid : oldVEventsMap.keySet())
        {
            if(!currentVEventsMap.containsKey(vEventUid))
                vEventsForCancel.add(oldVEventsMap.get(vEventUid));
        }
        for(String vEventUid : currentVEventsMap.keySet())
        {
            if(!oldVEventsMap.containsKey(vEventUid))
                vEventsForAdd.add(currentVEventsMap.get(vEventUid));
        }
        int sequence = iCalData.getSequence();

        SppScheduleICalCommits cancelRecipRequest = getVEventsMap(oldVEvents);
        if(!guidsForCancel.isEmpty())
        {
            String iCalendar = getICalendar(oldVEvents, schedule, ++sequence, true);
            cancelRecipRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), schedule.getGroup().getTitle(), guidsForCancel, iCalendar, cancelRecipRequest);

        }
        else
        {
            cancelRecipRequest.setRequestFailed(false);
        }

        SppScheduleICalCommits cancelRequest = getVEventsMap(vEventsForCancel);
        if(!vEventsForCancel.isEmpty())
        {
            String iCalendar = getICalendar(vEventsForCancel, schedule, ++sequence, true);
            List<String> nsiIdsForCancel = Lists.newArrayList(currentNsiIds);
            nsiIdsForCancel.remove(guidsForCancel);
            cancelRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), schedule.getGroup().getTitle(), nsiIdsForCancel, iCalendar, cancelRequest);
        }
        else cancelRequest.setRequestFailed(false);


        if(currentVEvents.isEmpty())
        {
            scheduleICalXST.setEvents(currentVEvents);
            scheduleICalXST.setStudentsNSIIds(currentNsiIds);
            iCalData.setSequence(sequence);
            iCalData.setXml(SppScheduleManager.instance().xStream().toXML(scheduleICalXST));
            iCalData.setChanged(false);
            baseUpdate(iCalData);
            return;
        }
        String iCalendar = getICalendar(currentVEvents, schedule, ++sequence, false);
        SppScheduleICalCommits publishRequest = FefuSubmitEventsClient.sendEvents(schedule.getId().toString(), schedule.getGroup().getTitle(), currentNsiIds, iCalendar, getVEventsMap(currentVEvents));


        if(!cancelRecipRequest.isRequestFailed() && !cancelRequest.isRequestFailed() && !publishRequest.isRequestFailed())
        {
            scheduleICalXST.setEvents(publishRequest.getCommitedVEvents());
            scheduleICalXST.setStudentsNSIIds(currentNsiIds);
            iCalData.setSequence(sequence);
            iCalData.setXml(SppScheduleManager.instance().xStream().toXML(scheduleICalXST));
            iCalData.setChanged(false);
            baseUpdate(iCalData);
        }
    }
    */
}
