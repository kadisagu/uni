/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;

/**
 * @author vnekrasov
 * @since 6/23/14
 */
@Configuration
public class SppTeacherPreferenceAddEdit extends BusinessComponentManager
{
    public static final String SEASON_DS = "seasonDS";
    public static final String BELLS_DS = "bellsDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(SEASON_DS, SppTeacherPreferenceManager.instance().seasonComboDSHandler()).addColumn("title", null, new IFormatter<SppScheduleSeason>()
                {
                    @Override
                    public String format(SppScheduleSeason source)
                    {
                        return source.getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getStartDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndDate()) + ")";
                    }
                }))
                .addDataSource(selectDS(BELLS_DS, SppTeacherPreferenceManager.instance().bellsComboDSHandler()))
                .create();
    }
}
