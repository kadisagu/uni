/* $Id: SppScheduleDailyView.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyEventDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyWarningLogDSHandler;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyWarningLog;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@Configuration
public class SppScheduleDailyView extends BusinessComponentManager
{
    public final static String SCHEDULE_EVENTS_DS = "eventsDS";
    public final static String WARNING_LOG_DS = "warningLogDS";

    @Bean
    public ColumnListExtPoint scheduleEventsCL()
    {
        return columnListExtPointBuilder(SCHEDULE_EVENTS_DS)
                .addColumn(dateColumn("date", SppScheduleDailyEvent.date()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().required(true))
                .addColumn(textColumn("bell", SppScheduleDailyEvent.bell().time()).required(true))
                .addColumn(textColumn("lectureRoom", SppScheduleDailyEvent.lectureRoom()))
                .addColumn(textColumn("subject", SppScheduleDailyEvent.subject()))
                .addColumn(textColumn("teacher", SppScheduleDailyEvent.teacher()))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onClickEditEvent").permissionKey("sppScheduleDailyViewEdit").disabled("ui:eventEditDisabled"))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDeleteEvent").permissionKey("sppScheduleDailyViewEdit").disabled("ui:eventDeleteDisabled"))
                .create();
    }

    @Bean
    public ColumnListExtPoint warningLogCL()
    {
        return columnListExtPointBuilder(WARNING_LOG_DS)
                .addColumn(textColumn("warningMessage", SppScheduleDailyWarningLog.warningMessage()))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SCHEDULE_EVENTS_DS, scheduleEventsCL(), scheduleEventsDSHandler()))
                .addDataSource(searchListDS(WARNING_LOG_DS, warningLogCL(), warningLogDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler warningLogDSHandler()
    {
        return new SppScheduleDailyWarningLogDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler scheduleEventsDSHandler()
    {
        return new SppScheduleDailyEventDSHandler(getName());
    }
}
