/* $Id: SppScheduleDailyOrgUnitPrintFormListUI.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.LearningProcessPrintFormList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyPrintFormDSHandler;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
public class SppScheduleDailyLearningProcessPrintFormListUI extends UIPresenter
{
    private BaseSearchListDataSource _printFormDS;

    public BaseSearchListDataSource getPrintFormDS()
    {
        if(null == _printFormDS)
            _printFormDS = _uiConfig.getDataSource(SppScheduleDailyLearningProcessPrintFormList.PRINT_FORM_DS);
        return _printFormDS;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleDailyLearningProcessPrintFormList.PRINT_FORM_DS.equals(dataSource.getName()))
        {
            //dataSource.put("orgUnitId", getOrgUnit().getId());
            dataSource.put(SppScheduleDailyPrintFormDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
            dataSource.putAll(_uiSettings.getAsMap(true, "groups", "orgUnitIds"));
        }

        if (SppScheduleDailyLearningProcessPrintFormList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put("orgUnitIds", _uiSettings.get("orgUnitIds"));
        }
    }

    public void onClickDownload()
    {
        SppScheduleDailyPrintForm printForm = getPrintFormDS().getRecordById(getListenerParameterAsLong());
        byte[] content = printForm.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("xls").fileName("schedule.xls").document(content), true);
    }

}
