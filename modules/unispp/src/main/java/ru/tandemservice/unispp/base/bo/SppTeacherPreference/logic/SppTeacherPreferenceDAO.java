/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.logic;

import com.google.common.collect.Maps;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.*;
import ru.tandemservice.unispp.base.vo.*;

import java.time.DayOfWeek;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 6/23/14
 */
@Transactional
public class SppTeacherPreferenceDAO extends UniBaseDao implements ISppTeacherPreferenceDAO
{
    @Override
    public List<ScheduleBellEntry> getBells(Long bellsId)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ScheduleBellEntry.class, "b");
        builder.where(eq(property("b", ScheduleBellEntry.schedule().id()), value(bellsId)));
        builder.order(property("b", ScheduleBellEntry.number()), OrderDirection.asc);

        return builder.createStatement(getSession()).list();
    }

    @Override
    public void saveOrUpdateTeacherPreferenceList(SppTeacherPreferenceList teacherPreference, ScheduleBell oldBells, boolean oldDiffEven)
    {
        boolean deleteAndCreateStructure = false;
        if (null == teacherPreference.getId()) deleteAndCreateStructure = true;
        if (null != oldBells && !oldBells.equals(teacherPreference.getBells())) deleteAndCreateStructure = true;

        // при изменении флага "Предпочтения для четной и нечетной недель" нужно либо копировать предпочтения в чётную либо убрать чётную
        boolean copyToEven = !deleteAndCreateStructure && !oldDiffEven && teacherPreference.isDiffEven();
        boolean deleteEven = !deleteAndCreateStructure && oldDiffEven && !teacherPreference.isDiffEven();

        saveOrUpdate(teacherPreference);
        if (deleteAndCreateStructure)
        {
            DQLDeleteBuilder deleteWeekRowsBuilder = new DQLDeleteBuilder(SppTeacherPreferenceWeekRow.class);
            deleteWeekRowsBuilder.where(eq(property(SppTeacherPreferenceWeekRow.teacherPreference().id()), value(teacherPreference.getId())));
            deleteWeekRowsBuilder.createStatement(getSession()).execute();

            List<ScheduleBellEntry> bells = getBells(teacherPreference.getBells().getId());
            for (ScheduleBellEntry bell : bells)
            {
                SppTeacherPreferenceWeekRow oddWeekRow = new SppTeacherPreferenceWeekRow();
                oddWeekRow.setTeacherPreference(teacherPreference);
                oddWeekRow.setBell(bell);
                oddWeekRow.setEven(false);
                save(oddWeekRow);

                if (teacherPreference.isDiffEven())
                {
                    SppTeacherPreferenceWeekRow evenWeekRow = new SppTeacherPreferenceWeekRow();
                    evenWeekRow.setTeacherPreference(teacherPreference);
                    evenWeekRow.setBell(bell);
                    evenWeekRow.setEven(true);
                    save(evenWeekRow);
                }
            }
        }
        else if (deleteEven)
        {
            // если флаг был выключен, то нужно просто удалить чётную неделю
            DQLDeleteBuilder deleteWeekRowsBuilder = new DQLDeleteBuilder(SppTeacherPreferenceWeekRow.class);
            deleteWeekRowsBuilder.where(eq(property(SppTeacherPreferenceWeekRow.teacherPreference().id()), value(teacherPreference.getId())));
            deleteWeekRowsBuilder.where(eq(property(SppTeacherPreferenceWeekRow.even()), value(Boolean.TRUE)));
            deleteWeekRowsBuilder.createStatement(getSession()).execute();
        }
        else if (copyToEven)
        {
            // если флаг был включен, то нужно копировать все предпочтения в чётную неделю
            DQLSelectBuilder selectPreferenceElement = new DQLSelectBuilder();
            selectPreferenceElement.fromEntity(SppTeacherPreferenceElement.class, "el");
            selectPreferenceElement.fetchPath(DQLJoinType.inner, SppTeacherPreferenceElement.weekRow().bell().fromAlias("el"), "bell");
            selectPreferenceElement.where(eq(property("el", SppTeacherPreferenceElement.weekRow().teacherPreference().id()), value(teacherPreference.getId())));
            Map<ScheduleBellEntry, List<SppTeacherPreferenceElement>> elementsMap = getList(selectPreferenceElement).stream()
                    .map(obj -> (SppTeacherPreferenceElement) obj)
                    .collect(Collectors.groupingBy(el -> el.getWeekRow().getBell(), Collectors.mapping(el -> el, Collectors.toList())));

            List<ScheduleBellEntry> bells = getBells(teacherPreference.getBells().getId());
            for (ScheduleBellEntry bell : bells)
            {
                SppTeacherPreferenceWeekRow evenWeekRow = new SppTeacherPreferenceWeekRow();
                evenWeekRow.setTeacherPreference(teacherPreference);
                evenWeekRow.setBell(bell);
                evenWeekRow.setEven(true);
                save(evenWeekRow);

                List<SppTeacherPreferenceElement> elements = elementsMap.getOrDefault(bell, Collections.emptyList());
                for (SppTeacherPreferenceElement element : elements)
                {
                    SppTeacherPreferenceElement evenElement = new SppTeacherPreferenceElement();
                    evenElement.setDayOfWeek(element.getDayOfWeek());
                    evenElement.setWeekRow(evenWeekRow);
                    evenElement.setBuilding(element.getBuilding());
                    evenElement.setLectureRoom(element.getLectureRoom());
                    evenElement.setUnwantedTime(element.isUnwantedTime());
                    save(evenElement);
                }
            }
        }
    }

    @Override
    public void saveOrUpdateTeacherSessionPreferenceList(SppTeacherSessionPreferenceList teacherPreference, ScheduleBell oldBells)
    {
        boolean clearPreferences = false;
        if (null != oldBells && !oldBells.equals(teacherPreference.getBells())) clearPreferences = true;
        if (clearPreferences)
        {
            DQLDeleteBuilder deletePreferences = new DQLDeleteBuilder(SppTeacherSessionPreferenceElement.class);
            deletePreferences.where(eq(property(SppTeacherSessionPreferenceElement.teacherPreference().id()), value(teacherPreference.getId())));
            deletePreferences.createStatement(getSession()).execute();
        }
        saveOrUpdate(teacherPreference);
    }

    @Override
    public void saveOrUpdateTeacherDailyPreferenceList(SppTeacherDailyPreferenceList teacherPreference, ScheduleBell oldBells)
    {
        boolean clearPreference = false;
        if (null != oldBells && !oldBells.equals(teacherPreference.getBells())) clearPreference = true;
        if (clearPreference)
        {
            DQLDeleteBuilder deletePreferences = new DQLDeleteBuilder(SppTeacherDailyPreferenceElement.class);
            deletePreferences.where(eq(property(SppTeacherDailyPreferenceElement.teacherPreference().id()), value(teacherPreference.getId())));
            deletePreferences.createStatement(getSession()).execute();
        }
        saveOrUpdate(teacherPreference);
    }

    @Override
    public void deleteTeacherPreferenceList(Long teacherPreferenceId)
    {
        IEntity preferenceList = get(teacherPreferenceId);
        DQLSelectBuilder selectElementBuilder = new DQLSelectBuilder();
        String alias = "pr";
        int count = 0;
        if (preferenceList instanceof SppTeacherPreferenceList)
        {
            selectElementBuilder.fromEntity(SppTeacherPreferenceElement.class, alias);
            selectElementBuilder.where(eq(property(SppTeacherPreferenceElement.weekRow().teacherPreference().id().fromAlias(alias)), value(teacherPreferenceId)));
            count = getCount(selectElementBuilder);
        }
        else if (preferenceList instanceof SppTeacherDailyPreferenceList)
        {
            selectElementBuilder.fromEntity(SppTeacherDailyPreferenceElement.class, alias);
            selectElementBuilder.where(eq(property(SppTeacherDailyPreferenceElement.teacherPreference().id().fromAlias(alias)), value(teacherPreferenceId)));
            count = getCount(selectElementBuilder);
        }
        else if (preferenceList instanceof SppTeacherSessionPreferenceList)
        {
            selectElementBuilder.fromEntity(SppTeacherSessionPreferenceElement.class, alias);
            selectElementBuilder.where(eq(property(SppTeacherSessionPreferenceElement.teacherPreference().id().fromAlias(alias)), value(teacherPreferenceId)));
            count = getCount(selectElementBuilder);
        }
        if (count > 0) throw new ApplicationException("Нельзя удалять непустой список предпочтений");

        delete(teacherPreferenceId);
    }

    @Override
    public SppTeacherPreferenceVO prepareTeacherPreferenceVO(SppTeacherPreferenceList teacherPreference)
    {
        SppTeacherPreferenceVO teacherPreferenceVO = new SppTeacherPreferenceVO(teacherPreference);
        SppTeacherPreferenceWeekVO oddWeek = new SppTeacherPreferenceWeekVO();
        SppTeacherPreferenceWeekVO evenWeek = new SppTeacherPreferenceWeekVO();
        teacherPreferenceVO.setOddWeek(oddWeek);
        teacherPreferenceVO.setEvenWeek(evenWeek);

        Map<ScheduleBellEntry, SppTeacherPreferenceWeekRowVO> oddWeekPeriodsRowMap = Maps.newHashMap();
        Map<ScheduleBellEntry, SppTeacherPreferenceWeekRowVO> evenWeekPeriodsRowMap = Maps.newHashMap();

        List<SppTeacherPreferenceWeekRow> weekRows = DataAccessServices.dao().getList(SppTeacherPreferenceWeekRow.class, SppTeacherPreferenceWeekRow.teacherPreference().id(), teacherPreference.getId());

        for (SppTeacherPreferenceWeekRow weekRow : weekRows)
        {
            if (weekRow.isEven())
                evenWeekPeriodsRowMap.put(weekRow.getBell(), new SppTeacherPreferenceWeekRowVO(weekRow));
            else
                oddWeekPeriodsRowMap.put(weekRow.getBell(), new SppTeacherPreferenceWeekRowVO(weekRow));
        }

        fillDays(oddWeekPeriodsRowMap);
        fillDays(evenWeekPeriodsRowMap);

        oddWeek.setWeekPeriodsMap(oddWeekPeriodsRowMap);
        evenWeek.setWeekPeriodsMap(evenWeekPeriodsRowMap);

        List<SppTeacherPreferenceElement> periods = DataAccessServices.dao().getList(SppTeacherPreferenceElement.class, SppTeacherPreferenceElement.weekRow().teacherPreference().id(), teacherPreference.getId());
        for (SppTeacherPreferenceElement period : periods)
        {
            SppTeacherPreferencePeriodVO periodVO = new SppTeacherPreferencePeriodVO(period);
            if (!period.getWeekRow().isEven())
                addPeriod(oddWeekPeriodsRowMap, periodVO);
            else
                addPeriod(evenWeekPeriodsRowMap, periodVO);
        }
        //sortWeeks(oddWeekPeriodsRowMap, evenWeekPeriodsRowMap);

        return teacherPreferenceVO;
    }

    private int getColumns(Map<ScheduleBellEntry, Map<Boolean, List<SppTeacherPreferenceElement>>> day, int i)
    {
        for (ScheduleBellEntry bell : day.keySet())
        {
            for (Boolean isEven : day.get(bell).keySet())
            {
                if (day.get(bell).get(isEven).size() > i) i = day.get(bell).get(isEven).size();
            }
        }
        return i;
    }

    private void fillDays(Map<ScheduleBellEntry, SppTeacherPreferenceWeekRowVO> weekPeriodsRowMap)
    {
        for (SppTeacherPreferenceWeekRowVO weekPeriodsRowVO : weekPeriodsRowMap.values())
        {
            weekPeriodsRowVO.setMonday(getNewSppTeacherPreferencePeriodsVO(weekPeriodsRowVO));
            weekPeriodsRowVO.setTuesday(getNewSppTeacherPreferencePeriodsVO(weekPeriodsRowVO));
            weekPeriodsRowVO.setWednesday(getNewSppTeacherPreferencePeriodsVO(weekPeriodsRowVO));
            weekPeriodsRowVO.setThursday(getNewSppTeacherPreferencePeriodsVO(weekPeriodsRowVO));
            weekPeriodsRowVO.setFriday(getNewSppTeacherPreferencePeriodsVO(weekPeriodsRowVO));
            weekPeriodsRowVO.setSaturday(getNewSppTeacherPreferencePeriodsVO(weekPeriodsRowVO));
            weekPeriodsRowVO.setSunday(getNewSppTeacherPreferencePeriodsVO(weekPeriodsRowVO));
        }
    }

    private void addPeriod(Map<ScheduleBellEntry, SppTeacherPreferenceWeekRowVO> weekPeriodsRowMap, SppTeacherPreferencePeriodVO periodVO)
    {
        SppTeacherPreferenceElement period = periodVO.getPeriod();
        SppTeacherPreferenceWeekRowVO weekPeriodsRowVO = weekPeriodsRowMap.get(period.getWeekRow().getBell());
        periodVO.setWeekPeriodsRowVO(weekPeriodsRowVO);
        if (DayOfWeek.MONDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getMonday().getPeriods().add(periodVO);
        } else if (DayOfWeek.TUESDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getTuesday().getPeriods().add(periodVO);
        } else if (DayOfWeek.WEDNESDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getWednesday().getPeriods().add(periodVO);
        } else if (DayOfWeek.THURSDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getThursday().getPeriods().add(periodVO);
        } else if (DayOfWeek.FRIDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getFriday().getPeriods().add(periodVO);
        } else if (DayOfWeek.SATURDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getSaturday().getPeriods().add(periodVO);
        } else if (DayOfWeek.SUNDAY.getValue() == period.getDayOfWeek())
        {
            weekPeriodsRowVO.getSunday().getPeriods().add(periodVO);
        }
    }

    private SppTeacherPreferencePeriodsVO getNewSppTeacherPreferencePeriodsVO(SppTeacherPreferenceWeekRowVO weekPeriodsRowVO)
    {
        SppTeacherPreferencePeriodsVO periodsVO = new SppTeacherPreferencePeriodsVO();
        periodsVO.setWeekPeriodsRowVO(weekPeriodsRowVO);
        return periodsVO;
    }

    @Override
    public void saveOrUpdatePeriods(List<SppTeacherPreferencePeriodVO> periods)
    {
        for (SppTeacherPreferencePeriodVO periodVO : periods)
        {
            SppTeacherPreferenceElement period = periodVO.getPeriodForSave();
            saveOrUpdate(period);
        }
    }

    @Override
    public void createOrUpdateSessionElement(SppTeacherSessionPreferenceElement element)
    {
        SppScheduleSessionSeason season = element.getTeacherPreference().getSppScheduleSessionSeason();
        if (element.getStartDate().before(season.getStartDate()))
            throw new ApplicationException("Событие не может быть раньше чем дата начала периода расписания");
        if (element.getEndDate().after(season.getEndDate()))
            throw new ApplicationException("Событие не может быть позже чем дата окончания периода расписания");
        saveOrUpdate(element);
    }

    @Override
    public void createOrUpdateDailyElement(SppTeacherDailyPreferenceElement element)
    {
        SppScheduleDailySeason season = element.getTeacherPreference().getSppScheduleDailySeason();
        if (element.getStartDate().before(season.getStartDate()))
            throw new ApplicationException("Событие не может быть раньше чем дата начала периода расписания");
        if (element.getEndDate().after(season.getEndDate()))
            throw new ApplicationException("Событие не может быть позже чем дата окончания периода расписания");
        saveOrUpdate(element);
    }

    @Override
    public void createOrUpdateElement(SppTeacherPreferenceElement element)
    {
        saveOrUpdate(element);
    }

}
