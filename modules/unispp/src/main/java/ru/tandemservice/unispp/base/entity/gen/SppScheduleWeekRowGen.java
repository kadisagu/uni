package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppScheduleWeekRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Недельная строка расписания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppScheduleWeekRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppScheduleWeekRow";
    public static final String ENTITY_NAME = "sppScheduleWeekRow";
    public static final int VERSION_HASH = -1888745179;
    private static IEntityMeta ENTITY_META;

    public static final String P_EVEN = "even";
    public static final String L_BELL = "bell";
    public static final String L_SCHEDULE = "schedule";

    private boolean _even;     // Четная неделя
    private ScheduleBellEntry _bell;     // Пара
    private SppSchedule _schedule;     // Расписание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Четная неделя. Свойство не может быть null.
     */
    @NotNull
    public boolean isEven()
    {
        return _even;
    }

    /**
     * @param even Четная неделя. Свойство не может быть null.
     */
    public void setEven(boolean even)
    {
        dirty(_even, even);
        _even = even;
    }

    /**
     * @return Пара.
     */
    public ScheduleBellEntry getBell()
    {
        return _bell;
    }

    /**
     * @param bell Пара.
     */
    public void setBell(ScheduleBellEntry bell)
    {
        dirty(_bell, bell);
        _bell = bell;
    }

    /**
     * @return Расписание.
     */
    public SppSchedule getSchedule()
    {
        return _schedule;
    }

    /**
     * @param schedule Расписание.
     */
    public void setSchedule(SppSchedule schedule)
    {
        dirty(_schedule, schedule);
        _schedule = schedule;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppScheduleWeekRowGen)
        {
            setEven(((SppScheduleWeekRow)another).isEven());
            setBell(((SppScheduleWeekRow)another).getBell());
            setSchedule(((SppScheduleWeekRow)another).getSchedule());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppScheduleWeekRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppScheduleWeekRow.class;
        }

        public T newInstance()
        {
            return (T) new SppScheduleWeekRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "even":
                    return obj.isEven();
                case "bell":
                    return obj.getBell();
                case "schedule":
                    return obj.getSchedule();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "even":
                    obj.setEven((Boolean) value);
                    return;
                case "bell":
                    obj.setBell((ScheduleBellEntry) value);
                    return;
                case "schedule":
                    obj.setSchedule((SppSchedule) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "even":
                        return true;
                case "bell":
                        return true;
                case "schedule":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "even":
                    return true;
                case "bell":
                    return true;
                case "schedule":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "even":
                    return Boolean.class;
                case "bell":
                    return ScheduleBellEntry.class;
                case "schedule":
                    return SppSchedule.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppScheduleWeekRow> _dslPath = new Path<SppScheduleWeekRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppScheduleWeekRow");
    }
            

    /**
     * @return Четная неделя. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleWeekRow#isEven()
     */
    public static PropertyPath<Boolean> even()
    {
        return _dslPath.even();
    }

    /**
     * @return Пара.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleWeekRow#getBell()
     */
    public static ScheduleBellEntry.Path<ScheduleBellEntry> bell()
    {
        return _dslPath.bell();
    }

    /**
     * @return Расписание.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleWeekRow#getSchedule()
     */
    public static SppSchedule.Path<SppSchedule> schedule()
    {
        return _dslPath.schedule();
    }

    public static class Path<E extends SppScheduleWeekRow> extends EntityPath<E>
    {
        private PropertyPath<Boolean> _even;
        private ScheduleBellEntry.Path<ScheduleBellEntry> _bell;
        private SppSchedule.Path<SppSchedule> _schedule;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Четная неделя. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleWeekRow#isEven()
     */
        public PropertyPath<Boolean> even()
        {
            if(_even == null )
                _even = new PropertyPath<Boolean>(SppScheduleWeekRowGen.P_EVEN, this);
            return _even;
        }

    /**
     * @return Пара.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleWeekRow#getBell()
     */
        public ScheduleBellEntry.Path<ScheduleBellEntry> bell()
        {
            if(_bell == null )
                _bell = new ScheduleBellEntry.Path<ScheduleBellEntry>(L_BELL, this);
            return _bell;
        }

    /**
     * @return Расписание.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleWeekRow#getSchedule()
     */
        public SppSchedule.Path<SppSchedule> schedule()
        {
            if(_schedule == null )
                _schedule = new SppSchedule.Path<SppSchedule>(L_SCHEDULE, this);
            return _schedule;
        }

        public Class getEntityClass()
        {
            return SppScheduleWeekRow.class;
        }

        public String getEntityName()
        {
            return "sppScheduleWeekRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
