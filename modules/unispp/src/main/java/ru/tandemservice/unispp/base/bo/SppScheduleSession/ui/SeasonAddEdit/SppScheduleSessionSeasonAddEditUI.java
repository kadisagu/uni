/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.SeasonAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;

/**
 * @author vnekrasov
 * @since 12/26/13
 */
@Input({
        @Bind(key= UIPresenter.PUBLISHER_ID, binding="seasonId")
})
public class SppScheduleSessionSeasonAddEditUI extends UIPresenter
{
    private Long _seasonId;
    private SppScheduleSessionSeason _season;

    public Long getSeasonId()
    {
        return _seasonId;
    }

    public void setSeasonId(Long seasonId)
    {
        _seasonId = seasonId;
    }

    public SppScheduleSessionSeason getSeason()
    {
        return _season;
    }

    public void setSeason(SppScheduleSessionSeason season)
    {
        _season = season;
    }

    public Boolean getAddForm()
    {
        return null == _seasonId;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null != _seasonId && null == _season)
            _season = DataAccessServices.dao().getNotNull(_seasonId);
        else
            _season = new SppScheduleSessionSeason();
    }

    public void onClickApply()
    {
        if(_season.getStartDate().after(_season.getEndDate()))
            _uiSupport.error("Поле «Дата начала» не может быть позже поля «Дата окончания»", "startDate", "endDate");
        if(getUserContext().getErrorCollector().hasErrors())
            return;
        SppScheduleSessionManager.instance().dao().createOrUpdateSeason(_season);
        deactivate();
    }
}
