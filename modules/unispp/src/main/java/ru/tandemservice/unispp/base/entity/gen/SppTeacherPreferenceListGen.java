package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список предпочтений преподавателя по расписанию
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppTeacherPreferenceListGen extends EntityBase
 implements INaturalIdentifiable<SppTeacherPreferenceListGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList";
    public static final String ENTITY_NAME = "sppTeacherPreferenceList";
    public static final int VERSION_HASH = -1177801467;
    private static IEntityMeta ENTITY_META;

    public static final String L_TEACHER = "teacher";
    public static final String L_SPP_SCHEDULE_SEASON = "sppScheduleSeason";
    public static final String P_TITLE = "title";
    public static final String L_BELLS = "bells";
    public static final String P_DIFF_EVEN = "diffEven";

    private Person _teacher;     // Преподаватель
    private SppScheduleSeason _sppScheduleSeason;     // Период расписания
    private String _title;     // Название
    private ScheduleBell _bells;     // Звонковое расписание
    private boolean _diffEven;     // Предпочтения для четной и нечетной недель

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Преподаватель. Свойство не может быть null.
     */
    @NotNull
    public Person getTeacher()
    {
        return _teacher;
    }

    /**
     * @param teacher Преподаватель. Свойство не может быть null.
     */
    public void setTeacher(Person teacher)
    {
        dirty(_teacher, teacher);
        _teacher = teacher;
    }

    /**
     * @return Период расписания. Свойство не может быть null.
     */
    @NotNull
    public SppScheduleSeason getSppScheduleSeason()
    {
        return _sppScheduleSeason;
    }

    /**
     * @param sppScheduleSeason Период расписания. Свойство не может быть null.
     */
    public void setSppScheduleSeason(SppScheduleSeason sppScheduleSeason)
    {
        dirty(_sppScheduleSeason, sppScheduleSeason);
        _sppScheduleSeason = sppScheduleSeason;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     */
    @NotNull
    public ScheduleBell getBells()
    {
        return _bells;
    }

    /**
     * @param bells Звонковое расписание. Свойство не может быть null.
     */
    public void setBells(ScheduleBell bells)
    {
        dirty(_bells, bells);
        _bells = bells;
    }

    /**
     * @return Предпочтения для четной и нечетной недель. Свойство не может быть null.
     */
    @NotNull
    public boolean isDiffEven()
    {
        return _diffEven;
    }

    /**
     * @param diffEven Предпочтения для четной и нечетной недель. Свойство не может быть null.
     */
    public void setDiffEven(boolean diffEven)
    {
        dirty(_diffEven, diffEven);
        _diffEven = diffEven;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppTeacherPreferenceListGen)
        {
            if (withNaturalIdProperties)
            {
                setTeacher(((SppTeacherPreferenceList)another).getTeacher());
                setSppScheduleSeason(((SppTeacherPreferenceList)another).getSppScheduleSeason());
            }
            setTitle(((SppTeacherPreferenceList)another).getTitle());
            setBells(((SppTeacherPreferenceList)another).getBells());
            setDiffEven(((SppTeacherPreferenceList)another).isDiffEven());
        }
    }

    public INaturalId<SppTeacherPreferenceListGen> getNaturalId()
    {
        return new NaturalId(getTeacher(), getSppScheduleSeason());
    }

    public static class NaturalId extends NaturalIdBase<SppTeacherPreferenceListGen>
    {
        private static final String PROXY_NAME = "SppTeacherPreferenceListNaturalProxy";

        private Long _teacher;
        private Long _sppScheduleSeason;

        public NaturalId()
        {}

        public NaturalId(Person teacher, SppScheduleSeason sppScheduleSeason)
        {
            _teacher = ((IEntity) teacher).getId();
            _sppScheduleSeason = ((IEntity) sppScheduleSeason).getId();
        }

        public Long getTeacher()
        {
            return _teacher;
        }

        public void setTeacher(Long teacher)
        {
            _teacher = teacher;
        }

        public Long getSppScheduleSeason()
        {
            return _sppScheduleSeason;
        }

        public void setSppScheduleSeason(Long sppScheduleSeason)
        {
            _sppScheduleSeason = sppScheduleSeason;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SppTeacherPreferenceListGen.NaturalId) ) return false;

            SppTeacherPreferenceListGen.NaturalId that = (NaturalId) o;

            if( !equals(getTeacher(), that.getTeacher()) ) return false;
            if( !equals(getSppScheduleSeason(), that.getSppScheduleSeason()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getTeacher());
            result = hashCode(result, getSppScheduleSeason());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getTeacher());
            sb.append("/");
            sb.append(getSppScheduleSeason());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppTeacherPreferenceListGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppTeacherPreferenceList.class;
        }

        public T newInstance()
        {
            return (T) new SppTeacherPreferenceList();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "teacher":
                    return obj.getTeacher();
                case "sppScheduleSeason":
                    return obj.getSppScheduleSeason();
                case "title":
                    return obj.getTitle();
                case "bells":
                    return obj.getBells();
                case "diffEven":
                    return obj.isDiffEven();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "teacher":
                    obj.setTeacher((Person) value);
                    return;
                case "sppScheduleSeason":
                    obj.setSppScheduleSeason((SppScheduleSeason) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "bells":
                    obj.setBells((ScheduleBell) value);
                    return;
                case "diffEven":
                    obj.setDiffEven((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "teacher":
                        return true;
                case "sppScheduleSeason":
                        return true;
                case "title":
                        return true;
                case "bells":
                        return true;
                case "diffEven":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "teacher":
                    return true;
                case "sppScheduleSeason":
                    return true;
                case "title":
                    return true;
                case "bells":
                    return true;
                case "diffEven":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "teacher":
                    return Person.class;
                case "sppScheduleSeason":
                    return SppScheduleSeason.class;
                case "title":
                    return String.class;
                case "bells":
                    return ScheduleBell.class;
                case "diffEven":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppTeacherPreferenceList> _dslPath = new Path<SppTeacherPreferenceList>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppTeacherPreferenceList");
    }
            

    /**
     * @return Преподаватель. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList#getTeacher()
     */
    public static Person.Path<Person> teacher()
    {
        return _dslPath.teacher();
    }

    /**
     * @return Период расписания. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList#getSppScheduleSeason()
     */
    public static SppScheduleSeason.Path<SppScheduleSeason> sppScheduleSeason()
    {
        return _dslPath.sppScheduleSeason();
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList#getBells()
     */
    public static ScheduleBell.Path<ScheduleBell> bells()
    {
        return _dslPath.bells();
    }

    /**
     * @return Предпочтения для четной и нечетной недель. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList#isDiffEven()
     */
    public static PropertyPath<Boolean> diffEven()
    {
        return _dslPath.diffEven();
    }

    public static class Path<E extends SppTeacherPreferenceList> extends EntityPath<E>
    {
        private Person.Path<Person> _teacher;
        private SppScheduleSeason.Path<SppScheduleSeason> _sppScheduleSeason;
        private PropertyPath<String> _title;
        private ScheduleBell.Path<ScheduleBell> _bells;
        private PropertyPath<Boolean> _diffEven;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Преподаватель. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList#getTeacher()
     */
        public Person.Path<Person> teacher()
        {
            if(_teacher == null )
                _teacher = new Person.Path<Person>(L_TEACHER, this);
            return _teacher;
        }

    /**
     * @return Период расписания. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList#getSppScheduleSeason()
     */
        public SppScheduleSeason.Path<SppScheduleSeason> sppScheduleSeason()
        {
            if(_sppScheduleSeason == null )
                _sppScheduleSeason = new SppScheduleSeason.Path<SppScheduleSeason>(L_SPP_SCHEDULE_SEASON, this);
            return _sppScheduleSeason;
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(SppTeacherPreferenceListGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList#getBells()
     */
        public ScheduleBell.Path<ScheduleBell> bells()
        {
            if(_bells == null )
                _bells = new ScheduleBell.Path<ScheduleBell>(L_BELLS, this);
            return _bells;
        }

    /**
     * @return Предпочтения для четной и нечетной недель. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList#isDiffEven()
     */
        public PropertyPath<Boolean> diffEven()
        {
            if(_diffEven == null )
                _diffEven = new PropertyPath<Boolean>(SppTeacherPreferenceListGen.P_DIFF_EVEN, this);
            return _diffEven;
        }

        public Class getEntityClass()
        {
            return SppTeacherPreferenceList.class;
        }

        public String getEntityName()
        {
            return "sppTeacherPreferenceList";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
