package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.entity.SppScheduleICal;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;
import ru.tandemservice.unispp.base.entity.catalog.SppScheduleStatus;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расписание сессии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppScheduleSessionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppScheduleSession";
    public static final String ENTITY_NAME = "sppScheduleSession";
    public static final int VERSION_HASH = 1880992412;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String P_APPROVED = "approved";
    public static final String P_ARCHIVED = "archived";
    public static final String L_STATUS = "status";
    public static final String L_GROUP = "group";
    public static final String L_BELLS = "bells";
    public static final String L_SEASON = "season";
    public static final String L_ICAL_DATA = "icalData";
    public static final String L_ICAL_TEACHER_DATA = "icalTeacherData";

    private String _title;     // Название
    private boolean _approved = false;     // Утверждено
    private boolean _archived = false;     // В архиве
    private SppScheduleStatus _status;     // Статус
    private Group _group;     // Группа
    private ScheduleBell _bells;     // Звонковое расписание
    private SppScheduleSessionSeason _season;     // Период расписания
    private SppScheduleICal _icalData;     // ICal
    private SppScheduleICal _icalTeacherData;     // ICal (teacher)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Утверждено. Свойство не может быть null.
     */
    @NotNull
    public boolean isApproved()
    {
        return _approved;
    }

    /**
     * @param approved Утверждено. Свойство не может быть null.
     */
    public void setApproved(boolean approved)
    {
        dirty(_approved, approved);
        _approved = approved;
    }

    /**
     * @return В архиве. Свойство не может быть null.
     */
    @NotNull
    public boolean isArchived()
    {
        return _archived;
    }

    /**
     * @param archived В архиве. Свойство не может быть null.
     */
    public void setArchived(boolean archived)
    {
        dirty(_archived, archived);
        _archived = archived;
    }

    /**
     * @return Статус. Свойство не может быть null.
     */
    @NotNull
    public SppScheduleStatus getStatus()
    {
        return _status;
    }

    /**
     * @param status Статус. Свойство не может быть null.
     */
    public void setStatus(SppScheduleStatus status)
    {
        dirty(_status, status);
        _status = status;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     */
    @NotNull
    public ScheduleBell getBells()
    {
        return _bells;
    }

    /**
     * @param bells Звонковое расписание. Свойство не может быть null.
     */
    public void setBells(ScheduleBell bells)
    {
        dirty(_bells, bells);
        _bells = bells;
    }

    /**
     * @return Период расписания.
     */
    public SppScheduleSessionSeason getSeason()
    {
        return _season;
    }

    /**
     * @param season Период расписания.
     */
    public void setSeason(SppScheduleSessionSeason season)
    {
        dirty(_season, season);
        _season = season;
    }

    /**
     * @return ICal.
     */
    public SppScheduleICal getIcalData()
    {
        return _icalData;
    }

    /**
     * @param icalData ICal.
     */
    public void setIcalData(SppScheduleICal icalData)
    {
        dirty(_icalData, icalData);
        _icalData = icalData;
    }

    /**
     * @return ICal (teacher).
     */
    public SppScheduleICal getIcalTeacherData()
    {
        return _icalTeacherData;
    }

    /**
     * @param icalTeacherData ICal (teacher).
     */
    public void setIcalTeacherData(SppScheduleICal icalTeacherData)
    {
        dirty(_icalTeacherData, icalTeacherData);
        _icalTeacherData = icalTeacherData;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppScheduleSessionGen)
        {
            setTitle(((SppScheduleSession)another).getTitle());
            setApproved(((SppScheduleSession)another).isApproved());
            setArchived(((SppScheduleSession)another).isArchived());
            setStatus(((SppScheduleSession)another).getStatus());
            setGroup(((SppScheduleSession)another).getGroup());
            setBells(((SppScheduleSession)another).getBells());
            setSeason(((SppScheduleSession)another).getSeason());
            setIcalData(((SppScheduleSession)another).getIcalData());
            setIcalTeacherData(((SppScheduleSession)another).getIcalTeacherData());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppScheduleSessionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppScheduleSession.class;
        }

        public T newInstance()
        {
            return (T) new SppScheduleSession();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "approved":
                    return obj.isApproved();
                case "archived":
                    return obj.isArchived();
                case "status":
                    return obj.getStatus();
                case "group":
                    return obj.getGroup();
                case "bells":
                    return obj.getBells();
                case "season":
                    return obj.getSeason();
                case "icalData":
                    return obj.getIcalData();
                case "icalTeacherData":
                    return obj.getIcalTeacherData();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "approved":
                    obj.setApproved((Boolean) value);
                    return;
                case "archived":
                    obj.setArchived((Boolean) value);
                    return;
                case "status":
                    obj.setStatus((SppScheduleStatus) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "bells":
                    obj.setBells((ScheduleBell) value);
                    return;
                case "season":
                    obj.setSeason((SppScheduleSessionSeason) value);
                    return;
                case "icalData":
                    obj.setIcalData((SppScheduleICal) value);
                    return;
                case "icalTeacherData":
                    obj.setIcalTeacherData((SppScheduleICal) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "approved":
                        return true;
                case "archived":
                        return true;
                case "status":
                        return true;
                case "group":
                        return true;
                case "bells":
                        return true;
                case "season":
                        return true;
                case "icalData":
                        return true;
                case "icalTeacherData":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "approved":
                    return true;
                case "archived":
                    return true;
                case "status":
                    return true;
                case "group":
                    return true;
                case "bells":
                    return true;
                case "season":
                    return true;
                case "icalData":
                    return true;
                case "icalTeacherData":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "approved":
                    return Boolean.class;
                case "archived":
                    return Boolean.class;
                case "status":
                    return SppScheduleStatus.class;
                case "group":
                    return Group.class;
                case "bells":
                    return ScheduleBell.class;
                case "season":
                    return SppScheduleSessionSeason.class;
                case "icalData":
                    return SppScheduleICal.class;
                case "icalTeacherData":
                    return SppScheduleICal.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppScheduleSession> _dslPath = new Path<SppScheduleSession>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppScheduleSession");
    }
            

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Утверждено. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#isApproved()
     */
    public static PropertyPath<Boolean> approved()
    {
        return _dslPath.approved();
    }

    /**
     * @return В архиве. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#isArchived()
     */
    public static PropertyPath<Boolean> archived()
    {
        return _dslPath.archived();
    }

    /**
     * @return Статус. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getStatus()
     */
    public static SppScheduleStatus.Path<SppScheduleStatus> status()
    {
        return _dslPath.status();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getBells()
     */
    public static ScheduleBell.Path<ScheduleBell> bells()
    {
        return _dslPath.bells();
    }

    /**
     * @return Период расписания.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getSeason()
     */
    public static SppScheduleSessionSeason.Path<SppScheduleSessionSeason> season()
    {
        return _dslPath.season();
    }

    /**
     * @return ICal.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getIcalData()
     */
    public static SppScheduleICal.Path<SppScheduleICal> icalData()
    {
        return _dslPath.icalData();
    }

    /**
     * @return ICal (teacher).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getIcalTeacherData()
     */
    public static SppScheduleICal.Path<SppScheduleICal> icalTeacherData()
    {
        return _dslPath.icalTeacherData();
    }

    public static class Path<E extends SppScheduleSession> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private PropertyPath<Boolean> _approved;
        private PropertyPath<Boolean> _archived;
        private SppScheduleStatus.Path<SppScheduleStatus> _status;
        private Group.Path<Group> _group;
        private ScheduleBell.Path<ScheduleBell> _bells;
        private SppScheduleSessionSeason.Path<SppScheduleSessionSeason> _season;
        private SppScheduleICal.Path<SppScheduleICal> _icalData;
        private SppScheduleICal.Path<SppScheduleICal> _icalTeacherData;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(SppScheduleSessionGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Утверждено. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#isApproved()
     */
        public PropertyPath<Boolean> approved()
        {
            if(_approved == null )
                _approved = new PropertyPath<Boolean>(SppScheduleSessionGen.P_APPROVED, this);
            return _approved;
        }

    /**
     * @return В архиве. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#isArchived()
     */
        public PropertyPath<Boolean> archived()
        {
            if(_archived == null )
                _archived = new PropertyPath<Boolean>(SppScheduleSessionGen.P_ARCHIVED, this);
            return _archived;
        }

    /**
     * @return Статус. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getStatus()
     */
        public SppScheduleStatus.Path<SppScheduleStatus> status()
        {
            if(_status == null )
                _status = new SppScheduleStatus.Path<SppScheduleStatus>(L_STATUS, this);
            return _status;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Звонковое расписание. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getBells()
     */
        public ScheduleBell.Path<ScheduleBell> bells()
        {
            if(_bells == null )
                _bells = new ScheduleBell.Path<ScheduleBell>(L_BELLS, this);
            return _bells;
        }

    /**
     * @return Период расписания.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getSeason()
     */
        public SppScheduleSessionSeason.Path<SppScheduleSessionSeason> season()
        {
            if(_season == null )
                _season = new SppScheduleSessionSeason.Path<SppScheduleSessionSeason>(L_SEASON, this);
            return _season;
        }

    /**
     * @return ICal.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getIcalData()
     */
        public SppScheduleICal.Path<SppScheduleICal> icalData()
        {
            if(_icalData == null )
                _icalData = new SppScheduleICal.Path<SppScheduleICal>(L_ICAL_DATA, this);
            return _icalData;
        }

    /**
     * @return ICal (teacher).
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSession#getIcalTeacherData()
     */
        public SppScheduleICal.Path<SppScheduleICal> icalTeacherData()
        {
            if(_icalTeacherData == null )
                _icalTeacherData = new SppScheduleICal.Path<SppScheduleICal>(L_ICAL_TEACHER_DATA, this);
            return _icalTeacherData;
        }

        public Class getEntityClass()
        {
            return SppScheduleSession.class;
        }

        public String getEntityName()
        {
            return "sppScheduleSession";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
