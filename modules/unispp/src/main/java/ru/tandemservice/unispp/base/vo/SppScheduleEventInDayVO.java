/* $Id$ */
package ru.tandemservice.unispp.base.vo;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriodFix;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionEvent;

import java.util.Date;

/**
 * Событие в расписании (любое)
 * (используется в расписании преподавателей)
 * @author Igor Belanov
 * @since 09.02.2017
 */
public class SppScheduleEventInDayVO
{
    // output wrapper properties
    public static final String PROPERTY_DATE = "date";
    public static final String PROPERTY_SUBJECT = "subject";
    public static final String PROPERTY_GROUP = "group";
    public static final String PROPERTY_LECTURE_ROOM = "lectureRoom";
    public static final String PROPERTY_TEACHER = "teacher";
    public static final String PROPERTY_STATUS = "status";

    // output status text
    private static final String STATUS_CHANGED_TEXT = "ИЗМЕНЕНО";
    private static final String STATUS_CANCELED_TEXT = "ОТМЕНЕНО";
    private static final String STATUS_ADDED_TEXT = "ДОБАВЛЕНО";

    // event status codes
    public static final IdentifiableWrapper STATUS_CHANGED = new IdentifiableWrapper(0L, "изменённые");
    public static final IdentifiableWrapper STATUS_CANCELED = new IdentifiableWrapper(1L, "отменённые");
    public static final IdentifiableWrapper STATUS_ADDED = new IdentifiableWrapper(2L, "добавленные");

    private LocalDate _date; // дата события
    private ScheduleBellEntry _bellEntry;
    private int _num = -1; // номер подгруппы (нужен только в обычных расписаниях)
    private String _groupTitle;
    private EppALoadType _subjectLoadType; // вид ауд. нагрузки (нужен только в обычных расписаниях и в подневных)
    private EppFControlActionType _subjectActionType; // форма итогового контроля (нужна только в расписаниях сессий и подневных)
    private EppRegistryElement _subjectVal;
    private String _subject;
    private PpsEntry _teacherVal;
    private String _teacher;
    private UniplacesPlace _lectureRoomVal;
    private String _lectureRoom;
    private IdentifiableWrapper _status;

    public SppScheduleEventInDayVO(SppScheduleDailyEvent event)
    {
        _date = LocalDate.fromDateFields(event.getDate());
        _bellEntry = event.getBell();
        _groupTitle = event.getSchedule().getGroup().getTitle();
        _subjectVal = event.getSubjectVal();
        _subject = event.getSubject();
        _teacherVal = event.getTeacherVal();
        _teacher = event.getTeacher();
        _lectureRoomVal = event.getLectureRoomVal();
        _lectureRoom = event.getLectureRoom();

        _subjectLoadType = event.getSubjectLoadType();
        _subjectActionType = event.getSubjectActionType();
    }

    public SppScheduleEventInDayVO(SppScheduleSessionEvent event)
    {
        _date = LocalDate.fromDateFields(event.getDate());
        _bellEntry = event.getBell();
        _groupTitle = event.getSchedule().getGroup().getTitle();
        _subjectVal = event.getSubjectVal();
        _subject = event.getSubject();
        _teacherVal = event.getTeacherVal();
        _teacher = event.getTeacher();
        _lectureRoomVal = event.getLectureRoomVal();
        _lectureRoom = event.getLectureRoom();

        _subjectActionType = event.getSubjectFormControl();
    }

    public SppScheduleEventInDayVO(SppSchedulePeriod period, LocalDate date)
    {
        _date = date;
        _bellEntry = period.getWeekRow().getBell();
        _groupTitle = period.getWeekRow().getSchedule().getGroup().getTitle();
        _subjectVal = period.getSubjectVal();
        _subject = period.getSubject();
        _teacherVal = period.getTeacherVal();
        _teacher = period.getTeacher();
        _lectureRoomVal = period.getLectureRoomVal();
        _lectureRoom = period.getLectureRoom();

        _subjectLoadType = period.getSubjectLoadType();
        _num = period.getPeriodNum();
    }

    public SppScheduleEventInDayVO(SppSchedulePeriodFix periodFix, IdentifiableWrapper status)
    {
        _date = LocalDate.fromDateFields(periodFix.getDate());
        _bellEntry = periodFix.getBell();
        _groupTitle = periodFix.getSppSchedule().getGroup().getTitle();
        _subjectVal = periodFix.getSubjectVal();
        _subject = periodFix.getSubject();
        _teacherVal = periodFix.getTeacherVal();
        _teacher = periodFix.getTeacher();
        _lectureRoomVal = periodFix.getLectureRoomVal();
        _lectureRoom = periodFix.getLectureRoom();

        _status = status;
        _num = periodFix.getPeriodNum();
        _subjectLoadType = periodFix.getSubjectLoadType();
    }

    public boolean isFixMatch(SppScheduleEventInDayVO fix)
    {
        return (fix.getDate().equals(_date) && fix.getBellEntry().equals(_bellEntry) && fix.getNum() == _num);
    }

    public DataWrapper getDataWrapper()
    {
        DataWrapper result = new DataWrapper();
        Date date = getDate().toLocalDateTime(
                new LocalTime(getBellEntry().getStartHour(), getBellEntry().getStartMin())).toDate();
        result.setProperty(PROPERTY_DATE, date);
        result.setProperty(PROPERTY_GROUP, getGroupTitle());
        result.setProperty(PROPERTY_LECTURE_ROOM, getLectureRoom());
        result.setProperty(PROPERTY_SUBJECT, getSubject());
        result.setProperty(PROPERTY_TEACHER, getTeacher());
        String status = "";
        if (SppScheduleEventInDayVO.STATUS_ADDED.equals(getStatus())) status = STATUS_ADDED_TEXT;
        else if (SppScheduleEventInDayVO.STATUS_CHANGED.equals(getStatus())) status = STATUS_CHANGED_TEXT;
        else if (SppScheduleEventInDayVO.STATUS_CANCELED.equals(getStatus())) status = STATUS_CANCELED_TEXT;
        result.setProperty(PROPERTY_STATUS, status);
        return result;
    }

    public boolean isCanceled()
    {
        return STATUS_CANCELED.equals(_status);
    }

    // getters & setters
    public LocalDate getDate()
    {
        return _date;
    }

    public ScheduleBellEntry getBellEntry()
    {
        return _bellEntry;
    }

    public int getNum()
    {
        return _num;
    }

    public String getGroupTitle()
    {
        return _groupTitle;
    }

    public EppALoadType getSubjectLoadType()
    {
        return _subjectLoadType;
    }

    public EppFControlActionType getSubjectActionType()
    {
        return _subjectActionType;
    }

    public EppRegistryElement getSubjectVal()
    {
        return _subjectVal;
    }

    public String getSubject()
    {
        return _subject;
    }

    public PpsEntry getTeacherVal()
    {
        return _teacherVal;
    }

    public String getTeacher()
    {
        return _teacher;
    }

    public UniplacesPlace getLectureRoomVal()
    {
        return _lectureRoomVal;
    }

    public String getLectureRoom()
    {
        return _lectureRoom;
    }

    public IdentifiableWrapper getStatus()
    {
        return _status;
    }

    public void setStatus(IdentifiableWrapper status)
    {
        _status = status;
    }
}
