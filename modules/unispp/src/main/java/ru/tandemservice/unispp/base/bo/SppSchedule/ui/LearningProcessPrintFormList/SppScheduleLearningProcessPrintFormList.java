/* $Id: SppScheduleLearningProcessPrintFormList.java 37786 2014-09-02 12:51:41Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.LearningProcessPrintFormList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppSchedulePrintFormDSHandler;
import ru.tandemservice.unispp.base.entity.SppSchedulePrintForm;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author nvankov
 * @since 9/25/13
 */
@Configuration
public class SppScheduleLearningProcessPrintFormList extends BusinessComponentManager
{
    public static final String PRINT_FORM_DS = "schedulePrintFormDS";
    public static final String YEAR_PART_DS = "yearPartDS";
    public static final String ADMIN_DS = "adminDS";
    public static final String GROUP_DS = "groupDS";
    public static final String ORG_UNIT_DS = "orgUnitDS";

    @Bean
    public ColumnListExtPoint printFormCL()
    {
        return columnListExtPointBuilder(PRINT_FORM_DS)
                .addColumn(dateColumn("createDate", SppSchedulePrintForm.createDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().required(true))
                .addColumn(textColumn("season", SppSchedulePrintForm.season().titleWithTime()))
                .addColumn(textColumn("yearPart", SppSchedulePrintForm.season().eppYearPart().title()))
                .addColumn(textColumn("ou", SppSchedulePrintForm.formativeOrgUnit().title()).order().required(true))
                .addColumn(textColumn("groups", SppSchedulePrintForm.groups()).order().required(true))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickDownload").permissionKey("sppScheduleLearningProcessPrintFormPrint"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRINT_FORM_DS, printFormCL(), printFormDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEAR_PART_DS, getName(), EppYearPart.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(ADMIN_DS, SppScheduleManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s()))
                .addDataSource(selectDS(GROUP_DS, SppScheduleManager.instance().groupComboDSHandler()))
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                        OrgUnitToKindRelation.class,
                        OrgUnitToKindRelation.orgUnitKind().code().s(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING,
                        OrgUnitToKindRelation.L_ORG_UNIT, property("e")
                ));
            }
        }
                .order(OrgUnit.title())
                .filter(OrgUnit.title());
    }

    @Bean
    public IDefaultSearchDataSourceHandler printFormDSHandler()
    {
        return new SppSchedulePrintFormDSHandler(getName());
    }
}
