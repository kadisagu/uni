/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.MassAddingExercise;

import com.beust.jcommander.internal.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleLectureRoomComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleSubjectComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleTeacherComboDSHandler;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;
import ru.tandemservice.unispp.base.entity.SppScheduleWeekRow;
import ru.tandemservice.unispp.base.vo.SppScheduleDisciplineVO;
import ru.tandemservice.unispp.base.vo.SppSchedulePeriodVO;
import ru.tandemservice.unispp.base.vo.SppScheduleWeekPeriodsRowVO;

import java.time.DayOfWeek;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 11.10.2016
 */
@Input({
        @Bind(key = SppScheduleMassAddingExerciseUI.BIND_SCHEDULE_IDS_LIST, binding = "scheduleIdsList", required = true)
})
public class SppScheduleMassAddingExerciseUI extends UIPresenter
{
    // settings
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String SUBJECT = "subject";
    public static final String SUBJECT_VAL = "subjectVal";
    public static final String TEACHER = "teacher";
    public static final String TEACHER_VAL = "teacherVal";
    public static final String LECTURE_ROOM_TYPE = "lectureRoomType";
    public static final String LECTURE_ROOM = "lectureRoom";
    public static final String LECTURE_ROOM_VAL = "lectureRoomVal";

    // ключ для передающего компонента, по которому передаётся список id расписаний
    public static final String BIND_SCHEDULE_IDS_LIST = "scheduleIdsList";

    // передаваемый из предыдущего компонента список id выбранных расписаний
    private List<Long> _scheduleIdsList;

    // звонковое расписание переданных расписаний
    private ScheduleBell _scheduleBell;

    // период расписания переданных расписаний
    private SppScheduleSeason _scheduleSeason;

    private final ISelectModel _dayOfWeekDS = DayOfWeekDSHandler.getSelectModel();
    private IdentifiableWrapper _dayOfWeekWrapper;
    private DataWrapper _bellScheduleEntryWrapper;
    private boolean _evenWeek = false;
    private boolean _empty = false;

    private boolean _checkTeacherPreference;

    @Override
    public void onComponentRefresh()
    {
        setCheckTeacherPreference(true);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleMassAddingExercise.BELL_SCHEDULE_ENTRY_DS.equals(dataSource.getName()))
        {
            if (getScheduleBell() != null)
                dataSource.put(SppScheduleMassAddingExercise.PROP_BELL_SCHEDULE_ID, getScheduleBell().getId());
        }
        else if (SppScheduleMassAddingExercise.SUBJECT_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleSubjectComboDSHandler.PROP_GROUP_IDS,
                    DataAccessServices.dao().getList(SppSchedule.class, getScheduleIdsList())
                            .stream().map(s -> s.getGroup().getId()).distinct().collect(Collectors.toList()));
            if (_scheduleSeason != null)
                dataSource.put(SppScheduleSubjectComboDSHandler.PROP_SEASON_ID, _scheduleSeason.getId());
        }
        else if (SppScheduleMassAddingExercise.TEACHER_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleTeacherComboDSHandler.PROP_SCHEDULE_IDS, getScheduleIdsList());
            dataSource.put(SppScheduleTeacherComboDSHandler.PROP_ORG_UNIT_ID, getSubjectOrgUnitId());
            dataSource.put(SppScheduleTeacherComboDSHandler.PROP_CHECK_FREE_TEACHERS, Boolean.TRUE);
            dataSource.put(SppScheduleTeacherComboDSHandler.PROP_CHECK_TEACHER_PREFERENCES, _checkTeacherPreference);

            if (getBellScheduleEntry() != null)
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_EVENT_BELL_ENTRY_ID, getBellScheduleEntry().getId());
            if (getDayOfWeek() != null)
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_EVENT_DAY_OF_WEEK, getDayOfWeek().getValue());
            if (getScheduleSeason() != null)
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_SEASON_ID, getScheduleSeason().getId());
            DataWrapper lectureRoomValWrapper = getSettings().get(LECTURE_ROOM_VAL);
            if (lectureRoomValWrapper != null && lectureRoomValWrapper.getId() != null)
                dataSource.put(SppScheduleTeacherComboDSHandler.PROP_LECTURE_ROOM_ID, lectureRoomValWrapper.getId());
            if (getStartDate() != null)
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_START_DATE, getStartDate());
            if (getEndDate() != null)
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_END_DATE, getEndDate());
        }
        else if (SppScheduleMassAddingExercise.LECTURE_ROOM_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_SCHEDULE_IDS, getScheduleIdsList());
            dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_CHECK_TEACHER_PREFERENCES, _checkTeacherPreference);

            if (getBellScheduleEntry() != null)
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_EVENT_BELL_ENTRY_ID, getBellScheduleEntry().getId());
            if (getDayOfWeek() != null)
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_EVENT_DAY_OF_WEEK, getDayOfWeek().getValue());
            if (getLectureRoomType() != null)
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_LECTURE_ROOM_TYPE, getLectureRoomType());
            if (getTeacherVal() != null)
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_TEACHER_ID, getTeacherVal().getId());
            if (getSubjectVal() != null && getSubjectVal().getDiscipline() != null)
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_DISCIPLINE_ID, getSubjectVal().getDiscipline().getId());
            if (getScheduleSeason() != null)
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_SEASON_ID, getScheduleSeason().getId());
            if (getStartDate() != null)
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_START_DATE, getStartDate());
            if (getEndDate() != null)
                dataSource.put(SppScheduleLectureRoomComboDSHandler.PROP_END_DATE, getEndDate());
        }
    }

    public void onClickSavePeriod()
    {
        final List<SppScheduleWeekRow> scheduleWeekRowList = getScheduleWeekRowList();
        List<SppSchedulePeriodVO> periodsVO = Lists.newArrayList();

        validate();

        // заранее подготовим те данные для занятий, которые не отличаются
        int dayOfWeek = getDayOfWeek() != null ? getDayOfWeek().getValue() : -1; // день недели точно заполнен (иначе мы сюда не попадём)
        Date startDate = getStartDate();
        Date endDate = getEndDate();
        boolean empty = isEmpty();
        String subject = getSubject();
        EppRegistryElement subjectVal = getSubjectVal() != null ? getSubjectVal().getDiscipline() : null;
        EppALoadType subjectLoadType = getSubjectVal() != null ? getSubjectVal().getEppALoadType() : null;
        String teacher = getTeacher();
        PpsEntry teacherVal = getTeacherVal();
        String lectureRoom = getLectureRoom();
        UniplacesPlace lectureRoomVal = getLectureRoomVal();
        String periodGroup = getPeriodGroup();

        for (SppScheduleWeekRow sppScheduleWeekRow : scheduleWeekRowList)
        {
            SppSchedulePeriod sppSchedulePeriod = new SppSchedulePeriod();

            // заранее полученные общие данные для всех добавляемых занятий,
            // объект будет завёрнут в ВОшку и при сохранении все ненужные данные будут отрезаны
            // (см. getPeriodForSave в SppSchedulePeriodVO)
            sppSchedulePeriod.setDayOfWeek(dayOfWeek);
            sppSchedulePeriod.setStartDate(startDate);
            sppSchedulePeriod.setEndDate(endDate);
            sppSchedulePeriod.setEmpty(empty);
            sppSchedulePeriod.setSubject(subject);
            sppSchedulePeriod.setSubjectVal(subjectVal);
            sppSchedulePeriod.setSubjectLoadType(subjectLoadType);
            sppSchedulePeriod.setTeacher(teacher);
            sppSchedulePeriod.setTeacherVal(teacherVal);
            sppSchedulePeriod.setLectureRoom(lectureRoom);
            sppSchedulePeriod.setLectureRoomVal(lectureRoomVal);
            sppSchedulePeriod.setPeriodGroup(periodGroup);

            // привязка к конкретному расписанию идёт через сущность weekRow (для которой тоже создадим ВОшку)
            SppSchedulePeriodVO sppSchedulePeriodVO = new SppSchedulePeriodVO(sppSchedulePeriod);
            sppSchedulePeriodVO.setWeekPeriodsRowVO(new SppScheduleWeekPeriodsRowVO(sppScheduleWeekRow));

            periodsVO.add(sppSchedulePeriodVO);
        }
        SppScheduleManager.instance().dao().saveOrUpdatePeriods(periodsVO);
        deactivate();
    }

    private List<SppScheduleWeekRow> getScheduleWeekRowList()
    {
        final List<Long> scheduleIdsList = getScheduleIdsList();
        final ScheduleBellEntry scheduleEntry = getBellScheduleEntry();
        final Boolean even = isEvenWeek();

        final String alias = "swr";
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleWeekRow.class, alias)
                .column(property(alias))
                .where(eq(property(alias, SppScheduleWeekRow.bell()), value(scheduleEntry)))
                .where(eq(property(alias, SppScheduleWeekRow.even()), value(even)))
                .where(in(property(alias, SppScheduleWeekRow.schedule().id()), scheduleIdsList));
        return DataAccessServices.dao().getList(builder);
    }

    private void validate()
    {
        Date startDate = getStartDate();
        Date endDate = getEndDate();

        if (startDate != null && endDate != null && startDate.after(endDate))
            throw new ApplicationException("Дата начала периода проведения идёт после даты его окончания");

        boolean beyondLeft = startDate != null && startDate.before(_scheduleSeason.getStartDate());
        boolean beyondRight = endDate != null && endDate.after(_scheduleSeason.getEndDate());
        if (beyondLeft && beyondRight)
            throw new ApplicationException("Период проведения пары выходит за пределы периода расписания");
        if (beyondLeft)
            throw new ApplicationException("Период проведения пары начинается раньше периода расписания");
        if (beyondRight)
            throw new ApplicationException("Период проведения пары заканчивается позже периода расписания");
    }


    // getters from saved parameters
    private Long getSubjectOrgUnitId()
    {
        if (getSubjectVal() != null && getSubjectVal().getDiscipline() != null)
            return getSubjectVal().getDiscipline().getOwner().getId();
        return null;
    }

    private ScheduleBellEntry getBellScheduleEntry()
    {
        return getBellScheduleEntryWrapper() == null ? null : getBellScheduleEntryWrapper().getWrapped();
    }

    private DayOfWeek getDayOfWeek()
    {
        //id is dayOfWeek code value
        final IdentifiableWrapper dayOfWeekWrapper = getDayOfWeekWrapper();
        if (dayOfWeekWrapper != null)
        {
            final int id = java.lang.Math.toIntExact(dayOfWeekWrapper.getId());
            return DayOfWeek.of(id);
        }
        return null;
    }

    private Date getStartDate()
    {
        return getSettings().get(START_DATE);
    }

    private Date getEndDate()
    {
        return getSettings().get(END_DATE);
    }

    private String getSubject()
    {
        return getSettings().get(SUBJECT);
    }

    private SppScheduleDisciplineVO getSubjectVal()
    {
        return getSettings().get(SUBJECT_VAL);
    }

    private String getTeacher()
    {
        return getSettings().get(TEACHER);
    }

    private PpsEntry getTeacherVal()
    {
        return getSettings().get(TEACHER_VAL);
    }

    private UniplacesClassroomType getLectureRoomType()
    {
        return getSettings().get(LECTURE_ROOM_TYPE);
    }

    private String getLectureRoom()
    {
        return getSettings().get(LECTURE_ROOM);
    }

    private UniplacesPlace getLectureRoomVal()
    {
        DataWrapper lectureRoomValWrapper = getSettings().get(LECTURE_ROOM_VAL);
        return (lectureRoomValWrapper != null && lectureRoomValWrapper.getId() != null) ? DataAccessServices.dao().getNotNull(lectureRoomValWrapper.getId()) : null;
    }

    public String getPeriodGroup()
    {
        return "";
    }


    // listeners
    public void onChangeSubject()
    {
        if (getSubjectVal() != null)
        {
            StringBuilder sb = new StringBuilder();
            final String title = getSubjectVal().getTitle();
            if (title != null)
            {
                sb.append(title.substring(0, title.length() - 2));
                if (getStartDate() != null || getEndDate() != null)
                {
                    sb.append(" (");
                    sb.append(getStartDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(getStartDate()) : "...").append(" - ");
                    sb.append(getEndDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndDate()) : "...");
                    sb.append(")");
                }
            }

            getSettings().set(SUBJECT, sb.toString());
        }
    }

    public void onChangeTeacher()
    {
        if (getTeacherVal() != null)
            getSettings().set(TEACHER, getTeacherVal().getFio());
    }

    public void onChangeLectureRoom()
    {
        UniplacesPlace lectureRoomVal = getLectureRoomVal();
        if (lectureRoomVal != null)
            getSettings().set(LECTURE_ROOM, lectureRoomVal.getTitle());
    }


    // getters & setters
    public List<Long> getScheduleIdsList()
    {
        return _scheduleIdsList;
    }

    public void setScheduleIdsList(List<Long> scheduleIdsList)
    {
        _scheduleIdsList = scheduleIdsList;

        // сразу поставим звонковое расписание
        if (scheduleIdsList != null && !scheduleIdsList.isEmpty())
        {
            _scheduleBell = DataAccessServices.dao().get(SppSchedule.class, scheduleIdsList.get(0)).getBells();
            _scheduleSeason = DataAccessServices.dao().get(SppSchedule.class, scheduleIdsList.get(0)).getSeason();
        }
    }

    public ScheduleBell getScheduleBell()
    {
        return _scheduleBell;
    }

    public SppScheduleSeason getScheduleSeason()
    {
        return _scheduleSeason;
    }

    public ISelectModel getDayOfWeekDS()
    {
        return _dayOfWeekDS;
    }

    public IdentifiableWrapper getDayOfWeekWrapper()
    {
        return _dayOfWeekWrapper;
    }

    public void setDayOfWeekWrapper(IdentifiableWrapper dayOfWeekWrapper)
    {
        _dayOfWeekWrapper = dayOfWeekWrapper;
    }

    public DataWrapper getBellScheduleEntryWrapper()
    {
        return _bellScheduleEntryWrapper;
    }

    public void setBellScheduleEntryWrapper(DataWrapper bellScheduleEntryWrapper)
    {
        this._bellScheduleEntryWrapper = bellScheduleEntryWrapper;
    }

    public boolean isEvenWeek()
    {
        return _evenWeek;
    }

    public void setEvenWeek(boolean evenWeek)
    {
        _evenWeek = evenWeek;
    }

    public boolean isEmpty()
    {
        return _empty;
    }

    public boolean isNotEmpty()
    {
        return !isEmpty();
    }

    public void setEmpty(boolean empty)
    {
        this._empty = empty;
    }

    public boolean isCheckTeacherPreference()
    {
        return _checkTeacherPreference;
    }

    public void setCheckTeacherPreference(boolean checkTeacherPreference)
    {
        _checkTeacherPreference = checkTeacherPreference;
    }
}
