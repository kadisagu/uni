package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проверка распределения аудиторной нагрузки
 *
 * Чекбоксы "Проверять распределение лекций" (практик, лабораторных) и выбор аудитории
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppRegElementLoadCheckGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck";
    public static final String ENTITY_NAME = "sppRegElementLoadCheck";
    public static final int VERSION_HASH = 1662270377;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_ELEMENT = "registryElement";
    public static final String L_EPP_A_LOAD_TYPE = "eppALoadType";
    public static final String P_CHECK_VALUE = "checkValue";
    public static final String L_UNIPLACES_CLASSROOM_TYPE = "uniplacesClassroomType";

    private EppRegistryElement _registryElement;     // Элемент реестра
    private EppALoadType _eppALoadType;     // Вид аудиторной нагрузки (типы аудиторных занятий)
    private boolean _checkValue = false; 
    private UniplacesClassroomType _uniplacesClassroomType;     // Тип учебного помещения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getRegistryElement()
    {
        return _registryElement;
    }

    /**
     * @param registryElement Элемент реестра. Свойство не может быть null.
     */
    public void setRegistryElement(EppRegistryElement registryElement)
    {
        dirty(_registryElement, registryElement);
        _registryElement = registryElement;
    }

    /**
     * @return Вид аудиторной нагрузки (типы аудиторных занятий). Свойство не может быть null.
     */
    @NotNull
    public EppALoadType getEppALoadType()
    {
        return _eppALoadType;
    }

    /**
     * @param eppALoadType Вид аудиторной нагрузки (типы аудиторных занятий). Свойство не может быть null.
     */
    public void setEppALoadType(EppALoadType eppALoadType)
    {
        dirty(_eppALoadType, eppALoadType);
        _eppALoadType = eppALoadType;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isCheckValue()
    {
        return _checkValue;
    }

    /**
     * @param checkValue  Свойство не может быть null.
     */
    public void setCheckValue(boolean checkValue)
    {
        dirty(_checkValue, checkValue);
        _checkValue = checkValue;
    }

    /**
     * @return Тип учебного помещения.
     */
    public UniplacesClassroomType getUniplacesClassroomType()
    {
        return _uniplacesClassroomType;
    }

    /**
     * @param uniplacesClassroomType Тип учебного помещения.
     */
    public void setUniplacesClassroomType(UniplacesClassroomType uniplacesClassroomType)
    {
        dirty(_uniplacesClassroomType, uniplacesClassroomType);
        _uniplacesClassroomType = uniplacesClassroomType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppRegElementLoadCheckGen)
        {
            setRegistryElement(((SppRegElementLoadCheck)another).getRegistryElement());
            setEppALoadType(((SppRegElementLoadCheck)another).getEppALoadType());
            setCheckValue(((SppRegElementLoadCheck)another).isCheckValue());
            setUniplacesClassroomType(((SppRegElementLoadCheck)another).getUniplacesClassroomType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppRegElementLoadCheckGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppRegElementLoadCheck.class;
        }

        public T newInstance()
        {
            return (T) new SppRegElementLoadCheck();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "registryElement":
                    return obj.getRegistryElement();
                case "eppALoadType":
                    return obj.getEppALoadType();
                case "checkValue":
                    return obj.isCheckValue();
                case "uniplacesClassroomType":
                    return obj.getUniplacesClassroomType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "registryElement":
                    obj.setRegistryElement((EppRegistryElement) value);
                    return;
                case "eppALoadType":
                    obj.setEppALoadType((EppALoadType) value);
                    return;
                case "checkValue":
                    obj.setCheckValue((Boolean) value);
                    return;
                case "uniplacesClassroomType":
                    obj.setUniplacesClassroomType((UniplacesClassroomType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "registryElement":
                        return true;
                case "eppALoadType":
                        return true;
                case "checkValue":
                        return true;
                case "uniplacesClassroomType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "registryElement":
                    return true;
                case "eppALoadType":
                    return true;
                case "checkValue":
                    return true;
                case "uniplacesClassroomType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "registryElement":
                    return EppRegistryElement.class;
                case "eppALoadType":
                    return EppALoadType.class;
                case "checkValue":
                    return Boolean.class;
                case "uniplacesClassroomType":
                    return UniplacesClassroomType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppRegElementLoadCheck> _dslPath = new Path<SppRegElementLoadCheck>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppRegElementLoadCheck");
    }
            

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck#getRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> registryElement()
    {
        return _dslPath.registryElement();
    }

    /**
     * @return Вид аудиторной нагрузки (типы аудиторных занятий). Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck#getEppALoadType()
     */
    public static EppALoadType.Path<EppALoadType> eppALoadType()
    {
        return _dslPath.eppALoadType();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck#isCheckValue()
     */
    public static PropertyPath<Boolean> checkValue()
    {
        return _dslPath.checkValue();
    }

    /**
     * @return Тип учебного помещения.
     * @see ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck#getUniplacesClassroomType()
     */
    public static UniplacesClassroomType.Path<UniplacesClassroomType> uniplacesClassroomType()
    {
        return _dslPath.uniplacesClassroomType();
    }

    public static class Path<E extends SppRegElementLoadCheck> extends EntityPath<E>
    {
        private EppRegistryElement.Path<EppRegistryElement> _registryElement;
        private EppALoadType.Path<EppALoadType> _eppALoadType;
        private PropertyPath<Boolean> _checkValue;
        private UniplacesClassroomType.Path<UniplacesClassroomType> _uniplacesClassroomType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck#getRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> registryElement()
        {
            if(_registryElement == null )
                _registryElement = new EppRegistryElement.Path<EppRegistryElement>(L_REGISTRY_ELEMENT, this);
            return _registryElement;
        }

    /**
     * @return Вид аудиторной нагрузки (типы аудиторных занятий). Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck#getEppALoadType()
     */
        public EppALoadType.Path<EppALoadType> eppALoadType()
        {
            if(_eppALoadType == null )
                _eppALoadType = new EppALoadType.Path<EppALoadType>(L_EPP_A_LOAD_TYPE, this);
            return _eppALoadType;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck#isCheckValue()
     */
        public PropertyPath<Boolean> checkValue()
        {
            if(_checkValue == null )
                _checkValue = new PropertyPath<Boolean>(SppRegElementLoadCheckGen.P_CHECK_VALUE, this);
            return _checkValue;
        }

    /**
     * @return Тип учебного помещения.
     * @see ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck#getUniplacesClassroomType()
     */
        public UniplacesClassroomType.Path<UniplacesClassroomType> uniplacesClassroomType()
        {
            if(_uniplacesClassroomType == null )
                _uniplacesClassroomType = new UniplacesClassroomType.Path<UniplacesClassroomType>(L_UNIPLACES_CLASSROOM_TYPE, this);
            return _uniplacesClassroomType;
        }

        public Class getEntityClass()
        {
            return SppRegElementLoadCheck.class;
        }

        public String getEntityName()
        {
            return "sppRegElementLoadCheck";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
