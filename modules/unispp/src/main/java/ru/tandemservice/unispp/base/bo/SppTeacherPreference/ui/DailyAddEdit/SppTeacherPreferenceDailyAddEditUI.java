/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.DailyAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.AddEdit.SppTeacherPreferenceAddEdit;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.DailyView.SppTeacherPreferenceDailyView;
import ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList;

/**
 * @author vnekrasov
 * @since 7/3/14
 */
@Input({
        @Bind(key = "teacherDailyPreferenceId", binding = "teacherDailyPreferenceId"),
        @Bind(key = "employeePostId", binding = "employeePostId")
})
public class SppTeacherPreferenceDailyAddEditUI extends UIPresenter
{
    private Long _employeePostId;
    private Long _teacherDailyPreferenceId;
    private SppTeacherDailyPreferenceList _teacherDailyPreference;
    private ScheduleBell _oldBells;

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        _employeePostId = employeePostId;
    }

    public Long getTeacherDailyPreferenceId()
    {
        return _teacherDailyPreferenceId;
    }

    public void setTeacherDailyPreferenceId(Long teacherDailyPreferenceId)
    {
        _teacherDailyPreferenceId = teacherDailyPreferenceId;
    }

    public SppTeacherDailyPreferenceList getTeacherDailyPreference()
    {
        return _teacherDailyPreference;
    }

    public void setTeacherDailyPreference(SppTeacherDailyPreferenceList TeacherDailyPreference)
    {
        _teacherDailyPreference = TeacherDailyPreference;
    }

    public Boolean getAddForm()
    {
        return null == _teacherDailyPreferenceId;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _teacherDailyPreference)
        {
            if (null != _teacherDailyPreferenceId)
            {
                _teacherDailyPreference = DataAccessServices.dao().get(_teacherDailyPreferenceId);
                _oldBells = _teacherDailyPreference.getBells();
            }
            else
            {
                _teacherDailyPreference = new SppTeacherDailyPreferenceList();
                if (_employeePostId != null)
                {
                    Person person = UniDaoFacade.getCoreDao().getProperty(EmployeePost.class, EmployeePost.person().s(), EmployeePost.P_ID, _employeePostId);
                    _teacherDailyPreference.setTeacher(person);
                }
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppTeacherPreferenceAddEdit.SEASON_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppTeacherPreferenceManager.EMPLOYEE, _teacherDailyPreference.getTeacher());
            dataSource.put(SppTeacherPreferenceManager.EDITED, _teacherDailyPreferenceId);
        }
    }

    public void onClickApply()
    {
        SppTeacherPreferenceManager.instance().dao().saveOrUpdateTeacherDailyPreferenceList(_teacherDailyPreference, _oldBells);
        if(getAddForm())
        {
            _uiConfig.deactivateComponent();
            _uiActivation.asDesktopRoot(SppTeacherPreferenceDailyView.class).parameter(PUBLISHER_ID, _teacherDailyPreference.getId()).activate();
        }
        else _uiConfig.deactivateComponent();
    }
}
