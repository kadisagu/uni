/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 7/2/14
 */
public class SppTeacherSessionPreferenceElementDSHandler extends DefaultSearchDataSourceHandler
{
    public SppTeacherSessionPreferenceElementDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long teacherSessionPreferenceId = context.getNotNull("teacherSessionPreferenceId");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppTeacherSessionPreferenceElement.class, "e");
        builder.where(eq(property("e", SppTeacherSessionPreferenceElement.teacherPreference().id()), value(teacherSessionPreferenceId)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().pageable(true).build();
    }
}
