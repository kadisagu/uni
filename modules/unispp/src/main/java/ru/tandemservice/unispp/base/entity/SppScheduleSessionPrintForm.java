package ru.tandemservice.unispp.base.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unispp.base.entity.gen.*;

/**
 * Печатная форма расписания сессии
 */
public class SppScheduleSessionPrintForm extends SppScheduleSessionPrintFormGen
{
    @EntityDSLSupport
    public String getFormWithTerrTitle()
    {
        return getFormativeOrgUnit().getTitle() +
                (getTerritorialOrgUnit() != null ? (" (" + getTerritorialOrgUnit().getTerritorialTitle() + ")") : "");
    }

}