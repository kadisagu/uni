/* $Id$ */
package ru.tandemservice.unispp.base.vo;

import com.google.common.collect.Lists;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppSchedule;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 8/30/13
 */
public class SppScheduleWeekVO
{
    private SppSchedule _schedule;

    private Map<ScheduleBellEntry, SppScheduleWeekPeriodsRowVO> _weekPeriodsMap;

    private List<SppScheduleWeekPeriodsRowVO> _weekPeriods;

    public SppSchedule getSchedule()
    {
        return _schedule;
    }

    public void setSchedule(SppSchedule schedule)
    {
        _schedule = schedule;
    }

    public Map<ScheduleBellEntry, SppScheduleWeekPeriodsRowVO> getWeekPeriodsMap()
    {
        return _weekPeriodsMap;
    }

    public void setWeekPeriodsMap(Map<ScheduleBellEntry, SppScheduleWeekPeriodsRowVO> weekPeriodsMap)
    {
        _weekPeriodsMap = weekPeriodsMap;
        _weekPeriods = Lists.newArrayList(weekPeriodsMap.values());
        Collections.sort(_weekPeriods, new Comparator<SppScheduleWeekPeriodsRowVO>()
        {
            @Override
            public int compare(SppScheduleWeekPeriodsRowVO o1, SppScheduleWeekPeriodsRowVO o2)
            {
                return o1.getTime().getNumber() - o2.getTime().getNumber();
            }
        });
    }

    public List<SppScheduleWeekPeriodsRowVO> getWeekPeriods()
    {
        return _weekPeriods;
    }
}

