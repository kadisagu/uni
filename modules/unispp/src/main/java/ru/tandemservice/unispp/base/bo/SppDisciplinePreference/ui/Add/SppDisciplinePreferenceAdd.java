/* $Id: */
package ru.tandemservice.unispp.base.bo.SppDisciplinePreference.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unispp.base.bo.SppDisciplinePreference.SppDisciplinePreferenceManager;

/**
 * @author vnekrasov
 * @since 7/4/14
 */
@Configuration
public class SppDisciplinePreferenceAdd extends BusinessComponentManager
{
    public static final String LECTURE_ROOM_DS = "lectureRoomDS";
    public static final String BUILDING_DS = "buildingDS";
    public static final String TEACHER_DS = "teacherDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(LECTURE_ROOM_DS, SppDisciplinePreferenceManager.instance().lectureRoomComboDSHandler()).addColumn("title", UniplacesPlace.title().s()).addColumn("location", UniplacesPlace.fullLocationInfo().s()))
                .addDataSource(selectDS(BUILDING_DS, SppDisciplinePreferenceManager.instance().buildingComboDSHandler()).addColumn("title", UniplacesBuilding.title().s()).addColumn("location", UniplacesBuilding.fullNumber().s()))
                .addDataSource(selectDS(TEACHER_DS, SppDisciplinePreferenceManager.instance().ppsComboDSHandler()).addColumn("fio", PpsEntry.titleFioInfoOrgUnit().s()))
                .create();
    }
}