/* $Id: SppScheduleDailyOrgUnitListUI.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.LearningProcessList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleRegistryDisciplineComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.LearningProcessList.SppScheduleLearningProcessList;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyDSHandler;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
public class SppScheduleDailyLearningProcessListUI extends UIPresenter
{
    private BaseSearchListDataSource _scheduleDS;

    public BaseSearchListDataSource getScheduleDS()
    {
        if(null == _scheduleDS)
            _scheduleDS = _uiConfig.getDataSource(SppScheduleDailyLearningProcessList.SCHEDULE_DS);
        return _scheduleDS;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleLearningProcessList.SCHEDULE_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleDailyDSHandler.PROP_TITLE, _uiSettings.get("title"));
            dataSource.put(SppScheduleDailyDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
            dataSource.putAll(_uiSettings.getAsMap(true, "courses", "eduLevels", "groups", "seasons", "orgUnitIds", "subjects"));
        }
        if(SppScheduleLearningProcessList.EDU_LEVEL_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "courses", "orgUnitIds"));
        }
        if(SppScheduleLearningProcessList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "courses", "eduLevels", "orgUnitIds"));
        }
        if(SppScheduleLearningProcessList.SUBJECT_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleRegistryDisciplineComboDSHandler.PROP_ORG_UNIT_IDS, _uiSettings.get("orgUnitIds"));
            dataSource.put(SppScheduleRegistryDisciplineComboDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
        }
        if(SppScheduleLearningProcessList.COURSE_DS.equals(dataSource.getName()))
        {
            dataSource.put("orgUnitIds", _uiSettings.get("orgUnitIds"));
        }
    }
}
