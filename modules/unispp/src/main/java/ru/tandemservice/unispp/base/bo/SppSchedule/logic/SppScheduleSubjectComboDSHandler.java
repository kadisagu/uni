/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.unispp.base.entity.SppRegElementExt;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;
import ru.tandemservice.unispp.base.vo.SppScheduleDisciplineVO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 10/1/13
 */
public class SppScheduleSubjectComboDSHandler extends SimpleTitledComboDataSourceHandler
{
    // properties
    public static final String PROP_GROUP_IDS = "groupIds";
    public static final String PROP_SEASON_ID = "seasonId";
    public static final String PROP_CHECK_DUPLICATES = "checkDuplicates";
    public static final String PROP_SCHEDULE_ID = "scheduleId";
    public static final String PROP_CURRENT_PERIOD_ID = "currentPeriodId";
    public static final String PROP_DAY_OF_WEEK = "dayOfWeek";

    /**
     * Хэндлер для селектов дисциплин.
     * На входе получает список групп (ids) и период расписания (id)
     * Для каждой группы берёт РУПы (объединение РУПов всех студентов группы соотв. части учебного года (из периода)) (группа -> множество РУПов).
     * Далее берёт дисциплины этих РУПов (получается множество множеств дисциплин).
     * Берёт пересечение этих множеств и возвращает эти дисциплины с формой ауд.нагрузки и семестром.
     */
    public SppScheduleSubjectComboDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String filter = input.getComboFilterByValue();
        final List<Long> groupIds = context.get(PROP_GROUP_IDS);
        final Long seasonId = context.get(PROP_SEASON_ID);

        final boolean checkDuplicates = context.getBoolean(PROP_CHECK_DUPLICATES, false);
        final Long scheduleId = context.get(PROP_SCHEDULE_ID);
        final Long currentPeriodId = context.get(PROP_CURRENT_PERIOD_ID);
        final Integer dayOfWeek = context.get(PROP_DAY_OF_WEEK);

        if (seasonId == null || groupIds == null || groupIds.isEmpty()) return super.execute(input, context);

        // возьмём нужную часть учебного года
        DQLSelectBuilder yearPartBuilder = new DQLSelectBuilder()
                .fromEntity(SppScheduleSeason.class, "s").column("yp")
                .joinPath(DQLJoinType.inner, SppScheduleSeason.eppYearPart().fromAlias("s"), "yp")
                .fetchPath(DQLJoinType.inner, EppYearPart.year().educationYear().fromAlias("yp"), "year")
                .fetchPath(DQLJoinType.inner, EppYearPart.part().fromAlias("yp"), "part")
                .where(eq(property("s.id"), value(seasonId)));
        List<EppYearPart> yearPartList = createStatement(yearPartBuilder).list();
        if (yearPartList.isEmpty()) return super.execute(input, context);
        EppYearPart yearPart = yearPartList.get(0);
        Long yearId = yearPart.getYear().getEducationYear().getId();
        Long partId = yearPart.getPart().getId();

        // билдеры дисциплин
        List<DQLSelectBuilder> disciplineBuilderList = new ArrayList<>();
        for (Long groupId : groupIds)
        {
            // возьмём РУПы студентов этой группы
            DQLSelectBuilder wpBuilder = new DQLSelectBuilder();
            wpBuilder.fromEntity(EppStudent2WorkPlan.class, "st2wp");
            wpBuilder.column(property("wpb.id"), "wpbId");
            wpBuilder.column(caseExpr(isNotNull(property("wp.id")), property("wp.id"), property("wpv", EppWorkPlanVersion.parent().id())), "wpId");
            wpBuilder.joinPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("st2wp"), "st2epv");
            wpBuilder.joinPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias("st2wp"), "wpb");
            wpBuilder.joinEntity("wpb", DQLJoinType.left, EppWorkPlan.class, "wp", eq(property("wp.id"), property("wpb.id")));
            wpBuilder.joinEntity("wpb", DQLJoinType.left, EppWorkPlanVersion.class, "wpv", eq(property("wpv.id"), property("wpb.id")));
            wpBuilder.where(in(property("st2epv", EppStudent2EduPlanVersion.student().id()),
                    new DQLSelectBuilder().fromEntity(Student.class, "st").column("st.id")
                            .where(eq(property("st", Student.group().id()), value(groupId)))
                            .where(eq(property("st", Student.archival()), value(Boolean.FALSE)))
                            .buildQuery()));
            // только по соответствующей части уч. года
            wpBuilder.where(eq(property("st2wp", EppStudent2WorkPlan.cachedEppYear().educationYear().id()), value(yearId)));
            wpBuilder.where(eq(property("st2wp", EppStudent2WorkPlan.cachedGridTerm().part().id()), value(partId)));
            // только актуальные
            wpBuilder.where(isNull(property("st2wp", EppStudent2WorkPlan.removalDate())));
            wpBuilder.where(isNull(property("st2epv", EppStudent2EduPlanVersion.removalDate())));
            wpBuilder.distinct();

            // возьмём все дисциплины этих РУПов
            DQLSelectBuilder disciplineBuilder = new DQLSelectBuilder();
            disciplineBuilder.fromDataSource(wpBuilder.buildQuery(), "wpSelect");
            disciplineBuilder.column("d.id", "dId").column("term.id", "termId");
            disciplineBuilder.joinEntity("wpSelect", DQLJoinType.inner, EppWorkPlan.class, "wp", eq(property("wp.id"), property("wpSelect.wpId")));
            disciplineBuilder.joinPath(DQLJoinType.inner, EppWorkPlan.term().fromAlias("wp"), "term");
            disciplineBuilder.joinEntity("wpSelect", DQLJoinType.inner, EppWorkPlanRegistryElementRow.class, "r", eq(
                    property("r", EppWorkPlanRegistryElementRow.workPlan().id()), property("wpSelect.wpbId")));
            disciplineBuilder.joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().fromAlias("r"), "el");
            disciplineBuilder.joinEntity("el", DQLJoinType.inner, EppRegistryDiscipline.class, "d", eq(property("el.id"), property("d.id")));
            disciplineBuilder.distinct();

            disciplineBuilderList.add(disciplineBuilder);
        }
        // билдер для пересечения
        Iterator<DQLSelectBuilder> iter = disciplineBuilderList.iterator();
        DQLSelectBuilder builder = iter.next();
        while (iter.hasNext()) { builder.intersect(iter.next().buildQuery()); }

        DQLSelectBuilder finalBuilder = new DQLSelectBuilder().fromDataSource(builder.buildQuery(), "dSelect");
        finalBuilder.column("d").column("al").column("term");
        finalBuilder.joinEntity("dSelect", DQLJoinType.inner, EppRegistryDiscipline.class, "d", eq(property("dSelect.dId"), property("d.id")));
        finalBuilder.joinEntity("dSelect", DQLJoinType.inner, Term.class, "term", eq(property("dSelect.termId"), property("term.id")));
        // подцепим ауд.нагрузку
        finalBuilder.joinEntity("d", DQLJoinType.inner, EppRegistryElementLoad.class, "rel",
                eq(property("d.id"), property("rel", EppRegistryElementLoad.registryElement().id())));
        finalBuilder.where(gt(property("rel", EppRegistryElementLoad.load()), value(0)));
        finalBuilder.joinEntity("rel", DQLJoinType.inner, EppALoadType.class, "al",
                eq(property("rel", EppRegistryElementLoad.loadType().id()), property("al.id")));
        finalBuilder.distinct();

        // фильтруем дубли (такие же занятие в текущем дне)
        if (checkDuplicates)
        {
            finalBuilder.joinEntity("d", DQLJoinType.left, SppRegElementExt.class, "elExt", eq(property("d.id"), property("elExt", SppRegElementExt.registryElement().id())));
            // либо соотв. флаг на проверку в предмете сброшен, либо предмет не был проставлен в этот день
            finalBuilder.where(or(
                    and(isNotNull(property("elExt.id")), eq(property("elExt", SppRegElementExt.checkDuplicates()), value(false))),
                    notExists(new DQLSelectBuilder()
                            .fromEntity(SppSchedulePeriod.class, "per")
                            .where(eq(property("per", SppSchedulePeriod.weekRow().schedule().id()), value(scheduleId)))
                            .where(ne(property("per", SppSchedulePeriod.id()), value(currentPeriodId)))
                            .where(eq(property("per", SppSchedulePeriod.dayOfWeek()), value(dayOfWeek)))
                            .where(eq(property("per", SppSchedulePeriod.subjectVal().id()), property("d.id")))
                            .where(eq(property("per", SppSchedulePeriod.subjectLoadType().id()), property("al.id")))
                            .buildQuery())));
        }

        // текстовой фильтр
        if (!StringUtils.isEmpty(filter))
            finalBuilder.where(like(DQLFunctions.upper(property("d", EppRegistryDiscipline.title())), value(CoreStringUtils.escapeLike(filter, true))));

        List<Object[]> finalList = createStatement(finalBuilder).list();
        List<SppScheduleDisciplineVO> disciplineVOs = Lists.newArrayList();
        for (Object[] row : finalList)
        {
            EppRegistryDiscipline discipline = (EppRegistryDiscipline) row[0];
            EppALoadType loadType = (EppALoadType) row[1];
            Term term = (Term) row[2];
            if (discipline != null && loadType != null && term != null)
                disciplineVOs.add(new SppScheduleDisciplineVO(discipline, loadType, term));
        }

        // сортируем и отправляем наружу
        Collections.sort(disciplineVOs);
        context.put(UIDefines.COMBO_OBJECT_LIST, disciplineVOs);

        return super.execute(input, context);
    }
}
