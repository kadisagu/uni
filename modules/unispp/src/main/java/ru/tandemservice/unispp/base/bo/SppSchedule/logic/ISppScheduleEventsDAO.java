/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.joda.time.LocalDate;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppScheduleICal;
import ru.tandemservice.unispp.base.vo.SppScheduleEventInDayVO;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 2/14/14
 */
public interface ISppScheduleEventsDAO extends INeedPersistenceSupport
{
    void sendICalendars();

    void sendICalendar(SppSchedule schedule);

    boolean getScheduleDataOrRecipientsChanged(Long scheduleId);

    void cancelICalendar(List<SppScheduleICal> iCalDataList);

    Map<String, LocalDate> getDatesMap(Date startDate, Date endDate);


    @Transactional(readOnly = true)
    List<SppScheduleEventInDayVO> createAllScheduleEvents(List<Long> scheduleIdsList, boolean withFixes, Date dateFrom, Date dateTo, List<Long> teacherIds, List<Long> placeIds, List<Long> subjectIds);

    @Transactional(readOnly = true)
    List<SppScheduleEventInDayVO> createAllScheduleEvents(List<Long> scheduleIdsList, boolean withFixes);
}
