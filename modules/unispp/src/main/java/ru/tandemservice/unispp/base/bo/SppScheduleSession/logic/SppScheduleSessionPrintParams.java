/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;

import java.util.List;

/**
 * @author vnekrasov
 * @since 12/30/13
 */
public class SppScheduleSessionPrintParams
{
    private OrgUnit _formativeOrgUnit; // формирующее подразделение
    private OrgUnit _territorialOrgUnit; // территориальное подразделение
    private List<EducationLevelsHighSchool> _eduLevels; // направление подготовки (специальность)
    private DevelopForm _developForm;
    private List<Course> _courseList; // курс
    private SppScheduleSessionSeason _season; // период расписания - обяз. сел;
    private ScheduleBell _bells; // звонковое расписание
    private List<SppScheduleSession> _schedules; // расписание - мультиселект - (формат - колонки название расписания, группа)
    private DataWrapper _term; // семестр - селект, обяз;
    private EmployeePost _chief; // руководитель подразделения
    private EmployeePost _chiefUMU; // начальник УМУ подразделения
    private List<EmployeePost> _headers;
    private EmployeePost _admin; // администратор ООП - обяз.сел. - по умолчанию текущий пользовател2.
    private EducationYear _educationYear;
    private OrgUnit _currentOrgUnit;

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public List<EducationLevelsHighSchool> getEduLevels()
    {
        return _eduLevels;
    }

    public void setEduLevels(List<EducationLevelsHighSchool> eduLevels)
    {
        _eduLevels = eduLevels;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        _developForm = developForm;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public SppScheduleSessionSeason getSeason()
    {
        return _season;
    }

    public void setSeason(SppScheduleSessionSeason season)
    {
        _season = season;
    }

    public ScheduleBell getBells()
    {
        return _bells;
    }

    public void setBells(ScheduleBell bells)
    {
        _bells = bells;
    }

    public List<SppScheduleSession> getSchedules()
    {
        return _schedules;
    }

    public void setSchedules(List<SppScheduleSession> schedules)
    {
        _schedules = schedules;
    }

    public DataWrapper getTerm()
    {
        return _term;
    }

    public void setTerm(DataWrapper term)
    {
        _term = term;
    }

    public EmployeePost getChief()
    {
        return _chief;
    }

    public void setChief(EmployeePost chief)
    {
        _chief = chief;
    }

    public EmployeePost getChiefUMU()
    {
        return _chiefUMU;
    }

    public void setChiefUMU(EmployeePost chiefUMU)
    {
        _chiefUMU = chiefUMU;
    }

    public EmployeePost getAdmin()
    {
        return _admin;
    }

    public void setAdmin(EmployeePost admin)
    {
        _admin = admin;
    }

    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    public void setEducationYear(EducationYear educationYear)
    {
        _educationYear = educationYear;
    }

    public OrgUnit getCurrentOrgUnit()
    {
        return _currentOrgUnit;
    }

    public void setCurrentOrgUnit(OrgUnit currentOrgUnit)
    {
        _currentOrgUnit = currentOrgUnit;
    }

    public List<EmployeePost> getHeaders()
    {
        return _headers;
    }

    public void setHeaders(List<EmployeePost> headers)
    {
        _headers = headers;
    }
}
