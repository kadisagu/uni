/* $Id: */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionWarningLog;

/**
 * @author vnekrasov
 * @since 7/14/15
 */
public class SppScheduleSessionWarningLogDSHandler extends DefaultSearchDataSourceHandler
{
    public SppScheduleSessionWarningLogDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long scheduleSessionId = context.get("scheduleSessionId");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleSessionWarningLog.class, "w");

        if(null != scheduleSessionId)
            builder.where(DQLExpressions.eq(DQLExpressions.property("w", SppScheduleSessionWarningLog.sppScheduleSession().id()), DQLExpressions.value(scheduleSessionId)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order().build();
    }
}
