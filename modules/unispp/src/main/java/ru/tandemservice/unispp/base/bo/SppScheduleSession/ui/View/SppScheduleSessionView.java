/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionEventDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionWarningLogDSHandler;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionEvent;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionWarningLog;


/**
 * @author vnekrasov
 * @since 12/26/13
 */
@Configuration
public class SppScheduleSessionView extends BusinessComponentManager
{
    public final static String SCHEDULE_EVENTS_DS = "eventsDS";
    public final static String WARNING_LOG_DS = "warningLogDS";

    @Bean
    public ColumnListExtPoint scheduleEventsCL()
    {
        return columnListExtPointBuilder(SCHEDULE_EVENTS_DS)
                .addColumn(dateColumn("date", SppScheduleSessionEvent.date()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().required(true))
                .addColumn(textColumn("bell", SppScheduleSessionEvent.bell().time()).required(true))
                .addColumn(textColumn("lectureRoom", SppScheduleSessionEvent.lectureRoom()))
                .addColumn(textColumn("subject", SppScheduleSessionEvent.subject()))
                .addColumn(textColumn("teacher", SppScheduleSessionEvent.teacher()))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onClickEditEvent").permissionKey("sppScheduleSessionViewEdit").disabled("ui:eventEditDisabled"))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onClickDeleteEvent").permissionKey("sppScheduleSessionViewEdit").disabled("ui:eventDeleteDisabled"))
                .create();
    }

    @Bean
    public ColumnListExtPoint warningLogCL()
    {
        return columnListExtPointBuilder(WARNING_LOG_DS)
                .addColumn(textColumn("warningMessage", SppScheduleSessionWarningLog.warningMessage()))
                .create();

    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SCHEDULE_EVENTS_DS, scheduleEventsCL(), scheduleEventsDSHandler()))
                .addDataSource(searchListDS(WARNING_LOG_DS, warningLogCL(), warningLogDSHandler()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler warningLogDSHandler()
    {
        return new SppScheduleSessionWarningLogDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler scheduleEventsDSHandler()
    {
        return new SppScheduleSessionEventDSHandler(getName());
    }
}
