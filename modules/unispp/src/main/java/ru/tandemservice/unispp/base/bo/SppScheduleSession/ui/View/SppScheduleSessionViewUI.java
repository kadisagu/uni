/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.View;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.AddEdit.SppScheduleSessionAddEdit;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.Copy.SppScheduleSessionCopy;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.EventAddEdit.SppScheduleSessionEventAddEdit;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.catalog.codes.SppScheduleStatusCodes;

import java.util.Map;

/**
 * @author vnekrasov
 * @since 12/26/13
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "scheduleId", required = true),
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class SppScheduleSessionViewUI extends UIPresenter
{
    private Long _scheduleId;
    private SppScheduleSession _schedule;
    private BaseSearchListDataSource _warningLogDS;
    private Long _orgUnitId;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

   public BaseSearchListDataSource getWarningLogDS()
      {
          if (_warningLogDS == null)
              _warningLogDS = getConfig().getDataSource(SppScheduleSessionView.WARNING_LOG_DS);
          return _warningLogDS;
      }

    public Long getScheduleId()
    {
        return _scheduleId;
    }

    public void setScheduleId(Long scheduleId)
    {
        _scheduleId = scheduleId;
    }

    //На формировании
    public Boolean getScheduleOnFormation()
    {
        return SppScheduleStatusCodes.FORMATION.equals(_schedule.getStatus().getCode());
    }

    //Утверждено
    public Boolean getScheduleApproved()
    {
        return _schedule.isApproved();
    }


    // В архиве
    public Boolean getScheduleArchived()
    {
        return _schedule.isArchived();
    }

    //    visible="ui:editScheduleVisible"
    public Boolean getEditScheduleVisible()
    {
        return getScheduleOnFormation() && !getScheduleArchived();
    }

    //    visible="ui:sentToArchiveVisible"
    public Boolean getSentToArchiveVisible()
    {
        return !getScheduleArchived();
    }
    //    visible="ui:restoreFromArchiveVisible"
    public Boolean getRestoreFromArchiveVisible()
    {
        return getScheduleArchived() && !_schedule.getGroup().isArchival();
    }

    //    visible="ui:approveVisible"
    public Boolean getApproveVisible()
    {
        return getScheduleOnFormation() && !getScheduleArchived();
    }

    //    visible="ui:sentToFormationVisible"
    public Boolean getSentToFormationVisible()
    {
        return getScheduleApproved() && !getScheduleArchived();
    }

    public Boolean getSentToPortalVisible()
    {
        boolean dataOrRecipientsChanged = SppScheduleSessionManager.instance().dao().getScheduleDataOrRecipientsChanged(_schedule.getId());
        return getScheduleApproved() && !getScheduleArchived() && dataOrRecipientsChanged;
    }

    public Boolean getAddEventVisible()
    {
        return !getScheduleArchived() && getScheduleOnFormation();
    }

    //    ui:eventDeleteDisabled
    public Boolean getEventDeleteDisabled()
    {
        return getScheduleArchived() || getScheduleApproved();
    }

    //    ui:eventEditDisabled
    public Boolean getEventEditDisabled()
    {
        return getScheduleArchived() || getScheduleApproved();
    }

    public Map getGroupParameterMap()
    {
        return new ParametersMap()
                .add(UIPresenter.PUBLISHER_ID, _schedule.getGroup().getId())
                .add("selectedTabId", "sppScheduleSessionTab");
    }

    @Override
    public void onComponentRefresh()
    {
        _schedule = DataAccessServices.dao().getNotNull(_scheduleId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleSessionView.SCHEDULE_EVENTS_DS.equals(dataSource.getName()))
        {
            dataSource.put("scheduleId", _schedule.getId());
        }

        if(SppScheduleSessionView.WARNING_LOG_DS.equals(dataSource.getName()))
        {
            dataSource.put("scheduleSessionId", _schedule.getId());
        }
    }

    public void onClickEditSchedule()
    {
        _uiActivation.asRegionDialog(SppScheduleSessionAddEdit.class)
                .parameter("scheduleId", _scheduleId)
                .parameter("orgUnitId", _orgUnitId)
                .activate();
    }

    public void onClickCopySchedule()
    {
        _uiActivation.asRegion(SppScheduleSessionCopy.class).parameter("scheduleSessionId", _scheduleId).activate();
    }

    public void onClickAddEvent()
    {
        _uiActivation.asRegion(SppScheduleSessionEventAddEdit.class).parameter("scheduleId", _scheduleId).activate();
    }

    public void onClickSentToArchive()
    {
        SppScheduleSessionManager.instance().dao().sentToArchive(_schedule.getId());
    }

    public void onClickRestoreFromArchive()
    {
        SppScheduleSessionManager.instance().dao().restoreFromArchive(_schedule.getId());
    }

    public void onClickPerformValidation()
    {
        SppScheduleSessionManager.instance().dao().validate(_schedule);
    }

    public void onClickApprove()
    {
        SppScheduleSessionManager.instance().dao().approve(_schedule.getId());
    }

    public void onClickSentToFormation()
    {
        SppScheduleSessionManager.instance().dao().sentToFormation(_schedule.getId());
    }

    public void onClickCreateICal()
    {
        SppScheduleSessionManager.instance().dao().sendICalendar(_schedule.getId());
    }

    public void onClickEditEvent()
    {
        _uiActivation.asRegion(SppScheduleSessionEventAddEdit.class)
                .parameter("scheduleId", _scheduleId)
                .parameter("eventId", getListenerParameterAsLong()).activate();
    }

    public void onClickDeleteEvent()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        updateScheduleAsChanged();
    }

    private void updateScheduleAsChanged()
    {
        SppScheduleSessionManager.instance().dao().updateScheduleAsChanged(_schedule.getId());
    }

    public void onPrint()
    {
//        List<FefuScheduleGroupPrintVO> groupPrintVOs = Lists.newArrayList();
//        groupPrintVOs.add(FefuScheduleManager.instance().dao().prepareScheduleGroupPrintVO(_schedule));
//        try
//        {
//            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("xls").fileName("schedule.xls").document(FefuSchedulePrintFormExcelContentBuilder.buildExcelContent(groupPrintVOs, null)), true);
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
    }
}
