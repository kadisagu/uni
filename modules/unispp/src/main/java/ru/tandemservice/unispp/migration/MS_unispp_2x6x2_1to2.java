package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unispp_2x6x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherDailyPreferenceList

		// создано обязательное свойство title
		{
            if (!tool.columnExists("spptchrdlyprfrnclst_t", "title_p"))
            {
			// создать колонку
			tool.createColumn("spptchrdlyprfrnclst_t", new DBColumn("title_p", DBType.createVarchar(255)));



			// задать значение по умолчанию
			java.lang.String defaultTitle = "test";
			tool.executeUpdate("update spptchrdlyprfrnclst_t set title_p=? where title_p is null", defaultTitle);

			// сделать колонку NOT NULL
			tool.setColumnNullable("spptchrdlyprfrnclst_t", "title_p", false);
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherPreferenceList

		// создано обязательное свойство title
		{
            if (!tool.columnExists("sppteacherpreferencelist_t", "title_p"))
            {
			// создать колонку
			tool.createColumn("sppteacherpreferencelist_t", new DBColumn("title_p", DBType.createVarchar(255)));

			// задать значение по умолчанию
			java.lang.String defaultTitle = "test";
			tool.executeUpdate("update sppteacherpreferencelist_t set title_p=? where title_p is null", defaultTitle);

			// сделать колонку NOT NULL
			tool.setColumnNullable("sppteacherpreferencelist_t", "title_p", false);
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherSessionPreferenceList

		// создано обязательное свойство title
		{
            if (!tool.columnExists("spptchrsssnprfrnclst_t", "title_p"))
            {
			// создать колонку
			tool.createColumn("spptchrsssnprfrnclst_t", new DBColumn("title_p", DBType.createVarchar(255)));

			// задать значение по умолчанию
		    java.lang.String defaultTitle = "test";
			tool.executeUpdate("update spptchrsssnprfrnclst_t set title_p=? where title_p is null", defaultTitle);

			// сделать колонку NOT NULL
			tool.setColumnNullable("spptchrsssnprfrnclst_t", "title_p", false);
            }
       }


    }
}