package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionWarningLog;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Журнал ошибок и предупреждений расписания сессии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppScheduleSessionWarningLogGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppScheduleSessionWarningLog";
    public static final String ENTITY_NAME = "sppScheduleSessionWarningLog";
    public static final int VERSION_HASH = -149032577;
    private static IEntityMeta ENTITY_META;

    public static final String L_SPP_SCHEDULE_SESSION = "sppScheduleSession";
    public static final String P_WARNING_MESSAGE = "warningMessage";

    private SppScheduleSession _sppScheduleSession;     // Расписание сессии
    private String _warningMessage;     // Сообщение о предупреждении

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Расписание сессии. Свойство не может быть null.
     */
    @NotNull
    public SppScheduleSession getSppScheduleSession()
    {
        return _sppScheduleSession;
    }

    /**
     * @param sppScheduleSession Расписание сессии. Свойство не может быть null.
     */
    public void setSppScheduleSession(SppScheduleSession sppScheduleSession)
    {
        dirty(_sppScheduleSession, sppScheduleSession);
        _sppScheduleSession = sppScheduleSession;
    }

    /**
     * @return Сообщение о предупреждении. Свойство не может быть null.
     */
    @NotNull
    @Length(max=1024)
    public String getWarningMessage()
    {
        return _warningMessage;
    }

    /**
     * @param warningMessage Сообщение о предупреждении. Свойство не может быть null.
     */
    public void setWarningMessage(String warningMessage)
    {
        dirty(_warningMessage, warningMessage);
        _warningMessage = warningMessage;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppScheduleSessionWarningLogGen)
        {
            setSppScheduleSession(((SppScheduleSessionWarningLog)another).getSppScheduleSession());
            setWarningMessage(((SppScheduleSessionWarningLog)another).getWarningMessage());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppScheduleSessionWarningLogGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppScheduleSessionWarningLog.class;
        }

        public T newInstance()
        {
            return (T) new SppScheduleSessionWarningLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sppScheduleSession":
                    return obj.getSppScheduleSession();
                case "warningMessage":
                    return obj.getWarningMessage();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sppScheduleSession":
                    obj.setSppScheduleSession((SppScheduleSession) value);
                    return;
                case "warningMessage":
                    obj.setWarningMessage((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sppScheduleSession":
                        return true;
                case "warningMessage":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sppScheduleSession":
                    return true;
                case "warningMessage":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sppScheduleSession":
                    return SppScheduleSession.class;
                case "warningMessage":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppScheduleSessionWarningLog> _dslPath = new Path<SppScheduleSessionWarningLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppScheduleSessionWarningLog");
    }
            

    /**
     * @return Расписание сессии. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionWarningLog#getSppScheduleSession()
     */
    public static SppScheduleSession.Path<SppScheduleSession> sppScheduleSession()
    {
        return _dslPath.sppScheduleSession();
    }

    /**
     * @return Сообщение о предупреждении. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionWarningLog#getWarningMessage()
     */
    public static PropertyPath<String> warningMessage()
    {
        return _dslPath.warningMessage();
    }

    public static class Path<E extends SppScheduleSessionWarningLog> extends EntityPath<E>
    {
        private SppScheduleSession.Path<SppScheduleSession> _sppScheduleSession;
        private PropertyPath<String> _warningMessage;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Расписание сессии. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionWarningLog#getSppScheduleSession()
     */
        public SppScheduleSession.Path<SppScheduleSession> sppScheduleSession()
        {
            if(_sppScheduleSession == null )
                _sppScheduleSession = new SppScheduleSession.Path<SppScheduleSession>(L_SPP_SCHEDULE_SESSION, this);
            return _sppScheduleSession;
        }

    /**
     * @return Сообщение о предупреждении. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionWarningLog#getWarningMessage()
     */
        public PropertyPath<String> warningMessage()
        {
            if(_warningMessage == null )
                _warningMessage = new PropertyPath<String>(SppScheduleSessionWarningLogGen.P_WARNING_MESSAGE, this);
            return _warningMessage;
        }

        public Class getEntityClass()
        {
            return SppScheduleSessionWarningLog.class;
        }

        public String getEntityName()
        {
            return "sppScheduleSessionWarningLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
