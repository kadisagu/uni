package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список предпочтений по аудиториям
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppDisciplinePreferenceListGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList";
    public static final String ENTITY_NAME = "sppDisciplinePreferenceList";
    public static final int VERSION_HASH = 22989215;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISCIPLINE = "discipline";
    public static final String L_TEACHER = "teacher";
    public static final String L_BUILDING = "building";
    public static final String L_LECTURE_ROOM = "lectureRoom";

    private EppRegistryElement _discipline;     // Предмет
    private PpsEntry _teacher;     // Преподаватель
    private UniplacesBuilding _building;     // Здание
    private UniplacesPlace _lectureRoom;     // Аудитория

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Предмет. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Предмет. Свойство не может быть null.
     */
    public void setDiscipline(EppRegistryElement discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Преподаватель.
     */
    public PpsEntry getTeacher()
    {
        return _teacher;
    }

    /**
     * @param teacher Преподаватель.
     */
    public void setTeacher(PpsEntry teacher)
    {
        dirty(_teacher, teacher);
        _teacher = teacher;
    }

    /**
     * @return Здание.
     */
    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    /**
     * @param building Здание.
     */
    public void setBuilding(UniplacesBuilding building)
    {
        dirty(_building, building);
        _building = building;
    }

    /**
     * @return Аудитория.
     */
    public UniplacesPlace getLectureRoom()
    {
        return _lectureRoom;
    }

    /**
     * @param lectureRoom Аудитория.
     */
    public void setLectureRoom(UniplacesPlace lectureRoom)
    {
        dirty(_lectureRoom, lectureRoom);
        _lectureRoom = lectureRoom;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppDisciplinePreferenceListGen)
        {
            setDiscipline(((SppDisciplinePreferenceList)another).getDiscipline());
            setTeacher(((SppDisciplinePreferenceList)another).getTeacher());
            setBuilding(((SppDisciplinePreferenceList)another).getBuilding());
            setLectureRoom(((SppDisciplinePreferenceList)another).getLectureRoom());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppDisciplinePreferenceListGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppDisciplinePreferenceList.class;
        }

        public T newInstance()
        {
            return (T) new SppDisciplinePreferenceList();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "discipline":
                    return obj.getDiscipline();
                case "teacher":
                    return obj.getTeacher();
                case "building":
                    return obj.getBuilding();
                case "lectureRoom":
                    return obj.getLectureRoom();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "discipline":
                    obj.setDiscipline((EppRegistryElement) value);
                    return;
                case "teacher":
                    obj.setTeacher((PpsEntry) value);
                    return;
                case "building":
                    obj.setBuilding((UniplacesBuilding) value);
                    return;
                case "lectureRoom":
                    obj.setLectureRoom((UniplacesPlace) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "discipline":
                        return true;
                case "teacher":
                        return true;
                case "building":
                        return true;
                case "lectureRoom":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "discipline":
                    return true;
                case "teacher":
                    return true;
                case "building":
                    return true;
                case "lectureRoom":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "discipline":
                    return EppRegistryElement.class;
                case "teacher":
                    return PpsEntry.class;
                case "building":
                    return UniplacesBuilding.class;
                case "lectureRoom":
                    return UniplacesPlace.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppDisciplinePreferenceList> _dslPath = new Path<SppDisciplinePreferenceList>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppDisciplinePreferenceList");
    }
            

    /**
     * @return Предмет. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList#getDiscipline()
     */
    public static EppRegistryElement.Path<EppRegistryElement> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList#getTeacher()
     */
    public static PpsEntry.Path<PpsEntry> teacher()
    {
        return _dslPath.teacher();
    }

    /**
     * @return Здание.
     * @see ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList#getBuilding()
     */
    public static UniplacesBuilding.Path<UniplacesBuilding> building()
    {
        return _dslPath.building();
    }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList#getLectureRoom()
     */
    public static UniplacesPlace.Path<UniplacesPlace> lectureRoom()
    {
        return _dslPath.lectureRoom();
    }

    public static class Path<E extends SppDisciplinePreferenceList> extends EntityPath<E>
    {
        private EppRegistryElement.Path<EppRegistryElement> _discipline;
        private PpsEntry.Path<PpsEntry> _teacher;
        private UniplacesBuilding.Path<UniplacesBuilding> _building;
        private UniplacesPlace.Path<UniplacesPlace> _lectureRoom;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Предмет. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList#getDiscipline()
     */
        public EppRegistryElement.Path<EppRegistryElement> discipline()
        {
            if(_discipline == null )
                _discipline = new EppRegistryElement.Path<EppRegistryElement>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList#getTeacher()
     */
        public PpsEntry.Path<PpsEntry> teacher()
        {
            if(_teacher == null )
                _teacher = new PpsEntry.Path<PpsEntry>(L_TEACHER, this);
            return _teacher;
        }

    /**
     * @return Здание.
     * @see ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList#getBuilding()
     */
        public UniplacesBuilding.Path<UniplacesBuilding> building()
        {
            if(_building == null )
                _building = new UniplacesBuilding.Path<UniplacesBuilding>(L_BUILDING, this);
            return _building;
        }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList#getLectureRoom()
     */
        public UniplacesPlace.Path<UniplacesPlace> lectureRoom()
        {
            if(_lectureRoom == null )
                _lectureRoom = new UniplacesPlace.Path<UniplacesPlace>(L_LECTURE_ROOM, this);
            return _lectureRoom;
        }

        public Class getEntityClass()
        {
            return SppDisciplinePreferenceList.class;
        }

        public String getEntityName()
        {
            return "sppDisciplinePreferenceList";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
