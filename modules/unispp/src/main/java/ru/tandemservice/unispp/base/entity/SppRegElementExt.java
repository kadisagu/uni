package ru.tandemservice.unispp.base.entity;

import ru.tandemservice.unispp.base.entity.gen.*;

/**
 * Доп. характеристики элемента реестра
 *
 * Чекбокс "Проверять, что разные типы аудиторных занятий проводятся в разные дни недели"
 */
public class SppRegElementExt extends SppRegElementExtGen
{
}