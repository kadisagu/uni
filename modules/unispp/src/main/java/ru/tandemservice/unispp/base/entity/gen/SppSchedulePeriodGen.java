package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;
import ru.tandemservice.unispp.base.entity.SppScheduleWeekRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Пара
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppSchedulePeriodGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppSchedulePeriod";
    public static final String ENTITY_NAME = "sppSchedulePeriod";
    public static final int VERSION_HASH = -753907753;
    private static IEntityMeta ENTITY_META;

    public static final String P_DAY_OF_WEEK = "dayOfWeek";
    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_EMPTY = "empty";
    public static final String P_LECTURE_ROOM = "lectureRoom";
    public static final String P_TEACHER = "teacher";
    public static final String P_SUBJECT = "subject";
    public static final String P_PERIOD_NUM = "periodNum";
    public static final String P_PERIOD_GROUP = "periodGroup";
    public static final String L_WEEK_ROW = "weekRow";
    public static final String L_TEACHER_VAL = "teacherVal";
    public static final String L_SUBJECT_VAL = "subjectVal";
    public static final String L_SUBJECT_LOAD_TYPE = "subjectLoadType";
    public static final String L_LECTURE_ROOM_VAL = "lectureRoomVal";

    private int _dayOfWeek;     // День недели
    private Date _startDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private boolean _empty;     // Занятия нет
    private String _lectureRoom;     // Аудитория
    private String _teacher;     // Преподаватель
    private String _subject;     // Предмет
    private int _periodNum;     // Номер
    private String _periodGroup;     // Подгруппа
    private SppScheduleWeekRow _weekRow;     // Недельная строка расписания
    private PpsEntry _teacherVal;     // Преподаватель (сущность)
    private EppRegistryElement _subjectVal;     // Предмет(сущность)
    private EppALoadType _subjectLoadType;     // Тип нагрузки(сущность)
    private UniplacesPlace _lectureRoomVal;     // Аудитория(сущность)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return День недели. Свойство не может быть null.
     */
    @NotNull
    public int getDayOfWeek()
    {
        return _dayOfWeek;
    }

    /**
     * @param dayOfWeek День недели. Свойство не может быть null.
     */
    public void setDayOfWeek(int dayOfWeek)
    {
        dirty(_dayOfWeek, dayOfWeek);
        _dayOfWeek = dayOfWeek;
    }

    /**
     * @return Дата начала.
     */
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Занятия нет. Свойство не может быть null.
     */
    @NotNull
    public boolean isEmpty()
    {
        return _empty;
    }

    /**
     * @param empty Занятия нет. Свойство не может быть null.
     */
    public void setEmpty(boolean empty)
    {
        dirty(_empty, empty);
        _empty = empty;
    }

    /**
     * @return Аудитория.
     */
    @Length(max=255)
    public String getLectureRoom()
    {
        return _lectureRoom;
    }

    /**
     * @param lectureRoom Аудитория.
     */
    public void setLectureRoom(String lectureRoom)
    {
        dirty(_lectureRoom, lectureRoom);
        _lectureRoom = lectureRoom;
    }

    /**
     * @return Преподаватель.
     */
    @Length(max=255)
    public String getTeacher()
    {
        return _teacher;
    }

    /**
     * @param teacher Преподаватель.
     */
    public void setTeacher(String teacher)
    {
        dirty(_teacher, teacher);
        _teacher = teacher;
    }

    /**
     * @return Предмет.
     */
    @Length(max=255)
    public String getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Предмет.
     */
    public void setSubject(String subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    public int getPeriodNum()
    {
        return _periodNum;
    }

    /**
     * @param periodNum Номер. Свойство не может быть null.
     */
    public void setPeriodNum(int periodNum)
    {
        dirty(_periodNum, periodNum);
        _periodNum = periodNum;
    }

    /**
     * @return Подгруппа.
     */
    @Length(max=255)
    public String getPeriodGroup()
    {
        return _periodGroup;
    }

    /**
     * @param periodGroup Подгруппа.
     */
    public void setPeriodGroup(String periodGroup)
    {
        dirty(_periodGroup, periodGroup);
        _periodGroup = periodGroup;
    }

    /**
     * @return Недельная строка расписания.
     */
    public SppScheduleWeekRow getWeekRow()
    {
        return _weekRow;
    }

    /**
     * @param weekRow Недельная строка расписания.
     */
    public void setWeekRow(SppScheduleWeekRow weekRow)
    {
        dirty(_weekRow, weekRow);
        _weekRow = weekRow;
    }

    /**
     * @return Преподаватель (сущность).
     */
    public PpsEntry getTeacherVal()
    {
        return _teacherVal;
    }

    /**
     * @param teacherVal Преподаватель (сущность).
     */
    public void setTeacherVal(PpsEntry teacherVal)
    {
        dirty(_teacherVal, teacherVal);
        _teacherVal = teacherVal;
    }

    /**
     * @return Предмет(сущность).
     */
    public EppRegistryElement getSubjectVal()
    {
        return _subjectVal;
    }

    /**
     * @param subjectVal Предмет(сущность).
     */
    public void setSubjectVal(EppRegistryElement subjectVal)
    {
        dirty(_subjectVal, subjectVal);
        _subjectVal = subjectVal;
    }

    /**
     * @return Тип нагрузки(сущность).
     */
    public EppALoadType getSubjectLoadType()
    {
        return _subjectLoadType;
    }

    /**
     * @param subjectLoadType Тип нагрузки(сущность).
     */
    public void setSubjectLoadType(EppALoadType subjectLoadType)
    {
        dirty(_subjectLoadType, subjectLoadType);
        _subjectLoadType = subjectLoadType;
    }

    /**
     * @return Аудитория(сущность).
     */
    public UniplacesPlace getLectureRoomVal()
    {
        return _lectureRoomVal;
    }

    /**
     * @param lectureRoomVal Аудитория(сущность).
     */
    public void setLectureRoomVal(UniplacesPlace lectureRoomVal)
    {
        dirty(_lectureRoomVal, lectureRoomVal);
        _lectureRoomVal = lectureRoomVal;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppSchedulePeriodGen)
        {
            setDayOfWeek(((SppSchedulePeriod)another).getDayOfWeek());
            setStartDate(((SppSchedulePeriod)another).getStartDate());
            setEndDate(((SppSchedulePeriod)another).getEndDate());
            setEmpty(((SppSchedulePeriod)another).isEmpty());
            setLectureRoom(((SppSchedulePeriod)another).getLectureRoom());
            setTeacher(((SppSchedulePeriod)another).getTeacher());
            setSubject(((SppSchedulePeriod)another).getSubject());
            setPeriodNum(((SppSchedulePeriod)another).getPeriodNum());
            setPeriodGroup(((SppSchedulePeriod)another).getPeriodGroup());
            setWeekRow(((SppSchedulePeriod)another).getWeekRow());
            setTeacherVal(((SppSchedulePeriod)another).getTeacherVal());
            setSubjectVal(((SppSchedulePeriod)another).getSubjectVal());
            setSubjectLoadType(((SppSchedulePeriod)another).getSubjectLoadType());
            setLectureRoomVal(((SppSchedulePeriod)another).getLectureRoomVal());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppSchedulePeriodGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppSchedulePeriod.class;
        }

        public T newInstance()
        {
            return (T) new SppSchedulePeriod();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "dayOfWeek":
                    return obj.getDayOfWeek();
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
                case "empty":
                    return obj.isEmpty();
                case "lectureRoom":
                    return obj.getLectureRoom();
                case "teacher":
                    return obj.getTeacher();
                case "subject":
                    return obj.getSubject();
                case "periodNum":
                    return obj.getPeriodNum();
                case "periodGroup":
                    return obj.getPeriodGroup();
                case "weekRow":
                    return obj.getWeekRow();
                case "teacherVal":
                    return obj.getTeacherVal();
                case "subjectVal":
                    return obj.getSubjectVal();
                case "subjectLoadType":
                    return obj.getSubjectLoadType();
                case "lectureRoomVal":
                    return obj.getLectureRoomVal();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "dayOfWeek":
                    obj.setDayOfWeek((Integer) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "empty":
                    obj.setEmpty((Boolean) value);
                    return;
                case "lectureRoom":
                    obj.setLectureRoom((String) value);
                    return;
                case "teacher":
                    obj.setTeacher((String) value);
                    return;
                case "subject":
                    obj.setSubject((String) value);
                    return;
                case "periodNum":
                    obj.setPeriodNum((Integer) value);
                    return;
                case "periodGroup":
                    obj.setPeriodGroup((String) value);
                    return;
                case "weekRow":
                    obj.setWeekRow((SppScheduleWeekRow) value);
                    return;
                case "teacherVal":
                    obj.setTeacherVal((PpsEntry) value);
                    return;
                case "subjectVal":
                    obj.setSubjectVal((EppRegistryElement) value);
                    return;
                case "subjectLoadType":
                    obj.setSubjectLoadType((EppALoadType) value);
                    return;
                case "lectureRoomVal":
                    obj.setLectureRoomVal((UniplacesPlace) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "dayOfWeek":
                        return true;
                case "startDate":
                        return true;
                case "endDate":
                        return true;
                case "empty":
                        return true;
                case "lectureRoom":
                        return true;
                case "teacher":
                        return true;
                case "subject":
                        return true;
                case "periodNum":
                        return true;
                case "periodGroup":
                        return true;
                case "weekRow":
                        return true;
                case "teacherVal":
                        return true;
                case "subjectVal":
                        return true;
                case "subjectLoadType":
                        return true;
                case "lectureRoomVal":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "dayOfWeek":
                    return true;
                case "startDate":
                    return true;
                case "endDate":
                    return true;
                case "empty":
                    return true;
                case "lectureRoom":
                    return true;
                case "teacher":
                    return true;
                case "subject":
                    return true;
                case "periodNum":
                    return true;
                case "periodGroup":
                    return true;
                case "weekRow":
                    return true;
                case "teacherVal":
                    return true;
                case "subjectVal":
                    return true;
                case "subjectLoadType":
                    return true;
                case "lectureRoomVal":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "dayOfWeek":
                    return Integer.class;
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "empty":
                    return Boolean.class;
                case "lectureRoom":
                    return String.class;
                case "teacher":
                    return String.class;
                case "subject":
                    return String.class;
                case "periodNum":
                    return Integer.class;
                case "periodGroup":
                    return String.class;
                case "weekRow":
                    return SppScheduleWeekRow.class;
                case "teacherVal":
                    return PpsEntry.class;
                case "subjectVal":
                    return EppRegistryElement.class;
                case "subjectLoadType":
                    return EppALoadType.class;
                case "lectureRoomVal":
                    return UniplacesPlace.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppSchedulePeriod> _dslPath = new Path<SppSchedulePeriod>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppSchedulePeriod");
    }
            

    /**
     * @return День недели. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getDayOfWeek()
     */
    public static PropertyPath<Integer> dayOfWeek()
    {
        return _dslPath.dayOfWeek();
    }

    /**
     * @return Дата начала.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Занятия нет. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#isEmpty()
     */
    public static PropertyPath<Boolean> empty()
    {
        return _dslPath.empty();
    }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getLectureRoom()
     */
    public static PropertyPath<String> lectureRoom()
    {
        return _dslPath.lectureRoom();
    }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getTeacher()
     */
    public static PropertyPath<String> teacher()
    {
        return _dslPath.teacher();
    }

    /**
     * @return Предмет.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getSubject()
     */
    public static PropertyPath<String> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getPeriodNum()
     */
    public static PropertyPath<Integer> periodNum()
    {
        return _dslPath.periodNum();
    }

    /**
     * @return Подгруппа.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getPeriodGroup()
     */
    public static PropertyPath<String> periodGroup()
    {
        return _dslPath.periodGroup();
    }

    /**
     * @return Недельная строка расписания.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getWeekRow()
     */
    public static SppScheduleWeekRow.Path<SppScheduleWeekRow> weekRow()
    {
        return _dslPath.weekRow();
    }

    /**
     * @return Преподаватель (сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getTeacherVal()
     */
    public static PpsEntry.Path<PpsEntry> teacherVal()
    {
        return _dslPath.teacherVal();
    }

    /**
     * @return Предмет(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getSubjectVal()
     */
    public static EppRegistryElement.Path<EppRegistryElement> subjectVal()
    {
        return _dslPath.subjectVal();
    }

    /**
     * @return Тип нагрузки(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getSubjectLoadType()
     */
    public static EppALoadType.Path<EppALoadType> subjectLoadType()
    {
        return _dslPath.subjectLoadType();
    }

    /**
     * @return Аудитория(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getLectureRoomVal()
     */
    public static UniplacesPlace.Path<UniplacesPlace> lectureRoomVal()
    {
        return _dslPath.lectureRoomVal();
    }

    public static class Path<E extends SppSchedulePeriod> extends EntityPath<E>
    {
        private PropertyPath<Integer> _dayOfWeek;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Boolean> _empty;
        private PropertyPath<String> _lectureRoom;
        private PropertyPath<String> _teacher;
        private PropertyPath<String> _subject;
        private PropertyPath<Integer> _periodNum;
        private PropertyPath<String> _periodGroup;
        private SppScheduleWeekRow.Path<SppScheduleWeekRow> _weekRow;
        private PpsEntry.Path<PpsEntry> _teacherVal;
        private EppRegistryElement.Path<EppRegistryElement> _subjectVal;
        private EppALoadType.Path<EppALoadType> _subjectLoadType;
        private UniplacesPlace.Path<UniplacesPlace> _lectureRoomVal;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return День недели. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getDayOfWeek()
     */
        public PropertyPath<Integer> dayOfWeek()
        {
            if(_dayOfWeek == null )
                _dayOfWeek = new PropertyPath<Integer>(SppSchedulePeriodGen.P_DAY_OF_WEEK, this);
            return _dayOfWeek;
        }

    /**
     * @return Дата начала.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(SppSchedulePeriodGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(SppSchedulePeriodGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Занятия нет. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#isEmpty()
     */
        public PropertyPath<Boolean> empty()
        {
            if(_empty == null )
                _empty = new PropertyPath<Boolean>(SppSchedulePeriodGen.P_EMPTY, this);
            return _empty;
        }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getLectureRoom()
     */
        public PropertyPath<String> lectureRoom()
        {
            if(_lectureRoom == null )
                _lectureRoom = new PropertyPath<String>(SppSchedulePeriodGen.P_LECTURE_ROOM, this);
            return _lectureRoom;
        }

    /**
     * @return Преподаватель.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getTeacher()
     */
        public PropertyPath<String> teacher()
        {
            if(_teacher == null )
                _teacher = new PropertyPath<String>(SppSchedulePeriodGen.P_TEACHER, this);
            return _teacher;
        }

    /**
     * @return Предмет.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getSubject()
     */
        public PropertyPath<String> subject()
        {
            if(_subject == null )
                _subject = new PropertyPath<String>(SppSchedulePeriodGen.P_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getPeriodNum()
     */
        public PropertyPath<Integer> periodNum()
        {
            if(_periodNum == null )
                _periodNum = new PropertyPath<Integer>(SppSchedulePeriodGen.P_PERIOD_NUM, this);
            return _periodNum;
        }

    /**
     * @return Подгруппа.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getPeriodGroup()
     */
        public PropertyPath<String> periodGroup()
        {
            if(_periodGroup == null )
                _periodGroup = new PropertyPath<String>(SppSchedulePeriodGen.P_PERIOD_GROUP, this);
            return _periodGroup;
        }

    /**
     * @return Недельная строка расписания.
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getWeekRow()
     */
        public SppScheduleWeekRow.Path<SppScheduleWeekRow> weekRow()
        {
            if(_weekRow == null )
                _weekRow = new SppScheduleWeekRow.Path<SppScheduleWeekRow>(L_WEEK_ROW, this);
            return _weekRow;
        }

    /**
     * @return Преподаватель (сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getTeacherVal()
     */
        public PpsEntry.Path<PpsEntry> teacherVal()
        {
            if(_teacherVal == null )
                _teacherVal = new PpsEntry.Path<PpsEntry>(L_TEACHER_VAL, this);
            return _teacherVal;
        }

    /**
     * @return Предмет(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getSubjectVal()
     */
        public EppRegistryElement.Path<EppRegistryElement> subjectVal()
        {
            if(_subjectVal == null )
                _subjectVal = new EppRegistryElement.Path<EppRegistryElement>(L_SUBJECT_VAL, this);
            return _subjectVal;
        }

    /**
     * @return Тип нагрузки(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getSubjectLoadType()
     */
        public EppALoadType.Path<EppALoadType> subjectLoadType()
        {
            if(_subjectLoadType == null )
                _subjectLoadType = new EppALoadType.Path<EppALoadType>(L_SUBJECT_LOAD_TYPE, this);
            return _subjectLoadType;
        }

    /**
     * @return Аудитория(сущность).
     * @see ru.tandemservice.unispp.base.entity.SppSchedulePeriod#getLectureRoomVal()
     */
        public UniplacesPlace.Path<UniplacesPlace> lectureRoomVal()
        {
            if(_lectureRoomVal == null )
                _lectureRoomVal = new UniplacesPlace.Path<UniplacesPlace>(L_LECTURE_ROOM_VAL, this);
            return _lectureRoomVal;
        }

        public Class getEntityClass()
        {
            return SppSchedulePeriod.class;
        }

        public String getEntityName()
        {
            return "sppSchedulePeriod";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
