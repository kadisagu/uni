/* $Id: */
package ru.tandemservice.unispp.base.vo;

import com.google.common.collect.Lists;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * @author vnekrasov
 * @since 6/23/14
 */
public class SppTeacherPreferenceWeekVO
{
    private SppTeacherPreferenceList _teacherPreference;

    private Map<ScheduleBellEntry, SppTeacherPreferenceWeekRowVO> _weekPeriodsMap;

    private List<SppTeacherPreferenceWeekRowVO> _weekPeriods;

    public SppTeacherPreferenceList getTeacherPreference()
    {
        return _teacherPreference;
    }

    public void setTeacherPreference(SppTeacherPreferenceList teacherPreference)
    {
        _teacherPreference = teacherPreference;
    }

    public Map<ScheduleBellEntry, SppTeacherPreferenceWeekRowVO> getWeekPeriodsMap()
    {
        return _weekPeriodsMap;
    }

    public void setWeekPeriodsMap(Map<ScheduleBellEntry, SppTeacherPreferenceWeekRowVO> weekPeriodsMap)
    {
        _weekPeriodsMap = weekPeriodsMap;
        _weekPeriods = Lists.newArrayList(weekPeriodsMap.values());
        Collections.sort(_weekPeriods, new Comparator<SppTeacherPreferenceWeekRowVO>()
        {
            @Override
            public int compare(SppTeacherPreferenceWeekRowVO o1, SppTeacherPreferenceWeekRowVO o2)
            {
                return o1.getTime().getNumber() - o2.getTime().getNumber();
            }
        });
    }

    public List<SppTeacherPreferenceWeekRowVO> getWeekPeriods()
    {
        return _weekPeriods;
    }
}

