/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.OrgUnitList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.View.SppScheduleView;
import ru.tandemservice.unispp.base.entity.SppSchedule;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 9/4/13
 */
@Configuration
public class SppScheduleOrgUnitList extends BusinessComponentManager
{
    public static final String SCHEDULE_DS = "scheduleDS";
    public static final String YEAR_PART_DS = "yearPartDS";
    public static final String COURSE_DS = "courseDS";
    public static final String GROUP_DS = "groupDS";
    public static final String EDU_LEVEL_DS = "eduLevelDS";

    @Bean
    public ColumnListExtPoint scheduleCL()
    {
        return columnListExtPointBuilder(SCHEDULE_DS)
                .addColumn(publisherColumn("title", SppSchedule.title()).businessComponent(SppScheduleView.class)
                                   .order().required(true).parameters("ui:parametersMap").width("300px"))
                .addColumn(textColumn("group", SppSchedule.group().title()).order().required(true).width("200px"))
                .addColumn(textColumn("yearPart", SppSchedule.season().eppYearPart().title()))
                .addColumn(dateColumn("startDate", SppSchedule.season().startDate()).order())
                .addColumn(dateColumn("endDate", SppSchedule.season().endDate()).order())
                .addColumn(booleanColumn("approved", SppSchedule.approved()))
                .addColumn(booleanColumn("archived", SppSchedule.archived()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled("ui:entityEditDisabled").permissionKey("ui:sec.orgUnit_editSppScheduleTab"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, new FormattedMessage("ui.deleteAlert", SppSchedule.title())).disabled("ui:entityDeleteDisabled").permissionKey("ui:sec.orgUnit_deleteSppScheduleTab"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SCHEDULE_DS, scheduleCL(), SppScheduleManager.instance().sppScheduleDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEAR_PART_DS, getName(), EppYearPart.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(COURSE_DS, SppScheduleManager.instance().courseComboDSHandler()))
                .addDataSource(selectDS(EDU_LEVEL_DS, eduLevelComboDSHandler()).addColumn(EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(selectDS(GROUP_DS, SppScheduleManager.instance().groupComboDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduLevelComboDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationLevelsHighSchool.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                Long orgUnitId = context.get("orgUnitId");
                List<Long> courses = context.get("courses");

                DQLSelectBuilder kindRelationsBuilder = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                        .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                        .where(eq(property("rel", OrgUnitToKindRelation.orgUnit().id()), value(orgUnitId)));

                List<OrgUnitKind> kindRelations = kindRelationsBuilder.createStatement(context.getSession()).list();

                Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                        .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

                IDQLExpression condition = nothing(); // false
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                    condition = or(condition, eq(property("g", Group.educationOrgUnit().educationLevelHighSchool().orgUnit().id()), value(orgUnitId)));
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                    condition = or(condition, eq(property("g", Group.educationOrgUnit().formativeOrgUnit().id()), value(orgUnitId)));
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                    condition = or(condition, eq(property("g", Group.educationOrgUnit().territorialOrgUnit().id()), value(orgUnitId)));

                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g");
                subBuilder.where(condition);
                subBuilder.where(eq(property("e"), property("g", Group.educationOrgUnit().educationLevelHighSchool())));

                if (null != courses && !courses.isEmpty())
                {
                    subBuilder.where(DQLExpressions.in(DQLExpressions.property("g", Group.course().id()), courses));
                }

                dql.where(exists(subBuilder.buildQuery()));
            }
        }
                .filter(EducationLevelsHighSchool.title())
                .order(EducationLevelsHighSchool.title());
    }
}
