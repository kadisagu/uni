/* $Id$ */
package ru.tandemservice.unispp.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 13.05.2013
 */
public class SppOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("unispp");
        config.setName("unispp-sec-config");
        config.setTitle("");

//
//        // права на сам объект «Расписание ДВФУ»
//
//        PermissionGroupMeta pg = CtrContractVersionUtil.registerPermissionGroup(config, FefuSchedule.class);
//        PermissionMetaUtil.createGroupRelation(config, pg.getName(), "fefuScheduleCG");
//        PermissionMetaUtil.createGroupRelation(config, pg.getName(), "fefuScheduleLC");

        ModuleGlobalGroupMeta moduleGlobalGroupMeta = PermissionMetaUtil.createModuleGlobalGroup(config, "orgstructModuleGlobalGroup", "Модуль «Орг. структура»");
        ModuleLocalGroupMeta moduleLocalGroupMeta = PermissionMetaUtil.createModuleLocalGroup(config, "orgstructLocalGroup", "Модуль «Орг. структура»");

        // для каждого типа подразделения добавляем права на Расписание(вкладка непосредственно подразделения)
        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();

            ClassGroupMeta globalClassGroup = PermissionMetaUtil.createClassGroup(moduleGlobalGroupMeta, code + "PermissionClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());
            ClassGroupMeta localClassGroup = PermissionMetaUtil.createClassGroup(moduleLocalGroupMeta, code + "LocalClassGroup", "Объект «" + description.getTitle() + "»", OrgUnit.class.getName());

            // Вкладка «Расписание»
            PermissionGroupMeta pgSppScheduleTab = PermissionMetaUtil.createPermissionGroup(config, code + "SppScheduleOrgUnitTabPermissionGroup", "Вкладка «Расписание»");
            PermissionMetaUtil.createGroupRelation(config, pgSppScheduleTab.getName(), globalClassGroup.getName());
            PermissionMetaUtil.createGroupRelation(config, pgSppScheduleTab.getName(), localClassGroup.getName());
            PermissionMetaUtil.createPermission(pgSppScheduleTab, "orgUnit_viewSppScheduleMainTab_" + code, "Просмотр");


            // "Подвкладка «Расписание групп»"
            final PermissionGroupMeta permissionGroupSchedule = PermissionMetaUtil.createPermissionGroup(pgSppScheduleTab, code + "sppScheduleTabPG", "Вкладка «Расписание групп»");
            PermissionMetaUtil.createPermission(permissionGroupSchedule, "orgUnit_viewSppScheduleTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupSchedule, "orgUnit_addSppScheduleTab_" + code, "Создание расписания");
            PermissionMetaUtil.createPermission(permissionGroupSchedule, "orgUnit_editSppScheduleTab_" + code, "Редактирование расписания");
            PermissionMetaUtil.createPermission(permissionGroupSchedule, "orgUnit_deleteSppScheduleTab_" + code, "Удаление расписания");


            // "Подвкладка «Печатные формы расписаний групп»"
            final PermissionGroupMeta permissionGroupSppSchedulePrintForms = PermissionMetaUtil.createPermissionGroup(pgSppScheduleTab, code + "sppSchedulePrintFormTabPG", "Вкладка «Печатные формы расписаний групп»");
            PermissionMetaUtil.createPermission(permissionGroupSppSchedulePrintForms, "orgUnit_viewSppSchedulePrintFormTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupSppSchedulePrintForms, "orgUnit_addSppSchedulePrintFormTab_" + code, "Добавление печатной формы");
            PermissionMetaUtil.createPermission(permissionGroupSppSchedulePrintForms, "orgUnit_getContentSppSchedulePrintFormTab_" + code, "Печать");
            PermissionMetaUtil.createPermission(permissionGroupSppSchedulePrintForms, "orgUnit_deleteSppSchedulePrintFormTab_" + code, "Удаление печатной формы");

            // "Подвкладка «Расписание сессии»"
            final PermissionGroupMeta permissionGroupScheduleSession = PermissionMetaUtil.createPermissionGroup(pgSppScheduleTab, code + "SppScheduleSessionTabPG", "Вкладка «Расписание сессий»");
            PermissionMetaUtil.createPermission(permissionGroupScheduleSession, "orgUnit_viewSppScheduleSessionTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupScheduleSession, "orgUnit_addSppScheduleSessionTab_" + code, "Создание расписания");
            PermissionMetaUtil.createPermission(permissionGroupScheduleSession, "orgUnit_editSppScheduleSessionTab_" + code, "Редактирование расписания");
            PermissionMetaUtil.createPermission(permissionGroupScheduleSession, "orgUnit_deleteSppScheduleSessionTab_" + code, "Удаление расписания");

            // "Подвкладка «Печатные формы расписаний сессии»"
            final PermissionGroupMeta permissionGroupSppScheduleSessionPrintForms = PermissionMetaUtil.createPermissionGroup(pgSppScheduleTab, code + "SppScheduleSessionPrintFormTab", "Вкладка «Печатные формы расписаний сессий»");
            PermissionMetaUtil.createPermission(permissionGroupSppScheduleSessionPrintForms, "orgUnit_viewSppScheduleSessionPrintFormTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupSppScheduleSessionPrintForms, "orgUnit_addSppScheduleSessionPrintFormTab_" + code, "Добавление печатной формы");
            PermissionMetaUtil.createPermission(permissionGroupSppScheduleSessionPrintForms, "orgUnit_getContentSppScheduleSessionPrintFormTab_" + code, "Печать");
            PermissionMetaUtil.createPermission(permissionGroupSppScheduleSessionPrintForms, "orgUnit_deleteSppScheduleSessionPrintFormTab_" + code, "Удаление печатной формы");

            // "Подвкладка «Подневное расписание»"
            final PermissionGroupMeta permissionGroupScheduleDaily = PermissionMetaUtil.createPermissionGroup(pgSppScheduleTab, code + "SppScheduleDailyTabPG", "Вкладка «Подневное расписание»");
            PermissionMetaUtil.createPermission(permissionGroupScheduleDaily, "orgUnit_viewSppScheduleDailyTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupScheduleDaily, "orgUnit_addSppScheduleDailyTab_" + code, "Создание расписания");
            PermissionMetaUtil.createPermission(permissionGroupScheduleDaily, "orgUnit_editSppScheduleDailyTab_" + code, "Редактирование расписания");
            PermissionMetaUtil.createPermission(permissionGroupScheduleDaily, "orgUnit_deleteSppScheduleDailyTab_" + code, "Удаление расписания");

            // "Подвкладка «Печатные формы подневного расписания»"
            final PermissionGroupMeta permissionGroupSppScheduleDailyPrintForms = PermissionMetaUtil.createPermissionGroup(pgSppScheduleTab, code + "SppScheduleDailyPrintFormTab", "Вкладка «Печатные формы подневных расписаний»");
            PermissionMetaUtil.createPermission(permissionGroupSppScheduleDailyPrintForms, "orgUnit_viewSppScheduleDailyPrintFormTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupSppScheduleDailyPrintForms, "orgUnit_addSppScheduleDailyPrintFormTab_" + code, "Добавление печатной формы");
            PermissionMetaUtil.createPermission(permissionGroupSppScheduleDailyPrintForms, "orgUnit_getContentSppScheduleDailyPrintFormTab_" + code, "Печать");
            PermissionMetaUtil.createPermission(permissionGroupSppScheduleDailyPrintForms, "orgUnit_deleteSppScheduleDailyPrintFormTab_" + code, "Удаление печатной формы");

            // "Подвкладка «Форма массового добавления занятий»"
            final PermissionGroupMeta permissionGroupSppScheduleExerciseMassAddingTab = PermissionMetaUtil.createPermissionGroup(pgSppScheduleTab, code + "SppScheduleExerciseMassAddingTab", "Вкладка «Форма массового добавления занятий»");
            PermissionMetaUtil.createPermission(permissionGroupSppScheduleExerciseMassAddingTab, "orgUnit_viewSppScheduleExerciseMassAddingTab_" + code, "Просмотр");

//            // Вкладка «Списки предпочтений преподавателя»
//            PermissionGroupMeta pgSppTeacherPreferenceTab = PermissionMetaUtil.createPermissionGroup(config, code + "SppTeacherPreferenceGroupTabPermissionGroup", "Вкладка «Списки предпочтений преподавателя»");
//            PermissionMetaUtil.createGroupRelation(config, pgSppTeacherPreferenceTab.getName(), globalClassGroup.getName());
//            PermissionMetaUtil.createGroupRelation(config, pgSppTeacherPreferenceTab.getName(), localClassGroup.getName());
//            PermissionMetaUtil.createPermission(pgSppTeacherPreferenceTab, "employee_viewSppTeacherPreferenceGroupTab_" + code, "Просмотр");

        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}