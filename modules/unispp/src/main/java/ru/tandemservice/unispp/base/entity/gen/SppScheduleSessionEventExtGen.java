package ru.tandemservice.unispp.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionEvent;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionEventExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Экзамены имеющие нулевой вес
 *
 * Экзамены имеющие нулевой вес
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SppScheduleSessionEventExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unispp.base.entity.SppScheduleSessionEventExt";
    public static final String ENTITY_NAME = "sppScheduleSessionEventExt";
    public static final int VERSION_HASH = -1421347660;
    private static IEntityMeta ENTITY_META;

    public static final String L_SPP_SCHEDULE_SESSION_EVENT = "sppScheduleSessionEvent";
    public static final String L_EPP_REGISTRY_DISCIPLINE = "eppRegistryDiscipline";
    public static final String L_EPP_F_CONTROL_ACTION_TYPE = "eppFControlActionType";

    private SppScheduleSessionEvent _sppScheduleSessionEvent;     // Событие сессии
    private EppRegistryDiscipline _eppRegistryDiscipline;     // Дисциплина (из реестра)
    private EppFControlActionType _eppFControlActionType;     // Форма итогового контроля

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Событие сессии. Свойство не может быть null.
     */
    @NotNull
    public SppScheduleSessionEvent getSppScheduleSessionEvent()
    {
        return _sppScheduleSessionEvent;
    }

    /**
     * @param sppScheduleSessionEvent Событие сессии. Свойство не может быть null.
     */
    public void setSppScheduleSessionEvent(SppScheduleSessionEvent sppScheduleSessionEvent)
    {
        dirty(_sppScheduleSessionEvent, sppScheduleSessionEvent);
        _sppScheduleSessionEvent = sppScheduleSessionEvent;
    }

    /**
     * @return Дисциплина (из реестра). Свойство не может быть null.
     */
    @NotNull
    public EppRegistryDiscipline getEppRegistryDiscipline()
    {
        return _eppRegistryDiscipline;
    }

    /**
     * @param eppRegistryDiscipline Дисциплина (из реестра). Свойство не может быть null.
     */
    public void setEppRegistryDiscipline(EppRegistryDiscipline eppRegistryDiscipline)
    {
        dirty(_eppRegistryDiscipline, eppRegistryDiscipline);
        _eppRegistryDiscipline = eppRegistryDiscipline;
    }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     */
    @NotNull
    public EppFControlActionType getEppFControlActionType()
    {
        return _eppFControlActionType;
    }

    /**
     * @param eppFControlActionType Форма итогового контроля. Свойство не может быть null.
     */
    public void setEppFControlActionType(EppFControlActionType eppFControlActionType)
    {
        dirty(_eppFControlActionType, eppFControlActionType);
        _eppFControlActionType = eppFControlActionType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SppScheduleSessionEventExtGen)
        {
            setSppScheduleSessionEvent(((SppScheduleSessionEventExt)another).getSppScheduleSessionEvent());
            setEppRegistryDiscipline(((SppScheduleSessionEventExt)another).getEppRegistryDiscipline());
            setEppFControlActionType(((SppScheduleSessionEventExt)another).getEppFControlActionType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SppScheduleSessionEventExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SppScheduleSessionEventExt.class;
        }

        public T newInstance()
        {
            return (T) new SppScheduleSessionEventExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sppScheduleSessionEvent":
                    return obj.getSppScheduleSessionEvent();
                case "eppRegistryDiscipline":
                    return obj.getEppRegistryDiscipline();
                case "eppFControlActionType":
                    return obj.getEppFControlActionType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sppScheduleSessionEvent":
                    obj.setSppScheduleSessionEvent((SppScheduleSessionEvent) value);
                    return;
                case "eppRegistryDiscipline":
                    obj.setEppRegistryDiscipline((EppRegistryDiscipline) value);
                    return;
                case "eppFControlActionType":
                    obj.setEppFControlActionType((EppFControlActionType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sppScheduleSessionEvent":
                        return true;
                case "eppRegistryDiscipline":
                        return true;
                case "eppFControlActionType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sppScheduleSessionEvent":
                    return true;
                case "eppRegistryDiscipline":
                    return true;
                case "eppFControlActionType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sppScheduleSessionEvent":
                    return SppScheduleSessionEvent.class;
                case "eppRegistryDiscipline":
                    return EppRegistryDiscipline.class;
                case "eppFControlActionType":
                    return EppFControlActionType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SppScheduleSessionEventExt> _dslPath = new Path<SppScheduleSessionEventExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SppScheduleSessionEventExt");
    }
            

    /**
     * @return Событие сессии. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionEventExt#getSppScheduleSessionEvent()
     */
    public static SppScheduleSessionEvent.Path<SppScheduleSessionEvent> sppScheduleSessionEvent()
    {
        return _dslPath.sppScheduleSessionEvent();
    }

    /**
     * @return Дисциплина (из реестра). Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionEventExt#getEppRegistryDiscipline()
     */
    public static EppRegistryDiscipline.Path<EppRegistryDiscipline> eppRegistryDiscipline()
    {
        return _dslPath.eppRegistryDiscipline();
    }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionEventExt#getEppFControlActionType()
     */
    public static EppFControlActionType.Path<EppFControlActionType> eppFControlActionType()
    {
        return _dslPath.eppFControlActionType();
    }

    public static class Path<E extends SppScheduleSessionEventExt> extends EntityPath<E>
    {
        private SppScheduleSessionEvent.Path<SppScheduleSessionEvent> _sppScheduleSessionEvent;
        private EppRegistryDiscipline.Path<EppRegistryDiscipline> _eppRegistryDiscipline;
        private EppFControlActionType.Path<EppFControlActionType> _eppFControlActionType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Событие сессии. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionEventExt#getSppScheduleSessionEvent()
     */
        public SppScheduleSessionEvent.Path<SppScheduleSessionEvent> sppScheduleSessionEvent()
        {
            if(_sppScheduleSessionEvent == null )
                _sppScheduleSessionEvent = new SppScheduleSessionEvent.Path<SppScheduleSessionEvent>(L_SPP_SCHEDULE_SESSION_EVENT, this);
            return _sppScheduleSessionEvent;
        }

    /**
     * @return Дисциплина (из реестра). Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionEventExt#getEppRegistryDiscipline()
     */
        public EppRegistryDiscipline.Path<EppRegistryDiscipline> eppRegistryDiscipline()
        {
            if(_eppRegistryDiscipline == null )
                _eppRegistryDiscipline = new EppRegistryDiscipline.Path<EppRegistryDiscipline>(L_EPP_REGISTRY_DISCIPLINE, this);
            return _eppRegistryDiscipline;
        }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     * @see ru.tandemservice.unispp.base.entity.SppScheduleSessionEventExt#getEppFControlActionType()
     */
        public EppFControlActionType.Path<EppFControlActionType> eppFControlActionType()
        {
            if(_eppFControlActionType == null )
                _eppFControlActionType = new EppFControlActionType.Path<EppFControlActionType>(L_EPP_F_CONTROL_ACTION_TYPE, this);
            return _eppFControlActionType;
        }

        public Class getEntityClass()
        {
            return SppScheduleSessionEventExt.class;
        }

        public String getEntityName()
        {
            return "sppScheduleSessionEventExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
