/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.unispp.base.entity.SppSchedule;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 9/2/13
 */
public class SppScheduleDSHandler extends DefaultSearchDataSourceHandler
{
    // properties
    public static final String PROP_YEAR_PART_ID = "yearPartId";
    public static final String PROP_ORG_UNIT_ID = "orgUnitId";
    public static final String PROP_ORG_UNIT_IDS = "orgUnitIds";
    public static final String PROP_TITLE = "title";
    public static final String PROP_START_DATE_FROM = "startDateFrom";
    public static final String PROP_START_DATE_TO = "startDateTo";
    public static final String PROP_END_DATE_FROM = "endDateFrom";
    public static final String PROP_END_DATE_TO = "endDateTo";
    public static final String PROP_BELL_SCHEDULE_ID = "bellScheduleId";
    public static final String PROP_GROUP_ID = "groupId";
    public static final String EMPTY_LIST = "emptyList";

    public SppScheduleDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        // в некоторых случаях нужно выводить пустой список (например, если не указаны обязательные в некоторых компонентах фильтры)
        if (context.getBoolean(EMPTY_LIST, Boolean.FALSE))
            return ListOutputBuilder.get(input, Lists.newArrayList()).pageable(true).build();

        final Long yearPartId = context.get(PROP_YEAR_PART_ID);

        final Long orgUnitId = context.get(PROP_ORG_UNIT_ID);
        final List<Long> orgUnitIds = orgUnitId == null ? context.get(PROP_ORG_UNIT_IDS) :
                Collections.singletonList(orgUnitId);

        final Long groupId = context.get(PROP_GROUP_ID);
        final List<Long> groups = groupId == null ? context.get("groups") :
                Collections.singletonList(groupId);

        final String title = context.get(PROP_TITLE);
        final Date startDateFrom = context.get(PROP_START_DATE_FROM);
        final Date startDateTo = context.get(PROP_START_DATE_TO);
        final Date endDateFrom = context.get(PROP_END_DATE_FROM);
        final Date endDateTo = context.get(PROP_END_DATE_TO);
        final Long bellScheduleId = context.get(PROP_BELL_SCHEDULE_ID);
        final List<Long> courses = context.get("courses");
        final List<Long> eduLevels = context.get("eduLevels");
        final List<Long> seasons = context.get("seasons");
        final List<Long> subjects = context.get("subjects");

        // создаём билдер, который пойдёт на выход
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SppSchedule.class, "s")
                .column("s")
                .fetchPath(DQLJoinType.inner, SppSchedule.season().fromAlias("s"), "season")
                .fetchPath(DQLJoinType.inner, SppSchedule.group().fromAlias("s"), "scheduleGroup");

        // основной фильтр по подразделениям
        if (null != orgUnitIds && !orgUnitIds.isEmpty())
        {
            DQLSelectBuilder kindRelationsBuilder= new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                    .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                    .where(in(property("rel", OrgUnitToKindRelation.orgUnit().id()), orgUnitIds));
            List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

            Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                    .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

            IDQLExpression condition = nothing(); // false
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                condition = or(condition, in(property("s", SppSchedule.group().educationOrgUnit().educationLevelHighSchool().orgUnit().id()), orgUnitIds));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                condition = or(condition, in(property("s", SppSchedule.group().educationOrgUnit().formativeOrgUnit().id()), orgUnitIds));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                condition = or(condition, in(property("s", SppSchedule.group().educationOrgUnit().territorialOrgUnit().id()), orgUnitIds));

            builder.where(condition);
        }

        // прочие фильтры
        if (null != subjects && !subjects.isEmpty())
        {
            DQLSelectBuilder elementsBuilder= new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "p")
                    .column(property("p", SppSchedulePeriod.weekRow().schedule().id()))
                    .where(in(property("p", SppSchedulePeriod.subjectVal().id()), subjects))
                    .where(in(property("p", SppSchedulePeriod.weekRow().schedule().archived()), value(false)));
            List<Long> ids = createStatement(elementsBuilder).list();
            builder.where(in(property("s", SppSchedule.id()), ids));
        }

        builder.where(betweenDays(SppScheduleSeason.startDate().fromAlias("season"), startDateFrom, startDateTo));
        builder.where(betweenDays(SppScheduleSeason.endDate().fromAlias("season"), endDateFrom, endDateTo));

        if (null != yearPartId)
            builder.where(eq(property("s", SppSchedule.season().eppYearPart().id()), value(yearPartId)));
        if (null != bellScheduleId)
            builder.where(eq(property("s", SppSchedule.bells().id()), value(bellScheduleId)));
        if (null != groups && !groups.isEmpty())
            builder.where(in(property("s", SppSchedule.group().id()), groups));
        if (null != courses && !courses.isEmpty())
            builder.where(in(property("s", SppSchedule.group().course().id()), courses));
        if (null != eduLevels && !eduLevels.isEmpty())
            builder.where(in(property("s", SppSchedule.group().educationOrgUnit().educationLevelHighSchool().id()), eduLevels));
        if (null != seasons && !seasons.isEmpty())
            builder.where(in(property("s", SppSchedule.season().id()), seasons));

        // фильтр по введённому названию
        if (!StringUtils.isEmpty(title))
            builder.where(like(DQLFunctions.upper(property("s", SppSchedule.title())), value(CoreStringUtils.escapeLike(title, true))));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().pageable(true).build();
    }
}
