package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unispp_2x7x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sppRegElementLoadCheck


		// удалено свойство uniplacesPlacePurpose
        if(tool.tableExists("spp_reg_element_load_check") && tool.columnExists("spp_reg_element_load_check", "uniplacesplacepurpose_id"))
		{

			// удалить колонку
			tool.dropColumn("spp_reg_element_load_check", "uniplacesplacepurpose_id");

		}

		// создано свойство uniplacesClassroomType
        if(tool.tableExists("spp_reg_element_load_check") && !tool.columnExists("spp_reg_element_load_check", "uniplacesclassroomtype_id"))
		{
			// создать колонку
			tool.createColumn("spp_reg_element_load_check", new DBColumn("uniplacesclassroomtype_id", DBType.LONG));

		}
    }
}