/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.SessionAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.AddEdit.SppTeacherPreferenceAddEdit;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.SessionView.SppTeacherPreferenceSessionView;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList;

/**
 * @author vnekrasov
 * @since 7/3/14
 */
@Input({
        @Bind(key = "teacherSessionPreferenceId", binding = "teacherSessionPreferenceId"),
        @Bind(key = "employeePostId", binding = "employeePostId")
})
public class SppTeacherPreferenceSessionAddEditUI extends UIPresenter
{
    private Long _employeePostId;
    private Long _teacherSessionPreferenceId;
    private SppTeacherSessionPreferenceList _teacherSessionPreference;
    private ScheduleBell _oldBells;

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        _employeePostId = employeePostId;
    }

    public Long getTeacherSessionPreferenceId()
    {
        return _teacherSessionPreferenceId;
    }

    public void setTeacherSessionPreferenceId(Long teacherSessionPreferenceId)
    {
        _teacherSessionPreferenceId = teacherSessionPreferenceId;
    }

    public SppTeacherSessionPreferenceList getTeacherSessionPreference()
    {
        return _teacherSessionPreference;
    }

    public void setTeacherSessionPreference(SppTeacherSessionPreferenceList teacherSessionPreference)
    {
        _teacherSessionPreference = teacherSessionPreference;
    }

    public Boolean getAddForm()
    {
        return null == _teacherSessionPreferenceId;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _teacherSessionPreference)
        {
            if (null != _teacherSessionPreferenceId)
            {
                _teacherSessionPreference = DataAccessServices.dao().get(_teacherSessionPreferenceId);
                _oldBells = _teacherSessionPreference.getBells();
            }
            else
            {
                _teacherSessionPreference = new SppTeacherSessionPreferenceList();
                if (_employeePostId != null)
                {
                    Person person = UniDaoFacade.getCoreDao().getProperty(EmployeePost.class, EmployeePost.person().s(), EmployeePost.P_ID, _employeePostId);
                    _teacherSessionPreference.setTeacher(person);
                }
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppTeacherPreferenceAddEdit.SEASON_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppTeacherPreferenceManager.EMPLOYEE, _teacherSessionPreference.getTeacher());
            dataSource.put(SppTeacherPreferenceManager.EDITED, _teacherSessionPreferenceId);
        }
    }

    public void onClickApply()
    {
        SppTeacherPreferenceManager.instance().dao().saveOrUpdateTeacherSessionPreferenceList(_teacherSessionPreference, _oldBells);
        if(getAddForm())
        {
            _uiConfig.deactivateComponent();
            _uiActivation.asDesktopRoot(SppTeacherPreferenceSessionView.class).parameter(PUBLISHER_ID, _teacherSessionPreference.getId()).activate();
        }
        else _uiConfig.deactivateComponent();
    }
}
