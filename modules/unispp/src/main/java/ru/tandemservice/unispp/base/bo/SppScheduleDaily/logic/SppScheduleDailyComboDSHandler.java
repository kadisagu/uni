/* $Id: SppScheduleDailyComboDSHandler.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
public class SppScheduleDailyComboDSHandler extends SimpleTitledComboDataSourceHandler
{
    public SppScheduleDailyComboDSHandler(String ownerId)
    {
        super(ownerId);
        _filtered = true;
    }

    public enum Columns
    {
        SCHEDULE(SppScheduleDaily.class, null, SppScheduleDaily.id(), SppScheduleDaily.title(), SppScheduleDaily.title()),
        SEASON(SppScheduleDailySeason.class, SppScheduleDaily.season(), SppScheduleDaily.season().id(), SppScheduleDaily.season().title(), SppScheduleDailySeason.title()),
        BELLS(ScheduleBell.class, SppScheduleDaily.bells(), SppScheduleDaily.bells().id(), SppScheduleDaily.bells().title(), ScheduleBell.title());

        Columns(Class entityClass, MetaDSLPath column, MetaDSLPath idColumn, MetaDSLPath orderPath, MetaDSLPath filterPath)
        {
            _entityClass = entityClass;
            _column = column;
            _idColumn = idColumn;
            _orderPath = orderPath;
            _filterPath = filterPath;
        }

        private Class _entityClass;
        private MetaDSLPath _column;
        private MetaDSLPath _idColumn;
        private MetaDSLPath _orderPath;
        private MetaDSLPath _filterPath;

        public MetaDSLPath getColumn()
        {
            return _column;
        }

        public MetaDSLPath getOrderPath()
        {
            return _orderPath;
        }

        public void setOrderPath(MetaDSLPath orderPath)
        {
            _orderPath = orderPath;
        }

        public void setColumn(MetaDSLPath column)
        {
            _column = column;
        }

        public MetaDSLPath getFilterPath()
        {
            return _filterPath;
        }

        public void setFilterPath(MetaDSLPath filterPath)
        {
            _filterPath = filterPath;
        }

        public Class getEntityClass()
        {
            return _entityClass;
        }

        public void setEntityClass(Class entityClass)
        {
            _entityClass = entityClass;
        }

        public MetaDSLPath getIdColumn()
        {
            return _idColumn;
        }

        public void setIdColumn(MetaDSLPath idColumn)
        {
            _idColumn = idColumn;
        }
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Columns column = context.get("column");
        SppScheduleDailyPrintParams scheduleData = context.get("scheduleData");

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(SppScheduleDaily.class, "s");
        subBuilder.where(eq(property("s", SppScheduleDaily.approved()), value(true)));
        subBuilder.where(eq(property("s", SppScheduleDaily.archived()), value(false)));

        if (null != scheduleData.getCurrentOrgUnit())
        {
            Long orgUnitId = scheduleData.getCurrentOrgUnit().getId();
            DQLSelectBuilder kindRelationsBuilder = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                    .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                    .where(eq(property("rel", OrgUnitToKindRelation.orgUnit().id()), value(orgUnitId)));
            List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

            Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                    .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

            IDQLExpression condition = nothing(); // false
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                condition = or(condition, eq(property("s", SppScheduleDaily.group().educationOrgUnit().educationLevelHighSchool().orgUnit().id()), value(orgUnitId)));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                condition = or(condition, eq(property("s", SppScheduleDaily.group().educationOrgUnit().formativeOrgUnit().id()), value(orgUnitId)));
            if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                condition = or(condition, eq(property("s", SppScheduleDaily.group().educationOrgUnit().territorialOrgUnit().id()), value(orgUnitId)));

            subBuilder.where(condition);
        }
        subBuilder.distinct();
        subBuilder.column(property("s", column.getIdColumn()));

        if (!Columns.SEASON.equals(column))
        {
            if (null == scheduleData.getSeason())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            if (null != scheduleData.getSeason())
                subBuilder.where(eq(property("s", SppScheduleDaily.season()), value(scheduleData.getSeason())));
        }
        if (!Columns.SEASON.equals(column) && !Columns.BELLS.equals(column))
        {
            if (null == scheduleData.getSeason() && null == scheduleData.getBells())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            if (null != scheduleData.getBells())
                subBuilder.where(eq(property("s", SppScheduleDaily.bells()), value(scheduleData.getBells())));
        }

        subBuilder.where(eq(property("s", SppScheduleDaily.group().educationOrgUnit().formativeOrgUnit()), value(scheduleData.getFormativeOrgUnit())));
        subBuilder.where(eq(property("s", SppScheduleDaily.group().educationOrgUnit().developForm()), value(scheduleData.getDevelopForm())));
//        subBuilder.where(eq(property("s", SppScheduleDaily.group().course()), value(scheduleData.getCourse())));

        addAdditionalFilters(subBuilder, scheduleData);

        if (null != scheduleData.getEduLevels() && !scheduleData.getEduLevels().isEmpty())
        {
            subBuilder.where(in(property("s", SppScheduleDaily.group().educationOrgUnit().educationLevelHighSchool()), scheduleData.getEduLevels()));
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(column.getEntityClass(), "e");
        builder.where(in(property("e.id"), subBuilder.buildQuery()));

        setOrderByProperty(column.getOrderPath().s());
        setFilterByProperty(column.getFilterPath().s());
        context.put(UIDefines.COMBO_OBJECT_LIST, createStatement(builder).list());
        return super.execute(input, context);
    }

    protected void addAdditionalFilters(DQLSelectBuilder builder, SppScheduleDailyPrintParams scheduleData)
    {
    }
}
