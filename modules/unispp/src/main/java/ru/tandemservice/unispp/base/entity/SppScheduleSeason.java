package ru.tandemservice.unispp.base.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unispp.base.entity.gen.*;

/**
 * Период расписания
 */
public class SppScheduleSeason extends SppScheduleSeasonGen
{
    @EntityDSLSupport
    @Override
    public String getTitleWithTime()
    {
        return getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(getStartDate()) + "-" + DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndDate()) + ")";
    }
}