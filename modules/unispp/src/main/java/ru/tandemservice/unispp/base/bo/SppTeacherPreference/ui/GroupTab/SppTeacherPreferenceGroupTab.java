/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.GroupTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.DailyView.SppTeacherPreferenceDailyView;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.SessionView.SppTeacherPreferenceSessionView;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.View.SppTeacherPreferenceView;
import ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList;

/**
 * @author vnekrasov
 * @since 6/23/14
 */
@Configuration
public class SppTeacherPreferenceGroupTab extends BusinessComponentManager
{
    public final static String TEACHER_PREFERENCE_DS = "teacherPreferenceDS";
    public final static String TEACHER_PREFERENCE_SESSION_DS = "teacherPreferenceSessionDS";
    public final static String TEACHER_PREFERENCE_DAILY_DS = "teacherPreferenceDailyDS";

    @Bean
    public ColumnListExtPoint preferenceSessionCL()
    {

        return columnListExtPointBuilder(TEACHER_PREFERENCE_SESSION_DS)
                .addColumn(publisherColumn("title", SppTeacherSessionPreferenceList.title()).businessComponent(SppTeacherPreferenceSessionView.class).order().required(true))
                .addColumn(dateColumn("startDate", SppTeacherSessionPreferenceList.sppScheduleSessionSeason().startDate()).order())
                .addColumn(dateColumn("endDate", SppTeacherSessionPreferenceList.sppScheduleSessionSeason().endDate()).order())
                .addColumn(textColumn("bells", SppTeacherSessionPreferenceList.bells().title()).order())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("ui:editPermissionKey"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert(new FormattedMessage("ui.deleteAlert", SppTeacherSessionPreferenceList.title())).permissionKey("ui:deletePermissionKey"))
                .create();
    }

    @Bean
    public ColumnListExtPoint preferenceCL()
    {

        return columnListExtPointBuilder(TEACHER_PREFERENCE_DS)
                .addColumn(publisherColumn("title", SppTeacherPreferenceList.title()).businessComponent(SppTeacherPreferenceView.class).order().required(true))
                .addColumn(dateColumn("startDate", SppTeacherPreferenceList.sppScheduleSeason().startDate()).order())
                .addColumn(dateColumn("endDate", SppTeacherPreferenceList.sppScheduleSeason().endDate()).order())
                .addColumn(textColumn("bells", SppTeacherPreferenceList.bells().title()).order())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("ui:editPermissionKey"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert(new FormattedMessage("ui.deleteAlert", SppTeacherPreferenceList.title())).permissionKey("ui:deletePermissionKey"))
                .create();
    }

    @Bean
    public ColumnListExtPoint preferenceDailyCL()
    {

        return columnListExtPointBuilder(TEACHER_PREFERENCE_DAILY_DS)
                .addColumn(publisherColumn("title", SppTeacherDailyPreferenceList.title()).businessComponent(SppTeacherPreferenceDailyView.class).order().required(true))
                .addColumn(dateColumn("startDate", SppTeacherDailyPreferenceList.sppScheduleDailySeason().startDate()).order())
                .addColumn(dateColumn("endDate", SppTeacherDailyPreferenceList.sppScheduleDailySeason().endDate()).order())
                .addColumn(textColumn("bells", SppTeacherDailyPreferenceList.bells().title()).order())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("ui:editPermissionKey"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert(new FormattedMessage("ui.deleteAlert", SppTeacherDailyPreferenceList.title())).permissionKey("ui:deletePermissionKey"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(TEACHER_PREFERENCE_DS, preferenceCL(), SppTeacherPreferenceManager.instance().sppTeacherPreferenceDSHandler()))
                .addDataSource(searchListDS(TEACHER_PREFERENCE_SESSION_DS, preferenceSessionCL(), SppTeacherPreferenceManager.instance().sppTeacherSessionPreferenceDSHandler()))
                .addDataSource(searchListDS(TEACHER_PREFERENCE_DAILY_DS, preferenceDailyCL(), SppTeacherPreferenceManager.instance().sppTeacherDailyPreferenceDSHandler()))
                .create();
    }
}
