package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unispp_2x11x1_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sppRegElementExt

		// создано обязательное свойство checkDuplicates
		{
			// создать колонку
			tool.createColumn("spp_reg_element_ext", new DBColumn("checkduplicates_p", DBType.BOOLEAN));

			tool.executeUpdate("update spp_reg_element_ext set checkduplicates_p=? where checkduplicates_p is null", true);

			// сделать колонку NOT NULL
			tool.setColumnNullable("spp_reg_element_ext", "checkduplicates_p", false);
		}
    }
}
