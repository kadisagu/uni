/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.unispp.base.entity.SppRegElementExt;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;
import ru.tandemservice.unispp.base.vo.SppScheduleDailyDisciplineVO;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 10/1/13
 */
public class SppScheduleDailySubjectComboDSHandler extends SimpleTitledComboDataSourceHandler
{
    // properties
    public static final String PROP_GROUP_ID = "groupId";
    public static final String PROP_SEASON_ID = "seasonId";
    public static final String PROP_CHECK_DUPLICATES = "checkDuplicates";
    public static final String PROP_SCHEDULE_ID = "scheduleId";
    public static final String PROP_CURRENT_EVENT_ID = "currentEventId";
    public static final String PROP_DATE = "date";

    public SppScheduleDailySubjectComboDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String filter = input.getComboFilterByValue();
        final Long groupId = context.getNotNull(PROP_GROUP_ID);
        final Long seasonId = context.get(PROP_SEASON_ID);

        final boolean checkDuplicates = context.getBoolean(PROP_CHECK_DUPLICATES, false);
        final Long scheduleId = context.get(PROP_SCHEDULE_ID);
        final Long currentEventId = context.get(PROP_CURRENT_EVENT_ID);
        final Date date = context.get(PROP_DATE);

        if (seasonId == null || groupId == null) return super.execute(input, context);

        // возьмём нужную часть учебного года
        DQLSelectBuilder yearPartBuilder = new DQLSelectBuilder()
                .fromEntity(SppScheduleDailySeason.class, "s").column("yp")
                .joinPath(DQLJoinType.inner, SppScheduleDailySeason.eppYearPart().fromAlias("s"), "yp")
                .fetchPath(DQLJoinType.inner, EppYearPart.year().educationYear().fromAlias("yp"), "year")
                .fetchPath(DQLJoinType.inner, EppYearPart.part().fromAlias("yp"), "part")
                .where(eq(property("s.id"), value(seasonId)));
        List<EppYearPart> yearPartList = createStatement(yearPartBuilder).list();
        if (yearPartList.isEmpty()) return super.execute(input, context);
        EppYearPart yearPart = yearPartList.get(0);
        Long yearId = yearPart.getYear().getEducationYear().getId();
        Long partId = yearPart.getPart().getId();

        List<SppScheduleDailyDisciplineVO> disciplineVOs = Lists.newArrayList();
        // для формы контроля и для ауд. нагрузок
        for (boolean fControl : new boolean[]{true, false})
        {
            // возьмём РУПы студентов этой группы
            DQLSelectBuilder wpBuilder = new DQLSelectBuilder();
            wpBuilder.fromEntity(EppStudent2WorkPlan.class, "st2wp");
            wpBuilder.column(property("wpb.id"), "wpbId");
            wpBuilder.joinPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("st2wp"), "st2epv");
            wpBuilder.joinPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias("st2wp"), "wpb");
            wpBuilder.where(in(property("st2epv", EppStudent2EduPlanVersion.student().id()),
                    new DQLSelectBuilder().fromEntity(Student.class, "st").column("st.id")
                            .where(eq(property("st", Student.group().id()), value(groupId)))
                            .where(eq(property("st", Student.archival()), value(Boolean.FALSE)))
                            .buildQuery()));
            // только по соответствующей части уч. года
            wpBuilder.where(eq(property("st2wp", EppStudent2WorkPlan.cachedEppYear().educationYear().id()), value(yearId)));
            wpBuilder.where(eq(property("st2wp", EppStudent2WorkPlan.cachedGridTerm().part().id()), value(partId)));
            // только актуальные
            wpBuilder.where(isNull(property("st2wp", EppStudent2WorkPlan.removalDate())));
            wpBuilder.where(isNull(property("st2epv", EppStudent2EduPlanVersion.removalDate())));
            wpBuilder.distinct();

            // возьмём все дисциплины этих РУПов
            DQLSelectBuilder builder = new DQLSelectBuilder();
            builder.fromDataSource(wpBuilder.buildQuery(), "wpSelect");
            builder.column("d");
            builder.joinEntity("wpSelect", DQLJoinType.inner, EppWorkPlanRegistryElementRow.class, "r", eq(
                    property("r", EppWorkPlanRegistryElementRow.workPlan().id()), property("wpSelect.wpbId")));
            builder.joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().fromAlias("r"), "el");
            builder.joinEntity("el", DQLJoinType.inner, EppRegistryDiscipline.class, "d", eq(property("el.id"), property("d.id")));

            if (!StringUtils.isEmpty(filter))
                builder.where(like(DQLFunctions.upper(property("d", EppRegistryDiscipline.title())), value(CoreStringUtils.escapeLike(filter, true))));

            if (fControl)
            {
                // формы итогового контроля
                builder.column("fc");
                builder.joinEntity("d", DQLJoinType.inner, EppRegistryElementPart.class, "elp",
                        eq(property("d.id"), property("elp", EppRegistryElementPart.registryElement().id())));
                builder.joinEntity("elp", DQLJoinType.inner, EppRegistryElementPartFControlAction.class, "fa",
                        eq(property("elp.id"), property("fa", EppRegistryElementPartFControlAction.part().id())));
                builder.joinPath(DQLJoinType.inner, EppRegistryElementPartFControlAction.controlAction().fromAlias("fa"), "fc");
                builder.distinct();

                // фильтруем дубли (такие же занятие в текущем дне)
                if (checkDuplicates)
                {
                    builder.joinEntity("d", DQLJoinType.left, SppRegElementExt.class, "elExt", eq(property("d.id"), property("elExt", SppRegElementExt.registryElement().id())));
                    // либо соотв. флаг на проверку в предмете сброшен, либо предмет не был проставлен в этот день
                    builder.where(or(
                            and(isNotNull(property("elExt.id")), eq(property("elExt", SppRegElementExt.checkDuplicates()), value(false))),
                            notExists(new DQLSelectBuilder()
                                    .fromEntity(SppScheduleDailyEvent.class, "per")
                                    .where(eq(property("per", SppScheduleDailyEvent.schedule().id()), value(scheduleId)))
                                    .where(ne(property("per", SppScheduleDailyEvent.id()), value(currentEventId)))
                                    .where(eq(property("per", SppScheduleDailyEvent.date()), valueDate(date)))
                                    .where(eq(property("per", SppScheduleDailyEvent.subjectVal().id()), property("d.id")))
                                    .where(eq(property("per", SppScheduleDailyEvent.subjectActionType().id()), property("fc.id")))
                                    .buildQuery())));
                }

                List<Object[]> list = createStatement(builder).list();
                for (Object[] row : list)
                {
                    EppRegistryDiscipline discipline = ((EppRegistryDiscipline) row[0]);
                    EppFControlActionType formControl = ((EppFControlActionType) row[1]);
                    if (discipline != null && formControl != null)
                        disciplineVOs.add(new SppScheduleDailyDisciplineVO(discipline, formControl));
                }
            }
            else
            {
                // ауд. нагрузки
                builder.column("al");
                builder.joinEntity("d", DQLJoinType.inner, EppRegistryElementLoad.class, "rel",
                        eq(property("d.id"), property("rel", EppRegistryElementLoad.registryElement().id())));
                builder.where(gt(property("rel", EppRegistryElementLoad.load()), value(0)));
                builder.joinEntity("rel", DQLJoinType.inner, EppALoadType.class, "al",
                        eq(property("rel", EppRegistryElementLoad.loadType().id()), property("al.id")));
                builder.distinct();

                // фильтруем дубли (такие же занятие в текущем дне)
                if (checkDuplicates)
                {
                    builder.joinEntity("d", DQLJoinType.left, SppRegElementExt.class, "elExt", eq(property("d.id"), property("elExt", SppRegElementExt.registryElement().id())));
                    // либо соотв. флаг на проверку в предмете сброшен, либо предмет не был проставлен в этот день
                    builder.where(or(
                            and(isNotNull(property("elExt.id")), eq(property("elExt", SppRegElementExt.checkDuplicates()), value(false))),
                            notExists(new DQLSelectBuilder()
                                    .fromEntity(SppScheduleDailyEvent.class, "per")
                                    .where(eq(property("per", SppScheduleDailyEvent.schedule().id()), value(scheduleId)))
                                    .where(ne(property("per", SppScheduleDailyEvent.id()), value(currentEventId)))
                                    .where(eq(property("per", SppScheduleDailyEvent.date()), valueDate(date)))
                                    .where(eq(property("per", SppScheduleDailyEvent.subjectVal().id()), property("d.id")))
                                    .where(eq(property("per", SppScheduleDailyEvent.subjectLoadType().id()), property("al.id")))
                                    .buildQuery())));
                }

                List<Object[]> list = createStatement(builder).list();
                for (Object[] row : list)
                {
                    EppRegistryDiscipline discipline = (EppRegistryDiscipline) row[0];
                    EppALoadType loadType = (EppALoadType) row[1];
                    if (discipline != null && loadType != null)
                        disciplineVOs.add(new SppScheduleDailyDisciplineVO(discipline, loadType));
                }
            }
        }

        // сортируем и отправляем наружу
        Collections.sort(disciplineVOs, (o1, o2) -> o1.getTitle().compareTo(o2.getTitle()));
        context.put(UIDefines.COMBO_OBJECT_LIST, disciplineVOs);

        return super.execute(input, context);
    }
}
