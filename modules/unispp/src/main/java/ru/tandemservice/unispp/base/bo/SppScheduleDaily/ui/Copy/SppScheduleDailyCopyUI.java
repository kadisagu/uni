/* $Id: */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.Copy;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleGroupComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.View.SppScheduleDailyView;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;

/**
 * @author vnekrasov
 * @since 7/7/14
 */
@Input({
        @Bind(key = "scheduleDailyId", binding = "scheduleDailyId"),
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class SppScheduleDailyCopyUI extends UIPresenter
{
    private Long _scheduleDailyId;
    private SppScheduleDaily _scheduleDaily;
    private SppScheduleDaily _newScheduleDaily;
    private Long _orgUnitId;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Long getScheduleDailyId()
    {
        return _scheduleDailyId;
    }

    public void setScheduleDailyId(Long scheduleDailyId)
    {
        _scheduleDailyId = scheduleDailyId;
    }

    public SppScheduleDaily getScheduleDaily()
    {
        return _scheduleDaily;
    }

    public void setScheduleDaily(SppScheduleDaily scheduleDaily)
    {
        _scheduleDaily = scheduleDaily;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _newScheduleDaily)
        {
            if(null != _scheduleDailyId)
            {
                _scheduleDaily = DataAccessServices.dao().get(_scheduleDailyId);
                _newScheduleDaily = new SppScheduleDaily();
                _orgUnitId = _orgUnitId != null ? _orgUnitId : _scheduleDaily.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getId();
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleDailyCopy.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put("groupId", _scheduleDaily.getGroup().getId());
            dataSource.put("orgUnitId", _orgUnitId);
            dataSource.put(SppScheduleGroupComboDSHandler.PROP_GROUPS_NOT_IN_ARCHIVE, Boolean.TRUE);
        }
    }

    public void onClickApply()
    {
        _newScheduleDaily.setApproved(_scheduleDaily.isApproved());
        _newScheduleDaily.setArchived(_scheduleDaily.isArchived());
        _newScheduleDaily.setStatus(_scheduleDaily.getStatus());
        _newScheduleDaily.setSeason(_scheduleDaily.getSeason());
        _newScheduleDaily.setIcalData(null);
        SppScheduleDailyManager.instance().dao().copySchedule(_scheduleDailyId, _newScheduleDaily);

        deactivate();
        _uiActivation.asDesktopRoot(SppScheduleDailyView.class).parameter(PUBLISHER_ID, _newScheduleDaily.getId()).activate();

    }
}
