/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference;

import com.google.common.collect.Lists;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniplaces.entity.catalog.codes.UniplacesPlacePurposeCodes;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppSchedule.logic.SppScheduleSeasonDSHandler;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.logic.*;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;
import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 2/13/14
 */
@Configuration
public class SppTeacherPreferenceManager extends BusinessObjectManager
{
    public static final String EMPLOYEE = "employee";
    public static final String EDITED = "edited_preference";
    //public static final String CAN_EDIT_BELL_SCHEDULE_ENTRIES_COND = CanEditBellScheduleEntriesPredicate.class.getSimpleName();
    public static final IdentifiableWrapper MONDAY = new IdentifiableWrapper(1L, "Понедельник");
    public static final IdentifiableWrapper TUESDAY = new IdentifiableWrapper(2L, "Вторник");
    public static final IdentifiableWrapper WEDNESDAY = new IdentifiableWrapper(3L, "Среда");
    public static final IdentifiableWrapper THURSDAY = new IdentifiableWrapper(4L, "Четверг");
    public static final IdentifiableWrapper FRIDAY = new IdentifiableWrapper(5L, "Пятница");
    public static final IdentifiableWrapper SATURDAY = new IdentifiableWrapper(6L, "Суббота");
    public static final IdentifiableWrapper SUNDAY = new IdentifiableWrapper(7L, "Воскресенье");

    public static SppTeacherPreferenceManager instance()
    {
        return instance(SppTeacherPreferenceManager.class);
    }

    @Bean
    public ISppTeacherPreferenceDAO dao()
    {
        return new SppTeacherPreferenceDAO();
    }


    @Bean
    public IDefaultSearchDataSourceHandler sppScheduleSeasonDSHandler()
    {
        return new SppScheduleSeasonDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler sppTeacherSessionPreferenceDSHandler()
    {
        return new SppTeacherSessionPreferenceDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler sppTeacherDailyPreferenceDSHandler()
    {
        return new SppTeacherDailyPreferenceDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler sppTeacherPreferenceDSHandler()
    {
        return new SppTeacherPreferenceDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler seasonComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), SppScheduleSeason.class, SppScheduleSeason.title())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                Object employee = ep.context.get(EMPLOYEE);
                Long editedPref = ep.context.get(EDITED);
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(SppTeacherPreferenceList.class, "pref")
                        .column(property("pref", SppTeacherPreferenceList.sppScheduleSeason()))
                        .where(eq(property("pref", SppTeacherPreferenceList.teacher()), commonValue(employee)));
                if (editedPref != null)
                    subBuilder.where(ne(property("pref.id"), value(editedPref)));

                ep.dqlBuilder.where(notIn(property("e.id"), subBuilder.buildQuery()));
            }
        };
    }

    @Bean
    public XStream xStream()
    {
        return new XStream(new StaxDriver());
    }

//    @Bean
//    public IDefaultComboDataSourceHandler termComboDSHandler()
//    {
//        return new SimpleTitledComboDataSourceHandler(getName()).
//        addItemList(termOptionsExtPoint());
//    }

//    //Семестры
//    public final static Long TERM_SPRING = 1L;
//    public final static Long TERM_AUTUMN = 2L;
//
//
//    @Bean
//    public ItemListExtPoint<DataWrapper> termOptionsExtPoint()
//    {
//        return itemList(DataWrapper.class).
//        add(TERM_SPRING.toString(), new DataWrapper(TERM_SPRING, "ui.term.spring")).
//        add(TERM_AUTUMN.toString(), new DataWrapper(TERM_AUTUMN, "ui.term.autumn")).
//        create();
//    }

    @Bean
    public IDefaultComboDataSourceHandler bellsComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), ScheduleBell.class, ScheduleBell.title())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", ScheduleBell.active()), DQLExpressions.value(Boolean.TRUE)));
            }

            @Override
            protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep)
            {
                ep.dqlBuilder.order(DQLExpressions.property("e", ScheduleBell.title()));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler bellEntryComboDSHandler()
    {
       return new EntityComboDataSourceHandler(getName(), ScheduleBellEntry.class)
       {
           @Override
           protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
           {
               super.applyWhereConditions(alias, dql, context);

               Long bellScheduleId = context.get("bellScheduleId");

               if (bellScheduleId != null)
                   dql.where(DQLExpressions.eq(property(alias, ScheduleBellEntry.schedule().id()), DQLExpressions.value(bellScheduleId)));
           }
       }
       .order(ScheduleBellEntry.startTime());
    }

    @Bean
    public IDefaultComboDataSourceHandler buildingComboDSHandler()
    {
       return new EntityComboDataSourceHandler(getName(), UniplacesBuilding.class)
               .order(UniplacesBuilding.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler lectureRoomComboDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UniplacesPlace.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                List<String> placePurposeCodeses = Lists.newArrayList();
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.AUDITORIYA_OBTSHAYA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.AUDITORIYA_SPETSIALIZIROVANNAYA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.LABORATORIYA_OBTSHAYA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.LABORATORIYA_SPETSIALIZIROVANNAYA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.VYCHISLITELNYY_KLASS);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.KABINET);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.SPETSIALIZIROVANNYY_KLASS);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.OBEKT_FIZKULTURY_I_SPORTA);
                placePurposeCodeses.add(UniplacesPlacePurposeCodes.TRUDOVOE_VOSPITANIE);
                dql.where(DQLExpressions.in(property(alias, UniplacesPlace.purpose().code()), placePurposeCodeses));
                dql.where(DQLExpressions.eq(property(alias, UniplacesPlace.condition().code()), DQLExpressions.value("1")));
                Long buildingId = context.get("buildingId");
                Collection<Long> buildingIds = context.get("buildingIds");
                if (buildingId != null)
                    dql.where(DQLExpressions.eq(property(alias, UniplacesPlace.floor().unit().building().id()), DQLExpressions.value(buildingId)));
                if (buildingIds != null && !buildingIds.isEmpty())
                    dql.where(DQLExpressions.in(property(alias, UniplacesPlace.floor().unit().building().id()), buildingIds));
            }
        }.order(UniplacesPlace.title()).filter(UniplacesPlace.title());

//        SppScheduleLectRoomComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler dayOfWeekComboDSHandler()
    {
       return new SimpleTitledComboDataSourceHandler(getName())
              .addAll(Arrays.asList(MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY));
    }

//    @Bean
//    public IDefaultComboDataSourceHandler schedulesComboDSHandler()
//    {
//        return new SppSchedulesComboDSHandler(getName());
//    }

    public static String getPermissionKey(String permissionKey, boolean himself)
    {
        return himself ? "" : permissionKey;
    }
}
