/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 25.10.2016
 */
public class SppScheduleCourseComboDSHandler extends DefaultComboDataSourceHandler
{
    // properties
    public static String PROP_ORG_UNIT_ID = "orgUnitId";
    public static String PROP_ORG_UNIT_IDS = "orgUnitIds";

    public SppScheduleCourseComboDSHandler(String ownerId)
    {
        super(ownerId, Course.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        Long orgUnitId = ep.context.get(PROP_ORG_UNIT_ID);
        List<Long> orgUnitIds = orgUnitId == null ? ep.context.get(PROP_ORG_UNIT_IDS) :
                Collections.singletonList(orgUnitId);

        DQLSelectBuilder kindRelationsBuilder= new DQLSelectBuilder()
                .fromEntity(OrgUnitToKindRelation.class, "rel")
                .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                .where(in(property("rel", OrgUnitToKindRelation.orgUnit().id()), orgUnitIds));
        List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

        Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

        IDQLExpression condition = nothing(); // false
        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
            condition = or(condition, in(property("g", Group.educationOrgUnit().educationLevelHighSchool().orgUnit().id()), orgUnitIds));
        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
            condition = or(condition, in(property("g", Group.educationOrgUnit().formativeOrgUnit().id()), orgUnitIds));
        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
            condition = or(condition, in(property("g", Group.educationOrgUnit().territorialOrgUnit().id()), orgUnitIds));

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g");
        subBuilder.where(condition);
        subBuilder.where(eq(property("e"), property("g", Group.course())));
        ep.dqlBuilder.where(DQLExpressions.exists(subBuilder.buildQuery()));
    }
}
