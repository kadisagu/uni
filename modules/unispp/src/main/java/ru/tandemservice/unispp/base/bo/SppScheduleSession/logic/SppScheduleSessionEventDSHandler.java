/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionEvent;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author vnekrasov
 * @since 12/27/13
 */
public class SppScheduleSessionEventDSHandler extends DefaultSearchDataSourceHandler
{
    public SppScheduleSessionEventDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long scheduleId = context.getNotNull("scheduleId");
        OrderDirection orderDirection = input.getEntityOrder().getDirection();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "e");
        builder.where(eq(property("e", SppScheduleSessionEvent.schedule().id()), value(scheduleId)));
        builder.order(property("e", SppScheduleSessionEvent.date()), orderDirection);
        builder.order(property("e", SppScheduleSessionEvent.bell().number()), orderDirection);

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }
}
