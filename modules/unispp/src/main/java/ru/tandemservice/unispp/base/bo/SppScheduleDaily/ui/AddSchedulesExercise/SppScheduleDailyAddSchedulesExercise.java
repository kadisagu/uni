/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.AddSchedulesExercise;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;

/**
 * @author Igor Belanov
 * @since 25.10.2016
 */
@Configuration
public class SppScheduleDailyAddSchedulesExercise extends BusinessComponentManager
{
    // data sources
    public static final String SEASON_DS = "seasonDS";
    public static final String BELLS_DS = "bellsDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(SEASON_DS, SppScheduleDailyManager.instance().seasonComboDSHandler()).addColumn("title", null, new IFormatter<SppScheduleDailySeason>()
                {
                    @Override
                    public String format(SppScheduleDailySeason source)
                    {
                        return source.getTitle() + " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getStartDate()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(source.getEndDate()) + ")";
                    }
                }))
                .addDataSource(selectDS(BELLS_DS, SppScheduleDailyManager.instance().bellsComboDSHandler()))
                .create();
    }
}
