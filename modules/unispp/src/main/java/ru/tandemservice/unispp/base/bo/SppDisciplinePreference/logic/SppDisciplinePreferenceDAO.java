/* $Id: */
package ru.tandemservice.unispp.base.bo.SppDisciplinePreference.logic;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unispp.base.entity.SppDisciplinePreferenceList;

/**
 * @author vnekrasov
 * @since 7/10/14
 */
@Transactional
public class SppDisciplinePreferenceDAO extends UniBaseDao implements ISppDisciplinePreferenceDAO
{

    @Override
    public void saveOrUpdateDisciplinePreferenceList(SppDisciplinePreferenceList preference)
    {
        saveOrUpdate(preference);
    }

    @Override
    public void deleteDisciplinePreferenceList(Long preferenceId)
    {
      delete(preferenceId);
    }

}
