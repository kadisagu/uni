/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.SessionView;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.SessionAddEdit.SppTeacherPreferenceSessionAddEdit;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.SessionElementAdd.SppTeacherPreferenceSessionElementAdd;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.SessionElementEdit.SppTeacherPreferenceSessionElementEdit;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author vnekrasov
 * @since 7/2/14
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "teacherSessionPreferenceId", required = true)
})
public class SppTeacherPreferenceSessionViewUI extends UIPresenter
{
    private Long _teacherSessionPreferenceId;
    private SppTeacherSessionPreferenceList _teacherSessionPreference;

    private boolean _himself;

    public Long getTeacherSessionPreferenceId()
    {
        return _teacherSessionPreferenceId;
    }

    public void setTeacherSessionPreferenceId(Long teacherSessionPreferenceId)
    {
        _teacherSessionPreferenceId = teacherSessionPreferenceId;
    }


    //    visible="ui:editScheduleVisible"
    public Boolean getEditTeacherSessionPreferenceVisible()
    {
        return true;
    }

    public Boolean getAddElementVisible()
    {
        return true;
    }

    //    ui:eventDeleteDisabled
    public Boolean getElementDeleteDisabled()
    {
        return false;
    }

    //    ui:eventEditDisabled
    public Boolean getElementEditDisabled()
    {
        return false;
    }

//    public Map getGroupParameterMap()
//    {
//        return new ParametersMap()
//                .add(UIPresenter.PUBLISHER_ID, _teacherSessionPreference.getGroup().getId())
//                .add("selectedTabId", "sppScheduleSessionTab");
//    }

    @Override
    public void onComponentRefresh()
    {
//        if(null == _schedule)
            _teacherSessionPreference = DataAccessServices.dao().get(_teacherSessionPreferenceId);

        IPrincipalContext principalContext = getConfig().getUserContext().getPrincipalContext();
        List<IPrincipalContext> personPrincipalContextList = _teacherSessionPreference.getTeacher().getPrincipalContextList();

        _himself = false;
        if (personPrincipalContextList != null)
        {
            _himself = personPrincipalContextList.stream().map(IPrincipalContext::getPrincipal)
                    .collect(Collectors.toSet()).contains(principalContext.getPrincipal());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppTeacherPreferenceSessionView.SESSION_PREFERENCE_ELEMENTS_DS.equals(dataSource.getName()))
        {
            dataSource.put("teacherSessionPreferenceId", _teacherSessionPreference.getId());
        }
    }

    public void onClickEditTeacherSessionPreference()
    {
        _uiActivation.asRegion(SppTeacherPreferenceSessionAddEdit.class).parameter("teacherSessionPreferenceId", _teacherSessionPreferenceId).activate();
    }

    public void onClickAddElement()
    {
        _uiActivation.asRegion(SppTeacherPreferenceSessionElementAdd.class).parameter("teacherSessionPreferenceId", _teacherSessionPreferenceId).activate();
    }

    public void onClickEditElement()
    {
        _uiActivation.asRegion(SppTeacherPreferenceSessionElementEdit.class)
                .parameter("teacherSessionPreferenceId", _teacherSessionPreferenceId)
                .parameter("elementId", getListenerParameterAsLong()).activate();
    }

    public void onClickDeleteElement()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        //updateTeacherSessionPreferenceAsChanged();
    }


    public String getViewPermissionKey()
        {
        return SppTeacherPreferenceManager.getPermissionKey("sppTeacherSessionPreferenceViewView", _himself);
    }

    public String getAddPermissionKey()
    {
        return SppTeacherPreferenceManager.getPermissionKey("sppTeacherSessionPreferenceViewAdd", _himself);
    }

    public String getEditPermissionKey()
    {
        return SppTeacherPreferenceManager.getPermissionKey("sppTeacherSessionPreferenceViewEdit", _himself);
    }
}
