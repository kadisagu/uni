/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.OrgUnitList;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.SppScheduleSessionDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.AddEdit.SppScheduleSessionAddEdit;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.AddSchedulesSelectGroupsList.SppScheduleSessionAddSchedulesSelectGroupsList;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.AddSchedulesSelectGroupsList.SppScheduleSessionAddSchedulesSelectGroupsListUI;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;

import java.util.Map;

/**
 * @author vnekrasov
 * @since 12/26/13
 */
public class SppScheduleSessionOrgUnitListUI extends OrgUnitUIPresenter
{
    private BaseSearchListDataSource _scheduleDS;

    public BaseSearchListDataSource getScheduleDS()
    {
        if(null == _scheduleDS)
            _scheduleDS = _uiConfig.getDataSource(SppScheduleSessionOrgUnitList.SCHEDULE_DS);
        return _scheduleDS;
    }

    public Boolean getEntityEditDisabled()
    {
        SppScheduleSession schedule = getScheduleDS().getCurrent();
        return schedule.isApproved() || schedule.isArchived();
    }

    public Boolean getEntityDeleteDisabled()
    {
        return false;
    }

    public Map<String, Object> getParametersMap()
    {
        return new ParametersMap()
                .add("orgUnitId", getOrgUnit().getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("orgUnitId", getOrgUnit().getId());
        if (SppScheduleSessionOrgUnitList.SCHEDULE_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleSessionDSHandler.PROP_TITLE, _uiSettings.get("title"));
            dataSource.put(SppScheduleSessionDSHandler.PROP_YEAR_PART_ID, getSettings().getEntityId("yearPart"));
            dataSource.putAll(_uiSettings.getAsMap(true, "courses", "eduLevels", "groups"));
        }
        if(SppScheduleSessionOrgUnitList.EDU_LEVEL_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "courses"));
        }
        if(SppScheduleSessionOrgUnitList.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "courses", "eduLevels"));
        }
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(SppScheduleSessionAddEdit.class).parameter("scheduleId", getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        SppScheduleSessionManager.instance().dao().deleteSchedule(getListenerParameterAsLong());
    }

    public void onClickAddSchedule()
    {
        _uiActivation.asRegionDialog(SppScheduleSessionAddEdit.class)
                .parameter("orgUnitId", getOrgUnit().getId())
                .activate();
    }

    public void onClickAddSchedules()
    {
        _uiActivation.asRegion(SppScheduleSessionAddSchedulesSelectGroupsList.class)
                .parameter(SppScheduleSessionAddSchedulesSelectGroupsListUI.BIND_ORG_UNIT_ID, getOrgUnit().getId())
                .activate();
    }
}
