package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unispp_2x6x2_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherDailyPreferenceElement

		// создано свойство teacherPreference
		{
            if (!tool.columnExists("spptchrdlyprfrncelmnt_t", "teacherpreference_id"))
            {
			// создать колонку
			tool.createColumn("spptchrdlyprfrncelmnt_t", new DBColumn("teacherpreference_id", DBType.LONG));
            }
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherPreferenceElement

		// создано свойство teacherPreference
		{
            if (!tool.columnExists("sppteacherpreferenceelement_t", "teacherpreference_id"))
            {
         // создать колонку
			tool.createColumn("sppteacherpreferenceelement_t", new DBColumn("teacherpreference_id", DBType.LONG));
            }

        }

		////////////////////////////////////////////////////////////////////////////////
		// сущность sppTeacherSessionPreferenceElement

		// создано свойство teacherPreference
		{
            if (!tool.columnExists("spptchrsssnprfrncelmnt_t", "teacherpreference_id"))
            {
			// создать колонку
			tool.createColumn("spptchrsssnprfrncelmnt_t", new DBColumn("teacherpreference_id", DBType.LONG));
            }
		}


    }
}