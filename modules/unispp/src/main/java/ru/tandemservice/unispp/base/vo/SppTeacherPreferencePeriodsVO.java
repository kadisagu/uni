/* $Id: */
package ru.tandemservice.unispp.base.vo;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author vnekrasov
 * @since 6/24/14
 */
public class SppTeacherPreferencePeriodsVO
{
    private List<SppTeacherPreferencePeriodVO> _periods = Lists.newArrayList();
    private SppTeacherPreferenceWeekRowVO _weekPeriodsRowVO;
    private SppTeacherPreferencePeriodVO _currentPeriod;
    private boolean _view = true;

    public List<SppTeacherPreferencePeriodVO> getPeriods()
    {
        return _periods;
    }

    public void setPeriods(List<SppTeacherPreferencePeriodVO> periods)
    {
        _periods = periods;
    }

    public SppTeacherPreferenceWeekRowVO getWeekPeriodsRowVO()
    {
        return _weekPeriodsRowVO;
    }

    public void setWeekPeriodsRowVO(SppTeacherPreferenceWeekRowVO weekPeriodsRowVO)
    {
        _weekPeriodsRowVO = weekPeriodsRowVO;
    }

    public SppTeacherPreferencePeriodVO getCurrentPeriod()
    {
        return _currentPeriod;
    }

    public void setCurrentPeriod(SppTeacherPreferencePeriodVO currentPeriod)
    {
        _currentPeriod = currentPeriod;
    }

    public void setView(boolean view)
    {
        _view = view;
    }

    public boolean isView()
    {
        return _view;
    }

    public boolean getGroupVisible()
    {
        return _periods.size() > 1;
    }

    public boolean getAddActive()
    {
        return true;
    }

    public boolean getEditActive()
    {
        return _view && getVisible();
    }

    public boolean getSaveActive()
    {
        return !_view;
    }

    public boolean getVisible()
    {
        return _periods.size() > 0;
    }
}
