/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.entity.*;

import java.util.Collection;
import java.util.List;

/**
 * @author vnekrasov
 * @since 12/27/13
 */
public interface ISppScheduleSessionDAO extends INeedPersistenceSupport
{
    void createOrUpdateSeason(SppScheduleSessionSeason season);

    void saveSchedules(List<SppScheduleSession> scheduleList);

    void saveOrUpdateSchedule(SppScheduleSession schedule, ScheduleBell oldBells);

    UniplacesClassroomType getDisciplineLectureRoomType(EppRegistryElement discipline, EppALoadType loadType);

    void copySchedule(Long scheduleSessionId, SppScheduleSession newScheduleSession);

    void sentToArchive(Long scheduleId);

    void restoreFromArchive(Long scheduleId);

    /**
     * Выполнение проверок, необходимых перед утверждением расписания
     *
     * @param schedule расписание
     */
    void validate(SppScheduleSession schedule);

    void approve(Long scheduleId);

    void sentToFormation(Long scheduleId);

    boolean getScheduleDataOrRecipientsChanged(Long scheduleId);

    void createOrUpdateEvent(SppScheduleSessionEvent event);

    void deleteSchedule(Long scheduleId);

    void sendICalendar(Long scheduleId);

    SppScheduleSessionPrintForm savePrintForm(SppScheduleSessionPrintParams printData);

    void sendICalendars();

    void updateScheduleAsChanged(Long scheduleId);

    List<SppScheduleSessionEventExt> getSppScheduleSessionEventExt(SppScheduleSessionEvent event);

    void saveSppScheduleSessionEventExtList(SppScheduleSessionEvent event, Collection<SppScheduleSessionEventExt> list);
}
