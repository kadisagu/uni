/* $Id: */
package ru.tandemservice.unispp.base.vo;

import ru.tandemservice.unispp.base.entity.SppTeacherPreferenceList;

/**
 * @author vnekrasov
 * @since 6/23/14
 */
public class SppTeacherPreferenceVO
{
    public static final int MONDAY = 1;
    public static final int TUESDAY = 2;
    public static final int WEDNESDAY = 3;
    public static final int THURSDAY = 4;
    public static final int FRIDAY = 5;
    public static final int SATURDAY = 6;
    public static final int SUNDAY = 7;

    private SppTeacherPreferenceList _teacherPreference;
    private SppTeacherPreferenceWeekVO _oddWeek;
    private SppTeacherPreferenceWeekVO _evenWeek;

    public SppTeacherPreferenceVO(SppTeacherPreferenceList teacherPreference)
    {
        _teacherPreference = teacherPreference;
    }

    public SppTeacherPreferenceList getTeacherPreference()
        {
            return _teacherPreference;
        }

    public void setTeacherPreference(SppTeacherPreferenceList teacherPreference)
    {
        _teacherPreference = teacherPreference;
    }

    public SppTeacherPreferenceWeekVO getOddWeek()
    {
        return _oddWeek;
    }

    public void setOddWeek(SppTeacherPreferenceWeekVO oddWeek)
    {
        _oddWeek = oddWeek;
    }

    public SppTeacherPreferenceWeekVO getEvenWeek()
    {
        return _evenWeek;
    }

    public void setEvenWeek(SppTeacherPreferenceWeekVO evenWeek)
    {
        _evenWeek = evenWeek;
    }
}
