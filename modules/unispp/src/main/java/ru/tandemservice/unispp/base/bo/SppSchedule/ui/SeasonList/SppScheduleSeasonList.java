/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.SeasonList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;

/**
 * @author nvankov
 * @since 9/3/13
 */
@Configuration
public class SppScheduleSeasonList extends BusinessComponentManager
{
    public final static String SCHEDULE_SEASON_DS = "scheduleSeasonDS";

    @Bean
    public ColumnListExtPoint scheduleSeasonCL()
    {
        return columnListExtPointBuilder(SCHEDULE_SEASON_DS)
                .addColumn(textColumn("title", SppScheduleSeason.title()).order().required(true))
                .addColumn(textColumn("yearPart", SppScheduleSeason.eppYearPart().title()).required(true))
                .addColumn(dateColumn("startDate", SppScheduleSeason.startDate()).order())
                .addColumn(dateColumn("endDate", SppScheduleSeason.endDate()).order())
                .addColumn(booleanColumn("startFromEven", SppScheduleSeason.startFromEven()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("sppScheduleSeasonListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("message:ui.deleteAlert").permissionKey("sppScheduleSeasonListDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SCHEDULE_SEASON_DS, scheduleSeasonCL(), SppScheduleManager.instance().sppScheduleSeasonDSHandler()))
                .create();
    }
}
