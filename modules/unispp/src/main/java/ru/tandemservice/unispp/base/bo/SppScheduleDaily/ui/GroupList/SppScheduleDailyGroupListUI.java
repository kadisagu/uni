/* $Id: SppScheduleDailyGroupListUI.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.GroupList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.AddEdit.SppScheduleDailyAddEdit;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "groupId", required = true)
})
public class SppScheduleDailyGroupListUI extends UIPresenter
{
    private Long _groupId;
    private Group _group;

    private BaseSearchListDataSource _scheduleDS;

    public BaseSearchListDataSource getScheduleDS()
    {
        if(null == _scheduleDS)
            _scheduleDS = _uiConfig.getDataSource(SppScheduleDailyGroupList.SCHEDULE_DS);
        return _scheduleDS;
    }

    public Long getGroupId()
    {
        return _groupId;
    }

    public void setGroupId(Long groupId)
    {
        _groupId = groupId;
    }

    public Boolean getAddScheduleVisible()
    {
        return !_group.isArchival();
    }

    public Boolean getEntityEditDisabled()
    {
        SppScheduleDaily schedule = getScheduleDS().getCurrent();
        return schedule.isApproved() || schedule.isArchived();
    }

    public Boolean getEntityDeleteDisabled()
    {
        return false;
    }

    @Override
    public void onComponentRefresh()
    {
        if(_group == null)
            _group = DataAccessServices.dao().get(_groupId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleDailyGroupList.SCHEDULE_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleDailyDSHandler.PROP_GROUP_ID, _groupId);
            dataSource.put(SppScheduleDailyDSHandler.PROP_TITLE, _uiSettings.get("title"));
        }
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(SppScheduleDailyAddEdit.class).parameter("scheduleId", getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        SppScheduleDailyManager.instance().dao().deleteSchedule(getListenerParameterAsLong());
    }

    public void onClickAddSchedule()
    {
        _uiActivation.asRegionDialog(SppScheduleDailyAddEdit.class)
                .parameter("groupId", _groupId)
                .activate();
    }
}
