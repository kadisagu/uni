/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.AddSchedulesExercise;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBell;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.entity.SppScheduleSession;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;
import ru.tandemservice.unispp.base.entity.catalog.SppScheduleStatus;
import ru.tandemservice.unispp.base.entity.catalog.codes.SppScheduleStatusCodes;

import java.util.List;

/**
 * @author Igor Belanov
 * @since 25.10.2016
 */
@Input({
        @Bind(key = SppScheduleSessionAddSchedulesExerciseUI.BIND_GROUP_IDS_LIST, binding = "groupIds", required = true)
})
public class SppScheduleSessionAddSchedulesExerciseUI extends UIPresenter
{
    public static final String BIND_GROUP_IDS_LIST = "groupIds";

    private List<Long> _groupIds;

    private String _title;
    private SppScheduleSessionSeason _season;
    private ScheduleBell _bells;

    public void onSaveSchedules()
    {
        List<Group> groupList = DataAccessServices.dao().getList(Group.class, Group.id(), _groupIds);
        SppScheduleStatus formationStatus = DataAccessServices.dao().getNotNull(SppScheduleStatus.class, SppScheduleStatus.code(), SppScheduleStatusCodes.FORMATION);

        List<SppScheduleSession> scheduleList = Lists.newArrayList();
        for (Group group : groupList)
        {
            SppScheduleSession schedule = new SppScheduleSession();
            schedule.setTitle(getTitleForSave(group));
            schedule.setApproved(false);
            schedule.setArchived(false);
            schedule.setStatus(formationStatus);
            schedule.setGroup(group);
            schedule.setBells(_bells);
            schedule.setSeason(_season);
            scheduleList.add(schedule);
        }
        SppScheduleSessionManager.instance().dao().saveSchedules(scheduleList);
        deactivate();
    }

    protected String getTitleForSave(Group group)
    {
        return getTitle() + " (группа " + group.getTitle() + ")";
    }

    // getters & setters
    public List<Long> getGroupIds()
    {
        return _groupIds;
    }

    public void setGroupIds(List<Long> groupIds)
    {
        _groupIds = groupIds;
    }

    public String getTitle()
    {
        return _title;
    }

    public void setTitle(String title)
    {
        _title = title;
    }

    public SppScheduleSessionSeason getSeason()
    {
        return _season;
    }

    public void setSeason(SppScheduleSessionSeason season)
    {
        _season = season;
    }

    public ScheduleBell getBells()
    {
        return _bells;
    }

    public void setBells(ScheduleBell bells)
    {
        _bells = bells;
    }
}
