/*$Id$*/
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.MassAddingExercise;

import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * @author DMITRY KNYAZEV
 * @since 04.12.2015
 */
class DayOfWeekDSHandler extends SimpleTitledComboDataSourceHandler
{
    public DayOfWeekDSHandler(String ownerId)
    {
        super(ownerId);
        filtered(true);
        addAll(getOptionList());
    }

    public static List<IdentifiableWrapper> getOptionList()
    {
        List<IdentifiableWrapper> wrapperList = new ArrayList<>(DayOfWeek.values().length);
        final Locale ru = new Locale("ru");
        for (DayOfWeek dayOfWeek : DayOfWeek.values())
        {
            final Long day = (long) dayOfWeek.getValue();
            final String title = String.valueOf(dayOfWeek.getDisplayName(TextStyle.FULL, ru));
            wrapperList.add(new IdentifiableWrapper(day, title));
        }
        return Collections.unmodifiableList(wrapperList);
    }

    public static DayOfWeek getDayOfWeek(@NotNull IdentifiableWrapper dayWrapper)
    {
        final int id = java.lang.Math.toIntExact(dayWrapper.getId());
        return DayOfWeek.of(id);
    }

    public static ISelectModel getSelectModel()
    {
        return new LazySimpleSelectModel<>(getOptionList());
    }
}
