/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.SessionElementAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionSeason;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceElement;
import ru.tandemservice.unispp.base.entity.SppTeacherSessionPreferenceList;

import java.util.*;

/**
 * @author vnekrasov
 * @since 7/3/14
 */
@Input({
        @Bind(key = "teacherSessionPreferenceId", binding = "teacherSessionPreferenceId")
})
public class SppTeacherPreferenceSessionElementAddUI extends UIPresenter
{
    private Long _teacherSessionPreferenceId;
    private SppTeacherSessionPreferenceList _teacherSessionPreference;
    private List<UniplacesBuilding> _buildingList;
    private List<UniplacesPlace> _lectureRoomList;
    private boolean _unwantedTime;
    private List<IdentifiableWrapper> _dayOfWeekList;
    private List<ScheduleBellEntry> _bellEntryList;
    private Date _startDate;
    private Date _endDate;

    public List<ScheduleBellEntry> getBellEntryList()
    {
        return _bellEntryList;
    }

    public void setBellEntryList(List<ScheduleBellEntry> bellEntryList)
    {
        _bellEntryList = bellEntryList;
    }

    public List<UniplacesPlace> getLectureRoomList()
    {
        return _lectureRoomList;
    }

    public void setLectureRoomList(List<UniplacesPlace> lectureRoomList)
    {
        _lectureRoomList = lectureRoomList;
    }

    public boolean isUnwantedTime()
    {
        return _unwantedTime;
    }

    public void setUnwantedTime(boolean unwantedTime)
    {
        _unwantedTime = unwantedTime;
    }

    public List<IdentifiableWrapper> getDayOfWeekList()
    {
        return _dayOfWeekList;
    }

    public void setDayOfWeekList(List<IdentifiableWrapper> dayOfWeekList)
    {
        _dayOfWeekList = dayOfWeekList;
    }

    public Date getStartDate()
    {
        return _startDate;
    }

    public void setStartDate(Date startDate)
    {
        _startDate = startDate;
    }

    public Date getEndDate()
    {
        return _endDate;
    }

    public void setEndDate(Date endDate)
    {
        _endDate = endDate;
    }

    public List<UniplacesBuilding> getBuildingList()
    {
        return _buildingList;
    }

    public void setBuildingList(List<UniplacesBuilding> buildingList)
    {
        _buildingList = buildingList;
    }

    public Long getTeacherSessionPreferenceId()
    {
        return _teacherSessionPreferenceId;
    }

    public void setTeacherSessionPreferenceId(Long teacherSessionPreferenceId)
    {
        _teacherSessionPreferenceId = teacherSessionPreferenceId;
    }

    public SppTeacherSessionPreferenceList getTeacherSessionPreference()
    {
        return _teacherSessionPreference;
    }

    public void setTeacherSessionPreference(SppTeacherSessionPreferenceList teacherSessionPreference)
    {
        _teacherSessionPreference = teacherSessionPreference;
    }

    public Boolean getAddForm()
    {
        return true;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _teacherSessionPreference)
        {
            _teacherSessionPreference = DataAccessServices.dao().getNotNull(_teacherSessionPreferenceId);
        }
//        if(null == _element)
//        {
//            if(null != _elementId)
//            {
//                _element = DataAccessServices.dao().get(_elementId);
//                if (_element.getDayOfWeek()==1)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.MONDAY;
//                if (_element.getDayOfWeek()==2)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.TUESDAY;
//                if (_element.getDayOfWeek()==3)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.WEDNESDAY;
//                if (_element.getDayOfWeek()==4)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.THURSDAY;
//                if (_element.getDayOfWeek()==5)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.FRIDAY;
//                if (_element.getDayOfWeek()==6)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.SATURDAY;
//                if (_element.getDayOfWeek()==7)
//                    _dayOfWeekWrapper = SppTeacherPreferenceManager.SUNDAY;
//            }
//            else
//            {
//                _element = new SppTeacherSessionPreferenceElement();
//                _element.setTeacherPreference(_teacherSessionPreference);
//            }
//        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppTeacherPreferenceSessionElementAdd.LECTURE_ROOM_DS.equals(dataSource.getName()))
        {
            dataSource.put("buildingIds", UniBaseUtils.getIdList(_buildingList));
        }
        else if (SppTeacherPreferenceSessionElementAdd.BELL_ENTRY_DS.equals(dataSource.getName()))
        {
            dataSource.put("bellScheduleId", _teacherSessionPreference.getBells().getId());
        }
    }

    public String getPeriodStr()
    {
        return _teacherSessionPreference.getSppScheduleSessionSeason().getTitleWithTime();
    }

    public void onChangeLectureRoom()
    {
//        if(null != _element.getLectureRoom())
//            _event.setLectureRoom(_event.getLectureRoomVal().getTitle());
    }

    public void onChangeBuilding()
    {

    }

    public void onChangeDayOfWeek()
    {
//        if(null != _dayOfWeekWrapper)
//            _element.setDayOfWeek(_dayOfWeekWrapper.getId().intValue());
    }


    public void onClickApply()
    {
        SppScheduleSessionSeason season = _teacherSessionPreference.getSppScheduleSessionSeason();
        if (_startDate.before(season.getStartDate()))
            _uiSupport.error("Событие не может быть раньше чем дата начала периода расписания", "startDate");
        if (_endDate.after(season.getEndDate()))
            _uiSupport.error("Событие не может быть позже чем дата окончания периода расписания", "startDate");
        if (getUserContext().getErrorCollector().hasErrors())
            return;

        List<ScheduleBellEntry> bellEntryList = _bellEntryList.isEmpty() ? Collections.singletonList(null) : _bellEntryList;
        List<IdentifiableWrapper> dayOfWeekList = _dayOfWeekList.isEmpty() ? Collections.singletonList(null) : _dayOfWeekList;
        List<UniplacesBuilding> buildingList = (_buildingList.isEmpty() || !(_lectureRoomList.isEmpty())) ? Collections.singletonList(null) : _buildingList;
        List<UniplacesPlace> lectureRoomList = _lectureRoomList.isEmpty() ? Collections.singletonList(null) : _lectureRoomList;

        for (ScheduleBellEntry bellEntry : bellEntryList)
        for (IdentifiableWrapper dayOfWeek : dayOfWeekList)
        for (UniplacesBuilding building : buildingList)
        for (UniplacesPlace lectureRoom : lectureRoomList)
        {
            SppTeacherSessionPreferenceElement element = new SppTeacherSessionPreferenceElement();
            element.setTeacherPreference(_teacherSessionPreference);
            if (bellEntry != null)
                element.setBell(bellEntry);
            if (dayOfWeek!=null)
                element.setDayOfWeek(dayOfWeek.getId().intValue());
            element.setBuilding(building);
            element.setLectureRoom(lectureRoom);
            element.setEndDate(_endDate);
            element.setStartDate(_startDate);
            element.setUnwantedTime(_unwantedTime);
            SppTeacherPreferenceManager.instance().dao().createOrUpdateSessionElement(element);
        }
        //SppScheduleSessionManager.instance().dao().updateScheduleAsChanged(_schedule.getId());
        deactivate();
    }
}
