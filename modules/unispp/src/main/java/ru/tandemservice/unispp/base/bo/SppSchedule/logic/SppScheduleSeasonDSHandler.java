/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unispp.base.entity.SppScheduleSeason;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author nvankov
 * @since 9/3/13
 */
public class SppScheduleSeasonDSHandler extends DefaultSearchDataSourceHandler
{
    public SppScheduleSeasonDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String title = context.get("title");
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppScheduleSeason.class, "s");

        if(!StringUtils.isEmpty(title))
            builder.where(like(DQLFunctions.upper(property("s", SppScheduleSeason.title())), value(CoreStringUtils.escapeLike(title, true))));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order().build();
    }
}
