/* $Id$ */
package ru.tandemservice.unispp.base.vo;

import com.google.common.collect.Maps;
import org.joda.time.LocalDate;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent;

import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 9/25/13
 */
public class SppScheduleDailyGroupPrintVO
{
    private SppScheduleDaily _schedule;
    private int _columnNum;
    private int _column;
    private int _totalColumnSize;
    private Map<LocalDate, List<SppScheduleDailyEvent>> _eventsMap = Maps.newHashMap();


    public SppScheduleDailyGroupPrintVO(SppScheduleDaily schedule)
    {
        _schedule = schedule;
    }

    public SppScheduleDaily getSchedule()
    {
        return _schedule;
    }

    public void setSchedule(SppScheduleDaily schedule)
    {
        _schedule = schedule;
    }

    public int getColumnNum()
    {
        return _columnNum;
    }

    public void setColumnNum(int columnNum)
    {
        _columnNum = columnNum;
    }

    public int getColumn()
    {
        return _column;
    }

    public void setColumn(int column)
    {
        _column = column;
    }

    public int getTotalColumnSize()
    {
        return _totalColumnSize;
    }

    public void setTotalColumnSize(int totalColumnSize)
    {
        _totalColumnSize = totalColumnSize;
    }

    public Map<LocalDate, List<SppScheduleDailyEvent>> getEventsMap()
    {
        return _eventsMap;
    }

    public void setEventsMap(Map<LocalDate, List<SppScheduleDailyEvent>> eventsMap)
    {
        _eventsMap = eventsMap;
    }
}
