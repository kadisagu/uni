/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceElement;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vnekrasov
 * @since 7/2/14
 */
public class SppTeacherDailyPreferenceElementDSHandler extends DefaultSearchDataSourceHandler
{
    public SppTeacherDailyPreferenceElementDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long teacherDailyPreferenceId = context.getNotNull("teacherDailyPreferenceId");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SppTeacherDailyPreferenceElement.class, "e");
        builder.where(eq(property("e", SppTeacherDailyPreferenceElement.teacherPreference().id()), value(teacherDailyPreferenceId)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().pageable(true).build();
    }
}
