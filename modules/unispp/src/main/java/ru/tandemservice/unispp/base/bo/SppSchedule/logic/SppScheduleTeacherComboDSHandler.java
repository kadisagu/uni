/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.*;
import ru.tandemservice.unispp.util.SppScheduleUtil;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.*;

/**
 * @author Alexey Lopatin
 * @since 07.11.2014
 */
public class SppScheduleTeacherComboDSHandler extends DefaultComboDataSourceHandler
{
    public static final String PROP_ORG_UNIT_ID = "orgUnitId";
    public static final String PROP_CHECK_FREE_TEACHERS = "checkFreeTeachers";
    public static final String PROP_CURRENT_PERIOD = "currentPeriod";
    public static final String PROP_LECTURE_ROOM_ID = "lectureRoomId";
    public static final String PROP_EVENT_DAY_OF_WEEK = "eventDayOfWeek";
    public static final String PROP_EVENT_BELL_ENTRY_ID = "eventBellEntryId";
    public static final String PROP_SCHEDULE_IDS = "scheduleIds";
    public static final String PROP_SEASON_ID = "seasonId";
    public static final String PROP_DISCIPLINE_ID = "disciplineId";
    public static final String PROP_CHECK_TEACHER_PREFERENCES = "checkTeacherPreferences";
    public static final String PROP_IS_EVEN = "isEven";
    public static final String PROP_START_DATE = "startDate";
    public static final String PROP_END_DATE = "endDate";

    public SppScheduleTeacherComboDSHandler(String ownerId)
    {
        super(ownerId, PpsEntry.class);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        String filter = ep.input.getComboFilterByValue();
        DQLSelectBuilder dql = ep.dqlBuilder;
        ExecutionContext context = ep.context;

        SppSchedulePeriod currentPeriod = context.get(PROP_CURRENT_PERIOD);
        Long lectureRoomId = context.get(PROP_LECTURE_ROOM_ID);
        Long disciplineId = context.get(PROP_DISCIPLINE_ID);
        Long orgUnitId = context.get(PROP_ORG_UNIT_ID);
        Long seasonId = context.get(PROP_SEASON_ID);
        Long bellEntryId = context.get(PROP_EVENT_BELL_ENTRY_ID);
        List<Long> sppScheduleIds = context.get(PROP_SCHEDULE_IDS);
        Integer eventDayOfWeek = context.get(PROP_EVENT_DAY_OF_WEEK);
        boolean checkFreeTeachers = context.getBoolean(PROP_CHECK_FREE_TEACHERS, Boolean.FALSE);
        boolean checkTeacherPreferences = context.getBoolean(PROP_CHECK_TEACHER_PREFERENCES, Boolean.FALSE);
        boolean isEven = context.getBoolean(PROP_IS_EVEN, Boolean.FALSE);
        Date startDate = context.get(PROP_START_DATE);
        Date endDate = context.get(PROP_END_DATE);

        if (checkFreeTeachers && null != bellEntryId && null != seasonId && null != eventDayOfWeek)
        {
            SppScheduleSeason season = DataAccessServices.dao().getNotNull(seasonId);
            ScheduleBellEntry bellEntry = DataAccessServices.dao().getNotNull(bellEntryId);
            Date periodStartDate = startDate != null ? startDate : season.getStartDate();
            Date periodEndDate = endDate != null ? endDate : season.getEndDate();

            DQLSelectBuilder builder1 = new DQLSelectBuilder().fromEntity(SppSchedulePeriod.class, "p")
                    .column(property("p", SppSchedulePeriod.teacherVal().id()))
                    .where(currentPeriod == null ? null : ne(property("p", SppSchedulePeriod.id()), value(currentPeriod.getId())))
                    .where(eq(property("p", SppSchedulePeriod.dayOfWeek()), value(eventDayOfWeek)))
                    // даты пересеклись
                    .where(or(
                            and(
                                    isNotNull(property("p", SppSchedulePeriod.endDate())),
                                    ge(property("p", SppSchedulePeriod.endDate()), valueDate(periodStartDate))),
                            and(
                                    isNull(property("p", SppSchedulePeriod.endDate())),
                                    ge(property("p", SppSchedulePeriod.weekRow().schedule().season().endDate()), valueDate(periodStartDate)))))
                    .where(or(
                            and(
                                    isNotNull(property("p", SppSchedulePeriod.startDate())),
                                    le(property("p", SppSchedulePeriod.startDate()), valueDate(periodEndDate))),
                            and(
                                    isNull(property("p", SppSchedulePeriod.startDate())),
                                    le(property("p", SppSchedulePeriod.weekRow().schedule().season().startDate()), valueDate(periodEndDate)))))
                    // звонковая пара пересеклась
                    .where(gt(property("p", SppSchedulePeriod.weekRow().bell().endTime()), value(bellEntry.getStartTime())))
                    .where(lt(property("p", SppSchedulePeriod.weekRow().bell().startTime()), value(bellEntry.getEndTime())))
                    .where(or(
                            eq(property("p", SppSchedulePeriod.weekRow().schedule().diffEven()), value(Boolean.FALSE)),
                            eq(property("p", SppSchedulePeriod.weekRow().even()), value(isEven)),
                            // либо неявно скопируется пара, которую мы устанавливаем (хотя бы одна)
                            notExists(new DQLSelectBuilder()
                                    .fromEntity(SppSchedulePeriod.class, "sp1")
                                    .where(in(property("sp1", SppSchedulePeriod.weekRow().schedule().id()), sppScheduleIds))
                                    .where(eq(property("sp1", SppSchedulePeriod.weekRow().schedule().diffEven()), value(Boolean.TRUE)))
                                    .where(eq(property("sp1", SppSchedulePeriod.dayOfWeek()), value(eventDayOfWeek)))
                                    .where(eq(property("sp1", SppSchedulePeriod.weekRow().bell().id()), value(bellEntryId)))
                                    .where(eq(property("sp1", SppSchedulePeriod.weekRow().even()), value(!isEven)))
                                    .buildQuery()),
                            // либо среди пар, которые пересекаются с нашей есть те, которые неявно скопируются
                            notExists(new DQLSelectBuilder()
                                    .fromEntity(SppSchedulePeriod.class, "sp2")
                                    .where(eq(property("sp2", SppSchedulePeriod.weekRow().schedule().id()), property("p", SppSchedulePeriod.weekRow().schedule().id())))
                                    .where(eq(property("sp2", SppSchedulePeriod.dayOfWeek()), property("p", SppSchedulePeriod.dayOfWeek())))
                                    .where(eq(property("sp2", SppSchedulePeriod.weekRow().bell().id()), property("p", SppSchedulePeriod.weekRow().bell().id())))
                                    .where(ne(property("sp2", SppSchedulePeriod.weekRow().even()), property("p", SppSchedulePeriod.weekRow().even())))
                                    .buildQuery())))
                    .where(isNotNull(property("p", SppSchedulePeriod.teacherVal())));

            DQLSelectBuilder builder2 = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "p").column("p")
//                    .column(property("p", SppScheduleDailyEvent.teacherVal().id()))
                    // todo добавить сравнение по дню недели (в dql вытаскивание dow не реализовано)
//                    .where(eq(property("p", SppScheduleDailyEvent.date()), value(eventDayOfWeek)))
                    // даты пересеклись
                    .where(ge(property("p", SppScheduleDailyEvent.date()), valueDate(periodStartDate)))
                    .where(le(property("p", SppScheduleDailyEvent.date()), valueDate(periodEndDate)))
                    // звонковая пара пересеклась
                    .where(gt(property("p", SppScheduleDailyEvent.bell().endTime()), value(bellEntry.getStartTime())))
                    .where(lt(property("p", SppScheduleDailyEvent.bell().startTime()), value(bellEntry.getEndTime())))
                    // todo возможно учесть неявное копирование, но тогда нужно вычислять в чётной неделе или в нечётной находится событие
                    .where(isNotNull(property("p", SppScheduleDailyEvent.teacherVal())));
            // костыль (пока нет dow в dql)
            List<Long> list2 = builder2.createStatement(getSession()).list().stream()
                    .map(o -> (SppScheduleDailyEvent)o)
                    .filter(o -> o != null && SppScheduleUtil.getDayOfWeek(o.getDate()) == eventDayOfWeek)
                    .map(EntityBase::getId).collect(Collectors.toList());

            DQLSelectBuilder builder3 = new DQLSelectBuilder().fromEntity(SppScheduleSessionEvent.class, "p").column("p")
//                    .column(property("p", SppScheduleSessionEvent.teacherVal().id()))
                    // todo добавить сравнение по дню недели (в dql вытаскивание dow не реализовано)
//                    .where(eq(property("p", SppScheduleDailyEvent.date()), value(eventDayOfWeek)))
                    // даты пересеклись
                    .where(ge(property("p", SppScheduleSessionEvent.date()), valueDate(periodStartDate)))
                    .where(le(property("p", SppScheduleSessionEvent.date()), valueDate(periodEndDate)))
                    // звонковая пара пересеклась
                    .where(gt(property("p", SppScheduleSessionEvent.bell().endTime()), value(bellEntry.getStartTime())))
                    .where(lt(property("p", SppScheduleSessionEvent.bell().startTime()), value(bellEntry.getEndTime())))
                    // todo возможно учесть неявное копирование, но тогда нужно вычислять в чётной неделе или в нечётной находится событие
                    .where(isNotNull(property("p", SppScheduleSessionEvent.teacherVal())));
            // костыль (пока нет dow в dql)
            List<Long> list3 = builder3.createStatement(getSession()).list().stream()
                    .map(o -> (SppScheduleSessionEvent)o)
                    .filter(o -> o != null && SppScheduleUtil.getDayOfWeek(o.getDate()) == eventDayOfWeek)
                    .map(EntityBase::getId).collect(Collectors.toList());

            dql.where(notIn(property("e", PpsEntry.id()), builder1.buildQuery()));
            if (list2 != null && !list2.isEmpty()) dql.where(notIn(property("e", PpsEntry.id()), list2));
            if (list3 != null && !list3.isEmpty()) dql.where(notIn(property("e", PpsEntry.id()), list3));
        }

        // фильтрация по предпочтениям места и времени, установленным преподавателем
        if (checkTeacherPreferences && bellEntryId != null && eventDayOfWeek != null && seasonId != null && sppScheduleIds != null && lectureRoomId != null)
        {
            // очень хитрый запрос с двумя подзапросами:
            // вытаскивает все id преподавателей, которые не хотят проводить лекцию в выбранной аудитории

            DQLSelectBuilder subQuery1Builder = new DQLSelectBuilder();
            subQuery1Builder.fromEntity(UniplacesPlace.class, "plc");
            // флаг нежелательности, id преподавателя, и аудитория (берём либо конкретную либо все из указанного здания)
            subQuery1Builder.column(property("tpe", SppTeacherPreferenceElement.unwantedTime()), "uTime");
            subQuery1Builder.column(property("tpe", SppTeacherPreferenceElement.weekRow().teacherPreference().teacher().id()), "teacherId");
            subQuery1Builder.column(caseExpr(
                    isNotNull(property("tpe", SppTeacherPreferenceElement.lectureRoom().id())),
                    property("tpe", SppTeacherPreferenceElement.lectureRoom().id()),
                    property("plc", UniplacesPlace.id())), "lectureRoomId");
            // прицепим всё что нужно
            subQuery1Builder.joinPath(DQLJoinType.inner, UniplacesPlace.floor().unit().fromAlias("plc"), "unt");
            subQuery1Builder.joinEntity("unt", DQLJoinType.right, SppTeacherPreferenceElement.class, "tpe", eq(
                    property("tpe", SppTeacherPreferenceElement.building().id()),
                    property("unt", UniplacesUnit.building().id())));
            // фильтры
            subQuery1Builder.where(eq(property("tpe", SppTeacherPreferenceElement.weekRow().teacherPreference().sppScheduleSeason().id()), value(seasonId)));
            subQuery1Builder.where(eq(property("tpe", SppTeacherPreferenceElement.dayOfWeek()), value(eventDayOfWeek)));
            subQuery1Builder.where(eq(property("tpe", SppTeacherPreferenceElement.weekRow().bell().id()), value(bellEntryId)));
            // учёт неявного копирования:
            subQuery1Builder.where(or(
                    // либо нет различия по чётности в предпочтении, либо чётность совпала, либо в соседней неделе пусто
                    // и после неявного копирования занятие попадёт в предпочтение
                    eq(property("tpe", SppTeacherPreferenceElement.weekRow().teacherPreference().diffEven()), value(Boolean.FALSE)),
                    eq(property("tpe", SppTeacherPreferenceElement.weekRow().even()), value(isEven)),
                    notExists(new DQLSelectBuilder()
                            .fromEntity(SppSchedulePeriod.class, "sp")
                            .where(in(property("sp", SppSchedulePeriod.weekRow().schedule().id()), sppScheduleIds))
                            .where(eq(property("sp", SppSchedulePeriod.weekRow().schedule().diffEven()), value(Boolean.TRUE)))
                            .where(eq(property("sp", SppSchedulePeriod.dayOfWeek()), value(eventDayOfWeek)))
                            .where(eq(property("sp", SppSchedulePeriod.weekRow().bell()), value(bellEntryId)))
                            .where(eq(property("sp", SppSchedulePeriod.weekRow().even()), value(!isEven)))
                            .buildQuery())));
            subQuery1Builder.distinct();

            // так же пронумеруем предпочтения по приоритету (предпочтительные вперёд, без места вперёд)
            DQLSelectBuilder subQuery2Builder = new DQLSelectBuilder();
            subQuery2Builder.fromDataSource(subQuery1Builder.buildQuery(), "tmp1");
            subQuery2Builder.column("tmp1.uTime").column("tmp1.teacherId").column("tmp1.lectureRoomId");
            subQuery2Builder.column(rowNumberBuilder()
                    .partition(property("tmp1.teacherId"))
                    .order(property("tmp1.uTime"))
                    .order(property("tmp1.lectureRoomId"), OrderDirection.desc)
                    .build(), "rn");
            subQuery2Builder.where(or(
                    eq(property("tmp1.uTime"), value(Boolean.FALSE)),
                    isNull(property("tmp1.lectureRoomId")),
                    eq(property("tmp1.lectureRoomId"), value(lectureRoomId))));

            // исключим преподавателя исходя из наиболее приоритетного предпочтения
            DQLSelectBuilder excludeTeacherBuilder = new DQLSelectBuilder();
            excludeTeacherBuilder.fromDataSource(subQuery2Builder.buildQuery(), "tmp2");
            excludeTeacherBuilder.column("tmp2.teacherId");
            excludeTeacherBuilder.where(eq(property("tmp2.rn"), value(1)));
            excludeTeacherBuilder.where(or(
                    // либо предпочтительная не совпала
                    and(
                            eq(property("tmp2.uTime"), value(Boolean.FALSE)),
                            ne(property("tmp2.lectureRoomId"), value(lectureRoomId))),
                    // либо нежелательная совпала (или нежелательны все)
                    and(
                            eq(property("tmp2.uTime"), value(Boolean.TRUE)),
                            or(
                                    isNull(property("tmp2.lectureRoomId")),
                                    eq(property("tmp2.lectureRoomId"), value(lectureRoomId))))));

            dql.where(notIn(property("e", PpsEntry.person().id()), excludeTeacherBuilder.buildQuery()));
        }

        // только активные
        dql.where(exists(new DQLSelectBuilder().fromEntity(EmployeePost4PpsEntry.class, "ep4pps")
                .where(eq(property("ep4pps", EmployeePost4PpsEntry.ppsEntry().id()), property("e", PpsEntry.id())))
                .where(isNull(property("ep4pps", EmployeePost4PpsEntry.removalDate())))
                .buildQuery()));

        if (null != disciplineId)
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(SppDisciplinePreferenceList.class, "e")
                    .column(property("e", SppDisciplinePreferenceList.teacher().id()))
                    .where(eq(property("e", SppDisciplinePreferenceList.discipline().id()), value(disciplineId)));
            dql.where(in(property("e", PpsEntry.id()), subBuilder.buildQuery()));
        }
        if (orgUnitId != null)
        {
            dql.where(eq(property("e", PpsEntry.orgUnit().id()), value(orgUnitId)));
        }
        if (!StringUtils.isEmpty(filter))
        {
            dql.where(like(upper(concat(
                    property("e", PpsEntry.person().identityCard().lastName()),
//                    property("e", EmployeePost.postRelation().postBoundedWithQGandQL().title()),
                    property("e", PpsEntry.orgUnit().fullTitle()))
            ), value(CoreStringUtils.escapeLike(filter, Boolean.TRUE))));
        }

        setOrderByProperty(PpsEntry.person().identityCard().lastName().s());
    }
}
