package ru.tandemservice.unispp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unispp_2x6x8_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sppScheduleSessionEvent

        // создано свойство formControl
        {
            // создать колонку
            tool.createColumn("sppschedulesessionevent_t", new DBColumn("subjectformcontrol_id", DBType.LONG));
        }

        Statement stmt = tool.getConnection().createStatement();
        ResultSet src = stmt.executeQuery("select id, subjectval_id from sppschedulesessionevent_t where subjectloadtype_id is not null");

        while (src.next())
        {
            Long id = src.getLong(1);
            Long subjectValId = src.getLong(2);
            Long controlaction_id = getId(tool, "select fa.controlaction_id from epp_reg_element_part_fca_t fa inner join epp_reg_element_part_t p on fa.part_id = p.id where p.registryelement_id = '" + subjectValId + "'");

            if (null == controlaction_id)
                tool.executeUpdate("delete from sppschedulesessionevent_t where id = ? ", id);
            else
                tool.executeUpdate("update sppschedulesessionevent_t set subjectformcontrol_id = ? where id = ? ", controlaction_id, id);
        }

		// удалено свойство subjectLoadType
		{
			// удалить колонку
			tool.dropColumn("sppschedulesessionevent_t", "subjectloadtype_id");
		}
    }

    private Long getId(DBTool tool, String sql) throws Exception
    {
        Statement stmt = tool.getConnection().createStatement();
        ResultSet src = stmt.executeQuery(sql);
        while (src.next())
        {
            return src.getLong(1);
        }
        return null;
    }
}