/* $Id: SppScheduleDailyViewUI.java 34872 2014-06-11 07:20:04Z vnekrasov $ */
package ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.View;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.AddEdit.SppScheduleDailyAddEdit;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.Copy.SppScheduleDailyCopy;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.EventAddEdit.SppScheduleDailyEventAddEdit;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;
import ru.tandemservice.unispp.base.entity.catalog.codes.SppScheduleStatusCodes;

import java.util.Map;

/**
 * @author vnekrasov
 * @since 6/11/14
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "scheduleId", required = true),
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class SppScheduleDailyViewUI extends UIPresenter
{
    private Long _scheduleId;
    private SppScheduleDaily _schedule;
    private BaseSearchListDataSource _warningLogDS;
    private Long _orgUnitId;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public BaseSearchListDataSource getWarningLogDS()
       {
           if (_warningLogDS == null)
               _warningLogDS = getConfig().getDataSource(SppScheduleDailyView.WARNING_LOG_DS);
           return _warningLogDS;
       }

    public Long getScheduleId()
    {
        return _scheduleId;
    }

    public void setScheduleId(Long scheduleId)
    {
        _scheduleId = scheduleId;
    }

    //На формировании
    public Boolean getScheduleOnFormation()
    {
        return SppScheduleStatusCodes.FORMATION.equals(_schedule.getStatus().getCode());
    }

    //Утверждено
    public Boolean getScheduleApproved()
    {
        return _schedule.isApproved();
    }


    // В архиве
    public Boolean getScheduleArchived()
    {
        return _schedule.isArchived();
    }

    //    visible="ui:editScheduleVisible"
    public Boolean getEditScheduleVisible()
    {
        return getScheduleOnFormation() && !getScheduleArchived();
    }

    //    visible="ui:sentToArchiveVisible"
    public Boolean getSentToArchiveVisible()
    {
        return !getScheduleArchived();
    }
    //    visible="ui:restoreFromArchiveVisible"
    public Boolean getRestoreFromArchiveVisible()
    {
        return getScheduleArchived() && !_schedule.getGroup().isArchival();
    }

    //    visible="ui:approveVisible"
    public Boolean getApproveVisible()
    {
        return getScheduleOnFormation() && !getScheduleArchived();
    }

    //    visible="ui:sentToFormationVisible"
    public Boolean getSentToFormationVisible()
    {
        return getScheduleApproved() && !getScheduleArchived();
    }

    public Boolean getSentToPortalVisible()
    {
        boolean dataOrRecipientsChanged = SppScheduleDailyManager.instance().dao().getScheduleDataOrRecipientsChanged(_schedule.getId());
        return getScheduleApproved() && !getScheduleArchived() && dataOrRecipientsChanged;
    }

    public Boolean getAddEventVisible()
    {
        return !getScheduleArchived() && getScheduleOnFormation();
    }

    //    ui:eventDeleteDisabled
    public Boolean getEventDeleteDisabled()
    {
        return getScheduleArchived() || getScheduleApproved();
    }

    //    ui:eventEditDisabled
    public Boolean getEventEditDisabled()
    {
        return getScheduleArchived() || getScheduleApproved();
    }

    public Map getGroupParameterMap()
    {
        return new ParametersMap()
                .add(UIPresenter.PUBLISHER_ID, _schedule.getGroup().getId())
                .add("selectedTabId", "sppScheduleDailyTab");
    }

    @Override
    public void onComponentRefresh()
    {
        _schedule = DataAccessServices.dao().getNotNull(_scheduleId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(SppScheduleDailyView.SCHEDULE_EVENTS_DS.equals(dataSource.getName()))
        {
            dataSource.put("scheduleId", _schedule.getId());
        }

        if(SppScheduleDailyView.WARNING_LOG_DS.equals(dataSource.getName()))
        {
            dataSource.put("scheduleDailyId", _schedule.getId());
        }
    }

    public void onClickEditSchedule()
    {
        _uiActivation.asRegionDialog(SppScheduleDailyAddEdit.class)
                .parameter("scheduleId", _scheduleId)
                .parameter("orgUnitId", _orgUnitId)
                .activate();
    }

    public void onClickCopySchedule()
    {
        _uiActivation.asRegion(SppScheduleDailyCopy.class).parameter("scheduleDailyId", _scheduleId).activate();
    }

    public void onClickAddEvent()
    {
        _uiActivation.asRegion(SppScheduleDailyEventAddEdit.class).parameter("scheduleId", _scheduleId).activate();
    }

    public void onClickSentToArchive()
    {
        SppScheduleDailyManager.instance().dao().sentToArchive(_schedule.getId());
    }

    public void onClickRestoreFromArchive()
    {
        SppScheduleDailyManager.instance().dao().restoreFromArchive(_schedule.getId());
    }

    public void onClickPerformValidation()
    {
        SppScheduleDailyManager.instance().dao().validate(_schedule);
    }

    public void onClickApprove()
    {
        SppScheduleDailyManager.instance().dao().approve(_schedule.getId());
    }

    public void onClickSentToFormation()
    {
        SppScheduleDailyManager.instance().dao().sentToFormation(_schedule.getId());
    }

    public void onClickCreateICal()
    {
        SppScheduleDailyManager.instance().dao().sendICalendar(_schedule.getId());
    }

    public void onClickEditEvent()
    {
        _uiActivation.asRegion(SppScheduleDailyEventAddEdit.class)
                .parameter("scheduleId", _scheduleId)
                .parameter("eventId", getListenerParameterAsLong()).activate();
    }

    public void onClickDeleteEvent()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        updateScheduleAsChanged();
    }

    private void updateScheduleAsChanged()
    {
        SppScheduleDailyManager.instance().dao().updateScheduleAsChanged(_schedule.getId());
    }

    public void onPrint()
    {
//        List<FefuScheduleGroupPrintVO> groupPrintVOs = Lists.newArrayList();
//        groupPrintVOs.add(FefuScheduleManager.instance().dao().prepareScheduleGroupPrintVO(_schedule));
//        try
//        {
//            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("xls").fileName("schedule.xls").document(FefuSchedulePrintFormExcelContentBuilder.buildExcelContent(groupPrintVOs, null)), true);
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
    }
}
