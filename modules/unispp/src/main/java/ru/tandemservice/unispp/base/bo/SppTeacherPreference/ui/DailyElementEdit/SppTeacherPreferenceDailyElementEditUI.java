/* $Id: */
package ru.tandemservice.unispp.base.bo.SppTeacherPreference.ui.DailyElementEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unispp.base.bo.SppTeacherPreference.SppTeacherPreferenceManager;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;
import ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceElement;
import ru.tandemservice.unispp.base.entity.SppTeacherDailyPreferenceList;

/**
 * @author vnekrasov
 * @since 7/3/14
 */
@Input({
        @Bind(key = "teacherDailyPreferenceId", binding = "teacherDailyPreferenceId"),
        @Bind(key = "elementId", binding = "elementId")
})
public class SppTeacherPreferenceDailyElementEditUI extends UIPresenter
{
    private Long _teacherDailyPreferenceId;
    private Long _elementId;
    private SppTeacherDailyPreferenceElement _element;
    private SppTeacherDailyPreferenceList _teacherDailyPreference;
    private Long _buildingId;

    public IdentifiableWrapper getDayOfWeekWrapper()
    {
        return _dayOfWeekWrapper;
    }

    public void setDayOfWeekWrapper(IdentifiableWrapper dayOfWeekWrapper)
    {
        _dayOfWeekWrapper = dayOfWeekWrapper;
    }

    private IdentifiableWrapper _dayOfWeekWrapper;

    public Long getTeacherDailyPreferenceId()
    {
        return _teacherDailyPreferenceId;
    }

    public void setTeacherDailyPreferenceId(Long teacherDailyPreferenceId)
    {
        _teacherDailyPreferenceId = teacherDailyPreferenceId;
    }

    public Long getElementId()
    {
        return _elementId;
    }

    public void setElementId(Long elementId)
    {
        _elementId = elementId;
    }

    public SppTeacherDailyPreferenceElement getElement()
    {
        return _element;
    }

    public void setElement(SppTeacherDailyPreferenceElement element)
    {
        _element = element;
    }

    public SppTeacherDailyPreferenceList getTeacherDailyPreference()
    {
        return _teacherDailyPreference;
    }

    public void setTeacherDailyPreference(SppTeacherDailyPreferenceList teacherDailyPreference)
    {
        _teacherDailyPreference = teacherDailyPreference;
    }

    public Boolean getAddForm()
    {
        return null == _elementId;
    }

    @Override
    public void onComponentRefresh()
    {
        if (null == _teacherDailyPreference)
        {
            _teacherDailyPreference = DataAccessServices.dao().getNotNull(_teacherDailyPreferenceId);
        }
        if (null == _element)
        {
            if (null != _elementId)
            {
                _element = DataAccessServices.dao().get(_elementId);
                _buildingId = _element.getBuilding() == null ? null : _element.getBuilding().getId();
                if (_element.getDayOfWeek() != null)
                {
                    if (_element.getDayOfWeek() == 1)
                        _dayOfWeekWrapper = SppTeacherPreferenceManager.MONDAY;
                    if (_element.getDayOfWeek() == 2)
                        _dayOfWeekWrapper = SppTeacherPreferenceManager.TUESDAY;
                    if (_element.getDayOfWeek() == 3)
                        _dayOfWeekWrapper = SppTeacherPreferenceManager.WEDNESDAY;
                    if (_element.getDayOfWeek() == 4)
                        _dayOfWeekWrapper = SppTeacherPreferenceManager.THURSDAY;
                    if (_element.getDayOfWeek() == 5)
                        _dayOfWeekWrapper = SppTeacherPreferenceManager.FRIDAY;
                    if (_element.getDayOfWeek() == 6)
                        _dayOfWeekWrapper = SppTeacherPreferenceManager.SATURDAY;
                    if (_element.getDayOfWeek() == 7)
                        _dayOfWeekWrapper = SppTeacherPreferenceManager.SUNDAY;
                }
            }
            else
            {
                _element = new SppTeacherDailyPreferenceElement();
                _element.setTeacherPreference(_teacherDailyPreference);
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppTeacherPreferenceDailyElementEdit.LECTURE_ROOM_DS.equals(dataSource.getName()))
        {
            dataSource.put("buildingId", _buildingId);
        }
        else if (SppTeacherPreferenceDailyElementEdit.BELL_ENTRY_DS.equals(dataSource.getName()))
        {
            dataSource.put("bellScheduleId", _teacherDailyPreference.getBells().getId());
        }
    }

    public String getPeriodStr()
    {
        return _teacherDailyPreference.getSppScheduleDailySeason().getTitleWithTime();
    }

    public void onChangeLectureRoom()
    {
//        if (null != _element.getLectureRoom())
//            _event.setLectureRoom(_event.getLectureRoomVal().getTitle());
    }

    public void onChangeBuilding()
    {
        _buildingId = _element.getBuilding() != null ? _element.getBuilding().getId() : null;
    }

    public void onChangeDayOfWeek()
    {
        if (null != _dayOfWeekWrapper)
            _element.setDayOfWeek(_dayOfWeekWrapper.getId().intValue());
    }


    public void onClickApply()
    {
        SppScheduleDailySeason season = _element.getTeacherPreference().getSppScheduleDailySeason();
        if (_element.getStartDate().before(season.getStartDate()))
            _uiSupport.error("Событие не может быть раньше чем дата начала периода расписания", "startDate");
        if (_element.getEndDate().after(season.getEndDate()))
            _uiSupport.error("Событие не может быть позже чем дата окончания периода расписания", "startDate");
        if (getUserContext().getErrorCollector().hasErrors())
            return;

        SppTeacherPreferenceManager.instance().dao().createOrUpdateDailyElement(_element);
        //SppScheduleSessionManager.instance().dao().updateScheduleAsChanged(_schedule.getId());
        deactivate();
    }
}
