/* $Id$ */
package ru.tandemservice.unispp.base.bo.SppSchedule.ui.GroupTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.View.SppScheduleView;
import ru.tandemservice.unispp.base.entity.SppSchedule;

/**
 * @author nvankov
 * @since 9/2/13
 */
@Configuration
public class SppScheduleGroupTab extends BusinessComponentManager
{
    public final static String SCHEDULE_DS = "scheduleDS";

    @Bean
    public ColumnListExtPoint scheduleCL()
    {
        return columnListExtPointBuilder(SCHEDULE_DS)
                .addColumn(publisherColumn("title", SppSchedule.title()).businessComponent(SppScheduleView.class).order().required(true))
                .addColumn(dateColumn("startDate", SppSchedule.season().startDate()).order())
                .addColumn(dateColumn("endDate", SppSchedule.season().endDate()).order())
                .addColumn(booleanColumn("approved", SppSchedule.approved()))
                .addColumn(booleanColumn("archived", SppSchedule.archived()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled("ui:entityEditDisabled").permissionKey("sppScheduleGroupTabEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).disabled("ui:entityDeleteDisabled").alert(new FormattedMessage("ui.deleteAlert", SppSchedule.title()))
                                   .permissionKey("sppScheduleGroupTabDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SCHEDULE_DS, scheduleCL(), SppScheduleManager.instance().sppScheduleDSHandler()))
                .create();
    }
}
