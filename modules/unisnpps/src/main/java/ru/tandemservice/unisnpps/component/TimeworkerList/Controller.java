// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerList;

import org.tandemframework.caf.report.ExcelListDataSourcePrinter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisnpps.component.TimeworkerAdd.ITimeworkerAddWizardComponents;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsSupernumeraryPps;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 21.12.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    protected static final String PERSON_FULL_FIO_KEY = "person.fullFio";

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareDataSource(component);
        model.setPrinter(new ExcelListDataSourcePrinter("Почасовики", model.getDataSource()).setupPrintAll());
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        DynamicListDataSource<UnisnppsTimeworker> dataSource = UniBaseUtils.createDataSource(component, getDao());

        new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return ParametersMap
                        .createWith("selectedTab", "timeworkerTab")
                        .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
            }
        };
        AbstractColumn fioColumn = new PublisherColumnBuilder("ФИО", UnisnppsSupernumeraryPps.person().fullFio().s(), "ppsTab").entity(UnisnppsTimeworker.pps().s()).build();
        dataSource.addColumn(fioColumn);
        AbstractColumn sexColumn = new SimpleColumn("Пол", UnisnppsTimeworker.pps().person().identityCard().sex().shortTitle().s()).setOrderable(false).setClickable(false);
        dataSource.addColumn(sexColumn);
        AbstractColumn idcColumn = new SimpleColumn("Паспорт", UnisnppsTimeworker.pps().person().s() + "." + Person.P_FULL_IDCARD_NUMBER).setOrderable(false).setClickable(false).setOrderable(false);
        dataSource.addColumn(idcColumn);
        AbstractColumn scDegreeColumn = new SimpleColumn("Ученые степени", "scDegree", RowCollectionFormatter.INSTANCE).setOrderable(false).setClickable(false);
        dataSource.addColumn(scDegreeColumn);
        AbstractColumn scStatusColumn = new SimpleColumn("Ученые звания", "scStatus", RowCollectionFormatter.INSTANCE).setOrderable(false).setClickable(false);
        dataSource.addColumn(scStatusColumn);
        dataSource.addColumn(new PublisherColumnBuilder("№ договора", UnisnppsTimeworker.number().s(), "timeworkerTab").build());
        dataSource.addColumn(new SimpleColumn("Кол-во часов", UnisnppsTimeworker.fractionalLoad().s(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", UnisnppsTimeworker.orgUnit().fullTitle().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Описание", UnisnppsTimeworker.comment().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата регистрации", UnisnppsTimeworker.regDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        ToggleColumn statusColumn = new ToggleColumn("Статус", UnisnppsTimeworker.archival().s());
        statusColumn.indicator(Boolean.FALSE, new IndicatorColumn.Item("toggled_on", "Почасовик не в архиве", "onClickSwitchArchival", "Списать почасовика «{0}» в архив?", new Object[]{UnisnppsTimeworker.pps().person().fullFio().s()}));
        statusColumn.indicator(Boolean.TRUE, new IndicatorColumn.Item("toggled_off", "Почасовик в архиве", "onClickSwitchArchival", "Восстановить почасовика «{0}» из архива?", new Object[]{UnisnppsTimeworker.pps().person().fullFio().s()}));
        statusColumn.setPermissionKey("changeArchiveTimeworker_list");
        dataSource.addColumn(statusColumn);
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditTimeworker").setPermissionKey("editTimeworker_list").setDisabledProperty(UnisnppsTimeworker.archival().s()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteTimeworker", "Удалить почасовика {0}?", UnisnppsTimeworker.title().s()).setPermissionKey("deleteTimeworker_list"));
        model.setDataSource(dataSource);

        List<AbstractColumn> mergeColumns = new ArrayList<AbstractColumn>();
        mergeColumns.add(fioColumn);
        mergeColumns.add(sexColumn);
        mergeColumns.add(idcColumn);
        mergeColumns.add(scDegreeColumn);
        mergeColumns.add(scStatusColumn);
        model.setMergeColumns(mergeColumns);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getSettings().clear();
        getDao().prepare(model);
        onClickSearch(component);
    }

    public void onClickAddTimeworker(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(ITimeworkerAddWizardComponents.SEARCH_STEP));
        //component.createDefaultChildRegion("dialog", new ComponentActivator(ITimeworkerAddWizardComponents.SEARCH_STEP));
        //component.createDefaultChildRegion(new ComponentActivator(ITimeworkerAddWizardComponents.SEARCH_STEP));
    }

    public void onClickEditTimeworker(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ITimeworkerAddWizardComponents.TIMEWORKER_DATA_STEP, new ParametersMap()
                .add("timeworkerId", (Long) component.getListenerParameter())));
    }

    public void onClickDeleteTimeworker(IBusinessComponent component)
    {
        getDao().delete((Long) component.getListenerParameter());
    }

    public void onClickSwitchArchival(IBusinessComponent component)
    {
        getDao().changeArchival((Long) component.getListenerParameter());
    }

    public void onClickArchive(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.unisnpps.component.TimeworkerArchive", new ParametersMap()
                .add("archival", Boolean.TRUE)));
    }

    public void onClickUnarchive(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.unisnpps.component.TimeworkerArchive", new ParametersMap()
                .add("archival", Boolean.FALSE)));
    }

}