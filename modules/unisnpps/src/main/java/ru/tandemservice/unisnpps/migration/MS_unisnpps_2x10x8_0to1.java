package ru.tandemservice.unisnpps.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.uni.migration.MS_uni_2x10x8_0to1;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unisnpps_2x10x8_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность timeworker4PpsEntry

		// создана новая сущность
		short entityCode;
		{
			// создать таблицу
			DBTable dbt = new DBTable("timeworker4pps_entry_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_timeworker4PpsEntry"),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("ppsentry_id", DBType.LONG).setNullable(false), 
				new DBColumn("timeworker_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			entityCode = tool.entityCodes().ensure("timeworker4PpsEntry");
		}

		// создано обязательное свойство eduYear
		{
			// создать колонку
			tool.createColumn("pps_entry_timeworker_t", new DBColumn("eduyear_id", DBType.LONG));

			// задать значение по умолчанию
			MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update pps_entry_timeworker_t set eduyear_id = ? where id = ?", DBType.LONG, DBType.LONG);

			List<Object[]> rows = tool.executeQuery(
					processor(Long.class, Long.class),
					"select pps_tw.id, tw.eduyear_id from pps_entry_timeworker_t pps_tw inner join snpps_timeworker tw on pps_tw.source_id = tw.id"
			);
			for (final Object[] row : rows)
			{
				Long ppsTwId = (Long) row[0];
				Long eduYearId = (Long) row[1];
				updater.addBatch(eduYearId, ppsTwId);
			}
			updater.executeUpdate(tool);

			// сделать колонку NOT NULL
			tool.setColumnNullable("pps_entry_timeworker_t", "eduyear_id", false);
		}

		////////////////////////////////////////////////////////////////////////////////
		// начинаем мердж

		MS_uni_2x10x8_0to1.mergePpsEntry(tool, entityCode, "pps_entry_timeworker_t", "eduyear_id", "timeworker4pps_entry_t", "timeworker_id");

		////////////////////////////////////////////////////////////////////////////////
		// сущность ppsEntryByTimeworker

		// удалено свойство source
		{
			// удалить колонку
			tool.dropColumn("pps_entry_timeworker_t", "source_id");
		}
		deletePersonRoles(tool);
    }

	private void deletePersonRoles(DBTool tool) throws Exception
	{
		short entityCode = tool.entityCodes().ensure("ppsEntryByTimeworker");

		List<Object[]> rows = tool.executeQuery(
				processor(Long.class),
				"select pr.id, pps.id from personrole_t pr " +
						"left join pps_entry_timeworker_t pps on pps.id = pr.id " +
						"where pr.discriminator = ? and pps.id is null", entityCode
		);
		List<Long> deleteIds = rows.stream().map(row -> (Long) row[0]).collect(Collectors.toList());
		int count = MigrationUtils.massDeleteByIds(tool, "personrole_t", deleteIds);
		tool.info(count + " rows (ppsEntryByTimeworker) removed by personrole_t");
	}
}