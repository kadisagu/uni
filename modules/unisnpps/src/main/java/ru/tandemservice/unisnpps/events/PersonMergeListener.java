// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.sec.entity.Principal;

import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.PersonDao;
import org.tandemframework.shared.person.base.bo.Person.util.IPersonMergeListener;
import ru.tandemservice.uni.dao.UniBaseDao;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsSupernumeraryPps;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

/**
 * @author oleyba
 * @since 17.01.2011
 */
public class PersonMergeListener extends UniBaseDao implements IPersonMergeListener
{
    @Override
    public List<PersonRole> doMerge(Person template, List<Person> duplicates)
    {
        Session session = getSession();

        Person2PrincipalRelation relation = get(Person2PrincipalRelation.class, Person2PrincipalRelation.L_PERSON, template);
        if (relation == null)
            relation = PersonManager.instance().dao().createPersonPrincipalRelation(template);
        Principal principal = relation.getPrincipal();

        List<Person> affectedPersons = new ArrayList<Person>();
        affectedPersons.addAll(duplicates);
        affectedPersons.add(template);

        MQBuilder ppsBuilder = new MQBuilder(UnisnppsSupernumeraryPps.ENTITY_CLASS, "pps");
        ppsBuilder.add(MQExpression.in("pps", UnisnppsSupernumeraryPps.person(), affectedPersons));
        ppsBuilder.addOrder("pps", UnisnppsSupernumeraryPps.regDate());
        List<UnisnppsSupernumeraryPps> duplicatePps = ppsBuilder.getResultList(session);

        if (duplicatePps.isEmpty())
            return Collections.emptyList();

        UnisnppsSupernumeraryPps basePps = duplicatePps.get(0);
        duplicatePps.remove(0);
        basePps.setPerson(template);
        basePps.setPrincipal(principal);
        session.update(basePps);

        if (duplicatePps.isEmpty())
        {
            session.flush();
            return Arrays.asList((PersonRole) basePps);
        }


        MQBuilder timeworkerBuilder = new MQBuilder(UnisnppsTimeworker.ENTITY_CLASS, "tw");
        timeworkerBuilder.add(MQExpression.in("tw", UnisnppsTimeworker.pps().person(), affectedPersons));
        List<UnisnppsTimeworker> requests = timeworkerBuilder.getResultList(getSession());

        for (UnisnppsTimeworker timeworker : requests)
        {
            timeworker.setPps(basePps);
            session.update(timeworker);
        }
        session.flush();

        for (UnisnppsSupernumeraryPps duplicate : duplicatePps)
            session.delete(duplicate);

        ArrayList<PersonRole> processed = new ArrayList<PersonRole>(duplicatePps);
        processed.add(basePps);
        return processed;
    }
}
