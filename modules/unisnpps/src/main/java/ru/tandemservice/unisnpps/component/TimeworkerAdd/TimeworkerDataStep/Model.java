// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerAdd.TimeworkerDataStep;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

/**
 * @author oleyba
 * @since 21.12.2010
 */
@Input({
        @Bind(key = "openPubAfterClose", binding = "openPubAfterClose"),
        @Bind(key = "showScienceBlock", binding = "showScienceBlock"),
        @Bind(key = "basisPersonId", binding = "basisPersonId"),
        @Bind(key = "timeworkerId", binding = "timeworker.id"),
        @Bind(key = "ppsId", binding = "ppsId")})
public class Model
{
    private String componentTitle = "";

    private Long basisPersonId;
    private Long ppsId;
    private UnisnppsTimeworker timeworker = new UnisnppsTimeworker();
    private List<ScienceDegree> scDegreeList;
    private List<ScienceStatus> scStatusList;

    private OrgUnitType orgUnitType;

    private ISelectModel eduYearModel;
    private ISelectModel orgUnitTypeModel;
    private ISelectModel orgUnitModel;
    private ISelectModel scDegreeModel;
    private ISelectModel scStatusModel;

    private boolean openPubAfterClose = false;
    private boolean showScienceBlock = false;

    public boolean isEditForm()
    {
        return null != getTimeworker().getId();
    }

    public String getFormTitle()
    {
        return isEditForm() ? "Редактирование данных почасовика: " + getTimeworker().getPps().getPerson().getFullFio() + " (договор №" + getTimeworker().getNumber() + ")" : "Добавление почасовика";
    }

    public Long getBasisPersonId()
    {
        return basisPersonId;
    }

    public void setBasisPersonId(Long basisPersonId)
    {
        this.basisPersonId = basisPersonId;
    }

    public Long getPpsId()
    {
        return ppsId;
    }

    public void setPpsId(Long ppsId)
    {
        this.ppsId = ppsId;
    }

    public UnisnppsTimeworker getTimeworker()
    {
        return timeworker;
    }

    public void setTimeworker(UnisnppsTimeworker timeworker)
    {
        this.timeworker = timeworker;
    }

    public String getComponentTitle()
    {
        return componentTitle;
    }

    public void setComponentTitle(String componentTitle)
    {
        this.componentTitle = componentTitle;
    }

    public OrgUnitType getOrgUnitType()
    {
        return orgUnitType;
    }

    public void setOrgUnitType(OrgUnitType orgUnitType)
    {
        this.orgUnitType = orgUnitType;
    }

    public ISelectModel getEduYearModel()
    {
        return eduYearModel;
    }

    public void setEduYearModel(ISelectModel eduYearModel)
    {
        this.eduYearModel = eduYearModel;
    }

    public ISelectModel getOrgUnitTypeModel()
    {
        return orgUnitTypeModel;
    }

    public void setOrgUnitTypeModel(ISelectModel orgUnitTypeModel)
    {
        this.orgUnitTypeModel = orgUnitTypeModel;
    }

    public ISelectModel getOrgUnitModel()
    {
        return orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel)
    {
        this.orgUnitModel = orgUnitModel;
    }

    public boolean isOpenPubAfterClose()
    {
        return openPubAfterClose;
    }

    public void setOpenPubAfterClose(boolean openPubAfterClose)
    {
        this.openPubAfterClose = openPubAfterClose;
    }

    public List<ScienceDegree> getScDegreeList()
    {
        return scDegreeList;
    }

    public void setScDegreeList(List<ScienceDegree> scDegreeList)
    {
        this.scDegreeList = scDegreeList;
    }

    public List<ScienceStatus> getScStatusList()
    {
        return scStatusList;
    }

    public void setScStatusList(List<ScienceStatus> scStatusList)
    {
        this.scStatusList = scStatusList;
    }

    public ISelectModel getScDegreeModel()
    {
        return scDegreeModel;
    }

    public void setScDegreeModel(ISelectModel scDegreeModel)
    {
        this.scDegreeModel = scDegreeModel;
    }

    public ISelectModel getScStatusModel()
    {
        return scStatusModel;
    }

    public void setScStatusModel(ISelectModel scStatusModel)
    {
        this.scStatusModel = scStatusModel;
    }

    public boolean isShowScienceBlock()
    {
        return showScienceBlock;
    }

    public void setShowScienceBlock(boolean showScienceBlock)
    {
        this.showScienceBlock = showScienceBlock;
    }
}
