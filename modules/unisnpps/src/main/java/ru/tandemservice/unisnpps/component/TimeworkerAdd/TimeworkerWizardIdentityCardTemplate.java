// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerAdd;

import java.io.Serializable;

import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.uni.dao.UniDaoFacade;

/**
 * @author oleyba
 * @since 24.12.2010
 */
public class TimeworkerWizardIdentityCardTemplate implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String firstName;
    private String lastName;
    private String middleName;
    private String seria;
    private String number;
    private Long type;

    public TimeworkerWizardIdentityCardTemplate(IdentityCard card)
    {
        firstName = card.getFirstName();
        lastName = card.getLastName();
        middleName = card.getMiddleName();
        seria = card.getSeria();
        number = card.getNumber();
        type = card.getCardType().getId();
    }

    public void apply(IdentityCard card)
    {
        card.setLastName(lastName);
        card.setFirstName(firstName);
        card.setMiddleName(middleName);
        card.setSeria(seria);
        card.setNumber(number);
        if (null != type)
            card.setCardType(UniDaoFacade.getCoreDao().get(IdentityCardType.class, type));
    }

}
