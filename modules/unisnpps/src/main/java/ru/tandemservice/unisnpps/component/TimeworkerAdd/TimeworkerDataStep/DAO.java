// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerAdd.TimeworkerDataStep;

import org.hibernate.Session;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsSupernumeraryPps;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 23.12.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setEduYearModel(new EducationYearModel());
        model.getTimeworker().setEduYear(get(EducationYear.class, EducationYear.current().s(), Boolean.TRUE));
        final List<OrgUnitType> activeTypeList = OrgUnitManager.instance().dao().getOrgUnitActiveTypeList();
        model.setOrgUnitTypeModel(new LazySimpleSelectModel<>(activeTypeList));
        model.setOrgUnitModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(final String filter)
            {
                if (model.getOrgUnitType() == null)
                    return ListResult.getEmpty();
                return OrgUnitManager.instance().dao().getOrgUnitList(filter, model.getOrgUnitType(), 50);
            }

            @Override
            public Object getValue(final Object primaryKey)
            {
                final OrgUnit orgUnit = get((Long) primaryKey);
                return orgUnit.getOrgUnitType().equals(model.getOrgUnitType()) ? orgUnit : null;
            }
        });
        model.setScDegreeModel(new LazySimpleSelectModel<>(ScienceDegree.class));
        model.setScStatusModel(new LazySimpleSelectModel<>(ScienceStatus.class));
        OrgUnitType cathedra = get(OrgUnitType.class, OrgUnitType.code().s(), OrgUnitTypeCodes.CATHEDRA);
        if (activeTypeList.contains(cathedra) && model.getOrgUnitType() == null)
            model.setOrgUnitType(cathedra);
        if (null != model.getTimeworker().getId())
        {
            model.setTimeworker(get(UnisnppsTimeworker.class, model.getTimeworker().getId()));
            model.setOrgUnitType(model.getTimeworker().getOrgUnit().getOrgUnitType());
        }
        if (null == model.getTimeworker().getDateFrom())
            model.getTimeworker().setDateFrom(new Date());
        // ученые степени и звания
        if (model.isShowScienceBlock())
        {
            Long personId = model.getBasisPersonId();
            if (null != model.getPpsId())
                personId = get(UnisnppsSupernumeraryPps.class, model.getPpsId()).getPerson().getId();
            if (null != model.getTimeworker().getId())
                personId = model.getTimeworker().getPerson().getId();
            model.setScStatusList(CommonBaseUtil.<ScienceStatus>getPropertiesList(getList(PersonAcademicStatus.class, PersonAcademicStatus.person().id().s(), personId), PersonAcademicStatus.academicStatus().s()));
            model.setScDegreeList(CommonBaseUtil.<ScienceDegree>getPropertiesList(getList(PersonAcademicDegree.class, PersonAcademicDegree.person().id().s(), personId), PersonAcademicDegree.academicDegree().s()));
        }
    }

    @Override
    public String getConfirmMessage(Model model)
    {
        final UnisnppsTimeworker timeworker = model.getTimeworker();

        // ищем ппс
        UnisnppsSupernumeraryPps pps = null;
        if (null != timeworker.getId())
            pps = timeworker.getPps();
        else if (null != model.getPpsId())
            pps = getNotNull(UnisnppsSupernumeraryPps.class, model.getPpsId());

        Session session = getSession();

        if (null == pps)
        {
            Person person = getNotNull(Person.class, model.getBasisPersonId());
            NamedSyncInTransactionCheckLocker.register(session, person.getId() + ".unisnppsPps");

            final List<UnisnppsSupernumeraryPps> ppsList = getList(UnisnppsSupernumeraryPps.class, UnisnppsSupernumeraryPps.L_PERSON, person);
            if (!ppsList.isEmpty())
                pps = ppsList.get(0);
        }

        if (null != pps)
        {
            // проверим, не регистрирует ли пользователь дублирующего почасовика
            MQBuilder builder = new MQBuilder(UnisnppsTimeworker.ENTITY_CLASS, "tw");
            if (null != timeworker.getId())
                builder.add(MQExpression.notEq("tw", "id", timeworker.getId()));
            builder.add(MQExpression.eq("tw", UnisnppsTimeworker.pps(), pps));
            builder.add(MQExpression.eq("tw", UnisnppsTimeworker.eduYear(), timeworker.getEduYear()));
            builder.add(MQExpression.eq("tw", UnisnppsTimeworker.orgUnit(), timeworker.getOrgUnit()));
            builder.add(MQExpression.eq("tw", UnisnppsTimeworker.dateFrom(), timeworker.getDateFrom()));
            builder.add(MQExpression.eq("tw", UnisnppsTimeworker.load(), timeworker.getLoad()));
            List<Object> duplicateList = builder.getResultList(session);
            if (!duplicateList.isEmpty())
                return "Найден уже зарегистрированный почасовик с такими же учебным годом, подразделением, количеством часов и датой начала действия. Зарегистрировать нового с теми же данными?";
        }

        return null;
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getTimeworker().getDateTo() != null && model.getTimeworker().getDateTo().before(model.getTimeworker().getDateFrom()))
            errors.add("Значение даты «Действует по» не может быть меньше значения «Действует с».", "dateFrom", "dateTo");
    }

    @Override
    public void update(Model model)
    {
        final UnisnppsTimeworker timeworker = model.getTimeworker();

        // ищем ппс
        UnisnppsSupernumeraryPps pps = null;
        if (null != timeworker.getId())
            pps = timeworker.getPps();
        else if (null != model.getPpsId())
            pps = getNotNull(UnisnppsSupernumeraryPps.class, model.getPpsId());

        Session session = getSession();

        if (null == pps)
        {
            Person person = getNotNull(Person.class, model.getBasisPersonId());
            NamedSyncInTransactionCheckLocker.register(session, person.getId() + ".unisnppsPps");

            final List<UnisnppsSupernumeraryPps> ppsList = getList(UnisnppsSupernumeraryPps.class, UnisnppsSupernumeraryPps.L_PERSON, person);
            if (!ppsList.isEmpty())
                pps = ppsList.get(0);
                // не нашли - регистрируем нового
            else
            {
                pps = new UnisnppsSupernumeraryPps();
                pps.setPerson(person);
                pps.setRegDate(new Date());
                session.save(pps);
            }
        }

        if (timeworker.getId() == null)
        {
            timeworker.setPps(pps);
            timeworker.setRegDate(new Date());
            while (true)
            {
                UniDaoFacade.getNumberDao().execute("unisnppsTimeworkerNumber", number -> {
                    String numberStr = String.valueOf(number + 1);
                    while (numberStr.length() < 4) numberStr = "0" + numberStr;
                    timeworker.setNumber(numberStr);
                    return number + 1;
                });
                MQBuilder check = new MQBuilder(UnisnppsTimeworker.ENTITY_CLASS, "tw");
                check.add(MQExpression.eq("tw", UnisnppsTimeworker.eduYear(), timeworker.getEduYear()));
                check.add(MQExpression.eq("tw", UnisnppsTimeworker.number(), timeworker.getNumber()));
                if (check.getResultCount(session) == 0) break;
            }
        }
        timeworker.setGeneratedTitle();
        session.saveOrUpdate(timeworker);

        // ученые степени и звания
        if (model.isShowScienceBlock())
        {
            Person person = timeworker.getPps().getPerson();

            Map<ScienceStatus, PersonAcademicStatus> statusMap = new HashMap<>(4);
            for (PersonAcademicStatus rel : getList(PersonAcademicStatus.class, PersonAcademicStatus.person().id().s(), person.getId()))
                statusMap.put(rel.getAcademicStatus(), rel);
            for (ScienceStatus status : model.getScStatusList())
            {
                if (statusMap.containsKey(status))
                {
                    statusMap.remove(status);
                    continue;
                }
                PersonAcademicStatus rel = new PersonAcademicStatus();
                rel.setPerson(person);
                rel.setAcademicStatus(status);
                save(rel);
            }
            for (PersonAcademicStatus rel : statusMap.values())
                delete(rel);

            Map<ScienceDegree, PersonAcademicDegree> degreeMap = new HashMap<>(4);
            for (PersonAcademicDegree rel : getList(PersonAcademicDegree.class, PersonAcademicDegree.person().id().s(), person.getId()))
                degreeMap.put(rel.getAcademicDegree(), rel);
            for (ScienceDegree degree : model.getScDegreeList())
            {
                if (degreeMap.containsKey(degree))
                {
                    degreeMap.remove(degree);
                    continue;
                }
                PersonAcademicDegree rel = new PersonAcademicDegree();
                rel.setPerson(person);
                rel.setAcademicDegree(degree);
                save(rel);
            }
            for (PersonAcademicDegree rel : degreeMap.values())
                delete(rel);
        }
    }

}

