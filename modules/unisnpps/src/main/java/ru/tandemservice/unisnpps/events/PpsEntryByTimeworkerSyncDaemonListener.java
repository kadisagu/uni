// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.events;

import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.event.single.ISingleEntityEvent;
import org.tandemframework.hibsupport.event.single.listener.IHibernateEventListener;

import org.tandemframework.hibsupport.transaction.sync.TransactionCompleteAction;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsSupernumeraryPps;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

/**
 * @author oleyba
 * @since 18.01.2011
 */
public class PpsEntryByTimeworkerSyncDaemonListener implements IHibernateEventListener<ISingleEntityEvent>
{

    private static final TransactionCompleteAction<Void> action = new TransactionCompleteAction<Void>()
    {
        @Override
        public void afterCompletion(Session session, int status, Void beforeCompleteResult)
        {
            PpsEntryByTimeworkerSynchronizer.DAEMON.wakeUpDaemon();
        }
    };

    @Override
    public void onEvent(ISingleEntityEvent event)
    {
        if (check(event))
            action.register(event.getSession());
    }

    private boolean check(ISingleEntityEvent event)
    {
        final IEntity entity = event.getEntity();
        return ((entity instanceof UnisnppsTimeworker) || (entity instanceof UnisnppsSupernumeraryPps));
    }

}
