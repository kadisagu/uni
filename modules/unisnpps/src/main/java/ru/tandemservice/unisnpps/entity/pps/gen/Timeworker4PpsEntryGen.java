package ru.tandemservice.unisnpps.entity.pps.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker;
import ru.tandemservice.unisnpps.entity.pps.Timeworker4PpsEntry;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Почасовик для записи в реестре ППС
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Timeworker4PpsEntryGen extends EntityBase
 implements INaturalIdentifiable<Timeworker4PpsEntryGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisnpps.entity.pps.Timeworker4PpsEntry";
    public static final String ENTITY_NAME = "timeworker4PpsEntry";
    public static final int VERSION_HASH = 2014509387;
    private static IEntityMeta ENTITY_META;

    public static final String L_PPS_ENTRY = "ppsEntry";
    public static final String L_TIMEWORKER = "timeworker";
    public static final String P_REMOVAL_DATE = "removalDate";

    private PpsEntryByTimeworker _ppsEntry;     // Запись в реестре ППС (на базе почасовика)
    private UnisnppsTimeworker _timeworker;     // Почасовик
    private Date _removalDate;     // Дата утраты актуальности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись в реестре ППС (на базе почасовика). Свойство не может быть null.
     */
    @NotNull
    public PpsEntryByTimeworker getPpsEntry()
    {
        return _ppsEntry;
    }

    /**
     * @param ppsEntry Запись в реестре ППС (на базе почасовика). Свойство не может быть null.
     */
    public void setPpsEntry(PpsEntryByTimeworker ppsEntry)
    {
        dirty(_ppsEntry, ppsEntry);
        _ppsEntry = ppsEntry;
    }

    /**
     * @return Почасовик. Свойство не может быть null.
     */
    @NotNull
    public UnisnppsTimeworker getTimeworker()
    {
        return _timeworker;
    }

    /**
     * @param timeworker Почасовик. Свойство не может быть null.
     */
    public void setTimeworker(UnisnppsTimeworker timeworker)
    {
        dirty(_timeworker, timeworker);
        _timeworker = timeworker;
    }

    /**
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Timeworker4PpsEntryGen)
        {
            if (withNaturalIdProperties)
            {
                setPpsEntry(((Timeworker4PpsEntry)another).getPpsEntry());
                setTimeworker(((Timeworker4PpsEntry)another).getTimeworker());
            }
            setRemovalDate(((Timeworker4PpsEntry)another).getRemovalDate());
        }
    }

    public INaturalId<Timeworker4PpsEntryGen> getNaturalId()
    {
        return new NaturalId(getPpsEntry(), getTimeworker());
    }

    public static class NaturalId extends NaturalIdBase<Timeworker4PpsEntryGen>
    {
        private static final String PROXY_NAME = "Timeworker4PpsEntryNaturalProxy";

        private Long _ppsEntry;
        private Long _timeworker;

        public NaturalId()
        {}

        public NaturalId(PpsEntryByTimeworker ppsEntry, UnisnppsTimeworker timeworker)
        {
            _ppsEntry = ((IEntity) ppsEntry).getId();
            _timeworker = ((IEntity) timeworker).getId();
        }

        public Long getPpsEntry()
        {
            return _ppsEntry;
        }

        public void setPpsEntry(Long ppsEntry)
        {
            _ppsEntry = ppsEntry;
        }

        public Long getTimeworker()
        {
            return _timeworker;
        }

        public void setTimeworker(Long timeworker)
        {
            _timeworker = timeworker;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof Timeworker4PpsEntryGen.NaturalId) ) return false;

            Timeworker4PpsEntryGen.NaturalId that = (NaturalId) o;

            if( !equals(getPpsEntry(), that.getPpsEntry()) ) return false;
            if( !equals(getTimeworker(), that.getTimeworker()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPpsEntry());
            result = hashCode(result, getTimeworker());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPpsEntry());
            sb.append("/");
            sb.append(getTimeworker());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Timeworker4PpsEntryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Timeworker4PpsEntry.class;
        }

        public T newInstance()
        {
            return (T) new Timeworker4PpsEntry();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "ppsEntry":
                    return obj.getPpsEntry();
                case "timeworker":
                    return obj.getTimeworker();
                case "removalDate":
                    return obj.getRemovalDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "ppsEntry":
                    obj.setPpsEntry((PpsEntryByTimeworker) value);
                    return;
                case "timeworker":
                    obj.setTimeworker((UnisnppsTimeworker) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "ppsEntry":
                        return true;
                case "timeworker":
                        return true;
                case "removalDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "ppsEntry":
                    return true;
                case "timeworker":
                    return true;
                case "removalDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "ppsEntry":
                    return PpsEntryByTimeworker.class;
                case "timeworker":
                    return UnisnppsTimeworker.class;
                case "removalDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Timeworker4PpsEntry> _dslPath = new Path<Timeworker4PpsEntry>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Timeworker4PpsEntry");
    }
            

    /**
     * @return Запись в реестре ППС (на базе почасовика). Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.Timeworker4PpsEntry#getPpsEntry()
     */
    public static PpsEntryByTimeworker.Path<PpsEntryByTimeworker> ppsEntry()
    {
        return _dslPath.ppsEntry();
    }

    /**
     * @return Почасовик. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.Timeworker4PpsEntry#getTimeworker()
     */
    public static UnisnppsTimeworker.Path<UnisnppsTimeworker> timeworker()
    {
        return _dslPath.timeworker();
    }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.unisnpps.entity.pps.Timeworker4PpsEntry#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    public static class Path<E extends Timeworker4PpsEntry> extends EntityPath<E>
    {
        private PpsEntryByTimeworker.Path<PpsEntryByTimeworker> _ppsEntry;
        private UnisnppsTimeworker.Path<UnisnppsTimeworker> _timeworker;
        private PropertyPath<Date> _removalDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись в реестре ППС (на базе почасовика). Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.Timeworker4PpsEntry#getPpsEntry()
     */
        public PpsEntryByTimeworker.Path<PpsEntryByTimeworker> ppsEntry()
        {
            if(_ppsEntry == null )
                _ppsEntry = new PpsEntryByTimeworker.Path<PpsEntryByTimeworker>(L_PPS_ENTRY, this);
            return _ppsEntry;
        }

    /**
     * @return Почасовик. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.Timeworker4PpsEntry#getTimeworker()
     */
        public UnisnppsTimeworker.Path<UnisnppsTimeworker> timeworker()
        {
            if(_timeworker == null )
                _timeworker = new UnisnppsTimeworker.Path<UnisnppsTimeworker>(L_TIMEWORKER, this);
            return _timeworker;
        }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.unisnpps.entity.pps.Timeworker4PpsEntry#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(Timeworker4PpsEntryGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

        public Class getEntityClass()
        {
            return Timeworker4PpsEntry.class;
        }

        public String getEntityName()
        {
            return "timeworker4PpsEntry";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
