package ru.tandemservice.unisnpps.entity.pps.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsSupernumeraryPps;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ППС по договору подряда
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisnppsSupernumeraryPpsGen extends PersonRole
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisnpps.entity.pps.UnisnppsSupernumeraryPps";
    public static final String ENTITY_NAME = "unisnppsSupernumeraryPps";
    public static final int VERSION_HASH = 1274437191;
    private static IEntityMeta ENTITY_META;

    public static final String P_REG_DATE = "regDate";

    private Date _regDate;     // Дата регистрации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата регистрации. Свойство не может быть null.
     */
    @NotNull
    public Date getRegDate()
    {
        return _regDate;
    }

    /**
     * @param regDate Дата регистрации. Свойство не может быть null.
     */
    public void setRegDate(Date regDate)
    {
        dirty(_regDate, regDate);
        _regDate = regDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UnisnppsSupernumeraryPpsGen)
        {
            setRegDate(((UnisnppsSupernumeraryPps)another).getRegDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisnppsSupernumeraryPpsGen> extends PersonRole.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisnppsSupernumeraryPps.class;
        }

        public T newInstance()
        {
            return (T) new UnisnppsSupernumeraryPps();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "regDate":
                    return obj.getRegDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "regDate":
                    obj.setRegDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "regDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "regDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "regDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UnisnppsSupernumeraryPps> _dslPath = new Path<UnisnppsSupernumeraryPps>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisnppsSupernumeraryPps");
    }
            

    /**
     * @return Дата регистрации. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsSupernumeraryPps#getRegDate()
     */
    public static PropertyPath<Date> regDate()
    {
        return _dslPath.regDate();
    }

    public static class Path<E extends UnisnppsSupernumeraryPps> extends PersonRole.Path<E>
    {
        private PropertyPath<Date> _regDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата регистрации. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsSupernumeraryPps#getRegDate()
     */
        public PropertyPath<Date> regDate()
        {
            if(_regDate == null )
                _regDate = new PropertyPath<Date>(UnisnppsSupernumeraryPpsGen.P_REG_DATE, this);
            return _regDate;
        }

        public Class getEntityClass()
        {
            return UnisnppsSupernumeraryPps.class;
        }

        public String getEntityName()
        {
            return "unisnppsSupernumeraryPps";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
