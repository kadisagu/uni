// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerArchive;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

/**
 * @author oleyba
 * @since 1/28/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEduYearModel(new EducationYearModel());
    }

    @Override
    public void update(Model model)
    {
        DQLUpdateBuilder upd = new DQLUpdateBuilder(UnisnppsTimeworker.class);
        upd.set(UnisnppsTimeworker.archival().s(), DQLExpressions.value(model.isArchival()));
        upd.where(DQLExpressions.eq(DQLExpressions.property(UnisnppsTimeworker.archival().s()), DQLExpressions.value(!model.isArchival())));
        upd.where(DQLExpressions.eq(DQLExpressions.property(UnisnppsTimeworker.eduYear().s()), DQLExpressions.value(model.getYear())));
        upd.createStatement(getSession()).execute();
    }
}
