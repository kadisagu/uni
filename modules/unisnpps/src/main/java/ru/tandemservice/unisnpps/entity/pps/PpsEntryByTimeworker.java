package ru.tandemservice.unisnpps.entity.pps;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisnpps.entity.pps.gen.PpsEntryByTimeworkerGen;

/**
 * Запись в реестре ППС (на базе почасовика)
 */
public class PpsEntryByTimeworker extends PpsEntryByTimeworkerGen
{
    public PpsEntryByTimeworker() {}

    public PpsEntryByTimeworker(Person person, OrgUnit orgUnit, EducationYear eduYear)
    {
        setPerson(person);
        setOrgUnit(orgUnit);
        setEduYear(eduYear);
    }

    @Override
    @EntityDSLSupport(parts = PpsEntryByTimeworker.P_TIME_AMOUNT_AS_LONG)
    public double getTimeAmount()
    {
        return 0.01d * getTimeAmountAsLong();
    }

    @Override
    public String getFullTitle()
    {
        return "Почасовик: " + getPerson().getFullFio();
    }

    @Override
    public String getContextTitle()
    {
        return "Запись ППС на основе почасовика: " + getTitleInfo();
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1048613")
    public String getExtendedTitle()
    {
        return getPerson().getFullFio() + " (" + getTitleInfo() + ")";
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1048613")
    public String getTitle()
    {
        if (null == getPerson()) {
            return this.getClass().getSimpleName();
        }
        return getPerson().getFio() + " (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getTimeAmount()) + " ч., " + getOrgUnit().getShortTitle() + ")";
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1048613")
    public String getShortTitle() {
        return getPerson().getFio();
    }

    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=1048613")
    public String getTitleInfo()
    {
        return getEduYear().getTitle() + ", " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getTimeAmount()) + " ч., " + getOrgUnit().getShortTitle();
    }

    /**
     * @return строка с информацией о почасовике
     * [уч. год], №[номер договора], [число часов] ч.
     */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=33065812")
    public String getTitlePostOrTimeWorkerData()
    {
        return getTitleInfo();
    }
}