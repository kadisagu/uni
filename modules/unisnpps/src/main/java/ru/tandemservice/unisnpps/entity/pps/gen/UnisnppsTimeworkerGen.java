package ru.tandemservice.unisnpps.entity.pps.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsSupernumeraryPps;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Почасовик
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisnppsTimeworkerGen extends EntityBase
 implements IPersistentPersonable{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker";
    public static final String ENTITY_NAME = "unisnppsTimeworker";
    public static final int VERSION_HASH = 2010058658;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON = "person";
    public static final String L_PPS = "pps";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_EDU_YEAR = "eduYear";
    public static final String P_REG_DATE = "regDate";
    public static final String P_LOAD = "load";
    public static final String P_ARCHIVAL = "archival";
    public static final String P_NUMBER = "number";
    public static final String P_TITLE = "title";
    public static final String P_COMMENT = "comment";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_FRACTIONAL_LOAD = "fractionalLoad";

    private Person _person;     // Персона
    private UnisnppsSupernumeraryPps _pps;     // ППС по договору подряда
    private OrgUnit _orgUnit;     // Подразделение
    private EducationYear _eduYear;     // Учебный год
    private Date _regDate;     // Дата регистрации
    private long _load;     // Число часов
    private boolean _archival; 
    private String _number;     // Номер договора
    private String _title;     // Название договора
    private String _comment;     // Текстовое описание договора
    private Date _dateFrom;     // Действует с
    private Date _dateTo;     // Действует по

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона.
     *
     * Это формула "pps.person".
     */
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Персона.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return ППС по договору подряда. Свойство не может быть null.
     */
    @NotNull
    public UnisnppsSupernumeraryPps getPps()
    {
        return _pps;
    }

    /**
     * @param pps ППС по договору подряда. Свойство не может быть null.
     */
    public void setPps(UnisnppsSupernumeraryPps pps)
    {
        dirty(_pps, pps);
        _pps = pps;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год. Свойство не может быть null.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    /**
     * @return Дата регистрации. Свойство не может быть null.
     */
    @NotNull
    public Date getRegDate()
    {
        return _regDate;
    }

    /**
     * @param regDate Дата регистрации. Свойство не может быть null.
     */
    public void setRegDate(Date regDate)
    {
        dirty(_regDate, regDate);
        _regDate = regDate;
    }

    /**
     * @return Число часов. Свойство не может быть null.
     */
    @NotNull
    public long getLoad()
    {
        return _load;
    }

    /**
     * @param load Число часов. Свойство не может быть null.
     */
    public void setLoad(long load)
    {
        dirty(_load, load);
        _load = load;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isArchival()
    {
        return _archival;
    }

    /**
     * @param archival  Свойство не может быть null.
     */
    public void setArchival(boolean archival)
    {
        dirty(_archival, archival);
        _archival = archival;
    }

    /**
     * @return Номер договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер договора. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Название договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название договора. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Текстовое описание договора.
     */
    @Length(max=4000)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Текстовое описание договора.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Действует с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Действует с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Действует по.
     */
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Действует по.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UnisnppsTimeworkerGen)
        {
            setPerson(((UnisnppsTimeworker)another).getPerson());
            setPps(((UnisnppsTimeworker)another).getPps());
            setOrgUnit(((UnisnppsTimeworker)another).getOrgUnit());
            setEduYear(((UnisnppsTimeworker)another).getEduYear());
            setRegDate(((UnisnppsTimeworker)another).getRegDate());
            setLoad(((UnisnppsTimeworker)another).getLoad());
            setArchival(((UnisnppsTimeworker)another).isArchival());
            setNumber(((UnisnppsTimeworker)another).getNumber());
            setTitle(((UnisnppsTimeworker)another).getTitle());
            setComment(((UnisnppsTimeworker)another).getComment());
            setDateFrom(((UnisnppsTimeworker)another).getDateFrom());
            setDateTo(((UnisnppsTimeworker)another).getDateTo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisnppsTimeworkerGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisnppsTimeworker.class;
        }

        public T newInstance()
        {
            return (T) new UnisnppsTimeworker();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "person":
                    return obj.getPerson();
                case "pps":
                    return obj.getPps();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "eduYear":
                    return obj.getEduYear();
                case "regDate":
                    return obj.getRegDate();
                case "load":
                    return obj.getLoad();
                case "archival":
                    return obj.isArchival();
                case "number":
                    return obj.getNumber();
                case "title":
                    return obj.getTitle();
                case "comment":
                    return obj.getComment();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "pps":
                    obj.setPps((UnisnppsSupernumeraryPps) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
                case "regDate":
                    obj.setRegDate((Date) value);
                    return;
                case "load":
                    obj.setLoad((Long) value);
                    return;
                case "archival":
                    obj.setArchival((Boolean) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "person":
                        return true;
                case "pps":
                        return true;
                case "orgUnit":
                        return true;
                case "eduYear":
                        return true;
                case "regDate":
                        return true;
                case "load":
                        return true;
                case "archival":
                        return true;
                case "number":
                        return true;
                case "title":
                        return true;
                case "comment":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "person":
                    return true;
                case "pps":
                    return true;
                case "orgUnit":
                    return true;
                case "eduYear":
                    return true;
                case "regDate":
                    return true;
                case "load":
                    return true;
                case "archival":
                    return true;
                case "number":
                    return true;
                case "title":
                    return true;
                case "comment":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "person":
                    return Person.class;
                case "pps":
                    return UnisnppsSupernumeraryPps.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "eduYear":
                    return EducationYear.class;
                case "regDate":
                    return Date.class;
                case "load":
                    return Long.class;
                case "archival":
                    return Boolean.class;
                case "number":
                    return String.class;
                case "title":
                    return String.class;
                case "comment":
                    return String.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UnisnppsTimeworker> _dslPath = new Path<UnisnppsTimeworker>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisnppsTimeworker");
    }
            

    /**
     * @return Персона.
     *
     * Это формула "pps.person".
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return ППС по договору подряда. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getPps()
     */
    public static UnisnppsSupernumeraryPps.Path<UnisnppsSupernumeraryPps> pps()
    {
        return _dslPath.pps();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    /**
     * @return Дата регистрации. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getRegDate()
     */
    public static PropertyPath<Date> regDate()
    {
        return _dslPath.regDate();
    }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getLoad()
     */
    public static PropertyPath<Long> load()
    {
        return _dslPath.load();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#isArchival()
     */
    public static PropertyPath<Boolean> archival()
    {
        return _dslPath.archival();
    }

    /**
     * @return Номер договора. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Название договора. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Текстовое описание договора.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Действует с. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Действует по.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getFractionalLoad()
     */
    public static SupportedPropertyPath<Double> fractionalLoad()
    {
        return _dslPath.fractionalLoad();
    }

    public static class Path<E extends UnisnppsTimeworker> extends EntityPath<E>
    {
        private Person.Path<Person> _person;
        private UnisnppsSupernumeraryPps.Path<UnisnppsSupernumeraryPps> _pps;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private EducationYear.Path<EducationYear> _eduYear;
        private PropertyPath<Date> _regDate;
        private PropertyPath<Long> _load;
        private PropertyPath<Boolean> _archival;
        private PropertyPath<String> _number;
        private PropertyPath<String> _title;
        private PropertyPath<String> _comment;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private SupportedPropertyPath<Double> _fractionalLoad;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона.
     *
     * Это формула "pps.person".
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return ППС по договору подряда. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getPps()
     */
        public UnisnppsSupernumeraryPps.Path<UnisnppsSupernumeraryPps> pps()
        {
            if(_pps == null )
                _pps = new UnisnppsSupernumeraryPps.Path<UnisnppsSupernumeraryPps>(L_PPS, this);
            return _pps;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

    /**
     * @return Дата регистрации. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getRegDate()
     */
        public PropertyPath<Date> regDate()
        {
            if(_regDate == null )
                _regDate = new PropertyPath<Date>(UnisnppsTimeworkerGen.P_REG_DATE, this);
            return _regDate;
        }

    /**
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getLoad()
     */
        public PropertyPath<Long> load()
        {
            if(_load == null )
                _load = new PropertyPath<Long>(UnisnppsTimeworkerGen.P_LOAD, this);
            return _load;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#isArchival()
     */
        public PropertyPath<Boolean> archival()
        {
            if(_archival == null )
                _archival = new PropertyPath<Boolean>(UnisnppsTimeworkerGen.P_ARCHIVAL, this);
            return _archival;
        }

    /**
     * @return Номер договора. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(UnisnppsTimeworkerGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Название договора. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UnisnppsTimeworkerGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Текстовое описание договора.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(UnisnppsTimeworkerGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Действует с. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(UnisnppsTimeworkerGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Действует по.
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(UnisnppsTimeworkerGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker#getFractionalLoad()
     */
        public SupportedPropertyPath<Double> fractionalLoad()
        {
            if(_fractionalLoad == null )
                _fractionalLoad = new SupportedPropertyPath<Double>(UnisnppsTimeworkerGen.P_FRACTIONAL_LOAD, this);
            return _fractionalLoad;
        }

        public Class getEntityClass()
        {
            return UnisnppsTimeworker.class;
        }

        public String getEntityName()
        {
            return "unisnppsTimeworker";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract double getFractionalLoad();
}
