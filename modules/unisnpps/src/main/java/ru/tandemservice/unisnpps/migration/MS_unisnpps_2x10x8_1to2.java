package ru.tandemservice.unisnpps.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unisnpps_2x10x8_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность ppsEntryByTimeworker

		// создано обязательное свойство timeAmountAsLong
		{
			// создать колонку
			tool.createColumn("pps_entry_timeworker_t", new DBColumn("timeamountaslong_p", DBType.LONG));

			// задать значение по умолчанию
			tool.executeUpdate("update pps_entry_timeworker_t set timeamountaslong_p=? where timeamountaslong_p is null", 0L);

			// сделать колонку NOT NULL
			tool.setColumnNullable("pps_entry_timeworker_t", "timeamountaslong_p", false);
		}
    }
}