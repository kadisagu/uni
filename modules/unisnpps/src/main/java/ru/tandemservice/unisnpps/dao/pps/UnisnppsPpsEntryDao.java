/* $Id:$ */
package ru.tandemservice.unisnpps.dao.pps;

import com.google.common.collect.Maps;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.utils.DQLSimple;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.pps.PpsEntryDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker;
import ru.tandemservice.unisnpps.entity.pps.Timeworker4PpsEntry;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 26.10.2016
 */
public class UnisnppsPpsEntryDao extends PpsEntryDao
{
    @Override
    public Map<PpsEntry, List<IEntity>> getPpsEntry2TimeWorkerMap(List<Long> ppsEntryIds)
    {
        Map<PpsEntry, List<IEntity>> resultMap = Maps.newHashMap();

        new DQLSimple<>(Timeworker4PpsEntry.class)
                .where(Timeworker4PpsEntry.ppsEntry().id(), ppsEntryIds)
                .where(Timeworker4PpsEntry.removalDate(), null)
                .order(Timeworker4PpsEntry.timeworker().title())
                .list().forEach(item -> SafeMap.safeGet(resultMap, item.getPpsEntry(), LinkedList.class).add(item.getTimeworker()));

        return resultMap;
    }

    @Override
    public DQLSelectBuilder getPpsEntryByTimeworkerBuilder(String alias, OrgUnit orgUnit, EducationYear eduYear)
    {
        return new DQLSelectBuilder()
                .fromEntity(PpsEntryByTimeworker.class, alias)
                .where(eq(property(alias, PpsEntryByTimeworker.orgUnit()), value(orgUnit)))
                .where(eq(property(alias, PpsEntryByTimeworker.eduYear()), value(eduYear)))
                .where(isNull(property(alias, PpsEntryByTimeworker.removalDate())));
    }
}
