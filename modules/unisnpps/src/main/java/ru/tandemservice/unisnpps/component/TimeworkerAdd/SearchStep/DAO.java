// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerAdd.SearchStep;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.Person.ui.SimilarAdd.PersonRoleParameter;
import org.tandemframework.shared.person.base.bo.Person.util.CongenialStringFinder;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.uni.UniDefines;

import java.util.List;

/**
 * @author oleyba
 * @since 20.12.2010
 */
public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.SimilarAdd.DAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setIdentityCardTypeList(CatalogManager.instance().dao().getCatalogItemListOrderByCode(IdentityCardType.class));
        model.getIdentityCard().setCardType(getByCode(IdentityCardType.class, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT));
    }

    @Override
    public IdentityCard findDuplicate(IdentityCard identityCard)
    {
        String[] properties = new String[]{IdentityCard.P_LAST_NAME, IdentityCard.P_FIRST_NAME, IdentityCard.P_SERIA, IdentityCard.P_NUMBER};
        List<CongenialStringFinder.SearchElement> identityCards = CongenialStringFinder.getCongenialIdentityCards(identityCard, properties, 0, getSession());
        return (!identityCards.isEmpty()) ? get(IdentityCard.class, identityCards.get(0).getCardId()) : null;
    }

    @Override
    public List<CongenialStringFinder.SearchElement> findSimilarPersons(IdentityCard identityCard)
    {
        String[] properties = new String[]{IdentityCard.P_LAST_NAME, IdentityCard.P_FIRST_NAME, IdentityCard.P_SERIA, IdentityCard.P_NUMBER};
        return CongenialStringFinder.getCongenialIdentityCards(identityCard, properties, 3, getSession());
    }

    @Override
    public void prepareListDataSource(Model model, IDataSettings settings)
    {
        super.prepareListDataSource(model, settings);
        DynamicListDataSource<PersonRoleParameter> dataSource = model.getDataSource();
        if (dataSource.getSelectedEntities() != null && dataSource.getSelectedEntities().isEmpty() && model.getDuplicate() != null)
            if (!dataSource.getEntityList().isEmpty())
            {
                PersonRoleParameter first = dataSource.getEntityList().get(0);
                RadioButtonColumn column = (RadioButtonColumn) dataSource.getColumn("radio");
                column.setSelectedEntity(first);
                dataSource.getSelectedEntities().add(first);
            }
    }
}
