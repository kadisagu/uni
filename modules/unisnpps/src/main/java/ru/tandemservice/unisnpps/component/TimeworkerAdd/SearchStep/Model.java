// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerAdd.SearchStep;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.tapestry.form.validator.BaseValidator;

import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author oleyba
 * @since 20.12.2010
 */

public class Model extends org.tandemframework.shared.person.base.bo.Person.ui.SimilarAdd.Model
{
    private IdentityCard _identityCard = new IdentityCard();
    private List<IdentityCardType> _identityCardTypeList;

    private boolean searchCompleted = false;
    private boolean initialised = false;

    private IdentityCard duplicate;

    public boolean isNothingFound()
    {
        return CollectionUtils.isEmpty(getPersons()) && duplicate == null;
    }

    public boolean isDuplicateFound()
    {
        return duplicate != null;
    }

    public boolean isSimilarPersonsFound()
    {
        return !CollectionUtils.isEmpty(getPersons());
    }

    public IdentityCard getIdentityCard()
    {
        return _identityCard;
    }

    public void setIdentityCard(IdentityCard identityCard)
    {
        _identityCard = identityCard;
    }

    public List<IdentityCardType> getIdentityCardTypeList()
    {
        return _identityCardTypeList;
    }

    public void setIdentityCardTypeList(List<IdentityCardType> identityCardTypeList)
    {
        _identityCardTypeList = identityCardTypeList;
    }

    public boolean isSearchCompleted()
    {
        return searchCompleted;
    }

    public void setSearchCompleted(boolean searchCompleted)
    {
        this.searchCompleted = searchCompleted;
    }

    public IdentityCard getDuplicate()
    {
        return duplicate;
    }

    public void setDuplicate(IdentityCard duplicate)
    {
        this.duplicate = duplicate;
    }

    public boolean isInitialised()
    {
        return initialised;
    }

    public void setInitialised(boolean initialised)
    {
        this.initialised = initialised;
    }
}
