// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerList;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.codes.OrgUnitTypeCodes;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsSupernumeraryPps;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

import java.util.*;

/**
 * @author oleyba
 * @since 21.12.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEduYearModel(new EducationYearModel());
        model.setArchivalModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(0L, "Показать архивных"),
                new IdentifiableWrapper(1L, "Показать не архивных")
        )));
        List<OrgUnit> orgUnits = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou").add(MQExpression.notEq("ou", OrgUnit.orgUnitType().code().s(), OrgUnitTypeCodes.TOP)).<OrgUnit>getResultList(getSession());
        model.setOrgUnitModel(new LazySimpleSelectModel<>(orgUnits, "fullTitle").setSearchFromStart(false).setSearchProperty("fullTitle"));
        if (model.getSettings().get("eduYear") == null)
            model.getSettings().set("eduYear", get(EducationYear.class, EducationYear.current().s(), Boolean.TRUE));
        model.setScDegreeModel(new LazySimpleSelectModel<>(ScienceDegree.class));
        model.setScStatusModel(new LazySimpleSelectModel<>(ScienceStatus.class));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        Object eduYear = model.getSettings().get("eduYear");
        String lastName = model.getSettings().get("lastName");
        String firstName = model.getSettings().get("firstName");
        String middleName = model.getSettings().get("middleName");
        Collection scDegree = model.getSettings().get("scDegree");
        Collection scStatus = model.getSettings().get("scStatus");
        String number = model.getSettings().get("number");
        Object orgUnit = model.getSettings().get("orgUnit");
        Date registeredFrom = model.getSettings().get("registeredFrom");
        Date registeredTo = model.getSettings().get("registeredTo");
        Object archival = model.getSettings().get("archival");

        MQBuilder builder = new MQBuilder(UnisnppsTimeworker.ENTITY_CLASS, "tw");
        builder.addJoinFetch("tw", UnisnppsTimeworker.pps(), "pps");
        builder.addJoinFetch("pps", UnisnppsSupernumeraryPps.person(), "person");
        builder.addJoinFetch("person", Person.identityCard(), "idc");
        FilterUtils.applySelectFilter(builder, "tw", UnisnppsTimeworker.eduYear().s(), eduYear);
        FilterUtils.applySimpleLikeFilter(builder, "tw", UnisnppsTimeworker.pps().person().identityCard().lastName().s(), lastName);
        FilterUtils.applySimpleLikeFilter(builder, "tw", UnisnppsTimeworker.pps().person().identityCard().firstName().s(), firstName);
        FilterUtils.applySimpleLikeFilter(builder, "tw", UnisnppsTimeworker.pps().person().identityCard().middleName().s(), middleName);
        if (!CollectionUtils.isEmpty(scDegree))
        {
            MQBuilder sub = new MQBuilder(PersonAcademicDegree.ENTITY_CLASS, "pad");
            sub.add(MQExpression.in("pad", PersonAcademicDegree.academicDegree(), scDegree));
            sub.add(MQExpression.eqProperty("pad", PersonAcademicDegree.person().id().s(), "person", "id"));
            builder.add(MQExpression.exists(sub));
        }
        if (!CollectionUtils.isEmpty(scStatus))
        {
            MQBuilder sub = new MQBuilder(PersonAcademicStatus.ENTITY_CLASS, "pas");
            sub.add(MQExpression.in("pas", PersonAcademicStatus.academicStatus(), scStatus));
            sub.add(MQExpression.eqProperty("pas", PersonAcademicStatus.person().id().s(), "person", "id"));
            builder.add(MQExpression.exists(sub));
        }
        FilterUtils.applySimpleLikeFilter(builder, "tw", UnisnppsTimeworker.number().s(), number);
        FilterUtils.applySelectFilter(builder, "tw", UnisnppsTimeworker.orgUnit().s(), orgUnit);
        if (null != archival)
            builder.add(MQExpression.eq("tw", UnisnppsTimeworker.archival(), 0L == ((IEntity) archival).getId()));
        if (null != registeredFrom)
            builder.add(UniMQExpression.greatOrEq("tw", UnisnppsTimeworker.regDate().s(), registeredFrom));
        if (null != registeredTo)
            builder.add(UniMQExpression.lessOrEq("tw", UnisnppsTimeworker.regDate().s(), registeredTo));
        OrderDescriptionRegistry reg = new OrderDescriptionRegistry("tw");
        reg.setOrders(Controller.PERSON_FULL_FIO_KEY, new OrderDescription("idc", IdentityCard.lastName()), new OrderDescription("idc", IdentityCard.firstName()), new OrderDescription("idc", IdentityCard.middleName()));
        DynamicListDataSource<UnisnppsTimeworker> dataSource = model.getDataSource();
        reg.applyOrder(builder, dataSource.getEntityOrder());
        if (dataSource.getEntityOrder().getKey().equals(Controller.PERSON_FULL_FIO_KEY))
        {
            builder.addOrder("tw", UnisnppsTimeworker.number());

            IMergeRowIdResolver mergeRowIdResolver = entity -> entity.getProperty(UnisnppsTimeworker.pps().id().s()).toString();
            for (AbstractColumn column : model.getMergeColumns())
                column.setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver);
        }
        else
        {
            for (AbstractColumn column : model.getMergeColumns())
                column.setMergeRows(false);
        }
        UniBaseUtils.createPage(dataSource, builder, getSession());

        List<Person> persons = CommonBaseUtil.getPropertiesList(dataSource.getEntityList(), UnisnppsTimeworker.pps().person().s());
        Map<Person, List<ScienceDegree>> degreeMap = new HashMap<>();
        for (PersonAcademicDegree rel : getList(PersonAcademicDegree.class, PersonAcademicDegree.person(), persons))
            SafeMap.safeGet(degreeMap, rel.getPerson(), ArrayList.class).add(rel.getAcademicDegree());
        Map<Person, List<ScienceStatus>> statusMap = new HashMap<>();
        for (PersonAcademicStatus rel : getList(PersonAcademicStatus.class, PersonAcademicStatus.person(), persons))
            SafeMap.safeGet(statusMap, rel.getPerson(), ArrayList.class).add(rel.getAcademicStatus());

        for (ViewWrapper<UnisnppsTimeworker> wrapper : ViewWrapper.<UnisnppsTimeworker>getPatchedList(dataSource))
        {
            Person person = wrapper.getEntity().getPps().getPerson();
            wrapper.setViewProperty("scDegree", degreeMap.get(person));
            wrapper.setViewProperty("scStatus", statusMap.get(person));
        }
    }

    @Override
    public void changeArchival(Long id)
    {
        final UnisnppsTimeworker timeWorker = get(UnisnppsTimeworker.class, id);
        timeWorker.setArchival(!timeWorker.isArchival());
        update(timeWorker);
    }
}
