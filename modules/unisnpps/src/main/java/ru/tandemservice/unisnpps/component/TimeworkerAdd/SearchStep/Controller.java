// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerAdd.SearchStep;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.Person.ui.SimilarAdd.PersonRoleParameter;
import org.tandemframework.shared.person.base.bo.Person.util.CongenialStringFinder;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unisnpps.component.TimeworkerAdd.ITimeworkerAddWizardComponents;
import ru.tandemservice.unisnpps.component.TimeworkerAdd.TimeworkerWizardIdentityCardTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 20.12.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        prepareDataSource(component);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        IdentityCard identityCard = model.getIdentityCard();

        IdentityCard duplicate = getDao().findDuplicate(identityCard);
        model.setDuplicate(duplicate);

        final List<CongenialStringFinder.SearchElement> similarPersons = getDao().findSimilarPersons(identityCard);
        if (null != duplicate)
        {
            // если есть точное совпадение, остальных в списке не выводим
            CongenialStringFinder.SearchElement duplElement = null;
            for (CongenialStringFinder.SearchElement element : similarPersons)
                if (duplicate.getId().equals(element.getCardId()))
                    duplElement = element;
            similarPersons.clear();
            similarPersons.add(duplElement);
        }
        model.setPersons(similarPersons);

        if (!CollectionUtils.isEmpty(similarPersons))
        {
            Model.CreateMethod newPersonMethod = new Model.CreateMethod(Model.CreateMethod.NEW_ID, "Добавить почасовика, создав новую персону");
            Model.CreateMethod onBasisMethod = new Model.CreateMethod(Model.CreateMethod.ON_BASIS_ID, "Добавить почасовика на основе существующей персоны");
            List<org.tandemframework.shared.person.base.bo.Person.ui.SimilarAdd.Model.CreateMethod> createMethods = new ArrayList<>();
            if (null == duplicate)
                createMethods.add(newPersonMethod);
            createMethods.add(onBasisMethod);
            model.setCreateMethods(createMethods);
            model.setCreateMethod(onBasisMethod);
        }

        model.setSearchCompleted(true);

        // ничего не нашли - перейдем сразу на следующую страницу
        if (model.isNothingFound())
            onClickNext(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        DynamicListDataSource<PersonRoleParameter> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(getModel(component1), component1.getSettings());
        });

        org.tandemframework.shared.person.base.bo.Person.ui.SimilarAdd.Controller.prepareDataSource(model, dataSource);
        model.setDataSource(dataSource);
        // todo
        // убрать колонку с радиобаттонами для случая с дубликатом
    }

    public void onClickNext(IBusinessComponent component)
    {
        Model model = getModel(component);

        // еще не искали подобных - запускаем поиск
        if (!model.isSearchCompleted())
        {
            onClickSearch(component);
            return;
        }

        // нашли ппс - переходим на форму задания данных почасовика
        if (model.isDuplicateFound())
        {
            // деактивируем компонент
            deactivate(component);

            Map<String, Object> params = new HashMap<>();
            params.put("basisPersonId", model.getDuplicate().getPerson().getId());
            params.put("openPubAfterClose", Boolean.TRUE);
            params.put("showScienceBlock", Boolean.TRUE);
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(ITimeworkerAddWizardComponents.TIMEWORKER_DATA_STEP, params));
            //component.createDefaultChildRegion("dialog", new ComponentActivator(ITimeworkerAddWizardComponents.TIMEWORKER_DATA_STEP, params));
            return;
        }

        // создаем на основе персоны - переходим на форму задания данных почасовика
        if (model.isSimilarPersonsFound() && model.getCreateMethod().getId().equals(Model.CreateMethod.ON_BASIS_ID))
        {
            Person selectedPerson = model.getSelectedPerson();
            if (null == selectedPerson)
                throw new ApplicationException("Не выбрана персона для основы.");

            // деактивируем компонент
            deactivate(component);

            Map<String, Object> params = new HashMap<>();
            params.put("basisPersonId", selectedPerson.getId());
            params.put("openPubAfterClose", Boolean.TRUE);
            params.put("showScienceBlock", Boolean.TRUE);
            ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(ITimeworkerAddWizardComponents.TIMEWORKER_DATA_STEP, params));
            //component.createDefaultChildRegion("dialog", new ComponentActivator(ITimeworkerAddWizardComponents.TIMEWORKER_DATA_STEP, params));
            return;
        }

        // деактивируем компонент
        deactivate(component);

        // создаем с нуля - переходим на форму удостоверения личности
        Map<String, Object> params = new HashMap<>();
        params.put("identityCard", new TimeworkerWizardIdentityCardTemplate(model.getIdentityCard()));
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(ITimeworkerAddWizardComponents.IDENTITY_CARD_STEP, params));
        //component.getParentRegion().getOwner().createDefaultChildRegion(new ComponentActivator(ITimeworkerAddWizardComponents.IDENTITY_CARD_STEP, params));
    }
}