// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerAdd.IdentityCardStep;

import java.util.List;

import org.apache.tapestry.form.validator.BaseValidator;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisnpps.component.TimeworkerAdd.TimeworkerWizardIdentityCardTemplate;

/**
 * @author oleyba
 * @since 21.12.2010
 */
@Input( @Bind(key = "identityCard", binding = "template"))
@Output( @Bind(key = "basisPersonId", binding = "basisPersonId"))
public class Model
{
    private List<IdentityCardType> _identityCardTypeList;
    private List<Sex> _sexList;
    private ISelectModel _countryModel;
    private ISelectModel _issuancePlaceModel;

    private TimeworkerWizardIdentityCardTemplate template;
    private IdentityCard identityCard;
    private Long basisPersonId;

    private AddressBase _address;
    private boolean _addressEquals;

    public IdentityCardType getIdentityCardType()
    {
        return getIdentityCard().getCardType();    
    }

    public IdentityCard getIdentityCard()
    {
        return identityCard;
    }

    public void setIdentityCard(IdentityCard identityCard)
    {
        this.identityCard = identityCard;
    }

    public Long getBasisPersonId()
    {
        return basisPersonId;
    }

    public void setBasisPersonId(Long basisPersonId)
    {
        this.basisPersonId = basisPersonId;
    }

    public TimeworkerWizardIdentityCardTemplate getTemplate()
    {
        return template;
    }

    public void setTemplate(TimeworkerWizardIdentityCardTemplate template)
    {
        this.template = template;
    }

    public List<IdentityCardType> getIdentityCardTypeList()
    {
        return _identityCardTypeList;
    }

    public void setIdentityCardTypeList(List<IdentityCardType> identityCardTypeList)
    {
        _identityCardTypeList = identityCardTypeList;
    }

    public List<Sex> getSexList()
    {
        return _sexList;
    }

    public void setSexList(List<Sex> sexList)
    {
        _sexList = sexList;
    }

    public ISelectModel getCountryModel()
    {
        return _countryModel;
    }

    public void setCountryModel(ISelectModel countryModel)
    {
        _countryModel = countryModel;
    }

    public ISelectModel getIssuancePlaceModel()
    {
        return _issuancePlaceModel;
    }

    public void setIssuancePlaceModel(ISelectModel issuancePlaceModel)
    {
        _issuancePlaceModel = issuancePlaceModel;
    }

    public AddressBase getAddress()
    {
        return _address;
    }

    public void setAddress(AddressBase address)
    {
        _address = address;
    }

    public boolean isAddressEquals()
    {
        return _addressEquals;
    }

    public void setAddressEquals(boolean addressEquals)
    {
        _addressEquals = addressEquals;
    }
}
