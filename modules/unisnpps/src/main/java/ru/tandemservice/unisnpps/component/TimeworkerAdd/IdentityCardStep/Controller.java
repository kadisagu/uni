// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerAdd.IdentityCardStep;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unisnpps.component.TimeworkerAdd.ITimeworkerAddWizardComponents;

import java.util.HashMap;
import java.util.Map;

/**
 * @author oleyba
 * @since 23.12.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public static final String REGION_ADDRESS = "addressRegion";

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setInline(true);
        addressConfig.setAreaVisible(true);
        addressConfig.setFieldSetTitle("Адрес регистрации");
        addressConfig.setWithoutFieldSet(true);
        component.createChildRegion(REGION_ADDRESS, new ComponentActivator(AddressBaseEditInline.class.getSimpleName(), new ParametersMap().add(AddressBaseEditInlineUI.BIND_CONFIG, addressConfig)));

    }

    public void onChangeIdCartType(IBusinessComponent component)
    {
        final Model model = getModel(component);
        model.getIdentityCard().setCitizenship(model.getIdentityCard().getCardType().getCitizenshipDefault());
    }

    public void onClickNext(IBusinessComponent component)
    {
        final Model model = getModel(component);

        AddressBaseEditInlineUI addressBaseEditInlineUI = (AddressBaseEditInlineUI) component.getChildRegion(REGION_ADDRESS).getActiveComponent().getPresenter();

        if(addressBaseEditInlineUI.getResult() == null) throw new RuntimeException("Адрес обязателен");
        if(!(addressBaseEditInlineUI.getResult() instanceof AddressDetailed)) throw new RuntimeException("Некорректный адрес");
        AddressBase address = addressBaseEditInlineUI.getResult();

        model.setAddress(address);

        getDao().update(model);

        deactivate(component);
        
        // переходим на форму задания данных ппс
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("basisPersonId", model.getBasisPersonId());
        params.put("openPubAfterClose", Boolean.TRUE);
        params.put("showScienceBlock", Boolean.TRUE);
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(ITimeworkerAddWizardComponents.TIMEWORKER_DATA_STEP, params));
        //component.createDefaultChildRegion("dialog", new ComponentActivator(ITimeworkerAddWizardComponents.TIMEWORKER_DATA_STEP, params));
    }
}