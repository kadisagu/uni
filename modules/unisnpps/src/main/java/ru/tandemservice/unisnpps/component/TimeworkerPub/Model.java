// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.Person.util.SecureRoleContext;
import ru.tandemservice.uni.component.person.util.ISecureRoleContextOwner;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

/**
 * @author oleyba
 * @since 24.12.2010
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "timeworker.id"),
        @Bind(key = "selectedTab"),
        @Bind(key = "selectedDataTab")
})
@Output({
        @Bind(key = ISecureRoleContext.SECURE_ROLE_CONTEXT, binding = ISecureRoleContextOwner.SECURE_ROLE_CONTEXT)
})
public class Model implements ISecureRoleContextOwner
{
    private UnisnppsTimeworker timeworker = new UnisnppsTimeworker();

    private String selectedTab;
    private String selectedDataTab;

    @Override
    public ISecureRoleContext getSecureRoleContext()
    {
        return SecureRoleContext.instance(timeworker).accessible(!timeworker.isArchival());
    }

    public UnisnppsTimeworker getTimeworker()
    {
        return timeworker;
    }

    public void setTimeworker(UnisnppsTimeworker timeworker)
    {
        this.timeworker = timeworker;
    }

    public String getSelectedTab()
    {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    public String getSelectedDataTab()
    {
        return selectedDataTab;
    }

    public void setSelectedDataTab(String selectedDataTab)
    {
        this.selectedDataTab = selectedDataTab;
    }
}
