// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerAdd.SearchStep;

import org.tandemframework.shared.person.base.bo.Person.util.CongenialStringFinder;
import org.tandemframework.shared.person.base.entity.IdentityCard;

import java.util.List;

/**
 * @author oleyba
 * @since 20.12.2010
 */
public interface IDAO extends org.tandemframework.shared.person.base.bo.Person.ui.SimilarAdd.IDAO<Model>
{
    IdentityCard findDuplicate(IdentityCard identityCard);

    List<CongenialStringFinder.SearchElement> findSimilarPersons(IdentityCard identityCard);
}
