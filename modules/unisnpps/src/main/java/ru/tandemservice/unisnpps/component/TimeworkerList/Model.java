// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerList;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourcePrinter;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

/**
 * @author oleyba
 * @since 21.12.2010
 */
public class Model
{
    private ISelectModel eduYearModel;
    private ISelectModel orgUnitModel;
    private ISelectModel archivalModel;
    private ISelectModel scDegreeModel;
    private ISelectModel scStatusModel;

    private IDataSettings settings;
    private DynamicListDataSource<UnisnppsTimeworker> dataSource;
    private List<AbstractColumn> mergeColumns = new ArrayList<AbstractColumn>();

    private IListDataSourcePrinter printer;

    public IListDataSourcePrinter getPrinter()
    {
        return printer;
    }

    public void setPrinter(IListDataSourcePrinter printer)
    {
        this.printer = printer;
    }

    public ISelectModel getEduYearModel()
    {
        return eduYearModel;
    }

    public void setEduYearModel(ISelectModel eduYearModel)
    {
        this.eduYearModel = eduYearModel;
    }

    public ISelectModel getOrgUnitModel()
    {
        return orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel)
    {
        this.orgUnitModel = orgUnitModel;
    }

    public ISelectModel getArchivalModel()
    {
        return archivalModel;
    }

    public void setArchivalModel(ISelectModel archivalModel)
    {
        this.archivalModel = archivalModel;
    }

    public IDataSettings getSettings()
    {
        return settings;
    }

    public void setSettings(IDataSettings settings)
    {
        this.settings = settings;
    }

    public DynamicListDataSource<UnisnppsTimeworker> getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<UnisnppsTimeworker> dataSource)
    {
        this.dataSource = dataSource;
    }

    public List<AbstractColumn> getMergeColumns()
    {
        return mergeColumns;
    }

    public void setMergeColumns(List<AbstractColumn> mergeColumns)
    {
        this.mergeColumns = mergeColumns;
    }

    public ISelectModel getScDegreeModel()
    {
        return scDegreeModel;
    }

    public void setScDegreeModel(ISelectModel scDegreeModel)
    {
        this.scDegreeModel = scDegreeModel;
    }

    public ISelectModel getScStatusModel()
    {
        return scStatusModel;
    }

    public void setScStatusModel(ISelectModel scStatusModel)
    {
        this.scStatusModel = scStatusModel;
    }
}
