// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerArchive;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

/**
 * @author oleyba
 * @since 1/28/11
 */
@Input(keys = "archival", bindings = "archival")
public class Model
{
    private ISelectModel eduYearModel;

    private EducationYear year;
    private boolean archival;

    public ISelectModel getEduYearModel()
    {
        return eduYearModel;
    }

    public void setEduYearModel(ISelectModel eduYearModel)
    {
        this.eduYearModel = eduYearModel;
    }

    public EducationYear getYear()
    {
        return year;
    }

    public void setYear(EducationYear year)
    {
        this.year = year;
    }

    public boolean isArchival()
    {
        return archival;
    }

    public void setArchival(boolean archival)
    {
        this.archival = archival;
    }
}
