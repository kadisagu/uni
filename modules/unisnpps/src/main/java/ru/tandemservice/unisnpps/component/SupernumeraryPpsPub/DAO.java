// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.SupernumeraryPpsPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsSupernumeraryPps;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

/**
 * @author oleyba
 * @since 24.12.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setPps(get(UnisnppsSupernumeraryPps.class, model.getPps().getId()));
        model.setDeleteDisabled(getList(UnisnppsTimeworker.class, UnisnppsTimeworker.pps().s(), model.getPps()).size() > 0);
    }

    @Override
    public void prepareTimeworkerDataSource(Model model)
    {
        DynamicListDataSource<UnisnppsTimeworker> dataSource = model.getTimeworkerDataSource();
        MQBuilder builder = new MQBuilder(UnisnppsTimeworker.ENTITY_CLASS, "tw");
        builder.add(MQExpression.eq("tw", UnisnppsTimeworker.pps(), model.getPps()));
        builder.applyOrder(dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }

    @Override
    public void changeArchival(Long id)
    {
        final UnisnppsTimeworker timeworker = get(UnisnppsTimeworker.class, id);
        timeworker.setArchival(!timeworker.isArchival());
        update(timeworker);
    }
}
