package ru.tandemservice.unisnpps.entity.pps.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись в реестре ППС (на базе почасовика)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PpsEntryByTimeworkerGen extends PpsEntry
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker";
    public static final String ENTITY_NAME = "ppsEntryByTimeworker";
    public static final int VERSION_HASH = 1758485723;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_YEAR = "eduYear";
    public static final String P_TIME_AMOUNT_AS_LONG = "timeAmountAsLong";
    public static final String P_TIME_AMOUNT = "timeAmount";

    private EducationYear _eduYear;     // Учебный год
    private long _timeAmountAsLong;     // Число часов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год. Свойство не может быть null.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Число часов. Свойство не может быть null.
     */
    @NotNull
    public long getTimeAmountAsLong()
    {
        return _timeAmountAsLong;
    }

    /**
     * @param timeAmountAsLong Число часов. Свойство не может быть null.
     */
    public void setTimeAmountAsLong(long timeAmountAsLong)
    {
        dirty(_timeAmountAsLong, timeAmountAsLong);
        _timeAmountAsLong = timeAmountAsLong;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PpsEntryByTimeworkerGen)
        {
            setEduYear(((PpsEntryByTimeworker)another).getEduYear());
            setTimeAmountAsLong(((PpsEntryByTimeworker)another).getTimeAmountAsLong());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PpsEntryByTimeworkerGen> extends PpsEntry.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PpsEntryByTimeworker.class;
        }

        public T newInstance()
        {
            return (T) new PpsEntryByTimeworker();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "eduYear":
                    return obj.getEduYear();
                case "timeAmountAsLong":
                    return obj.getTimeAmountAsLong();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
                case "timeAmountAsLong":
                    obj.setTimeAmountAsLong((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduYear":
                        return true;
                case "timeAmountAsLong":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduYear":
                    return true;
                case "timeAmountAsLong":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "eduYear":
                    return EducationYear.class;
                case "timeAmountAsLong":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PpsEntryByTimeworker> _dslPath = new Path<PpsEntryByTimeworker>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PpsEntryByTimeworker");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker#getTimeAmountAsLong()
     */
    public static PropertyPath<Long> timeAmountAsLong()
    {
        return _dslPath.timeAmountAsLong();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker#getTimeAmount()
     */
    public static SupportedPropertyPath<Double> timeAmount()
    {
        return _dslPath.timeAmount();
    }

    public static class Path<E extends PpsEntryByTimeworker> extends PpsEntry.Path<E>
    {
        private EducationYear.Path<EducationYear> _eduYear;
        private PropertyPath<Long> _timeAmountAsLong;
        private SupportedPropertyPath<Double> _timeAmount;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

    /**
     * Смещение на 2 знака для дробной части.
     *
     * @return Число часов. Свойство не может быть null.
     * @see ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker#getTimeAmountAsLong()
     */
        public PropertyPath<Long> timeAmountAsLong()
        {
            if(_timeAmountAsLong == null )
                _timeAmountAsLong = new PropertyPath<Long>(PpsEntryByTimeworkerGen.P_TIME_AMOUNT_AS_LONG, this);
            return _timeAmountAsLong;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker#getTimeAmount()
     */
        public SupportedPropertyPath<Double> timeAmount()
        {
            if(_timeAmount == null )
                _timeAmount = new SupportedPropertyPath<Double>(PpsEntryByTimeworkerGen.P_TIME_AMOUNT, this);
            return _timeAmount;
        }

        public Class getEntityClass()
        {
            return PpsEntryByTimeworker.class;
        }

        public String getEntityName()
        {
            return "ppsEntryByTimeworker";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract double getTimeAmount();
}
