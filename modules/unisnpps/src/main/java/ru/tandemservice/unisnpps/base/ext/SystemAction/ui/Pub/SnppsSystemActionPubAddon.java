/**
 *$Id$
 */
package ru.tandemservice.unisnpps.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unisnpps.events.PpsEntryByTimeworkerSynchronizer;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public class SnppsSystemActionPubAddon extends UIAddon
{
    public SnppsSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickUpdatePpsEntryByTimeworker()
    {
        PpsEntryByTimeworkerSynchronizer.instance.get().doSynchronize();
    }
}
