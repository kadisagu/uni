package ru.tandemservice.unisnpps.entity.pps;

import ru.tandemservice.unisnpps.entity.pps.gen.UnisnppsSupernumeraryPpsGen;

/**
 * Внештатный ппс
 */
public class UnisnppsSupernumeraryPps extends UnisnppsSupernumeraryPpsGen
{
    @Override
    public String getTitle()
    {
        return getFullTitle();
    }

    @Override
    public String getFullTitle()
    {
        return "ППС по договору подряда";
    }

    @Override
    public String getContextTitle()
    {
        return getFullTitle();
    }
}