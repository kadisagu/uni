// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerAdd.TimeworkerDataStep;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;

/**
 * @author oleyba
 * @since 23.12.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        final Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().validate(model, errors);
        if (errors.hasErrors())
            return;
        String confirmMessage = getDao().getConfirmMessage(model);
        if (null == component.getClientParameter() && null != confirmMessage)
        {
            ConfirmInfo confirm = new ConfirmInfo(confirmMessage, new ClickButtonAction("submit_desktop", "ok", true));
            TapSupportUtils.displayConfirm(confirm);
            return;
        }
        getDao().update(model);
        deactivate(component);
        if (model.isOpenPubAfterClose())
            activateInRoot(component, new PublisherActivator(model.getTimeworker(), new ParametersMap()
                    .add("selectedTab", "timeworkerTab")
            ));
    }

    public void doNothing(IBusinessComponent component)
    {

    }
}