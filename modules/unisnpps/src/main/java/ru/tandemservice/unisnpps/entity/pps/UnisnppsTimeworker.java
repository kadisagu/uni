package ru.tandemservice.unisnpps.entity.pps;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unisnpps.entity.pps.gen.UnisnppsTimeworkerGen;

import java.text.MessageFormat;

/**
 * Почасовик
 */
public class UnisnppsTimeworker extends UnisnppsTimeworkerGen
{
    public static final String P_PERIOD_TITLE = "periodTitle";

    @Override
    @EntityDSLSupport(parts = UnisnppsTimeworker.P_LOAD)
    public double getFractionalLoad()
    {
        return 0.01d * getLoad();
    }

    @EntityDSLSupport(parts = UnisnppsTimeworker.P_LOAD)
    public void setFractionalLoad(double area)
    {
        setLoad(Math.round(area * 100.0d));
    }

    public void setGeneratedTitle()
    {
        setTitle(getPps().getPerson().getFio() + " (" + getTitleInfo() + ")");
    }

    public String getTitleInfo()
    {
        return MessageFormat
                .format("{0}, №{1}, {2} ч., {3}",
                        getEduYear().getTitle(),
                        getNumber(),
                        DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getFractionalLoad()),
                        getOrgUnit().getShortTitle());
    }

    public String getStatus()
    {
        return isArchival() ? "в архиве" : "активный" ;
    }

    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + (getDateTo() == null ? "" : ( " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo())));
    }
}