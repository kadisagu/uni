/* $Id:$ */
package ru.tandemservice.unisnpps.base.ext.PpsEntry;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.PpsEntry.PpsEntryManager;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 23.10.2016
 */
@Configuration
public class PpsEntryExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IDefaultComboDataSourceHandler timeWorkerDSHandler()
    {
        return PpsEntry.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> {
                    boolean onlyOuPps = context.getBoolean(PpsEntryManager.PARAM_TIME_WORKER_ONLY_OU, false);

                    dql.where(instanceOf(alias, PpsEntryByTimeworker.class));
                    dql.where(isNull(property(alias, PpsEntry.removalDate())));

                    if (onlyOuPps)
                        FilterUtils.applySelectFilter(dql, alias, PpsEntry.orgUnit().id(), context.get(PpsEntryManager.PARAM_TIME_WORKER_ORG_UNIT_ID));

                    Long eduYearId = context.get(PpsEntryManager.PARAM_TIME_WORKER_EDU_YEAR_ID);
                    if (null != eduYearId)
                        dql.where(exists(PpsEntryByTimeworker.class, PpsEntryByTimeworker.id().s(), property(alias), PpsEntryByTimeworker.eduYear().id().s(), value(eduYearId)));

                    return dql;
                });
    }
}
