// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.events;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLCanDeleteExpressionBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.pps.IPpsEntrySynchronizer;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisnpps.entity.pps.PpsEntryByTimeworker;
import ru.tandemservice.unisnpps.entity.pps.Timeworker4PpsEntry;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 17.01.2011
 */
public class PpsEntryByTimeworkerSynchronizer extends UniBaseDao implements IPpsEntrySynchronizer
{
    public static final SpringBeanCache<IPpsEntrySynchronizer> instance = new SpringBeanCache<IPpsEntrySynchronizer>(PpsEntryByTimeworkerSynchronizer.class.getName());

    public static final SyncDaemon DAEMON = new SyncDaemon(PpsEntryByTimeworkerSynchronizer.class.getName(), 120, IPpsEntrySynchronizer.LOCKER_NAME)
    {
        @Override protected void main() {
            instance.get().doSynchronize();
        }
    };

    @Override
    public void doSynchronize()
    {
        Debug.begin("PpsEntryByTimeworkerSynchronizer");
        try
        {
            // обновляем дату утраты актуальности
            updateRemovalDate();
            // обновляем, либо удаляем неиспользуемые записи ППС
            updateOrDeletePpsEntry();

            Map<TripletKey<Person, OrgUnit, EducationYear>, Long> key2PpsEntryMap = Maps.newHashMap();
            Map<Long, Long> ppsEntry2TimeAmountMap = Maps.newHashMap();
            Map<Long, List<UnisnppsTimeworker>> ppsEntry2TimeworkerMap = Maps.newHashMap();

            // поднимаем все записи ППС на сотрудника, включая сущности, без связей (которые нельзя удалить)
            new DQLSelectBuilder().fromEntity(PpsEntryByTimeworker.class, "pps")
                    .joinEntity("pps", DQLJoinType.left, Timeworker4PpsEntry.class, "ep4pe", eq(property("pps.id"), property("ep4pe", Timeworker4PpsEntry.ppsEntry().id())))
                    .fetchPath(DQLJoinType.left, Timeworker4PpsEntry.timeworker().fromAlias("ep4pe"), "tw")
                    .column(property("pps"))
                    .column(property("ep4pe"))
                    .createStatement(getSession()).<Object[]>list()
                    .forEach(item -> {
                        PpsEntryByTimeworker ppsEntry = (PpsEntryByTimeworker) item[0];
                        Timeworker4PpsEntry ep4pe = (Timeworker4PpsEntry) item[1];
                        TripletKey<Person, OrgUnit, EducationYear> key = TripletKey.create(ppsEntry.getPerson(), ppsEntry.getOrgUnit(), ppsEntry.getEduYear());

                        key2PpsEntryMap.put(key, ppsEntry.getId());
                        ppsEntry2TimeAmountMap.put(ppsEntry.getId(), ppsEntry.getTimeAmountAsLong());
                        List<UnisnppsTimeworker> timeworkers = SafeMap.safeGet(ppsEntry2TimeworkerMap, ppsEntry.getId(), LinkedList.class);
                        if (null != ep4pe)
                            timeworkers.add(ep4pe.getTimeworker());
                    });

            // создаем записи ППС и соответстыующие с ними связи
            createPpsEntryAndRel(key2PpsEntryMap, ppsEntry2TimeworkerMap);
            // заполняем часы
            fillTimeAmountByPpsEntry(ppsEntry2TimeworkerMap, ppsEntry2TimeAmountMap);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }
        finally
        {
            Debug.end();
        }
    }

    /**
     * Отбираются "Запись в реестре ППС (на базе почасовика)" и "Почасовик для записи в реестре ППС" и в зависимости от условия:
     * - выставляется текущая дата утраты актуальности, если почасовик не в актуальном состоянии;
     * - удаляется дата утраты актуальности, если почасовик в актуальном состоянии.
     */
    private void updateRemovalDate()
    {
        // если clear = true => обнулить дату
        for (boolean clear : new boolean[]{false, true})
        {
            {
                DQLUpdateBuilder update = new DQLUpdateBuilder(PpsEntryByTimeworker.class)
                        .fromDataSource(
                                new DQLSelectBuilder()
                                        .fromEntity(Timeworker4PpsEntry.class, "pps")
                                        .column(property("pps", Timeworker4PpsEntry.ppsEntry().id()), "entryId")
                                        .where(clear
                                                       ? and(
                                                               isNotNull(property("pps", Timeworker4PpsEntry.ppsEntry().removalDate())),
                                                               in(property("pps", Timeworker4PpsEntry.timeworker().id()), getTimeworkerNSelectBuilder("tw", true).column(property("tw", "id")).buildQuery()))
                                                       : and(
                                                               isNull(property("pps", Timeworker4PpsEntry.ppsEntry().removalDate())),
                                                               notIn(property("pps", Timeworker4PpsEntry.timeworker().id()), getTimeworkerNSelectBuilder("tw", true).column(property("tw", "id")).buildQuery()))
                                        )
                                        .buildQuery(), "x")
                        .where(eq(property(PpsEntryByTimeworker.id()), property("x.entryId")));

                if (clear)
                    update.set(PpsEntryByTimeworker.removalDate().s(), "cast(null, date)");
                else
                    update.set(PpsEntryByTimeworker.removalDate().s(), value(new Date(), PropertyType.DATE));

                final int updateCount = executeAndClear(update);
                if (updateCount > 0)
                {
                    Debug.message("updated-inactive-rows-pps=" + updateCount);
                }
            }
            {
                DQLUpdateBuilder update = new DQLUpdateBuilder(Timeworker4PpsEntry.class)
                        .fromDataSource(
                                new DQLSelectBuilder()
                                        .fromEntity(Timeworker4PpsEntry.class, "tw4pe")
                                        .column(property("tw4pe.id"), "tw4peId")
                                        .where(clear
                                                       ? and(
                                                               isNotNull(property("tw4pe", Timeworker4PpsEntry.removalDate())),
                                                               in(property("tw4pe", Timeworker4PpsEntry.timeworker().id()), getTimeworkerNSelectBuilder("tw", true).column(property("tw", "id")).buildQuery()))
                                                       : and(
                                                               isNull(property("tw4pe", Timeworker4PpsEntry.removalDate())),
                                                               notIn(property("tw4pe", Timeworker4PpsEntry.timeworker().id()), getTimeworkerNSelectBuilder("tw", true).column(property("tw", "id")).buildQuery()))
                                        )
                                        .buildQuery(), "x")
                        .where(eq(property(Timeworker4PpsEntry.id()), property("x.tw4peId")));

                if (clear)
                    update.set(Timeworker4PpsEntry.removalDate().s(), "cast(null, date)");
                else
                    update.set(Timeworker4PpsEntry.removalDate().s(), value(new Date(), PropertyType.DATE));

                final int updateCount = executeAndClear(update);
                if (updateCount > 0)
                {
                    Debug.message("updated-active-rows-tw4pe=" + updateCount);
                }
            }
        }
    }

    private void updateOrDeletePpsEntry()
    {
        Debug.begin("PpsEntryByTimeworkerSynchronizer.updateOrDelete-ppsEntry");
        try
        {
            // удаляем все Timeworker4PpsEntry, для которых ключ свойств не соответствует связи. Такие записи не имею связей с другими сущностями, смело удаляем!
            {
                final int removeCount = new DQLDeleteBuilder(Timeworker4PpsEntry.class)
                        .fromDataSource(
                                new DQLSelectBuilder().fromEntity(Timeworker4PpsEntry.class, "ep4pe")
                                        .where(exists(
                                                new DQLSelectBuilder().fromEntity(Timeworker4PpsEntry.class, "rel")
                                                        .joinPath(DQLJoinType.inner, Timeworker4PpsEntry.timeworker().fromAlias("rel"), "tw")
                                                        .joinPath(DQLJoinType.inner, Timeworker4PpsEntry.ppsEntry().fromAlias("rel"), "pps")
                                                        .joinPath(DQLJoinType.left, UnisnppsTimeworker.person().fromAlias("tw"), "epPerson")
                                                        .joinPath(DQLJoinType.left, UnisnppsTimeworker.orgUnit().fromAlias("tw"), "epOu")
                                                        .joinPath(DQLJoinType.left, UnisnppsTimeworker.eduYear().fromAlias("tw"), "eduYear")
                                                        .where(eq(property("ep4pe.id"), property("rel.id")))
                                                        .where(or(
                                                                ne(property("pps", PpsEntryByTimeworker.person().id()), property("epPerson.id")),
                                                                ne(property("pps", PpsEntryByTimeworker.orgUnit().id()), property("epOu.id")),
                                                                ne(property("pps", PpsEntryByTimeworker.eduYear().id()), property("eduYear.id"))
                                                        ))
                                                        .column(property("rel.id"))
                                                        .buildQuery()
                                        ))
                                        .column(property("ep4pe.id"), "item_id")
                                        .buildQuery(), "x"
                        )
                        .where(eq(property(Timeworker4PpsEntry.id()), property("x.item_id")))
                        .createStatement(getSession()).execute();
                if (removeCount > 0)
                {
                    Debug.message("removed-timeworker4PpsEntry-rows=" + removeCount);
                }
            }

            // для записей ППС, заблокированых входящими ссылками и не имеющих связей Timeworker4PpsEntry ставим дату утраты актуальности
            {
                final int updateCount = new DQLUpdateBuilder(PpsEntryByTimeworker.class)
                        .fromDataSource(
                                new DQLSelectBuilder().fromEntity(PpsEntryByTimeworker.class, "pps")
                                        .where(notExists(Timeworker4PpsEntry.class, Timeworker4PpsEntry.ppsEntry().id().s(), property("pps.id")))
                                        .where(not(new DQLCanDeleteExpressionBuilder(PpsEntry.class, "pps.id").getExpression()))
                                        .column(property("pps.id"), "item_id")
                                        .buildQuery(), "x"
                        )
                        .where(eq(property(PpsEntryByTimeworker.id()), property("x.item_id")))
                        .set(PpsEntryByTimeworker.removalDate().s(), value(new Date(), PropertyType.DATE))
                        .createStatement(getSession()).execute();
                if (updateCount > 0)
                {
                    Debug.message("updated-ppsEntry-rows=" + updateCount);
                }
            }

            // удаляем для записей ППС, незаблокированые входящими ссылками и не имеющих связей Timeworker4PpsEntry
            {
                final int removeCount = new DQLDeleteBuilder(PpsEntryByTimeworker.class)
                        .fromDataSource(
                                new DQLSelectBuilder().fromEntity(PpsEntryByTimeworker.class, "pps")
                                        .where(notExists(Timeworker4PpsEntry.class, Timeworker4PpsEntry.ppsEntry().id().s(), property("pps.id")))
                                        .where(new DQLCanDeleteExpressionBuilder(PpsEntry.class, "pps.id").getExpression())
                                        .column(property("pps.id"), "item_id")
                                        .buildQuery(), "x"
                        )
                        .where(eq(property(PpsEntryByTimeworker.id()), property("x.item_id")))
                        .createStatement(getSession()).execute();

                if (removeCount > 0)
                {
                    Debug.message("removed-ppsEntry-rows=" + removeCount);
                }
            }
        }
        finally
        {
            Debug.end();
        }
    }

    private void createPpsEntryAndRel(Map<TripletKey<Person, OrgUnit, EducationYear>, Long> key2PpsEntryMap, Map<Long, List<UnisnppsTimeworker>> ppsEntry2TimeworkerMap)
    {
        Debug.begin("PpsEntryByTimeworkerSynchronizer.create");
        try
        {
            Date now = new Date();
            final short ppsEntryEntityCode = EntityRuntime.getMeta(PpsEntryByTimeworker.class).getEntityCode();
            final short timeworker4PEEntityCode = EntityRuntime.getMeta(Timeworker4PpsEntry.class).getEntityCode();

            List<Object[]> rows = getTimeworkerNSelectBuilder("tw", true)
                    .joinPath(DQLJoinType.inner, UnisnppsTimeworker.person().fromAlias("tw"), "person")
                    .joinPath(DQLJoinType.inner, UnisnppsTimeworker.orgUnit().fromAlias("tw"), "ou")
                    .joinPath(DQLJoinType.inner, UnisnppsTimeworker.eduYear().fromAlias("tw"), "eduYear")
                    .where(or(
                            notExists(Timeworker4PpsEntry.class, Timeworker4PpsEntry.timeworker().id().s(), property("tw", "id")),
                            notExists(
                                    new DQLSelectBuilder()
                                            .fromEntity(PpsEntryByTimeworker.class, "pps")
                                            .where(eq(property("pps", PpsEntryByTimeworker.person().id()), property("person.id")))
                                            .where(eq(property("pps", PpsEntryByTimeworker.orgUnit().id()), property("ou.id")))
                                            .where(eq(property("pps", PpsEntryByTimeworker.eduYear().id()), property("eduYear.id")))
                                            .buildQuery()
                            )
                    ))
                    .column(property("person"))
                    .column(property("ou"))
                    .column(property("eduYear"))
                    .column(property("tw"))
                    .createStatement(getSession()).list();

            Iterables.partition(rows, DQL.MAX_VALUES_ROW_NUMBER).forEach(items -> {
                DQLInsertValuesBuilder ppsEntryBuilder = new DQLInsertValuesBuilder(PpsEntryByTimeworker.class);
                DQLInsertValuesBuilder timeworker4PEBuilder = new DQLInsertValuesBuilder(Timeworker4PpsEntry.class);

                boolean savePpsEntry = false;
                boolean saveTimeworker4PE = false;

                for (Object[] item : items)
                {
                    Person person = (Person) item[0];
                    OrgUnit orgUnit = (OrgUnit) item[1];
                    EducationYear eduYear = (EducationYear) item[2];
                    UnisnppsTimeworker timeworker = (UnisnppsTimeworker) item[3];
                    boolean active = !timeworker.isArchival();

                    TripletKey<Person, OrgUnit, EducationYear> key = TripletKey.create(person, orgUnit, eduYear);
                    final MutableBoolean hasRow = new MutableBoolean(false);

                    Long ppsEntryId = key2PpsEntryMap.get(key);
                    if (null == ppsEntryId)
                    {
                        ppsEntryId = EntityIDGenerator.generateNewId(ppsEntryEntityCode);

                        ppsEntryBuilder.valuesFrom(new PpsEntryByTimeworker(person, orgUnit, eduYear));
                        ppsEntryBuilder.value(PpsEntryByTimeworker.P_ID, ppsEntryId);
                        ppsEntryBuilder.value(PpsEntryByTimeworker.P_CREATION_DATE, now);
                        ppsEntryBuilder.value(PpsEntryByTimeworker.P_REMOVAL_DATE, active ? null : now);
                        ppsEntryBuilder.addBatch();
                        savePpsEntry = true;

                        key2PpsEntryMap.put(key, ppsEntryId);
                    }
                    else
                    {
                        List<UnisnppsTimeworker> timeworkers = ppsEntry2TimeworkerMap.getOrDefault(ppsEntryId, Lists.newArrayList());
                        timeworkers.stream().filter(rel -> rel.getId().equals(timeworker.getId())).forEach(rel -> hasRow.setValue(true));
                    }

                    if (hasRow.isFalse())
                    {
                        timeworker4PEBuilder.value(Timeworker4PpsEntry.P_ID, EntityIDGenerator.generateNewId(timeworker4PEEntityCode));
                        timeworker4PEBuilder.value(Timeworker4PpsEntry.L_PPS_ENTRY, ppsEntryId);
                        timeworker4PEBuilder.value(Timeworker4PpsEntry.L_TIMEWORKER, timeworker);
                        timeworker4PEBuilder.addBatch();
                        saveTimeworker4PE = true;

                        SafeMap.safeGet(ppsEntry2TimeworkerMap, ppsEntryId, ArrayList.class).add(timeworker);
                    }
                }
                if (savePpsEntry) ppsEntryBuilder.createStatement(getSession()).execute();
                if (saveTimeworker4PE) timeworker4PEBuilder.createStatement(getSession()).execute();
            });
        }
        finally
        {
            Debug.end();
        }
    }

    private void fillTimeAmountByPpsEntry(Map<Long, List<UnisnppsTimeworker>> timeworkerMap, Map<Long, Long> timeAmountMap)
    {
        Debug.begin("PpsEntryByTimeworkerSynchronizer.fill-employeeCode");
        try
        {
            IDQLStatement updatePpsEntryStatement = new DQLUpdateBuilder(PpsEntryByTimeworker.class)
                    .set(PpsEntryByTimeworker.P_TIME_AMOUNT_AS_LONG, parameter("timeAmount", PropertyType.LONG))
                    .where(eq(property("id"), parameter("ppsEntryId", PropertyType.LONG)))
                    .createStatement(getSession());

            for (Map.Entry<Long, List<UnisnppsTimeworker>> entry : timeworkerMap.entrySet())
            {
                Long ppsEntryId = entry.getKey();
                long timeAmountNew = entry.getValue().stream().filter(t -> !t.isArchival()).mapToLong(UnisnppsTimeworker::getLoad).sum();
                long timeAmountOld = timeAmountMap.getOrDefault(entry.getKey(), 0L);

                if (timeAmountNew != timeAmountOld)
                {
                    updatePpsEntryStatement.setLong("timeAmount", timeAmountNew);
                    updatePpsEntryStatement.setLong("ppsEntryId", ppsEntryId);
                    updatePpsEntryStatement.execute();
                }
            }
        }
        finally
        {
            Debug.end();
        }
    }

    private DQLSelectBuilder getTimeworkerNSelectBuilder(String alias, boolean activeOnly)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(UnisnppsTimeworker.class, alias);
        if (activeOnly)
            dql.where(DQLExpressions.eq(DQLExpressions.property(alias, UnisnppsTimeworker.archival()), DQLExpressions.value(Boolean.FALSE)));
        return dql;
    }
}
