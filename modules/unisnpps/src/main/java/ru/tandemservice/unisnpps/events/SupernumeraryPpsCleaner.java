// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.events;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.pps.IPpsEntrySynchronizer;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsSupernumeraryPps;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

/**
 * @author oleyba
 * @since 24.01.2011
 */
public class SupernumeraryPpsCleaner extends UniBaseDao implements ICleaner
{
    public static final SpringBeanCache<ICleaner> instance = new SpringBeanCache<ICleaner>(SupernumeraryPpsCleaner.class.getName());

    public static final SyncDaemon DAEMON = new SyncDaemon(SupernumeraryPpsCleaner.class.getName(), 120, IPpsEntrySynchronizer.LOCKER_NAME) {
        @Override protected void main(){
            instance.get().doClean();
        }
    };

    @Override
    public void doClean()
    {
        MQBuilder builder = new MQBuilder(UnisnppsSupernumeraryPps.ENTITY_CLASS, "pps");
        MQBuilder subBuilder = new MQBuilder(UnisnppsTimeworker.ENTITY_CLASS, "tw", new String[]{UnisnppsTimeworker.pps().id().s()});
        builder.add(MQExpression.notIn("pps", "id", subBuilder));
        List<UnisnppsSupernumeraryPps> ppsList = builder.getResultList(getSession());
        for (UnisnppsSupernumeraryPps pps : ppsList)
            instance.get().doDelete(pps);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void doDelete(UnisnppsSupernumeraryPps pps)
    {
        try
        {
            delete(pps.getId());
            executeFlush();
        }
        catch (Exception e)
        {
            // ну и ладно, наверное, кто-то на него ссылался
        }
    }
}
