// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.SupernumeraryPpsPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import ru.tandemservice.unisnpps.component.TimeworkerAdd.ITimeworkerAddWizardComponents;
import ru.tandemservice.unisnpps.entity.pps.UnisnppsTimeworker;

/**
 * @author oleyba
 * @since 24.12.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        prepareTimeworkerDataSource(component);
    }

    private void prepareTimeworkerDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getTimeworkerDataSource() != null) return;

        DynamicListDataSource<UnisnppsTimeworker> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareTimeworkerDataSource(model);
        }, 5);


        new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return ParametersMap
                        .createWith("selectedTab", "timeworkerTab")
                        .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
            }
        };



        dataSource.addColumn(new SimpleColumn("Учебный год", UnisnppsTimeworker.eduYear().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new PublisherColumnBuilder("№ договора", UnisnppsTimeworker.number().s(), "timeworkerTab").build());
        dataSource.addColumn(new SimpleColumn("Период договора", UnisnppsTimeworker.P_PERIOD_TITLE).setOrderable(true).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во часов", UnisnppsTimeworker.fractionalLoad().s(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", UnisnppsTimeworker.orgUnit().fullTitle().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Описание", UnisnppsTimeworker.comment().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата регистрации", UnisnppsTimeworker.regDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(true).setClickable(false));
        ToggleColumn statusColumn = new ToggleColumn("Статус", UnisnppsTimeworker.archival().s());
        statusColumn.setPermissionKey("archiveTimeworker_pps");
        statusColumn.indicator(Boolean.FALSE, new IndicatorColumn.Item("toggled_on", "Почасовик не в архиве", "onClickSwitchArchival", "Списать почасовика «{0}» в архив?", new Object[]{UnisnppsTimeworker.pps().person().fullFio().s()}));
        statusColumn.indicator(Boolean.TRUE, new IndicatorColumn.Item("toggled_off", "Почасовик в архиве", "onClickSwitchArchival", "Восстановить почасовика «{0}» из архива?", new Object[]{UnisnppsTimeworker.pps().person().fullFio().s()}));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditTimeworker").setPermissionKey("editTimeworker_pps").setDisabledProperty(UnisnppsTimeworker.archival().s()));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteElement", "Удалить почасовика {0}?", UnisnppsTimeworker.title().s()).setPermissionKey("deleteTimeworker_pps"));
        dataSource.addColumn(statusColumn);

        model.setTimeworkerDataSource(dataSource);
    }

    public void onClickAddTimeworker(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ITimeworkerAddWizardComponents.TIMEWORKER_DATA_STEP, new ParametersMap()
                .add("ppsId", getModel(component).getPps().getId())));
    }

    public void onClickEditTimeworker(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(ITimeworkerAddWizardComponents.TIMEWORKER_DATA_STEP, new ParametersMap()
                .add("timeworkerId", (Long) component.getListenerParameter())));
    }

    public void onClickDeleteElement(IBusinessComponent component)
    {
        getDao().delete((Long) component.getListenerParameter());
    }

    public void onClickSwitchArchival(IBusinessComponent component)
    {
        getDao().changeArchival((Long) component.getListenerParameter());
    }

    public void onClickDeletePps(IBusinessComponent component)
    {
        getDao().delete(getModel(component).getPps());
    }
}