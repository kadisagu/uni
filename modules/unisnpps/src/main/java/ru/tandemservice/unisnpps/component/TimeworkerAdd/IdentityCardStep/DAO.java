// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisnpps.component.TimeworkerAdd.IdentityCardStep;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.bo.Fias.FiasManager;
import org.tandemframework.shared.fias.base.bo.util.CitizenshipAutocompleteModel;
import org.tandemframework.shared.fias.base.bo.util.CountryAutocompleteModel;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.bo.Person.util.IssuancePlaceSelectModel;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author oleyba
 * @since 22.12.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setSexList(getCatalogItemList(Sex.class));
        model.setIdentityCardTypeList(getCatalogItemListOrderByCode(IdentityCardType.class));
        model.setCountryModel(new CitizenshipAutocompleteModel());
        model.setIssuancePlaceModel(new IssuancePlaceSelectModel());

        if (model.getIdentityCard() != null)
            return;

        IdentityCard identityCard = new IdentityCard();
        model.setIdentityCard(identityCard);
        if (model.getTemplate() != null)
            model.getTemplate().apply(identityCard);
        else
           identityCard.setCardType(getCatalogItem(IdentityCardType.class, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT));
        identityCard.setCitizenship(get(AddressCountry.class, AddressCountry.P_CODE, IKladrDefines.RUSSIA_COUNTRY_CODE));
    }

    @Override
    public void update(Model model)
    {
        final IdentityCard identityCard = model.getIdentityCard();
        if (!identityCard.getCardType().isShowSeria())
            identityCard.setSeria(null);

        final Session session = getSession();

        final Person person = new Person();

        // сохраняем адрес регистрации (если указан город)
        AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(identityCard, AddressBaseUtils.getSameAddress(model.getAddress()), IdentityCard.L_ADDRESS, true);
        if(model.isAddressEquals())
            AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(person, AddressBaseUtils.getSameAddress(model.getAddress()), Person.L_ADDRESS, true);

        identityCard.setPhoto(new DatabaseFile());
        session.save(identityCard.getPhoto()); //сохраняем фотку
        session.save(identityCard); //сохраняем удостоверение личности

        person.setContactData(new PersonContactData());
        session.save(person.getContactData());

        person.setIdentityCard(identityCard);
        session.save(person); //cохраняем персону

        identityCard.setPerson(person);
        session.update(identityCard);

        model.setBasisPersonId(person.getId());
    }
}
