package unienr14_ctr.scripts.report

import jxl.Workbook
import jxl.WorkbookSettings
import jxl.write.Label
import org.apache.commons.lang.math.NumberUtils
import org.hibernate.Session
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract

return new EnrolledEntrantsContractDataExportPrint(
        session: session,
        extractIds: extractIds,
        contractIds: contractIds,
        versionIds: versionIds,
        contractorIds : contractorIds,
        payPromiseIds : payPromiseIds,
        eduPromiseIds : eduPromiseIds
).print()

/**
 * @author rsizonenko
 * @since 15.08.2014
 */

class EnrolledEntrantsContractDataExportPrint
{
    Session session
    List<Long> extractIds
    List<Long> contractIds
    List<Long> versionIds
    List<Long> contractorIds
    List<Long> payPromiseIds
    List<Long> eduPromiseIds
    List<EnrEnrollmentExtract> extracts
    List<CtrContractVersion> versions
    List<EnrEntrantContract> contracts
    List<EnrEntrantContract> payPromises
    List<EnrEntrantContract> eduPromises
    List<CtrContractVersionContractor> contractors
    Map<Long, EnrEntrantContract> contractMap = new HashMap<>()
    Map<Long, CtrContractVersion> versionMap = new HashMap<>()
    Map<Long, CtrContractVersionContractor> contractorMap = new HashMap<>()
    Map<Long, CtrPaymentPromice> payPromiseMap = new HashMap<>()
    Map<Long, EduCtrEducationPromise> eduPromiseMap = new HashMap<>()
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    def print()
    {

        // создаем печатную форму
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        def workbook = Workbook.createWorkbook(out, ws);


        contracts = IUniBaseDao.instance.get().getList(EnrEntrantContract.class, "id", contractIds)
        for (EnrEntrantContract contract : contracts)
        {
            contractMap.put(contract.getRequestedCompetition().getId(), contract)
        }

        versions = IUniBaseDao.instance.get().getList(CtrContractVersion.class, "id", versionIds)
        for (CtrContractVersion version : versions)
        {
            versionMap.put(version.getContract().getId(), version)
        }

        contractors = IUniBaseDao.instance.get().getList(CtrContractVersionContractor.class, "id", contractorIds)
        for (CtrContractVersionContractor contractor : contractors)
        {
            contractorMap.put(contractor.getOwner().getId(), contractor)
        }

        eduPromises = IUniBaseDao.instance.get().getList(EduCtrEducationPromise.class, "id", eduPromiseIds)
        for (EduCtrEducationPromise promise : eduPromises)
        {
            eduPromiseMap.put(promise.getDst().getId(), promise)
        }

        payPromises = IUniBaseDao.instance.get().getList(CtrPaymentPromice.class, "id", payPromiseIds)
        for (CtrPaymentPromice promise : payPromises)
        {
            payPromiseMap.put(promise.getSrc().getId(), promise)
        }

        // создаем лист
        def sheet = workbook.createSheet("Данные о договорах зачисленных абитуриентов", 0);

        extracts = IUniBaseDao.instance.get().getList(EnrEnrollmentExtract.class, "id", extractIds)

        printTable(sheet)

        workbook.write();
        workbook.close();

        // стандартные выходные параметры скрипта
        return [document: out.toByteArray(),
                fileName: "entrant_data_export_" + new Date().format("dd.MM.yyyy") + ".xls"]

    }

    def printTable(def sheet)
    {
        int colN = 0;

        ["personalNumber", "lastName", "firstName", "middleName", "cardType", "seria", "number", "issuanceDate", "issuancePlace", "sex", "birthDate",
         "regNumber", "programForm", "compensationType", "type", "institutionOrgUnit", "formativeOrgUnit", "programSubject", "programSet", "numberOrder", "numberContract",
         "typeContract", "durationBeginDate", "eduProgram", "contactor", "totalCost"].each {
            sheet.addCell(new Label(colN++, 0, it))
        }

        int rowNumber = 1;
        for (EnrEnrollmentExtract extract : extracts)
        {
            EnrRequestedCompetition reqComp = extract.requestedCompetition;
            EnrRequestedCompetitionTA reqCompTA = reqComp instanceof EnrRequestedCompetitionTA ? reqComp : null;
            EnrEntrantContract contract = contractMap.get(reqComp.id);
            CtrContractVersion version = contract != null ? versionMap.get(contract.contractObject.id) : null;
            CtrContractVersionContractor contractor = version != null ? contractorMap.get(version.id) : null;
            EduCtrEducationPromise eduPromise = contractor != null ? eduPromiseMap.get(contractor.id) : null;
            CtrPaymentPromice payPromise = contractor != null ? payPromiseMap.get(contractor.id) : null;

            sheet.addCell(!NumberUtils.isNumber(reqComp.request.entrant.personalNumber) ? new Label(0, rowNumber, reqComp.request.entrant.personalNumber) : new jxl.write.Number(0, rowNumber,  Double.valueOf(reqComp.request.entrant.personalNumber)))
            sheet.addCell(new Label(1, rowNumber, reqComp.request.entrant.person.identityCard.lastName))
            sheet.addCell(new Label(2, rowNumber, reqComp.request.entrant.person.identityCard.firstName))
            sheet.addCell(new Label(3, rowNumber, reqComp.request.entrant.person.identityCard.middleName))
            sheet.addCell(new Label(4, rowNumber, reqComp.request.entrant.person.identityCard.cardType.title))
            sheet.addCell(new Label(5, rowNumber, reqComp.request.entrant.person.identityCard.seria))
            sheet.addCell(new Label(6, rowNumber, reqComp.request.entrant.person.identityCard.number))
            sheet.addCell(new Label(7, rowNumber, reqComp.request.entrant.person.identityCard.issuanceDate?.format("dd.MM.yyyy")))
            sheet.addCell(new Label(8, rowNumber, reqComp.request.entrant.person.identityCard.issuancePlace))
            sheet.addCell(new Label(9, rowNumber, reqComp.request.entrant.person.identityCard.sex.code.equals(SexCodes.MALE) ? "муж" : "жен"))
            sheet.addCell(new Label(10, rowNumber, reqComp.request.entrant.person.identityCard.birthDate?.format("dd.MM.yyyy")))
            sheet.addCell(new Label(11, rowNumber, reqComp.getRequest().getRegNumber()))
            sheet.addCell(new Label(12, rowNumber, reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm().title))
            sheet.addCell(new Label(13, rowNumber, reqComp.getCompetition().getType().getCompensationType().shortTitle))
            sheet.addCell(new Label(14, rowNumber, reqComp.getCompetition().getType().shortTitle))
            sheet.addCell(new Label(15, rowNumber, reqComp.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().code))
            sheet.addCell(new Label(16, rowNumber, reqComp.getCompetition().getProgramSetOrgUnit().getEffectiveFormativeOrgUnit()?.getShortTitle()))
            sheet.addCell(new Label(17, rowNumber, reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject().titleWithCode))
            sheet.addCell(new Label(18, rowNumber, reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getTitle()))
            sheet.addCell(new Label(19, rowNumber, extract.order.number))
            if (contract != null) {
                sheet.addCell(new Label(20, rowNumber, contract.getContractObject().number))
                sheet.addCell(new Label(21, rowNumber, contract.getContractObject().getType().getTitle()))
                if (version != null) {
                    sheet.addCell(new Label(22, rowNumber, version?.durationBeginDate.format("dd.MM.yyyy")))
                    sheet.addCell(new Label(23, rowNumber, eduPromise?.getEduProgram()?.getTitleWithCodeAndConditions()))
                    sheet.addCell(new Label(24, rowNumber, contractor?.getContactor()?.getFullTitle()))
                    sheet.addCell(new Label(25, rowNumber, payPromise?.getSourceStage()?.getCost()?.costAsCurrencyString))
                }
            }
            rowNumber++;



        }
    }
}
