package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.ui.ListTab;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplate;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.EnrEntrantContractManager;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic.EnrEntrantContractListHandler;

@Input({
    @Bind(key=UIPresenter.PUBLISHER_ID, binding="entrantHolder.id", required=true)
})
public class EnrEntrantContractListTabUI extends UIPresenter
{

    private final EntityHolder<EnrEntrant> entrantHolder = new EntityHolder<>();
    public EntityHolder<EnrEntrant> getEntrantHolder() { return this.entrantHolder; }
    public EnrEntrant getEntrant() { return getEntrantHolder().getValue(); }

    private BaseSearchListDataSource contractListDataSource;
    public BaseSearchListDataSource getContractListDataSource() { return this.contractListDataSource; }

    @Override
    public void onComponentRefresh() {
        this.contractListDataSource = (BaseSearchListDataSource)this.getConfig().getDataSource(EnrEntrantContractListTab.CONTRACT_LIST_DS);
        getEntrantHolder().refresh(EnrEntrant.class);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (EnrEntrantContractListTab.CONTRACT_LIST_DS.equals(dataSource.getName())) {
            dataSource.put(EnrEntrantContractListHandler.PARAM_ENTRANT_ID, getEntrant().getId());
        }
    }

    public boolean isCurrentPaymentExistsDisabled() {
        // там внутри может быть dataWrapper
        return Boolean.TRUE.equals(
            ((IEntity)getContractListDataSource().getCurrent()).getProperty(
                EnrEntrantContract.paymentResultExists()
            )
        );
    }

    public void onClickAddEntrantContract() {
        this._uiActivation.asRegionDialog(CtrContractVersionAddByTemplate.class).parameter(IUIPresenter.PUBLISHER_ID, getEntrant().getId()).activate();
    }

    public void onClickSwitchPaymentExists()
    {
        Long id = getSupport().getListenerParameterAsLong();
        EnrEntrantContract rel = DataAccessServices.dao().getNotNull(EnrEntrantContract.class, id);
        EnrEntrantContractManager.instance().dao().doSetPaymentDate(rel.getId(), rel.isPaymentExists());
        getSupport().setRefreshScheduled(true);
    }

    public void onClickSwitchAttachContractToStudent(){
        Long id = getSupport().getListenerParameterAsLong();
        DataWrapper wrapper = getContractListDataSource().getRecordById(id);
        if((boolean) wrapper.getProperty(EnrEntrantContractListHandler.VIEW_PROPERTY_RELATION_WITH_STUDENT))
            EnrEntrantContractManager.instance().dao().doDetachEnrEntrantContractToStudent(((EnrEntrantContract)wrapper.getWrapped()).getContractObject().getId());
        else if(!(boolean) wrapper.getProperty(EnrEntrantContractListHandler.VIEW_PROPERTY_DISABLED_ATTACHMENT) &&
                (wrapper.getProperty(EnrEntrantContractListHandler.VIEW_PROPERTY_STUDENT) != null))
        {
            EnrEntrantContractManager.instance().dao().doAttachEnrEntrantContractToStudent(((EnrEntrantContract)wrapper.getWrapped()).getContractObject().getId());
        }


    }

}
