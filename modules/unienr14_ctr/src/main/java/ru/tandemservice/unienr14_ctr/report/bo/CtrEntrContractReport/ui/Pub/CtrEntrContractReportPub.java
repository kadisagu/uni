/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Stanislav Shibarshin
 * @since 03.10.2016
 */
@Configuration
public class CtrEntrContractReportPub extends BusinessComponentManager
{

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }

}
