// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unienr14_ctr.sec;

import java.util.Map;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.util.CtrContractVersionUtil;

import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;

/**
 * @author oleyba
 * @since 01.12.2010
 */
public class EnrCtrPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("unienr14_ctr");
        config.setName("unienr14_ctr-sec-config");
        config.setTitle("");

        // права на объект «Договор с абитуриентом»

        PermissionGroupMeta pg = CtrContractVersionUtil.registerPermissionGroup(config, EnrEntrantContract.class, EduContractManager.instance());
        PermissionMetaUtil.createGroupRelation(config, pg.getName(), "enrEntrantContractCG");

        ModuleLocalGroupMeta moduleLocalGroupMeta = PermissionMetaUtil.createModuleLocalGroup(config, "unienr14ctr", "Модуль «Договоры абитуриентов»");
        ClassGroupMeta lcg = PermissionMetaUtil.createClassGroup(moduleLocalGroupMeta, "enrEntrantContractLC", "Объект «Договор на обучение с абитуриентом»", "ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract");
        PermissionMetaUtil.createGroupRelation(config, pg.getName(), lcg.getName());

        securityConfigMetaMap.put(config.getName(), config);

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();

            PermissionGroupMeta pgEcfTab = PermissionMetaUtil.createPermissionGroup(config, code + "Enr14TabPermissionGroup", "Вкладка «Абитуриенты»");

            PermissionGroupMeta pgEcfPkgTab = PermissionMetaUtil.createPermissionGroup(pgEcfTab, code + "Enr14ContractTabPermissionGroup", "Вкладка «Договоры на обучение»");
            PermissionMetaUtil.createPermission(pgEcfPkgTab, "orgUnit_viewEnr14ContractTab_" + code, "Просмотр");

            final PermissionGroupMeta permissionGroupReports = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            final PermissionGroupMeta eduCtrReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReports, code + "EduCtrReportsPG", "Отчеты модуля «Договоры на обучение»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_viewCtrEntrContractReport_" + code, "Просмотр и печать отчета «Сводка по количеству оплаченных договоров ВО поступающих (по абитуриентам)»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_addCtrEntrContractReport_" + code, "Добавление отчета «Сводка по количеству оплаченных договоров ВО поступающих (по абитуриентам)»");
            PermissionMetaUtil.createPermission(eduCtrReports, "orgUnit_deleteCtrEntrContractReport_" + code, "Удаление отчета «Сводка по количеству оплаченных договоров ВО поступающих (по абитуриентам)»");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}
