package ru.tandemservice.unienr14_ctr.catalog.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem;
import ru.tandemservice.unienr14_ctr.catalog.entity.CtrScriptItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Конфигурация для скриптовой печати модуля «Договоры абитуриентов»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CtrScriptItemGen extends ScriptItem
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_ctr.catalog.entity.CtrScriptItem";
    public static final String ENTITY_NAME = "ctrScriptItem";
    public static final int VERSION_HASH = 2144102191;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof CtrScriptItemGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CtrScriptItemGen> extends ScriptItem.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CtrScriptItem.class;
        }

        public T newInstance()
        {
            return (T) new CtrScriptItem();
        }
    }
    private static final Path<CtrScriptItem> _dslPath = new Path<CtrScriptItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CtrScriptItem");
    }
            

    public static class Path<E extends CtrScriptItem> extends ScriptItem.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return CtrScriptItem.class;
        }

        public String getEntityName()
        {
            return "ctrScriptItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
