package ru.tandemservice.unienr14_ctr.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводка о ходе приема, средних баллах и договорах на обучение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CtrReportEntrantsPlanPointsGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints";
    public static final String ENTITY_NAME = "ctrReportEntrantsPlanPoints";
    public static final int VERSION_HASH = 946662165;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_STAGE = "stage";
    public static final String P_REQUEST_TYPE = "requestType";
    public static final String P_PROGRAM_FORM = "programForm";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления добавлены с
    private Date _dateTo;     // Заявления добавлены по
    private String _stage;     // Стадия приемной кампании
    private String _requestType;     // Вид заявления
    private String _programForm;     // Форма обучения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления добавлены с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления добавлены по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     */
    @NotNull
    public String getStage()
    {
        return _stage;
    }

    /**
     * @param stage Стадия приемной кампании. Свойство не может быть null.
     */
    public void setStage(String stage)
    {
        dirty(_stage, stage);
        _stage = stage;
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRequestType()
    {
        return _requestType;
    }

    /**
     * @param requestType Вид заявления. Свойство не может быть null.
     */
    public void setRequestType(String requestType)
    {
        dirty(_requestType, requestType);
        _requestType = requestType;
    }

    /**
     * @return Форма обучения.
     */
    public String getProgramForm()
    {
        return _programForm;
    }

    /**
     * @param programForm Форма обучения.
     */
    public void setProgramForm(String programForm)
    {
        dirty(_programForm, programForm);
        _programForm = programForm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof CtrReportEntrantsPlanPointsGen)
        {
            setEnrollmentCampaign(((CtrReportEntrantsPlanPoints)another).getEnrollmentCampaign());
            setDateFrom(((CtrReportEntrantsPlanPoints)another).getDateFrom());
            setDateTo(((CtrReportEntrantsPlanPoints)another).getDateTo());
            setStage(((CtrReportEntrantsPlanPoints)another).getStage());
            setRequestType(((CtrReportEntrantsPlanPoints)another).getRequestType());
            setProgramForm(((CtrReportEntrantsPlanPoints)another).getProgramForm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CtrReportEntrantsPlanPointsGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CtrReportEntrantsPlanPoints.class;
        }

        public T newInstance()
        {
            return (T) new CtrReportEntrantsPlanPoints();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "stage":
                    return obj.getStage();
                case "requestType":
                    return obj.getRequestType();
                case "programForm":
                    return obj.getProgramForm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "stage":
                    obj.setStage((String) value);
                    return;
                case "requestType":
                    obj.setRequestType((String) value);
                    return;
                case "programForm":
                    obj.setProgramForm((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "stage":
                        return true;
                case "requestType":
                        return true;
                case "programForm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "stage":
                    return true;
                case "requestType":
                    return true;
                case "programForm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "stage":
                    return String.class;
                case "requestType":
                    return String.class;
                case "programForm":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CtrReportEntrantsPlanPoints> _dslPath = new Path<CtrReportEntrantsPlanPoints>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CtrReportEntrantsPlanPoints");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints#getStage()
     */
    public static PropertyPath<String> stage()
    {
        return _dslPath.stage();
    }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints#getRequestType()
     */
    public static PropertyPath<String> requestType()
    {
        return _dslPath.requestType();
    }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints#getProgramForm()
     */
    public static PropertyPath<String> programForm()
    {
        return _dslPath.programForm();
    }

    public static class Path<E extends CtrReportEntrantsPlanPoints> extends StorableReport.Path<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _stage;
        private PropertyPath<String> _requestType;
        private PropertyPath<String> _programForm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления добавлены с. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(CtrReportEntrantsPlanPointsGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления добавлены по. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(CtrReportEntrantsPlanPointsGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints#getStage()
     */
        public PropertyPath<String> stage()
        {
            if(_stage == null )
                _stage = new PropertyPath<String>(CtrReportEntrantsPlanPointsGen.P_STAGE, this);
            return _stage;
        }

    /**
     * @return Вид заявления. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints#getRequestType()
     */
        public PropertyPath<String> requestType()
        {
            if(_requestType == null )
                _requestType = new PropertyPath<String>(CtrReportEntrantsPlanPointsGen.P_REQUEST_TYPE, this);
            return _requestType;
        }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints#getProgramForm()
     */
        public PropertyPath<String> programForm()
        {
            if(_programForm == null )
                _programForm = new PropertyPath<String>(CtrReportEntrantsPlanPointsGen.P_PROGRAM_FORM, this);
            return _programForm;
        }

        public Class getEntityClass()
        {
            return CtrReportEntrantsPlanPoints.class;
        }

        public String getEntityName()
        {
            return "ctrReportEntrantsPlanPoints";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
