package ru.tandemservice.unienr14_ctr.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь абитуриента с договором на обучение
 *
 * Связывает договор на обучение и абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEntrantContractGen extends EntityBase
 implements IEduContractRelation, INaturalIdentifiable<EnrEntrantContractGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract";
    public static final String ENTITY_NAME = "enrEntrantContract";
    public static final int VERSION_HASH = 535941221;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTRACT_OBJECT = "contractObject";
    public static final String L_REQUESTED_COMPETITION = "requestedCompetition";
    public static final String P_PAYMENT_RESULT_EXISTS = "paymentResultExists";
    public static final String P_PAYMENT_DATE = "paymentDate";
    public static final String P_PAYMENT_EXISTS = "paymentExists";

    private CtrContractObject _contractObject;     // Договор на обучение
    private EnrRequestedCompetition _requestedCompetition;     // Выбранный конкурс абитуриента
    private boolean _paymentResultExists = false;     // В договоре есть платеж
    private Date _paymentDate;     // Дата поступления оплаты
    private boolean _paymentExists = false;     // Поступила оплата, достаточная для зачисления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор на обучение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public CtrContractObject getContractObject()
    {
        return _contractObject;
    }

    /**
     * @param contractObject Договор на обучение. Свойство не может быть null и должно быть уникальным.
     */
    public void setContractObject(CtrContractObject contractObject)
    {
        dirty(_contractObject, contractObject);
        _contractObject = contractObject;
    }

    /**
     * @return Выбранный конкурс абитуриента. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return _requestedCompetition;
    }

    /**
     * @param requestedCompetition Выбранный конкурс абитуриента. Свойство не может быть null.
     */
    public void setRequestedCompetition(EnrRequestedCompetition requestedCompetition)
    {
        dirty(_requestedCompetition, requestedCompetition);
        _requestedCompetition = requestedCompetition;
    }

    /**
     * Признак, показывающий, что в договоре есть платеж.
     * Обновляется автоматически демоном EnrEntractContractDaemonBean.
     *
     * @return В договоре есть платеж. Свойство не может быть null.
     */
    @NotNull
    public boolean isPaymentResultExists()
    {
        return _paymentResultExists;
    }

    /**
     * @param paymentResultExists В договоре есть платеж. Свойство не может быть null.
     */
    public void setPaymentResultExists(boolean paymentResultExists)
    {
        dirty(_paymentResultExists, paymentResultExists);
        _paymentResultExists = paymentResultExists;
    }

    /**
     * Сокр. название: Дата поступления оплаты.
     * Дата поступления оплаты, показывающая дату, когда абитуриент совершившил платеж по данному договору, достаточный для зачисления.
     * Заполняется оператором. Используется при зачислении.
     * Обновляется автоматически демоном EnrEntractContractDaemonBean.
     *
     * @return Дата поступления оплаты.
     */
    public Date getPaymentDate()
    {
        return _paymentDate;
    }

    /**
     * @param paymentDate Дата поступления оплаты.
     */
    public void setPaymentDate(Date paymentDate)
    {
        dirty(_paymentDate, paymentDate);
        _paymentDate = paymentDate;
    }

    /**
     * Сокр. название: Поступила оплата, дост. для зачисления.
     * Признак, показывающий, что абитуриент считается совершившим платеж по данному договору, достаточный для зачисления.
     * Заполняется формулой: TRUE при заполненном поле «Дата поступления оплаты», FALSE при не заполненном.
     *
     * @return Поступила оплата, достаточная для зачисления. Свойство не может быть null.
     *
     * Это формула "case when (paymentDate is null) then false else true end".
     */
    // @NotNull
    public boolean isPaymentExists()
    {
        return _paymentExists;
    }

    /**
     * @param paymentExists Поступила оплата, достаточная для зачисления. Свойство не может быть null.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setPaymentExists(boolean paymentExists)
    {
        dirty(_paymentExists, paymentExists);
        _paymentExists = paymentExists;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEntrantContractGen)
        {
            if (withNaturalIdProperties)
            {
                setContractObject(((EnrEntrantContract)another).getContractObject());
                setRequestedCompetition(((EnrEntrantContract)another).getRequestedCompetition());
            }
            setPaymentResultExists(((EnrEntrantContract)another).isPaymentResultExists());
            setPaymentDate(((EnrEntrantContract)another).getPaymentDate());
            setPaymentExists(((EnrEntrantContract)another).isPaymentExists());
        }
    }

    public INaturalId<EnrEntrantContractGen> getNaturalId()
    {
        return new NaturalId(getContractObject(), getRequestedCompetition());
    }

    public static class NaturalId extends NaturalIdBase<EnrEntrantContractGen>
    {
        private static final String PROXY_NAME = "EnrEntrantContractNaturalProxy";

        private Long _contractObject;
        private Long _requestedCompetition;

        public NaturalId()
        {}

        public NaturalId(CtrContractObject contractObject, EnrRequestedCompetition requestedCompetition)
        {
            _contractObject = ((IEntity) contractObject).getId();
            _requestedCompetition = ((IEntity) requestedCompetition).getId();
        }

        public Long getContractObject()
        {
            return _contractObject;
        }

        public void setContractObject(Long contractObject)
        {
            _contractObject = contractObject;
        }

        public Long getRequestedCompetition()
        {
            return _requestedCompetition;
        }

        public void setRequestedCompetition(Long requestedCompetition)
        {
            _requestedCompetition = requestedCompetition;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEntrantContractGen.NaturalId) ) return false;

            EnrEntrantContractGen.NaturalId that = (NaturalId) o;

            if( !equals(getContractObject(), that.getContractObject()) ) return false;
            if( !equals(getRequestedCompetition(), that.getRequestedCompetition()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getContractObject());
            result = hashCode(result, getRequestedCompetition());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getContractObject());
            sb.append("/");
            sb.append(getRequestedCompetition());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEntrantContractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEntrantContract.class;
        }

        public T newInstance()
        {
            return (T) new EnrEntrantContract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "contractObject":
                    return obj.getContractObject();
                case "requestedCompetition":
                    return obj.getRequestedCompetition();
                case "paymentResultExists":
                    return obj.isPaymentResultExists();
                case "paymentDate":
                    return obj.getPaymentDate();
                case "paymentExists":
                    return obj.isPaymentExists();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "contractObject":
                    obj.setContractObject((CtrContractObject) value);
                    return;
                case "requestedCompetition":
                    obj.setRequestedCompetition((EnrRequestedCompetition) value);
                    return;
                case "paymentResultExists":
                    obj.setPaymentResultExists((Boolean) value);
                    return;
                case "paymentDate":
                    obj.setPaymentDate((Date) value);
                    return;
                case "paymentExists":
                    obj.setPaymentExists((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "contractObject":
                        return true;
                case "requestedCompetition":
                        return true;
                case "paymentResultExists":
                        return true;
                case "paymentDate":
                        return true;
                case "paymentExists":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "contractObject":
                    return true;
                case "requestedCompetition":
                    return true;
                case "paymentResultExists":
                    return true;
                case "paymentDate":
                    return true;
                case "paymentExists":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "contractObject":
                    return CtrContractObject.class;
                case "requestedCompetition":
                    return EnrRequestedCompetition.class;
                case "paymentResultExists":
                    return Boolean.class;
                case "paymentDate":
                    return Date.class;
                case "paymentExists":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEntrantContract> _dslPath = new Path<EnrEntrantContract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEntrantContract");
    }
            

    /**
     * @return Договор на обучение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract#getContractObject()
     */
    public static CtrContractObject.Path<CtrContractObject> contractObject()
    {
        return _dslPath.contractObject();
    }

    /**
     * @return Выбранный конкурс абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract#getRequestedCompetition()
     */
    public static EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
    {
        return _dslPath.requestedCompetition();
    }

    /**
     * Признак, показывающий, что в договоре есть платеж.
     * Обновляется автоматически демоном EnrEntractContractDaemonBean.
     *
     * @return В договоре есть платеж. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract#isPaymentResultExists()
     */
    public static PropertyPath<Boolean> paymentResultExists()
    {
        return _dslPath.paymentResultExists();
    }

    /**
     * Сокр. название: Дата поступления оплаты.
     * Дата поступления оплаты, показывающая дату, когда абитуриент совершившил платеж по данному договору, достаточный для зачисления.
     * Заполняется оператором. Используется при зачислении.
     * Обновляется автоматически демоном EnrEntractContractDaemonBean.
     *
     * @return Дата поступления оплаты.
     * @see ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract#getPaymentDate()
     */
    public static PropertyPath<Date> paymentDate()
    {
        return _dslPath.paymentDate();
    }

    /**
     * Сокр. название: Поступила оплата, дост. для зачисления.
     * Признак, показывающий, что абитуриент считается совершившим платеж по данному договору, достаточный для зачисления.
     * Заполняется формулой: TRUE при заполненном поле «Дата поступления оплаты», FALSE при не заполненном.
     *
     * @return Поступила оплата, достаточная для зачисления. Свойство не может быть null.
     *
     * Это формула "case when (paymentDate is null) then false else true end".
     * @see ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract#isPaymentExists()
     */
    public static PropertyPath<Boolean> paymentExists()
    {
        return _dslPath.paymentExists();
    }

    public static class Path<E extends EnrEntrantContract> extends EntityPath<E>
    {
        private CtrContractObject.Path<CtrContractObject> _contractObject;
        private EnrRequestedCompetition.Path<EnrRequestedCompetition> _requestedCompetition;
        private PropertyPath<Boolean> _paymentResultExists;
        private PropertyPath<Date> _paymentDate;
        private PropertyPath<Boolean> _paymentExists;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор на обучение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract#getContractObject()
     */
        public CtrContractObject.Path<CtrContractObject> contractObject()
        {
            if(_contractObject == null )
                _contractObject = new CtrContractObject.Path<CtrContractObject>(L_CONTRACT_OBJECT, this);
            return _contractObject;
        }

    /**
     * @return Выбранный конкурс абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract#getRequestedCompetition()
     */
        public EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
        {
            if(_requestedCompetition == null )
                _requestedCompetition = new EnrRequestedCompetition.Path<EnrRequestedCompetition>(L_REQUESTED_COMPETITION, this);
            return _requestedCompetition;
        }

    /**
     * Признак, показывающий, что в договоре есть платеж.
     * Обновляется автоматически демоном EnrEntractContractDaemonBean.
     *
     * @return В договоре есть платеж. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract#isPaymentResultExists()
     */
        public PropertyPath<Boolean> paymentResultExists()
        {
            if(_paymentResultExists == null )
                _paymentResultExists = new PropertyPath<Boolean>(EnrEntrantContractGen.P_PAYMENT_RESULT_EXISTS, this);
            return _paymentResultExists;
        }

    /**
     * Сокр. название: Дата поступления оплаты.
     * Дата поступления оплаты, показывающая дату, когда абитуриент совершившил платеж по данному договору, достаточный для зачисления.
     * Заполняется оператором. Используется при зачислении.
     * Обновляется автоматически демоном EnrEntractContractDaemonBean.
     *
     * @return Дата поступления оплаты.
     * @see ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract#getPaymentDate()
     */
        public PropertyPath<Date> paymentDate()
        {
            if(_paymentDate == null )
                _paymentDate = new PropertyPath<Date>(EnrEntrantContractGen.P_PAYMENT_DATE, this);
            return _paymentDate;
        }

    /**
     * Сокр. название: Поступила оплата, дост. для зачисления.
     * Признак, показывающий, что абитуриент считается совершившим платеж по данному договору, достаточный для зачисления.
     * Заполняется формулой: TRUE при заполненном поле «Дата поступления оплаты», FALSE при не заполненном.
     *
     * @return Поступила оплата, достаточная для зачисления. Свойство не может быть null.
     *
     * Это формула "case when (paymentDate is null) then false else true end".
     * @see ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract#isPaymentExists()
     */
        public PropertyPath<Boolean> paymentExists()
        {
            if(_paymentExists == null )
                _paymentExists = new PropertyPath<Boolean>(EnrEntrantContractGen.P_PAYMENT_EXISTS, this);
            return _paymentExists;
        }

        public Class getEntityClass()
        {
            return EnrEntrantContract.class;
        }

        public String getEntityName()
        {
            return "enrEntrantContract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
