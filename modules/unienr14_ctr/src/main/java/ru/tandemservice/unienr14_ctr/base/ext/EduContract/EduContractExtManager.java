/* $Id$ */
package ru.tandemservice.unienr14_ctr.base.ext.EduContract;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Irina Ugfeld
 * @since 12.02.2016
 */
@Configuration
public class EduContractExtManager extends BusinessObjectExtensionManager {
}