/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.ext.EnrReportPerson.ui.Add.block.entrantData;

//import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 06.07.2015
 */
public class EnrCtrDataParam implements IReportDQLModifier
{
    private IReportParam<DataWrapper> _hasContract = new ReportParam<>();
    private IReportParam<DataWrapper> _hasContractPayment = new ReportParam<>();

    private String entrantAlias(ReportDQL dql)
    {
        return dql.innerJoinEntity(EnrEntrant.class, EnrEntrant.person());
    }

    private String requestAlias(ReportDQL dql)
    {
        // соединяем заявление
        return dql.innerJoinEntity(entrantAlias(dql), EnrEntrantRequest.class, EnrEntrantRequest.entrant());
    }

    private String reqCompAlias(ReportDQL dql)
    {
        return dql.innerJoinEntity(requestAlias(dql), EnrRequestedCompetition.class, EnrRequestedCompetition.request());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {
        //
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        boolean hasPaymentConditionInBuilder = false; // флаг, чтобы не дублировать условие на наличие договора

        if (_hasContract.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "enrCtrData.hasContract", ((ITitled) _hasContract.getData()).getTitle());

            final IDQLExpression expression = exists(
                    EnrEntrantContract.class,
                    EnrEntrantContract.requestedCompetition().s(), property(reqCompAlias(dql))
            );
            if (TwinComboDataSourceHandler.getSelectedValueNotNull(_hasContract.getData())) {
                dql.builder.where(expression);
                hasPaymentConditionInBuilder = true;
            } else {
                // если "нет", то те, у кого связей нет
                dql.builder.where(not(expression));
            }
        }

        if (_hasContractPayment.isActive())
        {
            printInfo.addPrintParam(EnrReportPersonAddUI.ENTRANT_SCHEET, "enrCtrData.hasContractPayment", ((ITitled) _hasContractPayment.getData()).getTitle());

            final IDQLExpression expression = exists(
                    EnrEntrantContract.class,
                    EnrEntrantContract.requestedCompetition().s(), property(reqCompAlias(dql)),
                    EnrEntrantContract.paymentExists().s(), true
            );
            if (TwinComboDataSourceHandler.getSelectedValueNotNull(_hasContractPayment.getData())) {
                dql.builder.where(expression);

            } else {
                if (!hasPaymentConditionInBuilder) {
                    dql.builder.where(exists(
                            EnrEntrantContract.class,
                            EnrEntrantContract.requestedCompetition().s(), property(reqCompAlias(dql))
                    ));
                }

                dql.builder.where(not(expression));
            }
        }
    }

    public IReportParam<DataWrapper> getHasContract()
    {
        return _hasContract;
    }

    public IReportParam<DataWrapper> getHasContractPayment()
    {
        return _hasContractPayment;
    }
}