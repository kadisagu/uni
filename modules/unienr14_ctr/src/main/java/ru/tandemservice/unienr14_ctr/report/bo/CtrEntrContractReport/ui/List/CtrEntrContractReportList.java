/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.ui.Pub.CtrEntrContractReportPub;
import ru.tandemservice.unienr14_ctr.report.entity.CtrEntrContractReport;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Stanislav Shibarshin
 * @since 03.10.2016
 */
@Configuration
public class CtrEntrContractReportList extends BusinessComponentManager
{

    private DQLOrderDescriptionRegistry _orderDescriptionRegistry = new DQLOrderDescriptionRegistry(CtrEntrContractReport.class, "r");

    public static final String REPORT_LIST_DS = "reportListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REPORT_LIST_DS, columnListHandler(), reportListDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reportListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName(), CtrEntrContractReport.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CtrEntrContractReport.class, "r");

                Long orgUnitId = context.get("orgUnitId");
                if (orgUnitId != null) {
                    builder.where(eq(property(CtrEntrContractReport.orgUnit().id().fromAlias("r")), value(orgUnitId)));
                }

                _orderDescriptionRegistry.applyOrder(builder, input.getEntityOrder());

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }


    @Bean
    public ColumnListExtPoint columnListHandler()
    {
        return columnListExtPointBuilder(REPORT_LIST_DS)
                .addColumn(publisherColumn(CtrEntrContractReport.P_FORMING_DATE, CtrEntrContractReport.formingDate())
                        .formatter(DateFormatter.DATE_FORMATTER_WITH_TIME)
                        .businessComponent(CtrEntrContractReportPub.class)
                        .order())
                .addColumn(textColumn("enrollmentCampaign", CtrEntrContractReport.enrollmentCampaign().title()))
                .addColumn(actionColumn("print", new Icon("printer"), "onClickPrint"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon("delete"), DELETE_LISTENER)
                        .alert(FormattedMessage.with().template("reportListDS.delete.alert")
                                .parameter(CtrEntrContractReport.formingDate().s()).create()).permissionKey("ui:deleteKey"))
                .create();
    }

}
