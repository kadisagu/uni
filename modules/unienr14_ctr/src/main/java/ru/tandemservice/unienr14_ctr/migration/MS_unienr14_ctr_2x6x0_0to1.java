package ru.tandemservice.unienr14_ctr.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_ctr_2x6x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.0")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantContract

        // создано обязательное свойство paymentExists
        {
            // создать колонку
            tool.createColumn("enr14_entrant_contract_t", new DBColumn("paymentexists_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultPaymentExists = Boolean.FALSE;
            tool.executeUpdate("update enr14_entrant_contract_t set paymentexists_p=? where paymentexists_p is null", defaultPaymentExists);

            // сделать колонку NOT NULL
            tool.setColumnNullable("enr14_entrant_contract_t", "paymentexists_p", false);

        }


    }
}