/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.*;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unienr14_ctr.report.entity.CtrEntrContractReport;

/**
 * @author Stanislav Shibarshin
 * @since 03.10.2016
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true),
})
public class CtrEntrContractReportPubUI extends UIPresenter
{

    private Long _reportId;
    private CtrEntrContractReport _report;
    private OrgUnit _orgUnit;
    private CommonPostfixPermissionModel _secModel;

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(_reportId);
        if (_report != null) {
            _orgUnit = _report.getOrgUnit();
            if (_orgUnit != null) {
                _secModel = new OrgUnitSecModel(_orgUnit);
            }
        }
    }

    public void onClickPrint()
    {
        if (getReport().getContent().getContent() == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(getReport().getContent()).rtf(), true);
    }

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public CtrEntrContractReport getReport()
    {
        return _report;
    }

    public void setReport(CtrEntrContractReport report)
    {
        _report = report;
    }

    public String getViewKey()
    {
        return _orgUnit == null ? "ctrEntrContractReport" : _secModel.getPermission("orgUnit_viewCtrEntrContractReport");
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _orgUnit != null ? _orgUnit : super.getSecuredObject();
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        this._secModel = secModel;
    }
}
