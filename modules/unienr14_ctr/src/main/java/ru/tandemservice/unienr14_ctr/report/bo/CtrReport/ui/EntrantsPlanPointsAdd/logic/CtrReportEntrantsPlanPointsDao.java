/* $Id:$ */
package ru.tandemservice.unienr14_ctr.report.bo.CtrReport.ui.EntrantsPlanPointsAdd.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.RtfRowIntercepterRawText;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.node.RtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.catalog.entity.CtrScriptItem;
import ru.tandemservice.unienr14_ctr.catalog.entity.codes.CtrScriptItemCodes;
import ru.tandemservice.unienr14_ctr.report.bo.CtrReport.ui.EntrantsPlanPointsAdd.CtrReportEntrantsPlanPointsAddUI;
import ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 23.07.2014
 */
public class CtrReportEntrantsPlanPointsDao extends UniBaseDao implements ICtrReportEntrantsPlanPointsDao {

    Integer rowsCount = 0;

    @Override
    public long createReport(CtrReportEntrantsPlanPointsAddUI model) {
        CtrReportEntrantsPlanPoints report = new CtrReportEntrantsPlanPoints();
        
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        report.setStage(model.getStageSelector().getStage().getTitle());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, CtrReportEntrantsPlanPoints.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, CtrReportEntrantsPlanPoints.P_PROGRAM_FORM, "title");

        DatabaseFile content = new DatabaseFile();


        content.setContent(buildReport(model));

        content.setFilename("EnrReportOrgUnitEnrollmentResults.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    private byte[] buildReport(CtrReportEntrantsPlanPointsAddUI model)
    {
        NumberFormat formatter = new DecimalFormat("#0.00");

        // rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(CtrScriptItem.class, CtrScriptItemCodes.REPORT_ENTRANT_PLAN_POINTS);


        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());


        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        model.getStageSelector().applyFilter(requestedCompDQL, "reqComp");

        requestedCompDQL.column(property("reqComp"));

        List<EnrRequestedCompetition> fromQueryReqComps = getList(requestedCompDQL);


        requestedCompDQL.resetColumns()
                .joinEntity("reqComp", DQLJoinType.left, EnrRequestedProgram.class, "reqProgram", eq(property(EnrRequestedProgram.requestedCompetition().fromAlias("reqProgram")), property("reqComp")))
                .column(property("reqProgram"));

        List<EnrRequestedProgram> fromQueryReqPrograms = getList(requestedCompDQL);



        Set<EnrRequestedCompetition> requestedCompetitionSet = new HashSet<>();




        // Карта с ВВИ
        requestedCompDQL.resetColumns()
                .joinEntity("reqComp", DQLJoinType.inner, EnrChosenEntranceExam.class, "vvi", eq(property(EnrChosenEntranceExam.requestedCompetition().fromAlias("vvi")), property("reqComp")))
                .column(property("vvi"));
        List<EnrChosenEntranceExam> fromQueryExams = getList(requestedCompDQL);
        Map<EnrRequestedCompetition, Set<EnrChosenEntranceExam>> examMap = SafeMap.get(HashSet.class);
        for (EnrChosenEntranceExam exam : fromQueryExams) {
            examMap.get(exam.getRequestedCompetition()).add(exam);
        }

        // Карта с ВВИ-ф
        requestedCompDQL.resetColumns()
                .joinEntity("vvi", DQLJoinType.inner, EnrChosenEntranceExamForm.class, "vvif", eq(property(EnrChosenEntranceExamForm.chosenEntranceExam().fromAlias("vvif")), property("vvi")))
                .column(property("vvif"));
        List<EnrChosenEntranceExamForm> fromQueryExamForms = getList(requestedCompDQL);
        Map<EnrRequestedCompetition, Set<EnrChosenEntranceExamForm>> examFormMap = SafeMap.get(HashSet.class);
        for (EnrChosenEntranceExamForm exam : fromQueryExamForms) {
            examFormMap.get(exam.getChosenEntranceExam().getRequestedCompetition()).add(exam);
        }

        // Компаратор шорттайтлов
        Comparator<EduProgramSubject> subjectComparator = new Comparator<EduProgramSubject>() {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2) {
                if (o1.getShortTitle() == null)
                    throw new ApplicationException("Невозможно сформировать отчет, т.к. для направления подготовки «" + o1.getTitleWithCode() + "» не задано сокращенное название.");
                if (o2.getShortTitle() == null)
                    throw new ApplicationException("Невозможно сформировать отчет, т.к. для направления подготовки «" + o2.getTitleWithCode() + "» не задано сокращенное название.");

                int result = o1.getShortTitle().compareToIgnoreCase(o2.getShortTitle());
                if (result != 0) return result;
                return o1.getTitleWithCode().compareToIgnoreCase(o2.getTitleWithCode());
            }
        };
        Comparator<EduProgramSpecialization> specializationComparator = new Comparator<EduProgramSpecialization>() {
            @Override
            public int compare(EduProgramSpecialization o1, EduProgramSpecialization o2) {
                if (o1 == o2) return 0;
                if (o1 == null) return 1;
                if (o2 == null) return -1;
                int res = o1.getShortTitle().compareToIgnoreCase(o2.getShortTitle());
                if (res != 0) return res;
                return o1.getTitle().compareTo(o2.getTitle());
            }
        };

        // Карта с основными данными
        Map<EduProgramSubject, Map<EduProgramSpecialization, Map<EnrRequestedCompetition, Set<EnrRequestedProgram>>>> dataMap = new TreeMap<>(subjectComparator);

        for (EnrRequestedCompetition reqComp : fromQueryReqComps) {
            EduProgramSubject subject = reqComp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();
            EduProgramSpecialization specialization = null;

            requestedCompetitionSet.add(reqComp);

            final Map<EduProgramSpecialization, Map<EnrRequestedCompetition, Set<EnrRequestedProgram>>> eduProgramSpecializationMapMap = SafeMap.safeGet(dataMap, subject, TreeMap.class, specializationComparator);
            final Map<EnrRequestedCompetition, Set<EnrRequestedProgram>> enrRequestedCompetitionSetMap = SafeMap.safeGet(eduProgramSpecializationMapMap, specialization, HashMap.class);
            SafeMap.safeGet(enrRequestedCompetitionSetMap, reqComp, HashSet.class);
        }


        for (EnrRequestedProgram reqProgram : fromQueryReqPrograms) {
            if (reqProgram != null) {
                EduProgramSubject subject = reqProgram.getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject();
                EduProgramSpecialization specialization = reqProgram.getProgramSetItem().getProgram().getProgramSpecialization();
                EnrRequestedCompetition reqComp = reqProgram.getRequestedCompetition();


                // если у направленности нет сокращенного названия, формируем его.
                if (null == specialization.getShortTitle()) {
                    StringBuilder makeShortTitle = new StringBuilder();
                    for (String s : specialization.getTitle().split(" ")) {
                        makeShortTitle.append(s.charAt(0));
                    }
                    specialization.setShortTitle(makeShortTitle.toString());
                }


                requestedCompetitionSet.add(reqComp);


                final Map<EduProgramSpecialization, Map<EnrRequestedCompetition, Set<EnrRequestedProgram>>> eduProgramSpecializationMapMap = SafeMap.safeGet(dataMap, subject, TreeMap.class, specializationComparator);
                final Map<EnrRequestedCompetition, Set<EnrRequestedProgram>> enrRequestedCompetitionSetMap = SafeMap.safeGet(eduProgramSpecializationMapMap, specialization, HashMap.class);
                final Set<EnrRequestedProgram> enrRequestedPrograms = SafeMap.safeGet(enrRequestedCompetitionSetMap, reqComp, HashSet.class);
                enrRequestedPrograms.add(reqProgram);

            }
        }


        // Карта с договорами
        DQLSelectBuilder contractsSelectBuilder = new DQLSelectBuilder()
                .fromEntity(EnrEntrantContract.class, "contract")
                .where(in(property(EnrEntrantContract.requestedCompetition().fromAlias("contract")), requestedCompetitionSet));
        List<EnrEntrantContract> fromQueryContractList = getList(contractsSelectBuilder);
        Map<EnrRequestedCompetition, Set<EnrEntrantContract>> contractMap = SafeMap.get(HashSet.class);
        for (EnrEntrantContract contract : fromQueryContractList) {
            contractMap.get(contract.getRequestedCompetition()).add(contract);
        }

        DQLSelectBuilder competitionSelectBuilder = new DQLSelectBuilder()
                .fromEntity(EnrCompetition.class, "comp")
                .where(in(property(EnrCompetition.programSetOrgUnit().programSet().programSubject().fromAlias("comp")), dataMap.keySet()))
                .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().enrollmentCampaign().fromAlias("comp")), value(model.getEnrollmentCampaign())))
                .column(property("comp"));

        List<EnrCompetition> fromQueryCompList = getList(competitionSelectBuilder);

        Map<EduProgramSubject, Set<EnrProgramSetOrgUnit>> programSetOrgUnitsMap = SafeMap.get(HashSet.class);
        for (EnrCompetition competition : fromQueryCompList) {
            programSetOrgUnitsMap.get(competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject()).add(competition.getProgramSetOrgUnit());
        }

        String reqTypeCode = ((EnrRequestType)model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE).getValue()).getCode();

        // Таблицы
        List<String[]> budgetTable = new ArrayList<>();
        List<String[]> contractTable = new ArrayList<>();
        List<String> budgetHeader = formBudgetHeader(reqTypeCode);
        budgetTable.add(budgetHeader.toArray(new String[budgetHeader.size()]));

        List<String> contractHeader = formContractHeader();
        contractTable.add(contractHeader.toArray(new String[contractHeader.size()]));

        List<String> descriptors = new ArrayList<>();

        List<Double> uniTotalAvgs1 = new ArrayList<>();
        List<Double> uniTotalAvgs2 = new ArrayList<>();
        List<Double> uniTotalAvgs3 = new ArrayList<>();

        Integer uniTotalBudgetPlan = 0;
        Integer uniTotalBudgetRequests = 0;
        Integer uniTotalTAPlan = 0;
        Integer uniTotalTARequests = 0;
        Integer uniTotalBudgetOriginHandedIn = 0;
        Integer uniTotalExclusive = 0;
        Integer uniTotalNoExam = 0;
        Integer uniTotalContractPlan = 0;
        Integer uniTotalContractRequests = 0;
        Integer uniTotalContractOriginHandedIn = 0;
        Integer uniTotalContractPayed = 0;
        // Выводить ли сообщение "Не у всех абитуриентов выбраны приоритеты для образовательных программ"
        boolean showWarnMessage = false;

        // Заполняем таблицы
        for (Map.Entry<EduProgramSubject, Map<EduProgramSpecialization, Map<EnrRequestedCompetition, Set<EnrRequestedProgram>>>> subjectEntry : dataMap.entrySet()) {

            //Set<EnrProgramSetOrgUnit> programSetOrgUnits = new HashSet<>();

            Integer onSubjectBudgetTotalReq = 0;
            Set<EnrEntrantRequest> onSubjectUniqueBudgetRequests = new HashSet<>();
            Integer onSubjectTargetAdmReq = 0;
            Set<EnrEntrantRequest> onSubjectUniqueContractRequests = new HashSet<>();
            Integer onSubjectContractReq = 0;

            Set<EnrEntrantRequest> notTARequests = new HashSet<>();

            Set<EnrEntrantRequest> onSubjectBudgetOriginDocsHandedIn = new HashSet<>();
            Set<EnrEntrantRequest> onSubjectContractOriginDocsHandedIn = new HashSet<>();
            Integer onSubjectNoExam = 0;
            Integer onSubjectExclusive = 0;

            Integer onSubjectPayedContracts = 0;

            List<Double> onSubjectAverageMarks1 = new ArrayList<>();
            List<Double> onSubjectAverageMarks2 = new ArrayList<>();
            List<Double> onSubjectAverageMarks3 = new ArrayList<>();

            List<String[]> inSubjectBudgetSpecializationsTable = new ArrayList<>();
            List<String[]> inSubjectContractSpecializationsTable = new ArrayList<>();

            List<String> onSpecDescriptors = new ArrayList<>();

            // По направленности
            for (Map.Entry<EduProgramSpecialization, Map<EnrRequestedCompetition, Set<EnrRequestedProgram>>> specializationEntry : subjectEntry.getValue().entrySet()) {
                Set<EnrEntrantRequest> onSpecializationBudgetTotalRequests = new HashSet<>();
                Set<EnrEntrantRequest> onSpecializationTargetAdmRequests = new HashSet<>();
                Set<EnrEntrantRequest> onSpecializationContractRequests = new HashSet<>();

                Set<EnrRequestedCompetition> onSpecializationPayedContracts = new HashSet<>();

                Set<EnrEntrantRequest> onSpecializationBudgetOriginDocsHandedIn = new HashSet<>();
                Set<EnrEntrantRequest> onSpecializationContractOriginDocsHandedIn = new HashSet<>();

                Set<EnrEntrantRequest> onSpecializationNoExamRequests = new HashSet<>();
                Set<EnrEntrantRequest> onSpecializationExclusiveRequests = new HashSet<>();

                List<Double> onSpecAverageMarks1 = new ArrayList<>();
                List<Double> onSpecAverageMarks2 = new ArrayList<>();
                List<Double> onSpecAverageMarks3 = new ArrayList<>();

                List<String> budgetRow = new ArrayList<>();
                List<String> contractRow = new ArrayList<>();

                // Высчитываем данные
                for (Map.Entry<EnrRequestedCompetition, Set<EnrRequestedProgram>> reqCompEntry : specializationEntry.getValue().entrySet()) {
                    EnrRequestedCompetition reqComp = reqCompEntry.getKey();

                    //programSetOrgUnits.add(reqComp.getCompetition().getProgramSetOrgUnit());

                    boolean budget = reqComp.getCompetition().getType().getCompensationType().isBudget();
                    boolean target = false;

                    if (budget) {
                        onSpecializationBudgetTotalRequests.add(reqComp.getRequest());
                        onSubjectUniqueBudgetRequests.add(reqComp.getRequest());
                        if (reqComp instanceof EnrRequestedCompetitionTA) {
                            onSpecializationTargetAdmRequests.add(reqComp.getRequest());
                            target = true;
                        } else if (reqComp instanceof EnrRequestedCompetitionNoExams)
                            onSpecializationNoExamRequests.add(reqComp.getRequest());
                        else if (reqComp instanceof EnrRequestedCompetitionExclusive)
                            onSpecializationExclusiveRequests.add(reqComp.getRequest());

                        if (reqComp.getPriority() == 1 && reqComp.isOriginalDocumentHandedIn())
                            onSubjectBudgetOriginDocsHandedIn.add(reqComp.getRequest());

                        if (!target)
                            notTARequests.add(reqComp.getRequest());

                        // Свитч, зависящий от выбранного типа заявления
                        switch (reqTypeCode) {
                            case (EnrRequestTypeCodes.BS): {
                                if (examMap.containsKey(reqComp))
                                    for (EnrChosenEntranceExam exam : examMap.get(reqComp)) {
                                        if (exam.getMaxMarkForm() != null && exam.getMaxMarkForm().getPassForm().getCode().equals(EnrExamPassFormCodes.STATE_EXAM) && exam.getMarkAsDouble() != null && exam.getMarkAsDouble() != 0.0) {
                                            onSpecAverageMarks1.add(exam.getMarkAsDouble());
                                            uniTotalAvgs1.add(exam.getMarkAsDouble());
                                            if (!target) {
                                                onSpecAverageMarks2.add(exam.getMarkAsDouble());
                                                uniTotalAvgs2.add(exam.getMarkAsDouble());
                                            } else {
                                                onSpecAverageMarks3.add(exam.getMarkAsDouble());
                                                uniTotalAvgs3.add(exam.getMarkAsDouble());
                                            }
                                        }
                                    }
                                break;
                            }
                            case (EnrRequestTypeCodes.MASTER): {
                                if (examFormMap.containsKey(reqComp))
                                    for (EnrChosenEntranceExamForm examForm : examFormMap.get(reqComp)) {
                                        if (examForm.getMarkAsDoubleNullSafe() != 0) {
                                            onSpecAverageMarks1.add(examForm.getMarkAsDoubleNullSafe());
                                            uniTotalAvgs1.add(examForm.getMarkAsDoubleNullSafe());
                                        }
                                    }
                                break;
                            }
                            case (EnrRequestTypeCodes.HIGHER):
                            case (EnrRequestTypeCodes.POSTGRADUATE):
                            case (EnrRequestTypeCodes.TRAINEESHIP):
                            case (EnrRequestTypeCodes.INTERNSHIP):
                            {
                                if (examFormMap.containsKey(reqComp))
                                    for (EnrChosenEntranceExamForm examForm : examFormMap.get(reqComp)) {
                                        if (examForm.getMarkAsDoubleNullSafe() != 0) {
                                            onSpecAverageMarks1.add(examForm.getMarkAsDoubleNullSafe());
                                            uniTotalAvgs1.add(examForm.getMarkAsDoubleNullSafe());
                                        }
                                    }
                                if (examMap.containsKey(reqComp))
                                    for (EnrChosenEntranceExam exam : examMap.get(reqComp)) {
                                        if (exam.getActualExam().getPriority() == 1 && exam.getMarkAsDouble() != null && exam.getMarkAsDouble() != 0.0) {
                                            onSpecAverageMarks2.add(exam.getMarkAsDouble());
                                            uniTotalAvgs2.add(exam.getMarkAsDouble());
                                        }
                                    }
                                break;
                            }
                            case (EnrRequestTypeCodes.SPO): {
                                if (reqComp.getRequest().getEduDocument().getAvgMarkAsDouble() != null && reqComp.getRequest().getEduDocument().getAvgMarkAsDouble() != 0.0) {
                                    onSpecAverageMarks1.add(reqComp.getRequest().getEduDocument().getAvgMarkAsDouble());
                                    uniTotalAvgs1.add(reqComp.getRequest().getEduDocument().getAvgMarkAsDouble());
                                }
                                if (examFormMap.containsKey(reqComp))
                                    for (EnrChosenEntranceExamForm examForm : examFormMap.get(reqComp)) {
                                        if (examForm.getMarkAsDoubleNullSafe() != 0) {
                                            onSpecAverageMarks2.add(examForm.getMarkAsDoubleNullSafe());
                                            uniTotalAvgs2.add(examForm.getMarkAsDoubleNullSafe());
                                        }
                                    }
                                break;
                            }
                        }
                    } else {
                        onSpecializationContractRequests.add(reqComp.getRequest());
                        onSubjectUniqueContractRequests.add(reqComp.getRequest());
                        if (reqComp.getPriority() == 1 && reqComp.isOriginalDocumentHandedIn())
                            onSubjectContractOriginDocsHandedIn.add(reqComp.getRequest());

                        if (contractMap.containsKey(reqComp)) {
                            for (EnrEntrantContract contract : contractMap.get(reqComp)) {
                                if (contract.isPaymentExists()) {
                                    onSpecializationPayedContracts.add(reqComp);
                                    break;
                                }
                            }
                        }
                    }

                    for (EnrRequestedProgram requestedProgram : reqCompEntry.getValue()) {
                        if (requestedProgram.getPriority() == 1)
                            if (reqComp.getPriority() == 1 && reqComp.isOriginalDocumentHandedIn()) {
                                if (budget)
                                    onSpecializationBudgetOriginDocsHandedIn.add(reqComp.getRequest());
                                else
                                    onSpecializationContractOriginDocsHandedIn.add(reqComp.getRequest());
                            }
                    }
                }

                if (specializationEntry.getKey() != null) {
                    // Формируем столбец по направленности
                    onSpecDescriptors.add(specializationEntry.getKey().getShortTitle() + " - " + specializationEntry.getKey().getTitle());
                    budgetRow.add(specializationEntry.getKey().getShortTitle());

                    budgetRow.add("-");
                    budgetRow.add(String.valueOf(onSpecializationBudgetTotalRequests.size()));
                    onSubjectBudgetTotalReq += onSpecializationBudgetTotalRequests.size();
                    if (reqTypeCode.equals(EnrRequestTypeCodes.BS) || reqTypeCode.equals(EnrRequestTypeCodes.MASTER)) {
                        budgetRow.add("-");
                        budgetRow.add("-");
                        budgetRow.add("-");
                        budgetRow.add(String.valueOf(onSpecializationTargetAdmRequests.size()));
                    }
                    onSubjectTargetAdmReq += onSpecializationTargetAdmRequests.size();
                    budgetRow.add("-");
                    budgetRow.add(String.valueOf(onSpecializationBudgetOriginDocsHandedIn.size()));
                    budgetRow.add(String.valueOf(onSpecializationExclusiveRequests.size()));
                    onSubjectExclusive += onSpecializationExclusiveRequests.size();
                    budgetRow.add(String.valueOf(onSpecializationNoExamRequests.size()));
                    onSubjectNoExam += onSpecializationNoExamRequests.size();
                    switch (reqTypeCode) {
                        case (EnrRequestTypeCodes.BS): {
                            Double average1 = countAverage(onSpecAverageMarks1);
                            if (average1 != 0.0) {
                                budgetRow.add(formatter.format(average1));
                                onSubjectAverageMarks1.add(average1);
                            } else budgetRow.add("-");
                            Double average2 = countAverage(onSpecAverageMarks2);
                            if (average2 != 0.0) {
                                budgetRow.add(formatter.format(average2));
                                onSubjectAverageMarks2.add(average2);
                            } else budgetRow.add("-");
                            Double average3 = countAverage(onSpecAverageMarks3);
                            if (average3 != 0.0) {
                                budgetRow.add(formatter.format(average3));
                                onSubjectAverageMarks3.add(average3);
                            } else budgetRow.add("-");
                            break;
                        }
                        case (EnrRequestTypeCodes.MASTER): {
                            Double average1 = countAverage(onSpecAverageMarks1);
                            if (average1 != 0.0) {
                                budgetRow.add(formatter.format(average1));
                                onSubjectAverageMarks1.add(average1);
                            } else budgetRow.add("-");
                            break;
                        }
                        case (EnrRequestTypeCodes.HIGHER):
                        case (EnrRequestTypeCodes.POSTGRADUATE):
                        case (EnrRequestTypeCodes.TRAINEESHIP):
                        case (EnrRequestTypeCodes.INTERNSHIP):
                        {
                            Double average1 = countAverage(onSpecAverageMarks1);
                            if (average1 != 0.0) {
                                budgetRow.add(formatter.format(average1));
                                onSubjectAverageMarks1.add(average1);
                            } else budgetRow.add("-");
                            Double average2 = countAverage(onSpecAverageMarks2);
                            if (average2 != 0.0) {
                                budgetRow.add(formatter.format(average2));
                                onSubjectAverageMarks2.add(average2);
                            } else budgetRow.add("-");
                            break;
                        }
                        case (EnrRequestTypeCodes.SPO): {
                            Double average1 = countAverage(onSpecAverageMarks1);
                            if (average1 != 0.0) {
                                budgetRow.add(formatter.format(average1));
                                onSubjectAverageMarks1.add(average1);
                            } else budgetRow.add("-");
                            Double average2 = countAverage(onSpecAverageMarks2);
                            if (average2 != 0.0) {
                                budgetRow.add(formatter.format(average2));
                                onSubjectAverageMarks2.add(average2);
                            } else budgetRow.add("-");
                            break;
                        }
                    }

                    contractRow.add(specializationEntry.getKey().getShortTitle());
                    contractRow.add("-");
                    contractRow.add(String.valueOf(onSpecializationContractRequests.size()));
                    onSubjectContractReq += onSpecializationContractRequests.size();
                    contractRow.add(String.valueOf(onSpecializationContractOriginDocsHandedIn.size()));
                    contractRow.add(String.valueOf(onSpecializationPayedContracts.size()));
                    onSubjectPayedContracts += onSpecializationPayedContracts.size();

                    inSubjectBudgetSpecializationsTable.add(budgetRow.toArray(new String[budgetRow.size()]));
                    inSubjectContractSpecializationsTable.add(contractRow.toArray(new String[contractRow.size()]));
                } else {
                    onSubjectBudgetTotalReq += onSpecializationBudgetTotalRequests.size();
                    onSubjectTargetAdmReq += onSpecializationTargetAdmRequests.size();
                    onSubjectExclusive += onSpecializationExclusiveRequests.size();
                    onSubjectNoExam += onSpecializationNoExamRequests.size();
                    switch (reqTypeCode) {
                        case (EnrRequestTypeCodes.BS): {
                            Double average1 = countAverage(onSpecAverageMarks1);
                            if (average1 != 0.0) {
                                onSubjectAverageMarks1.add(average1);
                            } else budgetRow.add("-");
                            Double average2 = countAverage(onSpecAverageMarks2);
                            if (average2 != 0.0) {
                                onSubjectAverageMarks2.add(average2);
                            } else budgetRow.add("-");
                            Double average3 = countAverage(onSpecAverageMarks3);
                            if (average3 != 0.0) {
                                onSubjectAverageMarks3.add(average3);
                            } else budgetRow.add("-");
                            break;
                        }
                        case (EnrRequestTypeCodes.MASTER): {
                            Double average1 = countAverage(onSpecAverageMarks1);
                            if (average1 != 0.0) {
                                onSubjectAverageMarks1.add(average1);
                            } else budgetRow.add("-");
                            break;
                        }
                        case (EnrRequestTypeCodes.HIGHER):
                        case (EnrRequestTypeCodes.POSTGRADUATE):
                        case (EnrRequestTypeCodes.TRAINEESHIP):
                        case (EnrRequestTypeCodes.INTERNSHIP):
                        {
                            Double average1 = countAverage(onSpecAverageMarks1);
                            if (average1 != 0.0) {
                                onSubjectAverageMarks1.add(average1);
                            } else budgetRow.add("-");
                            Double average2 = countAverage(onSpecAverageMarks2);
                            if (average2 != 0.0) {
                                onSubjectAverageMarks2.add(average2);
                            } else budgetRow.add("-");
                            break;
                        }
                        case (EnrRequestTypeCodes.SPO): {
                            Double average1 = countAverage(onSpecAverageMarks1);
                            if (average1 != 0.0) {
                                onSubjectAverageMarks1.add(average1);
                            } else budgetRow.add("-");
                            Double average2 = countAverage(onSpecAverageMarks2);
                            if (average2 != 0.0) {
                                onSubjectAverageMarks2.add(average2);
                            } else budgetRow.add("-");
                            break;
                        }
                    }

                    onSubjectContractReq += onSpecializationContractRequests.size();
                    onSubjectPayedContracts += onSpecializationPayedContracts.size();

                }
            }

            // Формируем столбец по направлению
            Integer budgetPlan = 0;
            Integer taPlan = 0;
            Integer contractPlan = 0;


            for (EnrProgramSetOrgUnit orgUnit : programSetOrgUnitsMap.get(subjectEntry.getKey())) {
                budgetPlan += orgUnit.getMinisterialPlan();
                taPlan += orgUnit.getTargetAdmPlan();
                contractPlan += orgUnit.getContractPlan();
            }

            List<String> budgetRow = new ArrayList<>();
            List<String> contractRow = new ArrayList<>();
            String description = subjectEntry.getKey().getShortTitle() + " - " + subjectEntry.getKey().getTitle();
            budgetRow.add("\\b " + subjectEntry.getKey().getShortTitle());

            if (budgetPlan == 0) {
                budgetRow.add("\\b -");
            } else {
                budgetRow.add("\\b " + String.valueOf(budgetPlan));
                uniTotalBudgetPlan += budgetPlan;
            }


            if (onSubjectUniqueBudgetRequests.size() == onSubjectBudgetTotalReq) {
                budgetRow.add("\\b " + String.valueOf(onSubjectUniqueBudgetRequests.size()));
            } else {
                budgetRow.add("\\b\\cf1 " + String.valueOf(onSubjectUniqueBudgetRequests.size()));
                showWarnMessage = true;

            }

            uniTotalBudgetRequests += onSubjectUniqueBudgetRequests.size();
            if (budgetPlan != 0) {
                Double competitionBudget = 1.0 * onSubjectBudgetTotalReq / budgetPlan;
                budgetRow.add("\\b " + formatter.format(competitionBudget));
            } else {
                budgetRow.add("\\b -");
            }
            if (reqTypeCode.equals(EnrRequestTypeCodes.BS) || reqTypeCode.equals(EnrRequestTypeCodes.MASTER)) {


                int notTAPlan = budgetPlan - taPlan;
                if (notTAPlan != 0) {
                    Double competitionNotTA = 1.0 * (notTARequests.size()) / notTAPlan;
                    budgetRow.add("\\b " + formatter.format(competitionNotTA));
                } else {
                    budgetRow.add("\\b -");
                }
                if (taPlan != 0) {
                    budgetRow.add("\\b " + String.valueOf(taPlan));
                    uniTotalTAPlan += taPlan;
                    budgetRow.add("\\b " + String.valueOf(onSubjectTargetAdmReq));
                    uniTotalTARequests += onSubjectTargetAdmReq;
                    Double competititonTA = 1.0 * onSubjectTargetAdmReq / taPlan;
                    budgetRow.add("\\b " + formatter.format(competititonTA));
                } else {
                    budgetRow.add("\\b -");
                    budgetRow.add("\\b " + String.valueOf(onSubjectTargetAdmReq));
                    budgetRow.add("\\b -");
                }
            }
            budgetRow.add("\\b " + String.valueOf(onSubjectBudgetOriginDocsHandedIn.size()));
            uniTotalBudgetOriginHandedIn += onSubjectBudgetOriginDocsHandedIn.size();
            budgetRow.add("\\b " + String.valueOf(onSubjectExclusive));
            uniTotalExclusive += onSubjectExclusive;
            budgetRow.add("\\b " + String.valueOf(onSubjectNoExam));
            uniTotalNoExam += onSubjectNoExam;
            switch (reqTypeCode) {
                case (EnrRequestTypeCodes.BS): {
                    Double average1 = countAverage(onSubjectAverageMarks1);
                    if (average1 != 0)
                        budgetRow.add("\\b " + formatter.format(average1));
                    else budgetRow.add("\\b - ");
                    Double average2 = countAverage(onSubjectAverageMarks2);
                    if (average2 != 0)
                        budgetRow.add("\\b " + formatter.format(average2));
                    else budgetRow.add("\\b - ");
                    Double average3 = countAverage(onSubjectAverageMarks3);
                    if (average3 != 0)
                        budgetRow.add("\\b " + formatter.format(average3));
                    else budgetRow.add("\\b - ");
                    break;
                }
                case (EnrRequestTypeCodes.MASTER): {
                    Double average1 = countAverage(onSubjectAverageMarks1);
                    if (average1 != 0)
                        budgetRow.add("\\b " + formatter.format(average1));
                    else budgetRow.add("\\b - ");
                    break;
                }
                case (EnrRequestTypeCodes.HIGHER):
                case (EnrRequestTypeCodes.POSTGRADUATE) :
                case (EnrRequestTypeCodes.TRAINEESHIP):
                case (EnrRequestTypeCodes.INTERNSHIP): {
                    Double average1 = countAverage(onSubjectAverageMarks1);
                    if (average1 != 0)
                        budgetRow.add("\\b " + formatter.format(average1));
                    else budgetRow.add("\\b - ");
                    Double average2 = countAverage(onSubjectAverageMarks2);
                    if (average2 != 0)
                        budgetRow.add("\\b " + formatter.format(average2));
                    else budgetRow.add("\\b - ");
                    break;
                }
                case (EnrRequestTypeCodes.SPO): {
                    Double average1 = countAverage(onSubjectAverageMarks1);
                    if (average1 != 0)
                        budgetRow.add("\\b " + formatter.format(average1));
                    else budgetRow.add("\\b - ");
                    Double average2 = countAverage(onSubjectAverageMarks2);
                    if (average2 != 0)
                        budgetRow.add("\\b " + formatter.format(average2));
                    else budgetRow.add("\\b - ");
                    break;
                }
            }

            contractRow.add("\\b " + subjectEntry.getKey().getShortTitle());
            if (contractPlan == 0) {
                contractRow.add("\\b -");
            } else {
                contractRow.add("\\b " + String.valueOf(contractPlan));
                uniTotalContractPlan += contractPlan;
            }

            if (onSubjectUniqueContractRequests.size() == onSubjectContractReq)
                contractRow.add("\\b " + String.valueOf(onSubjectUniqueContractRequests.size()));
            else {
                contractRow.add("\\b\\cf1 " + String.valueOf(onSubjectUniqueContractRequests.size()));
                showWarnMessage = true;
            }

            uniTotalContractRequests += onSubjectUniqueContractRequests.size();

            contractRow.add("\\b " + String.valueOf(onSubjectContractOriginDocsHandedIn.size()));
            uniTotalContractOriginHandedIn += onSubjectContractOriginDocsHandedIn.size();
            contractRow.add("\\b " + String.valueOf(onSubjectPayedContracts));
            uniTotalContractPayed += onSubjectPayedContracts;

            // Если в направлении 1 направленность, то столбец с ней в документ не выводится
            if (inSubjectBudgetSpecializationsTable.size() > 1) {
                contractTable.addAll(inSubjectContractSpecializationsTable);
                budgetTable.addAll(inSubjectBudgetSpecializationsTable);
                descriptors.addAll(onSpecDescriptors);
            }

            descriptors.add(description);
            budgetTable.add(budgetRow.toArray(new String[budgetRow.size()]));
            contractTable.add(contractRow.toArray(new String[contractRow.size()]));

        }

        // Столбец "Всего по университету"
        List<String> budgetRow = new ArrayList<>();
        List<String> contractRow = new ArrayList<>();

        budgetRow.add("\\bВсего по университету");
        budgetRow.add(uniTotalBudgetPlan == 0 ? "-" : String.valueOf(uniTotalBudgetPlan));
        budgetRow.add(String.valueOf(uniTotalBudgetRequests));
        Double competitionBudget = 1.0*uniTotalBudgetRequests/uniTotalBudgetPlan;
        budgetRow.add(uniTotalBudgetPlan == 0 ? "-" : formatter.format(competitionBudget));
        if (reqTypeCode.equals(EnrRequestTypeCodes.BS) || reqTypeCode.equals(EnrRequestTypeCodes.MASTER)) {
            int uniNotTAPlan = uniTotalBudgetPlan - uniTotalTAPlan;
            if (uniNotTAPlan != 0) {
                Double competitionNotTA = 1.0 * (uniTotalBudgetRequests - uniTotalTARequests) / uniNotTAPlan;
                budgetRow.add(formatter.format(competitionNotTA));
            } else {
                budgetRow.add("-");
            }
            if (uniTotalTAPlan != 0) {
                budgetRow.add(String.valueOf(uniTotalTAPlan));
                budgetRow.add(String.valueOf(uniTotalTARequests));
                Double competitionTA = 1.0 * uniTotalTARequests / uniTotalTAPlan;
                budgetRow.add(formatter.format(competitionTA));
            } else {
                budgetRow.add("-");
                budgetRow.add("-");
                budgetRow.add("-");
            }
        }
        budgetRow.add(String.valueOf(uniTotalBudgetOriginHandedIn));
        budgetRow.add(String.valueOf(uniTotalExclusive));
        budgetRow.add(String.valueOf(uniTotalNoExam));
        switch (reqTypeCode)
        {
            case (EnrRequestTypeCodes.BS):
                budgetRow.add(formatter.format(countAverage(uniTotalAvgs1)));
                budgetRow.add(formatter.format(countAverage(uniTotalAvgs2)));
                budgetRow.add(formatter.format(countAverage(uniTotalAvgs3)));
                break;
            case (EnrRequestTypeCodes.MASTER):
                budgetRow.add(formatter.format(countAverage(uniTotalAvgs1)));
                break;
            case (EnrRequestTypeCodes.HIGHER):
            case (EnrRequestTypeCodes.POSTGRADUATE) :
            case (EnrRequestTypeCodes.TRAINEESHIP):
            case (EnrRequestTypeCodes.INTERNSHIP):
                budgetRow.add(formatter.format(countAverage(uniTotalAvgs1)));
                budgetRow.add(formatter.format(countAverage(uniTotalAvgs2)));
                break;
            case (EnrRequestTypeCodes.SPO):
                budgetRow.add(formatter.format(countAverage(uniTotalAvgs1)));
                budgetRow.add(formatter.format(countAverage(uniTotalAvgs2)));
                break;
        }

        budgetTable.add(1, budgetRow.toArray(new String[budgetRow.size()]));

        contractRow.add("\\bВсего по университету");
        contractRow.add(uniTotalContractPlan == 0 ? "-" : String.valueOf(uniTotalContractPlan));
        contractRow.add(String.valueOf(uniTotalContractRequests));
        contractRow.add(String.valueOf(uniTotalContractOriginHandedIn));
        contractRow.add(String.valueOf(uniTotalContractPayed));

        contractTable.add(1, contractRow.toArray(new String[contractRow.size()]));

        rowsCount = budgetTable.size() - 1;

        // Транспонируем таблицы
        String[][] transposedBudgetTable = new String[budgetHeader.size()][budgetTable.size()];
        String[][] transposedContractTable = new String[contractHeader.size()][contractTable.size()];

        for (int i = 0; i < budgetTable.size(); i++) {
            for (int j = 0; j < budgetTable.get(i).length; j++) {
                transposedBudgetTable[j][i] = budgetTable.get(i)[j];
            }
        }

        for (int i = 0; i < contractTable.size(); i++) {
            for (int j = 0; j < contractTable.get(i).length; j++) {
                transposedContractTable[j][i] = contractTable.get(i)[j];
            }
        }

        StringBuilder builder = new StringBuilder();

        for (String descriptor : descriptors) {
            builder.append(descriptor).append("\\line");
        }

        String[][] shortTitles = new String[][]
                {
                        {
                                builder.toString()
                        }
                };

        RtfTableModifier modifier = new RtfTableModifier();

        String type = ((EnrRequestType) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE).getValue()).getTitle();
        String form = UniStringUtils.join((Collection) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM).getValue(), EduProgramForm.title().s(), ", ");

        StringBuilder formBuilder = new StringBuilder();
        if (!form.isEmpty() && model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM).isEnableCheckboxChecked())
        {
            formBuilder.append("(").append(form).append(")");
        }

        new RtfInjectModifier().put("type", type).put("form", formBuilder.toString()).put("dateForm", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())).modify(document);


        modifier.put("shortTitle", shortTitles);
        modifier.put("shortTitle", new RtfRowIntercepterRawText());
        modifier.put("T", transposedBudgetTable);
        modifier.put("C", transposedContractTable);
        // интерцептер для разбиения на нужное кол-во столбцов
        IRtfRowIntercepter intercepter = new IRtfRowIntercepter() {
            public void beforeModify(RtfTable table, int currentRowIndex) {
                if (rowsCount > 0) {
                    int[] splitter = new int[rowsCount];
                    Arrays.fill(splitter, 1);
                    splitter[0] = 2;
                    RtfUtil.splitRow(table.getRowList().get(0), 1, null, splitter);
                    RtfUtil.splitRow(table.getRowList().get(1), 1, null, splitter);
                    RtfUtil.splitRow(table.getRowList().get(2), 1, null, splitter);
                }
                else RtfUtil.splitRow(table.getRowList().get(0), 1, (newCell, index) -> newCell.addElement(RtfBean.getElementFactory().createRtfText("В выборку не попали данные")), new int[]{1});
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                if (rowIndex==0 && colIndex ==0)
                    cell.append(IRtfData.QC);
                List<IRtfElement> list = new ArrayList<>();
                IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                text.setRaw(true);
                list.add(text);
                return list;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {

            }
        };

        modifier.put("T", intercepter);
        modifier.put("C", new IRtfRowIntercepter() {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex) {

            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                if (rowIndex==0 && colIndex ==0)
                    cell.append(IRtfData.QC);
                List<IRtfElement> list = new ArrayList<>();
                IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                text.setRaw(true);
                list.add(text);
                return list;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {

            }
        });
        modifier.modify(document);

        RtfText text = new RtfText();
        text.setString("{\\colortbl;\\red255\\green0\\blue0;}");
        text.setRaw(true);
        document.getElementList().add(0, text);


        RtfString message = new RtfString().append(showWarnMessage ? "\\cf1Не у всех абитуриентов выбраны приоритеты для образовательных программ" : "", true);

        new RtfInjectModifier().put("message", message).modify(document);




        return RtfUtil.toByteArray(document);
    }


    private List<String> formBudgetHeader(String reqTypeCode)
    {
        List<String> header = new ArrayList<>();

        header.add("\\qc\\bГ/б");
        header.add("\\bПлан приема г/б");
        header.add("\\bПодано заявлений г/б");
        header.add("\\bОбщий конкурс г/б");
        if (reqTypeCode.equals(EnrRequestTypeCodes.BS) || reqTypeCode.equals(EnrRequestTypeCodes.MASTER)) {
            header.add("\\bКонкурс г/б без ЦП");
            header.add("\\bПлан приема ЦП");
            header.add("\\bПодано заявлений ЦП");
            header.add("\\bКонкурс ЦП");
        }
        header.add("\\bПодали оригиналы докум.+1 пр.");
        header.add("\\bПодано заявлений (особое право)");
        header.add("\\bПодано заявлений (без ВИ)");

        switch (reqTypeCode) {
            case EnrRequestTypeCodes.BS:
                header.add("\\bСредний балл ЕГЭ (г/б)");
                header.add("\\bСредний балл ЕГЭ (г/б без ЦП)");
                header.add("\\bСредний балл ЕГЭ (ЦП)");
                break;
            case EnrRequestTypeCodes.MASTER:
                header.add("\\bСредний балл по ВИ");
                break;
            case EnrRequestTypeCodes.HIGHER:
            case EnrRequestTypeCodes.POSTGRADUATE:
            case EnrRequestTypeCodes.TRAINEESHIP:
            case EnrRequestTypeCodes.INTERNSHIP:
                header.add("\\bСредний балл");
                header.add("\\bСредний балл по спец.");
                break;
            case EnrRequestTypeCodes.SPO:
                header.add("\\bСр. балл док. об образ.");
                header.add("\\bСредний балл по ВИ");
                break;
        }
        return header;
    }

    private List<String> formContractHeader()
    {
        List<String> header = new ArrayList<>();

        header.add("\\bКонтракт");
        header.add("\\bПлан приёма с оплатой");
        header.add("\\bПодано заявлений");
        header.add("\\bПодали оригиналы докум.+1пр.");
        header.add("\\bКол-во оплаченных договоров");

        return header;
    }

    private Double countAverage(List<Double> list)
    {
        Double average = 0d;
        if (!list.isEmpty()) {
            for (Double value : list)
                average += value;

            average = average / list.size();
        }

        return average;
    }
}
