package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic;

import org.hibernate.Session;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLInsertValuesBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EnrEntrantContractDao extends UniBaseDao implements IEnrEntrantContractDao {

    @Override
    public void doSetPaymentDate(Long entrantContractId, boolean clean) {
        EnrEntrantContract entrantContract = getNotNull(entrantContractId);
        entrantContract.setPaymentDate(clean ? null : new Date());
        saveOrUpdate(entrantContract);
    }

    @Override
    public int doAttachEduContractsToStudents(EnrEnrollmentCampaign enrollmentCampaign)
    {
        DQLSelectBuilder builder = getEduContractsBuilder(enrollmentCampaign, null, null);

        List<Object[]> rows = this.getList(builder);
        if (rows.size() == 0) return 0;

        final Short entityCode = EntityRuntime.getMeta(EduCtrStudentContract.class).getEntityCode();
        final Session session = this.getSession();

        BatchUtils.execute(rows, DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            DQLInsertValuesBuilder insertBuilder = new DQLInsertValuesBuilder(EduCtrStudentContract.class);
            for (Object[] row : elements)
            {
                insertBuilder.value(EduCtrStudentContract.P_ID, EntityIDGenerator.generateNewId(entityCode));
                insertBuilder.value(EduCtrStudentContract.L_CONTRACT_OBJECT, ((CtrContractObject) row[0]).getId());
                insertBuilder.value(EduCtrStudentContract.L_STUDENT, ((Student)row[1]).getId());
                insertBuilder.addBatch();
            }

            insertBuilder.createStatement(session).execute();
        });

        return rows.size();
    }

    private DQLSelectBuilder getEduContractsBuilder(EnrEnrollmentCampaign enrollmentCampaign, Student student, EnrEntrant entrant) {
        DQLSelectBuilder builder =  new DQLSelectBuilder()
                .fromEntity(EnrEntrantContract.class, "e_ctr")
                .joinEntity("e_ctr", DQLJoinType.inner,EnrEnrollmentExtract.class, "ee",
                       eq(property("ee", EnrEnrollmentExtract.entity()), property("e_ctr", EnrEntrantContract.requestedCompetition())))
                .distinct()
                .where(eq(property("ee", EnrEnrollmentExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                .where(eq(property("ee", EnrEnrollmentExtract.cancelled()), value(Boolean.FALSE)))
                .where(isNotNull(property("ee", EnrEnrollmentExtract.student())))
                .where(eq(property("e_ctr", EnrEntrantContract.requestedCompetition().request().entrant().person()), property("ee", EnrEnrollmentExtract.student().person()))) // TODO: проблема описана в задаче DEV-9187. Удалить, когда будет принято решение создавать или нет констрейнт на соответствие персон
                .where(notExists(new DQLSelectBuilder()
                        .fromEntity(EduCtrStudentContract.class, "s_ctr")
                        .where(eq(property("s_ctr", EduCtrStudentContract.contractObject()), property("e_ctr", EnrEntrantContract.contractObject())))
                        .where(eq(property("s_ctr", EduCtrStudentContract.student().person()), property("e_ctr", EnrEntrantContract.requestedCompetition().request().entrant().person())))
                        .buildQuery()))
                .column(property("e_ctr", EnrEntrantContract.contractObject()))
                .column(property("ee", EnrEnrollmentExtract.student()));

        if(enrollmentCampaign != null)
        {
            builder.where(eq(property("e_ctr", EnrEntrantContract.requestedCompetition().request().entrant().enrollmentCampaign()), value(enrollmentCampaign)));
        }

        if (student != null)
        {
            builder.where(eq(property("ee", EnrEnrollmentExtract.student()), value(student)));
        }
        else if (entrant != null)
        {
            builder.where(eq(property("e_ctr", EnrEntrantContract.requestedCompetition().request().entrant()), value(entrant)));
        }
        return builder;
    }

    @Override
    public int doAttachEduContracts(Student student) {
        return doAttachEduContracts(student,null);
    }

    private int doAttachEduContracts(Student student, EnrEntrant entrant){
        DQLSelectBuilder builder = getEduContractsBuilder(null, student, entrant);

        int count = getCount(builder);
        List<Object[]> result = builder.createStatement(getSession()).list();

        result.forEach(r->{
            EduCtrStudentContract studentContract = new EduCtrStudentContract();
            studentContract.setStudent((Student) r [1]);
            studentContract.setContractObject((CtrContractObject)r[0]);
            save(studentContract);
        });

        return count;
    }

    @Override
    public int doAttachEduContracts(EnrEntrant entrant) {
        return doAttachEduContracts(null,entrant);
    }

    @Override
    public void doAttachEnrEntrantContractToStudent(Long contractObjectId) {
        DQLSelectBuilder builder = getEduContractsBuilder(null, null,null)
                .where(eq( property("e_ctr", EnrEntrantContract.contractObject().id()), value(contractObjectId)));

        Object result = builder.createStatement(getSession()).uniqueResult();

        if(result != null) {
            EduCtrStudentContract studentContract = new EduCtrStudentContract();
            studentContract.setContractObject((CtrContractObject)((Object[]) result)[0]);
            studentContract.setStudent((Student) ((Object[]) result)[1]);
            save(studentContract);
        }
    }

    @Override
    public void doDetachEnrEntrantContractToStudent(Long contractObjectId) {
        EduCtrStudentContract studentContract = new DQLSelectBuilder().fromEntity(EduCtrStudentContract.class, "sc")
                .where(eq(property("sc", EduCtrStudentContract.contractObject().id()), value(contractObjectId)))
                .where(exists(new DQLSelectBuilder().fromEntity(EnrEntrantContract.class, "ec")
                        .where(eq(property("ec", EnrEntrantContract.contractObject()), property("sc", EduCtrStudentContract.contractObject())))
                        .buildQuery()))
                .createStatement(getSession()).uniqueResult();
        if(studentContract != null)
            delete(studentContract);

    }

    @Override
    public boolean getExistsNotAttachContracts(Student student) {
        return existsEntity(getEduContractsBuilder(null, student,null).buildQuery());
    }

    @Override
    public boolean getExistsNotAttachContracts(EnrEntrant entrant) {
        return existsEntity(getEduContractsBuilder(null, null, entrant).buildQuery());
    }
}
