package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EnrEntrantContractListHandler extends EntityComboDataSourceHandler
{
    public static final String PARAM_ENTRANT_ID = "entrantId";
    public static final String PARAM_ENTRANT_PROMISE_ONLY = "entrantPromiseOnly";

    public static final String VIEW_PROPERTY_COMPETITION_ERROR = "educationPromiseListError";
    public static final String VIEW_PROPERTY_EDU_PROMISE_LIST = "educationPromiseList";
    public static final String VIEW_PROPERTY_EDU_PROMISE_FIRST = "educationPromiseFirst";

    public static final String VIEW_PROPERTY_STUDENT = "student";
    public static final String VIEW_PROPERTY_RELATION_WITH_STUDENT = "relationWithStudent";
    public static final String VIEW_PROPERTY_DISABLED_ATTACHMENT = "disabledAttachment";


    public EnrEntrantContractListHandler(final String ownerId) {
        super(ownerId, EnrEntrantContract.class);
        this.where(EnrEntrantContract.requestedCompetition().request().entrant().id(), EnrEntrantContractListHandler.PARAM_ENTRANT_ID, true);
    }

    @Override
    protected DSOutput execute(final DSInput input, final ExecutionContext context) {
        final Boolean entrantPromiseOnly = context.getBoolean(PARAM_ENTRANT_PROMISE_ONLY, Boolean.TRUE);

        final DSOutput output = super.execute(input, context);

        /* { enrEntrantContract.id -> { eduCtrEducationPromise } } */
        final Map<Long, List<EduCtrEducationPromise>> promiseMap = new HashMap<>();

        /* { enrProgramSetBase.id -> { eduProgram.id } } */
        final Map<Long, Set<Long>> programSetMap = new HashMap<>();

        final Map<Long, Student> studentsMap = new HashMap<>();
        final Map<Long,Boolean> relationsWithStudentMap = new HashMap<>();

        BatchUtils.execute(output.getRecordIds(), DQL.MAX_VALUES_ROW_NUMBER, ids -> {
            // promiseMap
            {
                final DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(EduCtrEducationPromise.class, "p")
                        .fromEntity(EnrEntrantContract.class, "x")
                        .where(eq(
                                property(EnrEntrantContract.contractObject().fromAlias("x")),
                                property(EduCtrEducationPromise.src().owner().contract().fromAlias("p"))
                        ))
                        .where(isNull(property(EduCtrEducationPromise.src().owner().removalDate().fromAlias("p"))))
                        .where(in(property("x.id"), ids))
                        .column(property("x.id"))
                        .column(property("p"));

                if (entrantPromiseOnly) {
                    dql.where(eq(
                            property(EnrEntrantContract.requestedCompetition().request().entrant().person().fromAlias("x")),
                            property(EduCtrEducationPromise.dst().contactor().person().fromAlias("p"))
                    ));
                }

                final List<Object[]> rows = dql.createStatement(context.getSession()).list();
                for (final Object[] row : rows) {
                    // как правило там одно значение
                    SafeMap.safeGet(promiseMap, (Long) row[0], LinkedList.class).add((EduCtrEducationPromise) row[1]);
                }
            }

            // programSetMap
            {
                // через programSetItem
                {
                    final DQLSelectBuilder dql = new DQLSelectBuilder()
                            .fromEntity(EnrEntrantContract.class, "x")
                            .where(in(property("x.id"), ids))

                            .fromEntity(EnrProgramSetItem.class, "psi")
                            .where(eq(
                                    property(EnrEntrantContract.requestedCompetition().competition().programSetOrgUnit().programSet().fromAlias("x")),
                                    property(EnrProgramSetItem.programSet().fromAlias("psi"))
                            ))

                            .column(property("x.id"))
                            .column(property(EnrProgramSetItem.program().id().fromAlias("psi")));

                    final List<Object[]> rows = dql.createStatement(context.getSession()).list();
                    for (final Object[] row: rows) {
                        SafeMap.safeGet(programSetMap, (Long)row[0], LinkedHashSet.class).add((Long)row[1]);
                    }
                }

                // прямая ссылка для СПО
                {
                    final DQLSelectBuilder dql = new DQLSelectBuilder()
                            .fromEntity(EnrEntrantContract.class, "x")
                            .where(in(property("x.id"), ids))

                            .fromEntity(EnrProgramSetSecondary.class, "ps")
                            .where(eq(
                                    property(EnrEntrantContract.requestedCompetition().competition().programSetOrgUnit().programSet().fromAlias("x")),
                                    property("ps")
                            ))

                            .column(property("x.id"))
                            .column(property(EnrProgramSetSecondary.program().id().fromAlias("ps")));

                    final List<Object[]> rows = dql.createStatement(context.getSession()).list();
                    for (final Object[] row: rows) {
                        SafeMap.safeGet(programSetMap, (Long)row[0], LinkedHashSet.class).add((Long)row[1]);
                    }
                }
            }

            //
            {
                final DQLSelectBuilder eduCtrStudentContractDql = new DQLSelectBuilder()
                        .fromEntity(EduCtrStudentContract.class, "ecsc")
                        .joinEntity("ecsc", DQLJoinType.inner, EnrEntrantContract.class, "ec", eq(property("ecsc", EduCtrStudentContract.contractObject()), property("ec", EnrEntrantContract.contractObject())))
                        .column(property("ec.id"))
                        .column(property("ecsc", EduCtrStudentContract.student()))
                        .where(in(property("ec", EnrEntrantContract.id()), ids));

                final DQLSelectBuilder enrollmentExtractsDql = new DQLSelectBuilder()
                        .fromEntity(EnrEnrollmentExtract.class, "ee")
                        .joinEntity("ee", DQLJoinType.inner, EnrEntrantContract.class, "ec", eq(property("ec", EnrEntrantContract.requestedCompetition()), property("ee", EnrEnrollmentExtract.entity())))
                        .column(property("ec.id"))
                        .column(property("ee", EnrEnrollmentExtract.student()))
                        .where(in(property("ec", EnrEntrantContract.id()), ids))
                        .where(eq(property("ee", EnrEnrollmentExtract.cancelled()), value(false)))
                        .where(eq(property("ee", EnrEnrollmentExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                        .where(isNotNull(property("ee", EnrEnrollmentExtract.student())));

                for(Long id : ids)
                {
                    relationsWithStudentMap.put(id, false);
                }

                for(Object[] row : enrollmentExtractsDql.createStatement(context.getSession()).<Object[]>list())
                {
                    studentsMap.put((Long)row[0], (Student)row[1]);
                }

                for(Object[] row : eduCtrStudentContractDql.createStatement(context.getSession()).<Object[]>list())
                {
                    studentsMap.put((Long)row[0], (Student)row[1]);
                    relationsWithStudentMap.put((Long)row[0], true);
                }

            }
        });

        // поехали
        return output.transform((EnrEntrantContract contract) -> {
            final DataWrapper wrapper = new DataWrapper(contract);

            List<EduCtrEducationPromise> list = promiseMap.get(contract.getId());
            if (null == list || list.isEmpty()) {
                wrapper.putInMap(VIEW_PROPERTY_EDU_PROMISE_LIST, Collections.emptyList());
                wrapper.putInMap(VIEW_PROPERTY_EDU_PROMISE_FIRST, null);
                wrapper.putInMap(VIEW_PROPERTY_COMPETITION_ERROR, Boolean.FALSE);
            } else {
                list = new ArrayList<>(list); // создаем list ровно на выбранные значения

                wrapper.putInMap(VIEW_PROPERTY_EDU_PROMISE_LIST, list);
                wrapper.putInMap(VIEW_PROPERTY_EDU_PROMISE_FIRST, list.get(0));

                final Set<Long> programSet = programSetMap.get(contract.getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getProgramSet().getId());
                if (null == programSet) {
                    wrapper.putInMap(VIEW_PROPERTY_COMPETITION_ERROR, Boolean.TRUE);
                } else {
                    boolean error = false;
                    for (final EduCtrEducationPromise p: list) {
                        error = error || !programSet.contains(p.getEduProgram().getId());
                    }
                    wrapper.putInMap(VIEW_PROPERTY_COMPETITION_ERROR, error);
                }

                wrapper.putInMap(VIEW_PROPERTY_STUDENT, studentsMap.get(contract.getId()));
                wrapper.putInMap(VIEW_PROPERTY_RELATION_WITH_STUDENT, relationsWithStudentMap.get(contract.getId()));
                wrapper.putInMap(VIEW_PROPERTY_DISABLED_ATTACHMENT,  studentsMap.get(contract.getId()) == null );

            }

            return wrapper;
        });
    }

}
