package ru.tandemservice.unienr14_ctr.base.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContextObject;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14_ctr.base.entity.gen.EnrEntrantContractGen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Связь абитуриента с договором на обучение по обр. программе
 *
 * Связывает договор на обучение и абитуриента
 */
public class EnrEntrantContract extends EnrEntrantContractGen implements ISecLocalEntityOwner, ICtrContextObject, ITitled
{

    public EnrEntrant getEntrant() { return getRequestedCompetition().getRequest().getEntrant(); }

    @Override
    public String getTitle() {
        if (getContractObject() == null) {
            return this.getClass().getSimpleName();
        }
        return getContractObject().getTitle() + ", " + getEntrant().getTitle();
    }

    @Override
    public PersonRole getPersonRole() {
        return getEntrant();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities() {
        final List<IEntity> result = new ArrayList<IEntity>(this.getEntrant().getSecLocalEntities());
        final EnrProgramSetOrgUnit programSetOrgUnit = this.getRequestedCompetition().getCompetition().getProgramSetOrgUnit();
        result.add(programSetOrgUnit.getOrgUnit().getInstitutionOrgUnit().getOrgUnit());
        result.add(programSetOrgUnit.getEffectiveFormativeOrgUnit());
        result.add(this.getContractObject().getOrgUnit());
        return result;
    }

}