package ru.tandemservice.unienr14_ctr.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14_ctr.report.bo.CtrReport.ui.EntrantsPlanPointsAdd.CtrReportEntrantsPlanPointsAdd;
import ru.tandemservice.unienr14_ctr.report.entity.gen.*;

import java.util.Arrays;
import java.util.List;

/**
 * Отчет «Сводка о ходе приема, средних баллах и договорах на обучение»
 */
public class CtrReportEntrantsPlanPoints extends CtrReportEntrantsPlanPointsGen implements IEnrReport
{
    public static final String REPORT_KEY = "enr14_ctrReportEntrantsPlanPoints";

    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(
            P_STAGE,
            P_REQUEST_TYPE,
            P_PROGRAM_FORM
    );

    public static IEnrStorableReportDesc getDescription() {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return CtrReportEntrantsPlanPoints.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return CtrReportEntrantsPlanPointsAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Сводка о ходе приема, средних баллах и договорах на обучение»"; }
            @Override public String getListTitle() { return "Список отчетов «Сводка о ходе приема, средних баллах и договорах на обучение»"; }
        };
    }

    @Override
    public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override
    public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }

}