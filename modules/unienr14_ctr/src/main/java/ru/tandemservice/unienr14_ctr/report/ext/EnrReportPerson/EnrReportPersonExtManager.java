/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.ext.EnrReportPerson;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Nikolay Fedorovskih
 * @since 06.07.2015
 */
@Configuration
public class EnrReportPersonExtManager extends BusinessObjectExtensionManager
{
}