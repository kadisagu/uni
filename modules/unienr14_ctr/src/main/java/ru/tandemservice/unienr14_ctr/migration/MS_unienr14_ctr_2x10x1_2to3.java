package ru.tandemservice.unienr14_ctr.migration;

import com.google.common.collect.Maps;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Map;

import static org.tandemframework.dbsupport.sql.SQLFrom.table;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_ctr_2x10x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrEntrantContract

        // создать колонку + обновить в ней данные только в том случае, если её не было до этого
        if (!tool.columnExists("enr14_entrant_contract_t", "paymentdate_p")) {
            tool.createColumn("enr14_entrant_contract_t", new DBColumn("paymentdate_p", DBType.DATE));

            ISQLTranslator translator = tool.getDialect().getSQLTranslator();
            // формируем мапу для первого условия:
            // Если договор имеет положительный признак «Поступила оплата достаточная для зачисления»
            // + Выставлять «Дату поступления оплаты» равной дате факта оплаты. Брать факт оплаты с самой ранней датой.

            SQLSelectQuery sql = new SQLSelectQuery();
            String resultSql = "select a.contract_id, MIN(a.timestamp_p) min_date from ctr_result_t a group by a.contract_id";

            sql.from(table("enr14_entrant_contract_t", "enr14")
                    .leftJoin(table("ctr_contractobj_t", "contract"), "enr14.contractobject_id = contract.id")
                    .leftJoin(SQLFrom.select(resultSql, "result"), "contract.id = result.contract_id"))
                    .where("enr14.paymentexists_p = ?")
                    .where("result.min_date is not null")
                    .column("enr14.id")
                    .column("result.min_date");

            PreparedStatement statement = tool.prepareStatement(translator.toSql(sql), Boolean.TRUE);

            ResultSet resultSet = statement.executeQuery();
            Map<Long, Date> changeMap = Maps.newHashMap();
            while (resultSet.next()) {
                changeMap.put(resultSet.getLong(1), resultSet.getDate(2));
            }

            // формируем мапу для второго условия:
            // Если договор имеет положительный признак «Поступила оплата достаточная для зачисления»
            // + то брать дату начала действия договора по документу первой версии.
            // Условие "фактов оплаты нет" будет выполняться в процессе обновления записей, путем запросов к мапам.

            sql = new SQLSelectQuery();
            resultSql = "select a.contract_id, MIN(a.durationbegindate_p) min_date from ctr_contractver_t a group by a.contract_id";

            sql.from(table("enr14_entrant_contract_t", "enr14")
                    .leftJoin(table("ctr_contractobj_t", "contract"), "enr14.contractobject_id = contract.id")
                    .leftJoin(SQLFrom.select(resultSql, "result"), "contract.id = result.contract_id"))
                    .where("enr14.paymentexists_p = ?")
                    .where("result.min_date is not null")
                    .column("enr14.id")
                    .column("result.min_date");

            statement = tool.prepareStatement(translator.toSql(sql), Boolean.TRUE);
            resultSet = statement.executeQuery();
            Map<Long, Date> changeMapNoPayment = Maps.newHashMap();
            while (resultSet.next()) {
                changeMapNoPayment.put(resultSet.getLong(1), resultSet.getDate(2));
            }


            // обновляем значения для договоров, c положительным признаком «Поступила оплата достаточная для зачисления»

            sql = new SQLSelectQuery();
            sql.from(table("enr14_entrant_contract_t", "enr14")).where("enr14.paymentexists_p = ?").column("enr14.id");

            statement = tool.prepareStatement(translator.toSql(sql), Boolean.TRUE);
            resultSet = statement.executeQuery();

            final MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update enr14_entrant_contract_t set paymentdate_p=? where id=?", DBType.DATE, DBType.LONG);
            while (resultSet.next()) {
                Long key = resultSet.getLong(1);

                if (changeMap.containsKey(key)) {
                    updater.addBatch(changeMap.get(key), key);
                } else if (changeMapNoPayment.containsKey(key)) {
                    updater.addBatch(changeMapNoPayment.get(key), key);
                } else {
                    throw new IllegalStateException("There is no appropriate date to update paymentdate_p in enr14_entrant_contract_t table with correct flag paymentexists_p = true");
                }
            }

            updater.executeUpdate(tool);

            // обновляем значения для договоров, c отрицательным признаком «Поступила оплата достаточная для зачисления»
            tool.executeUpdate("update enr14_entrant_contract_t set paymentdate_p=null where paymentexists_p = ?", Boolean.FALSE);
        }

        // свойство paymentExists стало формулой
        {
            // удалить колонку если это возможно
            if (tool.columnExists("enr14_entrant_contract_t", "paymentexists_p")) {
                tool.dropColumn("enr14_entrant_contract_t", "paymentexists_p");
            }
        }
    }
}