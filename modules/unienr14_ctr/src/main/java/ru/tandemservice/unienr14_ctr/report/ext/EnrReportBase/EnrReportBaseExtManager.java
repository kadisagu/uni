/* $Id:$ */
package ru.tandemservice.unienr14_ctr.report.ext.EnrReportBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unienr14_ctr.report.bo.CtrReport.ui.EnrolledEntrantsContractDataExportAdd.CtrReportEnrolledEntrantsContractDataExportAdd;
import ru.tandemservice.unienr14_ctr.report.entity.CtrReportEntrantsPlanPoints;

/**
 * @author rsizonenko
 * @since 23.07.2014
 */
@Configuration
public class EnrReportBaseExtManager extends BusinessObjectExtensionManager{

    @Autowired
    private EnrReportBaseManager _enrReportBaseManager;

    @Bean
    public ItemListExtension<EnrReportBaseManager.IEnrReportDefinition> reportListExtension()
    {
        return itemListExtension(_enrReportBaseManager.reportListExtPoint())
        .add("enr14_ctrReportEnrolledEntrantsContractDataExport", EnrReportBaseManager.getReportDefinition("enr14_ctrReportEnrolledEntrantsContractDataExport", CtrReportEnrolledEntrantsContractDataExportAdd.class))
        .add(CtrReportEntrantsPlanPoints.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(CtrReportEntrantsPlanPoints.REPORT_KEY))
            .create();
    }



    @Bean
    public ItemListExtension<IEnrStorableReportDesc> storableReportDescExtension()
    {
        return itemListExtension(_enrReportBaseManager.storableReportDescExtPoint())
                .add(CtrReportEntrantsPlanPoints.REPORT_KEY, CtrReportEntrantsPlanPoints.getDescription())
                .create();
    }
}
