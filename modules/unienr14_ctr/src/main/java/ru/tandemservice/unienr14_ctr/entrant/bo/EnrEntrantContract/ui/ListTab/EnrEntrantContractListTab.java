package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.ui.ListTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.list.column.SimpleMergeIdResolver;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.PubCtr.CtrContractVersionPubCtr;

import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic.EnrContractListDataSourceHandler;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic.EnrEntrantContractListHandler;

@Configuration
public class EnrEntrantContractListTab extends BusinessComponentManager
{
    public static final String CONTRACT_LIST_DS = "contractListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
        .addDataSource(this.searchListDS(CONTRACT_LIST_DS, this.contractListDSColumns(), this.contractListDSHandler()))
        .create();
    }

    @Bean
    public ColumnListExtPoint contractListDSColumns() {

        return columnListExtPointBuilder(CONTRACT_LIST_DS)
        .addColumn(publisherColumn("contractNumber", EnrEntrantContract.contractObject().number()).businessComponent(CtrContractVersionPubCtr.class).required(true).create())
        .addColumn(textColumn("competition", EnrEntrantContract.requestedCompetition().title()).merger(new SimpleMergeIdResolver(EnrEntrantContract.requestedCompetition().competition().id())).required(true))
        .addColumn(textColumn("eduProgram", EnrEntrantContractListHandler.VIEW_PROPERTY_EDU_PROMISE_FIRST+"."+EduCtrEducationPromise.eduProgram().title()).required(true))
                .addColumn(publisherColumn("student", EnrEntrantContractListHandler.VIEW_PROPERTY_STUDENT +"."+ Student.P_TITLE_WITHOUT_FIO).publisherLinkResolver(new SimplePublisherLinkResolver("student.id").param("selectedStudentTab", "studentTab")).required(true))
                .addColumn(
                        toggleColumn("paymentExists", EnrEntrantContract.paymentExists())
                                // .merger(contractMerger)
                                .disabled("ui:currentPaymentExistsDisabled")
                                .toggleOffListener("onClickSwitchPaymentExists").toggleOffLabel("Оплаты нет")
                                .toggleOnListener("onClickSwitchPaymentExists").toggleOnLabel("Поступила оплата, дост. для зачисления")
                                .toggleOffListenerAlert(alert("Сбросить оплату для договора «{0}»?", EnrEntrantContract.contractObject().number()))
                                .toggleOnListenerAlert(alert("Зарегистрировать оплату для договора «{0}»?", EnrEntrantContract.contractObject().number()))
                                .permissionKey("enr14EntrantPubContractListPaymentFlag")
                )
                .addColumn(
                        toggleColumn("connectionWithStudent", EnrEntrantContractListHandler.VIEW_PROPERTY_RELATION_WITH_STUDENT)
                                .toggleOnListener("onClickSwitchAttachContractToStudent").toggleOffLabel("Договор приклеплен к студенту")
                                .toggleOffListener("onClickSwitchAttachContractToStudent").toggleOffLabel("Договор не приклеплен к студенту")
                                .toggleOffListenerAlert(alert("Отвязать договор «{0}» от студента?", EnrEntrantContract.contractObject().number()))
                                .toggleOnListenerAlert(alert("Прикрепить договор «{0}» к студенту?", EnrEntrantContract.contractObject().number()))
                                .disabled(EnrEntrantContractListHandler.VIEW_PROPERTY_DISABLED_ATTACHMENT)
                                .permissionKey("enr14EntrantPubContractListAttachContract")
                )
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> contractListDSHandler() {
        return new EnrEntrantContractListHandler(getName())
        .order(EnrEntrantContract.requestedCompetition().competition().programSetOrgUnit().programSet().programSubject().subjectCode())
        .order(EnrEntrantContract.requestedCompetition().competition().programSetOrgUnit().programSet().title())
        .order(EnrEntrantContract.requestedCompetition().competition().programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().title())
        .order(EnrEntrantContract.requestedCompetition().competition().type().code())
        .order(EnrEntrantContract.requestedCompetition().id())
        .order(EnrEntrantContract.contractObject().number());
    }

}
