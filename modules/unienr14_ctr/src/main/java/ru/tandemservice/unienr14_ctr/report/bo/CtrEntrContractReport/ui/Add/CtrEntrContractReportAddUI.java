/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.*;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantDSHandler;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.ui.Pub.CtrEntrContractReportPub;
import ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.CtrEntrContractReportManager;
import ru.tandemservice.unienr14_ctr.report.entity.CtrEntrContractReport;

/**
 * @author Stanislav Shibarshin
 * @since 03.10.2016
 */
@State({
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class CtrEntrContractReportAddUI extends UIPresenter
{

    private Long _orgUnitId;
    private CtrEntrContractReport _report;
    private boolean _printRevenue;
    private long _reportId;

    private CommonPostfixPermissionModel _secModel;

    @Override
    public void onComponentRefresh()
    {
        if (_report == null) {
            _printRevenue = true;
            _report = new CtrEntrContractReport();
        }
        _report.setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        if (_orgUnitId != null) {
            _report.setOrgUnit(UniDaoFacade.getCoreDao().get(OrgUnit.class, _orgUnitId));
            _secModel = new OrgUnitSecModel(_report.getOrgUnit());
        }

        configUtil(getCompetitionFilterAddon());
    }

    @Override
    public void onComponentPrepareRender()
    {
        if (_reportId != 0)
        {
            activatePublisher();
            _reportId = 0L;
        }
    }

    public void onClickApply()
    {
        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Формирование отчета", new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                _reportId = CtrEntrContractReportManager.instance().dao().createReport(CtrEntrContractReportAddUI.this);
                return null;
            }
        }, ProcessDisplayMode.unknown));
    }

    public void activatePublisher()
    {
        _uiActivation.asDesktopRoot(CtrEntrContractReportPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, _reportId).activate();
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEntrantDSHandler.PARAM_ENR_COMPETITION_UTIL, getConfig().getAddon(CommonFilterAddon.class.getSimpleName()));
    }


    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(_report.getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
    }

    protected void configUtil(EnrCompetitionFilterAddon util)
    {
        util.configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util.addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);
    }

    protected void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), _report.getEnrollmentCampaign()))
                .configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().programSet().programSubject().subjectIndex().programKind().programHigherProf(), Boolean.TRUE));

        if (_orgUnitId != null) {
            util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().formativeOrgUnit().id(), _orgUnitId));
        }
    }



    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(CtrEntrContractReportAdd.COMPETITION_FILTER_ADDON);
    }

    public String getAddKey()
    {
        return _report.getOrgUnit() == null ? "ctrEntrContractReport" : getSecModel().getPermission("orgUnit_addCtrEntrContractReport");
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _report.getOrgUnit() != null ? _report.getOrgUnit() : super.getSecuredObject();
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }

    public boolean getPrintRevenue()
    {
        return _printRevenue;
    }

    public void setPrintRevenue(boolean printRevenue)
    {
        this._printRevenue = printRevenue;
    }

    public CtrEntrContractReport getReport()
    {
        return _report;
    }

    public void setReport(CtrEntrContractReport report)
    {
        this._report = report;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        this._secModel = secModel;
    }
}
