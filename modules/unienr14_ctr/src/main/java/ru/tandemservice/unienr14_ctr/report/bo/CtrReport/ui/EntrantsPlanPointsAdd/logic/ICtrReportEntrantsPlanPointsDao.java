/* $Id:$ */
package ru.tandemservice.unienr14_ctr.report.bo.CtrReport.ui.EntrantsPlanPointsAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14_ctr.report.bo.CtrReport.ui.EntrantsPlanPointsAdd.CtrReportEntrantsPlanPointsAddUI;

/**
 * @author rsizonenko
 * @since 23.07.2014
 */
public interface ICtrReportEntrantsPlanPointsDao extends INeedPersistenceSupport {
    long createReport(CtrReportEntrantsPlanPointsAddUI model);
}
