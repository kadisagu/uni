package ru.tandemservice.unienr14_ctr.request.ext.EnrRequestedCompetition.logic;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.Collections;
import java.util.Map;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;

import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.logic.IPropertyUpdateRule;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaignSettings;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;

/**
 * @author vdanilov
 */
public class EnrEntrantContractExistsRule implements IPropertyUpdateRule<Boolean> {

    public static final Long CHECK_MODE_NONE = 0L;
    public static final Long CHECK_MODE_THIS_CONTRACT_ONLY = 1L;
    public static final Long CHECK_MODE_THIS_CONTRACT_WITH_PAYMENT = 2L;
    public static final Long CHECK_MODE_THIS_CONTRACT_WITH_ANY_PAYMENT = 3L;
    public static final Long CHECK_MODE_ANY_CONTRACT_ONLY = 4L;
    public static final Long CHECK_MODE_ANY_CONTRACT_WITH_PAYMENT = 5L;

    public static final IPropertyUpdateRule<Boolean> INSTANCE = new EnrEntrantContractExistsRule();

    @Override
    public Map<Boolean, IDQLExpression> buildValueExpressionMap(String alias, Map<String, Object> params) {
        String tmp = alias+"_tmp";
        Long enrollmentCampaignId = (Long)params.get("enrollmentCampaignId");

        Long checkLevel = DataAccessServices.dao().<Long>getList(
            new DQLSelectBuilder()
            .fromEntity(EnrEnrollmentCampaign.class, "tmp")
            .column(property(EnrEnrollmentCampaign.settings().enrEntrantContractExistCheckMode().fromAlias("tmp")))
            .where(eq(property("tmp.id"), value(enrollmentCampaignId)))
        ).iterator().next();

        if (CHECK_MODE_NONE.equals(checkLevel)) {
            return null;
        }

        if (CHECK_MODE_THIS_CONTRACT_ONLY.equals(checkLevel)) {
            return Collections.singletonMap(
                Boolean.TRUE,
                    or(
                            and(
                                    eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)),
                                    exists(
                                            new DQLSelectBuilder()
                                                    .fromEntity(EnrEntrantContract.class, tmp)
                                                    .column(property(tmp, "id"))
                                                    .where(eq(property(EnrEntrantContract.requestedCompetition().fromAlias(tmp)), property(alias)))
                                                    .buildQuery()
                                    )
                            ),
                            eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET))
                    )
            );
        }

        if (CHECK_MODE_THIS_CONTRACT_WITH_PAYMENT.equals(checkLevel)) {
            return Collections.singletonMap(
                    Boolean.TRUE, or(
                            and(
                                    eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)),
                                    exists(
                                            new DQLSelectBuilder()
                                                    .fromEntity(EnrEntrantContract.class, tmp)
                                                    .column(property(tmp, "id"))
                                                    .where(eq(property(EnrEntrantContract.requestedCompetition().fromAlias(tmp)), property(alias)))
                                                    .where(eq(property(EnrEntrantContract.paymentExists().fromAlias(tmp)), value(Boolean.TRUE)))
                                                    .buildQuery()
                                    )
                            ),
                            eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET))
                    )
            );
        }

        if (CHECK_MODE_THIS_CONTRACT_WITH_ANY_PAYMENT.equals(checkLevel)) {
            return Collections.singletonMap(
                Boolean.TRUE,
                    or(
                            and(
                                    eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)),
                                    and(
                                            exists(
                                                    new DQLSelectBuilder()
                                                            .fromEntity(EnrEntrantContract.class, tmp)
                                                            .column(property(tmp, "id"))
                                                            .where(eq(property(EnrEntrantContract.requestedCompetition().fromAlias(tmp)), property(alias)))
                                                            .buildQuery()
                                            ),
                                            exists(
                                                    new DQLSelectBuilder()
                                                            .fromEntity(EnrEntrantContract.class, tmp)
                                                            .column(property(tmp, "id"))
                                                            .where(eq(property(EnrEntrantContract.requestedCompetition().request().entrant().fromAlias(tmp)), property(EnrRequestedCompetition.request().entrant().fromAlias(alias))))
                                                            .where(eq(property(EnrEntrantContract.paymentExists().fromAlias(tmp)), value(Boolean.TRUE)))
                                                            .buildQuery()
                                            )
                                    )
                            ),
                            eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET))
                    )
            );
        }

        if (CHECK_MODE_ANY_CONTRACT_ONLY.equals(checkLevel)) {
            return Collections.singletonMap(
                Boolean.TRUE,
                    or(
                            and(
                                    eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)),
                                    exists(
                                            new DQLSelectBuilder()
                                                    .fromEntity(EnrEntrantContract.class, tmp)
                                                    .column(property(tmp, "id"))
                                                    .where(eq(property(EnrEntrantContract.requestedCompetition().request().entrant().fromAlias(tmp)), property(EnrRequestedCompetition.request().entrant().fromAlias(alias))))
                                                    .buildQuery()
                                    )
                            ),
                            eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET))
                    )
            );
        }

        if (CHECK_MODE_ANY_CONTRACT_WITH_PAYMENT.equals(checkLevel)) {
            return Collections.singletonMap(
                Boolean.TRUE,
                    or(
                            and(
                                    eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)),
                                    exists(
                                            new DQLSelectBuilder()
                                                    .fromEntity(EnrEntrantContract.class, tmp)
                                                    .column(property(tmp, "id"))
                                                    .where(eq(property(EnrEntrantContract.requestedCompetition().request().entrant().fromAlias(tmp)), property(EnrRequestedCompetition.request().entrant().fromAlias(alias))))
                                                    .where(eq(property(EnrEntrantContract.paymentExists().fromAlias(tmp)), value(Boolean.TRUE)))
                                                    .buildQuery()
                                    )
                            ),
                            eq(property(EnrRequestedCompetition.competition().type().compensationType().code().fromAlias(alias)), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET))
                    )
            );
        }


        Debug.message(this.getClass().getName()+": Undefined checkLevel = "+checkLevel);
        return null;
    }

    @Override
    public void registerWakeUp(final IDSetEventListener wakeUpListener)
    {
        {
            final IDSetEventListener listener = event -> {
                if (event.getMultitude().getAffectedProperties().contains(EnrEntrantContract.P_PAYMENT_DATE)) {
                    wakeUpListener.onEvent(event);
                }
            };
            DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EnrEntrantContract.class, listener);
            DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, EnrEntrantContract.class, listener);
            DSetEventManager.getInstance().registerListener(DSetEventType.beforeDelete, EnrEntrantContract.class, wakeUpListener);
        }
    }


}
