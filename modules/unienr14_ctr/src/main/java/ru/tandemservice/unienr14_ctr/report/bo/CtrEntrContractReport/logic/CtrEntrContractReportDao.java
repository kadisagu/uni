/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.logic;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.RtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContractResultDao;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.catalog.entity.CtrScriptItem;
import ru.tandemservice.unienr14_ctr.catalog.entity.codes.CtrScriptItemCodes;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic.EntrantContractPaymentPromiceResultPair;
import ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.ui.Add.CtrEntrContractReportAddUI;
import ru.tandemservice.unienr14_ctr.report.entity.CtrEntrContractReport;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Stanislav Shibarshin
 * @since 03.10.2016
 */
public class CtrEntrContractReportDao extends UniBaseDao implements ICtrEntrContractReportDao
{

    @Override
    public Map<String, Multimap<String, List<Pair<Long, String>>>> prepareReportData(CtrEntrContractReport model, EnrCompetitionFilterAddon filter, boolean printRevenue)
    {
        // Получаем id заявок по выбранным фильтрам
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
                .filter(model.getEnrollmentCampaign());
        filter.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        List<Long> requestedCompetitionsIds = requestedCompDQL.column(property(EnrRequestedCompetition.id().fromAlias("reqComp"))).createStatement(getSession()).idList();

        // Получаем контракты по выбранным заявкам.
        DQLSelectBuilder contractListBuilder = new DQLSelectBuilder().fromEntity(EnrEntrantContract.class, "ctr")
                        .column("ctr")
                        .joinPath(DQLJoinType.inner, EnrEntrantContract.requestedCompetition().fromAlias("ctr"), "req_cmp")
                        .joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().fromAlias("req_cmp"), "req")
                        .joinPath(DQLJoinType.inner, EnrEntrantRequest.entrant().fromAlias("req"), "entr")
                        .where(in(EnrRequestedCompetition.id().fromAlias("req_cmp"), requestedCompetitionsIds))
                        .where(eq(property(EnrEntrantContract.paymentExists().fromAlias("ctr")), value(Boolean.TRUE)))
                        .where(eq(property(EnrEntrant.enrollmentCampaign().fromAlias("entr")), value(model.getEnrollmentCampaign())))
                        .where(eq(property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().subjectIndex().programKind().programHigherProf().fromAlias("req_cmp")), value(Boolean.TRUE)))
                        .order(property("req_cmp", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().code()))
                        .order(property("req_cmp", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().title()));

        if (model.getOrgUnit() != null) {
            contractListBuilder.where(or(
               eq(property("req_cmp", EnrRequestedCompetition.competition().programSetOrgUnit().educationOrgUnit().formativeOrgUnit().id()), value(model.getOrgUnit().getId())),
               eq(property("req_cmp", EnrRequestedCompetition.competition().programSetOrgUnit().educationOrgUnit().territorialOrgUnit().id()), value(model.getOrgUnit().getId()))

            ));
        }

        List<EnrEntrantContract> contractList = contractListBuilder.createStatement(getSession()).list();

        List<Long> contractIds = contractList.stream().map(c -> c.getContractObject().getId()).collect(Collectors.toList());
        Map<Long, CtrContractVersion> contractVersionMap = CtrContractVersionManager.instance().dao().getCurrentVersions(contractIds, new Date());

        Map<Long, Map<String, ICtrContractResultDao.ICtrContractStatus>> contractVersionStatusMap = CtrContractVersionManager.instance().results().getContractVersionStatusMap(ids(contractVersionMap.values()), new Date());

        Map<Long, List<Pair<Long, String>>> paymentDataMap = new HashMap<>();
        for (Map.Entry<Long, Map<String, ICtrContractResultDao.ICtrContractStatus>> versionEntry : contractVersionStatusMap.entrySet())
        {
            List<Currency> currencyList = getList(Currency.class);
            for (Map.Entry<String, ICtrContractResultDao.ICtrContractStatus> keyVersionStatusEntry : versionEntry.getValue().entrySet())
            {
                for (Currency currency : currencyList)
                {
                    if (EntrantContractPaymentPromiceResultPair.payDirectedTo(keyVersionStatusEntry.getKey(), TopOrgUnit.getInstance().getId(), currency.getCode()))
                    {
                        Collection<ICtrContractResultDao.ICtrContractStatusItem> list = keyVersionStatusEntry.getValue().getList();
                        ICtrContractResultDao.ICtrContractStatusItem[] objects = list.toArray(new ICtrContractResultDao.ICtrContractStatusItem[list.size()]);

                        for (int i = objects.length - 1; i >= 0; i--)
                        {
                            if (objects[i].getTimestamp() <= new Date().getTime() && objects[i].getResultAmount() != 0L)
                            {
                                if (!paymentDataMap.containsKey(versionEntry.getKey())) {
                                    paymentDataMap.put(versionEntry.getKey(), new ArrayList<>());
                                }
                                paymentDataMap.get(versionEntry.getKey()).add(new Pair<>(objects[i].getResultAmount(), currency.getCode()));
                                break;
                            }
                        }
                    }
                }
            }
        }

        // {Направление -> {Специальность -> {Список платежей студентов}}
        Map<String, Multimap<String, List<Pair<Long, String>>>> reportData = new LinkedHashMap<>();
        for (EnrEntrantContract contract : contractList)
        {
            CtrContractVersion contractVersion = contractVersionMap.get(contract.getContractObject().getId());
            if (contractVersion == null) continue;

            EduProgramHigherProf programHigherProf = getContractEduProgram(contract.getContractObject());

            String programSubject = programHigherProf.getProgramSubject().getTitleWithCode();
            String specialization = programHigherProf.getProgramSpecialization().getDisplayableTitle();

            if (!reportData.containsKey(programSubject)) {
                reportData.put(programSubject, ArrayListMultimap.create());
            }

            List<Pair<Long, String>> paymentResult = printRevenue ? paymentDataMap.get(contractVersion.getId()) : null;
            reportData.get(programSubject).put(specialization, paymentResult);
        }
        return reportData;
    }

    /**
     * Возвращает список фактов оплаты по контракту.
     */
    @Override
    public List<CtrPaymentResult> getPaymentsSum(CtrContractObject contract)
    {
        return new DQLSelectBuilder()
                .fromEntity(CtrPaymentResult.class, "p")
                .column(property("p"))
                .where(eq(property(CtrPaymentResult.contract().id().fromAlias("p")), value(contract.getId())))
                .order(property(CtrPaymentResult.timestamp().fromAlias("p")))
                .createStatement(getSession()).list();
    }

    /**
     * Возвращает образовательную программу ВПО из обязательств по котракту.
     */
    @Override
    public EduProgramHigherProf getContractEduProgram(CtrContractObject contract)
    {
        Long contractLastVersion = new DQLSelectBuilder()
                .fromEntity(CtrContractVersion.class, "ver")
                .order(property(CtrContractVersion.creationDate().fromAlias("ver")), OrderDirection.desc)
                .where(eq(property(CtrContractVersion.contract().id().fromAlias("ver")), value(contract.getId())))
                .createStatement(getSession()).setMaxResults(1).idUniqueResult();

        List<CtrContractPromice> promiceList = new DQLSelectBuilder()
                .fromEntity(CtrContractPromice.class, "p", true)
                .column(property("p"))
                .where(eq(property(CtrContractPromice.src().owner().id().fromAlias("p")), value(contractLastVersion)))
                .createStatement(getSession()).list();

        for (CtrContractPromice promise : promiceList)
        {
            if (promise instanceof EduCtrEducationPromise) {
                return (EduProgramHigherProf) ((EduCtrEducationPromise) promise).getEduProgram();
            }
        }
        return null;
    }

    @Override
    public long createReport(CtrEntrContractReportAddUI model)
    {
        IScriptItem template = SharedBaseDao.instance.get().getCatalogItem(CtrScriptItem.class, CtrScriptItemCodes.REPORT_ENTRANT_CONTRACT);
        RtfDocument document = new RtfReader().read(template.getTemplate());

        CtrEntrContractReport report = model.getReport();
        EnrCompetitionFilterAddon filter = model.getCompetitionFilterAddon();

        Map<String, Multimap<String, List<Pair<Long, String>>>> reportData = prepareReportData(report, model.getCompetitionFilterAddon(), model.getPrintRevenue());

        if (reportData.isEmpty()) {
            throw new ApplicationException("Не найдено оплаченных контрактов по выбранным параметрам.");
        }
        report.setFormingDate(new Date());

        fillDocument(document, model, reportData);

        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(document));
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        content.setFilename("Оплаченные договоры " + new DateFormatter(DateFormatter.PATTERN_WITH_TIME).format(report.getFormingDate()) + ".rtf");
        save(content);

        report.setRequestType(getFilterValue(filter.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE), "title","; "));
        report.setCompensationType(getFilterValue(filter.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE), "shortTitle","; "));
        report.setProgramForm(getFilterValue(filter.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM), "title","; "));
        report.setCompetitionType(getFilterValue(filter.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE), "title","; "));
        report.setEnrOrgUnit(getFilterValue(filter.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT), EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s(),"; "));
        report.setFormativeOrgUnit(getFilterValue(filter.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT), "fullTitle","; "));
        report.setProgramSubject(getFilterValue(filter.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT), "title","; "));
        report.setContent(content);
        save(report);

        return report.getId();
    }


    private void fillDocument(RtfDocument document, CtrEntrContractReportAddUI model, Map<String, Multimap<String, List<Pair<Long, String>>>> reportData)
    {
        RtfElement programSubjectElement = (RtfElement) UniRtfUtil.findElement(document.getElementList(), "eduProgramSubject");
        RtfTable tTableElement = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");

        if (!model.getPrintRevenue()) {
            removeRevenueColumn(tTableElement);
        }

        document.getElementList().remove(programSubjectElement);
        document.getElementList().remove(tTableElement);

        getFiltersModifier(model.getCompetitionFilterAddon(), model.getReport().getEnrollmentCampaign()).modify(document);

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("dateForm", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReport().getFormingDate()));
        // Вставляем таблицы
        for (String eduProgramSubject : reportData.keySet())
        {
            document.getElementList().add(programSubjectElement.getClone());
            document.getElementList().add(tTableElement.getClone());
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

            RtfTableModifier tableModifier = new RtfTableModifier();

            initTableData(reportData.get(eduProgramSubject), modifier,  tableModifier);
            modifier.put("eduProgramSubject", eduProgramSubject);

            customizeModifiers(modifier, tableModifier, reportData.get(eduProgramSubject));

            tableModifier.modify(document);
            modifier.modify(document);
        }
    }

    /**
     * Убирает из таблицы столбец Доход и выравнивает оставшиеся два стобца 2/1.
     * Для варианта отчета без колонки Доход. Ширина таблицы вычислятется по первой строке.
     *
     * @param table Таблица.
     */
    protected void removeRevenueColumn(RtfTable table)
    {
        int tableWidth = table.getRowList().get(0).getCellList()
                .stream()
                .mapToInt(RtfCell::getWidth).sum();

        for (RtfRow row : table.getRowList()) {
            RtfUtil.deleteCellPrevIncrease(row, 2);
            row.getCellList().get(0).setWidth(tableWidth/3 * 2);
            row.getCellList().get(1).setWidth(tableWidth/3);
        }
    }

    /**
     * Возвращает RtfInjectModifier с используемыми фильтрами в формате:
     *   Название параметра: выбранное значение
     *   Название параметра2: выбранное значение2
     * Перед фильтрами вставляется выбранная приемная комания.
     */
    private RtfInjectModifier getFiltersModifier(EnrCompetitionFilterAddon filterAddon, EnrEnrollmentCampaign enrollmentCampaign)
    {
        RtfInjectModifier filterHeaderModifier = new RtfInjectModifier();

        RtfString rtfString = new RtfString().append("Приемная кампания: " + enrollmentCampaign.getTitle());
        for (ICommonFilterItem filterItem : filterAddon.getFilterItemList())
        {
            if (filterItem.getValue() == null || !filterItem.isEnableCheckboxChecked()) continue;

            String filterPrintProperty = "title";
            // Из некоторых фильтров нужно доставать не title
            if (filterItem.getSettingsName().equals(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE))
                filterPrintProperty = "shortTitle";
            if (filterItem.getSettingsName().equals(EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT))
                filterPrintProperty = EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s();
            if (filterItem.getSettingsName().equals(EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT))
                filterPrintProperty = "fullTitle";

            rtfString.par();
            rtfString.append(filterItem.getDisplayName() + ": " + getFilterValue(filterItem, filterPrintProperty, ", "));
        }
        filterHeaderModifier.put("H", rtfString);
        customizeFiltersModifier(filterHeaderModifier, filterAddon, enrollmentCampaign);
        return filterHeaderModifier;
    }

    private void initTableData(Multimap<String, List<Pair<Long, String>>> specializationData,
                                 RtfInjectModifier modifier, RtfTableModifier tableModifier)
    {
        List<String[]> specializationTableData = new ArrayList<>();

        // Доход по направлению подготовки (Итого) { Валюта -> Сумма }
        Map<String, Long> totalRevenue = new HashMap<>();
        for (String specialization : specializationData.keySet().stream().sorted().collect(Collectors.toList()))
        {
            // Доход по специализации
            Map<String, Long> specializationRevenue = new HashMap<>();
            for (List<Pair<Long, String>> paymentResultList : specializationData.get(specialization))
            {
                if (paymentResultList == null) continue;

                // Получаем сумму платежей по контракту
                Map<String, Long> paymentResult = paymentResultList.stream()
                        .collect(Collectors.groupingBy(Pair::getY,
                                Collectors.summingLong(Pair::getX)));
                // И добавляем его к доходу по специализации
                specializationRevenue = Stream
                        .concat(specializationRevenue.entrySet().stream(), paymentResult.entrySet().stream())
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (revenue1, revenue2) -> revenue1 + revenue2));
            }
            // Дополняем итоговый доход доходами с специализации
            totalRevenue = Stream.concat(totalRevenue.entrySet().stream(), specializationRevenue.entrySet().stream())
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            Map.Entry::getValue,
                            (revenue1, revenue2) -> revenue1 + revenue2));

            String contractsNumber = String.valueOf(specializationData.get(specialization).size());
            String paymentsSum = getRevenueAsString(specializationRevenue);
            specializationTableData.add(new String[] {specialization, contractsNumber,  paymentsSum});
        }

        tableModifier.put("T", specializationTableData.toArray(new String[specializationTableData.size()][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                return colIndex == 2 ? new RtfString().append(value, true).toList() : super.beforeInject(table, row, cell, rowIndex, colIndex, value);
            }
        });
        modifier.put("total", String.valueOf(specializationData.size()));
        modifier.put("totalRevenue", new RtfString().append(getRevenueAsString(totalRevenue), true));
    }

    /**
     * Для каждого значения revenueHolder форматирует сумму Long в соотвествующей Currency валюте,
     * и объединяет полученные строки через ", \\par ".
     *
     * @param revenueHolder { Валюта -> Сумма }
     *
     * @return Результат. Если мапа пустая, возвращает 0L в рублях.
     */
    private String getRevenueAsString(Map<String, Long> revenueHolder)
    {
        if (revenueHolder.isEmpty()) {
            return MoneyFormatter.ruMoneyFormatter(4).format(0L);
        }
        return revenueHolder.entrySet()
                .stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .map(e -> MoneyFormatter.formatter(e.getKey(), 4).format(e.getValue()))
                .collect(Collectors.joining(", \\par "));
    }


    private String getFilterValue(ICommonFilterItem filterItem, String filterProperty, String separator)
    {
        if (!filterItem.isEnableCheckboxChecked() || filterItem.getValue() == null) {
            return null;
        }
        if (filterItem.getFormConfig().isMultiSelect()) {
            return UniStringUtils.join((Collection)filterItem.getValue(), filterProperty, separator);
        } else {
            return ((IEntity) filterItem.getValue()).getProperty(filterProperty).toString();
        }
    }

    /**
     * Точка расширения. Вызвается после заполнения данных в modifier'e, перед вставкой каждой таблицы.
     *
     * @param specializationData Данные специализаций текущего направления { Специализация -> Платежи }.
     */
    protected void customizeModifiers(RtfInjectModifier modifier, RtfTableModifier tableModifier, Multimap<String, List<Pair<Long, String>>> specializationData)
    {

    }

    /**
     * Точка расширения для секции фильтров. Вызывается после заполнения данных в modifier'е.
     *
     * @param enrollmentCampaign Выбранная приемная комания.
     */
    protected void customizeFiltersModifier(RtfInjectModifier filterModifier, EnrCompetitionFilterAddon filterAddon, EnrEnrollmentCampaign enrollmentCampaign)
    {

    }

}
