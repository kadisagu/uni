/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.ext.EnrReportPerson.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.block.BlockListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;

/**
 * @author Nikolay Fedorovskih
 * @since 06.07.2015
 */
@Configuration
public class EnrReportPersonAddExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = EnrReportPersonAddCtrAddon.class.getSimpleName();
    public static final String ENR_CTR_DATA = "enrCtrData";
    public static final String PREFETCH_HAS_CONTRACTS_MAP = "contractsMap";
    public static final String PREFETCH_HAS_CONTRACTS_PAYMENT_MAP = "contractsPaymentMap";
    @Autowired
    private EnrReportPersonAdd _enrReportPersonAdd;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrReportPersonAdd.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrReportPersonAddCtrAddon.class))
                .create();
    }

    @Bean
    public BlockListExtension enrEntrantDataBlockListExtension()
    {
        return blockListExtensionBuilder(_enrReportPersonAdd.enrEntrantDataBlockListExtPoint())
                .addAllAfter(EnrReportPersonAdd.PASS_DISCIPLINE)
                .addBlock(htmlBlock(ENR_CTR_DATA, "block/entrantData/EnrCtrData"))
                .create();
    }

    @Bean
    public BlockListExtension enrEntrantPrintBlockListExtension()
    {
        return blockListExtensionBuilder(_enrReportPersonAdd.enrEntrantPrintBlockListExtPoint())
                .addBlock(htmlBlock(ENR_CTR_DATA, "block/entrantData/EnrCtrPrint"))
                .create();
    }
}