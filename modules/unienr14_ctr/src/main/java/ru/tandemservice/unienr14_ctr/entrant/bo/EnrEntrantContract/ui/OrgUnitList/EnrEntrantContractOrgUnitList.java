/* $Id$ */
package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.ui.OrgUnitList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.SimpleMergeIdResolver;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.PubCtr.CtrContractVersionPubCtr;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractorRole;
import org.tandemframework.shared.ctr.catalog.entity.CtrPriceCategory;
import org.tandemframework.shared.ctr.catalog.entity.CtrPricePaymentGrid;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.base.entity.IEducationPromise;
import ru.tandemservice.unieductr.base.entity.gen.IEducationContractVersionTemplateDataGen;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic.EnrContractListDataSourceHandler;

import java.util.Collection;

/**
 * @author nvankov
 * @since 9/9/14
 */
@Configuration
public class EnrEntrantContractOrgUnitList extends BusinessComponentManager
{
    public static final String CONTRACT_LIST_DS = "contractListDS";

    public static final String DS_PAYMENT_GRID = "paymentGridDS";
    public static final String DS_PRICE_CATEGORY = "priceCategoryDS";
    public static final String DS_PRICE_SUB_CATEGORY = "priceSubCategoryDS";

    public static final String BIND_PAYMENT_GRID = "paymentGrid";
    public static final String BIND_PRICE_CATEGORY = "priceCategory";
    public static final String BIND_PRICE_SUB_CATEGORY = "priceSubCategory";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(this.searchListDS(CONTRACT_LIST_DS, this.contractListDSColumns(), this.contractListDSHandler()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .addDataSource(selectDS(DS_PAYMENT_GRID, paymentGridDSHandler()))
                .addDataSource(selectDS(DS_PRICE_CATEGORY, priceCategoryDSHandler()))
                .addDataSource(selectDS(DS_PRICE_SUB_CATEGORY, priceSubCategoryDSHandler()).addColumn("title", null, new IFormatter<CtrPriceCategory>() {
                    @Override public String format(CtrPriceCategory source) { return source.getTitle() + (source.getParent() == null ? "" : " (" + source.getParent().getTitle() + ")"); }}))
                .create();
    }

    @Bean
    public ColumnListExtPoint contractListDSColumns() {
        final IMergeRowIdResolver contractMerger = new SimpleMergeIdResolver(CtrContractVersion.contract().id());
        final IPublisherLinkResolver contractLinkResolver = new IPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) {
                final IEduContractRelation rel = (IEduContractRelation)entity.getProperty(EnrContractListDataSourceHandler.VIEW_ENTRANT_REL);
                if (null != rel) { return rel.getId(); }
                return ((CtrContractObject)entity.getProperty(CtrContractVersion.contract())).getId();
            }
            @Override public String getComponentName(final IEntity entity) {
                final IEduContractRelation rel = (IEduContractRelation)entity.getProperty(EnrContractListDataSourceHandler.VIEW_ENTRANT_REL);
                if (null == rel) { return null; }
                return CtrContractVersionPubCtr.class.getSimpleName();
            }
        };

        return this.columnListExtPointBuilder(CONTRACT_LIST_DS)

                // здесь должна быть ссылка на карточку договора (в контексте, если контекста нет - то в контексте самого договора)
                // .addColumn(textColumn("contractNumber", CtrContractVersion.contract().number()).merger(contractMerger).required(true))
                .addColumn(
                        publisherColumn("contractNumber", CtrContractVersion.contract().number())
                                .publisherLinkResolver(contractLinkResolver)
                                .merger(contractMerger)
                                .required(true)
                )

                        // здесь должна быть ссылка на страницу с договорами студента (пока делаем ссылку студентов)
                .addColumn(
                        publisherColumn("contractEntrantList", PersonRole.fullFio().fromAlias(IEduContractRelation.L_PERSON_ROLE))
                                .merger(contractMerger)
                                .entityListProperty(EnrContractListDataSourceHandler.VIEW_ENTRANT_REL_LIST)
                                        // .primaryKeyPath(PersonRole.id().fromAlias(IEduContractRelation.L_PERSON_ROLE))
                                .publisherLinkResolver(new SimplePublisherLinkResolver(PersonRole.id().fromAlias(IEduContractRelation.L_PERSON_ROLE)).param("selectedTab", "contractListTab"))
                                .required(true)
                )
                .addColumn(textColumn("versionTemplateYear", IEducationContractVersionTemplateDataGen.educationYear().title().fromAlias(EnrContractListDataSourceHandler.VIEW_TEMPLATE)).required(true))
                .addColumn(
                        publisherColumn("versionEduPromiseList", IEducationPromise.P_EDUCATION_PROMISE_DESCRIPTION)
                                .entityListProperty(EnrContractListDataSourceHandler.VIEW_EDU_PROMISE_LIST)
                                .primaryKeyPath(IEducationPromise.L_EDUCATION_PROMISE_TARGET + ".id")
                                .required(true)
                )

                .addColumn(textColumn("versionBeginDate", CtrContractVersion.durationBeginDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).required(true))
                .addColumn(textColumn("versionEndDate", CtrContractVersion.durationEndDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).required(true))
                .addColumn(textColumn("versionCustomer", CtrContractVersionContractorRole.contactor().contactor().title().fromAlias(EnrContractListDataSourceHandler.VIEW_ROLE_CUSTOMER)).required(true))
                        // .addColumn(textColumn("versionProvider", CtrContractVersionContractorRole.contactor().contactor().title().fromAlias(EduContractListDataSourceHandler.VIEW_ROLE_PROVIDER)).required(false))
                .addColumn(
                        toggleColumn("paymentExists", EnrEntrantContract.paymentExists().fromAlias(EnrContractListDataSourceHandler.VIEW_ENTRANT_REL))
                                // .merger(contractMerger)
                                .disabled("ui:currentPaymentExistsDisabled")
                                .toggleOffListener("onClickSwitchPaymentExists").toggleOffLabel("Оплаты нет")
                                .toggleOnListener("onClickSwitchPaymentExists").toggleOnLabel("Поступила оплата, дост. для зачисления")
                                .toggleOffListenerAlert(alert("Сбросить оплату для договора «{0}»?", CtrContractVersion.contract().number()))
                                .toggleOnListenerAlert(alert("Зарегистрировать оплату для договора «{0}»?", CtrContractVersion.contract().number()))
                                .permissionKey("enr14ContractListPaymentFlag")
                )
                .addColumn(booleanColumn("active", EnrContractListDataSourceHandler.VIEW_ACTIVE))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> contractListDSHandler() {
        return new EnrContractListDataSourceHandler(this.getName())
                .order(CtrContractVersion.contract().number())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler paymentGridDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), CtrPricePaymentGrid.class)
                .filter(CtrPricePaymentGrid.title())
                .order(CtrPricePaymentGrid.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler priceCategoryDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), CtrPriceCategory.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(DQLExpressions.isNull(DQLExpressions.property(alias, CtrPriceCategory.parent())));
            }
        }
                .filter(CtrPriceCategory.title())
                .order(CtrPriceCategory.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler priceSubCategoryDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), CtrPriceCategory.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(DQLExpressions.isNotNull(DQLExpressions.property(alias, CtrPriceCategory.parent())));

                Collection<CtrPriceCategory> priceCategories = context.get(BIND_PRICE_CATEGORY);
                if (priceCategories != null && !priceCategories.isEmpty())
                    dql.where(DQLExpressions.in(DQLExpressions.property(alias, CtrPriceCategory.parent()), priceCategories));
            }
        }
                .filter(CtrPriceCategory.title())
                .order(CtrPriceCategory.title());
    }
}



    