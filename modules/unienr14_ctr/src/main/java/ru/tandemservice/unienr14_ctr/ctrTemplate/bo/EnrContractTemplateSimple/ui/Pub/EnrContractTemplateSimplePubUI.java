package ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;

@Input({
    @Bind(key= UIPresenter.PUBLISHER_ID, binding="dataHolder.id", required=true)
})
public class EnrContractTemplateSimplePubUI extends UIPresenter
{
    private final EntityHolder<EnrContractTemplateDataSimple> dataHolder = new EntityHolder<>();
    public EntityHolder<EnrContractTemplateDataSimple> getDataHolder() { return this.dataHolder; }
    public EnrContractTemplateDataSimple getTemplateData() { return getDataHolder().getValue(); }

    public CtrContractVersion getContractVersion() { return getTemplateData().getOwner(); }
    public CtrContractObject getContract() { return getContractVersion().getContract(); }

    private EnrEntrantContract entrantContract;
    public EnrEntrantContract getEntrantContract() { return this.entrantContract; }
    public void setEntrantContract(EnrEntrantContract entrantContract) { this.entrantContract = entrantContract; }

    public EnrEntrant getEntrant() {
        if (null == getEntrantContract()) { return null; }
        return getEntrantContract().getEntrant();
    }

    @Override
    public void onComponentRefresh() {
        getDataHolder().refresh(EnrContractTemplateDataSimple.class);
        setEntrantContract(IUniBaseDao.instance.get().get(EnrEntrantContract.class, EnrEntrantContract.contractObject(), getContract()));
    }


}
