package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionDao;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractorRole;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.catalog.entity.CtrPriceCategory;
import org.tandemframework.shared.ctr.catalog.entity.CtrPricePaymentGrid;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.BaseEduContractListDataSourceHandler;
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.ui.GlobalList.EnrEntrantContractGlobalList;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.ui.GlobalList.EnrEntrantContractGlobalListUI;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * CtrContractVersion
 * @author vdanilov
 */
public class EnrContractListDataSourceHandler extends BaseEduContractListDataSourceHandler {

    // связи со студентами (наследники EnrEntrantContract)
    public static final String VIEW_ENTRANT_REL_LIST = BaseEduContractListDataSourceHandler.VIEW_REL_LIST;
    public static final String VIEW_ENTRANT_REL = BaseEduContractListDataSourceHandler.VIEW_REL;

    // обязательства по обчению (только те, которые наследуются от IEducationPromise)
    public static final String VIEW_EDU_PROMISE_LIST = BaseEduContractListDataSourceHandler.VIEW_EDU_PROMISE_LIST;
    public static final String VIEW_EDU_PROMISE = BaseEduContractListDataSourceHandler.VIEW_EDU_PROMISE;

    // роли
    public static final String VIEW_ROLE_LIST = BaseEduContractListDataSourceHandler.VIEW_ROLE_LIST;
    public static final String VIEW_ROLE_CUSTOMER = BaseEduContractListDataSourceHandler.VIEW_ROLE_CUSTOMER;
    public static final String VIEW_ROLE_PROVIDER = BaseEduContractListDataSourceHandler.VIEW_ROLE_PROVIDER;

    // шаблон (только те, которые наследуются от IEducationContractVersionTemplateData)
    public static final String VIEW_TEMPLATE = BaseEduContractListDataSourceHandler.VIEW_TEMPLATE;

    // Действует
    public static final String VIEW_ACTIVE = BaseEduContractListDataSourceHandler.VIEW_ACTIVE;

    // ПК (обязательный)
    public static final String PARAM_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    // данные договора
    public static final String PARAM_CONTRACT_NUMBER = "contractNumber";
    public static final String PARAM_ACTIVE = "active";

    // Данный абуитуриента
    public static final String PARAM_LAST_NAME = "lastName";
    public static final String PARAM_FIRST_NAME = "firstName";
    public static final String PARAM_MIDDLE_NAME = "middleName";

    //Формирующее или лицензированное подразделение
    public static final String PARAM_ORG_UNIT = "orgUnit";

    // утиль (конкурсы)
    public static final String PARAM_ENR_COMPETITION_UTIL = "enrCompetitionUtil";

    // утиль (заказчики)
    public static final String PARAM_CONTACTOR_DQL_QUERY_OR_IDS = "contactorDqlQueryOrIds";

    public static final String PARAM_DATE_FROM = "dateFrom";
    public static final String PARAM_DATE_TO = "dateTo";

    public static final String PARAM_PAYMENT_EXISTS = "paymentExists";

    public EnrContractListDataSourceHandler(final String ownerId) {
        super(ownerId);
    }

    @Override
    protected void applyWhereConditions(final String alias, final DQLSelectBuilder dql, final ExecutionContext context) {
        super.applyWhereConditions(alias, dql, context);

        final String contractNumber = StringUtils.trimToNull(context.<String>get(PARAM_CONTRACT_NUMBER));
        if (null != contractNumber) {
            FilterUtils.applySimpleLikeFilter(dql, alias, CtrContractVersion.contract().number(), contractNumber);
        }

        Object contactorDqlQueryOrIds = context.get(PARAM_CONTACTOR_DQL_QUERY_OR_IDS);
        if (null != contactorDqlQueryOrIds) {
            DQLSelectBuilder tmpDql = new DQLSelectBuilder()
            .fromEntity(CtrContractVersionContractorRole.class, "x")
            .column(property(CtrContractVersionContractorRole.contactor().owner().id().fromAlias("x")))
            .where(eq(property(CtrContractVersionContractorRole.role().code().fromAlias("x")), value(CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER)));
            if (contactorDqlQueryOrIds instanceof Long[]) {
                tmpDql.where(in(property(CtrContractVersionContractorRole.contactor().contactor().id().fromAlias("x")), Arrays.asList((Long[]) contactorDqlQueryOrIds)));
            } else if (contactorDqlQueryOrIds instanceof IDQLExpression) {
                tmpDql.where(in(property(CtrContractVersionContractorRole.contactor().contactor().id().fromAlias("x")), (IDQLExpression) contactorDqlQueryOrIds));
            } else {
                throw new ClassCastException(contactorDqlQueryOrIds.getClass().getName());
            }
            dql.where(in(property(alias, "id"), tmpDql.buildQuery()));
        }


        EnrEnrollmentCampaign ec = context.get(PARAM_ENROLLMENT_CAMPAIGN);

        final DQLSelectBuilder entrantRelationDql = new DQLSelectBuilder()
        .fromEntity(EnrEntrantContract.class, "enrRel")
        .column(property(EnrEntrantContract.contractObject().id().fromAlias("enrRel")))
        .joinPath(DQLJoinType.inner, EnrEntrantContract.requestedCompetition().fromAlias("enrRel"), "reqComp")
        .where(eq(property(EnrRequestedCompetition.request().entrant().enrollmentCampaign().fromAlias("reqComp")), value(ec)));

        DataWrapper paymentExists = context.get(PARAM_PAYMENT_EXISTS);
        if(paymentExists != null)
            entrantRelationDql.where(eq(property(EnrEntrantContract.paymentExists().fromAlias("enrRel")), value(TwinComboDataSourceHandler.getSelectedValueNotNull(paymentExists))));

        boolean joinIdCard = false;
        //noinspection ConstantConditions
        joinIdCard |= FilterUtils.applySimpleLikeFilter(entrantRelationDql, "idCard", IdentityCard.P_LAST_NAME, context.<String>get(PARAM_LAST_NAME));
        joinIdCard |= FilterUtils.applySimpleLikeFilter(entrantRelationDql, "idCard", IdentityCard.P_FIRST_NAME, context.<String>get(PARAM_FIRST_NAME));
        joinIdCard |= FilterUtils.applySimpleLikeFilter(entrantRelationDql, "idCard", IdentityCard.P_MIDDLE_NAME, context.<String>get(PARAM_MIDDLE_NAME));
        if (joinIdCard)
        {
            entrantRelationDql.joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().entrant().person().identityCard().fromAlias("reqComp"), "idCard");
        }

        Long orgUnitId = context.get(PARAM_ORG_UNIT);

        if(orgUnitId != null)
        {
            entrantRelationDql.where(or(
                    eq(property("reqComp", EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().id()), value(orgUnitId)),
                    eq(property("reqComp", EnrRequestedCompetition.competition().programSetOrgUnit().formativeOrgUnit().id()), value(orgUnitId))
            ));
        }

        Date dateFrom = context.get(PARAM_DATE_FROM);
        Date dateTo = context.get(PARAM_DATE_TO);

        if (null != dateFrom || null != dateTo)
        {
            entrantRelationDql.joinEntity("enrRel", DQLJoinType.inner, CtrContractVersion.class, "ccv", eq(property("enrRel", EnrEntrantContract.contractObject()), property("ccv", CtrContractVersion.contract())));
            entrantRelationDql.where(isNull(property("ccv", CtrContractVersion.removalDate())));
            entrantRelationDql.where(isNotNull(property("ccv", CtrContractVersion.activationDate())));
            if (null != dateFrom)
                entrantRelationDql.where(ge(property("ccv", CtrContractVersion.docStartDate()), value(dateFrom, PropertyType.DATE)));
            if (null != dateTo)
                entrantRelationDql.where(le(property("ccv", CtrContractVersion.docStartDate()), value(dateTo, PropertyType.DATE)));
        }

        // TODO: filter

        CommonFilterAddon<EnrCompetition> competitionUtil = context.get(PARAM_ENR_COMPETITION_UTIL);
        if (competitionUtil != null && !competitionUtil.isEmptyFilters())
        {
            entrantRelationDql.where(in(
                property(EnrRequestedCompetition.competition().fromAlias("reqComp")),
                competitionUtil.getEntityIdsFilteredBuilder("a").buildQuery()
            ));

            List<EduProgramProf> eduProgramList = getEduProgramList(competitionUtil);
            if (null != eduProgramList && !eduProgramList.isEmpty())
            {
                entrantRelationDql.where(exists(
                        new DQLSelectBuilder().fromEntity(EduCtrEducationPromise.class, "p").fromEntity(EnrEntrantContract.class, "c")
                                .where(eq(property("c", EnrEntrantContract.contractObject()), property("p", EduCtrEducationPromise.src().owner().contract())))
                                .where(eq(property("c.id"), property("enrRel.id")))
                                .where(in(property("p", EduCtrEducationPromise.eduProgram()), eduProgramList))
                                .buildQuery()
                ));
            }
        }

        // Договор действует
        Boolean active = context.get(PARAM_ACTIVE);
        if (active != null)
        {
            if (active)
            {
                dql.where(and(
                        isNotNull(property(alias, CtrContractVersion.P_ACTIVATION_DATE)),
                        isNull(property(alias, CtrContractVersion.P_REMOVAL_DATE))
                ));
            }
            else
            {
                dql.where(or(
                        isNull(property(alias, CtrContractVersion.P_ACTIVATION_DATE)),
                        isNotNull(property(alias, CtrContractVersion.P_REMOVAL_DATE))
                ));
            }
        }

        //paymentDate filter
        Date paymentDateFrom = context.get(EnrEntrantContractGlobalListUI.PARAM_PAYMENT_DATE_FROM);
        Date paymentDateTo = context.get(EnrEntrantContractGlobalListUI.PARAM_PAYMENT_DATE_TO);
        boolean paymentDateFilter = context.get(EnrEntrantContractGlobalListUI.PAYMENT_DATE_FILTER);
        if (paymentDateFilter) {
            if (paymentDateFrom != null) {
                entrantRelationDql.where(ge(property(EnrEntrantContract.paymentDate().fromAlias("enrRel")), value(paymentDateFrom, PropertyType.DATE)));
            }
            if (paymentDateTo != null) {
                entrantRelationDql.where(le(property(EnrEntrantContract.paymentDate().fromAlias("enrRel")), value(paymentDateTo, PropertyType.DATE)));
            }
        }

        dql.where(in(property(CtrContractVersion.contract().id().fromAlias(alias)), entrantRelationDql.buildQuery()));

        // только договоры на обучение
        dql.where(in(
            property(CtrContractVersion.contract().type().fromAlias(alias)),
            EduContractManager.instance().dao().getContractTypes(null)
        ));

        Collection<CtrPricePaymentGrid> paymentGrids = context.get(EnrEntrantContractGlobalList.BIND_PAYMENT_GRID);
        Collection<CtrPriceCategory> priceCategories = context.get(EnrEntrantContractGlobalList.BIND_PRICE_CATEGORY);
        Collection<CtrPriceCategory> priceSubCategories = context.get(EnrEntrantContractGlobalList.BIND_PRICE_SUB_CATEGORY);

        if ((paymentGrids != null && !paymentGrids.isEmpty()) || (priceCategories != null && !priceCategories.isEmpty()) || (priceSubCategories != null && !priceSubCategories.isEmpty()))
        {
            DQLSelectBuilder versionTemplateBuilder = new DQLSelectBuilder()
                .fromEntity(EduCtrContractVersionTemplateData.class, "vt")
                .joinPath(DQLJoinType.inner, EduCtrContractVersionTemplateData.cost().fromAlias("vt"), "cost")
                .joinPath(DQLJoinType.inner, CtrPriceElementCost.category().fromAlias("cost"), "cat")
                .joinPath(DQLJoinType.left, CtrPriceCategory.parent().fromAlias("cat"), "parent_cat")
                .where(eq(property("vt", EduCtrContractVersionTemplateData.owner().id()), property(alias, CtrContractVersion.id())));

            if (paymentGrids != null && !paymentGrids.isEmpty())
            {
                versionTemplateBuilder
                    .where(in(property("cost", CtrPriceElementCost.paymentGrid()), paymentGrids));
            }

            if (priceSubCategories != null && !priceSubCategories.isEmpty())
            {
                versionTemplateBuilder
                    .where(in(property("cat"), priceSubCategories));

            } else if (priceCategories != null && !priceCategories.isEmpty())
            {
                versionTemplateBuilder
                    .where(or(
                        in(property("cat"), priceCategories),
                        in(property("parent_cat"), priceCategories)));
            }

            dql.where(exists(versionTemplateBuilder.buildQuery()));
        }

        // сортировка по договору, затем в рамках договора по порядку
        dql.order(property(CtrContractVersion.contract().id().fromAlias(alias)));
        dql.order(CtrContractVersionDao.getNaturalOrderExpression(alias));
    }

    @SuppressWarnings("unchecked")
    private List<EduProgramProf> getEduProgramList(CommonFilterAddon<EnrCompetition> util)
    {
        ICommonFilterItem eduProgramFilter = util.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM);
        return (List<EduProgramProf>) eduProgramFilter.getValue();
    }

    @Override
    protected DSOutput execute(final DSInput input, final ExecutionContext context) {
        final DSOutput output = super.execute(input, context);
        final List<Long> recordIds = output.getRecordIds();

        // список связей для договоров
        // contractObject.id -> { relation }
        final Map<Long, List<EnrEntrantContract>> entrantListMap = getContractRelationListMap(context, recordIds, EnrEntrantContract.class, null);

        // обязательства по версиям
        // version.id -> { promise }
        final Map<Long, List<CtrContractPromice>> promiseListMap = getContractVersionPromiseListMap(context, recordIds);

        // роли
        // version.id -> { role }
        final Map<Long, List<CtrContractVersionContractorRole>> roleListMap = getContractVersionRoleListMap(context, recordIds);

        // шаблоны по версиям (только шаблоны на обучение)
        // version.id -> template
        final Map<Long, IEducationContractVersionTemplateData> templateMap = getContractVersionEduTemplateMap(context, recordIds);

        // поехали
        return output.transform((CtrContractVersion version) -> {
            final DataWrapper wrapper = new DataWrapper(version);
            setupEduContractRelationList(wrapper, entrantListMap);
            setupEducationPromiseList(wrapper, promiseListMap);
            setupRoleList(wrapper, roleListMap);
            setupTemplate(wrapper, templateMap);
            setupActive(wrapper);
            return wrapper;
        });
    }

}
