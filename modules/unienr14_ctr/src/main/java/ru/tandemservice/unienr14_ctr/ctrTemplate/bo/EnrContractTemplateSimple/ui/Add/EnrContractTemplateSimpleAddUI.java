package ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.ui.Add;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.StaticFullCheckSelectModel;
import org.tandemframework.shared.ctr.base.bo.Contactor.ui.PickForm.ContactorPickForm;
import org.tandemframework.shared.ctr.base.bo.Contactor.ui.PickForm.ContactorPickFormUI;
import org.tandemframework.shared.ctr.base.bo.Contactor.util.ContactorCategoryMeta;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplateUI;
import org.tandemframework.shared.ctr.base.bo.PhysicalContactor.ui.AddByNextOfKin.PhysicalContactorAddByNextOfKin;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.PhysicalContactor;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.CtrPrintTemplate;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.ContractSides;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractDAO;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.EduProgramPriceManager;
import ru.tandemservice.unieductr.base.bo.EduProgramPrice.utils.EduPriceSelectionModel;
import ru.tandemservice.unieductr.base.entity.EduProgramPrice;
import ru.tandemservice.unieductr.base.entity.gen.EduProgramPriceGen;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.EnrContractTemplateSimpleManager;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.IEnrContractTemplateSimpleAddData;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic.EduProgramSingleEntrantContractObjectFactory;

@Input({
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTEXT_ID, binding="entrantHolder.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_TYPE_ID, binding="contractTypeHolder.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_KIND_ID, binding="versionCreateData.contractKindHolder.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_PRINT_TEMPLATE_ID, binding="versionCreateData.printTemplateHolder.id")
})
public class EnrContractTemplateSimpleAddUI extends UIPresenter implements IEnrContractTemplateSimpleAddData
{
    public static final String REGION_CUSTOMER_PICK = "customerRegion";

    private final EntityHolder<EnrEntrant> entrantHolder = new EntityHolder<>();
    public EntityHolder<EnrEntrant> getEntrantHolder() { return this.entrantHolder; }
    public EnrEntrant getEntrant() { return this.getEntrantHolder().getValue(); }

    private final EntityHolder<CtrContractType> contractTypeHolder = new EntityHolder<>();
    public EntityHolder<CtrContractType> getContractTypeHolder() { return this.contractTypeHolder; }
    @Override public CtrContractType getContractType() { return this.getContractTypeHolder().getValue(); }

    private final CtrContractVersionCreateData versionCreateData = new CtrContractVersionCreateData();

    public CtrContractVersionCreateData getVersionCreateData()
    {
        return versionCreateData;
    }

    private final ISelectModel requestedCompetitionSelectModel = new DQLFullCheckSelectModel(EnrRequestedCompetition.competition().title()) {
        @Override protected DQLSelectBuilder query(final String alias, final String filter) {
            final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EnrRequestedCompetition.class, alias)
            .where(eq(property(EnrRequestedCompetition.request().entrant().fromAlias(alias)), value(EnrContractTemplateSimpleAddUI.this.getEntrant())))
            .where(in(property(EnrRequestedCompetition.competition().type().code().fromAlias(alias)), Arrays.asList(EnrCompetitionTypeCodes.CONTRACT, EnrCompetitionTypeCodes.NO_EXAM_CONTRACT)))
            .where(notIn(property(EnrRequestedCompetition.state().code().fromAlias(alias)), EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY, EnrEntrantStateCodes.ENTRANT_ARCHIVED))
            .where(eq(property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().subjectIndex().programKind().programHigherProf().fromAlias(alias)), value(Boolean.TRUE)));

            if (null != filter) {
                dql.where(this.like(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().title().fromAlias(alias), filter));
            }
            dql.order(property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().subjectIndex().programKind().code().fromAlias(alias)));
            dql.order(property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().subjectIndex().code().fromAlias(alias)));
            dql.order(property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().subjectCode().fromAlias(alias)));
            dql.order(property(EnrRequestedCompetition.competition().programSetOrgUnit().programSet().title().fromAlias(alias)));
            dql.order(property(EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().fromAlias(alias)));
            dql.order(property(EnrRequestedCompetition.competition().requestType().code().fromAlias(alias)));
            dql.order(property(EnrRequestedCompetition.competition().type().code().fromAlias(alias)));
            dql.order(property(EnrRequestedCompetition.priority().fromAlias(alias)));
            return dql;
        }


    };
    public ISelectModel getRequestedCompetitionSelectModel() {  return this.requestedCompetitionSelectModel; }


    private EnrRequestedCompetition requestedCompetition;
    @Override public EnrRequestedCompetition getRequestedCompetition() { return this.requestedCompetition; }
    public void setRequestedCompetition(final EnrRequestedCompetition requestedCompetition) { this.requestedCompetition = requestedCompetition; }

    public EnrCompetition getCompetition() {
        final EnrRequestedCompetition reqComp = this.getRequestedCompetition();
        return null == reqComp ? null : this.getRequestedCompetition().getCompetition();
    }
    @Override public OrgUnit getEffectiveFormativeOrgUnit() {
        final EnrCompetition comp = this.getCompetition();
        return null == comp ? null : comp.getProgramSetOrgUnit().getEffectiveFormativeOrgUnit();
    }

    private final ISelectModel eduProgramSelectModel = new DQLFullCheckSelectModel() {

        @Override
        public String getLabelFor(Object value, int columnIndex)
        {
            return ((EduProgramProf) value).getTitleAndConditionsShortWithForm();
        }

        @Override protected DQLSelectBuilder query(final String alias, final String filter) {

            // конкурс должен быть выбран
            if (null == EnrContractTemplateSimpleAddUI.this.getCompetition()) { return null; }

            final DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(EduProgramProf.class, alias)
            .where(
                // ОП из набора в ВПО
                in(
                    property(alias, "id"),
                    new DQLSelectBuilder()
                    .fromEntity(EnrProgramSetItem.class, "psi")
                    .column(property(EnrProgramSetItem.program().id().fromAlias("psi")))
                    .fromEntity(EnrCompetition.class, "c")
                    .where(eq(property("c"), value(EnrContractTemplateSimpleAddUI.this.getCompetition())))
                    .where(eq(property(EnrCompetition.programSetOrgUnit().programSet().fromAlias("c")), property(EnrProgramSetItem.programSet().fromAlias("psi"))))
                    .where(EnrContractTemplateSimpleAddUI.this.getCompetition().isAllowProgramPriorities() ?
                        in(property("psi", EnrProgramSetItem.id()),
                            new DQLSelectBuilder()
                        .fromEntity(EnrRequestedProgram.class, "p")
                        .column(property("p", EnrRequestedProgram.programSetItem().id()))
                        .where(eq(property("p", EnrRequestedProgram.requestedCompetition()), value(getRequestedCompetition())))
                        .buildQuery()) : null)
                        .buildQuery()));

            if (null != filter) {
                dql.where(this.like(EduProgram.title().fromAlias(alias), filter));
            }
            dql.order(property(EduProgram.title().fromAlias(alias)));
            return dql;
        }
    };
    public ISelectModel getEduProgramSelectModel() { return this.eduProgramSelectModel; }

    private EduProgramProf eduProgram;
    @Override public EduProgramProf getEduProgram() { return this.eduProgram; }
    public void setEduProgram(final EduProgramProf eduProgram) { setEduProgramPrice(getEduProgramPrice(this.eduProgram = eduProgram)); }

    private EduProgramPrice eduProgramPrice;
    public EduProgramPrice getEduProgramPrice() { return this.eduProgramPrice; }
    protected void setEduProgramPrice(EduProgramPrice eduProgramPrice) { this.eduProgramPrice = eduProgramPrice; }

    protected EduProgramPrice getEduProgramPrice(EduProgramProf eduProgram) {
        if (null == eduProgram) { return null; }
        return EduProgramPriceManager.instance().dao().doGetEduProgramPrice(eduProgram.getId());
    }

    public String getProviderSelectorDisplayName() {
        if (null != this.getCompetition()) {
            return "Исполнитель ("+this.getEffectiveFormativeOrgUnit().getFullTitle()+")";
        }
        return "Исполнитель";
    }

    private final ISelectModel providerModel = new StaticFullCheckSelectModel() {
        @Override protected List list() {
            // конкурс должен быть выбран
            if (null == EnrContractTemplateSimpleAddUI.this.getCompetition()) { return Collections.emptyList(); }

            OrgUnit effectiveFormativeOrgUnit = EnrContractTemplateSimpleAddUI.this.getEffectiveFormativeOrgUnit();
            Collection<ContactorPerson> presenters = EduContractManager.instance().dao().getAcademyPresenters(effectiveFormativeOrgUnit);
            return new ArrayList<>(presenters);
        }
    };
    public ISelectModel getProviderModel() { return this.providerModel; }

    private ContactorPerson provider;
    @Override public ContactorPerson getProvider() { return this.provider; }
    public void setProvider(final ContactorPerson provider) { this.provider = provider; }

    private ContactorPerson customer;
    @Override public ContactorPerson getCustomer() { return this.customer; }
    public void setCustomer(final ContactorPerson customer) { this.customer = customer; }


    private Date priceDate = new Date();
    @Override public Date getPriceDate() { return this.priceDate; }
    public void setPriceDate(final Date priceDate) { this.priceDate = priceDate; }

    private Date startDate = new Date();
    @Override public Date getStartDate() { return this.startDate; }
    public void setStartDate(final Date startDate) { this.startDate = startDate; }

    @Override public Date getEnrollmentDate() { return this.getStartDate(); }

    private Date endDate;
    @Override public Date getEndDate() { return this.endDate; }
    public void setEndDate(final Date endDate) { this.endDate = endDate; }

    private Date regDate;
    @Override public Date getRegDate() { return this.regDate; }
    public void setRegDate(final Date regDate) { this.regDate = regDate; }

    private final EduPriceSelectionModel priceSelection = new EduPriceSelectionModel() {
        @Override public Date getPriceDate() {
            return EnrContractTemplateSimpleAddUI.this.getStartDate();
        }
        @Override public ICtrPriceElement getPriceElement() {
            return EnrContractTemplateSimpleAddUI.this.getEduProgramPrice();
        }
    };
    public EduPriceSelectionModel getPriceSelection() { return this.priceSelection; }
    @Override public CtrPriceElementCost getSelectedCost() { return this.getPriceSelection().getSelectedCost(); }
    @Override public List<CtrPriceElementCostStage> getSelectedCostStages() { return this.getPriceSelection().getSelectedCostStages(); }

    @Override public String getCipher() {
        EduProgramPrice price = getEduProgramPrice();
        return null == price ? null : price.getCipher();
    }

    private boolean twoSides = true;
    private boolean threeSidesPerson = false;

    public boolean isTwoSides()
    {
        return twoSides;
    }

    public void setTwoSides(boolean twoSides)
    {
        this.twoSides = twoSides;
    }

    public boolean isThreeSidesPerson()
    {
        return threeSidesPerson;
    }

    public void setThreeSidesPerson(boolean threeSidesPerson)
    {
        this.threeSidesPerson = threeSidesPerson;
    }

    public void onChangeEduProgram() {
        this.setEduProgramPrice(getEduProgramPrice(this.getEduProgram()));
        this.getPriceSelection().refreshCostList();
        setEndDate(getSelectedCost() == null ? null : getSelectedCost().getPeriod().getDurationEnd());
    }

    public void onChangePriceDate() {
        this.getPriceSelection().refreshCostList();
        setEndDate(getSelectedCost() == null ? null : getSelectedCost().getPeriod().getDurationEnd());
    }

    public void onChangePrice() {
        this.getPriceSelection().refreshCostStageList(this.getPriceSelection().getSelectedCost());
    }

    @Override
    public void onComponentActivate() {
        final EnrEntrant entrant = this.getEntrantHolder().refresh(EnrEntrant.class);
        this.setCustomer(EduProgramSingleEntrantContractObjectFactory.getContactPersion(entrant));
    }

    @Override
    public void onComponentRefresh() {
        this.getEntrantHolder().refresh(EnrEntrant.class);
        this.getContractTypeHolder().refresh(CtrContractType.class);
        this.getVersionCreateData().doRefresh();
        ContractSides sides = EduContractManager.instance().dao().getContractSides(getVersionCreateData().getContractKind().getCode());
        if(ContractSides.TWO_SIDES.equals(sides))
        {
            setTwoSides(true);
            setThreeSidesPerson(false);
        }
        else if(ContractSides.THREE_SIDES_PERSON.equals(sides))
        {
            setTwoSides(false);
            setThreeSidesPerson(true);
        }
        else if(ContractSides.THREE_SIDES_ORG.equals(sides))
        {
            setTwoSides(false);
            setThreeSidesPerson(false);
        }
        this.getPriceSelection().refreshCostList();
    }

    @Override
    public void onComponentBindReturnParameters(final String childRegionName, final Map<String, Object> returnedData) {
        if (REGION_CUSTOMER_PICK.equals(childRegionName)) {
            final Object id = returnedData.get(ContactorPickFormUI.CONTACTOR_ID);
            if (id instanceof Long) {
                final ContactorPerson contactor = DataAccessServices.dao().get(ContactorPerson.class, (Long) id);
                if (null != contactor) { this.setCustomer(contactor); }
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(EduContractManager.DS_CUSTOMER.equals(dataSource.getName()))
        {
            dataSource.put(EduContractManager.BIND_PHYSICAL_CONTRACTORS, isTwoSides() || isThreeSidesPerson());
        }
    }

    public void onClickAddCustomer() {
        DataWrapper contactorType = ContactorCategoryMeta.getDataRecordMap().get(isThreeSidesPerson() ? PhysicalContactor.class.getName() : JuridicalContactor.class.getName());

        this.getActivationBuilder().asRegion(ContactorPickForm.class, REGION_CUSTOMER_PICK)
                .parameter(ContactorPickFormUI.PRE_SELECTED_CONTRACT_TYPE, contactorType)
                .activate();
    }

    public void onClickAddCustomerNextOfKin() {
        this.getActivationBuilder().asRegion(PhysicalContactorAddByNextOfKin.class, REGION_CUSTOMER_PICK).parameter(PhysicalContactorAddByNextOfKin.PERSON_ID, getEntrant().getPerson().getId()).activate();
    }

    public boolean isCustomerPickerActive() {
        return (null != this.getSupport().getChildUI(REGION_CUSTOMER_PICK));
    }

    public boolean isApplyDisabled() {
        return this.isCustomerPickerActive();
    }

    public void onClickApply()
    {
        if (this.isApplyDisabled()) { return; }

        if(getUserContext().getErrorCollector().hasErrors())
            return;

        final EnrContractTemplateDataSimple templateData = EnrContractTemplateSimpleManager.instance().dao().doCreateVersion(this);
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, templateData.getOwner().getId()).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickClose()
    {
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickBack()
    {
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.FALSE));
    }


}
