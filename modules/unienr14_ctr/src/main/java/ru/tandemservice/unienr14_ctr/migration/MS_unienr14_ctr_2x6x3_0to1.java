package ru.tandemservice.unienr14_ctr.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unienr14_ctr_2x6x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrReportEntrantsPlanPoints

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_ctr_rep_ent_plan_pts_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false), 
				new DBColumn("datefrom_p", DBType.TIMESTAMP).setNullable(false), 
				new DBColumn("dateto_p", DBType.TIMESTAMP).setNullable(false), 
				new DBColumn("stage_p", DBType.TEXT).setNullable(false), 
				new DBColumn("requesttype_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("programform_p", DBType.TEXT)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("ctrReportEntrantsPlanPoints");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность enr_ctrScriptItem

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_ctr_c_script_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey()
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("ctrScriptItem");

		}


    }
}