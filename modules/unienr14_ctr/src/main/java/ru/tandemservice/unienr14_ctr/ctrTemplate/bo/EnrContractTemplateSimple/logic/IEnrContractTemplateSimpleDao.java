package ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateDAO;

import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple;

/**
 * @author vdanilov
 */
public interface IEnrContractTemplateSimpleDao extends ICtrContractTemplateDAO, INeedPersistenceSupport {

    /**
     * @param ui данные формы содздания
     * @return templateData
     */
    EnrContractTemplateDataSimple doCreateVersion(IEnrContractTemplateSimpleAddData ui);

    /**
     * @param templateData
     */
    void doSaveTemplate(EnrContractTemplateDataSimple templateData);

}
