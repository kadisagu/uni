package ru.tandemservice.unienr14_ctr.settings.bo.EnrReqCompContractCheckMode;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.logic.SettingsOptionGroup;
import ru.tandemservice.unienr14.settings.entity.gen.EnrEnrollmentCampaignSettingsGen;
import ru.tandemservice.unienr14_ctr.request.ext.EnrRequestedCompetition.logic.EnrEntrantContractExistsRule;

/**
 * @author vdanilov
 */
@Configuration
public class EnrReqCompContractCheckModeManager extends BusinessObjectManager {

    public static final String ENR_SETTINGS_CONTRACT_CHECK_MODE = "contractCheckMode";

    public static EnrReqCompContractCheckModeManager instance()
    {
        return instance(EnrReqCompContractCheckModeManager.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler contractCheckModeDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).addItemList(contractCheckModeExtPoint());
    }

    @Bean
    public ItemListExtPoint<DataWrapper> contractCheckModeExtPoint()
    {
        return itemList(DataWrapper.class)
                .add(EnrEntrantContractExistsRule.CHECK_MODE_NONE.toString(), new DataWrapper(EnrEntrantContractExistsRule.CHECK_MODE_NONE, "Договор не требуется"))
                .add(EnrEntrantContractExistsRule.CHECK_MODE_THIS_CONTRACT_ONLY.toString() ,new DataWrapper(EnrEntrantContractExistsRule.CHECK_MODE_THIS_CONTRACT_ONLY, "Требуется договор на выбранный конкурс"))
                .add(EnrEntrantContractExistsRule.CHECK_MODE_THIS_CONTRACT_WITH_PAYMENT.toString() ,new DataWrapper(EnrEntrantContractExistsRule.CHECK_MODE_THIS_CONTRACT_WITH_PAYMENT, "Требуется договор (на данный выбранный конкурс) с оплатой"))
                .add(EnrEntrantContractExistsRule.CHECK_MODE_THIS_CONTRACT_WITH_ANY_PAYMENT.toString() ,new DataWrapper(EnrEntrantContractExistsRule.CHECK_MODE_THIS_CONTRACT_WITH_ANY_PAYMENT, "Требуется договор (на данный выбранный конкурс) и оплата (по любому выбранному конкурсу)"))
                .add(EnrEntrantContractExistsRule.CHECK_MODE_ANY_CONTRACT_ONLY.toString() ,new DataWrapper(EnrEntrantContractExistsRule.CHECK_MODE_ANY_CONTRACT_ONLY, "Требуется договор на любой выбранный конкурс"))
                .add(EnrEntrantContractExistsRule.CHECK_MODE_ANY_CONTRACT_WITH_PAYMENT.toString() ,new DataWrapper(EnrEntrantContractExistsRule.CHECK_MODE_ANY_CONTRACT_WITH_PAYMENT, "Требуется договор на любой выбранный конкурс с оплатой"))
                .create();
    }

    @Bean
    public SettingsOptionGroup contractCheckModeOptionGroup()
    {
        return new SettingsOptionGroup("Правила учета договора при поступлении на места по договору", ENR_SETTINGS_CONTRACT_CHECK_MODE, EnrEnrollmentCampaignSettingsGen.P_ENR_ENTRANT_CONTRACT_EXIST_CHECK_MODE, contractCheckModeDSHandler(), contractCheckModeExtPoint());
    }
}
