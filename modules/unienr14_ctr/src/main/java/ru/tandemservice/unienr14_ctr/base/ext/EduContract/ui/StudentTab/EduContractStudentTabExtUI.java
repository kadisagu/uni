/* $Id$ */
package ru.tandemservice.unienr14_ctr.base.ext.EduContract.ui.StudentTab;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.StudentTab.EduContractStudentTab;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.StudentTab.EduContractStudentTabUI;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.EnrEntrantContractManager;

/**
 * @author Irina Ugfeld
 * @since 12.02.2016
 */
public class EduContractStudentTabExtUI extends UIAddon {


    public EduContractStudentTabExtUI(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    public void onClickAttachEnrContract(){
        if(getPresenter() instanceof EduContractStudentTabUI){
            int contractsCount = EnrEntrantContractManager.instance().dao().doAttachEduContracts(((EduContractStudentTabUI)getPresenter()).getStudent());
            getPresenter().getSupport().info(getPresenterConfig().getProperty("ui.attachContracts.info", contractsCount));


        }
    }

    public boolean isNotAttachEnrContractsExists(){
        return !EnrEntrantContractManager.instance().dao().getExistsNotAttachContracts(((EduContractStudentTabUI)getPresenter()).getStudent());
    }

    public void onClickDetachContract(){
        if(getPresenterConfig().getDataSource(EduContractStudentTab.CONTRACT_LIST_DS) != null)
        {
            DataWrapper studentContract = getPresenterConfig().getDataSource(EduContractStudentTab.CONTRACT_LIST_DS).getRecordById(getListenerParameterAsLong());
            EnrEntrantContractManager.instance().dao().doDetachEnrEntrantContractToStudent(((EduCtrStudentContract)studentContract.getWrapped()).getContractObject().getId());
        }
    }
}