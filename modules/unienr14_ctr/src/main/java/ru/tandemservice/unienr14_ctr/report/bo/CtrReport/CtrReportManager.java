/* $Id:$ */
package ru.tandemservice.unienr14_ctr.report.bo.CtrReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14_ctr.report.bo.CtrReport.ui.EntrantsPlanPointsAdd.logic.CtrReportEntrantsPlanPointsDao;
import ru.tandemservice.unienr14_ctr.report.bo.CtrReport.ui.EntrantsPlanPointsAdd.logic.ICtrReportEntrantsPlanPointsDao;

/**
 * @author rsizonenko
 * @since 23.07.2014
 */
@Configuration
public class CtrReportManager extends BusinessObjectManager
{
    public static CtrReportManager instance()
    {
        return instance(CtrReportManager.class);
    }

    @Bean
    public ICtrReportEntrantsPlanPointsDao entrantsPlanPointsDao()
    {
        return new CtrReportEntrantsPlanPointsDao();
    }
}