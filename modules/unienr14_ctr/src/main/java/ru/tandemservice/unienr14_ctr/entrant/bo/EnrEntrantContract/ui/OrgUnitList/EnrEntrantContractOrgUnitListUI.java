/* $Id$ */
package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.ui.OrgUnitList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.ctr.base.bo.Contactor.ui.FilterBlock.ContactorFilterBlock;
import org.tandemframework.shared.ctr.base.bo.Contactor.ui.FilterBlock.ContactorFilterBlockUI;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.EnrEntrantContractManager;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic.EnrContractListDataSourceHandler;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.ui.GlobalList.EnrEntrantContractGlobalList;

/**
 * @author nvankov
 * @since 9/9/14
 */

@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orgUnitId", required = true)
})
public class EnrEntrantContractOrgUnitListUI extends UIPresenter
{
    private static final String CONTACTOR_FILTER_BLOCK_REGION = "contactorFilterBlockRegion";
    public static final String PARAM_PAYMENT_DATE_FROM = "paymentDateFrom";
    public static final String PARAM_PAYMENT_DATE_TO = "paymentDateTo";
    public static final String PAYMENT_DATE_FILTER = "paymentDateFilter";

    private Long _orgUnitId;
    private OrgUnit _orgUnit;

    public Long getOrgUnitId() { return _orgUnitId; }

    public void setOrgUnitId(Long orgUnitId) { _orgUnitId = orgUnitId; }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    private CommonPostfixPermissionModel _secModel;

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    private EnrEnrollmentCampaign enrollmentCampaign;

    public EnrEnrollmentCampaign getEnrollmentCampaign() { return this.enrollmentCampaign; }

    public void setEnrollmentCampaign(final EnrEnrollmentCampaign enrollmentCampaign) { this.enrollmentCampaign = enrollmentCampaign; }

    private BaseSearchListDataSource contractListDataSource;

    public BaseSearchListDataSource getContractListDataSource() { return this.contractListDataSource; }

    private CommonFilterAddon<EnrCompetition> competitionUtil;

    public CommonFilterAddon<EnrCompetition> getCompetitionUtil() { return this.competitionUtil; }

    @Override
    @SuppressWarnings("unchecked")
    public void onComponentRefresh()
    {
        this.setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        this.contractListDataSource = (BaseSearchListDataSource) this.getConfig().getDataSource(EnrEntrantContractGlobalList.CONTRACT_LIST_DS);

        setOrgUnit(DataAccessServices.dao().<OrgUnit>get(_orgUnitId));
        setSecModel(new OrgUnitSecModel(getOrgUnit()));

        final IUIPresenter filterBlock = getContactorFilterUI();
        if (filterBlock == null)
        {
            this._uiActivation.asRegion(ContactorFilterBlock.class, CONTACTOR_FILTER_BLOCK_REGION).activate();
        }
        else
        {
            filterBlock.onComponentRefresh();
        }

        this.competitionUtil = (CommonFilterAddon<EnrCompetition>) this.getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
        this.competitionUtil
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(false)
                .configSettings(this.getSettingsKey());

        this.competitionUtil.clearFilterItems();
        this.competitionUtil
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                        // .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG);

        this.configWhereFilters();
    }

    public ContactorFilterBlockUI getContactorFilterUI()
    {
        return this._uiSupport.getChildUI(CONTACTOR_FILTER_BLOCK_REGION);
    }

    private void configWhereFilters()
    {
        final CommonFilterAddon<EnrCompetition> util = this.getCompetitionUtil();
        if (util != null)
        {
            util
                    .clearWhereFilter()
                    .configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), this.getEnrollmentCampaign()))
                    .configWhereOrFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().institutionOrgUnit().orgUnit().id(), this.getOrgUnitId()), "formativeOrEnrOrgUnit")
                    .configWhereOrFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().formativeOrgUnit().id(), this.getOrgUnitId()), "formativeOrEnrOrgUnit");
        }
    }

    @Override
    public void onBeforeDataSourceFetch(final IUIDataSource dataSource)
    {
        if (EnrEntrantContractGlobalList.CONTRACT_LIST_DS.equals(dataSource.getName()))
        {
            dataSource.put(EnrContractListDataSourceHandler.PARAM_ENROLLMENT_CAMPAIGN, this.getEnrollmentCampaign());
            dataSource.put(EnrContractListDataSourceHandler.PARAM_ENR_COMPETITION_UTIL, this.getCompetitionUtil());
            dataSource.put(EnrContractListDataSourceHandler.PARAM_CONTACTOR_DQL_QUERY_OR_IDS, getContactorFilterUI().getQueryOrIds());

            dataSource.putAll(this.getSettings().getAsMap(
                    EnrContractListDataSourceHandler.PARAM_CONTRACT_NUMBER,
                    EnrContractListDataSourceHandler.PARAM_LAST_NAME,
                    EnrContractListDataSourceHandler.PARAM_FIRST_NAME,
                    EnrContractListDataSourceHandler.PARAM_MIDDLE_NAME,
                    EnrContractListDataSourceHandler.PARAM_DATE_FROM,
                    EnrContractListDataSourceHandler.PARAM_DATE_TO,
                    EnrContractListDataSourceHandler.PARAM_PAYMENT_EXISTS,
                    PARAM_PAYMENT_DATE_FROM,
                    PARAM_PAYMENT_DATE_TO
            ));


            IIdentifiable active = getSettings().get(EnrContractListDataSourceHandler.PARAM_ACTIVE);
            dataSource.put(EnrContractListDataSourceHandler.PARAM_ACTIVE, active != null ? TwinComboDataSourceHandler.getSelectedValue(active) : null);
            dataSource.put(EnrContractListDataSourceHandler.PARAM_ORG_UNIT, _orgUnitId);
            dataSource.put(PAYMENT_DATE_FILTER, !isPaymentDateDisable());
        }

        dataSource.put(EnrEntrantContractGlobalList.BIND_PAYMENT_GRID, this.getSettings().get("paymentGrid"));
        dataSource.put(EnrEntrantContractGlobalList.BIND_PRICE_CATEGORY, this.getSettings().get("priceCategory"));
        dataSource.put(EnrEntrantContractGlobalList.BIND_PRICE_SUB_CATEGORY, this.getSettings().get("priceSubCategory"));
    }

    public void onClickSwitchPaymentExists()
    {
        final Long id = this.getSupport().getListenerParameterAsLong();
        final DataWrapper record = this.getContractListDataSource().getRecordById(id);
        final EnrEntrantContract rel = record.get(EnrContractListDataSourceHandler.VIEW_ENTRANT_REL);
        EnrEntrantContractManager.instance().dao().doSetPaymentDate(rel.getId(), rel.isPaymentExists());
        getSupport().setRefreshScheduled(true);
    }

    public boolean isNothingSelected() { return null == this.getEnrollmentCampaign(); }

    public boolean isCurrentPaymentExistsDisabled()
    {
        // там внутри может быть dataWrapper
        return Boolean.TRUE.equals(
                ((IEntity) getContractListDataSource().getCurrent()).getProperty(
                        EnrEntrantContract.paymentResultExists().fromAlias(EnrContractListDataSourceHandler.VIEW_ENTRANT_REL)
                )
        );
    }

    public void onEnrCampaignRefresh()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(this.getEnrollmentCampaign());
        this.configWhereFilters();
    }

    public void onClickSearch()
    {
        this.getSettings().save();
        final CommonFilterAddon<EnrCompetition> util = this.getCompetitionUtil();
        if (util != null)
        {
            util.saveSettings();
        }
    }

    public void onClickClear()
    {
        this.getSettings().clear();
        final CommonFilterAddon<EnrCompetition> util = this.getCompetitionUtil();
        if (util != null)
        {
            util.clearSettings();
            this.configWhereFilters();
        }
        this.onClickSearch();
    }

    public boolean isPaymentDateDisable()
    {
        return getSettings().get(EnrContractListDataSourceHandler.PARAM_PAYMENT_EXISTS) == null ||
                ((DataWrapper)getSettings().get(EnrContractListDataSourceHandler.PARAM_PAYMENT_EXISTS)).getId().equals(TwinComboDataSourceHandler.NO_ID);
    }
}
