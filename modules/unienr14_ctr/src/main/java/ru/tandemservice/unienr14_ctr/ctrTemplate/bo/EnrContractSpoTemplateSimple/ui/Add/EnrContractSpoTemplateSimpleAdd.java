package ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.PhysicalContactor;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;

@Configuration
public class EnrContractSpoTemplateSimpleAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
        .addDataSource(EduContractManager.instance().customerDSConfig())
        .create();
    }
}
