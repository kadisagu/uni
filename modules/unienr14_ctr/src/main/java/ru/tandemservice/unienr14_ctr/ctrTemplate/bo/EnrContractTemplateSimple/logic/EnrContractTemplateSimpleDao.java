package ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic;

import java.util.*;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractKindSelectWrapper;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrVersionTemplatePromiceRestrictions;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.CtrTemplateScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractPrintUtils;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduCtrPaymentPromiceRestrictions;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractKindCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractTypeCodes;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.ui.Add.EnrContractTemplateSimpleAdd;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic.EduProgramSingleEntrantContractObjectFactory;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author vdanilov
 */
public class EnrContractTemplateSimpleDao extends UniBaseDao implements IEnrContractTemplateSimpleDao {

    // базовые (статические) методы для формирования и заполнения договора (если хочется, можно переопределить фабрику польностью в buildContractFactory)
    protected static abstract class ContractFactory<T extends IEnrContractTemplateSimpleAddData> extends EduProgramSingleEntrantContractObjectFactory
    {
        protected abstract T getUi();

        @Override protected EnrEntrant getStudent() { return getUi().getRequestedCompetition().getRequest().getEntrant(); }
        @Override protected OrgUnit getCtrContractObjectOrgUnit() { return getUi().getEffectiveFormativeOrgUnit(); }
        @Override protected CtrContractType getCtrContractObjectType() { return getUi().getContractType(); }
        @Override protected ContactorPerson getCustomer() { return getUi().getCustomer(); }
        @Override protected Collection<ContactorPerson> getAcademyPresenters(final CtrContractObject contractObject) {
            return Collections.singleton(getUi().getProvider());
        }

        @Override protected EduProgramProf getStudentEduProgram(final EnrEntrant entrant) { return getUi().getEduProgram(); }
        @Override protected Date getStudentEnrollmentDate(final EnrEntrant entrant) { return getUi().getEnrollmentDate(); }

        @Override protected Collection<CtrPriceElementCost> getEducationPromiseCostList(final EnrEntrant entrant, final EduCtrEducationPromise eduPromise, final CtrContractVersionContractor paymentSource, final Currency defaultCurrency) {
            final CtrPriceElementCost selectedCost = getUi().getSelectedCost();
            return null == selectedCost ? Collections.<CtrPriceElementCost>emptyList() : Collections.singleton(selectedCost);
        }

        @Override protected List<CtrPriceElementCostStage> getEducationPromiseCostListStages(final EnrEntrant student, final EduCtrEducationPromise eduPromise, final CtrPriceElementCost cost) {
            final List<CtrPriceElementCostStage> stageList = getUi().getSelectedCostStages();
            return null == stageList ? Collections.<CtrPriceElementCostStage>emptyList() : stageList;
        }
    }

    // метод для переопределения в проектном слое генерации номеров договоров
    protected String getContractObjectNumber(final IEnrContractTemplateSimpleAddData ui) {
        final EducationYear educationYear = ui.getRequestedCompetition().getRequest().getEntrant().getEnrollmentCampaign().getEducationYear();
        return EduContractManager.instance().dao().doGetNextNumber(String.valueOf(educationYear.getIntValue()));
    }

    // создает фабрику договоров (можно переопределять, если хочется)
    protected EduProgramSingleEntrantContractObjectFactory buildContractFactory(final IEnrContractTemplateSimpleAddData ui) {
        return new ContractFactory<IEnrContractTemplateSimpleAddData>() {
            @Override protected IEnrContractTemplateSimpleAddData getUi() { return ui; }

            @Override
            protected CtrContractVersionCreateData getVersionCreateData()
            {
                return getUi().getVersionCreateData();
            }

            @Override protected String getCtrContractObjectNumber() { return getContractObjectNumber(getUi()); }
        };
    }

    @Override
    public EnrContractTemplateDataSimple doCreateVersion(final IEnrContractTemplateSimpleAddData ui) {

        final EnrRequestedCompetition requestedCompetition = ui.getRequestedCompetition();
        final EnrEntrant entrant = requestedCompetition.getRequest().getEntrant();

        // создаем версию
        final CtrContractVersion version = buildContractFactory(ui).buildContractObject();

        // устанавливаем данные с формы в версию
        version.setContractRegistrationDate(new Date());
        version.setKind(ui.getVersionCreateData().getContractKind());
        version.setPrintTemplate(ui.getVersionCreateData().getPrintTemplate());

        // связываем версию с абитуриентом
        final EnrEntrantContract entrantContract = new EnrEntrantContract();
        entrantContract.setContractObject(version.getContract());
        entrantContract.setRequestedCompetition(requestedCompetition);
        this.save(entrantContract);

        // создаем шаблон (договора)
        final EnrContractTemplateDataSimple templateData = new EnrContractTemplateDataSimple();
        templateData.setCipher(ui.getCipher());
        templateData.setOwner(version);
        templateData.setRequestedCompetition(requestedCompetition);
        templateData.setEducationYear(entrant.getEnrollmentCampaign().getEducationYear()); // для договора - это тот же год, что и год ПК
        templateData.setCost(ui.getSelectedCost());
        this.save(templateData);

        // врзвращаем результат
        return templateData;
    }

    @Override
    public Map<CtrContractType, List<CtrContractKindSelectWrapper>> getVersionAddComponent(final Long contextEntityId)
    {
        final Map<CtrContractType, List<CtrContractKindSelectWrapper>> map = new HashMap<>();
        if (EnrEntrant.class.equals(EntityRuntime.getMeta(contextEntityId).getEntityClass()) && existsEntity(new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc")
            .where(eq(property("rc", EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().subjectIndex().programKind().programHigherProf()), value(true)))
            .where(eq(property("rc", EnrRequestedCompetition.request().entrant().id()), value(contextEntityId)))
            .buildQuery()))
        {
            CtrContractType contractType = getCatalogItem(CtrContractType.class, CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE_V_O);
            SafeMap.safeGet(map, contractType, ArrayList.class).add(new CtrContractKindSelectWrapper(getCatalogItem(CtrContractKind.class, CtrContractKindCodes.EDU_CONTRACT_VO_2_SIDES), EnrContractTemplateSimpleAdd.class));
            SafeMap.safeGet(map, contractType, ArrayList.class).add(new CtrContractKindSelectWrapper(getCatalogItem(CtrContractKind.class, CtrContractKindCodes.EDU_CONTRACT_VO_3_SIDES_PERSON), EnrContractTemplateSimpleAdd.class));
            SafeMap.safeGet(map, contractType, ArrayList.class).add(new CtrContractKindSelectWrapper(getCatalogItem(CtrContractKind.class, CtrContractKindCodes.EDU_CONTRACT_VO_3_SIDES_ORG), EnrContractTemplateSimpleAdd.class));
        }
        return map;
    }

    @Override
    public ICtrVersionTemplatePromiceRestrictions getPromiceRestrictions(final Class<? extends CtrContractPromice> promiceClass, final CtrContractVersionTemplateData templateData)
    {
        // проверяем, что это тот самый шаблон
        if (!(templateData instanceof EnrContractTemplateDataSimple)) {
            throw new IllegalStateException();
        }

        // для обязательств по оплате отделные разрешения
        if (CtrPaymentPromice.class.equals(promiceClass)) {
            return EduCtrPaymentPromiceRestrictions.get(templateData.getOwner());
        }

        // запрещаем редактировать все, кроме обязательств по оплате
        return ICtrVersionTemplatePromiceRestrictions.DENY_ALL;
    }

    @Override
    public void doSaveTemplate(final EnrContractTemplateDataSimple templateData) {
        this.saveOrUpdate(templateData.getOwner());
        this.saveOrUpdate(templateData);
    }

    @SuppressWarnings("unused")
    @Override
    public IDocumentRenderer print(final CtrContractVersionTemplateData templateData)
    {
        // чужие версии не печатаем
        if (!(templateData instanceof EnrContractTemplateDataSimple)) {
            throw new IllegalStateException();
        }

        final EnrContractTemplateDataSimple versionTemplateData = (EnrContractTemplateDataSimple) templateData;
        final CtrContractVersion contractVersion = templateData.getOwner();

        // проверяем, что можем печатать по шаблону

        final CtrContractVersionContractor providerRel = CtrContractVersionManager.instance().dao().getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);
        if (null == providerRel) {
            throw new ApplicationException("Печать по шаблону невозможна - для версии договора не указан исполнитель.");
        }

        final CtrContractVersionContractor customerRel = CtrContractVersionManager.instance().dao().getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);
        if (null == customerRel) {
            throw new ApplicationException("Печать по шаблону невозможна - для версии договора не указан заказчик.");
        }

        if (!(providerRel.getContactor() instanceof EmployeePostContactor)) {
            throw new ApplicationException("Печать по шаблону невозможна - неизвестный тип исполнителя.");
        }

        final List<EduCtrEducationPromise> promiseList = this.getList(EduCtrEducationPromise.class, EduCtrEducationPromise.dst().owner(), contractVersion);
        if (promiseList.size() > 1) {
            throw new ApplicationException("Печать по шаблону невозможна - в версии договора более одного обязательства по обучению по ОП.");
        }
        if (promiseList.size() < 1) {
            throw new ApplicationException("Печать по шаблону невозможна - в версии договора отсутствуют обязательства по обучению по ОП.");
        }

        final EduCtrEducationPromise eduPromise = promiseList.get(0);

        final Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(
            contractVersion.getPrintTemplate(),
            "versionTemplateDataId", versionTemplateData.getId(),
            "customerId", customerRel.getId(),
            "providerId", providerRel.getId(),
            "eduPromiseId", eduPromise.getId()
        );
        return (IDocumentRenderer)result.get(IScriptExecutor.RENDERER);
    }

    @Override
    public boolean isAllowDeleteTemplate(final CtrContractVersionTemplateData templateData) {
        return false;
    }

    @Override
    public boolean isAllowCreateNextVersionByCopy(final CtrContractVersionTemplateData templateData) {
        return false;
    }

    @Override
    public boolean isAllowCreateNextVersionByTemplate(final CtrContractVersionTemplateData templateData) {
        return true;
    }


}
