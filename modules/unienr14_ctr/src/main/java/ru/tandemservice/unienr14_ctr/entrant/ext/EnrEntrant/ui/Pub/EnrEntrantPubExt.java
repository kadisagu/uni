package ru.tandemservice.unienr14_ctr.entrant.ext.EnrEntrant.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;

import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.ui.ListTab.EnrEntrantContractListTab;

/**
 * @author vdanilov
 */
@Configuration
public class EnrEntrantPubExt extends BusinessComponentExtensionManager {

    @Autowired
    private EnrEntrantPub _enrEntrantPub;

    @Bean
    public TabPanelExtension entrantPubTabPanelExtension() {
        return tabPanelExtensionBuilder(_enrEntrantPub.entrantPubTabPanelExtPoint())
        .addTab(componentTab("contractListTab", EnrEntrantContractListTab.class).permissionKey("enr14EntrantPubContractListTabView"))
        .create();
    }

}
