package ru.tandemservice.unienr14_ctr.base.entity;

import ru.tandemservice.unienr14_ctr.base.entity.gen.*;

/**
 * Данные шаблона договора на обучение абитуриента
 *
 * Данные шаблона для создания договора на обучение абитуриента.
 */
public abstract class EnrContractTemplateData extends EnrContractTemplateDataGen
{
}