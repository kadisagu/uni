package ru.tandemservice.unienr14_ctr.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные шаблона договора на обучение абитуриента
 *
 * Данные шаблона для создания договора на обучение абитуриента.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrContractTemplateDataGen extends EduCtrContractVersionTemplateData
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateData";
    public static final String ENTITY_NAME = "enrContractTemplateData";
    public static final int VERSION_HASH = -874793358;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUESTED_COMPETITION = "requestedCompetition";

    private EnrRequestedCompetition _requestedCompetition;     // Выбранный конкурс абитуриента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выбранный конкурс абитуриента. Свойство не может быть null.
     */
    @NotNull
    public EnrRequestedCompetition getRequestedCompetition()
    {
        return _requestedCompetition;
    }

    /**
     * @param requestedCompetition Выбранный конкурс абитуриента. Свойство не может быть null.
     */
    public void setRequestedCompetition(EnrRequestedCompetition requestedCompetition)
    {
        dirty(_requestedCompetition, requestedCompetition);
        _requestedCompetition = requestedCompetition;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrContractTemplateDataGen)
        {
            setRequestedCompetition(((EnrContractTemplateData)another).getRequestedCompetition());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrContractTemplateDataGen> extends EduCtrContractVersionTemplateData.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrContractTemplateData.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EnrContractTemplateData is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "requestedCompetition":
                    return obj.getRequestedCompetition();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "requestedCompetition":
                    obj.setRequestedCompetition((EnrRequestedCompetition) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "requestedCompetition":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "requestedCompetition":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "requestedCompetition":
                    return EnrRequestedCompetition.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrContractTemplateData> _dslPath = new Path<EnrContractTemplateData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrContractTemplateData");
    }
            

    /**
     * @return Выбранный конкурс абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateData#getRequestedCompetition()
     */
    public static EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
    {
        return _dslPath.requestedCompetition();
    }

    public static class Path<E extends EnrContractTemplateData> extends EduCtrContractVersionTemplateData.Path<E>
    {
        private EnrRequestedCompetition.Path<EnrRequestedCompetition> _requestedCompetition;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выбранный конкурс абитуриента. Свойство не может быть null.
     * @see ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateData#getRequestedCompetition()
     */
        public EnrRequestedCompetition.Path<EnrRequestedCompetition> requestedCompetition()
        {
            if(_requestedCompetition == null )
                _requestedCompetition = new EnrRequestedCompetition.Path<EnrRequestedCompetition>(L_REQUESTED_COMPETITION, this);
            return _requestedCompetition;
        }

        public Class getEntityClass()
        {
            return EnrContractTemplateData.class;
        }

        public String getEntityName()
        {
            return "enrContractTemplateData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
