/* $Id$ */
package ru.tandemservice.unienr14_ctr.settings.ext.EnrSettings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.EnrSettingsManager;
import ru.tandemservice.unienr14.settings.bo.EnrSettings.logic.SettingsOptionGroup;
import ru.tandemservice.unienr14_ctr.settings.bo.EnrReqCompContractCheckMode.EnrReqCompContractCheckModeManager;

/**
 * @author nvankov
 * @since 2/18/16
 */
@Configuration
public class EnrSettingsExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private EnrSettingsManager _enrSettingsManager;

    @Bean
    public ItemListExtension<SettingsOptionGroup> settingsOptionsGroupExtension()
    {
        return itemListExtension(_enrSettingsManager.settingsOptionsGroupExtPoint())
                .add(EnrReqCompContractCheckModeManager.ENR_SETTINGS_CONTRACT_CHECK_MODE, EnrReqCompContractCheckModeManager.instance().contractCheckModeOptionGroup())
                .create();
    }


}
