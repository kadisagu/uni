package ru.tandemservice.unienr14_ctr.migration;

import com.google.common.collect.Maps;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.ctr.base.util.CtrMigrationUtil;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_ctr_2x10x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        final Map<String, Long> ctrPrintTemplateMap = Maps.newHashMap();

        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrContractVersion

        // заполняем printTemplate и kind

        SQLSelectQuery selectContractVersionQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")).column("id")
                .where("kind_id is null or printtemplate_id is null");

        List<Object[]> contractVersionList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectContractVersionQuery));

        for (Object[] contractVersion : contractVersionList)
        {
            setCtrPrintTemplate((Long) contractVersion[0], tool);
        }

        // Проверяем, что нет версий без вида, если нет, то делаем колонку обязательной
        SQLSelectQuery selectCtrVerQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")).column("id")
                .where("kind_id is null");

        List<Object[]> ctrVerList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectContractVersionQuery));

        if (ctrVerList.isEmpty())
        {
            tool.setColumnNullable("ctr_contractver_t", "kind_id", false);
        }
    }

    private void setCtrPrintTemplate(Long contractVersionId, DBTool tool) throws SQLException
    {
        // Получаем уже существующие элементы ctrcontractkind_t
        Map<String, Long> contractKindMap = MigrationUtils.getCatalogCode2IdMap(tool, "ctrcontractkind_t");

        // Получаем уже существующие элементы ctrPrintTemplate
        final Map<String, Long> ctrPrintTemplateMap = Maps.newHashMap();

        SQLSelectQuery selectCtrPrintTemplateQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_c_prnt_tmpl_t", "pt").innerJoin(SQLFrom.table("scriptitem_t", "si"), "si.id=pt.id"))
                .column("si.id")
                .column("si.code_p");

        List<Object[]> ctrPrintTemplates = tool.executeQuery(
                MigrationUtils.processor(Long.class, String.class),
                tool.getDialect().getSQLTranslator().toSql(selectCtrPrintTemplateQuery));

        for(Object[] obj : ctrPrintTemplates)
        {
            ctrPrintTemplateMap.put((String) obj[1], (Long) obj[0]);
        }

        SQLUpdateQuery updateQuery = new SQLUpdateQuery("ctr_contractver_t")
                .set("kind_id", "?")
                .set("printtemplate_id", "?")
                .where("id=?");


        // CtrPrintTemplateCodes
        /** Константа кода (code) элемента : Основной (title) */
        String EDU_CONTRACT_TEMPLATE_VO_2_SIDES = "edu.ctr.template.vo.2s";
        /** Константа кода (code) элемента : Основной (title) */
        String EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON = "edu.ctr.template.vo.3sp";
        /** Константа кода (code) элемента : Основной (title) */
        String EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG = "edu.ctr.template.vo.3so";
        /** Константа кода (code) элемента : Основной (title) */
        String EDU_CONTRACT_TEMPLATE_SPO_2_SIDES = "edu.ctr.template.spo.2s";
        /** Константа кода (code) элемента : Основной (title) */
        String EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_PERSON = "edu.ctr.template.spo.3sp";
        /** Константа кода (code) элемента : Основной (title) */
        String EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_ORG = "edu.ctr.template.spo.3so";

        // CtrContractKindCodes
        /** Константа кода (code) элемента : Договор на обучение по ОП ВО (двухсторонний) (title) */
        String EDU_CONTRACT_VO_2_SIDES = "edu.vo.2s";
        /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с физ. лицом) (title) */
        String EDU_CONTRACT_VO_3_SIDES_PERSON = "edu.vo.3sp";
        /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с юр. лицом) (title) */
        String EDU_CONTRACT_VO_3_SIDES_ORG = "edu.vo.3so";
        /** Константа кода (code) элемента : Договор на обучение по ОП СПО (двухсторонний) (title) */
        String EDU_CONTRACT_SPO_2_SIDES = "edu.spo.2s";
        /** Константа кода (code) элемента : Договор на обучение по ОП СПО (трехсторонний с физ. лицом) (title) */
        String EDU_CONTRACT_SPO_3_SIDES_PERSON = "edu.spo.3sp";
        /** Константа кода (code) элемента : Договор на обучение по ОП СПО (трехсторонний с юр. лицом) (title) */
        String EDU_CONTRACT_SPO_3_SIDES_ORG = "edu.spo.3so";

        // пропускаем если не "Договор на обучение ВО" ("01.01.vpo")
        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "enr14_ctmpldt_simple_t", tool) && CtrMigrationUtil.checkContractType(contractVersionId, "01.01.vpo", tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            EDU_CONTRACT_VO_2_SIDES,
                            EDU_CONTRACT_VO_3_SIDES_PERSON,
                            EDU_CONTRACT_VO_3_SIDES_ORG,
                            contractKindMap, tool),
                    getPrintTemplate(contractVersionId,
                            EDU_CONTRACT_TEMPLATE_VO_2_SIDES,
                            EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON,
                            EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG,
                            ctrPrintTemplateMap, tool),
                    contractVersionId);
            return;
        }

        // пропускаем если не "Договор на обучение СПО" ("01.01.spo")
        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "enr14_ctmpldt_spo_t", tool) && CtrMigrationUtil.checkContractType(contractVersionId, "01.01.spo", tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            EDU_CONTRACT_SPO_2_SIDES,
                            EDU_CONTRACT_SPO_3_SIDES_PERSON,
                            EDU_CONTRACT_SPO_3_SIDES_ORG,
                            contractKindMap, tool),
                    getPrintTemplate(contractVersionId,
                            EDU_CONTRACT_TEMPLATE_SPO_2_SIDES,
                            EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_PERSON,
                            EDU_CONTRACT_TEMPLATE_SPO_3_SIDES_ORG,
                            ctrPrintTemplateMap, tool),
                    contractVersionId);
        }
    }

    private Long getKind(Long contractVersionId, String twoSide, String threeSidePerson, String threeSideOrg, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        Long ctrKind;

        if(checkTwoSides(contractVersionId, tool))
        {
            ctrKind = contractKindMap.get(twoSide);
        }
        else
        {
            if(checkPersonSide(contractVersionId, tool))
            {
                ctrKind = contractKindMap.get(threeSidePerson);
            }
            else
            {
                ctrKind = contractKindMap.get(threeSideOrg);
            }
        }

        return ctrKind;
    }

    private Long getPrintTemplate(Long contractVersionId, String twoSide, String threeSidePerson, String threeSideOrg, Map<String, Long> ctrPrintTemplateMap, DBTool tool) throws SQLException
    {
        Long ctrPrintTemplate;

        if(checkTwoSides(contractVersionId, tool))
        {
            ctrPrintTemplate = ctrPrintTemplateMap.get(twoSide);
        }
        else
        {
            if(checkPersonSide(contractVersionId, tool))
            {
                ctrPrintTemplate = ctrPrintTemplateMap.get(threeSidePerson);
            }
            else
            {
                ctrPrintTemplate = ctrPrintTemplateMap.get(threeSideOrg);
            }
        }

        return ctrPrintTemplate;
    }

    private boolean checkTwoSides(Long contractVersionId, DBTool tool) throws SQLException
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();
        SQLSelectQuery contractorQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractor_role_t", "cr")
                .innerJoin(SQLFrom.table("ctr_c_contract_role_t", "r"), "r.id=cr.role_id")
                .innerJoin(SQLFrom.table("ctr_contractor_t", "c"), "c.id=cr.contactor_id")
                .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=c.contactor_id")
        )
                .where("c.owner_id=?")
                .where("r.code_p=?")
                .column("pr.person_id");

        // CtrContractRole
        /** Константа кода (code) элемента : Заказчик (title) */
        String EDU_CONTRACT_CUSTOMER = "01.01.customer";
        /** Константа кода (code) элемента : Исполнитель (title) */
        String EDU_CONTRACT_PROVIDER = "01.01.provider";

        Long providerPerson = (Long) tool.getUniqueResult(translator.toSql(contractorQuery), contractVersionId, EDU_CONTRACT_PROVIDER);
        Long customerPerson = (Long) tool.getUniqueResult(translator.toSql(contractorQuery), contractVersionId, EDU_CONTRACT_CUSTOMER);

        SQLSelectQuery studentPersonQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv").innerJoin(SQLFrom.table("eductr_student_contract_t", "sc"), "sc.contractobject_id=cv.contract_id") .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=sc.student_id")).column("pr.person_id")
                .where("cv.id=?");

        Long studentPerson = (Long) tool.getUniqueResult(translator.toSql(studentPersonQuery), contractVersionId);

        SQLSelectQuery entrantPersonQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")
                .innerJoin(SQLFrom.table("enr14_entrant_contract_t", "ec"), "ec.contractobject_id=cv.contract_id")
                .innerJoin(SQLFrom.table("enr14_requested_comp_t", "rc"), "rc.id=ec.requestedcompetition_id")
                .innerJoin(SQLFrom.table("enr14_request_t", "r"), "r.id=rc.request_id")
                .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=r.entrant_id")).column("pr.person_id")
                .where("cv.id=?");

        Long entrantPerson = (Long) tool.getUniqueResult(translator.toSql(entrantPersonQuery), contractVersionId);

        if(entrantPerson == null && studentPerson == null)
        {
            return true;
        }

        if(entrantPerson != null) {
            return Objects.equals(entrantPerson, providerPerson) || Objects.equals(entrantPerson, customerPerson);
        }

        return Objects.equals(studentPerson, providerPerson) || Objects.equals(studentPerson, customerPerson);
    }

    private boolean checkPersonSide(Long contractVersionId, DBTool tool) throws SQLException
    {
        // Проверяем по наличи юр. лица

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        SQLSelectQuery contractorQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractor_role_t", "cr")
                .innerJoin(SQLFrom.table("ctr_c_contract_role_t", "r"), "r.id=cr.role_id")
                .innerJoin(SQLFrom.table("ctr_contractor_t", "c"), "c.id=cr.contactor_id")
                .innerJoin(SQLFrom.table("juridicalcontactor_t", "jc"), "jc.id=c.contactor_id")
        )
                .where("c.owner_id=?")
                .column("jc.id");

        List<Object[]> juridicalContator = tool.executeQuery(MigrationUtils.processor(Long.class), translator.toSql(contractorQuery), contractVersionId);

        return juridicalContator.isEmpty();
    }

}
