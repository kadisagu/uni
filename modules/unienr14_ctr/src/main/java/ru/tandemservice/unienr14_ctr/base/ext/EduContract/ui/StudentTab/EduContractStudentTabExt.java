/* $Id$ */
package ru.tandemservice.unienr14_ctr.base.ext.EduContract.ui.StudentTab;

import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.config.handler.aggregate.IReadAggregateHandlerExtensionBuilder;
import org.tandemframework.caf.logic.config.handler.aggregate.ReadAggregateHandlerExtension;
import org.tandemframework.caf.logic.transformer.BaseOutputTransformer;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.StudentTab.EduContractStudentTab;
import ru.tandemservice.unieductr.base.bo.EduContract.ui.StudentTab.EduContractStudentTabUI;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.EnrEntrantContractManager;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLFunctions.countStar;

/**
 * @author Irina Ugfeld
 * @since 12.02.2016
 */
@Configuration
public class EduContractStudentTabExt extends BusinessComponentExtensionManager {

    public static final String BUTTON_NAME = "attachEnrContractsBtn";
    public static final String ADDON_NAME = "unienr14ctr"+EduContractStudentTabExtUI.class.getSimpleName();
    public static final String VIEW_PROPERTY_ATTACH_CONTRACTS_LIST = "attachContractsList";

    @Autowired
    private EduContractStudentTab _eduContractStudentTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_eduContractStudentTab.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EduContractStudentTabExtUI.class))
                .create();
    }

    @Bean
    public ButtonListExtension eduContractStudentTabBLExtension()
    {
        return buttonListExtensionBuilder(_eduContractStudentTab.eduContractStudentTabBLExtPoint())
                .addButton(submitButton(BUTTON_NAME, ADDON_NAME + ":onClickAttachEnrContract")
                .disabled("addon:"+ADDON_NAME+".notAttachEnrContractsExists")
                .permissionKey("attachEnrContract_eduCtrContractList_student").create())
                .create();
    }

    @Bean
    public ColumnListExtension contractListDSColumnsExtension() {
        return columnListExtensionBuilder(_eduContractStudentTab.contractListDSColumns())
                .addColumn(actionColumn("detachEnrContractColumn", new Icon("unlink"),ADDON_NAME+":onClickDetachContract")
                .permissionKey("detachEnrContract_eduCtrContractList_student")
                .disabled(VIEW_PROPERTY_ATTACH_CONTRACTS_LIST)
                        .alert(new FormattedMessage("ui.deleteContract.alert", EduCtrStudentContract.contractObject().number())))
                .create();
    }

    @Bean
    public ReadAggregateHandlerExtension contractListDSHandlerExt() {
        return readAggregateHandlerExtensionBuilder(_eduContractStudentTab.contractListDSHandler()).addOutputTransformer(new BaseOutputTransformer<DSInput, DSOutput>("test", getName())
        {
            @Override
            public void transformOutput(DSInput dsInput, DSOutput result, ExecutionContext context) {

                final Set<Long> attachContactsSet = Sets.newHashSet();

                BatchUtils.execute(result.getRecordIds(), DQL.MAX_VALUES_ROW_NUMBER, ids -> {

                    final DQLSelectBuilder dql = new DQLSelectBuilder()
                            .fromEntity(EduCtrStudentContract.class, "x")
                            .where(in(property("x.id"),ids))
                            .where(exists(new DQLSelectBuilder().fromEntity(EnrEntrantContract.class,"eec")
                                    .where(eq(property("eec",EnrEntrantContract.contractObject()),
                                            property("x",EduCtrStudentContract.contractObject()))).buildQuery()))
                            .joinEntity("x", DQLJoinType.inner,CtrContractVersion.class,"cv",
                                    eq(property("x",EduCtrStudentContract.contractObject()),
                                            property("cv",CtrContractVersion.contract())) )
                            .group(property("x.id"))
                            .having(le(countStar(),value(1)))
                            .column(property("x.id"));

                    attachContactsSet.addAll(DataAccessServices.dao().getList(dql));
                });


                for(DataWrapper wrapper : result.<DataWrapper>getRecordList())
               {
                   wrapper.putInMap(VIEW_PROPERTY_ATTACH_CONTRACTS_LIST, !attachContactsSet.contains(wrapper.getId()));
               }
            }
        }).create();
    }
}