package ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic;

import java.util.Date;
import java.util.List;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.CtrPrintTemplate;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * @author vdanilov
 */
public interface IEnrContractTemplateSimpleAddData {

    EnrRequestedCompetition getRequestedCompetition();
    OrgUnit getEffectiveFormativeOrgUnit();
    CtrContractType getContractType();
    CtrContractVersionCreateData getVersionCreateData();
    ContactorPerson getCustomer();
    ContactorPerson getProvider();
    EduProgramProf getEduProgram();
    Date getEnrollmentDate();
    Date getStartDate();
    Date getPriceDate();
    Date getEndDate();
    Date getRegDate();
    String getCipher();

    CtrPriceElementCost getSelectedCost();
    List<CtrPriceElementCostStage> getSelectedCostStages();

}
