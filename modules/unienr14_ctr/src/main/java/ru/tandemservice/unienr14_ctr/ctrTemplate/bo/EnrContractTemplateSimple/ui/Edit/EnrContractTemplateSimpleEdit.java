package ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.logic.EduCtrStudentContractTemplateDAO;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Configuration
public class EnrContractTemplateSimpleEdit extends BusinessComponentManager
{
    public static final String DS_COST = "costDS";
    public static final String BIND_DOC_START_DATE = "docStartDate";
    public static final String BIND_CONTRACT_VERSION = "contractVersion";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EduContractManager.instance().customerDSConfig())
            .addDataSource(selectDS(DS_COST, costDSHandler())
                .addColumn(CtrPriceElementCost.paymentGrid().title().s())
                .addColumn(CtrPriceElementCost.category().title().s())
                .addColumn(CtrPriceElementCost.costAsCurrencyString().s()))
            .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler costDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), CtrPriceElementCost.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Date docStartDate = context.get(BIND_DOC_START_DATE);
                if (docStartDate == null)
                    dql.where(nothing());

                else
                    dql.where(in(property(alias), EduCtrStudentContractTemplateDAO.createEduProgramPromisePriceElementCostBuilder(docStartDate, context.<CtrContractVersion>get(BIND_CONTRACT_VERSION)).buildQuery()));
            }
        }
            .filter(CtrPriceElementCost.paymentGrid().title())
            .filter(CtrPriceElementCost.category().title())
            .order(CtrPriceElementCost.paymentGrid().code())
            .order(CtrPriceElementCost.category().code())
            .order(CtrPriceElementCost.totalCostAsLong())
            .pageable(true);
    }
}
