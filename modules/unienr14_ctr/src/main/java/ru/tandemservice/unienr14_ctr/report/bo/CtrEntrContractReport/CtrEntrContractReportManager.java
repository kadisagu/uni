/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.logic.CtrEntrContractReportDao;
import ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.logic.ICtrEntrContractReportDao;

/**
 * @author Stanislav Shibarshin
 * @since 03.10.2016
 */
@Configuration
public class CtrEntrContractReportManager extends BusinessObjectManager
{

    public static CtrEntrContractReportManager instance()
    {
        return instance(CtrEntrContractReportManager.class);
    }

    @Bean
    public ICtrEntrContractReportDao dao()
    {
        return new CtrEntrContractReportDao();
    }

}
