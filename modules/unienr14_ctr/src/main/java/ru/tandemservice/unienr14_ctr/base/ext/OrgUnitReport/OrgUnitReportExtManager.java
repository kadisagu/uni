package ru.tandemservice.unienr14_ctr.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.ui.List.CtrEntrContractReportList;

/**
 * @author Stanislav Shibarshin
 * @since 15.09.2016
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager {

    /** Код блока отчетов на подразделении "Отчеты модуля «Договоры на обучение»" **/
    public static final String EDUCTR_ORG_UNIT_STUDENT_REPORT_BLOCK = "uniEductrOrgUnitStudentReportBlock";

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {

        final IItemListExtensionBuilder<OrgUnitReportDefinition> itemList = itemListExtension(_orgUnitReportManager.reportListExtPoint());

        return itemList
                .add("ctrEntrContractReport", new OrgUnitReportDefinition("Сводка по количеству оплаченных договоров ВО поступающих (по абитуриентам)", "ctrEntrContractReport", EDUCTR_ORG_UNIT_STUDENT_REPORT_BLOCK, CtrEntrContractReportList.class.getSimpleName(), "orgUnit_viewCtrEntrContractReport"))
                .create();
    }

}
