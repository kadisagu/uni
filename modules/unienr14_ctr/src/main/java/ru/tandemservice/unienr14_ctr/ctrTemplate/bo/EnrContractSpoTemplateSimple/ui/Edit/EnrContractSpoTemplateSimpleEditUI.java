package ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.ui.Edit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.StaticFullCheckSelectModel;
import org.tandemframework.shared.ctr.base.bo.Contactor.ui.PickForm.ContactorPickForm;
import org.tandemframework.shared.ctr.base.bo.Contactor.ui.PickForm.ContactorPickFormUI;
import org.tandemframework.shared.ctr.base.bo.Contactor.util.ContactorCategoryMeta;
import org.tandemframework.shared.ctr.base.bo.PhysicalContactor.ui.AddByNextOfKin.PhysicalContactorAddByNextOfKin;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.PhysicalContactor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractorRole;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.ContractSides;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.EduCtrStudentContractTemplateManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.logic.EduCtrStudentContractTemplateDAO;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractSpoTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.EnrContractSpoTemplateSimpleManager;

import java.util.*;

@Input({
    @Bind(key= UIPresenter.PUBLISHER_ID, binding="dataHolder.id", required=true)
})
public class EnrContractSpoTemplateSimpleEditUI extends UIPresenter
{
    public static final String REGION_CUSTOMER_PICK = "customerRegion";

    private final EntityHolder<EnrContractSpoTemplateDataSimple> dataHolder = new EntityHolder<>();
    public EntityHolder<EnrContractSpoTemplateDataSimple> getDataHolder() { return this.dataHolder; }
    public EnrContractSpoTemplateDataSimple getTemplateData() { return getDataHolder().getValue(); }

    public CtrContractVersion getVersion() { return getTemplateData().getOwner(); }
    public CtrContractObject getContract() { return getVersion().getContract(); }

    public EnrEntrant getEntrant()
    {
        return getTemplateData().getRequestedCompetition().getRequest().getEntrant();
    }

    public EnrCompetition getCompetition()
    {
        return this.getTemplateData().getRequestedCompetition().getCompetition();
    }

    public OrgUnit getEffectiveFormativeOrgUnit() {
        final EnrCompetition comp = this.getCompetition();
        return null == comp ? null : comp.getProgramSetOrgUnit().getEffectiveFormativeOrgUnit();
    }

    public String getProviderSelectorDisplayName() {
        if (null != this.getCompetition()) {
            return "Исполнитель ("+this.getEffectiveFormativeOrgUnit().getFullTitle()+")";
        }
        return "Исполнитель";
    }

    private final ISelectModel providerModel = new StaticFullCheckSelectModel() {
        @Override protected List list() {
            // конкурс должен быть выбран
            if (null == EnrContractSpoTemplateSimpleEditUI.this.getCompetition()) { return Collections.emptyList(); }

            OrgUnit effectiveFormativeOrgUnit = EnrContractSpoTemplateSimpleEditUI.this.getEffectiveFormativeOrgUnit();
            Collection<ContactorPerson> presenters = EduContractManager.instance().dao().getAcademyPresenters(effectiveFormativeOrgUnit);
            return new ArrayList<>(presenters);
        }
    };
    public ISelectModel getProviderModel() { return this.providerModel; }

    private ContactorPerson provider;
    public ContactorPerson getProvider() { return this.provider; }
    public void setProvider(final ContactorPerson provider) { this.provider = provider; }

    private ContactorPerson customer;
    public ContactorPerson getCustomer() { return this.customer; }
    public void setCustomer(final ContactorPerson customer) { this.customer = customer; }

    private boolean twoSides = true;
    private boolean threeSidesPerson = false;

    public boolean isTwoSides()
    {
        return twoSides;
    }

    public void setTwoSides(boolean twoSides)
    {
        this.twoSides = twoSides;
    }

    public boolean isThreeSidesPerson()
    {
        return threeSidesPerson;
    }

    public void setThreeSidesPerson(boolean threeSidesPerson)
    {
        this.threeSidesPerson = threeSidesPerson;
    }

    @Override

    public void onComponentActivate() {

        final Map<String, ContactorPerson> roleMap = new HashMap<>();
        for (final CtrContractVersionContractorRole rel : DataAccessServices.dao().getList(CtrContractVersionContractorRole.class, CtrContractVersionContractorRole.contactor().owner().id(), getVersion().getId(), CtrContractVersionContractorRole.title().s())) {
            roleMap.put(rel.getRole().getCode(), rel.getContactor().getContactor());
        }
        this.setProvider(roleMap.get(CtrContractRoleCodes.EDU_CONTRACT_PROVIDER));
        this.setCustomer(roleMap.get(CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER));
    }

    @Override
    public void onComponentRefresh() {
        getDataHolder().refresh(EnrContractSpoTemplateDataSimple.class);
        ContractSides sides = EduContractManager.instance().dao().getContractSides(getVersion().getKind().getCode());
        if(ContractSides.TWO_SIDES.equals(sides))
        {
            setTwoSides(true);
            setThreeSidesPerson(false);
        }
        else if(ContractSides.THREE_SIDES_PERSON.equals(sides))
        {
            setTwoSides(false);
            setThreeSidesPerson(true);
        }
        else if(ContractSides.THREE_SIDES_ORG.equals(sides))
        {
            setTwoSides(false);
            setThreeSidesPerson(false);
        }
        updateCostRequired();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrContractSpoTemplateSimpleEdit.BIND_DOC_START_DATE, getVersion().getDurationBeginDate());
        dataSource.put(EnrContractSpoTemplateSimpleEdit.BIND_CONTRACT_VERSION, getVersion());
        if(EduContractManager.DS_CUSTOMER.equals(dataSource.getName()))
        {
            dataSource.put(EduContractManager.BIND_PHYSICAL_CONTRACTORS, isTwoSides() || isThreeSidesPerson());
        }
    }

    @Override
    public void onComponentBindReturnParameters(final String childRegionName, final Map<String, Object> returnedData) {
        if (REGION_CUSTOMER_PICK.equals(childRegionName)) {
            final Object id = returnedData.get(ContactorPickFormUI.CONTACTOR_ID);
            if (id instanceof Long) {
                final ContactorPerson contactor = DataAccessServices.dao().get(ContactorPerson.class, (Long) id);
                if (null != contactor) { this.setCustomer(contactor); }
            }
        }
    }

    public void onClickAddCustomer() {
        DataWrapper contactorType = ContactorCategoryMeta.getDataRecordMap().get(isThreeSidesPerson() ? PhysicalContactor.class.getName() : JuridicalContactor.class.getName());

        this.getActivationBuilder().asRegion(ContactorPickForm.class, REGION_CUSTOMER_PICK)
                .parameter(ContactorPickFormUI.PRE_SELECTED_CONTRACT_TYPE, contactorType)
                .activate();
    }

    public void onClickAddCustomerNextOfKin() {
        this.getActivationBuilder().asRegion(PhysicalContactorAddByNextOfKin.class, REGION_CUSTOMER_PICK).parameter(PhysicalContactorAddByNextOfKin.PERSON_ID, getEntrant().getPerson().getId()).activate();
    }

    public boolean isCustomerPickerActive() {
        return (null != this.getSupport().getChildUI(REGION_CUSTOMER_PICK));
    }

    public boolean isApplyDisabled() {
        return this.isCustomerPickerActive();
    }

    public void onClickApply()
    {
        if(isApplyDisabled()) return;

        if(getTemplateData().getOwner().getDurationEndDate() != null && getTemplateData().getOwner().getDurationEndDate().before(getTemplateData().getOwner().getDurationBeginDate()))
            _uiSupport.error("Дата начала действия договора должна быть меньше даты окончания.", "eductr_date_be", "eductr_date_en");
        if(getUserContext().getErrorCollector().hasErrors())
            return;
        EduCtrStudentContractTemplateManager.instance().dao().doUpdateContractVersionContactor(getVersion(), getProvider(), getCustomer());
        EnrContractSpoTemplateSimpleManager.instance().dao().doSaveTemplate(getTemplateData());
        deactivate();
    }

    public void onClickClose()
    {
        deactivate();
    }

    public void onChangeDocStartDate()
    {
        updateCostRequired();
    }

    private void updateCostRequired()
    {
        if (getVersion().getDurationBeginDate() == null)
            return;

        _costRequired = IUniBaseDao.instance.get().getCount(EduCtrStudentContractTemplateDAO.createEduProgramPromisePriceElementCostBuilder(getVersion().getDurationBeginDate(), getVersion())) > 0;
    }

    private boolean _costRequired;
    public boolean isCostRequired() { return _costRequired; }
}
