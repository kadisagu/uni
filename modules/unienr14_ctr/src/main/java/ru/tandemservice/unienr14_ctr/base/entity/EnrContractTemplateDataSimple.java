package ru.tandemservice.unienr14_ctr.base.entity;

import ru.tandemservice.unienr14_ctr.base.entity.gen.EnrContractTemplateDataSimpleGen;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.EnrContractTemplateSimpleManager;

/**
 * Данные продуктового шаблона договора на обучение абитуриента
 *
 * Данные продуктового шаблона для создания договора на обучение абитуриента.
 */
public class EnrContractTemplateDataSimple extends EnrContractTemplateDataSimpleGen
{
    @Override
    public EnrContractTemplateSimpleManager getManager() {
        return EnrContractTemplateSimpleManager.instance();
    }
}