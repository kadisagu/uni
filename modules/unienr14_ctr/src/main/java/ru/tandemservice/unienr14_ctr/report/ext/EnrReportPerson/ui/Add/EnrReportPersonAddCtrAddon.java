/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.ext.EnrReportPerson.ui.Add;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;
import ru.tandemservice.unienr14_ctr.report.ext.EnrReportPerson.ui.Add.block.entrantData.EnrCtrDataParam;
import ru.tandemservice.unienr14_ctr.report.ext.EnrReportPerson.ui.Add.block.entrantData.EnrCtrPrintBlock;

/**
 * @author Nikolay Fedorovskih
 * @since 06.07.2015
 */
public class EnrReportPersonAddCtrAddon extends UIAddon implements IReportDQLModifierOwner
{
    private IReportDQLModifier _enrCtrData = new EnrCtrDataParam();
    private IReportPrintBlock _enrCtrPrintBlock = new EnrCtrPrintBlock();

    public EnrReportPersonAddCtrAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private boolean isEntrantScheet()
    {
        EnrReportPersonAddUI presenter = getPresenter();
        return presenter.isEntrantScheet();
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {
        if (isEntrantScheet()) {
            _enrCtrData.validateReportParams(errorCollector);
        }
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (isEntrantScheet()) {
            _enrCtrData.modify(dql, printInfo);

            _enrCtrPrintBlock.modify(dql, printInfo);
        }
    }

    public IReportDQLModifier getEnrCtrData()
    {
        return _enrCtrData;
    }

    public IReportPrintBlock getEnrCtrPrintBlock()
    {
        return _enrCtrPrintBlock;
    }
}