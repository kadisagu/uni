package ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateDAO;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractSpoTemplateDataSimple;

/**
 * @author vdanilov
 */
public interface IEnrContractSpoTemplateSimpleDao extends ICtrContractTemplateDAO, INeedPersistenceSupport {

    /**
     * @param ui данные формы содздания
     * @return templateData
     */
    EnrContractSpoTemplateDataSimple doCreateVersion(IEnrContractSpoTemplateSimpleAddData ui);

    /**
     * @param templateData
     */
    void doSaveTemplate(EnrContractSpoTemplateDataSimple templateData);

}
