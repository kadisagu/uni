package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.tool.IdGen;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.entity.EntityCodes;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrPromiceResultResolver;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentResult;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Stanislav Shibarshin
 * @since 11.01.2017
 */
public class EntrantContractPaymentPromiceResultPair extends CommonDAO implements ICtrPromiceResultResolver
{

    public static final String NAME = EntrantContractPaymentPromiceResultPair.class.getSimpleName();

    private static final EntityCodes<CtrPaymentPromice> PROMICE_CODES = new EntityCodes<>(CtrPaymentPromice.class);
    private static final EntityCodes<CtrPaymentResult> RESULT_CODES = new EntityCodes<>(CtrPaymentResult.class);

    @Override public String getName() { return EntrantContractPaymentPromiceResultPair.NAME; }
    @Override public int getAverageKeyNumberPerContract() { return 16; }

    protected String getKey(final Long src, final Long dst, final String currency) {
        return new StringBuilder("payment_enr")
        .append("\n").append(src)
        .append("\n").append(dst)
        .append("\n").append(currency)
        .toString();
    }

    public static String key(final Long src, final Long dst, final String currency)
    {
        return new StringBuilder("payment_enr")
                .append("\n").append(src)
                .append("\n").append(dst)
                .append("\n").append(currency)
                .toString();
    }

    /**
     * @param key ключ группы обязательств
     * @param dst направление
     * @param currency валюта
     * @return true, если это группа обязательств по оплате, направленная в сторону dst в указанной валюте, иначе false
     */
    public static boolean payDirectedTo(final String key, final Long dst, final String currency)
    {
        String[] split = key.split("\n");

        boolean isPayment = split[0].equals("payment_enr");
        boolean isDst = split[2].equals(dst.toString());
        boolean isCurrency = split[3].equals(currency);

        return isPayment && isDst && isCurrency;
    }

    protected void putAll(final Map<Long, ICompareWrapper> result, final boolean promice, final DQLSelectBuilder dql) {
        for (final Object[] row: CommonDAO.scrollRows(dql.createStatement(this.getSession())))
        {
            final Long id = (Long)row[0];
            final long timestamp = ((Date)row[1]).getTime();
            final long cost = (Long) row[2];
            final String key = this.getKey((Long)row[3], (Long)row[4], (String)row[5]);

            final ICompareWrapper wrapper = new ICompareWrapper() {
                @Override public Long getId() { return id; }
                @Override public boolean isPromice() { return promice; }
                @Override public long getTimestamp() { return timestamp; }
                @Override public String getKey() { return key; }
                @Override public long getAmount() { return cost; }
                @Override public String toString() {
                    return this.getKey()+"("+this.isPromice()+")"+"@"+this.getTimestamp()+":"+this.getAmount();
                }
            };

            if (null != result.put(id, wrapper)) {
                throw new IllegalStateException();
            }
        }
    }


    @SuppressWarnings("unchecked")
    @Override public Map<Long, ICompareWrapper> getPromiceKeys(Collection<Long> promiceIds) {
        final Collection<Short> codes = EntrantContractPaymentPromiceResultPair.PROMICE_CODES.get();
        promiceIds = CollectionUtils.select(promiceIds, promiceId -> (null != promiceId) && codes.contains(IdGen.getCode((Long)promiceId)));

        final Map<Long, ICompareWrapper> result = new HashMap<>(promiceIds.size());
        BatchUtils.execute(promiceIds, DQL.MAX_VALUES_ROW_NUMBER, promiceIdsPart -> EntrantContractPaymentPromiceResultPair.this.putAll(
                result,
                true,
                new DQLSelectBuilder()
                        .fromEntity(CtrPaymentPromice.class, "p").where(in(property("p.id"), promiceIdsPart))
                        .joinPath(DQLJoinType.inner, CtrPaymentPromice.src().fromAlias("p"), "src")
                        .joinPath(DQLJoinType.inner, CtrPaymentPromice.dst().fromAlias("p"), "dst")
                        .joinPath(DQLJoinType.inner, CtrContractVersionContractor.contactor().fromAlias("src"), "srcctr")
                        .joinPath(DQLJoinType.inner, CtrContractVersionContractor.contactor().fromAlias("dst"), "dstctr")
                        .joinPath(DQLJoinType.inner, CtrContractVersionContractor.owner().fromAlias("src"), "owner")
                        .joinPath(DQLJoinType.inner, CtrContractVersion.contract().fromAlias("owner"), "contract")
                        .joinEntity("contract", DQLJoinType.inner, EnrEntrantContract.class, "sc", eq(property("sc", EnrEntrantContract.contractObject()), property("contract")))
                        .joinPath(DQLJoinType.inner, EnrEntrantContract.requestedCompetition().fromAlias("sc"), "req_cmp")
                        .joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().fromAlias("req_cmp"), "req")
                        .joinEntity("sc", DQLJoinType.inner, EnrEntrant.class, "st", eq(property("st"), property("req", EnrEntrantRequest.entrant())))
                        .joinEntity("srcctr", DQLJoinType.left, EmployeePostContactor.class, "ecsrc", eq(property("ecsrc", EmployeePostContactor.id()), property("srcctr", ContactorPerson.id())))
                        .joinEntity("p", DQLJoinType.inner, TopOrgUnit.class, "topou", eq(value(true), value(true)))
                        .column(property(CtrPaymentPromice.id().fromAlias("p")))
                        .column(property(CtrPaymentPromice.deadlineDate().fromAlias("p")))
                        .column(caseExpr(isNotNull(property("ecsrc")), minus(value(0L), property(CtrPaymentPromice.costAsLong().fromAlias("p"))), property(CtrPaymentPromice.costAsLong().fromAlias("p"))))
                        .column(property("st.id"))
                        .column(property("topou.id"))
                        .joinPath(DQLJoinType.left, CtrPaymentPromice.currency().fromAlias("p"), "p_currency")
                        .column(property(Currency.code().fromAlias("p_currency")))
        ));
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override public Map<Long, ICompareWrapper> getResultKeys(Collection<Long> resultIds) {
        final Collection<Short> codes = EntrantContractPaymentPromiceResultPair.RESULT_CODES.get();
        resultIds = CollectionUtils.select(resultIds, resultId -> (null != resultId) && codes.contains(IdGen.getCode((Long)resultId)));

        final Map<Long, ICompareWrapper> result = new HashMap<>(resultIds.size());
        BatchUtils.execute(resultIds, DQL.MAX_VALUES_ROW_NUMBER, resultIds1 -> EntrantContractPaymentPromiceResultPair.this.putAll(
                result,
                false,
                new DQLSelectBuilder()
                .fromEntity(CtrPaymentResult.class, "r").where(in(property("r.id"), resultIds1))
                        .joinPath(DQLJoinType.inner, CtrPaymentResult.src().fromAlias("r"), "src")
                        .joinPath(DQLJoinType.inner, CtrPaymentResult.dst().fromAlias("r"), "dst")
                        .joinPath(DQLJoinType.inner, CtrPaymentResult.contract().fromAlias("r"), "contract")
                        .joinEntity("contract", DQLJoinType.inner, EnrEntrantContract.class, "sc", eq(property("sc", EnrEntrantContract.contractObject()), property("contract")))
                        .joinPath(DQLJoinType.inner, EnrEntrantContract.requestedCompetition().fromAlias("sc"), "req_cmp")
                        .joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().fromAlias("req_cmp"), "req")
                        .joinEntity("sc", DQLJoinType.inner, EnrEntrant.class, "st", eq(property("st"), property("req", EnrEntrantRequest.entrant())))
                        .joinEntity("src", DQLJoinType.left, EmployeePostContactor.class, "ecsrc", eq(property("ecsrc", EmployeePostContactor.id()), property("src", ContactorPerson.id())))
                        .joinEntity("r", DQLJoinType.inner, TopOrgUnit.class, "topou", eq(value(true), value(true)))
                .column(property(CtrPaymentResult.id().fromAlias("r")))
                .column(property(CtrPaymentResult.timestamp().fromAlias("r")))
                .column(caseExpr(isNotNull(property("ecsrc")), minus(value(0L), property(CtrPaymentResult.costAsLong().fromAlias("r"))), property(CtrPaymentResult.costAsLong().fromAlias("r"))))
                        .column(property("st.id"))
                        .column(property("topou.id"))
                .joinPath(DQLJoinType.left, CtrPaymentResult.currency().fromAlias("r"), "r_currency")
                .column(property(Currency.code().fromAlias("r_currency")))
        ));
        return result;
    }


}
