/* $Id$ */
package ru.tandemservice.unienr14_ctr.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.ui.List.CtrEntrContractReportList;

/**
 * @author Stanislav Shibarshin
 * @since 01.09.2016
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .add("ctrEntrContractReport", new GlobalReportDefinition("eduCtrContract", "ctrEntrContractReport", CtrEntrContractReportList.class.getSimpleName()))
                .create();
    }
}