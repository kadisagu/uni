package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.daemon;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentResult;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EnrEntractContractDaemonBean extends SharedBaseDao implements IEnrEntractContractDaemonBean {

    public static final SyncDaemon DAEMON = new SyncDaemon(EnrEntractContractDaemonBean.class.getName()) {
        @Override protected void initDaemon() {
            registerWakeUpListener4Class(CtrPaymentResult.class, CtrPaymentResult.P_COST_AS_LONG);
        }
        @Override protected void main() {
            IEnrEntractContractDaemonBean.instance.get().doUpdatePaymentResultExists();
        }
    };

    @Override
    public void doUpdatePaymentResultExists() {
        Debug.begin("EnrEntractContractDaemonBean.doUpdatePaymentResultExists");
        try {
            Debug.begin("set");
            try {
                this.executeAndClear(
                    new DQLUpdateBuilder(EnrEntrantContract.class)
                    .set(EnrEntrantContract.P_PAYMENT_DATE, valueDate(new Date()))
                    .set(EnrEntrantContract.P_PAYMENT_RESULT_EXISTS, value(Boolean.TRUE))
                    .where(or(
                        DQLExpressions.isNull(property(EnrEntrantContract.P_PAYMENT_DATE)),
                        eq(property(EnrEntrantContract.P_PAYMENT_RESULT_EXISTS), value(Boolean.FALSE))
                    ))
                    .where(gt(minus(
                            new DQLSelectBuilder()
                                    .fromEntity(CtrPaymentResult.class, "r")
                                    .joinEntity("r", DQLJoinType.inner, EmployeePostContactor.class, "epc", eq(property("epc", EmployeePostContactor.id()), property("r", CtrPaymentResult.dst().id())))
                                    .where(notExists(new DQLSelectBuilder().fromEntity(EmployeePostContactor.class,  "srcemp").where(eq(property("r", CtrPaymentResult.src().id()), property("srcemp", EmployeePostContactor.id()))).buildQuery()))
                                    .column(new DQLCaseExpressionBuilder().when(isNotNull(DQLFunctions.sum(property("r", CtrPaymentResult.costAsLong()))), DQLFunctions.sum(property("r", CtrPaymentResult.costAsLong()))).otherwise(value(0)).build())
                                    .where(eq(property(CtrPaymentResult.contract().fromAlias("r")), property(EnrEntrantContract.contractObject())))
                                    .buildQuery(),
                            new DQLSelectBuilder()
                                    .fromEntity(CtrPaymentResult.class, "rd")
                                    .joinEntity("rd", DQLJoinType.inner, EmployeePostContactor.class, "epcd", eq(property("epcd", EmployeePostContactor.id()), property("rd", CtrPaymentResult.src().id())))
                                    .where(notExists(new DQLSelectBuilder().fromEntity(EmployeePostContactor.class, "dstemp").where(eq(property("rd", CtrPaymentResult.dst().id()), property("dstemp", EmployeePostContactor.id()))).buildQuery()))
                                    .column(new DQLCaseExpressionBuilder().when(isNotNull(DQLFunctions.sum(property("rd", CtrPaymentResult.costAsLong()))), DQLFunctions.sum(property("rd", CtrPaymentResult.costAsLong()))).otherwise(value(0)).build())
                                    .where(eq(property(CtrPaymentResult.contract().fromAlias("rd")), property(EnrEntrantContract.contractObject())))
                                    .buildQuery()), value(0)))
                );
            } finally {
                Debug.end();
            }

            Debug.begin("clear");
            try {
                this.executeAndClear(
                    new DQLUpdateBuilder(EnrEntrantContract.class)
                    .set(EnrEntrantContract.P_PAYMENT_DATE, DQLExpressions.nul(PropertyType.DATE))
                    .set(EnrEntrantContract.P_PAYMENT_RESULT_EXISTS, value(Boolean.FALSE))
                    .where(eq(property(EnrEntrantContract.P_PAYMENT_RESULT_EXISTS), value(Boolean.TRUE)))
                            .where(le(minus(
                                    new DQLSelectBuilder()
                                            .fromEntity(CtrPaymentResult.class, "r")
                                            .joinEntity("r", DQLJoinType.inner, EmployeePostContactor.class, "epc", eq(property("epc", EmployeePostContactor.id()), property("r", CtrPaymentResult.dst().id())))
                                            .column(new DQLCaseExpressionBuilder().when(isNotNull(DQLFunctions.sum(property("r", CtrPaymentResult.costAsLong()))), DQLFunctions.sum(property("r", CtrPaymentResult.costAsLong()))).otherwise(value(0)).build())
                                            .where(eq(property(CtrPaymentResult.contract().fromAlias("r")), property(EnrEntrantContract.contractObject())))
                                            .buildQuery(),
                                    new DQLSelectBuilder()
                                            .fromEntity(CtrPaymentResult.class, "rd")
                                            .joinEntity("rd", DQLJoinType.inner, EmployeePostContactor.class, "epcd", eq(property("epcd", EmployeePostContactor.id()), property("rd", CtrPaymentResult.src().id())))
                                            .column(new DQLCaseExpressionBuilder().when(isNotNull(DQLFunctions.sum(property("rd", CtrPaymentResult.costAsLong()))), DQLFunctions.sum(property("rd", CtrPaymentResult.costAsLong()))).otherwise(value(0)).build())
                                            .where(eq(property(CtrPaymentResult.contract().fromAlias("rd")), property(EnrEntrantContract.contractObject())))
                                            .buildQuery()), value(0)))
                );
            } finally {
                Debug.end();
            }
        } finally {
            Debug.end();
        }
    }
}
