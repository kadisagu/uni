package ru.tandemservice.unienr14_ctr.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Конфигурация для скриптовой печати модуля «Договоры абитуриентов»"
 * Имя сущности : ctrScriptItem
 * Файл data.xml : unienr.print.catalog.data.xml
 */
public interface CtrScriptItemCodes
{
    /** Константа кода (code) элемента : Отчет «Сводка о ходе приема, средних баллах и договорах на обучение» (title) */
    String REPORT_ENTRANT_PLAN_POINTS = "report.entrantsPlanPoints";
    /** Константа кода (code) элемента : Отчет «Экспорт данных о договорах зачисленных абитуриентов» (title) */
    String REPORT_ENROLLED_ENTRANTS_CONTRACT_DATA_EXPORT = "report.enrolledEntrantsContractDataExport";
    /** Константа кода (code) элемента : Сводка по количеству оплаченных договоров ВО поступающих (по абитуриентам) (title) */
    String REPORT_ENTRANT_CONTRACT = "report.entrContractReport2";

    Set<String> CODES = ImmutableSet.of(REPORT_ENTRANT_PLAN_POINTS, REPORT_ENROLLED_ENTRANTS_CONTRACT_DATA_EXPORT, REPORT_ENTRANT_CONTRACT);
}
