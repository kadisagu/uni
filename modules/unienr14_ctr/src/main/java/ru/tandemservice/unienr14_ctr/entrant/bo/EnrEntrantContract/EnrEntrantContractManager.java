package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic.EnrEntrantContractDao;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic.IEnrEntrantContractDao;

/**
 * @author vdanilov
 */
@Configuration
public class EnrEntrantContractManager extends BusinessObjectManager {

    public static EnrEntrantContractManager instance() {
        return instance(EnrEntrantContractManager.class);
    }

    @Bean
    public IEnrEntrantContractDao dao() { return new EnrEntrantContractDao(); }


}
