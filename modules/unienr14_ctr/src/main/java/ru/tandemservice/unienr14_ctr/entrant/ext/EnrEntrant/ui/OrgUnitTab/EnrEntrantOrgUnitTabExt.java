/* $Id$ */
package ru.tandemservice.unienr14_ctr.entrant.ext.EnrEntrant.ui.OrgUnitTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.OrgUnitTab.EnrEntrantOrgUnitTab;
import ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.ui.OrgUnitList.EnrEntrantContractOrgUnitList;

/**
 * @author nvankov
 * @since 9/12/14
 */
@Configuration
public class EnrEntrantOrgUnitTabExt extends BusinessComponentExtensionManager
{
    public static final String ENR_CONTRACT_TAB = "enr14ContractTab";

    @Autowired
    private EnrEntrantOrgUnitTab _enrEntrantOrgUnitTab;

    @Bean
    public TabPanelExtension enr14TabPaneExtension()
    {
        return tabPanelExtensionBuilder(_enrEntrantOrgUnitTab.enr14TabPanelExtPoint())
                .addTab(componentTab(ENR_CONTRACT_TAB, EnrEntrantContractOrgUnitList.class).permissionKey("ui:secModel.orgUnit_viewEnr14ContractTab").securedObject("ui:orgUnit"))
                .create();
    }
}
