package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author vdanilov
 */
public interface IEnrEntrantContractDao extends INeedPersistenceSupport {

    /**
     * Изменяет значение enrEntrantContract.paymentDate
     * @param entrantContractId enrEntrantContract.id
     * @param clean занулять дату или нет
     */
    void doSetPaymentDate(Long entrantContractId, boolean clean);

    /** Прикрепляет к студентам договоры с абитуриентами, указанной ПК, которые еще не прикреплены.
     * @param enrollmentCampaign приемная кампания
     **/
    int doAttachEduContractsToStudents(EnrEnrollmentCampaign enrollmentCampaign);

    int doAttachEduContracts(Student student);
    int doAttachEduContracts(EnrEntrant entrant);

    void doAttachEnrEntrantContractToStudent(Long contractObjectId);
    void doDetachEnrEntrantContractToStudent(Long contractObjectId);

    boolean getExistsNotAttachContracts(Student student);
    boolean getExistsNotAttachContracts(EnrEntrant entrant);
}
