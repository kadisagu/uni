/* $Id:$ */
package ru.tandemservice.unienr14_ctr.report.bo.CtrReport.ui.EnrolledEntrantsContractDataExportAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportDateSelector;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.report.entity.EnrReportEntrantDataExport;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.catalog.entity.CtrScriptItem;
import ru.tandemservice.unienr14_ctr.catalog.entity.codes.CtrScriptItemCodes;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 15.08.2014
 */
public class CtrReportEnrolledEntrantsContractDataExportAddUI extends UIPresenter {

    EnrReportDateSelector dateSelector = new EnrReportDateSelector();
    private EnrEnrollmentCampaign enrollmentCampaign;

    private boolean selectDateFrom = false;
    private boolean selectDateTo = false;

    @Override
    public void onComponentRefresh() {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        dateSelector.setDateFrom(null);
        dateSelector.setDateTo(null);
    }

    public void onClickApply() {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        IUniBaseDao.instance.get().doInTransaction(session -> {
            EnrReportEntrantDataExport report = new EnrReportEntrantDataExport();

            report.setFormingDate(new Date());

            report.setEnrollmentCampaign(getEnrollmentCampaign());
            report.setDateFrom(getDateSelector().getDateFrom());
            report.setDateTo(getDateSelector().getDateTo());

            EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                    .notArchiveOnly()
                    .filter(getEnrollmentCampaign());


            requestedCompDQL
                    .joinEntity("reqComp", DQLJoinType.inner, EnrEnrollmentExtract.class, "extract", eq(property(EnrEnrollmentExtract.entity().fromAlias("extract")), property("reqComp")));

            if (selectDateFrom)
                requestedCompDQL.where(ge(property(EnrEnrollmentExtract.paragraph().order().actionDate().fromAlias("extract")), valueDate(getDateSelector().getDateFrom())));
            if (selectDateTo)
                requestedCompDQL.where(le(property(EnrEnrollmentExtract.paragraph().order().actionDate().fromAlias("extract")), valueDate(getDateSelector().getDateTo())));
            requestedCompDQL.where(eq(property("reqComp", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.ENROLLED)));
            requestedCompDQL.where(eq(property("reqComp", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)));
            requestedCompDQL.where(eq(property("extract", EnrEnrollmentExtract.cancelled()), value(false)));

            requestedCompDQL.order(property(EnrEntrant.person().identityCard().fullFio().fromAlias(requestedCompDQL.entrant())));

            requestedCompDQL.column(property(EnrEnrollmentExtract.id().fromAlias("extract")));
            List<Long> extracts = requestedCompDQL.createStatement(session).<Long>list();


            requestedCompDQL.resetColumns()
                    .joinEntity("reqComp", DQLJoinType.left, EnrEntrantContract.class, "contract", eq(property(EnrEntrantContract.requestedCompetition().fromAlias("contract")), property("reqComp")))
                    .column(property(EnrEntrantContract.id().fromAlias("contract")));

            List<Long> contracts = requestedCompDQL.createStatement(session).<Long>list();

            requestedCompDQL.joinPath(DQLJoinType.left, EnrEntrantContract.contractObject().fromAlias("contract"), "ctrObject");


            requestedCompDQL.resetColumns()
                    .joinEntity("ctrObject", DQLJoinType.left, CtrContractVersion.class, "version", eq(property(CtrContractVersion.contract().fromAlias("version")), property("ctrObject")))
                    .column(property(CtrContractVersion.id().fromAlias("version")));

            List<Long> versions = requestedCompDQL.createStatement(session).<Long>list();


            requestedCompDQL.resetColumns()
                    .joinEntity("version", DQLJoinType.left, CtrContractVersionContractor.class, "contractor", eq(property(CtrContractVersionContractor.owner().fromAlias("contractor")), property("version")))
                    .column(property(CtrContractVersionContractor.id().fromAlias("contractor")));

            List<Long> contractors = requestedCompDQL.createStatement(session).<Long>list();

            requestedCompDQL.resetColumns()
                    .joinEntity("contractor", DQLJoinType.left, CtrPaymentPromice.class, "payprom", eq(property(CtrPaymentPromice.dst().fromAlias("payprom")), property("contractor")))
                    .column(property(CtrPaymentPromice.id().fromAlias("payprom")));

            List<Long> paymentPromises = requestedCompDQL.createStatement(session).<Long>list();

            requestedCompDQL.resetColumns()
                    .joinEntity("contractor", DQLJoinType.left, EduCtrEducationPromise.class, "eduprom", eq(property(EduCtrEducationPromise.dst().fromAlias("eduprom")), property("contractor")))
                    .column(property(EduCtrEducationPromise.id().fromAlias("eduprom")));

            List<Long> educationPromises = requestedCompDQL.createStatement(session).<Long>list();

            Map<String, Object> scriptResult = CommonManager.instance().scriptDao()
                    .getScriptResult(DataAccessServices.dao().getByCode(CtrScriptItem.class, CtrScriptItemCodes.REPORT_ENROLLED_ENTRANTS_CONTRACT_DATA_EXPORT),
                                     "session", session,
                                     "template", new byte[]{}, // Шаблон не используем, делаем заглушку
                                     "extractIds", extracts,
                                     "contractIds", contracts,
                                     "versionIds", versions,
                                     "contractorIds", contractors,
                                     "payPromiseIds", paymentPromises,
                                     "eduPromiseIds", educationPromises
                    );

            byte[] content = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
            String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

            if (null == content) {
                throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");
            }
            if (null == filename || !filename.contains(".")) {
                throw new ApplicationException("Скрипт печати вернул некорректное имя файла. Обратитесь к администратору системы.");
            }
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(filename).document(content), false);

            return 0L;
        });

        deactivate();
    }


    public EnrReportDateSelector getDateSelector() {
        return dateSelector;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }

    public boolean isSelectDateFrom() {
        return selectDateFrom;
    }

    public void setSelectDateFrom(boolean selectDateFrom) {
        this.selectDateFrom = selectDateFrom;
    }

    public boolean isSelectDateTo() {
        return selectDateTo;
    }

    public void setSelectDateTo(boolean selectDateTo) {
        this.selectDateTo = selectDateTo;
    }
}