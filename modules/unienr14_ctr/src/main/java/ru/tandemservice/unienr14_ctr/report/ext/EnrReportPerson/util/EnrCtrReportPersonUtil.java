/* $Id:$ */
package ru.tandemservice.unienr14_ctr.report.ext.EnrReportPerson.util;

import com.beust.jcommander.internal.Lists;
import com.google.common.collect.Sets;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.report.ext.EnrReportPerson.ui.Add.EnrReportPersonAddExt;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 26.07.2016
 */
public class EnrCtrReportPersonUtil
{
    public static void prefetchHasContractsData(IReportPrintInfo printInfo, List<Object[]> rows, int directionIndex)
    {
        if (printInfo.getSharedObject(EnrReportPersonAddExt.PREFETCH_HAS_CONTRACTS_MAP) != null) return;

        final List<Long> reqCompIds = getEntityIds(rows, directionIndex, 3, EnrRequestedCompetition.class);

        final Set<Long> hasContractsSet = Sets.newHashSet();

        BatchUtils.execute(reqCompIds, DQL.MAX_VALUES_ROW_NUMBER, elements ->
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEntrantContract.class, "c")
                    .column(property("c", EnrEntrantContract.requestedCompetition().id()))
                    .where(in(property("c", EnrEntrantContract.requestedCompetition().id()), elements));

            hasContractsSet.addAll(IUniBaseDao.instance.get().getList(builder));
        });

        printInfo.putSharedObject(EnrReportPersonAddExt.PREFETCH_HAS_CONTRACTS_MAP, hasContractsSet);
    }

    public static boolean isHasContract(IReportPrintInfo printInfo, EnrRequestedCompetition competition)
    {
        if (competition == null)
            return false;

        Set<Long> set = printInfo.getSharedObject(EnrReportPersonAddExt.PREFETCH_HAS_CONTRACTS_MAP);
        return set.contains(competition.getId());
    }

    public static void prefetchHasContractPaymentData(IReportPrintInfo printInfo, List<Object[]> rows, int directionIndex)
    {
        if (printInfo.getSharedObject(EnrReportPersonAddExt.PREFETCH_HAS_CONTRACTS_PAYMENT_MAP) != null) return;

        final List<Long> reqCompIds = getEntityIds(rows, directionIndex, 3, EnrRequestedCompetition.class);

        final Set<Long> hasContractsPaymentSet = Sets.newHashSet();

        BatchUtils.execute(reqCompIds, DQL.MAX_VALUES_ROW_NUMBER, elements ->
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEntrantContract.class, "c")
                    .column(property("c", EnrEntrantContract.requestedCompetition().id()))
                    .where(in(property("c", EnrEntrantContract.requestedCompetition().id()), elements))
                    .where(eq(property("c", EnrEntrantContract.paymentExists()), value(true)));

            hasContractsPaymentSet.addAll(IUniBaseDao.instance.get().getList(builder));
        });

        printInfo.putSharedObject(EnrReportPersonAddExt.PREFETCH_HAS_CONTRACTS_PAYMENT_MAP, hasContractsPaymentSet);
    }

    public static boolean isHasContractPayment(IReportPrintInfo printInfo, EnrRequestedCompetition competition)
    {
        if (competition == null)
            return false;

        Set<Long> set = printInfo.getSharedObject(EnrReportPersonAddExt.PREFETCH_HAS_CONTRACTS_PAYMENT_MAP);
        return set.contains(competition.getId());
    }


    private static List<Long> getEntityIds(List<Object[]> rows, int entityListIndex, int maplevel, Class<?> check) {
        List<Long> ids = new ArrayList<>();
        for (Object[] row : rows) {
            _fillIdsFromMap(ids, row[entityListIndex], maplevel, check);
        }
        return ids;
    }

    private static void _fillIdsFromMap(Collection<Long> ids, Object o, int levels, Class<?> check)
    {
        // пустые значения пропускаем
        if (null == o) { return; }

        // если мы должны взять значение (ключ)
        if (0 == levels) {

            // предварительная обработка для Map.Entry
            if (o instanceof Map.Entry) {
                o = ((Map.Entry)o).getKey(); // значение берется из ключа
                if (null == o) { return; } // что, впринципе, бред... но чего только не случается
            }

            if (null != check) {
                if (!check.isInstance(o)) {
                    throw new ClassCastException(String.valueOf(ClassUtils.getUserClass(o)) + " is not instance of " + check);
                }
            }

            // обработка ключей
            if (o instanceof Long) { ids.add((Long)o); }
            else if (o instanceof IEntity) { ids.add(((IEntity) o).getId()); }
            else { throw new IllegalStateException(String.valueOf(ClassUtils.getUserClass(o))); }

            // значение получили - сохранили его в ids, модем выходить
            return;
        }

        // в противном случае - мы долдны получить список для дальнейшей обработки

        // предварительная обработка для Map.Entry
        if (o instanceof Map.Entry) {
            o = ((Map.Entry)o).getValue(); // коллекция берется из значения
            if (null == o) { return; } // что, впринципе, бред... но чего только не случается
        }

        // обработка значений
        if (o instanceof Collection) {
            for (Object item : (Collection)o) {
                if (null == item) { continue; }
                _fillIdsFromMap(ids, item, levels-1, check);
            }
        } else if (o instanceof Map) {
            for (Object entry : ((Map)o).entrySet()) {
                if (null == entry) { continue; }
                _fillIdsFromMap(ids, entry, levels-1, check);
            }
        } else {
            throw new IllegalStateException(String.valueOf(ClassUtils.getUserClass(o)));
        }
    }

}
