package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.daemon;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.DoNotUseMe;

/**
 * @author vdanilov
 */
public interface IEnrEntractContractDaemonBean extends INeedPersistenceSupport {

    public static final SingleValueCache<IEnrEntractContractDaemonBean> instance = new SpringBeanCache<IEnrEntractContractDaemonBean>(IEnrEntractContractDaemonBean.class.getName());

    /**
     * Обновляет признаки enrEntrantContract.paymentResultExists и enrEntrantContract.paymentExists
     */
    @DoNotUseMe(comment="внутренний механизм демона")
    void doUpdatePaymentResultExists();

}
