package ru.tandemservice.unienr14_ctr.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unienr14_ctr_2x10x8_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность ctrEntrContractReport

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enr14_ctr_entr_contr_rep",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_ctrentrcontractreport"), 
				new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false), 
				new DBColumn("requesttype_p", DBType.createVarchar(255)), 
				new DBColumn("compensationtype_p", DBType.createVarchar(255)), 
				new DBColumn("programform_p", DBType.createVarchar(255)), 
				new DBColumn("competitiontype_p", DBType.createVarchar(255)), 
				new DBColumn("enrorgunit_p", DBType.createVarchar(255)), 
				new DBColumn("formativeorgunit_p", DBType.createVarchar(255)), 
				new DBColumn("programsubject_p", DBType.createVarchar(255)), 
				new DBColumn("eduprogram_p", DBType.createVarchar(255)), 
				new DBColumn("programset_p", DBType.createVarchar(255)), 
				new DBColumn("orgunit_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("ctrEntrContractReport");

		}


    }
}