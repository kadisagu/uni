package ru.tandemservice.unienr14_ctr.request.ext.EnrRequestedCompetition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;

import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.EnrRequestedCompetitionManager;
import ru.tandemservice.unienr14.request.bo.EnrRequestedCompetition.logic.IPropertyUpdateRule;
import ru.tandemservice.unienr14_ctr.request.ext.EnrRequestedCompetition.logic.EnrEntrantContractExistsRule;

/**
 * @author vdanilov
 */
@Configuration
public class EnrRequestedCompetitionExtManager extends BusinessObjectExtensionManager {

    @Autowired
    private EnrRequestedCompetitionManager _enrRequestedCompetitionManager;

    @Bean
    public ItemListExtension<IPropertyUpdateRule<Boolean>> contractEnrollmentAvailableRulesExtension()
    {
        return itemListExtension(_enrRequestedCompetitionManager.enrollmentAvailableRules())
        .add("enrEntrantContract.status", EnrEntrantContractExistsRule.INSTANCE)
        .create();
    }

}
