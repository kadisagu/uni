package ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractSpoTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.logic.EnrContractSpoTemplateSimpleDao;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.logic.IEnrContractSpoTemplateSimpleDao;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.ui.Edit.EnrContractSpoTemplateSimpleEdit;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.ui.Pub.EnrContractSpoTemplateSimplePub;

/**
 * Продуктовый шаблон договора на обучение
 * @see EnrContractSpoTemplateDataSimple
 * @author vdanilov
 */
@Configuration
public class EnrContractSpoTemplateSimpleManager extends BusinessObjectManager implements ICtrContractTemplateManager {

    public static EnrContractSpoTemplateSimpleManager instance() {
        return BusinessObjectManager.instance(EnrContractSpoTemplateSimpleManager.class);
    }

    @Override
    public Class<EnrContractSpoTemplateDataSimple> getDataEntityClass() {
        return EnrContractSpoTemplateDataSimple.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataDisplayComponent() {
        return EnrContractSpoTemplateSimplePub.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataEditComponent() {
        return EnrContractSpoTemplateSimpleEdit.class;
    }

    @Override
    public boolean isAllowEditInWizard() {
        return false;
    }

    @Bean
    @Override
    public IEnrContractSpoTemplateSimpleDao dao() {
        return new EnrContractSpoTemplateSimpleDao();
    }

}
