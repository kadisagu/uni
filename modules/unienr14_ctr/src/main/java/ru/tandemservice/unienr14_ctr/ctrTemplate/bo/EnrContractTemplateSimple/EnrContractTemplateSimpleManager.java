package ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;

import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.EnrContractTemplateSimpleDao;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.IEnrContractTemplateSimpleDao;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.ui.Edit.EnrContractTemplateSimpleEdit;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.ui.Pub.EnrContractTemplateSimplePub;

/**
 * Продуктовый шаблон договора на обучение
 * @see EnrContractTemplateDataSimple
 * @author vdanilov
 */
@Configuration
public class EnrContractTemplateSimpleManager extends BusinessObjectManager implements ICtrContractTemplateManager {

    public static EnrContractTemplateSimpleManager instance() {
        return BusinessObjectManager.instance(EnrContractTemplateSimpleManager.class);
    }

    @Override
    public Class<EnrContractTemplateDataSimple> getDataEntityClass() {
        return EnrContractTemplateDataSimple.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataDisplayComponent() {
        return EnrContractTemplateSimplePub.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataEditComponent() {
        return EnrContractTemplateSimpleEdit.class;
    }

    @Override
    public boolean isAllowEditInWizard() {
        return false;
    }

    @Bean
    @Override
    public IEnrContractTemplateSimpleDao dao() {
        return new EnrContractTemplateSimpleDao();
    }

}
