package ru.tandemservice.unienr14_ctr.base.entity;

import ru.tandemservice.unienr14_ctr.base.entity.gen.*;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.EnrContractSpoTemplateSimpleManager;

/** @see ru.tandemservice.unienr14_ctr.base.entity.gen.EnrContractSpoTemplateDataSimpleGen */
public class EnrContractSpoTemplateDataSimple extends EnrContractSpoTemplateDataSimpleGen
{
    @Override
    public EnrContractSpoTemplateSimpleManager getManager() {
        return EnrContractSpoTemplateSimpleManager.instance();
    }
}