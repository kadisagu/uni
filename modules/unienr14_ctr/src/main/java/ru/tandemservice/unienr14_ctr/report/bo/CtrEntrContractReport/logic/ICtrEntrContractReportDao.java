/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.logic;

import com.google.common.collect.Multimap;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentResult;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.ui.Add.CtrEntrContractReportAddUI;
import ru.tandemservice.unienr14_ctr.report.entity.CtrEntrContractReport;

import java.util.List;
import java.util.Map;

/**
 * @author Stanislav Shibarshin
 * @since 03.10.2016
 */
public interface ICtrEntrContractReportDao extends INeedPersistenceSupport
{

    Map<String, Multimap<String, List<Pair<Long, String>>>> prepareReportData(CtrEntrContractReport model, EnrCompetitionFilterAddon filter, boolean printRevenue);

    EduProgramHigherProf getContractEduProgram(CtrContractObject contract);

    List<CtrPaymentResult> getPaymentsSum(CtrContractObject contract);

    long createReport(CtrEntrContractReportAddUI model);

}
