package ru.tandemservice.unienr14_ctr.entrant.bo.EnrEntrantContract.logic;

import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;

import ru.tandemservice.unieductr.base.bo.EduProgramContract.logic.EduProgramSingleStudentContractObjectFactory;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

/**
 * @author vdanilov
 */
public abstract class EduProgramSingleEntrantContractObjectFactory extends EduProgramSingleStudentContractObjectFactory<EnrEntrant> {

    @Override
    protected String getStudentTitle(EnrEntrant entrant) {
        return entrant.getFullTitle();
    }

    @Override
    protected ContactorPerson getStudentContactPersion(EnrEntrant entrant) {
        return getContactPersion(entrant);
    }

    public static ContactorPerson getContactPersion(EnrEntrant entrant) {
        return getContactPersion(entrant.getPerson());
    }

}
