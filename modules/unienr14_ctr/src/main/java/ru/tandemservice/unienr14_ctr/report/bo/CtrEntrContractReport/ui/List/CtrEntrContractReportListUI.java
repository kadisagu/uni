/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unienr14_ctr.report.bo.CtrEntrContractReport.ui.Add.CtrEntrContractReportAdd;
import ru.tandemservice.unienr14_ctr.report.entity.CtrEntrContractReport;

/**
 * @author Stanislav Shibarshin
 * @since 03.10.2016
 */
@State ({
        @Bind(key = "orgUnitId", binding = "orgUnitId")
})
public class CtrEntrContractReportListUI extends UIPresenter
{

    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private CommonPostfixPermissionModel _secModel;

    @Override
    public void onComponentRefresh()
    {
        if (_orgUnitId != null) {
            _orgUnit = UniDaoFacade.getCoreDao().get(OrgUnit.class, _orgUnitId);
            _secModel = new OrgUnitSecModel(_orgUnit);
        }
    }

    public void onClickAddReport()
    {
        _uiActivation.asRegion(CtrEntrContractReportAdd.class).activate();
    }

    public void onClickPrint()
    {
        CtrEntrContractReport report = DataAccessServices.dao().get(getListenerParameterAsLong());

        if (report.getContent().getContent() == null)
            throw new ApplicationException("Файл печатной формы пуст.");

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(report.getContent()), false);
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public String getViewKey()
    {
        return _orgUnit == null ? "ctrEntrContractReport" :  _secModel.getPermission("orgUnit_viewCtrEntrContractReport");
    }

    public String getAddKey()
    {
        return _orgUnit == null ? "addEduCtrStorableReport" : _secModel.getPermission("orgUnit_addCtrEntrContractReport");
    }

    public String getDeleteKey()
    {
        return _orgUnit == null ? "deleteEduCtrStorableReport" : _secModel.getPermission("orgUnit_deleteCtrEntrContractReport");
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (_orgUnitId != null) {
            dataSource.put("orgUnitId", _orgUnitId);
        }
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _orgUnit != null ? _orgUnit: super.getSecuredObject();
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        this._secModel = secModel;
    }
}
