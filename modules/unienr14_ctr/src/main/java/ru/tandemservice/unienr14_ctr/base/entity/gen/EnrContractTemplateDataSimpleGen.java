package ru.tandemservice.unienr14_ctr.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateData;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные продуктового шаблона договора на обучение абитуриента
 *
 * Данные продуктового шаблона для создания договора на обучение абитуриента.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrContractTemplateDataSimpleGen extends EnrContractTemplateData
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple";
    public static final String ENTITY_NAME = "enrContractTemplateDataSimple";
    public static final int VERSION_HASH = -1896930618;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EnrContractTemplateDataSimpleGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrContractTemplateDataSimpleGen> extends EnrContractTemplateData.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrContractTemplateDataSimple.class;
        }

        public T newInstance()
        {
            return (T) new EnrContractTemplateDataSimple();
        }
    }
    private static final Path<EnrContractTemplateDataSimple> _dslPath = new Path<EnrContractTemplateDataSimple>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrContractTemplateDataSimple");
    }
            

    public static class Path<E extends EnrContractTemplateDataSimple> extends EnrContractTemplateData.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EnrContractTemplateDataSimple.class;
        }

        public String getEntityName()
        {
            return "enrContractTemplateDataSimple";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
