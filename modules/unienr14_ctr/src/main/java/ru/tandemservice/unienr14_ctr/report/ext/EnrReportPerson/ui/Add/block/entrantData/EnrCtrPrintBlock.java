/* $Id$ */
package ru.tandemservice.unienr14_ctr.report.ext.EnrReportPerson.ui.Add.block.entrantData;

import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintMapColumn;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.util.EnrReportPersonUtil;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.report.ext.EnrReportPerson.util.EnrCtrReportPersonUtil;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 06.07.2015
 */
public class EnrCtrPrintBlock implements IReportPrintBlock
{
    private IReportPrintCheckbox _hasContract = new ReportPrintCheckbox();
    private IReportPrintCheckbox _hasContractPayment = new ReportPrintCheckbox();

    @Override
    public String getCheckJS()
    {
        return null;
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_hasContract.isActive() || _hasContractPayment.isActive())
        {
            final String entrantAlias = dql.leftJoinEntity(EnrEntrant.class, EnrEntrant.person());
            final String requestAlias = dql.leftJoinEntity(entrantAlias, EnrEntrantRequest.class, EnrEntrantRequest.entrant());
            final String reqCompAlias = dql.leftJoinEntity(requestAlias, EnrRequestedCompetition.class, EnrRequestedCompetition.request());

            final int reqCompIndex = dql.getColumnIndex(reqCompAlias, null);

            if (_hasContract.isActive()) {
                printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("hasContract", reqCompIndex, null, new IFormatter<EnrRequestedCompetition>() {
                    @Override public String format(EnrRequestedCompetition competition) {
                        return YesNoFormatter.INSTANCE.format( EnrCtrReportPersonUtil.isHasContract(printInfo, competition));
                    }
                })
                {
                    @Override
                    public void prefetch(List<Object[]> rows)
                    {
                        EnrCtrReportPersonUtil.prefetchHasContractsData(printInfo, rows, reqCompIndex);
                    }
                });
            }

            if (_hasContractPayment.isActive()){
                printInfo.addPrintColumn(EnrReportPersonAddUI.ENTRANT_SCHEET, new ReportPrintMapColumn("hasContractPayment", reqCompIndex, null, new IFormatter<EnrRequestedCompetition>() {
                    @Override public String format(EnrRequestedCompetition competition) {
                        return YesNoFormatter.INSTANCE.format( EnrCtrReportPersonUtil.isHasContractPayment(printInfo, competition));
                    }
                })
                {
                    @Override
                    public void prefetch(List<Object[]> rows)
                    {
                        EnrCtrReportPersonUtil.prefetchHasContractPaymentData(printInfo, rows, reqCompIndex);
                    }
                });
            }
        }
    }

    public IReportPrintCheckbox getHasContract()
    {
        return _hasContract;
    }

    public IReportPrintCheckbox getHasContractPayment()
    {
        return _hasContractPayment;
    }
}