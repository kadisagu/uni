package ru.tandemservice.unisession.entity.stateFinalExam.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionStateExamProtocol;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionStateFinalExamProtocol;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Протокол на ГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionStateExamProtocolGen extends SessionStateFinalExamProtocol
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.stateFinalExam.SessionStateExamProtocol";
    public static final String ENTITY_NAME = "sessionStateExamProtocol";
    public static final int VERSION_HASH = -1265949637;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionStateExamProtocolGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionStateExamProtocolGen> extends SessionStateFinalExamProtocol.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionStateExamProtocol.class;
        }

        public T newInstance()
        {
            return (T) new SessionStateExamProtocol();
        }
    }
    private static final Path<SessionStateExamProtocol> _dslPath = new Path<SessionStateExamProtocol>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionStateExamProtocol");
    }
            

    public static class Path<E extends SessionStateExamProtocol> extends SessionStateFinalExamProtocol.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return SessionStateExamProtocol.class;
        }

        public String getEntityName()
        {
            return "sessionStateExamProtocol";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
