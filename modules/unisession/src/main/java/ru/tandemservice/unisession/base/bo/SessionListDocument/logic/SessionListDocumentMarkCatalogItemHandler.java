package ru.tandemservice.unisession.base.bo.SessionListDocument.logic;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author avedernikov
 * @since 20.07.2016
 */
public class SessionListDocumentMarkCatalogItemHandler extends SimpleTitledComboDataSourceHandler
{
	public static final String PARAM_WPE_CACTION = "wpeCAction";
	public static final String PARAM_CURR_MARK_VALUE = "currMark";

	public SessionListDocumentMarkCatalogItemHandler(final String ownerId)
	{
		super(ownerId);
	}

	@Override
	protected DSOutput execute(DSInput input, ExecutionContext context)
	{
		EppStudentWpeCAction cAction = context.getNotNull(PARAM_WPE_CACTION);
		SessionMarkCatalogItem currMark = context.get(PARAM_CURR_MARK_VALUE);
		EppGradeScale scale = ISessionDocumentBaseDAO.instance.get().getGradeScale(cAction);

		ICommonDAO dao = DataAccessServices.dao();
		// Оценки из соответствующей шкалы оценок
		List<SessionMarkCatalogItem> marks = Lists.newArrayList(dao.getList(SessionMarkGradeValueCatalogItem.class, SessionMarkGradeValueCatalogItem.scale(), scale, SessionMarkGradeValueCatalogItem.P_PRIORITY));
		// Отметки, используемые в системе (а также текущая выставленная отметка, если она стала неиспользуемой после выставления)
		dao.getList(SessionMarkStateCatalogItem.class, SessionMarkStateCatalogItem.P_PRIORITY).stream()
				.filter(mark -> mark.isVisible() || mark.equals(currMark))
				.forEach(marks::add);

		if (CollectionUtils.isNotEmpty(input.getPrimaryKeys()))
			marks = marks.stream().filter(mark -> input.getPrimaryKeys().contains(mark.getId())).collect(Collectors.toList());

		context.put(UIDefines.COMBO_OBJECT_LIST, marks);
		return super.execute(input, context);
	}
}
