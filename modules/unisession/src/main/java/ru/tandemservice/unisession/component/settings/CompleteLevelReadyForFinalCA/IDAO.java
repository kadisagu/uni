package ru.tandemservice.unisession.component.settings.CompleteLevelReadyForFinalCA;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author iolshvang
 * @since 30.05.2011
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
    void doToggleReadyForFCA(Long id);
    boolean isFormAnyStates();
    void setFormAnyStates(boolean isFormAnyStates);
}