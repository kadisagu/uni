package ru.tandemservice.unisession.entity.document;

import ru.tandemservice.unisession.entity.document.gen.SessionTransferProtocolRowGen;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;

/** @see ru.tandemservice.unisession.entity.document.gen.SessionTransferProtocolRowGen */
public class SessionTransferProtocolRow extends SessionTransferProtocolRowGen
{
    public SessionTransferProtocolRow()
    {
    }

    public SessionTransferProtocolRow(SessionTransferProtocolDocument protocol, SessionALRequestRow requestRow, boolean needRetake)
    {
        setProtocol(protocol);
        setRequestRow(requestRow);
        setNeedRetake(needRetake);
    }
}