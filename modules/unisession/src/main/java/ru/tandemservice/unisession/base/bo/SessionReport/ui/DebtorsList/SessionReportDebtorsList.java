/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport;

/**
 * @author oleyba
 * @since 1/24/12
 */
@Configuration
public class SessionReportDebtorsList extends BusinessComponentManager
{
    public static final String DS_REPORTS = "sessionReportDebtorsDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(SessionReportManager.instance().eduYearDSConfig())
            .addDataSource(SessionReportManager.instance().yearPartDSConfig())
            .addDataSource(this.searchListDS(DS_REPORTS, this.sessionReportDebtorsDS(), this.sessionReportDebtorsDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint sessionReportDebtorsDS() {
        return this.columnListExtPointBuilder(DS_REPORTS)
            .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")).create())
            .addColumn(publisherColumn("date", UnisessionDebtorsReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
            .addColumn(textColumn("year", UnisessionDebtorsReport.sessionObject().educationYear().title().s()).create())
            .addColumn(textColumn("part", UnisessionDebtorsReport.sessionObject().yearDistributionPart().title().s()).create())
            .addColumn(textColumn("exec", UnisessionDebtorsReport.executor().s()).create())
            .addColumn(textColumn("course", UnisessionDebtorsReport.course().s()).create())
            .addColumn(textColumn("groups", UnisessionDebtorsReport.groups().s()).create())
            .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("ui:deleteStorableReportPermissionKey")
                .alert(FormattedMessage.with().template("sessionReportDebtorsDS.delete.alert").parameter(UnisessionDebtorsReport.formingDateStr().s()).create())
            )
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionReportDebtorsDSHandler() {
        return new SessionReportDebtorsDSHandler(this.getName());
    }
}