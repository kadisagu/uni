/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionMark.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionTransferInsideOperation;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/14/11
 */
public class SessionUnactualMarkDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_STUDENT = "studentKey";
    public static final String PARAM_TRANSFERRED = "transferredKey";
    public static final String PARAM_CURRENT_EPV = "сurrentEpv";

    public SessionUnactualMarkDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        final Student student = context.get(PARAM_STUDENT);

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionSlotRegularMark.class, "e")
            .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("e"), "s")
            .where(isNotNull(property(SessionDocumentSlot.studentWpeCAction().studentWpe().removalDate().fromAlias("s"))))
            .where(eq(property(SessionDocumentSlot.actualStudent().archival().fromAlias("s")), value(Boolean.FALSE)))
            .where(eq(property(SessionDocumentSlot.actualStudent().status().active().fromAlias("s")), value(Boolean.TRUE)))
            .column("e")
            .order(property(SessionMark.slot().studentWpeCAction().studentWpe().student().person().identityCard().fullFio().fromAlias("e")))
            .order(property(SessionMark.performDate().fromAlias("e")))
            ;

        Boolean currentEpv = context.get(PARAM_CURRENT_EPV);
        if (null != currentEpv) {
            IDQLExpression epvExpression = property(SessionDocumentSlot.studentWpeCAction().studentWpe().removalDate().fromAlias("s"));
            dql.where(currentEpv ? isNull(epvExpression) : isNotNull(epvExpression));
        }

        final Boolean transferred = context.get(PARAM_TRANSFERRED);
        if (null != transferred)
        {
            if (transferred)
                dql.joinEntity("e", DQLJoinType.inner, SessionTransferInsideOperation.class, "op", eq(property(SessionTransferInsideOperation.sourceMark().fromAlias("op")), property("e")));
            else {
                dql
                    .joinEntity("e", DQLJoinType.left, SessionTransferInsideOperation.class, "op", eq(property(SessionTransferInsideOperation.sourceMark().fromAlias("op")), property("e")))
                    .where(isNull("op.id"));
            }
        }

        if (null != student)
            dql.where(eq(property(SessionMark.slot().studentWpeCAction().studentWpe().student().fromAlias("e")), value(student)));

        return DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
    }
}

