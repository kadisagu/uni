/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import com.google.common.collect.Collections2;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;

import java.util.*;

/**
* @author oleyba
* @since 2/13/12
*/
public class EduLevelAndSpecAndCourseReportTable extends ReportTable
{
    private EduProgramSubject subject;
    private EduProgramSpecialization spec;

    public EduLevelAndSpecAndCourseReportTable(final EduProgramSubject subject, final EduProgramSpecialization spec, Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> students)
    {
        this.subject = subject;
        this.spec = spec;
        Set<Course> courses = new HashSet<>(Collections2.transform(
                Collections2.filter(students, input -> Objects.equals(input.getLevel().getEducationLevel().getEduProgramSubject(), subject) &&
                                            Objects.equals(input.getLevel().getEducationLevel().getEduProgramSpecialization(), spec)
                ),
                ISessionReportResultsDAO.ISessionResultsReportStudentData::getCourse
        ));

        for (final Course course : courses) {
            rowList.add(new ReportRow(course.getTitle(), CollectionUtils.select(
                    students, input -> Objects.equals(input.getCourse(), course) &&
                            Objects.equals(input.getLevel().getEducationLevel().getEduProgramSubject(), subject) &&
                            Objects.equals(input.getLevel().getEducationLevel().getEduProgramSpecialization(), spec)
            )));
        }
        Collections.sort(rowList, ITitled.TITLED_COMPARATOR);
        rowList.add(totalsRow(students));
    }

    @Override
    protected ReportRow totalsRow(Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> students)
    {
        return new ReportRow("Итого", CollectionUtils.select(students, input -> Objects.equals(input.getLevel().getEducationLevel().getEduProgramSubject(), subject) &&
                Objects.equals(input.getLevel().getEducationLevel().getEduProgramSpecialization(), spec)));
    }

    @Override
    public void modify(RtfDocument rtf)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("groupTitle", "Курс");
        modifier.put("Title", "Курс");
        modifier.put("criterion", "направлениям и профилям");
        modifier.put("eduLevelType", "");
        modifier.put("eduLevelTitle", subject.getTitleWithCode() +
                (spec != null ? " (" + spec.getDisplayableTitle() + ")" : " (до выбора направленности)"));
        modifier.modify(rtf);
    }


    public EduProgramSubject getSubject()
    {
        return subject;
    }

    public EduProgramSpecialization getSpec()
    {
        return spec;
    }
}
