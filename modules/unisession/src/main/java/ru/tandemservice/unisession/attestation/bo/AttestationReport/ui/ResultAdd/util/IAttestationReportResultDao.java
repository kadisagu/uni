/**
 *$Id:$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd.util;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationResultReport;

/**
 * @author Alexander Shaburov
 * @since 14.11.12
 */
public interface IAttestationReportResultDao extends INeedPersistenceSupport
{
    /**
     * Создает печатный документ отчета.
     * @return RtfDocument
     */
    <M extends Model> RtfDocument createReportRtfDocument(M model);

    /**
     * Сохраняет отчет и его печатную форму.
     */
    <M extends Model> SessionAttestationResultReport saveReport(M model, RtfDocument document);

    <M extends Model> ErrorCollector validate(M model);
}
