package ru.tandemservice.unisession.base.bo.StudentPortfolio.logic;

import com.google.common.collect.Iterables;
import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.commonbase.file.FileNotFoundApplicationException;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import org.tandemframework.shared.commonbase.remoteDocument.service.ICommonRemoteDocumentService;
import org.tandemframework.shared.commonbase.remoteDocument.service.IRemoteDocumentProvider;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.remoteDocument.ext.RemoteDocument.RemoteDocumentExtManager;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 11.12.2015
 */

public class StudentPortfolioDAO extends SharedBaseDao implements IStudentPortfolioDAO
{
	@Override
	public void addAchievementsByWpeCActions(Student student)
	{
		DQLSelectBuilder usedWpeCActions = usedActionsDql(student);
		List<EppStudentWpeCAction> cActionsToAdd = getActionsToAdd(student, usedWpeCActions);

		Date today = new Date();
		DQLInsertValuesBuilder insertDql = new DQLInsertValuesBuilder(StudentPortfolioElement.class);

		Iterables.partition(cActionsToAdd, DQL.MAX_VALUES_ROW_NUMBER).forEach(cActionList -> {
			cActionList.forEach(cAction -> {
				insertDql.valuesFrom(buildAchievement(student, cAction, today));
				insertDql.addBatch();
			});
			insertDql.createStatement(getSession()).execute();
		});
	}

	private DQLSelectBuilder usedActionsDql(Student student)
	{
		final String achievementAlias = "achievement";
		return new DQLSelectBuilder()
				.fromEntity(StudentPortfolioElement.class, achievementAlias)
				.column(property(achievementAlias, StudentPortfolioElement.wpeCAction().id()))
				.where(eq(property(achievementAlias, StudentPortfolioElement.student()), value(student)))
				.where(isNotNull(property(achievementAlias, StudentPortfolioElement.wpeCAction())));
	}

	private List<EppStudentWpeCAction> getActionsToAdd(Student student, DQLSelectBuilder actionsToSkip)
	{
		final String markAlias = "mark";
		final String documentAlias = "document";
		return new DQLSelectBuilder()
				.fromEntity(SessionMark.class, markAlias)
				.joinPath(DQLJoinType.inner, SessionMark.slot().document().fromAlias(markAlias), documentAlias)
				.column(property(markAlias, SessionMark.slot().studentWpeCAction()))
				.where(and(
						eq(property(markAlias, SessionMark.slot().studentWpeCAction().studentWpe().student()), value(student)),
						notIn(property(markAlias, SessionMark.slot().studentWpeCAction().id()), actionsToSkip.buildQuery()),
						instanceOf(documentAlias, SessionStudentGradeBookDocument.class),
						eq(property(markAlias, SessionMark.slot().inSession()), value(Boolean.FALSE)),
						eq(property(markAlias, SessionMark.cachedMarkPositiveStatus()), value(Boolean.TRUE))
				))
				.createStatement(getSession()).list();
	}

	private StudentPortfolioElement buildAchievement(Student student, EppStudentWpeCAction cAction, Date editDay)
	{
		StudentPortfolioElement achievement = new StudentPortfolioElement();
		achievement.setStudent(student);
		achievement.setEditDate(editDay);
		achievement.setTitle(cAction.getRegistryElementTitle());
		achievement.setWpeCAction(cAction);
		return achievement;
	}

	@Override
	public Long createOrUpdateAchievementAttachement(StudentPortfolioElement element, Long loadFileId, IUploadFile file)
	{
		return loadFileId != null ? updateRemoteDocument(element, loadFileId, file) : file != null ? createRemoteDocument(element, file) : null;
	}

	private Long updateRemoteDocument(StudentPortfolioElement element, Long loadFileId, IUploadFile file)
	{
		try (ICommonRemoteDocumentService documentService = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.STUDENT_PORTFOLIO_ACHIEVEMENT_SERVICE_NAME))
		{
			RemoteDocumentDTO documentDTO = documentService.get(loadFileId);
			if(documentDTO == null)
				return createRemoteDocument(element, file);

			if(file != null)
				documentDTO.setFileContent(file.getFileName(), IOUtils.toByteArray(file.getStream()));

			documentService.update(documentDTO);
            return documentDTO.getId();
		}
		catch (IOException e)
		{
			throw CoreExceptionUtils.getRuntimeException(e);
		}


	}

	private Long createRemoteDocument(StudentPortfolioElement element, IUploadFile file)
	{
		try (ICommonRemoteDocumentService documentService = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.STUDENT_PORTFOLIO_ACHIEVEMENT_SERVICE_NAME))
		{
			RemoteDocumentDTO documentDTO = new RemoteDocumentDTO(createFileTitle(element), RemoteDocumentExtManager.REMOTE_DOCUMENT_MODULE, RemoteDocumentExtManager.STUDENT_PORTFOLIO_ACHIEVEMENT_SERVICE_NAME);
			documentDTO.setOwnerId(element.getId());

			if(file != null)
				documentDTO.setFileContent(file.getFileName(), IOUtils.toByteArray(file.getStream()));

			Long attachmentId = documentService.insert(documentDTO);

			return attachmentId;
		}
		catch (IOException e)
		{
			throw CoreExceptionUtils.getRuntimeException(e);
		}
	}

	private String createFileTitle(StudentPortfolioElement element)
	{
		return "Достижение " + element.getTitle() + " студента " + element.getStudent().getFullFio();
	}

	@Override
	public RemoteDocumentDTO getAchievementAttachement(Long achievementId)
	{
		Long fileId = this.<StudentPortfolioElement>getNotNull(achievementId).getFile();
		try (final ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.STUDENT_PORTFOLIO_ACHIEVEMENT_SERVICE_NAME))
		{
			return service.get(fileId);
		}
	}

	@Override
	public void removeAttachementFromAchievement(Long achievementId)
	{
		Long fileId = this.<StudentPortfolioElement>getNotNull(achievementId).getFile();
		if (fileId == null)
			return;
		try (final ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get().taskService(RemoteDocumentExtManager.STUDENT_PORTFOLIO_ACHIEVEMENT_SERVICE_NAME))
		{
			RemoteDocumentDTO dto = null;
			try {
				dto = service.get(fileId);
			} catch (FileNotFoundApplicationException ignored) {
			}
			if (dto != null)
				service.delete(dto);
		}
	}
}
