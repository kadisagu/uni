/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.FqwSummaryResultAdd.logic;

import jxl.write.WritableCellFormat;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.AbsStateFinalAttestationRow;
import ru.tandemservice.unisession.base.bo.SessionReport.logic.SfaRaw;
import ru.tandemservice.unisession.entity.catalog.SessionRecommendation4FQW;
import ru.tandemservice.unisession.entity.catalog.SessionSource4FQWTheme;
import ru.tandemservice.unisession.entity.stateFinalExam.SessionFQWProtocol;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 02.12.2016
 */
public class Row extends ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultAdd.logic.Row
{
    protected int _honors = 0;                                                                      // Количество дипломов с отличием
    protected Double _honorsProgress = 0d;                                                          // Качественный показатель (дипл. с отл.)
    protected Map<SessionSource4FQWTheme, int[]> _themesNumbers = new HashMap<>();                  // Количество ВКР выполненных
    protected Map<SessionRecommendation4FQW, int[]> _recommendationsNumbers = new HashMap<>();      // Количество ВКР рекомендованных

    public Row(EducationOrgUnit educationOrgUnit, String title, WritableCellFormat titleStyle, WritableCellFormat intStyle, WritableCellFormat redIntStyle, WritableCellFormat doubleStyle)
    {
        super(educationOrgUnit, title, titleStyle, intStyle, redIntStyle, doubleStyle);
    }

    protected Row initProtocolsMap(List<SessionSource4FQWTheme> themes, List<SessionRecommendation4FQW> recommendations)
    {
        _themesNumbers = themes.stream().collect(Collectors.toMap(theme -> theme, themesNumbers -> new int[]{0}));
        _recommendationsNumbers = recommendations.stream().collect(Collectors.toMap(theme -> theme, themesNumbers -> new int[]{0}));

        return this;
    }

    @Override
    public void calcProgress()
    {
        super.calcProgress();
        _honorsProgress = _passed > 0 ? 100 * _honors / (double) _passed : null;
    }

    @Override
    protected void addFromRaw(Student student, List<? extends SfaRaw> raws)
    {
        super.addFromRaw(student, raws);
        if (raws != null)
        {
            raws.forEach(raw ->
                         {

                             SessionFQWProtocol protocol = ((Raw) raw).getProtocol();
                             if (protocol != null)
                             {
                                 if (protocol.isWithHonors())
                                     _honors++;

                                 SessionSource4FQWTheme theme = protocol.getSource4Theme();
                                 if (theme != null) _themesNumbers.get(theme)[0]++;

                                 SessionRecommendation4FQW recommendation = protocol.getRecommendation();
                                 if (recommendation != null) _recommendationsNumbers.get(recommendation)[0]++;
                             }
                         });
        }
    }

    @Override
    public <T extends AbsStateFinalAttestationRow> void addFromRow(T absRow)
    {
        super.addFromRow(absRow);

        if(absRow instanceof Row)
        {
            Row row = (Row)absRow;

            _honors += row.getHonors();

            for (Map.Entry<SessionSource4FQWTheme, int[]> entry : _themesNumbers.entrySet())
                entry.getValue()[0] += row.getThemesNumbers().get(entry.getKey())[0];

            for (Map.Entry<SessionRecommendation4FQW, int[]> entry : _recommendationsNumbers.entrySet())
                entry.getValue()[0] += row.getRecommendationsNumbers().get(entry.getKey())[0];
        }
    }

    public int getHonors()
    {
        return _honors;
    }

    public Double getHonorsProgress()
    {
        return _honorsProgress;
    }

    public Map<SessionSource4FQWTheme, int[]> getThemesNumbers()
    {
        return _themesNumbers;
    }

    public Map<SessionRecommendation4FQW, int[]> getRecommendationsNumbers()
    {
        return _recommendationsNumbers;
    }
}
