/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;

import javax.annotation.Nullable;
import java.util.*;

/**
* @author oleyba
* @since 2/13/12
*/
public class EduLevelReportTable extends ReportTable
{
    @Override
    public void modify(RtfDocument rtf)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("groupTitle", "Направление (специальность)");
        modifier.put("Title", "Направление");
        modifier.put("criterion", "направлениям");
        modifier.modify(rtf);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(rtf, Arrays.asList("eduLevelType", "eduLevelTitle"), false, false);
    }

    public EduLevelReportTable(Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> students, boolean useFullTitle)
    {
        Set<EducationLevelsHighSchool> levels = new HashSet<>(Collections2.transform(students, new Function<ISessionReportResultsDAO.ISessionResultsReportStudentData, EducationLevelsHighSchool>()
        {
            @Override
            public EducationLevelsHighSchool apply(@Nullable ISessionReportResultsDAO.ISessionResultsReportStudentData input)
            {
                return input.getLevel();
            }
        }));

        for (EducationLevelsHighSchool level : levels)
        {
            if (level.getEducationLevel().getEduProgramSubject() == null)
                UserContext.getInstance().getErrorCollector().addError("Не найдено сопоставление новому направлению для НПм «" + level.getDisplayableTitle() + "», необходимо заполнить настройку «Связь НПв, НПм с направлениями ОП»");
        }

        if(UserContext.getInstance().getErrorCollector().hasErrors())
            throw new ApplicationException();

        Set<EduProgramSubject> subjects = Sets.newHashSet();
        for (final EducationLevelsHighSchool level : levels)
        {
            subjects.add(level.getEducationLevel().getEduProgramSubject());
        }

        for(final EduProgramSubject subject : subjects)
        {
            rowList.add(new ReportRow(useFullTitle ? subject.getTitleWithCode() : subject.getShortTitleWithCode(), CollectionUtils.select(students, input -> input.getLevel().getEducationLevel().getEduProgramSubject().equals(subject))));
        }

        Collections.sort(rowList, ITitled.TITLED_COMPARATOR);
        rowList.add(totalsRow(students));
    }
}
