/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionDocument.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.component.edugroup.GroupViewWrapperBatchAction;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.base.bo.SessionDocument.util.SessionRealEduGroupFilterAddon;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 15.12.2015
 */
public class SessionRealEduGroupRowHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
	public static final String PROPERTY_CHECK_DISABLED = "checkDisabled";
	public static final String PROPERTY_COURSE = "course";
	public static final String PROPERTY_GROUP = "group";
	public static final String PROPERTY_PPS = "pps";
	public static final String PROPERTY_OWNER = "owner";

	private final Class<? extends EppRealEduGroup> _realEduGroupClazz;
	private final Class<? extends EppRealEduGroupRow> _eduGroupRowClazz;

	public SessionRealEduGroupRowHandler(String ownerId, Class<? extends EppRealEduGroup> realEduGroupClazz, Class<? extends EppRealEduGroupRow> eduGroupRowClazz)
	{
		super(ownerId);
		_realEduGroupClazz = realEduGroupClazz;
		_eduGroupRowClazz = eduGroupRowClazz;
	}

	public DQLSelectBuilder createBuilder(String alias, ExecutionContext context)
	{
		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(_eduGroupRowClazz, alias).distinct()
				.column(property(alias, EppRealEduGroupRow.group().id()))
				.column(property(alias, EppRealEduGroupRow.studentWpePart().studentWpe().course().intValue()))
				.column(property(alias, EppRealEduGroupRow.studentGroupTitle()))
				.joinPath(DQLJoinType.inner, EppRealEduGroupRow.studentWpePart().studentWpe().student().educationOrgUnit().fromAlias(alias), "ou")
				.joinPath(DQLJoinType.inner, EppRealEduGroupRow.group().summary().yearPart().fromAlias(alias), "yp")
				.where(isNull(property(alias, EppRealEduGroupRow.removalDate())))
				.where(eq(property("ou", EducationOrgUnit.groupOrgUnit()), value(context.<OrgUnit>get(AttestationManager.PARAM_ORG_UNIT))))
				.where(eq(property("yp", EppYearPart.year().educationYear()), value(context.<EducationYear>get(AttestationManager.PARAM_EDU_YEAR))))
				.where(eq(property("yp", EppYearPart.part()), value(context.<YearDistributionPart>get(AttestationManager.PARAM_YEAR_PART))))
				.order(property(alias, EppRealEduGroupRow.studentWpePart().studentWpe().course().intValue()))
				.order(property(alias, EppRealEduGroupRow.studentGroupTitle()));

		SessionRealEduGroupFilterAddon util = context.get(SessionRealEduGroupFilterAddon.UTIL_NAME);

		if (null != util && !util.isEmptyFilters())
			dql.where(in(property(alias, EppRealEduGroupRow.id()), util.getEntityIdsFilteredBuilder(alias).buildQuery()));

		return dql;
	}

	public Collection<Long> modifyIds(String alias, ExecutionContext context)
	{
		return createBuilder(alias, context).createStatement(context.getSession()).<Object[]>list().stream()
				.map(item -> (Long) item[0])
				.collect(Collectors.toList());
	}

	@Override
	protected DSOutput execute(DSInput input, ExecutionContext context)
	{
		String alias = "alias";
		Collection<Long> ids = modifyIds(alias, context);
		List<EppRealEduGroup> list = CommonDAO.sort(new DQLSelectBuilder().fromEntity(_realEduGroupClazz, alias).where(in(property(alias, "id"), ids)).createStatement(context.getSession()).list(), ids);
		List<ViewWrapper<EppRealEduGroup>> wrapperList = ViewWrapper.getPatchedList(list);
		BatchUtils.execute(wrapperList, DQL.MAX_VALUES_ROW_NUMBER, new GroupViewWrapperBatchAction<EppRealEduGroup>(_eduGroupRowClazz, context.getSession())
		{
			private final boolean ignoreCompleteLevels = ISessionDocumentBaseDAO.instance.get().isIgnoreRealEduGroupCompleteLevel();

			@Override
			public void execute(Collection<ViewWrapper<EppRealEduGroup>> collection)
			{
				final Collection<Long> ids = UniBaseDao.ids(wrapperList);

				final Map<Long, Map<String, Integer>> ownerMap = getRelationMap(ids, EppRealEduGroupRow.studentEducationOrgUnit().groupOrgUnit().title());
				final Map<Long, Map<Integer, Integer>> courseMap = getRelationMap(ids, EppRealEduGroupRow.studentWpePart().studentWpe().course().intValue());
				final Map<Long, Map<String, Integer>> eppStudentGroupMap = getRelationMap(ids, EppRealEduGroupRow.studentGroupTitle());
				final Map<Long, Map<Long, Integer>> ppsMap = getTutorMap(ids);


				for (ViewWrapper<EppRealEduGroup> wrapper : wrapperList)
				{
					boolean allowed = ignoreCompleteLevels || (null != wrapper.getEntity().getLevel() && wrapper.getEntity().getLevel().isReadyForFinalControlAction());

					wrapper.setViewProperty(PROPERTY_CHECK_DISABLED, !allowed);
					wrapper.setViewProperty(PROPERTY_COURSE, collect(courseMap, wrapper.getId(), null, false));
					wrapper.setViewProperty(PROPERTY_GROUP, collect(eppStudentGroupMap, wrapper.getId(), null, true));
					wrapper.setViewProperty(PROPERTY_PPS, collect(ppsMap, wrapper.getId(), ppsFormatter, false));

					final Map<String, Integer> o = ownerMap.get(wrapper.getId());
					if (null != o && o.size() > 1)
					{
						// деканат показывается только тогда, когда в группе их несколько
						// (если деканат один - то он совпадает с текущим подразделением - и его показывать смысла все равно нет)
						wrapper.setViewProperty(PROPERTY_OWNER, collect(ownerMap, wrapper.getId(), null, true));
					}
					else
						wrapper.setViewProperty(PROPERTY_OWNER, "");
				}
			}
		});
		DSOutput output = ListOutputBuilder.get(input, wrapperList).pageable(true).build();
		int countRecord = Math.max(5, Math.max(output.getRecordList().size(), input.getCountRecord()));
		output.setCountRecord(countRecord);
		return output;
	}
}

