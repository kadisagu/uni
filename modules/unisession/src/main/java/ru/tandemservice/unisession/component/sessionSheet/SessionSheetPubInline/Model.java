// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetPubInline;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.List;

/**
 * @author oleyba
 * @since 2/16/11
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="sheet.id")
})
public class Model
{
    private SessionSheetDocument sheet = new SessionSheetDocument();
    private SessionDocumentSlot slot;
    private SessionMark mark;
    private Double currentRating;
    private Double scoredPoints;

    private ISessionBrsDao.ISessionRatingSettings ratingSettings;

    private List<PpsEntry> tutors;

	private boolean themeRequired;
	private String theme;

    public boolean isUsePoints()
    {
        return getRatingSettings() != null && getRatingSettings().usePoints();
    }

    public boolean isUseCurrentRating()
    {
        return getRatingSettings() != null && getRatingSettings().useCurrentRating();
    }

    // getters and setters

    public SessionSheetDocument getSheet()
    {
        return this.sheet;
    }

    public void setSheet(final SessionSheetDocument sheet)
    {
        this.sheet = sheet;
    }

    public SessionDocumentSlot getSlot()
    {
        return this.slot;
    }

    public void setSlot(final SessionDocumentSlot slot)
    {
        this.slot = slot;
    }

    public SessionMark getMark()
    {
        return this.mark;
    }

    public void setMark(final SessionMark mark)
    {
        this.mark = mark;
    }

    public List<PpsEntry> getTutors()
    {
        return this.tutors;
    }

    public void setTutors(final List<PpsEntry> tutors)
    {
        this.tutors = tutors;
    }

    public Double getCurrentRating()
    {
        return currentRating;
    }

    public void setCurrentRating(Double currentRating)
    {
        this.currentRating = currentRating;
    }

    public Double getScoredPoints()
    {
        return scoredPoints;
    }

    public void setScoredPoints(Double scoredPoints)
    {
        this.scoredPoints = scoredPoints;
    }

    public ISessionBrsDao.ISessionRatingSettings getRatingSettings()
    {
        return ratingSettings;
    }

    public void setRatingSettings(ISessionBrsDao.ISessionRatingSettings ratingSettings)
    {
        this.ratingSettings = ratingSettings;
    }

	public boolean isThemeRequired()
	{
		return themeRequired;
	}

	public void setThemeRequired(boolean themeRequired)
	{
		this.themeRequired = themeRequired;
	}

	public String getTheme()
	{
		return theme;
	}

	public void setTheme(final String theme)
	{
		this.theme = theme;
	}
}
