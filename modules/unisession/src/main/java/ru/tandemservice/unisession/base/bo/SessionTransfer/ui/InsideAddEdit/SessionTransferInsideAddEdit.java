/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.InsideAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.StudentByGroupOrgUnitComboDSHandler;

import static org.tandemframework.hibsupport.dql.DQLExpressions.isNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 10/19/11
 */
@Configuration
public class SessionTransferInsideAddEdit extends BusinessComponentManager
{
    // todo уникальность в пределах чего нужна?

    public static final String DS_STUDENT = "studentDS";
    public static final String DS_TARGET_EPP_SLOT = "targetEppSlotDS";

    public static final String KEY_STUDENT = "student";
    public static final String KEY_TARGET_EDU_YEAR = "targetEduYear";
    public static final String KEY_TARGET_YEAR_PART = "targetYearPart";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
                .addDataSource(targetEppSlotDSDSConfig())
                .addDataSource(selectDS(DS_STUDENT, studentComboDSHandler()).addColumn(Student.titleWithFio().s()))
                .create();
    }

    @Bean
    public UIDataSourceConfig targetEppSlotDSDSConfig()
    {
        return SelectDSConfig.with(DS_TARGET_EPP_SLOT, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn("title", EppStudentWpeCAction.registryElementTitle().s())
                .handler(this.targetEppSlotDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> targetEppSlotDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppStudentWpeCAction.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(isNull(property(EppStudentWpeCAction.removalDate().fromAlias(alias))));
                if (!context.hasProperty(KEY_STUDENT) || !context.hasProperty(KEY_TARGET_EDU_YEAR) || !context.hasProperty(KEY_TARGET_YEAR_PART))
                    dql.where(isNull(alias + ".id"));
            }
        }
                .where(EppStudentWpeCAction.studentWpe().student(), KEY_STUDENT)
                .where(EppStudentWpeCAction.studentWpe().year().educationYear(), KEY_TARGET_EDU_YEAR)
                .where(EppStudentWpeCAction.studentWpe().part(), KEY_TARGET_YEAR_PART)
                .order(EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().title());
    }

    @Bean
    public IDefaultComboDataSourceHandler studentComboDSHandler()
    {
        return new StudentByGroupOrgUnitComboDSHandler(getName());
    }
}
