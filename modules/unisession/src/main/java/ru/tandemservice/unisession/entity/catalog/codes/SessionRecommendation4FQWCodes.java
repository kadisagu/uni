package ru.tandemservice.unisession.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Рекомендация по ВКР"
 * Имя сущности : sessionRecommendation4FQW
 * Файл data.xml : session.data.xml
 */
public interface SessionRecommendation4FQWCodes
{
    /** Константа кода (code) элемента : к опубликованию (title) */
    String FOR_PUBLICATION = "session.FOWrec.1";
    /** Константа кода (code) элемента : к внедрению (title) */
    String FOR_IMPLEMENTATION = "session.FOWrec.2";
    /** Константа кода (code) элемента : внедрена (title) */
    String IMPLEMENTED = "session.FOWrec.3";

    Set<String> CODES = ImmutableSet.of(FOR_PUBLICATION, FOR_IMPLEMENTATION, IMPLEMENTED);
}
