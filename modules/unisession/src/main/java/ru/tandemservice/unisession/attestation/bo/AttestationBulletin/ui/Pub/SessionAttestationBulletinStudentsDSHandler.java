/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.Pub;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/9/12
 */
public class SessionAttestationBulletinStudentsDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_BULLETIN = "bulletin";
    public static final String PROPERTY_DATA = "data";

    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    public SessionAttestationBulletinStudentsDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        final SessionAttestationBulletin bulletin = context.get(PARAM_BULLETIN);
        if (null == bulletin) {
            return ListOutputBuilder.get(dsInput, Collections.emptyList()).build();
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionAttestationSlot.class, "s")
            .column(property("s"))
            .where(eq(property(SessionAttestationSlot.bulletin().fromAlias("s")), value(bulletin)))
            ;

        registry.applyOrderWithLeftJoins(dql, dsInput.getEntityOrder());

        DQLSelectBuilder dataDql = new DQLSelectBuilder()
            .fromEntity(SessionAttestationSlotAdditionalData.class, "d").column("d")
            .where(eq(property(SessionAttestationSlotAdditionalData.slot().bulletin().fromAlias("d")), value(bulletin)))
            ;
        Map<Long, SessionAttestationSlotAdditionalData> dataMap = new HashMap<>();
        for (SessionAttestationSlotAdditionalData data : dataDql.createStatement(context.getSession()).<SessionAttestationSlotAdditionalData>list())
            dataMap.put(data.getSlot().getId(), data);
        
        DSOutput output = ListOutputBuilder.get(dsInput, dql.createStatement(context.getSession()).list()).build();
        for (DataWrapper wrapper : DataWrapper.wrap(output)) {
            wrapper.setProperty(PROPERTY_DATA, dataMap.get(wrapper.getId()));
        }
        
        return output;
    }

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(SessionAttestationSlot.class, "s");
    }
}

