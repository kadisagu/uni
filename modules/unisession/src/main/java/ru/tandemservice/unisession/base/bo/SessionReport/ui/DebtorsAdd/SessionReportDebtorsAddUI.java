/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsPub.SessionReportDebtorsPub;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 1/24/12
 */
@Input
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id", required = true)
})
public class SessionReportDebtorsAddUI extends UIPresenter
{
    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    private ISelectModel eduLevelModel;
    private ISelectModel groupModel;

    private EducationYear year;
    private YearDistributionPart part;

    private Date performDateFrom;
    private Date performDateTo;

    private boolean performDateFromActive;
    private boolean performDateToActive;

    private List<EppFControlActionType> eppFControlActionTypes;

    private boolean studentStatusInitialized;

    @Override
    public void onComponentActivate()
    {
        getOuHolder().refresh();
    }

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();

        if (null == getYear())
            setYear(EducationYear.getCurrentRequired());

        configUtil(getSessionFilterAddon());
    }

    public UniSessionFilterAddon getSessionFilterAddon()
    {
        return (UniSessionFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
    }

    public void onChangeStudentStatusActive()
    {
        if (!isStudentStatusInitialized()) {
            setStudentStatusInitialized(true);
        }
    }

    public void validate()
    {
        if (performDateFromActive && performDateToActive && (performDateFrom.getTime() - performDateTo.getTime() > 0))
            _uiSupport.error("Дата, указанная в параметре \"Дата сдачи мероприятия в ведомости с\" не должна быть позже даты в параметре \"Дата сдачи мероприятия в ведомости по\".", "dateFrom");
        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, getYear());
        dataSource.put(StudentCatalogsManager.PARAM_STUDENT_STATUS_ONLY_ACTIVE, Boolean.TRUE);
    }

    public void onClickApply()
    {
        validate();
        UnisessionDebtorsReport report = ISessionReportDebtorsDAO.instance.get().createStoredReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(SessionReportDebtorsPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }

    public void onChangeYearOrPart()
    {
        configUtilWhere(getSessionFilterAddon());
    }



    private void configUtil(UniSessionFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());


        util.clearFilterItems();

        util

                .addFilterItem(UniSessionFilterAddon.REGISTRY_STRUCTURE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.CONTROL_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.EDUCATION_LEVELS, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.DEVELOP_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.DEVELOP_CONDITION, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.DEVELOP_TECH, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.DEVELOP_PERIOD, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.COURSE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.GROUP_WITH_NO_GROUP, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.STUDENT_STATUS, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.STUDENT_CUSTOM_STATE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.TARGET_ADMISSION, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG);


        configUtilWhere(util);
    }

    public void configUtilWhere(UniSessionFilterAddon util)
    {
        util.clearWhereFilter();

        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().status().active(), true));
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().educationOrgUnit().formativeOrgUnit(), getOrgUnit()));

        if (null != year) util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().year().educationYear(), year));
        if (null != part) util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().part(), part));
    }





    // utils

    public SessionObject getSessionObject()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionObject.class, "obj").column("obj")
            .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(getOrgUnit())))
            .where(eq(property(SessionObject.yearDistributionPart().fromAlias("obj")), value(part)))
            .where(eq(property(SessionObject.educationYear().fromAlias("obj")), value(year)))
            .order(property(SessionObject.id().fromAlias("obj")));
        final List<SessionObject> list = dql.createStatement(_uiSupport.getSession()).<SessionObject>list();
        if (list.isEmpty())
            return null;
        if (list.size() != 1)
            throw new IllegalStateException();
        return list.get(0);
    }

    // getters and setters

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public ISelectModel getGroupModel()
    {
        return groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        this.groupModel = groupModel;
    }

    public EducationYear getYear()
    {
        return year;
    }

    public void setYear(EducationYear year)
    {
        this.year = year;
    }

    public YearDistributionPart getPart()
    {
        return part;
    }

    public void setPart(YearDistributionPart part)
    {
        this.part = part;
    }

    public ISelectModel getEduLevelModel()
    {
        return eduLevelModel;
    }

    public void setEduLevelModel(ISelectModel eduLevelModel)
    {
        this.eduLevelModel = eduLevelModel;
    }

    public boolean isStudentStatusInitialized()
    {
        return studentStatusInitialized;
    }

    public void setStudentStatusInitialized(boolean studentStatusInitialized)
    {
        this.studentStatusInitialized = studentStatusInitialized;
    }

    public List<EppFControlActionType> getEppFControlActionTypes() {
        return eppFControlActionTypes;
    }

    public void setEppFControlActionTypes(List<EppFControlActionType> eppFControlActionTypes) {
        this.eppFControlActionTypes = eppFControlActionTypes;
    }

    public Date getPerformDateFrom() {
        return performDateFrom;
    }

    public void setPerformDateFrom(Date performDateFrom) {
        this.performDateFrom = performDateFrom;
    }

    public Date getPerformDateTo() {
        return performDateTo;
    }

    public void setPerformDateTo(Date performDateTo) {
        this.performDateTo = performDateTo;
    }

    public boolean isPerformDateFromActive() {
        return performDateFromActive;
    }

    public void setPerformDateFromActive(boolean performDateFromActive) {
        this.performDateFromActive = performDateFromActive;
    }

    public boolean isPerformDateToActive() {
        return performDateToActive;
    }

    public void setPerformDateToActive(boolean performDateToActive) {
        this.performDateToActive = performDateToActive;
    }
}
