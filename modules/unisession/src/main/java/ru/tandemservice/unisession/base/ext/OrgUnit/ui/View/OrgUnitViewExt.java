/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unisession.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    @Autowired
    public OrgUnitView _orgUnitView;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        /**
         <component-tab 
         name="uniSessionOrgUnitSessionTab" label="Сессия"
         component-name="ru.tandemservice.unisession.component.orgunit.SessionTab" 
         secured-object="fast:model.orgUnit" permission-key="mvel:model.getPermissionKey('unisessionTabView_')"
         visible="ognl:@ru.tandemservice.unisession.dao.orgunit.ISessionOrgUnitDAO@instance.get().canShowSessionOrgUnitTab(model.orgUnit.id)" 
         before="abstractOrgUnit_OrgUnitSecSettings"
         />         */
        return tabPanelExtensionBuilder(_orgUnitView.orgUnitTabPanelExtPoint())
                .addTab(componentTab("uniSessionOrgUnitSessionTab", "ru.tandemservice.unisession.component.orgunit.SessionTab").permissionKey("ui:secModel.unisessionTabView").visible("ognl:@ru.tandemservice.unisession.dao.orgunit.ISessionOrgUnitDAO@instance.get().canShowSessionOrgUnitTab(presenter.orgUnit.id)").parameters("mvel:['orgUnitId':presenter.orgUnit.id]"))
                .create();
    }
}
