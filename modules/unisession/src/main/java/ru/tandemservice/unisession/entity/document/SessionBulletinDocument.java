package ru.tandemservice.unisession.entity.document;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.SessionMarkableDocHolder;
import ru.tandemservice.unisession.dao.bulletin.ISessionBulletinDAO;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.gen.SessionBulletinDocumentGen;
import ru.tandemservice.unisession.sec.ISessionSecDao;

import java.util.Collection;
import java.util.Date;

/**
 * Ведомость
 */
public class SessionBulletinDocument extends SessionBulletinDocumentGen implements SessionMarkableDocHolder.IMarkableDoc, SessionObject.ISessionObjectDependentDocument
{

    public SessionBulletinDocument() {}
    public SessionBulletinDocument(final SessionObject sessionObject, final EppRealEduGroup4ActionType group) {
        this.setSessionObject(sessionObject);
        this.setGroup(group);
    }


    @Override
    public INumberGenerationRule<SessionDocument> getNumberGenerationRule() {
        return ISessionBulletinDAO.instance.get().getNumberGenerationRule();
    }

    @Override
    @EntityDSLSupport(parts = "number")
    public String getTitle()
    {
        if (getGroup() == null) {
            return this.getClass().getSimpleName();
        }
        return "№" +this.getNumber() + " " + this.getGroup().getActivityPart().getTitleWithNumber() + ", " + StringUtils.uncapitalize(this.getGroup().getType().getTitle());
    }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeTitle()
    {
        return "Ведомость №" + this.getNumber();
    }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeShortTitle()
    {
        return "Вед. №" + this.getNumber();
    }

    @Override
    @EntityDSLSupport(parts = {SessionBulletinDocumentGen.L_GROUP + "." + EppRealEduGroup4ActionType.L_ACTIVITY_PART + "." + EppRegistryElement.P_TITLE, SessionBulletinDocumentGen.L_GROUP + "." + EppRealEduGroup4ActionType.L_TYPE + "." + EppControlActionType.P_TITLE})
    public String getRegistryElementTitle()
    {
        return this.getGroup().getActivityPart().getTitleWithNumber() + ", " + StringUtils.uncapitalize(this.getGroup().getType().getTitle());
    }

    public boolean isClosed()
    {
        return this.getCloseDate() != null;
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return ISessionSecDao.instance.get().getSecLocalEntities(this);
    }

    @Override
    public SessionMarkableDocHolder.IMarkableDocProperties getMarkingProperties() {
        return new SessionMarkableDocHolder.IMarkableDocProperties() {
            @Override
            public Date getPerformDate() {
                return SessionBulletinDocument.this.getPerformDate();
            }

            @Override
            public SessionComission getCommission() {
                return SessionBulletinDocument.this.getCommission();
            }

            @Override
            public EppRegistryElementPart getRegistryElementPart() {
                return SessionBulletinDocument.this.getGroup().getActivityPart();
            }

            @Override
            public EppFControlActionType getCAType() {
                return SessionBulletinDocument.this.getGroup().getActionType();
            }

            @Override
            public SessionObject getSessionObject() {
                return SessionBulletinDocument.this.getSessionObject();
            }
        };
    }

    @Override
    public OrgUnit getGroupOu()
    {
        return getSessionObject().getGroupOu();
    }
}