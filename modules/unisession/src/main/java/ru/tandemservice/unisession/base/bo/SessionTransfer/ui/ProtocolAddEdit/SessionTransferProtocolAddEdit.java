/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.unisession.base.bo.SessionTransfer.logic.SessionTransferProtocolWorkPlanDSHandler;
import ru.tandemservice.unisession.entity.mark.SessionALRequest;

import static org.tandemframework.hibsupport.dql.DQLExpressions.isNull;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexey Lopatin
 * @since 11.09.2015
 */
@Configuration
public class SessionTransferProtocolAddEdit extends BusinessComponentManager
{
    public static final String REQUEST_DS = "requestDS";
    public static final String WORK_PLAN_DS = "workPlanDS";
    public static final String PPS_DS = "ppsDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REQUEST_DS, requestDS()))
                .addDataSource(selectDS(WORK_PLAN_DS, workPlanDS()).addColumn(DevelopGridTerm.P_TERM_NUMBER).addColumn(DevelopGridTerm.L_PART).addColumn(EppWorkPlanBase.P_SHORT_TITLE))
                .addDataSource(selectDS(PPS_DS, ppsDS()).addColumn(PpsEntryByEmployeePost.P_TITLE))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> requestDS()
    {
        return new EntityComboDataSourceHandler(getName(), SessionALRequest.class)
                .where(SessionALRequest.student().id(), SessionTransferProtocolAddEditUI.PARAM_STUDENT_ID)
                .filter(SessionALRequest.requestDate())
                .order(SessionALRequest.requestDate());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> workPlanDS()
    {
        return new SessionTransferProtocolWorkPlanDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler ppsDS()
    {
        return new EntityComboDataSourceHandler(getName(), PpsEntryByEmployeePost.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(isNull(property(alias, PpsEntryByEmployeePost.removalDate())));
            }
        }
                .filter(PpsEntryByEmployeePost.person().identityCard().fullFio())
                .order(PpsEntryByEmployeePost.person().identityCard().fullFio());
    }
}
