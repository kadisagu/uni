/**
 *$Id$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport;

/**
 * @author Alexander Shaburov
 * @since 27.11.12
 */
@State
({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
})
public class AttestationReportTotalResultPubUI extends UIPresenter
{
    private Long _reportId;
    private SessionAttestationTotalResultReport _report;

    private CommonPostfixPermissionModelBase _sec;

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(SessionAttestationTotalResultReport.class, _reportId);
        _sec = _report.getOrgUnit() != null ? new OrgUnitSecModel(_report.getOrgUnit()) : new CommonPostfixPermissionModel(null);
    }

    // Listeners

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report.getId());
        deactivate();
    }

    public void onClickPrint()
    {
        byte[] content = _report.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(_report.getContent().getFilename()).document(content), true);
    }

    // Getters & Setters

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public SessionAttestationTotalResultReport getReport()
    {
        return _report;
    }

    public void setReport(SessionAttestationTotalResultReport report)
    {
        _report = report;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return _sec;
    }

    public void setSec(CommonPostfixPermissionModelBase sec)
    {
        _sec = sec;
    }

    public String getViewPermissionKey(){ return _sec.getPermission(getReport().getOrgUnit() != null ? "orgUnit_viewAttestationReportTotalResultList" : "attestationTotalResultReport"); }



    public String getDeleteStorableReportPermissionKey(){ return _sec.getPermission(getReport().getOrgUnit() != null ? "orgUnit_deleteAttestationReportTotalResultList" : "deleteSessionStorableReport"); }
}
