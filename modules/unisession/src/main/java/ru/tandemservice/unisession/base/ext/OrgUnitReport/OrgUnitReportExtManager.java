/* $Id:$ */
package ru.tandemservice.unisession.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.uniepp.util.GroupOrgUnitReportVisibleResolver;

/**
 * @author rsizonenko
 * @since 06.11.2015
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager {

    /** Код блока отчетов на подразделении "Отчеты модуля «Сессия»" **/
    public static final String UNISESSION_ORG_UNIT_REPORT_BLOCK = "unisessionOrgUnitStudentReportBlock";

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;


    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {

        final IItemListExtensionBuilder<OrgUnitReportBlockDefinition> builder = itemListExtension(_orgUnitReportManager.blockListExtPoint());

        return builder
                .add(UNISESSION_ORG_UNIT_REPORT_BLOCK, new OrgUnitReportBlockDefinition("Отчеты модуля «Сессия»", UNISESSION_ORG_UNIT_REPORT_BLOCK, new GroupOrgUnitReportVisibleResolver()))
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {

        final IItemListExtensionBuilder<OrgUnitReportDefinition> itemList = itemListExtension(_orgUnitReportManager.reportListExtPoint());

        return itemList
                .add("sessionSummaryBulletinReport", new OrgUnitReportDefinition("Сводная ведомость (на академ. группу)", "sessionSummaryBulletinReport", UNISESSION_ORG_UNIT_REPORT_BLOCK, "SessionReportSummaryBulletinList", "orgUnit_viewSessionReportSummaryBulletinList"))
                .add("sessionReportDebtors", new OrgUnitReportDefinition("Должники"                                     , "sessionReportDebtors", UNISESSION_ORG_UNIT_REPORT_BLOCK, "SessionReportDebtorsList", "orgUnit_viewSessionReportDebtorsList"))
                .add("sessionReportResults", new OrgUnitReportDefinition("Сведения об успеваемости студентов (по курсам, по группам, по направлениям)", "sessionReportResults", UNISESSION_ORG_UNIT_REPORT_BLOCK, "SessionReportResultsList", "orgUnit_viewSessionReportResultsList"))
                .add("sessionReportResultsByDisc", new OrgUnitReportDefinition("Сведения об успеваемости студентов (по дисциплинам)", "sessionReportResultsByDisc", UNISESSION_ORG_UNIT_REPORT_BLOCK, "SessionReportResultsByDiscList", "orgUnit_viewSessionReportResultsByDiscList"))
                .add("sessionReportGroupMarks", new OrgUnitReportDefinition("Сводная ведомость группы (по всем полученным студентами оценкам)", "sessionReportGroupMarks", UNISESSION_ORG_UNIT_REPORT_BLOCK, "SessionReportGroupMarksList", "orgUnit_viewSessionReportGroupMarksList"))
                .add("sessionReportGroupBulletinList", new OrgUnitReportDefinition("Сводные данные по ведомостям (на академ. группу)", "sessionReportGroupBulletinList", UNISESSION_ORG_UNIT_REPORT_BLOCK, "SessionReportGroupBulletinListList", "orgUnit_viewSessionReportGroupBulletinListList"))
                .add("attestationReportBulletinList", new OrgUnitReportDefinition("Аттестационная ведомость по дисциплине (на академ. группу)", "attestationReportBulletinList", UNISESSION_ORG_UNIT_REPORT_BLOCK, "AttestationReportBulletinList", "orgUnit_viewAttestationReportBulletinList"))
                .add("attestationReportResult", new OrgUnitReportDefinition("Сводная ведомость по межсессионной аттестации (на академ. группу)", "attestationReportResult", UNISESSION_ORG_UNIT_REPORT_BLOCK, "AttestationReportResultList", "orgUnit_viewAttestationReportResultList"))
                .add("attestationReportTotalResult", new OrgUnitReportDefinition("Итоги межсессионной аттестации студентов", "attestationReportTotalResult", UNISESSION_ORG_UNIT_REPORT_BLOCK, "AttestationReportTotalResultList", "orgUnit_viewAttestationReportTotalResultList"))
                .add("stateFinalExamResult", new OrgUnitReportDefinition("Результаты государственного экзамена", "stateFinalExamResult",  "uniOrgUnitStudentReportBlock", "SessionReportStateFinalExamResultList", "orgUnit_viewStateFinalExamResultList"))
                .add("finalQualWorkResult", new OrgUnitReportDefinition("Результаты защиты выпускной квалификационной работы", "finalQualWorkResult",  "uniOrgUnitStudentReportBlock", "SessionReportFinalQualWorkResultList", "orgUnit_viewFinalQualWorkResultList"))
                .add("stateFinalExamSummaryResult", new OrgUnitReportDefinition("Сводный отчет по результатам государственного экзамена", "stateFinalExamSummaryResult",  "uniOrgUnitStudentReportBlock", "SessionReportSfeSummaryResultList", "orgUnit_viewStateFinalExamSummaryResultList"))
                .add("finalQualWorkSummaryResult", new OrgUnitReportDefinition("Сводный отчет по результатам защит ВКР", "finalQualWorkSummaryResult",  "uniOrgUnitStudentReportBlock", "SessionReportFqwSummaryResultList", "orgUnit_viewFinalQualWorkSummaryResultList"))

                .add("sessionStudentsWithoutSessionSlotsGroupOuIndicator", new OrgUnitReportDefinition("Студенты, не включенные в документы сессии", "sessionStudentsWithoutSessionSlotsGroupOuIndicator", ru.tandemservice.uniepp.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK, "SessionIndicatorsStudentsWithoutSessionSlotsGroupOu"))
                .add("sessionStudentWithoutSessionBulletinSlots", new OrgUnitReportDefinition("Студенты, не включенные в атт. ведомости", "sessionStudentWithoutSessionBulletinSlots", ru.tandemservice.uniepp.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNIEPP_GROUP_ORG_UNIT_INDICATOR_BLOCK, "SessionIndicatorsStudentWithoutSessBullSlots"))
                .create();
    }
}
