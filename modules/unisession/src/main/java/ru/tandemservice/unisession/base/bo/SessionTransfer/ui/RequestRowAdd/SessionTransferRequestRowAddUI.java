/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.RequestRowAdd;

import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unisession.base.bo.SessionReexamination.SessionReexaminationManager;
import ru.tandemservice.unisession.base.bo.SessionReexamination.logic.SessionReexaminationRequestRowDSHandler;
import ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Pub.SessionReexaminationPubUI;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;
import ru.tandemservice.unisession.entity.mark.SessionALRequestRow;

import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * @author Alexey Lopatin
 * @since 22.09.2015
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "protocol.id")})
public class SessionTransferRequestRowAddUI extends UIPresenter
{
    public static final String PARAM_PROTOCOL = "protocol";

    private SessionTransferProtocolDocument _protocol = new SessionTransferProtocolDocument();

    @Override
    public void onComponentRefresh()
    {
        _protocol = DataAccessServices.dao().getNotNull(_protocol.getId());

        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(SessionTransferRequestRowAdd.REQUEST_ROW_DS);
        SessionReexaminationManager.instance().dao().doCreateControlActionColumns(_protocol.getRequest(), requestRowDS);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReexaminationPubUI.PARAM_REQUEST, _protocol.getRequest());
        dataSource.put(PARAM_PROTOCOL, _protocol);
    }

    public void onClickApply()
    {
        Collection<IEntity> selected = getConfig().<BaseSearchListDataSource>getDataSource(SessionTransferRequestRowAdd.REQUEST_ROW_DS).getOptionColumnSelectedObjects("check");
        if (selected.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одну строку из списка.");

        List<SessionALRequestRow> requestRows = Lists.newLinkedList();
        selected.forEach(item -> requestRows.add(((DataWrapper) item).getWrapped()));

        SessionTransferManager.instance().dao().saveProtocolRowAndMark(_protocol, requestRows);
        deactivate();
    }

    @SuppressWarnings("unchecked")
    public boolean isHasRegElementPartFControlAction()
    {
        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(SessionTransferRequestRowAdd.REQUEST_ROW_DS);
        DataWrapper wrapper = requestRowDS.getCurrent();
        String code = requestRowDS.getCurrentColumn().getName();

        Map<String, Boolean> hasRegElementPartFCAMap = (Map<String, Boolean>) wrapper.getProperty(SessionReexaminationRequestRowDSHandler.PROP_PART_TO_FCA_MAP);
        return hasRegElementPartFCAMap.get(code);
    }

    @SuppressWarnings("unchecked")
    public SessionMarkGradeValueCatalogItem getCurrentRequestRowMark()
    {
        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(SessionTransferRequestRowAdd.REQUEST_ROW_DS);
        DataWrapper wrapper = requestRowDS.getCurrent();
        String code = requestRowDS.getCurrentColumn().getName();

        Map<String, SessionMarkGradeValueCatalogItem> reExamMarkMap = (Map<String, SessionMarkGradeValueCatalogItem>) wrapper.getProperty(SessionReexaminationRequestRowDSHandler.PROP_RE_EXAM_MARK_MAP);
        return reExamMarkMap.get(code);
    }

    public SessionALRequestRow getCurrentRequestRow()
    {
        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(SessionTransferRequestRowAdd.REQUEST_ROW_DS);
        return ((DataWrapper) requestRowDS.getCurrent()).getWrapped();
    }

    public SessionTransferProtocolDocument getProtocol()
    {
        return _protocol;
    }

    public void setProtocol(SessionTransferProtocolDocument protocol)
    {
        _protocol = protocol;
    }
}
