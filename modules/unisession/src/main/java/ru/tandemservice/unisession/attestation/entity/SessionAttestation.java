package ru.tandemservice.unisession.attestation.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.attestation.entity.gen.SessionAttestationGen;
import ru.tandemservice.unisession.sec.ISessionSecDao;

import java.util.Collection;

/**
 * Аттестация
 *
 * Межсессионная аттестация - проводится перед конкретной сессией (в конкретном семестре) и имеет порядковый номер проведения.
 */
public class SessionAttestation extends SessionAttestationGen implements ITitled, ISecLocalEntityOwner
{
    @Override
    @EntityDSLSupport(parts = "number")
    public String getTitle()
    {
        if (getSessionObject() == null) {
            return this.getClass().getSimpleName();
        }
        return "Межсессионная аттестация №" + getNumber() + " " + getSessionObject().getTermTitle();
    }

    @Override
    @EntityDSLSupport(parts = "number")
    public String getShortTitle()
    {
        return "№" + getNumber() + " " + getSessionObject().getTermTitle();
    }

    public String getTypeTitle()
    {
        return "Аттестация №" + getNumber();
    }

    public OrgUnit getOrgUnit()
    {
        return getSessionObject().getOrgUnit();
    }

    public boolean isClosed()
    {
        return null != getCloseDate();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return ISessionSecDao.instance.get().getSecLocalEntities(this);
    }
}