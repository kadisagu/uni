/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import ru.tandemservice.unisession.base.bo.SessionReport.util.ISessionReportResultsBaseParams;

/**
* @author oleyba
* @since 2/19/12
*/
public interface ISessionReportResultsParams extends ISessionReportResultsBaseParams
{
    boolean isUseFullEduLevelTitle();
}
