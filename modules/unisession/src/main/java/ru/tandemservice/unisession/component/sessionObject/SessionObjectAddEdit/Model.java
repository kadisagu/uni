// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.sessionObject.SessionObjectAddEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.unisession.entity.document.SessionObject;

/**
 * @author oleyba
 * @since 2/8/11
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="orgUnitId"),
    @Bind(key="eduYearId", binding="eduYearId"),
    @Bind(key="id", binding="sessionObject.id")
})
@Output({@Bind(key="sessionObjectId", binding="sessionObject.id")})
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    private Long orgUnitId;
    private Long eduYearId;
    private SessionObject sessionObject = new SessionObject();

    private ISelectModel yearModel;
    private List<HSelectOption> partModel;

    public Long getOrgUnitId()
    {
        return this.orgUnitId;
    }

    public void setOrgUnitId(final Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }

    public Long getEduYearId()
    {
        return this.eduYearId;
    }

    public void setEduYearId(final Long eduYearId)
    {
        this.eduYearId = eduYearId;
    }

    public SessionObject getSessionObject()
    {
        return this.sessionObject;
    }

    public void setSessionObject(final SessionObject sessionObject)
    {
        this.sessionObject = sessionObject;
    }

    public ISelectModel getYearModel()
    {
        return this.yearModel;
    }

    public void setYearModel(final ISelectModel yearModel)
    {
        this.yearModel = yearModel;
    }

    public List<HSelectOption> getPartModel()
    {
        return this.partModel;
    }

    public void setPartModel(final List<HSelectOption> partModel)
    {
        this.partModel = partModel;
    }
}
