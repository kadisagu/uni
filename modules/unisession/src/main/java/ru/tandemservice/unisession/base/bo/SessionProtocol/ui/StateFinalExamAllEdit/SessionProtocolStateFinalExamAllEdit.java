/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.StateFinalExamAllEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.base.bo.SessionProtocol.SessionProtocolManager;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.AbstractAllEdit.SessionProtocolAbstractAllEdit;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
@Configuration
public class SessionProtocolStateFinalExamAllEdit extends SessionProtocolAbstractAllEdit
{
    public static final String COMMISSION_MEMBERS_DS = "commissionMembersDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(STATE_FINAL_EXAM_PROTOCOL_DS, stateFinalExamProtocolDSColumnExtPoint(),
                                            SessionProtocolManager.instance().stateFinalExamProtocolDSHandler()))
                .addDataSource(selectDS(COMMISSION_MEMBERS_DS, commissionMembersDSHandler()).addColumn(PpsEntry.fio().s()))
                .create();
    }

    @Bean
    public ColumnListExtPoint stateFinalExamProtocolDSColumnExtPoint()
    {
        IColumnListExtPointBuilder builder = columnListExtPointBuilder(STATE_FINAL_EXAM_PROTOCOL_DS);
        addProtocolListColumns(builder);
        return builder.create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> commissionMembersDSHandler()
    {
        EntityComboDataSourceHandler handler = PpsEntry.defaultSelectDSHandler(getName());

        handler.customize((alias, dql, context, filter) -> {
            SessionBulletinDocument bulletin = context.get(BULLETIN_PARAM);
            dql.where(exists(SessionComissionPps.class,
                             SessionComissionPps.pps().s(), property(alias),
                             SessionComissionPps.commission().s(), bulletin.getCommission()));
            return dql;
        });

        return handler;
    }
}