/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsByDiscList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.unisession.entity.report.UnisessionResultsByDiscReport;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/19/12
 */
public class SessionReportResultsByDiscDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_ORG_UNIT = "orgUnit";
    public static final String PARAM_EDU_YEAR = "eduYear";
    public static final String PARAM_YEAR_PART = "yearPart";

    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    public SessionReportResultsByDiscDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        final OrgUnit orgUnit = context.get(PARAM_ORG_UNIT);
        final EducationYear eduYear = context.get(PARAM_EDU_YEAR);
        final YearDistributionPart yearPart = context.get(PARAM_YEAR_PART);
        if (null == eduYear || null == yearPart) {
            return ListOutputBuilder.get(dsInput, Collections.emptyList()).build();
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(UnisessionResultsByDiscReport.class, "r")
            .where(eq(property(UnisessionResultsByDiscReport.educationYear().fromAlias("r")), value(eduYear)))
            .where(eq(property(UnisessionResultsByDiscReport.yearDistributionPart().fromAlias("r")), value(yearPart)))
            ;

        if (null != orgUnit)
            dql.where(eq(property(UnisessionResultsByDiscReport.orgUnit().fromAlias("r")), value(orgUnit)));
        else
            dql.where(isNull(property(UnisessionResultsByDiscReport.orgUnit().fromAlias("r"))));

        registry.applyOrder(dql, dsInput.getEntityOrder());

        return DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
    }

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(UnisessionResultsByDiscReport.class, "r");
    }
}