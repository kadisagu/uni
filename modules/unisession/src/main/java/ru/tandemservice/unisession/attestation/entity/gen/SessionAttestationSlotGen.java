package ru.tandemservice.unisession.attestation.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись студента в атт. ведомости
 *
 * Студент, включенный в атт. ведомость для получения отметки о прохождении аттестации.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionAttestationSlotGen extends EntityBase
 implements INaturalIdentifiable<SessionAttestationSlotGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot";
    public static final String ENTITY_NAME = "sessionAttestationSlot";
    public static final int VERSION_HASH = -1013565165;
    private static IEntityMeta ENTITY_META;

    public static final String L_BULLETIN = "bulletin";
    public static final String L_STUDENT_WPE = "studentWpe";
    public static final String L_MARK = "mark";
    public static final String P_FIXED_RATING_AS_LONG = "fixedRatingAsLong";
    public static final String P_FIXED_RATING = "fixedRating";

    private SessionAttestationBulletin _bulletin;     // Аттестационная ведомость
    private EppStudentWorkPlanElement _studentWpe;     // МСРП
    private SessionAttestationMarkCatalogItem _mark;     // Оценка текущего контроля
    private Long _fixedRatingAsLong;     // Рейтинг по дисциплине на момент прохождения аттестации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Аттестационная ведомость. Свойство не может быть null.
     */
    @NotNull
    public SessionAttestationBulletin getBulletin()
    {
        return _bulletin;
    }

    /**
     * @param bulletin Аттестационная ведомость. Свойство не может быть null.
     */
    public void setBulletin(SessionAttestationBulletin bulletin)
    {
        dirty(_bulletin, bulletin);
        _bulletin = bulletin;
    }

    /**
     * @return МСРП. Свойство не может быть null.
     */
    @NotNull
    public EppStudentWorkPlanElement getStudentWpe()
    {
        return _studentWpe;
    }

    /**
     * @param studentWpe МСРП. Свойство не может быть null.
     */
    public void setStudentWpe(EppStudentWorkPlanElement studentWpe)
    {
        dirty(_studentWpe, studentWpe);
        _studentWpe = studentWpe;
    }

    /**
     * @return Оценка текущего контроля.
     */
    public SessionAttestationMarkCatalogItem getMark()
    {
        return _mark;
    }

    /**
     * @param mark Оценка текущего контроля.
     */
    public void setMark(SessionAttestationMarkCatalogItem mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    /**
     * Если используется БРС, то при выставлении аттестации может фиксироваться текущий рейтинг по дисциплине,
     * на основании которого и выставлена отметка о прохождении аттестации.
     * Если БРС нет, или она этого не предусматривает, то рейтинг может не фиксироваться.
     * Значение сохраняется, как long, и приводится к double смещением на два порядка.
     *
     * @return Рейтинг по дисциплине на момент прохождения аттестации.
     */
    public Long getFixedRatingAsLong()
    {
        return _fixedRatingAsLong;
    }

    /**
     * @param fixedRatingAsLong Рейтинг по дисциплине на момент прохождения аттестации.
     */
    public void setFixedRatingAsLong(Long fixedRatingAsLong)
    {
        dirty(_fixedRatingAsLong, fixedRatingAsLong);
        _fixedRatingAsLong = fixedRatingAsLong;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionAttestationSlotGen)
        {
            if (withNaturalIdProperties)
            {
                setBulletin(((SessionAttestationSlot)another).getBulletin());
                setStudentWpe(((SessionAttestationSlot)another).getStudentWpe());
            }
            setMark(((SessionAttestationSlot)another).getMark());
            setFixedRatingAsLong(((SessionAttestationSlot)another).getFixedRatingAsLong());
        }
    }

    public INaturalId<SessionAttestationSlotGen> getNaturalId()
    {
        return new NaturalId(getBulletin(), getStudentWpe());
    }

    public static class NaturalId extends NaturalIdBase<SessionAttestationSlotGen>
    {
        private static final String PROXY_NAME = "SessionAttestationSlotNaturalProxy";

        private Long _bulletin;
        private Long _studentWpe;

        public NaturalId()
        {}

        public NaturalId(SessionAttestationBulletin bulletin, EppStudentWorkPlanElement studentWpe)
        {
            _bulletin = ((IEntity) bulletin).getId();
            _studentWpe = ((IEntity) studentWpe).getId();
        }

        public Long getBulletin()
        {
            return _bulletin;
        }

        public void setBulletin(Long bulletin)
        {
            _bulletin = bulletin;
        }

        public Long getStudentWpe()
        {
            return _studentWpe;
        }

        public void setStudentWpe(Long studentWpe)
        {
            _studentWpe = studentWpe;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionAttestationSlotGen.NaturalId) ) return false;

            SessionAttestationSlotGen.NaturalId that = (NaturalId) o;

            if( !equals(getBulletin(), that.getBulletin()) ) return false;
            if( !equals(getStudentWpe(), that.getStudentWpe()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getBulletin());
            result = hashCode(result, getStudentWpe());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getBulletin());
            sb.append("/");
            sb.append(getStudentWpe());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionAttestationSlotGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionAttestationSlot.class;
        }

        public T newInstance()
        {
            return (T) new SessionAttestationSlot();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "bulletin":
                    return obj.getBulletin();
                case "studentWpe":
                    return obj.getStudentWpe();
                case "mark":
                    return obj.getMark();
                case "fixedRatingAsLong":
                    return obj.getFixedRatingAsLong();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "bulletin":
                    obj.setBulletin((SessionAttestationBulletin) value);
                    return;
                case "studentWpe":
                    obj.setStudentWpe((EppStudentWorkPlanElement) value);
                    return;
                case "mark":
                    obj.setMark((SessionAttestationMarkCatalogItem) value);
                    return;
                case "fixedRatingAsLong":
                    obj.setFixedRatingAsLong((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "bulletin":
                        return true;
                case "studentWpe":
                        return true;
                case "mark":
                        return true;
                case "fixedRatingAsLong":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "bulletin":
                    return true;
                case "studentWpe":
                    return true;
                case "mark":
                    return true;
                case "fixedRatingAsLong":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "bulletin":
                    return SessionAttestationBulletin.class;
                case "studentWpe":
                    return EppStudentWorkPlanElement.class;
                case "mark":
                    return SessionAttestationMarkCatalogItem.class;
                case "fixedRatingAsLong":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionAttestationSlot> _dslPath = new Path<SessionAttestationSlot>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionAttestationSlot");
    }
            

    /**
     * @return Аттестационная ведомость. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot#getBulletin()
     */
    public static SessionAttestationBulletin.Path<SessionAttestationBulletin> bulletin()
    {
        return _dslPath.bulletin();
    }

    /**
     * @return МСРП. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot#getStudentWpe()
     */
    public static EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> studentWpe()
    {
        return _dslPath.studentWpe();
    }

    /**
     * @return Оценка текущего контроля.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot#getMark()
     */
    public static SessionAttestationMarkCatalogItem.Path<SessionAttestationMarkCatalogItem> mark()
    {
        return _dslPath.mark();
    }

    /**
     * Если используется БРС, то при выставлении аттестации может фиксироваться текущий рейтинг по дисциплине,
     * на основании которого и выставлена отметка о прохождении аттестации.
     * Если БРС нет, или она этого не предусматривает, то рейтинг может не фиксироваться.
     * Значение сохраняется, как long, и приводится к double смещением на два порядка.
     *
     * @return Рейтинг по дисциплине на момент прохождения аттестации.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot#getFixedRatingAsLong()
     */
    public static PropertyPath<Long> fixedRatingAsLong()
    {
        return _dslPath.fixedRatingAsLong();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot#getFixedRating()
     */
    public static SupportedPropertyPath<Double> fixedRating()
    {
        return _dslPath.fixedRating();
    }

    public static class Path<E extends SessionAttestationSlot> extends EntityPath<E>
    {
        private SessionAttestationBulletin.Path<SessionAttestationBulletin> _bulletin;
        private EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> _studentWpe;
        private SessionAttestationMarkCatalogItem.Path<SessionAttestationMarkCatalogItem> _mark;
        private PropertyPath<Long> _fixedRatingAsLong;
        private SupportedPropertyPath<Double> _fixedRating;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Аттестационная ведомость. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot#getBulletin()
     */
        public SessionAttestationBulletin.Path<SessionAttestationBulletin> bulletin()
        {
            if(_bulletin == null )
                _bulletin = new SessionAttestationBulletin.Path<SessionAttestationBulletin>(L_BULLETIN, this);
            return _bulletin;
        }

    /**
     * @return МСРП. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot#getStudentWpe()
     */
        public EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> studentWpe()
        {
            if(_studentWpe == null )
                _studentWpe = new EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement>(L_STUDENT_WPE, this);
            return _studentWpe;
        }

    /**
     * @return Оценка текущего контроля.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot#getMark()
     */
        public SessionAttestationMarkCatalogItem.Path<SessionAttestationMarkCatalogItem> mark()
        {
            if(_mark == null )
                _mark = new SessionAttestationMarkCatalogItem.Path<SessionAttestationMarkCatalogItem>(L_MARK, this);
            return _mark;
        }

    /**
     * Если используется БРС, то при выставлении аттестации может фиксироваться текущий рейтинг по дисциплине,
     * на основании которого и выставлена отметка о прохождении аттестации.
     * Если БРС нет, или она этого не предусматривает, то рейтинг может не фиксироваться.
     * Значение сохраняется, как long, и приводится к double смещением на два порядка.
     *
     * @return Рейтинг по дисциплине на момент прохождения аттестации.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot#getFixedRatingAsLong()
     */
        public PropertyPath<Long> fixedRatingAsLong()
        {
            if(_fixedRatingAsLong == null )
                _fixedRatingAsLong = new PropertyPath<Long>(SessionAttestationSlotGen.P_FIXED_RATING_AS_LONG, this);
            return _fixedRatingAsLong;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot#getFixedRating()
     */
        public SupportedPropertyPath<Double> fixedRating()
        {
            if(_fixedRating == null )
                _fixedRating = new SupportedPropertyPath<Double>(SessionAttestationSlotGen.P_FIXED_RATING, this);
            return _fixedRating;
        }

        public Class getEntityClass()
        {
            return SessionAttestationSlot.class;
        }

        public String getEntityName()
        {
            return "sessionAttestationSlot";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getFixedRating();
}
