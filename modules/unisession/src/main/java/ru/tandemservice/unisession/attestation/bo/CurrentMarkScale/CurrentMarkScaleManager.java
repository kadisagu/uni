/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.CurrentMarkScale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.logic.CurrentMarkScaleDAO;
import ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.logic.ICurrentMarkScaleDAO;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2014
 */
@Configuration
public class CurrentMarkScaleManager extends BusinessObjectManager
{
    public static CurrentMarkScaleManager instance()
    {
        return instance(CurrentMarkScaleManager.class);
    }

    @Bean
    public ICurrentMarkScaleDAO dao()
    {
        return new CurrentMarkScaleDAO();
    }

}