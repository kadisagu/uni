/* $Id$ */
package ru.tandemservice.unisession.component.orgunit.SessionRetakeDocTab;

import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionRetakeDoc.ui.Add.SessionRetakeDocAdd;
import ru.tandemservice.unisession.base.bo.SessionRetakeDoc.ui.Add.SessionRetakeDocAddUI;
import ru.tandemservice.unisession.dao.retakeDoc.ISessionRetakeDocDAO;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.print.ISessionRetakeDocPrintDAO;

import java.util.List;

/**
 * @author oleyba
 * @since 6/13/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final String settingsKey = "session.retakeDocList." + model.getOrgUnitId();
        model.setSettings(UniBaseUtils.getDataSettings(component, settingsKey));

        this.getDao().prepare(model);

        final DynamicListDataSource<SessionRetakeDocument> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().refreshDataSource(Controller.this.getModel(component1));
        });

        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new SimpleColumn("Дата\n формирования", SessionRetakeDocument.formingDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Ведомость", "titleWithCA"));
        dataSource.addColumn(new MultiValuesColumn("Группа\n (текущая)", "groups").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Числo\n студентов", "studentCount").setClickable(false).setOrderable(false).setWidth(1));
        dataSource.addColumn(new MultiValuesColumn("Преподаватель", "comission").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setOrderable(false).setWidth(20));
        dataSource.addColumn(new SimpleColumn("Дисциплина", SessionRetakeDocument.registryElementPart().titleWithNumber()).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Дата\n закрытия", SessionRetakeDocument.closeDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new ActionColumn("Выставить оценки", "edit_mark", "onClickMark").setPermissionKey(model.getSec().getPermission("markSessionRetakeDoc")));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey(model.getSec().getPermission("printSessionRetakeDoc")));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey(model.getSec().getPermission("editSessionRetakeDoc")));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить ведомость №{0}?", SessionRetakeDocument.number().s()).setPermissionKey(model.getSec().getPermission("deleteSessionRetakeDoc")));
        model.setDataSource(dataSource);
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onClickAddBulletins(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        CAFLegacySupportService.asDesktopRoot(SessionRetakeDocAdd.class.getSimpleName())
                .parameter(SessionReportManager.BIND_ORG_UNIT, model.getOrgUnitId())
                .parameter(SessionRetakeDocAddUI.PARAM_YEAR, model.getYear())
                .parameter(SessionRetakeDocAddUI.PARAM_YEAR_PART, model.getPart())
                .activate();
    }

    public void onClickAutoCreateBulletins(final IBusinessComponent component)
    {
        // WTF: почему не на самой форме?
        getDao().doRefreshFinalMarks();

        final Model model = this.getModel(component);
        final ParametersMap params = new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getOrgUnitId());
        SessionObject sessionObject = getDao().getSessionObject(model);

        if (sessionObject != null) {
            params.add("sessionObject", sessionObject.getId());
        }

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocAutocreate.Model.COMPONENT_NAME,
                params)
        );
    }

    public void onClickMark(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(
                                          ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model.COMPONENT_NAME,
                                          new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.<Long>getListenerParameter()))
        );
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocEdit.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.<Long>getListenerParameter())));
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        UniDaoFacade.getCoreDao().delete(component.<Long>getListenerParameter());
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        this.getDao().doPrintRetakeDoc(component.<Long>getListenerParameter());
    }

    public void onClickPrintSelected(final IBusinessComponent component)
    {
        final List<Long> selectedIds = UniBaseUtils.getIdList(((CheckboxColumn) this.getModel(component).getDataSource().getColumn("select")).getSelectedObjects());
        if (selectedIds.isEmpty()) {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }
        final RtfDocument document = ISessionRetakeDocPrintDAO.instance.get().printDocList(selectedIds);
        final byte[] content = RtfUtil.toByteArray(document);
        final Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Ведомость.rtf");
        this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", temporaryId).add("zip", Boolean.FALSE)));
    }

    public void onClickDeleteSelected(final IBusinessComponent component)
    {
        for (final Long id : this.getSelectedIds(component)) {
            UniDaoFacade.getCoreDao().delete(id);
        }
    }

    public void onSearchParamsChange(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        this.getDao().prepare(model);
    }

    public void onClickJoinSelected(final IBusinessComponent component)
    {
        ISessionRetakeDocDAO.instance.get().doJoin(this.getModel(component).getOrgUnitId(), this.getSelectedIds(component));
    }

    public void onClickSplitSelectedByGroups(final IBusinessComponent component)
    {
        ISessionRetakeDocDAO.instance.get().doSplit(this.getModel(component).getOrgUnitId(), this.getSelectedIds(component), SessionDocumentSlot.studentWpeCAction().studentWpe().student().group().id());
    }

    public void onClickSplitSelectedByCompensation(final IBusinessComponent component)
    {
        ISessionRetakeDocDAO.instance.get().doSplit(this.getModel(component).getOrgUnitId(), this.getSelectedIds(component), SessionDocumentSlot.studentWpeCAction().studentWpe().student().compensationType().id());
    }

    public void onClickSplitSelectedByComission(final IBusinessComponent component)
    {
        ISessionRetakeDocDAO.instance.get().doSplitByComission(this.getModel(component).getOrgUnitId(), this.getSelectedIds(component));
    }

    private List<Long> getSelectedIds(final IBusinessComponent component)
    {
        final List<Long> selectedIds = UniBaseUtils.getIdList(((CheckboxColumn) this.getModel(component).getDataSource().getColumn("select")).getSelectedObjects());
        if (selectedIds.isEmpty()) {
            throw new ApplicationException("Не выбраны ведомости.");
        }
        return selectedIds;
    }
}
