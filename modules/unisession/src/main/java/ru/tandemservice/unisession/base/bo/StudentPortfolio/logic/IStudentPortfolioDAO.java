package ru.tandemservice.unisession.base.bo.StudentPortfolio.logic;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.catalog.StudentPortfolioElement;

/**
 * @author avedernikov
 * @since 11.12.2015
 */

public interface IStudentPortfolioDAO extends ICommonDAO
{
	/**
	 * Добавляет в базу достижения по МСРП-ФК, на которые еще не ссылается ни одно достижение
	 * @param student Студент, для которого добавляем достижения
	 */
	void addAchievementsByWpeCActions(Student student);

	/**
	 * Добавить или обновить файл, прикрепленный к достижению студента
	 * @param element Достижение студента, к которому должен быть прикреплен файл
	 * @param loadFileId Текущий id прикрепленного файла (null, если файла еще нет)
	 * @param file Файл для заргузки на сервер
	 * @return id добавленного/обновленного файла
	 */
	Long createOrUpdateAchievementAttachement(StudentPortfolioElement element, Long loadFileId, IUploadFile file);

	/**
	 * Загрузить файл, приложенный к достижению студента
	 * @param achievementId id достижения
	 * @return Вложенный файл
	 */
	RemoteDocumentDTO getAchievementAttachement(Long achievementId);

	/**
	 * Удалить файл, приложенный к достижению студента
	 * @param achievementId id достижения
	 */
	void removeAttachementFromAchievement(Long achievementId);
}
