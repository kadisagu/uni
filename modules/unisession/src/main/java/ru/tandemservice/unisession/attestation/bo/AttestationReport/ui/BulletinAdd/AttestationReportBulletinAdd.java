/* $Id: SessionReportSummaryBulletinAdd.java 21825 2012-02-09 09:08:57Z oleyba $ */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.BulletinAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.BaseRegistryElementPartDSHandler;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.AttestationReportManager;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniAttestationFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/29/11
 */
@Configuration
public class AttestationReportBulletinAdd extends BusinessComponentManager
{
    public static final String PARAM_COURSE = AttestationReportManager.PARAM_COURSE;
    public static final String PARAM_GROUP = "group";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), UniAttestationFilterAddon.class))
            .addDataSource(AttestationReportManager.instance().attestationDSConfig())
            .addDataSource(disciplineDSConfig())
            .create();
    }

    @Bean
    public UIDataSourceConfig disciplineDSConfig()
    {
        return SelectDSConfig.with("disciplineDS", this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.disciplineDSHandler())
        .addColumn(EppRegistryElementPart.titleWithNumber().s())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> disciplineDSHandler()
    {
        return new BaseRegistryElementPartDSHandler(this.getName())
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder bulletinDQL = new DQLSelectBuilder()
                    .fromEntity(SessionAttestationSlot.class, "s")
                    .column(property(SessionAttestationSlot.bulletin().registryElementPart().id().fromAlias("s")))
                    .where(eq(property(SessionAttestationSlot.bulletin().attestation().fromAlias("s")), commonValue(context.get(AttestationReportManager.PARAM_ATTESTATION))))
                    .where(eq(property(SessionAttestationSlot.studentWpe().student().group().fromAlias("s")), commonValue(context.get(PARAM_GROUP))))
                    ;

                dql.where(in(property(EppRegistryElementPart.id().fromAlias(alias)), bulletinDQL.buildQuery()));
            }
        };
    }
}