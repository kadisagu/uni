package ru.tandemservice.unisession.entity.document;

import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.unisession.entity.document.gen.SessionTransferProtocolMarkGen;

/** @see ru.tandemservice.unisession.entity.document.gen.SessionTransferProtocolMarkGen */
public class SessionTransferProtocolMark extends SessionTransferProtocolMarkGen
{
    public SessionTransferProtocolMark()
    {
    }

    public SessionTransferProtocolMark(SessionTransferProtocolRow protocolRow, EppFControlActionType controlAction)
    {
        setProtocolRow(protocolRow);
        setControlAction(controlAction);
    }
}