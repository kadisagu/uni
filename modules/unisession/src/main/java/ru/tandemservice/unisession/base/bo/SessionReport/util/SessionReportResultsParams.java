/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.unisession.base.bo.SessionReport.util.results.ISessionReportResultsParams;
import ru.tandemservice.unisession.base.bo.SessionReport.util.resultsByDisc.ISessionReportResultsByDiscParams;

import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 2/8/12
 */
public class SessionReportResultsParams implements ISessionReportResultsParams, ISessionReportResultsByDiscParams
{
    private OrgUnit orgUnit;

    private YearDistributionPart yearPart;
    private List<OrgUnit> groupOrgUnitList;

    private EducationYear educationYear;

    private List<EppWorkPlanRowKind> discKindList;

    private boolean territorialOrgUnitActive;
    private List<OrgUnit> territorialOrgUnitList;

    private boolean _developFormActive;
    private List<DevelopForm> _developFormList;

    private boolean _developConditionActive;
    private List<DevelopCondition> _developConditionList;

    private boolean _developTechActive;
    private List<DevelopTech> _developTechList;

    private boolean _developPeriodActive;
    private List<DevelopPeriod> _developPeriodList;

    private boolean _courseActive;
    private List<Course> _courseList;

    private boolean _compensationTypeActive;
    private CompensationType _compensationType;

    private boolean _studentStatusActive;
    private List<StudentStatus> _studentStatusList;

    private boolean useFullEduLevelTitle;
    private boolean useFullDiscTitle;

    private boolean deadlineCheckActive;
    private Date deadlineCheck;

    private boolean targetAdmission;

    @Override
    public boolean isTerritorialOrgUnitActive()
    {
        return territorialOrgUnitActive;
    }

    @Override
    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return territorialOrgUnitList;
    }

    @Override
    public OrgUnit getOrgUnit()
    {
        return orgUnit;
    }

    @Override
    public YearDistributionPart getYearPart()
    {
        return yearPart;
    }

    @Override
    public List<OrgUnit> getGroupOrgUnitList()
    {
        return groupOrgUnitList;
    }

    @Override
    public List<EppWorkPlanRowKind> getDiscKindList()
    {
        return discKindList;
    }

    @Override
    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    @Override
    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    @Override
    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    @Override
    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    @Override
    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    @Override
    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    @Override
    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    @Override
    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    @Override
    public boolean isCourseActive()
    {
        return _courseActive;
    }

    @Override
    public List<Course> getCourseList()
    {
        return _courseList;
    }

    @Override
    public boolean isCompensationTypeActive()
    {
        return _compensationTypeActive;
    }

    @Override
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    @Override
    public boolean isStudentStatusActive()
    {
        return _studentStatusActive;
    }

    @Override
    public List<StudentStatus> getStudentStatusList()
    {
        return _studentStatusList;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this.orgUnit = orgUnit;
    }

    public void setYearPart(YearDistributionPart yearPart)
    {
        this.yearPart = yearPart;
    }

    public void setGroupOrgUnitList(List<OrgUnit> groupOrgUnitList)
    {
        this.groupOrgUnitList = groupOrgUnitList;
    }

    public void setDiscKindList(List<EppWorkPlanRowKind> discKindList)
    {
        this.discKindList= discKindList;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public void setCourseActive(boolean courseActive)
    {
        _courseActive = courseActive;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public void setCompensationTypeActive(boolean compensationTypeActive)
    {
        _compensationTypeActive = compensationTypeActive;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    public void setStudentStatusActive(boolean studentStatusActive)
    {
        _studentStatusActive = studentStatusActive;
    }

    public void setStudentStatusList(List<StudentStatus> studentStatusList)
    {
        _studentStatusList = studentStatusList;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        this.territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        this.territorialOrgUnitList = territorialOrgUnitList;
    }

    @Override
    public boolean isUseFullEduLevelTitle()
    {
        return useFullEduLevelTitle;
    }

    public void setUseFullEduLevelTitle(boolean useFullEduLevelTitle)
    {
        this.useFullEduLevelTitle = useFullEduLevelTitle;
    }

    @Override
    public boolean isUseFullDiscTitle()
    {
        return useFullDiscTitle;
    }

    public void setUseFullDiscTitle(boolean useFullDiscTitle)
    {
        this.useFullDiscTitle = useFullDiscTitle;
    }

    @Override
    public Date getDeadlineCheck()
    {
        return deadlineCheck;
    }

    public void setDeadlineCheck(Date deadlineCheck)
    {
        this.deadlineCheck = deadlineCheck;
    }

    @Override
    public boolean isDeadlineCheckActive()
    {
        return deadlineCheckActive;
    }

    public void setDeadlineCheckActive(boolean deadlineCheckActive)
    {
        this.deadlineCheckActive = deadlineCheckActive;
    }

    public EducationYear getEducationYear()
    {
        return educationYear;
    }

    public void setEducationYear(EducationYear educationYear)
    {
        this.educationYear = educationYear;
    }

    public boolean isTargetAdmission()
    {
        return targetAdmission;
    }

    public void setTargetAdmission(boolean targetAdmission)
    {
        this.targetAdmission = targetAdmission;
    }
}
