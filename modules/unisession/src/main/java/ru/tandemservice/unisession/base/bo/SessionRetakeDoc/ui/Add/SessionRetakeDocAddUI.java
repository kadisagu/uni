package ru.tandemservice.unisession.base.bo.SessionRetakeDoc.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterFactory;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.base.bo.SessionBulletin.ui.Add.SessionBulletinAdd;
import ru.tandemservice.unisession.base.bo.SessionDocument.util.SessionRealEduGroupFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.dao.retakeDoc.ISessionRetakeDocDAO;
import ru.tandemservice.unisession.dao.sessionObject.ISessionObjectDAO;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.Collection;

/**
 * @author avedernikov
 * @since 08.02.2016
 */
@State({
		@Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "holder.id", required = true),
		@Bind(key = SessionRetakeDocAddUI.PARAM_YEAR, binding = "eduYear"),
		@Bind(key = SessionRetakeDocAddUI.PARAM_YEAR_PART, binding = "yearPart")
})
public class SessionRetakeDocAddUI extends UIPresenter
{
	public static final String PARAM_YEAR = "year";
	public static final String PARAM_YEAR_PART = "part";

	public static final String SELECT_COLUMN = "select";

	private OrgUnitHolder _holder = new OrgUnitHolder();
	private EducationYear _eduYear;
	private YearDistributionPart _yearPart;

	private boolean _inSession = false;

	@Override
	public void onComponentRefresh()
	{
		getHolder().refresh();
		configUtil(getFilterAddon());
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case SessionRetakeDocAdd.EDU_GROUP_DS:
			{
				dataSource.put(AttestationManager.PARAM_ORG_UNIT, getOrgUnit());
				dataSource.put(AttestationManager.PARAM_EDU_YEAR, getEduYear());
				dataSource.put(AttestationManager.PARAM_YEAR_PART, getYearPart());
				dataSource.put(SessionRealEduGroupFilterAddon.UTIL_NAME, getFilterAddon());
				break;
			}
			case SessionReportManager.DS_EDU_YEAR:
			{
				dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
				break;
			}
			case SessionReportManager.DS_YEAR_PART:
			{
				dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
				dataSource.put(SessionReportManager.PARAM_EDU_YEAR, getEduYear());
				break;
			}
		}
	}

	public void onChangeYearOrPart()
	{
		configUtilWhere(getFilterAddon());
	}

	public void onClickShow()
	{
		configUtilWhere(getFilterAddon());
	}

	public void onClickClear()
	{
		getFilterAddon().clearSettings();
	}

	public void onClickApply()
	{
		Collection<IEntity> selected = getConfig().<BaseSearchListDataSource>getDataSource(SessionBulletinAdd.EDU_GROUP_DS).getOptionColumnSelectedObjects(SELECT_COLUMN);
		if (selected.isEmpty())
			throw new ApplicationException("Необходимо выбрать хотя бы одну УГС из списка.");

		SessionObject sessionObject = ISessionObjectDAO.instance.get().getSessionObject(getOrgUnit(), getEduYear(), getYearPart());
		ISessionRetakeDocDAO.instance.get().doAddDocumentsForEduGroups(CommonBaseEntityUtil.getIdList(selected), sessionObject, _inSession);
		selected.clear();
	}

	public SessionRealEduGroupFilterAddon getFilterAddon()
	{
		SessionObject sessionObject = ISessionObjectDAO.instance.get().getSessionObject(getOrgUnit(), getEduYear(), getYearPart());
		return ((SessionRealEduGroupFilterAddon) getConfig().getAddon(SessionRetakeDocAdd.EDU_GROUP_FILTER_ADDON))
				.customFilter((dql, alias, item, presenter) -> ISessionRetakeDocDAO.instance.get().filterGroupsNeedRetake(dql, alias, sessionObject));
	}

	private void configUtil(SessionRealEduGroupFilterAddon util)
	{
		util
				.configDoubleWidthFilters(false)
				.configSettings(getSettingsKey());

		util.clearFilterItems();

		ICommonFilterFactory<IEntity> aLoadType = config -> new SessionRealEduGroupFilterAddon.LoadTypeFilterItem(config, "Форма контроля", EppGroupTypeFCA.class);

		util
				.addFilterItem(SessionRealEduGroupFilterAddon.DEVELOP_FORM, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.DEVELOP_CONDITION, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.EDU_LEVEL, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.TERRITORIAL_ORG_UNIT, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.COURSE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.GROUP, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.DISCIPLINE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
				.addFilterItem(aLoadType, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG);

		util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_DEVELOP_FORM).setNextWithNewLine(false);
		util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_EDU_LEVEL).setDoubleWidth(true).setColSpan(2);
		util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_TERRITORIAL_ORG_UNIT).setDoubleWidth(true).setColSpan(2);
		util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_FORMATIVE_ORG_UNIT).setDoubleWidth(true).setColSpan(2);
		util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_COURSE).setNextWithNewLine(false);
		util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_DISCIPLINE).setDoubleWidth(true).setColSpan(2);

		configUtilWhere(util);
	}

	public void configUtilWhere(SessionRealEduGroupFilterAddon util)
	{
		util.clearWhereFilter();
		util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppRealEduGroupRow.studentWpePart().studentWpe().student().status().active(), true));

		if (null != getOrgUnit())
			util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppRealEduGroupRow.studentWpePart().studentWpe().student().educationOrgUnit().groupOrgUnit(), getOrgUnit()));
		if (null != _eduYear)
			util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppRealEduGroupRow.group().summary().yearPart().year().educationYear(), _eduYear));
		if (null != _yearPart)
			util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppRealEduGroupRow.group().summary().yearPart().part(), _yearPart));
	}

	public OrgUnit getOrgUnit()
	{
		return getHolder().getValue();
	}

	public String getSticker()
	{
		return getConfig().getProperty("ui.title", getOrgUnit().getFullTitle());
	}

	public String getCheckBoxHint()
	{
		return getConfig().getProperty("ui.checkBoxHint");
	}

	public OrgUnitHolder getHolder()
	{
		return _holder;
	}

	public void setHolder(OrgUnitHolder _holder)
	{
		this._holder = _holder;
	}

	public EducationYear getEduYear()
	{
		return _eduYear;
	}

	public void setEduYear(EducationYear _eduYear)
	{
		this._eduYear = _eduYear;
	}

	public YearDistributionPart getYearPart()
	{
		return _yearPart;
	}

	public void setYearPart(YearDistributionPart _yearPart)
	{
		this._yearPart = _yearPart;
	}

	public boolean isInSession()
	{
		return _inSession;
	}

	public void setInSession(boolean _inSession)
	{
		this._inSession = _inSession;
	}
}