package ru.tandemservice.unisession.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Должники»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisessionDebtorsReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport";
    public static final String ENTITY_NAME = "unisessionDebtorsReport";
    public static final int VERSION_HASH = 1636270456;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_EXECUTOR = "executor";
    public static final String L_SESSION_OBJECT = "sessionObject";
    public static final String P_PERFORM_DATE_FROM = "performDateFrom";
    public static final String P_PERFORM_DATE_TO = "performDateTo";
    public static final String P_COMPENSATION_TYPE = "compensationType";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String P_EDUCATION_LEVEL_HIGH_SCHOOL = "educationLevelHighSchool";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION = "developCondition";
    public static final String P_DEVELOP_TECH = "developTech";
    public static final String P_DEVELOP_PERIOD = "developPeriod";
    public static final String P_COURSE = "course";
    public static final String P_GROUPS = "groups";
    public static final String P_STUDENT_STATUS = "studentStatus";
    public static final String P_CONTROL_ACTION_TYPES = "controlActionTypes";
    public static final String P_REGISTRY_STRUCTURE = "registryStructure";
    public static final String P_TARGET_ADMISSION = "targetAdmission";
    public static final String P_CUSTOM_STATE = "customState";
    public static final String P_FORMING_DATE_STR = "formingDateStr";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private String _executor;     // Исполнитель
    private SessionObject _sessionObject;     // Сессия
    private Date _performDateFrom;     // Дата сдачи мероприятия в ведомости с
    private Date _performDateTo;     // Дата сдачи мероприятия в ведомости по
    private String _compensationType;     // Вид возмещения затрат
    private String _formativeOrgUnit;     // Формирующее подр.
    private String _territorialOrgUnit;     // Территориальное подр.
    private String _educationLevelHighSchool;     // Направления подготовки (специальности)
    private String _developForm;     // Форма освоения
    private String _developCondition;     // Условие освоения
    private String _developTech;     // Технология освоения
    private String _developPeriod;     // Срок освоения
    private String _course;     // Курс
    private String _groups;     // Группы
    private String _studentStatus;     // Состояние студента
    private String _controlActionTypes;     // Форма контроля
    private String _registryStructure;     // Вид мероприятия реестра
    private String _targetAdmission;     // Целевой прием
    private String _customState;     // Дополнительный статус

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    /**
     * @return Сессия. Свойство не может быть null.
     */
    @NotNull
    public SessionObject getSessionObject()
    {
        return _sessionObject;
    }

    /**
     * @param sessionObject Сессия. Свойство не может быть null.
     */
    public void setSessionObject(SessionObject sessionObject)
    {
        dirty(_sessionObject, sessionObject);
        _sessionObject = sessionObject;
    }

    /**
     * @return Дата сдачи мероприятия в ведомости с.
     */
    public Date getPerformDateFrom()
    {
        return _performDateFrom;
    }

    /**
     * @param performDateFrom Дата сдачи мероприятия в ведомости с.
     */
    public void setPerformDateFrom(Date performDateFrom)
    {
        dirty(_performDateFrom, performDateFrom);
        _performDateFrom = performDateFrom;
    }

    /**
     * @return Дата сдачи мероприятия в ведомости по.
     */
    public Date getPerformDateTo()
    {
        return _performDateTo;
    }

    /**
     * @param performDateTo Дата сдачи мероприятия в ведомости по.
     */
    public void setPerformDateTo(Date performDateTo)
    {
        dirty(_performDateTo, performDateTo);
        _performDateTo = performDateTo;
    }

    /**
     * @return Вид возмещения затрат.
     */
    @Length(max=255)
    public String getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(String compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подр..
     */
    public void setTerritorialOrgUnit(String territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Направления подготовки (специальности).
     */
    public String getEducationLevelHighSchool()
    {
        return _educationLevelHighSchool;
    }

    /**
     * @param educationLevelHighSchool Направления подготовки (специальности).
     */
    public void setEducationLevelHighSchool(String educationLevelHighSchool)
    {
        dirty(_educationLevelHighSchool, educationLevelHighSchool);
        _educationLevelHighSchool = educationLevelHighSchool;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(String developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения.
     */
    public void setDevelopPeriod(String developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группы.
     */
    public String getGroups()
    {
        return _groups;
    }

    /**
     * @param groups Группы.
     */
    public void setGroups(String groups)
    {
        dirty(_groups, groups);
        _groups = groups;
    }

    /**
     * @return Состояние студента.
     */
    @Length(max=255)
    public String getStudentStatus()
    {
        return _studentStatus;
    }

    /**
     * @param studentStatus Состояние студента.
     */
    public void setStudentStatus(String studentStatus)
    {
        dirty(_studentStatus, studentStatus);
        _studentStatus = studentStatus;
    }

    /**
     * @return Форма контроля.
     */
    public String getControlActionTypes()
    {
        return _controlActionTypes;
    }

    /**
     * @param controlActionTypes Форма контроля.
     */
    public void setControlActionTypes(String controlActionTypes)
    {
        dirty(_controlActionTypes, controlActionTypes);
        _controlActionTypes = controlActionTypes;
    }

    /**
     * @return Вид мероприятия реестра.
     */
    public String getRegistryStructure()
    {
        return _registryStructure;
    }

    /**
     * @param registryStructure Вид мероприятия реестра.
     */
    public void setRegistryStructure(String registryStructure)
    {
        dirty(_registryStructure, registryStructure);
        _registryStructure = registryStructure;
    }

    /**
     * @return Целевой прием.
     */
    @Length(max=255)
    public String getTargetAdmission()
    {
        return _targetAdmission;
    }

    /**
     * @param targetAdmission Целевой прием.
     */
    public void setTargetAdmission(String targetAdmission)
    {
        dirty(_targetAdmission, targetAdmission);
        _targetAdmission = targetAdmission;
    }

    /**
     * @return Дополнительный статус.
     */
    public String getCustomState()
    {
        return _customState;
    }

    /**
     * @param customState Дополнительный статус.
     */
    public void setCustomState(String customState)
    {
        dirty(_customState, customState);
        _customState = customState;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UnisessionDebtorsReportGen)
        {
            setContent(((UnisessionDebtorsReport)another).getContent());
            setFormingDate(((UnisessionDebtorsReport)another).getFormingDate());
            setExecutor(((UnisessionDebtorsReport)another).getExecutor());
            setSessionObject(((UnisessionDebtorsReport)another).getSessionObject());
            setPerformDateFrom(((UnisessionDebtorsReport)another).getPerformDateFrom());
            setPerformDateTo(((UnisessionDebtorsReport)another).getPerformDateTo());
            setCompensationType(((UnisessionDebtorsReport)another).getCompensationType());
            setFormativeOrgUnit(((UnisessionDebtorsReport)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((UnisessionDebtorsReport)another).getTerritorialOrgUnit());
            setEducationLevelHighSchool(((UnisessionDebtorsReport)another).getEducationLevelHighSchool());
            setDevelopForm(((UnisessionDebtorsReport)another).getDevelopForm());
            setDevelopCondition(((UnisessionDebtorsReport)another).getDevelopCondition());
            setDevelopTech(((UnisessionDebtorsReport)another).getDevelopTech());
            setDevelopPeriod(((UnisessionDebtorsReport)another).getDevelopPeriod());
            setCourse(((UnisessionDebtorsReport)another).getCourse());
            setGroups(((UnisessionDebtorsReport)another).getGroups());
            setStudentStatus(((UnisessionDebtorsReport)another).getStudentStatus());
            setControlActionTypes(((UnisessionDebtorsReport)another).getControlActionTypes());
            setRegistryStructure(((UnisessionDebtorsReport)another).getRegistryStructure());
            setTargetAdmission(((UnisessionDebtorsReport)another).getTargetAdmission());
            setCustomState(((UnisessionDebtorsReport)another).getCustomState());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisessionDebtorsReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisessionDebtorsReport.class;
        }

        public T newInstance()
        {
            return (T) new UnisessionDebtorsReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "executor":
                    return obj.getExecutor();
                case "sessionObject":
                    return obj.getSessionObject();
                case "performDateFrom":
                    return obj.getPerformDateFrom();
                case "performDateTo":
                    return obj.getPerformDateTo();
                case "compensationType":
                    return obj.getCompensationType();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "educationLevelHighSchool":
                    return obj.getEducationLevelHighSchool();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "course":
                    return obj.getCourse();
                case "groups":
                    return obj.getGroups();
                case "studentStatus":
                    return obj.getStudentStatus();
                case "controlActionTypes":
                    return obj.getControlActionTypes();
                case "registryStructure":
                    return obj.getRegistryStructure();
                case "targetAdmission":
                    return obj.getTargetAdmission();
                case "customState":
                    return obj.getCustomState();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
                case "sessionObject":
                    obj.setSessionObject((SessionObject) value);
                    return;
                case "performDateFrom":
                    obj.setPerformDateFrom((Date) value);
                    return;
                case "performDateTo":
                    obj.setPerformDateTo((Date) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((String) value);
                    return;
                case "educationLevelHighSchool":
                    obj.setEducationLevelHighSchool((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((String) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "groups":
                    obj.setGroups((String) value);
                    return;
                case "studentStatus":
                    obj.setStudentStatus((String) value);
                    return;
                case "controlActionTypes":
                    obj.setControlActionTypes((String) value);
                    return;
                case "registryStructure":
                    obj.setRegistryStructure((String) value);
                    return;
                case "targetAdmission":
                    obj.setTargetAdmission((String) value);
                    return;
                case "customState":
                    obj.setCustomState((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "executor":
                        return true;
                case "sessionObject":
                        return true;
                case "performDateFrom":
                        return true;
                case "performDateTo":
                        return true;
                case "compensationType":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "educationLevelHighSchool":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "course":
                        return true;
                case "groups":
                        return true;
                case "studentStatus":
                        return true;
                case "controlActionTypes":
                        return true;
                case "registryStructure":
                        return true;
                case "targetAdmission":
                        return true;
                case "customState":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "executor":
                    return true;
                case "sessionObject":
                    return true;
                case "performDateFrom":
                    return true;
                case "performDateTo":
                    return true;
                case "compensationType":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "educationLevelHighSchool":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "course":
                    return true;
                case "groups":
                    return true;
                case "studentStatus":
                    return true;
                case "controlActionTypes":
                    return true;
                case "registryStructure":
                    return true;
                case "targetAdmission":
                    return true;
                case "customState":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "executor":
                    return String.class;
                case "sessionObject":
                    return SessionObject.class;
                case "performDateFrom":
                    return Date.class;
                case "performDateTo":
                    return Date.class;
                case "compensationType":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "territorialOrgUnit":
                    return String.class;
                case "educationLevelHighSchool":
                    return String.class;
                case "developForm":
                    return String.class;
                case "developCondition":
                    return String.class;
                case "developTech":
                    return String.class;
                case "developPeriod":
                    return String.class;
                case "course":
                    return String.class;
                case "groups":
                    return String.class;
                case "studentStatus":
                    return String.class;
                case "controlActionTypes":
                    return String.class;
                case "registryStructure":
                    return String.class;
                case "targetAdmission":
                    return String.class;
                case "customState":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UnisessionDebtorsReport> _dslPath = new Path<UnisessionDebtorsReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisessionDebtorsReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getSessionObject()
     */
    public static SessionObject.Path<SessionObject> sessionObject()
    {
        return _dslPath.sessionObject();
    }

    /**
     * @return Дата сдачи мероприятия в ведомости с.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getPerformDateFrom()
     */
    public static PropertyPath<Date> performDateFrom()
    {
        return _dslPath.performDateFrom();
    }

    /**
     * @return Дата сдачи мероприятия в ведомости по.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getPerformDateTo()
     */
    public static PropertyPath<Date> performDateTo()
    {
        return _dslPath.performDateTo();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getCompensationType()
     */
    public static PropertyPath<String> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getTerritorialOrgUnit()
     */
    public static PropertyPath<String> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Направления подготовки (специальности).
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getEducationLevelHighSchool()
     */
    public static PropertyPath<String> educationLevelHighSchool()
    {
        return _dslPath.educationLevelHighSchool();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getDevelopTech()
     */
    public static PropertyPath<String> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getDevelopPeriod()
     */
    public static PropertyPath<String> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группы.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getGroups()
     */
    public static PropertyPath<String> groups()
    {
        return _dslPath.groups();
    }

    /**
     * @return Состояние студента.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getStudentStatus()
     */
    public static PropertyPath<String> studentStatus()
    {
        return _dslPath.studentStatus();
    }

    /**
     * @return Форма контроля.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getControlActionTypes()
     */
    public static PropertyPath<String> controlActionTypes()
    {
        return _dslPath.controlActionTypes();
    }

    /**
     * @return Вид мероприятия реестра.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getRegistryStructure()
     */
    public static PropertyPath<String> registryStructure()
    {
        return _dslPath.registryStructure();
    }

    /**
     * @return Целевой прием.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getTargetAdmission()
     */
    public static PropertyPath<String> targetAdmission()
    {
        return _dslPath.targetAdmission();
    }

    /**
     * @return Дополнительный статус.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getCustomState()
     */
    public static PropertyPath<String> customState()
    {
        return _dslPath.customState();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getFormingDateStr()
     */
    public static SupportedPropertyPath<String> formingDateStr()
    {
        return _dslPath.formingDateStr();
    }

    public static class Path<E extends UnisessionDebtorsReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<String> _executor;
        private SessionObject.Path<SessionObject> _sessionObject;
        private PropertyPath<Date> _performDateFrom;
        private PropertyPath<Date> _performDateTo;
        private PropertyPath<String> _compensationType;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _territorialOrgUnit;
        private PropertyPath<String> _educationLevelHighSchool;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _developCondition;
        private PropertyPath<String> _developTech;
        private PropertyPath<String> _developPeriod;
        private PropertyPath<String> _course;
        private PropertyPath<String> _groups;
        private PropertyPath<String> _studentStatus;
        private PropertyPath<String> _controlActionTypes;
        private PropertyPath<String> _registryStructure;
        private PropertyPath<String> _targetAdmission;
        private PropertyPath<String> _customState;
        private SupportedPropertyPath<String> _formingDateStr;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(UnisessionDebtorsReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(UnisessionDebtorsReportGen.P_EXECUTOR, this);
            return _executor;
        }

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getSessionObject()
     */
        public SessionObject.Path<SessionObject> sessionObject()
        {
            if(_sessionObject == null )
                _sessionObject = new SessionObject.Path<SessionObject>(L_SESSION_OBJECT, this);
            return _sessionObject;
        }

    /**
     * @return Дата сдачи мероприятия в ведомости с.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getPerformDateFrom()
     */
        public PropertyPath<Date> performDateFrom()
        {
            if(_performDateFrom == null )
                _performDateFrom = new PropertyPath<Date>(UnisessionDebtorsReportGen.P_PERFORM_DATE_FROM, this);
            return _performDateFrom;
        }

    /**
     * @return Дата сдачи мероприятия в ведомости по.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getPerformDateTo()
     */
        public PropertyPath<Date> performDateTo()
        {
            if(_performDateTo == null )
                _performDateTo = new PropertyPath<Date>(UnisessionDebtorsReportGen.P_PERFORM_DATE_TO, this);
            return _performDateTo;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getCompensationType()
     */
        public PropertyPath<String> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new PropertyPath<String>(UnisessionDebtorsReportGen.P_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(UnisessionDebtorsReportGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getTerritorialOrgUnit()
     */
        public PropertyPath<String> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new PropertyPath<String>(UnisessionDebtorsReportGen.P_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Направления подготовки (специальности).
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getEducationLevelHighSchool()
     */
        public PropertyPath<String> educationLevelHighSchool()
        {
            if(_educationLevelHighSchool == null )
                _educationLevelHighSchool = new PropertyPath<String>(UnisessionDebtorsReportGen.P_EDUCATION_LEVEL_HIGH_SCHOOL, this);
            return _educationLevelHighSchool;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(UnisessionDebtorsReportGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(UnisessionDebtorsReportGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getDevelopTech()
     */
        public PropertyPath<String> developTech()
        {
            if(_developTech == null )
                _developTech = new PropertyPath<String>(UnisessionDebtorsReportGen.P_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getDevelopPeriod()
     */
        public PropertyPath<String> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new PropertyPath<String>(UnisessionDebtorsReportGen.P_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(UnisessionDebtorsReportGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Группы.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getGroups()
     */
        public PropertyPath<String> groups()
        {
            if(_groups == null )
                _groups = new PropertyPath<String>(UnisessionDebtorsReportGen.P_GROUPS, this);
            return _groups;
        }

    /**
     * @return Состояние студента.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getStudentStatus()
     */
        public PropertyPath<String> studentStatus()
        {
            if(_studentStatus == null )
                _studentStatus = new PropertyPath<String>(UnisessionDebtorsReportGen.P_STUDENT_STATUS, this);
            return _studentStatus;
        }

    /**
     * @return Форма контроля.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getControlActionTypes()
     */
        public PropertyPath<String> controlActionTypes()
        {
            if(_controlActionTypes == null )
                _controlActionTypes = new PropertyPath<String>(UnisessionDebtorsReportGen.P_CONTROL_ACTION_TYPES, this);
            return _controlActionTypes;
        }

    /**
     * @return Вид мероприятия реестра.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getRegistryStructure()
     */
        public PropertyPath<String> registryStructure()
        {
            if(_registryStructure == null )
                _registryStructure = new PropertyPath<String>(UnisessionDebtorsReportGen.P_REGISTRY_STRUCTURE, this);
            return _registryStructure;
        }

    /**
     * @return Целевой прием.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getTargetAdmission()
     */
        public PropertyPath<String> targetAdmission()
        {
            if(_targetAdmission == null )
                _targetAdmission = new PropertyPath<String>(UnisessionDebtorsReportGen.P_TARGET_ADMISSION, this);
            return _targetAdmission;
        }

    /**
     * @return Дополнительный статус.
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getCustomState()
     */
        public PropertyPath<String> customState()
        {
            if(_customState == null )
                _customState = new PropertyPath<String>(UnisessionDebtorsReportGen.P_CUSTOM_STATE, this);
            return _customState;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport#getFormingDateStr()
     */
        public SupportedPropertyPath<String> formingDateStr()
        {
            if(_formingDateStr == null )
                _formingDateStr = new SupportedPropertyPath<String>(UnisessionDebtorsReportGen.P_FORMING_DATE_STR, this);
            return _formingDateStr;
        }

        public Class getEntityClass()
        {
            return UnisessionDebtorsReport.class;
        }

        public String getEntityName()
        {
            return "unisessionDebtorsReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFormingDateStr();
}
