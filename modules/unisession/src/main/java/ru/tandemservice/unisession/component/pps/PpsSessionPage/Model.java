/* $Id$ */
package ru.tandemservice.unisession.component.pps.PpsSessionPage;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.sec.IPrincipalContext;

import org.tandemframework.shared.person.base.entity.Person;

/**
 * @author oleyba
 * @since 4/21/11
 */
@State({
    @Bind(key = "selectedTab", binding = "selectedTab")
})
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();

    private IPrincipalContext _principalContext;
    private Person person;
    private boolean ppsFound;
    private String selectedTab;

    public IPrincipalContext getPrincipalContext()
    {
        return this._principalContext;
    }

    public void setPrincipalContext(final IPrincipalContext principalContext)
    {
        this._principalContext = principalContext;
    }

    public Person getPerson()
    {
        return this.person;
    }

    public void setPerson(final Person person)
    {
        this.person = person;
    }

    public String getSelectedTab()
    {
        return this.selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    public boolean isPpsFound()
    {
        return this.ppsFound;
    }

    public void setPpsFound(final boolean ppsFound)
    {
        this.ppsFound = ppsFound;
    }
}
