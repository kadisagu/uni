/* $Id$ */
package ru.tandemservice.unisession.migration;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Andrey Andreev
 * @since 07.12.2015
 */
@SuppressWarnings("unused")
public class MS_unisession_2x9x3_0to1 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.3")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if( tool.hasResultRows("select id from accessmatrix_t where permissionkey_p=?", "themeSessionBulletin") ) {
            tool.debug("'themeSessionBulletin' key already exists in accessmatrix_t");
            return;
        }

        final String funcPrefix = tool.getDataSource().getSqlFunctionPrefix();

        final int n = tool.executeUpdate("insert into accessmatrix_t (id, discriminator, role_id, permissionkey_p) " +
                                   " select " + funcPrefix + "createIdFromNumber(? + (row_number() over(order by m.id)), m.discriminator), " +
                                   "       m.discriminator," +
                                   "       m.role_id," +
                                   "       'themeSessionBulletin' " +
                                   " from accessmatrix_t m where permissionkey_p=?",
                           System.currentTimeMillis(), "markSessionBulletin");

        tool.debug(n + " new rows inserted to accessmatrix_t");
    }
}