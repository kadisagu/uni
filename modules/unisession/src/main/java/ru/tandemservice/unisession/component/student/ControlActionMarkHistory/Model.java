// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.student.ControlActionMarkHistory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolPub.SessionTransferProtocolPub;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionGlobalDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 2/9/11
 */
@Input({
    @Bind(key="controlActionId", binding="controlAction.id"),
    @Bind(key="plaintext", binding="plaintext"),
    @Bind(key="showAsSingle", binding="showAsSingle")
})
public class Model
{
    private Boolean plaintext = Boolean.FALSE;
    private Boolean showAsSingle = Boolean.FALSE;
    private Boolean dialogMode;
    private boolean empty;
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    private EppStudentWpeCAction controlAction = new EppStudentWpeCAction();

//    private DynamicListDataSource<SessionDocumentSlot> dataSource;
    private List<SessionDocumentSlot> data = Lists.newArrayList();
    private SessionDocumentSlot _current;
    private Integer _currentIndex;
    private Map<Long, SessionMark> _markMap = Maps.newHashMap();

    private String totalMark;
    private String totalSessionMark;

    private List<String> lineList;
    private String currentLine;

    public boolean isShowHeader()
    {
        return this.showAsSingle && !this.dialogMode;
    }

    public boolean isShowSearchListHeader()
    {
        return (this.showAsSingle && !this.dialogMode) || !this.showAsSingle;
    }

    public boolean isShowCloseButton()
    {
        return this.showAsSingle;
    }

    public EppStudentWpeCAction getControlAction()
    {
        return this.controlAction;
    }

    public void setControlAction(final EppStudentWpeCAction controlAction)
    {
        this.controlAction = controlAction;
    }

    public List<SessionDocumentSlot> getData()
    {
        return data;
    }

    public SessionDocumentSlot getCurrent()
    {
        return _current;
    }

    public void setCurrent(SessionDocumentSlot current)
    {
        _current = current;
    }

    public Integer getCurrentIndex()
    {
        return _currentIndex;
    }

    public void setCurrentIndex(Integer currentIndex)
    {
        _currentIndex = currentIndex;
    }

    public Map<Long, SessionMark> getMarkMap()
    {
        return _markMap;
    }

    public List<String> getLineList()
    {
        return this.lineList;
    }

    public void setLineList(final List<String> lineList)
    {
        this.lineList = lineList;
    }

    public Boolean getPlaintext()
    {
        return this.plaintext;
    }

    public void setPlaintext(final Boolean plaintext)
    {
        this.plaintext = plaintext;
    }

    public String getCurrentLine()
    {
        return this.currentLine;
    }

    public void setCurrentLine(final String currentLine)
    {
        this.currentLine = currentLine;
    }

    public boolean isEmpty()
    {
        return this.empty;
    }

    public void setEmpty(final boolean empty)
    {
        this.empty = empty;
    }

    public Boolean getShowAsSingle()
    {
        return this.showAsSingle;
    }

    public void setShowAsSingle(final Boolean showAsSingle)
    {
        this.showAsSingle = showAsSingle;
    }

    public Boolean getDialogMode()
    {
        return this.dialogMode;
    }

    public void setDialogMode(final Boolean dialogMode)
    {
        this.dialogMode = dialogMode;
    }

    public String getTotalMark()
    {
        return this.totalMark;
    }

    public void setTotalMark(final String totalMark)
    {
        this.totalMark = totalMark;
    }

    public String getTotalSessionMark()
    {
        return this.totalSessionMark;
    }

    public void setTotalSessionMark(final String totalSessionMark)
    {
        this.totalSessionMark = totalSessionMark;
    }


    public String getCurrentComponent()
    {
        SessionDocument document = getCurrent().getDocument();
        if(document instanceof SessionGlobalDocument)
        {
            return ru.tandemservice.uni.component.group.GroupPub.Model.class.getPackage().getName();
        }
        else if(document instanceof SessionTransferProtocolDocument)
            return SessionTransferProtocolPub.class.getSimpleName();
        else
            return "";
    }

    public Object getCurrentParameters()
    {
        SessionDocument document = getCurrent().getDocument();
        if(document instanceof SessionGlobalDocument)
        {
            return new ParametersMap()
                    .add(PublisherActivator.PUBLISHER_ID_KEY, getControlAction().getStudentWpe().getStudent().getGroup().getId())
                    .add("selectedTabId", "sessionGroupJournalTab");
        }
        else
            return document.getId();
    }

    public SessionMark getCurrentMark()
    {
        return getMarkMap().get(getCurrent().getId());
    }

    public boolean isDebugEnabled()
    {
        return Debug.isDisplay();
    }

    public String getCurrentMarkModificationDate()
    {
        return getCurrentMark() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(getCurrentMark().getModificationDate()) : null;
    }

    public String getCurrentMarkPerformDate()
    {
        return getCurrentMark() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(getCurrentMark().getPerformDate()) : null;
    }

    public String getCurrentInSession()
    {
        return YesNoFormatter.IGNORE_NUMBERS.format(getCurrent().isInSession());
    }
    public String getCurrentDocumentFormingDate()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getCurrent().getDocument().getFormingDate());
    }

    public Map<String, Object> getParametersForStudentPub()
    {
        return new ParametersMap().add("selectedDataTab", "studentDataTab")
                .add("selectedStudentTab", "studentNewSessionTab").add(PublisherActivator.PUBLISHER_ID_KEY, getControlAction().getStudentWpe().getStudent().getId());
    }
}
