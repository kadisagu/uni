/* $Id: SessionReportGroupBulletinListAddUI.java 21962 2012-02-19 13:57:27Z oleyba $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupBulletinListAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWorkPlanRowKindCodes;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupBulletinListPub.SessionReportGroupBulletinListPub;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/29/11
 */

@Input
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id", required = true)
})
public class SessionReportGroupBulletinListAddUI extends UIPresenter
{
    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    private boolean useFullDiscTitle;

    private Date performDateFrom;
    private Date performDateTo;

    private boolean performDateFromActive;
    private boolean performDateToActive;

    private EducationYear year;
    private YearDistributionPart part;

    private SessionObject sessionObject;

    @Override
    public void onComponentActivate()
    {
        getOuHolder().refresh();
    }

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();

        if (null == getYear())
            setYear(IUniBaseDao.instance.get().get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE));

        configUtil(getSessionFilterAddon());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, getYear());
    }




    public void onClickApply()
    {
        validate();
        setSessionObject();
        final ISessionGroupBulletinListPrintDAO dao = ISessionGroupBulletinListPrintDAO.instance.get();
        UnisessionGroupBulletinListReport report = dao.createStoredReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(SessionReportGroupBulletinListPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }


    public void doNothing()
    {
    }

    // utils

    public void validate()
    {
        if (performDateFromActive && performDateToActive && (performDateFrom.getTime() - performDateTo.getTime() > 0))
            _uiSupport.error("Дата, указанная в параметре \"Дата сдачи мероприятия в ведомости с\" не должна быть позже даты в параметре \"Дата сдачи мероприятия в ведомости по\".", "dateFrom");
        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    private void setSessionObject()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionObject.class, "obj").column("obj")
                .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(SessionReportGroupBulletinListAddUI.this.getOrgUnit())))
                .where(eq(property(SessionObject.yearDistributionPart().fromAlias("obj")), value(part)))
                .where(eq(property(SessionObject.educationYear().fromAlias("obj")), value(year)))
                .order(property(SessionObject.id().fromAlias("obj")));
        final List<SessionObject> list = dql.createStatement(_uiSupport.getSession()).list();
        if (list.isEmpty())
            return;
        if (list.size() != 1)
            throw new IllegalStateException();
        sessionObject = list.get(0);
    }

    // getters and setters

    public EducationYear getYear()
    {
        return year;
    }

    public void setYear(EducationYear year)
    {
        this.year = year;
    }

    public YearDistributionPart getPart()
    {
        return part;
    }

    public void setPart(YearDistributionPart part)
    {
        this.part = part;
    }

    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    public boolean isUseFullDiscTitle()
    {
        return useFullDiscTitle;
    }

    public void setUseFullDiscTitle(boolean useFullDiscTitle)
    {
        this.useFullDiscTitle = useFullDiscTitle;
    }

    public boolean isPerformDateToActive() {
        return performDateToActive;
    }

    public void setPerformDateToActive(boolean performDateToActive) {
        this.performDateToActive = performDateToActive;
    }

    public boolean isPerformDateFromActive() {
        return performDateFromActive;
    }

    public void setPerformDateFromActive(boolean performDateFromActive) {
        this.performDateFromActive = performDateFromActive;
    }

    public Date getPerformDateTo() {
        return performDateTo;
    }

    public void setPerformDateTo(Date performDateTo) {
        this.performDateTo = performDateTo;
    }

    public Date getPerformDateFrom() {
        return performDateFrom;
    }

    public void setPerformDateFrom(Date performDateFrom) {
        this.performDateFrom = performDateFrom;
    }

    public SessionObject getSessionObject() {
        return sessionObject;
    }

    // for filter addon

    public UniSessionFilterAddon getSessionFilterAddon()
    {
        return (UniSessionFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
    }

    public void onChangeYearOrPart()
    {
        configUtilWhere(getSessionFilterAddon());
    }

    private void configUtil(UniSessionFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());


        util.clearFilterItems();

        util

                .addFilterItem(UniSessionFilterAddon.DISC_KIND, new CommonFilterFormConfig(true, true, true, false, true, true))
                .addFilterItem(UniSessionFilterAddon.REGISTRY_STRUCTURE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.CONTROL_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(UniSessionFilterAddon.COURSE, new CommonFilterFormConfig(true, true, true, false, true, true))
                .addFilterItem(UniSessionFilterAddon.GROUP_WITH_NO_GROUP_ALT_TITLE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                ;

        final EppWorkPlanRowKind main = DataAccessServices.dao().getByCode(EppWorkPlanRowKind.class, EppWorkPlanRowKindCodes.MAIN);
        final List<EppWorkPlanRowKind> value = new ArrayList<>();
        value.add(main);
        util.getFilterItem(UniSessionFilterAddon.SETTINGS_NAME_DISC_KIND).setValue(value);
        util.saveSettings();
        configUtilWhere(util);
    }

    public void configUtilWhere(UniSessionFilterAddon util)
    {
        util.clearWhereFilter();

        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().status().active(), true));
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().educationOrgUnit().formativeOrgUnit(), getOrgUnit()));

        if (null != year) util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().year().educationYear(), year));
        if (null != part) util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().part(), part));
    }
}
