package ru.tandemservice.unisession.entity.mark.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisession.entity.mark.SessionMarkRatingData;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные рейтинга для оценки студента в сессии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionMarkRatingDataGen extends EntityBase
 implements INaturalIdentifiable<SessionMarkRatingDataGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.mark.SessionMarkRatingData";
    public static final String ENTITY_NAME = "sessionMarkRatingData";
    public static final int VERSION_HASH = -590317300;
    private static IEntityMeta ENTITY_META;

    public static final String L_MARK = "mark";
    public static final String P_SCORED_POINTS_AS_LONG = "scoredPointsAsLong";
    public static final String P_SCORED_POINTS = "scoredPoints";

    private SessionSlotRegularMark _mark;     // Оценка
    private long _scoredPointsAsLong;     // Баллы за сдачу мероприятия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Оценка. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public SessionSlotRegularMark getMark()
    {
        return _mark;
    }

    /**
     * @param mark Оценка. Свойство не может быть null и должно быть уникальным.
     */
    public void setMark(SessionSlotRegularMark mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    /**
     * Баллы, полученные непосредственно за сдачу мероприятия в рамках БРС.
     *
     * @return Баллы за сдачу мероприятия. Свойство не может быть null.
     */
    @NotNull
    public long getScoredPointsAsLong()
    {
        return _scoredPointsAsLong;
    }

    /**
     * @param scoredPointsAsLong Баллы за сдачу мероприятия. Свойство не может быть null.
     */
    public void setScoredPointsAsLong(long scoredPointsAsLong)
    {
        dirty(_scoredPointsAsLong, scoredPointsAsLong);
        _scoredPointsAsLong = scoredPointsAsLong;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionMarkRatingDataGen)
        {
            if (withNaturalIdProperties)
            {
                setMark(((SessionMarkRatingData)another).getMark());
            }
            setScoredPointsAsLong(((SessionMarkRatingData)another).getScoredPointsAsLong());
        }
    }

    public INaturalId<SessionMarkRatingDataGen> getNaturalId()
    {
        return new NaturalId(getMark());
    }

    public static class NaturalId extends NaturalIdBase<SessionMarkRatingDataGen>
    {
        private static final String PROXY_NAME = "SessionMarkRatingDataNaturalProxy";

        private Long _mark;

        public NaturalId()
        {}

        public NaturalId(SessionSlotRegularMark mark)
        {
            _mark = ((IEntity) mark).getId();
        }

        public Long getMark()
        {
            return _mark;
        }

        public void setMark(Long mark)
        {
            _mark = mark;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionMarkRatingDataGen.NaturalId) ) return false;

            SessionMarkRatingDataGen.NaturalId that = (NaturalId) o;

            if( !equals(getMark(), that.getMark()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getMark());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getMark());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionMarkRatingDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionMarkRatingData.class;
        }

        public T newInstance()
        {
            return (T) new SessionMarkRatingData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mark":
                    return obj.getMark();
                case "scoredPointsAsLong":
                    return obj.getScoredPointsAsLong();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mark":
                    obj.setMark((SessionSlotRegularMark) value);
                    return;
                case "scoredPointsAsLong":
                    obj.setScoredPointsAsLong((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mark":
                        return true;
                case "scoredPointsAsLong":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mark":
                    return true;
                case "scoredPointsAsLong":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mark":
                    return SessionSlotRegularMark.class;
                case "scoredPointsAsLong":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionMarkRatingData> _dslPath = new Path<SessionMarkRatingData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionMarkRatingData");
    }
            

    /**
     * @return Оценка. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.mark.SessionMarkRatingData#getMark()
     */
    public static SessionSlotRegularMark.Path<SessionSlotRegularMark> mark()
    {
        return _dslPath.mark();
    }

    /**
     * Баллы, полученные непосредственно за сдачу мероприятия в рамках БРС.
     *
     * @return Баллы за сдачу мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionMarkRatingData#getScoredPointsAsLong()
     */
    public static PropertyPath<Long> scoredPointsAsLong()
    {
        return _dslPath.scoredPointsAsLong();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.mark.SessionMarkRatingData#getScoredPoints()
     */
    public static SupportedPropertyPath<Double> scoredPoints()
    {
        return _dslPath.scoredPoints();
    }

    public static class Path<E extends SessionMarkRatingData> extends EntityPath<E>
    {
        private SessionSlotRegularMark.Path<SessionSlotRegularMark> _mark;
        private PropertyPath<Long> _scoredPointsAsLong;
        private SupportedPropertyPath<Double> _scoredPoints;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Оценка. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.mark.SessionMarkRatingData#getMark()
     */
        public SessionSlotRegularMark.Path<SessionSlotRegularMark> mark()
        {
            if(_mark == null )
                _mark = new SessionSlotRegularMark.Path<SessionSlotRegularMark>(L_MARK, this);
            return _mark;
        }

    /**
     * Баллы, полученные непосредственно за сдачу мероприятия в рамках БРС.
     *
     * @return Баллы за сдачу мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionMarkRatingData#getScoredPointsAsLong()
     */
        public PropertyPath<Long> scoredPointsAsLong()
        {
            if(_scoredPointsAsLong == null )
                _scoredPointsAsLong = new PropertyPath<Long>(SessionMarkRatingDataGen.P_SCORED_POINTS_AS_LONG, this);
            return _scoredPointsAsLong;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.mark.SessionMarkRatingData#getScoredPoints()
     */
        public SupportedPropertyPath<Double> scoredPoints()
        {
            if(_scoredPoints == null )
                _scoredPoints = new SupportedPropertyPath<Double>(SessionMarkRatingDataGen.P_SCORED_POINTS, this);
            return _scoredPoints;
        }

        public Class getEntityClass()
        {
            return SessionMarkRatingData.class;
        }

        public String getEntityName()
        {
            return "sessionMarkRatingData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getScoredPoints();
}
