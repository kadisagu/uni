/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinAdd;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.report.UnisessionSummaryBulletinReport;

import java.util.Collection;

/**
 * @author oleyba
 * @since 12/20/11
 */
public interface ISessionSummaryBulletinPrintDAO extends INeedPersistenceSupport
{
    SpringBeanCache<ISessionSummaryBulletinPrintDAO> instance = new SpringBeanCache<>(ISessionSummaryBulletinPrintDAO.class.getName());

	/**  Информация о сводной ведомости на одну группу */
    interface ISessionSummaryBulletinData
    {
        String getGroup();

		/** "Строки студентов" */
        Collection<ISessionSummaryBulletinStudent> getStudents();

		/**  Столбцы дисциплин (ФИК, дисциплина, ведомости) */
        Collection<ISessionSummaryBulletinAction> getActions();

	    /** Столбцы практик предыдущего семестра (ФИК, дисциплина, ведомость) */
		Collection<ISessionSummaryBulletinAction> getPrevTermPractices();

		/**
		 * Получить по "студенту" и столбцу (ФИК и дисциплина) вид дисциплины РУП. Вообще-то этот вид дисциплины даром никому не нужен, проверяется только его наличие по такому ключу.
		 * Если ничего нет, то видимо в РУП студента нет такого контрольного мероприятия (в отчете вместо оценки ставится прочерк).
		 */
        EppWorkPlanRowKind getWpRowKind(ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action);

	    /** Есть ли в РУП студента соответствующая практика (по факту - есть ли МСРП-ФК с соответствующими параметрами) */
        boolean isPrevTermPracticeInWorkPlan(EppRegistryElementPart registryElementPart, EppFControlActionType fcaType, Student student);

		/** Получить по "строке студента" и столбцу (ФИК и дисцпилина) оценку студента по дисциплине. */
        SessionMark getMark(ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action);

	    /** Получить оценку для практики предыдущего семестра по студенту, ФИК и дисциплине. Оценка может быть null. */
		SessionMark getPrevTermPracticeMark(EppRegistryElementPart registryElementPart, EppFControlActionType fcaType, Student student);

	    /** Показывать ли данные БРС по оценкам. */
        boolean isShowMarkPointsData();
    }

	/**
	 * Сваленная в кучу информация о студенте. По ней можно разбивать студентов по НПв и составлять список семестров. Используется в основном как заголовок строки таблицы (какой это студент).
	 */
    interface ISessionSummaryBulletinStudent
    {
        Student getStudent();
        EducationOrgUnit getEduOu();
        Course getCourse();
        Term getTerm();
    }

	/**
	 * Информация о столбце дисциплины, выводимая в заголовке - ФИК, дисциплина, список ведомостей
	 */
    interface ISessionSummaryBulletinAction
    {
        EppFControlActionType getControlActionType();
        EppRegistryElementPart getDiscipline();
        Collection<SessionBulletinDocument> getBulletins();
    }

    Collection<ISessionSummaryBulletinData> getSessionSummaryBulletinData(SessionReportSummaryBulletinAddUI model);

    byte[] print(SessionReportSummaryBulletinAddUI model, Collection<ISessionSummaryBulletinData> data);

    UnisessionSummaryBulletinReport createStoredReport(SessionReportSummaryBulletinAddUI model);
}
