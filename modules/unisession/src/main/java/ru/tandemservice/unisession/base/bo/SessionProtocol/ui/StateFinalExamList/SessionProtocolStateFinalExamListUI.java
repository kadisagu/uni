/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.StateFinalExamList;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unisession.base.bo.SessionProtocol.SessionProtocolManager;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.AbstractList.SessionProtocolAbstractListUI;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.StateFinalExamAllEdit.SessionProtocolStateFinalExamAllEdit;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "bulletinId", required = true)})
public class SessionProtocolStateFinalExamListUI extends SessionProtocolAbstractListUI
{
    // Listeners
    @Override
    public void onClickMassEditProtocols()
    {
        _uiActivation.asDesktopRoot(SessionProtocolStateFinalExamAllEdit.class)
                .parameters(ParametersMap.createWith(PublisherActivator.PUBLISHER_ID_KEY, _bulletinId))
                .activate();
    }

    @Override
    public void onClickPrint()
    {
        RtfDocument document = SessionProtocolManager.instance().stateExamProtocolPrintDAO()
                .printProtocols(Collections.singletonList(getListenerParameterAsLong()));

        if (document != null)
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(document).fileName("Протокол государственного экзамена.rtf").rtf(), false);
    }

    public void onClickMassPrintProtocols()
    {
        Collection<IEntity> selectedProtocols = getSelectedProtocols();
        if (CollectionUtils.isEmpty(selectedProtocols))
            throw new ApplicationException("Невозможно напечатать протоколы. Необходимо выбрать хотя бы один протокол.");

        List<Long> ids = selectedProtocols.stream().map(IEntity::getId).collect(Collectors.toList());
        RtfDocument document = SessionProtocolManager.instance().stateExamProtocolPrintDAO().printProtocols(ids);

        if (document != null)
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(document).fileName("Протоколы государственного экзамена.rtf").rtf(), false);
    }

    // Getters and Setters
    public String getMassDeletePermissionKey()
    {
        return "massDeleteStateExamProtocols";
    }

    public String getMassEditGECPermissionKey()
    {
        return "massEditGEC4StateExam";
    }

    public String getMassEditPermissionKey()
    {
        return "massEditStateExamProtocols";
    }

    public String getMassPrintPermissionKey()
    {
        return "massPrintStateExamProtocols";
    }
}