package ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentAddEdit;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;

import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;

/**
 * @author iolshvang
 * @since 25.04.2011
 */
public class SessionListDocumentEppSlotAddModel
{
    private TermWithActionSlot editRow = new TermWithActionSlot();

    private final StaticListDataSource<TermWithActionSlot> eppDataSource = new StaticListDataSource<TermWithActionSlot>();
    public StaticListDataSource<TermWithActionSlot> getEppDataSource() {
        return this.eppDataSource;
    }

    public IEntity getEditRow() {
        return this.editRow;
    }

    public void setEditRow(final TermWithActionSlot editRow) {
        this.editRow = editRow;
    }

    public Long getEditRowId() {
        final IEntity row = this.getEditRow();
        return (null == row) ? null : row.getId();
    }

    public boolean isCurrentRowInEditMode() {
        final Long editRowId = this.getEditRowId();
        return (null != editRowId && editRowId.equals(this.getEppDataSource().getCurrentEntity().getId()));
    }

    public boolean isEditMode() {
        return null != getEditRowId();
    }

    @SuppressWarnings("unchecked")
    public TermWithActionSlot findRow(final Long id) {
        if (null == id) { return null; }
        return CoreCollectionUtils.find(this.getEppDataSource().getRowList(), id);
    }

    @SuppressWarnings("unchecked")
    public Long addRow(final TermWithActionSlot row)
    {
        this.getEppDataSource().setupRows(this.getEppDataSource().getRowList());
        this.getEppDataSource().getRowList().add(row);
        return row.getId();
    }

    @SuppressWarnings("unchecked")
    public void setupRows(Collection<TermWithActionSlot> rows)
    {
        this.setEditRow(null);

        final List<TermWithActionSlot> currentList = this.getEppDataSource().getRowList();
        if (currentList.size() > 0)
        {
            // если что-то было. то применяем к тому, что есть в базе, то, что уже было
            try
            {
                rows = this.merge(rows, currentList);
            }
            catch (final Throwable t)
            {
                // do nothing (restore database values)
            }
        }

        this.getEppDataSource().setupRows(rows);
    }

    @SuppressWarnings("unchecked")
    protected List<TermWithActionSlot> merge(final Collection<TermWithActionSlot> targetRows, final List<TermWithActionSlot> sourceRows)
    {
        return new MergeAction<MultiKey, TermWithActionSlot>() {
            @Override protected TermWithActionSlot buildRow(final TermWithActionSlot source) { return source;  }
            @Override protected MultiKey key(final TermWithActionSlot source) { return source.getRowFullKey(); }
            @Override protected void fill(final TermWithActionSlot target, final TermWithActionSlot source) { }
        }.merge(targetRows, sourceRows);
    }

    @SuppressWarnings("unchecked")
    public void delete(final Long rowId)
    {
        if (null == rowId) { return;  }
        this.getEppDataSource().getRowList().remove(CoreCollectionUtils.find(this.getEppDataSource().getRowList(), rowId));
    }
}
