/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocPubForPps;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

/**
 * @author oleyba
 * @since 6/15/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setRetakeDoc(this.getNotNull(SessionRetakeDocument.class, model.getRetakeDoc().getId()));
        final Person person = PersonManager.instance().dao().getPerson(model.getPrincipalContext());
        if (!CollectionUtils.exists(getList(SessionComissionPps.class, SessionComissionPps.commission(), model.getRetakeDoc().getCommission()), sessionComissionPps -> sessionComissionPps.getPps().getPerson().equals(person)))
            throw new ApplicationException("Вы не состоите в коммиссии данной ведомости, поэтому не можете просматривать ее со своей личной страницы.");
    }
}
