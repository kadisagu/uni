package ru.tandemservice.unisession.attestation.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Доп. данные для записи студента в атт. ведомости
 *
 * Доп. данные для записи студента в атт. ведомости - вместе с отметкой об аттестации может фиксироваться число пропусков, и т.д.
 * Для создания подкласса в проектном слое и сохранения там же.
 * В продуктовом коде предполагается, что данные могут как присутствовать, так и отсутствовать, для каждой конкретной записи.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionAttestationSlotAdditionalDataGen extends EntityBase
 implements INaturalIdentifiable<SessionAttestationSlotAdditionalDataGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData";
    public static final String ENTITY_NAME = "sessionAttestationSlotAdditionalData";
    public static final int VERSION_HASH = 1622232757;
    private static IEntityMeta ENTITY_META;

    public static final String L_SLOT = "slot";

    private SessionAttestationSlot _slot;     // Запись студента в атт. ведомости

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись студента в атт. ведомости. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public SessionAttestationSlot getSlot()
    {
        return _slot;
    }

    /**
     * @param slot Запись студента в атт. ведомости. Свойство не может быть null и должно быть уникальным.
     */
    public void setSlot(SessionAttestationSlot slot)
    {
        dirty(_slot, slot);
        _slot = slot;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionAttestationSlotAdditionalDataGen)
        {
            if (withNaturalIdProperties)
            {
                setSlot(((SessionAttestationSlotAdditionalData)another).getSlot());
            }
        }
    }

    public INaturalId<SessionAttestationSlotAdditionalDataGen> getNaturalId()
    {
        return new NaturalId(getSlot());
    }

    public static class NaturalId extends NaturalIdBase<SessionAttestationSlotAdditionalDataGen>
    {
        private static final String PROXY_NAME = "SessionAttestationSlotAdditionalDataNaturalProxy";

        private Long _slot;

        public NaturalId()
        {}

        public NaturalId(SessionAttestationSlot slot)
        {
            _slot = ((IEntity) slot).getId();
        }

        public Long getSlot()
        {
            return _slot;
        }

        public void setSlot(Long slot)
        {
            _slot = slot;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SessionAttestationSlotAdditionalDataGen.NaturalId) ) return false;

            SessionAttestationSlotAdditionalDataGen.NaturalId that = (NaturalId) o;

            if( !equals(getSlot(), that.getSlot()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getSlot());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getSlot());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionAttestationSlotAdditionalDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionAttestationSlotAdditionalData.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("SessionAttestationSlotAdditionalData is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "slot":
                    return obj.getSlot();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "slot":
                    obj.setSlot((SessionAttestationSlot) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "slot":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "slot":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "slot":
                    return SessionAttestationSlot.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionAttestationSlotAdditionalData> _dslPath = new Path<SessionAttestationSlotAdditionalData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionAttestationSlotAdditionalData");
    }
            

    /**
     * @return Запись студента в атт. ведомости. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData#getSlot()
     */
    public static SessionAttestationSlot.Path<SessionAttestationSlot> slot()
    {
        return _dslPath.slot();
    }

    public static class Path<E extends SessionAttestationSlotAdditionalData> extends EntityPath<E>
    {
        private SessionAttestationSlot.Path<SessionAttestationSlot> _slot;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись студента в атт. ведомости. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData#getSlot()
     */
        public SessionAttestationSlot.Path<SessionAttestationSlot> slot()
        {
            if(_slot == null )
                _slot = new SessionAttestationSlot.Path<SessionAttestationSlot>(L_SLOT, this);
            return _slot;
        }

        public Class getEntityClass()
        {
            return SessionAttestationSlotAdditionalData.class;
        }

        public String getEntityName()
        {
            return "sessionAttestationSlotAdditionalData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
