package ru.tandemservice.unisession.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Сводная ведомость группы (по всем полученным студентами оценкам)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisessionGroupMarksReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport";
    public static final String ENTITY_NAME = "unisessionGroupMarksReport";
    public static final int VERSION_HASH = 1467201898;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_EXECUTOR = "executor";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_DISC_KINDS = "discKinds";
    public static final String P_COURSE = "course";
    public static final String P_GROUPS = "groups";
    public static final String P_CUSTOM_STATE = "customState";
    public static final String P_REGISTRY_STRUCTURE = "registryStructure";
    public static final String P_CONTROL_ACTION_TYPES = "controlActionTypes";
    public static final String P_REGISTRY_ELEMENT_PARTS = "registryElementParts";
    public static final String P_FORMING_DATE_STR = "formingDateStr";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private String _executor;     // Исполнитель
    private OrgUnit _orgUnit;     // Подразделение
    private String _discKinds;     // Включать дисциплины
    private String _course;     // Курс
    private String _groups;     // Группы
    private String _customState;     // Дополнительный статус
    private String _registryStructure;     // Вид мероприятия реестра
    private String _controlActionTypes;     // Форма контроля
    private String _registryElementParts;     // Мероприятия реестра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * Названия видов дисциплин РУП, выбранных при построении отчета, через запятую.
     *
     * @return Включать дисциплины. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDiscKinds()
    {
        return _discKinds;
    }

    /**
     * @param discKinds Включать дисциплины. Свойство не может быть null.
     */
    public void setDiscKinds(String discKinds)
    {
        dirty(_discKinds, discKinds);
        _discKinds = discKinds;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группы.
     */
    public String getGroups()
    {
        return _groups;
    }

    /**
     * @param groups Группы.
     */
    public void setGroups(String groups)
    {
        dirty(_groups, groups);
        _groups = groups;
    }

    /**
     * @return Дополнительный статус.
     */
    public String getCustomState()
    {
        return _customState;
    }

    /**
     * @param customState Дополнительный статус.
     */
    public void setCustomState(String customState)
    {
        dirty(_customState, customState);
        _customState = customState;
    }

    /**
     * @return Вид мероприятия реестра.
     */
    public String getRegistryStructure()
    {
        return _registryStructure;
    }

    /**
     * @param registryStructure Вид мероприятия реестра.
     */
    public void setRegistryStructure(String registryStructure)
    {
        dirty(_registryStructure, registryStructure);
        _registryStructure = registryStructure;
    }

    /**
     * @return Форма контроля.
     */
    public String getControlActionTypes()
    {
        return _controlActionTypes;
    }

    /**
     * @param controlActionTypes Форма контроля.
     */
    public void setControlActionTypes(String controlActionTypes)
    {
        dirty(_controlActionTypes, controlActionTypes);
        _controlActionTypes = controlActionTypes;
    }

    /**
     * @return Мероприятия реестра.
     */
    @Length(max=1024)
    public String getRegistryElementParts()
    {
        return _registryElementParts;
    }

    /**
     * @param registryElementParts Мероприятия реестра.
     */
    public void setRegistryElementParts(String registryElementParts)
    {
        dirty(_registryElementParts, registryElementParts);
        _registryElementParts = registryElementParts;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UnisessionGroupMarksReportGen)
        {
            setContent(((UnisessionGroupMarksReport)another).getContent());
            setFormingDate(((UnisessionGroupMarksReport)another).getFormingDate());
            setExecutor(((UnisessionGroupMarksReport)another).getExecutor());
            setOrgUnit(((UnisessionGroupMarksReport)another).getOrgUnit());
            setDiscKinds(((UnisessionGroupMarksReport)another).getDiscKinds());
            setCourse(((UnisessionGroupMarksReport)another).getCourse());
            setGroups(((UnisessionGroupMarksReport)another).getGroups());
            setCustomState(((UnisessionGroupMarksReport)another).getCustomState());
            setRegistryStructure(((UnisessionGroupMarksReport)another).getRegistryStructure());
            setControlActionTypes(((UnisessionGroupMarksReport)another).getControlActionTypes());
            setRegistryElementParts(((UnisessionGroupMarksReport)another).getRegistryElementParts());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisessionGroupMarksReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisessionGroupMarksReport.class;
        }

        public T newInstance()
        {
            return (T) new UnisessionGroupMarksReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "executor":
                    return obj.getExecutor();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "discKinds":
                    return obj.getDiscKinds();
                case "course":
                    return obj.getCourse();
                case "groups":
                    return obj.getGroups();
                case "customState":
                    return obj.getCustomState();
                case "registryStructure":
                    return obj.getRegistryStructure();
                case "controlActionTypes":
                    return obj.getControlActionTypes();
                case "registryElementParts":
                    return obj.getRegistryElementParts();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "discKinds":
                    obj.setDiscKinds((String) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "groups":
                    obj.setGroups((String) value);
                    return;
                case "customState":
                    obj.setCustomState((String) value);
                    return;
                case "registryStructure":
                    obj.setRegistryStructure((String) value);
                    return;
                case "controlActionTypes":
                    obj.setControlActionTypes((String) value);
                    return;
                case "registryElementParts":
                    obj.setRegistryElementParts((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "executor":
                        return true;
                case "orgUnit":
                        return true;
                case "discKinds":
                        return true;
                case "course":
                        return true;
                case "groups":
                        return true;
                case "customState":
                        return true;
                case "registryStructure":
                        return true;
                case "controlActionTypes":
                        return true;
                case "registryElementParts":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "executor":
                    return true;
                case "orgUnit":
                    return true;
                case "discKinds":
                    return true;
                case "course":
                    return true;
                case "groups":
                    return true;
                case "customState":
                    return true;
                case "registryStructure":
                    return true;
                case "controlActionTypes":
                    return true;
                case "registryElementParts":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "executor":
                    return String.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "discKinds":
                    return String.class;
                case "course":
                    return String.class;
                case "groups":
                    return String.class;
                case "customState":
                    return String.class;
                case "registryStructure":
                    return String.class;
                case "controlActionTypes":
                    return String.class;
                case "registryElementParts":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UnisessionGroupMarksReport> _dslPath = new Path<UnisessionGroupMarksReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisessionGroupMarksReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * Названия видов дисциплин РУП, выбранных при построении отчета, через запятую.
     *
     * @return Включать дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getDiscKinds()
     */
    public static PropertyPath<String> discKinds()
    {
        return _dslPath.discKinds();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группы.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getGroups()
     */
    public static PropertyPath<String> groups()
    {
        return _dslPath.groups();
    }

    /**
     * @return Дополнительный статус.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getCustomState()
     */
    public static PropertyPath<String> customState()
    {
        return _dslPath.customState();
    }

    /**
     * @return Вид мероприятия реестра.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getRegistryStructure()
     */
    public static PropertyPath<String> registryStructure()
    {
        return _dslPath.registryStructure();
    }

    /**
     * @return Форма контроля.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getControlActionTypes()
     */
    public static PropertyPath<String> controlActionTypes()
    {
        return _dslPath.controlActionTypes();
    }

    /**
     * @return Мероприятия реестра.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getRegistryElementParts()
     */
    public static PropertyPath<String> registryElementParts()
    {
        return _dslPath.registryElementParts();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getFormingDateStr()
     */
    public static SupportedPropertyPath<String> formingDateStr()
    {
        return _dslPath.formingDateStr();
    }

    public static class Path<E extends UnisessionGroupMarksReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<String> _executor;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _discKinds;
        private PropertyPath<String> _course;
        private PropertyPath<String> _groups;
        private PropertyPath<String> _customState;
        private PropertyPath<String> _registryStructure;
        private PropertyPath<String> _controlActionTypes;
        private PropertyPath<String> _registryElementParts;
        private SupportedPropertyPath<String> _formingDateStr;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(UnisessionGroupMarksReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(UnisessionGroupMarksReportGen.P_EXECUTOR, this);
            return _executor;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * Названия видов дисциплин РУП, выбранных при построении отчета, через запятую.
     *
     * @return Включать дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getDiscKinds()
     */
        public PropertyPath<String> discKinds()
        {
            if(_discKinds == null )
                _discKinds = new PropertyPath<String>(UnisessionGroupMarksReportGen.P_DISC_KINDS, this);
            return _discKinds;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(UnisessionGroupMarksReportGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Группы.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getGroups()
     */
        public PropertyPath<String> groups()
        {
            if(_groups == null )
                _groups = new PropertyPath<String>(UnisessionGroupMarksReportGen.P_GROUPS, this);
            return _groups;
        }

    /**
     * @return Дополнительный статус.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getCustomState()
     */
        public PropertyPath<String> customState()
        {
            if(_customState == null )
                _customState = new PropertyPath<String>(UnisessionGroupMarksReportGen.P_CUSTOM_STATE, this);
            return _customState;
        }

    /**
     * @return Вид мероприятия реестра.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getRegistryStructure()
     */
        public PropertyPath<String> registryStructure()
        {
            if(_registryStructure == null )
                _registryStructure = new PropertyPath<String>(UnisessionGroupMarksReportGen.P_REGISTRY_STRUCTURE, this);
            return _registryStructure;
        }

    /**
     * @return Форма контроля.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getControlActionTypes()
     */
        public PropertyPath<String> controlActionTypes()
        {
            if(_controlActionTypes == null )
                _controlActionTypes = new PropertyPath<String>(UnisessionGroupMarksReportGen.P_CONTROL_ACTION_TYPES, this);
            return _controlActionTypes;
        }

    /**
     * @return Мероприятия реестра.
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getRegistryElementParts()
     */
        public PropertyPath<String> registryElementParts()
        {
            if(_registryElementParts == null )
                _registryElementParts = new PropertyPath<String>(UnisessionGroupMarksReportGen.P_REGISTRY_ELEMENT_PARTS, this);
            return _registryElementParts;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport#getFormingDateStr()
     */
        public SupportedPropertyPath<String> formingDateStr()
        {
            if(_formingDateStr == null )
                _formingDateStr = new SupportedPropertyPath<String>(UnisessionGroupMarksReportGen.P_FORMING_DATE_STR, this);
            return _formingDateStr;
        }

        public Class getEntityClass()
        {
            return UnisessionGroupMarksReport.class;
        }

        public String getEntityName()
        {
            return "unisessionGroupMarksReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFormingDateStr();
}
