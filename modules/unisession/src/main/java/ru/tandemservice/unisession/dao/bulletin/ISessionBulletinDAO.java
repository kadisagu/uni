package ru.tandemservice.unisession.dao.bulletin;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.gen.EppRegistryStructureGen;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocument;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * @author vdanilov
 */
public interface ISessionBulletinDAO {

    SpringBeanCache<ISessionBulletinDAO> instance = new SpringBeanCache<>(ISessionBulletinDAO.class.getName());

    /**
     * Требует ли темы группа форм контроля, соответствующая виду УГС (ФК)
     * @param eppGroupType вид УГС
     */
    boolean isThemeRequired(EppGroupType eppGroupType);

	/**
	 * Требуется ли тема для экзаменационной ведомости. В настоящий момент проверяется выполнение хотя бы одного из двух условий:
	 * <ul>
	 *     <li/> Тип УГС-ФК ведомости требует темы (см. {@link #isThemeRequired(ru.tandemservice.uniepp.entity.catalog.EppGroupType)})
	 *     <li/> Тип элемента реестра из УГС требует темы (см. {@link EppRegistryStructureGen#isThemeRequired()})
	 * </ul>
	 * @param bulletin Экзаменационная ведомость
	 */
	boolean isBulletinRequireTheme(SessionBulletinDocument bulletin);

    /**
     * Правило генерации номеров для новых ведомостей
     * Логика в проекте - уникальные номера, инкремент с единицы в рамках учебного года
     * (на который создана сессия, в которой формируются ведомости)
     * @return сабж
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    INumberGenerationRule<SessionDocument> getNumberGenerationRule();

    /**
     * Формирует для указанных УГС ведомости
     * @param sessionObjectId сессия
     * @param eppRealEduGroup4ActionTypeIds (EppRealEduGroup4ActionType)
     * @return { group g.id -> bulletin b (b.group.id=g.id) }
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    Map<Long, SessionBulletinDocument> doCreateBulletin(Long sessionObjectId, Collection<Long> eppRealEduGroup4ActionTypeIds);

    /**
     * Синхронизирует список студентов в ведомости с УГС
     * @param bulletinId ведомость
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)

    void doRefreshBulletin(Long bulletinId);

    /**
     * Возвращает set из id ведомостей, которые можно удалить
     * @param ids ведомостей
     * @return ведомости, которые можно удалить
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Set<Long> findBulletinsToDelete(Collection<Long> ids);

}
