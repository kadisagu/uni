/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionSheet.ui.Forming;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionSheet.logic.SessionSheetFormingDebtorsSearchDSHandler;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;
import ru.tandemservice.unisession.entity.mark.SessionMark;


/**
 * @author Alexander Shaburov
 * @since 15.02.13
 */
@Configuration
public class SessionSheetForming extends BusinessComponentManager
{
    public static final String REASON_DS = "reasonDS";

    public static final String DEBTORS_SEARCH_DS = "debtorsSearchDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REASON_DS, reasonDSHandler()))
                .addDataSource(SessionReportManager.instance().eduYearDSConfig())
                .addDataSource(SessionReportManager.instance().yearPartDSConfig())
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(searchListDS(DEBTORS_SEARCH_DS, studentSearchDSColumns(), studentSearchDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reasonDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), SessionsSimpleDocumentReason.class)
                .where(SessionsSimpleDocumentReason.formOnDebts(), Boolean.TRUE)
                .order(SessionsSimpleDocumentReason.title())
                .filter(SessionsSimpleDocumentReason.title());
    }

    @Bean
    public ColumnListExtPoint studentSearchDSColumns()
    {
        return columnListExtPointBuilder(DEBTORS_SEARCH_DS)
                .addColumn(checkboxColumn("check").visible("ui:emptyMarkList"))
                .addColumn(textColumn("studentFio", SessionMark.slot().studentWpeCAction().studentWpe().student().person().fio()))
                .addColumn(textColumn("course", SessionMark.slot().studentWpeCAction().studentWpe().student().course().title()))
                .addColumn(textColumn("group", SessionMark.slot().studentWpeCAction().studentWpe().student().group().title()))
                .addColumn(textColumn("discipline", SessionMark.slot().studentWpeCAction().studentWpe().registryElementPart().titleWithNumber()))
                .addColumn(textColumn("control", SessionMark.slot().studentWpeCAction().type().title()))
                .addColumn(textColumn("mark", SessionMark.valueTitle()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchDSHandler()
    {
        return new SessionSheetFormingDebtorsSearchDSHandler(getName());
    }
}
