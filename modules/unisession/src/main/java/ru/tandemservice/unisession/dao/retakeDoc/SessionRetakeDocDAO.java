/* $Id$ */
package ru.tandemservice.unisession.dao.retakeDoc;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.hibsupport.log.InsertEntityEvent;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.dao.comission.ISessionCommissionDAO;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/20/11
 */
public class SessionRetakeDocDAO extends UniBaseDao implements ISessionRetakeDocDAO
{
	@Override
	public void filterGroupsNeedRetake(final DQLSelectBuilder groupRowDql, final String groupRowAlias, final SessionObject sessionObject)
	{
		final String slotAlias = "slot";
		final String bulletinAlias = "bulletin";
		DQLSelectBuilder slots = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, slotAlias).star()
				.where(eq(property(groupRowAlias, EppRealEduGroupRow.studentWpePart()), property(slotAlias, SessionDocumentSlot.studentWpeCAction())));

		// Фильтруем слоты, оставляя только те, на которые нужны пересдачи
		filterSlotsNeedRetake(slots, slotAlias, bulletinAlias, null, null, sessionObject);

		// Фильтруем строки УГС, оставляя только те, на которые есть отфильтрованные слоты в ведомостях - нам нужно оставить только те строки УГС, на которые трубуются пересдачи.
		groupRowDql.where(exists(slots.buildQuery()));
	}

	/**
	 * Оставить в запросе к SessionDocumentSlot только записи студентов из экзаменационных ведомостей текущей сессии с МСРП-ФК требующими пересдачи, с опциональной фильтрацией по оценкам и УГС
	 * МСРП-ФК требует пересдачи, если оно активно, итоговая оценка в зачетке отрицательная и не "в сессию", при этом:
	 * Верно, что либо нет слота по МСРП-ФК ни в какой ведомости пересдач (еще не пересдавал), либо все оценки в ведомостях пересдач отрицательные (так и не пересдал)
	 * То же самое, иначе: не верно, что по МСРП-ФК есть слот в ведомости пересдач без оценки (ведомость пересдачи сформирована, но самой пересдачи еще не было),
	 * либо есть положительная оценка в ведомости пересдачи (успешно пересдал)
	 * (все ведомости пересдач и экзаменационные ведомости - текущей сессии)
	 * @param slotsDql - запрос с записям студента в экзаменационных ведомостях (SessionDocumentSlot)
	 * @param slotAlias - алиас записи студента в ведомости
	 * @param bulletinAlias - алиас ведомости, к которой относится запись студента (ведомость будет приджойнена к слоту)
	 * @param markList - опциональный список id оценок - если не пуст, то будут оставлены только слоты, на которые указывают оценки из списка
	 * @param groupIds - опциональный список id УГС - если не пуст, то будут оставлены только слоты экзаменационных ведомостей для УГС из списка
	 * @param sessionObject - текущая сессия
	 */
	private void filterSlotsNeedRetake(final DQLSelectBuilder slotsDql, final String slotAlias, final String bulletinAlias, final List<Long> markList, final List<Long> groupIds, final SessionObject sessionObject)
	{
		final String bulletinMarkAlias = "bulletinMark";
		final String bulletinSlotAlias = "bulletinSlot";
		final String gradeBookAlias = "gradeBook";
		DQLSelectBuilder wpeCActionsNeedRetake = new DQLSelectBuilder()
				.fromEntity(SessionMark.class, bulletinMarkAlias)
				.joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias(bulletinMarkAlias), bulletinSlotAlias)
				.column(property(bulletinSlotAlias, SessionDocumentSlot.studentWpeCAction().id()))

				// оценка должна требовать пересдачи
				.where(eq(property(bulletinMarkAlias, SessionMark.cachedMarkPositiveStatus()), value(Boolean.FALSE)))

				// только активные МСРП (неархивные студенты в активном состоянии)
				.where(isNull(property(bulletinSlotAlias, SessionDocumentSlot.studentWpeCAction().removalDate())))

				// итоговая по всем оценкам (in-session=false)
				.where(eq(property(bulletinSlotAlias, SessionDocumentSlot.inSession()), value(Boolean.FALSE)))

				// итоговая оценка - должна быть в зачетке
				.joinEntity(bulletinSlotAlias, DQLJoinType.inner, SessionStudentGradeBookDocument.class, gradeBookAlias,
						eq(property(gradeBookAlias), property(bulletinSlotAlias, SessionDocumentSlot.document())));

		if (!CollectionUtils.isEmpty(markList))
			wpeCActionsNeedRetake.where(in(property(bulletinMarkAlias, SessionMark.id()), markList));

		final String retakeSlotAlias = "retakeSlot";
		final String retakeMarkAlias = "retakeMark";
		final String retakeDocAlias = "retakeDoc";
		DQLSelectBuilder wpeCActionsRetakenOrPlannedToRetake = new DQLSelectBuilder()
				.fromEntity(SessionDocumentSlot.class, retakeSlotAlias)
				.column(property(retakeSlotAlias, SessionDocumentSlot.studentWpeCAction().id()))

				// нет оценки или есть положительная оценка
				.joinEntity(retakeSlotAlias, DQLJoinType.left, SessionMark.class, retakeMarkAlias, eq(property(retakeSlotAlias), property(retakeMarkAlias, SessionMark.slot())))
				.where(or(
						isNull(property(retakeMarkAlias, SessionMark.id())),
						eq(property(retakeMarkAlias, SessionMark.cachedMarkPositiveStatus()), value(Boolean.TRUE))
				))

				// оценка в ведомости пересдач текущей сессии
				.joinEntity(retakeSlotAlias, DQLJoinType.inner, SessionRetakeDocument.class, retakeDocAlias, eq(property(retakeDocAlias), property(retakeSlotAlias, SessionDocumentSlot.document())))
				.where(eq(property(retakeDocAlias, SessionRetakeDocument.sessionObject()), value(sessionObject)));

		slotsDql
				.where(in(property(slotAlias, SessionDocumentSlot.studentWpeCAction().id()), wpeCActionsNeedRetake.buildQuery()))
				.where(notIn(property(slotAlias, SessionDocumentSlot.studentWpeCAction().id()), wpeCActionsRetakenOrPlannedToRetake.buildQuery()))
				.joinEntity(slotAlias, DQLJoinType.inner, SessionBulletinDocument.class, bulletinAlias, eq(property(slotAlias, SessionDocumentSlot.document()), property(bulletinAlias)))
				.where(eq(property(bulletinAlias, SessionBulletinDocument.sessionObject()), value(sessionObject)));

		if (!CollectionUtils.isEmpty(groupIds))
		{
			slotsDql.where(exists(
					EppRealEduGroupRow.class,
					EppRealEduGroupRow.studentWpePart().s(), property(slotAlias, SessionDocumentSlot.studentWpeCAction()),
					EppRealEduGroupRow.group().s(), groupIds));
		}
	}

	@Override
	public void doAddDocumentsForEduGroups(final List<Long> groupIds, SessionObject sessionObject, boolean inSession)
	{
		final ISessionRetakeDocDAO.GroupingSettings addDocumentsGroupingSettings = new ISessionRetakeDocDAO.GroupingSettings() {
			@Override public GroupSplitOption getGroupSplitOption() { return GroupSplitOption.SPLIT_BY_EPP_GROUP; }
			@Override public boolean isGroupByCommission() { return false; }
			@Override public boolean isGroupByCompensationType() { return false; }
		};
		doCreateDocuments(sessionObject, addDocumentsGroupingSettings, null, groupIds, inSession);
	}

	@Override
	public void doAutocreateDocuments(final SessionObject sessionObject, final GroupingSettings groupingSettings, final List<Long> markList)
	{
		doCreateDocuments(sessionObject, groupingSettings, markList, null, true);
	}

	/**
	 * Сформировать ведомости пересдач на указанную сессию с опциональной фильтрацией по оценкам и УГС и заданной политикой разделения записей по разным ведомостям пересдач
	 * @param sessionObject - текущая сессия
	 * @param groupingSettings - политика разбиения записей студентов по различным ведомостям (в одну ведомость попадают только записи с совпадающими значениями выбранных параметров - см.{@link ru.tandemservice.unisession.dao.retakeDoc.ISessionRetakeDocDAO.GroupingSettings})
	 * @param markList - опциональный список id оценок - если не пуст, то ведомости будут сформированы только по слотам, на которые указывают оценки из списка
	 * @param groupIds - опциональный список id УГС - если не пуст, то ведомости будут сформированы только по слотам экзаменационных ведомостей для УГС из списка
	 * @param inSession - все сформированные ведомости будут иметь признак "в сессию" с указанным значением
	 */
	private void doCreateDocuments(final SessionObject sessionObject, final GroupingSettings groupingSettings, final List<Long> markList, final List<Long> groupIds, boolean inSession)
	{
		final Session session = this.lock(sessionObject.getOrgUnit().getId());

		// Список слотов ведомостей текущей сессии
		final String slotAlias = "slot";
		final String bulletinAlias = "bulletin";
		final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, slotAlias);
		filterSlotsNeedRetake(dql, slotAlias, bulletinAlias, markList, groupIds, sessionObject);

		final DQLSelectColumnNumerator n = new DQLSelectColumnNumerator(dql);
		final int c_regel = n.column(property(SessionBulletinDocument.group().activityPart().id().fromAlias(bulletinAlias)));		/* regel [0] */
		final int c_type  = n.column(property(SessionBulletinDocument.group().type().id().fromAlias(bulletinAlias)));				/* ca-type [1] */
		final int c_wpca  = n.column(property(SessionDocumentSlot.studentWpeCAction().id().fromAlias(slotAlias)));					/* wpca [2] */
		final int c_comm  = n.column(property(SessionDocumentSlot.commission().id().fromAlias(slotAlias)));							/* comission [3] */
		final int c_comp  = n.column(property(SessionDocumentSlot.actualStudent().compensationType().id().fromAlias(slotAlias)));	/* current-compensation [4] */

		final int c_rel_grp = n.column(property(SessionDocumentSlot.actualStudent().group().id().fromAlias(slotAlias)));			/* current-group [5] */
		final int c_edu_grp = n.column(property(SessionBulletinDocument.group().id().fromAlias(bulletinAlias)));					/* current-group [6] */

		// Конвертация List<Object[]> -> List<Long[]> для удобства, чтоб каждый раз не кастовать Object к Long (почему-то dql.createStatement(session).<List[]>list() кидает исключение)
		final List<Long[]> slots = dql.createStatement(session).<Object[]>list().stream()
				.map((Object[] arr) -> Arrays.stream(arr).map((Object obj) -> (Long) obj).toArray(Long[]::new))
				.collect(Collectors.toList());

		final List<Long> comissions = slots.stream().map(arr -> arr[c_comm]).distinct().collect(Collectors.toList());

		// Комиссия -> множество ППС в комиссии
		final HashMultimap<Long, Long> commission2ppsMap = HashMultimap.create();
		for (List<Long> idsPart : Lists.partition(comissions, DQL.MAX_VALUES_ROW_NUMBER))
		{
			new DQLSelectBuilder()
					.fromEntity(SessionComissionPps.class, "pps")
					.where(in(property(SessionComissionPps.commission().id().fromAlias("pps")), idsPart))
					.column(property(SessionComissionPps.commission().id().fromAlias("pps")))
					.column(property(SessionComissionPps.pps().id().fromAlias("pps")))
					.createStatement(session).<Object[]>list()
					.forEach(row -> commission2ppsMap.put((Long) row[0], (Long) row[1]));
		}

		// distinctSlots.values() - те же slots, с удаленными дублями (с совпадающими записью реестра, видом УГС и МСРП-ФК)
		Map<MultiKey, Long[]> distinctSlots = new HashMap<>();
		slots.forEach(row -> distinctSlots.put(new MultiKey(row[c_regel], row[c_type], row[c_wpca]), row));

		// В одну ведомость попадают только слоты с одинаковой частью элемента реестра и типом УГС.
		// Если заданы соответствующие разбиения, то также с одинаковой комиссией, типом возмещения затрат и учебной группой (академической или учебной).
		// Если разбиение не задано - в одну ведомость попадут слоты даже с разным значением соответствующего параметра
		final Map<MultiKey, List<Long[]>> retakeDoc2slots = distinctSlots.values().stream().collect(Collectors.groupingBy(row ->
		{
			final EppRegistryElementPart regElement = (EppRegistryElementPart) session.load(EppRegistryElementPart.class, row[c_regel]);
			final Long grType = row[c_type];
			final Set<Long> commPps = groupingSettings.isGroupByCommission() ? commission2ppsMap.get(row[c_comm]) : null;
			final Long compensationType = groupingSettings.isGroupByCompensationType() ? row[c_comp] : null;
			final Long group;
			if (ISessionRetakeDocDAO.GroupingSettings.GroupSplitOption.SPLIT_BY_GROUP.equals(groupingSettings.getGroupSplitOption()))
				group = row[c_rel_grp];
			else if (ISessionRetakeDocDAO.GroupingSettings.GroupSplitOption.SPLIT_BY_EPP_GROUP.equals(groupingSettings.getGroupSplitOption()))
				group = row[c_edu_grp];
			else group = null;

			return new MultiKey(regElement, grType, commPps, compensationType, group);
		}));

		retakeDoc2slots.forEach((key, slotRows) -> createRetakeDocument(
				sessionObject,
				(EppRegistryElementPart) key.getKey(0),
				slotRows.stream()
						.map(row -> new Pair<>((EppStudentWpeCAction) session.load(EppStudentWpeCAction.class, row[c_wpca]), commission2ppsMap.get(row[c_comm])))
						.collect(Collectors.toList()),
				inSession,
				session));
	}

	private void createRetakeDocument(SessionObject sessionObject, EppRegistryElementPart regElement, List<Pair<EppStudentWpeCAction, Set<Long>>> slotInfo, boolean inSession, Session session)
	{
		final SessionComission docCommission = new SessionComission();
		session.save(docCommission);

        final SessionRetakeDocument doc = setupAndSaveDocument(sessionObject, regElement, docCommission, session);

		final Set<Long> usedWpca = new HashSet<>();
		slotInfo.forEach(info -> {
			if (usedWpca.add(info.getX().getId()))
			{
				final SessionComission slotCommission = new SessionComission();
				session.save(slotCommission);

				final EppStudentWpeCAction wpca = info.getX();
				final SessionDocumentSlot newSlot = new SessionDocumentSlot();
				newSlot.setDocument(doc);
				newSlot.setStudentWpeCAction(wpca);
				newSlot.setCommission(slotCommission);
				newSlot.setInSession(inSession);
				session.save(newSlot);

				final Set<Long> ppsIds = info.getY();
				if (!CollectionUtils.isEmpty(ppsIds))
					ISessionCommissionDAO.instance.get().saveOrUpdateCommissionIds(slotCommission, ppsIds);
			}
		});
		final Set<Long> docPps = slotInfo.stream().filter(info -> info.getY() != null).flatMap(info -> info.getY().stream()).collect(Collectors.toSet());
		if (!docPps.isEmpty())
			ISessionCommissionDAO.instance.get().saveOrUpdateCommissionIds(docCommission, docPps);

		createThemesByLastWpeThemes(doc);

		session.flush();
		session.clear();
	}

	private SessionRetakeDocument setupAndSaveDocument(SessionObject sessionObject, EppRegistryElementPart registryElementPart, SessionComission commission, Session session)
	{
		final SessionRetakeDocument doc = new SessionRetakeDocument();
		doc.setSessionObject(sessionObject);
		doc.setNumber(INumberQueueDAO.instance.get().getNextNumber(doc));
		doc.setFormingDate(new Date());
		doc.setRegistryElementPart(registryElementPart);
		doc.setCommission(commission);
		session.save(doc);
		return doc;
	}

    @Override
    public void doJoin(final Long orgUnitId, final List<Long> selectedIds)
    {
		if (selectedIds.isEmpty())
			return;

        final Session session = this.lock(orgUnitId, selectedIds);

        // проверяем наличие оценок (видимо, более одной оценки на мсрп-по-фк)
        // TODO: implement me

		if (isMultipleFcaTypes(selectedIds, session))
			throw new ApplicationException("Нельзя объединять ведомости по разным формам контроля");

		List<SessionRetakeDocument> retakeDocuments = getList(SessionRetakeDocument.class, SessionRetakeDocument.id(), selectedIds);

		List<SessionObject> sessionObjects = retakeDocuments.stream().map(SessionRetakeDocument::getSessionObject).distinct().collect(Collectors.toList());
		List<EppRegistryElementPart> regElemParts = retakeDocuments.stream().map(SessionRetakeDocument::getRegistryElementPart).distinct().collect(Collectors.toList());

		// TODO: вроде бы можно так делать - просто получится несколько ведомостей (каждая - объединение ведомостей с одинаковыми дисциплинами). Тогда нужно группировать их.
		if (sessionObjects.size() > 1 || regElemParts.size() > 1)
			throw new ApplicationException("Нельзя объединять ведомости с разными дисциплинами.");

        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
		{
            joinRetakeDocsWithSameRegElemParts(retakeDocuments, sessionObjects.get(0), regElemParts.get(0), session);

            // важно, чтобы объекты были в базе
            session.flush();
            session.clear();

        }
		finally
		{
            eventLock.release();
            CoreServices.eventService().fireEvent(new InsertEntityEvent("Сформированы ведомости.", get(OrgUnit.class, orgUnitId)));
        }

        // чистим все комиссии (из всех ведомостей)
        ISessionCommissionDAO.instance.get().doCleanUp();
    }

	private boolean isMultipleFcaTypes(List<Long> retakeDocumentIds, Session session)
	{
		final String slotAlias = "slot";
		final String groupTypeAlias = "groupType";
		final String fcaTypeAlias = "fcaType";

		final Number fcaTypesCount = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, slotAlias)
					.where(in(property(slotAlias, SessionDocumentSlot.document().id()), retakeDocumentIds))
					.joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().type().fromAlias(slotAlias), groupTypeAlias)
					.joinEntity(groupTypeAlias, DQLJoinType.inner, EppFControlActionType.class, fcaTypeAlias,
								eq(property(fcaTypeAlias, EppFControlActionType.eppGroupType()), property(groupTypeAlias)))
					.column(property(fcaTypeAlias, EppFControlActionType.id()))
					.distinct()
					.createCountStatement(new DQLExecutionContext(session)).uniqueResult();
		return fcaTypesCount.intValue() > 1;
	}

	private void joinRetakeDocsWithSameRegElemParts(final List<SessionRetakeDocument> retakeDocuments, SessionObject sessionObject, EppRegistryElementPart regElemPart, Session session)
	{
		final String slotAlias = "slot";
		final int slotsInDocuments = getList(SessionDocumentSlot.class, SessionDocumentSlot.document().s(), retakeDocuments).size();
		final int studentsInDocuments = getCount(new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, slotAlias)
				.where(in(property(slotAlias, SessionDocumentSlot.document()), retakeDocuments))
				.column(property(slotAlias, SessionDocumentSlot.studentWpeCAction().studentWpe().student().id()))
				.distinct());
		if (slotsInDocuments != studentsInDocuments)
			throw new ApplicationException("Нельзя объединять ведомости с одинаковыми студентами");

		DQLSelectBuilder commissionsOfRetakeDocs = new DQLSelectBuilder().fromEntity(SessionRetakeDocument.class, "x")
				.column(property(SessionRetakeDocument.commission().id().fromAlias("x")))
				.where(in(property("x"), retakeDocuments));
		// ППС из комиссий непосредственно ведомостей (не слотов)
		List<Long> ppsFromRetakeDocCommissions = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "rel")
				.column(property(SessionComissionPps.pps().id().fromAlias("rel")))
				.predicate(DQLPredicateType.distinct)
				.where(in(property(SessionComissionPps.commission().id().fromAlias("rel")), commissionsOfRetakeDocs.buildQuery()))
				.createStatement(session).<Long>list();
		List<PpsEntry> ppsList = getList(PpsEntry.class, PpsEntry.id(), ppsFromRetakeDocCommissions);
		final SessionComission docCommission = ISessionCommissionDAO.instance.get().saveOrUpdateCommission(null, ppsList);

		// создаем ведомость
		final SessionRetakeDocument doc = setupAndSaveDocument(sessionObject, regElemPart, docCommission, session);

		// важно, чтобы объекты были в базе
		session.flush();

		// переносим все слоты (с оценками и всем прочим)
		new DQLUpdateBuilder(SessionDocumentSlot.class)
				.set(SessionDocumentSlot.document().s(), value(doc))
				.where(in(property(SessionDocumentSlot.document()), retakeDocuments))
				.createStatement(session).execute();

		// создаем комиссию из всех товарищей, которые сейчас в ведомости
		setupRetakeDocumentComission(doc, session);

		// удаляем документы
		new DQLDeleteBuilder(SessionRetakeDocument.class)
				.where(in(property("id"), ids(retakeDocuments)))
				.createStatement(session).execute();
	}

	@Override
    public void doSplit(final Long orgUnitId, final List<Long> selectedIds, final PropertyPath<?> path)
    {
        final Session session = this.lock(orgUnitId, selectedIds);

        // обрабатываем каждый документ отдельно
        for (final Long sourceDocId: selectedIds)
        {
            final SessionRetakeDocument source = this.get(SessionRetakeDocument.class, sourceDocId);

            final Map<Object, Set<Long>> path2slotIdsMap = SafeMap.get(HashSet.class);
            {
                final List<Object[]> rows = new DQLSelectBuilder()
						.fromEntity(SessionDocumentSlot.class, "s")
						.where(eq(property(SessionDocumentSlot.document().fromAlias("s")), value(source)))
						.column(property(path.fromAlias("s")))
						.column(property(SessionDocumentSlot.id().fromAlias("s")))
                .createStatement(session).list();

                for (final Object[] row: rows) { path2slotIdsMap.get(row[0]).add((Long)row[1]); }
            }

            for (final Set<Long> slotIds: path2slotIdsMap.values())
            {
                // создаем ведомость перестач
                final SessionRetakeDocument doc = this.createRetakeDocument(source, session);

                // переносим все слоты (с оценками и всем прочим)
                new DQLUpdateBuilder(SessionDocumentSlot.class)
						.set(SessionDocumentSlot.document().s(), value(doc))
						.where(in(property(SessionDocumentSlot.id()), slotIds))
                .createStatement(session).execute();

                // создаем комиссию из всез товарищей, которые сейчас в ведомости
                this.setupRetakeDocumentComission(doc, session);
            }

            // удаляем исходную ведомость
            session.delete(source);
        }

        // важно, чтобы объекты были в базе
        session.flush();

        // чистим все комиссии (из всех ведомостей)
        ISessionCommissionDAO.instance.get().doCleanUp();
    }

	@Override
	public String regElementTitleWithFCATypes(SessionRetakeDocument document)
	{
		final String slotAlias = "slot";
		final String wpeCActionAlias = "wpeCAction";
		final String fcaTypeAlias = "fcaType";
		final List<String> fcaTypes = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, slotAlias)
				.where(eq(property(slotAlias, SessionDocumentSlot.document()), value(document)))
				.joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias(slotAlias), wpeCActionAlias)
				.joinEntity(wpeCActionAlias, DQLJoinType.inner, EppFControlActionType.class, fcaTypeAlias,
							eq(property(wpeCActionAlias, EppStudentWpeCAction.type()), property(fcaTypeAlias, EppFControlActionType.eppGroupType())))
				.column(property(fcaTypeAlias, EppFControlActionType.title()))
				.predicate(DQLPredicateType.distinct)
				.createStatement(getSession()).list();
		return document.getRegistryElementPart().getTitleWithNumber() + ", " + String.join(", ", fcaTypes);
	}

	@Override
	public void createThemesByLastWpeThemes(SessionRetakeDocument retakeDocument)
	{
		List<SessionDocumentSlot> slots = IUniBaseDao.instance.get().getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), retakeDocument);
		List<EppStudentWpeCAction> wpeCActions = Lists.transform(slots, SessionDocumentSlot::getStudentWpeCAction);
		List<SessionProjectTheme> existingThemes = IUniBaseDao.instance.get().getList(SessionProjectTheme.class, SessionProjectTheme.slot().studentWpeCAction(), wpeCActions);
		for (SessionDocumentSlot retakeDocSlot : slots)
		{
			if (CollectionUtils.exists(existingThemes, theme -> ((SessionProjectTheme)theme).getSlot().equals(retakeDocSlot)))
				continue;
			Optional<SessionProjectTheme> wpeTheme = existingThemes.stream()
					.filter(theme -> theme.getSlot().getStudentWpeCAction().equals(retakeDocSlot.getStudentWpeCAction()))
					.max(Comparator.comparing((SessionProjectTheme theme) -> theme.getSlot().getDocument().getFormingDate()));
			if (wpeTheme.isPresent())
			{
				SessionProjectTheme retakeDocTheme = new SessionProjectTheme();
				retakeDocTheme.setSlot(retakeDocSlot);
				retakeDocTheme.setTheme(wpeTheme.get().getTheme());
				save(retakeDocTheme);
			}
		}
		getSession().flush();
		getSession().clear();
	}


	@Override
    public void doSplitByComission(final Long orgUnitId, final List<Long> selectedIds)
    {
        final Session session = this.lock(orgUnitId, selectedIds);

        // обрабатываем каждый документ отдельно
        for (final Long sourceDocId: selectedIds)
        {
            final SessionRetakeDocument source = this.get(SessionRetakeDocument.class, sourceDocId);

            final Map<Set<Long>, Set<Long>> ppsSet2slotIdsMap = SafeMap.get(HashSet.class);
            {
                final List<Object[]> rows = new DQLSelectBuilder()
						.fromEntity(SessionDocumentSlot.class, "s")
						.where(eq(property(SessionDocumentSlot.document().fromAlias("s")), value(source)))
						.joinEntity("s", DQLJoinType.left, SessionComissionPps.class, "pps", eq(property(SessionComissionPps.commission().fromAlias("pps")), property(SessionDocumentSlot.commission().fromAlias("s"))))
						.column(property(SessionDocumentSlot.id().fromAlias("s")))
						.column(property(SessionComissionPps.pps().id().fromAlias("pps")))
                .createStatement(session).list();

                final Map<Long, Set<Long>> slotId2ppsIdsMap = SafeMap.get(HashSet.class);
                for (final Object[] row: rows) { slotId2ppsIdsMap.get(row[0]).add((Long)row[1]); }
                for (final Map.Entry<Long, Set<Long>> e: slotId2ppsIdsMap.entrySet()) { ppsSet2slotIdsMap.get(e.getValue()).add(e.getKey()); }
            }

            for (final Set<Long> slotIds: ppsSet2slotIdsMap.values())
            {
                // создаем ведомость перестач
                final SessionRetakeDocument doc = this.createRetakeDocument(source, session);

                // переносим все слоты (с оценками и всем прочим)
                new DQLUpdateBuilder(SessionDocumentSlot.class)
						.set(SessionDocumentSlot.document().s(), value(doc))
						.where(in(property(SessionDocumentSlot.id()), slotIds))
                .createStatement(session).execute();

                // создаем комиссию из всез товарищей, которые сейчас в ведомости
                this.setupRetakeDocumentComission(doc, session);
            }

            // удаляем исходную ведомость
            session.delete(source);
        }

        // важно, чтобы объекты были в базе
        session.flush();

        // чистим все комиссии (из всех ведомостей)
        ISessionCommissionDAO.instance.get().doCleanUp();
    }


    protected SessionRetakeDocument createRetakeDocument(final SessionRetakeDocument source, final Session session)
    {
        // создаем комиссию из всез товарищей (из всех товарищей из ведомости)
        final SessionComission docCommission = ISessionCommissionDAO.instance.get().saveOrUpdateCommission(
                null,
                this.getList(PpsEntry.class, "id",
                        new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "rel")
								.column(property(SessionComissionPps.pps().id().fromAlias("rel")))
								.predicate(DQLPredicateType.distinct)
								.where(eq(property(SessionComissionPps.commission().fromAlias("rel")), value(source.getCommission())))
                        .createStatement(session).<Long>list()
                )
        );

        // создаем ведомость
        final SessionRetakeDocument doc = setupAndSaveDocument(source.getSessionObject(), source.getRegistryElementPart(), docCommission, session);

        // важно, чтобы объекты были в базе
        session.flush();
        return doc;
    }

	/**
	 * Обновить комиссию ведомости пересдач - оставить ППС, которые уже в ней есть, плюс добавить ППС из комиссий всех слотов ведомости. ППС не будут дублироваться.
	 * @param doc Ведомость пересдач
	 * @param session Хибернатовская сессия
	 */
    protected void setupRetakeDocumentComission(final SessionRetakeDocument doc, final Session session)
    {
		DQLSelectBuilder commissionsFromDocSlots = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "y")
				.column(property(SessionDocumentSlot.commission().id().fromAlias("y")))
				.where(eq(property(SessionDocumentSlot.document().fromAlias("y")), value(doc)));
		List<Long> ppsFromDocAndSlotsIds = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "rel")
			.column(property(SessionComissionPps.pps().id().fromAlias("rel")))
			.predicate(DQLPredicateType.distinct)
			.where(or(
					eq(property(SessionComissionPps.commission().fromAlias("rel")), value(doc.getCommission())),
					in(property(SessionComissionPps.commission().id().fromAlias("rel")), commissionsFromDocSlots.buildQuery())
			))
			.createStatement(session).<Long>list();
		List<PpsEntry> ppsList = getList(PpsEntry.class, PpsEntry.id(), ppsFromDocAndSlotsIds);
        ISessionCommissionDAO.instance.get().saveOrUpdateCommission(doc.getCommission(), ppsList);
        // TODO: слить комиссии по всем слотам (удалить дубли).
		// Т.е. если в слотах из ведомости1 и ведомости2 были идентичные комиссии, то должна остаться одна, и все соответствующие слоты должны ссылаться уже на нее.
    }

    protected Session lock(final Long orgUnitId, final List<Long> selectedIds)
    {
        final Session session = this.lock(orgUnitId);

        // проверяем закрытость
        if (
                new DQLSelectBuilder()
						.fromEntity(SessionRetakeDocument.class, "srd")
						.column(property("srd.id"))
						.where(in(property("srd.id"), selectedIds))
						.where(isNotNull(property(SessionRetakeDocument.closeDate().fromAlias("srd"))))
                .createStatement(session).list().size() > 0
        ) {
            throw new ApplicationException("Нельзя изменять закрытые ведомости.");
        }

        // проверяем наличие оценок
        if (
                new DQLSelectBuilder()
						.fromEntity(SessionMark.class, "m")
						.column(property(SessionMark.id().fromAlias("m")))
						.where(in(property(SessionMark.slot().document().id().fromAlias("m")), selectedIds))
                .createStatement(session).list().size() > 0
        ) {
            throw new ApplicationException("Нельзя изменять ведомости с оценками.");
        }

        return session;
    }

    protected Session lock(final Long orgUnitId)
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, "session-retake-" + String.valueOf(orgUnitId));
        return session;
    }
}

