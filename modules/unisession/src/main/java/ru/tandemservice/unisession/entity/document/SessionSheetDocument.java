package ru.tandemservice.unisession.entity.document;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.dao.sheet.ISessionSheetDAO;
import ru.tandemservice.unisession.entity.document.gen.SessionSheetDocumentGen;
import ru.tandemservice.unisession.sec.ISessionSecDao;

/**
 * Экзаменационный лист
 */
public class SessionSheetDocument extends SessionSheetDocumentGen
{
    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeTitle()
    {
        return "Экзаменационный лист №" + this.getNumber();
    }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeShortTitle()
    {
        return "Экз. лист №" + this.getNumber();
    }

    @Override
    public INumberGenerationRule<SessionSheetDocument> getNumberGenerationRule()
    {
        return ISessionSheetDAO.instance.get().getNumberGenerationRule();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return ISessionSecDao.instance.get().getSecLocalEntities(this);
    }

    public String getNumberPrefix()
    {
        final Calendar formingDate = Calendar.getInstance();
        formingDate.setTime(getFormingDate());
        final int year = formingDate.get(Calendar.YEAR);

        return (year % 100) + ".";
    }

    @Override
    public OrgUnit getGroupOu()
    {
        return getOrgUnit();
    }
}