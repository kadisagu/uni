/* $Id$ */
package ru.tandemservice.unisession.component.log.SessionDocumentLogView;

import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionTransferOperation;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Arrays;
import java.util.List;

/**
 * @author oleyba
 * @since 6/28/11
 */
public class DAO extends ru.tandemservice.uni.component.log.EntityLogViewBase.DAO
{
    private static final List<String> _entityClassNames = Arrays.asList(
            SessionDocument.ENTITY_CLASS,
            SessionDocumentSlot.ENTITY_CLASS,
            SessionTransferOperation.ENTITY_CLASS,
            SessionProjectTheme.ENTITY_CLASS,
            SessionMark.ENTITY_CLASS,
            SessionStudentNotAllowed.ENTITY_CLASS,
            SessionStudentNotAllowedForBulletin.ENTITY_CLASS
    );

    @Override
    protected List<String> getEntityClassNames()
    {
        return _entityClassNames;
    }
}
