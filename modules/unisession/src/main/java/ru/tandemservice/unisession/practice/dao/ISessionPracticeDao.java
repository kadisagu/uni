/* $Id$ */
package ru.tandemservice.unisession.practice.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.SessionMarkableDocHolder;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

import java.util.Collection;
import java.util.Map;

/**
 * Коннектор Практик к сессии
 *
 * @author Nikolay Fedorovskih
 * @since 05.11.2014
 */
public interface ISessionPracticeDao
{
    SpringBeanCache<ISessionPracticeDao> instance = new SpringBeanCache<>(ISessionPracticeDao.class.getName());

    /**
     * @return Выводить ли колонку «Оценка руководителя» на карточке ведомости и на форме выставления оценок.
     */
    boolean showPracticeTutorMarkColumnInBulletin(SessionMarkableDocHolder.IMarkableDoc document);

    /**
     * Получение оценок по практике для набора МСРП
     *
     * @param studentWpeCActionIds список идентификаторов МСРП
     * @return id МСРП -> оценка руководителя практики из направления на практику
     */
    Map<Long, IPracticeResult> getPracticeTutorMarks(Collection<Long> studentWpeCActionIds);

    /**
     * Интерфейс для передачи оценки и состояния направления на практику.
     */
    interface IPracticeResult
    {
        /**
         * Закрыто ли направление на практику
         *
         * @return false - если направление на практику еще на этапе согласования или проверки результатов.
         */
        boolean isClosed();

        /**
         * Оценка за прохождение практики
         */
        SessionMarkGradeValueCatalogItem getMark();
    }
}