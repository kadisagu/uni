/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsAdd;

import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.SessionReportUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 1/24/12
 */
public class SessionReportDebtorsDAO extends UniBaseDao implements ISessionReportDebtorsDAO
{
    @Override
    public UnisessionDebtorsReport createStoredReport(SessionReportDebtorsAddUI model)
    {
        UnisessionDebtorsReport report = new UnisessionDebtorsReport();

        final UniSessionFilterAddon filterAddon = model.getSessionFilterAddon();

        report.setFormingDate(new Date());
        report.setExecutor(PersonSecurityUtil.getExecutor());

        final SessionObject sessionObject = model.getSessionObject();
        report.setSessionObject(sessionObject);

        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COURSE, UnisessionDebtorsReport.P_COURSE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_GROUP_WITH_NOGROUP, UnisessionDebtorsReport.P_GROUPS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_STUDENT_STATUS, UnisessionDebtorsReport.P_STUDENT_STATUS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_TARGET_ADMISSION, UnisessionDebtorsReport.P_TARGET_ADMISSION, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COMPENSATION_TYPE, UnisessionDebtorsReport.P_COMPENSATION_TYPE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_FORM, UnisessionDebtorsReport.P_DEVELOP_FORM, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_CONDITION, UnisessionDebtorsReport.P_DEVELOP_CONDITION, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_PERIOD, UnisessionDebtorsReport.P_DEVELOP_PERIOD, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_TECH, UnisessionDebtorsReport.P_DEVELOP_TECH, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_EDUCATION_LEVELS, UnisessionDebtorsReport.P_EDUCATION_LEVEL_HIGH_SCHOOL, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_REGISTRY_STRUCTURE, UnisessionDebtorsReport.P_REGISTRY_STRUCTURE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_CONTROL_FORM, UnisessionDebtorsReport.P_CONTROL_ACTION_TYPES, "title");


        DatabaseFile content = new DatabaseFile();
        content.setContent(print(model, report, sessionObject));
        save(content);
        report.setContent(content);

        save(report);

        return report;
    }

    private byte[] print(SessionReportDebtorsAddUI model, UnisessionDebtorsReport report, SessionObject sessionObject)
    {
        final UniSessionFilterAddon filterAddon = model.getSessionFilterAddon();


        if (null == sessionObject)
            throw new IllegalStateException("SessionObject is required in report parameters.");

        UnisessionTemplate templateItem = getCatalogItem(UnisessionTemplate.class, UnisessionCommonTemplateCodes.DEBTORS_REPORT);
        if (templateItem == null) {
            throw new RuntimeException("Печатный шаблон есть, но не импортирован в справочник шаблонов.");
        }
        RtfDocument rtf = new RtfReader().read(templateItem.getContent());

        List<String[]> header = new ArrayList<>();


        header.add(new String[]{"Дата формирования отчета:", DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date())});
        header.add(new String[]{"Учебный год:", sessionObject.getEducationYear().getTitle()});
        header.add(new String[]{"Семестр:", sessionObject.getYearDistributionPart().getTitle()});
        if (null != report.getPerformDateFrom()) {
            header.add(new String[]{"Дата сдачи мероприятия в ведомости с:", DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getPerformDateFrom())}); }
        if (null != report.getPerformDateTo()) {
            header.add(new String[]{"Дата сдачи мероприятия в ведомости по:", DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getPerformDateTo())});
        }
        if (null != report.getRegistryStructure()) {
            header.add(new String[]{"Вид мероприятия реестра:", report.getRegistryStructure()}); }
        if (null != report.getControlActionTypes()) {
            header.add(new String[]{"Форма контроля:", report.getControlActionTypes()}); }
        if (null != report.getCompensationType()) {
            header.add(new String[]{"Вид возмещения затрат:", report.getCompensationType()}); }
        if (null != report.getEducationLevelHighSchool()) {
            header.add(new String[]{"Направление подготовки (специальность):", report.getEducationLevelHighSchool()}); }
        if (null != report.getDevelopForm()) {
            header.add(new String[]{"Форма освоения:", report.getDevelopForm()}); }
        if (null != report.getDevelopCondition()) {
            header.add(new String[]{"Условие освоения:", report.getDevelopCondition()}); }
        if (null != report.getDevelopTech()) {
            header.add(new String[]{"Технология освоения:", report.getDevelopTech()}); }
        if (null != report.getDevelopPeriod()) {
            header.add(new String[]{"Срок освоения:", report.getDevelopPeriod()}); }
        if (null != report.getCourse()) {
            header.add(new String[]{"Курс:", report.getCourse()}); }
        if (null != report.getGroups()) {
            header.add(new String[]{"Группа:", report.getGroups()}); }
        if (null != report.getStudentStatus()) {
            header.add(new String[]{"Состояние студента:", report.getStudentStatus()}); }


        if (null != report.getCustomState()) {
            header.add(new String[]{"Дополнительный статус:", report.getCustomState()}); }
        if (null != report.getTargetAdmission()) {
            header.add(new String[]{"Целевой прием:", report.getTargetAdmission()}); }



        new RtfTableModifier().put("TH", header.toArray(new String[header.size()][])).modify(rtf);

        String studentAlias = "s";

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionMark.class, "m")
            .where(eq(property(SessionMark.cachedMarkPositiveStatus().fromAlias("m")), value(Boolean.FALSE)))
            .where(eq(property(SessionMark.slot().inSession().fromAlias("m")), value(Boolean.FALSE)))
            .fromEntity(SessionStudentGradeBookDocument.class, "doc")
            .where(eq(property(SessionMark.slot().document().fromAlias("m")), property("doc")))
            .fromEntity(Student.class, studentAlias)
            .where(eq(property(SessionStudentGradeBookDocument.student().fromAlias("doc")), property(studentAlias)))
            .where(isNull(property(SessionMark.slot().studentWpeCAction().removalDate().fromAlias("m"))))
            .where(eq(property(Student.archival().fromAlias(studentAlias)), value(Boolean.FALSE)))
            .where(eq(property(Student.status().active().fromAlias(studentAlias)), value(Boolean.TRUE)))
            .joinEntity(studentAlias, DQLJoinType.left, Group.class, "g", eq(property(Student.group().fromAlias(studentAlias)), property("g")))
            .joinPath(DQLJoinType.inner, SessionMark.slot().studentWpeCAction().fromAlias("m"), "wpeCa")
            .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("wpeCa"), "wpe")
            .joinEntity("wpeCa", DQLJoinType.inner, EppFControlActionType.class, "ca", eq(property("wpeCa", EppStudentWpeCAction.type()), property("ca", EppFControlActionType.eppGroupType())))
            .column(property(Student.id().fromAlias(studentAlias)))
            .column(property(Student.person().identityCard().fullFio().fromAlias(studentAlias)))
            .column(property(Student.compensationType().code().fromAlias(studentAlias)))
            .column(property(Student.course().title().fromAlias(studentAlias)))
            .column(property(Group.title().fromAlias("g")))
            .column(property(EppStudentWpeCAction.studentWpe().term().intValue().fromAlias("wpeCa")))
            .column(property(EppStudentWpeCAction.studentWpe().year().educationYear().id().fromAlias("wpeCa")))
            .column(property(EppStudentWpeCAction.studentWpe().part().id().fromAlias("wpeCa")))
            .column(property("ca"))
            .column(property(EppStudentWpeCAction.studentWpe().registryElementPart().fromAlias("wpeCa")))
            .column(property(SessionMark.id().fromAlias("m")))
            .column(property(SessionMark.slot().studentWpeCAction().id().fromAlias("m")))
            ;

        if (null != model.getSessionObject())
            dql.where(eq(property(studentAlias, Student.educationOrgUnit().groupOrgUnit()), value(model.getSessionObject().getOrgUnit())));

        dql.where(betweenDays(SessionMark.performDate().fromAlias("m"), model.isPerformDateFromActive() ? model.getPerformDateFrom() : null,
                model.isPerformDateToActive()? model.getPerformDateTo() : null));



        filterAddon.applyFilters(dql, "wpeCa");

        dql.order(property(Student.course().intValue().fromAlias(studentAlias)));
        dql.order(property(Group.title().fromAlias("g")));
        dql.order(property(Student.person().identityCard().fullFio().fromAlias(studentAlias)));
        dql.order(property(SessionMark.slot().studentWpeCAction().type().priority().fromAlias("m")));
        dql.order(property(SessionMark.slot().studentWpeCAction().studentWpe().registryElementPart().registryElement().title().fromAlias("m")));


        Map<Long, StudentData> studentRowMap = new LinkedHashMap<>();
        for (Object[] row : dql.createStatement(getSession()).<Object[]>list()) {

            Long studentId = (Long) row[0];
            StudentData studentData = studentRowMap.get(studentId);
            if (null == studentData) {
                studentRowMap.put(studentId, studentData = new StudentData());
                studentData.setRow(new String[] {
                    "",
                    (String) row[1],
                    CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(row[2]) ? "Б" : "К",
                    (String) row[3],
                    (String) row[4],
                    "",
                    ""
                });
            }
            int gridTerm = (Integer) row[5];
            if (sessionObject.getEducationYear().getId().equals(row[6]) && sessionObject.getYearDistributionPart().getId().equals(row[7]))
                studentData.setCurrentTerm(gridTerm);
            Map<EppFControlActionType, List<EppRegistryElementPart>> debtMap = studentData.getDebtMap().get(gridTerm);
            if (null == debtMap)
                studentData.getDebtMap().put(gridTerm, debtMap = new LinkedHashMap<>());
            SafeMap.safeGet(debtMap, (EppFControlActionType) row[8], ArrayList.class).add((EppRegistryElementPart) row[9]);
        }

        int studentCount = 1;
        List<String[]> studentTableData = new ArrayList<>();
        for (StudentData studentData : studentRowMap.values()) {
            int maxTerm = studentData.getCurrentTerm() == 0 ? 100 : studentData.getCurrentTerm();
            studentData.getRow()[0] = String.valueOf(studentCount++);
            StringBuilder builder = new StringBuilder();
            for (int term = 1; term < maxTerm; term++) {
                printDebts(builder, term, studentData.getDebtMap().get(term));
            }
            studentData.getRow()[5] = builder.toString();
            if (studentData.getCurrentTerm() != 0) {
                builder = new StringBuilder();
                printDebts(builder, studentData.getCurrentTerm(), studentData.getDebtMap().get(studentData.getCurrentTerm()));
                studentData.getRow()[6] = builder.toString();
            }
            else
                studentData.getRow()[6] = "";
            studentTableData.add(studentData.getRow());
        }

        new RtfTableModifier()
            .put("T", studentTableData.toArray(new String[studentTableData.size()][]))
            .put("T", new RtfRowIntercepterBase()
            {
                @Override
                public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
                {
                    List<IRtfElement> list = new ArrayList<>();
                    IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                    text.setRaw(true);
                    list.add(text);
                    return list;
                }
            })
            .modify(rtf);

        // должность и ФИО руководителя подразделения
        RtfInjectModifier modifier = new RtfInjectModifier();
        SessionReportManager.addOuLeaderData(modifier, sessionObject.getOrgUnit(), "ouleader", "FIOouleader");
        modifier.modify(rtf);

        return RtfUtil.toByteArray(rtf);
    }

    private void printDebts(StringBuilder builder, int term, Map<EppFControlActionType, List<EppRegistryElementPart>> debtMap)
    {
        if (null == debtMap)
            return;
        if (builder.length() > 0)
            builder.append("\\par ");
        builder.append(term).append(" семестр: ");
        boolean first = true;
        for (Map.Entry<EppFControlActionType, List<EppRegistryElementPart>> e : debtMap.entrySet()) {
            if (!first)
                builder.append(", ");
            first = false;
            builder
                .append(e.getValue().size())
                .append(" ")
                .append(e.getKey().getAbbreviation())
                .append(" (")
                .append(UniStringUtils.join(e.getValue(), EppRegistryElementPart.title().s(), ", "))
                .append(")")
                ;
        }
    }

    private static class StudentData
    {
        String[] row;

        int currentTerm;
        Map<Integer, Map<EppFControlActionType, List<EppRegistryElementPart>>> debtMap = new HashMap<>();

        private StudentData() { }
        public int getCurrentTerm() { return currentTerm; }
        public void setCurrentTerm(int currentTerm) { this.currentTerm = currentTerm; }
        public Map<Integer, Map<EppFControlActionType, List<EppRegistryElementPart>>> getDebtMap() { return debtMap; }
        public String[] getRow() { return row; }
        public void setRow(String[] row) { this.row = row; }
    }
}
