package ru.tandemservice.unisession.entity.mark;

import ru.tandemservice.unisession.entity.mark.gen.SessionSlotRegularMarkGen;

/**
 * Оценка студента в сессию (регулярная)
 *
 * показщывает, что данная оценка выставлена человеком
 */
public abstract class SessionSlotRegularMark extends SessionSlotRegularMarkGen
{
    public abstract int getSessionMarkTypePriority();
    public abstract int getSessionMarkValuePriority();
    public abstract long getComparablePoints();

    public int SessionMarkDocumentPriority() {
        // ведомости должны идти впереди, однако нельзя здесь вызывать методы класса документа
        // иначе они будут подниматься как объекты (а в демоне это смерть от поноса)
        // надо подумать как лучше вычислять этот приоритет
        return 0; // пока возвращаем 0 как и раньше
    }
}