/* $*/

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinEdit;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.base.bo.PpsEntry.util.PpsEntrySelectBlockData;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.dao.comission.ISessionCommissionDAO;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 3/3/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setBulletin(this.getNotNull(SessionBulletinDocument.class, model.getBulletin().getId()));
        this.checkEditAllowed(model);
        List<PpsEntry> ppsList = CommonBaseUtil.getPropertiesList(this.getList(SessionComissionPps.class, SessionComissionPps.commission().s(), model.getBulletin().getCommission()), SessionComissionPps.pps().s());
        model.setPpsData(new PpsEntrySelectBlockData(model.getBulletin().getGroup().getTutorOu().getId()).setMultiSelect(true).setInitiallySelectedPps(ppsList));
    }

    @Override
    public void update(final Model model)
    {
        SessionBulletinDocument bulletinDocument = model.getBulletin();
        NamedSyncInTransactionCheckLocker.register(this.getSession(), String.valueOf(bulletinDocument.getId()));
        this.checkEditAllowed(model);
        bulletinDocument.setCommission(ISessionCommissionDAO.instance.get().saveOrUpdateCommission(bulletinDocument.getCommission(), model.getPpsList()));
        this.update(bulletinDocument);

        final List<SessionDocumentSlot> slotList = this.getList(SessionDocumentSlot.class, SessionDocumentSlot.document().s(), bulletinDocument);

        for (final SessionDocumentSlot slot : slotList) {
            slot.setCommission(ISessionCommissionDAO.instance.get().saveOrUpdateCommission(slot.getCommission(), model.getPpsList()));
            this.update(slot);
        }
    }

    protected void checkEditAllowed(final Model model)
    {
        // проверим, что все в порядке с ведомостью и ее можно редактировать
        // прямыми запросами, потому что состояние объектов в сессии могло устареть
        checkBulletinClosed(model);
        checkBulletinHasMark(model);
    }

    protected void checkBulletinClosed (final Model model) {
        DQLSelectBuilder check = new DQLSelectBuilder()
                .fromEntity(SessionBulletinDocument.class, "b")
                .where(eq(property(SessionBulletinDocument.id().fromAlias("b")), value(model.getBulletin().getId())))
                .where(isNotNull(property(SessionBulletinDocument.closeDate().fromAlias("b"))));
        Number checkResult = check.createCountStatement(new DQLExecutionContext(this.getSession())).<Number>uniqueResult();
        if (checkResult != null && checkResult.intValue() > 0) {
            throw new ApplicationException("Ведомость закрыта, редактирование невозможно.", true);
        }
    }

    protected void checkBulletinHasMark (Model model) {
        DQLSelectBuilder check = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "m")
                .where(eq(property(SessionMark.slot().document().id().fromAlias("m")), value(model.getBulletin().getId())));
        Number checkResult = check.createCountStatement(new DQLExecutionContext(this.getSession())).<Number>uniqueResult();
        if (checkResult != null && checkResult.intValue() > 0) {
            throw new ApplicationException("В ведомости уже есть оценки, редактирование невозможно.", true);
        }
    }
}
