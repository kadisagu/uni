/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import com.google.common.collect.Collections2;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.SessionReportResultsAddUI;
import ru.tandemservice.unisession.base.bo.SessionReport.util.SessionReportResultsGrouping;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;

import java.util.*;

/**
 * @author oleyba
 * @since 2/13/12
 */
public class SessionReportResultsUtils
{
    public static List<ISessionReportResultsDAO.ISessionReportResultsGrouping> getDefaultGroupingList()
    {
        return Arrays.asList(
            (ISessionReportResultsDAO.ISessionReportResultsGrouping) new Grouping(1, "sessionReportResultsGrouping.byGroup", "по группам")
            {
                @Override
                public List<ISessionReportResultsDAO.ISessionResultsReportTable> createTableList(SessionReportResultsAddUI model, Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> studentData)
                {
                    return Collections.singletonList((ISessionReportResultsDAO.ISessionResultsReportTable) new GroupReportTable(studentData));
                }
            },
            new Grouping(2, "sessionReportResultsGrouping.byCourse", "по курсам")
            {
                @Override
                public List<ISessionReportResultsDAO.ISessionResultsReportTable> createTableList(SessionReportResultsAddUI model, Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> studentData)
                {
                    return Collections.singletonList((ISessionReportResultsDAO.ISessionResultsReportTable) new CourseReportTable(studentData));
                }
            },
            new Grouping(3, "sessionReportResultsGrouping.byLevel", "по направлениям")
            {
                @Override
                public List<ISessionReportResultsDAO.ISessionResultsReportTable> createTableList(SessionReportResultsAddUI model, Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> studentData)
                {
                    return Collections.singletonList((ISessionReportResultsDAO.ISessionResultsReportTable) new EduLevelReportTable(studentData, model.isUseFullEduLevelTitle()));
                }

                @Override
                public boolean showEduLevelTitleOption()
                {
                    return true;
                }
            }
            ,
            new Grouping(4, "sessionReportResultsGrouping.byLevelAndCourse", "по направлениям (детализация)")
            {
                @Override
                public List<ISessionReportResultsDAO.ISessionResultsReportTable> createTableList(SessionReportResultsAddUI model, Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> studentData)
                {
                    Set<EducationLevelsHighSchool> levels = new HashSet<>(Collections2.transform(studentData, ISessionReportResultsDAO.ISessionResultsReportStudentData::getLevel));
                    List<ISessionReportResultsDAO.ISessionResultsReportTable> tableList = new ArrayList<>();

                    for (EducationLevelsHighSchool level : levels)
                    {
                        if (level.getEducationLevel().getEduProgramSubject() == null)
                            UserContext.getInstance().getErrorCollector().addError("Не найдено сопоставление новому направлению для НПм «" + level.getDisplayableTitle() + "», необходимо заполнить настройку «Связь НПв, НПм с направлениями ОП»");
                    }

                    if(UserContext.getInstance().getErrorCollector().hasErrors())
                        throw new ApplicationException();

                    Set<EduProgramSubject> subjects = Sets.newHashSet();

                    for (EducationLevelsHighSchool level : levels)
                        subjects.add(level.getEducationLevel().getEduProgramSubject());

                    for (EduProgramSubject subject : subjects)
                        tableList.add(new EduLevelAndCourseReportTable(subject, studentData));

                    Collections.sort(tableList, new Comparator<Object>()
                    {
                        @Override
                        public int compare(Object o1, Object o2)
                        {
                            return ((EduLevelAndCourseReportTable) o1).getSubject().getTitleWithCode().compareTo(((EduLevelAndCourseReportTable) o2).getSubject().getTitleWithCode());
                        }
                    });
                    return tableList;
                }
            }
            ,
            new Grouping(5, "sessionReportResultsGrouping.byLevelAndSpec", "по направлениям и профилям")
            {
                @Override
                public List<ISessionReportResultsDAO.ISessionResultsReportTable> createTableList(SessionReportResultsAddUI model, Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> studentData)
                {
                    return Collections.singletonList((ISessionReportResultsDAO.ISessionResultsReportTable) new EduSubjectAndSpecialisationReportTable(studentData, model.isUseFullEduLevelTitle()));
                }

                @Override
                public boolean showEduLevelTitleOption()
                {
                    return true;
                }
            }
            ,
            new Grouping(6, "sessionReportResultsGrouping.byLevelAndSpecAndCourse", "по направлениям и профилям (детализация)")
            {
                @Override
                public List<ISessionReportResultsDAO.ISessionResultsReportTable> createTableList(SessionReportResultsAddUI model, Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> studentData)
                {
                    Set<EducationLevelsHighSchool> levels = new HashSet<>(Collections2.transform(studentData, ISessionReportResultsDAO.ISessionResultsReportStudentData::getLevel));
                    List<ISessionReportResultsDAO.ISessionResultsReportTable> tableList = new ArrayList<>();
                    Map<EduProgramSubject, List<EduProgramSpecialization>> subjectListMap = Maps.newHashMap();

                    for (EducationLevelsHighSchool level : levels)
                    {
                        if(!SafeMap.safeGet(subjectListMap, level.getEducationLevel().getEduProgramSubject(), ArrayList.class).contains(level.getEducationLevel().getEduProgramSpecialization()))
                            subjectListMap.get(level.getEducationLevel().getEduProgramSubject()).add(level.getEducationLevel().getEduProgramSpecialization());
                    }

                    for(Map.Entry<EduProgramSubject, List<EduProgramSpecialization>> entry : subjectListMap.entrySet())
                    {
                        for(EduProgramSpecialization spec : entry.getValue())
                        {
                            tableList.add(new EduLevelAndSpecAndCourseReportTable(entry.getKey(), spec, studentData));
                        }
                    }

                    Collections.sort(tableList, new Comparator<Object>()
                    {
                        @Override
                        public int compare(Object o1, Object o2)
                        {
                            String title1 = ((EduLevelAndSpecAndCourseReportTable) o1).getSubject().getTitleWithCode() +
                                    (((EduLevelAndSpecAndCourseReportTable) o1).getSpec() != null ? " (" + ((EduLevelAndSpecAndCourseReportTable) o1).getSpec().getDisplayableTitle() + ")" : "");
                            String title2 = ((EduLevelAndSpecAndCourseReportTable) o2).getSubject().getTitleWithCode() +
                                    (((EduLevelAndSpecAndCourseReportTable) o2).getSpec() != null ? " (" + ((EduLevelAndSpecAndCourseReportTable) o2).getSpec().getDisplayableTitle() + ")" : "");

                            return title1.compareTo(title2);
                        }
                    });
                    return tableList;
                }
            }
        );
    }

    public static List<ISessionReportResultsDAO.ISessionResultsReportColumn> getDefaultReportColumnList()
    {
        final IUniBaseDao dao = IUniBaseDao.instance.get();

        final SessionMarkCatalogItem notAllowed = dao.getByNaturalId(new SessionMarkCatalogItem.NaturalId(SessionMarkStateCatalogItemCodes.NO_ADMISSION, SessionMarkStateCatalogItem.CATALOG_CODE));
        final SessionMarkCatalogItem unnecessary = dao.getByNaturalId(new SessionMarkCatalogItem.NaturalId(SessionMarkStateCatalogItemCodes.UNNECESSARY, SessionMarkStateCatalogItem.CATALOG_CODE));
        final SessionMarkCatalogItem n_zach = dao.getByNaturalId(new SessionMarkCatalogItem.NaturalId(SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO, SessionMarkGradeValueCatalogItem.CATALOG_CODE));
        final SessionMarkCatalogItem zach = dao.getByNaturalId(new SessionMarkCatalogItem.NaturalId(SessionMarkGradeValueCatalogItemCodes.ZACHTENO, SessionMarkGradeValueCatalogItem.CATALOG_CODE));
        final SessionMarkCatalogItem n_ud = dao.getByNaturalId(new SessionMarkCatalogItem.NaturalId(SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO, SessionMarkGradeValueCatalogItem.CATALOG_CODE));
        final SessionMarkCatalogItem ud = dao.getByNaturalId(new SessionMarkCatalogItem.NaturalId(SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO, SessionMarkGradeValueCatalogItem.CATALOG_CODE));
        final SessionMarkCatalogItem hor = dao.getByNaturalId(new SessionMarkCatalogItem.NaturalId(SessionMarkGradeValueCatalogItemCodes.HOROSHO, SessionMarkGradeValueCatalogItem.CATALOG_CODE));
        final SessionMarkCatalogItem otl = dao.getByNaturalId(new SessionMarkCatalogItem.NaturalId(SessionMarkGradeValueCatalogItemCodes.OTLICHNO, SessionMarkGradeValueCatalogItem.CATALOG_CODE));

        final EppGradeScale scale5 = dao.getByNaturalId(new EppGradeScale.NaturalId(EppGradeScaleCodes.SCALE_5));

        // Количество студентов к началу сессии - количество студентов полученных путем выше для данного типа группировки (для группы, для курса или для НПП).
        final StudentReportColumn allStudents = new StudentReportColumn(null)
        {
            @Override
            public boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student)
            {
                return true;
            }
        };
        //Обязаны сдавать экзамены - из числа студентов "Количество студентов к началу сессии"
        // строки считаем таких, у которых есть хотя бы одно МСРПпоФИК из набора (набор определен выше) в ведомости,
        // для которого (для такого МСРПпоФИК) в качестве итоговой оценки нет отметки не требующей пересдачи
        // (т.е. лишь бы в качестве итоговой оценки не было отметки не требующей пересдачи).
        final StudentReportColumn haveToPassExams = new StudentReportColumn(null)
        {
            @Override
            public boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student)
            {
                // извращенную логику обработки применяем отдельным запросом
                return student.isHaveToPassExams();
            }
        };
        //Допущено к экзаменам - "Обязаны сдавать экзамены" минус те студенты,
        // которые имеют глобальный недопуск к сессии в этой годо-части
        // или есть хотя бы одна итоговая оценка "Не допущен" по одному из МСРПпоФИК из набора.
        final StudentReportColumn allowedToPassExams = new StudentReportColumn(haveToPassExams)
        {
            @Override
            public boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student)
            {
                return student.isAllowedForSession() && student.getMarkCountMap().get(notAllowed) == 0;
            }
        };
        //Явилось на сессию по всем предметам -
        // из числа студентов "Допущено к экзаменам" строки считаем таких студентов, у которых
        // итоговой оценкой по всем МСРПпоФИК из набора является оценка
        // (зачет, незачет, 2, 3, 4 или 5)
        // или отметка "не должен сдавать".
        final StudentReportColumn appearedForAllExams = new StudentReportColumn(allowedToPassExams)
        {
            @Override
            public boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student)
            {
                int sum = student.getMarkCountMap().get(unnecessary)
                    + student.getMarkCountMap().get(n_zach)
                    + student.getMarkCountMap().get(zach)
                    + student.getMarkCountMap().get(n_ud)
                    + student.getMarkCountMap().get(ud)
                    + student.getMarkCountMap().get(hor)
                    + student.getMarkCountMap().get(otl);
                return student.isAllowedForSession() && student.getControlActionCount() == sum;
            }
        };
        //Не явилось хотя бы на один предмет:
        //    неуваж. абс. - количество студентов из попавших в "Допущено к экзаменам" строки,
        // у которых в качестве итоговой оценки есть хотя бы одна отметка,
        // отметки "не должен сдавать" при этом не учитываем,
        // с признаком "неуважительная неявка" (т.е. снят флаг "уважительная неявка" в справочнике у отметки)
        // по одному из МСРПпоФИК из набора. Т.е. посчитанные студенты сюда, не могут попасть в "уваж. абс.".
        final StudentReportColumn notAppearInvalid = new StudentReportColumn(allowedToPassExams)
        {
            @Override
            public boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student)
            {
                for (Map.Entry<SessionMarkCatalogItem, Integer> entry : student.getMarkCountMap().entrySet()) {
                    if (entry.getValue() > 0 && entry.getKey() instanceof SessionMarkStateCatalogItem && !SessionMarkStateCatalogItemCodes.UNNECESSARY.equals(entry.getKey().getCode()) && !((SessionMarkStateCatalogItem) entry.getKey()).isValid())
                        return true;
                }
                return false;
            }
        };
        //Не явилось хотя бы на один предмет:
        //    уваж. абс. - количество студентов из попавших в "Допущено к экзаменам" строки,
        // у которых в качестве итоговой оценки есть хотя бы одна отметка,
        // отметки "не должен сдавать" при этом не учитываем,
        // с признаком "уважительная неявка" по одному из МСРПпоФИК из набора
        // И при этом они (студенты) не попали в колонку "неуваж. абс.".
        final StudentReportColumn notAppearValid = new StudentReportColumn(allowedToPassExams)
        {
            @Override
            public boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student)
            {
                if (notAppearInvalid.check(student))
                    return false;
                for (Map.Entry<SessionMarkCatalogItem, Integer> entry : student.getMarkCountMap().entrySet()) {
                    if (entry.getValue() > 0 && entry.getKey() instanceof SessionMarkStateCatalogItem && !SessionMarkStateCatalogItemCodes.UNNECESSARY.equals(entry.getKey().getCode()) && ((SessionMarkStateCatalogItem) entry.getKey()).isValid())
                        return true;
                }
                return false;
            }
        };
        //Сдали в срок - количество студентов из числа "Допущено к экзаменам",
        // у которых по всем МСРПпоФИК из набора итоговые оценки в сессию (именно в сессию) положительные
        // или в качестве оценки отметка "не должен сдавать".
        final StudentReportColumn passedExamsInTime = new StudentReportColumn(allowedToPassExams)
        {
            @Override
            public boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student)
            {
                int count = 0;
                for (Map.Entry<SessionMarkCatalogItem, Integer> entry : student.getInSessionMarkCountMap().entrySet()) {
                    if (entry.getKey() instanceof SessionMarkStateCatalogItem && SessionMarkStateCatalogItemCodes.UNNECESSARY.equals(entry.getKey().getCode()))
                        count += entry.getValue();
                    if (entry.getKey() instanceof SessionMarkGradeValueCatalogItem && ((SessionMarkGradeValueCatalogItem)entry.getKey()).isPositive())
                        count += entry.getValue();
                }
                return student.isAllowedForSession() && student.getMarkCountMap().get(notAllowed) == 0 && student.getControlActionCount() == count;
            }
        };
        //Сдали все предметы положительно - количество студентов из числа "Допущено к экзаменам",
        // у которых по всем МСРПпоФИК из набора итоговые оценки положительные или в качестве оценки отметка "не должен сдавать".
        final StudentReportColumn passedExams = new StudentReportColumn(allowedToPassExams)
        {
            @Override
            public boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student)
            {
                int count = 0;
                for (Map.Entry<SessionMarkCatalogItem, Integer> entry : student.getMarkCountMap().entrySet()) {
                    if (entry.getKey() instanceof SessionMarkStateCatalogItem && SessionMarkStateCatalogItemCodes.UNNECESSARY.equals(entry.getKey().getCode()))
                        count += entry.getValue();
                    if (entry.getKey() instanceof SessionMarkGradeValueCatalogItem && ((SessionMarkGradeValueCatalogItem)entry.getKey()).isPositive())
                        count += entry.getValue();
                }
                return student.isAllowedForSession() && student.getMarkCountMap().get(notAllowed) == 0 && student.getControlActionCount() == count;
            }
        };
        //Из них по предметам:
        //    5 - количество студентов из числа "Сдали все предметы положительно", у которых по всем МСРПпоФИК из набора итоговые оценки одна из "зачет" или "5", или отметка "не должен сдавать".
        final StudentReportColumn all5 = new StudentReportColumn(passedExams)
        {
            @Override
            public boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student)
            {
                int sum = student.getMarkCountMap().get(unnecessary)
                    + student.getMarkCountMap().get(zach)
                    + student.getMarkCountMap().get(otl);
                return student.isAllowedForSession() && student.getMarkCountMap().get(notAllowed) == 0 &&  student.getControlActionCount() == sum;
            }
        };
        //    5 и 4 - количество студентов из числа "Сдали все предметы положительно", у которых по всем МСРПпоФИК из набора итоговые оценки одна из "зачет", "5" или "4", или отметка "не должен сдавать".
        final StudentReportColumn all54 = new StudentReportColumn(passedExams)
        {
            @Override
            public boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student)
            {
                int sum = student.getMarkCountMap().get(unnecessary)
                    + student.getMarkCountMap().get(zach)
                    + student.getMarkCountMap().get(hor)
                    + student.getMarkCountMap().get(otl);
                return student.isAllowedForSession() && student.getMarkCountMap().get(notAllowed) == 0 && student.getControlActionCount() == sum && student.getMarkCountMap().get(hor) > 0;
            }
        };
        //    5,4 и 3 - нужно из "Сдали все предметы положительно" вычесть число студентов попавших в "5" и "5 и 4".
        final StudentReportColumn all543 = new StudentReportColumn(passedExams)
        {
            @Override
            public boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student)
            {
                return student.isAllowedForSession() && student.getMarkCountMap().get(notAllowed) == 0 && !all5.check(student) && !all54.check(student);
            }
        };
        //Получили неудовлетворительно - количество студентов из числа "Допущено к экзаменам", у которых по одному из МСРПпоФИК из набора
        // есть итоговая оценка "2" или "незачет".
        final StudentReportColumn notPassedExams = new StudentReportColumn(allowedToPassExams)
        {
            @Override
            public boolean check(ISessionReportResultsDAO.ISessionResultsReportStudentData student)
            {
                return student.isAllowedForSession() && student.getMarkCountMap().get(notAllowed) == 0 && student.getMarkCountMap().get(n_ud) > 0 || student.getMarkCountMap().get(n_zach) > 0;
            }
        };
        //Общее количество оценок - по всем студентам из числа "Допущено к экзаменам" по всем МСРПпоФИК из набора
        // считаем количество МСРПпоФИК с итоговыми оценками по пятибалльной шкале
        // (другими словами, количество МСРПпоФИК, у которых итоговая оценка одна из "5", "4", "3" или "2").
        final CalculatedReportColumn totalMarks = new CalculatedReportColumn()
        {
            @Override
            public Double calculate(ISessionReportResultsDAO.ISessionResultsReportRow row)
            {
                int count = 0;
                for (ISessionReportResultsDAO.ISessionResultsReportStudentData student : row.getStudents())
                {
                    if(student.isAllowedForSession() && student.getMarkCountMap().get(notAllowed) == 0)
                        count += student.getScaleControlActionCountMap().get(scale5);
                }
                return (double) count;
            }
        };
        //Из них 5 (4, 3, 2) - из числа итоговых оценок "Общее количество оценок"
        // считаем те, которые "5" (4, 3, 2), соответственно.
        final CalculatedReportColumn c5 = new CalculatedReportColumn()
        {
            @Override
            public Double calculate(ISessionReportResultsDAO.ISessionResultsReportRow row)
            {
                int count = 0;
                for (ISessionReportResultsDAO.ISessionResultsReportStudentData student : row.getStudents())
                {
                    if(student.isAllowedForSession() && student.getMarkCountMap().get(notAllowed) == 0)
                        count += student.getMarkCountMap().get(otl);
                }
                return (double) count;
            }
        };
        final CalculatedReportColumn c4 = new CalculatedReportColumn()
        {
            @Override
            public Double calculate(ISessionReportResultsDAO.ISessionResultsReportRow row)
            {
                int count = 0;
                for (ISessionReportResultsDAO.ISessionResultsReportStudentData student : row.getStudents())
                {
                    if (student.isAllowedForSession() && student.getMarkCountMap().get(notAllowed) == 0)
                        count += student.getMarkCountMap().get(hor);
                }
                return (double) count;
            }
        };
        final CalculatedReportColumn c3 = new CalculatedReportColumn()
        {
            @Override
            public Double calculate(ISessionReportResultsDAO.ISessionResultsReportRow row)
            {
                int count = 0;
                for (ISessionReportResultsDAO.ISessionResultsReportStudentData student : row.getStudents())
                {
                    if (student.isAllowedForSession() && student.getMarkCountMap().get(notAllowed) == 0)
                        count += student.getMarkCountMap().get(ud);
                }
                return (double) count;
            }
        };
        final CalculatedReportColumn c2 = new CalculatedReportColumn()
        {
            @Override
            public Double calculate(ISessionReportResultsDAO.ISessionResultsReportRow row)
            {
                int count = 0;
                for (ISessionReportResultsDAO.ISessionResultsReportStudentData student : row.getStudents())
                {
                    if (student.isAllowedForSession() && student.getMarkCountMap().get(notAllowed) == 0)
                        count += student.getMarkCountMap().get(n_ud);
                }
                return (double) count;
            }
        };
        return Arrays.asList(
            (ISessionReportResultsDAO.ISessionResultsReportColumn) allStudents,
            haveToPassExams,
            allowedToPassExams,
            // от обязаны сдавать экзамены
            new PercentageReportColumn(haveToPassExams, allowedToPassExams),
            appearedForAllExams,
            // % считается от значения в строке "Допущено к экзаменам".
            new PercentageReportColumn(allowedToPassExams, appearedForAllExams),
            notAppearValid,
            // % считается от значения в строке "Допущено к экзаменам".
            new PercentageReportColumn(allowedToPassExams, notAppearValid),
            notAppearInvalid,
            // % считается от значения в строке "Допущено к экзаменам".
            new PercentageReportColumn(allowedToPassExams, notAppearInvalid),
            passedExamsInTime,
            passedExams,
            // от допущено к экзаменам
            new PercentageReportColumn(allowedToPassExams, passedExams),
            all5,
            // от сдали все предметы положительно
            new PercentageReportColumn(passedExams, all5),
            all54,
            // от сдали все предметы положительно
            new PercentageReportColumn(passedExams, all54),
            all543,
            // от сдали все предметы положительно
            new PercentageReportColumn(passedExams, all543),
            notPassedExams,
            totalMarks,
            c5,
            // % считаем от числа "Общее количество оценок".
            new PercentageReportColumn(totalMarks, c5),
            c4,
            new PercentageReportColumn(totalMarks, c4),
            c3,
            new PercentageReportColumn(totalMarks, c3),
            c2,
            new PercentageReportColumn(totalMarks, c2),
            //% успеваемости - ("Сдали все предметы положительно"*100)/"Обязаны сдавать экзамены".
            new PercentageReportColumn(haveToPassExams, passedExams)
        );
    }

    private static abstract class Grouping extends SessionReportResultsGrouping implements ISessionReportResultsDAO.ISessionReportResultsGrouping
    {
        protected Grouping(int num, String key, String title)
        {
            super(num, key, title);
        }

        @Override public boolean showEduLevelTitleOption() { return false; }
    }
}
