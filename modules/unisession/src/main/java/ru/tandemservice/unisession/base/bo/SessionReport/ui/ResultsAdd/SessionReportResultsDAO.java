/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableObject;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterItem;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.DebugUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearEducationProcessGen;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.SessionReportUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.util.results.SessionReportResultsUtils;
import ru.tandemservice.unisession.entity.allowance.ISessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.allowance.gen.ISessionStudentNotAllowedGen;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.report.UnisessionResultsReport;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/7/12
 */
public class SessionReportResultsDAO extends UniBaseDao implements ISessionReportResultsDAO
{
    @Override
    public UnisessionResultsReport createStoredReport(SessionReportResultsAddUI model, final ISessionReportResultsGrouping grp) throws Exception
    {
        final UnisessionResultsReport report = new UnisessionResultsReport();

        report.setFormingDate(new Date());
        report.setExecutor(PersonSecurityUtil.getExecutor());

        report.setOrgUnit(model.getOrgUnit());
        report.setEducationYear(model.getYear());
        report.setYearDistributionPart(model.getPart());

        final UniSessionFilterAddon filterAddon = model.getSessionFilterAddon();

        if (null == grp)
            throw new IllegalStateException();

        report.setGrouping(grp.getTitle());

        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DISC_KIND, UnisessionResultsReport.P_DISC_KINDS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COMPENSATION_TYPE, UnisessionResultsReport.P_COMPENSATION_TYPE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_GROUP_ORG_UNIT, UnisessionResultsReport.P_GROUP_ORG_UNIT, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_TERRITORIAL_ORG_UNIT, UnisessionResultsReport.P_TERRITORIAL_ORG_UNIT, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_FORM, UnisessionResultsReport.P_DEVELOP_FORM, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_CONDITION, UnisessionResultsReport.P_DEVELOP_CONDITION, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_TECH, UnisessionResultsReport.P_DEVELOP_TECH, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_PERIOD, UnisessionResultsReport.P_DEVELOP_PERIOD, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COURSE, UnisessionResultsReport.P_COURSE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_STUDENT_STATUS, UnisessionResultsReport.P_STUDENT_STATUS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_CUSTOM_STATE, UnisessionResultsReport.P_CUSTOM_STATE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_TARGET_ADMISSION, UnisessionResultsReport.P_TARGET_ADMISSION, "title");



        DatabaseFile content = new DatabaseFile();
        content.setContent(print(model, grp));
        save(content);
        report.setContent(content);

        save(report);

        return report;
    }

    /**
     * Проверка шкал оценок - может ли быть построен отчет с такими данными.
     * Продуктовая логика работает только в том случае, если все оценки из пятибалльной и "зачет-незачет" шкал,
     * поэтому если найдены оценки в другой шкале, в этом методе выдается сообщение об ошибке.
     * Соответственно, если переопределена логика работы отчета, нужно переопределить и логику работы этой проверки
     * @param students подготовленный набор данных о студентах и оценках, попавших в статистику отчета
     */
    protected void checkScales(Collection<ISessionResultsReportStudentData> students)
    {
        for (ISessionResultsReportStudentData student : students) {
            for (Map.Entry<EppGradeScale, Integer> entry : student.getScaleControlActionCountMap().entrySet()) {
                if (entry.getValue() > 0 && !EppGradeScaleCodes.SCALE_2.equals(entry.getKey().getCode()) && !EppGradeScaleCodes.SCALE_5.equals(entry.getKey().getCode()))
                    throw new ApplicationException("Отчет не может быть построен, т.к. у некоторых студентов есть мероприятия со шкалой оценок отличной от пятибалльной или зачет/незачет.");
            }
        }
    }

    /**
     * Отдельный метод для вычисления набора id студентов, которые обязаны сдавать экзамены.
     * В разных реализациях процесса этот набор может вычисляться по-разному -
     * опираться на подготовленные данные о студентах и оценках,
     * или обращаться к базе за дополнительными данными.
     * @param model параметры построения отчета
     * @param students подготовленный набор данных о студентах и оценках, попавших в статистику отчета
     * @return набор id студентов, которые обязаны сдавать экзамены.
     */
    protected Set<Long> prepareHaveToPassSet(final SessionReportResultsAddUI model, Collection<ISessionResultsReportStudentData> students)
    {
        final Collection<Long> ids = CollectionUtils.collect(students, ISessionResultsReportStudentData::getStudentId);

        final EducationYear educationYear = model.getYear();
        final EppYearEducationProcess eppYear = getByNaturalId(new EppYearEducationProcessGen.NaturalId(educationYear));

        /*
        Фильтруем студентов:

            отбрасываем архивных и неактивных
            фильтруем по выбранным значениям в параметрах отчета (вид затрат, ФУТС, курс, состояние, терр. подразделение - по текущим данным студента)
            фильтруем по параметру «Включать дисциплины» - берем только тех студентов, которые попали в ведомость по МСРП-ФК, актуальному на данный момент, в котором, соответственно, есть ссылка на исходную строку РУП, вид которой подходит под выбранное значение фильтра

        Таким образом получаем студентов, вошедших в статистику отчета.

        Для каждого из полученных студентов получаем набор МСРПпоФИК
            актуальные МСРПпоФИК
            по соответствующей годо-части, выбранной в параметрах отчета
            и соответствующие параметру "Включать дисциплины" (признак в РУПе - для строки РУПа, от которой было порождено МСРП)

        Количество студентов к началу сессии - количество студентов полученных путем выше для данного типа группировки (для группы, для курса или для НПП).

        Обязаны сдавать экзамены
            из числа студентов "Количество студентов к началу сессии" строки считаем таких, у которых
            есть хотя бы одно МСРПпоФИК из набора (набор определен выше) в ведомости
            для которого (для такого МСРПпоФИК) в качестве итоговой оценки нет отметки не требующей пересдачи (т.е. лишь бы в качестве итоговой оценки не было отметки не требующей пересдачи).
         */

        final Set<Long> haveToPass = new HashSet<>();
        BatchUtils.execute(ids, 256, elements -> {
            DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(Student.class, "s")
            .where(in(property(Student.id().fromAlias("s")), elements))
            .fromEntity(SessionDocumentSlot.class, "slot")
            .where(eq(property(SessionDocumentSlot.actualStudent().fromAlias("slot")), property("s")))
            .where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().year().fromAlias("slot")), value(eppYear)))
            .where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().part().fromAlias("slot")), value(model.getPart())))
            .fromEntity(SessionBulletinDocument.class, "b")
            .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("b")))
            .fromEntity(SessionDocumentSlot.class, "slot_g")
            .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")), property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot_g"))))
            .fromEntity(SessionStudentGradeBookDocument.class, "g")
            .where(eq(property(SessionDocumentSlot.document().fromAlias("slot_g")), property("g")))
            .joinEntity("slot_g", DQLJoinType.left, SessionMark.class, "m", eq(property(SessionMark.slot().fromAlias("m")), property("slot_g")))
            .joinEntity("m", DQLJoinType.left, SessionMarkStateCatalogItem.class, "mstate", eq(property(SessionMark.cachedMarkValue().fromAlias("m")), property("mstate")))
            .joinEntity("m", DQLJoinType.left, SessionMarkGradeValueCatalogItem.class, "mgrade", eq(property(SessionMark.cachedMarkValue().fromAlias("m")), property("mgrade")))
            .where(or(
                    isNull("m"),
                    eq(property(SessionMarkStateCatalogItem.remarkable().fromAlias("mstate")), value(Boolean.TRUE)),
                    isNotNull("mgrade")))
            .column("s.id")
            ;

            List<Long> filteredIds = dql.createStatement(getSession()).list();
            Debug.message("filteredIds.size = " + filteredIds.size());

            haveToPass.addAll(filteredIds);
        });
        return haveToPass;
    }

    private byte[] print(final SessionReportResultsAddUI model, final ISessionReportResultsGrouping grp) throws Exception
    {
        UnisessionTemplate templateItem = getCatalogItem(UnisessionTemplate.class, UnisessionCommonTemplateCodes.RESULTS_REPORT);
        if (templateItem == null) {
            throw new RuntimeException("Печатный шаблон есть, но не импортирован в справочник шаблонов.");
        }
        final RtfDocument template = new RtfReader().read(templateItem.getContent());

        final MutableObject result = new MutableObject();

        final Collection<ISessionResultsReportStudentData> students = prepareStudentData(model);

        DebugUtils.debug(logger, new DebugUtils.Section("checkScales")
        {
            @Override
            public void execute()
            {
                checkScales(students);
            }
        });

        DebugUtils.debug(logger, new DebugUtils.Section("filling rtf") {
            @Override public void execute() {
                List<ISessionResultsReportTable> tableList = grp.createTableList(model, students);

                for (ISessionResultsReportTable table : tableList)
                {
                    RtfDocument document = print(model, table, template.getClone());
                    if (null == result.getValue())
                        result.setValue(document);
                    else if (null != document)
                    {
                        RtfDocument currentDocument = (RtfDocument) result.getValue();
                        RtfUtil.modifySourceList(currentDocument.getHeader(), document.getHeader(), document.getElementList());
                        currentDocument.getElementList().addAll(document.getElementList());
                    }
                }
            }
        });



        if (null != result.getValue()) {
            final MutableObject bytes = new MutableObject();
            DebugUtils.debug(logger, new DebugUtils.Section("printing rtf to byte[]") {
                @Override public void execute() {
                    bytes.setValue(RtfUtil.toByteArray((RtfDocument) result.getValue()));
                }
            });
            return (byte[]) bytes.getValue();
        }
        else
            throw new ApplicationException("Нет данных для построения отчета.");
    }

    private RtfDocument print(SessionReportResultsAddUI model, ISessionResultsReportTable table, RtfDocument rtf)
    {
        table.modify(rtf);

        OrgUnit academy = TopOrgUnit.getInstance();
        InflectorVariant inflVariant = IUniBaseDao.instance.get().getCatalogItem(InflectorVariant.class, InflectorVariantCodes.RU_GENITIVE);

        final ICommonFilterItem compensationFilter = model.getSessionFilterAddon().getFilterItem(UniSessionFilterAddon.SETTINGS_NAME_COMPENSATION_TYPE);

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("vuzTitle", academy.getPrintTitle());
        if (model.getOrgUnit() == null)
            SharedRtfUtil.removeParagraphsWithTagsRecursive(rtf, Collections.singletonList("groupOrgUnitTitle"), false, false);
        else
            modifier.put("groupOrgUnitTitle", model.getOrgUnit().getPrintTitle());
        modifier.put("compType", compensationFilter.isEnableCheckboxChecked() ? "(" + ((CompensationType) compensationFilter.getValue()).getShortTitle() + ")" : "");
        modifier.put("season", StringUtils.trimToEmpty(DeclinableManager.instance().dao().getPropertyValue(model.getPart(), YearDistributionPart.DECLINATION_PROPERTY_SESSION_SHORT_TITLE, inflVariant)));
        modifier.put("eduYear", model.getYear().getTitle());
        modifier.put("executor", PersonSecurityUtil.getExecutor());
        modifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));

        SessionReportManager.addOuLeaderData(modifier, model.getOrgUnit(), "ouleader", "FIOouleader");
        modifier.modify(rtf);

        final List<ISessionResultsReportColumn> reportColumnList = SessionReportResultsUtils.getDefaultReportColumnList();
        RtfTableModifier tableModifier = new RtfTableModifier().put("T", table.getTableData(reportColumnList));
        tableModifier.modify(rtf);

        return rtf;
    }

    private Collection<ISessionResultsReportStudentData> prepareStudentData(final SessionReportResultsAddUI model) throws Exception
    {
        final List<SessionMarkCatalogItem> marks = getList(SessionMarkCatalogItem.class);
        final List<EppGradeScale> scales = getList(EppGradeScale.class);

        final Map<Long, StudentData> map = new HashMap<>();

        final EducationYear educationYear = model.getYear();


        final UniSessionFilterAddon filterAddon = model.getSessionFilterAddon();

        DebugUtils.debug(logger, new DebugUtils.Section("prepareStudentList")
        {
            @Override
            public void execute()
            {

                DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Student.class, "s")
                .joinEntity("s", DQLJoinType.left, Group.class, "grp", eq(property(Student.group().fromAlias("s")), property("grp")))
                .fromEntity(SessionDocumentSlot.class, "slot")
                .where(eq(property(SessionDocumentSlot.actualStudent().fromAlias("slot")), property("s")))
                .fromEntity(SessionBulletinDocument.class, "b")
                .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "wpca")
                .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("b")))
                .column("s.id")
                .column("grp")
                .column(property(SessionDocumentSlot.studentWpeCAction().studentWpe().course().fromAlias("slot")))
                .column(property(Student.educationOrgUnit().educationLevelHighSchool().fromAlias("s")))
                ;

                dql
                // активные и неархивные студенты
                .where(eq(property(Student.archival().fromAlias("s")), value(Boolean.FALSE)))
                .where(eq(property(Student.status().active().fromAlias("s")), value(Boolean.TRUE)))
                // только актуальные мероприятия, по которым студент включен в ведомость
                .where(isNull(property(SessionDocumentSlot.studentWpeCAction().removalDate().fromAlias("slot"))))
                .where(eq(property(SessionBulletinDocument.sessionObject().educationYear().fromAlias("b")), value(educationYear)))
                // обязательные параметры отчета - часть года, виды дисциплин
                .where(eq(property(SessionBulletinDocument.sessionObject().yearDistributionPart().fromAlias("b")), value(model.getPart())))
                ;

                filterAddon.applyFilters(dql, "wpca");

                for (Object row[] : dql.createStatement(getSession()).<Object[]>list()) {
                    final StudentData studentData = new StudentData((Long) row[0], (Group) row[1], (Course) row[2], (EducationLevelsHighSchool) row[3]);
                    map.put((Long) row[0], studentData);
                    for (SessionMarkCatalogItem mark : marks) {
                        studentData.getInSessionMarkCountMap().put(mark, 0);
                        studentData.getMarkCountMap().put(mark, 0);
                    }
                    for (EppGradeScale scale : scales)
                        studentData.getScaleControlActionCountMap().put(scale, 0);
                }
            }
        });

        DebugUtils.debug(logger, new DebugUtils.Section("prepareAllowance")
        {
            @Override
            public void execute()
            {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ISessionStudentNotAllowed.class, "na")
                .joinPath(DQLJoinType.inner, ISessionStudentNotAllowedGen.student().fromAlias("na"), "s")
                .column("s.id")
                ;

                dql
                // активные и неархивные студенты
                .where(eq(property(Student.archival().fromAlias("s")), value(Boolean.FALSE)))
                .where(eq(property(Student.status().active().fromAlias("s")), value(Boolean.TRUE)))
                .where(isNull(property(ISessionStudentNotAllowedGen.removalDate().fromAlias("na"))))

                .joinEntity("na", DQLJoinType.left, SessionStudentNotAllowed.class, "sna", eq(property("na"), property("sna")))
                .joinEntity("na", DQLJoinType.left, SessionStudentNotAllowedForBulletin.class, "snab", eq(property("na"), property("snab")))

                .where(or(eq(property(SessionStudentNotAllowed.session().educationYear().fromAlias("sna")), value(educationYear)),
                        eq(property(SessionStudentNotAllowedForBulletin.bulletin().sessionObject().educationYear().fromAlias("snab")), value(educationYear))))

                // обязательные параметры отчета - часть года
                .where(or(eq(property(SessionStudentNotAllowed.session().yearDistributionPart().fromAlias("sna")), value(model.getYear())),
                        eq(property(SessionStudentNotAllowedForBulletin.bulletin().sessionObject().yearDistributionPart().fromAlias("snab")), value(model.getPart()))))
                ;

                // деканаты
                final ICommonFilterItem groupOUFI = filterAddon.getFilterItem(UniSessionFilterAddon.SETTINGS_NAME_GROUP_ORG_UNIT);

                if (groupOUFI.isEnableCheckboxChecked())
                    dql.where(or(
                            in(property(SessionStudentNotAllowed.session().orgUnit().fromAlias("sna")), ((List) groupOUFI.getValue())),
                            in(property(SessionStudentNotAllowedForBulletin.bulletin().sessionObject().orgUnit().fromAlias("snab")), (List) groupOUFI.getValue())
                    ));

                for (Long id : dql.createStatement(getSession()).<Long>list()) {
                    StudentData student = map.get(id);
                    if (null != student)
                        student.allowedForSession = false;
                }
            }
        });

        DebugUtils.debug(logger, new DebugUtils.Section("prepareMarkData")
        {
            @Override
            public void execute()
            {
                final Map<Long, SessionMarkCatalogItem> markValueMap = new HashMap<>();
                for (SessionMarkCatalogItem mark : getList(SessionMarkCatalogItem.class)) {
                    markValueMap.put(mark.getId(), mark);
                }

                BatchUtils.execute(map.keySet(), 256, elements -> {
                    DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(Student.class, "s")
                    .where(in(property(Student.id().fromAlias("s")), elements))
                    .fromEntity(SessionDocumentSlot.class, "slot")
                    .where(eq(property(SessionDocumentSlot.actualStudent().fromAlias("slot")), property("s")))
                    .fromEntity(SessionStudentGradeBookDocument.class, "g")
                    .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("g")))
                    .joinEntity("slot", DQLJoinType.left, SessionMark.class, "m", eq(property(SessionMark.slot().fromAlias("m")), property("slot")))
                    .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "wpca")
                    .column("s.id")
                    .column(property(SessionDocumentSlot.studentWpeCAction().id().fromAlias("slot")))
                    .column(property(SessionDocumentSlot.inSession().fromAlias("slot")))
                    .column(property(SessionMark.cachedMarkValue().id().fromAlias("m")))
                    ;

                    // актуальные МСРП-ФК по году и части построения отчета (т.е. текущему году и выбранной части)
                    dql
                        .where(isNull(property(SessionDocumentSlot.studentWpeCAction().removalDate().fromAlias("slot"))))
                        .where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear().fromAlias("slot")), value(educationYear)))
                        .where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().part().fromAlias("slot")), value(model.getPart())))
                        ;



                    // виды дисциплин
                    filterAddon.applyFilters(dql, "wpca");
                    // нужно пропустить те мероприятия, которые стоят в ведомостях после указанного дедлайна
                    //Set<Long> skip = new HashSet<>();

                    dql.where(betweenDays(SessionMark.performDate().fromAlias("m"), model.isPerformDateFromActive() ? model.getPerformDateFrom() : null,
                            model.isPerformDateToActive()? model.getPerformDateTo() : null));

                    for (Object[] row : dql.createStatement(getSession()).<Object[]>list()) {

                        //if (skip.contains((Long) row[1])) {
                        //    continue;
                        //}

                        final StudentData student = map.get(row[0]);
                        Boolean inSession = (Boolean) row[2];
                        SessionMarkCatalogItem mark = markValueMap.get(row[3]);
                        SessionMarkGradeValueCatalogItem grade = (mark instanceof SessionMarkGradeValueCatalogItem) ? (SessionMarkGradeValueCatalogItem) mark : null;


                        if (!inSession) {
                            student.setControlActionCount(student.getControlActionCount() + 1); }
                        if (mark != null) {
                            if (inSession) {
                                student.getInSessionMarkCountMap().put(mark, student.getInSessionMarkCountMap().get(mark) + 1);
                            }
                            else {
                                student.getMarkCountMap().put(mark, student.getMarkCountMap().get(mark) + 1);
                                if (null != grade) {
                                    student.getScaleControlActionCountMap().put(grade.getScale(), student.getScaleControlActionCountMap().get(grade.getScale()) + 1);
                                }
                                student.setMarkCount(student.getMarkCount() + 1);
                            }
                        }
                    }
                });
            }
        });

        final ArrayList<ISessionResultsReportStudentData> students = new ArrayList<>(map.values());

        DebugUtils.debug(logger, new DebugUtils.Section("prepareHaveToPassSet")
        {
            @Override
            public void execute()
            {
                for (Long id : prepareHaveToPassSet(model, students))
                    map.get(id).setHaveToPassExams(true);
            }
        });

        return students;
    }

    private static class StudentData implements ISessionResultsReportStudentData
    {
        private Long studentId;

        private Group group;
        private Course course;
        private EducationLevelsHighSchool level;

        private int controlActionCount;
        private int markCount;

        private boolean allowedForSession = true;
        private boolean haveToPassExams = false;

        private Map<SessionMarkCatalogItem, Integer> markCountMap = new HashMap<>();
        private Map<SessionMarkCatalogItem, Integer> inSessionMarkCountMap = new HashMap<>();
        private Map<EppGradeScale, Integer> scaleControlActionCountMap = new HashMap<>();

        StudentData(Long studentId, Group group, Course course, EducationLevelsHighSchool level)
        {
            this.studentId = studentId;
            this.group = group;
            this.course = course;
            this.level = level;
        }

        @Override public Long getStudentId() { return studentId; }
        @Override public Group getGroup() { return group; }
        @Override public Course getCourse() { return course; }
        @Override public EducationLevelsHighSchool getLevel() { return level; }
        @Override public Map<SessionMarkCatalogItem, Integer> getMarkCountMap() { return markCountMap; }
        @Override public int getControlActionCount() { return controlActionCount; }
        public void setControlActionCount(int controlActionCount) { this.controlActionCount = controlActionCount; }
        @Override public int getMarkCount() { return markCount; }
        public void setMarkCount(int markCount) { this.markCount = markCount; }
        @Override public boolean isAllowedForSession() { return allowedForSession; }
        public void setAllowedForSession(boolean allowedForSession) { this.allowedForSession = allowedForSession; }
        @Override public Map<SessionMarkCatalogItem, Integer> getInSessionMarkCountMap() { return inSessionMarkCountMap; }
        @Override public Map<EppGradeScale, Integer> getScaleControlActionCountMap() { return scaleControlActionCountMap; }
        @Override public boolean isHaveToPassExams() { return haveToPassExams;}
        public void setHaveToPassExams(boolean haveToPassExams) { this.haveToPassExams = haveToPassExams; }
    }
}