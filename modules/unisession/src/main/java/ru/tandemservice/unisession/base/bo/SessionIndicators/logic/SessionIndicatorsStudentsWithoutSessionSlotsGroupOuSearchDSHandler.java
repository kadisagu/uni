/**
 *$Id$
 */
package ru.tandemservice.unisession.base.bo.SessionIndicators.logic;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementGen;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanRowGen;
import ru.tandemservice.unisession.base.bo.SessionIndicators.ui.StudentsWithoutSessionSlotsGroupOu.SessionIndicatorsStudentsWithoutSessionSlotsGroupOuUI;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 20.03.13
 */
public class SessionIndicatorsStudentsWithoutSessionSlotsGroupOuSearchDSHandler extends AbstractStudentSearchListDSHandler
{
    public static final long OPTION_ID_NO_BULLETINS = 0L;
    public static final long OPTION_ID_NO_DOCS = 1L;
    public static final long OPTION_ID_NO_FINAL_MARKS = 2L;

    private static final String STUDENT_2_SLOT_LIST_MAP = "student2slotListMap";

    public static final String VIEW_PROP_BROKEN_SLOT_LIST = "brokenSlotList";

    public SessionIndicatorsStudentsWithoutSessionSlotsGroupOuSearchDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);

        final DQLSelectBuilder slotDQL = getSlotDQL(context);
        if (null == slotDQL)
        {
            builder.where(isNull(property(Student.id().fromAlias(alias))));
            return;
        }

        final OrgUnit orgUnit = context.get(SessionIndicatorsStudentsWithoutSessionSlotsGroupOuUI.PROP_ORG_UNIT);

        // только неархивные студенты
        builder
                .where(eq(property(Student.archival().fromAlias(alias)), value(false)))
                .where(eq(property(Student.educationOrgUnit().groupOrgUnit().fromAlias(alias)), value(orgUnit)));

        // есть нужные слоты в сессии
        slotDQL
                .where(eq(property(EppStudentWpeCAction.studentWpe().student().fromAlias("cas")), property(alias)))
                .column(property("cas.id"));

        builder.where(exists(slotDQL.buildQuery()));
    }

    @Override
    protected void prepareWrappData(List<Long> recordIds, DSInput input, final ExecutionContext context)
    {
        super.prepareWrappData(recordIds, input, context);

        final Map<Long, List<EppStudentWpeCAction>> student2slotListMap = SafeMap.get(ArrayList.class);

        if (null == getSlotDQL(context))
            return;

        for (List<Long> elements : Lists.partition(recordIds, DQL.MAX_VALUES_ROW_NUMBER)) {
            final DQLSelectBuilder slotBuilder = getSlotDQL(context);

            slotBuilder.column(property("cas"));
            slotBuilder.where(in(EppStudentWpeCAction.studentWpe().student().id().fromAlias("cas"), elements));

            slotBuilder.fetchPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("cas"), "slot");
            slotBuilder.fetchPath(DQLJoinType.inner, EppStudentWorkPlanElement.sourceRow().fromAlias("slot"), "source");
            slotBuilder.fetchPath(DQLJoinType.inner, EppStudentWorkPlanElement.registryElementPart().fromAlias("slot"), "regelpart");
            slotBuilder.fetchPath(DQLJoinType.inner, EppRegistryElementPart.registryElement().fromAlias("regelpart"), "regel");

            slotBuilder.order(property(EppStudentWorkPlanElement.year().educationYear().intValue().fromAlias("slot")));
            slotBuilder.order(property(EppStudentWorkPlanElement.term().intValue().fromAlias("slot")));
            slotBuilder.order(property(EppStudentWpeCAction.type().priority().fromAlias("cas")));
            slotBuilder.order(property(EppWorkPlanRowGen.number().fromAlias("source")));
            slotBuilder.order(property(EppRegistryElementGen.title().fromAlias("regel")));
            slotBuilder.order(property("regel.id"));

            for (final EppStudentWpeCAction slot: slotBuilder.createStatement(context.getSession()).<EppStudentWpeCAction>list())
                student2slotListMap.get(slot.getStudentWpe().getStudent().getId()).add(slot);
        }

        context.put(STUDENT_2_SLOT_LIST_MAP, student2slotListMap);
    }

    @Override
    protected void wrap(DataWrapper wrapper, ExecutionContext context)
    {
        super.wrap(wrapper, context);

        final Map<Long, List<EppStudentWpeCAction>> student2slotListMap = context.get(STUDENT_2_SLOT_LIST_MAP);

        wrapper.setProperty(VIEW_PROP_BROKEN_SLOT_LIST, "");

        if (!student2slotListMap.containsKey(wrapper.getId()))
            return;

        final StringBuilder builder = new StringBuilder();

        long id = 0;
        for (final EppStudentWpeCAction cas: student2slotListMap.get(wrapper.getId()))
        {
            if (id != cas.getStudentWpe().getTerm().getId()) {
                id = cas.getStudentWpe().getTerm().getId();
                if (builder.length() > 0) { builder.append('\n'); }
            }
            builder.append(cas.getStudentWpe().getYear().getEducationYear().getTitle());
            builder.append(' ').append(cas.getStudentWpe().getPart().getShortTitleWithParent());
            builder.append(' ').append(cas.getStudentWpe().getTerm().getIntValue()).append(" с. (").append(cas.getStudentWpe().getCourse().getIntValue()).append(" к.").append(")");
            final EppWorkPlanRow sourceRow = cas.getStudentWpe().getSourceRow();
            if (null != sourceRow) {
                builder.append(' ').append(StringUtils.trimToEmpty(sourceRow.getNumber()));
                final String realTitle = sourceRow.getTitle();
                final int i = realTitle.indexOf(' ', 20);
                final String trimmedTitle = StringUtils.left(realTitle, (i < 1 ? 20 : i));
                if ((trimmedTitle.length()+3) < realTitle.length()) {
                    builder.append(' ').append(trimmedTitle).append("...");
                } else {
                    builder.append(' ').append(realTitle);
                }
            }
            builder.append(" (").append(cas.getStudentWpe().getRegistryElementPart().getNumber()).append("/").append(cas.getStudentWpe().getRegistryElementPart().getRegistryElement().getParts());
            builder.append(", ").append(UniEppUtils.formatLoad(cas.getStudentWpe().getRegistryElementPart().getRegistryElement().getSizeAsDouble(), false));
            builder.append(", ").append(cas.getType().getShortTitle()).append(")");
            builder.append("\n");
        }

        wrapper.setProperty(VIEW_PROP_BROKEN_SLOT_LIST, builder.toString());
    }

    protected DQLSelectBuilder getSlotDQL(ExecutionContext context)
    {
        final SessionObject sessionObject = context.get(SessionIndicatorsStudentsWithoutSessionSlotsGroupOuUI.PROP_SESSION_OBJECT);
        final DataWrapper option = context.get(SessionIndicatorsStudentsWithoutSessionSlotsGroupOuUI.PROP_SEARCH_OPTION);
        final Collection regEl = context.get(SessionIndicatorsStudentsWithoutSessionSlotsGroupOuUI.PROP_REGISTRY_ELEMENT_LIST);

        if (null == sessionObject || null == option)
            return null;

        final DQLSelectBuilder docDQL = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "sds").column(property("sds.id"))
                .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("sds")), property("cas")));

        if (OPTION_ID_NO_BULLETINS == option.getId())
            docDQL.joinEntity("sds", DQLJoinType.inner, SessionBulletinDocument.class, "b",
                eq(property(SessionDocumentSlot.document().fromAlias("sds")), property("b")));
        else if (OPTION_ID_NO_DOCS == option.getId()) {
            docDQL.joinEntity("sds", DQLJoinType.left, SessionBulletinDocument.class, "b",
                eq(property(SessionDocumentSlot.document().fromAlias("sds")), property("b")));
            docDQL.joinEntity("sds", DQLJoinType.left, SessionSimpleDocument.class, "doc",
                eq(property(SessionDocumentSlot.document().fromAlias("sds")), property("doc")));
            docDQL.where(or(isNotNull("b.id"), isNotNull("doc.id")));
        }
        else if (OPTION_ID_NO_FINAL_MARKS == option.getId()) {
            docDQL.joinEntity("sds", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "b",
                eq(property(SessionDocumentSlot.document().fromAlias("sds")), property("b")));
            docDQL.joinEntity("sds", DQLJoinType.inner, SessionMark.class, "m", eq(property(SessionMark.slot().fromAlias("m")), property("sds")));
        }

        final DQLSelectBuilder slotDQL = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, "cas")
                .where(isNull(property(EppStudentWpeCAction.removalDate().fromAlias("cas"))))
                .where(eq(property(EppStudentWpeCAction.studentWpe().part().fromAlias("cas")), value(sessionObject.getYearDistributionPart())))
                .where(eq(property(EppStudentWpeCAction.studentWpe().year().educationYear().current().fromAlias("cas")), value(true)))
                .where(notExists(docDQL.buildQuery()));

        if (!CollectionUtils.isEmpty(regEl))
            slotDQL.where(in(property(EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().fromAlias("cas")), regEl));

        return slotDQL;
    }
}
