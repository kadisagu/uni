/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupMarksAdd;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.DebugUtils;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.util.SessionReportUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport;

import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Сводная ведомость группы (по всем полученным студентами оценкам)
 * @author oleyba
 * @since 2/21/12
 */
public class SessionReportGroupMarksDAO extends UniBaseDao implements ISessionReportGroupMarksDAO
{
    @Override
    public UnisessionGroupMarksReport createStoredReport(SessionReportGroupMarksAddUI model) throws Exception
    {
        UnisessionGroupMarksReport report = createReport();

		DatabaseFile content = new DatabaseFile();
		byte[] reportBytes = print(model);
		content.setContent(reportBytes);
		save(content);
		report.setContent(content);

		fillReport(report, model);
        save(report);
        return report;
    }

	/** Создать объект отчета. Для переопределения в проектах. */
	protected UnisessionGroupMarksReport createReport()
	{
		return new UnisessionGroupMarksReport();
	}

	/** Заполнить поля отчета. Для переопределения в проектах. */
	protected void fillReport(UnisessionGroupMarksReport report, SessionReportGroupMarksAddUI model)
	{
		report.setFormingDate(new Date());
		report.setExecutor(PersonSecurityUtil.getExecutor());
		report.setOrgUnit(model.getOrgUnit());

		String disciplineString = model.getDisciplines().stream().map(EppRegistryElementPart::getTitle).collect(Collectors.joining("; "));
		report.setRegistryElementParts(disciplineString);

		final UniSessionFilterAddon filterAddon = model.getSessionFilterAddon();

		SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DISC_KIND, UnisessionGroupMarksReport.P_DISC_KINDS, "title");
		SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_GROUP_WITH_NOGROUP, UnisessionGroupMarksReport.P_GROUPS, "title");
		SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COURSE, UnisessionGroupMarksReport.P_COURSE, "title");
		SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_CONTROL_FORM, UnisessionGroupMarksReport.P_CONTROL_ACTION_TYPES, "title");
		SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_CUSTOM_STATE, UnisessionGroupMarksReport.P_CUSTOM_STATE, "title");
	}

    /**
     * Для переопределения в проектах.
     *
     * @param defaultFormat Формат по умолчанию
     * @return Формат для ячеек с оценками
     * @throws WriteException
     */
    protected Map<SessionMarkCatalogItem, WritableCellFormat> initCellFormatsForMarkCells(final WritableCellFormat defaultFormat) throws WriteException
    {
        return SafeMap.get(key -> defaultFormat);
    }

    private byte[] print(final SessionReportGroupMarksAddUI model) throws Exception
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        CellFormats formats = new CellFormats();
        
        final List<ReportTable> tableList = new ArrayList<>();
        DebugUtils.debug(logger, new DebugUtils.Section("prepareMarkData")
        {
            @Override
            public void execute()
            {
                prepareReportData(model, tableList);
            }
        });

        if (tableList.isEmpty())
            throw new ApplicationException("При выбранных параметрах отчета в статистику не попала ни одна оценка.");

        for (ReportTable reportTable : tableList) {

            WritableSheet sheet = workbook.createSheet(reportTable.getSheetTitle(), workbook.getSheets().length);
            sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
            sheet.getSettings().setCopies(1);

            sheet.setColumnView(0, 3);
            sheet.setColumnView(1, 30);
            for (int i = 2; i < reportTable.getColumns().size(); i++)
                sheet.setColumnView(i, 6);

            int excelRow = 0;
            int excelColumn = 0;

            int studentNum = 1;

            final boolean viewByTerm = model.getDiscView().equals(SessionReportGroupMarksAddUI.DISC_VIEW_BY_TERM);

			Comparator<ReportColumn> simpleComparator = Comparator.comparing((ReportColumn column) -> column.getCaType().getPriority())
					.thenComparing(column -> column.getDiscipline().getTitle(), String.CASE_INSENSITIVE_ORDER)
					.thenComparing(column -> column.getDiscipline().getRegistryElement().getNumber(), String.CASE_INSENSITIVE_ORDER)
					.thenComparing(column -> column.getDiscipline().getNumber());

			Collections.sort(reportTable.columnList, viewByTerm ? Comparator.comparing(ReportColumn::getTerm).thenComparing(simpleComparator) : simpleComparator);

            if (reportTable.getGroup() != null) {
                sheet.addCell(new Label(1, excelRow, reportTable.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getTypeTitle(), formats.header_horizontal));
                sheet.mergeCells(2, excelRow, 12, excelRow);
                sheet.addCell(new Label(2, excelRow++, reportTable.getGroup().getEducationOrgUnit().getFormativeOrgUnit().getTitle(), formats.header_horizontal));

                sheet.addCell(new Label(1, excelRow, "Направление, специальность", formats.header_horizontal));
                sheet.mergeCells(2, excelRow, 12, excelRow);
                sheet.addCell(new Label(2, excelRow++, reportTable.getGroup().getEducationOrgUnit().getTitle(), formats.header_horizontal));

                sheet.addCell(new Label(1, excelRow, "Форма обучения", formats.header_horizontal));
                sheet.mergeCells(2, excelRow, 12, excelRow);
                sheet.addCell(new Label(2, excelRow++, reportTable.getGroup().getEducationOrgUnit().getDevelopForm().getProgramForm().getTitle(), formats.header_horizontal));

                sheet.addCell(new Label(1, excelRow, "Группа", formats.header_horizontal));
                sheet.mergeCells(2, excelRow, 12, excelRow);
                sheet.addCell(new Label(2, excelRow++, reportTable.getGroup().getTitle(), formats.header_horizontal));
            }

            sheet.addCell(new Label(1, excelRow, "Дисциплины", formats.header_horizontal));
            sheet.mergeCells(2, excelRow, 12, excelRow);
            sheet.addCell(new Label(2, excelRow++, model.getDiscView().getTitle(), formats.header_horizontal));

            int termRow = excelRow++;

            sheet.addCell(new Label(excelColumn++, excelRow, "№", formats.header_horizontal));
            sheet.addCell(new Label(excelColumn++, excelRow, "ФИО", formats.header_horizontal));

            sheet.setRowView(excelRow, 3000);


            int prevTerm = 0;
            int lastTerm = 0;
            int startTermCol = 0;

            for (ReportColumn column : reportTable.getColumns()) {
                if (column.getTerm() != prevTerm)
                {
                    if (prevTerm != 0)
                    {
                        sheet.mergeCells(startTermCol, termRow, excelColumn - 1, termRow);
                        sheet.addCell(new Label(startTermCol, termRow, String.valueOf(prevTerm), formats.header_horizontal));
                    }
                    prevTerm = column.getTerm();
                    startTermCol = excelColumn;
                }
                sheet.addCell(new Label(excelColumn++, excelRow, column.getTitle(), formats.header_vertical));
                lastTerm = column.getTerm();
            }
            if (lastTerm == prevTerm)
            {
                sheet.mergeCells(startTermCol, termRow, excelColumn - 1, termRow);
                sheet.addCell(new Label(startTermCol, termRow, String.valueOf(prevTerm), formats.header_horizontal));
            }

            excelColumn += addAverageMarkColumnTitle(sheet, excelColumn, excelRow, formats.header_vertical);

            Map<SessionMarkCatalogItem, WritableCellFormat> markCellFormats = initCellFormatsForMarkCells(formats.def);

            for (IdentifiableWrapper row : reportTable.getRows()) {
                excelColumn = 0;
                excelRow++;
                sheet.addCell(new Label(excelColumn++, excelRow, String.valueOf(studentNum++), formats.def));
                sheet.addCell(new Label(excelColumn++, excelRow, row.getTitle(), formats.fio));
                for (ReportColumn column : reportTable.getColumns()) {
                    SessionMarkCatalogItem mark = reportTable.getMark(row, column);
                    sheet.addCell(new Label(excelColumn++, excelRow, reportTable.getCellValue(row, column), markCellFormats.get(mark)));
                }
                excelColumn += addAverageMarkCell(sheet, reportTable, row, excelColumn, excelRow, formats.def);
            }
        }

        workbook.write();
        workbook.close();

        return out.toByteArray();
    }

    private void prepareReportData(SessionReportGroupMarksAddUI model, List<ReportTable> tableList)
    {
        DQLSelectBuilder dql = buildDqlQuery(model);
        Map<Group, ReportTable> data = new HashMap<>();
        Map<Group, Map<MultiKey, Set<ReportColumn>>> discTitles = new HashMap<>();
        Map<Long, Integer> termMap = DevelopGridDAO.getIdToTermNumberMap();
        for (Object[] row : dql.createStatement(getSession()).<Object[]>list()) {
            Long studentId = (Long) row[0];
            IdentifiableWrapper student = new IdentifiableWrapper(studentId, (String) row[1]);
            Group group = (Group) row[2];
            EppRegistryElementPart discipline = (EppRegistryElementPart) row[3];
            EppFControlActionType caType = (EppFControlActionType) row[4];
            SessionMarkCatalogItem mark = (SessionMarkCatalogItem) row[5];
            Long termId = (Long) row[6];

            ReportColumn column = new ReportColumn(discipline, caType, termMap.get(termId));

            SafeMap.safeGet(SafeMap.safeGet(discTitles, group, HashMap.class), new MultiKey(discipline.getRegistryElement().getShortTitleSafe(), discipline.getNumber(), caType.getId()), HashSet.class).add(column);
            
            ReportTable reportTable = data.get(group);
            if (null == reportTable)
                data.put(group, reportTable = new ReportTable(group));
            
            reportTable.rows.add(student);
            reportTable.columns.add(column);
            reportTable.markMap.put(ReportTable.key(student.getId(), discipline, caType), mark);
        }
        
        for (Map<MultiKey, Set<ReportColumn>> map : discTitles.values())
            for (Set<ReportColumn> columns : map.values()) {
                if (columns.size() > 1)
                    for (ReportColumn column : columns)
                        column.showNumber = true;
            }

        tableList.addAll(data.values());
        Collections.sort(tableList);
    }

	/** Построить страшный запрос по данным отчета. Для переопределения в проектах. */
	protected DQLSelectBuilder buildDqlQuery(SessionReportGroupMarksAddUI model)
	{
		DQLSelectBuilder dql = new DQLSelectBuilder()
				.fromEntity(Student.class, "s")
				.joinPath(DQLJoinType.left, Student.group().fromAlias("s"), "grp")
				.joinPath(DQLJoinType.left, Group.course().fromAlias("grp"), "grp_course")
				.joinPath(DQLJoinType.inner, Student.course().fromAlias("s"), "s_course")
				.fromEntity(SessionDocumentSlot.class, "slot")
				.joinEntity("slot", DQLJoinType.left, SessionMark.class, "m", eq(property("m", SessionMark.slot()), property("slot")))
				.joinPath(DQLJoinType.left, SessionMark.cachedMarkValue().fromAlias("m"), "mvalue")
				.joinPath(DQLJoinType.left, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "wpeCa")
				.joinEntity("wpeCa", DQLJoinType.inner, EppFControlActionType.class, "ca", eq(property("wpeCa", EppStudentWpeCAction.type()), property("ca", EppFControlActionType.eppGroupType())))
				.fromEntity(SessionStudentGradeBookDocument.class, "doc")
				.where(eq(property("slot", SessionDocumentSlot.document()), property("doc")))
				.where(eq(property("doc", SessionStudentGradeBookDocument.student()), property("s")));

		// итоговые актуальные оценки
		dql.where(eq(property("slot", SessionDocumentSlot.inSession()), value(Boolean.FALSE)));
		dql.where(isNull(property("wpeCa", EppStudentWpeCAction.removalDate())));

		// активные и неархивные студенты
		dql.where(eq(property("s", Student.archival()), value(Boolean.FALSE)));
		dql.where(eq(property("s", Student.status().active()), value(Boolean.TRUE)));

		dql.where(eq(property("s", Student.educationOrgUnit().formativeOrgUnit()), value(model.getOrgUnit())));

		dql
				.column("s.id")
				.column(property("s", Student.person().identityCard().fullFio()))
				.column(property("grp"))
				.column(property("wpeCa", EppStudentWpeCAction.studentWpe().registryElementPart()))
				.column(property("ca"))
				.column("mvalue")
				.column(property("wpeCa", EppStudentWpeCAction.studentWpe().term().id()));

		model.getSessionFilterAddon().applyFilters(dql, "wpeCa");

		if(model.isDisciplineSelected())
		{
			List<EppRegistryElementPart> disciplines = model.getDisciplines();
			FilterUtils.applySelectFilter(dql, "wpeCa", EppStudentWpeCAction.studentWpe().registryElementPart(), disciplines);
		}

		return dql;
	}

    /**
     * Для переопределения в проекте, добавление колонки со средним баллом
     */
    protected int addAverageMarkCell(WritableSheet sheet, ReportTable reportTable, IdentifiableWrapper row, int excelColumn,
                                      int excelRow, WritableCellFormat format) throws WriteException
    {
        return 0;
    }

    /**
     * Для переопределения в проекте, добавление столбца со средним баллом
     */
    protected int addAverageMarkColumnTitle(WritableSheet sheet, int excelColumn,
                                             int excelRow, WritableCellFormat format) throws WriteException
    {
        return 0;
    }

    protected static class ReportTable implements Comparable
    {
        private Group group;
        
        private Set<ReportColumn> columns = new HashSet<>();
        private Set<IdentifiableWrapper> rows = new HashSet<>();
        private Map<MultiKey, SessionMarkCatalogItem> markMap = new HashMap<>();
        
        private ArrayList<IdentifiableWrapper> rowList;
        private ArrayList<ReportColumn> columnList;

        private ReportTable(Group group)
        {
            this.group = group;
        }

        @Override
        public int compareTo(Object o)
        {
            if (!(o instanceof ReportTable))
                return 0;
            if (this.getGroup() == null)
                return -1;
            if (((ReportTable) o).getGroup() == null)
                return 1;
            return this.getGroup().getTitle().compareTo(((ReportTable) o).getGroup().getTitle());
        }

        private Group getGroup() { return group; }
        
        public List<IdentifiableWrapper> getRows() {
            if (null == rowList) {
                rowList = new ArrayList<>(rows);
                Collections.sort(rowList, ITitled.TITLED_COMPARATOR);
            }
            return rowList; 
        }
        
        public List<ReportColumn> getColumns() {
            if (null == columnList) {
                columnList = new ArrayList<>(columns);
                Collections.sort(columnList);
            }
            return columnList; 
        }
        
        public String getSheetTitle()
        {
            if (getGroup() == null)
                return "вне_групп";
            String grTitle =  getGroup().getTitle();
            if (grTitle.contains("/")) grTitle = grTitle.replaceAll("/", "_");  // заменяем недопустимый символ
            return grTitle;
        }
        
        public String getCellValue(IdentifiableWrapper row, ReportColumn column)
        {
            MultiKey key = key(row, column);
            if (!markMap.containsKey(key))
                return "х";
            SessionMarkCatalogItem mark = markMap.get(key);
            if (null == mark)
                return "";
            if (mark instanceof SessionMarkStateCatalogItem && SessionMarkStateCatalogItemCodes.UNNECESSARY.equals(mark.getCode()))
                return "х";
            return mark.getShortTitle();
        }

        private static MultiKey key(IdentifiableWrapper row, ReportColumn column) {
            return key(row.getId(), column.discipline, column.caType);
        }

        private static MultiKey key(Long studentId, EppRegistryElementPart discipline, EppFControlActionType caType) {
            return new MultiKey(studentId, discipline.getId(), caType.getId());
        }

        public SessionMarkCatalogItem getMark(IdentifiableWrapper row, ReportColumn column) {
            MultiKey key = key(row, column);
            return markMap.get(key);
        }
    }

    protected static class ReportColumn implements Comparable
    {
        private EppRegistryElementPart discipline;
        private EppFControlActionType caType;
        private MultiKey key;
        private boolean showNumber;
        private int term;

        public ReportColumn(EppRegistryElementPart discipline, EppFControlActionType caType, int term)
        {
            this.discipline = discipline;
            this.caType = caType;
            this.key = key(discipline, caType);
            this.term = term;
        }

        public static MultiKey key(EppRegistryElementPart discipline, EppFControlActionType caType)
        {
            return new MultiKey(discipline.getId(), caType.getId());
        }
        
        public String getTitle()
        {
            return discipline.getRegistryElement().getShortTitleSafe() +
                (showNumber ? " (" + discipline.getRegistryElement().getNumber() + ")" : "") +
                (discipline.getRegistryElement().getParts() == 1 ? "" : " " + discipline.getNumber() + "/" + discipline.getRegistryElement().getParts()) +
                ", " + caType.getAbbreviation();
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (!(o instanceof ReportColumn)) return false;
            ReportColumn column = (ReportColumn) o;
            return (key != null ? key.equals(column.key) : column.key == null);
        }

        @Override
        public int hashCode()
        {
            return key != null ? key.hashCode() : 0;
        }

        @Override
        public int compareTo(Object o)
        {
            if (!(o instanceof ReportColumn))
                return 1;
            ReportColumn other = (ReportColumn) o;
            int result = caType.getPriority() - other.caType.getPriority();
            if (0 == result) result = discipline.getRegistryElement().getNumber().compareTo(other.discipline.getRegistryElement().getNumber());
            if (0 == result) result = discipline.getNumber() - other.discipline.getNumber();
            return result;
        }

        public EppRegistryElementPart getDiscipline() {
            return discipline;
        }

        public void setDiscipline(EppRegistryElementPart discipline) {
            this.discipline = discipline;
        }

        public EppFControlActionType getCaType() {
            return caType;
        }

        public void setCaType(EppFControlActionType caType) {
            this.caType = caType;
        }

        public int getTerm() {
            return term;
        }
    }

    private static class CellFormats
    {
        final WritableCellFormat def;
        final WritableCellFormat header_vertical;
        final WritableCellFormat header_horizontal;
        final WritableCellFormat fio;
        final WritableCellFormat fio_inactive;

        private CellFormats() throws WriteException
        {
            def = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));
            def.setBorder(Border.ALL, BorderLineStyle.THIN);
            def.setAlignment(Alignment.RIGHT);

            header_vertical = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));
            header_vertical.setBorder(Border.ALL, BorderLineStyle.THIN);
            header_vertical.setOrientation(Orientation.PLUS_90);
            header_vertical.setAlignment(Alignment.CENTRE);
            header_vertical.setWrap(true);

            header_horizontal = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));
            header_horizontal.setBorder(Border.ALL, BorderLineStyle.THIN);
            header_horizontal.setAlignment(Alignment.CENTRE);
            header_horizontal.setVerticalAlignment(VerticalAlignment.CENTRE);

            fio = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9));
            fio.setBorder(Border.ALL, BorderLineStyle.THIN);
            fio.setAlignment(Alignment.LEFT);

            fio_inactive = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 9, WritableFont.NO_BOLD, true));
            fio_inactive.setBorder(Border.ALL, BorderLineStyle.THIN);
            fio_inactive.setAlignment(Alignment.LEFT);
        }
    }
}