package ru.tandemservice.unisession.component.catalog.sessionMarkGradeValue.SessionMarkGradeValueAddEdit;

import java.util.List;

import org.tandemframework.hibsupport.DataAccessServices;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

/**
 * @author iolshvang
 */
public class DAO extends DefaultCatalogAddEditDAO<SessionMarkGradeValueCatalogItem, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        if (model.getCatalogItem().getScale() == null)
        {
            EppGradeScale scale = getNotNull(model.getScale().getId());
            model.getCatalogItem().setScale(scale);
            if (model.isAddForm())
            {
                final List<SessionMarkGradeValueCatalogItem> orderedMarks = DataAccessServices.dao().getList(SessionMarkGradeValueCatalogItem.class, SessionMarkGradeValueCatalogItem.scale(), scale, SessionMarkGradeValueCatalogItem.P_PRIORITY);
                final int maxPriorityValue = orderedMarks.isEmpty() ? 1 : orderedMarks.get(orderedMarks.size() - 1).getPriority();
                model.getCatalogItem().setPriority(maxPriorityValue + 1);
            }
        }
    }
}
