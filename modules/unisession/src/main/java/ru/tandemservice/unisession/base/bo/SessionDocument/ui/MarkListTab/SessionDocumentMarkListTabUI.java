/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionDocument.ui.MarkListTab;

import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.base.bo.SessionDocument.SessionDocumentTabManager;
import ru.tandemservice.unisession.base.bo.SessionDocument.ui.logic.SessionDocumentMarkListDSHandler;
import ru.tandemservice.unisession.base.bo.SessionSheet.ui.Forming.SessionSheetForming;
import ru.tandemservice.unisession.base.bo.SessionSheet.ui.Forming.SessionSheetFormingUI;
import ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocAutocreate.Model;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.print.ISessionRetakeDocPrintDAO;
import ru.tandemservice.unisession.print.ISessionSheetPrintDAO;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 12.05.2015
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "holder.id")})
public class SessionDocumentMarkListTabUI extends UIPresenter
{
    public static final String PARAM_ORG_UNIT_ID = "orgUnitId";
    public static final String PARAM_YEAR = "eduYear";
    public static final String PARAM_PART = "yearPart";
    public static final String PARAM_DOC_NUMBER = "docNumber";
    public static final String PARAM_STUDENT_LAST_NAME = "studentLastName";
    public static final String PARAM_STUDENT_FIRST_NAME = "studentFirstName";
    public static final String PARAM_STATUS_MARK = "statusMark";
    public static final String PARAM_SESSION_MARK_LIST = "sessionMarkList";
    public static final String PARAM_WORK_PLAN_ROW_KIND_LIST = "workPlanRowKindList";
    public static final String PARAM_COURSE_LIST = "courseList";
    public static final String PARAM_GROUP_LIST = "groupList";
    public static final String PARAM_CHECKED_GRADE_BOOK = "checkedGradeBook";
    public static final String PARAM_TEACHER = "teacher";

    private final EntityHolder<OrgUnit> _holder = new EntityHolder<>();
    private CommonPostfixPermissionModel _sec;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        setSec(new OrgUnitSecModel(getOrgUnit()));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SessionDocumentMarkListTab.MARK_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_ORG_UNIT_ID, getOrgUnitId());
            dataSource.putAll(getSettings().getAsMap(
                    PARAM_YEAR,
                    PARAM_PART,
                    PARAM_DOC_NUMBER,
                    PARAM_STUDENT_LAST_NAME,
                    PARAM_STUDENT_FIRST_NAME,
                    PARAM_STATUS_MARK,
                    PARAM_SESSION_MARK_LIST,
                    PARAM_WORK_PLAN_ROW_KIND_LIST,
                    PARAM_COURSE_LIST,
                    PARAM_GROUP_LIST,
                    PARAM_CHECKED_GRADE_BOOK,
                    PARAM_TEACHER
            ));
        }
        else if (SessionDocumentMarkListTab.GROUP_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_COURSE_LIST, getSettings().get(PARAM_COURSE_LIST));
        }
    }

    public void onClickOneSheetForming()
    {
        IEntity entity = getConfig().getDataSourceRecordByListenerId(SessionDocumentMarkListTab.MARK_DS);
        onClickSheetForming(Lists.newArrayList(entity));
    }

    public void onClickSheetForming()
    {
        Collection<IEntity> selected = getSelectedIds();
        if (selected.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одного должника из списка.");

        onClickSheetForming(selected);
    }

    public void onClickSheetForming(Collection<IEntity> selected)
    {
        List<SessionMark> markList = Lists.newArrayList();
        for (IEntity entity : selected)
        {
            DataWrapper wrapper = (DataWrapper) entity;
            SessionMark totalMark = (SessionMark) wrapper.getProperty("totalMark");
            if (null == totalMark)
                throw new ApplicationException("У выбранных элементов отсутствуют итоговые оценки.");

            Boolean status = totalMark.getCachedMarkPositiveStatus();
            if (null == status || status)
                throw new ApplicationException("Формировать ЭЛ в данном разделе можно только для пересдачи неудовлетворительных оценок и требующих пересдачи отметок.");

            markList.add(totalMark);
        }

        _uiActivation.asRegionDialog(SessionSheetForming.class)
                .parameters(new ParametersMap().add(UIPresenter.PUBLISHER_ID, getOrgUnitId()).add(SessionSheetFormingUI.PARAM_MARK_LIST, markList))
                .activate();
    }

    public void onClickBulletinsForming()
    {
        Collection<IEntity> selected = getSelectedIds();
        if (selected.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одного должника из списка.");

        EducationYear year = null;
        YearDistributionPart part = null;
        List<SessionMark> markList = Lists.newArrayList();
        for (IEntity entity : selected)
        {
            DataWrapper wrapper = (DataWrapper) entity;
            EppStudentWorkPlanElement element = (EppStudentWorkPlanElement) wrapper.getProperty(SessionDocumentMarkListDSHandler.PROPERTY_ELEMENT);
            SessionMark totalMark = (SessionMark) wrapper.getProperty(SessionDocumentMarkListDSHandler.PROPERTY_TOTAL_MARK);
            if (null == totalMark)
                throw new ApplicationException("У выбранных элементов отсутствуют итоговые оценки.");

            Boolean status = totalMark.getCachedMarkPositiveStatus();
            EducationYear elementYear = element.getYear().getEducationYear();
            YearDistributionPart elementPart = element.getPart();

            if (null == status || status)
                throw new ApplicationException("Формировать ведомости пересдач в данном разделе можно только для пересдачи неудовлетворительных оценок и требующих пересдачи отметок.");

            if (null == year)
            {
                year = elementYear;
                part = elementPart;
            }

            if (!year.equals(elementYear) || !part.equals(elementPart))
                throw new ApplicationException("Необходимо, чтобы выбранные итоговые оценки имели один общий семестр.");

            markList.add(totalMark);
        }

        DQLSelectBuilder sessionDql = new DQLSelectBuilder().fromEntity(SessionObject.class, "s")
                .where(eq(property("s", SessionObject.orgUnit().id()), value(getOrgUnitId())))
                .where(eq(property("s", SessionObject.educationYear()), value(year)))
                .where(eq(property("s", SessionObject.yearDistributionPart()), value(part)));

        List<SessionObject> sessionObjectList = DataAccessServices.dao().getList(sessionDql);
        if (sessionObjectList.isEmpty())
            throw new ApplicationException("Не удалось определить семестр.");

        _uiActivation.asRegionDialog(Model.COMPONENT_NAME)
                .parameters(new ParametersMap().add(UIPresenter.PUBLISHER_ID, getOrgUnitId()).add("sessionObject", sessionObjectList.get(0).getId()).add("markList", markList))
                .activate();
    }

    public void onClickChangeCheckedGradeBook()
    {
        DataWrapper currentRow = getSelectRow();
        boolean checkedGradeBook = (boolean) currentRow.getProperty(SessionDocumentMarkListDSHandler.PROPERTY_CHECKED_GRADE_BOOK);

        SessionDocumentTabManager.instance().dao().changeCheckedGradeBook(Collections.singleton(currentRow.getId()), checkedGradeBook);
    }

    public void onClickShowHistory()
    {
        _uiActivation.asRegionDialog(ru.tandemservice.unisession.component.student.ControlActionMarkHistory.Model.COMPONENT_NAME)
                .parameters(new ParametersMap().add("controlActionId", getListenerParameter()).add("showAsSingle", true))
                .activate();
    }

    public void onClickPrintSheetDocument()
    {
        SessionSheetDocument sheetDocument = (SessionSheetDocument) getSelectRow().getProperty(SessionDocumentMarkListDSHandler.PROPERTY_SHEET_DOCUMENT);
        RtfDocument document = ISessionSheetPrintDAO.instance.get().printSheet(sheetDocument.getId());
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Экзаменационный лист.rtf").document(document), true);
    }

    public void onClickPrintSheetDocuments()
    {
        List<Long> sheetDocumentIds = Lists.newArrayList();

        for (IEntity row : getSelectedIds())
        {
            SessionSheetDocument sheetDocument = (SessionSheetDocument) row.getProperty(SessionDocumentMarkListDSHandler.PROPERTY_SHEET_DOCUMENT);
            if (null != sheetDocument)
                sheetDocumentIds.add(sheetDocument.getId());
        }
        if (sheetDocumentIds.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы один экзаменационный лист из списка.");

        String fileName = (sheetDocumentIds.size() > 1 ? "Экзаменационные листы" : "Экзаменационный лист") + ".rtf";
        RtfDocument document = sheetDocumentIds.size() > 1
                ? ISessionSheetPrintDAO.instance.get().printSheetList(sheetDocumentIds)
                : ISessionSheetPrintDAO.instance.get().printSheet(sheetDocumentIds.get(0));

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName).document(document), true);
    }

    public void onClickCheckedGradeBook()
    {
        Collection<IEntity> selected = getSelectedIds();
        selected.removeIf(item -> (boolean) item.getProperty(SessionDocumentMarkListDSHandler.PROPERTY_CHECKED_GRADE_BOOK_DISABLED));

        if (selected.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одну положительную оценку из списка.");

        SessionDocumentTabManager.instance().dao().changeCheckedGradeBook(CommonDAO.ids(selected), false);
    }

    public void onClickPrintRetakeDocument()
    {
        SessionRetakeDocument retakeDocument = (SessionRetakeDocument) getSelectRow().getProperty(SessionDocumentMarkListDSHandler.PROPERTY_RETAKE_DOCUMENT);
        RtfDocument document = ISessionRetakeDocPrintDAO.instance.get().printDoc(retakeDocument.getId());
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Ведомость.rtf").document(document), true);
    }

    public void onClickSearch()
    {
        getSettings().save();
    }

    public void onClickClear()
    {
        getSettings().clear();
    }

    public EntityHolder<OrgUnit> getHolder()
    {
        return _holder;
    }

    public boolean isDisabledPrintSheetDocument()
    {
        return null == getCurrentRow().getProperty(SessionDocumentMarkListDSHandler.PROPERTY_SHEET_DOCUMENT);
    }

    public boolean isDisabledPrintRetakeDocument()
    {
        return null == getCurrentRow().getProperty(SessionDocumentMarkListDSHandler.PROPERTY_RETAKE_DOCUMENT);
    }

    public boolean isDisabledShowHistory()
    {
        return null == getCurrentRow().getProperty(SessionDocumentMarkListDSHandler.PROPERTY_RETAKE_COUNT);
    }

    private Collection<IEntity> getSelectedIds()
    {
        return ((BaseSearchListDataSource) getConfig().getDataSource(SessionDocumentMarkListTab.MARK_DS)).getOptionColumnSelectedObjects("select");
    }

    private DataWrapper getCurrentRow()
    {
        return getConfig().getDataSource(SessionDocumentMarkListTab.MARK_DS).getCurrent();
    }

    public DataWrapper getSelectRow()
    {
        return getConfig().getDataSource(SessionDocumentMarkListTab.MARK_DS).getRecordById(getListenerParameterAsLong());
    }

    public Long getOrgUnitId()
    {
        return getHolder().getId();
    }

    public OrgUnit getOrgUnit()
    {
        return getHolder().getValue();
    }

    public CommonPostfixPermissionModel getSec()
    {
        return _sec;
    }

    public void setSec(CommonPostfixPermissionModel sec)
    {
        _sec = sec;
    }
}
