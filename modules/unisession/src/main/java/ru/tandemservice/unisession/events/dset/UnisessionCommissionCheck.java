/* $Id$ */
package ru.tandemservice.unisession.events.dset;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.concurrent.semaphore.IThreadSemaphore;
import org.tandemframework.core.util.concurrent.semaphore.ThreadSemaphore;
import org.tandemframework.core.util.concurrent.semaphore.ThreadSemaphoreHolder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/20/11
 */
public class UnisessionCommissionCheck implements IDSetEventListener
{
    private static final ThreadSemaphoreHolder<ThreadSemaphore> _semaphoreHolder = new ThreadSemaphoreHolder<ThreadSemaphore>();
    public static IThreadSemaphore lock() {
        return new ThreadSemaphore(_semaphoreHolder);
    }

    @Override
    public void onEvent(final DSetEvent event)
    {
        if (_semaphoreHolder.hasSemaphore()) { return; }

        final DQLSelectBuilder closedDocs = new DQLSelectBuilder()
            .fromEntity(SessionDocumentSlot.class, "slot")
            .joinEntity("slot", DQLJoinType.inner, SessionComissionPps.class, "rel", eq(property(SessionComissionPps.commission().fromAlias("rel")), property(SessionDocumentSlot.commission().fromAlias("slot"))))

            .joinEntity("slot", DQLJoinType.left, SessionTransferDocument.class, "td", eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("td")))
            .where(isNull("td.id"))

            .where(in(property(SessionComissionPps.id().fromAlias("rel")), event.getMultitude().getInExpression()))
            .where(isNotNull(property(SessionDocumentSlot.document().closeDate().fromAlias("slot"))))
            .column(property("mark.id"));
        final Number count = closedDocs.createCountStatement(event.getContext()).uniqueResult();
        if (count != null && count.longValue() > 0) {
            throw new ApplicationException("Нельзя изменять состав преподавателей в закрытом документе.", true);
        }
    }
}
