/* $Id: daoItemPub.vm 6177 2009-01-13 14:09:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.catalog.eppGradeScale.EppGradeScaleItemPub;

import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.data.ItemPolicy;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author AutoGenerator
 * Created on 15.02.2011
 */
public class DAO extends DefaultCatalogItemPubDAO<EppGradeScale, Model> implements IDAO
{
    @Override
    public void updatePositive(final long markId)
    {
        final SessionMarkGradeValueCatalogItem mark = get(SessionMarkGradeValueCatalogItem.class, markId);
        mark.setPositive(!mark.isPositive());
        update(mark);
    }

    @Override
    public void deleteMark(final Long id)
    {
        delete(id);
    }

    @Override
    public void prepareListDataSource(final Model model, IDataSettings settings)
    {
        final List<SessionMarkGradeValueCatalogItem> orderedMarks = getOrderedMarks(model.getCatalogItem());
        final DynamicListDataSource<SessionMarkGradeValueCatalogItem> dataSource = model.getDataSource();
        dataSource.setTotalSize(orderedMarks.size());
        dataSource.setCountRow(Math.max(4, 1 + orderedMarks.size()));
        dataSource.createPage(orderedMarks);

        final Set<Long> disabledIds = new HashSet<>();
        for (final IEntity entity : model.getDataSource().getEntityList())
        {
            String code;
            try {
                code = ((ICatalogItem) entity).getCode();
            } catch (final ClassCastException c) {
                // view wrappers
                code = (String) entity.getProperty("code");
            }
            final ItemPolicy itemPolicy = EntityDataRuntime.getEntityPolicy("sessionMarkGradeValueCatalogItem").getItemPolicy(code);

            // системные элементы не могут быть удалены из справочника
            if (itemPolicy != null && !SynchronizeMeta.temp.equals(itemPolicy.getSynchronize()))
            {
                disabledIds.add(entity.getId());
            }
        }

        model.setDisabledIds(disabledIds);

    }

    @Override
    public void updatePriorityUp(final Long markValueId)
    {
	    SessionMarkGradeValueCatalogItem markValue = get(markValueId);
	    CommonManager.instance().commonPriorityDao().doChangePriorityUp(markValueId, SessionMarkGradeValueCatalogItem.scale(), markValue.getScale());
    }

    @Override
    public void updatePriorityVeryUp(final Long markValueId)
    {
        final Session session = getSession();
        final SessionMarkGradeValueCatalogItem markValue = get(markValueId);
        final List<SessionMarkGradeValueCatalogItem> orderedMarks = getOrderedMarks(markValue.getScale());
        markValue.setPriority(orderedMarks.get(0).getPriority()-1);
        session.update(markValue);
        session.flush();
	    CommonManager.instance().commonPriorityDao().doNormalizePriority(SessionMarkGradeValueCatalogItem.class, SessionMarkGradeValueCatalogItem.scale(), markValue.getScale());
    }

    @Override
    public void updatePriorityDown(final Long markValueId)
    {
	    SessionMarkGradeValueCatalogItem markValue = get(markValueId);
	    CommonManager.instance().commonPriorityDao().doChangePriorityDown(markValueId, SessionMarkGradeValueCatalogItem.scale(), markValue.getScale());
    }

    @Override
    public void updatePriorityVeryDown(final Long markValueId)
    {
        final Session session = getSession();
        final SessionMarkGradeValueCatalogItem markValue = get(markValueId);
        final List<SessionMarkGradeValueCatalogItem> orderedMarks = getOrderedMarks(markValue.getScale());
        markValue.setPriority(orderedMarks.get(orderedMarks.size()-1).getPriority()+1);
        session.update(markValue);
        session.flush();
	    CommonManager.instance().commonPriorityDao().doNormalizePriority(SessionMarkGradeValueCatalogItem.class, SessionMarkGradeValueCatalogItem.scale(), markValue.getScale());
    }

    @Override
    public IEntityHandler getUpDisabledEntityHandler()
    {
        return (entity -> {
            SessionMarkGradeValueCatalogItem markEntity = (SessionMarkGradeValueCatalogItem)entity;
            final List<SessionMarkGradeValueCatalogItem> orderedMarks = getOrderedMarks(markEntity.getScale());
            return markEntity.getPriority() == orderedMarks.get(0).getPriority();
        });
    }

    @Override
    public IEntityHandler getDownDisabledEntityHandler()
    {
        return (entity -> {
            SessionMarkGradeValueCatalogItem markEntity = (SessionMarkGradeValueCatalogItem)entity;
            final List<SessionMarkGradeValueCatalogItem> orderedMarks = getOrderedMarks(markEntity.getScale());
            return (markEntity.getPriority() == orderedMarks.get(orderedMarks.size()-1).getPriority());
        });
    }

	private List<SessionMarkGradeValueCatalogItem> getOrderedMarks(EppGradeScale scale)
	{
		return DataAccessServices.dao().getList(SessionMarkGradeValueCatalogItem.class, SessionMarkGradeValueCatalogItem.scale(), scale, SessionMarkGradeValueCatalogItem.P_PRIORITY);
	}

}
