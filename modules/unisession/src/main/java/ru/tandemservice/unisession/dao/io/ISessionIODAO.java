/* $Id$ */
package ru.tandemservice.unisession.dao.io;

import com.healthmarketscience.jackcess.Database;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.io.OutputStream;

/**
 * @author oleyba
 * @since 11/2/11
 */
public interface ISessionIODAO
{
    SpringBeanCache<ISessionIODAO> instance = new SpringBeanCache<>(ISessionIODAO.class.getName());

    /**
     * Выгружает шаблон для построения файла для импорта оценок
     * mdb с информацией по студентам, ппс, оргюнитам, справочникам
     * @param outputStream -
     */
    @Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
    void exportSessionTemplate(OutputStream outputStream);

    /**
     * Выгружает шаблон для построения файла для импорта оценок
     * mdb с информацией по студентам, ппс, оргюнитам, справочникам
     * @param mdb -
     */
    @Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
    void exportSessionTemplate(final Database mdb) throws Exception;

    /**
     * Загружает оценки, создает МСРП и РУП на их основе
     * @param mdb -
     */
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false, isolation = Isolation.REPEATABLE_READ)
    void doImportMarks(final Database mdb);
}
