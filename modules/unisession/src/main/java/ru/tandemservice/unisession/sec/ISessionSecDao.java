/* $Id:$ */
package ru.tandemservice.unisession.sec;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.entity.document.*;

import java.util.Collection;

/**
 * @author oleyba
 * @since 11/26/14
 */
public interface ISessionSecDao
{
    SpringBeanCache<ISessionSecDao> instance = new SpringBeanCache<ISessionSecDao>(ISessionSecDao.class.getName());

    Collection<IEntity> getSecLocalEntities(SessionAttestation attestation);

    Collection<IEntity> getSecLocalEntities(SessionAttestationBulletin bulletin);

    Collection<IEntity> getSecLocalEntities(SessionBulletinDocument bulletin);

    Collection<IEntity> getSecLocalEntities(SessionGlobalDocument globalDocument);

    Collection<IEntity> getSecLocalEntities(SessionListDocument listDocument);

    Collection<IEntity> getSecLocalEntities(SessionObject sessionObject);

    Collection<IEntity> getSecLocalEntities(SessionRetakeDocument retakeDocument);

    Collection<IEntity> getSecLocalEntities(SessionSheetDocument sheetDocument);

    Collection<IEntity> getSecLocalEntities(SessionStudentGradeBookDocument gradeBookDocument);

    Collection<IEntity> getSecLocalEntities(SessionTransferDocument transferDocument);

    Collection<IEntity> getSecLocalEntities(SessionTransferProtocolDocument protocol);
}
