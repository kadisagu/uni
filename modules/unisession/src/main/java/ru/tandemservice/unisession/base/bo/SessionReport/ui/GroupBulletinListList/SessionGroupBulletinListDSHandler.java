/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupBulletinListList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/24/12
 */
public class SessionGroupBulletinListDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PARAM_SESSION_OBJECT = "sessionObjectKey";
    public static final String PARAM_COURSE = "course";

    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    public SessionGroupBulletinListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        final SessionObject sessionObject = context.get(PARAM_SESSION_OBJECT);
        final String course = context.get(PARAM_COURSE);
        if (null == sessionObject && null == course) {
            return ListOutputBuilder.get(dsInput, Collections.emptyList()).build();
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(UnisessionGroupBulletinListReport.class, "r")
            .where(sessionObject == null ? isNull("r.id") : eq(property(UnisessionGroupBulletinListReport.sessionObject().fromAlias("r")), value(sessionObject)))
            ;

        if (null != course)
            dql.where(eq(property("r", UnisessionGroupBulletinListReport.course()), value(course)));

        registry.applyOrder(dql, dsInput.getEntityOrder());

        return DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
    }

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(UnisessionGroupBulletinListReport.class, "r");
    }


}

