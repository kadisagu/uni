package ru.tandemservice.unisession.base.bo.SessionBulletin.ui.Add;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.hibsupport.log.InsertEntityEvent;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.ICommonFilterFactory;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.base.bo.SessionDocument.util.SessionRealEduGroupFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.dao.bulletin.ISessionBulletinDAO;
import ru.tandemservice.unisession.dao.sessionObject.ISessionObjectDAO;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.notExists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author avedernikov
 * @since 29.12.2015
 */
@State({
		@Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "holder.id", required = true),
		@Bind(key = SessionBulletinAddUI.PARAM_YEAR, binding = "eduYear"),
		@Bind(key = SessionBulletinAddUI.PARAM_YEAR_PART, binding = "yearPart")
})
public class SessionBulletinAddUI extends UIPresenter
{
	public static final String PARAM_YEAR = "year";
	public static final String PARAM_YEAR_PART = "part";

	public static final String SELECT_COLUMN = "select";

	private OrgUnitHolder _holder = new OrgUnitHolder();
	private EducationYear _eduYear;
	private YearDistributionPart _yearPart;

	/**
	 * Для правильной настройки аддона нужно вначале его сконфигурировать ({@link SessionBulletinAddUI#configUtil}), а затем инициализировать
	 * (это происходит автоматически в {@link SessionRealEduGroupFilterAddon#onComponentRefresh}, который в свою очередь автоматически вызывается
	 * после аналогичного метода презентера).<br/>
	 *
	 * При этом {@link SessionBulletinAddUI#onComponentRefresh} вызывается не только при активации БК и по F5, но и после того, как прогресс-бар
	 * (возникает при нажатии на кнопку Сформировать {@link SessionBulletinAddUI#onClickApply}) исчезнет. А повторная инициализация приводит, в частности,
	 * к сбросу значения селектов аддона, что нежелательно в данном случае.<br/>
	 *
	 * Поэтому аддон мы настраиваем в момент активации БК, после чего говорим, что вызывать инициализацию аддона нужно не более одного раза (т.е. только
	 * сразу после активации), но не по F5 и не после закрытия прогресс-бара ({@code filterAddon.setNeedRefresh(false);}).<br/>
	 *
	 * Нужно при этом учесть, что на страницу можно попасть не только с помощью активации БК, но и по навигации браузера (кнопка Назад). Во втором случае
	 * аддон уже находится в инициализированном состоянии. Нужно сбросить все сохранившиеся с прошлого раза значения в фильтрах - т.е. сконфигурировать и
	 * инициализировать аддон. Т.к. {@code needRefresh} аддона уже равно {@code false}, то для того, чтобы инициализация прошла, нужно задать ей значение
	 * {@code true}, а в конце - вернуть значение {@code false}.
	 */
	@Override
	public void onComponentActivate()
	{
		SessionRealEduGroupFilterAddon filterAddon = getFilterAddon();
		configUtil(filterAddon);
		filterAddon.setNeedRefresh(true);
		filterAddon.onComponentRefresh();
		filterAddon.setNeedRefresh(false);
	}

	@Override
	public void onComponentRefresh()
	{
		getHolder().refresh();
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case SessionBulletinAdd.EDU_GROUP_DS:
			{
				dataSource.put(AttestationManager.PARAM_ORG_UNIT, getOrgUnit());
				dataSource.put(AttestationManager.PARAM_EDU_YEAR, getEduYear());
				dataSource.put(AttestationManager.PARAM_YEAR_PART, getYearPart());
				dataSource.put(SessionRealEduGroupFilterAddon.UTIL_NAME, getFilterAddon());
				break;
			}
			case SessionReportManager.DS_EDU_YEAR:
			{
				dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
				break;
			}
			case SessionReportManager.DS_YEAR_PART:
			{
				dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
				dataSource.put(SessionReportManager.PARAM_EDU_YEAR, getEduYear());
				break;
			}
		}
	}

	public void onChangeYearOrPart()
	{
		configUtilWhere(getFilterAddon());
	}

	public void onClickShow()
	{
		configUtilWhere(getFilterAddon());
	}

	public void onClickClear()
	{
		getFilterAddon().clearSettings();
	}

	public void onClickApply()
	{
		final Collection<IEntity> selected = getConfig().<BaseSearchListDataSource>getDataSource(SessionBulletinAdd.EDU_GROUP_DS).getOptionColumnSelectedObjects(SELECT_COLUMN);
		if (selected.isEmpty())
			throw new ApplicationException("Необходимо выбрать хотя бы одну УГС из списка.");

        final List<Long> groupIds = CommonBaseEntityUtil.getIdList(selected);
        selected.clear();

        final SessionObject sessionObject = ISessionObjectDAO.instance.get().getSessionObject(getOrgUnit(), getEduYear(), getYearPart());
        final Long sessionObjectId = sessionObject.getId();
        IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(ProcessState state)
            {
                try (IEventServiceLock ignored = CoreServices.eventService().lock()) {

                    int currentValue = 0;
                    for (final Long id : groupIds) {

                        // каждый в своей транзакции
                        ISessionBulletinDAO.instance.get().doCreateBulletin(sessionObjectId, ImmutableList.of(id));

                        state.setMaxValue(100);
                        state.setCurrentValue((100 * (currentValue++)) / groupIds.size());
                        Thread.yield();
                    }

                } catch (final Exception t) {

                    CoreExceptionUtils.logException(t.getMessage(), t);
                    return new ProcessResult(new ApplicationException("Произошла ошибка при формировании ведомостей.", t));

                } finally {

                    CoreServices.eventService().fireEvent(new InsertEntityEvent("Сформированы ведомости.", getOrgUnit()));
                }

                return null;
            }
        };

        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Формирование ведомостей", process, ProcessDisplayMode.percent));
    }

	public SessionRealEduGroupFilterAddon getFilterAddon()
	{
		return (SessionRealEduGroupFilterAddon) getConfig().getAddon(SessionBulletinAdd.EDU_GROUP_FILTER_ADDON);
	}

	private void configUtil(SessionRealEduGroupFilterAddon util)
	{
		util
			.configDoubleWidthFilters(false)
			.configSettings(getSettingsKey());

		util.clearFilterItems();

		ICommonFilterFactory<IEntity> aLoadType = config -> new SessionRealEduGroupFilterAddon.LoadTypeFilterItem(config, "Форма контроля", EppGroupTypeFCA.class);

		util
			.addFilterItem(SessionRealEduGroupFilterAddon.DEVELOP_FORM, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.DEVELOP_CONDITION, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.EDU_LEVEL, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.TERRITORIAL_ORG_UNIT, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.COURSE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.GROUP, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
				.addFilterItem(SessionRealEduGroupFilterAddon.DISCIPLINE, CommonFilterFormConfig.DEFAULT_MULTI_SELECT_FILTER_CONFIG)
				.addFilterItem(aLoadType, CommonFilterFormConfig.DEFAULT_SELECT_FILTER_CONFIG);

		util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_DEVELOP_FORM).setNextWithNewLine(false);
		util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_EDU_LEVEL).setDoubleWidth(true).setColSpan(2);
		util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_TERRITORIAL_ORG_UNIT).setDoubleWidth(true).setColSpan(2);
		util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_FORMATIVE_ORG_UNIT).setDoubleWidth(true).setColSpan(2);
		util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_COURSE).setNextWithNewLine(false);
		util.getFilterItem(SessionRealEduGroupFilterAddon.SETTINGS_NAME_DISCIPLINE).setDoubleWidth(true).setColSpan(2);

		configUtilWhere(util);
	}

	public void configUtilWhere(SessionRealEduGroupFilterAddon util)
	{
		util.clearWhereFilter();
		util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppRealEduGroupRow.studentWpePart().studentWpe().student().status().active(), true));

		if (null != getOrgUnit())
			util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppRealEduGroupRow.studentWpePart().studentWpe().student().educationOrgUnit().groupOrgUnit(), getOrgUnit()));
		if (null != _eduYear)
			util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppRealEduGroupRow.group().summary().yearPart().year().educationYear(), _eduYear));
		if (null != _yearPart)
			util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppRealEduGroupRow.group().summary().yearPart().part(), _yearPart));

        util.customFilter((dql, alias, item, presenter) -> dql.where(notExists(SessionBulletinDocument.class, SessionBulletinDocument.group().s(), property(alias, EppRealEduGroup4ActionTypeRow.group()))));
	}

	public OrgUnit getOrgUnit()
	{
		return getHolder().getValue();
	}

	public String getSticker()
	{
		return getConfig().getProperty("ui.title", getOrgUnit().getFullTitle());
	}

	public String getCheckBoxHint()
	{
		return getConfig().getProperty("ui.checkBoxHint");
	}

	public OrgUnitHolder getHolder()
	{
		return _holder;
	}

	public void setHolder(OrgUnitHolder _holder)
	{
		this._holder = _holder;
	}

	public EducationYear getEduYear()
	{
		return _eduYear;
	}

	public void setEduYear(EducationYear _eduYear)
	{
		this._eduYear = _eduYear;
	}

	public YearDistributionPart getYearPart()
	{
		return _yearPart;
	}

	public void setYearPart(YearDistributionPart _yearPart)
	{
		this._yearPart = _yearPart;
	}
}
