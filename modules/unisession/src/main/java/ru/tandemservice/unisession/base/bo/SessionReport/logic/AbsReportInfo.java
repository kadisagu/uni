/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.logic;

/**
 * @author Andrey Andreev
 * @since 08.12.2016
 */
public abstract class AbsReportInfo<Report>
{
    /**
     * Отчет
     */
    protected Report _report;

    public AbsReportInfo(Report report)
    {
        _report = report;
    }

    public Report getReport()
    {
        return _report;
    }
}
