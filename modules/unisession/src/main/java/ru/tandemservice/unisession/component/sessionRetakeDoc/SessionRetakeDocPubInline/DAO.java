/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocPubInline;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsRatingValueFormatter;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.entity.document.SessionSlotRatingData;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionMarkRatingData;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkState;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 6/15/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setRetakeDoc(this.getNotNull(SessionRetakeDocument.class, model.getRetakeDoc().getId()));
        model.setTutors(CommonBaseUtil.<PpsEntry>getPropertiesList(this.getList(SessionComissionPps.class, SessionComissionPps.commission().s(), model.getRetakeDoc().getCommission()), SessionComissionPps.pps().s()));
        model.setWithUnknownMarks(ISharedBaseDao.instance.get().existsEntity(
                SessionSlotMarkState.class,
                SessionSlotMarkState.slot().document().s(), model.getRetakeDoc(),
                SessionSlotMarkState.value().code().s(), SessionMarkStateCatalogItemCodes.NOT_APPEAR_UNKNOWN
        ));
        this.prepareRegElementTitle(model);

        Map<ISessionBrsDao.ISessionRatingSettingsKey, ISessionBrsDao.ISessionRatingSettings> ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(model.getRetakeDoc());

        for (ISessionBrsDao.ISessionRatingSettings settings : ratingSettings.values())
		{
            model.setUseCurrentRating(model.isUseCurrentRating() || settings.useCurrentRating());
            model.setUsePoints(model.isUsePoints() || settings.usePoints());
        }

		model.setThemeRequired(ISessionDocumentBaseDAO.instance.get().isThemeRequired(model.getRetakeDoc()));
    }

    private void prepareRegElementTitle(final Model model)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder()
				.fromEntity(SessionDocumentSlot.class, "slot")
				.where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), value(model.getRetakeDoc())))
				.joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "wpe")
				.joinEntity("slot", DQLJoinType.inner, EppFControlActionType.class, "ca", eq(property("wpe", EppStudentWpeCAction.type()), property("ca", EppFControlActionType.eppGroupType())))
				.column(property("ca", EppFControlActionType.title()))
				.predicate(DQLPredicateType.distinct);
        final Set<String> caTypeTitles = new HashSet<>(dql.createStatement(this.getSession()).<String>list());
        model.setRegistryElementTitle(model.getRetakeDoc().getRegistryElementPart().getTitleWithNumber() + ", " + StringUtils.join(caTypeTitles, ", "));
        model.setShowCATypeColumn(caTypeTitles.size() > 1);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(final Model model)
    {
        final DynamicListDataSource dataSource = model.getDataSource();

        final DQLSelectBuilder slotDQL = new DQLSelectBuilder()
				.fromEntity(SessionDocumentSlot.class, "slot")
				.column("slot");
        this.fetchStudentData(slotDQL, "slot");
        slotDQL.where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), value(model.getRetakeDoc())));
        final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(SessionDocumentSlot.class, "slot");
        order.setOrders("person.fullFio", new OrderDescription(SessionDocumentSlot.actualStudent().person().identityCard().fullFio().s()));
        order.applyOrder(slotDQL, dataSource.getEntityOrder());
        final List<SessionDocumentSlot> slotList = slotDQL.createStatement(this.getSession()).list();
        UniBaseUtils.createFullPage(dataSource, slotList);

        final Map<SessionDocumentSlot, SessionMark> markMap = new HashMap<>();
        final Map<SessionDocumentSlot, String> pointsMap = new HashMap<>();
        for (final SessionMark mark : getList(SessionMark.class, SessionMark.slot().document(), model.getRetakeDoc()))
		{
            markMap.put(mark.getSlot(), mark);
            pointsMap.put(mark.getSlot(), BrsRatingValueFormatter.instance.get().format(mark.getPoints()));
        }

        final Map<SessionDocumentSlot, String> scoredPointsMap = new HashMap<>();
        for (final SessionMarkRatingData mark : getList(SessionMarkRatingData.class, SessionMarkRatingData.mark().slot().document(), model.getRetakeDoc()))
            scoredPointsMap.put(mark.getMark().getSlot(), BrsRatingValueFormatter.instance.get().format(mark.getScoredPoints()));
        final Map<SessionDocumentSlot, String> currentRatingMap = new HashMap<>();
        for (final SessionSlotRatingData mark : getList(SessionSlotRatingData.class, SessionSlotRatingData.slot().document(), model.getRetakeDoc()))
            currentRatingMap.put(mark.getSlot(), BrsRatingValueFormatter.instance.get().format(mark.getFixedCurrentRating()));

        dataSource.setRowCustomizer(new SimpleRowCustomizer<ViewWrapper>() {
            @Override
			public String getRowStyle(final ViewWrapper row)
			{
				final SessionMark mark = markMap.get(row.getEntity());
                if (mark instanceof SessionSlotMarkState && ((SessionSlotMarkState)mark).getValueItem().getCode().equals(SessionMarkStateCatalogItemCodes.NOT_APPEAR_UNKNOWN))
                    return "background-color: " + UniDefines.COLOR_YELLOW + ";";
                return "";
            }
        });

        DQLSelectBuilder markDQL = new DQLSelectBuilder()
				.fromEntity(SessionSlotMarkGradeValue.class, "m")
				.fromEntity(SessionDocumentSlot.class, "slot")
				.column("m.id")
				.column("slot")
				.where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), value(model.getRetakeDoc())))
				.where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")), property(SessionSlotMarkGradeValue.slot().studentWpeCAction().fromAlias("m"))));
		final Multiset<SessionDocumentSlot> markCountMap = HashMultiset.create();
        for (final Object[] row : markDQL.createStatement(this.getSession()).<Object[]>list())
			markCountMap.add((SessionDocumentSlot)row[1]);

		final String themeAlias = "theme";
		Map<SessionDocumentSlot, SessionProjectTheme> slot2projectTheme = new DQLSelectBuilder().fromEntity(SessionProjectTheme.class, themeAlias)
				.where(in(property(themeAlias, SessionProjectTheme.slot()), slotList))
				.createStatement(getSession()).<SessionProjectTheme>list().stream()
				.collect(Collectors.toMap(SessionProjectTheme::getSlot, theme -> theme));

        for (final ViewWrapper<SessionDocumentSlot> wrapper : ViewWrapper.<SessionDocumentSlot>getPatchedList(dataSource))
        {
            final SessionDocumentSlot slot = wrapper.getEntity();
            wrapper.setViewProperty(IDAO.V_MARK, markMap.get(slot));
			SessionProjectTheme theme = slot2projectTheme.get(slot);
			wrapper.setViewProperty(IDAO.PROP_PROJECT_THEME, theme == null ? null : theme.getTheme());
            final Integer markCount = markCountMap.contains(slot) ? markCountMap.count(slot) : null;
            wrapper.setViewProperty(IDAO.V_MARK_COUNT, markCount == null ? "нет" : String.valueOf(markCount - 1));
            wrapper.setViewProperty("points", pointsMap.get(slot));
            wrapper.setViewProperty("scoredPoints", scoredPointsMap.get(slot));
            wrapper.setViewProperty("currentRating", currentRatingMap.get(slot));
        }
    }

    private void fetchStudentData(final DQLSelectBuilder slotDQL, final String slotAlias)
    {
        slotDQL
				.fetchPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias(slotAlias), "wpca")
				.fetchPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("wpca"), "epvSlot")
				.fetchPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("epvSlot"), "student")
				.fetchPath(DQLJoinType.inner, Student.person().fromAlias("student"), "person")
				.fetchPath(DQLJoinType.inner, Person.identityCard().fromAlias("person"), "idc");
    }
}
