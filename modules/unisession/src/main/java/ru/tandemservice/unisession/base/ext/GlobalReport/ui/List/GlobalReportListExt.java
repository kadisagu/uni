/* $Id$ */
package ru.tandemservice.unisession.base.ext.GlobalReport.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.ui.List.GlobalReportList;

/**
 * @author azhebko
 * @since 28.03.2014
 */
@Configuration
public class GlobalReportListExt extends BusinessComponentExtensionManager
{
    @Autowired
    private GlobalReportList _globalReportList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_globalReportList.presenterExtPoint())
                .addAddon(uiAddon(UnisessionGlobalReportLIstAddon.NAME, UnisessionGlobalReportLIstAddon.class))
                .create();
    }
}