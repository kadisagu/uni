/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocPub;

import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unisession.base.bo.SessionRetakeDoc.ui.EditThemes.SessionRetakeDocEditThemes;

/**
 * @author oleyba
 * @since 6/15/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
    }

    public void onClickClose(final IBusinessComponent component)
    {
        this.getDao().doCloseBulletin(this.getModel(component));
    }

    public void onClickOpen(final IBusinessComponent component)
    {
        this.getDao().doOpenBulletin(this.getModel(component));
    }

    public void onClickMark(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(
                                          ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model.COMPONENT_NAME,
                                          new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getRetakeDoc().getId()))
        );
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocEdit.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getRetakeDoc().getId())));
    }

	public void onClickEditThemes(final IBusinessComponent component)
	{
		final Model model = this.getModel(component);

		CAFLegacySupportService.asDesktopRoot(SessionRetakeDocEditThemes.class.getSimpleName())
				.parameter(UIPresenter.PUBLISHER_ID, model.getRetakeDoc().getId())
				.activate();
	}

    public void onClickPrint(final IBusinessComponent component)
    {
        this.getDao().doPrintBulletin(this.getModel(component));
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        this.getDao().delete(this.getModel(component).getRetakeDoc());
        this.deactivate(component);
    }
}