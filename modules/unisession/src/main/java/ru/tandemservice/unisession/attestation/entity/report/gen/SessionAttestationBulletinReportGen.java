package ru.tandemservice.unisession.attestation.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Аттестационная ведомость по дисциплине (на академ. группу)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionAttestationBulletinReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport";
    public static final String ENTITY_NAME = "sessionAttestationBulletinReport";
    public static final int VERSION_HASH = 1393712897;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_EXECUTOR = "executor";
    public static final String L_ATTESTATION = "attestation";
    public static final String P_GROUP = "group";
    public static final String P_DISCIPLINE = "discipline";
    public static final String P_COURSE = "course";
    public static final String P_CUSTOM_STATE = "customState";
    public static final String P_TARGET_ADMISSION = "targetAdmission";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private String _executor;     // Исполнитель
    private SessionAttestation _attestation;     // Аттестация
    private String _group;     // Группа
    private String _discipline;     // Дисциплина
    private String _course;     // Курс
    private String _customState;     // Дополнительный статус
    private String _targetAdmission;     // Целевой прием

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    /**
     * @return Аттестация. Свойство не может быть null.
     */
    @NotNull
    public SessionAttestation getAttestation()
    {
        return _attestation;
    }

    /**
     * @param attestation Аттестация. Свойство не может быть null.
     */
    public void setAttestation(SessionAttestation attestation)
    {
        dirty(_attestation, attestation);
        _attestation = attestation;
    }

    /**
     * @return Группа.
     */
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Дисциплина.
     */
    public String getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина.
     */
    public void setDiscipline(String discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Дополнительный статус.
     */
    public String getCustomState()
    {
        return _customState;
    }

    /**
     * @param customState Дополнительный статус.
     */
    public void setCustomState(String customState)
    {
        dirty(_customState, customState);
        _customState = customState;
    }

    /**
     * @return Целевой прием.
     */
    @Length(max=255)
    public String getTargetAdmission()
    {
        return _targetAdmission;
    }

    /**
     * @param targetAdmission Целевой прием.
     */
    public void setTargetAdmission(String targetAdmission)
    {
        dirty(_targetAdmission, targetAdmission);
        _targetAdmission = targetAdmission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionAttestationBulletinReportGen)
        {
            setContent(((SessionAttestationBulletinReport)another).getContent());
            setFormingDate(((SessionAttestationBulletinReport)another).getFormingDate());
            setExecutor(((SessionAttestationBulletinReport)another).getExecutor());
            setAttestation(((SessionAttestationBulletinReport)another).getAttestation());
            setGroup(((SessionAttestationBulletinReport)another).getGroup());
            setDiscipline(((SessionAttestationBulletinReport)another).getDiscipline());
            setCourse(((SessionAttestationBulletinReport)another).getCourse());
            setCustomState(((SessionAttestationBulletinReport)another).getCustomState());
            setTargetAdmission(((SessionAttestationBulletinReport)another).getTargetAdmission());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionAttestationBulletinReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionAttestationBulletinReport.class;
        }

        public T newInstance()
        {
            return (T) new SessionAttestationBulletinReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "executor":
                    return obj.getExecutor();
                case "attestation":
                    return obj.getAttestation();
                case "group":
                    return obj.getGroup();
                case "discipline":
                    return obj.getDiscipline();
                case "course":
                    return obj.getCourse();
                case "customState":
                    return obj.getCustomState();
                case "targetAdmission":
                    return obj.getTargetAdmission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
                case "attestation":
                    obj.setAttestation((SessionAttestation) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
                case "discipline":
                    obj.setDiscipline((String) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "customState":
                    obj.setCustomState((String) value);
                    return;
                case "targetAdmission":
                    obj.setTargetAdmission((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "executor":
                        return true;
                case "attestation":
                        return true;
                case "group":
                        return true;
                case "discipline":
                        return true;
                case "course":
                        return true;
                case "customState":
                        return true;
                case "targetAdmission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "executor":
                    return true;
                case "attestation":
                    return true;
                case "group":
                    return true;
                case "discipline":
                    return true;
                case "course":
                    return true;
                case "customState":
                    return true;
                case "targetAdmission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "executor":
                    return String.class;
                case "attestation":
                    return SessionAttestation.class;
                case "group":
                    return String.class;
                case "discipline":
                    return String.class;
                case "course":
                    return String.class;
                case "customState":
                    return String.class;
                case "targetAdmission":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionAttestationBulletinReport> _dslPath = new Path<SessionAttestationBulletinReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionAttestationBulletinReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @return Аттестация. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getAttestation()
     */
    public static SessionAttestation.Path<SessionAttestation> attestation()
    {
        return _dslPath.attestation();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Дисциплина.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getDiscipline()
     */
    public static PropertyPath<String> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Дополнительный статус.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getCustomState()
     */
    public static PropertyPath<String> customState()
    {
        return _dslPath.customState();
    }

    /**
     * @return Целевой прием.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getTargetAdmission()
     */
    public static PropertyPath<String> targetAdmission()
    {
        return _dslPath.targetAdmission();
    }

    public static class Path<E extends SessionAttestationBulletinReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<String> _executor;
        private SessionAttestation.Path<SessionAttestation> _attestation;
        private PropertyPath<String> _group;
        private PropertyPath<String> _discipline;
        private PropertyPath<String> _course;
        private PropertyPath<String> _customState;
        private PropertyPath<String> _targetAdmission;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(SessionAttestationBulletinReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(SessionAttestationBulletinReportGen.P_EXECUTOR, this);
            return _executor;
        }

    /**
     * @return Аттестация. Свойство не может быть null.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getAttestation()
     */
        public SessionAttestation.Path<SessionAttestation> attestation()
        {
            if(_attestation == null )
                _attestation = new SessionAttestation.Path<SessionAttestation>(L_ATTESTATION, this);
            return _attestation;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(SessionAttestationBulletinReportGen.P_GROUP, this);
            return _group;
        }

    /**
     * @return Дисциплина.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getDiscipline()
     */
        public PropertyPath<String> discipline()
        {
            if(_discipline == null )
                _discipline = new PropertyPath<String>(SessionAttestationBulletinReportGen.P_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(SessionAttestationBulletinReportGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Дополнительный статус.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getCustomState()
     */
        public PropertyPath<String> customState()
        {
            if(_customState == null )
                _customState = new PropertyPath<String>(SessionAttestationBulletinReportGen.P_CUSTOM_STATE, this);
            return _customState;
        }

    /**
     * @return Целевой прием.
     * @see ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport#getTargetAdmission()
     */
        public PropertyPath<String> targetAdmission()
        {
            if(_targetAdmission == null )
                _targetAdmission = new PropertyPath<String>(SessionAttestationBulletinReportGen.P_TARGET_ADMISSION, this);
            return _targetAdmission;
        }

        public Class getEntityClass()
        {
            return SessionAttestationBulletinReport.class;
        }

        public String getEntityName()
        {
            return "sessionAttestationBulletinReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
