/**
 *$Id$
 */
package ru.tandemservice.unisession.base.ext.EppEduGroup.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.base.bo.EppEduGroup.logic.EppEduGroupList4StudentIdsDSHandler;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexander Shaburov
 * @since 17.07.13
 */
public class EppEduGroupList4StudentIdsExtSessionDSHandler extends EppEduGroupList4StudentIdsDSHandler
{
    public EppEduGroupList4StudentIdsExtSessionDSHandler(String ownerId)
    {
        super(ownerId);
    }

    public static final String COLUMN_BULLETIN = "columnBulletin";

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final DSOutput output = super.execute(input, context);

        final Map<Long, SessionBulletinDocument> bulletinMap = new HashMap<>();
        BatchUtils.execute(output.getRecordIds(), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            final List<SessionBulletinDocument> list = new DQLSelectBuilder().fromEntity(SessionBulletinDocument.class, "d").column(property("d"))
                    .where(in(property(SessionBulletinDocument.group().id().fromAlias("d")), elements))
                    .createStatement(context.getSession()).list();

            for (SessionBulletinDocument bulletin : list)
                if (bulletinMap.put(bulletin.getGroup().getId(), bulletin) != null)
                    throw new IllegalStateException();
        });

        for (DataWrapper wrapper : output.<DataWrapper>getRecordList())
        {
            wrapper.setProperty(COLUMN_BULLETIN, bulletinMap.get(wrapper.getId()));
        }

        return output;
    }
}
