/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocAutocreate;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.List;

/**
 * @author oleyba
 * @since 6/14/11
 */
@Input({
    @Bind(key= "sessionObject", binding="sessionObject.id"),
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orgUnitHolder.id", required = true),
    @Bind(key = "markList", binding = "markList")
})
public class Model
{
    public static String COMPONENT_NAME = Model.class.getPackage().getName();

    private final EntityHolder<OrgUnit> orgUnitHolder = new EntityHolder<OrgUnit>();

    private ISelectModel sessionObjectModel;
    private ISelectModel groupSplitModel;

    private SessionObject sessionObject = new SessionObject();
    private boolean groupByCommission;
    private boolean groupByCompensationType;
    private IdentifiableWrapper groupSplit;

    private boolean initialized;
    private List<Long> _markList;

    public Long getOrgUnitId()
    {
        return this.getOrgUnitHolder().getId();
    }

    public OrgUnit getOrgUnit()
    {
        return this.getOrgUnitHolder().getValue();
    }

    // accessors

    public boolean isNotEmptyMarkList()
    {
        return !(_markList == null || _markList.isEmpty());
    }

    public EntityHolder<OrgUnit> getOrgUnitHolder()
    {
        return this.orgUnitHolder;
    }

    public ISelectModel getSessionObjectModel()
    {
        return this.sessionObjectModel;
    }

    public void setSessionObjectModel(final ISelectModel sessionObjectModel)
    {
        this.sessionObjectModel = sessionObjectModel;
    }

    public SessionObject getSessionObject()
    {
        return this.sessionObject;
    }

    public void setSessionObject(final SessionObject sessionObject)
    {
        this.sessionObject = sessionObject;
    }

    public boolean isGroupByCommission()
    {
        return this.groupByCommission;
    }

    public void setGroupByCommission(final boolean groupByCommission)
    {
        this.groupByCommission = groupByCommission;
    }

    public boolean isGroupByCompensationType()
    {
        return this.groupByCompensationType;
    }

    public void setGroupByCompensationType(final boolean groupByCompensationType)
    {
        this.groupByCompensationType = groupByCompensationType;
    }

    public static String getCOMPONENT_NAME()
    {
        return Model.COMPONENT_NAME;
    }

    public static void setCOMPONENT_NAME(final String COMPONENT_NAME)
    {
        Model.COMPONENT_NAME = COMPONENT_NAME;
    }

    public ISelectModel getGroupSplitModel()
    {
        return this.groupSplitModel;
    }

    public void setGroupSplitModel(final ISelectModel groupSplitModel)
    {
        this.groupSplitModel = groupSplitModel;
    }

    public IdentifiableWrapper getGroupSplit()
    {
        return this.groupSplit;
    }

    public void setGroupSplit(final IdentifiableWrapper groupSplit)
    {
        this.groupSplit = groupSplit;
    }

    public boolean isInitialized()
    {
        return this.initialized;
    }

    public void setInitialized(final boolean initialized)
    {
        this.initialized = initialized;
    }

    public List<Long> getMarkList()
    {
        return _markList;
    }

    public void setMarkList(List<Long> markList)
    {
        _markList = markList;
    }
}
