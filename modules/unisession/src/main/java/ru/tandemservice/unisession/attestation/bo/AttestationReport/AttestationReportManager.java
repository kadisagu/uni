/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.AttestationReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.BulletinAdd.AttestationReportBulletinPrintDAO;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.BulletinAdd.IAttestationReportBulletinPrintDAO;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd.util.AttestationReportResultDao;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd.util.IAttestationReportResultDao;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util.AttestationTotalResultReportDao;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util.IAttestationTotalResultReportDao;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/16/11
 */
@Configuration
public class AttestationReportManager extends BusinessObjectManager
{
    public static final String PARAM_ORG_UNIT = SessionReportManager.PARAM_ORG_UNIT;
    public static final String PARAM_ATTESTATION = "attestation";
    public static final String PARAM_COURSE = "course";
    public static final String PARAM_GROUP = "group";
    public static final String PARAM_EDU_YEAR_ONLY_CURRENT = SessionReportManager.PARAM_EDU_YEAR_ONLY_CURRENT;

    public static final String DS_ATTESTATION = "attestationDS";
    public static final String DS_GROUP = "groupDS";


    public static AttestationReportManager instance()
    {
        return instance(AttestationReportManager.class);
    }

    @Bean
    public IAttestationReportBulletinPrintDAO bulletinPrintDao()
    {
        return new AttestationReportBulletinPrintDAO();
    }

    @Bean
    public IAttestationReportResultDao attestationReportResultDao()
    {
        return new AttestationReportResultDao();
    }

    @Bean
    public IAttestationTotalResultReportDao attestationTotalResultReportDao()
    {
        return new AttestationTotalResultReportDao();
    }

    @Bean
    public UIDataSourceConfig attestationDSConfig()
    {
        return SelectDSConfig.with(DS_ATTESTATION, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.attestationDSHandler())
        .addColumn(SessionAttestation.shortTitle().s())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> attestationDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), SessionAttestation.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final Object currentOnly = context.get(PARAM_EDU_YEAR_ONLY_CURRENT);
                if (Boolean.TRUE.equals(currentOnly)) {
                    dql.where(eq(property(SessionAttestation.sessionObject().educationYear().current().fromAlias(alias)), value(Boolean.TRUE)));
                }
            }

            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                return super.query(alias, filter)
                    .where(likeUpper(
                        DQLFunctions.concat(
                            DQLFunctions.cast(property(alias, SessionAttestation.number()), PropertyType.STRING),
                            property(alias, SessionAttestation.sessionObject().yearDistributionPart().title()),
                            property(alias, SessionAttestation.sessionObject().educationYear().title())),
                        value(CoreStringUtils.escapeLike(filter))));
            }
        }
            .where(SessionAttestation.sessionObject().orgUnit(), PARAM_ORG_UNIT)
            .pageable(true)
            .order(SessionAttestation.sessionObject().educationYear().intValue())
            .order(SessionAttestation.sessionObject().yearDistributionPart().number())
            .order(SessionAttestation.number());
    }

    /**
     * Зависит от аттестации и курса, выбранного в поле "Курс"
     */
    @Bean
    public UIDataSourceConfig groupDSConfig()
    {
        return SelectDSConfig.with(DS_GROUP, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.groupDSHandler())
        .addColumn(Group.title().s())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> groupDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), Group.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder bulletinDQL = new DQLSelectBuilder()
                    .fromEntity(SessionAttestationSlot.class, "s")
                    .column(property(SessionAttestationSlot.studentWpe().student().group().id().fromAlias("s")))
                    .where(eq(property(SessionAttestationSlot.bulletin().attestation().fromAlias("s")), commonValue(context.get(PARAM_ATTESTATION))));

                dql.where(in(property(Group.id().fromAlias(alias)), bulletinDQL.buildQuery()));
            }
        }
            .where(Group.course(), PARAM_COURSE)
            .pageable(true)
            .order(Group.title())
            .filter(Group.title())
            ;
    }
}