/* $*/

package ru.tandemservice.unisession.print;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionListDocument;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author iolshvang
 * @since 25.04.2011
 */
public class SessionListDocumentPrintDAO extends UniBaseDao implements ISessionListDocumentPrintDAO
{

    @Override
    public final RtfDocument printSessionListDocument(final Long id)
    {
        return this.printSessionListDocument(this.get(SessionListDocument.class, id));
    }

    protected RtfDocument printSessionListDocument(final SessionListDocument sessionListDocument)
    {
        final UnisessionCommonTemplate template = this.get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.EXAM_CARD);
        final RtfDocument document = new RtfReader().read(template.getContent()).getClone();

        final TopOrgUnit academy = TopOrgUnit.getInstance();

        final List<SessionDocumentSlot> slotList = this.getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), sessionListDocument);
        if (slotList.size()==0)
            return document;

        final SessionDocumentSlot slot = slotList.get(0);
        final RtfInjectModifier modifier = new RtfInjectModifier();
        SessionReportManager.addOuLeaderData(modifier, sessionListDocument.getOrgUnit(), "ouleader", "leaderFio");
        modifier.put("academyTitle", academy.getNominativeCaseTitle() == null ? academy.getTitle() == null ? "" : academy.getTitle() : academy.getNominativeCaseTitle());
        modifier.put("documentNumber", sessionListDocument.getNumber());
        modifier.put("ouTitle", null!=sessionListDocument.getOrgUnit().getNominativeCaseTitle()?sessionListDocument.getOrgUnit().getNominativeCaseTitle():sessionListDocument.getOrgUnit().getTitle());
        modifier.put("Number", sessionListDocument.getNumber());
        modifier.put("FIO", slot.getActualStudent().getPerson().getFullFio());
        modifier.put("groupTitle", null!=slot.getActualStudent().getGroup()?slot.getActualStudent().getGroup().getTitle():"");
        modifier.put("reason", sessionListDocument.getReason().getTitle());
        modifier.put("formingDate", sessionListDocument.getFormingDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(sessionListDocument.getFormingDate()));
        modifier.put("deadlineDate", sessionListDocument.getDeadlineDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(sessionListDocument.getDeadlineDate()));
        modifier.modify(document);

        final RtfTableModifier tableModifier = new RtfTableModifier();

        final List<String[]> dataLines = this.getDataLines(sessionListDocument);
        tableModifier.put("T", dataLines.toArray(new String[dataLines.size()][]));

		if (ISessionDocumentBaseDAO.instance.get().isThemeRequired(sessionListDocument))
		{
			final List<String[]> themeLines = getThemeLines(sessionListDocument);
			final String[][] themeArray = new String[themeLines.size()][];
			tableModifier.put("TTheme", themeLines.toArray(themeArray));
		}
		else
		{
			tableModifier.remove("TTheme", 1, 0);
		}

        tableModifier.modify(document);

        return document;
    }

    private List<String[]> getDataLines(final SessionListDocument sessionListDocument)
    {
        int cnt = 1;
        final List<String[]> paragraphDataLines = new ArrayList<>();
        final List<SessionDocumentSlot> slotList = this.getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), sessionListDocument);
        Collections.sort(slotList, new EntityComparator<>(new EntityOrder(SessionDocumentSlot.studentWpeCAction().studentWpe().term().intValue())));

        final Map<Long, SessionMark> markMap = new HashMap<>();
        final DQLSelectBuilder markDQL = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "mark")
                .column("mark")
                .where(eq(property("mark.slot.document"), value(sessionListDocument)));
        for (final SessionMark mark : markDQL.createStatement(this.getSession()).<SessionMark>list()) {
            markMap.put(mark.getSlot().getId(), mark);
        }

        final Map<Long, List<PpsEntry>> tutorMap = new HashMap<>();
        final DQLSelectBuilder tutorDQL = new DQLSelectBuilder()
                .fromEntity(SessionComissionPps.class, "rel")
                .column(property(SessionComissionPps.pps().fromAlias("rel")))
                .joinPath(DQLJoinType.inner, SessionComissionPps.commission().fromAlias("rel"), "comm")
                .joinEntity("comm", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property("comm.id"), property(SessionDocumentSlot.commission().id().fromAlias("slot"))))
                .column(property(SessionDocumentSlot.id().fromAlias("slot")))
                .where(eq(property("slot.document"), value(sessionListDocument)))
                .order(property(SessionComissionPps.pps().person().identityCard().fullFio().fromAlias("rel")));
        for (final Object[] row : tutorDQL.createStatement(this.getSession()).<Object[]>list()) {
            SafeMap.safeGet(tutorMap, (Long) row[1], ArrayList.class).add((PpsEntry) row[0]);
        }

        for(final SessionDocumentSlot sds : slotList)
        {
            final SessionMark sMark = markMap.get(sds.getId());
            String pps = "", mark ="", date = "";
            if (sMark != null)
            {
                final List<PpsEntry> ppsList = tutorMap.get(sds.getId());
                pps = UniStringUtils.join(ppsList, PpsEntry.person().identityCard().fio().s(), ", ");
                mark = sMark.getValueShortTitle();
                date = DateFormatter.DEFAULT_DATE_FORMATTER.format(sMark.getPerformDate());
            }
            EppStudentWpeCAction wpeCAction = sds.getStudentWpeCAction();
            String term = sds.getTermTitle() + " №" + wpeCAction.getStudentWpe().getTerm().getTitle();
            paragraphDataLines.add(new String[] {String.valueOf(cnt++)+".", term, wpeCAction.getRegistryElementTitle(), mark, date, pps});
        }
        return paragraphDataLines;
    }

	private List<String[]> getThemeLines(final SessionListDocument listDocument)
	{
		final String slotAlias = "slot";
		final String groupTypeAlias = "groupType";
		final String fcaTypeAlias = "fcaType";
		final String themeAlias = "theme";
		List<Object[]> rows = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, slotAlias)
				.where(eq(property(slotAlias, SessionDocumentSlot.document()), value(listDocument)))
				.joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().type().fromAlias(slotAlias), groupTypeAlias)
				.joinEntity(groupTypeAlias, DQLJoinType.inner, EppFControlActionType.class, fcaTypeAlias,
							eq(property(groupTypeAlias), property(fcaTypeAlias, EppFControlActionType.eppGroupType())))
				.where(eq(property(fcaTypeAlias, EppFControlActionType.group().themeRequired()), value(Boolean.TRUE)))
				.joinEntity(slotAlias, DQLJoinType.left, SessionProjectTheme.class, themeAlias, eq(property(slotAlias), property(themeAlias, SessionProjectTheme.slot())))
				.order(property(slotAlias, SessionDocumentSlot.studentWpeCAction().studentWpe().term().intValue()))
				.column(property(slotAlias, SessionDocumentSlot.studentWpeCAction()))
				.column(property(themeAlias, SessionProjectTheme.theme()))
				.createStatement(getSession()).<Object[]>list();
		return rows.stream()
				.map((Object[] row) -> new String[] { ((EppStudentWpeCAction)(row[0])).getRegistryElementTitle(), (String)row[1]})
				.collect(Collectors.toList());
	}
}
