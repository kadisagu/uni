/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.DebtorsAdd;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.report.UnisessionDebtorsReport;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 1/24/12
 */
public interface ISessionReportDebtorsDAO extends INeedPersistenceSupport
{
    SpringBeanCache<ISessionReportDebtorsDAO> instance = new SpringBeanCache<ISessionReportDebtorsDAO>(ISessionReportDebtorsDAO.class.getName());

    interface ISessionReportDebtorsParams
    {
        SessionObject getSessionObject();
        boolean isEducationLevelHighSchoolActive();
        List<EducationLevelsHighSchool> getEducationLevelHighSchoolList();
        boolean isDevelopFormActive();
        List<DevelopForm> getDevelopFormList();
        boolean isDevelopConditionActive();
        List<DevelopCondition> getDevelopConditionList();
        boolean isDevelopTechActive();
        List<DevelopTech> getDevelopTechList();
        boolean isDevelopPeriodActive();
        List<DevelopPeriod> getDevelopPeriodList();
        boolean isCourseActive();
        List<Course> getCourseList();
        boolean isGroupActive();
        boolean isIncludeStudentsWithoutGroups();
        List<Group> getGroupList();
        boolean isCompensationTypeActive();
        CompensationType getCompensationType();
        boolean isStudentStatusActive();
        List<StudentStatus> getStudentStatusList();
        boolean isDeadlineCheckActive();
        Date getDeadlineCheck();
        Collection<EppFControlActionType> getEppFControlActionTypes();
        boolean isEppFControlActionTypeCheckActive();

    }

    UnisessionDebtorsReport createStoredReport(SessionReportDebtorsAddUI model);
}
