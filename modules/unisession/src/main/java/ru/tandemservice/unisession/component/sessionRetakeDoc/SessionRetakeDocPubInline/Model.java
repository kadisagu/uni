/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocPubInline;

/*
 * @author oleyba
 * @since 6/15/11
 */

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

import java.util.*;
import java.util.stream.Collectors;

@Input({
    @Bind(key= PublisherActivator.PUBLISHER_ID_KEY, binding="retakeDoc.id")
})
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    public static final String BIND_COLUMN_PKEY_HANDLER = "columnPKeyHandler";

    private SessionRetakeDocument retakeDoc = new SessionRetakeDocument();
    private boolean withUnknownMarks;
    private List<PpsEntry> tutors;
    private Group currentGroup;

    private String registryElementTitle;
    private boolean showCATypeColumn;

    private boolean useCurrentRating;
    private boolean usePoints;
	private boolean themeRequired;

    private DynamicListDataSource<SessionDocumentSlot> dataSource = new DynamicListDataSource<>();

    public SessionRetakeDocument getRetakeDoc()
    {
        return this.retakeDoc;
    }

    public void setRetakeDoc(final SessionRetakeDocument retakeDoc)
    {
        this.retakeDoc = retakeDoc;
    }

    public boolean isWithUnknownMarks()
    {
        return this.withUnknownMarks;
    }

    public void setWithUnknownMarks(final boolean withUnknownMarks)
    {
        this.withUnknownMarks = withUnknownMarks;
    }

    public List<PpsEntry> getTutors()
    {
        return this.tutors;
    }

    public void setTutors(final List<PpsEntry> tutors)
    {
        this.tutors = tutors;
    }

    public DynamicListDataSource<SessionDocumentSlot> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<SessionDocumentSlot> dataSource)
    {
        this.dataSource = dataSource;
    }

    public String getRegistryElementTitle()
    {
        return this.registryElementTitle;
    }

    public void setRegistryElementTitle(final String registryElementTitle)
    {
        this.registryElementTitle = registryElementTitle;
    }

    public boolean isShowCATypeColumn()
    {
        return this.showCATypeColumn;
    }

    public void setShowCATypeColumn(final boolean showCATypeColumn)
    {
        this.showCATypeColumn = showCATypeColumn;
    }

    public boolean isUseCurrentRating()
    {
        return useCurrentRating;
    }

    public void setUseCurrentRating(boolean useCurrentRating)
    {
        this.useCurrentRating = useCurrentRating;
    }

    public boolean isUsePoints()
    {
        return usePoints;
    }

    public void setUsePoints(boolean usePoints)
    {
        this.usePoints = usePoints;
    }

	public boolean isThemeRequired()
	{
		return themeRequired;
	}

	public void setThemeRequired(boolean themeRequired)
	{
		this.themeRequired = themeRequired;
	}

    public Collection<Group> getStudentGroupList()
    {
        final List<SessionDocumentSlot> slotList = DataAccessServices.dao().getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), retakeDoc);
        return slotList.stream()
                .map(slot -> slot.getActualStudent().getGroup())
                .filter(group -> group != null)
                .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(Group::getTitle).thenComparing(Group::getId))));
    }

    public Group getCurrentGroup() {
        return currentGroup;
    }

    public void setCurrentGroup(Group currentGroup) {
        this.currentGroup = currentGroup;
    }
}
