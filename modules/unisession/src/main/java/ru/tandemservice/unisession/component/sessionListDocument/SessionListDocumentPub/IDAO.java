package ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author iolshvang
 * @since 13.04.2011
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
	String PROP_PROJECT_THEME = "theme";

    void deleteSlot(IBusinessComponent context, Model model);

    void delete(IEntity entity);
}
