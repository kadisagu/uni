/* $Id: SessionReportGroupMarksListUI.java 21676 2012-01-24 08:19:15Z oleyba $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupMarksList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupMarksAdd.SessionReportGroupMarksAdd;
import ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/16/11
 */
@State
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id", required = true)
})
public class SessionReportGroupMarksListUI extends UIPresenter
{
    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    // actions

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();
    }

    public void onSearchParamsChange()
    {
        onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(SessionGroupMarksDSHandler.PARAM_COURSE, getSettings().get(SessionGroupMarksDSHandler.PARAM_COURSE));
    }

    public void onClickAddReport()
    {
        getActivationBuilder().asRegion(SessionReportGroupMarksAdd.class).parameter(SessionReportManager.BIND_ORG_UNIT, getOrgUnit().getId()).activate();
    }

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT).parameter("reportId", getListenerParameterAsLong()).parameter("extension", "xls").parameter("zip", Boolean.FALSE).activate();
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    // preseneter

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    // getters and setters

    public SingleSelectTextModel getCourseModel()
    {
        return new SingleSelectTextModel() {
            @Override
            public ListResult findValues(String filter) {

                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UnisessionGroupMarksReport.class, "rep")
                        .distinct()
                        .column(property(UnisessionGroupMarksReport.course().fromAlias("rep")))
                        .order(property(UnisessionGroupMarksReport.course().fromAlias("rep")))
                        ;

                if (ouHolder.getValue() != null)
                    builder.where(eq(property(UnisessionGroupMarksReport.orgUnit().fromAlias("rep")), value(ouHolder.getValue())));

                FilterUtils.applyLikeFilter(builder, filter, UnisessionGroupMarksReport.course().fromAlias("rep"));

                return new ListResult<>(DataAccessServices.dao().getList(builder));

            }
        };
    }


    public String getViewPermissionKey(){ return getSec().getPermission("orgUnit_viewSessionReportGroupMarksList"); }

    public String getAddStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_addSessionReportGroupMarksList"); }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_deleteSessionReportGroupMarksList"); }



    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }
}
