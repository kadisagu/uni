/**
 *$Id$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.AttestationReportTotalResultAdd;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.SessionReportUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniAttestationFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 22.11.12
 */
public class AttestationTotalResultReportDao extends UniBaseDao implements IAttestationTotalResultReportDao
{
    public static final String STUDENT_AMOUNT_COLUMN = "studentAmount";
    public static final String DISCIPLINE_AMOUNT_COLUMN = "disciplineAmount";
    public static final String PASSED_AMOUNT_COLUMN = "passedAmount";
    public static final String PASSED_PERCENT_COLUMN = "passedPercent";
    public static final String NOT_PASSED_2_COLUMN = "notPassed2Amount";
    public static final String NOT_PASSED_OTHER_COLUMN = "notPassedOtherAmount";

    @Override
    public <M extends Model> RtfDocument createReportRtfDocument(M model)
    {
        UnisessionCommonTemplate template = getCatalogItem(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.ATTESTATION_TOTAL_RESULT_REPORT);
        RtfDocument document = new RtfReader().read(template.getContent());

        // если студенты группируются по направлениям с детализацией, то данные агрегируются не так как для остальных вариантов группировки
        if (model.getResultFor().getId().equals(AttestationReportTotalResultAdd.DW_DIRECTION_DETAIL_ID))
        {
            prepareGroupedEduLvlHSStudentWrapper(model);

            return prepareRowAndInjectModifier(model, document);
        }
        else
        {
            prepareStudentList(model);
            prepareRowWrapper(model);

            return injectModifier(document, model);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <M extends Model> SessionAttestationTotalResultReport saveReport(M model, RtfDocument document)
    {
        SessionAttestationTotalResultReport report = new SessionAttestationTotalResultReport();

        final UniAttestationFilterAddon filterAddon = model.getFilterAddon();


        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(document));
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        content.setFilename("sessionAttestationTotalResultReport_" + new DateFormatter("dd_mm_yyyy").format(new Date()) + ".rtf");

        save(content);


        report.setContent(content);
        report.setFormingDate(new Date());
        report.setEducationYear(model.getEduYear());
        report.setYearDistributionPart(model.getYearPart());
        report.setAttestation(model.getAttestation());
        report.setAttestationNumber(model.getAttNumber() != null ? model.getAttNumber().intValue() : null);
        report.setAttestationResultFor(model.getResultFor().getTitle());
        report.setFormingOrgUnits(model.getFormativeOrgUnitList() != null ? UniStringUtils.join(model.getFormativeOrgUnitList(), OrgUnit.fullTitle().s(), ", ") : null);

        report.setOrgUnit(model.getOrgUnit());

        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COURSE, SessionAttestationTotalResultReport.P_COURSE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COMPENSATION_TYPE, SessionAttestationTotalResultReport.P_COMPENSATION_TYPE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_FORM, SessionAttestationTotalResultReport.P_DEVELOP_FORM, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_CONDITION, SessionAttestationTotalResultReport.P_DEVELOP_CONDITION, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_TECH, SessionAttestationTotalResultReport.P_DEVELOP_TECH, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_DEVELOP_PERIOD, SessionAttestationTotalResultReport.P_DEVELOP_PERIOD, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_STUDENT_STATUS, SessionAttestationTotalResultReport.P_STUDENT_STATUS, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_CUSTOM_STATE, SessionAttestationTotalResultReport.P_CUSTOM_STATE, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_TARGET_ADMISSION, SessionAttestationTotalResultReport.P_TARGET_ADMISSION, "title");

        save(report);

        return report;
    }

    /**
     * Подготавливает враперы для студентов.
     * Для каждого варианта группировки студентов в враперы будут разные.
     */
    protected <M extends Model> M prepareStudentList(M model)
    {
        DQLSelectBuilder builder = model.getFilterAddon().prepareEntityBuilder("s", null)
                .joinPath(DQLJoinType.left, SessionAttestationSlot.studentWpe().student().fromAlias("s"), "stud")
                .joinPath(DQLJoinType.left, SessionAttestationSlot.studentWpe().fromAlias("s"), "wpe")
                .joinPath(DQLJoinType.left, SessionAttestationSlot.mark().fromAlias("s"), "mark")
                .column(property(Student.id().fromAlias("stud")))
                .column(property(getGroupedStudentIdPath(model.getResultFor().getId()).fromAlias("stud")))
                .column(property(SessionAttestationSlot.bulletin().registryElementPart().id().fromAlias("s")))
                .column("mark");


        builder.joinPath(DQLJoinType.inner, SessionAttestationSlot.studentWpe().student().educationOrgUnit().fromAlias("s"), "ou");

        Map<Long, StudentWrapper> studentWrapperMap = new HashMap<>();
        for (Object[] objects : builder.createStatement(getSession()).<Object[]>list())
        {
            StudentWrapper wrapper = studentWrapperMap.get(objects[0]);
            if (wrapper == null)
                studentWrapperMap.put((Long) objects[0], wrapper = new StudentWrapper((Long) objects[0], (Long) objects[1]));
            Boolean passed = wrapper.getDicsPassedMap().get(objects[2]);
            SessionAttestationMarkCatalogItem mark = (SessionAttestationMarkCatalogItem) objects[3];
            if (passed != null)
            {
                wrapper.getDicsPassedMap().put((Long) objects[2], passed || (mark != null && mark.isPositive()));
            }
            else
            {
                wrapper.getDicsPassedMap().put((Long) objects[2], mark != null ? mark.isPositive() : null);
            }
        }


        model.setStudentWrapperList(new ArrayList<>(studentWrapperMap.values()));
        return model;
    }

    /**
     * Подготавливает логику колонок и вычисляет значения в строках отчета.
     */
    protected <M extends Model> M prepareRowWrapper(M model)
    {
        final DoubleFormatter doubleFormatter = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS;

        List<RowWrapper> rowWrapperList = getRowWrapperRawList(model);
        for (final RowWrapper row : rowWrapperList)
        {
            final Set<Long> discSet = new HashSet<>();
            row.addIncrement(STUDENT_AMOUNT_COLUMN, wrapper -> row.getGroupedId() != null ? row.getGroupedId().equals(wrapper.getGroupedEntityId()) : wrapper.getGroupedEntityId() == null);
            row.addIncrement(PASSED_AMOUNT_COLUMN, wrapper -> {
                if (row.getGroupedId() != null ? row.getGroupedId().equals(wrapper.getGroupedEntityId()) : wrapper.getGroupedEntityId() == null)
                {
                    return !wrapper.getDicsPassedMap().values().contains(null) && !wrapper.getDicsPassedMap().values().contains(Boolean.FALSE);
                }
                return false;
            });
            row.addIncrement("discAdd", wrapper -> {
                if (row.getGroupedId() != null ? row.getGroupedId().equals(wrapper.getGroupedEntityId()) : wrapper.getGroupedEntityId() == null)
                {
                    discSet.addAll(wrapper.getDicsPassedMap().keySet());
                }
                return false;
            });
            row.addIncrement(NOT_PASSED_2_COLUMN, wrapper -> {
                if (row.getGroupedId() != null ? row.getGroupedId().equals(wrapper.getGroupedEntityId()) : wrapper.getGroupedEntityId() == null)
                {
                    int result = 0;
                    ArrayList<Boolean> list = new ArrayList<>(wrapper.getDicsPassedMap().values());
                    while (list.remove(null))
                    {
                        result++;
                    }
                    while (list.remove(Boolean.FALSE))
                    {
                        result++;
                    }
                    return result > 0 && result <= 2;

                }
                return false;
            });
            row.addIncrement(NOT_PASSED_OTHER_COLUMN, wrapper -> {
                if (row.getGroupedId() != null ? row.getGroupedId().equals(wrapper.getGroupedEntityId()) : wrapper.getGroupedEntityId() == null)
                {
                    int result = 0;
                    ArrayList<Boolean> list = new ArrayList<>(wrapper.getDicsPassedMap().values());
                    while (list.remove(null))
                    {
                        result++;
                    }
                    while (list.remove(Boolean.FALSE))
                    {
                        result++;
                    }
                    return result > 2;

                }
                return false;
            });

            for (StudentWrapper wrapper : model.getStudentWrapperList())
                row.inc(wrapper);

            row.setProperty(DISCIPLINE_AMOUNT_COLUMN, discSet.size());
            final Double source = 100d * ((Number) row.getProperty(PASSED_AMOUNT_COLUMN)).doubleValue() / ((Number) row.getProperty(STUDENT_AMOUNT_COLUMN)).doubleValue();
            row.setProperty(PASSED_PERCENT_COLUMN, doubleFormatter.format(source.isNaN() ? 0d : source));
        }

        model.setRowWrapperList(rowWrapperList);

        return model;
    }

    /**
     * Подготавливает строки отчета. Без логики колонок.
     * Для каждого варианта группировки студентов в отчете строки будут разные.
     */
    protected <M extends Model> List<RowWrapper> getRowWrapperRawList(M model)
    {
        DQLSelectBuilder builder = model.getFilterAddon().prepareEntityBuilder("s", null);
        addJoinPathAndColumns(builder, model.getResultFor().getId());

        builder.predicate(DQLPredicateType.distinct);

        List<RowWrapper> resultList = new ArrayList<>();
        for (Object[] objects : builder.createStatement(getSession()).<Object[]>list())
        {
            resultList.add(new RowWrapper((Long)objects[0], objects[1] == null ? "вне групп" : (String)objects[1]));
        }

        return resultList;
    }

    protected String getObjectToStringOrZeroIfNull(Object o)
    {
        return o != null ? o.toString() : "0";
    }

    /**
     * Заменяет метки в документе по данным в модели.
     */
    protected <M extends Model> RtfDocument injectModifier(RtfDocument document, M model)
    {
        List<String[]> lineList = new ArrayList<>();
        for (RowWrapper row : model.getRowWrapperList())
        {
            String[] line = new String[7];

            line[0] = row.getTitle();
            line[1] = getObjectToStringOrZeroIfNull(row.getProperty(STUDENT_AMOUNT_COLUMN));
            line[2] = getObjectToStringOrZeroIfNull(row.getProperty(DISCIPLINE_AMOUNT_COLUMN));
            line[3] = getObjectToStringOrZeroIfNull(row.getProperty(PASSED_AMOUNT_COLUMN));
            line[4] = getObjectToStringOrZeroIfNull(row.getProperty(PASSED_PERCENT_COLUMN));
            line[5] = getObjectToStringOrZeroIfNull(row.getProperty(NOT_PASSED_2_COLUMN));
            line[6] = getObjectToStringOrZeroIfNull(row.getProperty(NOT_PASSED_OTHER_COLUMN));

            lineList.add(line);
        }

        TopOrgUnit topOrgUnit = TopOrgUnit.getInstance();
        String academyTitle = topOrgUnit.getNominativeCaseTitle() != null ? topOrgUnit.getNominativeCaseTitle() : topOrgUnit.getFullTitle();
        String formativeOrgUnits = model.getOrgUnit() == null ? null : (model.getOrgUnit().getNominativeCaseTitle() != null ? model.getOrgUnit().getNominativeCaseTitle() : model.getOrgUnit().getFullTitle());
        String term = String.valueOf(model.getAttestation() != null ? model.getAttestation().getSessionObject().getYearDistributionPart().getNumber() : model.getYearPart().getNumber());

        RtfInjectModifier modifier = new RtfInjectModifier();

        if (model.getSpecialityGroupTitle() != null)
            modifier.put("specialityGroupTitle", model.getSpecialityGroupTitle());
        if (formativeOrgUnits != null)
            modifier.put("formativeOrgUnits", formativeOrgUnits);

        //должность и ФИО руководителя
        SessionReportManager.addOuLeaderData(modifier, model.getOrgUnit(), "ouleader", "FIOouleader");

        modifier
                .put("academyTitle", academyTitle)
                .put("eduYear", model.getEduYear().getTitle())
                .put("term", term)
                .put("groupTitle", getGroupTitle(model.getResultFor().getId()))
                .put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()))
                .put("executor", UserContext.getInstance().getPrincipalContext().getFio())
                .modify(document);

        RtfTableModifier tableModifier = new RtfTableModifier();

        if (formativeOrgUnits == null)
            tableModifier.put("formativeOrgUnits", (String[][]) null);
        if (model.getSpecialityGroupTitle() == null)
            tableModifier.put("specialityGroupTitle", (String[][]) null);

        tableModifier
                .put("T", lineList.toArray(new String[lineList.size()][7]))
                .modify(document);

        return document;
    }

    /**
     * Заголовок перовй колонки отчета. Зависит от того как группируются студенты.
     */
    protected String getGroupTitle(Long resultForId)
    {
        if (resultForId.equals(AttestationReportTotalResultAdd.DW_GROUP_ID))
            return "Группа";
        if (resultForId.equals(AttestationReportTotalResultAdd.DW_COURSE_ID) || resultForId.equals(AttestationReportTotalResultAdd.DW_DIRECTION_DETAIL_ID))
            return "Курс";
        if (resultForId.equals(AttestationReportTotalResultAdd.DW_DIRECTION_ID))
            return "Направление";

        throw new IllegalArgumentException("unknown grouped");
    }

    /**
     * Путь до id проперти студента, по которой группируются студенты в отчете.
     */
    protected MetaDSLPath getGroupedStudentIdPath(Long resultForId)
    {
        if (resultForId.equals(AttestationReportTotalResultAdd.DW_GROUP_ID))
            return Student.group().id();
        if (resultForId.equals(AttestationReportTotalResultAdd.DW_COURSE_ID))
            return Student.course().id();
        if (resultForId.equals(AttestationReportTotalResultAdd.DW_DIRECTION_ID))
            return Student.educationOrgUnit().educationLevelHighSchool().id();

        throw new IllegalArgumentException("unknown grouped");
    }

    /**
     * Добавляет в билдер пути и колонки, зависят от того как группируются студенты в отчете.
     * В выборке должно быть две колонки: {groupEntityIdId, rowTitle}
     */
    protected DQLSelectBuilder addJoinPathAndColumns(DQLSelectBuilder builder, Long resultForId)
    {
        if (resultForId.equals(AttestationReportTotalResultAdd.DW_GROUP_ID))
        {
            builder
                    .joinPath(DQLJoinType.left, SessionAttestationSlot.studentWpe().student().group().fromAlias("s"), "g")
                    .column(property(Group.id().fromAlias("g")))
                    .column(property(Group.title().fromAlias("g")))
                    .order(property(SessionAttestationSlot.studentWpe().student().group().title().fromAlias("s")));
        }
        else if (resultForId.equals(AttestationReportTotalResultAdd.DW_COURSE_ID))
        {
            builder
                    .joinPath(DQLJoinType.left, SessionAttestationSlot.studentWpe().student().course().fromAlias("s"), "c")
                    .column(property(Course.id().fromAlias("c")))
                    .column(property(Course.title().fromAlias("c")))
                    .order(property(SessionAttestationSlot.studentWpe().student().course().title().fromAlias("s")));
        }
        else if (resultForId.equals(AttestationReportTotalResultAdd.DW_DIRECTION_ID))
        {
            builder
                    .joinPath(DQLJoinType.left, SessionAttestationSlot.studentWpe().student().educationOrgUnit().educationLevelHighSchool().fromAlias("s"), "eduLvl")
                    .column(property(EducationLevelsHighSchool.id().fromAlias("eduLvl")))
                    .column(property(EducationLevelsHighSchool.displayableTitle().fromAlias("eduLvl")))
                    .order(property(SessionAttestationSlot.studentWpe().student().educationOrgUnit().educationLevelHighSchool().displayableTitle().fromAlias("s")));
        }
        else
            throw new IllegalArgumentException("unknown grouped");

        return builder;
    }

    /**
     * Группируем студентов по НПВ.
     */
    protected <M extends Model> void prepareGroupedEduLvlHSStudentWrapper(M model)
    {
        DQLSelectBuilder builder = model.getFilterAddon().prepareEntityBuilder("s", null)
                .joinPath(DQLJoinType.left, SessionAttestationSlot.mark().fromAlias("s"), "mark")
                .column(property(SessionAttestationSlot.studentWpe().student().id().fromAlias("s")))
                .column(property(SessionAttestationSlot.studentWpe().student().course().id().fromAlias("s")))
                .column(property(SessionAttestationSlot.bulletin().registryElementPart().id().fromAlias("s")))
                .column("mark")
                .column(property(SessionAttestationSlot.studentWpe().student().educationOrgUnit().educationLevelHighSchool().id().fromAlias("s")));

        Map<Long, Map<Long, StudentWrapper>> eduLvlStudentWrapperMap = new HashMap<>();
        for (Object[] objects : builder.createStatement(getSession()).<Object[]>list())
        {
            Map<Long, StudentWrapper> studentWrapperMap = eduLvlStudentWrapperMap.get(objects[4]);
            if (studentWrapperMap == null)
                eduLvlStudentWrapperMap.put((Long) objects[4], studentWrapperMap = new HashMap<>());

            StudentWrapper wrapper = studentWrapperMap.get(objects[0]);
            if (wrapper == null)
                studentWrapperMap.put((Long) objects[0], wrapper = new StudentWrapper((Long) objects[0], (Long) objects[1]));
            Boolean passed = wrapper.getDicsPassedMap().get(objects[2]);
            SessionAttestationMarkCatalogItem mark = (SessionAttestationMarkCatalogItem) objects[3];
            if (passed != null)
            {
                wrapper.getDicsPassedMap().put((Long) objects[2], passed || (mark != null && mark.isPositive()));
            }
            else
            {
                wrapper.getDicsPassedMap().put((Long) objects[2], mark != null ? mark.isPositive() : null);
            }
        }

        Map<Long, List<StudentWrapper>> resultMap = new HashMap<>();
        for (Map.Entry<Long, Map<Long, StudentWrapper>> entry : eduLvlStudentWrapperMap.entrySet())
            resultMap.put(entry.getKey(), new ArrayList<>(entry.getValue().values()));

        model.setGroupedStudentWrapperMap(resultMap);
    }

    /**
     * На каждое НПВ - своя таблица.
     * Подгтавливаем строки для каждой таблицы и заменяем метки.
     */
    protected <M extends Model> RtfDocument prepareRowAndInjectModifier(M model, RtfDocument document)
    {
        if (model.getGroupedStudentWrapperMap().isEmpty())
        {
            model.setSpecialityGroupTitle("");
            model.setRowWrapperList(new ArrayList<>());
            model.setStudentWrapperList(new ArrayList<>());

            return injectModifier(document, model);
        }

        Map<Long, String> eduLvlTitleMap = new HashMap<>();
        for (EducationLevelsHighSchool eduLvl : getList(EducationLevelsHighSchool.class, EducationLevelsHighSchool.displayableTitle().s()))
            eduLvlTitleMap.put(eduLvl.getId(), eduLvl.getDisplayableTitle());

        final DoubleFormatter doubleFormatter = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS;

        // ищем таблицу в документе
        RtfSearchResult tableSearchResult = UniRtfUtil.findRtfTableMark(document, "T");//таблица с данными по аттестации
        // сохраняем пустой "шаблон" таблиц
        RtfTable emptyTable = (RtfTable) tableSearchResult.getElementList().get(tableSearchResult.getIndex()).getClone();

        List<IRtfElement> elementList = document.getElementList();

        Iterator<Map.Entry<Long, List<StudentWrapper>>> iterator = model.getGroupedStudentWrapperMap().entrySet().iterator();
        for (Map.Entry<Long, List<StudentWrapper>> entry; iterator.hasNext();)
        {
            entry = iterator.next();

            List<RowWrapper> rowWrapperList = new ArrayList<>();
            for (Course course : DevelopGridDAO.getCourseMap().values())
            {
                final RowWrapper row = new RowWrapper(course.getId(), course.getTitle());

                final Set<Long> discSet = new HashSet<>();
                row.addIncrement(STUDENT_AMOUNT_COLUMN, wrapper -> row.getGroupedId() != null ? row.getGroupedId().equals(wrapper.getGroupedEntityId()) : wrapper.getGroupedEntityId() == null);

                row.addIncrement(PASSED_AMOUNT_COLUMN, wrapper -> {
                    if (row.getGroupedId() != null ? row.getGroupedId().equals(wrapper.getGroupedEntityId()) : wrapper.getGroupedEntityId() == null)
                    {
                        return !wrapper.getDicsPassedMap().values().contains(null) && !wrapper.getDicsPassedMap().values().contains(Boolean.FALSE);
                    }
                    return false;
                });

                row.addIncrement("discAdd", wrapper -> {
                    if (row.getGroupedId() != null ? row.getGroupedId().equals(wrapper.getGroupedEntityId()) : wrapper.getGroupedEntityId() == null)
                    {
                        discSet.addAll(wrapper.getDicsPassedMap().keySet());
                    }
                    return false;
                });
                row.addIncrement(NOT_PASSED_2_COLUMN, wrapper -> {
                    if (row.getGroupedId() != null ? row.getGroupedId().equals(wrapper.getGroupedEntityId()) : wrapper.getGroupedEntityId() == null)
                    {
                        int result = 0;
                        ArrayList<Boolean> list = new ArrayList<>(wrapper.getDicsPassedMap().values());
                        while (list.remove(null))
                        {
                            result++;
                        }
                        while (list.remove(Boolean.FALSE))
                        {
                            result++;
                        }
                        return result > 0 && result <= 2;

                    }
                    return false;
                });

                row.addIncrement(NOT_PASSED_OTHER_COLUMN, wrapper -> {
                    if (row.getGroupedId() != null ? row.getGroupedId().equals(wrapper.getGroupedEntityId()) : wrapper.getGroupedEntityId() == null)
                    {
                        int result = 0;
                        ArrayList<Boolean> list = new ArrayList<>(wrapper.getDicsPassedMap().values());
                        while (list.remove(null))
                        {
                            result++;
                        }
                        while (list.remove(Boolean.FALSE))
                        {
                            result++;
                        }
                        return result > 2;

                    }
                    return false;
                });

                for (StudentWrapper wrapper : entry.getValue())
                    row.inc(wrapper);

                row.setProperty(DISCIPLINE_AMOUNT_COLUMN, discSet.size());
                row.setProperty(PASSED_PERCENT_COLUMN, doubleFormatter.format(Double.valueOf(row.getProperty(STUDENT_AMOUNT_COLUMN).toString()).equals(0d) ? 0d : Double.valueOf(row.getProperty(PASSED_AMOUNT_COLUMN).toString()) / Double.valueOf(row.getProperty(STUDENT_AMOUNT_COLUMN).toString()) * 100));

                rowWrapperList.add(row);
            }

            model.setSpecialityGroupTitle(eduLvlTitleMap.get(entry.getKey()));
            model.setRowWrapperList(rowWrapperList);
            model.setStudentWrapperList(entry.getValue());

            injectModifier(document, model);

            // если это не последняя группа
            if (iterator.hasNext())
            {
                // то новый лист
                insertPageBreak(elementList);
                // и новая таблица
                elementList.add(emptyTable.getClone());
            }
        }

        return document;
    }

    /**
     * Вставляет разрыв страницы в документ.
     * @param elementList список элементов в документе
     */
    private void insertPageBreak(List<IRtfElement> elementList)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));

        elementList.add(elementFactory.createRtfControl(IRtfData.PAGE));

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));
    }
}
