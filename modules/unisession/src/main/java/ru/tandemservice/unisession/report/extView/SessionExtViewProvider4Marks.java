package ru.tandemservice.unisession.report.extView;

import org.tandemframework.hibsupport.dql.DQLCaseExpressionBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * session_marks_ext_view
 * @author vdanilov
 */
public class SessionExtViewProvider4Marks extends SimpleDQLExternalViewConfig {

    @Override
    protected DQLSelectBuilder buildDqlQuery()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
        .fromEntity(SessionMark.class, "sessionMark")
        .joinPath(DQLJoinType.inner, SessionMark.slot().studentWpeCAction().fromAlias("sessionMark"), "wpeCA")
        .where(isNull(property(EppStudentWpeCAction.removalDate().fromAlias("wpeCA")))); // только актуальные

        // Тип мероприятия
        dql.joinEntity("sessionMark", DQLJoinType.inner, EppFControlActionType.class, "ca",
                       eq(property("wpeCA", EppStudentWpeCAction.type()), property("ca", EppFControlActionType.eppGroupType())));

        // данные самого объекта
        column(dql, property(SessionMark.id().fromAlias("sessionMark")), "mark_id").comment("uid оценки");
        column(dql, property(SessionMark.performDate().fromAlias("sessionMark")), "mark_perform_date").comment("дата сдачи");

        // значение оценки
        dql.joinPath(DQLJoinType.inner, SessionMark.cachedMarkValue().fromAlias("sessionMark"), "markValue");
        column(dql, property(SessionMarkCatalogItem.code().fromAlias("markValue")), "value_code").comment("код значения оценки/отметки");
        column(dql, property(SessionMarkCatalogItem.title().fromAlias("markValue")), "value_title").comment("значение оценки/отметки");
        column(dql, property(SessionMarkCatalogItem.shortTitle().fromAlias("markValue")), "value_shortTitle").comment("значение оценки/отметки, сокр. вариант");

        // тип оценки (instanceof использовать нельзя - нужно делать join таблицам)
        dql.joinEntity("markValue", DQLJoinType.left, SessionMarkStateCatalogItem.class, "markValue_state", eq(property("markValue.id"), property("markValue_state.id")));
        dql.joinEntity("markValue", DQLJoinType.left, SessionMarkGradeValueCatalogItem.class, "markValue_grade", eq(property("markValue.id"), property("markValue_grade.id")));
        column(
            dql,
            new DQLCaseExpressionBuilder()
            .when(isNotNull(property("markValue_state.id")), value("отметка"))
            .when(isNotNull(property("markValue_grade.id")), value("оценка"))
            .otherwise(value(""))
            .build(),
            "value_type"
        ).comment("тип оценки - оценка/отметка");

        // документ (instanceof использовать нельзя - нужно делать join таблицам)
        dql.joinPath(DQLJoinType.inner, SessionMark.slot().document().fromAlias("sessionMark"), "document");
        dql.joinEntity("document", DQLJoinType.left, SessionObject.class, "document_so", eq(property("document.id"), property("document_so.id")));
        dql.joinEntity("document", DQLJoinType.left, SessionBulletinDocument.class, "document_bl", eq(property("document.id"), property("document_bl.id")));
        dql.joinEntity("document", DQLJoinType.left, SessionRetakeDocument.class, "document_rt", eq(property("document.id"), property("document_rt.id")));
        dql.joinEntity("document", DQLJoinType.left, SessionListDocument.class, "document_ls", eq(property("document.id"), property("document_ls.id")));
        dql.joinEntity("document", DQLJoinType.left, SessionSheetDocument.class, "document_sh", eq(property("document.id"), property("document_sh.id")));
        dql.joinEntity("document", DQLJoinType.left, SessionStudentGradeBookDocument.class, "document_gb", eq(property("document.id"), property("document_gb.id")));
        dql.joinEntity("document", DQLJoinType.left, SessionTransferInsideDocument.class, "document_tri", eq(property("document.id"), property("document_tri.id")));
        dql.joinEntity("document", DQLJoinType.left, SessionTransferOutsideDocument.class, "document_tro", eq(property("document.id"), property("document_tro.id")));
        column(
            dql,
            new DQLCaseExpressionBuilder()
            .when(isNotNull(property("document_so")), value("сводная ведомость"))
            .when(isNotNull(property("document_bl")), value("ведомость"))
            .when(isNotNull(property("document_rt")), value("ведомость пересдач"))
            .when(isNotNull(property("document_ls")), value("экзам. карточка"))
            .when(isNotNull(property("document_sh")), value("экзам. лист"))
            .when(isNotNull(property("document_gb")), value("итоговая оценка"))
            .when(isNotNull(property("document_tri")), value("внутреннее перезачтение"))
            .when(isNotNull(property("document_tro")), value("внешнее перезачтение"))
            .otherwise(value(""))
            .build(),
            "document_type"
        ).comment("вид документа");
        column(
            dql,
            new DQLCaseExpressionBuilder()
            .when(isNotNull(property("document_so")), value(""))
            .when(isNotNull(property("document_gb")), value(""))
            .otherwise(property(SessionDocument.number().fromAlias("document")))
            .build(),
            "document_number"
        ).comment("номер документа");

        // признаки «в сессию» и «итоговая»
        booleanColumn(dql, property(SessionMark.slot().inSession().fromAlias("sessionMark")), "mark_in_session", 1, 0)
        .comment("для обычных оценок: 1 - если оценка получена в сессию, 0 - вне сессии")
        .comment("для итоговых: 1 - итоговая только среди оценок в сессию, 0 - итоговая среди всех");

        column(
            dql,
            new DQLCaseExpressionBuilder()
            .when(isNotNull(property("document_gb")), value(1))
            .otherwise(value(0))
            .build(),
            "mark_is_final"
        ).comment("1 - итоговая оценка, 0 - оценка по документу");

        // год и семестр
        column(dql, property(EppStudentWpeCAction.studentWpe().year().educationYear().title().fromAlias("wpeCA")), "year_title").comment("учебный год");
        column(dql, property(EppStudentWpeCAction.studentWpe().year().educationYear().intValue().fromAlias("wpeCA")), "year_number").comment("первый календарный год учебного года");
        column(dql, property(EppStudentWpeCAction.studentWpe().term().intValue().fromAlias("wpeCA")), "term_number").comment("семестр обучения");
        column(dql, property(EppStudentWpeCAction.studentWpe().course().intValue().fromAlias("wpeCA")), "course_number").comment("курс (на котором получена оценка)");
        column(dql, property(EppStudentWpeCAction.studentWpe().part().yearDistribution().code().fromAlias("wpeCA")), "year_distrib_code").comment("код разбиения года по семестрам");
        column(dql, property(EppStudentWpeCAction.studentWpe().part().yearDistribution().title().fromAlias("wpeCA")), "year_distrib_title").comment("название разбиения года по семестрам");
        column(dql, property(EppStudentWpeCAction.studentWpe().part().code().fromAlias("wpeCA")), "year_part_code").comment("код части уч. года");
        column(dql, property(EppStudentWpeCAction.studentWpe().part().title().fromAlias("wpeCA")), "year_part_title").comment("часть уч. года");

        // форма конторля
        column(dql, property(EppFControlActionType.code().fromAlias("ca")), "controlaction_code").comment("код формы контроля");
        column(dql, property(EppFControlActionType.title().fromAlias("ca")), "controlaction_title").comment("название формы контроля");

        // дисциплина и ее часть
        column(dql, property(EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().id().fromAlias("wpeCA")), "regel_id").comment("uid дисциплины реестра");
        column(dql, property(EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().number().fromAlias("wpeCA")), "regel_number").comment("реестровый номер дисциплины");
        column(dql, property(EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().title().fromAlias("wpeCA")), "regel_title").comment("название дисциплины");
        column(dql, property(EppStudentWpeCAction.studentWpe().registryElementPart().id().fromAlias("wpeCA")), "regel_part_id").comment("uid части дисциплины");
        column(dql, property(EppStudentWpeCAction.studentWpe().registryElementPart().number().fromAlias("wpeCA")), "regel_part_numer").comment("порядковый номер части дисциплины");

        // студент
        dql.joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().student().fromAlias("wpeCA"), "student");
        column(dql, property(Student.id().fromAlias("student")), "student_id").comment("uid студента");
        column(dql, property(Student.personalNumber().fromAlias("student")), "student_personal_number").comment("личный номер студента");
        column(dql, property(Student.person().identityCard().firstName().fromAlias("student")), "student_first_name").comment("имя студента");
        column(dql, property(Student.person().identityCard().middleName().fromAlias("student")), "student_middle_name").comment("отчество студента");
        column(dql, property(Student.person().identityCard().lastName().fromAlias("student")), "student_last_name").comment("фамилия студента");

        // и группа (left join т.к. группы может не быть)
        dql.joinPath(DQLJoinType.left, Student.group().fromAlias("student"), "group");
        column(dql, property(Group.id().fromAlias("group")), "student_current_group_id").comment("uid текущей академ. группы студента");
        column(dql, property(Group.title().fromAlias("group")), "student_current_group_title").comment("название текущей академ. группы студента");

        // НПП
        dql.joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().student().educationOrgUnit().fromAlias("wpeCA"), "eduOu");
        column(dql, property(EducationOrgUnit.id().fromAlias("eduOu")), "eduOu_id").comment("uid направления подготовки подразделения");
        column(dql, property(EducationOrgUnit.formativeOrgUnit().id().fromAlias("eduOu")), "formOu_id").comment("uid формирующего подразделения");
        column(dql, property(EducationOrgUnit.formativeOrgUnit().title().fromAlias("eduOu")), "formOu_title").comment("название формирующего подразделения");
        column(dql, property(EducationOrgUnit.formativeOrgUnit().shortTitle().fromAlias("eduOu")), "formOu_shortTitle").comment("сокр. название формирующего подразделения");
        column(dql, property(EducationOrgUnit.territorialOrgUnit().id().fromAlias("eduOu")), "terrOu_id").comment("uid территориального подразделения");
        column(dql, property(EducationOrgUnit.territorialOrgUnit().title().fromAlias("eduOu")), "terrOu_title").comment("название территориального подразделения");
        column(dql, property(EducationOrgUnit.territorialOrgUnit().shortTitle().fromAlias("eduOu")), "terrOu_shortTitle").comment("сокр. название территориального подразделения");

        column(dql, property(EducationOrgUnit.educationLevelHighSchool().title().fromAlias("eduOu")), "eduOu_title").comment("название направления подготовки");
        column(dql, property(EducationOrgUnit.educationLevelHighSchool().fullTitle().fromAlias("eduOu")), "eduOu_fullTitle").comment("полное название направления подготовки");
        column(dql, property(EducationOrgUnit.educationLevelHighSchool().shortTitle().fromAlias("eduOu")), "eduOu_shortTitle").comment("сокращенное название направления подготовки");
        column(dql, property(EducationOrgUnit.developForm().title().fromAlias("eduOu")), "developForm").comment("форма обучения");
        column(dql, property(EducationOrgUnit.developCondition().title().fromAlias("eduOu")), "developCondition").comment("условия обучения");

        return dql;
    }

}
