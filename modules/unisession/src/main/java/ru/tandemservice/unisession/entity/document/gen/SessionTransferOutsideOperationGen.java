package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.unisession.entity.document.SessionTransferOperation;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Перезачтенное мероприятие из другого ОУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionTransferOutsideOperationGen extends SessionTransferOperation
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation";
    public static final String ENTITY_NAME = "sessionTransferOutsideOperation";
    public static final int VERSION_HASH = -1434763289;
    private static IEntityMeta ENTITY_META;

    public static final String P_DISCIPLINE = "discipline";
    public static final String L_CONTROL_ACTION = "controlAction";
    public static final String P_MARK = "mark";

    private String _discipline;     // Перезачитываемая дисциплина (по документу)
    private EppFControlActionType _controlAction;     // Форма контроля (по документу)
    private String _mark;     // Оценка (по документу)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Перезачитываемая дисциплина (по документу). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Перезачитываемая дисциплина (по документу). Свойство не может быть null.
     */
    public void setDiscipline(String discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Форма контроля (по документу). Свойство не может быть null.
     */
    @NotNull
    public EppFControlActionType getControlAction()
    {
        return _controlAction;
    }

    /**
     * @param controlAction Форма контроля (по документу). Свойство не может быть null.
     */
    public void setControlAction(EppFControlActionType controlAction)
    {
        dirty(_controlAction, controlAction);
        _controlAction = controlAction;
    }

    /**
     * @return Оценка (по документу). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getMark()
    {
        return _mark;
    }

    /**
     * @param mark Оценка (по документу). Свойство не может быть null.
     */
    public void setMark(String mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionTransferOutsideOperationGen)
        {
            setDiscipline(((SessionTransferOutsideOperation)another).getDiscipline());
            setControlAction(((SessionTransferOutsideOperation)another).getControlAction());
            setMark(((SessionTransferOutsideOperation)another).getMark());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionTransferOutsideOperationGen> extends SessionTransferOperation.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionTransferOutsideOperation.class;
        }

        public T newInstance()
        {
            return (T) new SessionTransferOutsideOperation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "discipline":
                    return obj.getDiscipline();
                case "controlAction":
                    return obj.getControlAction();
                case "mark":
                    return obj.getMark();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "discipline":
                    obj.setDiscipline((String) value);
                    return;
                case "controlAction":
                    obj.setControlAction((EppFControlActionType) value);
                    return;
                case "mark":
                    obj.setMark((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "discipline":
                        return true;
                case "controlAction":
                        return true;
                case "mark":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "discipline":
                    return true;
                case "controlAction":
                    return true;
                case "mark":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "discipline":
                    return String.class;
                case "controlAction":
                    return EppFControlActionType.class;
                case "mark":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionTransferOutsideOperation> _dslPath = new Path<SessionTransferOutsideOperation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionTransferOutsideOperation");
    }
            

    /**
     * @return Перезачитываемая дисциплина (по документу). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation#getDiscipline()
     */
    public static PropertyPath<String> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Форма контроля (по документу). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation#getControlAction()
     */
    public static EppFControlActionType.Path<EppFControlActionType> controlAction()
    {
        return _dslPath.controlAction();
    }

    /**
     * @return Оценка (по документу). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation#getMark()
     */
    public static PropertyPath<String> mark()
    {
        return _dslPath.mark();
    }

    public static class Path<E extends SessionTransferOutsideOperation> extends SessionTransferOperation.Path<E>
    {
        private PropertyPath<String> _discipline;
        private EppFControlActionType.Path<EppFControlActionType> _controlAction;
        private PropertyPath<String> _mark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Перезачитываемая дисциплина (по документу). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation#getDiscipline()
     */
        public PropertyPath<String> discipline()
        {
            if(_discipline == null )
                _discipline = new PropertyPath<String>(SessionTransferOutsideOperationGen.P_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Форма контроля (по документу). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation#getControlAction()
     */
        public EppFControlActionType.Path<EppFControlActionType> controlAction()
        {
            if(_controlAction == null )
                _controlAction = new EppFControlActionType.Path<EppFControlActionType>(L_CONTROL_ACTION, this);
            return _controlAction;
        }

    /**
     * @return Оценка (по документу). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionTransferOutsideOperation#getMark()
     */
        public PropertyPath<String> mark()
        {
            if(_mark == null )
                _mark = new PropertyPath<String>(SessionTransferOutsideOperationGen.P_MARK, this);
            return _mark;
        }

        public Class getEntityClass()
        {
            return SessionTransferOutsideOperation.class;
        }

        public String getEntityName()
        {
            return "sessionTransferOutsideOperation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
