package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unisession_2x9x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionALRequest

		// создано обязательное свойство block
		{
			// создать колонку
			tool.createColumn("session_al_req_t", new DBColumn("block_id", DBType.LONG));

			// задать значение по умолчанию
            Statement stmt = tool.getConnection().createStatement();
            PreparedStatement update = tool.prepareStatement("update session_al_req_t set block_id = ? where id = ?");
            ResultSet rs = stmt.executeQuery("select distinct reqRow.request_id, rowBase.owner_id from session_al_req_row_t reqRow " +
                                                     "inner join epp_epvrowterm_t rowTerm on reqRow.rowterm_id = rowTerm.id " +
                                                     "inner join epp_epvrow_base_t rowBase on rowTerm.row_id = rowBase.id");

            while(rs.next())
            {
                Long requestId = rs.getLong(1);
                Long blockId = rs.getLong(2);

                update.setLong(1, blockId);
                update.setLong(2, requestId);
                update.executeUpdate();
            }

			// сделать колонку NOT NULL
			tool.setColumnNullable("session_al_req_t", "block_id", false);
		}
    }
}