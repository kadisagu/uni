package ru.tandemservice.unisession.entity.report;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.unisession.entity.report.gen.UnisessionDebtorsReportGen;

/**
 * Отчет «Должники»
 */
public class UnisessionDebtorsReport extends UnisessionDebtorsReportGen implements IStorableReport
{
    @Override
    @EntityDSLSupport(parts = UnisessionDebtorsReport.P_FORMING_DATE)
    public String getFormingDateStr()
    {
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(getFormingDate());
    }
}