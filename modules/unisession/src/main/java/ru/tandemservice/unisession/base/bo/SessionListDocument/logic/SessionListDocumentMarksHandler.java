package ru.tandemservice.unisession.base.bo.SessionListDocument.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author avedernikov
 * @since 19.07.2016
 */
public class SessionListDocumentMarksHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
	public static final String PARAM_ROWS = "rows";

	public SessionListDocumentMarksHandler(String ownerId)
	{
		super(ownerId);
	}

	@Override
	protected DSOutput execute(DSInput input, ExecutionContext context)
	{
		List<SessionListDocumentMarkRow> rows = context.getNotNull(PARAM_ROWS);
		Collections.sort(rows, Comparator
				.comparing((SessionListDocumentMarkRow row) -> row.getSlot().getStudentWpeCAction().getRegistryElementTitle())
				.thenComparing(row -> row.getSlot().getStudentWpeCAction().getStudentWpe().getTerm().getIntValue()));
		return ListOutputBuilder.get(input, rows).build();
	}
}