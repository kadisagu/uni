package ru.tandemservice.unisession.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отметка
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionMarkStateCatalogItemGen extends SessionMarkCatalogItem
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem";
    public static final String ENTITY_NAME = "sessionMarkStateCatalogItem";
    public static final int VERSION_HASH = -1429663748;
    private static IEntityMeta ENTITY_META;

    public static final String P_PRIORITY = "priority";
    public static final String P_VALID = "valid";
    public static final String P_REMARKABLE = "remarkable";
    public static final String P_VISIBLE = "visible";

    private int _priority;     // Приоритет
    private boolean _valid;     // Уважительная неявка
    private boolean _remarkable;     // Требует пересдачи
    private boolean _visible = true;     // Используется

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null и должно быть уникальным.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Уважительная неявка. Свойство не может быть null.
     */
    @NotNull
    public boolean isValid()
    {
        return _valid;
    }

    /**
     * @param valid Уважительная неявка. Свойство не может быть null.
     */
    public void setValid(boolean valid)
    {
        dirty(_valid, valid);
        _valid = valid;
    }

    /**
     * @return Требует пересдачи. Свойство не может быть null.
     */
    @NotNull
    public boolean isRemarkable()
    {
        return _remarkable;
    }

    /**
     * @param remarkable Требует пересдачи. Свойство не может быть null.
     */
    public void setRemarkable(boolean remarkable)
    {
        dirty(_remarkable, remarkable);
        _remarkable = remarkable;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isVisible()
    {
        return _visible;
    }

    /**
     * @param visible Используется. Свойство не может быть null.
     */
    public void setVisible(boolean visible)
    {
        dirty(_visible, visible);
        _visible = visible;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionMarkStateCatalogItemGen)
        {
            setPriority(((SessionMarkStateCatalogItem)another).getPriority());
            setValid(((SessionMarkStateCatalogItem)another).isValid());
            setRemarkable(((SessionMarkStateCatalogItem)another).isRemarkable());
            setVisible(((SessionMarkStateCatalogItem)another).isVisible());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionMarkStateCatalogItemGen> extends SessionMarkCatalogItem.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionMarkStateCatalogItem.class;
        }

        public T newInstance()
        {
            return (T) new SessionMarkStateCatalogItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "priority":
                    return obj.getPriority();
                case "valid":
                    return obj.isValid();
                case "remarkable":
                    return obj.isRemarkable();
                case "visible":
                    return obj.isVisible();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "valid":
                    obj.setValid((Boolean) value);
                    return;
                case "remarkable":
                    obj.setRemarkable((Boolean) value);
                    return;
                case "visible":
                    obj.setVisible((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "priority":
                        return true;
                case "valid":
                        return true;
                case "remarkable":
                        return true;
                case "visible":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "priority":
                    return true;
                case "valid":
                    return true;
                case "remarkable":
                    return true;
                case "visible":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "priority":
                    return Integer.class;
                case "valid":
                    return Boolean.class;
                case "remarkable":
                    return Boolean.class;
                case "visible":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionMarkStateCatalogItem> _dslPath = new Path<SessionMarkStateCatalogItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionMarkStateCatalogItem");
    }
            

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Уважительная неявка. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem#isValid()
     */
    public static PropertyPath<Boolean> valid()
    {
        return _dslPath.valid();
    }

    /**
     * @return Требует пересдачи. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem#isRemarkable()
     */
    public static PropertyPath<Boolean> remarkable()
    {
        return _dslPath.remarkable();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem#isVisible()
     */
    public static PropertyPath<Boolean> visible()
    {
        return _dslPath.visible();
    }

    public static class Path<E extends SessionMarkStateCatalogItem> extends SessionMarkCatalogItem.Path<E>
    {
        private PropertyPath<Integer> _priority;
        private PropertyPath<Boolean> _valid;
        private PropertyPath<Boolean> _remarkable;
        private PropertyPath<Boolean> _visible;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приоритет. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(SessionMarkStateCatalogItemGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Уважительная неявка. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem#isValid()
     */
        public PropertyPath<Boolean> valid()
        {
            if(_valid == null )
                _valid = new PropertyPath<Boolean>(SessionMarkStateCatalogItemGen.P_VALID, this);
            return _valid;
        }

    /**
     * @return Требует пересдачи. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem#isRemarkable()
     */
        public PropertyPath<Boolean> remarkable()
        {
            if(_remarkable == null )
                _remarkable = new PropertyPath<Boolean>(SessionMarkStateCatalogItemGen.P_REMARKABLE, this);
            return _remarkable;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem#isVisible()
     */
        public PropertyPath<Boolean> visible()
        {
            if(_visible == null )
                _visible = new PropertyPath<Boolean>(SessionMarkStateCatalogItemGen.P_VISIBLE, this);
            return _visible;
        }

        public Class getEntityClass()
        {
            return SessionMarkStateCatalogItem.class;
        }

        public String getEntityName()
        {
            return "sessionMarkStateCatalogItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
