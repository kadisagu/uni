/* $Id:$ */
package ru.tandemservice.unisession.base.bo.SessionListDocument.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unisession.entity.catalog.SessionsSimpleDocumentReason;
import ru.tandemservice.unisession.entity.document.SessionListDocument;

import java.util.List;

/**
 * @author oleyba
 * @since 4/10/15
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "card.id")
})
public class SessionListDocumentEditUI extends UIPresenter
{
    private SessionListDocument card = new SessionListDocument();

    @Override
    public void onComponentRefresh()
    {
        setCard(DataAccessServices.dao().getNotNull(SessionListDocument.class, getCard().getId()));
    }

    public void onClickApply() {
        DataAccessServices.dao().update(getCard());
        deactivate();
    }

    // getters and setters

    public SessionListDocument getCard()
    {
        return card;
    }

    public void setCard(SessionListDocument card)
    {
        this.card = card;
    }
}