package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ сессии
 *
 * Документ сессии - абстрактный объект, в котором фиксируются оценки студентов
 * После фиксации документа (closeDate != null) любые изменения в документе запрещены,
 * его печатная версия (должна быть) сохраняется в нем и существует вечно
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionDocument";
    public static final String ENTITY_NAME = "sessionDocument";
    public static final int VERSION_HASH = 1586369262;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_CLOSE_DATE = "closeDate";
    public static final String P_TYPE_SHORT_TITLE = "typeShortTitle";
    public static final String P_TYPE_TITLE = "typeTitle";

    private String _number;     // Номер документа
    private Date _formingDate;     // Дата формирования (фактическая)
    private Date _closeDate;     // Дата закрытия (фактическая)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер документа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер документа. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата формирования (фактическая). Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования (фактическая). Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Дата закрытия (фактическая).
     */
    public Date getCloseDate()
    {
        return _closeDate;
    }

    /**
     * @param closeDate Дата закрытия (фактическая).
     */
    public void setCloseDate(Date closeDate)
    {
        dirty(_closeDate, closeDate);
        _closeDate = closeDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionDocumentGen)
        {
            setNumber(((SessionDocument)another).getNumber());
            setFormingDate(((SessionDocument)another).getFormingDate());
            setCloseDate(((SessionDocument)another).getCloseDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionDocument.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("SessionDocument is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "formingDate":
                    return obj.getFormingDate();
                case "closeDate":
                    return obj.getCloseDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "closeDate":
                    obj.setCloseDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "formingDate":
                        return true;
                case "closeDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "formingDate":
                    return true;
                case "closeDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return String.class;
                case "formingDate":
                    return Date.class;
                case "closeDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionDocument> _dslPath = new Path<SessionDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionDocument");
    }
            

    /**
     * @return Номер документа. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocument#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата формирования (фактическая). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocument#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Дата закрытия (фактическая).
     * @see ru.tandemservice.unisession.entity.document.SessionDocument#getCloseDate()
     */
    public static PropertyPath<Date> closeDate()
    {
        return _dslPath.closeDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionDocument#getTypeShortTitle()
     */
    public static SupportedPropertyPath<String> typeShortTitle()
    {
        return _dslPath.typeShortTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionDocument#getTypeTitle()
     */
    public static SupportedPropertyPath<String> typeTitle()
    {
        return _dslPath.typeTitle();
    }

    public static class Path<E extends SessionDocument> extends EntityPath<E>
    {
        private PropertyPath<String> _number;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _closeDate;
        private SupportedPropertyPath<String> _typeShortTitle;
        private SupportedPropertyPath<String> _typeTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер документа. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocument#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(SessionDocumentGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата формирования (фактическая). Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.document.SessionDocument#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(SessionDocumentGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Дата закрытия (фактическая).
     * @see ru.tandemservice.unisession.entity.document.SessionDocument#getCloseDate()
     */
        public PropertyPath<Date> closeDate()
        {
            if(_closeDate == null )
                _closeDate = new PropertyPath<Date>(SessionDocumentGen.P_CLOSE_DATE, this);
            return _closeDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionDocument#getTypeShortTitle()
     */
        public SupportedPropertyPath<String> typeShortTitle()
        {
            if(_typeShortTitle == null )
                _typeShortTitle = new SupportedPropertyPath<String>(SessionDocumentGen.P_TYPE_SHORT_TITLE, this);
            return _typeShortTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisession.entity.document.SessionDocument#getTypeTitle()
     */
        public SupportedPropertyPath<String> typeTitle()
        {
            if(_typeTitle == null )
                _typeTitle = new SupportedPropertyPath<String>(SessionDocumentGen.P_TYPE_TITLE, this);
            return _typeTitle;
        }

        public Class getEntityClass()
        {
            return SessionDocument.class;
        }

        public String getEntityName()
        {
            return "sessionDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTypeShortTitle();

    public abstract String getTypeTitle();
}
