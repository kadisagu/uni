// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisession.component.student.ControlActionMarkHistory;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/9/11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setControlAction(this.getNotNull(EppStudentWpeCAction.class, model.getControlAction().getId()));

        this.prepareMarkInfo(model);

        model.setLineList(new ArrayList<String>());
        for (final SessionDocumentSlot slot : model.getData())
        {
            final StringBuilder line = new StringBuilder();
            line.append(slot.getDocument().getTypeTitle()).append(" (").append("дата формирования: ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(slot.getDocument().getFormingDate())).append(")");
            final SessionMark mark = model.getMarkMap().get(slot.getId());
            if (mark != null) {
                line.append(", оценка: ").append(mark.getValueTitle()).append(", дата сдачи: ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(mark.getPerformDate()));
            }
            model.getLineList().add(line.toString());
        }

        model.setEmpty(model.getData().isEmpty());

    }

    private void prepareMarkInfo(final Model model)
    {
        final DQLSelectBuilder slotsDQL = new DQLSelectBuilder()
        .fromEntity(SessionDocumentSlot.class, "slot")
        .column("slot")
        .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")), value(model.getControlAction())))
        // не в зачетке
        .joinEntity("slot", DQLJoinType.left, SessionStudentGradeBookDocument.class, "book", eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("book")))
        .where(isNull("book.id"))
        // или с оценкой, или не в семестровом журнале
        .joinEntity("slot", DQLJoinType.left, SessionMark.class, "mark", eq(property(SessionMark.slot().fromAlias("mark")), property("slot")))
        .joinEntity("slot", DQLJoinType.left, SessionObject.class, "so", eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("so")))
        .where(or(isNull("so.id"), isNotNull("mark.id")))
        .order(property(SessionDocumentSlot.document().formingDate().fromAlias("slot")));
        model.getData().clear();
        model.getMarkMap().clear();

        model.getData().addAll(slotsDQL.createStatement(this.getSession()).<SessionDocumentSlot>list());

        final DQLSelectBuilder marksDQL = new DQLSelectBuilder()
        .fromEntity(SessionMark.class, "mark")
        .column("mark")
        .where(in(property(SessionMark.slot().id().fromAlias("mark")), model.getData()));
        for (final SessionMark mark : marksDQL.createStatement(this.getSession()).<SessionMark>list()) {
            model.getMarkMap().put(mark.getSlot().getId(), mark);
        }

        final DQLSelectBuilder totals = new DQLSelectBuilder()
        .fromEntity(SessionMark.class, "mark")
        .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
        .joinEntity("slot", DQLJoinType.inner, SessionStudentGradeBookDocument.class, "book", eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("book")))
        .where(eq(property(SessionMark.slot().studentWpeCAction().fromAlias("mark")), value(model.getControlAction())))
        .column("mark");

        model.setTotalMark("-");
        model.setTotalSessionMark("-");
        for (final SessionMark mark : totals.createStatement(this.getSession()).<SessionMark>list()) {
            if (mark.isInSession()) {
                model.setTotalSessionMark(mark.getValueTitle());
            } else {
                model.setTotalMark(mark.getValueTitle());
            }
        }
    }
}
