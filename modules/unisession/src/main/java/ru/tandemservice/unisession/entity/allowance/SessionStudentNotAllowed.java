package ru.tandemservice.unisession.entity.allowance;

import ru.tandemservice.unisession.entity.allowance.gen.SessionStudentNotAllowedGen;

/**
 * Недопуск к сдаче сессии
 */
public class SessionStudentNotAllowed extends SessionStudentNotAllowedGen
{
    public boolean isNotActive()
    {
        return null != this.getRemovalDate();
    }

    public boolean isActive()
    {
        return null == this.getRemovalDate();
    }
}