package ru.tandemservice.unisession.entity.report;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unisession.entity.report.gen.*;

/** @see ru.tandemservice.unisession.entity.report.gen.StateFinalAttestationResultGen */
public abstract class StateFinalAttestationResult extends StateFinalAttestationResultGen
{

    public String getExamsPeriod()
    {
        DateFormatter formatter = DateFormatter.DEFAULT_DATE_FORMATTER;
        String dateFrom = getDateFrom() == null ? "" : "c " + formatter.format(getDateFrom());
        String dateTo = getDateTo() == null ? "" : "по " + formatter.format(getDateTo());
        return dateFrom + ((dateFrom.length() > 0 && dateTo.length() > 0) ? " " : "") + dateTo;
    }
}