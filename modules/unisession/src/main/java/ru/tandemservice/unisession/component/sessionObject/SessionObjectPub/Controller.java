/* $*/

package ru.tandemservice.unisession.component.sessionObject.SessionObjectPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.utils.PublisherColumnBuilder;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;

/**
 * @author oleyba
 * @since 3/24/11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        this.prepareSessionListDataSource(component);
        this.prepareBulletinListDataSource(component);
    }

    private void prepareSessionListDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final DynamicListDataSource<SessionStudentNotAllowed> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareSessionDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("№ зач. книжки", SessionStudentNotAllowed.student().bookNumber().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new PublisherColumnBuilder("ФИО", Student.FIO_KEY, "studentTab").tabBind(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY).entity(SessionStudentNotAllowed.student().s()).build());
        dataSource.addColumn(new SimpleColumn("Вид затрат", SessionStudentNotAllowed.student().compensationType().shortTitle().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Снять недопуск", "unlock_session", "onClickUnlock").setDisabledProperty("notActive").setPermissionKey("allowStudentSessionObject"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить запись о недопуске студента (студентки) «{0}»?", SessionStudentNotAllowed.student().person().fullFio()).setPermissionKey("deleteAllowanceInfoSessionObject"));

        model.setSessionDataSource(dataSource);
        this.getDao().prepareListDataSource(model);
    }

    private void prepareBulletinListDataSource(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final DynamicListDataSource<SessionStudentNotAllowedForBulletin> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareBulletinDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("№ зач. книжки", SessionStudentNotAllowedForBulletin.student().bookNumber().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new PublisherColumnBuilder("ФИО", Student.FIO_KEY, "studentTab").tabBind(ru.tandemservice.uni.component.student.StudentPub.Model.SELECTED_TAB_BIND_KEY).entity(SessionStudentNotAllowedForBulletin.student().s()).build());
        dataSource.addColumn(new SimpleColumn("Вид затрат", SessionStudentNotAllowedForBulletin.student().compensationType().shortTitle().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Снять недопуск", "unlock_session", "onClickUnlock").setDisabledProperty("notActive").setPermissionKey("allowStudentSessionObject"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить запись о недопуске студента (студентки) «{0}»?", SessionStudentNotAllowed.student().person().fullFio()).setPermissionKey("deleteAllowanceInfoSessionObject"));

        model.setBulletinDataSource(dataSource);
        this.getDao().prepareListDataSource(model);
    }

    public void onClickEditSessionObject(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionObject.SessionObjectAddEdit.Model.COMPONENT_NAME,
                new ParametersMap().add("id", this.getModel(component).getSession().getId())));
    }

    public void onClickDeleteSessionObject(final IBusinessComponent component)
    {
        this.getDao().delete(this.getModel(component).getSession().getId());
        this.deactivate(component);
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        this.getDao().deleteRow(component);
    }

    public void onClickUnlock(final IBusinessComponent component)
    {
        this.getDao().doUnlock(component.getListenerParameter());
    }

    public void onClickAddNotAllowed(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.allowance.SessionAllowanceAdd.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(component).getSession().getId())));
    }
}