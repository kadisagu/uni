package ru.tandemservice.unisession.entity.mark.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Оценка студента в сессии (регулярная)
 *
 * Показывает, что данная оценка выставлена человеком
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionSlotRegularMarkGen extends SessionMark
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark";
    public static final String ENTITY_NAME = "sessionSlotRegularMark";
    public static final int VERSION_HASH = 898046808;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionSlotRegularMarkGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionSlotRegularMarkGen> extends SessionMark.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionSlotRegularMark.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("SessionSlotRegularMark is abstract");
        }
    }
    private static final Path<SessionSlotRegularMark> _dslPath = new Path<SessionSlotRegularMark>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionSlotRegularMark");
    }
            

    public static class Path<E extends SessionSlotRegularMark> extends SessionMark.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return SessionSlotRegularMark.class;
        }

        public String getEntityName()
        {
            return "sessionSlotRegularMark";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
