package ru.tandemservice.unisession.component.group.JournalTab;

import org.tandemframework.core.CoreCollectionUtils.Pair;

import ru.tandemservice.uni.dao.IPrepareable;

public interface IDAO extends IPrepareable<Model>
{
    void prepareDataSource4Term(Model model);

    void prepare4Edit(Model model);

    boolean saveChanges(Model model);
}
