/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.Attestation.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;

/**
 * @author oleyba
 * @since 10/9/12
 */
@State({
    @Bind(key= UIPresenter.PUBLISHER_ID, binding="attestation.id")
})
public class AttestationPubUI extends UIPresenter
{
    private SessionAttestation attestation = new SessionAttestation();

    @Override
    public void onComponentRefresh()
    {
        setAttestation(IUniBaseDao.instance.get().get(SessionAttestation.class, getAttestation().getId()));
    }

    public void onClickClose()
    {
        AttestationManager.instance().dao().doClose(getAttestation());
    }

    public void onClickReopen()
    {
        AttestationManager.instance().dao().changeState(getAttestation());
    }

    public void onClickDelete()
    {
        IUniBaseDao.instance.get().delete(getAttestation());
        deactivate();
    }

    public SessionAttestation getAttestation()
    {
        return attestation;
    }

    public void setAttestation(SessionAttestation attestation)
    {
        this.attestation = attestation;
    }
}
