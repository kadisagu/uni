package ru.tandemservice.unisession.entity.document.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.document.SessionTransferDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Внешнее перезачтение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionTransferOutsideDocumentGen extends SessionTransferDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.document.SessionTransferOutsideDocument";
    public static final String ENTITY_NAME = "sessionTransferOutsideDocument";
    public static final int VERSION_HASH = 331345042;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionTransferOutsideDocumentGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionTransferOutsideDocumentGen> extends SessionTransferDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionTransferOutsideDocument.class;
        }

        public T newInstance()
        {
            return (T) new SessionTransferOutsideDocument();
        }
    }
    private static final Path<SessionTransferOutsideDocument> _dslPath = new Path<SessionTransferOutsideDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionTransferOutsideDocument");
    }
            

    public static class Path<E extends SessionTransferOutsideDocument> extends SessionTransferDocument.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return SessionTransferOutsideDocument.class;
        }

        public String getEntityName()
        {
            return "sessionTransferOutsideDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
