/* $Id: SessionReportGroupBulletinListListUI.java 21676 2012-01-24 08:19:15Z oleyba $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupBulletinListList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupBulletinListAdd.SessionReportGroupBulletinListAdd;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 12/16/11
 */
@State
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id", required = true)
})
public class SessionReportGroupBulletinListListUI extends UIPresenter
{
    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    // actions

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();
    }

    public void onSearchParamsChange()
    {
        onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());

        final Object year = getSettings().get("year");
        final Object part = getSettings().get("part");

        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, year);

        if (null == year || null == part)
            return;

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionObject.class, "obj").column("obj")
            .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(getOrgUnit())))
            .where(eq(property(SessionObject.yearDistributionPart().fromAlias("obj")), commonValue(part)))
            .where(eq(property(SessionObject.educationYear().fromAlias("obj")), commonValue(year)))
            .order(property(SessionObject.id().fromAlias("obj")));
        final List<Object> list = dql.createStatement(_uiSupport.getSession()).list();

        if (list.isEmpty())
            return;
        if (list.size() != 1)
            throw new IllegalStateException();

        dataSource.put(getSessionObjectParamName(), list.get(0));

        dataSource.put(SessionGroupBulletinListDSHandler.PARAM_COURSE, getSettings().get(SessionGroupBulletinListDSHandler.PARAM_COURSE));
    }

    public void onClickAddReport()
    {
        getActivationBuilder().asRegion(SessionReportGroupBulletinListAdd.class).parameter(SessionReportManager.BIND_ORG_UNIT, getOrgUnit().getId()).activate();
    }

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT).parameter("reportId", getListenerParameterAsLong()).parameter("extension", "rtf").parameter("zip", Boolean.FALSE).activate();
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    // preseneter

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    // utils

    protected String getSessionObjectParamName()
    {
        return SessionGroupBulletinListDSHandler.PARAM_SESSION_OBJECT;
    }

    public SingleSelectTextModel getCourseModel()
    {
        return new SingleSelectTextModel() {
            @Override
            public ListResult findValues(String filter) {

                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UnisessionGroupBulletinListReport.class, "rep")
                        .column(property("rep", UnisessionGroupBulletinListReport.course()))
                        .order(property("rep", UnisessionGroupBulletinListReport.course()))
                        .distinct();

                FilterUtils.applyLikeFilter(builder, filter, UnisessionGroupBulletinListReport.course().fromAlias("rep"));

                if (ouHolder.getValue() != null)
                    builder.where(eq(property(UnisessionGroupBulletinListReport.sessionObject().orgUnit().fromAlias("rep")), value(ouHolder.getValue())));

                return new ListResult<>(DataAccessServices.dao().getList(builder));

            }
        };
    }

    // getters and setters

    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }

    public String getViewPermissionKey(){ return getSec().getPermission("orgUnit_viewSessionReportGroupBulletinListList"); }

    public String getAddStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_addSessionReportGroupBulletinListList"); }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_deleteSessionReportGroupBulletinListList"); }

}
