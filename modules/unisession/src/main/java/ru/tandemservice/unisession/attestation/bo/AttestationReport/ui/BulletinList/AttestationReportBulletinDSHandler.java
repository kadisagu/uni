/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.BulletinList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.AttestationReportManager;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationBulletinReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 11/8/12
 */
public class AttestationReportBulletinDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    public AttestationReportBulletinDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {
        final OrgUnit orgUnit = context.get(SessionReportManager.PARAM_ORG_UNIT);
        final SessionAttestation sessionAttestation = context.get(AttestationReportManager.PARAM_ATTESTATION);
        final String group = context.get(AttestationReportManager.PARAM_GROUP);


        if (null == orgUnit || (null == sessionAttestation && null == group)) {
            return ListOutputBuilder.get(dsInput, Collections.emptyList()).build();
        }

        DQLSelectBuilder dql = new DQLSelectBuilder()
            .fromEntity(SessionAttestationBulletinReport.class, "r")
            ;

        dql.where(eq(property(SessionAttestationBulletinReport.attestation().sessionObject().orgUnit().fromAlias("r")), value(orgUnit)));

        if (null != sessionAttestation)
            dql.where(eq(property(SessionAttestationBulletinReport.attestation().fromAlias("r")), value(sessionAttestation)));

        if (null != group)
            dql.where(eq(property(SessionAttestationBulletinReport.group().fromAlias("r")), value(group)));

        registry.applyOrder(dql, dsInput.getEntityOrder());

        return DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
    }

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(SessionAttestationBulletinReport.class, "r");
    }
}