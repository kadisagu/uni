package ru.tandemservice.unisession.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * Копирование новых прав из старых для кнопок "Добавить ведомости пересдач" и "Добавить ведомости" (для аттестационных) - раньше были одинаковые права для двух аналогичных кнопок, теперь разные.
 * @author avedernikov
 * @since 13.04.2017
 */
public class MS_unisession_2x11x4_0to1 extends IndependentMigrationScript
{
	@Override
	public ScriptDependency[] getBoundaryDependencies()
	{
		return new ScriptDependency[]
				{
						new ScriptDependency("org.tandemframework", "1.6.18"),
						new ScriptDependency("org.tandemframework.shared", "1.11.4")
				};
	}

	@Override
	public void run(DBTool tool) throws Exception
	{
		createSamePermissions(tool, "autoCreateSessionRetakeDocs", "addSessionRetakeDocs");
		createSamePermissions(tool, "autoCreateAttestationBulletins", "addAttestationBulletins");
	}

	private static void createSamePermissions(DBTool tool, String sourcePermissionPrefix, String targetPermissionPrefix) throws SQLException
	{
		final String likeByPrefixPredicate = "accMat.permissionkey_p like '" + sourcePermissionPrefix + "_%'";
		SQLSelectQuery rolesWithSourcePerm = new SQLSelectQuery()
				.from(SQLFrom.table("accessmatrix_t", "accMat"))
				.column("accMat.role_id").column("accMat.permissionkey_p")
				.where(likeByPrefixPredicate);
		List<Object[]> rows = tool.executeQuery(MigrationUtils.processor(Long.class, String.class), tool.getDialect().getSQLTranslator().toSql(rolesWithSourcePerm));

		for (Object[] row : rows)
		{
			final String sourceKey = (String)row[1];
			row[1] = sourceKey.replace(sourcePermissionPrefix, targetPermissionPrefix);
		}

		final Short accessMatrixCode = tool.entityCodes().ensure("accessMatrix");
		MigrationUtils.BatchInsertBuilder inserter = new MigrationUtils.BatchInsertBuilder(accessMatrixCode, "role_id", "permissionkey_p");
		rows.forEach(row -> inserter.addRow(row[0], row[1]));
		inserter.executeInsert(tool, "accessmatrix_t");
	}
}
