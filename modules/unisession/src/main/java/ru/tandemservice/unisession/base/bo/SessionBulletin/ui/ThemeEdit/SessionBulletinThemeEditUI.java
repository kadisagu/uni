package ru.tandemservice.unisession.base.bo.SessionBulletin.ui.ThemeEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.unisession.base.bo.SessionBulletin.SessionBulletinManager;
import ru.tandemservice.unisession.base.bo.SessionBulletin.logic.ISessionBulletinThemeDAO;
import ru.tandemservice.unisession.base.bo.SessionBulletin.logic.SessionBulletinThemeRow;
import ru.tandemservice.unisession.base.bo.SessionBulletin.logic.SessionBulletinThemesDSHandler;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.ArrayList;
import java.util.List;

/**
 * @author avedernikov
 * @since 08.08.2016
 */
@State({
		@Bind(key = UIPresenter.PUBLISHER_ID, binding = "bulletin.id")
})
public class SessionBulletinThemeEditUI extends UIPresenter
{
	private SessionBulletinDocument bulletin = new SessionBulletinDocument();

	private List<SessionBulletinThemeRow> rows = new ArrayList<>();

	@Override
	public void onComponentRefresh()
	{
		ISessionBulletinThemeDAO dao = SessionBulletinManager.instance().themeDAO();
		bulletin = dao.getNotNull(SessionBulletinDocument.class, bulletin.getId());
		rows = dao.getBulletinThemeRows(bulletin);
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case SessionBulletinThemeEdit.THEMES_DS:
				dataSource.put(SessionBulletinThemesDSHandler.PARAM_ROWS, rows);
				break;
		}
	}

	public void onClickApply()
	{
		ISessionDocumentBaseDAO baseDAO = ISessionDocumentBaseDAO.instance.get();
		rows.forEach(row -> baseDAO.updateProjectTheme(row.getSlot(), row.getTheme(), null, row.getSupervisor()));

		if (bulletin.getGroup().getActivityPart().getRegistryElement().getParent().isThemeRequired())
			SessionBulletinManager.instance().themeDAO().updateFinalQualifyingWorkForStudents(rows);

		deactivate();
	}

	public SessionBulletinDocument getBulletin()
	{
		return bulletin;
	}

	public SessionBulletinThemeRow getCurrRow()
	{
		return getConfig().getDataSourceCurrentRecord(SessionBulletinThemeEdit.THEMES_DS);
	}
}
