/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package ru.tandemservice.unisession.component.orgunit.SessionTransferDocListTab;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author Vasily Zhukov
 * @since 10.01.2012
 */
public interface IDAO extends IPrepareable<Model>
{
    @Transactional(propagation= Propagation.SUPPORTS)
    void refreshDataSource(Model model);
}
