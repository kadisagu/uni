/**
 *$Id$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util;

import java.util.List;
import java.util.Map;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniAttestationFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;

/**
 * @author Alexander Shaburov
 * @since 22.11.12
 */
public class Model
{
    // form filters
    private EducationYear _eduYear;
    private SessionAttestation _attestation;
    private YearDistributionPart _yearPart;
    private Long _attNumber;
    private DataWrapper _resultFor;
    private OrgUnit _orgUnit;
    private List<OrgUnit> _formativeOrgUnitList;

    private UniAttestationFilterAddon _filterAddon;

    // print properties
    private List<StudentWrapper> _studentWrapperList;
    private List<RowWrapper> _rowWrapperList;
    private String _specialityGroupTitle;
    private Map<Long, List<StudentWrapper>> _groupedStudentWrapperMap;

    // Getters & Setters

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public Map<Long, List<StudentWrapper>> getGroupedStudentWrapperMap()
    {
        return _groupedStudentWrapperMap;
    }

    public void setGroupedStudentWrapperMap(Map<Long, List<StudentWrapper>> groupedStudentWrapperMap)
    {
        _groupedStudentWrapperMap = groupedStudentWrapperMap;
    }

    public String getSpecialityGroupTitle()
    {
        return _specialityGroupTitle;
    }

    public void setSpecialityGroupTitle(String specialityGroupTitle)
    {
        _specialityGroupTitle = specialityGroupTitle;
    }

    public List<StudentWrapper> getStudentWrapperList()
    {
        return _studentWrapperList;
    }

    public void setStudentWrapperList(List<StudentWrapper> studentWrapperList)
    {
        _studentWrapperList = studentWrapperList;
    }

    public List<RowWrapper> getRowWrapperList()
    {
        return _rowWrapperList;
    }

    public void setRowWrapperList(List<RowWrapper> rowWrapperList)
    {
        _rowWrapperList = rowWrapperList;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    public void setEduYear(EducationYear eduYear)
    {
        _eduYear = eduYear;
    }

    public SessionAttestation getAttestation()
    {
        return _attestation;
    }

    public void setAttestation(SessionAttestation attestation)
    {
        _attestation = attestation;
    }

    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    public void setYearPart(YearDistributionPart yearPart)
    {
        _yearPart = yearPart;
    }

    public Long getAttNumber()
    {
        return _attNumber;
    }

    public void setAttNumber(Long attNumber)
    {
        _attNumber = attNumber;
    }

    public DataWrapper getResultFor()
    {
        return _resultFor;
    }

    public void setResultFor(DataWrapper resultFor)
    {
        _resultFor = resultFor;
    }

    public UniAttestationFilterAddon getFilterAddon() {
        return _filterAddon;
    }

    public void setFilterAddon(UniAttestationFilterAddon filterAddon) {
        _filterAddon = filterAddon;
    }
}
