package ru.tandemservice.unisession.base.bo.SessionListDocument.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.unisession.entity.document.SessionListDocument;

import java.util.List;

/**
 * @author avedernikov
 * @since 20.07.2016
 */
public interface ISessionListDocumentMarkDAO extends ICommonDAO, INeedPersistenceSupport
{
	/**
	 * Получить список строк, содержащих информацию об оценке в экзаменационной карточке ({@link SessionListDocumentMarkRow}).
	 * @param listDocument Экзаменационная карточка, для которой требуются данные об оценках.
	 */
	@Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
	List<SessionListDocumentMarkRow> getListDocumentMarkRows(SessionListDocument listDocument);
}
