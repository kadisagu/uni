/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionSheet.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.GroupModel;
import ru.tandemservice.unisession.base.bo.SessionSheet.ui.Forming.SessionSheetFormingUI;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 18.02.13
 */
public class SessionSheetFormingDebtorsSearchDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public SessionSheetFormingDebtorsSearchDSHandler(String ownerId)
    {
        super(ownerId, SessionMark.class);
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final OrgUnit groupOrgUnit = context.get(SessionSheetFormingUI.PARAM_GROUP_ORG_UNIT);
        final EducationYear year = context.get(SessionReportManager.PARAM_EDU_YEAR);
        final YearDistributionPart yearPart = context.get(SessionSheetFormingUI.PARAM_EDU_YEAR_PART);
        final List<Course> courseList = context.get(SessionSheetFormingUI.PARAM_COURSE_LIST);
        final List<Group> groupList = context.get(SessionSheetFormingUI.PARAM_GROUP_LIST);
        final List<SessionMark> markList = context.get(SessionSheetFormingUI.PARAM_MARK_LIST);
        boolean isMarkListEmpty = markList == null || markList.isEmpty();

        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionMark.class, "m").column(property("m"));

        if (isMarkListEmpty)
        {
            if (yearPart == null || year == null)
            {
                final DSOutput build = ListOutputBuilder.get(input, new ArrayList<>()).build();
                build.setCountRecord(5);
                return build;
            }

            builder.where(in(property(SessionMark.slot().document().id().fromAlias("m")),
                             new DQLSelectBuilder().fromEntity(SessionStudentGradeBookDocument.class, "g").column(property(SessionStudentGradeBookDocument.id().fromAlias("g")))
                                     .buildQuery()))
                    .where(isNull(property(SessionMark.slot().studentWpeCAction().removalDate().fromAlias("m"))))
                    .where(eq(property(SessionMark.slot().studentWpeCAction().studentWpe().student().educationOrgUnit().groupOrgUnit().fromAlias("m")), value(groupOrgUnit)))
                    .where(eq(property(SessionMark.slot().inSession().fromAlias("m")), value(Boolean.FALSE)))
                    .where(eq(property(SessionMark.slot().studentWpeCAction().studentWpe().part().fromAlias("m")), value(yearPart)))
                    .where(eq(property(SessionMark.slot().studentWpeCAction().studentWpe().year().educationYear().fromAlias("m")), value(year)))
                    .where(eq(property(SessionMark.cachedMarkPositiveStatus().fromAlias("m")), value(Boolean.FALSE)))
                    .where(notExists(
                            new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "s").column(property(SessionDocumentSlot.id().fromAlias("s")))
                                    .where(notIn(property(SessionDocumentSlot.document().id().fromAlias("s")),
                                                 new DQLSelectBuilder().fromEntity(SessionStudentGradeBookDocument.class, "sg").column(property(SessionStudentGradeBookDocument.id().fromAlias("sg")))
                                                         .buildQuery()))
                                    .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("s")), property(SessionMark.slot().studentWpeCAction().fromAlias("m"))))
                                    .where(notExists(
                                            new DQLSelectBuilder().fromEntity(SessionMark.class, "slotMark").column(property(SessionMark.id().fromAlias("slotMark")))
                                                    .where(eq(property(SessionMark.slot().fromAlias("slotMark")), property("s")))
                                                    .buildQuery()))
                                    .buildQuery()));


            if (courseList != null && !courseList.isEmpty())
                builder.where(in(property(SessionMark.slot().studentWpeCAction().studentWpe().student().course().fromAlias("m")), courseList));

            if (groupList != null && !groupList.isEmpty())
            {
                if (groupList.contains(GroupModel.WRAPPER_NO_GROUP))
                {
                    groupList.remove(GroupModel.WRAPPER_NO_GROUP);
                    builder.where(or(
                            isNull(property(SessionMark.slot().studentWpeCAction().studentWpe().student().group().fromAlias("m"))),
                            in(property(SessionMark.slot().studentWpeCAction().studentWpe().student().group().fromAlias("m")), groupList)));
                }
                else
                {
                    builder.where(in(property(SessionMark.slot().studentWpeCAction().studentWpe().student().group().fromAlias("m")), groupList));
                }

            }
        }
        else
            builder.where(in(property("m", SessionMark.id()), markList));

        builder
                .order(property(SessionMark.slot().studentWpeCAction().studentWpe().student().course().fromAlias("m")))
                .order(property(SessionMark.slot().studentWpeCAction().studentWpe().student().group().fromAlias("m")))
                .order(property(SessionMark.slot().studentWpeCAction().studentWpe().student().person().identityCard().lastName().fromAlias("m")))
                .order(property(SessionMark.slot().studentWpeCAction().studentWpe().student().person().identityCard().firstName().fromAlias("m")))
                .order(property(SessionMark.slot().studentWpeCAction().studentWpe().student().person().identityCard().middleName().fromAlias("m")))
                .order(property(SessionMark.slot().studentWpeCAction().studentWpe().registryElementPart().registryElement().title().fromAlias("m")))
                .order(property(SessionMark.slot().studentWpeCAction().type().priority().fromAlias("m")));
        //сделал кол-во строк выбираемым на форме
        //input.setCountRecord(Integer.parseInt(ApplicationRuntime.getProperty("sessionSheetForming.debtorsSearchDS.countRecord")));
        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(isMarkListEmpty).build();
    }
}
