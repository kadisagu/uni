/**
 *$Id$
 */
package ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.AttestationReportManager;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.AttestationReportTotalResultAdd;
import ru.tandemservice.unisession.attestation.entity.report.SessionAttestationTotalResultReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

/**
 * @author Alexander Shaburov
 * @since 22.11.12
 */
@State
({
    @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "orgUnitHolder.id")
})
public class AttestationReportTotalResultListUI extends UIPresenter
{
    public static final String PARAM_ORG_UNIT = AttestationReportManager.PARAM_ORG_UNIT;

    public static final String PARAM_EDU_YEAR = SessionReportManager.PARAM_EDU_YEAR;
    public static final String PARAM_YEAR_PART = "yearDistributionPart";
    public static final String PARAM_ATT_NUMBER = "attestationNumber";
    public static final String PARAM_ATTESTATION = "attestation";

    private OrgUnitHolder _orgUnitHolder = new OrgUnitHolder();

    @Override
    public void onComponentRefresh()
    {
        _orgUnitHolder.refresh();
    }

    public boolean isOnOrgUnit()
    {
        return _orgUnitHolder.getId() != null;
    }

    public void onClickAddReport()
    {
        getActivationBuilder().asRegion(AttestationReportTotalResultAdd.class)
            .parameter(SessionReportManager.BIND_ORG_UNIT, _orgUnitHolder.getId())
            .activate();
    }

    public void onClickPrintReport()
    {
        SessionAttestationTotalResultReport report = DataAccessServices.dao().getNotNull(SessionAttestationTotalResultReport.class, getListenerParameterAsLong());

        byte[] content = report.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(report.getContent().getFilename()).document(content), false);
    }

    public void onClickDeleteReport()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_ORG_UNIT, _orgUnitHolder.getValue());

        dataSource.put(PARAM_EDU_YEAR, getSettings().get(PARAM_EDU_YEAR));
        dataSource.put(PARAM_YEAR_PART, getSettings().get(PARAM_YEAR_PART));
        dataSource.put(PARAM_ATT_NUMBER, getSettings().get(PARAM_ATT_NUMBER));
        dataSource.put(PARAM_ATTESTATION, getSettings().get(PARAM_ATTESTATION));
    }

    // Calculate getters

    @Override
    public ISecured getSecuredObject()
    {
        return _orgUnitHolder.getId() != null ? _orgUnitHolder.getValue() : super.getSecuredObject();
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return _orgUnitHolder.getId() != null ? _orgUnitHolder.getSecModel() : new CommonPostfixPermissionModel(null);
    }

    public String getViewPermissionKey(){ return getSec().getPermission(_orgUnitHolder.getId() != null ? "orgUnit_viewAttestationReportTotalResultList" : "attestationTotalResultReport"); }

    public String getAddStorableReportPermissionKey(){ return getSec().getPermission(_orgUnitHolder.getId() != null ? "orgUnit_addAttestationReportTotalResultList" : "addSessionStorableReport"); }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission(_orgUnitHolder.getId() != null ? "orgUnit_deleteAttestationReportTotalResultList" : "deleteSessionStorableReport"); }

    // Getters & Setters

    public OrgUnitHolder getOrgUnitHolder()
    {
        return _orgUnitHolder;
    }
}
