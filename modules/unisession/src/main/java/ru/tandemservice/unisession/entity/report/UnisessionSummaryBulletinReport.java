package ru.tandemservice.unisession.entity.report;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.unisession.entity.report.gen.UnisessionSummaryBulletinReportGen;

/**
 * Отчет «Сводная ведомость»
 */
public class UnisessionSummaryBulletinReport extends UnisessionSummaryBulletinReportGen implements IStorableReport
{
    @Override
    @EntityDSLSupport(parts = UnisessionSummaryBulletinReport.P_FORMING_DATE)
    public String getFormingDateStr()
    {
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(getFormingDate());
    }
}