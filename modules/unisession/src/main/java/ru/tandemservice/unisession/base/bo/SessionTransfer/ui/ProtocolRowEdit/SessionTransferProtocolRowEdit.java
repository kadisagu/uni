/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolRowEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unisession.base.bo.SessionReexamination.SessionReexaminationManager;

/**
 * @author Alexey Lopatin
 * @since 17.09.2015
 */
@Configuration
public class SessionTransferProtocolRowEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(SessionReexaminationManager.instance().reExaminationDSConfig())
                .addDataSource(SessionReexaminationManager.instance().markDSConfig())
                .create();
    }
}
