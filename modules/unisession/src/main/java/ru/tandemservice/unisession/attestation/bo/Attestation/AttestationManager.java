/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.Attestation;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unisession.attestation.bo.Attestation.logic.ISessionAttestationDao;
import ru.tandemservice.unisession.attestation.bo.Attestation.logic.SessionAttestationDao;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/9/12
 */
@Configuration
public class AttestationManager extends BusinessObjectManager
{
    public static final String PARAM_ORG_UNIT = "orgUnit";
    public static final String PARAM_EDU_YEAR = "year";
    public static final String PARAM_YEAR_PART = "part";

    public static final String DS_ATTESTATION = "attestationDS";

    public static AttestationManager instance()
    {
        return instance(AttestationManager.class);
    }

    @Bean
    public ISessionAttestationDao dao()
    {
        return new SessionAttestationDao();
    }

    @Bean
    public UIDataSourceConfig attestationDSConfig()
    {
        return SelectDSConfig.with(DS_ATTESTATION, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(this.attestationDSHandler())
        .addColumn(SessionAttestation.number().s())
        .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler attestationDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), SessionAttestation.class)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                final DQLSelectBuilder dql = super.query(alias, filter);
                if (StringUtils.isNotEmpty(filter)) {
                    dql.where(likeUpper(DQLFunctions.cast(property(SessionAttestation.number().fromAlias(alias)), PropertyType.STRING), value(CoreStringUtils.escapeLike(filter))));
                }
                return dql;
            }
        }
            .pageable(true)
            .where(SessionAttestation.sessionObject().orgUnit(), PARAM_ORG_UNIT)
            .where(SessionAttestation.sessionObject().educationYear(), PARAM_EDU_YEAR)
            .where(SessionAttestation.sessionObject().yearDistributionPart(), PARAM_YEAR_PART)
            .order(SessionAttestation.number())
        ;
    }
}
