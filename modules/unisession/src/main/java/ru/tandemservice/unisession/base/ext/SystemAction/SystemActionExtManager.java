/**
 *$Id$
 */
package ru.tandemservice.unisession.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.unisession.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("uniio_exportSessionTemplate", new SystemActionDefinition("unisession", "exportSessionTemplate", "onClickExportSessionTemplate", SystemActionPubExt.SESSION_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uniio_importMarks", new SystemActionDefinition("unisession", "importMarks", "onClickImportMarks", SystemActionPubExt.SESSION_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unisession_forceRefreshTotalMarks", new SystemActionDefinition("unisession", "forceRefreshTotalMarks", "onClickRefreshTotalMarks", SystemActionPubExt.SESSION_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}
