/* $*/

package ru.tandemservice.unisession.component.orgunit.SessionSheetListTab;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author oleyba
 * @since 2/28/11
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{
    void delete(Long id);
}
