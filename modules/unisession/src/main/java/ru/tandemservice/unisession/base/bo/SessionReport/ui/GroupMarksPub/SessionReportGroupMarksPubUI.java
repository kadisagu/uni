/* $Id: SessionReportGroupMarksPubUI.java 21962 2012-02-19 13:57:27Z oleyba $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupMarksPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unisession.entity.report.UnisessionGroupMarksReport;

/**
 * @author iolshvang
 * @since 14.07.11 21:44
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "report.id")
})
public class SessionReportGroupMarksPubUI extends UIPresenter
{
    private UnisessionGroupMarksReport _report = new UnisessionGroupMarksReport();
    private CommonPostfixPermissionModelBase sec;

    @Override
    public void onComponentRefresh()
    {
        setReport(DataAccessServices.dao().getNotNull(UnisessionGroupMarksReport.class, getReport().getId()));
        setSec(new OrgUnitSecModel(getReport().getOrgUnit()));
    }

    // Listeners

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report.getId());
        deactivate();
    }

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT).parameter("reportId", _report.getId()).parameter("extension", "xls").parameter("zip", Boolean.FALSE).activate();
    }

    // Getters & Setters

    public UnisessionGroupMarksReport getReport()
    {
        return _report;
    }

    public void setReport(UnisessionGroupMarksReport report)
    {
        _report = report;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return sec;
    }

    public void setSec(CommonPostfixPermissionModelBase sec)
    {
        this.sec = sec;
    }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_deleteSessionReportGroupMarksList"); }

}
