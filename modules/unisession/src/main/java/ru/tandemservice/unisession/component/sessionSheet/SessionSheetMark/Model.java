/* $*/

package ru.tandemservice.unisession.component.sessionSheet.SessionSheetMark;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.form.translator.Translator;
import org.apache.tapestry.form.validator.Max;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.translator.StrongNumberTranslator;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 2/22/11
 */
@Input({
    @Bind(key=PublisherActivator.PUBLISHER_ID_KEY, binding="sheet.id")})
@Output({
    @Bind(key="markId", binding="markId"),
    @Bind(key="controlActionId", binding="slot.student.id")
})
public class Model
{
    public static final String COMPONENT_NAME = Model.class.getPackage().getName();
    private SessionSheetDocument sheet = new SessionSheetDocument();
    private SessionDocumentSlot slot = new SessionDocumentSlot();
    private SessionTermModel.TermWrapper term;
    private List<PpsEntry> ppsList;

    private ISessionBrsDao.ISessionRatingSettings ratingSettings;
    private Double currentRating;
    private boolean needToSaveCurrentRatingData;

    private Double points;
    private Long markId;
    private SessionMarkCatalogItem mark;
    private Date performDate;

    private ISelectModel studentModel;
    private ISelectModel termModel;
    private ISelectModel controlActionModel;
    private ISelectModel ppsModel;
    private ISelectModel markModel;

    public List<Validator> getPointsValidator()
    {
        ISessionBrsDao.ISessionPointsValidator validator = getRatingSettings() == null ? null : getRatingSettings().pointsValidator();
        if (null == validator) { return null; }

        return Arrays.<Validator>asList(
            new Min("min=" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(validator.min())),
            new Max("max=" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(validator.max()))
        );
    }

    public Translator getPointsTranslator()
    {
        ISessionBrsDao.ISessionPointsValidator validator = getRatingSettings() == null ? null : getRatingSettings().pointsValidator();
        if (null == validator) {
            return new StrongNumberTranslator();
        }

        return new StrongNumberTranslator(validator.precision() <= 0 ? "pattern=#" : StringUtils.rightPad("pattern=#.", validator.precision(), '#'));
    }

    public boolean isUsePoints()
    {
        return getRatingSettings() != null && getRatingSettings().usePoints();
    }

    public boolean isUseCurrentRating()
    {
        return getRatingSettings() != null && getRatingSettings().useCurrentRating();
    }

    // getters and setters

    public SessionSheetDocument getSheet()
    {
        return this.sheet;
    }

    public void setSheet(final SessionSheetDocument sheet)
    {
        this.sheet = sheet;
    }

    public Long getMarkId()
    {
        return this.markId;
    }

    public void setMarkId(final Long markId)
    {
        this.markId = markId;
    }

    public SessionDocumentSlot getSlot()
    {
        return this.slot;
    }

    public void setSlot(final SessionDocumentSlot slot)
    {
        this.slot = slot;
    }

    public SessionTermModel.TermWrapper getTerm()
    {
        return this.term;
    }

    public void setTerm(final SessionTermModel.TermWrapper term)
    {
        this.term = term;
    }

    public List<PpsEntry> getPpsList()
    {
        return this.ppsList;
    }

    public void setPpsList(final List<PpsEntry> ppsList)
    {
        this.ppsList = ppsList;
    }

    public SessionMarkCatalogItem getMark()
    {
        return this.mark;
    }

    public void setMark(final SessionMarkCatalogItem mark)
    {
        this.mark = mark;
    }

    public Date getPerformDate()
    {
        return this.performDate;
    }

    public void setPerformDate(final Date performDate)
    {
        this.performDate = performDate;
    }

    public ISelectModel getStudentModel()
    {
        return this.studentModel;
    }

    public void setStudentModel(final ISelectModel studentModel)
    {
        this.studentModel = studentModel;
    }

    public ISelectModel getTermModel()
    {
        return this.termModel;
    }

    public void setTermModel(final ISelectModel termModel)
    {
        this.termModel = termModel;
    }

    public ISelectModel getControlActionModel()
    {
        return this.controlActionModel;
    }

    public void setControlActionModel(final ISelectModel controlActionModel)
    {
        this.controlActionModel = controlActionModel;
    }

    public ISelectModel getPpsModel()
    {
        return this.ppsModel;
    }

    public void setPpsModel(final ISelectModel ppsModel)
    {
        this.ppsModel = ppsModel;
    }

    public ISelectModel getMarkModel()
    {
        return this.markModel;
    }

    public void setMarkModel(final ISelectModel markModel)
    {
        this.markModel = markModel;
    }

    public Double getPoints()
    {
        return points;
    }

    public void setPoints(Double points)
    {
        this.points = points;
    }

    public ISessionBrsDao.ISessionRatingSettings getRatingSettings()
    {
        return ratingSettings;
    }

    public void setRatingSettings(ISessionBrsDao.ISessionRatingSettings ratingSettings)
    {
        this.ratingSettings = ratingSettings;
    }

    public Double getCurrentRating()
    {
        return currentRating;
    }

    public void setCurrentRating(Double currentRating)
    {
        this.currentRating = currentRating;
    }

    public boolean isNeedToSaveCurrentRatingData()
    {
        return needToSaveCurrentRatingData;
    }

    public void setNeedToSaveCurrentRatingData(boolean needToSaveCurrentRatingData)
    {
        this.needToSaveCurrentRatingData = needToSaveCurrentRatingData;
    }
}
