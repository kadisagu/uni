/* $*/

package ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinEdit;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author oleyba
 * @since 3/3/11
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{

}
