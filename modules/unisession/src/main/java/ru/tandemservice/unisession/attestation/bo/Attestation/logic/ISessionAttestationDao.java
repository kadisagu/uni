/* $Id:$ */
package ru.tandemservice.unisession.attestation.bo.Attestation.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.Collection;

/**
 * @author oleyba
 * @since 10/9/12
 */
public interface ISessionAttestationDao extends INeedPersistenceSupport
{
    SessionAttestation addAttestation(SessionObject sessionObject);

    void changeState(SessionAttestation attestation);

    void doClose(SessionAttestation attestation);
}
