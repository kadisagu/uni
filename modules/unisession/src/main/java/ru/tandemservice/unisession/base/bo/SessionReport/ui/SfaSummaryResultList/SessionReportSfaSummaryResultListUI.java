/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.SfaSummaryResultList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.unisession.entity.report.SfaSummaryResult;

/**
 * @author Andrey Andreev
 * @since 27.10.2016
 */
public abstract class SessionReportSfaSummaryResultListUI extends UIPresenter
{
    protected OrgUnitHolder _ouHolder = new OrgUnitHolder();
    protected UniEduProgramEducationOrgUnitAddon _educationOrgUnitUtil;


    @Override
    public void onComponentRefresh()
    {
        configEducationOrgUnitFields();
    }

    protected void configEducationOrgUnitFields()
    {
        _educationOrgUnitUtil = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        UniEduProgramEducationOrgUnitAddon.Filters formativeOrgUnitFilter = UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT;
        if (_educationOrgUnitUtil != null)
        {
            _educationOrgUnitUtil.configSettings(this.getSettingsKey());
            _educationOrgUnitUtil.configUseFilters(
                    UniEduProgramEducationOrgUnitAddon.Filters.QUALIFICATION,
                    formativeOrgUnitFilter,
                    UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.PRODUCING_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_TECH
            );
            if (getOuHolder().getId() != null)
            {
                _educationOrgUnitUtil.configDisabledFilters(formativeOrgUnitFilter);
                _educationOrgUnitUtil.getSettings().set(formativeOrgUnitFilter.getSettingsName(), _ouHolder.getValue());
                _educationOrgUnitUtil.getValuesMap().put(formativeOrgUnitFilter, _ouHolder.getValue());
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportSfaSummaryResultList.EDUCATION_ORG_UNIT_UTIL_PARAM, _educationOrgUnitUtil);
        dataSource.put(SessionReportSfaSummaryResultList.EDUCATION_YEAR_PARAM, _uiSettings.get(SessionReportSfaSummaryResultList.EDUCATION_YEAR_PARAM));
    }

    // Listeners
    public abstract void onClickAdd();

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickPrint()
    {
        SfaSummaryResult examResult = DataAccessServices.dao().get(getListenerParameterAsLong());
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(examResult.getContent()), false);
    }

    public void saveSettings()
    {
        super.saveSettings();
        if (_educationOrgUnitUtil != null)
            _educationOrgUnitUtil.saveSettings();
    }

    @Override
    public void clearSettings()
    {
        super.clearSettings();
        if (_educationOrgUnitUtil != null)
            _educationOrgUnitUtil.clearFilters();
    }

    // Getters & Setters
    public OrgUnitHolder getOuHolder()
    {
        return _ouHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }

    @Override
    public ISecured getSecuredObject()
    {
        return getOuHolder().getId() == null ? super.getSecuredObject() : getOuHolder().getValue();
    }

    public abstract String getViewPermissionKey();

    public abstract String getAddPermissionKey();

    public abstract String getDeletePermissionKey();
}
