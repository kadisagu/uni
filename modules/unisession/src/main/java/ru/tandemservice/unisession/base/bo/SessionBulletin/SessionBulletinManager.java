package ru.tandemservice.unisession.base.bo.SessionBulletin;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unisession.base.bo.SessionBulletin.logic.ISessionBulletinThemeDAO;
import ru.tandemservice.unisession.base.bo.SessionBulletin.logic.SessionBulletinThemeDAO;

/**
 * @author avedernikov
 * @since 29.12.2015
 */
@Configuration
public class SessionBulletinManager extends BusinessObjectManager
{
	public static SessionBulletinManager instance()
	{
		return instance(SessionBulletinManager.class);
	}

	@Bean
	public ISessionBulletinThemeDAO themeDAO()
	{
		return new SessionBulletinThemeDAO();
	}
}
