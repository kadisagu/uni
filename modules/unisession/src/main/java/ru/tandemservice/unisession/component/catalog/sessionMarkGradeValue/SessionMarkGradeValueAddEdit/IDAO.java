package ru.tandemservice.unisession.component.catalog.sessionMarkGradeValue.SessionMarkGradeValueAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

/**
 * @author iolshvang
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<SessionMarkGradeValueCatalogItem,Model>
{
}
