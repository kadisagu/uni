package ru.tandemservice.unisession.entity.catalog;

import ru.tandemservice.unisession.entity.catalog.gen.SessionMarkGradeValueCatalogItemGen;

/**
 * Оценка (из шкалы оценок)
 */
public class SessionMarkGradeValueCatalogItem extends SessionMarkGradeValueCatalogItemGen
{
    public static final String CATALOG_CODE = SessionMarkGradeValueCatalogItem.ENTITY_NAME;
    @Override public String getCatalogCode() { return SessionMarkGradeValueCatalogItem.CATALOG_CODE; }
    @Override public boolean isCachedPositiveStatus() { return this.isPositive(); }
}