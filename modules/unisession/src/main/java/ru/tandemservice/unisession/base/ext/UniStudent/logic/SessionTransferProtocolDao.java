/* $Id$ */
package ru.tandemservice.unisession.base.ext.UniStudent.logic;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uni.base.bo.UniStudent.logic.sessionTransferProtocol.ISessionTransferProtocolDao;
import ru.tandemservice.uni.base.bo.UniStudent.logic.sessionTransferProtocol.ISessionTransferProtocolProperty;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 10.12.2015
 */
public class SessionTransferProtocolDao extends UniBaseDao implements ISessionTransferProtocolDao
{

    @Override
    public ISingleSelectModel getSessionTransferProtocolModel(final Student student)
    {
        return new CommonSingleSelectModel()
        {
            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                SessionTransferProtocolDocument protocol = (SessionTransferProtocolDocument) value;

                return "Протокол №" + protocol.getNumber()
                        + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(protocol.getProtocolDate());
            }

            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                String p_alias = "p";
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(SessionTransferProtocolDocument.class, p_alias)
                        .column(property(p_alias))
                        .where(eq(property(p_alias, SessionTransferProtocolDocument.request().student()), value(student)))
                        .order(property(p_alias, SessionTransferProtocolDocument.number()));

                if (o != null)
                    builder.where(eq(property(p_alias, SessionTransferProtocolDocument.id()), value((Long) o)));

                return new DQLListResultBuilder(builder);
            }
        };
    }

    @Override
    public PersonEduDocument getEduDocumentFromProtocol(String protocolNumber, Date protocolDate, Student student)
    {
        String alias = "p";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionTransferProtocolDocument.class, alias)
                .column(property(alias, SessionTransferProtocolDocument.request().eduDocument()))
                .where(eq(property(alias, SessionTransferProtocolDocument.number()), value(protocolNumber)))
                .where(eq(property(alias, SessionTransferProtocolDocument.protocolDate()), valueDate(protocolDate)))
                .where(eq(property(alias, SessionTransferProtocolDocument.request().student()), value(student)))
                .order(property(alias, SessionTransferProtocolDocument.id()), OrderDirection.desc)
                .top(1);

        return dql.createStatement(getSession()).uniqueResult();
    }

    @Override
    public ISessionTransferProtocolProperty getSessionTransferProtocolDocument(String protocolNumber, Date protocolDate, Student student)
    {
        String alias = "p";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionTransferProtocolDocument.class, alias)
                .column(property(alias))
                .where(eq(property(alias, SessionTransferProtocolDocument.number()), value(protocolNumber)))
                .where(eq(property(alias, SessionTransferProtocolDocument.protocolDate()), valueDate(protocolDate)))
                .where(eq(property(alias, SessionTransferProtocolDocument.request().student()), value(student)))
                .order(property(alias, SessionTransferProtocolDocument.id()), OrderDirection.desc)
                .top(1);

        return dql.createStatement(getSession()).uniqueResult();
    }
}