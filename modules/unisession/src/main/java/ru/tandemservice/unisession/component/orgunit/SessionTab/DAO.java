package ru.tandemservice.unisession.component.orgunit.SessionTab;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.UniBaseDao;

public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        final OrgUnit ou = model.getOrgUnitHolder().refresh(OrgUnit.class);
        model.setSecModel(new OrgUnitSecModel(ou));
    }
}
