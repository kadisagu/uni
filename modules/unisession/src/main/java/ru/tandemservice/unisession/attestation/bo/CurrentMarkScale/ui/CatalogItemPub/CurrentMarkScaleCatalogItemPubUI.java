/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.ui.CatalogItemPub;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.DynamicItemPub.CatalogDynamicItemPubUI;
import ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.CurrentMarkScaleManager;
import ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.ui.MarkAddEdit.CurrentMarkScaleMarkAddEdit;
import ru.tandemservice.unisession.attestation.bo.CurrentMarkScale.ui.MarkAddEdit.CurrentMarkScaleMarkAddEditUI;

/**
 * @author Nikolay Fedorovskih
 * @since 07.10.2014
 */
public class CurrentMarkScaleCatalogItemPubUI extends CatalogDynamicItemPubUI
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(CurrentMarkScaleCatalogItemPub.MARK_SCALE_PARAM, getCatalogItem());
    }

    public void onChangePositive()
    {
        CurrentMarkScaleManager.instance().dao().invertMarkPositive(getListenerParameterAsLong());
    }

    public void onDeleteEntityFromList()
    {
        CurrentMarkScaleManager.instance().dao().delete(getListenerParameterAsLong());
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(CurrentMarkScaleMarkAddEdit.class)
                .parameter(CurrentMarkScaleMarkAddEditUI.MARK_BIND_PARAM, getListenerParameterAsLong())
                .activate();
    }

    public void onClickAddMark()
    {
        _uiActivation.asRegionDialog(CurrentMarkScaleMarkAddEdit.class)
                .parameter(CurrentMarkScaleMarkAddEditUI.SCALE_BIND_PARAM, getCatalogItem().getId())
                .activate();
    }

    public void onClickToTop()
    {
        CurrentMarkScaleManager.instance().dao().moveToTopMarkPriority(getListenerParameterAsLong());
        getSupport().doRefresh();
    }

    public void onClickUp()
    {
        CurrentMarkScaleManager.instance().dao().moveUpMarkPriority(getListenerParameterAsLong());
        getSupport().doRefresh();
    }

    public void onClickDown()
    {
        CurrentMarkScaleManager.instance().dao().moveDownMarkPriority(getListenerParameterAsLong());
        getSupport().doRefresh();
    }

    public void onClickToBottom()
    {
        CurrentMarkScaleManager.instance().dao().moveToBottomMarkPriority(getListenerParameterAsLong());
        getSupport().doRefresh();
    }
}