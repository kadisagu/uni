package ru.tandemservice.unisession.entity.document;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisession.entity.document.gen.SessionGlobalDocumentGen;
import ru.tandemservice.unisession.sec.ISessionSecDao;

import java.util.Collection;
import java.util.Collections;

/**
 * Документ для массового ввода оценок
 *
 * Автоматически создается при выставлении оценок в рамках семестрового журнала, обеспечивает массовый ввод оценок.
 */
public class SessionGlobalDocument extends SessionGlobalDocumentGen
{
    @Override
    public OrgUnit getGroupOu()
    {
        return null;
    }

    @Override
    public String getTitle()
    {
        if (getEducationYear() == null) {
            return this.getClass().getSimpleName();
        }
        return "Семестровый журнал " + getEducationYear().getTitle() + ", " + getYearDistributionPart().getTitle();
    }

    @Override
    public String getTypeTitle()
    {
        return "Семестровый журнал " + getEducationYear().getTitle() + ", " + getYearDistributionPart().getTitle();
    }

    @Override
    public String getTypeShortTitle()
    {
        return "Сем. журнал " + getEducationYear().getTitle() + ", " + getYearDistributionPart().getShortTitle();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return ISessionSecDao.instance.get().getSecLocalEntities(this);
    }
}