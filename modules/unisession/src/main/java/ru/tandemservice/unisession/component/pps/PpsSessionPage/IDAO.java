/* $Id$ */
package ru.tandemservice.unisession.component.pps.PpsSessionPage;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

/**
 * @author oleyba
 * @since 4/21/11
 */
public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model>
{

}
