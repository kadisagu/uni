/* $*/

package ru.tandemservice.unisession.dao.comission;

import java.util.Collection;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unisession.entity.comission.SessionComission;

/**
 * @author oleyba
 * @since 3/3/11
 */
public interface ISessionCommissionDAO
{
    SpringBeanCache<ISessionCommissionDAO> instance = new SpringBeanCache<ISessionCommissionDAO>(ISessionCommissionDAO.class.getName());
    String CLEANUP_LOCKER_NAME = "sessionCommissionDAO.cleanup.lock";

    /**
     * Изменяет состав существующей комиссии, или создает новую, если commission = null
     * @param commission комиссия
     * @param ppsList состав преподавателей
     * @return комиссия - та же, если она была, или новая, если был null
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    SessionComission saveOrUpdateCommission(SessionComission commission, Collection<PpsEntry> ppsList);


    /**
     * Изменяет состав существующей комиссии, или создает новую, если commission = null
     * @param commission комиссия
     * @param ppsListIds состав преподавателей (ids)
     * @return комиссия - та же, если она была, или новая, если был null
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    SessionComission saveOrUpdateCommissionIds(SessionComission commission, Collection<Long> ppsListIds);

    /**
     *  убивает все неиспользуемые комиссии
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doCleanUp();
}
