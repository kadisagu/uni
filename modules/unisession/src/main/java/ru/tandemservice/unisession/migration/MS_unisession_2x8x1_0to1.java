package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unisession_2x8x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность unisessionDebtorsReport

		// создано свойство controlActionTypes
		{
			// создать колонку
			tool.createColumn("session_rep_debtors_t", new DBColumn("controlactiontypes_p", DBType.TEXT));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность unisessionGroupBulletinListReport

		// создано свойство controlActionTypes
		{
			// создать колонку
			tool.createColumn("session_rep_grp_bull_list_t", new DBColumn("controlactiontypes_p", DBType.TEXT));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность unisessionSummaryBulletinReport

		// создано свойство controlActionTypes
		{
			// создать колонку
			tool.createColumn("session_rep_summary_bull_t", new DBColumn("controlactiontypes_p", DBType.TEXT));

		}


    }
}