package ru.tandemservice.unisession.base.bo.SessionMark.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author vdanilov
 */
public interface ISessionMarkDaemonBean {

    SpringBeanCache<ISessionMarkDaemonBean> instance = new SpringBeanCache<ISessionMarkDaemonBean>(ISessionMarkDaemonBean.class.getName());

    /** обновляет слоты студентов в зачетках (из МСРП) */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    boolean doRefreshStudentGradeBookSlots();

    /** обновляет итоговые оценки студента
     * @param refreshAll полное обновление ВСЕХ активных итоговых оценок всех студентов
     **/
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    boolean doRefreshStudentFinalMarks(final boolean refreshAll);

    /** обновляет кэш в регулярных оценках */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    boolean doRefreshRegularMarkCacheFields();

    /** обновляет кэш в оценках-ссылках */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    boolean doRefreshLinkMarkCacheFields();



}
