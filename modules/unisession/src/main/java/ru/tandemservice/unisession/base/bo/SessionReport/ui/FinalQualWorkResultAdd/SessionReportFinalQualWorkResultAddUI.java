/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.FinalQualWorkResultAdd;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.IRootComponentActivationBuilder;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.FinalQualWorkResultPub.SessionReportFinalQualWorkResultPub;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.StateFinalAttestationResultAdd.SessionReportStateFinalAttestationResultAddUI;
import ru.tandemservice.unisession.entity.report.FinalQualWorkResult;

/**
 * @author Andrey Andreev
 * @since 03.11.2016
 */
@State({@Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id")})
public class SessionReportFinalQualWorkResultAddUI extends SessionReportStateFinalAttestationResultAddUI<FinalQualWorkResult>
{


    @Override
    public FinalQualWorkResult initReport()
    {
        return new FinalQualWorkResult();
    }

    @Override
    public void createStoredReport(FinalQualWorkResult report)
    {
        SessionReportManager.instance().finalQualWorkResultPrintDAO().createStoredReport(report, getOuHolder().getId());
    }

    @Override
    public void openReportPub(FinalQualWorkResult report)
    {
        IRootComponentActivationBuilder builder = getActivationBuilder()
                .asDesktopRoot(SessionReportFinalQualWorkResultPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId());
        if (getOuHolder().getId() != null)
            builder.parameter(SessionReportManager.BIND_ORG_UNIT, getOuHolder().getId());
        builder.activate();
    }

    @Override
    public String getAddPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_addFinalQualWorkResult" : "addSessionStorableReport");
    }
}
