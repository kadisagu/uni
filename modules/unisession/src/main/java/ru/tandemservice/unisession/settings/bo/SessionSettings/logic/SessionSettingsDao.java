/* $Id$ */
package ru.tandemservice.unisession.settings.bo.SessionSettings.logic;

import com.google.common.collect.Maps;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.Map;

import static ru.tandemservice.unisession.UnisessionDefines.*;

/**
 * @author Alexey Lopatin
 * @since 26.05.2015
 */
public class SessionSettingsDao extends UniBaseDao implements ISessionSettingsDao
{
    @Override
    public Map<String, Boolean> getSettingsFormationTransferMap()
    {
        Map<String, Boolean> resultMap = Maps.newLinkedHashMap();
        IDataSettings settings = DataSettingsFacade.getSettings(UNISESSION_SETTINGS);
        resultMap.put(SETTINGS_MARK_NEGATIVE_ITEM, getSettingsFormationTransferValue(settings, SETTINGS_MARK_NEGATIVE_ITEM));
        resultMap.put(SETTINGS_MARK_RETAKE_ITEM, getSettingsFormationTransferValue(settings, SETTINGS_MARK_RETAKE_ITEM));
        resultMap.put(SETTINGS_MARK_NOT_RETAKE_ITEM, getSettingsFormationTransferValue(settings, SETTINGS_MARK_NOT_RETAKE_ITEM));
        return resultMap;
    }

    private Boolean getSettingsFormationTransferValue(IDataSettings settings, String key)
    {
        Boolean value = settings.get(key);
        if (null == value)
        {
            settings.set(key, value = Boolean.TRUE);
            DataSettingsFacade.saveSettings(settings);
        }
        return value;
    }
}
