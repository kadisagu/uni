/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocPub;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.print.ISessionRetakeDocPrintDAO;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkState;

/**
 * @author oleyba
 * @since 6/15/11
 */
public class DAO extends UniBaseDao implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setRetakeDoc(this.getNotNull(SessionRetakeDocument.class, model.getRetakeDoc().getId()));
        model.setWithUnknownMarks(ISharedBaseDao.instance.get().existsEntity(
                SessionSlotMarkState.class,
                SessionSlotMarkState.slot().document().s(), model.getRetakeDoc(),
                SessionSlotMarkState.value().code().s(), SessionMarkStateCatalogItemCodes.NOT_APPEAR_UNKNOWN
        ));
		model.setThemeRequired(ISessionDocumentBaseDAO.instance.get().isThemeRequired(model.getRetakeDoc()));
    }

    public void doCloseBulletin(final Model model)
    {
        ISessionDocumentBaseDAO.instance.get().doCloseDocument(model.getRetakeDoc());
    }

    @Override
    public void doOpenBulletin(final Model model)
    {
        model.getRetakeDoc().setCloseDate(null);
        this.update(model.getRetakeDoc());
    }

    @Override
    public void doPrintBulletin(final Model model)
    {
        if ( model.getRetakeDoc().isClosed())
        {
            final SessionDocumentPrintVersion rel = this.get(SessionDocumentPrintVersion.class, SessionDocumentPrintVersion.doc().id().s(),  model.getRetakeDoc().getId());
            if (null != rel)
            {
                final byte[] content = rel.getContent();
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Ведомость.rtf").document(content), true);
                return;
            }
        }
        final RtfDocument document = ISessionRetakeDocPrintDAO.instance.get().printDoc(model.getRetakeDoc().getId());
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Ведомость.rtf").document(document), true);
    }

    @Override
    public void delete(final SessionRetakeDocument retakeDoc)
    {
        this.delete(retakeDoc.getId());
    }
}
