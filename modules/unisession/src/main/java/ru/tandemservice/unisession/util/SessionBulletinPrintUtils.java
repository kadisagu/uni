/* $Id$ */
package ru.tandemservice.unisession.util;

import org.tandemframework.core.entity.dsl.IPropertyPath;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.shared.person.catalog.entity.gen.EduLevelGen;
import ru.tandemservice.uni.entity.catalog.gen.DevelopFormGen;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 13.04.2016
 */
public class SessionBulletinPrintUtils
{
    // направления подготовки профессионального образования
    public static void printEduProgramSubject(RtfInjectModifier modifier, Set<EppStudentWpeCAction> dataSet)
    {
        final IPropertyPath subjectPath = EppStudentWpeCAction.studentWpe().student().educationOrgUnit().educationLevelHighSchool().educationLevel().programSubjectTitleWithCode();
        Set<String> titles = CommonBaseEntityUtil.getSortedPropertiesSet(dataSet, subjectPath, CommonCollator.COMMON_STRING_COMPARATOR);
        Iterator<String> iterator = titles.iterator();
        RtfString rtf = new RtfString();
        while (iterator.hasNext())
        {
            rtf.append(iterator.next());
            if (iterator.hasNext())
                rtf.par();
        }
        modifier.put("eduProgramSubject", rtf);

    }

    // направленности высшего профессионального образования
    public static void printEduProgramSpecialization(RtfInjectModifier modifier, Set<EppStudentWpeCAction> dataSet)
    {
        final IPropertyPath specPath = EppStudentWpeCAction.studentWpe().student().educationOrgUnit().educationLevelHighSchool().educationLevel().programSpecializationTitle();
        Set<String> titles = CommonBaseEntityUtil.getSortedPropertiesSet(dataSet, specPath, CommonCollator.COMMON_STRING_COMPARATOR);
        Iterator<String> iterator = titles.iterator();
        RtfString rtf = new RtfString();
        while (iterator.hasNext())
        {
            rtf.append(iterator.next());
            if (iterator.hasNext())
                rtf.par();
        }
        modifier.put("eduProgramSpecialization",titles.isEmpty() ?new RtfString().append("—") : rtf);
    }

    // уровни образования
    public static void printEduLevels(RtfInjectModifier modifier, Set<EppStudentWpeCAction> dataSet)
    {
        List<Student> studentList = dataSet.stream().map(e->e.getStudentWpe().getStudent()).collect(Collectors.toList());
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "st2epv")
                .joinPath(DQLJoinType.inner, EppStudent2EduPlanVersion.eduPlanVersion().fromAlias("st2epv"), "epv")
                .joinEntity("epv", DQLJoinType.inner, EppEduPlanProf.class, "plan",
                            eq(property("epv", EppEduPlanVersion.eduPlan()), property("plan")))
                .where(in(property("st2epv", EppStudent2EduPlanVersion.student()), studentList))
                .where(isNull(property("st2epv", EppStudent2EduPlanVersion.removalDate())))
                .column(property("plan", EppEduPlanProf.baseLevel()))
                .distinct();

        List<EduLevel> eduLevel = DataAccessServices.dao().getList(builder);
        final String eduLevels = eduLevel.stream()
                .sorted(CommonCatalogUtil.CATALOG_CODE_COMPARATOR)
                .map(EduLevelGen::getTitle)
                .collect(Collectors.joining(", "));

        modifier.put("eduLevel", eduLevels);
    }

    // формы обучения студентов
    public static void printDevelopForms(RtfInjectModifier modifier, Set<EppStudentWpeCAction> dataSet)
    {
        final String developForms = dataSet.stream()
                .map(printRow -> printRow.getStudentWpe().getStudent().getEducationOrgUnit().getDevelopForm())
                .distinct().sorted(CommonCatalogUtil.CATALOG_CODE_COMPARATOR)
                .map(DevelopFormGen::getTitle)
                .collect(Collectors.joining(", "));

        modifier.put("developForm", developForms);
    }
}