/* $Id$ */
package ru.tandemservice.unisession.component.log.SessionDocumentLogView;

import org.tandemframework.core.component.IBusinessComponent;

import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author oleyba
 * @since 6/28/11
 */
public class Controller extends ru.tandemservice.uni.component.log.EntityLogViewBase.Controller
{

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = component.getModel();
        model.setSettings(UniBaseUtils.getDataSettings(component, "logView.sessionDocument.filter."));
        this.getDao().prepare(model);
        this.prepareDataSource(component);
    }
}