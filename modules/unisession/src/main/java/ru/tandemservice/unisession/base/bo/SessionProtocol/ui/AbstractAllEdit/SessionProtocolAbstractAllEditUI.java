/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionProtocol.ui.AbstractAllEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unisession.base.bo.SessionProtocol.logic.ProtocolRow;
import ru.tandemservice.unisession.base.bo.SessionProtocol.logic.Utils;
import ru.tandemservice.unisession.base.bo.SessionProtocol.ui.AbstractList.SessionProtocolAbstractList;
import ru.tandemservice.unisession.entity.catalog.SessionRoleInGEC;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.List;

/**
 * @author Andrey Andreev
 * @since 12.09.2016
 */
public abstract class SessionProtocolAbstractAllEditUI extends UIPresenter
{
    protected Long _bulletinId;
    protected SessionBulletinDocument _bulletin;

    @Override
    public void onComponentRefresh()
    {
        ICommonDAO dao = DataAccessServices.dao();
        _bulletin = dao.get(SessionBulletinDocument.class, _bulletinId);

        dao.getList(SessionRoleInGEC.class).forEach(role -> {
            getDataSource().getColumn(role.getCode()).setLabel(role.getTitle());
        });

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionProtocolAbstractAllEdit.BULLETIN_ID_PARAM, _bulletinId);
        dataSource.put(SessionProtocolAbstractAllEdit.BULLETIN_PARAM, _bulletin);
    }

    // Listeners
    public void onClickApply()
    {
        ICommonDAO dao = DataAccessServices.dao();

        List<ProtocolRow> protocols = getDataSource().<ProtocolRow>getRecords();

        protocols.forEach(row -> row.update(dao));

        Utils.deleteUselessCommissions();

        deactivate();
    }

    // Getters and Setters
    public Long getBulletinId()
    {
        return _bulletinId;
    }

    public void setBulletinId(Long bulletinId)
    {
        this._bulletinId = bulletinId;
    }

    public ProtocolRow getCurrentRow()
    {
        return getDataSource().<ProtocolRow>getCurrent();
    }

    public List<PpsEntry> getCurrentMemberList()
    {
        BaseSearchListDataSource dataSource = getDataSource();
        ProtocolRow current = dataSource.<ProtocolRow>getCurrent();
        String name = dataSource.getCurrentColumn().getName();

        return current.getMemberList4Role(DataAccessServices.dao().getByCode(SessionRoleInGEC.class, name));
    }

    public void setCurrentMemberList(List<PpsEntry> ppsEntryList)
    {
        BaseSearchListDataSource dataSource = getDataSource();
        ProtocolRow current = dataSource.<ProtocolRow>getCurrent();
        String name = dataSource.getCurrentColumn().getName();

        current.getPpsListByRole().put(DataAccessServices.dao().getByCode(SessionRoleInGEC.class, name), ppsEntryList);
    }

    public BaseSearchListDataSource getDataSource()
    {
        return _uiConfig.getDataSource(SessionProtocolAbstractList.STATE_FINAL_EXAM_PROTOCOL_DS);
    }
}