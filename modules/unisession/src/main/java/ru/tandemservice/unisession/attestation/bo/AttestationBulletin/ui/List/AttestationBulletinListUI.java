/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.List;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.base.ui.OrgUnitUIPresenter;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.Add.AttestationBulletinAdd;
import ru.tandemservice.unisession.attestation.entity.SessionAttestation;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 1/24/12
 */
public class AttestationBulletinListUI extends OrgUnitUIPresenter
{
    public static final String BIND_EDU_YEAR = "year";
    public static final String BIND_YEAR_PART = "part";
    public static final String BIND_ATTESTATION = "attestation";

    // actions

    @Override
    public void onComponentRefresh()
    {
        SessionAttestation attestation = getAttestation();
        if (attestation != null && !attestation.getOrgUnit().equals(getOrgUnit()))
        {
            getSettings().set(BIND_ATTESTATION, findSimilar(attestation, getOrgUnit(), getSettings().get(BIND_EDU_YEAR), getSettings().get(BIND_YEAR_PART)));
            saveSettings();
        }
    }

    public void onSearchParamsChange()
    {
        onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(AttestationManager.PARAM_EDU_YEAR, getSettings().get(BIND_EDU_YEAR));
        dataSource.put(AttestationManager.PARAM_YEAR_PART, getSettings().get(BIND_YEAR_PART));
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, getSettings().get(BIND_EDU_YEAR));
        dataSource.put(AttestationBulletinManager.BIND_ATTESTATION, getAttestation());
        dataSource.put(AttestationBulletinManager.BIND_OWNER_ORGUNIT_LIST, getSettings().get("ownerOrgUnitList"));
        dataSource.put(AttestationBulletinManager.BIND_DISCIPLINE_LIST, getSettings().get("disciplineList"));
        dataSource.put(AttestationBulletinManager.BIND_COURSE_LIST, getSettings().get("courseList"));
        dataSource.put(AttestationBulletinManager.BIND_GROUP_LIST, getSettings().get("groupList"));
        dataSource.put(AttestationBulletinManager.BIND_DOCUMENT_NUMBER, getSettings().get("documentNumber"));
        dataSource.put(AttestationBulletinManager.BIND_BULL_STATUS, getSettings().get("bullStatus"));
        dataSource.put(AttestationBulletinManager.BIND_REGISTRY_STRYCTURE, getSettings().get("registryStructure"));
        dataSource.put(AttestationBulletinManager.BIND_STUDENT_LAST_NAME, getSettings().get("studentLastName"));
        dataSource.put(AttestationBulletinManager.BIND_PPS_LAST_NAME, getSettings().get("ppsLastName"));
    }

    public void onClickGenerate()
    {
        saveSettings();
        if (null == getAttestation())
            throw new ApplicationException("Чтобы сгенерировать ведомости, выберите в фильтрах аттестацию.");

        AttestationBulletinManager.instance().dao().doGenerateBulletins(getAttestation(), null);
    }

    public void onClickBulletinsAdd()
    {
        _uiActivation.asRegion(AttestationBulletinAdd.class)
                .parameter(SessionReportManager.BIND_ORG_UNIT, getOrgUnit().getId())
                .parameter(BIND_ATTESTATION, getAttestation())
                .top().activate();
    }

	public void onClickPrintSelectedBulletins()
	{
		Collection<IEntity> selected = getConfig().<BaseSearchListDataSource>getDataSource(AttestationBulletinList.DS_BULLETINS).getOptionColumnSelectedObjects(AttestationBulletinList.SELECT_COLUMN);
		if (selected.isEmpty())
			throw new ApplicationException("Необходимо выбрать хотя бы одну ведомость.");
		List<Long> bulletinIds = selected.stream().map(IEntity::getId).collect(Collectors.toList());
		RtfDocument doc = AttestationBulletinManager.instance().printDao().printBulletins(bulletinIds);
		BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().document(doc).fileName(getConfig().getProperty("ui.bulletinsFileName")), false);
	}

    public void onClickBulletinsDelete()
    {
        Collection<IEntity> selected = getConfig().<BaseSearchListDataSource>getDataSource(AttestationBulletinList.DS_BULLETINS).getOptionColumnSelectedObjects(AttestationBulletinList.SELECT_COLUMN);
        if (selected.isEmpty())
            throw new ApplicationException("Не выбраны ведомости для удаления.");

        Set<Long> idsToDelete = AttestationBulletinManager.instance().dao().getBulletinWithoutMarks(UniBaseDao.ids(selected));
        for (Long id : idsToDelete)
            DataAccessServices.dao().delete(id);

        ContextLocal.getInfoCollector().add("Из выбранных ведомостей (" + selected.size() + ") удалено: " + idsToDelete.size() + ", невозможно удалить: " + (selected.size() - idsToDelete.size()) + ".");
        selected.clear();
    }

    public void onClickPrint()
    {
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(AttestationBulletinManager.instance().printDao().printBulletin(getListenerParameterAsLong()), "Ведомость.rtf");
        getActivationBuilder()
            .asDesktopRoot(IUniComponents.PRINT_REPORT)
            .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "rtf"))
            .activate();
    }

    public void onDeleteEntityFromList()
    {
        AttestationBulletinManager.instance().dao().deleteBulletin(getListenerParameterAsLong());
    }
    
    public void onClickAutoMark()
    {
        ISessionBrsDao.instance.get().doFillAttestationMarkBasedOnCurrentRating(IUniBaseDao.instance.get().get(SessionAttestationBulletin.class, getListenerParameterAsLong()));
    }

    public boolean isAutoMarkEnabled()
    {
        return ISessionBrsDao.instance.get().isUseCurrentRating(getAttestation());
    }

    public boolean isAttestationClosed()
    {
        return getAttestation() != null && getAttestation().isClosed();
    }

    private Object findSimilar(SessionAttestation attestation, OrgUnit orgUnit, EducationYear year, YearDistributionPart part) {
        List<SessionAttestation> attestations = IUniBaseDao.instance.get().getList(new DQLSelectBuilder()
            .fromEntity(SessionAttestation.class, "a")
            .where(eq(property(SessionAttestation.sessionObject().orgUnit().fromAlias("a")), value(orgUnit)))
            .where(eq(property(SessionAttestation.sessionObject().educationYear().fromAlias("a")), value(year)))
            .where(eq(property(SessionAttestation.sessionObject().yearDistributionPart().fromAlias("a")), value(part)))
            .order(property(SessionAttestation.number().fromAlias("a")))
        );
        for (SessionAttestation similar : attestations) {
            if (similar.getNumber() == attestation.getNumber())
                return similar;
        }
        return attestations.isEmpty() ? null : attestations.get(0);
    }

    // preseneter

    public SessionAttestation getAttestation()
    {
        return getSettings().get(BIND_ATTESTATION);
    }

    public CommonPostfixPermissionModelBase getSecModel()
    {
        return getSec();
    }
}