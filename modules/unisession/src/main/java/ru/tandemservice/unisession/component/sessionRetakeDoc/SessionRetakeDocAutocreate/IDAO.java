/* $Id$ */
package ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocAutocreate;

import ru.tandemservice.uni.dao.IPrepareable;

/**
 * @author oleyba
 * @since 6/14/11
 */
public interface IDAO extends IPrepareable<Model>
{
    void doCreateDocuments(Model model);
}
