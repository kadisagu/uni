/* $Id: SessionReportGroupBulletinListList.java 21825 2012-02-09 09:08:57Z oleyba $ */
package ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupBulletinListList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.entity.report.UnisessionGroupBulletinListReport;

/**
 * @author oleyba
 * @since 12/16/11
 */
@Configuration
public class SessionReportGroupBulletinListList extends BusinessComponentManager
{
    public static final String DS_REPORTS = "sessionReportGroupBulletinListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
            .addDataSource(SessionReportManager.instance().eduYearDSConfig())
            .addDataSource(SessionReportManager.instance().yearPartDSConfig())
            .addDataSource(this.searchListDS(DS_REPORTS, this.sessionGroupBulletinListDS(), this.sessionGroupBulletinListDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint sessionGroupBulletinListDS() {
        return this.columnListExtPointBuilder(DS_REPORTS)
            .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")).create())
            .addColumn(publisherColumn("date", UnisessionGroupBulletinListReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
            .addColumn(textColumn("year", UnisessionGroupBulletinListReport.sessionObject().educationYear().title().s()).create())
            .addColumn(textColumn("part", UnisessionGroupBulletinListReport.sessionObject().yearDistributionPart().title().s()).create())
            .addColumn(textColumn("exec", UnisessionGroupBulletinListReport.executor().s()).create())
            .addColumn(textColumn("course", UnisessionGroupBulletinListReport.course().s()).create())
            .addColumn(textColumn("group", UnisessionGroupBulletinListReport.groups().s()).create())
            .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
            .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).permissionKey("ui:deleteStorableReportPermissionKey")
                .alert(FormattedMessage.with().template("sessionReportGroupBulletinListDS.delete.alert").parameter(UnisessionGroupBulletinListReport.formingDateStr().s()).create())
            )
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionGroupBulletinListDSHandler() {
        return new SessionGroupBulletinListDSHandler(this.getName());
    }
}
