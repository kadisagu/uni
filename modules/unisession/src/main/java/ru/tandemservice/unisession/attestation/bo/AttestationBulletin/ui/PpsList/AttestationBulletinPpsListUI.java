/* $Id$ */
package ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.PpsList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unisession.attestation.bo.Attestation.AttestationManager;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.AttestationBulletinManager;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationBulletin;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;

/**
 * @author oleyba
 * @since 1/24/12
 */
public class AttestationBulletinPpsListUI extends UIPresenter
{
    public static final String PARAM_ATT_NUMBER = "attestationNumber";

    private boolean isUseCurrentRating;

    @Override
    public void onComponentRefresh()
    {
    }

    public void onSearchParamsChange()
    {
        onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(AttestationManager.PARAM_EDU_YEAR, getSettings().get("year"));
        dataSource.put(AttestationManager.PARAM_YEAR_PART, getSettings().get("part"));
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, getSettings().get("year"));
        dataSource.put(AttestationBulletinManager.BIND_PPS_PERSON, PersonManager.instance().dao().getPerson(getUserContext().getPrincipalContext()));
        dataSource.put(AttestationBulletinManager.BIND_COURSE_LIST, getSettings().get("courseList"));
        dataSource.put(AttestationBulletinManager.BIND_GROUP_LIST, getSettings().get("groupList"));
        dataSource.put(AttestationBulletinManager.BIND_ATTESTATION_NUMBER, getSettings().get("attestationNumber"));
        dataSource.put(AttestationBulletinManager.BIND_OWNER_ORGUNIT_LIST, getSettings().get("ownerOrgUnitList"));
        dataSource.put(AttestationBulletinManager.BIND_DISCIPLINE_LIST, getSettings().get("disciplineList"));
        dataSource.put(AttestationBulletinManager.BIND_BULL_STATUS, getSettings().get("bullStatus"));
        dataSource.put(AttestationBulletinManager.BIND_DOCUMENT_NUMBER, getSettings().get("documentNumber"));
        dataSource.put(AttestationBulletinManager.BIND_REGISTRY_STRYCTURE, getSettings().get("registryStructure"));
        dataSource.put(AttestationBulletinManager.BIND_STUDENT_LAST_NAME, getSettings().get("studentLastName"));
    }

    public void onClickPrint()
    {
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(AttestationBulletinManager.instance().printDao().printBulletin(getListenerParameterAsLong()), "Ведомость.rtf");
        getActivationBuilder()
            .asDesktopRoot(IUniComponents.PRINT_REPORT)
            .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "rtf"))
            .activate();
    }

    public void onClickAutoMark()
    {
        ISessionBrsDao.instance.get().doFillAttestationMarkBasedOnCurrentRating(IUniBaseDao.instance.get().get(SessionAttestationBulletin.class, getListenerParameterAsLong()));
    }

//    public void onClickAddReport()
//    {
//        getActivationBuilder().asRegionDialog(SessionReportDebtorsAdd.class).parameter(SessionReportManager.BIND_ORG_UNIT, getOrgUnit().getId()).activate();
//    }
//
//    public void onClickPrint()
//    {
//        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT).parameter("reportId", getListenerParameterAsLong()).parameter("extension", "rtf").parameter("zip", Boolean.FALSE).activate();
//    }
//
//    public void onDeleteEntityFromList()
//    {
//        UniBaseDao.instance2.get().delete(getListenerParameterAsLong());
//    }


    public boolean isUseCurrentRating()
    {
        return isUseCurrentRating;
    }

    public void setUseCurrentRating(boolean useCurrentRating)
    {
        isUseCurrentRating = useCurrentRating;
    }
}