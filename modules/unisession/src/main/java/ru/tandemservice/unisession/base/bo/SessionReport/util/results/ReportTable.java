/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionReport.util.results;

import org.apache.commons.collections15.CollectionUtils;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.ResultsAdd.ISessionReportResultsDAO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
* @author oleyba
* @since 2/13/12
*/
public abstract class ReportTable implements ISessionReportResultsDAO.ISessionResultsReportTable
{
    protected List<ReportRow> rowList = new ArrayList<>();

    @Override
    public String[][] getTableData(List<ISessionReportResultsDAO.ISessionResultsReportColumn> columnList)
    {
        String[][] table = new String[rowList.size()][columnList.size()+1];
        for (int rowNum = 0; rowNum < rowList.size(); rowNum++) {
            final ReportRow reportRow = rowList.get(rowNum);
            table[rowNum][0] = reportRow.getTitle();
            for (int colNum = 0; colNum < columnList.size(); colNum++)
                table[rowNum][colNum+1] = columnList.get(colNum).cell(reportRow);
        }
        return table;
    }

    protected ReportRow totalsRow(Collection<ISessionReportResultsDAO.ISessionResultsReportStudentData> students)
    {
        return new ReportRow("Итого", CollectionUtils.select(students, input -> true));
    }
}
