package ru.tandemservice.unisession.component.orgunit.SessionBulletinListTab;

import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.unisession.base.bo.SessionBulletin.ui.Add.SessionBulletinAdd;
import ru.tandemservice.unisession.base.bo.SessionBulletin.ui.Add.SessionBulletinAddUI;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.dao.bulletin.ISessionBulletinDAO;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.print.ISessionBulletinPrintDAO;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final String settingsKey = "session.bulletins." + model.getOrgUnitId();
        model.setSettings(UniBaseUtils.getDataSettings(component, settingsKey));

        this.getDao().prepare(model);
        this.initYearAndPart(model);

        final DynamicListDataSource<SessionBulletinDocument> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().refreshDataSource(Controller.this.getModel(component1));
        });

        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new SimpleColumn("Дата\n формирования", SessionBulletinDocument.formingDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Ведомость", "title"));
        dataSource.addColumn(new MultiValuesColumn("Курс", "course").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new MultiValuesColumn("Группа\n (учебная)", "groups").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new MultiValuesColumn("Группа\n (текущая)", "currGroups").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Числo\n студентов", "studentCount").setClickable(false).setOrderable(false).setWidth(1));
        dataSource.addColumn(new MultiValuesColumn("Преподаватель", "comission").setFormatter(UniEppUtils.NEW_LINE_FORMATTER).setClickable(false).setOrderable(false).setWidth(20));
        dataSource.addColumn(new SimpleColumn("Дата\n сдачи", SessionBulletinDocument.performDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Дата\n закрытия", SessionBulletinDocument.closeDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setWidth(1));
        dataSource.addColumn(new ActionColumn("Выставить оценки", "edit_mark", "onClickMark").setPermissionKey(model.getSec().getPermission("markSessionBulletin")));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey(model.getSec().getPermission("printSessionBulletin")));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey(model.getSec().getPermission("editSessionBulletin")));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить ведомость №{0}?", SessionBulletinDocument.number().s()).setPermissionKey(model.getSec().getPermission("deleteSessionBulletin")));
        model.setDataSource(dataSource);
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onClickAutoCreateBulletins(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        CAFLegacySupportService.asDesktopRoot(SessionBulletinAdd.class.getSimpleName())
                .parameter(SessionReportManager.BIND_ORG_UNIT, model.getOrgUnitId())
                .parameter(SessionBulletinAddUI.PARAM_YEAR, model.getYear())
                .parameter(SessionBulletinAddUI.PARAM_YEAR_PART, model.getPart())
                .activate();
    }

    public void onClickMark(final IBusinessComponent component)
    {
        CAFLegacySupportService.asDesktopRoot(ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model.COMPONENT_NAME)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, component.<Long>getListenerParameter())
                .activate();
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinEdit.Model.COMPONENT_NAME,
            new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, component.<Long>getListenerParameter())));
    }

    public void onClickDelete(final IBusinessComponent component)
    {
        this.getDao().deleteBulletin(component.<Long>getListenerParameter());
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        this.getDao().doPrintBulletin(component.<Long>getListenerParameter());
    }

    public void onClickPrintSelected(final IBusinessComponent component)
    {
        final List<Long> selectedIds = UniBaseUtils.getIdList(((CheckboxColumn) this.getModel(component).getDataSource().getColumn("select")).getSelectedObjects());
        if (selectedIds.isEmpty()) {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }
        final RtfDocument document = ISessionBulletinPrintDAO.instance.get().printBulletinList(selectedIds);
        final byte[] content = RtfUtil.toByteArray(document);
        final Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Ведомость.rtf");
        this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", temporaryId).add("zip", Boolean.FALSE)));
    }

    public void onClickDeleteSelected(final IBusinessComponent component)
    {
        final List<Long> selectedIds = UniBaseUtils.getIdList(((CheckboxColumn) this.getModel(component).getDataSource().getColumn("select")).getSelectedObjects());
        if (selectedIds.isEmpty()) {
            throw new ApplicationException("Не выбраны ведомости для удаления.");
        }
        final Set<Long> idsToDelete = ISessionBulletinDAO.instance.get().findBulletinsToDelete(selectedIds);
        for (final Long id : idsToDelete) {
            UniDaoFacade.getCoreDao().delete(id);
        }
        ContextLocal.getInfoCollector().add("Из выбранных ведомостей ("+selectedIds.size()+") удалено: "+idsToDelete.size()+", невозможно удалить: "+(selectedIds.size()-idsToDelete.size())+".");
    }

    public void onSearchParamsChange(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        this.getDao().prepare(model);
    }


    private void initYearAndPart(final Model model)
    {
        if (null != model.getYear() && null != model.getPart()) {
            return;
        }

        final Object[] row = UniDaoFacade.getCoreDao().getCalculatedValue(session -> {
            final DQLSelectBuilder rootDql = new DQLSelectBuilder().fromEntity(SessionBulletinDocument.class, "b");
            rootDql.where(DQLExpressions.eq(DQLExpressions.property(SessionBulletinDocument.sessionObject().orgUnit().fromAlias("b")), DQLExpressions.value(model.getOrgUnit())));
            if (null != model.getYear()) {
                rootDql.where(DQLExpressions.eq(DQLExpressions.property(SessionBulletinDocument.sessionObject().educationYear().fromAlias("b")), DQLExpressions.value(model.getYear())));
            }

            rootDql.column(DQLExpressions.property(SessionBulletinDocument.sessionObject().educationYear().id().fromAlias("b")));
            rootDql.column(DQLExpressions.property(SessionBulletinDocument.sessionObject().yearDistributionPart().id().fromAlias("b")));
            rootDql.order(DQLExpressions.property(SessionBulletinDocument.formingDate().fromAlias("b")), OrderDirection.desc);
            final Iterator<Object[]> it = UniBaseDao.scrollRows(rootDql.createStatement(session)).iterator();
            if (it.hasNext()) { return it.next(); }
            return null;
        });

        if (null != row) {
            model.setYear(UniDaoFacade.getCoreDao().getNotNull(EducationYear.class, (Long)row[0]));
            model.setPart(UniDaoFacade.getCoreDao().getNotNull(YearDistributionPart.class, (Long)row[1]));
        }
    }
}
