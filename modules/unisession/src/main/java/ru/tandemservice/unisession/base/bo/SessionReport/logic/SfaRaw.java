/* $Id: $ */
package ru.tandemservice.unisession.base.bo.SessionReport.logic;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.entity.mark.SessionMark;

/**
 * @author Andrey Andreev
 * @since 08.12.2016
 */
public class SfaRaw
{
    protected EducationOrgUnit _educationOrgUnit;
    protected EppStudentWorkPlanElement _wpe;
    protected SessionMark _mark;


    public SfaRaw(Object[] raw)
    {
        _educationOrgUnit = (EducationOrgUnit) raw[0];
        _wpe = (EppStudentWorkPlanElement) raw[1];
        _mark = (SessionMark) raw[2];

    }

    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _educationOrgUnit.getFormativeOrgUnit();
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return _educationOrgUnit.getTerritorialOrgUnit();
    }

    public EducationLevels getEducationLevels()
    {
        return _educationOrgUnit.getEducationLevelHighSchool().getEducationLevel();
    }


    public EppStudentWorkPlanElement getWpe()
    {
        return _wpe;
    }

    public EppRegistryElement getRegistryElement()
    {
        return _wpe.getRegistryElementPart().getRegistryElement();
    }

    public Student getStudent()
    {
        return _wpe.getStudent();
    }


    public SessionMark getMark()
    {
        return _mark;
    }

    public void setMark(SessionMark mark)
    {
        _mark = mark;
    }
}