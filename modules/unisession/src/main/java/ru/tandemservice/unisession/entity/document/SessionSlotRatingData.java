package ru.tandemservice.unisession.entity.document;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.document.gen.SessionSlotRatingDataGen;

/**
 * Данные рейтинга для записи в документе сессии
 */
public class SessionSlotRatingData extends SessionSlotRatingDataGen
{
    @Override
    @EntityDSLSupport(parts = SessionSlotRatingData.P_FIXED_CURRENT_RATING_AS_LONG)
    public Double getFixedCurrentRating()
    {
        return getFixedCurrentRatingAsLong() == null ? null : ((double) getFixedCurrentRatingAsLong()) / 100;
    }

    public void setFixedCurrentRating(Double grade)
    {
        setFixedCurrentRatingAsLong(null == grade ? null : Math.round(grade * 100));
    }

    public ISessionBrsDao.IStudentCurrentRatingData getRatingData()
    {
        return new ISessionBrsDao.IStudentCurrentRatingData()
        {
            @Override public ISessionBrsDao.IRatingValue getRatingValue()
            {
                return new ISessionBrsDao.IRatingValue()
                {
                    @Override public String getMessage() { return null; }
                    @Override public Double getValue() { return getFixedCurrentRating(); }
                };
            }

            @Override public boolean isAllowed() { return SessionSlotRatingData.this.isAllowed(); }

            // для переопределения в проектном слое
            @Override public ISessionBrsDao.IRatingValue getRatingAdditParam(String key) { return null; }
        };
    }
}