package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unisession_2x11x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionSlotMarkGradeValue

		// создано обязательное свойство checkedGradeBook
		{
			// создать колонку
			tool.createColumn("session_mark_value_t", new DBColumn("checkedgradebook_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update session_mark_value_t set checkedgradebook_p = ? where checkedgradebook_p is null", false);

            // Для архивных студентов в состоянии "отчислен" (8), либо "зак. c дипломом" (3) выставляем значение по умолчанию - true
            String query = "update session_mark_value_t set checkedgradebook_p = ? where id in (" +
                    "select mv.id from session_mark_value_t mv " +
                    "inner join session_mark_t m on mv.id = m.id " +
                    "inner join session_doc_slot_t slot on m.slot_id = slot.id " +
                    "inner join epp_student_wpe_part_t wpe_p on slot.studentwpecaction_id = wpe_p.id " +
                    "inner join epp_student_wpe_t wpe on wpe_p.studentwpe_id = wpe.id " +
                    "inner join student_t st on wpe.student_id = st.id " +
                    "inner join studentstatus_t ss on st.status_id = ss.id " +
                    "where m.cachedmarkpositivestatus_p = ? and (st.archival_p = ? or ss.code_p = ? or ss.code_p = ?))";

            tool.executeUpdate(query, true, true, true, "3", "8");

			// сделать колонку NOT NULL
			tool.setColumnNullable("session_mark_value_t", "checkedgradebook_p", false);
		}
    }
}