/* $*/

package ru.tandemservice.unisession.component.orgunit.SessionListDocumentTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.base.bo.SessionListDocument.ui.Edit.SessionListDocumentEdit;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionListDocument;
import ru.tandemservice.unisession.entity.document.SessionSimpleDocument;
import ru.tandemservice.unisession.print.ISessionListDocumentPrintDAO;

/**
 * @author iolshvang
 * @since 12.04.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        model.setSettingsKey("session.sessionListDocument." + model.getOrgUnitId());
        model.setSettings(UniBaseUtils.getDataSettings(component, model.getSettingsKey()));

        final DynamicListDataSource<SessionListDocument> dataSource = UniBaseUtils.createDataSource(component, this.getDao());

        dataSource.addColumn(new SimpleColumn("Номер", SessionSimpleDocument.number()).setClickable(true).setOrderable(true));
        dataSource.addColumn(new PublisherLinkColumn("Студент", Student.person().identityCard().fullFio().s(), SessionListDocument.student().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вид затрат", SessionListDocument.student().compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Семестр", IDAO.PROP_TERM).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата\n выдачи", SessionSimpleDocument.formingDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(true));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey(model.getSec().getPermission("printSessionListDocument")));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey(model.getSec().getPermission("editSessionListDocument")));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить экз. карточку №{0}?", SessionSimpleDocument.number().s()).setPermissionKey(model.getSec().getPermission("deleteSessionListDocument")));

        model.setDataSource(dataSource);
    }

    public void onClickPrint(final IBusinessComponent component)
    {
        final RtfDocument document = ISessionListDocumentPrintDAO.instance.get().printSessionListDocument(component.<Long>getListenerParameter());
        final byte[] content = RtfUtil.toByteArray(document);
        final Integer temporaryId = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "ExamCard.rtf");
        this.activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap().add("id", temporaryId).add("zip", Boolean.FALSE)));
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onClickAddCard(final IBusinessComponent context)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentAddEdit.Model.COMPONENT_NAME,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, this.getModel(context).getOrgUnit().getId())));
    }

    public void onClickEdit(final IBusinessComponent context)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
            SessionListDocumentEdit.class.getSimpleName(),
            new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, context.<Long>getListenerParameter())));
    }

    public void onClickDelete(final IBusinessComponent context)
    {
        this.getDao().doDeleteCard(context.<Long>getListenerParameter());
    }

}