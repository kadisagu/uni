package ru.tandemservice.unisession.entity.document;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;

import ru.tandemservice.unisession.entity.document.gen.SessionSimpleDocumentGen;

/**
 * Экзаменационный лист, карточка
 */
public abstract class SessionSimpleDocument extends SessionSimpleDocumentGen
{

    @Override
    @EntityDSLSupport(parts = "number")
    public String getTitle() { return this.getNumber(); }

}