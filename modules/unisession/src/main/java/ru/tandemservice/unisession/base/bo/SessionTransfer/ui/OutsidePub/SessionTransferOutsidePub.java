/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsidePub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 10/19/11
 */
@Configuration
public class SessionTransferOutsidePub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder().create();
    }
}