package ru.tandemservice.unisession.component.catalog.sessionMarkStateCatalogItem.SessionMarkStateCatalogItemAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;

/**
 * @author AutoGenerator
 * Created on 15.02.2011
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<SessionMarkStateCatalogItem, Model>
{
}
