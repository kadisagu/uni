package ru.tandemservice.unisession.entity.mark.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Оценка студента в сессии (ссылка)
 *
 * Показывает, что данная оценка является копией другой оценки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionSlotLinkMarkGen extends SessionMark
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark";
    public static final String ENTITY_NAME = "sessionSlotLinkMark";
    public static final int VERSION_HASH = 734893743;
    private static IEntityMeta ENTITY_META;

    public static final String L_TARGET = "target";

    private SessionSlotRegularMark _target;     // Оценка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Оценка. Свойство не может быть null.
     */
    @NotNull
    public SessionSlotRegularMark getTarget()
    {
        return _target;
    }

    /**
     * @param target Оценка. Свойство не может быть null.
     */
    public void setTarget(SessionSlotRegularMark target)
    {
        dirty(_target, target);
        _target = target;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SessionSlotLinkMarkGen)
        {
            setTarget(((SessionSlotLinkMark)another).getTarget());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionSlotLinkMarkGen> extends SessionMark.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionSlotLinkMark.class;
        }

        public T newInstance()
        {
            return (T) new SessionSlotLinkMark();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "target":
                    return obj.getTarget();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "target":
                    obj.setTarget((SessionSlotRegularMark) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "target":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "target":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "target":
                    return SessionSlotRegularMark.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionSlotLinkMark> _dslPath = new Path<SessionSlotLinkMark>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionSlotLinkMark");
    }
            

    /**
     * @return Оценка. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark#getTarget()
     */
    public static SessionSlotRegularMark.Path<SessionSlotRegularMark> target()
    {
        return _dslPath.target();
    }

    public static class Path<E extends SessionSlotLinkMark> extends SessionMark.Path<E>
    {
        private SessionSlotRegularMark.Path<SessionSlotRegularMark> _target;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Оценка. Свойство не может быть null.
     * @see ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark#getTarget()
     */
        public SessionSlotRegularMark.Path<SessionSlotRegularMark> target()
        {
            if(_target == null )
                _target = new SessionSlotRegularMark.Path<SessionSlotRegularMark>(L_TARGET, this);
            return _target;
        }

        public Class getEntityClass()
        {
            return SessionSlotLinkMark.class;
        }

        public String getEntityName()
        {
            return "sessionSlotLinkMark";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
