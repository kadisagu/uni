package ru.tandemservice.unisession.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unisession_2x10x7_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.7")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionGECMember

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("sessiongecmember_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessiongecmember"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("commission_id", DBType.LONG),
				new DBColumn("roleingec_id", DBType.LONG).setNullable(false), 
				new DBColumn("pps_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sessionGECMember");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionGovExamCommission

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("sessiongovexamcommission_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessiongovexamcommission"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sessionGovExamCommission");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionStateFinalExamProtocol

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("sssnsttfnlexmprtcl_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_14c23a12"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("documentslot_id", DBType.LONG).setNullable(false), 
				new DBColumn("commission_id", DBType.LONG).setNullable(false), 
				new DBColumn("number_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("formingdate_p", DBType.TIMESTAMP).setNullable(false), 
				new DBColumn("comment_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sessionStateFinalExamProtocol");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionFQWProtocol

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("session_fqw_protocol_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessionfqwprotocol"), 
				new DBColumn("withhonors_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("consultant_id", DBType.LONG), 
				new DBColumn("source4theme_id", DBType.LONG), 
				new DBColumn("recommendation_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sessionFQWProtocol");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sessionStateExamProtocol

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("session_state_exam_protocol_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sessionstateexamprotocol")
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sessionStateExamProtocol");

		}


    }
}