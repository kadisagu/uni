/* $Id$ */
package ru.tandemservice.unisession.base.bo.SessionMark.ui.UnactualList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.SessionUnactualMarkDSHandler;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 10/14/11
 */
@Configuration
public class SessionMarkUnactualList extends BusinessComponentManager
{
    public static final String DS_SESSION_MARK_UNACTUAL = "sessionMarkUnactualDS";
    public static final String DS_SESSION_MARK_UNACTUAL_LIST_STUDENT = "sessionMarkUnactualListStudentDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .addDataSource(this.searchListDS(DS_SESSION_MARK_UNACTUAL, this.sessionMarkUnactualDS(), this.sessionMarkUnactualDSHandler()))
        .addDataSource(sessionMarkUnactualListStudentDSConfig())
        .create();
    }

    @Bean
    public ColumnListExtPoint sessionMarkUnactualDS() {
        final IPublisherLinkResolver resolver = new IPublisherLinkResolver() {
            @Override public Object getParameters(final IEntity entity) {
                return new ParametersMap()
                    .add(PublisherActivator.PUBLISHER_ID_KEY, (entity instanceof SessionMark) ? ((SessionMark) entity).getSlot().getActualStudent().getId() : null)
                    .add("selectedStudentTab", "studentNewSessionTab")
                    .add("selectedDataTab", "studentSessionDocumentTab")
                    ;
            }
            @Override public String getComponentName(final IEntity entity) {
                return "ru.tandemservice.uni.component.student.StudentPub";
            }
        };

        return this.columnListExtPointBuilder(DS_SESSION_MARK_UNACTUAL)
        .addColumn(textColumn("term", SessionMark.slot().termTitle().s()).create())
        .addColumn(publisherColumn("student", SessionMark.slot().studentWpeCAction().studentWpe().student().person().identityCard().fullFio().s()).publisherLinkResolver(resolver).create())
        .addColumn(textColumn("group", SessionMark.slot().studentWpeCAction().studentWpe().student().group().title().s()).create())
        .addColumn(textColumn("course", SessionMark.slot().studentWpeCAction().studentWpe().student().course().title().s()).create())
        .addColumn(textColumn("disc", SessionMark.slot().studentWpeCAction().studentWpe().registryElementPart().titleWithNumber().s()).create())
        .addColumn(textColumn("mark", SessionMark.valueTitle()).create())
        .addColumn(textColumn("date", SessionMark.performDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).create())
        .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert(FormattedMessage.with().template("sessionMarkUnactualDS.deleteAlert").create()).create())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionMarkUnactualDSHandler() {
        return new SessionUnactualMarkDSHandler(this.getName());
    }

    @Bean
    public UIDataSourceConfig sessionMarkUnactualListStudentDSConfig()
    {
        return SelectDSConfig.with(DS_SESSION_MARK_UNACTUAL_LIST_STUDENT, this.getName())
        .dataSourceClass(SelectDataSource.class)
        .addColumn("title", Student.titleWithFio().s())
        .handler(this.sessionMarkUnactualListStudentDSHandler())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionMarkUnactualListStudentDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), Student.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                final DQLSelectBuilder studentDQL = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "m").column("m.id")
                .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("m"), "s")
                .where(eq(property(SessionDocumentSlot.actualStudent().fromAlias("s")), property(alias)))
                .where(isNotNull(property(SessionDocumentSlot.studentWpeCAction().studentWpe().removalDate().fromAlias("s"))))
                .where(eq(property(SessionDocumentSlot.actualStudent().archival().fromAlias("s")), value(Boolean.FALSE)))
                .where(eq(property(SessionDocumentSlot.actualStudent().status().active().fromAlias("s")), value(Boolean.TRUE)))
                ;
                dql.where(exists(studentDQL.buildQuery()));
            }
        }
        .pageable(true)
        .order(Student.person().identityCard().fullFio())
        .filter(Student.person().identityCard().fullFio())
        ;
    }
}
